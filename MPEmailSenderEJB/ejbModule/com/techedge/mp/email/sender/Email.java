/*
 * JBoss, Home of Professional Open Source
 * Copyright 2012, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the 
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,  
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.techedge.mp.email.sender;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.email.sender.business.interfaces.Attachment;

/**
 * <p>
 * {@link Email} contains all the business logic for the application, and also serves as the
 * controller for the JSF view.
 * </p>
 * <p>
 * It contains address, subject, and content for the <code>email</code> to be sent.
 * </p>
 * <p>
 * The {@link #send()} method provides the business logic to send the email
 * </p>
 * 
 * @author Joel Tosi
 * 
 */

@Named
@SessionScoped
public class Email implements Serializable {
	
	private static final long serialVersionUID = 1544680932114626710L;
	
	/**
	 * Resource for sending the email.  The mail subsystem is defined in either standalone.xml or domain.xml in your 
	 * respective configuration directory. 
	 */
	@Resource (mappedName="java:jboss/mail/Gmail")
	private Session mySession;

	private String to;
	
	private String cc;
	
	private String bcc;
	
	private String from;
	
	private String subject;
	
	private String body;
	
	private List<Attachment> attachmentList = new ArrayList<Attachment>(0);
		
	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public List<Attachment> getAttachmentList() {
        return attachmentList;
    }

    public void setAttachmentList(List<Attachment> attachmentList) {
        this.attachmentList = attachmentList;
    }

    /**
	 * Method to send the email based upon values entered in the JSF view.  Exception should be handled in a production 
	 * usage but is not handled in this example.
	 * @throws Exception
	 */
	public void send(String host, String port, String proxyHost, String proxyPort, String proxyNoHosts) throws Exception
	{
		Session session = getSession(host, port);
		
		/*
		Message message = new MimeMessage(session);
		message.setFrom(new InternetAddress(from));
		Address toAddress= new InternetAddress(to);
		message.addRecipient(Message.RecipientType.TO, toAddress);
		message.setSubject(subject);
		message.setContent(body, "text/html; charset=\"utf-8\"");
		Transport.send(message);
		*/
		
		Proxy proxy = new Proxy(proxyHost, proxyPort, proxyNoHosts);
		proxy.setHttp();
		
		// creates a new e-mail message
        Message message = new MimeMessage(session);
 
        message.setFrom(new InternetAddress(from));
        
        // single recipient
        //Address toAddress= new InternetAddress(to);
        //message.addRecipient(Message.RecipientType.TO, toAddress);
        
        message.addRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
        
        if (cc != null) {
            message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(cc));
        }
        
        if (bcc != null) {
            message.addRecipients(Message.RecipientType.BCC, InternetAddress.parse(cc));
        }

        message.setSubject(subject);
 
        // creates message part
        MimeBodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setContent(body, "text/html; charset=\"utf-8\"");
 
        // creates multi-part
        Multipart multipart = new MimeMultipart();
        multipart.addBodyPart(messageBodyPart);
 
        // adds attachments
        if (attachmentList != null && !attachmentList.isEmpty() ) {
            
            for (Attachment attachment : attachmentList) {
            
                MimeBodyPart attachmentPart = null;
                
                try {
                    DataSource ds = new ByteArrayDataSource(attachment.getBytes(), "application/octet-stream");
                    attachmentPart = new MimeBodyPart();
                    attachmentPart.setDataHandler(new DataHandler(ds));
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                
                attachmentPart.setFileName(attachment.getFileName());
                
                multipart.addBodyPart(attachmentPart);
            }
        }
 
        // sets the multi-part as e-mail's content
        message.setContent(multipart, "multipart/mixed; charset=\"utf-8\"");
 
        // sends the e-mail
        Transport.send(message);
        
        proxy.unsetHttp();
        
	}
	
	
	private Session getSession(String host, String port) {
		//Authenticator authenticator = new Authenticator();

		Properties properties = new Properties();
		//properties.setProperty("mail.smtp.submitter", authenticator.getPasswordAuthentication().getUserName());
		//properties.setProperty("mail.smtp.auth", "true");
		
		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.smtp.port", port);

		//return Session.getInstance( properties, authenticator);
		return Session.getInstance( properties );
	}

	/*
	private class Authenticator extends javax.mail.Authenticator {
		private PasswordAuthentication authentication;

		public Authenticator() {
			String username = "enimpdev@techedgegroup.com";
			String password = "";
			authentication = new PasswordAuthentication(username, password);
		}

		protected PasswordAuthentication getPasswordAuthentication() {
			return authentication;
		}
	}
	*/
}
