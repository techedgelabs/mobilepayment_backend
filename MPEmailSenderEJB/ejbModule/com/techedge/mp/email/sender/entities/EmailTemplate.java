package com.techedge.mp.email.sender.entities;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import com.techedge.mp.email.sender.business.interfaces.EmailType;

public class EmailTemplate {

	private EmailType type;
	private String path;
	private String subject;
	
	public EmailTemplate() {}

	public EmailType getType() {
		return type;
	}
	public void setType(EmailType type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	public String getBody() {
		
		BufferedReader br = null;
		String currentLine = "";
		String body = "";
		
		String urlTemplate = System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "mail" + File.separator + this.path;
		
		try {
			br = new BufferedReader(new FileReader(urlTemplate));
			
			while ((currentLine = br.readLine()) != null) {
				body = body + currentLine;
			}
		} catch (IOException e) {
			System.out.println("Error opening template mail file: " + urlTemplate + " message: " + e.getMessage());
		} finally {
			try {
				if ( br != null) {
					br.close();
				}
			} catch (IOException e) {
				System.out.println("Error closing template mail file");
			}
		}
		
		if ( body.equals("") ) {
			
			// Utilizza un formato di default
			/*
			body += "Ciao %%NAME%%,\n";
			body += "\n";
			body += "stai per entrare nel mondo di Mobile Payment!\n";
			body += "\n";
			body += "Per completare l'iscrizione clicca sul link %%ACTIVATION_LINK%%?verificationCode=%%VERIFICATION_CODE%%&email=%%EMAIL%%";
			*/
			
			body += "<html>";
			body += "<head>";
			body += "<title></title>";
			body += "<meta content=\"text/html; charset=utf-8\" http-equiv=\"Content-Type\">";
			body += "<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">";
			body += "</head>";
			body += "<body style=\"margin: 0; padding: 0;\">";
			body += "<h1 style=\"margin: 0px; font-size: 26px; line-height: 36px; font-weight:normal;\">Ciao %%NAME%%,</h1>";
			body += "Per completare l�iscrizione clicca sul link<br/><a href=\"%%ACTIVATION_LINK%%verificationCode=%%VERIFICATION_CODE%%&email=%%EMAIL%%\" style=\"color: #c40e24; font-weight: bold;\">%%ACTIVATION_LINK%%verificationCode=%%VERIFICATION_CODE%%&email=%%EMAIL%%</a>";
			body += "</body>";
			body += "</html>";
			
		}
		
		return body;
	}
}
