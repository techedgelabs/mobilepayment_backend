package com.techedge.mp.email.sender.business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.email.sender.Email;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.email.sender.entities.EmailTemplate;


/**
 * Session Bean implementation class EmailSender
 */
@Stateless
@LocalBean
public class EmailSender implements EmailSenderRemote, EmailSenderLocal {

    private static final String     SEND_MAIL_SUCCESS            = "SEND_MAIL_200";
    private static final String     SEND_MAIL_FAILED             = "SEND_MAIL_300";
    private final static String     PARAM_MAIL_TEMPLATE_BASE_URL = "MAIL_TEMPLATE_BASE_URL";

    private String                  host                         = null;
    private String                  port                         = null;
    private String                  proxyHost                    = null;
    private String                  proxyPort                    = null;
    private String                  proxyNoHosts                 = null;
    private String                  sender                       = null;
    private String                  templateBaseUrl              = null;

    private List<EmailTemplate>     emailTemplateList            = new ArrayList<EmailTemplate>(0);

    private ParametersServiceRemote parametersService            = null;

    /**
     * Default constructor.
     */
    public EmailSender() {

        //String date = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());

        // Inizializzazione della lista dei template delle email
        EmailTemplate confirmEmailTemplate = new EmailTemplate();
        confirmEmailTemplate.setType(EmailType.CONFIRM_EMAIL);
        confirmEmailTemplate.setSubject("Eni Pay - conferma email");
        confirmEmailTemplate.setPath("conferma_mail.html");
        this.emailTemplateList.add(confirmEmailTemplate);

        EmailTemplate confirmEmailTemplateV2 = new EmailTemplate();
        confirmEmailTemplateV2.setType(EmailType.CONFIRM_EMAIL_V2);
        confirmEmailTemplateV2.setSubject("Eni Station + Conferma email");
        confirmEmailTemplateV2.setPath("v2/conferma_mail.html");
        this.emailTemplateList.add(confirmEmailTemplateV2);

        EmailTemplate confirmEmailTemplateBusiness = new EmailTemplate();
        confirmEmailTemplateBusiness.setType(EmailType.CONFIRM_EMAIL_BUSINESS);
        confirmEmailTemplateBusiness.setSubject("enistation P.Iva - Conferma email");
        confirmEmailTemplateBusiness.setPath("v2/conferma_mail_business.html");
        this.emailTemplateList.add(confirmEmailTemplateBusiness);

        EmailTemplate recoveryPasswordTemplate = new EmailTemplate();
        recoveryPasswordTemplate.setType(EmailType.RESCUE_PASSWORD_EMAIL);
        recoveryPasswordTemplate.setSubject("Eni Pay - recupero password");
        recoveryPasswordTemplate.setPath("recupero_password.html");
        this.emailTemplateList.add(recoveryPasswordTemplate);

        EmailTemplate recoveryPasswordTemplateV2 = new EmailTemplate();
        recoveryPasswordTemplateV2.setType(EmailType.RESCUE_PASSWORD_EMAIL_V2);
        recoveryPasswordTemplateV2.setSubject("Eni Station + Recupero password");
        recoveryPasswordTemplateV2.setPath("v2/recupero_password.html");
        this.emailTemplateList.add(recoveryPasswordTemplateV2);

        EmailTemplate recoveryPasswordTemplateBusiness = new EmailTemplate();
        recoveryPasswordTemplateBusiness.setType(EmailType.RESCUE_PASSWORD_EMAIL_BUSINESS);
        recoveryPasswordTemplateBusiness.setSubject("enistation P.Iva - Recupero password");
        recoveryPasswordTemplateBusiness.setPath("v2/recupero_password_business.html");
        this.emailTemplateList.add(recoveryPasswordTemplateBusiness);

        EmailTemplate transactionSummaryTemplate = new EmailTemplate();
        transactionSummaryTemplate.setType(EmailType.TRANSACTION_SUMMARY);
        transactionSummaryTemplate.setSubject("Eni Pay - transazione del ");
        transactionSummaryTemplate.setPath("transaction_summary.html");
        this.emailTemplateList.add(transactionSummaryTemplate);

        EmailTemplate transactionSummaryTemplateV2 = new EmailTemplate();
        transactionSummaryTemplateV2.setType(EmailType.TRANSACTION_SUMMARY_V2);
        transactionSummaryTemplateV2.setSubject("Eni Station + Transazione del ");
        transactionSummaryTemplateV2.setPath("/v2/transaction_summary.html");
        this.emailTemplateList.add(transactionSummaryTemplateV2);

        EmailTemplate mobilePhoneVerificationTemplate = new EmailTemplate();
        mobilePhoneVerificationTemplate.setType(EmailType.MOBILE_PHONE_VERIFICATION);
        mobilePhoneVerificationTemplate.setSubject("Eni Pay - verifica telefono cellulare");
        mobilePhoneVerificationTemplate.setPath("mobile_phone_verification.html");
        this.emailTemplateList.add(mobilePhoneVerificationTemplate);

        EmailTemplate mobilePhoneVerificationTemplateV2 = new EmailTemplate();
        mobilePhoneVerificationTemplateV2.setType(EmailType.MOBILE_PHONE_VERIFICATION_V2);
        mobilePhoneVerificationTemplateV2.setSubject("Eni Station + Verifica telefono cellulare");
        mobilePhoneVerificationTemplateV2.setPath("v2/mobile_phone_verification.html");
        this.emailTemplateList.add(mobilePhoneVerificationTemplateV2);
        
        EmailTemplate deviceVerificationTemplateV2 = new EmailTemplate();
        deviceVerificationTemplateV2.setType(EmailType.DEVICE_VERIFICATION_V2);
        deviceVerificationTemplateV2.setSubject("Eni Station + Verifica device");
        deviceVerificationTemplateV2.setPath("v2/device_verification.html");
        this.emailTemplateList.add(deviceVerificationTemplateV2);
        /*
        EmailTemplate postPaidReceiptTemplate = new EmailTemplate();
        postPaidReceiptTemplate.setType(EmailType.POSTPAID_REFUEL_RECAP);
        postPaidReceiptTemplate.setSubject("Eni Pay - ricevuta di acquisto");
        postPaidReceiptTemplate.setPath("riepilogo_refuel_postpaid.html");
        this.emailTemplateList.add(postPaidReceiptTemplate);

        EmailTemplate prePaidTransactionSuccessfulTemplate = new EmailTemplate();
        prePaidTransactionSuccessfulTemplate.setType(EmailType.PREPAID_TRANSACTION_SUCCESSFUL);
        prePaidTransactionSuccessfulTemplate.setSubject("Eni Pay - transazione del ");
        prePaidTransactionSuccessfulTemplate.setPath("prepaid_transaction_successful.html");
        this.emailTemplateList.add(prePaidTransactionSuccessfulTemplate);

        EmailTemplate prePaidTransactionNotAuthoizedTemplate = new EmailTemplate();
        prePaidTransactionNotAuthoizedTemplate.setType(EmailType.PREPAID_TRANSACTION_NOT_AUTHORIZED);
        prePaidTransactionNotAuthoizedTemplate.setSubject("Eni Pay - transazione del ");
        prePaidTransactionNotAuthoizedTemplate.setPath("prepaid_transaction_not_authorized.html");
        this.emailTemplateList.add(prePaidTransactionNotAuthoizedTemplate);

        EmailTemplate prePaidTransactionDeleteTemplate = new EmailTemplate();
        prePaidTransactionDeleteTemplate.setType(EmailType.PREPAID_TRANSACTION_DELETE);
        prePaidTransactionDeleteTemplate.setSubject("Eni Pay - transazione del ");
        prePaidTransactionDeleteTemplate.setPath("prepaid_transaction_delete.html");
        this.emailTemplateList.add(prePaidTransactionDeleteTemplate);

        EmailTemplate prePaidTransactionProcessingTemplate = new EmailTemplate();
        prePaidTransactionProcessingTemplate.setType(EmailType.PREPAID_TRANSACTION_PROCESSING);
        prePaidTransactionProcessingTemplate.setSubject("Eni Pay - transazione del ");
        prePaidTransactionProcessingTemplate.setPath("prepaid_transaction_processing.html");
        this.emailTemplateList.add(prePaidTransactionProcessingTemplate);

        EmailTemplate shopReceiptTemplate = new EmailTemplate();
        shopReceiptTemplate.setType(EmailType.SHOP_RECAP);
        shopReceiptTemplate.setSubject("Eni Pay - ricevuta rifornimento");
        shopReceiptTemplate.setPath("riepilogo_shop.html");
        this.emailTemplateList.add(shopReceiptTemplate);
        */
        EmailTemplate registrationCompleteTemplate = new EmailTemplate();
        registrationCompleteTemplate.setType(EmailType.CONFIRM_REGISTRATION);
        registrationCompleteTemplate.setSubject("Eni Pay - iscrizione completata");
        registrationCompleteTemplate.setPath("iscrizione_completata.html");
        this.emailTemplateList.add(registrationCompleteTemplate);

        EmailTemplate registrationCompleteTemplateV2 = new EmailTemplate();
        registrationCompleteTemplateV2.setType(EmailType.CONFIRM_REGISTRATION_V2);
        registrationCompleteTemplateV2.setSubject("Eni Station + Iscrizione completata");
        registrationCompleteTemplateV2.setPath("v2/iscrizione_completata.html");
        this.emailTemplateList.add(registrationCompleteTemplateV2);

        EmailTemplate registrationCompleteTemplateBusiness = new EmailTemplate();
        registrationCompleteTemplateBusiness.setType(EmailType.CONFIRM_REGISTRATION_BUSINESS);
        registrationCompleteTemplateBusiness.setSubject("enistation P.Iva - Iscrizione completata");
        registrationCompleteTemplateBusiness.setPath("v2/iscrizione_completata_business.html");
        this.emailTemplateList.add(registrationCompleteTemplateBusiness);

        EmailTemplate voucherReportTemplate = new EmailTemplate();
        voucherReportTemplate.setType(EmailType.VOUCHER_REPORT);
        voucherReportTemplate.setSubject("Eni Pay - report consumo voucher");
        voucherReportTemplate.setPath("voucher_report.html");
        this.emailTemplateList.add(voucherReportTemplate);

        EmailTemplate pendingTransactionReportTemplate = new EmailTemplate();
        pendingTransactionReportTemplate.setType(EmailType.PENDING_TRANSACTION_REPORT);
        pendingTransactionReportTemplate.setSubject("Eni Pay - report transazioni interrotte");
        pendingTransactionReportTemplate.setPath("pending_transaction_report.html");
        this.emailTemplateList.add(pendingTransactionReportTemplate);

        EmailTemplate pendingTransactionReportTemplateV2 = new EmailTemplate();
        pendingTransactionReportTemplateV2.setType(EmailType.PENDING_TRANSACTION_REPORT_V2);
        pendingTransactionReportTemplateV2.setSubject("Eni Station + Report transazioni interrotte");
        pendingTransactionReportTemplateV2.setPath("v2/pending_transaction_report.html");
        this.emailTemplateList.add(pendingTransactionReportTemplateV2);

        EmailTemplate weeklyTransactionReportTemplate = new EmailTemplate();
        weeklyTransactionReportTemplate.setType(EmailType.WEEKLY_TRANSACTION_REPORT);
        weeklyTransactionReportTemplate.setSubject("Eni Pay - transazioni e utenti");
        weeklyTransactionReportTemplate.setPath("riepilogo_transazioni_settimanali.html");
        this.emailTemplateList.add(weeklyTransactionReportTemplate);

        EmailTemplate weeklyTransactionReportTemplateV2 = new EmailTemplate();
        weeklyTransactionReportTemplateV2.setType(EmailType.WEEKLY_TRANSACTION_REPORT_V2);
        weeklyTransactionReportTemplateV2.setSubject("Eni Station + Transazioni e utenti");
        weeklyTransactionReportTemplateV2.setPath("v2/riepilogo_transazioni_settimanali.html");
        this.emailTemplateList.add(weeklyTransactionReportTemplateV2);

        EmailTemplate recoverUsernameTemplate = new EmailTemplate();
        recoverUsernameTemplate.setType(EmailType.RECOVER_USERNAME);
        recoverUsernameTemplate.setSubject("Eni Pay - recupero username");
        recoverUsernameTemplate.setPath("recupero_username.html");
        this.emailTemplateList.add(recoverUsernameTemplate);

        EmailTemplate recoverUsernameTemplateV2 = new EmailTemplate();
        recoverUsernameTemplateV2.setType(EmailType.RECOVER_USERNAME_V2);
        recoverUsernameTemplateV2.setSubject("Eni Station + Recupero username");
        recoverUsernameTemplateV2.setPath("v2/recupero_username.html");
        this.emailTemplateList.add(recoverUsernameTemplateV2);

        EmailTemplate recoverUsernameTemplateBusiness = new EmailTemplate();
        recoverUsernameTemplateBusiness.setType(EmailType.RECOVER_USERNAME_BUSINESS);
        recoverUsernameTemplateBusiness.setSubject("enistation P.Iva - Recupero username");
        recoverUsernameTemplateBusiness.setPath("v2/recupero_username_business.html");
        this.emailTemplateList.add(recoverUsernameTemplateBusiness);

        EmailTemplate reconciliationTransactionTemplate = new EmailTemplate();
        reconciliationTransactionTemplate.setType(EmailType.RECONCILIATION_TRANSACTION_REPORT);
        reconciliationTransactionTemplate.setSubject("Eni Pay - report delle transazioni riconciliate");
        reconciliationTransactionTemplate.setPath("reconciliation_transaction_report.html");
        this.emailTemplateList.add(reconciliationTransactionTemplate);

        EmailTemplate reconciliationTransactionTemplateV2 = new EmailTemplate();
        reconciliationTransactionTemplateV2.setType(EmailType.RECONCILIATION_TRANSACTION_REPORT_V2);
        reconciliationTransactionTemplateV2.setSubject("Eni Station + Report delle transazioni riconciliate");
        reconciliationTransactionTemplateV2.setPath("v2/reconciliation_transaction_report.html");
        this.emailTemplateList.add(reconciliationTransactionTemplateV2);

        EmailTemplate reconciliationUserTemplate = new EmailTemplate();
        reconciliationUserTemplate.setType(EmailType.RECONCILIATION_USER_REPORT);
        reconciliationUserTemplate.setSubject("Eni Pay - report delle operazioni riconciliate");
        reconciliationUserTemplate.setPath("reconciliation_user_report.html");
        this.emailTemplateList.add(reconciliationUserTemplate);

        EmailTemplate reconciliationUserTemplateV2 = new EmailTemplate();
        reconciliationUserTemplateV2.setType(EmailType.RECONCILIATION_USER_REPORT_V2);
        reconciliationUserTemplateV2.setSubject("Eni Station + Report delle operazioni riconciliate");
        reconciliationUserTemplateV2.setPath("v2/reconciliation_user_report.html");
        this.emailTemplateList.add(reconciliationUserTemplateV2);
        
        EmailTemplate reconciliationParkingTemplateV2 = new EmailTemplate();
        reconciliationParkingTemplateV2.setType(EmailType.RECONCILIATION_PARKING_REPORT_V2);
        reconciliationParkingTemplateV2.setSubject("Eni Station + Report delle transazioni di sosta riconciliate");
        reconciliationParkingTemplateV2.setPath("v2/reconciliation_parking_report.html");
        this.emailTemplateList.add(reconciliationParkingTemplateV2);

        EmailTemplate paymentMethodNotVerified = new EmailTemplate();
        paymentMethodNotVerified.setType(EmailType.PAYMENT_METHOD_NOT_VERIFIED);
        paymentMethodNotVerified.setSubject("Eni Pay - Esaurimento cap per utilizzo servizio con metodo di pagamento non verificato");
        paymentMethodNotVerified.setPath("payment_method_not_verified.html");
        this.emailTemplateList.add(paymentMethodNotVerified);

        EmailTemplate paymentMethodNotVerifiedV2 = new EmailTemplate();
        paymentMethodNotVerifiedV2.setType(EmailType.PAYMENT_METHOD_NOT_VERIFIED_V2);
        paymentMethodNotVerifiedV2.setSubject("Eni Station + Esaurimento cap per utilizzo servizio con metodo di pagamento non verificato");
        paymentMethodNotVerifiedV2.setPath("v2/payment_method_not_verified.html");
        this.emailTemplateList.add(paymentMethodNotVerifiedV2);

        EmailTemplate promotionWelcome = new EmailTemplate();
        promotionWelcome.setType(EmailType.PROMOTION_WELCOME);
        promotionWelcome.setSubject("Eni Pay - Voucher di benvenuto");
        promotionWelcome.setPath("promotion_welcome.html");
        this.emailTemplateList.add(promotionWelcome);

        EmailTemplate promotionMastercard = new EmailTemplate();
        promotionMastercard.setType(EmailType.PROMOTION_MASTERCARD);
        promotionMastercard.setSubject("Eni Pay - Voucher utenti Mastercard");
        promotionMastercard.setPath("promotion_mastercard.html");
        this.emailTemplateList.add(promotionMastercard);

        EmailTemplate dailyStatisticsReport = new EmailTemplate();
        dailyStatisticsReport.setType(EmailType.DAILY_STATISTICS_REPORT);
        dailyStatisticsReport.setSubject("Eni Pay - Report statistiche giornaliero");
        dailyStatisticsReport.setPath("daily_report.html");
        this.emailTemplateList.add(dailyStatisticsReport);

        EmailTemplate dailyStatisticsReportV2 = new EmailTemplate();
        dailyStatisticsReportV2.setType(EmailType.DAILY_STATISTICS_REPORT_V2);
        dailyStatisticsReportV2.setSubject("Eni Station + Report statistiche giornaliero");
        dailyStatisticsReportV2.setPath("v2/daily_report.html");
        this.emailTemplateList.add(dailyStatisticsReportV2);
        
        EmailTemplate errorDailyStatisticsReportV2 = new EmailTemplate();
        errorDailyStatisticsReportV2.setType(EmailType.ERROR_DAILY_STATISTICS_REPORT_V2);
        errorDailyStatisticsReportV2.setSubject("Eni Station + Elaborazione Report statistiche giornaliero");
        errorDailyStatisticsReportV2.setPath("v2/error_daily_report.html");
        this.emailTemplateList.add(errorDailyStatisticsReportV2);
        
        EmailTemplate dailyStatisticsReportBusiness = new EmailTemplate();
        dailyStatisticsReportBusiness.setType(EmailType.DAILY_STATISTICS_REPORT_BUSINESS);
        dailyStatisticsReportBusiness.setSubject("enistation P.Iva - Report statistiche giornaliero");
        dailyStatisticsReportBusiness.setPath("v2/daily_report_business.html");
        this.emailTemplateList.add(dailyStatisticsReportBusiness);

        EmailTemplate voucherSummaryTemplate = new EmailTemplate();
        voucherSummaryTemplate.setType(EmailType.VOUCHER_SUMMARY);
        voucherSummaryTemplate.setSubject("Eni Pay - acquisto voucher del ");
        voucherSummaryTemplate.setPath("voucher_summary.html");
        this.emailTemplateList.add(voucherSummaryTemplate);

        EmailTemplate dailyConsumeVoucherReport = new EmailTemplate();
        dailyConsumeVoucherReport.setType(EmailType.DAILY_CONSUME_VOUCHER_REPORT);
        dailyConsumeVoucherReport.setSubject("Eni Pay - Report giornaliero bruciatura voucher");
        dailyConsumeVoucherReport.setPath("daily_consume_voucher_report.html");
        this.emailTemplateList.add(dailyConsumeVoucherReport);

        EmailTemplate dailyConsumeVoucherReportV2 = new EmailTemplate();
        dailyConsumeVoucherReportV2.setType(EmailType.DAILY_CONSUME_VOUCHER_REPORT_V2);
        dailyConsumeVoucherReportV2.setSubject("Eni Station + Report giornaliero bruciatura voucher");
        dailyConsumeVoucherReportV2.setPath("v2/daily_consume_voucher_report.html");
        this.emailTemplateList.add(dailyConsumeVoucherReportV2);

        EmailTemplate refuelingCheckConnection = new EmailTemplate();
        refuelingCheckConnection.setType(EmailType.REFUELING_CHECK_CONNECTION);
        refuelingCheckConnection.setSubject("Eni Pay - Controllo connessione servizi Refueling");
        refuelingCheckConnection.setPath("refueling_check_connection.html");
        this.emailTemplateList.add(refuelingCheckConnection);

        EmailTemplate fidelityCheckConnection = new EmailTemplate();
        fidelityCheckConnection.setType(EmailType.FIDELITY_CHECK_CONNECTION);
        fidelityCheckConnection.setSubject("Eni Pay - Controllo connessione servizi Quenit");
        fidelityCheckConnection.setPath("fidelity_check_connection.html");
        this.emailTemplateList.add(fidelityCheckConnection);

        EmailTemplate forecourtCheckConnection = new EmailTemplate();
        forecourtCheckConnection.setType(EmailType.FORECOURT_CHECK_CONNECTION);
        forecourtCheckConnection.setSubject("Eni Pay - Controllo connessione servizi Fortech");
        forecourtCheckConnection.setPath("forecourt_check_connection.html");
        this.emailTemplateList.add(forecourtCheckConnection);

        EmailTemplate backendCheckConnection = new EmailTemplate();
        backendCheckConnection.setType(EmailType.BACKEND_CHECK_CONNECTION_V2);
        backendCheckConnection.setSubject("Eni Station + Controllo connessione servizi backend");
        backendCheckConnection.setPath("v2/backend_check_connection.html");
        this.emailTemplateList.add(backendCheckConnection);

        EmailTemplate newAppAvailable = new EmailTemplate();
        newAppAvailable.setType(EmailType.NEW_APP_AVAILABLE);
        newAppAvailable.setSubject("Eni Pay - E' disponibile una nuova versione dell'app");
        newAppAvailable.setPath("new_app_available.html");
        this.emailTemplateList.add(newAppAvailable);

        EmailTemplate newAppAvailableV2 = new EmailTemplate();
        newAppAvailableV2.setType(EmailType.NEW_APP_AVAILABLE_V2);
        newAppAvailableV2.setSubject("Eni Station + E' disponibile una nuova versione dell'app");
        newAppAvailableV2.setPath("v2/new_app_available.html");
        this.emailTemplateList.add(newAppAvailable);

        EmailTemplate voucherRewardAlert = new EmailTemplate();
        voucherRewardAlert.setType(EmailType.VOUCHER_REWARD_ALERT);
        voucherRewardAlert.setSubject("Eni Pay - Associazione Voucher Reward");
        voucherRewardAlert.setPath("voucher_reward_alert.html");
        this.emailTemplateList.add(voucherRewardAlert);

        EmailTemplate voucherRewardAlertV2 = new EmailTemplate();
        voucherRewardAlertV2.setType(EmailType.VOUCHER_REWARD_ALERT_V2);
        voucherRewardAlertV2.setSubject("Eni Station + Associazione Voucher Reward");
        voucherRewardAlertV2.setPath("v2/voucher_reward_alert.html");
        this.emailTemplateList.add(voucherRewardAlertV2);

        EmailTemplate crmOutboundSummary = new EmailTemplate();
        crmOutboundSummary.setType(EmailType.CRM_OUTBOUND_SUMMARY);
        crmOutboundSummary.setSubject("Eni Station + Esito CRM Outbound");
        crmOutboundSummary.setPath("v2/crm_outbound_summary.html");
        this.emailTemplateList.add(crmOutboundSummary);

        EmailTemplate weeklyStatisticsReportV2 = new EmailTemplate();
        weeklyStatisticsReportV2.setType(EmailType.WEEKLY_STATISTICS_REPORT_V2);
        weeklyStatisticsReportV2.setSubject("Eni Station + Report settimanale transazioni per PV");
        weeklyStatisticsReportV2.setPath("v2/weekly_report.html");
        this.emailTemplateList.add(weeklyStatisticsReportV2);
        
        EmailTemplate weeklyStatisticsReportV2Business = new EmailTemplate();
        weeklyStatisticsReportV2Business.setType(EmailType.WEEKLY_STATISTICS_REPORT_BUSINESS);
        weeklyStatisticsReportV2Business.setSubject("Eni Station P.Iva - Report settimanale transazioni per PV");
        weeklyStatisticsReportV2Business.setPath("v2/weekly_report_business.html");
        this.emailTemplateList.add(weeklyStatisticsReportV2Business);
        
        EmailTemplate monthlyMyCiceroRefuelingReport = new EmailTemplate();
        monthlyMyCiceroRefuelingReport.setType(EmailType.MONTHLY_MYCICERO_REFUELING_REPORT);
        monthlyMyCiceroRefuelingReport.setSubject("Eni Station + Report transazioni Pi� Servito utenti MyCicero");
        monthlyMyCiceroRefuelingReport.setPath("v2/monthly_mycicero_refueling_report.html");
        this.emailTemplateList.add(monthlyMyCiceroRefuelingReport);
        
        EmailTemplate updateStationsList = new EmailTemplate();
        updateStationsList.setType(EmailType.UPDATE_STATIONS_LIST);
        updateStationsList.setSubject("Eni Station + Report aggiornamento pv");
        updateStationsList.setPath("v2/update_stations_list.html");
        this.emailTemplateList.add(updateStationsList);
        
        EmailTemplate requestResetPin = new EmailTemplate();
        requestResetPin.setType(EmailType.REQUEST_RESET_PIN_V2);
        requestResetPin.setSubject("Eni Station + Reset pin");
        requestResetPin.setPath("v2/reset_pin.html");
        this.emailTemplateList.add(requestResetPin);
        
        EmailTemplate welcomeVodafoneBlack = new EmailTemplate();
        welcomeVodafoneBlack.setType(EmailType.WELCOME_VODAFONE);
        welcomeVodafoneBlack.setSubject("Benvenuto nell�iniziativa Eni Station + per \"Happy Black\"");
        welcomeVodafoneBlack.setPath("v2/dem_welcome_vodafone.html");
        this.emailTemplateList.add(welcomeVodafoneBlack);
        
        EmailTemplate winnerVodafoneBlack = new EmailTemplate();
        winnerVodafoneBlack.setType(EmailType.WINNER_VODAFONE);
        winnerVodafoneBlack.setSubject("Ecco il tuo voucher carburante da 5 �");
        winnerVodafoneBlack.setPath("v2/dem_winner_vodafone.html");
        this.emailTemplateList.add(winnerVodafoneBlack);
    }

    @Override
    public void init(String host, String port, String proxyHost, String proxyPort, String proxyNoHosts, String from) {

        this.host = host;
        this.port = port;
        this.sender = from;

        if (proxyHost != null) {
            this.proxyHost = proxyHost;
            this.proxyNoHosts = proxyNoHosts;
        }

        if (proxyHost != null && proxyPort != null) {
            this.proxyPort = proxyPort;
        }
        
        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
            this.templateBaseUrl = parametersService.getParamValue(EmailSender.PARAM_MAIL_TEMPLATE_BASE_URL);
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Errore nella inizializzazione del servizio Parameters: " + e.getMessage());
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Errore nella inizializzazione dei parametri: " + e.getMessage());
        }
    }

    @Override
    public String sendEmail(EmailType type, String from, String to, String cc, String bcc, String subject, List<Parameter> parameters) {

        // Ottieni il template 
        EmailTemplate emailTemplate = getEmailTemplateByType(type);
        if (emailTemplate == null) {
            System.out.println("Il template " + type.toString() + " non esiste");
            return EmailSender.SEND_MAIL_FAILED;
        }

        // Genera il body dell'email
        String emailBody = generateEmailBody(emailTemplate, parameters);
        if (emailBody == null) {
            System.out.println("Errore nella generazione del body dell'email");
            return EmailSender.SEND_MAIL_FAILED;
        }

        // Creazione dell'oggetto Email
        Email email = new Email();

        if (from != null) {
            email.setFrom(from);
        }
        else {
            email.setFrom(this.sender);
        }

        email.setTo(to);
        /*
        if (cc != null) {
            email.setCc(cc);
        }
        
        if (bcc != null) {
            email.setBcc(bcc);
        }
        */
        if (subject == null) {
            subject = emailTemplate.getSubject();
        }

        email.setSubject(subject);
        email.setBody(emailBody);

        try {
            email.send(this.host, this.port, this.proxyHost, this.proxyPort, this.proxyNoHosts);
        }
        catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
            return EmailSender.SEND_MAIL_FAILED;
        }

        return EmailSender.SEND_MAIL_SUCCESS;
    }

    @Override
    public String sendEmail(EmailType type, String from, String to, List<Parameter> parameters) {
        return sendEmail(type, from, to, null, null, null, parameters);
    }

    @Override
    public String sendEmail(EmailType type, String to, List<Parameter> parameters) {
        return sendEmail(type, null, to, null, null, null, parameters);
    }

    @Override
    public String sendEmailWithAttachments(EmailType type, String from, String to, String cc, String bcc, String subject, List<Parameter> parameters, List<Attachment> attachments) {

        // Ottieni il template 
        EmailTemplate emailTemplate = getEmailTemplateByType(type);
        if (emailTemplate == null) {
            System.out.println("Il template " + type.toString() + " non esiste");
            return EmailSender.SEND_MAIL_FAILED;
        }

        // Genera il body dell'email
        String emailBody = generateEmailBody(emailTemplate, parameters);
        if (emailBody == null) {
            System.out.println("Errore nella generazione del body dell'email");
            return EmailSender.SEND_MAIL_FAILED;
        }

        // Creazione dell'oggetto Email
        Email email = new Email();

        if (from != null) {
            email.setFrom(from);
        }
        else {
            email.setFrom(this.sender);
        }

        email.setTo(to);
        /*
        if (cc != null) {
            email.setCc(cc);
        }
        
        if (bcc != null) {
            email.setBcc(bcc);
        }
        */
        if (subject == null) {
            subject = emailTemplate.getSubject();
        }

        email.setSubject(subject);
        email.setBody(emailBody);

        if (attachments != null) {
        	for (Attachment attachment : attachments) {

            	attachment.setBytes(attachment.getBytes());
            	attachment.setFileName(attachment.getFileName());

            	email.getAttachmentList().add(attachment);
        	}
        }

        try {
            email.send(this.host, this.port, this.proxyHost, this.proxyPort, this.proxyNoHosts);
        }
        catch (Exception ex) {
            System.out.println("Error: " + ex.getMessage());
            ex.printStackTrace();
            return EmailSender.SEND_MAIL_FAILED;
        }

        return EmailSender.SEND_MAIL_SUCCESS;
    }

    @Override
    public String sendEmailWithAttachments(EmailType type, String from, String to, String subject, List<Parameter> parameters, List<Attachment> attachments) {
        return sendEmailWithAttachments(type, from, to, null, null, subject, parameters, attachments);
    }

    @Override
    public String sendEmailWithAttachments(EmailType type, String to, List<Parameter> parameters, List<Attachment> attachments) {
        return sendEmailWithAttachments(type, null, to, null, null, null, parameters, attachments);
    }

    @Override
    public String sendEmailWithAttachments(EmailType type, String to, String subject, List<Parameter> parameters, List<Attachment> attachments) {
        return sendEmailWithAttachments(type, null, to, null, null, subject, parameters, attachments);
    }

    private EmailTemplate getEmailTemplateByType(EmailType type) {

        for (EmailTemplate emailTemplate : this.emailTemplateList) {

            if (emailTemplate.getType().equals(type)) {

                return emailTemplate;
            }
        }

        return null;
    }

    private String generateEmailBody(EmailTemplate emailTemplate, List<Parameter> parameters) {

        String body = emailTemplate.getBody();
        
        if (this.templateBaseUrl != null) {
            body = body.replaceAll("%%TEMPLATE_BASE_URL%%", this.templateBaseUrl);
        }
        
        if (parameters == null) {
            return body;
        }

        for (Parameter parameter : parameters) {

            //System.out.println( "Parameter name: " + parameter.getName() + ", vaule: " + parameter.getValue() );

            if (parameter.getName() != null && body.contains("%%" + parameter.getName() + "%%")) {

                System.out.println("found");

                String value = "";
                if (parameter.getValue() != null) {
                    value = parameter.getValue();
                }

                body = body.replaceAll("%%" + parameter.getName() + "%%", value);
            }
        }

        //System.out.println("body: " + body);

        return body;
    }

    @Override
    public String getSender() {
        return this.sender;
    }

}
