package com.techedge.mp.email.sender.business;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;

public class EJBHomeCache {

    final String parametersServiceRemoteJndi  = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
    final String loggerServiceRemoteJndi      = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
    
    private static EJBHomeCache instance;
    
    protected Context context = null;
    
    protected ParametersServiceRemote parametersService = null;
    protected LoggerServiceRemote     loggerService     = null;
    
    private EJBHomeCache( ) throws InterfaceNotFoundException {
        
        final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();
        
        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
        
        try {
            context = new InitialContext(jndiProperties);
        } catch (NamingException e) {

            throw new InterfaceNotFoundException("Naming exception: " + e.getMessage());
        }
    }

    public static synchronized EJBHomeCache getInstance() throws InterfaceNotFoundException
    {
        if (instance == null)
            instance = new EJBHomeCache();
        
        return instance;
    }

    public ParametersServiceRemote getParametersService() throws InterfaceNotFoundException
    {
        if (parametersService == null) {
            try {
                this.parametersService = (ParametersServiceRemote) context.lookup(parametersServiceRemoteJndi);
            }
            catch (Exception ex) {
                throw new InterfaceNotFoundException(ex.getMessage());
            }
        }
        
        return parametersService;
    }
    
    public LoggerServiceRemote getLoggerService() throws InterfaceNotFoundException
    {
        if (loggerService == null) {
            try {
                this.loggerService = (LoggerServiceRemote)context.lookup(loggerServiceRemoteJndi);
            }
            catch (Exception ex) {
                throw new InterfaceNotFoundException(ex.getMessage());
            }
        }

        return loggerService;
    }
        
}
