package com.techedge.mp.email.test;

import java.util.Arrays;

import org.junit.Test;

import com.techedge.mp.email.sender.Email;
import com.techedge.mp.email.sender.business.interfaces.Attachment;

public class EmailTester {

    @Test
    public void testSend() {
        //fail("Not yet implemented");
        Email email = new Email();
        
        email.setFrom("luca.mancini@techedgegroup.com");
        email.setTo("luca.mancini@techedgegroup.com");
        email.setSubject("test");
        
        email.setBody("test");
        
        Attachment attachment = new Attachment();
        
        byte[] bytes = new byte[100];
        Arrays.fill( bytes, (byte) 0 );
        
        attachment.setBytes(bytes);
        attachment.setFileName("prova.xlsx");
        
        email.getAttachmentList().add(attachment);
        
        try {
            email.send("mpsmtp.enimp.pri", "25", "mpsquid.enimp.pri", "3128", null);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
