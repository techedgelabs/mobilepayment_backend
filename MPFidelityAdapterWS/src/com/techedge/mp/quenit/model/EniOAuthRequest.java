package com.techedge.mp.quenit.model;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import org.scribe.exceptions.OAuthConnectionException;
import org.scribe.exceptions.OAuthException;
import org.scribe.model.OAuthRequest;
import org.scribe.model.Verb;

public class EniOAuthRequest extends OAuthRequest {

    HttpURLConnection    connection;
    private String       payload                       = null;
    private byte[]       bytePayload                   = null;
    private boolean      connectionKeepAlive           = false;
    private boolean      followRedirects               = true;
    private Long         connectTimeout                = null;
    private Long         readTimeout                   = null;
    private final String CONTENT_LENGTH                = "Content-Length";
    private final String CONTENT_TYPE                  = "Content-Type";
    private final String ACCEPT_CHARSET                = "Accept-Charset";
    private final String DEFAULT_CONTENT_TYPE          = "text/xml";
    private final String DEFAULT_CHARSET               = "UTF-8";
    private final String DEFAULT_KEY_STORE_TYPE        = "JKS";
    private final String DEFAULT_KEY_MANAGER_ALGORITHM = "SunX509";
    private final String DEFAULT_SSL_VERSION           = "TLS";

    public EniOAuthRequest(String url) {
        super(Verb.POST, url);
    }

    public EniResponse send(String keyStore, String keyStorePassword, String keyPassword, String KeyStoreType, String KeyManagerAlgorithm, String SSLVersion) {
        try {
            createConnection(keyStore, keyStorePassword, keyPassword, KeyStoreType, KeyManagerAlgorithm, SSLVersion);
            return doSend();
        }
        catch (Exception e) {
            throw new OAuthConnectionException(e);
        }
    }

    public EniResponse send(String keyStore, String keyStorePassword, String keyPassword) {
        return send(keyStore, keyStorePassword, keyPassword, DEFAULT_KEY_STORE_TYPE, DEFAULT_KEY_MANAGER_ALGORITHM, DEFAULT_SSL_VERSION);
    }

    public EniResponse send(String keyStore, String keyStorePassword) {
        return send(keyStore, keyStorePassword, keyStorePassword);
    }

    private void createConnection(String keyStore, String keyStorePassword, String keyPassword, String KeyStoreType, String KeyManagerAlgorithm, String SSLVersion)
            throws NoSuchAlgorithmException, KeyStoreException, CertificateException, FileNotFoundException, IOException, UnrecoverableKeyException, KeyManagementException {
        String completeUrl = getCompleteUrl();
        URL url = new URL(completeUrl);
        if (connection == null) {

            if (url.getProtocol().equals("https")) {
                SSLContext sslcontext = SSLContext.getInstance(SSLVersion);
                KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerAlgorithm);
                KeyStore ks = KeyStore.getInstance(KeyStoreType);
                ks.load(new FileInputStream(keyStore), keyStorePassword.toCharArray());
                kmf.init(ks, keyPassword.toCharArray());

                /*
                 * TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                 * tmf.init(ks);
                 * TrustManager[] tm = tmf.getTrustManagers();
                 */
                TrustManager[] tm = null;
                sslcontext.init(kmf.getKeyManagers(), tm, null);
                SSLSocketFactory sslSocketFactory = sslcontext.getSocketFactory();
                HttpsURLConnection.setDefaultSSLSocketFactory(sslSocketFactory);
                connection = (HttpsURLConnection) url.openConnection();
                connection.setInstanceFollowRedirects(followRedirects);
            }
            else {
                connection = (HttpURLConnection) new URL(completeUrl).openConnection();
            }

            System.setProperty("http.keepAlive", connectionKeepAlive ? "true" : "false");
            connection.setInstanceFollowRedirects(followRedirects);
        }
    }

    private EniResponse doSend() throws IOException {
        connection.setRequestMethod(getVerb().name());
        if (connectTimeout != null) {
            connection.setConnectTimeout(connectTimeout.intValue());
        }
        if (readTimeout != null) {
            connection.setReadTimeout(readTimeout.intValue());
        }
        addHeaders(connection);

        addBody(connection, getByteBodyContents());

        return new EniResponse(connection);
    }

    private void addHeaders(HttpURLConnection conn) {
        for (String key : getHeaders().keySet())
            conn.setRequestProperty(key, getHeaders().get(key));
    }

    private void addBody(HttpURLConnection conn, byte[] content) throws IOException {
        conn.setRequestProperty(CONTENT_LENGTH, String.valueOf(content.length));

        if (conn.getRequestProperty(ACCEPT_CHARSET) == null) {
            conn.setRequestProperty(ACCEPT_CHARSET, DEFAULT_CHARSET);
        }
        // Set default content type if none is set.
        if (conn.getRequestProperty(CONTENT_TYPE) == null) {
            conn.setRequestProperty(CONTENT_TYPE, DEFAULT_CONTENT_TYPE);
        }
        conn.setDoOutput(true);
        conn.getOutputStream().write(content);
    }

    private byte[] getByteBodyContents() {
        if (bytePayload != null)
            return bytePayload;
        String body = (payload != null) ? payload : getBodyParams().asFormUrlEncodedString();
        try {
            return body.getBytes(getCharset());
        }
        catch (UnsupportedEncodingException uee) {
            throw new OAuthException("Unsupported Charset: " + getCharset(), uee);
        }
    }

    public void addPayload(String payload) {
        this.payload = payload;
    }

    public void addPayload(byte[] payload) {
        this.bytePayload = payload.clone();
    }

    public boolean isConnectionKeepAlive() {
        return connectionKeepAlive;
    }

    public void setConnectionKeepAlive(boolean connectionKeepAlive) {
        this.connectionKeepAlive = connectionKeepAlive;
    }

    public boolean isFollowRedirects() {
        return followRedirects;
    }

    public void setFollowRedirects(boolean followRedirects) {
        this.followRedirects = followRedirects;
    }

    public Long getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(Long connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public Long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(Long readTimeout) {
        this.readTimeout = readTimeout;
    }

    @Override
    public String toString() {
        return String.format("@EniOAuthRequest(%s)", getUrl());
    }

}
