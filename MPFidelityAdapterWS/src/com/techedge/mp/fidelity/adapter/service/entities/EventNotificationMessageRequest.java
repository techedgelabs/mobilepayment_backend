package com.techedge.mp.fidelity.adapter.service.entities;

import java.math.BigInteger;
import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventNotificationMessageRequest", propOrder = { "eventType", "srcTransactionID", "requestTimestamp", "fiscalCode", "sessionID", "panCode", "cardStatus",
        "stationID", "terminalID", "transactionResult", "transactionDate", "credits", "balance", "balanceAmount", "marketingMsg", "warningMsg", "operationType", "subtype",
        "loyaltyTransactionDetail", "rewardDetail" })
public class EventNotificationMessageRequest {

    @XmlElement(required = true)
    private EventType                eventType;
    @XmlElement(required = true)
    private String                   fiscalCode;
    @XmlElement(required = true)
    private String                   srcTransactionID;
    @XmlElement(required = true)
    private Long                     requestTimestamp;
    @XmlElement(required = false)
    private LoyaltyTransactionDetail loyaltyTransactionDetail;
    @XmlElement(required = false)
    private RewardDetail             rewardDetail;
    @XmlElement(required = false)
    private String                   sessionID;
    @XmlElement(required = true)
    private String                   panCode;
    @XmlElement(required = true)
    private CardStatus               cardStatus;
    @XmlElement(required = true)
    private String                   stationID;
    @XmlElement(required = false, nillable = true)
    private String                   terminalID;
    @XmlElement(required = true)
    private String                   transactionResult;
    @XmlElement(required = false)
    private Date                     transactionDate;
    @XmlElement(required = false)
    private BigInteger               credits;
    @XmlElement(required = true)
    private BigInteger               balance;
    @XmlElement(required = true)
    private BigInteger               balanceAmount;
    @XmlElement(required = false, nillable = true)
    private String                   marketingMsg;
    @XmlElement(required = false, nillable = true)
    private String                   warningMsg;
    @XmlElement(required = true)
    private String                   operationType;
    @XmlElement(required = false)
    private String                   subtype;

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public LoyaltyTransactionDetail getLoyaltyTransactionDetail() {
        return loyaltyTransactionDetail;
    }

    public void setLoyaltyTransactionDetail(LoyaltyTransactionDetail loyaltyTransactionDetail) {
        this.loyaltyTransactionDetail = loyaltyTransactionDetail;
    }

    public RewardDetail getRewardDetail() {
        return rewardDetail;
    }

    public void setRewardDetail(RewardDetail rewardDetail) {
        this.rewardDetail = rewardDetail;
    }

    public String getSessionID() {
        return sessionID;
    }

    public void setSessionID(String sessionID) {
        this.sessionID = sessionID;
    }

    public CardStatus getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(CardStatus cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getPanCode() {
        return panCode;
    }

    public void setPanCode(String panCode) {
        this.panCode = panCode;
    }

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getTerminalID() {
        return terminalID;
    }

    public void setTerminalID(String terminalID) {
        this.terminalID = terminalID;
    }

    public String getTransactionResult() {
        return transactionResult;
    }

    public void setTransactionResult(String transactionResult) {
        this.transactionResult = transactionResult;
    }

    public Date getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(Date transactionDate) {
        this.transactionDate = transactionDate;
    }

    public BigInteger getCredits() {
        return credits;
    }

    public void setCredits(BigInteger credits) {
        this.credits = credits;
    }

    public BigInteger getBalance() {
        return balance;
    }

    public void setBalance(BigInteger balance) {
        this.balance = balance;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public BigInteger getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(BigInteger balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }
}
