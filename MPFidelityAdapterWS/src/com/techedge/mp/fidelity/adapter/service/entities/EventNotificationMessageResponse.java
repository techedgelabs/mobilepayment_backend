package com.techedge.mp.fidelity.adapter.service.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventNotificationMessageResponse", propOrder = { "statusCode", "messageCode" })
public class EventNotificationMessageResponse {

    @XmlElement(required = true, nillable = false)
    protected String statusCode;
    @XmlElement(required = true, nillable = false)
    protected String messageCode;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

}
