package com.techedge.mp.fidelity.adapter.service.entities;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nonOilDetail", propOrder = { "productID", "productDescription", "amount", "credits" })
public class NonOilDetail {

    @XmlElement(required = false)
    private String     productID;
    @XmlElement(required = false)
    private String     productDescription;
    @XmlElement(required = true)
    private BigDecimal amount;
    @XmlElement(required = true)
    private BigInteger credits;

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigInteger getCredits() {
        return credits;
    }

    public void setCredits(BigInteger credits) {
        this.credits = credits;
    }

}
