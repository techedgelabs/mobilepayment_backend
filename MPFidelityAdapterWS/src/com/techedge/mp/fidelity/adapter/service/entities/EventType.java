package com.techedge.mp.fidelity.adapter.service.entities;

public enum EventType {
    LOYALTY("LOYALTY"),
    REWARD("REWARD");
    
    private String code;
    
    private EventType(String code) {
        this.code = code;
    }
    
    public String getCode() {
        return code;
    }
    
}
