package com.techedge.mp.fidelity.adapter.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import com.techedge.mp.core.business.EventNotificationServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.CheckTransactionResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MulticardStatusNotificationResponse;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.loyalty.EventNotification;
import com.techedge.mp.core.business.interfaces.loyalty.EventNotificationType;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransaction;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionNonOil;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionRefuel;
import com.techedge.mp.core.business.interfaces.loyalty.RewardTransaction;
import com.techedge.mp.core.business.interfaces.loyalty.RewardTransactionParameter;
import com.techedge.mp.fidelity.adapter.service.entities.CheckPaymentMessageRequest;
import com.techedge.mp.fidelity.adapter.service.entities.CheckPaymentMessageResponse;
import com.techedge.mp.fidelity.adapter.service.entities.CheckSessionMessageRequest;
import com.techedge.mp.fidelity.adapter.service.entities.CheckSessionMessageResponse;
import com.techedge.mp.fidelity.adapter.service.entities.DpanStatus;
import com.techedge.mp.fidelity.adapter.service.entities.DpanStatusEnum;
import com.techedge.mp.fidelity.adapter.service.entities.EventNotificationMessageRequest;
import com.techedge.mp.fidelity.adapter.service.entities.EventNotificationMessageResponse;
import com.techedge.mp.fidelity.adapter.service.entities.LoyaltyTransactionDetail;
import com.techedge.mp.fidelity.adapter.service.entities.MulticardRefuelingStatusNotificationMessageRequest;
import com.techedge.mp.fidelity.adapter.service.entities.MulticardRefuelingStatusNotificationMessageResponse;
import com.techedge.mp.fidelity.adapter.service.entities.MulticardStatusNotificationMessageRequest;
import com.techedge.mp.fidelity.adapter.service.entities.MulticardStatusNotificationMessageResponse;
import com.techedge.mp.fidelity.adapter.service.entities.NonOilDetail;
import com.techedge.mp.fidelity.adapter.service.entities.ParameterDetails;
import com.techedge.mp.fidelity.adapter.service.entities.RefuelDetail;
import com.techedge.mp.fidelity.adapter.service.entities.RewardDetail;
import com.techedge.mp.fidelity.adapter.service.entities.TransactionMessageDetail;
import com.techedge.mp.fidelity.adapter.service.utilities.ResponseHelper;

//@HandlerChain(file = "handler-chain.xml")
@WebService()
public class FidelityAdapter {

    private Properties                     prop                     = new Properties();

    private LoggerServiceRemote            loggerService            = null;
    private UserServiceRemote              userService              = null;
    private EventNotificationServiceRemote eventNotificationService = null;

    public FidelityAdapter() {
        initService();
    }

    private void initService() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("response_service.properties");

        try {
            prop.load(inputStream);
            if (inputStream == null) {
                throw new FileNotFoundException("property file response_service.properties not found in the classpath");
            }
        }
        catch (IOException e) {
            System.err.println("File properties is not available: " + e.getMessage());
        }
    }

    private void log(ErrorLevel level, String methodName, String groupId, String phaseId, String message) {
        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, this.getClass().getSimpleName(), methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {
            System.err.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    @WebMethod(operationName = "checkSession")
    @WebResult(name = "checkSessionResponse")
    public @XmlElement(required = true)
    CheckSessionMessageResponse checkSession(@XmlElement(required = true) @WebParam(name = "checkSessionRequest") CheckSessionMessageRequest checkSessionMessageRequest) {

        /*
         * Set<Pair<String, String>> inputParameters = new HashSet<Pair<String,
         * String>>(); inputParameters.add(new Pair<String,
         * String>("operationID", checkSessionMessageRequest.getOperationID()));
         * if (checkSessionMessageRequest.getRequestTimestamp() != null) {
         * inputParameters.add(new Pair<String, String>("requestTimestamp",
         * checkSessionMessageRequest.getRequestTimestamp().toString())); } else
         * { inputParameters.add(new Pair<String, String>("requestTimestamp",
         * "null")); } inputParameters.add(new Pair<String, String>("sessionID",
         * checkSessionMessageRequest.getSessionID())); inputParameters.add(new
         * Pair<String, String>("fiscalCode",
         * checkSessionMessageRequest.getFiscalCode()));
         * 
         * log(ErrorLevel.DEBUG, "checkSession",
         * checkSessionMessageRequest.getOperationID(), "opening",
         * ActivityLog.createLogMessage(inputParameters));
         */

        CheckSessionMessageResponse checkSessionMessageResponse = new CheckSessionMessageResponse();

        Boolean errorFound = false;

        if (checkSessionMessageRequest.getOperationID() == null || checkSessionMessageRequest.getOperationID().trim().isEmpty()) {

            checkSessionMessageResponse.setOperationID("null");
            checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
            checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));

            errorFound = true;

            log(ErrorLevel.INFO, "checkSession", checkSessionMessageRequest.getOperationID(), null, "OperationID null or empty");

        }
        else {

            if (checkSessionMessageRequest.getOperationID().length() != 33) {

                if (checkSessionMessageRequest.getOperationID().length() > 33) {
                    checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID().substring(0, 33));
                }
                else {
                    checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
                }
                checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
                checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));

                errorFound = true;

                log(ErrorLevel.INFO, "checkSession", checkSessionMessageRequest.getOperationID(), null, "OperationID invalid length");
            }
        }

        if (!errorFound) {

            if (checkSessionMessageRequest.getSessionID() == null || checkSessionMessageRequest.getSessionID().trim().isEmpty()) {

                checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
                checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
                checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));

                errorFound = true;

                log(ErrorLevel.INFO, "checkSession", checkSessionMessageRequest.getOperationID(), null, "SessionID null or empty");

            }
            else {

                if (checkSessionMessageRequest.getSessionID().length() != 32) {

                    checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
                    checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
                    checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));

                    errorFound = true;

                    log(ErrorLevel.INFO, "checkSession", checkSessionMessageRequest.getOperationID(), null, "SessionID invalid length");
                }
            }
        }

        if (!errorFound) {

            if (checkSessionMessageRequest.getFiscalCode() == null || checkSessionMessageRequest.getFiscalCode().trim().isEmpty()) {

                checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
                checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
                checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));

                errorFound = true;

                log(ErrorLevel.INFO, "checkSession", checkSessionMessageRequest.getOperationID(), null, "FiscalCode null or empty");

            }
            else {

                /* Controllo disattivato per gestire gli utenti MyMulticard che valorizzano il campo fiscalCode con l'email
                if (checkSessionMessageRequest.getFiscalCode().length() != 16) {

                    checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
                    checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
                    checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));

                    errorFound = true;

                    log(ErrorLevel.INFO, "checkSession", checkSessionMessageRequest.getOperationID(), null, "FiscalCode invalid length");
                }
                */
            }
        }

        if (!errorFound) {

            try {
                this.userService = EJBHomeCache.getInstance().getUserService();

                String response = userService.checkLoyaltySession(checkSessionMessageRequest.getSessionID(), checkSessionMessageRequest.getFiscalCode());
                checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());

                if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.USER_CHECK_LOYALTY_SESSION_SUCCESS)) {
                    checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_SUCCESS);
                    checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_SUCCESS));
                }
                else {
                    checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_FAILURE);
                    checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_FAILURE));
                }
            }
            catch (InterfaceNotFoundException ex) {
                log(ErrorLevel.ERROR, "checkSession", checkSessionMessageRequest.getOperationID(), null, ex.getMessage());
                checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
                checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_SYSTEM_ERROR);
                checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_SYSTEM_ERROR));
            }
        }

        /*
         * Set<Pair<String, String>> outputParameters = new HashSet<Pair<String,
         * String>>(); outputParameters.add(new Pair<String,
         * String>("operationID",
         * checkSessionMessageResponse.getOperationID()));
         * outputParameters.add(new Pair<String, String>("statusCode",
         * checkSessionMessageResponse.getStatusCode()));
         * outputParameters.add(new Pair<String, String>("statusMessage",
         * checkSessionMessageResponse.getMessageCode()));
         * 
         * log(ErrorLevel.INFO, "checkSession",
         * checkSessionMessageRequest.getOperationID(), "closing",
         * ActivityLog.createLogMessage(outputParameters));
         */
        return checkSessionMessageResponse;
    }

    @WebMethod(operationName = "eventNotification")
    @WebResult(name = "eventNotificationResponse")
    public @XmlElement(required = true)
    EventNotificationMessageResponse eventNotification(
            @XmlElement(required = true) @WebParam(name = "eventNotificationRequest") EventNotificationMessageRequest eventNotificationMessageRequest) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        String eventType = "";

        if (eventNotificationMessageRequest.getEventType() != null) {
            eventType = eventNotificationMessageRequest.getEventType().getCode();
        }

        inputParameters.add(new Pair<String, String>("eventType", eventType));
        inputParameters.add(new Pair<String, String>("fiscalCode", eventNotificationMessageRequest.getFiscalCode()));
        if (eventNotificationMessageRequest.getRequestTimestamp() != null) {
            inputParameters.add(new Pair<String, String>("requestTimestamp", eventNotificationMessageRequest.getRequestTimestamp().toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("requestTimestamp", ""));
        }
        inputParameters.add(new Pair<String, String>("srcTransactionID", eventNotificationMessageRequest.getSrcTransactionID()));
        inputParameters.add(new Pair<String, String>("sessionID", eventNotificationMessageRequest.getSessionID()));
        if (eventNotificationMessageRequest.getCardStatus() != null) {
            inputParameters.add(new Pair<String, String>("cardStatus", eventNotificationMessageRequest.getCardStatus().getStatus()));
        }
        else {
            inputParameters.add(new Pair<String, String>("cardStatus", ""));
        }
        inputParameters.add(new Pair<String, String>("panCode", eventNotificationMessageRequest.getPanCode()));
        inputParameters.add(new Pair<String, String>("stationID", eventNotificationMessageRequest.getStationID()));
        inputParameters.add(new Pair<String, String>("terminalID", eventNotificationMessageRequest.getTerminalID()));
        inputParameters.add(new Pair<String, String>("transactionResult", eventNotificationMessageRequest.getTransactionResult()));
        if (eventNotificationMessageRequest.getTransactionDate() != null) {
            inputParameters.add(new Pair<String, String>("transactionDate", eventNotificationMessageRequest.getTransactionDate().toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("transactionDate", ""));
        }
        inputParameters.add(new Pair<String, String>("transactionResult", eventNotificationMessageRequest.getTransactionResult()));
        if (eventNotificationMessageRequest.getCredits() != null) {
            inputParameters.add(new Pair<String, String>("credits", eventNotificationMessageRequest.getCredits().toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("credits", ""));
        }
        if (eventNotificationMessageRequest.getMarketingMsg() != null) {
            inputParameters.add(new Pair<String, String>("marketingMsg", eventNotificationMessageRequest.getMarketingMsg()));
        }
        else {
            inputParameters.add(new Pair<String, String>("marketingMsg", ""));
        }
        if (eventNotificationMessageRequest.getWarningMsg() != null) {
            inputParameters.add(new Pair<String, String>("warningMsg", eventNotificationMessageRequest.getWarningMsg()));
        }
        else {
            inputParameters.add(new Pair<String, String>("warningMsg", ""));
        }
        if (eventNotificationMessageRequest.getLoyaltyTransactionDetail() != null) {
            LoyaltyTransactionDetail loyaltyTransactionDetail = eventNotificationMessageRequest.getLoyaltyTransactionDetail();
            String message = "{ " + " paymentMode: " + loyaltyTransactionDetail.getPaymentMode() + ", refuelDetail: ";

            if (loyaltyTransactionDetail.getRefuelDetail() != null) {
                message += " { ";

                for (RefuelDetail refuelDetail : loyaltyTransactionDetail.getRefuelDetail()) {
                    String messageTmp = "{ " + "refuelMode: " + refuelDetail.getRefuelMode() + ", pumpNumber: "
                            + (refuelDetail.getPumpNumber() != null ? refuelDetail.getPumpNumber() : "") + ", productID: " + refuelDetail.getProductID() + ", productDescription: "
                            + (refuelDetail.getProductDescription() != null ? refuelDetail.getProductDescription() : "") + ", fuelQuantity: " + refuelDetail.getFuelQuantity()
                            + ", amount: " + refuelDetail.getAmount() + ", credits: " + refuelDetail.getCredits() + " }";

                    message += messageTmp;
                }
            }
            else {
                message += "null";
            }

            message += ", nonOilDetail: ";

            if (loyaltyTransactionDetail.getNonOilDetail() != null) {
                message += " { ";

                for (NonOilDetail nonOilDetail : loyaltyTransactionDetail.getNonOilDetail()) {
                    String messageTmp = "{ " + "productID: " + (nonOilDetail.getProductID() != null ? nonOilDetail.getProductID() : "") + ", productDescription: "
                            + (nonOilDetail.getProductDescription() != null ? nonOilDetail.getProductDescription() : "") + ", amount: " + nonOilDetail.getAmount() + ", credits: "
                            + nonOilDetail.getCredits() + " }";

                    message += messageTmp;
                }
            }
            else {
                message += "null";
            }

            inputParameters.add(new Pair<String, String>("loyaltyTransactionDetail", message));
        }
        else {
            inputParameters.add(new Pair<String, String>("loyaltyTransactionDetail", "null"));
        }

        if (eventNotificationMessageRequest.getOperationType() != null) {
            inputParameters.add(new Pair<String, String>("operationType", eventNotificationMessageRequest.getOperationType()));
        }
        else {
            inputParameters.add(new Pair<String, String>("operationType", ""));
        }

        if (eventNotificationMessageRequest.getSubtype() != null) {
            inputParameters.add(new Pair<String, String>("subtype", eventNotificationMessageRequest.getSubtype()));
        }
        else {
            inputParameters.add(new Pair<String, String>("subtype", ""));
        }
        if (eventNotificationMessageRequest.getRewardDetail() != null) {
            RewardDetail rewardDetail = eventNotificationMessageRequest.getRewardDetail();
            String message = "{ " + "parameterDetails: ";

            if (rewardDetail.getParameterDetails() != null) {
                for (ParameterDetails parameterDetails : rewardDetail.getParameterDetails()) {
                    String messageTmp = "{ parameterID: " + parameterDetails.getParameterID() + ", parameterValue: " + parameterDetails.getParameterValue() + " }";

                    message += messageTmp;
                }
            }
            else {
                message += "null";
            }

            inputParameters.add(new Pair<String, String>("rewardDetail", message));
        }
        else {
            inputParameters.add(new Pair<String, String>("rewardDetail", "null"));
        }

        log(ErrorLevel.DEBUG, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), "opening", ActivityLog.createLogMessage(inputParameters));

        EventNotification eventNotification = new EventNotification();
        EventNotificationMessageResponse eventNotificationMessageResponse = new EventNotificationMessageResponse();
        boolean validation = true;

        /*
         * Inizio implementazione per bloccare le richieste effettuate per
         * verificare la comunicazione tra sistemi
         */
        /*
         * if (eventNotificationMessageRequest.getSrcTransactionID() != null &&
         * eventNotificationMessageRequest
         * .getSrcTransactionID().equals("GG-1612-01512635149486034619")) {
         * 
         * System.out.println(
         * "Chiamata da servizio di Quenit per verificare la comunicazione tra i sistemi"
         * );
         * 
         * eventNotificationMessageResponse.setStatusCode(ResponseHelper.
         * EVENT_NOTIFICATION_SUCCESS);
         * eventNotificationMessageResponse.setMessageCode
         * (prop.getProperty(ResponseHelper.EVENT_NOTIFICATION_SUCCESS));
         * 
         * Set<Pair<String, String>> outputParameters = new HashSet<Pair<String,
         * String>>(); outputParameters.add(new Pair<String,
         * String>("statusCode",
         * eventNotificationMessageResponse.getStatusCode()));
         * outputParameters.add(new Pair<String, String>("statusMessage",
         * eventNotificationMessageResponse.getMessageCode()));
         * 
         * log(ErrorLevel.DEBUG, "eventNotification",
         * eventNotificationMessageRequest.getRequestTimestamp().toString(),
         * "closing", ActivityLog.createLogMessage(inputParameters));
         * 
         * return eventNotificationMessageResponse; }
         */
        /*
         * Fine implementazione per bloccare le richieste effettuate per
         * verificare la comunicazione tra sistemi
         */

        if (eventNotificationMessageRequest.getRequestTimestamp() == null) {
            log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "requestTimestamp element is null");
            eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
            eventNotificationMessageResponse.setMessageCode("requestTimestamp element is null");
            validation = false;
        }

        if (eventNotificationMessageRequest.getTransactionDate() == null) {
            log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "transactionDate element is null");
            eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
            eventNotificationMessageResponse.setMessageCode("transactionDate element is null");
            validation = false;
        }

        try {
            BigInteger balanceAmount = eventNotificationMessageRequest.getBalanceAmount();
            if (balanceAmount == null) {

                log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "balance element is null");
                eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
                eventNotificationMessageResponse.setMessageCode("balanceAmount element is null");
                validation = false;
            }
        }
        catch (NumberFormatException ex) { // handle your exception
            log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "balance element is null");
            eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
            eventNotificationMessageResponse.setMessageCode("balanceAmount element is null");
            validation = false;
        }

        try {
            BigInteger balance = eventNotificationMessageRequest.getBalance();
            if (balance == null) {

                log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "balance element is null");
                eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
                eventNotificationMessageResponse.setMessageCode("balance element is null");
                validation = false;
            }
        }
        catch (NumberFormatException ex) { // handle your exception
            log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "balance element is null");
            eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
            eventNotificationMessageResponse.setMessageCode("balance element is null");
            validation = false;
        }

        try {
            BigInteger credits = eventNotificationMessageRequest.getCredits();
            if (credits == null) {

                log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "credits element is null");
                eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
                eventNotificationMessageResponse.setMessageCode("credits element is null");
                validation = false;
            }
        }
        catch (NumberFormatException ex) { // handle your exception
            log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "balance element is null");
            eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
            eventNotificationMessageResponse.setMessageCode("credits element is null");
            validation = false;
        }

        if (eventNotificationMessageRequest.getEventType() == null) {

            log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "eventType element is null");
            eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
            eventNotificationMessageResponse.setMessageCode("eventType element is null");
            validation = false;
        }

        if (eventNotificationMessageRequest.getFiscalCode() == null || eventNotificationMessageRequest.getFiscalCode().isEmpty()) {

            log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "fiscalCode element is null or empty");
            eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
            eventNotificationMessageResponse.setMessageCode("fiscalCode element is null or empty");
            validation = false;
        }

        if (eventNotificationMessageRequest.getPanCode() == null || eventNotificationMessageRequest.getPanCode().isEmpty()) {

            log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "panCode element is null or empty");
            eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
            eventNotificationMessageResponse.setMessageCode("panCode element is null or empty");
            validation = false;
        }

        if (eventNotificationMessageRequest.getStationID() == null || eventNotificationMessageRequest.getStationID().isEmpty()) {

            log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, "stationID element is null or empty");
            eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
            eventNotificationMessageResponse.setMessageCode("stationID element is null or empty");
            validation = false;
        }

        if (validation) {
            eventNotification.setEventType(EventNotificationType.valueOf(eventNotificationMessageRequest.getEventType().getCode()));
            eventNotification.setFiscalCode(eventNotificationMessageRequest.getFiscalCode());
            eventNotification.setRequestTimestamp(new Date(eventNotificationMessageRequest.getRequestTimestamp()));
            eventNotification.setSrcTransactionID(eventNotificationMessageRequest.getSrcTransactionID());

            if (eventNotificationMessageRequest.getCardStatus() != null) {
                eventNotification.setCardStatus(eventNotificationMessageRequest.getCardStatus().getStatus());
            }
            if (eventNotificationMessageRequest.getBalance() != null) {
                eventNotification.setBalance(eventNotificationMessageRequest.getBalance().intValue());
            }
            else {
                eventNotification.setBalance(0);
            }
            if (eventNotificationMessageRequest.getCredits() != null) {
                eventNotification.setCredits(eventNotificationMessageRequest.getCredits().intValue());
            }
            else {
                eventNotification.setCredits(0);
            }

            eventNotification.setSessionID(eventNotificationMessageRequest.getSessionID());
            eventNotification.setPanCode(eventNotificationMessageRequest.getPanCode());
            eventNotification.setStationID(eventNotificationMessageRequest.getStationID());
            eventNotification.setTerminalID(eventNotificationMessageRequest.getTerminalID());
            eventNotification.setTransactionResult(eventNotificationMessageRequest.getTransactionResult());
            eventNotification.setTransactionDate(eventNotificationMessageRequest.getTransactionDate());

            if (eventNotificationMessageRequest.getMarketingMsg() != null && !eventNotificationMessageRequest.getMarketingMsg().isEmpty()) {
                eventNotification.setMarketingMsg(eventNotificationMessageRequest.getMarketingMsg());
            }

            if (eventNotificationMessageRequest.getWarningMsg() != null && !eventNotificationMessageRequest.getWarningMsg().isEmpty()) {
                eventNotification.setWarningMsg(eventNotificationMessageRequest.getWarningMsg());
            }

            if (eventNotificationMessageRequest.getLoyaltyTransactionDetail() != null) {
                LoyaltyTransactionDetail loyaltyTransactionDetail = eventNotificationMessageRequest.getLoyaltyTransactionDetail();
                LoyaltyTransaction loyaltyTransaction = new LoyaltyTransaction();
                loyaltyTransaction.setPaymentMode(loyaltyTransactionDetail.getPaymentMode());

                if (loyaltyTransactionDetail.getRefuelDetail() != null) {
                    for (RefuelDetail refuelDetail : loyaltyTransactionDetail.getRefuelDetail()) {
                        LoyaltyTransactionRefuel refuel = new LoyaltyTransactionRefuel();
                        refuel.setRefuelMode(refuelDetail.getRefuelMode());
                        refuel.setPumpNumber(refuelDetail.getPumpNumber());
                        refuel.setProductID(refuelDetail.getProductID());
                        refuel.setProductDescription(refuelDetail.getProductDescription());
                        if (refuelDetail.getFuelQuantity() != null) {
                            refuel.setFuelQuantity(refuelDetail.getFuelQuantity().doubleValue());
                        }
                        else {
                            refuel.setFuelQuantity(0.0);
                        }
                        if (refuelDetail.getAmount() != null) {
                            refuel.setAmount(refuelDetail.getAmount().doubleValue());
                        }
                        else {
                            refuel.setAmount(0.0);
                        }
                        if (refuelDetail.getCredits() != null) {
                            refuel.setCredits(refuelDetail.getCredits().intValue());
                        }
                        else {
                            refuel.setCredits(0);
                        }
                        loyaltyTransaction.getRefuelDetails().add(refuel);
                    }
                }

                if (loyaltyTransactionDetail.getNonOilDetail() != null) {
                    for (NonOilDetail nonOilDetail : loyaltyTransactionDetail.getNonOilDetail()) {
                        LoyaltyTransactionNonOil nonOil = new LoyaltyTransactionNonOil();
                        nonOil.setProductID(nonOilDetail.getProductID());
                        nonOil.setProductDescription(nonOilDetail.getProductDescription());
                        if (nonOilDetail.getAmount() != null) {
                            nonOil.setAmount(nonOilDetail.getAmount().doubleValue());
                        }
                        else {
                            nonOil.setAmount(0.0);
                        }
                        if (nonOilDetail.getCredits() != null) {
                            nonOil.setCredits(nonOilDetail.getCredits().intValue());
                        }
                        else {
                            nonOil.setCredits(0);
                        }

                        loyaltyTransaction.getNonOilDetails().add(nonOil);
                    }
                }

                eventNotification.setLoyaltyTransactionDetail(loyaltyTransaction);
                eventNotification.setOperationType(eventNotificationMessageRequest.getOperationType());
                eventNotification.setSubtype(eventNotificationMessageRequest.getSubtype());
            }

            if (eventNotificationMessageRequest.getRewardDetail() != null) {
                RewardDetail rewardDetail = eventNotificationMessageRequest.getRewardDetail();
                RewardTransaction rewardTransaction = new RewardTransaction();

                if (rewardDetail.getParameterDetails() != null) {
                    for (ParameterDetails parameterDetails : rewardDetail.getParameterDetails()) {
                        RewardTransactionParameter parameter = new RewardTransactionParameter();
                        parameter.setParameterID(parameterDetails.getParameterID());
                        parameter.setParameterValue(parameterDetails.getParameterValue());

                        rewardTransaction.getParameterDetails().add(parameter);
                    }

                    eventNotification.setRewardTransactionDetail(rewardTransaction);
                }
            }

            try {
                this.eventNotificationService = EJBHomeCache.getInstance().getEventNotificationService();
                String response = eventNotificationService.setEventNotification(eventNotification);

                if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.USER_SET_EVENT_NOTIFICATION_USER_NOT_FOUND)) {
                    eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_USER_NOT_FOUND);
                    eventNotificationMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.EVENT_NOTIFICATION_USER_NOT_FOUND));
                }

                if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.USER_SET_EVENT_NOTIFICATION_SESSIONID_NOT_FOUND)) {
                    eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_INVALID_SESSION);
                    eventNotificationMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.EVENT_NOTIFICATION_INVALID_SESSION));
                }

                if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.SYSTEM_ERROR)) {
                    eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
                    eventNotificationMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.EVENT_NOTIFICATION_ERROR));
                }

                if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.USER_SET_EVENT_NOTIFICATION_STATION_NOT_FOUND)) {
                    eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_STATION_NOT_ENABLED);
                    eventNotificationMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.EVENT_NOTIFICATION_STATION_NOT_ENABLED));
                }

                if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.USER_SET_EVENT_NOTIFICATION_FAILURE)
                        || response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.USER_SET_EVENT_NOTIFICATION_ENDPOINT_NOT_FOUND)
                        || response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.USER_SET_EVENT_NOTIFICATION_SUCCESS)) {
                    eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_SUCCESS);
                    eventNotificationMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.EVENT_NOTIFICATION_SUCCESS));
                }

            }
            catch (InterfaceNotFoundException ex) {
                log(ErrorLevel.ERROR, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), null, ex.getMessage());
                eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", eventNotificationMessageResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", eventNotificationMessageResponse.getMessageCode()));

        log(ErrorLevel.DEBUG, "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), "closing", ActivityLog.createLogMessage(inputParameters));

        return eventNotificationMessageResponse;
    }

    @WebMethod(operationName = "checkPayment")
    @WebResult(name = "checkPaymentResponse")
    public @XmlElement(required = true)
    CheckPaymentMessageResponse checkPayment(@XmlElement(required = true) @WebParam(name = "checkPaymentRequest") CheckPaymentMessageRequest checkPaymentRequest) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationID", checkPaymentRequest.getOperationID()));
        if (checkPaymentRequest.getRequestTimestamp() != null) {
            inputParameters.add(new Pair<String, String>("requestTimestamp", checkPaymentRequest.getRequestTimestamp().toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("requestTimestamp", "null"));
        }
        inputParameters.add(new Pair<String, String>("pumpNumber", checkPaymentRequest.getPumpNumber()));
        inputParameters.add(new Pair<String, String>("stationID", checkPaymentRequest.getStationID()));

        log(ErrorLevel.DEBUG, "checkPayment", checkPaymentRequest.getOperationID(), "opening", ActivityLog.createLogMessage(inputParameters));

        CheckPaymentMessageResponse checkPaymentResponse = new CheckPaymentMessageResponse();

        Boolean errorFound = false;

        if (checkPaymentRequest.getOperationID() == null || checkPaymentRequest.getOperationID().trim().isEmpty()) {

            checkPaymentResponse.setOperationID("null");
            checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_INVALID_REQUEST);
            // checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_INVALID_REQUEST));
            checkPaymentResponse.setMessageCode("OperationID null or empty");
            errorFound = true;
            log(ErrorLevel.INFO, "checkPayment", checkPaymentRequest.getOperationID(), null, "OperationID null or empty");

        }
        else {

            if (checkPaymentRequest.getOperationID().length() != 33) {

                if (checkPaymentRequest.getOperationID().length() > 33) {
                    checkPaymentResponse.setOperationID(checkPaymentRequest.getOperationID().substring(0, 33));
                }
                else {
                    checkPaymentResponse.setOperationID(checkPaymentRequest.getOperationID());
                }
                checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_INVALID_REQUEST);
                // checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_INVALID_REQUEST));
                checkPaymentResponse.setMessageCode("OperationID invalid length");
                errorFound = true;
                log(ErrorLevel.INFO, "checkPayment", checkPaymentRequest.getOperationID(), null, "OperationID invalid length");
            }
        }

        if (!errorFound) {

            if (checkPaymentRequest.getStationID() == null || checkPaymentRequest.getStationID().trim().isEmpty()) {

                checkPaymentResponse.setOperationID(checkPaymentRequest.getOperationID());
                checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_INVALID_REQUEST);
                // checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_INVALID_REQUEST));
                checkPaymentResponse.setMessageCode("stationID null or empty");
                errorFound = true;
                log(ErrorLevel.INFO, "checkPayment", checkPaymentRequest.getOperationID(), null, "stationID null or empty");

            }
            else {

                if (checkPaymentRequest.getStationID().length() != 5) {

                    checkPaymentResponse.setOperationID(checkPaymentRequest.getOperationID());
                    checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_INVALID_REQUEST);
                    // checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_INVALID_REQUEST));
                    checkPaymentResponse.setMessageCode("stationID invalid length");
                    errorFound = true;
                    log(ErrorLevel.INFO, "checkPayment", checkPaymentRequest.getOperationID(), null, "stationID invalid length");
                }
            }
        }

        if (!errorFound) {

            if (checkPaymentRequest.getPumpNumber() == null || checkPaymentRequest.getPumpNumber().trim().isEmpty()) {

                checkPaymentResponse.setOperationID(checkPaymentRequest.getOperationID());
                checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_INVALID_REQUEST);
                // checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_INVALID_REQUEST));
                checkPaymentResponse.setMessageCode("pump number null or empty");
                errorFound = true;
                log(ErrorLevel.INFO, "checkPayment", checkPaymentRequest.getOperationID(), null, "pump number null or empty");

            }
            else {

                if (checkPaymentRequest.getPumpNumber().length() > 2) {

                    checkPaymentResponse.setOperationID(checkPaymentRequest.getOperationID());
                    checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_INVALID_REQUEST);
                    // checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_INVALID_REQUEST));
                    checkPaymentResponse.setMessageCode("pump number invalid length");
                    errorFound = true;
                    log(ErrorLevel.INFO, "checkPayment", checkPaymentRequest.getOperationID(), null, "pump number invalid length");
                }
                else {
                    if (checkPaymentRequest.getPumpNumber().startsWith("0")) {
                        String replace = checkPaymentRequest.getPumpNumber().substring(1);
                        log(ErrorLevel.INFO, "checkPayment", checkPaymentRequest.getOperationID(), null,
                                "Rimozione dello zero iniziale. Prima: " + checkPaymentRequest.getPumpNumber() + ", dopo: " + replace);
                        checkPaymentRequest.setPumpNumber(replace);
                    }
                }
            }
        }

        if (!errorFound) {

            try {
                this.eventNotificationService = EJBHomeCache.getInstance().getEventNotificationService();
                checkPaymentResponse.setOperationID(checkPaymentRequest.getOperationID());

                CheckTransactionResponse response = this.eventNotificationService.checkTransaction(checkPaymentRequest.getOperationID(), checkPaymentRequest.getRequestTimestamp(),
                        checkPaymentRequest.getStationID(), checkPaymentRequest.getPumpNumber());

                if (response.getStatusCode().equals(com.techedge.mp.core.business.interfaces.ResponseHelper.TRANSACTION_SUCCESS)) {
                    checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_SUCCESS);
                    checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_SUCCESS));
                }
                else if (response.getStatusCode().equals(com.techedge.mp.core.business.interfaces.ResponseHelper.TRANSACTION_FAILURE)) {
                    checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_FAILURE);
                    checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_FAILURE));
                }
                else if (response.getStatusCode().equals(com.techedge.mp.core.business.interfaces.ResponseHelper.TRANSACTION_NOT_FOUND)) {
                    checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_NOT_FOUND);
                    checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_NOT_FOUND));
                }
                else if (response.getStatusCode().equals(com.techedge.mp.core.business.interfaces.ResponseHelper.TRANSACTION_PARAMETER_NOT_FOUND)) {
                    checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_FAILURE);
                    checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_FAILURE));
                }
                else if (response.getStatusCode().equals(com.techedge.mp.core.business.interfaces.ResponseHelper.TRANSACTION_ON_HOLD)) {
                    checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_ON_HOLD);
                    checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_ON_HOLD));
                }
                else if (response.getStatusCode().equals(com.techedge.mp.core.business.interfaces.ResponseHelper.TRANSACTION_CANCELLED)) {
                    checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_CANCELLED);
                    checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_CANCELLED));
                }
                else if (response.getStatusCode().equals(com.techedge.mp.core.business.interfaces.ResponseHelper.TRANSACTION_UNPAID)) {
                    checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_UNPAID);
                    checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_UNPAID));
                }
                else if (response.getStatusCode().equals(com.techedge.mp.core.business.interfaces.ResponseHelper.TRANSACTION_STATION_NOT_ENABLED)) {
                    checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_STATION_NOT_ENABLED);
                    checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_STATION_NOT_ENABLED));
                }
                else if (response.getStatusCode().equals(com.techedge.mp.core.business.interfaces.ResponseHelper.TRANSACTION_PUMP_NOT_VALID)) {
                    checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_PUMP_NOT_VALID);
                    checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_PUMP_NOT_VALID));
                }

                if (response.getTransationDetail() != null) {
                    TransactionMessageDetail transactionMessageDetail = new TransactionMessageDetail();
                    transactionMessageDetail.setAmount(response.getTransationDetail().getAmount());
                    transactionMessageDetail.setFuelQuantity(response.getTransationDetail().getFuelQuantity());
                    transactionMessageDetail.setAuthorizationCode(response.getTransationDetail().getAuthorizationCode());
                    transactionMessageDetail.setBankTransactionID(response.getTransationDetail().getBankTransactionID());
                    transactionMessageDetail.setProductCode(response.getTransationDetail().getProductCode());
                    transactionMessageDetail.setPumpNumber(response.getTransationDetail().getPumpNumber());
                    transactionMessageDetail.setStationID(response.getTransationDetail().getStationID());

                    checkPaymentResponse.setTransactionDetail(transactionMessageDetail);
                }
            }
            catch (InterfaceNotFoundException ex) {
                log(ErrorLevel.ERROR, "checkPayment", checkPaymentRequest.getOperationID(), null, ex.getMessage());
                checkPaymentResponse.setOperationID(checkPaymentRequest.getOperationID());
                checkPaymentResponse.setStatusCode(ResponseHelper.TRANSACTION_SYSTEM_ERROR);
                checkPaymentResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_SYSTEM_ERROR));
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("operationID", checkPaymentResponse.getOperationID()));
        outputParameters.add(new Pair<String, String>("statusCode", checkPaymentResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", checkPaymentResponse.getMessageCode()));
        if (checkPaymentResponse.getTransactionDetail() != null) {
            outputParameters.add(new Pair<String, String>("authorization code", checkPaymentResponse.getTransactionDetail().getAuthorizationCode()));
            outputParameters.add(new Pair<String, String>("amount", checkPaymentResponse.getTransactionDetail().getAmount().toString()));
            outputParameters.add(new Pair<String, String>("bankTransactionID", checkPaymentResponse.getTransactionDetail().getBankTransactionID()));
            outputParameters.add(new Pair<String, String>("fuelQuantity", checkPaymentResponse.getTransactionDetail().getFuelQuantity().toString()));
            outputParameters.add(new Pair<String, String>("product description", checkPaymentResponse.getTransactionDetail().getProductCode()));
            outputParameters.add(new Pair<String, String>("pump number", checkPaymentResponse.getTransactionDetail().getPumpNumber()));
            outputParameters.add(new Pair<String, String>("station id ", checkPaymentResponse.getTransactionDetail().getStationID()));
        }
        log(ErrorLevel.INFO, "checkPayment", checkPaymentRequest.getOperationID(), "closing", ActivityLog.createLogMessage(outputParameters));

        return checkPaymentResponse;
    }

    public static boolean isNumeric(String str) {
        if (str == null) {
            return false;
        }
        int sz = str.length();
        for (int i = 0; i < sz; i++) {
            if (Character.isDigit(str.charAt(i)) == false) {
                return false;
            }
        }
        return true;
    }

    @WebMethod(operationName = "multicardStatusNotification")
    @WebResult(name = "multicardStatusNotificationResponse")
    public @XmlElement(required = true)
    MulticardStatusNotificationMessageResponse multicardStatusNotification(
            @XmlElement(required = true) @WebParam(name = "multicardStatusNotificationRequest") MulticardStatusNotificationMessageRequest multicardStatusNotificationMessageRequest) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        
        inputParameters.add(new Pair<String, String>("operationID", multicardStatusNotificationMessageRequest.getOperationID()));
        if (multicardStatusNotificationMessageRequest.getRequestTimestamp() != null) {
            inputParameters.add(new Pair<String, String>("requestTimestamp", multicardStatusNotificationMessageRequest.getRequestTimestamp().toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("requestTimestamp", "null"));
        }

        if (multicardStatusNotificationMessageRequest.getUserID() != null) {
            inputParameters.add(new Pair<String, String>("userID", multicardStatusNotificationMessageRequest.getUserID()));
        }
        else {
            inputParameters.add(new Pair<String, String>("userID", "null"));
        }

        if (multicardStatusNotificationMessageRequest.getDpanStatus() != null) {
            for (DpanStatus item : multicardStatusNotificationMessageRequest.getDpanStatus()) {
                inputParameters.add(new Pair<String, String>("DPANStatus", item.getDPAN() + " - " + item.getStatus()));
            }
        }
        else {
            inputParameters.add(new Pair<String, String>("dpanStatus", "null"));
        }

        MulticardStatusNotificationMessageResponse multicardStatusNotificationResponse = new MulticardStatusNotificationMessageResponse();

        ArrayList<String> listPan = new ArrayList<>();

        if (multicardStatusNotificationMessageRequest.getDpanStatus() == null || multicardStatusNotificationMessageRequest.getDpanStatus().isEmpty()) {
            multicardStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
            multicardStatusNotificationResponse.setMessageCode("DPAN Status null or empty");
            return multicardStatusNotificationResponse;

        }

        if (multicardStatusNotificationMessageRequest.getOperationID() == null || multicardStatusNotificationMessageRequest.getOperationID().trim().isEmpty()) {

            multicardStatusNotificationResponse.setOperationID("null");
            multicardStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
            multicardStatusNotificationResponse.setMessageCode("OperationID null or empty");
            return multicardStatusNotificationResponse;
        }
        else {

            if (multicardStatusNotificationMessageRequest.getOperationID().length() != 33) {

                if (multicardStatusNotificationMessageRequest.getOperationID().length() > 33) {
                    multicardStatusNotificationResponse.setOperationID(multicardStatusNotificationMessageRequest.getOperationID().substring(0, 33));
                }
                else {
                    multicardStatusNotificationResponse.setOperationID(multicardStatusNotificationMessageRequest.getOperationID());
                }
                multicardStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
                multicardStatusNotificationResponse.setMessageCode("OperationID invalid length");
                return multicardStatusNotificationResponse;
            }
        }

        if (multicardStatusNotificationMessageRequest.getUserID() == null || multicardStatusNotificationMessageRequest.getUserID().trim().isEmpty()) {

            multicardStatusNotificationResponse.setOperationID("null");
            multicardStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
            multicardStatusNotificationResponse.setMessageCode("OperationID null or empty");
        }
        else {

            if (multicardStatusNotificationMessageRequest.getUserID().length() > 50) {

                multicardStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
                multicardStatusNotificationResponse.setMessageCode("UserID invalid length");
                return multicardStatusNotificationResponse;
            }
        }
        if (multicardStatusNotificationMessageRequest.getDpanStatus() != null || !multicardStatusNotificationMessageRequest.getDpanStatus().isEmpty()) {
            for (DpanStatus item : multicardStatusNotificationMessageRequest.getDpanStatus()) {
                try {
                    DpanStatusEnum.valueOf(item.getStatus().name());
                    listPan.add((item.getStatus().name()));
                }
                catch (Exception ex) {
                    multicardStatusNotificationResponse.setStatusCode(ResponseHelper.TRANSACTION_INVALID_REQUEST);
                    multicardStatusNotificationResponse.setMessageCode("Invalid DPAN Status");
                    multicardStatusNotificationResponse.setOperationID(multicardStatusNotificationMessageRequest.getOperationID());
                    return multicardStatusNotificationResponse;
                }
            }

        }

        MulticardStatusNotificationResponse response = new MulticardStatusNotificationResponse();

        try {
            this.eventNotificationService = EJBHomeCache.getInstance().getEventNotificationService();
            
            multicardStatusNotificationResponse.setOperationID(multicardStatusNotificationMessageRequest.getOperationID());
            
            ArrayList<com.techedge.mp.core.business.interfaces.DpanStatus> dpanStatusList = new ArrayList<>(0);
            
            if (multicardStatusNotificationMessageRequest.getDpanStatus() != null && !multicardStatusNotificationMessageRequest.getDpanStatus().isEmpty()) {
                
                for (DpanStatus item : multicardStatusNotificationMessageRequest.getDpanStatus()) {
                    
                    com.techedge.mp.core.business.interfaces.DpanStatus dpanStatusNew = new com.techedge.mp.core.business.interfaces.DpanStatus();
                    dpanStatusNew.setDPAN(item.getDPAN());
                    dpanStatusNew.setStatus(com.techedge.mp.core.business.interfaces.DpanStatusEnum.valueOf(item.getStatus().name()));
                    dpanStatusList.add(dpanStatusNew);
                }
            }

            response = this.eventNotificationService.multicardStatusNotification(multicardStatusNotificationMessageRequest.getOperationID(),
                    multicardStatusNotificationMessageRequest.getRequestTimestamp(), multicardStatusNotificationMessageRequest.getUserID(), dpanStatusList);
        }
        catch (InterfaceNotFoundException ex) {
            log(ErrorLevel.ERROR, "checkPayment", multicardStatusNotificationMessageRequest.getOperationID(), null, ex.getMessage());
            multicardStatusNotificationResponse.setOperationID(multicardStatusNotificationMessageRequest.getOperationID());
            multicardStatusNotificationResponse.setStatusCode(ResponseHelper.TRANSACTION_SYSTEM_ERROR);
            multicardStatusNotificationResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_SYSTEM_ERROR));
        }

        multicardStatusNotificationResponse.setOperationID(response.getOperationID());
        multicardStatusNotificationResponse.setStatusCode(response.getStatusCode());
        multicardStatusNotificationResponse.setMessageCode(prop.getProperty(response.getStatusCode()));

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("operationID", multicardStatusNotificationResponse.getOperationID()));
        outputParameters.add(new Pair<String, String>("statusCode", multicardStatusNotificationResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", multicardStatusNotificationResponse.getMessageCode()));

        return multicardStatusNotificationResponse;
    }
    
    
    @WebMethod(operationName = "multicardRefuelingStatusNotification")
    @WebResult(name = "multicardRefuelingStatusNotificationResponse")
    public @XmlElement(required = true)
    MulticardRefuelingStatusNotificationMessageResponse multicardRefuelingStatusNotification(
            @XmlElement(required = true) @WebParam(name = "multicardRefuelingStatusNotificationRequest") MulticardRefuelingStatusNotificationMessageRequest multicardRefuelingStatusNotificationMessageRequest) {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        
        inputParameters.add(new Pair<String, String>("operationID", multicardRefuelingStatusNotificationMessageRequest.getOperationID()));
        if (multicardRefuelingStatusNotificationMessageRequest.getRequestTimestamp() != null) {
            inputParameters.add(new Pair<String, String>("requestTimestamp", multicardRefuelingStatusNotificationMessageRequest.getRequestTimestamp().toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("requestTimestamp", "null"));
        }

        if (multicardRefuelingStatusNotificationMessageRequest.getUserID() != null) {
            inputParameters.add(new Pair<String, String>("userID", multicardRefuelingStatusNotificationMessageRequest.getUserID()));
        }
        else {
            inputParameters.add(new Pair<String, String>("userID", "null"));
        }
        
        if (multicardRefuelingStatusNotificationMessageRequest.getServerSerialNumber() != null) {
            inputParameters.add(new Pair<String, String>("serverSerialNumber", multicardRefuelingStatusNotificationMessageRequest.getServerSerialNumber()));
        }
        else {
            inputParameters.add(new Pair<String, String>("serverSerialNumber", "null"));
        }

        if (multicardRefuelingStatusNotificationMessageRequest.getDpanStatus() != null) {
            for (DpanStatus item : multicardRefuelingStatusNotificationMessageRequest.getDpanStatus()) {
                inputParameters.add(new Pair<String, String>("DPANStatus", item.getDPAN() + " - " + item.getStatus()));
            }
        }
        else {
            inputParameters.add(new Pair<String, String>("dpanStatus", "null"));
        }
        
        log(ErrorLevel.INFO, "multicardRefuelingStatusNotification", multicardRefuelingStatusNotificationMessageRequest.getOperationID(), "opening", ActivityLog.createLogMessage(inputParameters));

        MulticardRefuelingStatusNotificationMessageResponse multicardRefuelingStatusNotificationResponse = new MulticardRefuelingStatusNotificationMessageResponse();

        ArrayList<String> listPan = new ArrayList<>();

        if (multicardRefuelingStatusNotificationMessageRequest.getDpanStatus() == null || multicardRefuelingStatusNotificationMessageRequest.getDpanStatus().isEmpty()) {
            multicardRefuelingStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
            multicardRefuelingStatusNotificationResponse.setMessageCode("DPAN Status null or empty");
            return multicardRefuelingStatusNotificationResponse;

        }

        if (multicardRefuelingStatusNotificationMessageRequest.getOperationID() == null || multicardRefuelingStatusNotificationMessageRequest.getOperationID().trim().isEmpty()) {

            multicardRefuelingStatusNotificationResponse.setOperationID("null");
            multicardRefuelingStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
            multicardRefuelingStatusNotificationResponse.setMessageCode("OperationID null or empty");
            return multicardRefuelingStatusNotificationResponse;
        }
        else {

            if (multicardRefuelingStatusNotificationMessageRequest.getOperationID().length() != 33) {

                if (multicardRefuelingStatusNotificationMessageRequest.getOperationID().length() > 33) {
                    multicardRefuelingStatusNotificationResponse.setOperationID(multicardRefuelingStatusNotificationMessageRequest.getOperationID().substring(0, 33));
                }
                else {
                    multicardRefuelingStatusNotificationResponse.setOperationID(multicardRefuelingStatusNotificationMessageRequest.getOperationID());
                }
                multicardRefuelingStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
                multicardRefuelingStatusNotificationResponse.setMessageCode("OperationID invalid length");
                return multicardRefuelingStatusNotificationResponse;
            }
        }

        if (multicardRefuelingStatusNotificationMessageRequest.getUserID() == null || multicardRefuelingStatusNotificationMessageRequest.getUserID().trim().isEmpty()) {

            multicardRefuelingStatusNotificationResponse.setOperationID("null");
            multicardRefuelingStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
            multicardRefuelingStatusNotificationResponse.setMessageCode("OperationID null or empty");
        }
        else {

            if (multicardRefuelingStatusNotificationMessageRequest.getUserID().length() > 50) {

                multicardRefuelingStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
                multicardRefuelingStatusNotificationResponse.setMessageCode("UserID invalid length");
                return multicardRefuelingStatusNotificationResponse;
            }
        }
        
        if (multicardRefuelingStatusNotificationMessageRequest.getServerSerialNumber() == null || multicardRefuelingStatusNotificationMessageRequest.getServerSerialNumber().trim().isEmpty()) {

            multicardRefuelingStatusNotificationResponse.setOperationID("null");
            multicardRefuelingStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
            multicardRefuelingStatusNotificationResponse.setMessageCode("OperationID null or empty");
        }
        else {

            if (multicardRefuelingStatusNotificationMessageRequest.getServerSerialNumber().length() > 40) {

                multicardRefuelingStatusNotificationResponse.setStatusCode(ResponseHelper.MULTICARD_STATUS_INVALID_REQUEST);
                multicardRefuelingStatusNotificationResponse.setMessageCode("UserID invalid length");
                return multicardRefuelingStatusNotificationResponse;
            }
        }
        
        if (multicardRefuelingStatusNotificationMessageRequest.getDpanStatus() != null || !multicardRefuelingStatusNotificationMessageRequest.getDpanStatus().isEmpty()) {
            for (DpanStatus item : multicardRefuelingStatusNotificationMessageRequest.getDpanStatus()) {
                try {
                    DpanStatusEnum.valueOf(item.getStatus().name());
                    listPan.add((item.getStatus().name()));
                }
                catch (Exception ex) {
                    multicardRefuelingStatusNotificationResponse.setStatusCode(ResponseHelper.TRANSACTION_INVALID_REQUEST);
                    multicardRefuelingStatusNotificationResponse.setMessageCode("Invalid DPAN Status");
                    multicardRefuelingStatusNotificationResponse.setOperationID(multicardRefuelingStatusNotificationMessageRequest.getOperationID());
                    return multicardRefuelingStatusNotificationResponse;
                }
            }

        }

        
        MulticardStatusNotificationResponse response = new MulticardStatusNotificationResponse();

        try {
            this.eventNotificationService = EJBHomeCache.getInstance().getEventNotificationService();
            
            multicardRefuelingStatusNotificationResponse.setOperationID(multicardRefuelingStatusNotificationMessageRequest.getOperationID());
            
            ArrayList<com.techedge.mp.core.business.interfaces.DpanStatus> dpanStatusList = new ArrayList<>(0);
            
            if (multicardRefuelingStatusNotificationMessageRequest.getDpanStatus() != null && !multicardRefuelingStatusNotificationMessageRequest.getDpanStatus().isEmpty()) {
                
                for (DpanStatus item : multicardRefuelingStatusNotificationMessageRequest.getDpanStatus()) {
                    
                    com.techedge.mp.core.business.interfaces.DpanStatus dpanStatusNew = new com.techedge.mp.core.business.interfaces.DpanStatus();
                    dpanStatusNew.setDPAN(item.getDPAN());
                    dpanStatusNew.setStatus(com.techedge.mp.core.business.interfaces.DpanStatusEnum.valueOf(item.getStatus().name()));
                    dpanStatusList.add(dpanStatusNew);
                }
            }
            
            String userId = multicardRefuelingStatusNotificationMessageRequest.getUserID().toLowerCase();
            System.out.println("Email convertita in minuscolo: " + userId);

            response = this.eventNotificationService.multicardRefuelingStatusNotification(multicardRefuelingStatusNotificationMessageRequest.getOperationID(),
                    multicardRefuelingStatusNotificationMessageRequest.getRequestTimestamp(), userId,
                    multicardRefuelingStatusNotificationMessageRequest.getServerSerialNumber(), dpanStatusList );
        }
        catch (InterfaceNotFoundException ex) {
            
            log(ErrorLevel.ERROR, "multicardRefuelingStatusNotification", multicardRefuelingStatusNotificationResponse.getOperationID(), null, ex.getMessage());
            
            multicardRefuelingStatusNotificationResponse.setOperationID(multicardRefuelingStatusNotificationMessageRequest.getOperationID());
            multicardRefuelingStatusNotificationResponse.setStatusCode(ResponseHelper.TRANSACTION_SYSTEM_ERROR);
            multicardRefuelingStatusNotificationResponse.setMessageCode(prop.getProperty(ResponseHelper.TRANSACTION_SYSTEM_ERROR));
            
            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("operationID", multicardRefuelingStatusNotificationResponse.getOperationID()));
            outputParameters.add(new Pair<String, String>("statusCode", multicardRefuelingStatusNotificationResponse.getStatusCode()));
            outputParameters.add(new Pair<String, String>("statusMessage", multicardRefuelingStatusNotificationResponse.getMessageCode()));
            
            log(ErrorLevel.INFO, "multicardRefuelingStatusNotification", multicardRefuelingStatusNotificationMessageRequest.getOperationID(), "closing", ActivityLog.createLogMessage(outputParameters));

            return multicardRefuelingStatusNotificationResponse;
        }

        multicardRefuelingStatusNotificationResponse.setOperationID(multicardRefuelingStatusNotificationMessageRequest.getOperationID());
        multicardRefuelingStatusNotificationResponse.setStatusCode(response.getStatusCode());
        multicardRefuelingStatusNotificationResponse.setMessageCode(prop.getProperty(response.getStatusCode()));

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("operationID", multicardRefuelingStatusNotificationResponse.getOperationID()));
        outputParameters.add(new Pair<String, String>("statusCode", multicardRefuelingStatusNotificationResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", multicardRefuelingStatusNotificationResponse.getMessageCode()));
        
        log(ErrorLevel.INFO, "multicardRefuelingStatusNotification", multicardRefuelingStatusNotificationMessageRequest.getOperationID(), "closing", ActivityLog.createLogMessage(outputParameters));

        return multicardRefuelingStatusNotificationResponse;
    }
}