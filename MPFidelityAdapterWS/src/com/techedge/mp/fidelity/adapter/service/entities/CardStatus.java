package com.techedge.mp.fidelity.adapter.service.entities;

public enum CardStatus {
    G("G"),  //Gold
    S("S"),  //Silver
    P("P"),  //Platinum
    M("M"),  //Young
    D("D");  //Dipendente
    
    private String status;
    
    private CardStatus(String status) {
        this.status = status;
    }
    
    public String getStatus() {
        return status;
    }
    
}
