package com.techedge.mp.fidelity.adapter.service.entities;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionMessageDetail", propOrder = { "stationID", "pumpNumber", "amount", "fuelQuantity", "productCode", "authorizationCode", "bankTransactionID"})
public class TransactionMessageDetail {
    @XmlElement(required = true)
    private String     stationID;

    @XmlElement(required = false)
    private String     pumpNumber;

    @XmlElement(required = true)
    private BigDecimal amount;

    @XmlElement(required = true)
    private BigDecimal fuelQuantity; 

    @XmlElement(required = true, nillable = true)
    private String     productCode;

    @XmlElement(required = false, nillable = true)
    private String     authorizationCode;

    @XmlElement(required = false, nillable = true)
    private String     bankTransactionID;

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(String pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigDecimal getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(BigDecimal fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productDescription) {
        this.productCode = productDescription;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getBankTransactionID() {
        return bankTransactionID;
    }

    public void setBankTransactionID(String bankTransactionID) {
        this.bankTransactionID = bankTransactionID;
    }

}
