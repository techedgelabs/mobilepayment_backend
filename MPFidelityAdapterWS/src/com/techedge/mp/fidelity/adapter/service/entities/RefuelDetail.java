package com.techedge.mp.fidelity.adapter.service.entities;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "refuelDetail", propOrder = { "refuelMode", "pumpNumber", "productID", "productDescription", "fuelQuantity", "amount", "credits" })
public class RefuelDetail {
    @XmlElement(required = true)
    private String     refuelMode;
    @XmlElement(required = false)
    private String     pumpNumber;
    @XmlElement(required = true)
    private String     productID;
    @XmlElement(required = false, nillable = true)
    private String     productDescription;
    @XmlElement(required = true)
    private BigDecimal fuelQuantity;
    @XmlElement(required = true)
    private BigDecimal amount;
    @XmlElement(required = true)
    private BigInteger credits;

    public String getRefuelMode() {
        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }

    public String getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(String pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public BigDecimal getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(BigDecimal fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public BigInteger getCredits() {
        return credits;
    }

    public void setCredits(BigInteger credits) {
        this.credits = credits;
    }

}
