package com.techedge.mp.fidelity.adapter.service.entities;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "multicardRefuelingStatusNotificationMessageRequest", propOrder = { "operationID", "requestTimestamp", "userID", "serverSerialNumber", "dpanStatus" })
public class MulticardRefuelingStatusNotificationMessageRequest {

    @XmlElement(required = true)
    private String operationID;
    @XmlElement(required = true)
    private Long   requestTimestamp;
    @XmlElement(required = true)
    private String userID;
    @XmlElement(required = true)
    private String serverSerialNumber ;
    @XmlElement(required = true)
    private ArrayList<DpanStatus> dpanStatus;
    
    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

	public String getUserID() {
		return userID;
	}

	public void setUserID(String userID) {
		this.userID = userID;
	}

	public String getServerSerialNumber() {
        return serverSerialNumber;
    }

    public void setServerSerialNumber(String serverSerialNumber) {
        this.serverSerialNumber = serverSerialNumber;
    }

    public ArrayList<DpanStatus> getDpanStatus() {
		return dpanStatus;
	}

	public void setDpanStatus(ArrayList<DpanStatus> dpanStatus) {
		this.dpanStatus = dpanStatus;
	}



}
