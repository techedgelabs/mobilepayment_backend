package com.techedge.mp.fidelity.adapter.service.entities;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rewardDetail", propOrder = { "parameterDetails" })
public class RewardDetail {

    @XmlElement(required = true)
    private ArrayList<ParameterDetails> parameterDetails;

    public ArrayList<ParameterDetails> getParameterDetails() {
        return parameterDetails;
    }

    public void setParameterDetails(ArrayList<ParameterDetails> parameterDetails) {
        this.parameterDetails = parameterDetails;
    }

}
