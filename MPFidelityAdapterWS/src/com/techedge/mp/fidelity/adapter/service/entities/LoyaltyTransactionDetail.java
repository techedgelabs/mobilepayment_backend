package com.techedge.mp.fidelity.adapter.service.entities;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loyaltyTransactionDetail", propOrder = {"paymentMode", "refuelDetail", "nonOilDetail", })
public class LoyaltyTransactionDetail {

    @XmlElement(required = true)
    private String                  paymentMode;
    @XmlElement(required = false, nillable = true)
    private ArrayList<RefuelDetail> refuelDetail;
    @XmlElement(required = false, nillable = true)
    private ArrayList<NonOilDetail> nonOilDetail;

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public ArrayList<RefuelDetail> getRefuelDetail() {
        return refuelDetail;
    }

    public void setRefuelDetail(ArrayList<RefuelDetail> refuelDetail) {
        this.refuelDetail = refuelDetail;
    }

    public ArrayList<NonOilDetail> getNonOilDetail() {
        return nonOilDetail;
    }

    public void setNonOilDetail(ArrayList<NonOilDetail> nonOilDetail) {
        this.nonOilDetail = nonOilDetail;
    }

}
