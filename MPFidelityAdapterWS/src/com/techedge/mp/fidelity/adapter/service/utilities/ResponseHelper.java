package com.techedge.mp.fidelity.adapter.service.utilities;

public class ResponseHelper {

    public final static String CHECK_SESSION_SUCCESS              = "CHECK_SESSION_200";
    public final static String CHECK_SESSION_FAILURE              = "CHECK_SESSION_300";
    public final static String CHECK_SESSION_INVALID_REQUEST      = "CHECK_SESSION_400";
    public final static String CHECK_SESSION_SYSTEM_ERROR         = "CHECK_SESSION_500";

    public final static String EVENT_NOTIFICATION_SUCCESS         = "MESSAGE_RECEIVED_200";
    public final static String EVENT_NOTIFICATION_USER_NOT_FOUND  = "USER_NOT_FOUND_404";
    public final static String EVENT_NOTIFICATION_INVALID_SESSION = "INVALID_SESSION_500";
    public final static String EVENT_NOTIFICATION_ERROR           = "INTERNAL_ERROR_500";
    public final static String EVENT_NOTIFICATION_STATION_NOT_ENABLED = "STATION_NOT_ENABLED";

    public final static String TRANSACTION_SUCCESS                = "TRANSACTION_PAID";
    public final static String TRANSACTION_FAILURE                = "ERROR";
    public final static String TRANSACTION_INVALID_REQUEST        = "ERROR";
    public final static String TRANSACTION_SYSTEM_ERROR           = "ERROR";
    public final static String TRANSACTION_NOT_FOUND              = "TRANSACTION_NOT_FOUND";
    public final static String TRANSACTION_ON_HOLD                = "TRANSACTION_ON_HOLD";
    public final static String TRANSACTION_UNPAID                 = "TRANSACTION_UNPAID";
    public final static String TRANSACTION_CANCELLED              = "TRANSACTION_CANCELLED";
    public final static String TRANSACTION_PARAMETER_NOT_FOUND    = "ERROR";
    public final static String TRANSACTION_STATION_NOT_ENABLED    = "STATION_NOT_ENABLED";
    public final static String TRANSACTION_PUMP_NOT_VALID         = "PUMP_NOT_VALID";
    
    public final static String MULTICARD_STATUS_SUCCESS                = "MULTICARD_STATUS_PAID";
    public final static String MULTICARD_STATUS_FAILURE                = "ERROR";
    public final static String MULTICARD_STATUS_INVALID_REQUEST        = "ERROR";
    public final static String MULTICARD_STATUS_SYSTEM_ERROR           = "ERROR";
    public final static String MULTICARD_STATUS_NOT_FOUND              = "MULTICARD_STATUS_NOT_FOUND";
    public final static String MULTICARD_STATUS_ON_HOLD                = "MULTICARD_STATUS_ON_HOLD";
    public final static String MULTICARD_STATUS_UNPAID                 = "MULTICARD_STATUS_UNPAID";
    public final static String MULTICARD_STATUS_CANCELLED              = "MULTICARD_STATUS_CANCELLED";
    public final static String MULTICARD_STATUS_PARAMETER_NOT_FOUND    = "ERROR";

}
