package com.techedge.mp.fidelity.adapter.service;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.techedge.mp.core.business.EventNotificationServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;

public class EJBHomeCache {

    final String                             parametersServiceRemoteJndi        = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
    final String                             loggerServiceRemoteJndi            = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
    final String                             userServiceRemoteJndi              = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/UserService!com.techedge.mp.core.business.UserServiceRemote";
    final String                             eventNotificationServiceRemoteJndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/EventNotificationService!com.techedge.mp.core.business.EventNotificationServiceRemote";

    private static EJBHomeCache              instance;

    protected Context                        context                            = null;

    protected ParametersServiceRemote        parametersService                  = null;
    protected LoggerServiceRemote            loggerService                      = null;
    protected UserServiceRemote              userService                        = null;
    protected EventNotificationServiceRemote eventNotificationService           = null;

    private EJBHomeCache() throws InterfaceNotFoundException {

        final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();

        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

        try {
            context = new InitialContext(jndiProperties);
        }
        catch (NamingException e) {

            throw new InterfaceNotFoundException("Naming exception: " + e.getMessage());
        }
    }

    public static synchronized EJBHomeCache getInstance() throws InterfaceNotFoundException {
        if (instance == null)
            instance = new EJBHomeCache();

        return instance;
    }

    public ParametersServiceRemote getParametersService() throws InterfaceNotFoundException {
        if (parametersService == null) {
            try {
                this.parametersService = (ParametersServiceRemote) context.lookup(parametersServiceRemoteJndi);
            }
            catch (Exception ex) {

                throw new InterfaceNotFoundException(parametersServiceRemoteJndi);
            }
        }
        return parametersService;
    }

    public LoggerServiceRemote getLoggerService() throws InterfaceNotFoundException {
        if (loggerService == null) {
            try {
                loggerService = (LoggerServiceRemote) context.lookup(loggerServiceRemoteJndi);
            }
            catch (Exception ex) {

                throw new InterfaceNotFoundException(loggerServiceRemoteJndi);
            }
        }
        return loggerService;
    }

    public UserServiceRemote getUserService() throws InterfaceNotFoundException {

        if (this.userService == null) {

            try {

                this.userService = (UserServiceRemote) context.lookup(userServiceRemoteJndi);
            }
            catch (Exception ex) {

                throw new InterfaceNotFoundException(userServiceRemoteJndi);
            }
        }

        return userService;
    }

    public EventNotificationServiceRemote getEventNotificationService() throws InterfaceNotFoundException {

        if (this.eventNotificationService == null) {

            try {

                this.eventNotificationService = (EventNotificationServiceRemote) context.lookup(eventNotificationServiceRemoteJndi);
            }
            catch (Exception ex) {

                throw new InterfaceNotFoundException(eventNotificationServiceRemoteJndi);
            }
        }

        return eventNotificationService;
    }
}
