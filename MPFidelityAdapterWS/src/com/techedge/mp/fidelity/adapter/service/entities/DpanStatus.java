package com.techedge.mp.fidelity.adapter.service.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DpanStatus", propOrder = { "DPAN", "status" })
public class DpanStatus {
    @XmlElement(required = true)
    private String     DPAN;
    @XmlElement(required = true)
    private DpanStatusEnum     status;
	public String getDPAN() {
		return DPAN;
	}
	public void setDPAN(String dPAN) {
		DPAN = dPAN;
	}
	public DpanStatusEnum getStatus() {
		return status;
	}
	public void setStatus(DpanStatusEnum status) {
		this.status = status;
	} 
}
