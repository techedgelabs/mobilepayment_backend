package com.techedge.mp.fidelity.adapter.service.entities;

public enum DpanStatusEnum {
	PRE_ENROLLED,
	ACTIVATED,
	CANCELLED,
	BLOCKED,
	REASSIGNED;
    
}
