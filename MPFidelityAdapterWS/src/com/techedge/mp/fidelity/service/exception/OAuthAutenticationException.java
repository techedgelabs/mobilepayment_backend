package com.techedge.mp.fidelity.service.exception;

public class OAuthAutenticationException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 3685231397172066332L;
    
    public OAuthAutenticationException(String message) {
        super(message);
    }

}
