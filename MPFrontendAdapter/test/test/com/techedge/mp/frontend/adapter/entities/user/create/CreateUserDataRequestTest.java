package test.com.techedge.mp.frontend.adapter.entities.user.create;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.AddressData;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.SecurityData;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.TermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.user.create.CreateUserDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.create.CreateUserLoyaltyCard;

public class CreateUserDataRequestTest extends BaseTestCase {

    private CreateUserDataRequest data;
    private AddressData           address;

    // assigning the values
    protected void setUp() {
        data = new CreateUserDataRequest();
        SecurityData securityData = new SecurityData();
        securityData.setEmail("provaemailtest@alice.it");
        securityData.setPassword("Password12345");
        data.setSecurityData(securityData);
        data.setFirstName("name");
        data.setLastName("surname");
        data.setFiscalCode("QWERTYUIOPLKJHGF");
        CustomDate customDate = new CustomDate();
        customDate.setDay(10);
        customDate.setMonth(1);
        customDate.setYear(1990);
        data.setDateOfBirth(customDate);
        data.setBirthMunicipality("muni");
        data.setBirthProvince("province");
        data.setLanguage("IT");
        data.setSex("M");

        List<TermsOfServiceData> termsOfService = new ArrayList<TermsOfServiceData>(0);
        TermsOfServiceData term = new TermsOfServiceData();
        term.setAccepted(true);
        term.setId("01");
        termsOfService.add(term);
        data.setTermsOfService(termsOfService);

        address = new AddressData();
        address.setCity("Rome");
        address.setCountryCodeId("ITA");
        address.setCountryCodeName("ITA");
        address.setHouseNumber("0076");
        address.setRegion("Lazio");
        address.setStreet("Via del Corso");
        address.setZipCode("00100");

        data.setAddressData(address);

        data.setBillingAddressData(address);

        CreateUserLoyaltyCard loyaltyCard = new CreateUserLoyaltyCard();
        loyaltyCard.setId("001");
        data.setLoyaltyCard(loyaltyCard);

    }

    @Test
    public void testSecurityDataNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setSecurityData(null);
        assertEquals(StatusCode.USER_CREATE_PRIVACY_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testTestOfServiceDataIDNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.getTermsOfService().get(0).setId(null);
        assertEquals(StatusCode.USER_CREATE_PRIVACY_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testFirstNameNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setFirstName(null);
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testFirstNameIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setFirstName("user1user1user1user1user1user1user1user1user1user1user1user1user1user1");
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testLastNameNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setLastName(null);
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testLastNameIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setLastName("user1user1user1user1user1user1user1user1user1user1user1user1user1user1");
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testFiscalCodeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setFiscalCode(null);
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testFiscalCodeIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setFiscalCode("user1user1user1user1user1user1user1user1user1user1user1user1user1user1");
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testBirthMunicipalityNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setBirthMunicipality(null);
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testBirthMunicipalityIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setBirthMunicipality("");
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testBirthMunicipalityMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setBirthMunicipality("test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testBirthProvinceNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setBirthProvince(null);
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testBirthProvinceIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setBirthProvince("");
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testBirthProvinceMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setBirthProvince("test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

//    @Test
//    public void testLanguageNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
//        data.setLanguage(null);
//        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
//    }

//    @Test
//    public void testLanguageIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
//        data.setLanguage("ASTT");
//        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
//    }

    @Test
    public void testSexNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setSex(null);
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testSexIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setSex("RT");
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataCityIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setCity("");
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataCityNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setCity(null);
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataCityMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setCity("RomeRomeRomeRomeRomeRomeRomeRomeRomeRomeRome");
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataCountryCodeMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setCountryCodeId("italyitalyitalyitalyitalyitalyitalyitalyitalyitaly");
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataCountryCodeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setCountryCodeId(null);
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataCountryCodeIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setCountryCodeId(null);
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataHouseNumberNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setHouseNumber(null);
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataHouseNumberIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setHouseNumber("");
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataHouseNumberMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setHouseNumber("househousehousehousehousehousehousehousehousehousehousehousehouse");
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataRegionNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setRegion(null);
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataRegionIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setRegion("");
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataRegionMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setRegion("househousehousehousehousehousehousehousehousehousehousehousehouse");
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataStreetNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setStreet(null);
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataStreetIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setStreet("");
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataStreetMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setStreet("househousehousehousehousehousehousehousehousehousehousehousehouse");
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataZipCodeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setZipCode(null);
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataZipCodeIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setZipCode("");
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAddressDataZipCodeMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        address.setZipCode("househousehousehousehousehousehousehousehousehousehousehousehouse");
        data.setAddressData(address);
        assertEquals(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testLoyaltyCardMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        CreateUserLoyaltyCard card = data.getLoyaltyCard();
        card.setId("househousehousehousehousehousehousehousehousehousehousehousehouse");
        data.setLoyaltyCard(card);
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testLoyaltyCardNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        CreateUserLoyaltyCard card = data.getLoyaltyCard();
        card.setId(null);
        data.setLoyaltyCard(card);
        assertEquals(StatusCode.USER_CREATE_MASTER_DATA_WRONG, data.check().getStatusCode());
    }
}