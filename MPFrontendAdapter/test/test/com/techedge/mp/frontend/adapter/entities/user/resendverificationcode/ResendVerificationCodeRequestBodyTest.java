package test.com.techedge.mp.frontend.adapter.entities.user.resendverificationcode;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.resendverificationcode.ResendValidationRequestBody;

public class ResendVerificationCodeRequestBodyTest extends BaseTestCase {

    private ResendValidationRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new ResendValidationRequestBody();
        body.setId("1");
        body.setType("type");
    }

    @Test
    public void testMobilePhoneNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setId(null);
        assertEquals(StatusCode.USER_RESEND_VALIDATATION_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testMobilePhoneIsZero() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setId("0");
        assertEquals(StatusCode.USER_RESEND_VALIDATATION_INVALID_PARAMETERS, body.check().getStatusCode());
    }
    
}