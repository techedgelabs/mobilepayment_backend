package test.com.techedge.mp.frontend.adapter.entities.user.insertipaymentmethod;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.insertpaymentmethod.InsertPaymentMethodBodyRequest;

public class InsertPaymentMethodRequestBodyTest extends BaseTestCase {

    private InsertPaymentMethodBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new InsertPaymentMethodBodyRequest();
        body.setNewPin("1234");
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setBrand("Brand");
        paymentMethod.setDefaultMethod(true);
        paymentMethod.setType("1");
        paymentMethod.setStatus(1);
        body.setPaymentMethod(paymentMethod);
    }

    @Test
    public void testPaymentMethodNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPaymentMethod(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }
    
    @Test
    public void testPaymentMethodTypeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getPaymentMethod().setType(null);
        assertEquals(StatusCode.USER_INSERT_PAYMENT_METHOD_TYPE_WRONG, body.check().getStatusCode());
    }
    
    @Test
    public void testPaymentMethodPinWrong() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setNewPin("1234455");
        assertEquals(StatusCode.USER_INSERT_PAYMENT_METHOD_PIN_WRONG, body.check().getStatusCode());
    }
}