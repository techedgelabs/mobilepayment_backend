package test.com.techedge.mp.frontend.adapter.webservices.common;

import java.util.List;

import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.EnablePumpResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetPumpStatusResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetTransactionStatusResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.PaymentAuthorizationResult;
import com.techedge.mp.forecourt.adapter.business.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendPaymentTransactionResultResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendStationListPaymentResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.StationPayment;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionReconciliationResponse;

public class MockForecourtInfoService implements ForecourtInfoServiceRemote {

    @Override
    public GetPumpStatusResponse getPumpStatus(String requestID, String stationID, String pumpID, String transactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GetStationDetailsResponse getStationDetails(String requestID, String stationID, String pumpID, Boolean pumpDetailsReq) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EnablePumpResponse enablePump(String requestID, String transactionID, String stationID, String pumpID, String paymentMode, String productID, Double amount,
            PaymentAuthorizationResult paymentAuthorizationResult) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GetTransactionStatusResponse getTransactionStatus(String requestID, String transactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionReconciliationResponse transactionReconciliation(String requestID, List<String> transactionIDList) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public SendPaymentTransactionResultResponse sendPaymentTransactionResult(String requestID, String transactionID, Double amount,
            PaymentTransactionResult paymentTransactionResult) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public SendStationListPaymentResponse sendStationListPayment(String requestID, List<StationPayment> stationPaymentList) {
        // TODO Auto-generated method stub
        return null;
    }
}
