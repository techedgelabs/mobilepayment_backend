package test.com.techedge.mp.frontend.adapter.webservices.common;

import com.techedge.mp.core.business.RefuelingServiceRemote;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingCreateMPTransactionResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTokenResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTransactionReportResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTransactionStatusResponse;

public class MockRefuelingService implements RefuelingServiceRemote {

    @Override
    public RefuelingGetMPTokenResponse getMPToken(String requestID, String username, String password) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RefuelingCreateMPTransactionResponse createMPTransaction(String requestID, String mpToken, String srcTransactionID, String stationID, String pumpID, Double amount,
            String currency, Integer pumpNumber, String refuelMode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RefuelingGetMPTransactionStatusResponse getMPTransactionStatus(String requestID, String mpToken, String srcTransactionID, String mpTransactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RefuelingGetMPTransactionReportResponse getMPTransactionReport(String requestID, String mpToken, String startDate, String endDate, String mpTransactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String notifySubscription(String requestId, String fiscalCode) {
        // TODO Auto-generated method stub
        return null;
    }

}
