package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockBpelServiceRemoteService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockForecourtInfoService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail.RetrieveRefuelPaymentDetailBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail.RetrieveRefuelPaymentDetailRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail.RetrieveRefuelPaymentDetailResponse;
import com.techedge.mp.frontend.adapter.entities.requests.RefuelRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrieveRefuelPaymentDetailRefuelService extends BaseTestCase {
    private FrontendAdapterService                 frontend;
    private RetrieveRefuelPaymentDetailRequest     request;
    private RetrieveRefuelPaymentDetailBodyRequest body;
    private RetrieveRefuelPaymentDetailResponse    response;
    private Response                               baseResponse;
    private String                                 json;
    private Gson                                   gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrieveRefuelPaymentDetailRequest();
        body = new RetrieveRefuelPaymentDetailBodyRequest();
        body.setRefuelID("qwertyuiopasdfghjklzxcvbnmqwerty");;
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoService());
        EJBHomeCache.getInstance().setBpelService(new MockBpelServiceRemoteService());
    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockRetrieveRefuelPaymentDetailTransactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockRetrieveRefuelPaymentDetailUserServiceSuccess());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setRetrieveRefuelPaymentDetail(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (RetrieveRefuelPaymentDetailResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIEVE_PAYMENT_DETAIL_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockRetrieveRefuelPaymentDetailTransactionServiceFailure());
        EJBHomeCache.getInstance().setUserService(new MockRetrieveRefuelPaymentDetailUserServiceSuccess());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setRetrieveRefuelPaymentDetail(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (RetrieveRefuelPaymentDetailResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIEVE_PAYMENT_DETAIL_FAILURE, response.getStatus().getStatusCode());
    }

}
