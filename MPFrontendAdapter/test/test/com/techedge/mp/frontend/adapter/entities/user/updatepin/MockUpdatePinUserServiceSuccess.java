package test.com.techedge.mp.frontend.adapter.entities.user.updatepin;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.user.UserUpdatePinResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockUpdatePinUserServiceSuccess extends MockUserService {

    @Override
    public UserUpdatePinResponse updatePin(String ticketId, String requestId, Long cardId, String cardType, String oldPin, String newPin) {
        UserUpdatePinResponse response = new UserUpdatePinResponse();
        response.setPinCheckMaxAttempts(1);
        response.setStatusCode(StatusCode.USER_PIN_SUCCESS);
        return response;
    }

}
