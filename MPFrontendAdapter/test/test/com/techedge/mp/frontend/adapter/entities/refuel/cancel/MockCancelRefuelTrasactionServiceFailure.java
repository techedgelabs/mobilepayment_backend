package test.com.techedge.mp.frontend.adapter.entities.refuel.cancel;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockCancelRefuelTrasactionServiceFailure extends MockTransactionService {
    @Override
    public String persistUndoRefuelStatus(String ticketID, String requestID, String refuelID) {
        // TODO Auto-generated method stub
        return StatusCode.REFUEL_CANCEL_FAILURE;
    }
}