package test.com.techedge.mp.frontend.adapter.entities.user.retrievedocument;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.RetrieveDocumentData;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrieveDocumentUserServiceFailure extends MockUserService {

    @Override
    public RetrieveDocumentData retrieveDocument(String ticketId, String requestId, String documentID) {
        RetrieveDocumentData data = new RetrieveDocumentData();
        data.setStatusCode(StatusCode.RETRIEVE_DOCUMENT_FAILURE);
        return data;
    }

}
