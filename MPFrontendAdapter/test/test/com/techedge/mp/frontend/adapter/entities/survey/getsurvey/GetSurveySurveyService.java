package test.com.techedge.mp.frontend.adapter.entities.survey.getsurvey;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockSurveyService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.SurveyRequest;
import com.techedge.mp.frontend.adapter.entities.survey.getsurvey.GetSurveyBodyRequest;
import com.techedge.mp.frontend.adapter.entities.survey.getsurvey.GetSurveyRequest;
import com.techedge.mp.frontend.adapter.entities.survey.getsurvey.GetSurveyResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class GetSurveySurveyService extends BaseTestCase {
    private FrontendAdapterService frontend;
    private GetSurveyRequest       request;
    private GetSurveyBodyRequest   body;
    private GetSurveyResponse      response;
    private Response               baseResponse;
    private String                 json;
    private Gson                   gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new GetSurveyRequest();
        body = new GetSurveyBodyRequest();
        body.setCode("123");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());
        EJBHomeCache.getInstance().setSurveyService(new MockSurveyService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setSurveyService(new MockGetSurveySurveyServiceSuccess());
        SurveyRequest surveyRequest = new SurveyRequest();
        surveyRequest.setGetSurveyRequest(request);
        json = gson.toJson(surveyRequest, SurveyRequest.class);
        baseResponse = frontend.surveyJsonHandler(json);
        response = (GetSurveyResponse) baseResponse.getEntity();
        assertEquals(StatusCode.SURVEY_GET_SURVEY_SUCCESS, response.getStatus().getStatusCode());
    }
}
