package test.com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions.ManagerRetrieveTransactionsBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions.ManagerRetrieveTransactionsRequest;

public class ManagerRetrieveTransactionsRequestTest extends BaseTestCase {

    private ManagerRetrieveTransactionsRequest     request;
    private ManagerRetrieveTransactionsBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new ManagerRetrieveTransactionsRequest();
        body = new ManagerRetrieveTransactionsBodyRequest();
        body.setPumpMaxTransactions(3);
        body.setStationID("stationID");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.MANAGER_RETRIEVE_TRANSACTIONS_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.getCredential().setTicketID(getTicketID_false());
        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(ResponseHelper.MANAGER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}