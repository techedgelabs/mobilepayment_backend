package test.com.techedge.mp.frontend.adapter.entities.refuel.cancel;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.cancel.CancelRefuelBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.cancel.CancelRefuelRequest;

public class CancelRefuelRequestTest extends BaseTestCase {

    private CancelRefuelRequest     request;
    private CancelRefuelBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new CancelRefuelRequest();
        body = new CancelRefuelBodyRequest();
        body.setRefuelID("refuelrefuelrefuelrefuelrefuel12");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.REFUEL_CANCEL_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
    
    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}