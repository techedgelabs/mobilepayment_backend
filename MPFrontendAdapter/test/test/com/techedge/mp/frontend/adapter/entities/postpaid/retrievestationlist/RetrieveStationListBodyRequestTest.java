package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveStationListBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveStationUserPositionRequest;

public class RetrieveStationListBodyRequestTest extends BaseTestCase {

    private RetrieveStationListBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveStationListBodyRequest();
        RetrieveStationUserPositionRequest position = new RetrieveStationUserPositionRequest();
        position.setLatitude(45.3);
        position.setLongitude(34.5);
        body.setUserPosition(position);
    }

    @Test
    public void testUserPositionNull() {
        body.setUserPosition(null);
        assertEquals(StatusCode.STATION_RETRIEVE_USER_POSITION_WRONG, body.check().getStatusCode());
    }

}