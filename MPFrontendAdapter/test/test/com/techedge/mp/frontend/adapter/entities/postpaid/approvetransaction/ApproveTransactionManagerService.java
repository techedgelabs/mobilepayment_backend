package test.com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionPaymentMethodBody;
import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionTransactionCredential;
import com.techedge.mp.frontend.adapter.entities.requests.PoPRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class ApproveTransactionManagerService extends BaseTestCase {
    private FrontendAdapterService                  frontend;
    private ApproveTransactionRequest               request;
    private ApproveTransactionBodyRequest           body;
    private ApproveTransactionResponse              response;
    private ApproveTransactionPaymentMethodBody     method;
    private ApproveTransactionTransactionCredential credential;
    private Response                                baseResponse;
    private String                                  json;
    private Gson                                    gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new ApproveTransactionRequest();
        method = new ApproveTransactionPaymentMethodBody();
        credential = new ApproveTransactionTransactionCredential();
        credential.setRequestID(getRequestID_true());
        credential.setTicketID(getTicketID_true());
        body = new ApproveTransactionBodyRequest();
        body.setTransactionID("qwertyuiqwertyuiqwertyuiqwertyui");
        body.setUseVoucher(true);
        method.setId(1L);
        method.setType("type");
        body.setPaymentMethod(method);
        request.setBody(body);
        request.setCredential(credential);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockApproveTransactionPostpaidServiceSuccess());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setApproveTransaction(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (ApproveTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.POP_APPROVE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockApproveTransactionPostpaidServiceFailure());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setApproveTransaction(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (ApproveTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.POP_APPROVE_FAILURE, response.getStatus().getStatusCode());
    }
}
