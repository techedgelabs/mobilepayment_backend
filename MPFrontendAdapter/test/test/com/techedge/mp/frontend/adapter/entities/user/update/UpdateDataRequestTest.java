package test.com.techedge.mp.frontend.adapter.entities.user.update;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserContactData;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserDataRequest;

public class UpdateDataRequestTest extends BaseTestCase {

    private UpdateUserDataRequest data;

    // assigning the values
    protected void setUp() {
        data = new UpdateUserDataRequest();
        data.setFirstName("name");
        data.setLastName("surname");
        CustomDate customDate = new CustomDate();
        customDate.setDay(10);
        customDate.setMonth(1);
        customDate.setYear(1990);
        data.setDateOfBirth(customDate);
        data.setBirthMunicipality("muni");
        data.setBirthProvince("province");
        data.setLanguage("IT");
        data.setSex("M");
        UpdateUserContactData contactData = new UpdateUserContactData();
        MobilePhone mobilePhone = new MobilePhone();
        mobilePhone.setNumber("1234567890");
        mobilePhone.setPrefix("0039");
        contactData.getMobilePhones().add(mobilePhone);
        data.setContactData(contactData);
    }

    @Test
    public void testFirstNameIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setFirstName("user1user1user1user1user1user1user1user1user1user1user1user1user1user1");
        assertEquals(StatusCode.USER_UPD_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testLastNameIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setLastName("user1user1user1user1user1user1user1user1user1user1user1user1user1user1");
        assertEquals(StatusCode.USER_UPD_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testBirthMunicipalityIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setBirthMunicipality("");
        assertEquals(StatusCode.USER_UPD_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testBirthMunicipalityMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setBirthMunicipality("test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_UPD_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testBirthProvinceIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setBirthProvince("");
        assertEquals(StatusCode.USER_UPD_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testBirthProvinceMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setBirthProvince("test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_UPD_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testLanguageIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setLanguage("ASTT");
        assertEquals(StatusCode.USER_UPD_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testSexIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setSex("RT");
        assertEquals(StatusCode.USER_UPD_MASTER_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testContactDataMobilePhoneNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.getContactData().setMobilePhones(null);
        assertEquals(StatusCode.USER_UPD_CONTACT_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testContactDataMobilePhoneMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        MobilePhone mobilePhone = new MobilePhone();
        mobilePhone.setNumber("abrcdfgteeflksjsecmrsswsws");
        data.getContactData().getMobilePhones().add(mobilePhone);
        assertEquals(StatusCode.USER_UPD_CONTACT_DATA_WRONG, data.check().getStatusCode());
    }

}