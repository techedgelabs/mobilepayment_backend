package test.com.techedge.mp.frontend.adapter.entities.manager.rescuepassword;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.rescuepassword.ManagerRescuePasswordBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.rescuepassword.ManagerRescuePasswordRequest;

public class ManagerRescuePasswordRequestTest extends BaseTestCase {

    private ManagerRescuePasswordRequest     request;
    private ManagerRescuePasswordBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new ManagerRescuePasswordRequest();
        body = new ManagerRescuePasswordBodyRequest();
        body.setEmail("a@a.it");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.MANAGER_RESCUE_PASSWORD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.getCredential().setTicketID(getTicketID_false());
        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}