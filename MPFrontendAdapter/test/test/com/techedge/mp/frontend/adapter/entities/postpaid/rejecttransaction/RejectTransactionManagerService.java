package test.com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction.RejectPoPTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction.RejectPoPTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction.RejectPoPTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.requests.PoPRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RejectTransactionManagerService extends BaseTestCase {
    private FrontendAdapterService          frontend;
    private RejectPoPTransactionRequest     request;
    private RejectPoPTransactionBodyRequest body;
    private RejectPoPTransactionResponse    response;
    private Response                        baseResponse;
    private String                          json;
    private Gson                            gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RejectPoPTransactionRequest();
        body = new RejectPoPTransactionBodyRequest();
        body.setTransactionId("qwertyuiqwertyuiqwertyuiqwertyui");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRejectTransactionPostpaidServiceSuccess());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRejectTransaction(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RejectPoPTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.POP_REJECT_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRejectTransactionPostpaidServiceFailure());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRejectTransaction(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RejectPoPTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.POP_REJECT_FAILURE, response.getStatus().getStatusCode());
    }
}
