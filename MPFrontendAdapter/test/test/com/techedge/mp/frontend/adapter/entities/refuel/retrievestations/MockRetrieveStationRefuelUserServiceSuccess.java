package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievestations;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.ResponseHelper;

public class MockRetrieveStationRefuelUserServiceSuccess extends MockUserService {
    @Override
    public String checkAuthorization(String ticketID, Integer operationType) {
        // TODO Auto-generated method stub
        return ResponseHelper.CHECK_AUTHORIZATION_SUCCESS;
    }
}
