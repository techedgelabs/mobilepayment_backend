package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.interfaces.PaymentRefuelDetailResponse;
import com.techedge.mp.core.business.interfaces.PaymentRefuelHistoryResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrieveRefuelPaymentHistoryTransactionServiceFailure extends MockTransactionService {
    @Override
    public PaymentRefuelHistoryResponse retrieveRefuelPaymentHistory(String requestID, String ticketID, Integer pageNumber, Integer itemForPage, Date startDate, Date enddate,
            Boolean detail) {
        PaymentRefuelHistoryResponse response = new PaymentRefuelHistoryResponse();
        response.setPageOffset(1);
        response.setPageTotal(2);
        List<PaymentRefuelDetailResponse> list = new ArrayList<PaymentRefuelDetailResponse>(0);
        PaymentRefuelDetailResponse responseDetail = new PaymentRefuelDetailResponse();
        responseDetail.setAuthCode("auth");
        responseDetail.setBankTransactionID("bank");
        responseDetail.setCurrency("currency");
        responseDetail.setDate(new Timestamp(1470650231L));
        responseDetail.setFinalAmount(12.2);
        responseDetail.setFuelAmount(13.3);
        responseDetail.setFuelQuantity(13.2);
        responseDetail.setInitialAmount(10.1);
        responseDetail.setMaskedPan("maskerd");
        responseDetail.setRefuelID("refuID");
        responseDetail.setSelectedPumpFuelType("select");
        responseDetail.setSelectedPumpID("pump");
        responseDetail.setSelectedPumpNumber(13);
        responseDetail.setSelectedStationAddress("address");
        responseDetail.setSelectedStationCity("city");
        responseDetail.setSelectedStationCountry("stationSel");
        responseDetail.setSelectedStationID("ID");
        responseDetail.setSelectedStationLatitude(45.3);
        responseDetail.setSelectedStationLongitude(35.5);
        responseDetail.setSelectedStationName("termini");
        responseDetail.setSelectedStationProvince("RM");
        responseDetail.setShopLogin("shop");
        responseDetail.setSourceStatus("source");
        responseDetail.setSourceType("source");
        responseDetail.setStatus("status");
        responseDetail.setStatusCode(StatusCode.RETRIEVE_PAYMENT_DETAIL_FAILURE);
        responseDetail.setSubStatus("substatus");
        response.setPaymentRefuelDetailHistory(list);
        response.setStatusCode(StatusCode.REFUEL_EXEC_PAY_HISTORY_FAILURE);
        return response;
    }
}