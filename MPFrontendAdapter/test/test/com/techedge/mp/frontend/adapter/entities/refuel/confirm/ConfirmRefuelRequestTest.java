package test.com.techedge.mp.frontend.adapter.entities.refuel.confirm;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.confirm.ConfirmRefuelBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.confirm.ConfirmRefuelRequest;

public class ConfirmRefuelRequestTest extends BaseTestCase {

    private ConfirmRefuelRequest     request;
    private ConfirmRefuelBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new ConfirmRefuelRequest();
        body = new ConfirmRefuelBodyRequest();
        body.setRefuelID("refuelrefuelrefuelrefuelrefuel12");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.REFUEL_CONFIRM_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}