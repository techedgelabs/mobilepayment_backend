package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbyqrcode;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbyqrcode.RetrievePoPTransactionByQRcodeBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbyqrcode.RetrievePoPTransactionByQRcodeRequest;

public class RetrieveTransactionByQRCodeRequestTest extends BaseTestCase {

    private RetrievePoPTransactionByQRcodeRequest     request;
    private RetrievePoPTransactionByQRcodeBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RetrievePoPTransactionByQRcodeRequest();
        body = new RetrievePoPTransactionByQRcodeBodyRequest();
        body.setQRCode("codecodecodecodecodecodecodecode");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.POP_GET_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}