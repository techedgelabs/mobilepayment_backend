package test.com.techedge.mp.frontend.adapter.entities.user.create;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.AddressData;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.SecurityData;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.create.CreateUserDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.create.CreateUserLoyaltyCard;
import com.techedge.mp.frontend.adapter.entities.user.create.CreateUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.create.CreateUserRequestBody;

public class CreateRequestTest extends BaseTestCase {

    private CreateUserRequest     request;
    private CreateUserRequestBody body;
    private CreateUserDataRequest data;

    // assigning the values
    protected void setUp() {
        request = new CreateUserRequest();
        body = new CreateUserRequestBody();
        data = new CreateUserDataRequest();
        SecurityData securityData = new SecurityData();
        securityData.setEmail("a@a.it");
        securityData.setPassword("Password12345");
        data.setSecurityData(securityData);
        data.setFirstName("name");
        data.setLastName("surname");
        data.setFiscalCode("QWERTYUIOPLKJHGF");
        CustomDate customDate = new CustomDate();
        customDate.setDay(10);
        customDate.setMonth(1);
        customDate.setYear(1990);
        data.setDateOfBirth(customDate);
        data.setBirthMunicipality("muni");
        data.setBirthProvince("province");
        data.setLanguage("IT");
        data.setSex("M");

        AddressData address = new AddressData();
        address.setCity("Rome");
        address.setCountryCodeId("ITA");
        address.setCountryCodeName("ITA");
        address.setHouseNumber("0076");
        address.setRegion("Lazio");
        address.setStreet("Via del Corso");
        address.setZipCode("00100");

        data.setAddressData(address);

        data.setBillingAddressData(address);

        CreateUserLoyaltyCard loyaltyCard = new CreateUserLoyaltyCard();
        loyaltyCard.setId("001");
        data.setLoyaltyCard(loyaltyCard);

        body.setUserData(data);

        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_CREATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}