package test.com.techedge.mp.frontend.adapter.entities.user.insertipaymentmethod;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.insertpaymentmethod.InsertPaymentMethodBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.insertpaymentmethod.InsertPaymentMethodRequest;
import com.techedge.mp.frontend.adapter.entities.user.insertpaymentmethod.InsertPaymentMethodResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestInsertPaymentMethodUserService extends BaseTestCase {
    private FrontendAdapterService         frontend;
    private InsertPaymentMethodRequest     request;
    private InsertPaymentMethodBodyRequest body;
    private InsertPaymentMethodResponse    response;
    private Response                       baseResponse;
    private String                         json;
    private Gson                           gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new InsertPaymentMethodRequest();
        body = new InsertPaymentMethodBodyRequest();
        body.setNewPin("1234");
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setBrand("Brand");
        paymentMethod.setDefaultMethod(true);
        paymentMethod.setType("1");
        paymentMethod.setStatus(1);
        body.setPaymentMethod(paymentMethod);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testGetActiveVoucherSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockInsertPaymentMethodUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setInsertPaymentMethod(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (InsertPaymentMethodResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_INSERT_PAYMENT_METHOD_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testGetActiveVoucherFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockInsertPaymentMethodUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setInsertPaymentMethod(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (InsertPaymentMethodResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_INSERT_PAYMENT_METHOD_FAILURE, response.getStatus().getStatusCode());
    }
}
