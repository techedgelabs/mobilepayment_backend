package test.com.techedge.mp.frontend.adapter.entities.refuel.createtransaction;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionCredential;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionDataRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionRequest;

public class CreateTransactionRefuelRequestTest extends BaseTestCase {

    private CreateRefuelTransactionRequest     request;
    private CreateRefuelTransactionBodyRequest body;
    private CreateRefuelTransactionDataRequest data;

    // assigning the values
    protected void setUp() {
        request = new CreateRefuelTransactionRequest();
        body = new CreateRefuelTransactionBodyRequest();
        data = new CreateRefuelTransactionDataRequest();

        data.setAmount(1);
        data.setOutOfRange("true");
        data.setPumpID("123456789");
        data.setStationID("123");
        data.setUseVoucher(true);
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setBrand("Brand");
        paymentMethod.setDefaultMethod(true);
        paymentMethod.setId(1L);
        paymentMethod.setType("1");
        paymentMethod.setStatus(1);
        data.setPaymentMethod(paymentMethod);
        body.setRefuelPaymentData(data);
        CreateRefuelTransactionCredential credential = new CreateRefuelTransactionCredential();
        credential.setTicketID(getTicketID_true());
        credential.setRequestID(getRequestID_true());
        credential.setPin("1234");
        request.setCredential(credential);
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.getCredential().setTicketID(getTicketID_false());
        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testCredentialPinNull() {

        request.getCredential().setPin(null);

        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_PIN_WRONG, request.check().getStatusCode());
    }

}