package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockPostpaidService;

import com.techedge.mp.core.business.interfaces.GetStationListResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrieveStationListPostpaidServiceFailure extends MockPostpaidService {
    @Override
    public GetStationListResponse getStationList(String requestID, String ticketID, Double userPositionLatitude, Double userPositionLongitude) {
        GetStationListResponse response = new GetStationListResponse();
        response.setStatusCode(StatusCode.STATION_RETRIEVE_FAILURE);
        return response;
    }
}
