package test.com.techedge.mp.frontend.adapter.entities.user.updatepassword;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.updatepassword.UpdatePasswordRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatepassword.UpdatePasswordRequestBody;

public class UpdatePasswordRequestTest extends BaseTestCase {

    private UpdatePasswordRequest     request;
    private UpdatePasswordRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new UpdatePasswordRequest();
        body = new UpdatePasswordRequestBody();
        body.setNewPassword("Password123");
        body.setOldPassword("NewPasswoer456");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_PWD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}