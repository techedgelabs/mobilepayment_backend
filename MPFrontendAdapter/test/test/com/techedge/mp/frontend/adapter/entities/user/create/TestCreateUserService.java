package test.com.techedge.mp.frontend.adapter.entities.user.create;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.authentication.AuthenticationUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.authentication.AuthenticationUserRequestBody;
import com.techedge.mp.frontend.adapter.entities.user.authentication.AuthenticationUserResponseFailure;
import com.techedge.mp.frontend.adapter.entities.user.authentication.AuthenticationUserResponseSuccess;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestCreateUserService extends TestCase {
    private FrontendAdapterService            frontend;
    private AuthenticationUserRequest         request;
    private AuthenticationUserRequestBody     body;
    private AuthenticationUserResponseSuccess response_success;
    private AuthenticationUserResponseFailure response_fail;
    private Response                          baseResponse;
    private String                            json;
    private Gson                              gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new AuthenticationUserRequest();
        body = new AuthenticationUserRequestBody();
        body.setDeviceID("Nexus 5");
        body.setDeviceName("Nexus");
        body.setPassword("1234");
        body.setRequestID("EJB-10101010101");
        body.setUsername("user");
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testCreateSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockCreateUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setAuthentication(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response_success = (AuthenticationUserResponseSuccess) baseResponse.getEntity();
        assertEquals(StatusCode.USER_AUTH_SUCCESS, response_success.getStatus().getStatusCode());
    }

    @Test
    public void testCreateFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockCreateUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setAuthentication(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response_fail = (AuthenticationUserResponseFailure) baseResponse.getEntity();
        assertEquals(StatusCode.USER_AUTH_FAILURE, response_fail.getStatus().getStatusCode());
    }

}
