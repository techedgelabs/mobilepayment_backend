package test.com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockBpelServiceRemoteService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockForecourtInfoService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction.CreateApplePayRefuelTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction.CreateApplePayRefuelTransactionDataRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction.CreateApplePayRefuelTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction.CreateApplePayRefuelTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.requests.RefuelRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class CreateApplePayTransactionRefuelRefuelService extends BaseTestCase {
    private FrontendAdapterService                     frontend;
    private CreateApplePayRefuelTransactionRequest     createApplePayRefuelTransactionRequest;
    private CreateApplePayRefuelTransactionBodyRequest createApplePayRefuelTransactionBodyRequest;
    private CreateApplePayRefuelTransactionDataRequest createApplePayRefuelTransactionDataRequest;
    private CreateApplePayRefuelTransactionResponse    createApplePayRefuelTransactionResponse;
    private Response                                   baseResponse;
    private String                                     json;
    private Gson                                       gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {

        frontend = new FrontendAdapterService();
        createApplePayRefuelTransactionRequest = new CreateApplePayRefuelTransactionRequest();
        createApplePayRefuelTransactionBodyRequest = new CreateApplePayRefuelTransactionBodyRequest();
        createApplePayRefuelTransactionDataRequest = new CreateApplePayRefuelTransactionDataRequest();

        createApplePayRefuelTransactionDataRequest.setAmount(1);
        createApplePayRefuelTransactionDataRequest.setOutOfRange("true");
        createApplePayRefuelTransactionDataRequest.setPumpID("123456789");
        createApplePayRefuelTransactionDataRequest.setStationID("123");
        createApplePayRefuelTransactionDataRequest.setUseVoucher(true);
        createApplePayRefuelTransactionDataRequest.setOutOfRange("true");
        createApplePayRefuelTransactionDataRequest.setApplePayPKPaymentToken("wjkaryrfgnqiqy=");

        Credential credential = new Credential();
        credential.setTicketID(getTicketID_true());
        credential.setRequestID(getRequestID_true());
        createApplePayRefuelTransactionRequest.setCredential(credential);
        createApplePayRefuelTransactionRequest.setBody(createApplePayRefuelTransactionBodyRequest);

        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoService());
        EJBHomeCache.getInstance().setBpelService(new MockBpelServiceRemoteService());
    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockCreateTransactionRefuelTrasactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockCreateTransactionRefuelUserServiceSuccess());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtServiceSuccess());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setCreateApplePayRefuelTransaction(createApplePayRefuelTransactionRequest);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        createApplePayRefuelTransactionResponse = (CreateApplePayRefuelTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS, createApplePayRefuelTransactionResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameCheckAuthorizationFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockCreateTransactionRefuelTrasactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockCreateTransactionRefuelUserServiceFailure());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtServiceSuccess());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setCreateApplePayRefuelTransaction(createApplePayRefuelTransactionRequest);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        createApplePayRefuelTransactionResponse = (CreateApplePayRefuelTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_REQU_UNAUTHORIZED, createApplePayRefuelTransactionResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernamePumpFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockCreateTransactionRefuelTrasactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockCreateTransactionRefuelUserServiceSuccess());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtServicePumpFailure());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setCreateApplePayRefuelTransaction(createApplePayRefuelTransactionRequest);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        createApplePayRefuelTransactionResponse = (CreateApplePayRefuelTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_FAILURE, createApplePayRefuelTransactionResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameForecourtFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockCreateTransactionRefuelTrasactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockCreateTransactionRefuelUserServiceSuccess());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtServiceFailure());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setCreateApplePayRefuelTransaction(createApplePayRefuelTransactionRequest);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        createApplePayRefuelTransactionResponse = (CreateApplePayRefuelTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_FAILURE, createApplePayRefuelTransactionResponse.getStatus().getStatusCode());
    }

}
