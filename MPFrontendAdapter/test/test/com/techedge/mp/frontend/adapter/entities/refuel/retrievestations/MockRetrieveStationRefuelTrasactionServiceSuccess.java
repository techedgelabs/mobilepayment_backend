package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievestations;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;

public class MockRetrieveStationRefuelTrasactionServiceSuccess extends MockTransactionService {

    @Override
    public String retrieveStationIdByCoords(Double latitude, Double longitude) {
        // TODO Auto-generated method stub
        return StatusCode.STATION_RETRIEVE_SUCCESS;
    }

    @Override
    public String retrieveStationId(String beaconCode) {
        // TODO Auto-generated method stub
        return StatusCode.STATION_RETRIEVE_SUCCESS;
    }

    @Override
    public String refreshStationInfo(String ticketID, String stationID, String address, String city, String country, String province, Double latitude, Double longitude) {
        // TODO Auto-generated method stub
        return StatusCode.STATION_RETRIEVE_SUCCESS;
    }
}
