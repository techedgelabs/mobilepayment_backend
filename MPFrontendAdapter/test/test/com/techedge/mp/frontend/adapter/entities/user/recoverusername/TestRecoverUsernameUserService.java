package test.com.techedge.mp.frontend.adapter.entities.user.recoverusername;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.recoverusername.RecoverUsernameRequest;
import com.techedge.mp.frontend.adapter.entities.user.recoverusername.RecoverUsernameRequestBody;
import com.techedge.mp.frontend.adapter.entities.user.recoverusername.RecoverUsernameResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestRecoverUsernameUserService extends BaseTestCase {
    private FrontendAdapterService     frontend;
    private RecoverUsernameRequest     request;
    private RecoverUsernameRequestBody body;
    private RecoverUsernameResponse    response;
    private Response                   baseResponse;
    private String                     json;
    private Gson                       gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RecoverUsernameRequest();
        body = new RecoverUsernameRequestBody();
        body.setFiscalcode("qwertyuiopasdfgh");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRecoverUsernameUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setRecoverUsername(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RecoverUsernameResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_RECOVER_USERNAME_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRecoverUsernameUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setRecoverUsername(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RecoverUsernameResponse) baseResponse.getEntity();
        //Torna sempre SUCCESS
        assertEquals(StatusCode.USER_RECOVER_USERNAME_SUCCESS, response.getStatus().getStatusCode());
    }
}
