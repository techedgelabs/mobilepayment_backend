package test.com.techedge.mp.frontend.adapter.entities.user.removevoucher;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.removevoucher.RemoveVoucherBodyRequest;

public class RemoveRemoveVoucherRequestBodyTest extends BaseTestCase {

    private RemoveVoucherBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RemoveVoucherBodyRequest();
        body.setVoucherCode("123");
    }

    @Test
    public void testVoucherNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVoucherCode(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testVoucherIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVoucherCode("");
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testVoucherMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVoucherCode("setVoucherCodesetVoucherCodesetVoucherCodesetVoucherCode");
        assertEquals(StatusCode.USER_REMOVE_VOUCHER_CODE_WRONG, body.check().getStatusCode());
    }
}