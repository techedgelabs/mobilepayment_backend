package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievestations;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrieveStationRefuelTransactionServiceFailure extends MockTransactionService {
    @Override
    public String retrieveStationIdByCoords(Double latitude, Double longitude) {
        // TODO Auto-generated method stub
        return StatusCode.STATION_RETRIEVE_FAILURE;
    }

    @Override
    public String retrieveStationId(String beaconCode) {
        // TODO Auto-generated method stub
        return StatusCode.STATION_RETRIEVE_FAILURE;
    }

    @Override
    public String refreshStationInfo(String ticketID, String stationID, String address, String city, String country, String province, Double latitude, Double longitude) {
        // TODO Auto-generated method stub
        return StatusCode.STATION_RETRIEVE_FAILURE;
    }
}