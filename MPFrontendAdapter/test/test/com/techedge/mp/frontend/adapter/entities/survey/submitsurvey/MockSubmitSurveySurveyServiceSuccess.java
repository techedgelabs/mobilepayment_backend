package test.com.techedge.mp.frontend.adapter.entities.survey.submitsurvey;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockSurveyService;

import com.techedge.mp.core.business.interfaces.SurveyAnswers;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockSubmitSurveySurveyServiceSuccess extends MockSurveyService {

    @Override
    public String surveySubmit(String ticketId, String requestId, String code, String key, SurveyAnswers answers) {
        // TODO Auto-generated method stub
        return StatusCode.SURVEY_SUBMIT_SURVEY_SUCCESS;
    }

}
