package test.com.techedge.mp.frontend.adapter.entities.user.retrievetermofservice;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.RetrieveTermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrievePaymentDataUserServiceSuccess extends MockUserService {

    @Override
    public RetrieveTermsOfServiceData retrieveTermsOfService(String ticketId, String requestId, Boolean isOptional) {
        RetrieveTermsOfServiceData data = new RetrieveTermsOfServiceData();
        data.setStatusCode(StatusCode.RETRIEVE_TERMS_SERVICE_SUCCESS);
        return data;
    }

}
