package test.com.techedge.mp.frontend.adapter.entities.user.getactivevoucher;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.getactivevouchers.GetActiveVouchersBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.getactivevouchers.GetActiveVouchersRequest;
import com.techedge.mp.frontend.adapter.entities.user.getactivevouchers.GetActiveVouchersResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestGetActiveVoucherUserService extends BaseTestCase {
    private FrontendAdapterService       frontend;
    private GetActiveVouchersRequest     request;
    private GetActiveVouchersBodyRequest body;
    private GetActiveVouchersResponse    response;
    private Response                     baseResponse;
    private String                       json;
    private Gson                         gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new GetActiveVouchersRequest();
        body = new GetActiveVouchersBodyRequest();
        body.setRefresh(true);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testGetActiveVoucherSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockGetActiveVoucherUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setGetActiveVouchers(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (GetActiveVouchersResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_GET_ACTIVE_VOUCHERS_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testGetActiveVoucherFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockGetActiveVoucherUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setGetActiveVouchers(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (GetActiveVouchersResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_GET_ACTIVE_VOUCHERS_FAILURE, response.getStatus().getStatusCode());
    }
}
