package test.com.techedge.mp.frontend.adapter.entities.user.removevoucher;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.removevoucher.RemoveVoucherBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.removevoucher.RemoveVoucherRequest;

public class RemoveRemoveVoucherRequestTest extends BaseTestCase {

    private RemoveVoucherRequest     request;
    private RemoveVoucherBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RemoveVoucherRequest();
        body = new RemoveVoucherBodyRequest();
        body.setVoucherCode("123");
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_REMOVE_VOUCHER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}