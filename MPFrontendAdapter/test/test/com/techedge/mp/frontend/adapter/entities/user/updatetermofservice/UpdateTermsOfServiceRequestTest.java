package test.com.techedge.mp.frontend.adapter.entities.user.updatetermofservice;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.TermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.user.updatetermsofservice.UpdateTermsOfServiceRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatetermsofservice.UpdateTermsOfServiceRequestBody;

public class UpdateTermsOfServiceRequestTest extends BaseTestCase {

    private UpdateTermsOfServiceRequest     request;
    private UpdateTermsOfServiceRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new UpdateTermsOfServiceRequest();
        body = new UpdateTermsOfServiceRequestBody();
        TermsOfServiceData data = new TermsOfServiceData();
        data.setId("PRIVACY_1");
        data.setAccepted(true);
        List<TermsOfServiceData> lista = new ArrayList<TermsOfServiceData>(0);
        lista.add(data);
        body.setTermsOfService(lista);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_UPDATE_TERMS_OF_SERVICE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}