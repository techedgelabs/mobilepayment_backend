package test.com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockPostpaidService;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockApproveTransactionPostpaidServiceFailure extends MockPostpaidService {
    @Override
    public PostPaidApproveShopTransactionResponse approveShopTransaction(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId, String paymentMethodType,
            String encodedPin, Boolean useVoucher) {
        PostPaidApproveShopTransactionResponse response = new PostPaidApproveShopTransactionResponse();
        response.setMessageCode("message");
        response.setPinCheckMaxAttempts(2);
        response.setStatusCode(StatusCode.POP_APPROVE_FAILURE);
        return response;
    }
}
