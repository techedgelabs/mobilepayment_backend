package test.com.techedge.mp.frontend.adapter.entities.user.insertipaymentmethod;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.insertpaymentmethod.InsertPaymentMethodBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.insertpaymentmethod.InsertPaymentMethodRequest;

public class InsertPaymentMethodRequestTest extends BaseTestCase {

    private InsertPaymentMethodRequest     request;
    private InsertPaymentMethodBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new InsertPaymentMethodRequest();
        body = new InsertPaymentMethodBodyRequest();
        body.setNewPin("1234");
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setBrand("Brand");
        paymentMethod.setDefaultMethod(true);
        paymentMethod.setType("1");
        paymentMethod.setStatus(1);
        body.setPaymentMethod(paymentMethod);
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_INSERT_PAYMENT_METHOD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}