package test.com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockPostpaidService;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRejectShopTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRejectTransactionPostpaidServiceSuccess extends MockPostpaidService {
    @Override
    public PostPaidRejectShopTransactionResponse rejectShopTransaction(String requestID, String ticketID, String mpTransactionID) {
        PostPaidRejectShopTransactionResponse response = new PostPaidRejectShopTransactionResponse();
        response.setStatusCode(StatusCode.POP_REJECT_SUCCESS);
        response.setMessageCode("message");
        return response;
    }
}
