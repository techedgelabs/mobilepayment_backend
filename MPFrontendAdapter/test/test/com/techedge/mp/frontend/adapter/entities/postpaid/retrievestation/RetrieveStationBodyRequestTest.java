package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievestation;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestation.RetrieveStationBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestation.RetrieveStationUserPositionRequest;

public class RetrieveStationBodyRequestTest extends BaseTestCase {

    private RetrieveStationBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveStationBodyRequest();
        body.setBeaconCode("beacon");
        body.setCodeType("GPS");
        RetrieveStationUserPositionRequest position = new RetrieveStationUserPositionRequest();
        position.setLatitude(45.3);
        position.setLongitude(34.5);
        body.setUserPosition(position);
    }

    @Test
    public void testCodeTypeNull() {
        body.setCodeType(null);
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testCodeTypeIsEmpty() {
        body.setCodeType("");
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testCodeTypeMajor() {
        body.setCodeType("Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1");
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testBeaconCodeMajor() {
        body.setCodeType("GPS");
        body.setBeaconCode("test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testUserPositionNullGPS() {
        body.setBeaconCode(null);
        body.setUserPosition(null);
        assertEquals(StatusCode.STATION_RETRIEVE_USER_POSITION_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testBeaconCodeMajorBEACON_CODE() {
        body.setCodeType("BEACON_CODE");
        body.setBeaconCode("test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.STATION_RETRIEVE_BEACONCODE_WRONG, body.check().getStatusCode());
    }
}