package test.com.techedge.mp.frontend.adapter.entities.manager.rescuepassword;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.rescuepassword.ManagerRescuePasswordBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.rescuepassword.ManagerRescuePasswordRequest;
import com.techedge.mp.frontend.adapter.entities.manager.rescuepassword.ManagerRescuePasswordResponse;
import com.techedge.mp.frontend.adapter.entities.requests.ManagerRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class ManagerRescuePasswordManagerService extends BaseTestCase {
    private FrontendAdapterService           frontend;
    private ManagerRescuePasswordRequest     request;
    private ManagerRescuePasswordBodyRequest body;
    private ManagerRescuePasswordResponse    response;
    private Response                         baseResponse;
    private String                           json;
    private Gson                             gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new ManagerRescuePasswordRequest();
        body = new ManagerRescuePasswordBodyRequest();
        body.setEmail("a@a.it");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerRescuePasswordManagerServiceSuccess());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setRescuePassword(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerRescuePasswordResponse) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_RESCUE_PASSWORD_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerRescuePasswordManagerServiceFailure());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setRescuePassword(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerRescuePasswordResponse) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_RESCUE_PASSWORD_FAILURE, response.getStatus().getStatusCode());
    }
}
