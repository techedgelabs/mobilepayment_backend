package test.com.techedge.mp.frontend.adapter.webservices.common;

import java.util.Date;
import java.util.List;

import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.interfaces.AuthenticationResponse;
import com.techedge.mp.core.business.interfaces.CheckAvailabilityAmountData;
import com.techedge.mp.core.business.interfaces.GetActiveVouchersData;
import com.techedge.mp.core.business.interfaces.GetAvailableLoyaltyCardsData;
import com.techedge.mp.core.business.interfaces.MulticardPaymentResponse;
import com.techedge.mp.core.business.interfaces.PaymentInfoResponse;
import com.techedge.mp.core.business.interfaces.PaymentResponse;
import com.techedge.mp.core.business.interfaces.PrefixNumberResult;
import com.techedge.mp.core.business.interfaces.RetrieveCitiesData;
import com.techedge.mp.core.business.interfaces.RetrieveDocumentData;
import com.techedge.mp.core.business.interfaces.RetrieveTermsOfServiceData;
import com.techedge.mp.core.business.interfaces.RetrieveUserDeviceData;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.ValidatePaymentMethodResponse;
import com.techedge.mp.core.business.interfaces.loyalty.InfoRedemptionResponse;
import com.techedge.mp.core.business.interfaces.loyalty.RedemptionResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserUpdatePinResponse;

public class MockUserService implements UserServiceRemote {

    @Override
    public String createSystemUser() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String createUser(String ticketId, String requestId, User user, Long loyaltyCardId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String updateUser(String ticketId, String requestId, User user) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AuthenticationResponse authentication(String username, String password, String requestId, String deviceId, String deviceName, String deviceToken) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String logout(String ticketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String validateField(String ticketId, String requestId, String deviceId, String verificationType, String verificationField, String verificationCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ValidatePaymentMethodResponse validatePaymentMethod(String ticketId, String requestId, Long cardId, String cardType, Double verificationAmount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String updatePassword(String ticketId, String requestId, String oldPassword, String newPassword) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public UserUpdatePinResponse updatePin(String ticketId, String requestId, Long cardId, String cardType, String oldPin, String newPin) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentResponse insertPaymentMethod(String ticketId, String requestId, String paymentMethodType, String newPin) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String removePaymentMethod(String ticketId, String requestId, Long paymentMethodId, String paymentMethodType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String setDefaultPaymentMethod(String ticketId, String requestId, Long paymentMethodId, String paymentMethodType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentInfoResponse retrievePaymentData(String ticketId, String requestId, Long cardId, String cardType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String updateUserPaymentData(String transactionType, String transactionResult, String shopTransactionID, String bankTransactionID, String authorizationCode,
            String currency, String amount, String country, String buyerName, String buyerEmail, String errorCode, String errorDescription, String alertCode,
            String alertDescription, String TransactionKey, String token, String tokenExpiryMonth, String tokenExpiryYear, String cardBin, String TDLevel) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String checkAuthorization(String ticketID, Integer operationType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveCitiesData retrieveCities(String ticketID, String requestID, String searchKey) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String checkTransactionAuthorization(Double amount, String shopTransactionID, String valuta, String token, String operation) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String updateAvailableCap(Double amount, String shopTransactionID, String valuta, String token) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String updateEffectiveCap(Double amount, String shopTransactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String refundAvailableCap(String shopTransactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String rescuePassword(String ticketId, String requestId, String i_mail) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String recoverUsername(String ticketId, String requestId, String fiscalcode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveTermsOfServiceData retrieveTermsOfService(String ticketId, String requestId, Boolean isOptional) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveDocumentData retrieveDocument(String ticketId, String requestId, String documentID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String loadVoucher(String ticketId, String requestId, String voucherCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String loadPromoVoucher(String voucherCode, Long userId, String verificationValue) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GetActiveVouchersData getActiveVouchers(String ticketId, String requestId, Boolean refresh, String loyaltySessionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String removeVoucher(String ticketId, String requestId, String voucherCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String setDefaultLoyaltyCard(String ticketId, String requestId, String panCode, String eanCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GetAvailableLoyaltyCardsData getAvailableLoyaltyCards(String ticketId, String requestId, String fiscalcode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String setUseVoucher(String ticketId, String requestId, boolean useVoucher) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String resetPin(String ticketId, String requestId, String password, String newPin) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String skipPaymentMethodConfiguration(String ticketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PrefixNumberResult retrieveAllPrefixNumber(String ticketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String cancelMobilePhoneUpdate(String ticketId, String requestId, Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String updateMobilePhone(String ticketId, String requestId, String oldNumber, String newNumber) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String userResendValidation(String ticketId, String requestId, String id, String validitationType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CheckAvailabilityAmountData userCheckAvailabilityAmount(String ticketID, String requestID, Double amount, String loyaltySessionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String updateTermsOfService(String ticketID, String requestID, List<TermsOfService> termsOfServiceList) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String updateSmsLog(String correlationID, String destinationAddress, String message, String mtMessageID, Integer statusCode, Integer responseCode, Integer reasonCode,
            String responseMessage, Date operatorTimestamp, Date providerTimestamp, String operator, boolean firstUpdate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String checkLoyaltySession(String sessionId, String fiscalCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public InfoRedemptionResponse infoRedemption(String ticketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }
    @Override
    public RetrieveUserDeviceData retrieveActiveDevice(String ticketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String removeActiveUserDevice(String ticketId, String requestId, String userDeviceID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RedemptionResponse redemption(String ticketId, String requestId, Integer redemptionCode, String pin) {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
	public MulticardPaymentResponse insertMulticardPaymentMethod(String ticketId,
			String requestId) {
		// TODO Auto-generated method stub
		return null;
	}



}
