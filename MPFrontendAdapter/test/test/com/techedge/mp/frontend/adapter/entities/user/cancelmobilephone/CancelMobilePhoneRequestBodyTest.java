package test.com.techedge.mp.frontend.adapter.entities.user.cancelmobilephone;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.cancelmobilephone.CancelMobilePhoneRequestBody;

public class CancelMobilePhoneRequestBodyTest extends BaseTestCase {

    private CancelMobilePhoneRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new CancelMobilePhoneRequestBody();
        body.setMobilePhoneId(3213456789L);
    }

    @Test
    public void testMobiePhoneNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setMobilePhoneId(null);
        assertEquals(StatusCode.CANCEL_MOBILE_PHONE_INVALID_PARAMETERS, body.check().getStatusCode());
    }

}