package test.com.techedge.mp.frontend.adapter.entities.user.updatepin;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.updatepin.UpdatePinRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatepin.UpdatePinRequestBody;

public class UpdatePinRequestTest extends BaseTestCase {

    private UpdatePinRequest     request;
    private UpdatePinRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new UpdatePinRequest();
        body = new UpdatePinRequestBody();
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(1L);
        paymentMethod.setStatus(1);
        paymentMethod.setType("card");
        body.setNewPin("1234");
        body.setOldPin("1235");
        body.setPaymentMethod(paymentMethod);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_PIN_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}