package test.com.techedge.mp.frontend.adapter.entities.user.updatepin;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.updatepin.UpdatePinRequestBody;

public class UpdatePinRequestBodyTest extends BaseTestCase {

    private UpdatePinRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new UpdatePinRequestBody();
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(1L);
        paymentMethod.setStatus(1);
        paymentMethod.setType("card");
        body.setNewPin("1234");
        body.setOldPin("1235");
        body.setPaymentMethod(paymentMethod);
    }

    @Test
    public void testNewPinNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setNewPin(null);
        assertEquals(StatusCode.USER_PIN_NEW_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testNewPinIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setNewPin("");
        assertEquals(StatusCode.USER_PIN_NEW_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testNewPinMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setNewPin("Test1test1test1test1test1test1test1test1test1Test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_PIN_NEW_WRONG, body.check().getStatusCode());
    }

}