package test.com.techedge.mp.frontend.adapter.entities.user.skippaymentmethodconfiguration;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.skippaymentmethodconfiguration.SkipPaymentMethodConfigurationRequest;
import com.techedge.mp.frontend.adapter.entities.user.skippaymentmethodconfiguration.SkipPaymentMethodConfigurationResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestSkipPaymentMethodConfigurationUserService extends BaseTestCase {
    private FrontendAdapterService                 frontend;
    private SkipPaymentMethodConfigurationRequest  request;
    private SkipPaymentMethodConfigurationResponse response;
    private Response                               baseResponse;
    private String                                 json;
    private Gson                                   gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new SkipPaymentMethodConfigurationRequest();
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockSkipPaymentMethodConfigurationUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setSkipPaymentMethodConfiguration(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (SkipPaymentMethodConfigurationResponse) baseResponse.getEntity();
        assertEquals(StatusCode.SKIP_PAYMENT_METHOD_CONFIGURATION_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockSkipPaymentMethodConfigurationUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setSkipPaymentMethodConfiguration(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (SkipPaymentMethodConfigurationResponse) baseResponse.getEntity();
        assertEquals(StatusCode.SKIP_PAYMENT_METHOD_CONFIGURATION_FAILURE, response.getStatus().getStatusCode());
    }
}
