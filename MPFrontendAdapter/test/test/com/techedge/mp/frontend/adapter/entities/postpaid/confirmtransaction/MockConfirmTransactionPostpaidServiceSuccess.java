package test.com.techedge.mp.frontend.adapter.entities.postpaid.confirmtransaction;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockPostpaidService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockConfirmTransactionPostpaidServiceSuccess extends MockPostpaidService {
    @Override
    public String confirmTransaction(String requestID, String ticketID, String transactionID) {
        // TODO Auto-generated method stub
        return StatusCode.POP_CONFIRM_TRANSACTION_SUCCESS;
    }
}
