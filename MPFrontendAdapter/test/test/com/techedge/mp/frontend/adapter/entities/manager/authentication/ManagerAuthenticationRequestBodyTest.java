package test.com.techedge.mp.frontend.adapter.entities.manager.authentication;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.authentication.ManagerAuthenticationBodyRequest;

public class ManagerAuthenticationRequestBodyTest extends BaseTestCase {

    private ManagerAuthenticationBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new ManagerAuthenticationBodyRequest();
        body.setDeviceID("device");
        body.setDeviceName("name");
        body.setPassword("Password12345GH");
        body.setRequestID(getRequestID_true());
        body.setUsername("user");
    }

    @Test
    public void testUsernameNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setUsername(null);
        assertEquals(StatusCode.MANAGER_AUTH_EMAIL_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testUsernameMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setUsername("test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.MANAGER_AUTH_EMAIL_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testPasswordNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPassword(null);
        assertEquals(StatusCode.MANAGER_AUTH_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testPasswordMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPassword("test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.MANAGER_AUTH_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testRequestIDNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRequestID(null);
        assertEquals(ResponseHelper.MANAGER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testRequestIDIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRequestID("");
        assertEquals(ResponseHelper.MANAGER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testRequestIDMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRequestID("test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(ResponseHelper.MANAGER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testDeviceIDMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setDeviceID("devicedevicedevicedevicedevicedevicedevicedevice");
        assertEquals(StatusCode.MANAGER_AUTH_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testDeviceIDIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setDeviceID("");
        assertEquals(StatusCode.MANAGER_AUTH_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testDeviceNameMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setDeviceName("devicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevicedevice");
        assertEquals(ResponseHelper.MANAGER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testDeviceNameIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setDeviceName("");
        assertEquals(ResponseHelper.MANAGER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

}