package test.com.techedge.mp.frontend.adapter.entities.refuel.confirm;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockConfirmRefuelTrasactionServiceFailure extends MockTransactionService {
    @Override
    public String confirmRefuel(String ticketID, String requestID, String refuelID) {
        // TODO Auto-generated method stub
        return StatusCode.REFUEL_CONFIRM_FAILURE;
    }
}