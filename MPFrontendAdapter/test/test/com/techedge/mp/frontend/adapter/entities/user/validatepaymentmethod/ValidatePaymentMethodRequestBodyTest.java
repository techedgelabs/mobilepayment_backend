package test.com.techedge.mp.frontend.adapter.entities.user.validatepaymentmethod;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.validatepaymentmethod.ValidatePaymentMethodBodyRequest;

public class ValidatePaymentMethodRequestBodyTest extends BaseTestCase {

    private ValidatePaymentMethodBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new ValidatePaymentMethodBodyRequest();
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setBrand("Brand");
        paymentMethod.setDefaultMethod(true);
        paymentMethod.setId(1L);
        paymentMethod.setType("1");
        paymentMethod.setStatus(1);
        body.setPaymentMethod(paymentMethod);
        body.setVerificationAmount(1);
    }

    @Test
    public void testPaymentMethodNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPaymentMethod(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testPaymentMethodIDNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getPaymentMethod().setId(null);
        assertEquals(StatusCode.USER_VALIDATE_PAYMENT_METHOD_ID_ERROR, body.check().getStatusCode());
    }

    @Test
    public void testPaymentMethodTypeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getPaymentMethod().setType(null);
        assertEquals(StatusCode.USER_VALIDATE_PAYMENT_METHOD_TYPE_ERROR, body.check().getStatusCode());
    }
    
    @Test
    public void testPaymentMethodTypeMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getPaymentMethod().setType("test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_VALIDATE_PAYMENT_METHOD_TYPE_ERROR, body.check().getStatusCode());
    }
    
    @Test
    public void testPaymentMethodAmountNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVerificationAmount(null);
        assertEquals(StatusCode.USER_VALIDATE_PAYMENT_METHOD_AMOUNT_ERROR, body.check().getStatusCode());
    }
    
    @Test
    public void testPaymentMethodAmountMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVerificationAmount(-3);
        assertEquals(StatusCode.USER_VALIDATE_PAYMENT_METHOD_AMOUNT_ERROR, body.check().getStatusCode());
    }
}