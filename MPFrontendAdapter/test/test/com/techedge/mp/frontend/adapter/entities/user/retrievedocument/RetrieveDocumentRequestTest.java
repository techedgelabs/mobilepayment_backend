package test.com.techedge.mp.frontend.adapter.entities.user.retrievedocument;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.retrievedocument.RetrieveDocumentBody;
import com.techedge.mp.frontend.adapter.entities.user.retrievedocument.RetrieveDocumentRequest;

public class RetrieveDocumentRequestTest extends BaseTestCase {

    private RetrieveDocumentRequest request;
    private RetrieveDocumentBody    body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveDocumentRequest();
        body = new RetrieveDocumentBody();
        body.setDocumentId("documentID");
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.RETRIEVE_DOCUMENT_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testDocumentNull() {

        body.setDocumentId(null);

        assertEquals(StatusCode.RETRIEVE_DOCUMENT_FAILURE, body.check().getStatusCode());
    }
}