package test.com.techedge.mp.frontend.adapter.entities.user.insertipaymentmethod;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.PaymentResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockInsertPaymentMethodUserServiceSuccess extends MockUserService {

    @Override
    public PaymentResponse insertPaymentMethod(String ticketId, String requestId, String paymentMethodType, String newPin) {
        PaymentResponse response = new PaymentResponse();
        response.setStatusCode(StatusCode.USER_INSERT_PAYMENT_METHOD_SUCCESS);
        response.setShopLogin("shop");
        response.setSecureString("secure");
        response.setPaymentMethodType("type");
        return response;
    }

}
