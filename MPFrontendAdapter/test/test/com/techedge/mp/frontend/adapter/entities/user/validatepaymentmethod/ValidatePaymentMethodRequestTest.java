package test.com.techedge.mp.frontend.adapter.entities.user.validatepaymentmethod;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.validatepaymentmethod.ValidatePaymentMethodBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.validatepaymentmethod.ValidatePaymentMethodRequest;

public class ValidatePaymentMethodRequestTest extends BaseTestCase {

    private ValidatePaymentMethodRequest     request;
    private ValidatePaymentMethodBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new ValidatePaymentMethodRequest();
        body = new ValidatePaymentMethodBodyRequest();
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setBrand("Brand");
        paymentMethod.setDefaultMethod(true);
        paymentMethod.setId(1L);
        paymentMethod.setType("1");
        paymentMethod.setStatus(1);
        body.setPaymentMethod(paymentMethod);
        body.setVerificationAmount(1);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_VALIDATE_PAYMENT_METHOD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}