package test.com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionPaymentMethodBody;

public class ApproveTransactionBodyRequestTest extends BaseTestCase {

    private ApproveTransactionBodyRequest       body;
    private ApproveTransactionPaymentMethodBody method;

    // assigning the values
    protected void setUp() {
        method = new ApproveTransactionPaymentMethodBody();
        body = new ApproveTransactionBodyRequest();
        body.setTransactionID("qwertyuiqwertyuiqwertyuiqwertyui");
        body.setUseVoucher(true);
        method.setId(1L);
        method.setType("type");
        body.setPaymentMethod(method);
    }

    @Test
    public void testTransactionIDNull() {
        body.setTransactionID(null);
        assertEquals(StatusCode.POP_APPROVE_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testTransactionIDIsEmpty() {
        body.setTransactionID("");
        assertEquals(StatusCode.POP_APPROVE_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testTransactionIDNotValid() {
        body.setTransactionID("ciao");
        assertEquals(StatusCode.POP_APPROVE_ID_WRONG, body.check().getStatusCode());
    }
}