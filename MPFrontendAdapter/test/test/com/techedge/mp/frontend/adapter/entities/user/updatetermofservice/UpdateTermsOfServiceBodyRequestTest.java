package test.com.techedge.mp.frontend.adapter.entities.user.updatetermofservice;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.TermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.user.updatetermsofservice.UpdateTermsOfServiceRequestBody;

public class UpdateTermsOfServiceBodyRequestTest extends BaseTestCase {

    private UpdateTermsOfServiceRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new UpdateTermsOfServiceRequestBody();
        TermsOfServiceData data = new TermsOfServiceData();
        data.setId("PRIVACY_1");
        data.setAccepted(true);
        List<TermsOfServiceData> lista = new ArrayList<TermsOfServiceData>(0);
        body.setTermsOfService(lista);
    }

    @Test
    public void testAmountNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setTermsOfService(null);
        assertEquals(StatusCode.USER_UPDATE_TERMS_OF_SERVICE_SUCCESS, body.check().getStatusCode());
    }

}