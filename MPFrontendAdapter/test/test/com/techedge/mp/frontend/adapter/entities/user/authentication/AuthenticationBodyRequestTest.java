package test.com.techedge.mp.frontend.adapter.entities.user.authentication;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.authentication.AuthenticationUserRequestBody;

public class AuthenticationBodyRequestTest extends BaseTestCase {

    private AuthenticationUserRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new AuthenticationUserRequestBody();
        body.setDeviceID("nexus");
        body.setDeviceName("device");
        body.setPassword("123");
        body.setRequestID("EJB-1345678");
        body.setUsername("user");

    }

    @Test
    public void testUsernameNull() {
        body.setUsername(null);
        assertEquals(StatusCode.USER_AUTH_EMAIL_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testUsernameIsEmpty() {
        body.setUsername("test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_AUTH_EMAIL_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testPasswordNull() {
        body.setPassword(null);
        assertEquals(StatusCode.USER_AUTH_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testPasswordIsEmpty() {
        body.setPassword("test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_AUTH_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testRequestIDNull() {
        body.setRequestID(null);
        assertEquals(StatusCode.USER_AUTH_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testRequestIDIsEmpty() {
        body.setRequestID("test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_AUTH_FAILURE, body.check().getStatusCode());
    }

}