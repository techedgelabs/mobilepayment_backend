package test.com.techedge.mp.frontend.adapter.entities.user.retrievecities;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrievecities.RetrieveCitiesRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrievecities.RetrieveCitiesRequestBody;
import com.techedge.mp.frontend.adapter.entities.user.retrievecities.RetrieveCitiesResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrieveCitiesUserService extends BaseTestCase {
    private FrontendAdapterService    frontend;
    private RetrieveCitiesRequest     request;
    private RetrieveCitiesRequestBody body;
    private RetrieveCitiesResponse    response;
    private Response                  baseResponse;
    private String                    json;
    private Gson                      gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrieveCitiesRequest();
        body = new RetrieveCitiesRequestBody();
        body.setSearchKey("key");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRetrieveCitiesUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setRetrieveCities(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RetrieveCitiesResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIEVE_CITIES_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRetrieveCitiesUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setRetrieveCities(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RetrieveCitiesResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIEVE_CITIES_FAILURE, response.getStatus().getStatusCode());
    }
}
