package test.com.techedge.mp.frontend.adapter.entities.user.updatemobilephone;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.updatemobilephone.UpdateMobilePhoneRequestBody;

public class UpdateMobilePhoneRequestBodyTest extends BaseTestCase {

    private UpdateMobilePhoneRequestBody body;
    private MobilePhone                  data;

    // assigning the values
    protected void setUp() {
        body = new UpdateMobilePhoneRequestBody();
        data = new MobilePhone();
        data.setNumber("3456678908");
        data.setPrefix("0078");
        body.setMobilePhone(data);
    }

    @Test
    public void testMobilePhoneNumberNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setNumber(null);
        body.setMobilePhone(data);

        assertEquals(StatusCode.UPD_MOBILE_PHONE_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testMobilePhonePrefixNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setPrefix("");
        body.setMobilePhone(data);

        assertEquals(StatusCode.UPD_MOBILE_PHONE_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testMobilePhoneNumberIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setNumber("");
        body.setMobilePhone(data);

        assertEquals(StatusCode.UPD_MOBILE_PHONE_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testMobilePhonePrefixIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setPrefix("");
        body.setMobilePhone(data);

        assertEquals(StatusCode.UPD_MOBILE_PHONE_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testMobilePhoneNumberMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setNumber("test1test1test1test1test1test1test1test1test1");
        body.setMobilePhone(data);

        assertEquals(StatusCode.UPD_MOBILE_PHONE_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testMobilePhonePrefixMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setPrefix("test1test1test1test1test1");
        assertEquals(StatusCode.UPD_MOBILE_PHONE_INVALID_PARAMETERS, body.check().getStatusCode());
    }
}