package test.com.techedge.mp.frontend.adapter.entities.postpaid.confirmtransaction;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.confirm.ConfirmTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.confirm.ConfirmTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.confirm.ConfirmTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.requests.PoPRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class ConfirmTransactionManagerService extends BaseTestCase {
    private FrontendAdapterService        frontend;
    private ConfirmTransactionRequest     request;
    private ConfirmTransactionBodyRequest body;
    private ConfirmTransactionResponse    response;
    private Response                      baseResponse;
    private String                        json;
    private Gson                          gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new ConfirmTransactionRequest();
        body = new ConfirmTransactionBodyRequest();
        body.setTransactionID("qwertyuiqwertyuiqwertyuiqwertyui");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testConfirmTransactionSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockConfirmTransactionPostpaidServiceSuccess());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setConfirmTransaction(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (ConfirmTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.POP_CONFIRM_TRANSACTION_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testConfirmTransactionFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockConfirmTransactionPostpaidServiceFailure());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setConfirmTransaction(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (ConfirmTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.POP_CONFIRM_TRANSACTION_FAILURE, response.getStatus().getStatusCode());
    }
}
