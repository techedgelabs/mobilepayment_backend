package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail.RetrieveRefuelPaymentDetailBodyRequest;

public class RetrieveRefuelPaymentDetailBodyTest extends BaseTestCase {

    private RetrieveRefuelPaymentDetailBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveRefuelPaymentDetailBodyRequest();
        body.setRefuelID("qwertyuiopasdfghjklzxcvbnmqwerty");;
    }

    @Test
    public void testRefuelIDNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefuelID(null);
        assertEquals(StatusCode.RETRIEVE_PAYMENT_DETAIL_REFUELID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testRefuelIDNotValid() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefuelID("123");
        assertEquals(StatusCode.RETRIEVE_PAYMENT_DETAIL_REFUELID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testRefuelIDIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefuelID("");
        assertEquals(StatusCode.RETRIEVE_PAYMENT_DETAIL_REFUELID_WRONG, body.check().getStatusCode());
    }
}