package test.com.techedge.mp.frontend.adapter.entities.survey.submitsurvey;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.survey.submitsurvey.SubmitSurveyBodyRequest;

public class SubmitSurveyRequestBodyTest extends BaseTestCase {

    private SubmitSurveyBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new SubmitSurveyBodyRequest();
        body.setCode("code");
        body.setKey("key");
    }

    @Test
    public void testCodeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCode(null);
        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testCodeIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCode("");
        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testKeyNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setKey(null);
        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testKeyIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setKey("");
        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }
}