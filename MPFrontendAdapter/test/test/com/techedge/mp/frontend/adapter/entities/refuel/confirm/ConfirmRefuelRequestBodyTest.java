package test.com.techedge.mp.frontend.adapter.entities.refuel.confirm;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.confirm.ConfirmRefuelBodyRequest;

public class ConfirmRefuelRequestBodyTest extends BaseTestCase {

    private ConfirmRefuelBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new ConfirmRefuelBodyRequest();
        body.setRefuelID("refuelrefuelrefuelrefuelrefuel12");
    }

    @Test
    public void testRefuelIDNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefuelID(null);
        assertEquals(StatusCode.REFUEL_CONFIRM_REFUEL_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testRefuelIDLenght() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefuelID("test");
        assertEquals(StatusCode.REFUEL_CONFIRM_REFUEL_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testRefuelIDIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefuelID("");
        assertEquals(StatusCode.REFUEL_CONFIRM_REFUEL_ID_WRONG, body.check().getStatusCode());
    }
}