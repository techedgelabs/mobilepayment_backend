package test.com.techedge.mp.frontend.adapter.entities.refuel.createtransaction;

import java.util.ArrayList;
import java.util.List;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockForecourtInfoService;

import com.techedge.mp.bpel.operation.refuel.business.interfaces.RegisterInfo;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetPumpStatusResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.StationDetail;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockForecourtInfoServiceSuccess extends MockForecourtInfoService {

    @Override
    public GetStationDetailsResponse getStationDetails(String requestID, String stationID, String pumpID, Boolean pumpDetailsReq) {
        GetStationDetailsResponse response = new GetStationDetailsResponse();
        StationDetail stationDetail = new StationDetail();
        stationDetail.setAddress("address");
        stationDetail.setCountry("ITA");
        stationDetail.setCity("rome");
        stationDetail.setLatitude("45.3");
        stationDetail.setLongitude("34.4");
        stationDetail.setProvince("RM");
        List<PumpDetail> pumInfoList = new ArrayList<PumpDetail>(0);
        PumpDetail info = new PumpDetail();
        info.setPumpID("pumpID");
        info.setPumpNumber("123");
        info.setPumpStatus("status");
        info.setRefuelMode("refuel");

        ProductDetail pInfo = new ProductDetail();
        pInfo.setFuelType("fuel");
        pInfo.setProductDescription("desc");
        pInfo.setProductID("productID");
        pInfo.setProductPrice(1.2);
        List<ProductDetail> listP = new ArrayList<ProductDetail>(0);
        listP.add(pInfo);

        info.setProductDetails(listP);
        pumInfoList.add(info);
        stationDetail.setPumpDetails(pumInfoList);

        List<RegisterInfo> registerList = new ArrayList<RegisterInfo>(0);
        RegisterInfo infoRegister = new RegisterInfo();
        infoRegister.setRegisterID("id");
        infoRegister.setRegisterNumber("numb");
        infoRegister.setRegisterStatus("status");
        registerList.add(infoRegister);
        stationDetail.setStationID("station");
        response.setStationDetail(stationDetail);
        response.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);
        return response;
    }

    @Override
    public GetPumpStatusResponse getPumpStatus(String requestID, String stationID, String pumpID, String transactionID) {
        GetPumpStatusResponse response = new GetPumpStatusResponse();
        response.setRefuelMode("mode");
        response.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);
        return response;
    }

}
