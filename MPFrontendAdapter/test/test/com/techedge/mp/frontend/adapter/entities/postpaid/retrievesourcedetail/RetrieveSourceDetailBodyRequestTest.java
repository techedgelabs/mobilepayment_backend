package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievesourcedetail;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievesourcedetail.RetrieveSourceDetailBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationUserPositionRequest;

public class RetrieveSourceDetailBodyRequestTest extends BaseTestCase {

    private RetrieveSourceDetailBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveSourceDetailBodyRequest();
        body.setBeaconCode("beacon");
        body.setCodeType("QR-CODE");
        body.setSourceID("source");
        RetrieveStationUserPositionRequest position = new RetrieveStationUserPositionRequest();
        position.setLatitude(45.3);
        position.setLongitude(34.5);
        body.setUserPosition(position);
    }

    @Test
    public void testCodeTypeNull() {
        body.setCodeType(null);
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testCodeTypeIsEmpty() {
        body.setCodeType("");
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testCodeTypeNotValid() {
        body.setCodeType("Ciao");
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testCodeTypeMajor() {
        body.setCodeType("Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1Ciao1");
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testSourceIDNull() {
        body.setSourceID(null);
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testSourceIDIsEmpty() {
        body.setSourceID("");
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testSourceIDMajor() {
        body.setSourceID("test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testBeaconCodeMajor() {
        body.setBeaconCode("test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.STATION_RETRIEVE_BEACONCODE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testLatitudeNull() {
        body.getUserPosition().setLatitude(null);
        assertEquals(StatusCode.STATION_RETRIEVE_USER_POSITION_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testLongitudeNull() {
        body.getUserPosition().setLongitude(null);
        assertEquals(StatusCode.STATION_RETRIEVE_USER_POSITION_WRONG, body.check().getStatusCode());
    }
}