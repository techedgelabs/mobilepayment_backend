package test.com.techedge.mp.frontend.adapter.entities.user.getactivevoucher;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.getactivevouchers.GetActiveVouchersBodyRequest;

public class GetActiveVoucherRequestBodyTest extends BaseTestCase {

    private GetActiveVouchersBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new GetActiveVouchersBodyRequest();
        body.setRefresh(true);
    }

    @Test
    public void testRefreshNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefresh(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }
}