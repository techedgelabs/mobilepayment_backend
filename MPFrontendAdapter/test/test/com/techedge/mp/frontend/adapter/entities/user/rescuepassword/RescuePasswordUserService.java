package test.com.techedge.mp.frontend.adapter.entities.user.rescuepassword;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.entities.refuel.retrievestations.MockParameterServiceSuccess;
import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.rescuepassword.RescuePasswordRequest;
import com.techedge.mp.frontend.adapter.entities.user.rescuepassword.RescuePasswordRequestBody;
import com.techedge.mp.frontend.adapter.entities.user.rescuepassword.RescuePasswordResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RescuePasswordUserService extends BaseTestCase {
    private FrontendAdapterService    frontend;
    private RescuePasswordRequest     request;
    private RescuePasswordRequestBody body;
    private RescuePasswordResponse    response;
    private Response                  baseResponse;
    private String                    json;
    private Gson                      gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RescuePasswordRequest();
        body = new RescuePasswordRequestBody();
        body.setEmail("a@a-it");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRescuePasswordUserServiceSuccess());
        EJBHomeCache.getInstance().setParametersService(new MockParameterServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setRescuePasswordMethod(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RescuePasswordResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_RESCUE_PASSWORD_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRescuePasswordUserServiceFailure());
        EJBHomeCache.getInstance().setParametersService(new MockParameterServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setRescuePasswordMethod(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RescuePasswordResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_RESCUE_PASSWORD_FAILURE, response.getStatus().getStatusCode());
    }
}
