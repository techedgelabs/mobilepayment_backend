package test.com.techedge.mp.frontend.adapter.entities.user.checkavailableamount;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.checkavailabilityamount.CheckAvailabilityAmountBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatepin.UpdatePinRequestBody;

public class CheckAvailabilityAmountBodyRequestTest extends BaseTestCase {

    private CheckAvailabilityAmountBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new CheckAvailabilityAmountBodyRequest();
        body.setAmount(1);
    }

    @Test
    public void testAmountNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setAmount(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

}