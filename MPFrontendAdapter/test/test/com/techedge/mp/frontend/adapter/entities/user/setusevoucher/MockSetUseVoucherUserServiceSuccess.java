package test.com.techedge.mp.frontend.adapter.entities.user.setusevoucher;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockSetUseVoucherUserServiceSuccess extends MockUserService {

    @Override
    public String setUseVoucher(String ticketId, String requestId, boolean useVoucher) {
        // TODO Auto-generated method stub
        return StatusCode.USER_SET_USE_VOUCHER_SUCCESS;
    }

}
