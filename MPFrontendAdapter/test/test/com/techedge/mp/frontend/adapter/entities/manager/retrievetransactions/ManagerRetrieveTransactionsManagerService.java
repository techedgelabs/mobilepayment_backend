package test.com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions.ManagerRetrieveTransactionsBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions.ManagerRetrieveTransactionsRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions.ManagerRetrieveTransactionsResponse;
import com.techedge.mp.frontend.adapter.entities.requests.ManagerRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class ManagerRetrieveTransactionsManagerService extends BaseTestCase {
    private FrontendAdapterService                 frontend;
    private ManagerRetrieveTransactionsRequest     request;
    private ManagerRetrieveTransactionsBodyRequest body;
    private ManagerRetrieveTransactionsResponse    response;
    private Response                               baseResponse;
    private String                                 json;
    private Gson                                   gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new ManagerRetrieveTransactionsRequest();
        body = new ManagerRetrieveTransactionsBodyRequest();
        body.setPumpMaxTransactions(3);
        body.setStationID("stationID");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerRetrieveTransactionsManagerServiceSuccess());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setRetrieveTransactions(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerRetrieveTransactionsResponse) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_RETRIEVE_TRANSACTIONS_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerRetrieveTransactionsManagerServiceFailure());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setRetrieveTransactions(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerRetrieveTransactionsResponse) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_RETRIEVE_TRANSACTIONS_FAILURE, response.getStatus().getStatusCode());
    }
}
