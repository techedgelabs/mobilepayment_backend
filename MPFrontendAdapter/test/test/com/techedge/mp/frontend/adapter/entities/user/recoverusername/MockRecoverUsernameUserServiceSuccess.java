package test.com.techedge.mp.frontend.adapter.entities.user.recoverusername;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRecoverUsernameUserServiceSuccess extends MockUserService {

    @Override
    public String recoverUsername(String ticketId, String requestId, String fiscalcode) {
        // TODO Auto-generated method stub
        return StatusCode.USER_RECOVER_USERNAME_SUCCESS;
    }

}
