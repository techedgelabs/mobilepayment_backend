package test.com.techedge.mp.frontend.adapter.entities.user.setdefaultpaymentmethod;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.setdefaultpaymentmethod.SetDefaultPaymentMethodBodyRequest;

public class SetDefaultPaymentMethodRequestBodyTest extends BaseTestCase {

    private SetDefaultPaymentMethodBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new SetDefaultPaymentMethodBodyRequest();
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setBrand("Brand");
        paymentMethod.setId(1L);
        paymentMethod.setDefaultMethod(true);
        paymentMethod.setType("1");
        paymentMethod.setStatus(1);
        body.setPaymentMethod(paymentMethod);
    }

    @Test
    public void testPaymentMethodNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPaymentMethod(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testPaymentMethodIDNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getPaymentMethod().setId(null);
        assertEquals(StatusCode.USER_SET_DEFAULT_PAYMENT_METHOD_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testPaymentMethodTypeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getPaymentMethod().setType(null);
        assertEquals(StatusCode.USER_SET_DEFAULT_PAYMENT_METHOD_TYPE_WRONG, body.check().getStatusCode());
    }
}