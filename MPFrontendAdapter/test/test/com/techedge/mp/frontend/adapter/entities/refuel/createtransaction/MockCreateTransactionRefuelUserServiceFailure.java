package test.com.techedge.mp.frontend.adapter.entities.refuel.createtransaction;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.ResponseHelper;

public class MockCreateTransactionRefuelUserServiceFailure extends MockUserService {
    @Override
    public String checkAuthorization(String ticketID, Integer operationType) {
        // TODO Auto-generated method stub
        return ResponseHelper.CHECK_AUTHORIZATION_FAILED;
    }
}
