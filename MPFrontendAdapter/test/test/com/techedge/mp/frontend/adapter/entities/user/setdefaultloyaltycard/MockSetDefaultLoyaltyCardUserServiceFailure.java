package test.com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockSetDefaultLoyaltyCardUserServiceFailure extends MockUserService {

    @Override
    public String setDefaultLoyaltyCard(String ticketId, String requestId, String panCode, String eanCode) {
        // TODO Auto-generated method stub
        return StatusCode.USER_SET_DEFAULT_LOYALTY_CARD_FAILURE;
    }

}
