package test.com.techedge.mp.frontend.adapter.entities.user.getactivevoucher;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.getactivevouchers.GetActiveVouchersBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.getactivevouchers.GetActiveVouchersRequest;

public class GetActiveVoucherRequestTest extends BaseTestCase {

    private GetActiveVouchersRequest     request;
    private GetActiveVouchersBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new GetActiveVouchersRequest();
        body = new GetActiveVouchersBodyRequest();
        body.setRefresh(true);
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_GET_ACTIVE_VOUCHERS_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}