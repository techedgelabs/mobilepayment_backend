package test.com.techedge.mp.frontend.adapter.webservices.common;

import com.techedge.mp.core.business.SurveyServiceRemote;
import com.techedge.mp.core.business.interfaces.Survey;
import com.techedge.mp.core.business.interfaces.SurveyAnswers;

public class MockSurveyService implements SurveyServiceRemote {

    @Override
    public Survey surveyGet(String ticketId, String requestId, String code) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String surveySubmit(String ticketId, String requestId, String code, String key, SurveyAnswers answers) {
        // TODO Auto-generated method stub
        return null;
    }

}
