package test.com.techedge.mp.frontend.adapter.entities.user.checkavailableamount;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.checkavailabilityamount.CheckAvailabilityAmountBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.checkavailabilityamount.CheckAvailabilityAmountRequest;

public class CheckAvailabilityAmountRequestTest extends BaseTestCase {

    private CheckAvailabilityAmountRequest     request;
    private CheckAvailabilityAmountBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new CheckAvailabilityAmountRequest();
        body = new CheckAvailabilityAmountBodyRequest();
        body.setAmount(1);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.CHECK_AVAILABILITY_AMOUNT_SUCCESS, request.check().getStatusCode());

    }


    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}