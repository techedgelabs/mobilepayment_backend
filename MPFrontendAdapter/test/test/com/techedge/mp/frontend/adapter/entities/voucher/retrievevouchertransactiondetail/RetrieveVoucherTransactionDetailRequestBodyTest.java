package test.com.techedge.mp.frontend.adapter.entities.voucher.retrievevouchertransactiondetail;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.voucher.retrievevouchertransactiondetail.RetrieveVoucherTransactionDetailBodyRequest;

public class RetrieveVoucherTransactionDetailRequestBodyTest extends BaseTestCase {

    private RetrieveVoucherTransactionDetailBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveVoucherTransactionDetailBodyRequest();
        body.setVoucherTransactionId("transaction");
    }

    @Test
    public void testVoucherTransactionIdNull() {
        body.setVoucherTransactionId(null);
        assertEquals(StatusCode.RETRIEVE_VOUCHER_TRANSACTION_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testVoucherTransactionIdIsEmpty() {
        body.setVoucherTransactionId("");
        assertEquals(StatusCode.RETRIEVE_VOUCHER_TRANSACTION_INVALID_PARAMETERS, body.check().getStatusCode());
    }

}