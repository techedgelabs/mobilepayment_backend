package test.com.techedge.mp.frontend.adapter.entities.user.getavailableloyalycards;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.getavailableloyaltycards.GetAvailableLoyaltyCardsBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.getavailableloyaltycards.GetAvailableLoyaltyCardsRequest;

public class GetAvailableLoyaltyCardsRequestTest extends BaseTestCase {

    private GetAvailableLoyaltyCardsRequest     request;
    private GetAvailableLoyaltyCardsBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new GetAvailableLoyaltyCardsRequest();
        body = new GetAvailableLoyaltyCardsBodyRequest();
        body.setFiscalcode("qwertyuiopasdfgh");
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_GET_AVAILABLE_LOYALTY_CARDS_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}