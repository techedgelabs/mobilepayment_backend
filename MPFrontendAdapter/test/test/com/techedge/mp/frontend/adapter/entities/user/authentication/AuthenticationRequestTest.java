package test.com.techedge.mp.frontend.adapter.entities.user.authentication;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.authentication.AuthenticationUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.authentication.AuthenticationUserRequestBody;

public class AuthenticationRequestTest extends BaseTestCase {

    private AuthenticationUserRequest     request;
    private AuthenticationUserRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new AuthenticationUserRequest();
        body = new AuthenticationUserRequestBody();
        body.setDeviceID("nexus");
        body.setDeviceName("device");
        body.setPassword("123");
        body.setRequestID("EJB-1345678");
        body.setUsername("user");
        request.setBody(body);
    }

    @Test
    public void testBodyOk() {

        assertEquals(StatusCode.USER_AUTH_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}