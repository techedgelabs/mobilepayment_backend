package test.com.techedge.mp.frontend.adapter.entities.refuel.confirm;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockBpelServiceRemoteService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockForecourtInfoService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.confirm.ConfirmRefuelBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.confirm.ConfirmRefuelRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.confirm.ConfirmRefuelResponse;
import com.techedge.mp.frontend.adapter.entities.requests.RefuelRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class ConfirmRefuelRefuelService extends BaseTestCase {
    private FrontendAdapterService   frontend;
    private ConfirmRefuelRequest     request;
    private ConfirmRefuelBodyRequest body;
    private ConfirmRefuelResponse    response;
    private Response                 baseResponse;
    private String                   json;
    private Gson                     gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new ConfirmRefuelRequest();
        body = new ConfirmRefuelBodyRequest();
        body.setRefuelID("refuelrefuelrefuelrefuelrefuel12");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoService());
        EJBHomeCache.getInstance().setBpelService(new MockBpelServiceRemoteService());

    }

    @Test
    public void testConfirmRefuelSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockConfirmRefuelTrasactionServiceSuccess());
        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setConfirmRefuel(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (ConfirmRefuelResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_CONFIRM_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testConfirmRefuelFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockConfirmRefuelTrasactionServiceFailure());
        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setConfirmRefuel(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (ConfirmRefuelResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_CONFIRM_FAILURE, response.getStatus().getStatusCode());
    }
}
