package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory.RetrieveTransactionHistoryBodyRequest;

public class RetrieveTransactionHistoryBodyRequestTest extends BaseTestCase {

    private RetrieveTransactionHistoryBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveTransactionHistoryBodyRequest();
        body.setItemsLimit(3);
        body.setPageOffset(4);
        body.setRefuelPostpaid(true);
        body.setRefuelPrepaid(true);
        body.setShop(true);
        CustomDate startDate = new CustomDate();
        startDate.setDay(1);
        startDate.setMonth(1);
        startDate.setYear(1990);
        body.setStartDate(startDate);
        CustomDate endDate = new CustomDate();
        endDate.setDay(1);
        endDate.setMonth(1);
        endDate.setYear(1990);
        body.setEndDate(endDate);
    }

    @Test
    public void testItemLimit() {
        body.setItemsLimit(50);
        assertEquals(StatusCode.RETRIEVE_TRANSACTION_HISTORY_LIMIT_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateYearMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setYear(1300);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateYearMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setYear(3200);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateMonthMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setMonth(0);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateMonthMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setMonth(14);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateDayMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setDay(0);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateDayMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setDay(34);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateYearMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setYear(1300);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateYearMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setYear(3200);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateMonthMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setMonth(0);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateMonthMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setMonth(14);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateDayMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setDay(0);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateDayMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setDay(34);
        assertEquals(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateNull() {
        body.setStartDate(null);
        assertEquals(StatusCode.RETRIEVE_TRANSACTION_HISTORY_STARTDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateNull() {
        body.setEndDate(null);
        assertEquals(StatusCode.RETRIEVE_TRANSACTION_HISTORY_ENDDATE_WRONG, body.check().getStatusCode());
    }

}