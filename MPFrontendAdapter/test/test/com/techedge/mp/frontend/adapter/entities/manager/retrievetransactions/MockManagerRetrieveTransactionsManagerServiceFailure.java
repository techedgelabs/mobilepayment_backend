package test.com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions;

import java.util.ArrayList;
import java.util.List;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockManagerService;

import com.techedge.mp.core.business.interfaces.ManagerRetrieveTransactionsResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PaymentHistory;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockManagerRetrieveTransactionsManagerServiceFailure extends MockManagerService {
    @Override
    public ManagerRetrieveTransactionsResponse retrieveTransactions(String ticketId, String requestId, String stationID, Integer pumpMaxTransactions) {
        ManagerRetrieveTransactionsResponse response = new ManagerRetrieveTransactionsResponse();
        response.setStatusCode(StatusCode.MANAGER_RETRIEVE_TRANSACTIONS_FAILURE);
        List<PaymentHistory> list = new ArrayList<PaymentHistory>(0);
        response.setTransactionHistory(list);
        return response;
    }
}
