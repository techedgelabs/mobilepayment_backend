package test.com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions.ManagerRetrieveTransactionsBodyRequest;

public class ManagerRetrieveTransactionsBodyRequestTest extends BaseTestCase {

    private ManagerRetrieveTransactionsBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new ManagerRetrieveTransactionsBodyRequest();
        body.setPumpMaxTransactions(3);
        body.setStationID("stationID");
    }

    @Test
    public void testStationIDNull() {
        body.setStationID(null);
        assertEquals(ResponseHelper.MANAGER_RETRIEVE_TRANSACTIONS_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testStationIDIsEmpty() {
        body.setStationID("");
        assertEquals(ResponseHelper.MANAGER_RETRIEVE_TRANSACTIONS_INVALID_PARAMETERS, body.check().getStatusCode());
    }
    
    @Test
    public void testPumpNull() {
        body.setPumpMaxTransactions(null);
        assertEquals(ResponseHelper.MANAGER_RETRIEVE_TRANSACTIONS_INVALID_PARAMETERS, body.check().getStatusCode());
    }

}