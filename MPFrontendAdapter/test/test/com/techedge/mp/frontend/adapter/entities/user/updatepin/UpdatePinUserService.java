package test.com.techedge.mp.frontend.adapter.entities.user.updatepin;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatepin.UpdatePinRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatepin.UpdatePinRequestBody;
import com.techedge.mp.frontend.adapter.entities.user.updatepin.UpdatePinResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class UpdatePinUserService extends BaseTestCase {
    private FrontendAdapterService frontend;
    private UpdatePinRequest       request;
    private UpdatePinRequestBody   body;
    private UpdatePinResponse      response;
    private Response               baseResponse;
    private String                 json;
    private Gson                   gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new UpdatePinRequest();
        body = new UpdatePinRequestBody();
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(1L);
        paymentMethod.setStatus(1);
        paymentMethod.setType("card");
        body.setNewPin("1234");
        body.setOldPin("1235");
        body.setPaymentMethod(paymentMethod);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockUpdatePinUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setUpdatePin(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (UpdatePinResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_PIN_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockUpdatePinUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setUpdatePin(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (UpdatePinResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_PIN_FAILURE, response.getStatus().getStatusCode());
    }
}
