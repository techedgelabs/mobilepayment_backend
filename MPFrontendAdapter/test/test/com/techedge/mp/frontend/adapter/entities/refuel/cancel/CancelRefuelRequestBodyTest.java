package test.com.techedge.mp.frontend.adapter.entities.refuel.cancel;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.cancel.CancelRefuelBodyRequest;

public class CancelRefuelRequestBodyTest extends BaseTestCase {

    private CancelRefuelBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new CancelRefuelBodyRequest();
        body.setRefuelID("refuelrefuelrefuelrefuelrefuel12");
    }

    @Test
    public void testRefuelIDNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefuelID(null);
        assertEquals(StatusCode.REFUEL_CANCEL_REFUEL_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testRefuelIDLenght() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefuelID("test");
        assertEquals(StatusCode.REFUEL_CANCEL_REFUEL_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testRefuelIDIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefuelID("");
        assertEquals(StatusCode.REFUEL_CANCEL_REFUEL_ID_WRONG, body.check().getStatusCode());
    }
}