package test.com.techedge.mp.frontend.adapter.entities.user.getavailableloyalycards;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.GetAvailableLoyaltyCardsData;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockGetAvailableLoyaltyCardsUserServiceFailure extends MockUserService {

    @Override
    public GetAvailableLoyaltyCardsData getAvailableLoyaltyCards(String ticketId, String requestId, String fiscalcode) {
        GetAvailableLoyaltyCardsData data = new GetAvailableLoyaltyCardsData();
        data.setBalance(1);
        data.setStatusCode(StatusCode.USER_GET_AVAILABLE_LOYALTY_CARDS_GENERIC_ERROR);
        return data;
    }

}
