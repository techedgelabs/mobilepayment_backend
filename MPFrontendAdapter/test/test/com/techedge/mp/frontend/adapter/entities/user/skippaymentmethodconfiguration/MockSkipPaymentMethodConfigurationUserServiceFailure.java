package test.com.techedge.mp.frontend.adapter.entities.user.skippaymentmethodconfiguration;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockSkipPaymentMethodConfigurationUserServiceFailure extends MockUserService {

    @Override
    public String skipPaymentMethodConfiguration(String ticketId, String requestId) {
        // TODO Auto-generated method stub
        return StatusCode.SKIP_PAYMENT_METHOD_CONFIGURATION_FAILURE;
    }

}
