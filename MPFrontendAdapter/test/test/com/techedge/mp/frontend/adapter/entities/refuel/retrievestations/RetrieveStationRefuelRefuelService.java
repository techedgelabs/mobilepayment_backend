package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievestations;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockBpelServiceRemoteService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockForecourtInfoService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationResponse;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationUserPositionRequest;
import com.techedge.mp.frontend.adapter.entities.requests.RefuelRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrieveStationRefuelRefuelService extends BaseTestCase {
    private FrontendAdapterService     frontend;
    private RetrieveStationRequest     request;
    private RetrieveStationBodyRequest body;
    private RetrieveStationResponse    response;
    private Response                   baseResponse;
    private String                     json;
    private Gson                       gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrieveStationRequest();
        body = new RetrieveStationBodyRequest();
        RetrieveStationUserPositionRequest position = new RetrieveStationUserPositionRequest();
        position.setLatitude(45.4);
        position.setLongitude(35.5);
        body.setBeaconCode("beacon");
        body.setCode("code");
        body.setCodeType("code");
        body.setUserPosition(position);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoService());
        EJBHomeCache.getInstance().setBpelService(new MockBpelServiceRemoteService());
    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockRetrieveStationRefuelTrasactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockRetrieveStationRefuelUserServiceSuccess());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtServiceSuccess());
        EJBHomeCache.getInstance().setParametersService(new MockParameterServiceSuccess());
        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setRetrieveStation(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (RetrieveStationResponse) baseResponse.getEntity();
        assertEquals(StatusCode.STATION_RETRIEVE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockRetrieveStationRefuelTransactionServiceFailure());
        EJBHomeCache.getInstance().setUserService(new MockRetrieveStationRefuelUserServiceSuccess());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtServiceFailure());
        EJBHomeCache.getInstance().setParametersService(new MockParameterServiceSuccess());
        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setRetrieveStation(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (RetrieveStationResponse) baseResponse.getEntity();
        assertEquals(StatusCode.STATION_RETRIEVE_FAILURE, response.getStatus().getStatusCode());
    }

}
