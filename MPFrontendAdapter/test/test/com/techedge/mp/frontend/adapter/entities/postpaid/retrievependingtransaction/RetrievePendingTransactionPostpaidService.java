package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievependingtransaction;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievependingtransaction.RetrievePendingTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievependingtransaction.RetrievePendingTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.requests.PoPRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrievePendingTransactionPostpaidService extends BaseTestCase {
    private FrontendAdapterService             frontend;
    private RetrievePendingTransactionRequest  request;
    private RetrievePendingTransactionResponse response;
    private Response                           baseResponse;
    private String                             json;
    private Gson                               gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrievePendingTransactionRequest();
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrievePendingTransactionPostpaidServiceSuccess());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrievePendingTransaction(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrievePendingTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.STATION_RETRIEVE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrievePendingTransactionPostpaidServiceFailure());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrievePendingTransaction(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrievePendingTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.STATION_RETRIEVE_FAILURE, response.getStatus().getStatusCode());
    }
}
