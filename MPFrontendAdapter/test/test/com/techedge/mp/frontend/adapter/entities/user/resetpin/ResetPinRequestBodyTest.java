package test.com.techedge.mp.frontend.adapter.entities.user.resetpin;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.resetpin.ResetPinRequestBody;

public class ResetPinRequestBodyTest extends BaseTestCase {

    private ResetPinRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new ResetPinRequestBody();
        body.setPassword("Password123");
    }

    @Test
    public void testPasswordNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPassword(null);
        assertEquals(StatusCode.USER_PWD_OLD_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testPasswordIsZero() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPassword("");
        assertEquals(StatusCode.USER_PWD_PASSWORD_SHORT, body.check().getStatusCode());
    }

    @Test
    public void testPasswordMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPassword("Test1Test1Test1Test1Test1Test1Test1Test1Test1Test1");
        assertEquals(StatusCode.USER_PWD_OLD_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testPasswordNotValid() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPassword("test");
        assertEquals(StatusCode.USER_PWD_PASSWORD_SHORT, body.check().getStatusCode());
    }

}