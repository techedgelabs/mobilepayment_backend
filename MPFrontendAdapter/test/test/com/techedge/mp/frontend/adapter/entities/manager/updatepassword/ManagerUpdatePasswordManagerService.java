package test.com.techedge.mp.frontend.adapter.entities.manager.updatepassword;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.updatepassword.ManagerUpdatePasswordBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.updatepassword.ManagerUpdatePasswordRequest;
import com.techedge.mp.frontend.adapter.entities.manager.updatepassword.ManagerUpdatePasswordResponse;
import com.techedge.mp.frontend.adapter.entities.requests.ManagerRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class ManagerUpdatePasswordManagerService extends BaseTestCase {
    private FrontendAdapterService           frontend;
    private ManagerUpdatePasswordRequest     request;
    private ManagerUpdatePasswordBodyRequest body;
    private ManagerUpdatePasswordResponse    response;
    private Response                         baseResponse;
    private String                           json;
    private Gson                             gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new ManagerUpdatePasswordRequest();
        body = new ManagerUpdatePasswordBodyRequest();
        body.setNewPassword("Password123");
        body.setOldPassword("pAssWord345");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerUpdatePasswordManagerServiceSuccess());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setUpdatePassword(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerUpdatePasswordResponse) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_PWD_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerUpdatePasswordManagerServiceFailure());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setUpdatePassword(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerUpdatePasswordResponse) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_PWD_FAILURE, response.getStatus().getStatusCode());
    }
}
