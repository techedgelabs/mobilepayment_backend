package test.com.techedge.mp.frontend.adapter.entities.refuel.createtransaction;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockBpelServiceRemoteService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockForecourtInfoService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionCredential;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionDataRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.requests.RefuelRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class CreateTransactionRefuelRefuelService extends BaseTestCase {
    private FrontendAdapterService             frontend;
    private CreateRefuelTransactionRequest     request;
    private CreateRefuelTransactionBodyRequest body;
    private CreateRefuelTransactionDataRequest data;
    private CreateRefuelTransactionResponse    response;
    private Response                           baseResponse;
    private String                             json;
    private Gson                               gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new CreateRefuelTransactionRequest();
        body = new CreateRefuelTransactionBodyRequest();
        data = new CreateRefuelTransactionDataRequest();
        data.setAmount(1);
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setBrand("Brand");
        paymentMethod.setId(1L);
        paymentMethod.setDefaultMethod(true);
        paymentMethod.setType("1");
        paymentMethod.setStatus(1);
        data.setPaymentMethod(paymentMethod);
        data.setOutOfRange("true");
        data.setPumpID("123456789");
        data.setStationID("123");
        data.setUseVoucher(true);
        data.setOutOfRange("true");
        body.setRefuelPaymentData(data);
        CreateRefuelTransactionCredential credential = new CreateRefuelTransactionCredential();
        credential.setTicketID(getTicketID_true());
        credential.setRequestID(getRequestID_true());
        credential.setPin("1234");
        request.setCredential(credential);
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoService());
        EJBHomeCache.getInstance().setBpelService(new MockBpelServiceRemoteService());
    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockCreateTransactionRefuelTrasactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockCreateTransactionRefuelUserServiceSuccess());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoServiceSuccess());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setCreateRefuelTransaction(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (CreateRefuelTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameCheckAuthorizationFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockCreateTransactionRefuelTrasactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockCreateTransactionRefuelUserServiceFailure());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoServiceSuccess());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setCreateRefuelTransaction(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (CreateRefuelTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_REQU_UNAUTHORIZED, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernamePumpFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockCreateTransactionRefuelTrasactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockCreateTransactionRefuelUserServiceSuccess());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoServicePumpFailure());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setCreateRefuelTransaction(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (CreateRefuelTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_FAILURE, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameForecourtFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockCreateTransactionRefuelTrasactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockCreateTransactionRefuelUserServiceSuccess());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoServiceFailure());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setCreateRefuelTransaction(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (CreateRefuelTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_FAILURE, response.getStatus().getStatusCode());
    }

}
