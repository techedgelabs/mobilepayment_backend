package test.com.techedge.mp.frontend.adapter.entities.user.update;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.SecurityData;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserPersonalDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserPersonalDataRequestBody;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserResponse;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserUserData;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestUpdateUserService extends BaseTestCase {
    private FrontendAdapterService            frontend;
    private UpdateUserPersonalDataRequest     request;
    private UpdateUserPersonalDataRequestBody body;
    private UpdateUserUserData                data;
    private UpdateUserResponse                response;
    private Response                          baseResponse;
    private String                            json;
    private Gson                              gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new UpdateUserPersonalDataRequest();
        body = new UpdateUserPersonalDataRequestBody();
        data = new UpdateUserDataRequest();
        SecurityData securityData = new SecurityData();
        securityData.setEmail("a@a.it");
        securityData.setPassword("Password12345");
        data.setFirstName("name");
        data.setLastName("surname");
        CustomDate customDate = new CustomDate();
        customDate.setDay(10);
        customDate.setMonth(1);
        customDate.setYear(1990);
        data.setDateOfBirth(customDate);
        data.setBirthMunicipality("muni");
        data.setBirthProvince("province");
        data.setLanguage("IT");
        data.setSex("M");
        request.setCredential(createCredentialTrue());
        body.setUserPersonalData(data);
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testCreateSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockUpdateUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setUpdateUserPersonalData(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (UpdateUserResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_UPD_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testCreateFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockUpdateUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setUpdateUserPersonalData(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (UpdateUserResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_UPD_FAILURE, response.getStatus().getStatusCode());
    }

}
