package test.com.techedge.mp.frontend.adapter.entities.refuel.createtransaction;

import java.util.ArrayList;
import java.util.List;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockForecourtInfoService;

import com.techedge.mp.bpel.operation.refuel.business.interfaces.ProductInfo;
import com.techedge.mp.bpel.operation.refuel.business.interfaces.PumpInfo;
import com.techedge.mp.bpel.operation.refuel.business.interfaces.RegisterInfo;
import com.techedge.mp.bpel.operation.refuel.business.interfaces.StationInfo;
import com.techedge.mp.bpel.operation.refuel.business.interfaces.StationInfoRequest;
import com.techedge.mp.bpel.operation.refuel.business.interfaces.StationInfoResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetPumpStatusResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockForecourtInfoServiceFailure extends MockForecourtInfoService {
    public StationInfoResponse getStationDetails(StationInfoRequest stationDetailsRequest) {
        StationInfoResponse response = new StationInfoResponse();
        StationInfo stationInfo = new StationInfo();
        stationInfo.setAddress("address");
        stationInfo.setCountry("ITA");
        stationInfo.setCity("rome");
        stationInfo.setLatitude("45.3");
        stationInfo.setLongitude("34.4");
        stationInfo.setName("name");
        stationInfo.setProvince("RM");
        List<PumpInfo> pumInfoList = new ArrayList<PumpInfo>(0);
        PumpInfo info = new PumpInfo();
        info.setPumpID("pumpID");
        info.setPumpNumber("123");
        info.setPumpStatus("status");
        info.setRefuelMode("refuel");

        ProductInfo pInfo = new ProductInfo();
        pInfo.setFuelType("fuel");
        pInfo.setProductDescription("desc");
        pInfo.setProductID("productID");
        pInfo.setProductPrice(1.2);
        List<ProductInfo> listP = new ArrayList<ProductInfo>(0);
        listP.add(pInfo);

        info.setProductList(listP);
        pumInfoList.add(info);
        stationInfo.setPumpList(pumInfoList);

        List<RegisterInfo> registerList = new ArrayList<RegisterInfo>(0);
        RegisterInfo infoRegister = new RegisterInfo();
        infoRegister.setRegisterID("id");
        infoRegister.setRegisterNumber("numb");
        infoRegister.setRegisterStatus("status");
        registerList.add(infoRegister);
        stationInfo.setRegisterList(registerList);
        stationInfo.setStationID("station");
        response.setStationInfo(stationInfo);
        response.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);
        return response;
    }

    @Override
    public GetPumpStatusResponse getPumpStatus(String requestID, String stationID, String pumpID, String transactionID) {
        GetPumpStatusResponse response = new GetPumpStatusResponse();
        response.setRefuelMode("mode");
        response.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_FAILURE);
        return response;
    }
}
