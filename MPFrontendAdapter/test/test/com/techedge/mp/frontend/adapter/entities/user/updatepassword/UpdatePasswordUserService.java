package test.com.techedge.mp.frontend.adapter.entities.user.updatepassword;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatepassword.UpdatePasswordRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatepassword.UpdatePasswordRequestBody;
import com.techedge.mp.frontend.adapter.entities.user.updatepassword.UpdatePasswordResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class UpdatePasswordUserService extends BaseTestCase {
    private FrontendAdapterService    frontend;
    private UpdatePasswordRequest     request;
    private UpdatePasswordRequestBody body;
    private UpdatePasswordResponse    response;
    private Response                  baseResponse;
    private String                    json;
    private Gson                      gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new UpdatePasswordRequest();
        body = new UpdatePasswordRequestBody();
        body.setNewPassword("Password123");
        body.setOldPassword("NewPasswoer456");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockUpdatePasswordUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setUpdatePassword(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (UpdatePasswordResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_PWD_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockUpdatePasswordUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setUpdatePassword(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (UpdatePasswordResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_PWD_FAILURE, response.getStatus().getStatusCode());
    }
}
