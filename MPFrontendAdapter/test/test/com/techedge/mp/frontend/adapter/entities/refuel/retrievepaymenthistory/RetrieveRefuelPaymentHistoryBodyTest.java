package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory.RetrieveRefuelPaymentHistoryBodyRequest;

public class RetrieveRefuelPaymentHistoryBodyTest extends BaseTestCase {

    private RetrieveRefuelPaymentHistoryBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveRefuelPaymentHistoryBodyRequest();
        CustomDate customDate = new CustomDate();
        customDate.setDay(10);
        customDate.setMonth(1);
        customDate.setYear(1990);
        CustomDate customDateEnd = new CustomDate();
        customDateEnd.setDay(10);
        customDateEnd.setMonth(1);
        customDateEnd.setYear(1990);
        body.setStartDate(customDate);
        body.setEndDate(customDateEnd);
        body.setItemsLimit(1);
        body.setPageOffset(1);
        body.setDetails(true);
    }

    @Test
    public void testItemLimitsNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setItemsLimit(null);
        assertEquals(StatusCode.PAYMENT_RETRIEVE_HISTORY_LIMIT_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testItemLimitsOver() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setItemsLimit(25);
        assertEquals(StatusCode.RETRIEVE_TRANSACTION_HISTORY_LIMIT_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setStartDate(null);
        assertEquals(StatusCode.PAYMENT_RETRIEVE_HISTORY_STARTDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateYearMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setYear(1300);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_STARTDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateYearMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setYear(3200);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_STARTDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateMonthMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setMonth(0);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_STARTDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateMonthMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setMonth(14);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_STARTDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateDayMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setDay(0);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_STARTDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testStartDateDayMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getStartDate().setDay(34);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_STARTDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateYearMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setYear(1300);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_ENDDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateYearMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setYear(3200);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_ENDDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateMonthMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setMonth(0);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_ENDDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateMonthMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setMonth(14);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_ENDDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateDayMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setDay(0);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_ENDDATE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testEndDateDayMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.getEndDate().setDay(34);
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_ENDDATE_WRONG, body.check().getStatusCode());
    }
}