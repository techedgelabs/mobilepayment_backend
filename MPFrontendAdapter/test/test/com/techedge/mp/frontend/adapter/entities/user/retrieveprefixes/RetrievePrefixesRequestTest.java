package test.com.techedge.mp.frontend.adapter.entities.user.retrieveprefixes;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.retrieveprefixes.RetrievePrefixesRequest;

public class RetrievePrefixesRequestTest extends BaseTestCase {

    private RetrievePrefixesRequest request;

    // assigning the values
    protected void setUp() {
        request = new RetrievePrefixesRequest();
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.RETRIEVE_PREFIXES_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}