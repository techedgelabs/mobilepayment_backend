package test.com.techedge.mp.frontend.adapter.entities.user.validatepaymentmethod;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockValidatePaymentMethodUserServiceSuccess extends MockUserService {

    @Override
    public String validateField(String ticketId, String requestId, String deviceId, String verificationType, String verificationField, String verificationCode) {
        // TODO Auto-generated method stub
        return StatusCode.USER_VALID_SUCCESS;
    }

}
