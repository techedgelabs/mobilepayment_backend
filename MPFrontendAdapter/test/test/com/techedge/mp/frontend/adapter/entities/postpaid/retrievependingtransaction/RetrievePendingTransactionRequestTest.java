package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievependingtransaction;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievependingtransaction.RetrievePendingTransactionRequest;

public class RetrievePendingTransactionRequestTest extends BaseTestCase {

    private RetrievePendingTransactionRequest request;

    // assigning the values
    protected void setUp() {
        request = new RetrievePendingTransactionRequest();
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.REFUEL_PENDING_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}