package test.com.techedge.mp.frontend.adapter.entities.manager.authentication;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.authentication.ManagerAuthenticationBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.authentication.ManagerAuthenticationRequest;
import com.techedge.mp.frontend.adapter.entities.manager.authentication.ManagerAuthenticationResponseFailure;
import com.techedge.mp.frontend.adapter.entities.manager.authentication.ManagerAuthenticationResponseSuccess;
import com.techedge.mp.frontend.adapter.entities.requests.ManagerRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class ManagerAuthenticationManagerService extends BaseTestCase {
    private FrontendAdapterService               frontend;
    private ManagerAuthenticationRequest         request;
    private ManagerAuthenticationBodyRequest     body;
    private ManagerAuthenticationResponseSuccess response;
    private ManagerAuthenticationResponseFailure response_fail;
    private Response                             baseResponse;
    private String                               json;
    private Gson                                 gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new ManagerAuthenticationRequest();
        body = new ManagerAuthenticationBodyRequest();
        body.setDeviceID("device");
        body.setDeviceName("name");
        body.setPassword("Password12345GH");
        body.setRequestID(getRequestID_true());
        body.setUsername("user");
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testManagerAuthSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerAuthenticationManagerServiceSuccess());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setAuthentication(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerAuthenticationResponseSuccess) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_AUTH_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testManagerAuthFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerAuthenticationManagerServiceFailure());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setAuthentication(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response_fail = (ManagerAuthenticationResponseFailure) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_AUTH_FAILURE, response_fail.getStatus().getStatusCode());
    }
}
