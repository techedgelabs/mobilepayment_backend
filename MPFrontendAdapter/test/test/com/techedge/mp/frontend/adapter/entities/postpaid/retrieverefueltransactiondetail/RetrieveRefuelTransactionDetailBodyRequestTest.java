package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail.RetrieveRefuelTransactionDetailBodyRequest;

public class RetrieveRefuelTransactionDetailBodyRequestTest extends BaseTestCase {

    private RetrieveRefuelTransactionDetailBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveRefuelTransactionDetailBodyRequest();
        body.setTransactionId("qwertyuiqwertyuiqwertyuiqwertyui");
    }

    @Test
    public void testTransactionIDNull() {
        body.setTransactionId(null);

        assertEquals(StatusCode.RETRIVE_REFUEL_TRANSACTION_DETAIL_INVALID_PARAMETERS, body.check().getStatusCode());

    }

    @Test
    public void testTransactionIDIsEmpty() {

        body.setTransactionId("");

        assertEquals(StatusCode.RETRIVE_REFUEL_TRANSACTION_DETAIL_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testTransactionIDNot32() {

        body.setTransactionId("aswdfff");

        assertEquals(StatusCode.RETRIVE_REFUEL_TRANSACTION_DETAIL_INVALID_PARAMETERS, body.check().getStatusCode());
    }
}