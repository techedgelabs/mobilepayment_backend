package test.com.techedge.mp.frontend.adapter.entities.manager.updatepassword;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.updatepassword.ManagerUpdatePasswordBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.updatepassword.ManagerUpdatePasswordRequest;

public class ManagerUpdatePasswordRequestTest extends BaseTestCase {

    private ManagerUpdatePasswordRequest     request;
    private ManagerUpdatePasswordBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new ManagerUpdatePasswordRequest();
        body = new ManagerUpdatePasswordBodyRequest();
        body.setNewPassword("Password123");
        body.setOldPassword("pAssWord345");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.MANAGER_PWD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.getCredential().setTicketID(getTicketID_false());
        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}