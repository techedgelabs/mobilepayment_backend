package test.com.techedge.mp.frontend.adapter.entities.manager.logout;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.logout.ManagerLogoutRequest;
import com.techedge.mp.frontend.adapter.entities.manager.logout.ManagerLogoutResponse;
import com.techedge.mp.frontend.adapter.entities.requests.ManagerRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class ManagerLogoutManagerService extends BaseTestCase {
    private FrontendAdapterService frontend;
    private ManagerLogoutRequest   request;
    private ManagerLogoutResponse  response;
    private Response               baseResponse;
    private String                 json;
    private Gson                   gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new ManagerLogoutRequest();
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testManagerAuthSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerLogoutManagerServiceSuccess());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setLogout(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerLogoutResponse) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_LOGOUT_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testManagerAuthFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerLogoutManagerServiceFailure());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setLogout(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerLogoutResponse) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_LOGOUT_FAILURE, response.getStatus().getStatusCode());
    }
}
