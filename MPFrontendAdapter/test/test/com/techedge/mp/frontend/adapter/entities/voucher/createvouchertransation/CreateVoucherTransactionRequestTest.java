package test.com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionCredential;
import com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation.CreateVoucherTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation.CreateVoucherTransactionCredential;
import com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation.CreateVoucherTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation.VoucherPaymentDataRequest;

public class CreateVoucherTransactionRequestTest extends BaseTestCase {

    private CreateVoucherTransactionRequest     request;
    private CreateVoucherTransactionBodyRequest body;
    private VoucherPaymentDataRequest           data;
    private PaymentMethod                       paymentMethod;

    // assigning the values
    protected void setUp() {
        request = new CreateVoucherTransactionRequest();
        body = new CreateVoucherTransactionBodyRequest();
        data = new VoucherPaymentDataRequest();
        paymentMethod = new PaymentMethod();
        paymentMethod.setType("tipo");
        paymentMethod.setId(1L);
        data.setAmount(10);
        data.setPaymentMethod(paymentMethod);
        body.setVoucherPaymentData(data);
        CreateVoucherTransactionCredential credential = new CreateVoucherTransactionCredential();
        credential.setTicketID(getTicketID_true());
        credential.setRequestID(getRequestID_true());
        credential.setPin("1234");
        request.setCredential(credential);
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.CREATE_VOUCHER_TRANSACTION_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        CreateVoucherTransactionCredential credential = new CreateVoucherTransactionCredential();
        credential.setTicketID(getTicketID_false());
        credential.setRequestID(getRequestID_false());
        credential.setPin("1234");
        request.setCredential(credential);

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        CreateVoucherTransactionCredential credential = new CreateVoucherTransactionCredential();
        credential.setTicketID(getTicketID_false());
        credential.setRequestID(getRequestID_true());
        credential.setPin("1234");
        request.setCredential(credential);

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        CreateVoucherTransactionCredential credential = new CreateVoucherTransactionCredential();
        credential.setTicketID(getTicketID_true());
        credential.setRequestID(getRequestID_false());
        credential.setPin("1234");
        request.setCredential(credential);

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}