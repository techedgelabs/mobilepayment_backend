package test.com.techedge.mp.frontend.adapter.entities.survey.getsurvey;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.survey.getsurvey.GetSurveyBodyRequest;

public class GetSurveyRequestBodyTest extends BaseTestCase {

    private GetSurveyBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new GetSurveyBodyRequest();
        body.setCode("code");
    }

    @Test
    public void testCodeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCode(null);
        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testCodeIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCode("");
        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }
}