package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievestations;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationUserPositionRequest;

public class RetrieveStationRefuelRequestTest extends BaseTestCase {

    private RetrieveStationRequest     request;
    private RetrieveStationBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveStationRequest();
        body = new RetrieveStationBodyRequest();
        RetrieveStationUserPositionRequest position = new RetrieveStationUserPositionRequest();
        position.setLatitude(45.4);
        position.setLongitude(35.5);
        body.setBeaconCode("beacon");
        body.setCode("code");
        body.setCodeType("code");
        body.setUserPosition(position);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.STATION_RETRIEVE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.getCredential().setTicketID(getTicketID_false());
        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}