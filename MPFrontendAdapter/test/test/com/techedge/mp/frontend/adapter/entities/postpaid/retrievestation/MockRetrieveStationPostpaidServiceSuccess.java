package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievestation;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockPostpaidService;

import com.techedge.mp.core.business.interfaces.CashInfo;
import com.techedge.mp.core.business.interfaces.postpaid.CorePumpDetail;
import com.techedge.mp.core.business.interfaces.postpaid.GetSourceDetailResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrieveStationPostpaidServiceSuccess extends MockPostpaidService {
    @Override
    public GetSourceDetailResponse getSourceDetail(String requestID, String ticketID, String codeType, String sourceID, String beaconCode, Double userPositionLatitude,
            Double userPositionLongitude) {
        GetSourceDetailResponse response = new GetSourceDetailResponse();
        response.setAmount(1.1);
        response.setCashInfo(new CashInfo());
        response.setMessageCode("message");
        response.setObjectStatus("bject");
        response.setObjectType("type");
        CorePumpDetail pump = new CorePumpDetail();
        pump.setPumpID("value");
        pump.setPumpNumber("123");
        pump.setPumpStatus("status");
        pump.setRefuelMode("refuel");
        response.setPumpInfo(pump);
        response.setStationAddress("address");
        response.setStationCity("city");
        response.setStationCountry("italy");
        response.setStationID("001");
        response.setStationLatitude(45.3);
        response.setStationLongitude(34.2);
        response.setStationName("name");
        response.setStationProvince("RM");
        response.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);
        response.setTransactionID("000011");
        response.setTransactionStatus("statusT");
        return response;
    }
}
