package test.com.techedge.mp.frontend.adapter.entities.user.setusevoucher;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.setusevoucher.SetUseVoucherBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.setusevoucher.SetUseVoucherRequest;
import com.techedge.mp.frontend.adapter.entities.user.setusevoucher.SetUseVoucherResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestSetUseVoucherUserService extends BaseTestCase {
    private FrontendAdapterService   frontend;
    private SetUseVoucherRequest     request;
    private SetUseVoucherBodyRequest body;
    private SetUseVoucherResponse    response;
    private Response                 baseResponse;
    private String                   json;
    private Gson                     gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new SetUseVoucherRequest();
        body = new SetUseVoucherBodyRequest();
        body.setUseVoucher(true);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testSetUseVoucherSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockSetUseVoucherUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setSetUseVoucher(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (SetUseVoucherResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_SET_USE_VOUCHER_SUCCESS, response.getStatus().getStatusCode());
    }
}
