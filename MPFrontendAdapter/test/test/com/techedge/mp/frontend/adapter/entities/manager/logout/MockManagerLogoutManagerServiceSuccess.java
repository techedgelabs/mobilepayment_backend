package test.com.techedge.mp.frontend.adapter.entities.manager.logout;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockManagerService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockManagerLogoutManagerServiceSuccess extends MockManagerService {
    @Override
    public String logout(String ticketId, String requestId) {
        // TODO Auto-generated method stub
        return StatusCode.MANAGER_LOGOUT_SUCCESS;
    }
}
