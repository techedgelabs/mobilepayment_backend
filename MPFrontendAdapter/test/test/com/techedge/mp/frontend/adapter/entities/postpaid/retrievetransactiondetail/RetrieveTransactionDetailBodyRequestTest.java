package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail.RetrieveTransactionDetailBodyRequest;

public class RetrieveTransactionDetailBodyRequestTest extends BaseTestCase {

    private RetrieveTransactionDetailBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveTransactionDetailBodyRequest();
        body.setTransactionId("codecodecodecodecodecodecodecode");
    }

    @Test
    public void testTransactionIdNull() {
        body.setTransactionId(null);
        assertEquals(StatusCode.POP_DETAIL_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testTransactionIsEmpty() {
        body.setTransactionId("");
        assertEquals(StatusCode.POP_DETAIL_ID_WRONG, body.check().getStatusCode());
    }
    
    @Test
    public void testTransactionNotValid() {
        body.setTransactionId("aa");
        assertEquals(StatusCode.POP_DETAIL_ID_WRONG, body.check().getStatusCode());
    }
    
}