package test.com.techedge.mp.frontend.adapter.entities.manager.authentication;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.manager.authentication.ManagerAuthenticationBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.authentication.ManagerAuthenticationRequest;

public class ManagerAuthenticationRequestTest extends BaseTestCase {

    private ManagerAuthenticationRequest     request;
    private ManagerAuthenticationBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new ManagerAuthenticationRequest();
        body = new ManagerAuthenticationBodyRequest();
        body.setDeviceID("device");
        body.setDeviceName("name");
        body.setPassword("Password12345GH");
        body.setRequestID(getRequestID_true());
        body.setUsername("user");
        request.setBody(body);
    }

    @Test
    public void testBodyTrue() {
        assertEquals(ResponseHelper.MANAGER_AUTH_SUCCESS, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(ResponseHelper.MANAGER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}