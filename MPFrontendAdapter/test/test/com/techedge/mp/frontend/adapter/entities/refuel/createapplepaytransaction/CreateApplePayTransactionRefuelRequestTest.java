package test.com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction.CreateApplePayRefuelTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction.CreateApplePayRefuelTransactionDataRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction.CreateApplePayRefuelTransactionRequest;

public class CreateApplePayTransactionRefuelRequestTest extends BaseTestCase {

    private CreateApplePayRefuelTransactionRequest     createApplePayRefuelTransactionRequest;
    private CreateApplePayRefuelTransactionBodyRequest createApplePayRefuelTransactionBodyRequest;
    private CreateApplePayRefuelTransactionDataRequest createApplePayRefuelTransactionDataRequest;

    // assigning the values
    protected void setUp() {
        createApplePayRefuelTransactionRequest = new CreateApplePayRefuelTransactionRequest();
        createApplePayRefuelTransactionBodyRequest = new CreateApplePayRefuelTransactionBodyRequest();
        createApplePayRefuelTransactionDataRequest = new CreateApplePayRefuelTransactionDataRequest();

        createApplePayRefuelTransactionDataRequest.setAmount(1);
        createApplePayRefuelTransactionDataRequest.setOutOfRange("true");
        createApplePayRefuelTransactionDataRequest.setPumpID("123456789");
        createApplePayRefuelTransactionDataRequest.setStationID("123");
        createApplePayRefuelTransactionDataRequest.setUseVoucher(true);
        createApplePayRefuelTransactionDataRequest.setApplePayPKPaymentToken("wriiyfngqqayiewgfmx=");
        createApplePayRefuelTransactionBodyRequest.setApplePayRefuelPaymentData(createApplePayRefuelTransactionDataRequest);
        
        Credential credential = new Credential();
        credential.setTicketID(getTicketID_true());
        credential.setRequestID(getRequestID_true());
        
        createApplePayRefuelTransactionRequest.setCredential(credential);
        createApplePayRefuelTransactionRequest.setBody(createApplePayRefuelTransactionBodyRequest);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_SUCCESS, createApplePayRefuelTransactionRequest.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        createApplePayRefuelTransactionRequest.getCredential().setTicketID(getTicketID_false());
        createApplePayRefuelTransactionRequest.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, createApplePayRefuelTransactionRequest.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        createApplePayRefuelTransactionRequest.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, createApplePayRefuelTransactionRequest.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        createApplePayRefuelTransactionRequest.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, createApplePayRefuelTransactionRequest.check().getStatusCode());
    }

}