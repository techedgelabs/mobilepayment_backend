package test.com.techedge.mp.frontend.adapter.webservices.common;

import java.util.Date;
import java.util.List;

import com.techedge.mp.forecourt.integration.shop.interfaces.ShopRefuelDetail;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShoppingCartDetail;
import com.techedge.mp.core.business.PostPaidTransactionServiceRemote;
import com.techedge.mp.core.business.interfaces.GetStationListResponse;
import com.techedge.mp.core.business.interfaces.GetStationResponse;
import com.techedge.mp.core.business.interfaces.postpaid.GetSourceDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCreateShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidEnableShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetPendingTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRejectShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidReverseShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.RetrieveTransactionHistoryResponse;

public class MockPostpaidService implements PostPaidTransactionServiceRemote {

    @Override
    public PostPaidEnableShopTransactionResponse enableShopTransaction(String requestID, String mpTransactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PostPaidApproveShopTransactionResponse approveShopTransaction(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId, String paymentMethodType,
            String encodedPin, Boolean useVoucher) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PostPaidRejectShopTransactionResponse rejectShopTransaction(String requestID, String ticketID, String mpTransactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PostPaidGetShopTransactionResponse getShopTransaction(String requestID, String mpTransactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PostPaidGetTransactionDetailResponse getTransactionDetail(String requestID, String ticketID, String mpTransactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PostPaidReverseShopTransactionResponse reverseShopTransaction(String requestID, String mpTransactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GetSourceDetailResponse getSourceDetail(String requestID, String ticketID, String codeType, String sourceID, String beaconCode, Double userPositionLatitude,
            Double userPositionLongitude) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GetStationResponse getStation(String requestID, String ticketID, String codeType, String beaconCode, Double userPositionLatitude, Double userPositionLongitude) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GetStationListResponse getStationList(String requestID, String ticketID, Double userPositionLatitude, Double userPositionLongitude) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveTransactionHistoryResponse retrieveTransactionHistory(String requestID, String ticketID, Date startDate, Date endDate, int itemsLimit, int pageOffset) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PostPaidGetPendingTransactionResponse retrievePoPPendingTransaction(String requestID, String ticketID, String loyaltySessionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String archiveTransactions() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String confirmTransaction(String requestID, String ticketID, String transactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PostPaidCreateShopTransactionResponse createShopTransaction(String requestID, String source, String sourceID, String sourceNumber, String stationID,
            String srcTransactionID, Double amount, String productType, List<ShoppingCartDetail> shoppingCartList, List<ShopRefuelDetail> shopRefuelDetail) {
        // TODO Auto-generated method stub
        return null;
    }

}
