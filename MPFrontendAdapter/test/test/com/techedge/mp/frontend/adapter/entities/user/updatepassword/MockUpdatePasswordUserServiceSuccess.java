package test.com.techedge.mp.frontend.adapter.entities.user.updatepassword;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockUpdatePasswordUserServiceSuccess extends MockUserService {

    @Override
    public String updatePassword(String ticketId, String requestId, String oldPassword, String newPassword) {
        // TODO Auto-generated method stub
        return StatusCode.USER_PWD_SUCCESS;
    }

}
