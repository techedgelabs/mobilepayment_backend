package test.com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard.SetDefaultLoyaltyCardBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard.SetDefaultLoyaltyCardRequest;
import com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard.SetDefaultLoyaltyCardResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestSetDefaultLoyaltyCardUserService extends BaseTestCase {
    private FrontendAdapterService           frontend;
    private SetDefaultLoyaltyCardRequest     request;
    private SetDefaultLoyaltyCardBodyRequest body;
    private SetDefaultLoyaltyCardResponse    response;
    private Response                         baseResponse;
    private String                           json;
    private Gson                             gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new SetDefaultLoyaltyCardRequest();
        body = new SetDefaultLoyaltyCardBodyRequest();
        body.setPanCode("panCode");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testSetDefaultLoyaltySuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockSetDefaultLoyaltyCardUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setSetDefaultLoyaltyCard(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (SetDefaultLoyaltyCardResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_SET_DEFAULT_LOYALTY_CARD_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testSetDefaultLoyaltyFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockSetDefaultLoyaltyCardUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setSetDefaultLoyaltyCard(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (SetDefaultLoyaltyCardResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_SET_DEFAULT_LOYALTY_CARD_FAILURE, response.getStatus().getStatusCode());
    }
}
