package test.com.techedge.mp.frontend.adapter.entities.refuel.createtransaction;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockForecourtInfoService;

import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockForecourtInfoServicePumpFailure extends MockForecourtInfoService {
    @Override
    public GetStationDetailsResponse getStationDetails(String requestID, String stationID, String pumpID, Boolean pumpDetailsReq) {
        GetStationDetailsResponse response = new GetStationDetailsResponse();
        response.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_FAILURE);
        return response;
    }
}
