package test.com.techedge.mp.frontend.adapter.entities.user.validatefield;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.validatefield.ValidateFieldRequest;
import com.techedge.mp.frontend.adapter.entities.user.validatefield.ValidateFieldRequestBody;
import com.techedge.mp.frontend.adapter.entities.user.validatefield.ValidateFieldResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class ValidateFieldUserService extends BaseTestCase {
    private FrontendAdapterService   frontend;
    private ValidateFieldRequest     request;
    private ValidateFieldRequestBody body;
    private ValidateFieldResponse    response;
    private Response                 baseResponse;
    private String                   json;
    private Gson                     gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new ValidateFieldRequest();
        body = new ValidateFieldRequestBody();
        body.setType("email_address");
        body.setVerificationCode("testtest12");
        body.setVerificationField("testtest12");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockValidateFieldUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setValidateField(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (ValidateFieldResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_VALID_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockValidateFieldUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setValidateField(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (ValidateFieldResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_VALID_FAILURE, response.getStatus().getStatusCode());
    }
}
