package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory.RetrieveRefuelPaymentHistoryBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory.RetrieveRefuelPaymentHistoryRequest;

public class RetrieveRefuelPaymentHistoryRequestTest extends BaseTestCase {

    private RetrieveRefuelPaymentHistoryRequest     request;
    private RetrieveRefuelPaymentHistoryBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveRefuelPaymentHistoryRequest();
        body = new RetrieveRefuelPaymentHistoryBodyRequest();
        CustomDate customDate = new CustomDate();
        customDate.setDay(10);
        customDate.setMonth(1);
        customDate.setYear(1990);
        body.setStartDate(customDate);
        body.setEndDate(customDate);
        body.setItemsLimit(1);
        body.setPageOffset(1);
        body.setDetails(true);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.getCredential().setTicketID(getTicketID_false());
        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}