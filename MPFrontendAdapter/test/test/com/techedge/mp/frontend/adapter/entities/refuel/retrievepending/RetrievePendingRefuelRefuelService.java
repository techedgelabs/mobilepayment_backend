package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievepending;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockBpelServiceRemoteService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockForecourtInfoService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepending.RetrievePendingRefuelRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepending.RetrievePendingRefuelResponse;
import com.techedge.mp.frontend.adapter.entities.requests.RefuelRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrievePendingRefuelRefuelService extends BaseTestCase {
    private FrontendAdapterService        frontend;
    private RetrievePendingRefuelRequest  request;
    private RetrievePendingRefuelResponse response;
    private Response                      baseResponse;
    private String                        json;
    private Gson                          gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrievePendingRefuelRequest();
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoService());
        EJBHomeCache.getInstance().setBpelService(new MockBpelServiceRemoteService());
    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockRetrievePendingRefuelTrasactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockRetrievePendingRefuelUserServiceSuccess());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setRetrievePendingRefuel(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (RetrievePendingRefuelResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_PENDING_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockRetrievePendingRefuelTransactionServiceFailure());
        EJBHomeCache.getInstance().setUserService(new MockRetrievePendingRefuelUserServiceSuccess());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setRetrievePendingRefuel(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (RetrievePendingRefuelResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_PENDING_FAILURE, response.getStatus().getStatusCode());
    }

}
