package test.com.techedge.mp.frontend.adapter.entities.user.getcreditvoucher;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.getcreditvoucher.GetCreditVoucherRequestBody;

public class GetCreditVoucherRequestBodyTest extends BaseTestCase {

    private GetCreditVoucherRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new GetCreditVoucherRequestBody();
        body.setRefresh(true);
    }

    @Test
    public void testCreditVoucherNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefresh(null);
        assertEquals(StatusCode.GET_CREDIT_VOUCHER_INVALID_PARAMETERS, body.check().getStatusCode());
    }

}