package test.com.techedge.mp.frontend.adapter.entities.user.validatefield;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.validatefield.ValidateFieldRequestBody;

public class ValidateFieldRequestBodyTest extends BaseTestCase {

    private ValidateFieldRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new ValidateFieldRequestBody();
        body.setType("email_address");
        body.setVerificationCode("testtest12");
        body.setVerificationField("testtest12");
    }

    @Test
    public void testValidateFieldTypeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setType(null);
        assertEquals(StatusCode.USER_VALID_FIELD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testValidateFieldTypeMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setType("test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_VALID_FIELD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testValidateFieldVerificationFieldNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVerificationField(null);
        assertEquals(StatusCode.USER_VALID_FIELD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testValidateFieldVerificationFieldMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVerificationField("test1test1test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_VALID_FIELD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testValidateFieldVerificationCodeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVerificationCode(null);
        assertEquals(StatusCode.USER_VALID_CODE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testValidateFieldVerificationCodeMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVerificationCode("test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_VALID_CODE_WRONG, body.check().getStatusCode());
    }
}