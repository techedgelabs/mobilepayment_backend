package test.com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction.CreateApplePayRefuelTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction.CreateApplePayRefuelTransactionDataRequest;

public class CreateApplePayTransactionRefuelRequestBodyTest extends BaseTestCase {

    private CreateApplePayRefuelTransactionBodyRequest createApplePayRefuelTransactionBodyRequest;
    private CreateApplePayRefuelTransactionDataRequest createApplePayRefuelTransactionDataRequest;

    // assigning the values
    protected void setUp() {
        createApplePayRefuelTransactionBodyRequest = new CreateApplePayRefuelTransactionBodyRequest();
        createApplePayRefuelTransactionDataRequest = new CreateApplePayRefuelTransactionDataRequest();

        createApplePayRefuelTransactionDataRequest.setAmount(1);
        createApplePayRefuelTransactionDataRequest.setOutOfRange("true");
        createApplePayRefuelTransactionDataRequest.setPumpID("123456789");
        createApplePayRefuelTransactionDataRequest.setStationID("123");
        createApplePayRefuelTransactionDataRequest.setUseVoucher(true);
        createApplePayRefuelTransactionDataRequest.setApplePayPKPaymentToken("wwryrnfxqimmrifqi=");
        
        createApplePayRefuelTransactionBodyRequest.setApplePayRefuelPaymentData(createApplePayRefuelTransactionDataRequest);
    }

    @Test
    public void testDataNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        createApplePayRefuelTransactionBodyRequest.setApplePayRefuelPaymentData(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, createApplePayRefuelTransactionBodyRequest.check().getStatusCode());
    }

    @Test
    public void testStationIDNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        createApplePayRefuelTransactionDataRequest.setStationID(null);
        assertEquals(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_DATA_WRONG, createApplePayRefuelTransactionBodyRequest.check().getStatusCode());
    }

    @Test
    public void testStationIDIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        createApplePayRefuelTransactionDataRequest.setStationID("");
        assertEquals(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_DATA_WRONG, createApplePayRefuelTransactionBodyRequest.check().getStatusCode());
    }

    @Test
    public void testStationIDMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        createApplePayRefuelTransactionDataRequest.setStationID("qwertyuiopsdfgh");
        assertEquals(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_DATA_WRONG, createApplePayRefuelTransactionBodyRequest.check().getStatusCode());
    }

    @Test
    public void testPumpIDNotValid() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        createApplePayRefuelTransactionDataRequest.setPumpID("qwertyuiopsdfgh");
        assertEquals(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_DATA_WRONG, createApplePayRefuelTransactionBodyRequest.check().getStatusCode());
    }

    @Test
    public void testPumpIDNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        createApplePayRefuelTransactionDataRequest.setPumpID(null);
        assertEquals(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_DATA_WRONG, createApplePayRefuelTransactionBodyRequest.check().getStatusCode());
    }

    @Test
    public void testPumpIDIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        createApplePayRefuelTransactionDataRequest.setPumpID("");
        assertEquals(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_DATA_WRONG, createApplePayRefuelTransactionBodyRequest.check().getStatusCode());
    }

    @Test
    public void testAmountNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        createApplePayRefuelTransactionDataRequest.setAmount(null);
        assertEquals(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_DATA_WRONG, createApplePayRefuelTransactionBodyRequest.check().getStatusCode());
    }

    @Test
    public void testOutOfRangeNotTrueOrFalse() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        createApplePayRefuelTransactionDataRequest.setOutOfRange("test");
        assertEquals(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_DATA_WRONG, createApplePayRefuelTransactionBodyRequest.check().getStatusCode());
    }
}