package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail;

import java.util.Calendar;
import java.util.HashSet;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockPostpaidService;

import com.techedge.mp.core.business.interfaces.PostPaidCart;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.postpaid.CorePumpDetail;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrieveRefuelTransactionDetailPostpaidServiceFailure extends MockPostpaidService {
    @Override
    public PostPaidGetTransactionDetailResponse getTransactionDetail(String requestID, String ticketID, String mpTransactionID) {
        PostPaidGetTransactionDetailResponse response = new PostPaidGetTransactionDetailResponse();
        response.setAmount(1.1);
        response.setAcquirerID("acquired");
        response.setAuthorizationCode("data");
        response.setBankTansactionID("idbank");
        response.setCartList(new HashSet<PostPaidCart>(0));
        response.setCashID("cashID");
        response.setCashNumber("number");
        response.setCreationTimestamp(Calendar.getInstance().getTime());
        response.setCurrency("EUR");
        response.setLastModifyTimestamp(Calendar.getInstance().getTime());
        response.setMessageCode("message");
        response.setSrcTransactionID("id");
        Station station = new Station();
        station.setId(1L);
        response.setStation(station);
        CorePumpDetail pump = new CorePumpDetail();
        pump.setPumpID("value");
        pump.setPumpNumber("123");
        pump.setPumpStatus("status");
        pump.setRefuelMode("refuel");
        response.setStatusCode(StatusCode.RETRIVE_REFUEL_TRANSACTION_DETAIL_FAILURE);
        return response;
    }
}
