package test.com.techedge.mp.frontend.adapter.webservices.common;

import java.util.Date;

import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.interfaces.CreateApplePayRefuelResponse;
import com.techedge.mp.core.business.interfaces.CreateRefuelResponse;
import com.techedge.mp.core.business.interfaces.PaymentRefuelDetailResponse;
import com.techedge.mp.core.business.interfaces.PaymentRefuelHistoryResponse;
import com.techedge.mp.core.business.interfaces.PendingRefuelResponse;
import com.techedge.mp.core.business.interfaces.PendingTransactionRefuelResponse;
import com.techedge.mp.core.business.interfaces.RetrieveTransactionEventsData;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.TransactionCancelPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.TransactionConsumeVoucherPreAuthResponse;
import com.techedge.mp.core.business.interfaces.TransactionExistsResponse;
import com.techedge.mp.core.business.interfaces.TransactionPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.TransactionUseVoucherResponse;

public class MockTransactionService implements TransactionServiceRemote {

    @Override
    public CreateRefuelResponse createRefuel(String ticketID, String requestID, String encodedPin, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Double amountVoucher, Boolean useVoucher, Long cardId, String cardType, String outOfRange, String refuelMode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Boolean checkUndoOperation(String transactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String persistUndoRefuelStatus(String ticketID, String requestID, String refuelID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String confirmRefuel(String ticketID, String requestID, String refuelID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PendingRefuelResponse retrievePendingRefuel(String requestID, String ticketID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentRefuelDetailResponse retrieveRefuelPaymentDetail(String requestID, String ticketID, String refuelID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String persistStartRefuelReceivedStatus(String requestID, String transactionID, String source) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String persistEndRefuelReceivedStatus(String requestID, String transactionID, Double amount, Double fuelQuantity, String fuelType, String productDescription,
            String productID, String timestampEndRefuel, Double unitPrice, String source) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String persistTransactionStatusRequestKoStatus(String requestID, String transactionID, String subStatus, String subStatusDescription) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String persistNewStatus(String statusCode, String messageCode, String transactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String persistPumpAvailabilityStatus(String statusCode, String messageCode, String transactionID, String requestID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String persistPumpEnableStatus(String statusCode, String messageCode, String transactionID, String requestID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String persistPumpGenericFaultStatus(String transactionID, String requestID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String setGFGNotification(String transactionID, boolean flag_value, String electronicInvoiceID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String persistPaymentAuthorizationStatus(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId,
            String authorizationCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String persistPaymentDeletionStatus(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String persistPaymentCompletionStatus(String statusCode, String subStatusCode, String errorCode, String errorMessage, String transactionID, String bankTransactionId,
            Double effectiveAmount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionExistsResponse transactionExists(String transactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String refreshStationInfo(String ticketID, String stationID, String address, String city, String country, String province, Double latitude, Double longitude) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentRefuelHistoryResponse retrieveRefuelPaymentHistory(String requestID, String ticketID, Integer pageNumber, Integer itemForPage, Date startDate, Date enddate,
            Boolean detail) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveTransactionEventsData retrieveEventList(String requestID, String transactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String retrieveStationId(String beaconCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public int updateReconciliationAttempts(String transactionID, Integer attemptsLeft) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public String retrieveStationIdByCoords(Double latitude, Double longitude) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PendingTransactionRefuelResponse finalizePendingRefuel() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Transaction getTransactionDetail(String requestId, String mpTransactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionUseVoucherResponse transactionUseVoucher(String mpTransactionID, Double amount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionPreAuthorizationConsumeVoucherResponse preAuthorizationConsumeVoucher(String transactionID, Double amount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionCancelPreAuthorizationConsumeVoucherResponse cancelPreAuthorizationConsumeVoucher(String transactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionConsumeVoucherPreAuthResponse consumeVoucherPreAuth(String transactionID, Double amount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public TransactionPreAuthorizationConsumeVoucherResponse preAuthorizationConsumeVoucherNoTransaction(Long userID, String mpTransactionID, String stationID, Double amount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CreateApplePayRefuelResponse createApplePayRefuel(String ticketID, String requestID, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Double amountVoucher, Boolean useVoucher, String applePayPKPaymentToken, String outOfRange, String refuelMode) {
        // TODO Auto-generated method stub
        return null;
    }
    /*
    @Override
    public String setGFGElectronicInvoiceID(String transactionID, String value) {
        // TODO Auto-generated method stub
        return null;
    }
    */

    @Override
    public String persistTransactionOperation(String requestID, String transactionID, String code, String message, String status, String remoteTransactionId, String operationType,
            String operationId, Long requestTimestamp, Integer amount) {
        // TODO Auto-generated method stub
        return null;
    }
}
