package test.com.techedge.mp.frontend.adapter.entities.refuel.createtransaction;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionDataRequest;

public class CreateTransactionRefuelRequestBodyTest extends BaseTestCase {

    private CreateRefuelTransactionBodyRequest body;
    private CreateRefuelTransactionDataRequest data;

    // assigning the values
    protected void setUp() {
        body = new CreateRefuelTransactionBodyRequest();
        data = new CreateRefuelTransactionDataRequest();

        data.setAmount(1);
        data.setOutOfRange("true");
        data.setPumpID("123456789");
        data.setStationID("123");
        data.setUseVoucher(true);
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setBrand("Brand");
        paymentMethod.setDefaultMethod(true);
        paymentMethod.setId(1L);
        paymentMethod.setType("1");
        paymentMethod.setStatus(1);
        data.setPaymentMethod(paymentMethod);
        body.setRefuelPaymentData(data);

    }

    @Test
    public void testDataNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setRefuelPaymentData(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testStationIDNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setStationID(null);
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testStationIDIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setStationID("");
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testStationIDMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setStationID("qwertyuiopsdfgh");
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testPumpIDNotValid() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setPumpID("qwertyuiopsdfgh");
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testPumpIDNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setPumpID(null);
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testPumpIDIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setPumpID("");
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testAmountNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setAmount(null);
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG, data.check().getStatusCode());
    }

    @Test
    public void testOutOfRangeNotTrueOrFalse() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        data.setOutOfRange("test");
        assertEquals(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG, data.check().getStatusCode());
    }
}