package test.com.techedge.mp.frontend.adapter.entities.user.removepaymentmethod;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRemovePaymentMethodUserServiceFailure extends MockUserService {

    @Override
    public String removePaymentMethod(String ticketId, String requestId, Long paymentMethodId, String paymentMethodType) {
        // TODO Auto-generated method stub
        return StatusCode.USER_REMOVE_PAYMENT_METHOD_FAILURE;
    }

}
