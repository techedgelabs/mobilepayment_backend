package test.com.techedge.mp.frontend.adapter.entities.user.loadvoucher;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockLoadVoucherUserServiceFailure extends MockUserService {

    @Override
    public String loadVoucher(String ticketId, String requestId, String voucherCode) {
        return StatusCode.USER_LOAD_VOUCHER_FAILURE;
    }

}
