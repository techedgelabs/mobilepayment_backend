package test.com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionPaymentMethodBody;
import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionTransactionCredential;

public class ApproveTransactionRequestTest extends BaseTestCase {

    private ApproveTransactionRequest               request;
    private ApproveTransactionBodyRequest           body;
    private ApproveTransactionPaymentMethodBody     method;
    private ApproveTransactionTransactionCredential credential;

    // assigning the values
    protected void setUp() {
        request = new ApproveTransactionRequest();
        method = new ApproveTransactionPaymentMethodBody();
        credential = new ApproveTransactionTransactionCredential();
        credential.setRequestID(getRequestID_true());
        credential.setTicketID(getTicketID_true());
        body = new ApproveTransactionBodyRequest();
        body.setTransactionID("qwertyuiqwertyuiqwertyuiqwertyui");
        body.setUseVoucher(true);
        method.setId(1L);
        method.setType("type");
        body.setPaymentMethod(method);
        request.setBody(body);
        request.setCredential(credential);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.POP_APPROVE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.getCredential().setTicketID(getTicketID_false());
        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}