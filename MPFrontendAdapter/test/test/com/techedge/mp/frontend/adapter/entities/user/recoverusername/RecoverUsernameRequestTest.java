package test.com.techedge.mp.frontend.adapter.entities.user.recoverusername;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.recoverusername.RecoverUsernameRequest;
import com.techedge.mp.frontend.adapter.entities.user.recoverusername.RecoverUsernameRequestBody;

public class RecoverUsernameRequestTest extends BaseTestCase {

    private RecoverUsernameRequest     request;
    private RecoverUsernameRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new RecoverUsernameRequest();
        body = new RecoverUsernameRequestBody();
        body.setFiscalcode("qwertyuiopsdfghj");
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_RECOVER_USERNAME_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}