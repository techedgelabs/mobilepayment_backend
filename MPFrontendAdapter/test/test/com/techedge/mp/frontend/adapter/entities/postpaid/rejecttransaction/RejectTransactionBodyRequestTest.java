package test.com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction.RejectPoPTransactionBodyRequest;

public class RejectTransactionBodyRequestTest extends BaseTestCase {

    private RejectPoPTransactionBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RejectPoPTransactionBodyRequest();
        body.setTransactionId("qwertyuiqwertyuiqwertyuiqwertyui");
    }

    @Test
    public void testTransactionIDNull() {
        body.setTransactionId(null);
        assertEquals(StatusCode.POP_REJECT_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testTransactionIDIsEmpty() {
        body.setTransactionId("");
        assertEquals(StatusCode.POP_REJECT_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testTransactionIDNotValid() {
        body.setTransactionId("ciao");
        assertEquals(StatusCode.POP_REJECT_ID_WRONG, body.check().getStatusCode());
    }
}