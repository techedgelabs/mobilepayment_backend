package test.com.techedge.mp.frontend.adapter.entities.manager.authentication;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockManagerService;

import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.ManagerAuthenticationResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockManagerAuthenticationManagerServiceSuccess extends MockManagerService {
    @Override
    public ManagerAuthenticationResponse authentication(String username, String password, String requestId, String deviceId, String deviceName) {
        ManagerAuthenticationResponse response = new ManagerAuthenticationResponse();
        response.setTicketId("ticket");
        response.setStatusCode(StatusCode.MANAGER_AUTH_SUCCESS);
        Manager manager = new Manager();
        manager.setEmail("a@a.it");
        manager.setFirstName("Andrea");
        manager.setId(12L);
        manager.setLastName("last");
        manager.setPassword("Pass123GHJ");
        manager.setStatus(1);
        manager.setUsername("user");
        response.setManager(manager);
        return response;
    }
}
