package test.com.techedge.mp.frontend.adapter.entities.user.updatemobilephone;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.updatemobilephone.UpdateMobilePhoneRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatemobilephone.UpdateMobilePhoneRequestBody;

public class UpdateMobilePhoneRequestTest extends BaseTestCase {

    private UpdateMobilePhoneRequest     request;
    private UpdateMobilePhoneRequestBody body;
    private MobilePhone              data;

    // assigning the values
    protected void setUp() {
        request = new UpdateMobilePhoneRequest();
        body = new UpdateMobilePhoneRequestBody();
        data = new MobilePhone();
        data.setNumber("3456678908");
        data.setPrefix("0078");
        body.setMobilePhone(data);
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.UPD_MOBILE_PHONE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.UPD_MOBILE_PHONE_FAILURE, request.check().getStatusCode());
    }

}