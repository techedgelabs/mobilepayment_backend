package test.com.techedge.mp.frontend.adapter.entities.manager.retrievestation;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockManagerService;

import com.techedge.mp.core.business.interfaces.GetStationInfo;
import com.techedge.mp.core.business.interfaces.ManagerStationDetailsResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;

public class MockRetrieveStationManagerServiceFailure extends MockManagerService {
    @Override
    public ManagerStationDetailsResponse stationDetails(String ticketId, String requestId, String stationId) {
        ManagerStationDetailsResponse response = new ManagerStationDetailsResponse();
        response.setStatusCode(ResponseHelper.MANAGER_STATION_FAILURE);
        GetStationInfo stationInfo = new GetStationInfo();
        stationInfo.setAddress("add");
        stationInfo.setCity("rome");
        stationInfo.setCode("code");
        stationInfo.setCountry("country");
        stationInfo.setDistance(1.1);
        stationInfo.setLatitude("45.5");
        stationInfo.setLongitude("45.5");
        stationInfo.setName("name");
        stationInfo.setPostpaidActive(true);
        stationInfo.setPrepaidActive(true);
        stationInfo.setProvince("RM");
        stationInfo.setShopActive(true);
        stationInfo.setStationId("001");
        response.setStationInfo(stationInfo);
        return response;
    }
}
