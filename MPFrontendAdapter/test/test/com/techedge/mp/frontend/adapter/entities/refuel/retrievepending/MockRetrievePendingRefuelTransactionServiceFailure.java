package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievepending;

import java.sql.Timestamp;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.interfaces.PendingRefuelResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrievePendingRefuelTransactionServiceFailure extends MockTransactionService {
    @Override
    public PendingRefuelResponse retrievePendingRefuel(String requestID, String ticketID) {
        PendingRefuelResponse responseDetail = new PendingRefuelResponse();
        responseDetail.setAuthCode("auth");
        responseDetail.setBankTransactionID("bank");
        responseDetail.setCurrency("currency");
        responseDetail.setDate(new Timestamp(1470650231L));
        responseDetail.setFinalAmount(12.2);
        responseDetail.setFuelAmount(13.3);
        responseDetail.setFuelQuantity(13.2);
        responseDetail.setInitialAmount(10.1);
        responseDetail.setMaskedPan("maskerd");
        responseDetail.setRefuelID("refuID");
        responseDetail.setSelectedPumpFuelType("select");
        responseDetail.setSelectedPumpID("pump");
        responseDetail.setSelectedPumpNumber(13);
        responseDetail.setSelectedStationAddress("address");
        responseDetail.setSelectedStationCity("city");
        responseDetail.setSelectedStationCountry("stationSel");
        responseDetail.setSelectedStationID("ID");
        responseDetail.setSelectedStationLatitude(45.3);
        responseDetail.setSelectedStationLongitude(35.5);
        responseDetail.setSelectedStationName("termini");
        responseDetail.setSelectedStationProvince("RM");
        responseDetail.setShopLogin("shop");
        responseDetail.setStatus("status");
        responseDetail.setStatusCode(StatusCode.REFUEL_PENDING_FAILURE);
        responseDetail.setSubStatus("substatus");
        return responseDetail;
    }
}