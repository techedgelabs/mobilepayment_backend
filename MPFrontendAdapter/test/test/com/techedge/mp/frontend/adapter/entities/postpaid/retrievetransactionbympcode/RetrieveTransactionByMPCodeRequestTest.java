package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbympcode;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbympcode.RetrievePoPTransactionByMPcodeBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbympcode.RetrievePoPTransactionByMPcodeRequest;

public class RetrieveTransactionByMPCodeRequestTest extends BaseTestCase {

    private RetrievePoPTransactionByMPcodeRequest     request;
    private RetrievePoPTransactionByMPcodeBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RetrievePoPTransactionByMPcodeRequest();
        body = new RetrievePoPTransactionByMPcodeBodyRequest();
        body.setMPCode("codecodecodecodecodecodecodecode");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.POP_GET_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}