package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory.RetrieveTransactionHistoryBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory.RetrieveTransactionHistoryRequest;

public class RetrieveTransactionHistoryRequestTest extends BaseTestCase {

    private RetrieveTransactionHistoryRequest     request;
    private RetrieveTransactionHistoryBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveTransactionHistoryRequest();
        body = new RetrieveTransactionHistoryBodyRequest();
        body.setItemsLimit(3);
        body.setPageOffset(4);
        body.setRefuelPostpaid(true);
        body.setRefuelPrepaid(true);
        body.setShop(true);
        CustomDate startDate = new CustomDate();
        startDate.setDay(1);
        startDate.setMonth(1);
        startDate.setYear(1990);
        body.setStartDate(startDate);
        CustomDate endDate = new CustomDate();
        endDate.setDay(1);
        endDate.setMonth(1);
        endDate.setYear(1990);
        body.setEndDate(endDate);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.RETRIEVE_TRANSACTION_HISTORY_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}