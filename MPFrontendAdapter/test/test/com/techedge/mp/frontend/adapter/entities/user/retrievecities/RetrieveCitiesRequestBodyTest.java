package test.com.techedge.mp.frontend.adapter.entities.user.retrievecities;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.retrievecities.RetrieveCitiesRequestBody;

public class RetrieveCitiesRequestBodyTest extends BaseTestCase {

    private RetrieveCitiesRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveCitiesRequestBody();
        body.setSearchKey("key");
    }

    @Test
    public void testKeyNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setSearchKey(null);
        assertEquals(StatusCode.RETRIEVE_CITIES_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testKeyMinor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setSearchKey("q");
        assertEquals(StatusCode.RETRIEVE_CITIES_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testKeyMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setSearchKey("q11332q11332q11332q11332q11332q11332q11332q11332q11332q11332");
        assertEquals(StatusCode.RETRIEVE_CITIES_FAILURE, body.check().getStatusCode());
    }
}