package test.com.techedge.mp.frontend.adapter.entities.manager.retrievestation;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.manager.retrievestation.ManagerRetrieveStationDetailsBodyRequest;

public class ManagerRetrieveStationBodyRequestTest extends BaseTestCase {

    private ManagerRetrieveStationDetailsBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new ManagerRetrieveStationDetailsBodyRequest();
        body.setStationID("001");
    }

    @Test
    public void testStationIDNull() {
        body.setStationID(null);
        assertEquals(ResponseHelper.MANAGER_STATION_INVALID_STATION_ID, body.check().getStatusCode());
    }

    @Test
    public void testMailIsEmpty() {
        body.setStationID("");
        assertEquals(ResponseHelper.MANAGER_STATION_INVALID_STATION_ID, body.check().getStatusCode());
    }

}