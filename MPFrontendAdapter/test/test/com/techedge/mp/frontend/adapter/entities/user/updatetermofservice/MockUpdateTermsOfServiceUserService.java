package test.com.techedge.mp.frontend.adapter.entities.user.updatetermofservice;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockParameterService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.TermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatetermsofservice.UpdateTermsOfServiceRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatetermsofservice.UpdateTermsOfServiceRequestBody;
import com.techedge.mp.frontend.adapter.entities.user.updatetermsofservice.UpdateTermsOfServiceResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class MockUpdateTermsOfServiceUserService extends BaseTestCase {
    private FrontendAdapterService          frontend;
    private UpdateTermsOfServiceRequest     request;
    private UpdateTermsOfServiceRequestBody body;
    private UpdateTermsOfServiceResponse    response;
    private Response                        baseResponse;
    private String                          json;
    private Gson                            gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new UpdateTermsOfServiceRequest();
        body = new UpdateTermsOfServiceRequestBody();
        TermsOfServiceData data = new TermsOfServiceData();
        data.setId("PRIVACY_1");
        data.setAccepted(true);
        List<TermsOfServiceData> lista = new ArrayList<TermsOfServiceData>(0);
        lista.add(data);
        body.setTermsOfService(lista);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockUpdateTermsOfServiceUserServiceSuccess());
        EJBHomeCache.getInstance().setParametersService(new MockParameterService());
        UserRequest userRequest = new UserRequest();
        userRequest.setUpdateTermsOfService(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (UpdateTermsOfServiceResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_UPDATE_TERMS_OF_SERVICE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockUpdateTermsOfServiceUserServiceFailure());
        EJBHomeCache.getInstance().setParametersService(new MockParameterService());
        UserRequest userRequest = new UserRequest();
        userRequest.setUpdateTermsOfService(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (UpdateTermsOfServiceResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_UPDATE_TERMS_OF_SERVICE_SUCCESS, response.getStatus().getStatusCode());
    }
}
