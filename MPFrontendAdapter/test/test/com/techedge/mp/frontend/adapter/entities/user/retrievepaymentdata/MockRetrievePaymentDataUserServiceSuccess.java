package test.com.techedge.mp.frontend.adapter.entities.user.retrievepaymentdata;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.PaymentInfoResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrievePaymentDataUserServiceSuccess extends MockUserService {

    @Override
    public PaymentInfoResponse retrievePaymentData(String ticketId, String requestId, Long cardId, String cardType) {
        PaymentInfoResponse response = new PaymentInfoResponse();
        response.setStatusCode(StatusCode.USER_RETRIEVE_PAYMENT_DATA_SUCCESS);
        return response;
    }

}
