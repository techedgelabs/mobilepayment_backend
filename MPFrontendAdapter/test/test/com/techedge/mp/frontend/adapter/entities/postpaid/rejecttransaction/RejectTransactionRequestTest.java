package test.com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction.RejectPoPTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction.RejectPoPTransactionRequest;

public class RejectTransactionRequestTest extends BaseTestCase {

    private RejectPoPTransactionRequest     request;
    private RejectPoPTransactionBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RejectPoPTransactionRequest();
        body = new RejectPoPTransactionBodyRequest();
        body.setTransactionId("qwertyuiqwertyuiqwertyuiqwertyui");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.POP_REJECT_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}