package test.com.techedge.mp.frontend.adapter.entities.manager.rescuepassword;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.rescuepassword.ManagerRescuePasswordBodyRequest;

public class ManagerRescuePasswordBodyRequestTest extends BaseTestCase {

    private ManagerRescuePasswordBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new ManagerRescuePasswordBodyRequest();
        body.setEmail("a@a.it");
    }

    @Test
    public void testMailNull() {
        body.setEmail(null);
        assertEquals(StatusCode.MANAGER_RESCUE_PASSWORD_ERROR, body.check().getStatusCode());
    }

    @Test
    public void testMailMajor() {
        body.setEmail("ciaociaociaociaociaociaociaociaociaociaociaociaociaociaociaociaociaociaociaociaociao");
        assertEquals(StatusCode.MANAGER_RESCUE_PASSWORD_ERROR, body.check().getStatusCode());
    }

}