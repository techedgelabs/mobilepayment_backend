package test.com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockManagerService;

import com.techedge.mp.core.business.interfaces.ManagerRetrieveVouchersResponse;
import com.techedge.mp.core.business.interfaces.postpaid.VoucherHistory;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockManagerRetrieveVouchersManagerServiceFailure extends MockManagerService {
    @Override
    public ManagerRetrieveVouchersResponse retrieveVouchers(String ticketId, String requestId, String stationID, Date startDate, Date endDate) {
        ManagerRetrieveVouchersResponse response = new ManagerRetrieveVouchersResponse();
        response.setStatusCode(StatusCode.MANAGER_RETRIEVE_VOUCHERS_FAILURE);
        List<VoucherHistory> list = new ArrayList<VoucherHistory>(0);
        response.setVoucherHistory(list);
        return response;
    }
}
