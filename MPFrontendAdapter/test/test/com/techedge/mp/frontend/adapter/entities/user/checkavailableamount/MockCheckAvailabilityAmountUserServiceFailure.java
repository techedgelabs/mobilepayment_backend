package test.com.techedge.mp.frontend.adapter.entities.user.checkavailableamount;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.CheckAvailabilityAmountData;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockCheckAvailabilityAmountUserServiceFailure extends MockUserService {

    @Override
    public CheckAvailabilityAmountData userCheckAvailabilityAmount(String ticketID, String requestID, Double amount, String loyaltySessionID) {
        CheckAvailabilityAmountData data = new CheckAvailabilityAmountData();
        data.setAllowResize(true);
        data.setMaxAmount(10.0);
        data.setStatusCode(StatusCode.CHECK_AVAILABILITY_AMOUNT_FAILURE);
        data.setThresholdAmount(1.1);
        return data;
    }
}
