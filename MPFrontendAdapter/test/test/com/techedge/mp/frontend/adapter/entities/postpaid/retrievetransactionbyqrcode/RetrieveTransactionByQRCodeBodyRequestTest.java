package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbyqrcode;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbyqrcode.RetrievePoPTransactionByQRcodeBodyRequest;

public class RetrieveTransactionByQRCodeBodyRequestTest extends BaseTestCase {

    private RetrievePoPTransactionByQRcodeBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrievePoPTransactionByQRcodeBodyRequest();
        body.setQRCode("POP_GET_SUCCESS");
    }

    @Test
    public void testTransactionIdNull() {
        body.setQRCode(null);
        assertEquals(StatusCode.POP_GET_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testTransactionIsEmpty() {
        body.setQRCode("");
        assertEquals(StatusCode.POP_GET_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testTransactionNotValid() {
        body.setQRCode("aa");
        assertEquals(StatusCode.POP_GET_ID_WRONG, body.check().getStatusCode());
    }

}