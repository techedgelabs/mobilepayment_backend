package test.com.techedge.mp.frontend.adapter.entities.user.retrievetermofservice;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.retrievetermsofservice.RetrieveTermsOfServiceRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrievetermsofservice.RetrieveTermsOfServiceRequestBody;

public class RetrievePaymentDataRequestTest extends BaseTestCase {

    private RetrieveTermsOfServiceRequest     request;
    private RetrieveTermsOfServiceRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveTermsOfServiceRequest();
        body = new RetrieveTermsOfServiceRequestBody();
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.RETRIEVE_TERMS_SERVICE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}