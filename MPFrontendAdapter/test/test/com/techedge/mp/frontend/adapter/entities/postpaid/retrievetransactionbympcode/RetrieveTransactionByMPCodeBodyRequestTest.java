package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbympcode;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbympcode.RetrievePoPTransactionByMPcodeBodyRequest;

public class RetrieveTransactionByMPCodeBodyRequestTest extends BaseTestCase {

    private RetrievePoPTransactionByMPcodeBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrievePoPTransactionByMPcodeBodyRequest();
        body.setMPCode("POP_GET_SUCCESS");
    }

    @Test
    public void testTransactionIdNull() {
        body.setMPCode(null);
        assertEquals(StatusCode.POP_GET_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testTransactionIsEmpty() {
        body.setMPCode("");
        assertEquals(StatusCode.POP_GET_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testTransactionNotValid() {
        body.setMPCode("aa");
        assertEquals(StatusCode.POP_GET_ID_WRONG, body.check().getStatusCode());
    }

}