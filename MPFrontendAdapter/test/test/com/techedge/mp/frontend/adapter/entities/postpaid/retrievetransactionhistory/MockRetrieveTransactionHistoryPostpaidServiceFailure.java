package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory;

import java.util.Date;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockPostpaidService;

import com.techedge.mp.core.business.interfaces.postpaid.RetrieveTransactionHistoryResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrieveTransactionHistoryPostpaidServiceFailure extends MockPostpaidService {
    @Override
    public RetrieveTransactionHistoryResponse retrieveTransactionHistory(String requestID, String ticketID, Date startDate, Date endDate, int itemsLimit, int pageOffset) {
        RetrieveTransactionHistoryResponse response = new RetrieveTransactionHistoryResponse();
        response.setPageOffset(1);
        response.setPagesTotal(2);
        response.setStatusCode(StatusCode.RETRIEVE_TRANSACTION_HISTORY_FAILURE);
        return response;
    }
}