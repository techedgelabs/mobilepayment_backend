package test.com.techedge.mp.frontend.adapter.entities.user.rescuepassword;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.rescuepassword.RescuePasswordRequestBody;

public class RescuePasswordRequestBodyTest extends BaseTestCase {

    private RescuePasswordRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new RescuePasswordRequestBody();
        body.setEmail("a@a.it");
    }

    @Test
    public void testEmailNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setEmail(null);
        assertEquals(StatusCode.USER_RESCUE_PASSWORD_ERROR, body.check().getStatusCode());
    }

    @Test
    public void testEmailMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setEmail("setVoucherCodesetVoucherCodesetVoucherCodesetVoucherCode");
        assertEquals(StatusCode.USER_RESCUE_PASSWORD_ERROR, body.check().getStatusCode());
    }
}