package test.com.techedge.mp.frontend.adapter.entities.user.update;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.SecurityData;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserPersonalDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserPersonalDataRequestBody;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserUserData;

public class UpdateRequestTest extends BaseTestCase {

    private UpdateUserPersonalDataRequest     request;
    private UpdateUserPersonalDataRequestBody body;
    private UpdateUserUserData                data;

    // assigning the values
    protected void setUp() {
        request = new UpdateUserPersonalDataRequest();
        body = new UpdateUserPersonalDataRequestBody();
        data = new UpdateUserDataRequest();
        SecurityData securityData = new SecurityData();
        securityData.setEmail("a@a.it");
        securityData.setPassword("Password12345");
        data.setFirstName("name");
        data.setLastName("surname");
        CustomDate customDate = new CustomDate();
        customDate.setDay(10);
        customDate.setMonth(1);
        customDate.setYear(1990);
        data.setDateOfBirth(customDate);
        data.setBirthMunicipality("muni");
        data.setBirthProvince("province");
        data.setLanguage("IT");
        data.setSex("M");

        body.setUserPersonalData(data);
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_UPD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}