package test.com.techedge.mp.frontend.adapter.entities.user.checkavailableamount;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockParameterService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.checkavailabilityamount.CheckAvailabilityAmountBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.checkavailabilityamount.CheckAvailabilityAmountRequest;
import com.techedge.mp.frontend.adapter.entities.user.checkavailabilityamount.CheckAvailabilityAmountResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class MockCheckAvailabilityAmountUserService extends BaseTestCase {
    private FrontendAdapterService             frontend;
    private CheckAvailabilityAmountRequest     request;
    private CheckAvailabilityAmountBodyRequest body;
    private CheckAvailabilityAmountResponse    response;
    private Response                           baseResponse;
    private String                             json;
    private Gson                               gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new CheckAvailabilityAmountRequest();
        body = new CheckAvailabilityAmountBodyRequest();
        body.setAmount(1);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockCheckAvailabilityAmountUserServiceSuccess());
        EJBHomeCache.getInstance().setParametersService(new MockParameterService());
        UserRequest userRequest = new UserRequest();
        userRequest.setCheckAvailabilityAmount(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (CheckAvailabilityAmountResponse) baseResponse.getEntity();
        assertEquals(StatusCode.CHECK_AVAILABILITY_AMOUNT_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockCheckAvailabilityAmountUserServiceFailure());
        EJBHomeCache.getInstance().setParametersService(new MockParameterService());
        UserRequest userRequest = new UserRequest();
        userRequest.setCheckAvailabilityAmount(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (CheckAvailabilityAmountResponse) baseResponse.getEntity();
        assertEquals(StatusCode.CHECK_AVAILABILITY_AMOUNT_FAILURE, response.getStatus().getStatusCode());
    }
}
