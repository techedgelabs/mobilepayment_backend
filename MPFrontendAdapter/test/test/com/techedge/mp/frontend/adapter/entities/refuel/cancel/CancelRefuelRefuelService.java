package test.com.techedge.mp.frontend.adapter.entities.refuel.cancel;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockBpelServiceRemoteService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockForecourtInfoService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.cancel.CancelRefuelBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.cancel.CancelRefuelRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.cancel.CancelRefuelResponse;
import com.techedge.mp.frontend.adapter.entities.requests.RefuelRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class CancelRefuelRefuelService extends BaseTestCase {
    private FrontendAdapterService  frontend;
    private CancelRefuelRequest     request;
    private CancelRefuelBodyRequest body;
    private CancelRefuelResponse    response;
    private Response                baseResponse;
    private String                  json;
    private Gson                    gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new CancelRefuelRequest();
        body = new CancelRefuelBodyRequest();
        body.setRefuelID("refuelrefuelrefuelrefuelrefuel12");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoService());
        EJBHomeCache.getInstance().setBpelService(new MockBpelServiceRemoteService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockCancelRefuelTrasactionServiceSuccess());
        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setCancelRefuel(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (CancelRefuelResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_CANCEL_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockCancelRefuelTrasactionServiceFailure());
        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setCancelRefuel(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (CancelRefuelResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_CANCEL_FAILURE, response.getStatus().getStatusCode());
    }
}
