package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveListResponse;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveStationListBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveStationListRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveStationUserPositionRequest;
import com.techedge.mp.frontend.adapter.entities.requests.PoPRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrieveStationListPostpaidService extends BaseTestCase {
    private FrontendAdapterService         frontend;
    private RetrieveStationListRequest     request;
    private RetrieveStationListBodyRequest body;
    private RetrieveListResponse           response;
    private Response                       baseResponse;
    private String                         json;
    private Gson                           gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrieveStationListRequest();
        body = new RetrieveStationListBodyRequest();
        RetrieveStationUserPositionRequest position = new RetrieveStationUserPositionRequest();
        position.setLatitude(45.3);
        position.setLongitude(34.5);
        body.setUserPosition(position);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrieveStationListPostpaidServiceSuccess());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrieveStationList(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrieveListResponse) baseResponse.getEntity();
        assertEquals(StatusCode.STATION_RETRIEVE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrieveStationListPostpaidServiceFailure());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrieveStationList(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrieveListResponse) baseResponse.getEntity();
        assertEquals(StatusCode.STATION_RETRIEVE_FAILURE, response.getStatus().getStatusCode());
    }
}
