package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievesourcedetail;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievesourcedetail.RetrieveSourceDetailBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievesourcedetail.RetrieveSourceDetailRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationUserPositionRequest;

public class RetrieveSourceDetailRequestTest extends BaseTestCase {

    private RetrieveSourceDetailRequest     request;
    private RetrieveSourceDetailBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveSourceDetailRequest();
        body = new RetrieveSourceDetailBodyRequest();
        body.setBeaconCode("beacon");
        body.setCodeType("QR-CODE");
        body.setSourceID("source");
        RetrieveStationUserPositionRequest position = new RetrieveStationUserPositionRequest();
        position.setLatitude(45.3);
        position.setLongitude(34.5);
        body.setUserPosition(position);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.STATION_RETRIEVE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}