package test.com.techedge.mp.frontend.adapter.entities.user.update;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.SecurityData;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserPersonalDataRequestBody;

public class UpdateRequestBodyTest extends BaseTestCase {

    private UpdateUserPersonalDataRequestBody body;
    private UpdateUserDataRequest             data;

    // assigning the values
    protected void setUp() {
        body = new UpdateUserPersonalDataRequestBody();
        data = new UpdateUserDataRequest();
        SecurityData securityData = new SecurityData();
        securityData.setEmail("a@a.it");
        securityData.setPassword("Password12345");
        data.setFirstName("name");
        data.setLastName("surname");
        CustomDate customDate = new CustomDate();
        customDate.setDay(10);
        customDate.setMonth(1);
        customDate.setYear(1990);
        data.setDateOfBirth(customDate);
        data.setBirthMunicipality("muni");
        data.setBirthProvince("province");
        data.setLanguage("IT");
        data.setSex("M");

        body.setUserPersonalData(data);
    }

    @Test
    public void testUserDataNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setUserPersonalData(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }
}