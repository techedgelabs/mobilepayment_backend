package test.com.techedge.mp.frontend.adapter.entities.user.recoverusername;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.recoverusername.RecoverUsernameRequestBody;

public class RecoverUsernameRequestBodyTest extends BaseTestCase {

    private RecoverUsernameRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new RecoverUsernameRequestBody();
        body.setFiscalcode("qwertyuiopsdfghj");
    }

    @Test
    public void testFiscalCodeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setFiscalcode(null);
        assertEquals(StatusCode.USER_RECOVER_USERNAME_INVALID_TAX_CODE, body.check().getStatusCode());
    }

    @Test
    public void testFiscalCodeIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setFiscalcode("");
        assertEquals(StatusCode.USER_RECOVER_USERNAME_INVALID_TAX_CODE, body.check().getStatusCode());
    }

    @Test
    public void testFiscalCodeMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setFiscalcode("test1test1test1test1");
        assertEquals(StatusCode.USER_RECOVER_USERNAME_INVALID_TAX_CODE, body.check().getStatusCode());
    }
}