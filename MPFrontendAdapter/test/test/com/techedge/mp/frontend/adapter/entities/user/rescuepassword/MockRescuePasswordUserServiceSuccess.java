package test.com.techedge.mp.frontend.adapter.entities.user.rescuepassword;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRescuePasswordUserServiceSuccess extends MockUserService {

    @Override
    public String rescuePassword(String ticketId, String requestId, String i_mail) {
        // TODO Auto-generated method stub
        return StatusCode.USER_RESCUE_PASSWORD_SUCCESS;
    }

}
