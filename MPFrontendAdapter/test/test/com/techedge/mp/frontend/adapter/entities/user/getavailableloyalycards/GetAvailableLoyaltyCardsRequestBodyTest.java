package test.com.techedge.mp.frontend.adapter.entities.user.getavailableloyalycards;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.getavailableloyaltycards.GetAvailableLoyaltyCardsBodyRequest;

public class GetAvailableLoyaltyCardsRequestBodyTest extends BaseTestCase {

    private GetAvailableLoyaltyCardsBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new GetAvailableLoyaltyCardsBodyRequest();
        body.setFiscalcode("qwertyuiopasdfgh");
    }

    @Test
    public void testFiscalCodenotValid() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setFiscalcode("abc");
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }
}