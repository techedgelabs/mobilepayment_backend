package test.com.techedge.mp.frontend.adapter.entities.survey.submitsurvey;

import java.util.ArrayList;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockSurveyService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.SurveyRequest;
import com.techedge.mp.frontend.adapter.entities.survey.submitsurvey.SubmitSurveyBodyRequest;
import com.techedge.mp.frontend.adapter.entities.survey.submitsurvey.SubmitSurveyBodyRequest.Answer;
import com.techedge.mp.frontend.adapter.entities.survey.submitsurvey.SubmitSurveyRequest;
import com.techedge.mp.frontend.adapter.entities.survey.submitsurvey.SubmitSurveyResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class SubmitSurveySurveyService extends BaseTestCase {
    private FrontendAdapterService  frontend;
    private SubmitSurveyRequest     request;
    private SubmitSurveyBodyRequest body;
    private SubmitSurveyResponse    response;
    private Response                baseResponse;
    private String                  json;
    private Gson                    gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new SubmitSurveyRequest();
        body = new SubmitSurveyBodyRequest();
        body.setCode("123");
        body.setKey("123");
        ArrayList<Answer> list = new ArrayList<Answer>(0);
        body.setAnswers(list);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());
        EJBHomeCache.getInstance().setSurveyService(new MockSurveyService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setSurveyService(new MockSubmitSurveySurveyServiceSuccess());
        SurveyRequest surveyRequest = new SurveyRequest();
        surveyRequest.setSubmitSurveyRequest(request);
        json = gson.toJson(surveyRequest, SurveyRequest.class);
        baseResponse = frontend.surveyJsonHandler(json);
        response = (SubmitSurveyResponse) baseResponse.getEntity();
        assertEquals(StatusCode.SURVEY_SUBMIT_SURVEY_SUCCESS, response.getStatus().getStatusCode());
    }
}
