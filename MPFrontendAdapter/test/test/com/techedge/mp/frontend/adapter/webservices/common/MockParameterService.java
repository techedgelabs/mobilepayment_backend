package test.com.techedge.mp.frontend.adapter.webservices.common;

import java.util.List;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ParamInfo;

public class MockParameterService implements ParametersServiceRemote {

    @Override
    public String getParamValue(String paramName) throws ParameterNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public String getParamValueNoCache(String paramName) throws ParameterNotFoundException {
        // TODO Auto-generated method stub
        return null;
    }    
    
    @Override
    public List<ParamInfo> refreshParameters() {
        // TODO Auto-generated method stub
        return null;
    }

}
