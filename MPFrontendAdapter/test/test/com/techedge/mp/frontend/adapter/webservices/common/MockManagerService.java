package test.com.techedge.mp.frontend.adapter.webservices.common;

import java.util.Date;

import com.techedge.mp.core.business.ManagerServiceRemote;
import com.techedge.mp.core.business.interfaces.ManagerAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.ManagerRetrieveTransactionsResponse;
import com.techedge.mp.core.business.interfaces.ManagerRetrieveVouchersResponse;
import com.techedge.mp.core.business.interfaces.ManagerStationDetailsResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;

public class MockManagerService implements ManagerServiceRemote {

    @Override
    public ManagerAuthenticationResponse authentication(String username, String password, String requestId, String deviceId, String deviceName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String logout(String ticketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String updatePassword(String ticketId, String requestId, String oldPassword, String newPassword) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String rescuePassword(String ticketId, String requestId, String email) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ManagerStationDetailsResponse stationDetails(String ticketId, String requestId, String stationId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ManagerRetrieveTransactionsResponse retrieveTransactions(String ticketId, String requestId, String stationID, Integer pumpMaxTransactions) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ManagerRetrieveVouchersResponse retrieveVouchers(String ticketId, String requestId, String stationID, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PostPaidGetTransactionDetailResponse getTransactionDetail(String requestID, String ticketID, String mpTransactionID) {
        // TODO Auto-generated method stub
        return null;
    }

}
