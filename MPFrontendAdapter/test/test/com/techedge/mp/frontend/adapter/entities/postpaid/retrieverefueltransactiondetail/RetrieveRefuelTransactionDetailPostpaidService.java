package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockParameterService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail.RetrieveRefuelTransactionDetailBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail.RetrieveRefuelTransactionDetailRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail.RetrieveRefuelTransactionDetailResponse;
import com.techedge.mp.frontend.adapter.entities.requests.PoPRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrieveRefuelTransactionDetailPostpaidService extends BaseTestCase {
    private FrontendAdapterService                     frontend;
    private RetrieveRefuelTransactionDetailRequest     request;
    private RetrieveRefuelTransactionDetailBodyRequest body;
    private RetrieveRefuelTransactionDetailResponse    response;
    private Response                                   baseResponse;
    private String                                     json;
    private Gson                                       gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrieveRefuelTransactionDetailRequest();
        body = new RetrieveRefuelTransactionDetailBodyRequest();
        request.setCredential(createCredentialTrue());
        body.setTransactionId("qwertyuiqwertyuiqwertyuiqwertyui");
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());
        EJBHomeCache.getInstance().setParametersService(new MockParameterService());

    }

    @Test
    public void testRefuelTransactionDetailSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrieveRefuelTransactionDetailPostpaidServiceSuccess());
        EJBHomeCache.getInstance().setParametersService(new MockParameterService());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrieveRefuelTransactionDetail(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrieveRefuelTransactionDetailResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIVE_REFUEL_TRANSACTION_DETAIL_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRefuelTransactionDetailFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrieveRefuelTransactionDetailPostpaidServiceFailure());
        EJBHomeCache.getInstance().setParametersService(new MockParameterService());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrieveRefuelTransactionDetail(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrieveRefuelTransactionDetailResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIVE_REFUEL_TRANSACTION_DETAIL_FAILURE, response.getStatus().getStatusCode());
    }
}
