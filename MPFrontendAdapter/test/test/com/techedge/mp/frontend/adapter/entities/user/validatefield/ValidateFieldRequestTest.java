package test.com.techedge.mp.frontend.adapter.entities.user.validatefield;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.validatefield.ValidateFieldRequest;
import com.techedge.mp.frontend.adapter.entities.user.validatefield.ValidateFieldRequestBody;

public class ValidateFieldRequestTest extends BaseTestCase {

    private ValidateFieldRequest     request;
    private ValidateFieldRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new ValidateFieldRequest();
        body = new ValidateFieldRequestBody();
        body.setType("email_address");
        body.setVerificationCode("testtest12");
        body.setVerificationField("testtest12");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_VALID_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}