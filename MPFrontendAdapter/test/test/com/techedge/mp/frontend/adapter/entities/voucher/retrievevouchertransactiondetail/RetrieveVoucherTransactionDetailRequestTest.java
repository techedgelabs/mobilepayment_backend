package test.com.techedge.mp.frontend.adapter.entities.voucher.retrievevouchertransactiondetail;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.voucher.retrievevouchertransactiondetail.RetrieveVoucherTransactionDetailRequest;
import com.techedge.mp.frontend.adapter.entities.voucher.retrievevouchertransactiondetail.RetrieveVoucherTransactionDetailBodyRequest;

public class RetrieveVoucherTransactionDetailRequestTest extends BaseTestCase {

    private RetrieveVoucherTransactionDetailRequest     request;
    private RetrieveVoucherTransactionDetailBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveVoucherTransactionDetailRequest();
        body = new RetrieveVoucherTransactionDetailBodyRequest();
        body.setVoucherTransactionId("transaction");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.RETRIEVE_VOUCHER_TRANSACTION_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}