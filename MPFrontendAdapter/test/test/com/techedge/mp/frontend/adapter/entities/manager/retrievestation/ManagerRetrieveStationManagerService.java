package test.com.techedge.mp.frontend.adapter.entities.manager.retrievestation;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.manager.retrievestation.ManagerRetrieveStationDetailsBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievestation.ManagerRetrieveStationDetailsRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievestation.ManagerRetrieveStationDetailsResponse;
import com.techedge.mp.frontend.adapter.entities.requests.ManagerRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class ManagerRetrieveStationManagerService extends BaseTestCase {
    private FrontendAdapterService                   frontend;
    private ManagerRetrieveStationDetailsRequest     request;
    private ManagerRetrieveStationDetailsBodyRequest body;
    private ManagerRetrieveStationDetailsResponse    response;
    private Response                                 baseResponse;
    private String                                   json;
    private Gson                                     gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new ManagerRetrieveStationDetailsRequest();
        body = new ManagerRetrieveStationDetailsBodyRequest();
        body.setStationID("001");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockRetrieveStationManagerServiceSuccess());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setRetrieveStationDetails(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerRetrieveStationDetailsResponse) baseResponse.getEntity();
        assertEquals(ResponseHelper.MANAGER_STATION_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockRetrieveStationManagerServiceFailure());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setRetrieveStationDetails(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerRetrieveStationDetailsResponse) baseResponse.getEntity();
        assertEquals(ResponseHelper.MANAGER_STATION_FAILURE, response.getStatus().getStatusCode());
    }
}
