package test.com.techedge.mp.frontend.adapter.entities.manager.rescuepassword;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockManagerService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockManagerRescuePasswordManagerServiceFailure extends MockManagerService {
    @Override
    public String rescuePassword(String ticketId, String requestId, String email) {
        // TODO Auto-generated method stub
        return StatusCode.MANAGER_RESCUE_PASSWORD_FAILURE;
    }

}
