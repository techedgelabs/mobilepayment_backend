package test.com.techedge.mp.frontend.adapter.entities.survey.getsurvey;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockSurveyService;

import com.techedge.mp.core.business.interfaces.Survey;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockGetSurveySurveyServiceSuccess extends MockSurveyService {

    @Override
    public Survey surveyGet(String ticketId, String requestId, String code) {
        Survey survey = new Survey();
        survey.setCode("qwe");
        survey.setDescription("desc");
        survey.setNote("1");
        survey.setStatusCode(StatusCode.SURVEY_GET_SURVEY_SUCCESS);
        return survey;
    }

}
