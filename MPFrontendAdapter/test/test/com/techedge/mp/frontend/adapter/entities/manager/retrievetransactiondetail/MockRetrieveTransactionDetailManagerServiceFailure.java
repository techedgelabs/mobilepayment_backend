package test.com.techedge.mp.frontend.adapter.entities.manager.retrievetransactiondetail;

import java.sql.Date;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockManagerService;

import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrieveTransactionDetailManagerServiceFailure extends MockManagerService {
    @Override
    public PostPaidGetTransactionDetailResponse getTransactionDetail(String requestID, String ticketID, String mpTransactionID) {

        PostPaidGetTransactionDetailResponse response = new PostPaidGetTransactionDetailResponse();
        response.setAcquirerID("acquired");
        response.setAmount(1.1);
        response.setAuthorizationCode("code");
        response.setBankTansactionID("bank");
        response.setCashID("id");
        response.setCashNumber("123");
        Date date = new Date(1470817466L);
        response.setCreationTimestamp(date);
        response.setCurrency("EUR");
        response.setId(1L);
        response.setLastModifyTimestamp(date);
        response.setMessageCode("message");
        response.setMpTransactionID("qw");
        response.setMpTransactionStatus("status");
        response.setNotificationCreated(true);
        response.setNotificationPaid(true);
        response.setNotificationUser(true);
        response.setPanCode("panCode");
        response.setPaymentMethodId(1L);
        response.setPaymentMethodType("type");
        response.setPaymentMode("mode");
        response.setPaymentType("type");
        response.setProductType("type");
        response.setPumpFuelType("type");
        response.setPumpID("01");
        response.setPumpNumber("123");
        response.setPumpStatus("status");
        response.setShopLogin("login");
        response.setSource("source");
        response.setSourceID("001");
        response.setSourceNumber("234");
        response.setSourceStatus("status");
        response.setSourceType("tuo");
        response.setSrcTransactionID("001");
        response.setSrcTransactionStatus("status");
        response.setStation(new Station());
        response.setStatus("status");
        response.setStatusCode(StatusCode.POP_DETAIL_FAILURE);
        response.setStatusDescription("desc");
        response.setStatusTitle("title");
        response.setStatusType("tupe");
        response.setSubStatus("sub");
        response.setSurveyUrl("url");
        response.setToken("token");
        response.setToReconcile(true);
        response.setTransactionType("type");
        response.setUser(new User());
        response.setUseVoucher(true);
        return response;
    }
}
