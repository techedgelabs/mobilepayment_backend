package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail.RetrieveRefuelTransactionDetailBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail.RetrieveRefuelTransactionDetailRequest;

public class RetrieveRefuelTransactionDetailRequestTest extends BaseTestCase {

    private RetrieveRefuelTransactionDetailRequest     request;
    private RetrieveRefuelTransactionDetailBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveRefuelTransactionDetailRequest();
        body = new RetrieveRefuelTransactionDetailBodyRequest();
        request.setCredential(createCredentialTrue());
        body.setTransactionId("qwertyuiqwertyuiqwertyuiqwertyui");
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.RETRIVE_REFUEL_TRANSACTION_DETAIL_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}