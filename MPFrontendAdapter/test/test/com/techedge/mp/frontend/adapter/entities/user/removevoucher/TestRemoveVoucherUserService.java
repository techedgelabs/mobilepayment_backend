package test.com.techedge.mp.frontend.adapter.entities.user.removevoucher;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.entities.refuel.retrievestations.MockParameterServiceSuccess;
import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.removevoucher.RemoveVoucherBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.removevoucher.RemoveVoucherRequest;
import com.techedge.mp.frontend.adapter.entities.user.removevoucher.RemoveVoucherResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestRemoveVoucherUserService extends BaseTestCase {
    private FrontendAdapterService   frontend;
    private RemoveVoucherRequest     request;
    private RemoveVoucherBodyRequest body;
    private RemoveVoucherResponse    response;
    private Response                 baseResponse;
    private String                   json;
    private Gson                     gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RemoveVoucherRequest();
        body = new RemoveVoucherBodyRequest();
        body.setVoucherCode("123");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRemoveVoucherUserServiceSuccess());
        EJBHomeCache.getInstance().setParametersService(new MockParameterServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setRemoveVoucher(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RemoveVoucherResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_REMOVE_VOUCHER_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRemoveVoucherUserServiceFailure());
        EJBHomeCache.getInstance().setParametersService(new MockParameterServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setRemoveVoucher(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RemoveVoucherResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_REMOVE_VOUCHER_FAILURE, response.getStatus().getStatusCode());
    }
}
