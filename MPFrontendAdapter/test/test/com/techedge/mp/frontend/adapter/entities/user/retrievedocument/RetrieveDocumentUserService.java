package test.com.techedge.mp.frontend.adapter.entities.user.retrievedocument;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrievedocument.RetrieveDocumentBody;
import com.techedge.mp.frontend.adapter.entities.user.retrievedocument.RetrieveDocumentRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrievedocument.RetrieveDocumentResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrieveDocumentUserService extends BaseTestCase {
    private FrontendAdapterService   frontend;
    private RetrieveDocumentRequest  request;
    private RetrieveDocumentBody     body;
    private RetrieveDocumentResponse response;
    private Response                 baseResponse;
    private String                   json;
    private Gson                     gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrieveDocumentRequest();
        body = new RetrieveDocumentBody();
        body.setDocumentId("key");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRetrieveDocumentSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRetrieveDocumentUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setRetrieveDocument(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RetrieveDocumentResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIEVE_DOCUMENT_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveDocumentFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRetrieveDocumentUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setRetrieveDocument(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RetrieveDocumentResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIEVE_DOCUMENT_FAILURE, response.getStatus().getStatusCode());
    }
}
