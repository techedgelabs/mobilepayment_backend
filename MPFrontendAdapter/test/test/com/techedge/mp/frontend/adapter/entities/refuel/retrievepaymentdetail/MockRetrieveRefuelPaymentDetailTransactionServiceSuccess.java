package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail;

import java.sql.Timestamp;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.interfaces.PaymentRefuelDetailResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrieveRefuelPaymentDetailTransactionServiceSuccess extends MockTransactionService {
    @Override
    public PaymentRefuelDetailResponse retrieveRefuelPaymentDetail(String requestID, String ticketID, String refuelID) {
        PaymentRefuelDetailResponse response = new PaymentRefuelDetailResponse();
        response.setAuthCode("auth");
        response.setBankTransactionID("bank");
        response.setCurrency("currency");
        response.setDate(new Timestamp(1470650231L));
        response.setFinalAmount(12.2);
        response.setFuelAmount(13.3);
        response.setFuelQuantity(13.2);
        response.setInitialAmount(10.1);
        response.setMaskedPan("maskerd");
        response.setRefuelID("refuID");
        response.setSelectedPumpFuelType("select");
        response.setSelectedPumpID("pump");
        response.setSelectedPumpNumber(13);
        response.setSelectedStationAddress("address");
        response.setSelectedStationCity("city");
        response.setSelectedStationCountry("stationSel");
        response.setSelectedStationID("ID");
        response.setSelectedStationLatitude(45.3);
        response.setSelectedStationLongitude(35.5);
        response.setSelectedStationName("termini");
        response.setSelectedStationProvince("RM");
        response.setShopLogin("shop");
        response.setSourceStatus("source");
        response.setSourceType("source");
        response.setStatus("status");
        response.setStatusCode(StatusCode.RETRIEVE_PAYMENT_DETAIL_SUCCESS);
        response.setSubStatus("substatus");
        response.setUseVoucher(true);
        return response;
    }
}
