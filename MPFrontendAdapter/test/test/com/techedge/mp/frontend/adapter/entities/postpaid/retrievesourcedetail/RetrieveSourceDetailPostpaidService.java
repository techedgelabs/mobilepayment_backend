package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievesourcedetail;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievesourcedetail.RetrieveSourceDetailBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievesourcedetail.RetrieveSourceDetailRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievesourcedetail.RetrieveSourceDetailResponse;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationUserPositionRequest;
import com.techedge.mp.frontend.adapter.entities.requests.PoPRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrieveSourceDetailPostpaidService extends BaseTestCase {
    private FrontendAdapterService          frontend;
    private RetrieveSourceDetailRequest     request;
    private RetrieveSourceDetailBodyRequest body;
    private RetrieveSourceDetailResponse    response;
    private Response                        baseResponse;
    private String                          json;
    private Gson                            gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrieveSourceDetailRequest();
        body = new RetrieveSourceDetailBodyRequest();
        body.setBeaconCode("beacon");
        body.setCodeType("QR-CODE");
        body.setSourceID("source");
        RetrieveStationUserPositionRequest position = new RetrieveStationUserPositionRequest();
        position.setLatitude(45.3);
        position.setLongitude(34.5);
        body.setUserPosition(position);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrieveSourceDetailPostpaidServiceSuccess());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrieveSourceDetail(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrieveSourceDetailResponse) baseResponse.getEntity();
        assertEquals(StatusCode.STATION_RETRIEVE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrieveSourceDetailPostpaidServiceFailure());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrieveSourceDetail(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrieveSourceDetailResponse) baseResponse.getEntity();
        assertEquals(StatusCode.STATION_RETRIEVE_FAILURE, response.getStatus().getStatusCode());
    }
}
