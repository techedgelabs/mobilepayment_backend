package test.com.techedge.mp.frontend.adapter.entities.user.getavailableloyalycards;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.getavailableloyaltycards.GetAvailableLoyaltyCardsBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.getavailableloyaltycards.GetAvailableLoyaltyCardsRequest;
import com.techedge.mp.frontend.adapter.entities.user.getavailableloyaltycards.GetAvailableLoyaltyCardsResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestGetAvailableLoyaltyCardsUserService extends BaseTestCase {
    private FrontendAdapterService              frontend;
    private GetAvailableLoyaltyCardsRequest     request;
    private GetAvailableLoyaltyCardsBodyRequest body;
    private GetAvailableLoyaltyCardsResponse    response;
    private Response                            baseResponse;
    private String                              json;
    private Gson                                gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new GetAvailableLoyaltyCardsRequest();
        body = new GetAvailableLoyaltyCardsBodyRequest();
        body.setFiscalcode("qwertyuioplkjhgf");;
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testGetActiveVoucherSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockGetAvailableLoyaltyCardsUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setGetAvailableLoyaltyCards(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (GetAvailableLoyaltyCardsResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_GET_AVAILABLE_LOYALTY_CARDS_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testGetActiveVoucherFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockGetAvailableLoyaltyCardsUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setGetAvailableLoyaltyCards(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (GetAvailableLoyaltyCardsResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_GET_AVAILABLE_LOYALTY_CARDS_GENERIC_ERROR, response.getStatus().getStatusCode());
    }
}
