package test.com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers.ManagerRetrieveVouchersBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers.ManagerRetrieveVouchersRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers.ManagerRetrieveVouchersResponse;
import com.techedge.mp.frontend.adapter.entities.requests.ManagerRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class ManagerRetrieveVouchersManagerService extends BaseTestCase {
    private FrontendAdapterService             frontend;
    private ManagerRetrieveVouchersRequest     request;
    private ManagerRetrieveVouchersBodyRequest body;
    private ManagerRetrieveVouchersResponse    response;
    private Response                           baseResponse;
    private String                             json;
    private Gson                               gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new ManagerRetrieveVouchersRequest();
        body = new ManagerRetrieveVouchersBodyRequest();
        CustomDate endDate = new CustomDate();
        endDate.setDay(10);
        endDate.setMonth(3);
        endDate.setYear(1990);
        body.setEndDate(endDate);
        CustomDate startDate = new CustomDate();
        startDate.setDay(10);
        startDate.setMonth(3);
        startDate.setYear(1990);
        body.setStartDate(startDate);
        body.setStationID("stationID");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerRetrieveVouchersManagerServiceSuccess());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setRetrieveVouchers(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerRetrieveVouchersResponse) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_RETRIEVE_VOUCHERS_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setManagerService(new MockManagerRetrieveVouchersManagerServiceFailure());
        ManagerRequest managerRequest = new ManagerRequest();
        managerRequest.setRetrieveVouchers(request);
        json = gson.toJson(managerRequest, ManagerRequest.class);
        baseResponse = frontend.managerJsonHandler(json);
        response = (ManagerRetrieveVouchersResponse) baseResponse.getEntity();
        assertEquals(StatusCode.MANAGER_RETRIEVE_VOUCHERS_FAILURE, response.getStatus().getStatusCode());
    }
}
