package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.ResponseHelper;

public class MockRetrieveRefuelPaymentDetailUserServiceFailure extends MockUserService {
    @Override
    public String checkAuthorization(String ticketID, Integer operationType) {
        // TODO Auto-generated method stub
        return ResponseHelper.CHECK_AUTHORIZATION_FAILED;
    }
}
