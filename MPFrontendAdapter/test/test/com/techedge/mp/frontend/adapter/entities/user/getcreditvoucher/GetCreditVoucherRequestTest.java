package test.com.techedge.mp.frontend.adapter.entities.user.getcreditvoucher;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.getcreditvoucher.GetCreditVoucherRequest;
import com.techedge.mp.frontend.adapter.entities.user.getcreditvoucher.GetCreditVoucherRequestBody;

public class GetCreditVoucherRequestTest extends BaseTestCase {

    private GetCreditVoucherRequest     request;
    private GetCreditVoucherRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new GetCreditVoucherRequest();
        body = new GetCreditVoucherRequestBody();
        body.setRefresh(true);
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.GET_CREDIT_VOUCHER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.GET_CREDIT_VOUCHER_FAILURE, request.check().getStatusCode());
    }

}