package test.com.techedge.mp.frontend.adapter.entities.user.authentication;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.Address;
import com.techedge.mp.core.business.interfaces.AuthenticationResponse;
import com.techedge.mp.core.business.interfaces.LastLoginData;
import com.techedge.mp.core.business.interfaces.LastLoginErrorData;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockAuthenticationUserServiceSuccess extends MockUserService {

    @Override
    public AuthenticationResponse authentication(String username, String password, String requestId, 
            String deviceId, String deviceName, String deviceToken) {
        AuthenticationResponse response = new AuthenticationResponse();
        response.setStatusCode(StatusCode.USER_AUTH_SUCCESS);

        User user = new User();
        user.setCapAvailable(12.0);
        user.setCapEffective(12.0);
        user.setContactDataMobilephone("012345");
        user.setContactDataStatus(1);
        user.setCreateDate(Calendar.getInstance().getTime());
        user.setCreditDataVoucher(1);

        LastLoginData lastLoginData = new LastLoginData();
        lastLoginData.setDeviceId("nexus");
        lastLoginData.setDeviceName("name");
        lastLoginData.setLatitude(45.3);
        lastLoginData.setLongitude(35.5);
        Timestamp time = new Timestamp(1470214665L);
        lastLoginData.setTime(time);
        user.setLastLoginData(lastLoginData);

        user.setExternalUserId("External");

        LastLoginErrorData lastLoginErrorData = new LastLoginErrorData();
        lastLoginErrorData.setAttempt(1);
        lastLoginErrorData.setTime(Calendar.getInstance().getTime());
        user.setLastLoginErrorData(lastLoginErrorData);

        Set<LoyaltyCard> loyaltyCardList = new HashSet<LoyaltyCard>(0);
        LoyaltyCard loyaltyCard = new LoyaltyCard();
        loyaltyCard.setDefaultCard(true);
        loyaltyCard.setEanCode("string");
        loyaltyCard.setPanCode("panCode");
        loyaltyCard.setStatus("1");
        loyaltyCard.setType("type");
        user.setLoyaltyCardList(loyaltyCardList);

        Set<PaymentInfo> paymentData = new HashSet<PaymentInfo>(0);
        PaymentInfo paymentInfo = new PaymentInfo();
        paymentInfo.setAttemptsLeft(1);
        paymentInfo.setBrand("brand");
        paymentInfo.setCardBin("cardBin");
        paymentInfo.setCheckAmount(1.1);
        paymentInfo.setCheckBankTransactionID("check");
        paymentInfo.setCheckCurrency("checkCurrency");
        user.setPaymentData(paymentData);

        PersonalData personalData = new PersonalData();
        Address address = new Address();
        address.setCity("Rome");
        address.setCountryCodeId("ITA");
        address.setCountryCodeName("ITA");
        address.setHouseNumber("0076");
        address.setRegion("Lazio");
        address.setStreet("Via del Corso");
        address.setZipcode("00100");
        personalData.setAddress(address);
        personalData.setBillingAddress(address);
        personalData.setBirthDate(Calendar.getInstance().getTime());
        personalData.setBirthMunicipality("birthMun");
        personalData.setBirthProvince("province");
        personalData.setFirstName("mario");
        personalData.setFiscalCode("CVBNMADFGHJK");
        personalData.setLanguage("ITA");
        personalData.setLastName("Rossi");
        personalData.setSecurityDataEmail("security");
        personalData.setSecurityDataPassword("passwordData");
        personalData.setSex("M");
        personalData.setTermsOfServiceData(new HashSet<TermsOfService>(0));
        user.setPersonalData(personalData);

        user.setUserStatus(1);
        user.setUserStatusRegistrationCompleted(true);
        user.setUserType(1);
        user.setUseVoucher(true);
        user.setVoucherList(new HashSet<Voucher>(0));

        response.setUser(user);
        return response;
    }

}
