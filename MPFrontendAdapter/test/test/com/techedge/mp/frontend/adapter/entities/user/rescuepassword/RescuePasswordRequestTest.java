package test.com.techedge.mp.frontend.adapter.entities.user.rescuepassword;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.rescuepassword.RescuePasswordRequest;
import com.techedge.mp.frontend.adapter.entities.user.rescuepassword.RescuePasswordRequestBody;

public class RescuePasswordRequestTest extends BaseTestCase {

    private RescuePasswordRequest     request;
    private RescuePasswordRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new RescuePasswordRequest();
        body = new RescuePasswordRequestBody();
        body.setEmail("a@a.it");
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_RESCUE_PASSWORD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}