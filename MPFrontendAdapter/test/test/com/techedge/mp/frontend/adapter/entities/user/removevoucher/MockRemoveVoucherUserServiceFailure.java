package test.com.techedge.mp.frontend.adapter.entities.user.removevoucher;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRemoveVoucherUserServiceFailure extends MockUserService {

    @Override
    public String removeVoucher(String ticketId, String requestId, String voucherCode) {
        // TODO Auto-generated method stub
        return StatusCode.USER_REMOVE_VOUCHER_FAILURE;
    }

}
