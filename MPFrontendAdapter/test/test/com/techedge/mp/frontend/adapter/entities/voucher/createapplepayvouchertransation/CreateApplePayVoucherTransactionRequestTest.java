package test.com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation.ApplePayVoucherPaymentDataRequest;
import com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation.CreateApplePayVoucherTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation.CreateApplePayVoucherTransactionRequest;

public class CreateApplePayVoucherTransactionRequestTest extends BaseTestCase {

    private CreateApplePayVoucherTransactionRequest     createApplePayVoucherTransactionRequest;
    private CreateApplePayVoucherTransactionBodyRequest createApplePayVoucherTransactionBodyRequest;
    private ApplePayVoucherPaymentDataRequest           applePayVoucherPaymentDataRequest;

    // assigning the values
    protected void setUp() {
        createApplePayVoucherTransactionRequest = new CreateApplePayVoucherTransactionRequest();
        createApplePayVoucherTransactionBodyRequest = new CreateApplePayVoucherTransactionBodyRequest();
        applePayVoucherPaymentDataRequest = new ApplePayVoucherPaymentDataRequest();

        String applePayPKPaymentToken = "aeiyrfgximaemurfxqoiqewumrfi=";
        Integer amount = 10;

        applePayVoucherPaymentDataRequest.setAmount(amount);
        applePayVoucherPaymentDataRequest.setApplePayPKPaymentToken(applePayPKPaymentToken);

        createApplePayVoucherTransactionBodyRequest.setApplePayVoucherPaymentData(applePayVoucherPaymentDataRequest);

        Credential credential = new Credential();
        credential.setTicketID(getTicketID_true());
        credential.setRequestID(getRequestID_true());

        createApplePayVoucherTransactionRequest.setCredential(credential);
        createApplePayVoucherTransactionRequest.setBody(createApplePayVoucherTransactionBodyRequest);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.CREATE_APPLE_PAY_VOUCHER_TRANSACTION_SUCCESS, createApplePayVoucherTransactionRequest.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        Credential credential = new Credential();
        credential.setTicketID(getTicketID_false());
        credential.setRequestID(getRequestID_false());
        createApplePayVoucherTransactionRequest.setCredential(credential);

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, createApplePayVoucherTransactionRequest.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        Credential credential = new Credential();
        credential.setTicketID(getTicketID_false());
        credential.setRequestID(getRequestID_true());
        createApplePayVoucherTransactionRequest.setCredential(credential);

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, createApplePayVoucherTransactionRequest.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        Credential credential = new Credential();
        credential.setTicketID(getTicketID_true());
        credential.setRequestID(getRequestID_false());
        createApplePayVoucherTransactionRequest.setCredential(credential);

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, createApplePayVoucherTransactionRequest.check().getStatusCode());
    }

}