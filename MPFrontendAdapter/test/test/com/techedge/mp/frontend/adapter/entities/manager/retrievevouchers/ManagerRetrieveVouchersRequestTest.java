package test.com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers.ManagerRetrieveVouchersBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers.ManagerRetrieveVouchersRequest;

public class ManagerRetrieveVouchersRequestTest extends BaseTestCase {

    private ManagerRetrieveVouchersRequest     request;
    private ManagerRetrieveVouchersBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new ManagerRetrieveVouchersRequest();
        body = new ManagerRetrieveVouchersBodyRequest();
        CustomDate endDate = new CustomDate();
        endDate.setDay(10);
        endDate.setMonth(3);
        endDate.setYear(1990);
        body.setEndDate(endDate);
        CustomDate startDate = new CustomDate();
        startDate.setDay(10);
        startDate.setMonth(3);
        startDate.setYear(1990);
        body.setStartDate(startDate);
        body.setStationID("stationID");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.MANAGER_RETRIEVE_VOUCHERS_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.getCredential().setTicketID(getTicketID_false());
        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(ResponseHelper.MANAGER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}