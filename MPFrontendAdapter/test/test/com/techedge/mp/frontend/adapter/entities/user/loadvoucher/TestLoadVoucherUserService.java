package test.com.techedge.mp.frontend.adapter.entities.user.loadvoucher;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.loadvoucher.LoadVoucherBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.loadvoucher.LoadVoucherRequest;
import com.techedge.mp.frontend.adapter.entities.user.loadvoucher.LoadVoucherResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestLoadVoucherUserService extends BaseTestCase {
    private FrontendAdapterService frontend;
    private LoadVoucherRequest     request;
    private LoadVoucherBodyRequest body;
    private LoadVoucherResponse    response;
    private Response               baseResponse;
    private String                 json;
    private Gson                   gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new LoadVoucherRequest();
        body = new LoadVoucherBodyRequest();
        body.setVoucherCode("voucher");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testLoadVoucherSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockLoadVoucherUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setLoadVoucher(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (LoadVoucherResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_LOAD_VOUCHER_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testLoadVoucherFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockLoadVoucherUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setLoadVoucher(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (LoadVoucherResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_LOAD_VOUCHER_FAILURE, response.getStatus().getStatusCode());
    }
}
