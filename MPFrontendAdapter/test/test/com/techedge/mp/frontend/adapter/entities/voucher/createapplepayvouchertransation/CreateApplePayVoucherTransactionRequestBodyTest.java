package test.com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation.ApplePayVoucherPaymentDataRequest;
import com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation.CreateApplePayVoucherTransactionBodyRequest;

public class CreateApplePayVoucherTransactionRequestBodyTest extends BaseTestCase {

    private CreateApplePayVoucherTransactionBodyRequest createApplePayVoucherTransactionBodyRequest;
    private ApplePayVoucherPaymentDataRequest           applePayVoucherPaymentDataRequest;

    // assigning the values
    protected void setUp() {
        createApplePayVoucherTransactionBodyRequest = new CreateApplePayVoucherTransactionBodyRequest();
        applePayVoucherPaymentDataRequest           = new ApplePayVoucherPaymentDataRequest();
        
        Integer amount = 10;
        String  applePayPKPaymentToken = "fifuqqmeirxfqiue=";
        
        applePayVoucherPaymentDataRequest.setAmount(amount);
        applePayVoucherPaymentDataRequest.setApplePayPKPaymentToken(applePayPKPaymentToken);
        
        createApplePayVoucherTransactionBodyRequest.setApplePayVoucherPaymentData(applePayVoucherPaymentDataRequest);
    }

    @Test
    public void testAmountNull() {
        applePayVoucherPaymentDataRequest.setAmount(null);
        assertEquals(StatusCode.CREATE_APPLE_PAY_VOUCHER_TRANSACTION_INVALID_PARAMETERS, applePayVoucherPaymentDataRequest.check().getStatusCode());
    }
    
    @Test
    public void testApplePayPKPaymentTokenNull() {
        applePayVoucherPaymentDataRequest.setApplePayPKPaymentToken(null);
        assertEquals(StatusCode.CREATE_APPLE_PAY_VOUCHER_TRANSACTION_INVALID_PARAMETERS, applePayVoucherPaymentDataRequest.check().getStatusCode());
    }

}