package test.com.techedge.mp.frontend.adapter.entities.postpaid.confirmtransaction;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.confirm.ConfirmTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.confirm.ConfirmTransactionRequest;

public class ConfirmTransactionRequestTest extends BaseTestCase {

    private ConfirmTransactionRequest     request;
    private ConfirmTransactionBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new ConfirmTransactionRequest();
        body = new ConfirmTransactionBodyRequest();
        body.setTransactionID("qwertyuiqwertyuiqwertyuiqwertyui");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.POP_CONFIRM_TRANSACTION_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}