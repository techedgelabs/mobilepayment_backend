package test.com.techedge.mp.frontend.adapter.entities.manager.updatepassword;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockManagerService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockManagerUpdatePasswordManagerServiceSuccess extends MockManagerService {
    @Override
    public String updatePassword(String ticketId, String requestId, String oldPassword, String newPassword) {
        // TODO Auto-generated method stub
        return StatusCode.MANAGER_PWD_SUCCESS;
    }
}
