package test.com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard.SetDefaultLoyaltyCardBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard.SetDefaultLoyaltyCardRequest;

public class SetDefaultLoyaltyCardRequestTest extends BaseTestCase {

    private SetDefaultLoyaltyCardRequest     request;
    private SetDefaultLoyaltyCardBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new SetDefaultLoyaltyCardRequest();
        body = new SetDefaultLoyaltyCardBodyRequest();
        body.setPanCode("panCode");
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_SET_DEFAULT_LOYALTY_CARD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}