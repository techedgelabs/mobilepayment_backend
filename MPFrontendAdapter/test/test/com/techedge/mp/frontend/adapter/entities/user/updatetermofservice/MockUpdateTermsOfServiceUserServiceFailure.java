package test.com.techedge.mp.frontend.adapter.entities.user.updatetermofservice;

import java.util.List;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockUpdateTermsOfServiceUserServiceFailure extends MockUserService {

    @Override
    public String updateTermsOfService(String ticketID, String requestID, List<TermsOfService> termsOfServiceList) {
        // TODO Auto-generated method stub
        return StatusCode.USER_UPDATE_TERMS_OF_SERVICE_SUCCESS;
    }
}
