package test.com.techedge.mp.frontend.adapter.entities.user.skippaymentmethodconfiguration;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.skippaymentmethodconfiguration.SkipPaymentMethodConfigurationRequest;

public class SkipPaymentMethodConfigurationRequestTest extends BaseTestCase {

    private SkipPaymentMethodConfigurationRequest request;

    // assigning the values
    protected void setUp() {
        request = new SkipPaymentMethodConfigurationRequest();
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.SKIP_PAYMENT_METHOD_CONFIGURATION_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}