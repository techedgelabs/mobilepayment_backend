package test.com.techedge.mp.frontend.adapter.entities.user.setusevoucher;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.setusevoucher.SetUseVoucherBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.setusevoucher.SetUseVoucherRequest;

public class SetUseVoucherRequestTest extends BaseTestCase {

    private SetUseVoucherRequest     request;
    private SetUseVoucherBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new SetUseVoucherRequest();
        body = new SetUseVoucherBodyRequest();
        body.setUseVoucher(true);
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_SET_USE_VOUCHER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}