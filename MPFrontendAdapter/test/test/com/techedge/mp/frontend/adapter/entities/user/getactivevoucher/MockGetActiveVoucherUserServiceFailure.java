package test.com.techedge.mp.frontend.adapter.entities.user.getactivevoucher;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.GetActiveVouchersData;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockGetActiveVoucherUserServiceFailure extends MockUserService {

    @Override
    public GetActiveVouchersData getActiveVouchers(String ticketId, String requestId, Boolean refresh, String loyaltySessionID) {
        GetActiveVouchersData data = new GetActiveVouchersData();
        data.setStatusCode(StatusCode.USER_GET_ACTIVE_VOUCHERS_FAILURE);
        data.setTotal(10.0);
        return data;
    }

}
