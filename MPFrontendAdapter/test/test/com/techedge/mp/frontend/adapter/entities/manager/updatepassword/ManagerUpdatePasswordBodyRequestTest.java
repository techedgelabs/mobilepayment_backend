package test.com.techedge.mp.frontend.adapter.entities.manager.updatepassword;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.updatepassword.ManagerUpdatePasswordBodyRequest;

public class ManagerUpdatePasswordBodyRequestTest extends BaseTestCase {

    private ManagerUpdatePasswordBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new ManagerUpdatePasswordBodyRequest();
        body.setNewPassword("Password123");
        body.setOldPassword("pAssWord345");
    }

    @Test
    public void testNewPasswordNull() {
        body.setNewPassword(null);
        assertEquals(StatusCode.USER_PWD_OLD_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testNewPasswordMajor() {
        body.setNewPassword("test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_PWD_OLD_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testOldPasswordNull() {
        body.setOldPassword(null);
        assertEquals(StatusCode.MANAGER_PWD_OLD_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testOldPassowordIsEmpty() {
        body.setOldPassword("");
        assertEquals(StatusCode.MANAGER_PWD_OLD_PASSWORD_WRONG, body.check().getStatusCode());
    }

}