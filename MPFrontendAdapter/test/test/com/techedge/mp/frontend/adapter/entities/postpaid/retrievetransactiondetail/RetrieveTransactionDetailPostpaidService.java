package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail.RetrieveTransactionDetailBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail.RetrieveTransactionDetailRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail.RetrieveTransactionDetailResponse;
import com.techedge.mp.frontend.adapter.entities.requests.PoPRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrieveTransactionDetailPostpaidService extends BaseTestCase {
    private FrontendAdapterService               frontend;
    private RetrieveTransactionDetailRequest     request;
    private RetrieveTransactionDetailBodyRequest body;
    private RetrieveTransactionDetailResponse    response;
    private Response                             baseResponse;
    private String                               json;
    private Gson                                 gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrieveTransactionDetailRequest();
        body = new RetrieveTransactionDetailBodyRequest();
        body.setTransactionId("codecodecodecodecodecodecodecode");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRescuePasswordSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrieveTransactionDetailPostpaidServiceSuccess());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrieveTransactionDetail(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrieveTransactionDetailResponse) baseResponse.getEntity();
        assertEquals(StatusCode.POP_DETAIL_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRescuePasswordFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrieveTransactionDetailPostpaidServiceFailure());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrieveTransactionDetail(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrieveTransactionDetailResponse) baseResponse.getEntity();
        assertEquals(StatusCode.POP_DETAIL_FAILURE, response.getStatus().getStatusCode());
    }
}
