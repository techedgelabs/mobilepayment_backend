package test.com.techedge.mp.frontend.adapter.entities.user.logout;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockLogoutUserServiceFailure extends MockUserService {

    @Override
    public String logout(String ticketId, String requestId) {
        // TODO Auto-generated method stub
        return StatusCode.USER_LOGOUT_FAILURE;
    }

}
