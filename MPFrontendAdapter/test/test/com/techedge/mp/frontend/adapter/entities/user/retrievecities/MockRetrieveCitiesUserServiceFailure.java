package test.com.techedge.mp.frontend.adapter.entities.user.retrievecities;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.RetrieveCitiesData;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrieveCitiesUserServiceFailure extends MockUserService {

    @Override
    public RetrieveCitiesData retrieveCities(String ticketID, String requestID, String searchKey) {
        RetrieveCitiesData data = new RetrieveCitiesData();
        data.setStatusCode(StatusCode.RETRIEVE_CITIES_FAILURE);
        return data;
    }

}
