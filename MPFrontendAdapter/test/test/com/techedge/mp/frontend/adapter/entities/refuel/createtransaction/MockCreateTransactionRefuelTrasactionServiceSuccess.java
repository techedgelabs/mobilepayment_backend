package test.com.techedge.mp.frontend.adapter.entities.refuel.createtransaction;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.interfaces.CreateRefuelResponse;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockCreateTransactionRefuelTrasactionServiceSuccess extends MockTransactionService {
    @Override
    public CreateRefuelResponse createRefuel(String ticketID, String requestID, String encodedPin, String stationID, String pumpID, Integer pumpNumber, String productID,
            String productDescription, Double amount, Double amountVoucher, Boolean useVoucher, Long cardId, String cardType, String outOfRange, String refuelMode) {
        CreateRefuelResponse response = new CreateRefuelResponse();
        response.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);
        return response;
    }
}
