package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail.RetrieveRefuelPaymentDetailBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail.RetrieveRefuelPaymentDetailRequest;

public class RetrieveRefuelPaymentDetailRequestTest extends BaseTestCase {

    private RetrieveRefuelPaymentDetailRequest     request;
    private RetrieveRefuelPaymentDetailBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveRefuelPaymentDetailRequest();
        body = new RetrieveRefuelPaymentDetailBodyRequest();
        body.setRefuelID("qwertyuiopasdfghjklzxcvbnmqwerty");;
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.RETRIEVE_PAYMENT_DETAIL_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.getCredential().setTicketID(getTicketID_false());
        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
    

    @Test
    public void testBodyNull() {
        request.setBody(null);
        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}