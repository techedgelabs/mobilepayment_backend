package test.com.techedge.mp.frontend.adapter.entities.survey.getsurvey;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.survey.getsurvey.GetSurveyBodyRequest;
import com.techedge.mp.frontend.adapter.entities.survey.getsurvey.GetSurveyRequest;

public class GetSurveyRequestTest extends BaseTestCase {

    private GetSurveyRequest     request;
    private GetSurveyBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new GetSurveyRequest();
        body = new GetSurveyBodyRequest();
        body.setCode("code");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.SURVEY_GET_SURVEY_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}