package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievestations;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockParameterService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;

public class MockParameterServiceSuccess extends MockParameterService {

    @Override
    public String getParamValue(String paramName) throws ParameterNotFoundException {
        // TODO Auto-generated method stub
        return "0.1";
    }
}
