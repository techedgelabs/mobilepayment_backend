package test.com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation.CreateVoucherTransactionBodyRequest;
import com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation.VoucherPaymentDataRequest;

public class CreateVoucherTransactionRequestBodyTest extends BaseTestCase {

    private CreateVoucherTransactionBodyRequest body;
    private VoucherPaymentDataRequest           data;
    private PaymentMethod                       paymentMethod;

    // assigning the values
    protected void setUp() {
        body = new CreateVoucherTransactionBodyRequest();
        data = new VoucherPaymentDataRequest();
        paymentMethod = new PaymentMethod();
        paymentMethod.setType("tipo");
        paymentMethod.setId(1L);
        data.setAmount(10);
        data.setPaymentMethod(paymentMethod);
        body.setVoucherPaymentData(data);
    }

    @Test
    public void testTypeNull() {
        paymentMethod.setType(null);
        data.setPaymentMethod(paymentMethod);
        assertEquals(StatusCode.CREATE_VOUCHER_TRANSACTION_INVALID_PARAMETERS, data.check().getStatusCode());
    }

    @Test
    public void testTypeIsEmpty() {
        paymentMethod.setType("");
        data.setPaymentMethod(paymentMethod);

        assertEquals(StatusCode.CREATE_VOUCHER_TRANSACTION_INVALID_PARAMETERS, data.check().getStatusCode());
    }

    @Test
    public void testIdNull() {
        paymentMethod.setType(null);
        data.setPaymentMethod(paymentMethod);
        assertEquals(StatusCode.CREATE_VOUCHER_TRANSACTION_INVALID_PARAMETERS, data.check().getStatusCode());
    }

    @Test
    public void testAmountNull() {
        data.setAmount(null);
        assertEquals(StatusCode.CREATE_VOUCHER_TRANSACTION_INVALID_PARAMETERS, data.check().getStatusCode());
    }

}