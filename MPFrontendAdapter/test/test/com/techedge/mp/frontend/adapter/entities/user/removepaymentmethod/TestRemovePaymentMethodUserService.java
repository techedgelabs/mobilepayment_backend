package test.com.techedge.mp.frontend.adapter.entities.user.removepaymentmethod;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.removepaymentmethod.RemovePaymentMethodBodyRequest;
import com.techedge.mp.frontend.adapter.entities.user.removepaymentmethod.RemovePaymentMethodRequest;
import com.techedge.mp.frontend.adapter.entities.user.removepaymentmethod.RemovePaymentMethodResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestRemovePaymentMethodUserService extends BaseTestCase {
    private FrontendAdapterService         frontend;
    private RemovePaymentMethodRequest     request;
    private RemovePaymentMethodBodyRequest body;
    private RemovePaymentMethodResponse    response;
    private Response                       baseResponse;
    private String                         json;
    private Gson                           gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RemovePaymentMethodRequest();
        body = new RemovePaymentMethodBodyRequest();
        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setBrand("Brand");
        paymentMethod.setId(1L);
        paymentMethod.setDefaultMethod(true);
        paymentMethod.setType("1");
        paymentMethod.setStatus(1);
        body.setPaymentMethod(paymentMethod);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRemovePaymentMethodUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setRemovePaymentMethod(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RemovePaymentMethodResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_REMOVE_PAYMENT_METHOD_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockRemovePaymentMethodUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setRemovePaymentMethod(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (RemovePaymentMethodResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_REMOVE_PAYMENT_METHOD_FAILURE, response.getStatus().getStatusCode());
    }
}
