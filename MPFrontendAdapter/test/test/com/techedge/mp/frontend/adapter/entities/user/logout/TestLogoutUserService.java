package test.com.techedge.mp.frontend.adapter.entities.user.logout;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.frontend.adapter.entities.user.logout.LogoutUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.logout.LogoutUserResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class TestLogoutUserService extends BaseTestCase {
    private FrontendAdapterService frontend;
    private LogoutUserRequest      request;
    private LogoutUserResponse     response;
    private Response               baseResponse;
    private String                 json;
    private Gson                   gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new LogoutUserRequest();
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testLoadVoucherSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockLogoutUserServiceSuccess());
        UserRequest userRequest = new UserRequest();
        userRequest.setLogout(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (LogoutUserResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_LOGOUT_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testLoadVoucherFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setUserService(new MockLogoutUserServiceFailure());
        UserRequest userRequest = new UserRequest();
        userRequest.setLogout(request);
        json = gson.toJson(userRequest, UserRequest.class);
        baseResponse = frontend.userJsonHandler(json);
        response = (LogoutUserResponse) baseResponse.getEntity();
        assertEquals(StatusCode.USER_LOGOUT_FAILURE, response.getStatus().getStatusCode());
    }
}
