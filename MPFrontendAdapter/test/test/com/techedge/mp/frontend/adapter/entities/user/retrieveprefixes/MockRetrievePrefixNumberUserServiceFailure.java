package test.com.techedge.mp.frontend.adapter.entities.user.retrieveprefixes;

import java.util.ArrayList;
import java.util.List;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.PrefixNumberResult;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockRetrievePrefixNumberUserServiceFailure extends MockUserService {

    @Override
    public PrefixNumberResult retrieveAllPrefixNumber(String ticketId, String requestId) {
        PrefixNumberResult result = new PrefixNumberResult();
        List<String> list = new ArrayList<String>(0);
        list.add("1");
        list.add("3");
        result.setListPrefix(list);
        result.setStatusCode(StatusCode.RETRIEVE_PREFIXES_FAILURE);
        return result;
    }
}
