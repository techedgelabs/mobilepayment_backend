package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockBpelServiceRemoteService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockForecourtInfoService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockTransactionService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory.RetrieveRefuelPaymentHistoryBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory.RetrieveRefuelPaymentHistoryRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory.RetrieveRefuelPaymentHistoryResponse;
import com.techedge.mp.frontend.adapter.entities.requests.RefuelRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrieveRefuelPaymentHistoryRefuelService extends BaseTestCase {
    private FrontendAdapterService                  frontend;
    private RetrieveRefuelPaymentHistoryRequest     request;
    private RetrieveRefuelPaymentHistoryBodyRequest body;
    private RetrieveRefuelPaymentHistoryResponse    response;
    private Response                                baseResponse;
    private String                                  json;
    private Gson                                    gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrieveRefuelPaymentHistoryRequest();
        body = new RetrieveRefuelPaymentHistoryBodyRequest();
        CustomDate customDate = new CustomDate();
        customDate.setDay(10);
        customDate.setMonth(1);
        customDate.setYear(1990);
        body.setStartDate(customDate);
        body.setEndDate(customDate);
        body.setItemsLimit(1);
        body.setPageOffset(1);
        body.setDetails(true);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setForecourtInfoService(new MockForecourtInfoService());
        EJBHomeCache.getInstance().setBpelService(new MockBpelServiceRemoteService());
    }

    @Test
    public void testRecoverUsernameSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockRetrieveRefuelPaymentHistoryTrasactionServiceSuccess());
        EJBHomeCache.getInstance().setUserService(new MockRetrieveRefuelPaymentHistoryUserServiceSuccess());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setRetrieveRefuelPaymentHistory(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (RetrieveRefuelPaymentHistoryResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRecoverUsernameFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setTransactionService(new MockRetrieveRefuelPaymentHistoryTransactionServiceFailure());
        EJBHomeCache.getInstance().setUserService(new MockRetrieveRefuelPaymentHistoryUserServiceSuccess());

        RefuelRequest refuelRequest = new RefuelRequest();
        refuelRequest.setRetrieveRefuelPaymentHistory(request);
        json = gson.toJson(refuelRequest, RefuelRequest.class);
        baseResponse = frontend.refuelJsonHandler(json);
        response = (RetrieveRefuelPaymentHistoryResponse) baseResponse.getEntity();
        assertEquals(StatusCode.REFUEL_EXEC_PAY_HISTORY_FAILURE, response.getStatus().getStatusCode());
    }

}
