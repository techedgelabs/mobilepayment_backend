package test.com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard.SetDefaultLoyaltyCardBodyRequest;

public class SetDefaultLoyaltyCardRequestBodyTest extends BaseTestCase {

    private SetDefaultLoyaltyCardBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new SetDefaultLoyaltyCardBodyRequest();
        body.setPanCode("panCode");
    }

    @Test
    public void testPanCodeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPanCode(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testPanCodeIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPanCode("");
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testPanCodeMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setPanCode("test1test1test1test1");
        assertEquals(StatusCode.USER_SET_DEFAULT_LOYALTY_CARD_PANCODE_WRONG, body.check().getStatusCode());
    }
}