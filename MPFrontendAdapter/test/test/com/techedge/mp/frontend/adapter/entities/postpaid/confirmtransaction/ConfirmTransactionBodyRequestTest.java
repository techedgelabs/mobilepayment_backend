package test.com.techedge.mp.frontend.adapter.entities.postpaid.confirmtransaction;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.confirm.ConfirmTransactionBodyRequest;

public class ConfirmTransactionBodyRequestTest extends BaseTestCase {

    private ConfirmTransactionBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new ConfirmTransactionBodyRequest();
        body.setTransactionID("qwertyuiqwertyuiqwertyuiqwertyui");
    }

    @Test
    public void testTransactionIDNull() {
        body.setTransactionID(null);
        assertEquals(StatusCode.POP_CONFIRM_TRANSACTION_WRONG_ID, body.check().getStatusCode());
    }

    @Test
    public void testTransactionIDIsEmpty() {
        body.setTransactionID("");
        assertEquals(StatusCode.POP_CONFIRM_TRANSACTION_WRONG_ID, body.check().getStatusCode());
    }

    @Test
    public void testTransactionIDNotValid() {
        body.setTransactionID("ciao");
        assertEquals(StatusCode.POP_CONFIRM_TRANSACTION_WRONG_ID, body.check().getStatusCode());
    }
}