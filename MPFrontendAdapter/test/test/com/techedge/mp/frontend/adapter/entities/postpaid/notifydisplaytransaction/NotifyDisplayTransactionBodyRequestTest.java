package test.com.techedge.mp.frontend.adapter.entities.postpaid.notifydisplaytransaction;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.notifydisplaytransaction.NotifyDisplayTransactionBodyRequest;

public class NotifyDisplayTransactionBodyRequestTest extends BaseTestCase {

    private NotifyDisplayTransactionBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new NotifyDisplayTransactionBodyRequest();
        body.setMPCode("qwertyuiqwertyuiqwertyuiqwertyui");
    }

    @Test
    public void testMPCodeNull() {
        body.setMPCode(null);
        assertEquals(StatusCode.REFUEL_CONFIRM_REFUEL_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testMPCodeIsEmpty() {
        body.setMPCode("");
        assertEquals(StatusCode.REFUEL_CONFIRM_REFUEL_ID_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testMPCodeNotValid() {
        body.setMPCode("ciao");
        assertEquals(StatusCode.REFUEL_CONFIRM_REFUEL_ID_WRONG, body.check().getStatusCode());
    }
}