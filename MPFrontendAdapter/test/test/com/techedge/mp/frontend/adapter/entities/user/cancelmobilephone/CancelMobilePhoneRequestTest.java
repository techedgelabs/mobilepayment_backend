package test.com.techedge.mp.frontend.adapter.entities.user.cancelmobilephone;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.cancelmobilephone.CancelMobilePhoneRequest;
import com.techedge.mp.frontend.adapter.entities.user.cancelmobilephone.CancelMobilePhoneRequestBody;

public class CancelMobilePhoneRequestTest extends BaseTestCase {

    private CancelMobilePhoneRequest     request;
    private CancelMobilePhoneRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new CancelMobilePhoneRequest();
        body = new CancelMobilePhoneRequestBody();
        body.setMobilePhoneId(3213456789L);
        request.setBody(body);
        request.setCredential(createCredentialTrue());

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.CANCEL_MOBILE_PHONE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.CANCEL_MOBILE_PHONE_FAILURE, request.check().getStatusCode());
    }

}