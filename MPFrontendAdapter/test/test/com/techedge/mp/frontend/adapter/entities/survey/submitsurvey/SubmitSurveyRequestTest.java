package test.com.techedge.mp.frontend.adapter.entities.survey.submitsurvey;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.survey.submitsurvey.SubmitSurveyBodyRequest;
import com.techedge.mp.frontend.adapter.entities.survey.submitsurvey.SubmitSurveyRequest;

public class SubmitSurveyRequestTest extends BaseTestCase {

    private SubmitSurveyRequest     request;
    private SubmitSurveyBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new SubmitSurveyRequest();
        body = new SubmitSurveyBodyRequest();
        body.setCode("code");
        body.setKey("key");
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.SURVEY_SUBMIT_SURVEY_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}