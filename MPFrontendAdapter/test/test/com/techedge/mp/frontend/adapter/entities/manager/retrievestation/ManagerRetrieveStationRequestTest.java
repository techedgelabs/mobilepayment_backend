package test.com.techedge.mp.frontend.adapter.entities.manager.retrievestation;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.retrievestation.ManagerRetrieveStationDetailsBodyRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievestation.ManagerRetrieveStationDetailsRequest;

public class ManagerRetrieveStationRequestTest extends BaseTestCase {

    private ManagerRetrieveStationDetailsRequest     request;
    private ManagerRetrieveStationDetailsBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new ManagerRetrieveStationDetailsRequest();
        body = new ManagerRetrieveStationDetailsBodyRequest();
        body.setStationID("001");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(ResponseHelper.MANAGER_STATION_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.getCredential().setTicketID(getTicketID_false());
        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}