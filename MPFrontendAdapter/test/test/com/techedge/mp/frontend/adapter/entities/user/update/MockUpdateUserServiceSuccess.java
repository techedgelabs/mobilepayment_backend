package test.com.techedge.mp.frontend.adapter.entities.user.update;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockUpdateUserServiceSuccess extends MockUserService {
    @Override
    public String updateUser(String ticketId, String requestId, User user) {

        return StatusCode.USER_UPD_SUCCESS;
    }

}
