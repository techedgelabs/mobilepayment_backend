package test.com.techedge.mp.frontend.adapter.entities.user.updatepassword;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.updatepassword.UpdatePasswordRequestBody;

public class UpdatePasswordRequestBodyTest extends BaseTestCase {

    private UpdatePasswordRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new UpdatePasswordRequestBody();
        body.setNewPassword("Password123");
        body.setOldPassword("NewPasswoer456");
    }

    @Test
    public void testOldPasswordNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setOldPassword(null);
        assertEquals(StatusCode.USER_PWD_OLD_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testOldPasswordIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setOldPassword("");
        assertEquals(StatusCode.USER_PWD_OLD_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testNewPasswordNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setNewPassword(null);
        assertEquals(StatusCode.USER_PWD_OLD_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testNewPasswordMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setNewPassword("Test1test1test1test1test1test1test1test1test1Test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.USER_PWD_OLD_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testNewPasswordUpperCase() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setNewPassword("ciaociaociao");
        assertEquals(StatusCode.USER_PWD_UPPER_LESS, body.check().getStatusCode());
    }

    @Test
    public void testNewPasswordLowerCase() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setNewPassword("CIAOCAIOCIAO");
        assertEquals(StatusCode.USER_PWD_LOWER_LESS, body.check().getStatusCode());
    }

    @Test
    public void testNewPasswordShort() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setNewPassword("ciao");
        assertEquals(StatusCode.USER_PWD_PASSWORD_SHORT, body.check().getStatusCode());
    }

}