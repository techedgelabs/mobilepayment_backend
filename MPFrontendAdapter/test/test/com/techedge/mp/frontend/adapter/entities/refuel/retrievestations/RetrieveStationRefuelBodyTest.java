package test.com.techedge.mp.frontend.adapter.entities.refuel.retrievestations;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationBodyRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationUserPositionRequest;

public class RetrieveStationRefuelBodyTest extends BaseTestCase {

    private RetrieveStationBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveStationBodyRequest();
        RetrieveStationUserPositionRequest position = new RetrieveStationUserPositionRequest();
        position.setLatitude(45.4);
        position.setLongitude(35.5);
        body.setBeaconCode("beacon");
        body.setCode("code");
        body.setCodeType("code");
        body.setUserPosition(position);
    }

    @Test
    public void testCodeTypeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCodeType(null);
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testCodeTypeIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCodeType("");
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testCodeTypeMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCodeType("GPS12GPS12GPS12GPS12GPS12GPS12GPS12GPS12GPS12GPS12");
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG, body.check().getStatusCode());
    }
    
    @Test
    public void testCodeTypeGPS() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCodeType("GPS");
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testCodeTypeGPSPositionNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCodeType("GPS");
        body.setCode(null);
        body.setBeaconCode(null);
        body.setUserPosition(null);
        assertEquals(StatusCode.STATION_RETRIEVE_USER_POSITION_WRONG, body.check().getStatusCode());
    }
    
    @Test
    public void testCodeTypeGPSPositionLatitudeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCodeType("GPS");
        body.setCode(null);
        body.setBeaconCode(null);        body.getUserPosition().setLatitude(null);
        assertEquals(StatusCode.STATION_RETRIEVE_USER_POSITION_WRONG, body.check().getStatusCode());
    }
    
    @Test
    public void testCodeTypeGPSPositionLongitudeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCodeType("GPS");
        body.setCode(null);
        body.setBeaconCode(null);
        body.getUserPosition().setLongitude(null);
        assertEquals(StatusCode.STATION_RETRIEVE_USER_POSITION_WRONG, body.check().getStatusCode());
    }
    
    @Test
    public void testCodeNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCode(null);
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_WRONG, body.check().getStatusCode());
    }
    
    @Test
    public void testCodeIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCode("");
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_WRONG, body.check().getStatusCode());
    }
    
    @Test
    public void testCodeMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setCode("GPS12GPS12GPS12GPS12GPS12GPS12GPS12GPS12GPS12GPS12");
        assertEquals(StatusCode.STATION_RETRIEVE_CODE_WRONG, body.check().getStatusCode());
    }
    
    @Test
    public void testBeaconCodeMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setBeaconCode("GPS12GPS12GPS12GPS12GPS12GPS12GPS12GPS12GPS12GPS12");
        assertEquals(StatusCode.STATION_RETRIEVE_BEACONCODE_WRONG, body.check().getStatusCode());
    }

}