package test.com.techedge.mp.frontend.adapter.entities.user.resendverificationcode;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.resendverificationcode.ResendValidationRequest;
import com.techedge.mp.frontend.adapter.entities.user.resendverificationcode.ResendValidationRequestBody;

public class ResendVerificationCodeRequestTest extends BaseTestCase {

    private ResendValidationRequest     request;
    private ResendValidationRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new ResendValidationRequest();
        body = new ResendValidationRequestBody();
        body.setId("1");
        body.setType("type");
        request.setBody(body);
        request.setCredential(createCredentialTrue());

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_RESEND_VALIDATATION_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.USER_RESEND_VALIDATATION_INVALID_PARAMETERS, request.check().getStatusCode());
    }

}