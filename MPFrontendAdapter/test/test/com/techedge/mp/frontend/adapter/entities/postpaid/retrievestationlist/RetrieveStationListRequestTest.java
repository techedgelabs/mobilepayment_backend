package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveStationListBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveStationListRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveStationUserPositionRequest;

public class RetrieveStationListRequestTest extends BaseTestCase {

    private RetrieveStationListRequest     request;
    private RetrieveStationListBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveStationListRequest();
        body = new RetrieveStationListBodyRequest();
        RetrieveStationUserPositionRequest position = new RetrieveStationUserPositionRequest();
        position.setLatitude(45.3);
        position.setLongitude(34.5);
        body.setUserPosition(position);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        assertEquals(StatusCode.STATION_RETRIEVE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.SURVEY_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.getCredential().setTicketID(getTicketID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.getCredential().setRequestID(getRequestID_false());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}