package test.com.techedge.mp.frontend.adapter.entities.user.resetpin;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.resetpin.ResetPinRequest;
import com.techedge.mp.frontend.adapter.entities.user.resetpin.ResetPinRequestBody;

public class ResetPinRequestTest extends BaseTestCase {

    private ResetPinRequest     request;
    private ResetPinRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new ResetPinRequest();
        body = new ResetPinRequestBody();
        body.setPassword("Password123");
        body.setNewPin("1456");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.USER_PIN_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.USER_PIN_FAILURE, request.check().getStatusCode());
    }

}