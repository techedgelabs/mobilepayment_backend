package test.com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers.ManagerRetrieveVouchersBodyRequest;

public class ManagerRetrieveVouchersBodyRequestTest extends BaseTestCase {

    private ManagerRetrieveVouchersBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new ManagerRetrieveVouchersBodyRequest();
        CustomDate endDate = new CustomDate();
        endDate.setDay(10);
        endDate.setMonth(3);
        endDate.setYear(1990);
        body.setEndDate(endDate);
        CustomDate startDate = new CustomDate();
        startDate.setDay(10);
        startDate.setMonth(3);
        startDate.setYear(1990);
        body.setStartDate(startDate);
        body.setStationID("stationID");
    }

    @Test
    public void testStationIDNull() {
        body.setStationID(null);
        assertEquals(StatusCode.MANAGER_RETRIEVE_VOUCHERS_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testStationIDIsEmpty() {
        body.setStationID("");
        assertEquals(StatusCode.MANAGER_RETRIEVE_VOUCHERS_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testStartDateNull() {
        body.setStartDate(null);
        assertEquals(StatusCode.MANAGER_RETRIEVE_VOUCHERS_INVALID_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testEndDateNull() {
        body.setEndDate(null);
        assertEquals(StatusCode.MANAGER_RETRIEVE_VOUCHERS_INVALID_PARAMETERS, body.check().getStatusCode());
    }

}