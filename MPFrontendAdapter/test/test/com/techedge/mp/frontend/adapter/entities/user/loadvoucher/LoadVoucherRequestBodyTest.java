package test.com.techedge.mp.frontend.adapter.entities.user.loadvoucher;

import java.lang.reflect.InvocationTargetException;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.loadvoucher.LoadVoucherBodyRequest;

public class LoadVoucherRequestBodyTest extends BaseTestCase {

    private LoadVoucherBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new LoadVoucherBodyRequest();
        body.setVoucherCode("1234");
    }

    @Test
    public void testVoucherNull() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVoucherCode(null);
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testVoucherIsEmpty() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVoucherCode("");
        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testVoucherMajor() throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        body.setVoucherCode("test1test1test1test1");
        assertEquals(StatusCode.USER_LOAD_VOUCHER_CODE_WRONG, body.check().getStatusCode());
    }
}