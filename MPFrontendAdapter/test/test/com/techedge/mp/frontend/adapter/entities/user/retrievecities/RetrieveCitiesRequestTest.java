package test.com.techedge.mp.frontend.adapter.entities.user.retrievecities;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.user.retrievecities.RetrieveCitiesRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrievecities.RetrieveCitiesRequestBody;

public class RetrieveCitiesRequestTest extends BaseTestCase {

    private RetrieveCitiesRequest     request;
    private RetrieveCitiesRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveCitiesRequest();
        body = new RetrieveCitiesRequestBody();
        body.setSearchKey("key");
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());

        assertEquals(StatusCode.RETRIEVE_CITIES_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.USER_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}