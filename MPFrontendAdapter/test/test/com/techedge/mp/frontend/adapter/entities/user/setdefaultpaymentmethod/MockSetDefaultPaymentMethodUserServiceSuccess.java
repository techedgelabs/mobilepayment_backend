package test.com.techedge.mp.frontend.adapter.entities.user.setdefaultpaymentmethod;

import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;

public class MockSetDefaultPaymentMethodUserServiceSuccess extends MockUserService {

    @Override
    public String setDefaultPaymentMethod(String ticketId, String requestId, Long paymentMethodId, String paymentMethodType) {
        // TODO Auto-generated method stub
        return StatusCode.USER_SET_DEFAULT_PAYMENT_METHOD_SUCCESS;
    }

}
