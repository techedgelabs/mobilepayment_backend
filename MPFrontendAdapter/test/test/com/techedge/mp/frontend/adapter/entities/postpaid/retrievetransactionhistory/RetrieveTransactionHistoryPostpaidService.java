package test.com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontend.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontend.adapter.webservices.common.MockUserService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory.RetrieveTransactionHistoryBodyRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory.RetrieveTransactionHistoryRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory.RetrieveTransactionHistoryResponse;
import com.techedge.mp.frontend.adapter.entities.requests.PoPRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontend.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontend.adapter.webservices.FrontendAdapterService;

public class RetrieveTransactionHistoryPostpaidService extends BaseTestCase {
    private FrontendAdapterService                frontend;
    private RetrieveTransactionHistoryRequest     request;
    private RetrieveTransactionHistoryBodyRequest body;
    private RetrieveTransactionHistoryResponse    response;
    private Response                              baseResponse;
    private String                                json;
    private Gson                                  gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendAdapterService();
        request = new RetrieveTransactionHistoryRequest();
        body = new RetrieveTransactionHistoryBodyRequest();
        body.setItemsLimit(3);
        body.setPageOffset(4);
        body.setRefuelPostpaid(true);
        body.setRefuelPrepaid(true);
        body.setShop(true);
        CustomDate startDate = new CustomDate();
        startDate.setDay(1);
        startDate.setMonth(1);
        startDate.setYear(1990);
        body.setStartDate(startDate);
        CustomDate endDate = new CustomDate();
        endDate.setDay(1);
        endDate.setMonth(1);
        endDate.setYear(1990);
        body.setEndDate(endDate);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setUserService(new MockUserService());

    }

    @Test
    public void testRetrieveTransactionHistoryPoPSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrieveTransactionHistoryPostpaidServiceSuccess());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrieveTransactionHistory(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrieveTransactionHistoryResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIEVE_TRANSACTION_HISTORY_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveTransactionHistoryPoPFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setPostPaidTransactionService(new MockRetrieveTransactionHistoryPostpaidServiceFailure());
        PoPRequest popRequest = new PoPRequest();
        popRequest.setRetrieveTransactionHistory(request);
        json = gson.toJson(popRequest, PoPRequest.class);
        baseResponse = frontend.popJsonHandler(json);
        response = (RetrieveTransactionHistoryResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIEVE_TRANSACTION_HISTORY_FAILURE, response.getStatus().getStatusCode());
    }

}
