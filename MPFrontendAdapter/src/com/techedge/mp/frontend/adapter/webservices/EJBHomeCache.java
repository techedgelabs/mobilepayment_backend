package com.techedge.mp.frontend.adapter.webservices;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.techedge.mp.bpel.operation.refuel.business.BPELServiceRemote;
import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.EventNotificationServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ManagerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.ParkingTransactionV2ServiceRemote;
import com.techedge.mp.core.business.PostPaidTransactionServiceRemote;
import com.techedge.mp.core.business.PostPaidV2TransactionServiceRemote;
import com.techedge.mp.core.business.SurveyServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.TransactionV2ServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.UserV2ServiceRemote;
import com.techedge.mp.core.business.VoucherTransactionServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;

public class EJBHomeCache {

    final String                                 userServiceRemoteJndi                  = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/UserService!com.techedge.mp.core.business.UserServiceRemote";
    final String                                 transactionServiceRemoteJndi           = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/TransactionService!com.techedge.mp.core.business.TransactionServiceRemote";
    final String                                 transactionV2ServiceRemoteJndi         = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/TransactionV2Service!com.techedge.mp.core.business.TransactionV2ServiceRemote";
    final String                                 parametersServiceRemoteJndi            = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
    final String                                 loggerServiceRemoteJndi                = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
    final String                                 bpelServiceRemoteJndi                  = "java:global/MPBPELOperationRefuelEAR/MPBPELOperationRefuelEJB/BPELService!com.techedge.mp.bpel.operation.refuel.business.BPELServiceRemote";
    final String                                 forecourtInfoServiceRemoteJndi         = "java:global/MPForecourtAdapterEAR/MPForecourtAdapterEJB/ForecourtInfoService!com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote";
    final String                                 postPaidTransactionServiceRemoteJndi   = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/PostPaidTransactionService!com.techedge.mp.core.business.PostPaidTransactionServiceRemote";
    final String                                 managerServiceRemoteJndi               = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ManagerService!com.techedge.mp.core.business.ManagerServiceRemote";
    final String                                 surveyServiceRemoteJndi                = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/SurveyService!com.techedge.mp.core.business.SurveyServiceRemote";
    final String                                 voucherServiceRemoteJndi               = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/VoucherTransactionService!com.techedge.mp.core.business.VoucherTransactionServiceRemote";
    final String                                 userV2ServiceRemoteJndi                = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/UserV2Service!com.techedge.mp.core.business.UserV2ServiceRemote";
    final String                                 eventNotificationServiceRemoteJndi     = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/EventNotificationService!com.techedge.mp.core.business.EventNotificationServiceRemote";
    final String                                 crmServiceRemoteJndi                   = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/CRMService!com.techedge.mp.core.business.CRMServiceRemote";
    final String                                 postPaidV2TransactionServiceRemoteJndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/PostPaidV2TransactionService!com.techedge.mp.core.business.PostPaidV2TransactionServiceRemote";
    final String                                 parkingTransactionV2ServiceRemoteJndi  = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParkingTransactionV2Service!com.techedge.mp.core.business.ParkingTransactionV2ServiceRemote";

    private static EJBHomeCache                  instance;

    protected Context                            context                                = null;

    protected UserServiceRemote                  userService                            = null;
    protected TransactionServiceRemote           transactionService                     = null;
    protected TransactionV2ServiceRemote         transactionV2Service                   = null;
    protected ParametersServiceRemote            parametersService                      = null;
    protected LoggerServiceRemote                loggerService                          = null;
    protected BPELServiceRemote                  bpelService                            = null;
    protected ForecourtInfoServiceRemote         forecourtInfoService                   = null;
    protected PostPaidTransactionServiceRemote   postPaidTransactionService             = null;
    protected ManagerServiceRemote               managerService                         = null;
    protected SurveyServiceRemote                surveyService                          = null;
    protected VoucherTransactionServiceRemote    voucherService                         = null;
    protected UserV2ServiceRemote                userV2Service                          = null;
    protected EventNotificationServiceRemote     eventNotificationService               = null;
    protected CRMServiceRemote                   crmService                             = null;
    protected PostPaidV2TransactionServiceRemote postPaidV2TransactionService           = null;
    protected ParkingTransactionV2ServiceRemote  parkingTransactionV2Service            = null;

    private EJBHomeCache() throws InterfaceNotFoundException {

        final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();

        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

        try {

            context = new InitialContext(jndiProperties);
            userService = (UserServiceRemote) context.lookup(userServiceRemoteJndi);
            transactionService = (TransactionServiceRemote) context.lookup(transactionServiceRemoteJndi);
            transactionV2Service = (TransactionV2ServiceRemote) context.lookup(transactionV2ServiceRemoteJndi);
            parametersService = (ParametersServiceRemote) context.lookup(parametersServiceRemoteJndi);
            loggerService = (LoggerServiceRemote) context.lookup(loggerServiceRemoteJndi);
            bpelService = (BPELServiceRemote) context.lookup(bpelServiceRemoteJndi);
            forecourtInfoService = (ForecourtInfoServiceRemote) context.lookup(forecourtInfoServiceRemoteJndi);
            postPaidTransactionService = (PostPaidTransactionServiceRemote) context.lookup(postPaidTransactionServiceRemoteJndi);
            managerService = (ManagerServiceRemote) context.lookup(managerServiceRemoteJndi);
            surveyService = (SurveyServiceRemote) context.lookup(surveyServiceRemoteJndi);
            voucherService = (VoucherTransactionServiceRemote) context.lookup(voucherServiceRemoteJndi);
            userV2Service = (UserV2ServiceRemote) context.lookup(userV2ServiceRemoteJndi);
            eventNotificationService = (EventNotificationServiceRemote) context.lookup(eventNotificationServiceRemoteJndi);
            crmService = (CRMServiceRemote) context.lookup(crmServiceRemoteJndi);
            postPaidV2TransactionService = (PostPaidV2TransactionServiceRemote) context.lookup(postPaidV2TransactionServiceRemoteJndi);
            parkingTransactionV2Service = (ParkingTransactionV2ServiceRemote) context.lookup(parkingTransactionV2ServiceRemoteJndi);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw new InterfaceNotFoundException(ex.getMessage());
        }

    }

    public static synchronized EJBHomeCache getInstance() throws InterfaceNotFoundException {
        if (instance == null)
            instance = new EJBHomeCache();

        return instance;
    }

    public UserServiceRemote getUserService() {
        return userService;
    }

    public TransactionServiceRemote getTransactionService() {
        return transactionService;
    }

    public ParametersServiceRemote getParametersService() {
        return parametersService;
    }

    public LoggerServiceRemote getLoggerService() {
        return loggerService;
    }

    public BPELServiceRemote getBpelService() {
        return bpelService;
    }

    public ForecourtInfoServiceRemote getForecourtInfoService() {
        return forecourtInfoService;
    }

    public PostPaidTransactionServiceRemote getPostPaidTransactionService() {
        return postPaidTransactionService;
    }

    public ManagerServiceRemote getManagerService() {
        return managerService;
    }

    public SurveyServiceRemote getSurveyService() {
        return surveyService;
    }

    public void setUserService(UserServiceRemote userService) {
        this.userService = userService;
    }

    public void setLoggerService(LoggerServiceRemote loggerService) {
        this.loggerService = loggerService;
    }

    public void setTransactionService(TransactionServiceRemote transactionService) {
        this.transactionService = transactionService;
    }

    public void setParametersService(ParametersServiceRemote parametersService) {
        this.parametersService = parametersService;
    }

    public void setManagerService(ManagerServiceRemote managerService) {
        this.managerService = managerService;
    }

    public void setSurveyService(SurveyServiceRemote surveyService) {
        this.surveyService = surveyService;
    }

    public void setBpelService(BPELServiceRemote bpelService) {
        this.bpelService = bpelService;
    }

    public void setPostPaidTransactionService(PostPaidTransactionServiceRemote postPaidTransactionService) {
        this.postPaidTransactionService = postPaidTransactionService;
    }

    public VoucherTransactionServiceRemote getVoucherService() {
        return voucherService;
    }

    public void setVoucherService(VoucherTransactionServiceRemote voucherService) {
        this.voucherService = voucherService;
    }

    public UserV2ServiceRemote getUserV2Service() {
        return userV2Service;
    }

    public EventNotificationServiceRemote getEventNotificationService() {
        return eventNotificationService;
    }

    public CRMServiceRemote getCRMService() {
        return crmService;
    }

    public void setForecourtInfoService(ForecourtInfoServiceRemote forecourtInfoService) {
        this.forecourtInfoService = forecourtInfoService;
    }

    public TransactionV2ServiceRemote getTransactionV2Service() {
        return transactionV2Service;
    }

    public void setTransactionV2Service(TransactionV2ServiceRemote transactionV2Service) {
        this.transactionV2Service = transactionV2Service;
    }

    public PostPaidV2TransactionServiceRemote getPostPaidV2TransactionService() {
        return postPaidV2TransactionService;
    }

    public ParkingTransactionV2ServiceRemote getParkingTransactionV2Service() {
        return parkingTransactionV2Service;
    }

}
