package com.techedge.mp.frontend.adapter.webservices;

import java.util.Properties;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class AbstractRequestUtility {

    public static Gson       gson = new Gson();
    public static String     json;

    @SuppressWarnings("rawtypes")
    public static Response abstractRequestInitialize(Properties prop, AbstractRequest abstractRequest, LoggerServiceRemote loggerService, Class frontendEndpoint, 
            String stringRequest, String methodLogger, String requestName) {
        Status status = null;

        try {

            if (abstractRequest == null) {
                loggerService.log(ErrorLevel.INFO, frontendEndpoint.getSimpleName(), methodLogger, requestName, "opening", stringRequest);

                BaseResponse baseResponse = new BaseResponse();

                Status statusResponse = new Status();
                statusResponse.setStatusCode(StatusCode.ABSTRACT_REQUEST_ERROR);
                statusResponse.setStatusMessage(prop.getProperty(StatusCode.ABSTRACT_REQUEST_ERROR));
                baseResponse.setStatus(statusResponse);
                json = gson.toJson(baseResponse);

                loggerService.log(ErrorLevel.INFO, frontendEndpoint.getSimpleName(), methodLogger, requestName, "closing", json.toString());

                return Response.status(200).entity(baseResponse).build();

            }
            abstractRequest.setProp(prop);
            status = abstractRequest.check();

            loggerService.log(ErrorLevel.INFO, frontendEndpoint.getSimpleName(), methodLogger, requestName, "opening", stringRequest);

            if (Validator.isValid(status.getStatusCode())) {

                BaseResponse response = abstractRequest.execute();

                json = gson.toJson(response);
                
                if (
                        requestName.equals("retrieveDocument") ||
                        requestName.equals("getCities") ||
                        requestName.equals("retrieveParkingZones") ||
                        requestName.equals("retrieveParkingZonesByCity") ||
                        requestName.equals("getHomePartnerList") ||
                        requestName.equals("getPartnerList") ||
                        requestName.equals("getAwardList") ||
                        requestName.equals("getReceipt")
                   ) {
                    if (json.length() >= 255) {
                        json = json.substring(0, 255) + "...";
                    }
                }

                loggerService.log(ErrorLevel.INFO, frontendEndpoint.getSimpleName(), methodLogger, requestName, "closing", json.toString());

                return Response.status(200).entity(response).build();
            }

        }
        catch (Exception e) {
            e.printStackTrace();
 
            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);
            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, frontendEndpoint.getSimpleName(), methodLogger, requestName, "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();

        }
        
        BaseResponse baseResponse = new BaseResponse();

        Status statusResponse = new Status();
        statusResponse.setStatusCode(status.getStatusCode());
        statusResponse.setStatusMessage(prop.getProperty(status.getStatusCode()));
        baseResponse.setStatus(statusResponse);

        json = gson.toJson(baseResponse);
        loggerService.log(ErrorLevel.INFO, frontendEndpoint.getSimpleName(), methodLogger, requestName, "closing", json.toString());

        return Response.status(200).entity(baseResponse).build();
    }
}
