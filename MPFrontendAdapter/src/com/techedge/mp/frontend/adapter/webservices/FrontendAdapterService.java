package com.techedge.mp.frontend.adapter.webservices;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.ejb.EJBException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("")
public class FrontendAdapterService {

    private Properties prop = new Properties();

    public FrontendAdapterService() throws EJBException {
        loadFileProperty();
    }

    private void loadFileProperty() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("response_service.properties");
        try {
            prop.load(inputStream);
            if (inputStream == null) {
                throw new FileNotFoundException("property file response_service.properties not found in the classpath");
            }
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @POST()
    @Path("/UserJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response userJsonHandler(String stringRequest) {

        return UserJsonFrontendAdapterService.userJsonHandler(stringRequest, prop);

    }

    @POST()
    @Path("/RefuelJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response refuelJsonHandler(String stringRequest) {

        return RefuelJsonFrontendAdapterService.refuelJsonHandler(stringRequest, prop);

    }

    @POST()
    @Path("/SystemJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response systemJsonHandler(String stringRequest) {
        return SystemJsonFrontendAdapterService.systemJsonHandler(stringRequest, prop);

    }

    @POST()
    @Path("/PoPJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response popJsonHandler(String stringRequest) {
        return PopJsonFrontendAdapterService.popJsonHandler(stringRequest, prop);
    }

    @POST()
    @Path("/ManagerJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response managerJsonHandler(String stringRequest) {
        return ManagerJsonFrontendAdapterService.managerJsonHandler(stringRequest, prop);
    }

    @POST()
    @Path("/SurveyJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response surveyJsonHandler(String stringRequest) {
        return SurveyJsonFrontendAdapterService.surveyJsonHandler(stringRequest, prop);
    }

    @POST()
    @Path("/VoucherJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response voucherJsonHandler(String stringRequest) {

        return VoucherJsonFrontendAdapterService.voucherJsonHandler(stringRequest, prop);

    }

    @POST()
    @Path("/v2/UserJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response userV2JsonHandler(String stringRequest) {

        return UserV2JsonFrontendAdapterService.userV2JsonHandler(stringRequest, prop);
    }
    
    @POST()
    @Path("/v2/RefuelJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response refuelV2JsonHandler(String stringRequest) {

        return RefuelV2JsonFrontendAdapterService.refuelV2JsonHandler(stringRequest, prop);
    }

    @POST()
    @Path("/v2/PoPJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response popV2JsonHandler(String stringRequest) {

        return PopV2JsonFrontendAdapterService.popV2JsonHandler(stringRequest, prop);
    }

    @POST()
    @Path("/EventNotificationJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eventNotificationJsonHandler(String stringRequest) {

        return EventNotificationJsonFrontendAdapterService.eventNotificationJsonHandler(stringRequest, prop);

    }
    
    @POST()
    @Path("/v2/ParkingJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response parkingV2JsonHandler(String stringRequest) {

        return ParkingV2JsonFrontendAdapterService.parkingV2JsonHandler(stringRequest, prop);
    }

    @POST()
    @Path("/business/UserJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response userBusinessJsonHandler(String stringRequest) {

        return UserBusinessJsonFrontendAdapterService.userBusinessJsonHandler(stringRequest, prop);
    }

    @POST()
    @Path("/business/RefuelJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response refuelBusinessJsonHandler(String stringRequest) {

        return RefuelBusinessJsonFrontendAdapterService.refuelBusinessJsonHandler(stringRequest, prop);
    }

    @POST()
    @Path("/business/PoPJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response popBusinessJsonHandler(String stringRequest) {

        return PopBusinessJsonFrontendAdapterService.popBusinessJsonHandler(stringRequest, prop);
    }
    
    @POST()
    @Path("/business/SystemJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response systemBusinessJsonHandler(String stringRequest) {
        return SystemBusinessJsonFrontendAdapterService.systemBusinessJsonHandler(stringRequest, prop);

    }
    
}