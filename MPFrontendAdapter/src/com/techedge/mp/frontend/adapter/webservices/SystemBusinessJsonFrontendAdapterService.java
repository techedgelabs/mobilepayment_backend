package com.techedge.mp.frontend.adapter.webservices;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.frontend.adapter.entities.common.AmountInfo;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.FrontendParameter;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.v2.AppLinkInfo;
import com.techedge.mp.frontend.adapter.entities.system.business.SystemBusinessRequest;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsApplicationBodyResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsBodyResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsDinamycPageBodyResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsFirstStartBodyResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsHowToBodyResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsRefuelBodyResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsRegistrationBodyResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsRegistrationDepositCardBodyResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsRequest;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsSecurityBodyResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsStaticPageBodyResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsStationBodyResponse;
import com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings.RetrieveApplicationSettingsSupportBodyResponse;

public class SystemBusinessJsonFrontendAdapterService {

    public static Gson       gson = new Gson();
    public static String     json;

    public static Response systemBusinessJsonHandler(String stringRequest, Properties prop) {

        LoggerServiceRemote loggerService = null;

        try {
            loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.RETRIEVE_APPLICATION_SETTINGS_SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.RETRIEVE_APPLICATION_SETTINGS_SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }

        loggerService.log(ErrorLevel.INFO, SystemBusinessJsonFrontendAdapterService.class.getClass().getSimpleName(), "systemBusinessJsonHandler", null, "opening", stringRequest);

        Status status = null;

        ParametersServiceRemote parametersService = null;

        try {
            parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            status = new Status();
            status.setStatusCode(StatusCode.RETRIEVE_APPLICATION_SETTINGS_SYSTEM_ERROR);
            status.setStatusMessage(prop.getProperty(status.getStatusCode()));

            return Response.status(200).entity(status).build();
        }

        String depositCardAnd = null;
        String depositCardiOS = null;
        String depositCardWP = null;
        String minVersionAnd = null;
        String minVersioniOS = null;
        String minVersionWP = null;
        String appIdAnd = null;
        String appIdiOS = null;
        String appIdWP = null;
        String howtoBaseUrl = null;
        Integer howtoPageCount = null;
        Integer transactionMinAmount = null;
        Integer transactionMaxAmount = null;
        Integer transactionAmount1 = null;
        Integer transactionAmount2 = null;
        Integer transactionAmount3 = null;
        Integer transactionAmount4 = null;
        Integer transactionAmount5 = null;
        Integer transactionAmount6 = null;
        Integer transactionAmountFull = null;
        String gpsEnabled = null;
        String robotUrl = null;
        String callCenter = null;
        String callCenterReal = null;
        String callCenterText = null;
        String faqUrl = null;
        String captchaUrl = null;
        /*
        String landingHomeUrl = null;
        String landingRegistrationUrl = null;
        String landingHomeTitle = null;
        String landingRegistrationTitle = null;
        String tutorial = null;
        */
        String firstStartText = null;
        String newAppAvailableUrl = null;
        String publicKey = null;
        String redemptionUrl = null;
        String infoTokenUrl = null;
        String stationFinderUrl = null; 
        String firstStartUrl = null;
        String businessInfoUrl = null;
        
        String captchaPublicKeyAndroid = null;
        String captchaPublicKeyIOS     = null;

        try {
            depositCardAnd = parametersService.getParamValue(FrontendParameter.PARAM_DEPOSIT_CARD_URL_AND);
            depositCardiOS = parametersService.getParamValue(FrontendParameter.PARAM_DEPOSIT_CARD_URL_IOS);
            depositCardWP = parametersService.getParamValue(FrontendParameter.PARAM_DEPOSIT_CARD_URL_WP);
            minVersionAnd = parametersService.getParamValue(FrontendParameter.PARAM_MIN_VERSION_ANDROID_BUSINESS);
            minVersioniOS = parametersService.getParamValue(FrontendParameter.PARAM_MIN_VERSION_IOS_BUSINESS);
            minVersionWP = parametersService.getParamValue(FrontendParameter.PARAM_MIN_VERSION_WP_BUSINESS);
            appIdAnd = parametersService.getParamValue(FrontendParameter.PARAM_APP_ID_ANDROID_BUSINESS);
            appIdiOS = parametersService.getParamValue(FrontendParameter.PARAM_APP_ID_IOS_BUSINESS);
            appIdWP = parametersService.getParamValue(FrontendParameter.PARAM_APP_ID_WP_BUSINESS);
            howtoBaseUrl = parametersService.getParamValue(FrontendParameter.PARAM_HOWTO_BASEURL);
            howtoPageCount = Integer.valueOf(parametersService.getParamValue(FrontendParameter.PARAM_HOWTO_PAGECOUNT));
            transactionMinAmount = Integer.valueOf(parametersService.getParamValue(FrontendParameter.PARAM_TRANSACTION_MIN_AMOUNT));
            transactionMaxAmount = Integer.valueOf(parametersService.getParamValue(FrontendParameter.PARAM_TRANSACTION_MAX_AMOUNT));
            transactionAmount1 = Integer.valueOf(parametersService.getParamValue(FrontendParameter.PARAM_TRANSACTION_AMOUNT_1));
            transactionAmount2 = Integer.valueOf(parametersService.getParamValue(FrontendParameter.PARAM_TRANSACTION_AMOUNT_2));
            transactionAmount3 = Integer.valueOf(parametersService.getParamValue(FrontendParameter.PARAM_TRANSACTION_AMOUNT_3));
            transactionAmount4 = Integer.valueOf(parametersService.getParamValue(FrontendParameter.PARAM_TRANSACTION_AMOUNT_4));
            transactionAmount5 = Integer.valueOf(parametersService.getParamValue(FrontendParameter.PARAM_TRANSACTION_AMOUNT_5));
            transactionAmount6 = Integer.valueOf(parametersService.getParamValue(FrontendParameter.PARAM_TRANSACTION_AMOUNT_6));
            transactionAmountFull = Integer.valueOf(parametersService.getParamValue(FrontendParameter.PARAM_TRANSACTION_AMOUNT_FULL));
            gpsEnabled = parametersService.getParamValue(FrontendParameter.PARAM_GPS_ENABLED);
            robotUrl = parametersService.getParamValue(FrontendParameter.PARAM_ROBOT_URL);
            callCenter = parametersService.getParamValue(FrontendParameter.PARAM_CALL_CENTER);
            callCenterReal = parametersService.getParamValue(FrontendParameter.PARAM_CALL_CENTER_REAL);
            callCenterText = parametersService.getParamValue(FrontendParameter.PARAM_CALL_CENTER_TEXT);
            faqUrl = parametersService.getParamValue(FrontendParameter.PARAM_FAQ_BUSINESS_URL);
            captchaUrl = parametersService.getParamValue(FrontendParameter.PARAM_CAPTCHA_URL);
            firstStartText = parametersService.getParamValue(FrontendParameter.PARAM_FIRST_START_TEXT);
            newAppAvailableUrl = parametersService.getParamValue(FrontendParameter.PARAM_NEW_APP_AVAILABLE_URL);
            redemptionUrl = parametersService.getParamValue(FrontendParameter.PARAM_REDEMPTION_URL);
            infoTokenUrl = parametersService.getParamValue(FrontendParameter.PARAM_INFO_TOKEN_URL);
            stationFinderUrl = parametersService.getParamValue(FrontendParameter.PARAM_STATION_FINDER_URL);
            firstStartUrl = parametersService.getParamValue(FrontendParameter.PARAM_FIRST_START_URL_BUSINESS);
            businessInfoUrl = parametersService.getParamValue(FrontendParameter.PARAM_BUSINESS_INFO_URL_BUSINESS);
            
            captchaPublicKeyAndroid = parametersService.getParamValue(FrontendParameter.PARAM_CAPTCHA_PUBLIC_KEY_ANDROID);
            captchaPublicKeyIOS     = parametersService.getParamValue(FrontendParameter.PARAM_CAPTCHA_PUBLIC_KEY_IOS);

        }
        catch (ParameterNotFoundException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.RETRIEVE_SYSTEM_PARAMETERS_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.RETRIEVE_SYSTEM_PARAMETERS_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, SystemBusinessJsonFrontendAdapterService.class.getClass().getSimpleName(), "systemBusinessJsonHandler", null, "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        try {
            //File filePublicPEM = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "security" + File.separator + "frontend.public.pem");
            File filePublicPEM = new File(parametersService.getParamValue(FrontendParameter.PARAM_SECURITY_RSA_PUBLIC_PEM_PATH));
            FileInputStream input = new FileInputStream(filePublicPEM);
            byte[] keyBytes = new byte[input.available()];
            input.read(keyBytes);
            input.close();
            publicKey = new String(keyBytes, "UTF-8").replaceAll("(-+BEGIN PUBLIC KEY-+\\r?\\n|-+END PUBLIC KEY-+\\r?\\n?)", "");

        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, SystemBusinessJsonFrontendAdapterService.class.getClass().getSimpleName(), "systemBusinessJsonHandler", null, null,
                    "Error reading RSA public file PEM: " + ex.getMessage());
        }

        SystemBusinessRequest request = null;

        try {
            request = gson.fromJson(stringRequest, SystemBusinessRequest.class);
        }
        catch (JsonSyntaxException jsonEx) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.USER_REQU_JSON_SYNTAX_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.USER_REQU_JSON_SYNTAX_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, SystemBusinessJsonFrontendAdapterService.class.getClass().getSimpleName(), "systemBusinessJsonHandler", null, "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();

        }

        status = request.check();

        if (Validator.isValid(status.getStatusCode())) {

            if (request.getRetrieveApplicationSettings() != null) {

                RetrieveApplicationSettingsRequest retrieveApplicationSettingsRequest = request.getRetrieveApplicationSettings();

                RetrieveApplicationSettingsResponse retrieveApplicationSettingsResponse = new RetrieveApplicationSettingsResponse();

                RetrieveApplicationSettingsBodyResponse retrieveApplicationSettingsBodyResponse = new RetrieveApplicationSettingsBodyResponse();

                String appVersion = retrieveApplicationSettingsRequest.getCredential().getAppVersion();
                String requestId = retrieveApplicationSettingsRequest.getCredential().getRequestID();

                // Calcolo il tipo di dispositivo utilizzato dall'utente
                String deviceType = null;
                if (requestId.startsWith("AND")) {
                    deviceType = "ANDROID";
                }
                else if (requestId.startsWith("IPH")) {
                    deviceType = "IOS";
                }

                /*
                if (deviceType == null) {
                    System.out.println("DeviceData non risocnosciuto - WEB");
                }
                else {
                    System.out.println("DeviceData risocnosciuto - " + deviceType);
                }
                */

                if (deviceType != null) {

                    // Se la versione � inferiore a quella minima, bisogna nascondere le url delle pagine welcome, registration e deposit
                    String minVersion = null;

                    if (deviceType.equals("ANDROID")) {
                        minVersion = minVersionAnd;
                    }

                    if (deviceType.equals("IOS")) {
                        minVersion = minVersioniOS;
                    }

                    //System.out.println("DeviceData version: " + appVersion + ", min version: " + minVersion);

                    Version app = new Version(appVersion);
                    Version min = new Version(minVersion);

                    if (app.compareTo(min) == -1) {

                        System.out.println("La versione del device utilizzato � inferiore a quella minima");

                        depositCardAnd = newAppAvailableUrl;
                        depositCardiOS = newAppAvailableUrl;
                        depositCardWP = newAppAvailableUrl;
                        //landingHomeUrl = newAppAvailableUrl;
                        //landingRegistrationUrl = newAppAvailableUrl;
                    }

                }

                // vecchie informazioni per retrocompatibilit�
                retrieveApplicationSettingsBodyResponse.setDepositCardAnd(depositCardAnd);
                retrieveApplicationSettingsBodyResponse.setDepositCardiOS(depositCardiOS);
                retrieveApplicationSettingsBodyResponse.setDepositCardWP(depositCardWP);

                // Url per avvio Robot per rifornimento
                retrieveApplicationSettingsBodyResponse.setRobotUrl(robotUrl);

                retrieveApplicationSettingsBodyResponse.setMinAmount(transactionMinAmount);
                retrieveApplicationSettingsBodyResponse.setMaxAmount(transactionMaxAmount);

                // AmountInfo 1
                AmountInfo amountInfo_1 = new AmountInfo();
                amountInfo_1.setAmount(transactionAmount1);
                amountInfo_1.setType("default_amount");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(amountInfo_1);

                // AmountInfo 2
                AmountInfo amountInfo_2 = new AmountInfo();
                amountInfo_2.setAmount(transactionAmount2);
                amountInfo_2.setType("default_amount");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(amountInfo_2);

                // AmountInfo 3
                AmountInfo amountInfo_3 = new AmountInfo();
                amountInfo_3.setAmount(transactionAmount3);
                amountInfo_3.setType("default_amount");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(amountInfo_3);

                // AmountInfo 4
                AmountInfo amountInfo_4 = new AmountInfo();
                amountInfo_4.setAmount(transactionAmount4);
                amountInfo_4.setType("default_amount");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(amountInfo_4);

                // AmountInfo 5
                AmountInfo amountInfo_5 = new AmountInfo();
                amountInfo_5.setAmount(transactionAmount5);
                amountInfo_5.setType("default_amount");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(amountInfo_5);
                
                // AmountInfo 5
                AmountInfo amountInfo_6 = new AmountInfo();
                amountInfo_6.setAmount(transactionAmount6);
                amountInfo_6.setType("default_amount");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(amountInfo_6);

                // AmountInfo Full
                AmountInfo amountInfoFull = new AmountInfo();
                amountInfoFull.setAmount(transactionAmountFull);
                amountInfoFull.setType("amount_full");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(amountInfoFull);

                // VoucherAmountInfo 1
                AmountInfo voucherAmountInfo_1 = new AmountInfo();
                voucherAmountInfo_1.setAmount(500);
                voucherAmountInfo_1.setType("default_amount");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(voucherAmountInfo_1);

                // VoucherAmountInfo 2
                AmountInfo voucherAmountInfo_2 = new AmountInfo();
                voucherAmountInfo_2.setAmount(1000);
                voucherAmountInfo_2.setType("default_amount");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(voucherAmountInfo_2);

                // VoucherAmountInfo 3
                AmountInfo voucherAmountInfo_3 = new AmountInfo();
                voucherAmountInfo_3.setAmount(2000);
                voucherAmountInfo_3.setType("default_amount");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(voucherAmountInfo_3);

                // VoucherAmountInfo 4
                AmountInfo voucherAmountInfo_4 = new AmountInfo();
                voucherAmountInfo_4.setAmount(3000);
                voucherAmountInfo_4.setType("default_amount");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(voucherAmountInfo_4);

                // VoucherAmountInfo 5
                AmountInfo voucherAmountInfo_5 = new AmountInfo();
                voucherAmountInfo_5.setAmount(5000);
                voucherAmountInfo_5.setType("default_amount");
                retrieveApplicationSettingsBodyResponse.getDefaultAmount().add(voucherAmountInfo_5);

                // application
                RetrieveApplicationSettingsApplicationBodyResponse application = new RetrieveApplicationSettingsApplicationBodyResponse();
                application.setMinVersionAnd(minVersionAnd);
                application.setMinVersioniOS(minVersioniOS);
                application.setMinVersionWP(minVersionWP);
                application.setAppIdAnd(appIdAnd);
                application.setAppIdiOS(appIdiOS);
                application.setAppIdWP(appIdWP);
                retrieveApplicationSettingsBodyResponse.setApplication(application);

                // registration
                RetrieveApplicationSettingsRegistrationBodyResponse registration = new RetrieveApplicationSettingsRegistrationBodyResponse();
                RetrieveApplicationSettingsRegistrationDepositCardBodyResponse depositCard = new RetrieveApplicationSettingsRegistrationDepositCardBodyResponse();
                depositCard.setDepositCardAnd(depositCardAnd);
                depositCard.setDepositCardiOS(depositCardiOS);
                depositCard.setDepositCardWP(depositCardWP);
                registration.setDepositCard(depositCard);
                retrieveApplicationSettingsBodyResponse.setRegistration(registration);

                // howTo
                RetrieveApplicationSettingsHowToBodyResponse howTo = new RetrieveApplicationSettingsHowToBodyResponse();
                howTo.setBaseUrl(howtoBaseUrl);
                howTo.setPageCount(howtoPageCount);
                retrieveApplicationSettingsBodyResponse.setHowTo(howTo);

                // refuel
                RetrieveApplicationSettingsRefuelBodyResponse refuel = new RetrieveApplicationSettingsRefuelBodyResponse();
                refuel.setMinAmount(transactionMinAmount);
                refuel.setMaxAmount(transactionMaxAmount);
                refuel.getDefaultAmount().add(amountInfo_1);
                refuel.getDefaultAmount().add(amountInfo_2);
                refuel.getDefaultAmount().add(amountInfo_3);
                refuel.getDefaultAmount().add(amountInfo_4);
                refuel.getDefaultAmount().add(amountInfo_5);
                refuel.getDefaultAmount().add(amountInfo_6);
                refuel.getDefaultAmount().add(amountInfoFull);
                retrieveApplicationSettingsBodyResponse.setRefuel(refuel);

                // refuelNewFlow - da settare
                RetrieveApplicationSettingsRefuelBodyResponse refuelNewFlow = new RetrieveApplicationSettingsRefuelBodyResponse();
                refuelNewFlow.setMinAmount(transactionMinAmount);
                refuelNewFlow.setMaxAmount(transactionMaxAmount);
                refuelNewFlow.getDefaultAmount().add(amountInfo_1);
                refuelNewFlow.getDefaultAmount().add(amountInfo_2);
                refuelNewFlow.getDefaultAmount().add(amountInfo_3);
                refuelNewFlow.getDefaultAmount().add(amountInfo_4);
                refuelNewFlow.getDefaultAmount().add(amountInfo_5);
                refuelNewFlow.getDefaultAmount().add(amountInfo_6);
                refuelNewFlow.getDefaultAmount().add(amountInfoFull);
                retrieveApplicationSettingsBodyResponse.setRefuelNewFlow(refuelNewFlow);

                // station
                RetrieveApplicationSettingsStationBodyResponse station = new RetrieveApplicationSettingsStationBodyResponse();
                station.setGpsEnabled(gpsEnabled);
                retrieveApplicationSettingsBodyResponse.setStation(station);

                // support
                RetrieveApplicationSettingsSupportBodyResponse support = new RetrieveApplicationSettingsSupportBodyResponse();
                support.setCallCenter(callCenter);
                support.setCallCenterReal(callCenterReal);
                support.setCallCenterText(callCenterText);
                retrieveApplicationSettingsBodyResponse.setSupport(support);

                // static pages
                RetrieveApplicationSettingsStaticPageBodyResponse staticPage = new RetrieveApplicationSettingsStaticPageBodyResponse();
                staticPage.setFaq(faqUrl);
                /*
                staticPage.setLandingHome(landingHomeUrl);
                staticPage.setLandingRegistration(landingRegistrationUrl);
                staticPage.setLandingHomeTitle(landingHomeTitle);
                staticPage.setLandingRegistrationTitle(landingRegistrationTitle);
                staticPage.setTutorial(tutorial);
                */
                staticPage.setRedemptionUrl(redemptionUrl);
                staticPage.setInfoTokenUrl(infoTokenUrl);
                staticPage.setStationFinderUrl(stationFinderUrl);
                staticPage.setFirstStartUrl(firstStartUrl);
                staticPage.setCaptchaUrl(captchaUrl);
                retrieveApplicationSettingsBodyResponse.setStaticPage(staticPage);
                
                // dinamyc pages
                RetrieveApplicationSettingsDinamycPageBodyResponse dinamycPage = new RetrieveApplicationSettingsDinamycPageBodyResponse();
                
                AppLinkInfo dinamycFaqUrl           = null;
                AppLinkInfo dinamycRedemptionUrl    = null;
                AppLinkInfo dinamycInfoTokenUrl     = null;
                AppLinkInfo dinamycStationFinderUrl = null;
                AppLinkInfo dinamycFirstStartUrl    = null;
                AppLinkInfo dinamycBusinessInfoUrl  = null;
                
                String faqTitle                 = "";
                String redemptionTitle          = "";
                String infoTokenTitle           = "Come usare il codice?";
                String stationFinderUrlTitle    = "";
                String firstStartUrlTitle       = "";
                String businessInfoUrlTitle     = "";
                
                if (faqUrl != null && !faqUrl.isEmpty()) {
                    dinamycFaqUrl = new AppLinkInfo();
                    dinamycFaqUrl.setTitle(faqTitle);
                    dinamycFaqUrl.setLocation(faqUrl);
                    dinamycFaqUrl.setType(PushNotificationContentType.INTERNAL);
                }
                
                if (redemptionUrl != null && !redemptionUrl.isEmpty()) {
                    dinamycRedemptionUrl = new AppLinkInfo();
                    dinamycRedemptionUrl.setTitle(redemptionTitle);
                    dinamycRedemptionUrl.setLocation(redemptionUrl);
                    dinamycRedemptionUrl.setType(PushNotificationContentType.INTERNAL);
                }
                
                if (infoTokenUrl != null && !infoTokenUrl.isEmpty()) {
                    dinamycInfoTokenUrl = new AppLinkInfo();
                    dinamycInfoTokenUrl.setTitle(infoTokenTitle);
                    dinamycInfoTokenUrl.setLocation(infoTokenUrl);
                    dinamycInfoTokenUrl.setType(PushNotificationContentType.INTERNAL);
                }
                
                if (stationFinderUrl != null && !stationFinderUrl.isEmpty()) {
                    dinamycStationFinderUrl = new AppLinkInfo();
                    dinamycStationFinderUrl.setTitle(stationFinderUrlTitle);
                    dinamycStationFinderUrl.setLocation(stationFinderUrl);
                    dinamycStationFinderUrl.setType(PushNotificationContentType.INTERNAL);
                }
                
                if (firstStartUrl != null && !firstStartUrl.isEmpty()) {
                    dinamycFirstStartUrl = new AppLinkInfo();
                    dinamycFirstStartUrl.setTitle(firstStartUrlTitle);
                    dinamycFirstStartUrl.setLocation(firstStartUrl);
                    dinamycFirstStartUrl.setType(PushNotificationContentType.INTERNAL);
                }
                
                if (businessInfoUrl != null && !businessInfoUrl.isEmpty()) {
                    dinamycBusinessInfoUrl = new AppLinkInfo();
                    dinamycBusinessInfoUrl.setTitle(businessInfoUrlTitle);
                    dinamycBusinessInfoUrl.setLocation(businessInfoUrl);
                    dinamycBusinessInfoUrl.setType(PushNotificationContentType.INTERNAL);
                }
                
                dinamycPage.setFaq(dinamycFaqUrl);
                dinamycPage.setRedemptionUrl(dinamycRedemptionUrl);
                dinamycPage.setInfoTokenUrl(dinamycInfoTokenUrl);
                dinamycPage.setStationFinderUrl(dinamycStationFinderUrl);
                dinamycPage.setFirstStartUrl(dinamycFirstStartUrl);
                dinamycPage.setBusinessInfoUrl(dinamycBusinessInfoUrl);

                retrieveApplicationSettingsBodyResponse.setDinamycPage(dinamycPage);

                // first start
                RetrieveApplicationSettingsFirstStartBodyResponse firstStart = new RetrieveApplicationSettingsFirstStartBodyResponse();
                firstStart.setText(firstStartText);
                retrieveApplicationSettingsBodyResponse.setFirstStart(firstStart);

                //security
                RetrieveApplicationSettingsSecurityBodyResponse security = new RetrieveApplicationSettingsSecurityBodyResponse();
                security.setPublicKey(publicKey);
                security.setCaptchaPublicKeyAndroid(captchaPublicKeyAndroid);
                security.setCaptchaPublicKeyIOS(captchaPublicKeyIOS);
                retrieveApplicationSettingsBodyResponse.setSecurity(security);
                
                retrieveApplicationSettingsResponse.setBody(retrieveApplicationSettingsBodyResponse);

                status.setStatusMessage(prop.getProperty(status.getStatusCode()));
                retrieveApplicationSettingsResponse.setStatus(status);

                json = gson.toJson(retrieveApplicationSettingsResponse);
                
                /*
                 if (json.length() >= 255) {
                 
                    json = json.substring(0, 255) + "...";
                }*/
                
                loggerService.log(ErrorLevel.INFO, SystemBusinessJsonFrontendAdapterService.class.getClass().getSimpleName(), "systemBusinessJsonHandler", null, "closing", json.toString());

                return Response.status(200).entity(retrieveApplicationSettingsResponse).build();
            }
        }

        BaseResponse baseResponse = new BaseResponse();

        Status statusResponse = new Status();
        statusResponse.setStatusCode(status.getStatusCode());
        statusResponse.setStatusMessage(prop.getProperty(status.getStatusCode()));
        baseResponse.setStatus(statusResponse);

        json = gson.toJson(baseResponse);
        loggerService.log(ErrorLevel.INFO, SystemBusinessJsonFrontendAdapterService.class.getClass().getSimpleName(), "systemBusinessJsonHandler", null, "closing", json.toString());

        return Response.status(200).entity(baseResponse).build();

    }

}