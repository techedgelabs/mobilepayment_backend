package com.techedge.mp.frontend.adapter.webservices;

import java.util.Properties;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.VoucherTransactionServiceRemote;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;
import com.techedge.mp.frontend.adapter.entities.requests.VoucherRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;

public class VoucherJsonFrontendAdapterService {

    public static Properties prop = new Properties();
    public static Gson       gson = new Gson();
    public static String     json;

    public static Response voucherJsonHandler(String stringRequest, Properties prop) {

        VoucherRequest request = null;

        // logger.log(Level.INFO, "request : " + stringRequest);

        LoggerServiceRemote loggerService = null;
        ParametersServiceRemote parametersService = null;

        try {
            loggerService = EJBHomeCache.getInstance().getLoggerService();
            parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }
        
        try {
            request = gson.fromJson(stringRequest, VoucherRequest.class);
        }
        catch (JsonSyntaxException jsonEx) {

            loggerService.log(ErrorLevel.INFO, VoucherJsonFrontendAdapterService.class.getSimpleName(), "voucherJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.VOUCHER_REQU_JSON_SYNTAX_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.VOUCHER_REQU_JSON_SYNTAX_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, VoucherJsonFrontendAdapterService.class.getSimpleName(), "voucherJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        VoucherTransactionServiceRemote voucherService = null;

        try {
            voucherService = EJBHomeCache.getInstance().getVoucherService();
        }
        catch (InterfaceNotFoundException e) {

            loggerService.log(ErrorLevel.INFO, VoucherJsonFrontendAdapterService.class.getSimpleName(), "voucherJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, VoucherJsonFrontendAdapterService.class.getSimpleName(), "voucherJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        if (voucherService == null) {

            loggerService.log(ErrorLevel.INFO, VoucherJsonFrontendAdapterService.class.getSimpleName(), "voucherJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, VoucherJsonFrontendAdapterService.class.getSimpleName(), "voucherJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        AbstractRequest abstractRequest = null;
        try {
            abstractRequest = request.getAbstractRequest();
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }

        if (abstractRequest != null) {
            abstractRequest.setVoucherTransactionServiceRemote(voucherService);
            abstractRequest.setParameterServiceRemote(parametersService);
            abstractRequest.setLoggerServiceRemote(loggerService);
        }

        return AbstractRequestUtility.abstractRequestInitialize(prop, abstractRequest, loggerService, VoucherJsonFrontendAdapterService.class, stringRequest, 
                "voucherJsonHandler", request.getRequestName());
    }

}