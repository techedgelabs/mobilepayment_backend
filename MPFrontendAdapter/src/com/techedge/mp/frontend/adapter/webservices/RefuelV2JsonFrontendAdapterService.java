package com.techedge.mp.frontend.adapter.webservices;

import java.util.Properties;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.techedge.mp.bpel.operation.refuel.business.BPELServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.TransactionV2ServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;
import com.techedge.mp.frontend.adapter.entities.requests.RefuelV2Request;

public class RefuelV2JsonFrontendAdapterService {

    public static Gson       gson = new Gson();
    public static String     json;

    public static Response refuelV2JsonHandler(String stringRequest, Properties prop) {

        RefuelV2Request request = null;

        LoggerServiceRemote loggerService = null;

        try {
            loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }

        try {
            request = gson.fromJson(stringRequest, RefuelV2Request.class);
        }
        catch (JsonSyntaxException jsonEx) {
            loggerService.log(ErrorLevel.INFO, RefuelV2JsonFrontendAdapterService.class.getSimpleName(), "refuelV2JsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.USER_REQU_JSON_SYNTAX_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.USER_REQU_JSON_SYNTAX_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, RefuelV2JsonFrontendAdapterService.class.getSimpleName(), "refuelV2JsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }
        
        UserServiceRemote userService = null;
        TransactionServiceRemote transactionService = null;
        TransactionV2ServiceRemote transactionV2Service = null;
        BPELServiceRemote bpelService = null;
        ForecourtInfoServiceRemote forecourtInfoService = null;
        ParametersServiceRemote parametersService = null;


        try {
            userService = EJBHomeCache.getInstance().getUserService();
            transactionService = EJBHomeCache.getInstance().getTransactionService();
            transactionV2Service = EJBHomeCache.getInstance().getTransactionV2Service();
            bpelService = EJBHomeCache.getInstance().getBpelService();
            forecourtInfoService = EJBHomeCache.getInstance().getForecourtInfoService();
            parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            loggerService.log(ErrorLevel.INFO, RefuelV2JsonFrontendAdapterService.class.getSimpleName(), "refuelV2JsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, RefuelV2JsonFrontendAdapterService.class.getSimpleName(), "refuelV2JsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        if (userService == null || transactionService == null || transactionService == null || bpelService == null || forecourtInfoService == null) {

            loggerService.log(ErrorLevel.INFO, RefuelV2JsonFrontendAdapterService.class.getSimpleName(), "refuelV2JsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, RefuelV2JsonFrontendAdapterService.class.getSimpleName(), "refuelV2JsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        AbstractRequest abstractRequest = null;
        try {
            abstractRequest = request.getAbstractRequest();
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }

        if (abstractRequest != null) {
            abstractRequest.setLoggerServiceRemote(loggerService);
            abstractRequest.setBpelServiceRemote(bpelService);
            abstractRequest.setUserServiceRemote(userService);
            abstractRequest.setTransactionServiceRemote(transactionService);
            abstractRequest.setTransactionV2ServiceRemote(transactionV2Service);
            abstractRequest.setParameterServiceRemote(parametersService);
            abstractRequest.setForecourtInfoServiceRemote(forecourtInfoService);
            
            return AbstractRequestUtility.abstractRequestInitialize(prop, abstractRequest, loggerService, RefuelV2JsonFrontendAdapterService.class, stringRequest, 
                    "refuelV2JsonHandler", request.getRequestName());
        }
        return RefuelJsonFrontendAdapterService.refuelJsonHandler(stringRequest, prop);
    }

}