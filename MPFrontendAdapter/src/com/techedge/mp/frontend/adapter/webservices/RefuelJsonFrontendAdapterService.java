package com.techedge.mp.frontend.adapter.webservices;

import java.util.Properties;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.techedge.mp.bpel.operation.refuel.business.BPELServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;
import com.techedge.mp.frontend.adapter.entities.requests.RefuelRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;

public class RefuelJsonFrontendAdapterService {

    public static Gson       gson = new Gson();
    public static String     json;

    public static Response refuelJsonHandler(String stringRequest, Properties prop) {

        Status status = null;

        RefuelRequest request = null;

        LoggerServiceRemote loggerService = null;

        try {
            loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.REFUEL_SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.REFUEL_SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }

        try {

            request = gson.fromJson(stringRequest, RefuelRequest.class);
        }
        catch (JsonSyntaxException jsonEx) {

            loggerService.log(ErrorLevel.INFO, RefuelJsonFrontendAdapterService.class.getSimpleName(), "refuelJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.USER_REQU_JSON_SYNTAX_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.USER_REQU_JSON_SYNTAX_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, RefuelJsonFrontendAdapterService.class.getSimpleName(), "refuelJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();

        }

        UserServiceRemote userService = null;
        TransactionServiceRemote transactionService = null;
        BPELServiceRemote bpelService = null;
        ForecourtInfoServiceRemote forecourtInfoService = null;
        ParametersServiceRemote parametersService = null;

        try {
            userService = EJBHomeCache.getInstance().getUserService();
            transactionService = EJBHomeCache.getInstance().getTransactionService();
            bpelService = EJBHomeCache.getInstance().getBpelService();
            forecourtInfoService = EJBHomeCache.getInstance().getForecourtInfoService();
            parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            status = new Status();
            status.setStatusCode(StatusCode.REFUEL_SYSTEM_ERROR);
            status.setStatusMessage(prop.getProperty(status.getStatusCode()));

            return Response.status(200).entity(status).build();
        }

        if (userService == null || transactionService == null || bpelService == null) {

            status = new Status();
            status.setStatusCode(StatusCode.REFUEL_SYSTEM_ERROR);
            status.setStatusMessage(prop.getProperty(status.getStatusCode()));

            return Response.status(200).entity(status).build();
        }

        AbstractRequest abstractRequest = null;
        try {
            abstractRequest = request.getAbstractRequest();
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (abstractRequest != null) {
            abstractRequest.setLoggerServiceRemote(loggerService);
            abstractRequest.setBpelServiceRemote(bpelService);
            abstractRequest.setUserServiceRemote(userService);
            abstractRequest.setTransactionServiceRemote(transactionService);
            abstractRequest.setParameterServiceRemote(parametersService);
            abstractRequest.setForecourtInfoServiceRemote(forecourtInfoService);
        }

        return AbstractRequestUtility.abstractRequestInitialize(prop, abstractRequest, loggerService, RefuelJsonFrontendAdapterService.class, stringRequest, 
                "refuelJsonHandler", request.getRequestName());
    }

}