package com.techedge.mp.frontend.adapter.webservices;

import java.util.Properties;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.PostPaidTransactionServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;
import com.techedge.mp.frontend.adapter.entities.requests.PoPRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;

public class PopJsonFrontendAdapterService {

    public static Gson       gson = new Gson();
    public static String     json;

    public static Response popJsonHandler(String stringRequest, Properties prop) {

        Status status = null;

        PoPRequest request = null;

        LoggerServiceRemote loggerService = null;

        try {
            loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }

        ParametersServiceRemote parametersService = null;

        try {
            parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            status = new Status();
            status.setStatusCode(StatusCode.RETRIEVE_APPLICATION_SETTINGS_SYSTEM_ERROR);
            status.setStatusMessage(prop.getProperty(status.getStatusCode()));

            return Response.status(200).entity(status).build();
        }

        try {
            request = gson.fromJson(stringRequest, PoPRequest.class);
        }
        catch (JsonSyntaxException jsonEx) {
            
            loggerService.log(ErrorLevel.INFO, PopJsonFrontendAdapterService.class.getSimpleName(), "popJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.USER_REQU_JSON_SYNTAX_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.USER_REQU_JSON_SYNTAX_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, PopJsonFrontendAdapterService.class.getSimpleName(), "popJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();

        }
        catch (Exception ex) {

            loggerService.log(ErrorLevel.INFO, PopJsonFrontendAdapterService.class.getSimpleName(), "popJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.USER_REQU_JSON_SYNTAX_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.USER_REQU_JSON_SYNTAX_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, PopJsonFrontendAdapterService.class.getSimpleName(), "popJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();

        }

        UserServiceRemote userService = null;
        PostPaidTransactionServiceRemote postPaidTransactionService = null;

        try {
            postPaidTransactionService = EJBHomeCache.getInstance().getPostPaidTransactionService();
            userService = EJBHomeCache.getInstance().getUserService();
        }
        catch (InterfaceNotFoundException e) {

            loggerService.log(ErrorLevel.INFO, PopJsonFrontendAdapterService.class.getSimpleName(), "popJsonHandler", request.getRequestName(), "opening", stringRequest);
            
            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, PopJsonFrontendAdapterService.class.getSimpleName(), "popJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        if (postPaidTransactionService == null || userService == null) {

            loggerService.log(ErrorLevel.INFO, PopJsonFrontendAdapterService.class.getSimpleName(), "popJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, PopJsonFrontendAdapterService.class.getSimpleName(), "popJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        AbstractRequest abstractRequest = null;
        try {
            abstractRequest = request.getAbstractRequest();
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        if (abstractRequest != null) {
            abstractRequest.setPostPaidTransactionServiceRemote(postPaidTransactionService);
            abstractRequest.setUserServiceRemote(userService);
            abstractRequest.setLoggerServiceRemote(loggerService);
            abstractRequest.setParameterServiceRemote(parametersService);
        }

        return AbstractRequestUtility.abstractRequestInitialize(prop, abstractRequest, loggerService, PopJsonFrontendAdapterService.class, stringRequest, 
                "popJsonHandler", request.getRequestName());
    }
}