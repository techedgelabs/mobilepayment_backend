package com.techedge.mp.frontend.adapter.webservices;

import java.util.Properties;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ManagerServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;
import com.techedge.mp.frontend.adapter.entities.requests.ManagerRequest;

public class ManagerJsonFrontendAdapterService {

    public static Gson       gson = new Gson();
    public static String     json;

    public static Response managerJsonHandler(String stringRequest, Properties prop) {

        ManagerRequest request = null;

        LoggerServiceRemote loggerService = null;

        try {
            loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }

        try {
            request = gson.fromJson(stringRequest, ManagerRequest.class);
        }
        catch (JsonSyntaxException jsonEx) {

            loggerService.log(ErrorLevel.INFO, ManagerJsonFrontendAdapterService.class.getSimpleName(), "managerJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.USER_REQU_JSON_SYNTAX_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.USER_REQU_JSON_SYNTAX_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, ManagerJsonFrontendAdapterService.class.getSimpleName(), "managerJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();

        }

        // TODO da aggiornare;
        ManagerServiceRemote managerService = null;

        try {
            managerService = EJBHomeCache.getInstance().getManagerService();
        }
        catch (InterfaceNotFoundException e) {

            loggerService.log(ErrorLevel.INFO, ManagerJsonFrontendAdapterService.class.getSimpleName(), "managerJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, ManagerJsonFrontendAdapterService.class.getSimpleName(), "managerJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        if (managerService == null) {

            loggerService.log(ErrorLevel.INFO, ManagerJsonFrontendAdapterService.class.getSimpleName(), "managerJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, ManagerJsonFrontendAdapterService.class.getSimpleName(), "managerJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        // TODO fine da aggiornare;

        AbstractRequest abstractRequest = null;
        try {
            abstractRequest = request.getAbstractRequest();
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (abstractRequest != null) {
            abstractRequest.setManagerServiceRemote(managerService);
            abstractRequest.setLoggerServiceRemote(loggerService);
        }

        return AbstractRequestUtility.abstractRequestInitialize(prop, abstractRequest, loggerService, ManagerJsonFrontendAdapterService.class, stringRequest, 
                "managerJsonHandler", request.getRequestName());
    }

}