package com.techedge.mp.frontend.adapter.webservices;

import java.util.Properties;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.ParkingTransactionV2ServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;
import com.techedge.mp.frontend.adapter.entities.requests.UserRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;

public class UserJsonFrontendAdapterService {

    public static Gson   gson = new Gson();
    public static String json;

    public static Response userJsonHandler(String stringRequest, Properties prop) {

        UserRequest request = null;

        LoggerServiceRemote loggerService = null;
        
        ParkingTransactionV2ServiceRemote parkingTransactionV2Service  = null;

        try {
            loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }

        ParametersServiceRemote parametersService = null;

        try {
            parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in parameterService initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }

        try {
            request = gson.fromJson(stringRequest, UserRequest.class);
        }
        catch (JsonSyntaxException jsonEx) {
            
            loggerService.log(ErrorLevel.INFO, UserJsonFrontendAdapterService.class.getSimpleName(), "userJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.USER_REQU_JSON_SYNTAX_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.USER_REQU_JSON_SYNTAX_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            loggerService.log(ErrorLevel.INFO, UserJsonFrontendAdapterService.class.getSimpleName(), "userJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        UserServiceRemote userService = null;

        try {
            userService = EJBHomeCache.getInstance().getUserService();
        }
        catch (InterfaceNotFoundException e) {
            
            loggerService.log(ErrorLevel.INFO, UserJsonFrontendAdapterService.class.getSimpleName(), "userJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, UserJsonFrontendAdapterService.class.getSimpleName(), "userJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        if (userService == null) {
            
            loggerService.log(ErrorLevel.INFO, UserJsonFrontendAdapterService.class.getSimpleName(), "userJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            loggerService.log(ErrorLevel.INFO, UserJsonFrontendAdapterService.class.getSimpleName(), "userJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }
        
        try {
            parkingTransactionV2Service = EJBHomeCache.getInstance().getParkingTransactionV2Service();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }
        
        if (parkingTransactionV2Service == null) {

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }
        
        AbstractRequest abstractRequest = null;
        try {
            abstractRequest = request.getAbstractRequest();
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        if (abstractRequest != null) {
            abstractRequest.setUserServiceRemote(userService);
            abstractRequest.setLoggerServiceRemote(loggerService);
            abstractRequest.setParameterServiceRemote(parametersService);
            abstractRequest.setParkingTransactionV2ServiceRemote(parkingTransactionV2Service);
        }

        return AbstractRequestUtility.abstractRequestInitialize(prop, abstractRequest, loggerService, UserJsonFrontendAdapterService.class, stringRequest, 
                "userJsonHandler", request.getRequestName());

    }
}