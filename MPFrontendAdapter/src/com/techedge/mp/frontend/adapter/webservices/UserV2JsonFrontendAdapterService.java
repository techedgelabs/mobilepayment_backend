package com.techedge.mp.frontend.adapter.webservices;

import java.util.Properties;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.ParkingTransactionV2ServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.UserV2ServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;
import com.techedge.mp.frontend.adapter.entities.requests.UserV2Request;

public class UserV2JsonFrontendAdapterService {

    public static Gson       gson = new Gson();
    public static String     json;

    public static Response userV2JsonHandler(String stringRequest, Properties prop) {

        UserV2Request request = null;

        LoggerServiceRemote loggerService = null;

        try {
            loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }
        
        ParametersServiceRemote parametersService = null;

        try {
            parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in parameterService initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }

        CRMServiceRemote crmService = null;

        try {
            crmService = EJBHomeCache.getInstance().getCRMService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in crmService initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }

        try {
            request = gson.fromJson(stringRequest, UserV2Request.class);
        }
        catch (JsonSyntaxException jsonEx) {
            
            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.USER_REQU_JSON_SYNTAX_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.USER_REQU_JSON_SYNTAX_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        UserServiceRemote userService = null;

        try {
            userService = EJBHomeCache.getInstance().getUserService();
        }
        catch (InterfaceNotFoundException e) {

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        if (userService == null) {

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }
        
        UserV2ServiceRemote userV2Service = null;

        try {
            userV2Service = EJBHomeCache.getInstance().getUserV2Service();
        }
        catch (InterfaceNotFoundException e) {

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        if (userV2Service == null) {

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }
        
        ParkingTransactionV2ServiceRemote parkingTransactionV2Service  = null;

        try {
            parkingTransactionV2Service = EJBHomeCache.getInstance().getParkingTransactionV2Service();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }
        
        if (parkingTransactionV2Service == null) {

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);

            loggerService.log(ErrorLevel.INFO, UserV2JsonFrontendAdapterService.class.getSimpleName(), "userV2JsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        AbstractRequest abstractRequest = null;
        try {
            abstractRequest = request.getAbstractRequest();
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }

        if (abstractRequest != null) {
            abstractRequest.setUserServiceRemote(userService);
            abstractRequest.setUserV2ServiceRemote(userV2Service);
            abstractRequest.setLoggerServiceRemote(loggerService);
            abstractRequest.setParameterServiceRemote(parametersService);
            abstractRequest.setCrmServiceRemote(crmService);
            abstractRequest.setParkingTransactionV2ServiceRemote(parkingTransactionV2Service);
            return AbstractRequestUtility.abstractRequestInitialize(prop, abstractRequest, loggerService, UserV2JsonFrontendAdapterService.class, stringRequest, 
                    "userV2JsonHandler", request.getRequestName());
        }
        return UserJsonFrontendAdapterService.userJsonHandler(stringRequest, prop);
    }

}