package com.techedge.mp.frontend.adapter.webservices;

import java.util.Properties;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.SurveyServiceRemote;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;
import com.techedge.mp.frontend.adapter.entities.requests.SurveyRequest;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;

public class SurveyJsonFrontendAdapterService {

    public static Gson       gson = new Gson();
    public static String     json;

    public static Response surveyJsonHandler(String stringRequest, Properties prop) {

        SurveyRequest request = null;

        LoggerServiceRemote loggerService = null;

        try {
            loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }

        try {
            request = gson.fromJson(stringRequest, SurveyRequest.class);
        }
        catch (JsonSyntaxException jsonEx) {

            loggerService.log(ErrorLevel.INFO, SurveyJsonFrontendAdapterService.class.getSimpleName(), "surveyJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SURVEY_REQU_JSON_SYNTAX_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SURVEY_REQU_JSON_SYNTAX_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, SurveyJsonFrontendAdapterService.class.getSimpleName(), "surveyJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();

        }

        SurveyServiceRemote surveyService = null;

        try {
            surveyService = EJBHomeCache.getInstance().getSurveyService();
        }
        catch (InterfaceNotFoundException e) {

            loggerService.log(ErrorLevel.INFO, SurveyJsonFrontendAdapterService.class.getSimpleName(), "surveyJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, SurveyJsonFrontendAdapterService.class.getSimpleName(), "surveyJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        if (surveyService == null) {

            loggerService.log(ErrorLevel.INFO, SurveyJsonFrontendAdapterService.class.getSimpleName(), "surveyJsonHandler", request.getRequestName(), "opening", stringRequest);

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            json = gson.toJson(baseResponse);
            loggerService.log(ErrorLevel.INFO, SurveyJsonFrontendAdapterService.class.getSimpleName(), "surveyJsonHandler", request.getRequestName(), "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }
        AbstractRequest abstractRequest = null;
        try {
            abstractRequest = request.getAbstractRequest();
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (abstractRequest != null) {
            abstractRequest.setSurveyServiceRemote(surveyService);
            abstractRequest.setLoggerServiceRemote(loggerService);
        }

        return AbstractRequestUtility.abstractRequestInitialize(prop, abstractRequest, loggerService, SurveyJsonFrontendAdapterService.class, stringRequest, 
                "surveyJsonHandler", request.getRequestName());
    }

}