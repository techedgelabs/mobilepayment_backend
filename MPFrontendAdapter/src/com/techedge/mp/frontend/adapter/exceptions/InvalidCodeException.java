package com.techedge.mp.frontend.adapter.exceptions;

public class InvalidCodeException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6629877841482289201L;
	
	private String code;
	
	public InvalidCodeException(String code) {
		
		this.code = code;
	}
	
	public String getCode()
	{
		return code;
	}
}
