package com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings;


public class RetrieveApplicationSettingsFirstStartBodyResponse {

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
