package com.techedge.mp.frontend.adapter.entities.common;

public class Status {
	
	
	private String statusCode;
	private String statusMessage;
	
	public Status() {}
	
	public Status(String statusCode, String statusCessage) {
		this.statusCode = statusCode;
		this.statusMessage = statusCessage;
	}

	public String getStatusCode() {
		return statusCode;
	}
	
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getStatusMessage() {
		return statusMessage;
	}
	
	public void setStatusMessage(String statusMessage) {
		this.statusMessage = statusMessage;
	}
	
}
