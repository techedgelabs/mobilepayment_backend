package com.techedge.mp.frontend.adapter.entities.user.v2.getpromopopup;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.AppLink;

public class GetPromoPopupResponseBody implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 433246770458413191L;

    private String            type;
    private AppLink           appLink;
    private String            body;
    private String            bannerUrl;
    private Boolean           showAlways;
    private String            promoId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public AppLink getAppLink() {
        return appLink;
    }

    public void setAppLink(AppLink appLink) {
        this.appLink = appLink;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public Boolean getShowAlways() {
        return showAlways;
    }

    public void setShowAlways(Boolean showAlways) {
        this.showAlways = showAlways;
    }

    public String getPromoId() {
        return promoId;
    }

    public void setPromoId(String promoId) {
        this.promoId = promoId;
    }

}
