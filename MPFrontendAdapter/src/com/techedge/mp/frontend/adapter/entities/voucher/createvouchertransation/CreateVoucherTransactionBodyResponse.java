package com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation;


public class CreateVoucherTransactionBodyResponse {

    private String voucherTransactionID;
    private String voucherCode;
    private Integer checkPinAttemptsLeft;
    
    public String getVoucherTransactionID() {
    
        return voucherTransactionID;
    }
    
    public void setVoucherTransactionID(String voucherTransactionID) {
    
        this.voucherTransactionID = voucherTransactionID;
    }
    
    public String getVoucherCode() {
    
        return voucherCode;
    }
    
    public void setVoucherCode(String voucherCode) {
    
        this.voucherCode = voucherCode;
    }

    public Integer getCheckPinAttemptsLeft() {
        return checkPinAttemptsLeft;
    }

    public void setCheckPinAttemptsLeft(Integer checkPinAttemptsLeft) {
        this.checkPinAttemptsLeft = checkPinAttemptsLeft;
    }
}
