package com.techedge.mp.frontend.adapter.entities.user.update;

import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UpdateUserUserData implements Validable {
	
	
	public UpdateUserUserData() {}
	
    private String firstName;
    private String lastName;
    private CustomDate dateOfBirth;
    private String birthMunicipality;
    private String birthProvince;
    private String language;
    private String sex;

    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public CustomDate getDateOfBirth() {
        return dateOfBirth;
    }


    public void setDateOfBirth(CustomDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    public String getBirthMunicipality() {
        return birthMunicipality;
    }

    public void setBirthMunicipality(String birthMunicipality) {
        this.birthMunicipality = birthMunicipality;
    }

    public String getBirthProvince() {
        return birthProvince;
    }

    public void setBirthProvince(String birthProvince) {
        this.birthProvince = birthProvince;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
    	
	
	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.getFirstName() != null) {
			
			if(this.getFirstName().length() > 40 || this.getFirstName().trim() == "") {
			
				status.setStatusCode(StatusCode.USER_UPD_MASTER_DATA_WRONG);
				
				return status;
			
			}
			
		}
		
		if(this.getLastName() != null) {
			
			if(this.getLastName().length() > 40 || this.getLastName().trim() == "") {
			
				status.setStatusCode(StatusCode.USER_UPD_MASTER_DATA_WRONG);
				
				return status;
			
			}
			
		}
		
		if(this.getDateOfBirth() != null) {
			
			if(this.getDateOfBirth().getYear() == null || this.getDateOfBirth().getYear() < 1900 || this.getDateOfBirth().getYear() > 2050) {
				
				status.setStatusCode(StatusCode.USER_UPD_DATE_BIRTH_WRONG);
				
				return status;
				
			}
			
			if(this.getDateOfBirth().getMonth() == null || this.getDateOfBirth().getMonth() < 1 || this.getDateOfBirth().getMonth() > 12) {
				
				status.setStatusCode(StatusCode.USER_UPD_DATE_BIRTH_WRONG);

				return status;
				
			}
			
			if(this.getDateOfBirth().getDay() == null || this.getDateOfBirth().getDay() < 1 && this.getDateOfBirth().getDay() > 31) {
				
				status.setStatusCode(StatusCode.USER_UPD_DATE_BIRTH_WRONG);

				return status;
				
			}
			
		}
		
		if(this.getBirthMunicipality() != null) {
			
			if(this.getBirthMunicipality().length() > 40 || this.getBirthMunicipality().trim() == "") {
			
				status.setStatusCode(StatusCode.USER_UPD_MASTER_DATA_WRONG);
	
				return status;
				
			}
			
		}
		
		if(this.getBirthProvince() != null) {
			
			if(this.getBirthProvince().length() > 40 || this.getBirthProvince().trim() == "") {
			
				status.setStatusCode(StatusCode.USER_UPD_MASTER_DATA_WRONG);
	
				return status;
			
			}
			
		}
		
		if(this.getLanguage() != null) {
			
			if(this.getLanguage().trim().length() != 2) {
			
				status.setStatusCode(StatusCode.USER_UPD_MASTER_DATA_WRONG);
	
				return status;
			
			}
			
		}
		
		if(this.getSex() != null) {
			
			if(!this.getSex().equals("M") && !this.getSex().equals("F")) {
			
				status.setStatusCode(StatusCode.USER_UPD_MASTER_DATA_WRONG);
	
				return status;
			
			}
			
		}
		
		status.setStatusCode(StatusCode.USER_UPD_SUCCESS);
			
		return status;
	}
	
}
