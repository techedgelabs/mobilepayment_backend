package com.techedge.mp.frontend.adapter.entities.user.checkavailabilityamount;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class CheckAvailabilityAmountResponse extends BaseResponse {

	
	private CheckAvailabilityAmountBodyResponse body;

	
	public CheckAvailabilityAmountBodyResponse getBody() {
		return body;
	}

	
	public void setBody(CheckAvailabilityAmountBodyResponse body) {
		this.body = body;
	}
	
	
	
}
