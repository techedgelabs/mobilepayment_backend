package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;

public class ReceiptTransactionDetail {

    private ArrayList<ReceiptTransactionDetailSection> receiptSection = new ArrayList<ReceiptTransactionDetailSection>();  
    
    public ArrayList<ReceiptTransactionDetailSection> getReceiptSection() {
        return receiptSection;
    }
    
    public ReceiptTransactionDetailSection createSection(String title, String type, String icon, Boolean separator) {
        ReceiptTransactionDetailSection section = new ReceiptTransactionDetailSection();
        
        section.setTitle(title);
        section.setSectionType(type);
        section.setIcon(icon);
        section.setSeparator(separator);
        
        receiptSection.add(section);
        
        return section;
    }
    
    public void removeSection(ReceiptTransactionDetailSection section) {
        receiptSection.remove(section);
    }
    
    public void addDataToSection(ReceiptTransactionDetailSection section, String key, String label, String unit, String value, int position, String link) {
        
        int sectionIndex = receiptSection.indexOf(section);
        
        if (sectionIndex == -1) {
            System.out.println("RefuelTransactionDetailRecipt - RefuelTransactionDetailReceiptSection non trovato");
        }
        
        section.addDataToSection(key, label, unit, value, position, link);
        
        receiptSection.set(sectionIndex, section);
        
    }
    
    public int getSectionDataSize(ReceiptTransactionDetailSection section) {
        return section.getReceiptData().size();
    }
}
