package com.techedge.mp.frontend.adapter.entities.interfaces;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;

public class AbstractBaseResponse {

    private String json;
    private Gson   gson = new Gson();

    public String getJson() {
        json = gson.toJson(this);
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public Gson getGson() {
        return gson;
    }

    public void setGson(Gson gson) {
        this.gson = gson;
    }

    public Response response() {
        if (json == null) {
            json = gson.toJson(this);
        }
        return Response.status(200).entity(this).build();
    }
}
