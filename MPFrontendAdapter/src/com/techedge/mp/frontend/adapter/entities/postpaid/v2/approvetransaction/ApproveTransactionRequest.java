package com.techedge.mp.frontend.adapter.entities.postpaid.v2.approvetransaction;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ApproveTransactionRequest extends AbstractRequest implements Validable {

    private ApproveTransactionCredential  credential;
    private ApproveTransactionBodyRequest body;

    public ApproveTransactionCredential getCredential() {
        return credential;
    }

    public void setCredential(ApproveTransactionCredential credential) {
        this.credential = credential;
    }

    public ApproveTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(ApproveTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("POSTPAID-APPROVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        ApproveTransactionRequest approvePoPTransactionRequest = this;
        ApproveTransactionResponse approvePoPTransactionResponse = new ApproveTransactionResponse();

        PostPaidApproveShopTransactionResponse postPaidApproveShopTransactionResponse = getPostPaidV2TransactionServiceRemote().approvePopTransaction(
                approvePoPTransactionRequest.getCredential().getRequestID(), approvePoPTransactionRequest.getCredential().getTicketID(),
                approvePoPTransactionRequest.getBody().getTransactionID(), approvePoPTransactionRequest.getBody().getPaymentMethod().getId(),
                approvePoPTransactionRequest.getBody().getPaymentMethod().getType(), approvePoPTransactionRequest.getCredential().getPin());

        ApproveTransactionBodyResponse approvePoPTransactionBodyResponse = new ApproveTransactionBodyResponse();
        approvePoPTransactionBodyResponse.setCheckPinAttemptsLeft(postPaidApproveShopTransactionResponse.getPinCheckMaxAttempts());
        
        Status status = new Status();
        status.setStatusCode(postPaidApproveShopTransactionResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(postPaidApproveShopTransactionResponse.getStatusCode()));
        approvePoPTransactionResponse.setStatus(status);
        approvePoPTransactionResponse.setBody(approvePoPTransactionBodyResponse);

        return approvePoPTransactionResponse;
    }

}