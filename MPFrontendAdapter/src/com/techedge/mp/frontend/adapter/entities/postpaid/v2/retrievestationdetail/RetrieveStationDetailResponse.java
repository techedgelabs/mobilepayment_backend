package com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievestationdetail;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveStationDetailResponse extends BaseResponse {

	
	private RetrieveStationDetailBodyResponse body;

	
	public synchronized RetrieveStationDetailBodyResponse getBody() {
		return body;
	}

	public synchronized void setBody(RetrieveStationDetailBodyResponse body) {
		this.body = body;
	}
	
	
	
}
