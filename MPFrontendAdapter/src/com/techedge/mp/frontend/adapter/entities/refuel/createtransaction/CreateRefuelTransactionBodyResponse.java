package com.techedge.mp.frontend.adapter.entities.refuel.createtransaction;


public class CreateRefuelTransactionBodyResponse {

	private Integer checkPinAttemptsLeft;
	private Integer maxAmount;
    private Integer thresholdAmount;

	public Integer getCheckPinAttemptsLeft() {
		return checkPinAttemptsLeft;
	}
	public void setCheckPinAttemptsLeft(Integer checkPinAttemptsLeft) {
		this.checkPinAttemptsLeft = checkPinAttemptsLeft;
	}
	
    public Integer getMaxAmount() {
        return maxAmount;
    }
    public void setMaxAmount(Integer maxAmount) {
        this.maxAmount = maxAmount;
    }
    
    public Integer getThresholdAmount() {
        return thresholdAmount;
    }
    public void setThresholdAmount(Integer thresholdAmount) {
        this.thresholdAmount = thresholdAmount;
    }
}
