package com.techedge.mp.frontend.adapter.entities.user.v2.getawardlist;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.AwardData;
import com.techedge.mp.frontend.adapter.entities.common.AwardGroup;
import com.techedge.mp.frontend.adapter.entities.common.BrandData;
import com.techedge.mp.frontend.adapter.entities.common.CategoryBurnInfo;
import com.techedge.mp.frontend.adapter.entities.common.RedemptionData;

public class GetAwardListBodyResponse {

    private List<BrandData> brandDataList           = new ArrayList<BrandData>(0);
    private List<CategoryBurnInfo> categoryBurnList = new ArrayList<CategoryBurnInfo>(0);
    private List<RedemptionData> redemptions        = new ArrayList<RedemptionData>(0);
    private List<RedemptionData> parkingRedemptions = new ArrayList<RedemptionData>(0);
    
    private List<AwardGroup> redeemableAwardGroupList    = new ArrayList<AwardGroup>(0);
    private List<AwardGroup> notRedeemableAwardGroupList = new ArrayList<AwardGroup>(0);
    


    public List<BrandData> getBrandDataList() {
        return brandDataList;
    }

    public void setBrandDataList(List<BrandData> brandDataList) {
        this.brandDataList = brandDataList;
    }

    public List<CategoryBurnInfo> getCategoryBurnList() {
        return categoryBurnList;
    }

    public void setCategoryBurnList(List<CategoryBurnInfo> categoryBurnList) {
        this.categoryBurnList = categoryBurnList;
    }

    public List<RedemptionData> getRedemptions() {
        return redemptions;
    }

    public void setRedemptions(List<RedemptionData> redemptions) {
        this.redemptions = redemptions;
    }

    public List<AwardGroup> getRedeemableAwardGroupList() {
        return redeemableAwardGroupList;
    }

    public void setRedeemableAwardGroupList(List<AwardGroup> redeemableAwardGroupList) {
        this.redeemableAwardGroupList = redeemableAwardGroupList;
    }

    public List<AwardGroup> getNotRedeemableAwardGroupList() {
        return notRedeemableAwardGroupList;
    }

    public void setNotRedeemableAwardGroupList(List<AwardGroup> notRedeemableAwardGroupList) {
        this.notRedeemableAwardGroupList = notRedeemableAwardGroupList;
    }

    public List<RedemptionData> getParkingRedemptions() {
        return parkingRedemptions;
    }

    public void setParkingRedemptions(List<RedemptionData> parkingRedemptions) {
        this.parkingRedemptions = parkingRedemptions;
    }

}
