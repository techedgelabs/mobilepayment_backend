package com.techedge.mp.frontend.adapter.entities.common;

public class BaseResponse {

    private Status status;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

}
