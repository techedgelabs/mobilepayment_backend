package com.techedge.mp.frontend.adapter.entities.user.business.authentication;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class AuthenticationUserResponseSuccess extends BaseResponse {

	private AuthenticationUserBodyResponse body;

	public AuthenticationUserBodyResponse getBody() {
		return body;
	}

	public void setBody(AuthenticationUserBodyResponse body) {
		this.body = body;
	}
}
