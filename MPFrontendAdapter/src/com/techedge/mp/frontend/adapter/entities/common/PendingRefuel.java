package com.techedge.mp.frontend.adapter.entities.common;



public class PendingRefuel {



	private String refuelID;
	private String status;
	private String subStatus;
	private boolean useVoucher;
	private Double initialAmount;



	public String getRefuelID() {
		return refuelID;
	}
	public void setRefuelID(String refuelID) {
		this.refuelID = refuelID;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSubStatus() {
		return subStatus;
	}
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}
	public boolean isUseVoucher() {
		return useVoucher;
	}
	public void setUseVoucher(boolean useVoucher) {
		this.useVoucher = useVoucher;
	}
	public Double getInitialAmount() {
		return initialAmount;
	}
	public void setInitialAmount(Double initialAmount) {
		this.initialAmount = initialAmount;
	}
}
