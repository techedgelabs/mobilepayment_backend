package com.techedge.mp.frontend.adapter.entities.parking.v2.approveextendparkingtransaction;

import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ApproveExtendParkingTransactionRequestBody implements Validable {

    private CustomTimestamp                                  requestedEndTime;
    private String                                           transactionID;
    private ApproveExtendParkingTransactionPaymentMethodBody paymentMethod;

    @Override
    public Status check() {

        Status status = new Status();

        /*if (this.paymentMethod != null) {

            status = this.paymentMethod.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.APPROVE_PARKING_TRANSACTION_INVALID_REQUEST);

            return status;
        }*/

        if (this.transactionID == null || this.transactionID.isEmpty()) {

            status.setStatusCode("TBD");

            return status;

        }

        status.setStatusCode(StatusCode.APPROVE_PARKING_TRANSACTION_SUCCESS);

        return status;
    }

    public CustomTimestamp getRequestedEndTime() {
        return requestedEndTime;
    }

    public void setRequestedEndTime(CustomTimestamp requestedEndTime) {
        this.requestedEndTime = requestedEndTime;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public ApproveExtendParkingTransactionPaymentMethodBody getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(ApproveExtendParkingTransactionPaymentMethodBody paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

}
