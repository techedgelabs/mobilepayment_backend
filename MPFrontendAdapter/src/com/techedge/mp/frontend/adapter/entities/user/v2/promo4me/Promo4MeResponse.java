package com.techedge.mp.frontend.adapter.entities.user.v2.promo4me;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class Promo4MeResponse extends BaseResponse {

    private Promo4MeBodyResponse body;

    public Promo4MeBodyResponse getBody() {
        return body;
    }

    public void setBody(Promo4MeBodyResponse body) {
        this.body = body;
    }
}
