package com.techedge.mp.frontend.adapter.entities.eventnotification.retrievetransactiondetail;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.techedge.mp.core.business.interfaces.loyalty.EventNotification;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransaction;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionNonOil;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionRefuel;
import com.techedge.mp.core.business.interfaces.loyalty.RewardTransactionParameter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetail;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetailSection;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetailTransaction;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveLoyaltyTransactionDetailRequest extends AbstractRequest {

    private Status                                      status = new Status();

    private Credential                                  credential;
    private RetrieveLoyaltyTransactionDetailBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveLoyaltyTransactionDetailBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveLoyaltyTransactionDetailBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIVE-NOTIFICATION-TRANSACTION-DETAIL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;
            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
            return status;
        }

        return status;
    }

    @Override
    public BaseResponse execute() {

        RetrieveLoyaltyTransactionDetailRequest retrieveLoyaltyTransactionDetailRequest = this;
        RetrieveLoyaltyTransactionDetailResponse retrieveLoyaltyTransactionDetailResponse = new RetrieveLoyaltyTransactionDetailResponse();
        RetrieveLoyaltyTransactionDetailBodyResponse retrieveLoyaltyTransactionDetailBodyResponse = new RetrieveLoyaltyTransactionDetailBodyResponse();

        String ticketId = retrieveLoyaltyTransactionDetailRequest.getCredential().getTicketID();
        String requestId = retrieveLoyaltyTransactionDetailRequest.getCredential().getRequestID();
        Long eventNotificationId = retrieveLoyaltyTransactionDetailRequest.getBody().getEventNotificationID();

        EventNotification eventNotificationResponse = getEventNotificationServiceRemote().getTransactionDetail(ticketId, requestId, eventNotificationId);

        status.setStatusCode(eventNotificationResponse.getStatusCode());
        //status.setStatusCode(StatusCode.RETRIEVE_EVENT_NOTIFICATION_TRANSACTION_DETAIL_SUCCESS);
        //status.setStatusMessage("STATUS MESSAGE TEST");
        status.setStatusMessage(prop.getProperty(status.getStatusCode()));
        retrieveLoyaltyTransactionDetailResponse.setStatus(status);

        if (!status.getStatusCode().contains("200")) {
            retrieveLoyaltyTransactionDetailResponse.setStatus(status);
            return retrieveLoyaltyTransactionDetailResponse;
        }

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");

        retrieveLoyaltyTransactionDetailBodyResponse.setSourceType(eventNotificationResponse.getEventType().getValue());
        //retrieveTransactionDetailBodyResponse.setSourceType("BODY RESPONSE Source type");

        retrieveLoyaltyTransactionDetailBodyResponse.setSourceStatus("BODY RESPONSE Source status test");

        retrieveLoyaltyTransactionDetailBodyResponse.setSourceStatus(retrieveLoyaltyTransactionDetailBodyResponse.getSourceStatus());
        retrieveLoyaltyTransactionDetailBodyResponse.setSourceType(retrieveLoyaltyTransactionDetailBodyResponse.getSourceType());

        Integer amount = 0;

        ReceiptTransactionDetailTransaction transaction = new ReceiptTransactionDetailTransaction();
        transaction.setAmount(amount);
        transaction.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(new Date().getTime())));

        ReceiptTransactionDetail receipt = new ReceiptTransactionDetail();

        ReceiptTransactionDetailSection sectionRefuel = receipt.createSection("Rifornimento", "RECEIPT", null, false);
        ReceiptTransactionDetailSection sectionPromo = receipt.createSection("Messaggio Promozionale", "PROMO", null, true);
        ReceiptTransactionDetailSection sectionLoyalty = receipt.createSection("Punti Loyalty", "RECEIPT", null, false);
        ReceiptTransactionDetailSection sectionReward = receipt.createSection("Premio", "RECEIPT", null, false);

        int sectionPositionRefuel = 1;
        int sectionPositionPromo = 1;
        int sectionPositionLoyalty = 1;
        int sectionPositionReward = 1;

        // SECTION REFUEL

        LoyaltyTransaction loyaltyTransactiondetails = eventNotificationResponse.getLoyaltyTransactionDetail();
        transaction.setTransactionID(eventNotificationResponse.getSessionID());

        transaction.setStatusTitle(null);        //TBD
        transaction.setStatusDescription(null); //TBD

        // Trascodifica dello stato
        transaction.setStatus(null);            //TBD

        transaction.setSubStatus(null);         //TBD

        retrieveLoyaltyTransactionDetailBodyResponse.setTransaction(transaction);
        String stationId = eventNotificationResponse.getStationID();

        String creationTimestamp = new SimpleDateFormat("dd/MM/yyyy").format(eventNotificationResponse.getTransactionDate());
        receipt.addDataToSection(sectionRefuel, "DATE", "DATA", null, creationTimestamp, sectionPositionRefuel++, null);
        receipt.addDataToSection(sectionRefuel, "STATION_ID", "CODICE PUNTO VENDITA", null, stationId, sectionPositionRefuel++, null);

        if (eventNotificationResponse.getTerminalID() != null) {
            receipt.addDataToSection(sectionRefuel, "SHOP_CASH_ID", "COD CASSA", null, eventNotificationResponse.getTerminalID(), sectionPositionRefuel++, null);
        }

        // START SECTION LOYALTY
        String panCode = null;
        String credits = "0";
        String balanceAmount = "0";

        if (eventNotificationResponse.getPanCode() != null) {
            panCode = eventNotificationResponse.getPanCode();
        }

        if (eventNotificationResponse.getCredits() != null) {
            credits = eventNotificationResponse.getCredits().toString();
        }

        if (eventNotificationResponse.getBalance() != null) {
            balanceAmount = eventNotificationResponse.getBalance().toString();
        }

        if (panCode != null) {
            receipt.addDataToSection(sectionLoyalty, "LOYALTY_PAN", "CODICE", null, eventNotificationResponse.getPanCode(), sectionPositionLoyalty++, null);
        }

        receipt.addDataToSection(sectionLoyalty, "LOYALTY_CREDITS", "PUNTI FEDELTA'", null, credits, sectionPositionLoyalty++, null);
        receipt.addDataToSection(sectionLoyalty, "LOYALTY_BALANCE", "SALDO PUNTI", null, balanceAmount, sectionPositionLoyalty++, null);

        if (eventNotificationResponse.getMarketingMsg() != null) {
            String marketingMsg = eventNotificationResponse.getMarketingMsg().replaceAll("[\\s\\t\\n\\r ]{2,}", " ");
            receipt.addDataToSection(sectionPromo, "LOYALTY_MSG", "", null, marketingMsg, sectionPositionPromo++, null);
        }

        if (eventNotificationResponse.getWarningMsg() != null) {
            String warningMsg = eventNotificationResponse.getWarningMsg().replaceAll("[\\s\\t\\n\\r ]{2,}", " ");
            receipt.addDataToSection(sectionLoyalty, "LOYALTY_MSG", "", null, warningMsg, sectionPositionLoyalty++, null);
        }


        // END SECTION LOYALTY

        if (loyaltyTransactiondetails != null) {

            if (!loyaltyTransactiondetails.getNonOilDetails().isEmpty()) {
                for (LoyaltyTransactionNonOil nonOilDetailElement : loyaltyTransactiondetails.getNonOilDetails()) {
                    String cartAmount = "0.0";
                    if (nonOilDetailElement.getAmount() != null) {
                        cartAmount = currencyFormat.format(nonOilDetailElement.getAmount());
                    }

                    receipt.addDataToSection(sectionRefuel, "SHOP_AMOUNT", "IMPORTO SHOP", "�", cartAmount, sectionPositionRefuel++, null);
                    //receipt.addDataToSection(sectionRefuel, "PRODUCT_ID", "PRODUCT ID", null, nonOilDetailElement.getProductID(), sectionPositionRefuel++, null);
                    receipt.addDataToSection(sectionRefuel, "PRODUCT_DESCRIPTION", "TIPO DI PRODOTTO", null, nonOilDetailElement.getProductDescription(),
                            sectionPositionRefuel++, null);
                }
            }

            if (!loyaltyTransactiondetails.getRefuelDetails().isEmpty()) {
                for (LoyaltyTransactionRefuel refuelDetailElement : loyaltyTransactiondetails.getRefuelDetails()) {
                    if (refuelDetailElement.getPumpNumber() != null) {
                        receipt.addDataToSection(sectionRefuel, "REFUEL_PUMP_NUMBER", "EROGATORE", null, refuelDetailElement.getPumpNumber().toString(), sectionPositionRefuel++, null);
                    }
                    
                    String refuelQuantity = "0.0";
                    if (refuelDetailElement.getFuelQuantity() != null) {
                        refuelQuantity = numberFormat.format(refuelDetailElement.getFuelQuantity().doubleValue());
                    }
                    
                    receipt.addDataToSection(sectionRefuel, "REFUEL_FUEL_QUANTITY", "EROGATO", "lt", refuelQuantity, sectionPositionRefuel++, null);
                    receipt.addDataToSection(sectionRefuel, "REFUEL_FUEL_TYPE", "TIPO DI CARBURANTE", null, refuelDetailElement.getProductDescription(), sectionPositionRefuel++,
                            null);

                    String refuelAmount = "0.0";
                    if (refuelDetailElement.getAmount() != null) {
                        refuelAmount = currencyFormat.format(refuelDetailElement.getAmount());
                    }
                    
                    if (refuelDetailElement.getAmount() != null && refuelDetailElement.getAmount() > 0.0) {
                        receipt.addDataToSection(sectionRefuel, "REFUEL_AMOUNT", "IMPORTO CARBURANTE", "�", refuelAmount, sectionPositionRefuel++, null);
                    }
                }
            }

            // END SECTION REFUEL
        }

        // START REWARD
        if (eventNotificationResponse.getRewardTransactionDetail() != null) {
            for (RewardTransactionParameter rewardParameter : eventNotificationResponse.getRewardTransactionDetail().getParameterDetails()) {
                String label = rewardParameter.getParameterID().toUpperCase();
                String key = rewardParameter.getParameterID().toUpperCase().replaceAll(" ", "_");

                if (label.equals("TESTO DA DWH")) {
                    label = null;
                    continue;
                }

                receipt.addDataToSection(sectionReward, key, label, null, rewardParameter.getParameterValue(), sectionPositionReward++, null);
            }
        }
        else {
            receipt.removeSection(sectionReward);
        }
        // END REWARD

        status.setStatusCode(eventNotificationResponse.getStatusCode());

        retrieveLoyaltyTransactionDetailBodyResponse.setReceipt(receipt);
        retrieveLoyaltyTransactionDetailResponse.setBody(retrieveLoyaltyTransactionDetailBodyResponse);
        retrieveLoyaltyTransactionDetailResponse.setStatus(status);

        return retrieveLoyaltyTransactionDetailResponse;
    }
}
