package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbympcode;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrievePoPTransactionByMPcodeBodyRequest implements Validable {
	
	private String code;

	public String getMPcode() {
		return code;
	}

	public void setMPCode(String mpCode) {
		this.code = mpCode;
	}


	@Override
	public Status check() {
		
		Status status = new Status();		
		
		if(this.code == null || this.code.length() != 32 || this.code.trim() == "") {
			
			status.setStatusCode(StatusCode.POP_GET_ID_WRONG);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.POP_GET_SUCCESS);
		
		return status;
	}
	
}