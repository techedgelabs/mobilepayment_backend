package com.techedge.mp.frontend.adapter.entities.user.v2.getpartneractionurl;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.PartnerActionUrlResponse;
import com.techedge.mp.core.business.interfaces.PartnerDetailInfoData;
import com.techedge.mp.core.business.interfaces.PartnerListInfoDataResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.PartnerInfo;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetPartnerActionUrlRequest extends AbstractRequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    private GetPartnerActionUrlRequestBody body;


    public GetPartnerActionUrlRequestBody getBody() {
        return body;
    }

    public void setBody(GetPartnerActionUrlRequestBody body) {
        this.body = body;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-ACTION-URL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }
        
        if(this.body==null){
            status.setStatusCode(ResponseHelper.USER_V2_GET_PARTNER_ACTION_URL_FAILURE);
            return status;
        }
        
        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        else {

            status.setStatusCode(StatusCode.GET_ACTION_URL_SUCCESS);

        }

        return status;

    }

    @Override
    public BaseResponse execute() {

        GetPartnerActionUrlResponse getPartnerActionUrlResponse = new GetPartnerActionUrlResponse();
        GetPartnerActionUrlBodyResponse getPartnerActionUrlBodyResponse = new GetPartnerActionUrlBodyResponse();
        
        PartnerActionUrlResponse response = getUserV2ServiceRemote().getPartnerActionUrl(this.getCredential().getRequestID(),
                this.getCredential().getTicketID(), this.getBody().getPartnerId());


        status.setStatusCode(response.getStatusCode());
        status.setStatusMessage(prop.getProperty(response.getStatusCode()));
        String partnerActionUrl=response.getPartnerActionUrl();
//        if(response!=null && partnerActionUrl !=null && (! partnerActionUrl.isEmpty())){
            
            getPartnerActionUrlBodyResponse.setUrl(response.getPartnerActionUrl());
            getPartnerActionUrlBodyResponse.setKey(response.getKey());
            getPartnerActionUrlBodyResponse.setActionForwardId(response.getActionForwardId());
            
            status.setStatusCode(response.getStatusCode());
            status.setStatusMessage(prop.getProperty(response.getStatusCode()));
            
            getPartnerActionUrlResponse.setBody(getPartnerActionUrlBodyResponse);
//        }
        
        status.setStatusMessage(prop.getProperty(response.getStatusCode()));
        getPartnerActionUrlResponse.setStatus(status);
        
        return getPartnerActionUrlResponse;

    }

}
