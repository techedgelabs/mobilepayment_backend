package com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.PaymentRefuelDetailResponse;
import com.techedge.mp.core.business.interfaces.PaymentRefuelHistoryResponse;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Receipt;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveRefuelPaymentHistoryRequest extends AbstractRequest implements Validable {

    private Credential                              credential;
    private RetrieveRefuelPaymentHistoryBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveRefuelPaymentHistoryBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveRefuelPaymentHistoryBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PAY-HISTORY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.REFUEL_EXEC_PAY_HISTORY_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        System.out.println("Retrieve Fuel Payment History");

        RetrieveRefuelPaymentHistoryRequest retrieveRefuelPaymentHistoryRequest = this;

        Boolean details = retrieveRefuelPaymentHistoryRequest.getBody().getDetails();

        PaymentRefuelHistoryResponse paymentRefuelHistoryResponse = getTransactionServiceRemote().retrieveRefuelPaymentHistory(
                retrieveRefuelPaymentHistoryRequest.getCredential().getRequestID(), retrieveRefuelPaymentHistoryRequest.getCredential().getTicketID(),
                retrieveRefuelPaymentHistoryRequest.getBody().getPageOffset(), retrieveRefuelPaymentHistoryRequest.getBody().getItemsLimit(),
                retrieveRefuelPaymentHistoryRequest.getBody().getStartDate().toDate(), retrieveRefuelPaymentHistoryRequest.getBody().getEndDate().toDate(), details);

        RetrieveRefuelPaymentHistoryResponse retrieveRefuelPaymentHistoryResponse = new RetrieveRefuelPaymentHistoryResponse();

        Status statusResponse = new Status();
        statusResponse.setStatusCode(paymentRefuelHistoryResponse.getStatusCode());
        statusResponse.setStatusMessage(prop.getProperty(paymentRefuelHistoryResponse.getStatusCode()));

        retrieveRefuelPaymentHistoryResponse.setStatus(statusResponse);

        if (Validator.isValid(paymentRefuelHistoryResponse.getStatusCode())) {

            List<PaymentRefuelDetailResponse> paymentRefuelDetailResponseList = paymentRefuelHistoryResponse.getPaymentRefuelDetailHistory();

            List<RetrieveRefuelPaymentHistoryListResponse> retrieveRefuelPaymentHistoryListResponseArray = new ArrayList<RetrieveRefuelPaymentHistoryListResponse>(0);

            RetrieveRefuelPaymentHistoryBodyResponse retrieveRefuelPaymentHistoryBodyResponse = new RetrieveRefuelPaymentHistoryBodyResponse();
            retrieveRefuelPaymentHistoryBodyResponse.setPageOffset(paymentRefuelHistoryResponse.getPageOffset());
            retrieveRefuelPaymentHistoryBodyResponse.setPagesTotal(paymentRefuelHistoryResponse.getPageTotal());

            RetrieveRefuelPaymentHistoryListResponse retrieveRefuelPaymentHistoryListResponse = null;

            for (PaymentRefuelDetailResponse paymentRefuelDetailResponse : paymentRefuelDetailResponseList) {

                retrieveRefuelPaymentHistoryListResponse = new RetrieveRefuelPaymentHistoryListResponse();

                if (details != null && details == true) {

                    retrieveRefuelPaymentHistoryListResponse.setRefuelID(paymentRefuelDetailResponse.getRefuelID());
                    retrieveRefuelPaymentHistoryListResponse.setStatus(paymentRefuelDetailResponse.getStatus());
                    retrieveRefuelPaymentHistoryListResponse.setSubStatus(paymentRefuelDetailResponse.getSubStatus());
                    retrieveRefuelPaymentHistoryListResponse.setUseVoucher(paymentRefuelDetailResponse.getUseVoucher());
                    retrieveRefuelPaymentHistoryListResponse.setInitialAmount(AmountConverter.toMobile(paymentRefuelDetailResponse.getInitialAmount()));

                    // Valorizzazione scontrino
                    Receipt receipt = new Receipt();
                    receipt.setRefuelID(paymentRefuelDetailResponse.getRefuelID());
                    receipt.setDate(CustomTimestamp.createCustomTimestamp(paymentRefuelDetailResponse.getDate()));
                    receipt.setFinalAmount(AmountConverter.toMobile(paymentRefuelDetailResponse.getFinalAmount()));
                    receipt.setFuelAmount(AmountConverter.toMobile3(paymentRefuelDetailResponse.getFuelAmount()));
                    receipt.setFuelQuantity(paymentRefuelDetailResponse.getFuelQuantity());
                    receipt.setBankTransactionID(paymentRefuelDetailResponse.getBankTransactionID());
                    receipt.setShopLogin(paymentRefuelDetailResponse.getShopLogin());
                    receipt.setCurrency(paymentRefuelDetailResponse.getCurrency());
                    receipt.setMaskedPan(paymentRefuelDetailResponse.getMaskedPan());
                    receipt.setAuthCode(paymentRefuelDetailResponse.getAuthCode());
                    retrieveRefuelPaymentHistoryListResponse.setReceipt(receipt);

                    // Valorizzazione stazione
                    LocationData locationData = new LocationData();

                    String address = paymentRefuelDetailResponse.getSelectedStationAddress();
                    String city = paymentRefuelDetailResponse.getSelectedStationCity();
                    String province = paymentRefuelDetailResponse.getSelectedStationProvince();
                    String country = paymentRefuelDetailResponse.getSelectedStationCountry();

                    locationData.setAddress(address);
                    locationData.setCity(city);
                    locationData.setProvince(province);
                    locationData.setCountry(country);
                    locationData.setLatitude(paymentRefuelDetailResponse.getSelectedStationLatitude());
                    locationData.setLongitude(paymentRefuelDetailResponse.getSelectedStationLongitude());

                    Station station = new Station();
                    station.setStationID(paymentRefuelDetailResponse.getSelectedStationID());
                    station.setName(paymentRefuelDetailResponse.getSelectedStationName());
                    station.setLocationData(locationData);
                    retrieveRefuelPaymentHistoryListResponse.setStation(station);

                    // Valorizzazione pompa
                    Pump pump = new Pump();
                    pump.setPumpID(paymentRefuelDetailResponse.getSelectedPumpID());
                    pump.setNumber(paymentRefuelDetailResponse.getSelectedPumpNumber());
                    pump.getFuelType().add(paymentRefuelDetailResponse.getSelectedPumpFuelType());
                    retrieveRefuelPaymentHistoryListResponse.setPump(pump);
                }
                else {

                    retrieveRefuelPaymentHistoryListResponse.setRefuelID(paymentRefuelDetailResponse.getRefuelID());
                    retrieveRefuelPaymentHistoryListResponse.setStatus(paymentRefuelDetailResponse.getStatus());
                    retrieveRefuelPaymentHistoryListResponse.setSubStatus(paymentRefuelDetailResponse.getSubStatus());

                    // Valorizzazione scontrino
                    Receipt receipt = new Receipt();
                    receipt.setDate(CustomTimestamp.createCustomTimestamp(paymentRefuelDetailResponse.getDate()));
                    receipt.setFinalAmount(AmountConverter.toMobile(paymentRefuelDetailResponse.getFinalAmount()));
                    retrieveRefuelPaymentHistoryListResponse.setReceipt(receipt);
                }

                retrieveRefuelPaymentHistoryListResponseArray.add(retrieveRefuelPaymentHistoryListResponse);
            }

            retrieveRefuelPaymentHistoryBodyResponse.setPaymentHistory(retrieveRefuelPaymentHistoryListResponseArray);

            retrieveRefuelPaymentHistoryResponse.setBody(retrieveRefuelPaymentHistoryBodyResponse);

        }

        return retrieveRefuelPaymentHistoryResponse;
    }

}
