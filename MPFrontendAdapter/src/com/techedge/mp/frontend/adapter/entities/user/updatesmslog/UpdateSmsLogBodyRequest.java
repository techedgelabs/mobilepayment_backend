package com.techedge.mp.frontend.adapter.entities.user.updatesmslog;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UpdateSmsLogBodyRequest implements Validable {

    private String  CorrelationId;
    private String  MessageId;
    private String  DestinationAddress;
    private Integer StatusCode;
    private Integer ReasonCode;
    private String  OperatorTimeStamp;
    private String  TimeStamp;
    private String  Operator;
    private String  StatusText;
    private Integer OperatorNetworkCode;
    private String  Message;

    public String getCorrelationId() {
        return CorrelationId;
    }

    public void setCorrelationId(String correlationId) {
        CorrelationId = correlationId;
    }

    public String getMessageId() {
        return MessageId;
    }

    public void setMessageId(String messageId) {
        MessageId = messageId;
    }

    public String getDestinationAddress() {
        return DestinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        DestinationAddress = destinationAddress;
    }

    public Integer getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(Integer statusCode) {
        StatusCode = statusCode;
    }

    public Integer getReasonCode() {
        return ReasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        ReasonCode = reasonCode;
    }

    public String getOperatorTimeStamp() {
        return OperatorTimeStamp;
    }

    public void setOperatorTimeStamp(String operatorTimeStamp) {
        OperatorTimeStamp = operatorTimeStamp;
    }

    public String getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        TimeStamp = timeStamp;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getStatusText() {
        return StatusText;
    }

    public void setStatusText(String statusText) {
        StatusText = statusText;
    }

    public Integer getOperatorNetworkCode() {
        return OperatorNetworkCode;
    }

    public void setOperatorNetworkCode(Integer operatorNetworkCode) {
        OperatorNetworkCode = operatorNetworkCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.CorrelationId == null && this.MessageId == null) {

            status.setStatusCode(com.techedge.mp.frontend.adapter.entities.common.StatusCode.UPDATE_SMS_LOG_INVALID_REQUEST);
            return status;

        }

        status.setStatusCode(com.techedge.mp.frontend.adapter.entities.common.StatusCode.UPDATE_SMS_LOG_SUCCESS);

        return status;

    }

}
