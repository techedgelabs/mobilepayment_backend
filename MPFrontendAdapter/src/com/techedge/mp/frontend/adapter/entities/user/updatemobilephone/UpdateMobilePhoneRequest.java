package com.techedge.mp.frontend.adapter.entities.user.updatemobilephone;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class UpdateMobilePhoneRequest extends AbstractRequest implements Validable {

    private Status                       status = new Status();

    private Credential                   credential;
    private UpdateMobilePhoneRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateMobilePhoneRequestBody getBody() {
        return body;
    }

    public void setBody(UpdateMobilePhoneRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPD-MOBILE-PHONE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {
            status.setStatusCode(StatusCode.UPD_MOBILE_PHONE_FAILURE);
            return status;
        }

        status.setStatusCode(StatusCode.UPD_MOBILE_PHONE_SUCCESS);
        status.setStatusMessage("Mobile phone number updated");

        return status;

    }

    @Override
    public BaseResponse execute() {
        UpdateMobilePhoneRequest updateMobilePhoneRequest = this;

        String response = getUserServiceRemote().updateMobilePhone(updateMobilePhoneRequest.getCredential().getTicketID(), updateMobilePhoneRequest.getCredential().getRequestID(),
                updateMobilePhoneRequest.getBody().getMobilePhone().getPrefix(), updateMobilePhoneRequest.getBody().getMobilePhone().getNumber());

        UpdateMobilePhoneResponse cancelMobilePhoneResponse = new UpdateMobilePhoneResponse();

        status.setStatusCode(response);

        status.setStatusMessage(prop.getProperty(response));

        cancelMobilePhoneResponse.setStatus(status);

        return cancelMobilePhoneResponse;
    }

}
