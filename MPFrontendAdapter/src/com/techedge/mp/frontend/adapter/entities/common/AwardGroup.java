package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;

public class AwardGroup {

    private Integer    brandId;
    
    private  List<AwardData> awardDataList = new ArrayList<AwardData>(0);

    public Integer getBrandId() {
        return brandId;
    }

    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }

    public List<AwardData> getAwardDataList() {
        return awardDataList;
    }

    public void setAwardDataList(List<AwardData> awardDataList) {
        this.awardDataList = awardDataList;
    }
    
    

}
