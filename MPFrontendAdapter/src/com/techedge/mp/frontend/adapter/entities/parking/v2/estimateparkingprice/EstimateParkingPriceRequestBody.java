package com.techedge.mp.frontend.adapter.entities.parking.v2.estimateparkingprice;

import java.util.Date;

import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class EstimateParkingPriceRequestBody implements Validable {

    private String          plateNumber;
    private CustomTimestamp requestedEndTime;
    private String          parkingZoneId;
    private String          stallCode;
    private String          cityId;

    @Override
    public Status check() {

        Status status = new Status();
        try {
            Date reqEndTime = CustomTimestamp.convertToDate(requestedEndTime);
        }
        catch (Exception e) {
            status.setStatusCode(StatusCode.ESTIMATE_PARKING_PRICE_INVALID_REQUEST);

            return status;
        }

        if (this.plateNumber == null || this.plateNumber.isEmpty()) {

            status.setStatusCode(StatusCode.ESTIMATE_PARKING_PRICE_INVALID_REQUEST);

            return status;

        }

        if (this.parkingZoneId == null || this.parkingZoneId.isEmpty()) {

            status.setStatusCode(StatusCode.ESTIMATE_PARKING_PRICE_INVALID_REQUEST);

            return status;

        }
        
        if (this.cityId == null || this.cityId.isEmpty()) {

            status.setStatusCode(StatusCode.ESTIMATE_PARKING_PRICE_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ESTIMATE_PARKING_PRICE_SUCCESS);

        return status;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public CustomTimestamp getRequestedEndTime() {
        return requestedEndTime;
    }

    public void setRequestedEndTime(CustomTimestamp requestedEndTime) {
        this.requestedEndTime = requestedEndTime;
    }

    public String getParkingZoneId() {
        return parkingZoneId;
    }

    public void setParkingZoneId(String parkingZoneId) {
        this.parkingZoneId = parkingZoneId;
    }

    public String getStallCode() {
        return stallCode;
    }

    public void setStallCode(String stallCode) {
        this.stallCode = stallCode;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

}
