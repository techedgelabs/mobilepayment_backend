package com.techedge.mp.frontend.adapter.entities.survey.getsurvey;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Survey;
import com.techedge.mp.frontend.adapter.entities.common.SurveyQuestion;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetSurveyRequest extends AbstractRequest implements Validable {

    private Credential           credential;
    private GetSurveyBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GetSurveyBodyRequest getBody() {
        return body;
    }

    public void setBody(GetSurveyBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-SURVEY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {
            status.setStatusCode(StatusCode.SURVEY_REQU_INVALID_REQUEST);
            return status;
        }

        status.setStatusCode(StatusCode.SURVEY_GET_SURVEY_SUCCESS);

        return status;
    }

    @Override
    public BaseResponse execute() {
        GetSurveyRequest getSurveyRequest = this;
        GetSurveyBodyResponse getSurveyBodyResponse = new GetSurveyBodyResponse();
        GetSurveyResponse getSurveyResponse = new GetSurveyResponse();

        com.techedge.mp.core.business.interfaces.Survey coreSurvey = getSurveyServiceRemote().surveyGet(getSurveyRequest.getCredential().getTicketID(),
                getSurveyRequest.getCredential().getRequestID(), getSurveyRequest.getBody().getCode());

        Survey survey = new Survey();
        survey.setCode(coreSurvey.getCode());
        survey.setDescription(coreSurvey.getDescription());
        survey.setNote(coreSurvey.getNote());

        for (com.techedge.mp.core.business.interfaces.SurveyQuestion coreQuestion : coreSurvey.getQuestions()) {
            SurveyQuestion question = new SurveyQuestion();
            question.setCode(coreQuestion.getCode());
            question.setSequence(coreQuestion.getSequence());
            question.setText(coreQuestion.getText());
            question.setType(coreQuestion.getType());
            survey.getQuestions().add(question);
        }

        getSurveyBodyResponse.setSurvey(survey);
        getSurveyResponse.setBody(getSurveyBodyResponse);

        Status statusResponse = new Status();
        statusResponse.setStatusCode(coreSurvey.getStatusCode());
        statusResponse.setStatusMessage(prop.getProperty(statusResponse.getStatusCode()));
        getSurveyResponse.setStatus(statusResponse);

        return getSurveyResponse;
    }

}
