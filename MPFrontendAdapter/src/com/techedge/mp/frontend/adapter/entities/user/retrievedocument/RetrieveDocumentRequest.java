package com.techedge.mp.frontend.adapter.entities.user.retrievedocument;

import com.techedge.mp.core.business.interfaces.RetrieveDocumentData;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveDocumentRequest extends AbstractRequest implements Validable {

    private Status               status = new Status();

    private Credential           credential;
    private RetrieveDocumentBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveDocumentBody getBody() {
        return body;
    }

    public void setBody(RetrieveDocumentBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CITIES", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        RetrieveDocumentRequest retrieveDocumentRequest = this;
        RetrieveDocumentData retrieveDocumentsData = getUserServiceRemote().retrieveDocument(retrieveDocumentRequest.getCredential().getTicketID(),
                retrieveDocumentRequest.getCredential().getRequestID(), retrieveDocumentRequest.getBody().getDocumentId());

        RetrieveDocumentResponse retrieveDocumentResponse = new RetrieveDocumentResponse();

        status.setStatusCode(retrieveDocumentsData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveDocumentsData.getStatusCode()));

        RetrieveDocumentResponseBody retrieveDocumentResponseBody = new RetrieveDocumentResponseBody();

        retrieveDocumentResponseBody.setDocumentBase64(retrieveDocumentsData.getDocumentBase64());
        retrieveDocumentResponseBody.setDimension(retrieveDocumentsData.getDimension());
        retrieveDocumentResponseBody.setFilename(retrieveDocumentsData.getFilename());
        retrieveDocumentResponse.setBody(retrieveDocumentResponseBody);
        retrieveDocumentResponse.setStatus(status);

        RetrieveDocumentResponse retrieveDocumentModResponse = new RetrieveDocumentResponse();
        RetrieveDocumentResponseBody retrieveDocumentModResponseBody = new RetrieveDocumentResponseBody();
        if (retrieveDocumentsData.getDocumentBase64() != null && retrieveDocumentsData.getDocumentBase64().length() > 51) {
            retrieveDocumentModResponseBody.setDocumentBase64(retrieveDocumentsData.getDocumentBase64().substring(0, 50) + "...");
        }
        else if (retrieveDocumentsData.getDocumentBase64() != null && retrieveDocumentsData.getDocumentBase64().length() < 51) {
            retrieveDocumentModResponseBody.setDocumentBase64(retrieveDocumentsData.getDocumentBase64());

        }
        retrieveDocumentModResponseBody.setDimension(retrieveDocumentsData.getDimension());
        retrieveDocumentModResponseBody.setFilename(retrieveDocumentsData.getFilename());
        retrieveDocumentModResponse.setBody(retrieveDocumentModResponseBody);
        retrieveDocumentModResponse.setStatus(status);

        return retrieveDocumentResponse;
    }

}
