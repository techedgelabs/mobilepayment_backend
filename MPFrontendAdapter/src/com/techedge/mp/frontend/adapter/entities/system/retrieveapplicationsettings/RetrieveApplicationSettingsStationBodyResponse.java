package com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings;


public class RetrieveApplicationSettingsStationBodyResponse {

	private String gpsEnabled;
	private String beaconEnabled;
	
	public String getGpsEnabled() {
		return gpsEnabled;
	}
	public void setGpsEnabled(String gpsEnabled) {
		this.gpsEnabled = gpsEnabled;
	}
	
	public String getBeaconEnabled() {
		return beaconEnabled;
	}
	public void setBeaconEnabled(String beaconEnabled) {
		this.beaconEnabled = beaconEnabled;
	}
}
