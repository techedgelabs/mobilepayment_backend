package com.techedge.mp.frontend.adapter.entities.user.retrievedocument;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveDocumentResponse extends BaseResponse {

	private RetrieveDocumentResponseBody body;

	public RetrieveDocumentResponseBody getBody() {
		return body;
	}
	public void setBody(RetrieveDocumentResponseBody body) {
		this.body = body;
	}
}
