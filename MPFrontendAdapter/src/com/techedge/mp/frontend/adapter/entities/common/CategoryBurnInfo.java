package com.techedge.mp.frontend.adapter.entities.common;

public class CategoryBurnInfo {
    
    private Integer categoryId;
    private String category;
    private String categoryImageUrl;
    
    public Integer getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getCategoryImageUrl() {
        return categoryImageUrl;
    }
    public void setCategoryImageUrl(String categoryImageUrl) {
        this.categoryImageUrl = categoryImageUrl;
    }
    
}
