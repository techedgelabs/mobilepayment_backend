package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.postpaid.v2.approvemulticardtransaction.ApproveMulticardTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.v2.approvetransaction.ApproveTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievesourcedetail.RetrieveSourceDetailRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievestation.RetrieveStationRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievestationdetail.RetrieveStationDetailRequest;

public class PoPV2Request extends RootRequest {

    protected ApproveTransactionRequest          approvePoPTransaction;
    protected ApproveMulticardTransactionRequest approveMulticardPoPTransaction;
    protected RetrieveSourceDetailRequest        retrieveSourceDetail;
    protected RetrieveSourceDetailRequest        retrievePoPSourceDetail;
    protected RetrieveStationDetailRequest       retrievePoPStationDetail;
    protected RetrieveStationRequest             retrieveStation;

    public ApproveTransactionRequest getApprovePoPTransaction() {
        return approvePoPTransaction;
    }

    public void setApprovePoPTransaction(ApproveTransactionRequest approvePoPTransaction) {
        this.approvePoPTransaction = approvePoPTransaction;
    }

    public RetrieveSourceDetailRequest getRetrieveSourceDetail() {
        return retrieveSourceDetail;
    }

    public void setRetrieveSourceDetail(RetrieveSourceDetailRequest retrieveSourceDetail) {
        this.retrieveSourceDetail = retrieveSourceDetail;
    }
    
    public RetrieveSourceDetailRequest getRetrievePoPSourceDetail() {
        return retrievePoPSourceDetail;
    }

    public void setRetrievePoPSourceDetail(RetrieveSourceDetailRequest retrievePoPSourceDetail) {
        this.retrievePoPSourceDetail = retrievePoPSourceDetail;
    }

    public RetrieveStationDetailRequest getRetrievePoPStationDetail() {
        return retrievePoPStationDetail;
    }

    public void setRetrievePoPStationDetail(RetrieveStationDetailRequest retrievePoPStationDetail) {
        this.retrievePoPStationDetail = retrievePoPStationDetail;
    }

    public RetrieveStationRequest getRetrieveStation() {
        return retrieveStation;
    }

    public void setRetrieveStation(RetrieveStationRequest retrieveStation) {
        this.retrieveStation = retrieveStation;
    }

    public ApproveMulticardTransactionRequest getApproveMulticardPoPTransaction() {
        return approveMulticardPoPTransaction;
    }

    public void setApproveMulticardPoPTransaction(ApproveMulticardTransactionRequest approveMulticardPoPTransaction) {
        this.approveMulticardPoPTransaction = approveMulticardPoPTransaction;
    }
    
}