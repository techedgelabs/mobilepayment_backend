package com.techedge.mp.frontend.adapter.entities.parking.v2.estimateextendedparkingprice;

import java.util.Date;

import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class EstimateExtendedParkingPriceRequestBody implements Validable {

    private CustomTimestamp requestedEndTime;
    private String          transactionID;

    @Override
    public Status check() {

        Status status = new Status();
        try {
            Date reqEndTime = CustomTimestamp.convertToDate(requestedEndTime);
        }
        catch (Exception e) {
            status.setStatusCode(StatusCode.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_FAILURE);

            return status;
        }

        if (this.transactionID == null || this.transactionID.isEmpty()) {

            status.setStatusCode("TBD");

            return status;

        }

        status.setStatusCode(StatusCode.POSTPAID_V2_APPROVE_TRANSACTION_SUCCESS);

        return status;
    }

    public CustomTimestamp getRequestedEndTime() {
        return requestedEndTime;
    }

    public void setRequestedEndTime(CustomTimestamp requestedEndTime) {
        this.requestedEndTime = requestedEndTime;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

}
