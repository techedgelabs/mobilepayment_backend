package com.techedge.mp.frontend.adapter.entities.common;

public enum EnumCodeType {
	
	GPS,
	BEACON_CODE,
	ID
}
