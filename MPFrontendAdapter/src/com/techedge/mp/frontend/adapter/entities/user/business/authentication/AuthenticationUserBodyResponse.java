package com.techedge.mp.frontend.adapter.entities.user.business.authentication;

import java.util.ArrayList;
import java.util.List;

public class AuthenticationUserBodyResponse {

	private String ticketID;
	private AuthenticationUserDataResponse userData;
    private List<String> notEditableFields = new ArrayList<String>(0);
	
	public String getTicketID() {
		return ticketID;
	}

	public void setTicketID(String ticketID) {
		this.ticketID = ticketID;
	}

	public AuthenticationUserDataResponse getUserData() {
		return userData;
	}
	
	public void setUserData(AuthenticationUserDataResponse userData) {
		this.userData = userData;
	}
	
	public List<String> getNotEditableFields() {
        return notEditableFields;
    }
	
	public void setNotEditableFields(List<String> notEditableFields) {
        this.notEditableFields = notEditableFields;
    }
}
