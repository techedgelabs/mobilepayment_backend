package com.techedge.mp.frontend.adapter.entities.user.v2.externalauthentication;

import java.util.ArrayList;
import java.util.List;

public class ExternalAuthenticationUserResponseBody {

    private String                               ticketID;
    private String                               loyaltySessionID;
    private ExternalAuthenticationUserDataResponse userData;
    private List<String>                         notEditableFields = new ArrayList<String>(0);

    public String getTicketID() {
        return ticketID;
    }

    public void setTicketID(String ticketID) {
        this.ticketID = ticketID;
    }

    public String getLoyaltySessionID() {
        return loyaltySessionID;
    }

    public void setLoyaltySessionID(String loyaltySessionID) {
        this.loyaltySessionID = loyaltySessionID;
    }

    public ExternalAuthenticationUserDataResponse getUserData() {
        return userData;
    }

    public void setUserData(ExternalAuthenticationUserDataResponse userData) {
        this.userData = userData;
    }

    public List<String> getNotEditableFields() {
        return notEditableFields;
    }

    public void setNotEditableFields(List<String> notEditableFields) {
        this.notEditableFields = notEditableFields;
    }

}
