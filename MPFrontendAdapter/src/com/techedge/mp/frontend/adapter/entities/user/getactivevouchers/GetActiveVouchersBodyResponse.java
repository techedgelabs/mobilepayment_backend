package com.techedge.mp.frontend.adapter.entities.user.getactivevouchers;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.VoucherData;

public class GetActiveVouchersBodyResponse {

    private Integer           total;

    private List<VoucherData> vouchers        = new ArrayList<VoucherData>(0);
    private List<VoucherData> parkingVouchers = new ArrayList<VoucherData>(0);

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<VoucherData> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<VoucherData> vouchers) {
        this.vouchers = vouchers;
    }

    public List<VoucherData> getParkingVouchers() {
        return parkingVouchers;
    }

    public void setParkingVouchers(List<VoucherData> parkingVouchers) {
        this.parkingVouchers = parkingVouchers;
    }
    
}
