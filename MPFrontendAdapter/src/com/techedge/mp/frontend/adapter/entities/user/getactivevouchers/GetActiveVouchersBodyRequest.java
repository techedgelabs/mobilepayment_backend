package com.techedge.mp.frontend.adapter.entities.user.getactivevouchers;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class GetActiveVouchersBodyRequest implements Validable {

    private Boolean refresh;
    private Boolean noDetail;

    public Boolean getRefresh() {
        return refresh;
    }
    public void setRefresh(Boolean refresh) {
        this.refresh = refresh;
    }

    public Boolean getNoDetail() {
        return noDetail;
    }
    public void setNoDetail(Boolean noDetail) {
        this.noDetail = noDetail;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.refresh == null) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_GET_ACTIVE_VOUCHERS_SUCCESS);

        return status;

    }

}
