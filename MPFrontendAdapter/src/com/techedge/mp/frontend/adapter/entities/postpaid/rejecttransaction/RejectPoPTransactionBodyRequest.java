package com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RejectPoPTransactionBodyRequest implements Validable {
	
	private String transactionId;

	public String getTransactionId() {
		return transactionId;
	}

	public void setTransactionId(String mpCode) {
		this.transactionId = mpCode;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.transactionId == null || this.transactionId.length() != 32 || this.transactionId.trim() == "") {
			
			status.setStatusCode(StatusCode.POP_REJECT_ID_WRONG);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.POP_REJECT_SUCCESS);
		
		return status;
	}
	
}