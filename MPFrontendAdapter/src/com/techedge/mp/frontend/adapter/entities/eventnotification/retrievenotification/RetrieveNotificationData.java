package com.techedge.mp.frontend.adapter.entities.eventnotification.retrievenotification;

import com.techedge.mp.core.business.interfaces.AppLink;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;

public class RetrieveNotificationData {

    private Long            id;
    private String          type;
    private String          title;
    private String          text;
    private String          actionText;
    private String          actionLocation;
    private CustomTimestamp sendDate;
    private CustomTimestamp expireDate;
    private Boolean         showDialog;
    private AppLink         appLink;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public CustomTimestamp getSendDate() {
        return sendDate;
    }

    public void setSendDate(CustomTimestamp sendDate) {
        this.sendDate = sendDate;
    }

    public CustomTimestamp getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(CustomTimestamp expireDate) {
        this.expireDate = expireDate;
    }

    public AppLink getAppLink() {
        return appLink;
    }

    public void setAppLink(AppLink appLink) {
        this.appLink = appLink;
    }

    public String getActionText() {
        return actionText;
    }

    public void setActionText(String actionText) {
        this.actionText = actionText;
    }

    public String getActionLocation() {
        return actionLocation;
    }

    public void setActionLocation(String actionLocation) {
        this.actionLocation = actionLocation;
    }

    public Boolean getShowDialog() {
        return showDialog;
    }

    public void setShowDialog(Boolean showDialog) {
        this.showDialog = showDialog;
    }

    //private appLink TBD

}
