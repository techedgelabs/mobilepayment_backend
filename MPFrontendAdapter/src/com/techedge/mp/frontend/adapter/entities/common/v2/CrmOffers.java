package com.techedge.mp.frontend.adapter.entities.common.v2;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.AppLink;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;

public class CrmOffers implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7670459508389153156L;

    private String            description;                            //description

    private String            code;

    private String            name;

    private AppLink           appLink          = new AppLink();
    
    private CustomDate        date;

    public String getDescription() {
        return description;
    }

    public void setDescription(String content) {
        this.description = content;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AppLink getAppLink() {
        return appLink;
    }

    public void setAppLink(AppLink appLink) {
        this.appLink = appLink;
    }

    public CustomDate getDate() {
        return date;
    }

    public void setDate(CustomDate date) {
        this.date = date;
    }

}
