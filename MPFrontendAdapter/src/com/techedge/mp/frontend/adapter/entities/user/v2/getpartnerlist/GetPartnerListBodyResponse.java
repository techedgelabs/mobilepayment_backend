package com.techedge.mp.frontend.adapter.entities.user.v2.getpartnerlist;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.CategoryEarnInfo;
import com.techedge.mp.frontend.adapter.entities.common.PartnerInfo;

public class GetPartnerListBodyResponse{
    
    private List<PartnerInfo> partnerList = new ArrayList<PartnerInfo>(0);
    private List<CategoryEarnInfo> categoryEarnList = new ArrayList<CategoryEarnInfo>(0);

    public List<PartnerInfo> getPartnerList() {
        return partnerList;
    }
    
    public void setPartnerList(List<PartnerInfo> partnerList) {
        this.partnerList = partnerList;
    }

    public List<CategoryEarnInfo> getCategoryEarnList() {
        return categoryEarnList;
    }

    public void setCategoryEarnList(List<CategoryEarnInfo> categoryEarnList) {
        this.categoryEarnList = categoryEarnList;
    }
    

}
