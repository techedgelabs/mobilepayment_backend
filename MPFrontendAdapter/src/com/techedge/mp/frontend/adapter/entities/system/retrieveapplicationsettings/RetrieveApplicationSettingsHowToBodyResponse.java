package com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings;


public class RetrieveApplicationSettingsHowToBodyResponse {

	private Integer pageCount;
	private String baseUrl;
	
	public Integer getPageCount() {
		return pageCount;
	}
	public void setPageCount(Integer pageCount) {
		this.pageCount = pageCount;
	}
	
	public String getBaseUrl() {
		return baseUrl;
	}
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}
}
