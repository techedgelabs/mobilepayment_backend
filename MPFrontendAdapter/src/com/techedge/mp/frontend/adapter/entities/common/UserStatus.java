package com.techedge.mp.frontend.adapter.entities.common;

public class UserStatus {

    private int     status;
    private boolean registrationCompleted;
    private boolean newUserType;
    private boolean termsOfServiceAccepted;
    private boolean virtualizationCompleted;
    private boolean loyaltyCheckEnabled;
    private boolean eniStationUserType;
    private boolean DepositCardStepCompleted;
    private boolean updatedMission;
    private boolean parkingTransactionPending;
    private String  type;

    public UserStatus() {}

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isRegistrationCompleted() {
        return registrationCompleted;
    }

    public void setRegistrationCompleted(boolean registrationCompleted) {
        this.registrationCompleted = registrationCompleted;
    }

    public boolean isNewUserType() {
        return newUserType;
    }

    public void setNewUserType(boolean newUserType) {
        this.newUserType = newUserType;
    }

    public boolean isTermsOfServiceAccepted() {
        return termsOfServiceAccepted;
    }

    public void setTermsOfServiceAccepted(boolean termsOfServiceAccepted) {
        this.termsOfServiceAccepted = termsOfServiceAccepted;
    }

    public boolean isVirtualizationCompleted() {
        return virtualizationCompleted;
    }

    public void setVirtualizationCompleted(boolean virtualizationCompleted) {
        this.virtualizationCompleted = virtualizationCompleted;
    }

    public boolean isLoyaltyCheckEnabled() {
        return loyaltyCheckEnabled;
    }

    public void setLoyaltyCheckEnabled(boolean loyaltyCheckEnabled) {
        this.loyaltyCheckEnabled = loyaltyCheckEnabled;
    }

    public boolean isEniStationUserType() {
        return eniStationUserType;
    }

    public void setEniStationUserType(boolean eniStationUserType) {
        this.eniStationUserType = eniStationUserType;
    }

    public boolean isDepositCardStepCompleted() {
        return DepositCardStepCompleted;
    }

    public void setDepositCardStepCompleted(boolean depositCardStepCompleted) {
        DepositCardStepCompleted = depositCardStepCompleted;
    }

    public boolean isUpdatedMission() {
        return updatedMission;
    }

    public void setUpdatedMission(boolean updatedMission) {
        this.updatedMission = updatedMission;
    }

    public boolean isParkingTransactionPending() {
        return parkingTransactionPending;
    }

    public void setParkingTransactionPending(boolean parkingTransactionPending) {
        this.parkingTransactionPending = parkingTransactionPending;
    }
    
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
