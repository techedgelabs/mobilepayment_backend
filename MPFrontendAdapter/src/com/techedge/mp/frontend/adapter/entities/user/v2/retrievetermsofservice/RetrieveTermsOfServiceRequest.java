package com.techedge.mp.frontend.adapter.entities.user.v2.retrievetermsofservice;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.DocumentCheckInfo;
import com.techedge.mp.core.business.interfaces.DocumentInfo;
import com.techedge.mp.core.business.interfaces.RetrieveTermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.FrontendParameter;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveTermsOfServiceRequest extends AbstractRequest implements Validable {

    private Status                            status = new Status();

    private Credential                        credential;
    private RetrieveTermsOfServiceRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveTermsOfServiceRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveTermsOfServiceRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("TERMS_SERVICE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        // Lettura parametri da db
        String termsOfServiceInfo    = null;
        String termsOfServiceMessage = null;
        try {
            termsOfServiceInfo    = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_TERMS_OF_SERVICE_INFO_NEW_ACQUIRER);
            termsOfServiceMessage = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_TERMS_OF_SERVICE_MESSAGE_NEW_ACQUIRER);
        }
        catch (ParameterNotFoundException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);
            return baseResponse;
        }
        RetrieveTermsOfServiceRequest retrieveTermsOfServiceRequest = this;
        Boolean isOptional = null;
        
        if (retrieveTermsOfServiceRequest.getBody() != null) {
            isOptional = retrieveTermsOfServiceRequest.getBody().getIsOptional();
        }
        
        RetrieveTermsOfServiceData retrieveDocumentsData = getUserV2ServiceRemote().retrieveTermsOfService(retrieveTermsOfServiceRequest.getCredential().getTicketID(),
                retrieveTermsOfServiceRequest.getCredential().getRequestID(), isOptional);

        RetrieveTermsOfServiceResponse retrieveTermsOfServiceResponse = new RetrieveTermsOfServiceResponse();

        status.setStatusCode(retrieveDocumentsData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveDocumentsData.getStatusCode()));

        RetrieveTermsOfServiceResponseBody retrieveTermsOfServiceResponsebody = new RetrieveTermsOfServiceResponseBody();

        for(DocumentInfo documentInfo : retrieveDocumentsData.getDocuments()) {
            
            Boolean nullCheckList = true;
            
            for(DocumentCheckInfo documentCheckInfo : documentInfo.getCheckList()) {
                
                if ( (documentCheckInfo.getConditionText() != null && !documentCheckInfo.getConditionText().isEmpty()) ||
                     (documentCheckInfo.getSubTitle()      != null && !documentCheckInfo.getSubTitle().isEmpty()) ) {
                    
                    nullCheckList = false;
                }
            }
            
            if (nullCheckList) {
                
                documentInfo.setCheckList(null);
            }
            
            retrieveTermsOfServiceResponsebody.getTermsOfServiceList().add(documentInfo);
        }
        
        retrieveTermsOfServiceResponsebody.setTermsOfServiceInfo(termsOfServiceInfo);
        retrieveTermsOfServiceResponsebody.setTermsOfServiceMessage(termsOfServiceMessage);

        retrieveTermsOfServiceResponse.setBody(retrieveTermsOfServiceResponsebody);
        retrieveTermsOfServiceResponse.setStatus(status);

        return retrieveTermsOfServiceResponse;
    }

}
