package com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievestation;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class RetrieveStationUserPositionRequest implements Validable {

	
	private Double latitude;
	private Double longitude;
	
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	
	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.latitude == null) {
			
			status.setStatusCode(StatusCode.STATION_RETRIEVE_USER_POSITION_WRONG);
			
			return status;
			
		}
		
		if(this.longitude == null) {
			
			status.setStatusCode(StatusCode.STATION_RETRIEVE_USER_POSITION_WRONG);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);
		
		return status;
		
	}
	
	
}
