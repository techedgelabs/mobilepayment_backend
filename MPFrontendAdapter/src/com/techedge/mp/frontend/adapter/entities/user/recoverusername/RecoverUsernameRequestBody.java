package com.techedge.mp.frontend.adapter.entities.user.recoverusername;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RecoverUsernameRequestBody implements Validable {

    private String fiscalCode;

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalcode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.fiscalCode == null || fiscalCode.equals("") || this.fiscalCode.length() != 16) {

            status.setStatusCode(StatusCode.USER_RECOVER_USERNAME_INVALID_TAX_CODE);

            return status;
        }

        status.setStatusCode(StatusCode.USER_RECOVER_USERNAME_SUCCESS);
		return status;
	}
}