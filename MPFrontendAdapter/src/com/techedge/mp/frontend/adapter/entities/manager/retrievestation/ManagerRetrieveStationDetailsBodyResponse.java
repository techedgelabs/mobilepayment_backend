package com.techedge.mp.frontend.adapter.entities.manager.retrievestation;

import com.techedge.mp.frontend.adapter.entities.common.ManagerStationDetails;

public class ManagerRetrieveStationDetailsBodyResponse {

	
	private ManagerStationDetails station;

    public ManagerStationDetails getStation() {
        return station;
    }

    public void setStation(ManagerStationDetails station) {
        this.station = station;
    }
	
	
}
