package com.techedge.mp.frontend.adapter.entities.user.v2.getvirtualizationattemptsleft;

import java.io.Serializable;

public class VirtualizationAttemptsLeftActionResponseBody implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1392223526792268059L;
    
    private Integer attemptsLeft;

    public Integer getAttemptsLeft() {
        return attemptsLeft;
    }

    public void setAttemptsLeft(Integer attemptsLeft) {
        this.attemptsLeft = attemptsLeft;
    }

}
