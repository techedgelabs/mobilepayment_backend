package com.techedge.mp.frontend.adapter.entities.user.v2.updateusersdata;

import java.util.HashMap;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class UpdateUsersDataRequest extends AbstractRequest {

    private Status                     status = new Status();

    private Credential                 credential;
    private UpdateUsersDataBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateUsersDataBodyRequest getBody() {
        return body;
    }

    public void setBody(UpdateUsersDataBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-USERS-DATA", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        return status;
    }

    @Override
    public BaseResponse execute() {

        UpdateUsersDataRequest updateUsersDataRequest = this;
        UpdateUsersDataResponse updateUsersDataResponse = new UpdateUsersDataResponse();

        String ticketId = updateUsersDataRequest.getCredential().getTicketID();
        String requestId = updateUsersDataRequest.getCredential().getRequestID();
        HashMap<String, Object> usersData = updateUsersDataRequest.getBody().getUsersData();

        String response = getUserV2ServiceRemote().updateUsersData(ticketId, requestId, usersData);

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updateUsersDataResponse.setStatus(status);

        return updateUsersDataResponse;
    }

}
