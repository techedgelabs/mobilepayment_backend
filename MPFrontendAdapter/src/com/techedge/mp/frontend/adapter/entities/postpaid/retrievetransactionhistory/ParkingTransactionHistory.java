package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory;

import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone;

public class ParkingTransactionHistory {

  private String sourceType;
  private String sourceStatus;
  private PostPaidTransactionHistoryData transaction;
  private ParkingZone parkingZone;


  public String getSourceType() {
    return sourceType;
  }

  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }

  public String getSourceStatus() {
    return sourceStatus;
  }

  public void setSourceStatus(String sourceStatus) {
    this.sourceStatus = sourceStatus;
  }

  public PostPaidTransactionHistoryData getTransaction() {
    return transaction;
  }

  public void setTransaction(PostPaidTransactionHistoryData transaction) {
    this.transaction = transaction;
  }

  public ParkingZone getParkingZone() {
    return parkingZone;
  }

  public void setParkingZone(ParkingZone parkingZone) {
    this.parkingZone = parkingZone;
  }

}
