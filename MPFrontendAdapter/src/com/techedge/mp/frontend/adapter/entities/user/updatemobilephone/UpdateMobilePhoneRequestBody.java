package com.techedge.mp.frontend.adapter.entities.user.updatemobilephone;

import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UpdateMobilePhoneRequestBody implements Validable {

    private MobilePhone mobilePhone;

    public MobilePhone getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(MobilePhone mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    @Override
    public Status check() {

        Status status = new Status();

            if (mobilePhone == null || mobilePhone.getNumber() == null || mobilePhone.getNumber().length() > 20 || mobilePhone.getPrefix() == null || 
                    mobilePhone.getNumber().isEmpty() || mobilePhone.getPrefix().isEmpty() || mobilePhone.getPrefix().length() > 20) {

            status.setStatusCode(StatusCode.UPD_MOBILE_PHONE_INVALID_PARAMETERS);
            return status;

        }

        status.setStatusCode(StatusCode.UPD_MOBILE_PHONE_SUCCESS);

        return status;

    }

}
