package com.techedge.mp.frontend.adapter.entities.common.business;

public class StatusCode {

    public final static String SYSTEM_ERROR                                              = "SYSTEM_ERROR_500";

    public final static String USER_BUSINESS_AUTH_SUCCESS                                         = "USER_AUTH_200";
    public final static String USER_BUSINESS_AUTH_FAILURE                                         = "USER_AUTH_300";
    public final static String USER_BUSINESS_AUTH_LOGIN_ERROR                                     = "USER_AUTH_301";
    public final static String USER_BUSINESS_AUTH_EMAIL_WRONG                                     = "USER_AUTH_401";
    public final static String USER_BUSINESS_AUTH_PASSWORD_WRONG                                  = "USER_AUTH_402";
    public final static String USER_BUSINESS_AUTH_PASSWORD_SHORT                                  = "USER_AUTH_409";
    public final static String USER_BUSINESS_AUTH_NUMBER_LESS                                     = "USER_AUTH_410";
    public final static String USER_BUSINESS_AUTH_LOWER_LESS                                      = "USER_AUTH_411";
    public final static String USER_BUSINESS_AUTH_UPPER_LESS                                      = "USER_AUTH_412";
    public final static String USER_BUSINESS_AUTH_INVALID_REQUEST                                 = "USER_REQU_400";
    public final static String USER_BUSINESS_AUTH_TICKETID_WRONG                                  = "USER_AUTH_413";

    public final static String USER_BUSINESS_CREATE_SUCCESS                                       = "USER_CREATE_200";
    public final static String USER_BUSINESS_CREATE_FAILURE                                       = "USER_CREATE_300";
    public final static String USER_BUSINESS_CREATE_EMAIL_WRONG                                   = "USER_CREATE_401";
    public final static String USER_BUSINESS_CREATE_PASSWORD_WRONG                                = "USER_CREATE_402";
    public final static String USER_BUSINESS_CREATE_MASTER_DATA_WRONG                             = "USER_CREATE_403";
    public final static String USER_BUSINESS_CREATE_PRIVACY_DATA_WRONG                            = "USER_CREATE_407";
    public final static String USER_BUSINESS_CREATE_USERNAME_PASSWORD_EQUALS                      = "USER_CREATE_408";
    public final static String USER_BUSINESS_CREATE_PASSWORD_SHORT                                = "USER_CREATE_409";
    public final static String USER_BUSINESS_CREATE_NUMBER_LESS                                   = "USER_CREATE_410";
    public final static String USER_BUSINESS_CREATE_LOWER_LESS                                    = "USER_CREATE_411";
    public final static String USER_BUSINESS_CREATE_UPPER_LESS                                    = "USER_CREATE_412";
    public final static String USER_BUSINESS_CREATE_DATE_BIRTH_WRONG                              = "USER_CREATE_413";
    public final static String USER_BUSINESS_CREATE_CONTACT_DATA_WRONG                            = "USER_CREATE_414";
    public final static String USER_BUSINESS_CREATE_SYSTEM_ERROR                                  = "USER_CREATE_500";
    public final static String USER_BUSINESS_CREATE_MAIL_EXIST                                    = "USER_CREATE_501";

    public final static String USER_BUSINESS_LOGOUT_SUCCESS                                       = "USER_LOGOUT_200";
    public final static String USER_BUSINESS_LOGOUT_FAILURE                                       = "USER_LOGOUT_300";
    public final static String USER_BUSINESS_LOGOUT_ERROR                                         = "USER_LOGOUT_301";

    public final static String USER_BUSINESS_CHECK_SUCCESS                                        = "USER_BUSINESS_CHECK_200";
    public final static String USER_BUSINESS_CHECK_FAILURE                                        = "USER_BUSINESS_CHECK_300";
    public final static String USER_BUSINESS_CHECK_BILLING_ADDRESS_DATA_REQUIRED                  = "USER_REQU_400";
    public final static String USER_BUSINESS_CHECK_PEC_EMAIL_OR_SDI_CODE_REQUIRED                 = "USER_BUSINESS_CHECK_301";
    public final static String USER_BUSINESS_CHECK_VAT_NUMBER_INVALID                             = "USER_BUSINESS_CHECK_302";
    public final static String USER_BUSINESS_CHECK_SDI_CODE_INVALID                               = "USER_BUSINESS_CHECK_303";
    public final static String USER_BUSINESS_CHECK_BILLING_ADDRESS_ADDRESS_INVALID                = "USER_BUSINESS_CHECK_304";
    public final static String USER_BUSINESS_CHECK_BILLING_ADDRESS_PROVINCE_INVALID               = "USER_BUSINESS_CHECK_305";
    public final static String USER_BUSINESS_CHECK_BILLING_ADDRESS_CITY_INVALID                   = "USER_BUSINESS_CHECK_308";
    public final static String USER_BUSINESS_CHECK_BILLING_ADDRESS_ZIPCODE_INVALID                = "USER_BUSINESS_CHECK_306";
    public final static String USER_BUSINESS_CHECK_BILLING_ADDRESS_STREET_NUMBER_INVALID          = "USER_BUSINESS_CHECK_307";
    public final static String USER_BUSINESS_CHECK_BILLING_ADDRESS_COUNTRY_REQUIRED               = "USER_REQU_400";
    public final static String USER_BUSINESS_CHECK_FIRST_NAME_REQUIRED                            = "USER_REQU_400";
    public final static String USER_BUSINESS_CHECK_LAST_NAME_REQUIRED                             = "USER_REQU_400";
    public final static String USER_BUSINESS_CHECK_FISCALCODE_REQUIRED                            = "USER_REQU_400";
    public final static String USER_BUSINESS_CHECK_DATE_OF_BIRTH_REQUIRED                         = "USER_REQU_400";
    public final static String USER_BUSINESS_CHECK_BIRTH_MUNICIPALITY_REQUIRED                    = "USER_REQU_400";
    public final static String USER_BUSINESS_CHECK_BIRTH_PROVINCE_REQUIRED                        = "USER_REQU_400";
    public final static String USER_BUSINESS_CHECK_SEX_REQUIRED                                   = "USER_REQU_400";
    public final static String USER_BUSINESS_CHECK_SECURITY_DATA_REQUIRED                         = "USER_REQU_400";
    public final static String USER_BUSINESS_CHECK_USER_STATUS_REQUIRED                           = "USER_REQU_400";
    public final static String USER_BUSINESS_CHECK_BUSINESS_NAME_INVALID                          = "USER_BUSINESS_CHECK_309";
    public final static String USER_BUSINESS_CHECK_PEC_EMAIL_INVALID                              = "USER_BUSINESS_CHECK_310";
    public final static String USER_BUSINESS_CHECK_LICENSE_PLATE_INVALID                          = "USER_BUSINESS_CHECK_311";
    public final static String USER_BUSINESS_CHECK_FISCAL_CODE_INVALID                            = "USER_BUSINESS_CHECK_312";
    public final static String USER_BUSINESS_CHECK_BILLING_ADDRESS_LENGTH_INVALID                 = "USER_BUSINESS_CHECK_313";
    public final static String USER_BUSINESS_CHECK_FIRST_NAME_INVALID                             = "USER_BUSINESS_CHECK_314";
    public final static String USER_BUSINESS_CHECK_LAST_NAME_INVALID                              = "USER_BUSINESS_CHECK_315";
    
    public final static String USER_BUSINESS_REQU_INVALID_REQUEST                                 = "USER_REQU_400";
    public final static String USER_BUSINESS_REQU_INVALID_TICKET                                  = "USER_REQU_401";
    public final static String USER_BUSINESS_REQU_JSON_SYNTAX_ERROR                               = "USER_REQU_402";
    public final static String USER_BUSINESS_REQU_UNAUTHORIZED                                    = "USER_REQU_403";
    public final static String USER_BUSINESS_REQU_UNSUPPORTED_DEVICE                              = "USER_REQU_405";
    
    public final static String USER_BUSINESS_UPDATE_BUSINESS_DATA_SUCCESS                         = "USER_UPDATE_200";
    
    public final static String POSTPAID_BUSINESS_APPROVE_TRANSACTION_SUCCESS                      = "POSTPAID_V2_APPROVE_TRANSACTION_200";
    public final static String POSTPAID_BUSINESS_APPROVE_TRANSACTION_FAILURE                      = "POSTPAID_V2_APPROVE_TRANSACTION_300";
    public final static String POSTPAID_BUSINESS_APPROVE_TRANSACTION_INVALID_REQUEST              = "USER_REQU_400";
    public final static String POSTPAID_BUSINESS_APPROVE_TRANSACTION_UNAUTHORIZED                 = "USER_REQU_403";
    public final static String POSTPAID_BUSINESS_APPROVE_TRANSACTION_PIN_WRONG                    = "POSTPAID_V2_APPROVE_TRANSACTION_301";
    public final static String POSTPAID_BUSINESS_APPROVE_TRANSACTION_ID_WRONG                     = "POSTPAID_V2_APPROVE_TRANSACTION_302";
    
    public final static String RETRIVE_REFUEL_TRANSACTION_DETAIL_SUCCESS                         = "RETRIVE_REFUEL_TRANSACTION_DETAIL_200";
    public final static String RETRIVE_REFUEL_TRANSACTION_DETAIL_FAILURE                         = "RETRIVE_REFUEL_TRANSACTION_DETAIL_300";
    public final static String RETRIVE_REFUEL_TRANSACTION_DETAIL_INVALID_PARAMETERS              = "RETRIVE_REFUEL_TRANSACTION_DETAIL_301";
    
    public final static String USER_BUSINESS_RECOVER_USERNAME_SUCCESS                            = "USER_RECOVER_USERNAME_200";
    public final static String USER_BUSINESS_RECOVER_USERNAME_FAILURE                            = "USER_RECOVER_USERNAME_300";
    public final static String USER_BUSINESS_RECOVER_USERNAME_INVALID_TAX_CODE                   = "USER_RECOVER_USERNAME_301";
    public final static String USER_BUSINESS_RECOVER_USERNAME_INVALID_REQUEST                    = "USER_REQU_400";
    public final static String USER_BUSINESS_RECOVER_USERNAME_UNAUTHORIZED                       = "USER_REQU_403";
    
    public final static String USER_BUSINESS_RESCUE_PASSWORD_SUCCESS                             = "USER_RESCUE_200";
    public final static String USER_BUSINESS_RESCUE_PASSWORD_FAILURE                             = "USER_RESCUE_300";
    public final static String USER_BUSINESS_RESCUE_PASSWORD_ERROR                               = "USER_RESCUE_301";
    
    public final static String USER_BUSINESS_CLONE_PAYMENT_METHOD_SUCCESS                          = "USER_BUSINESS_CLONE_PAYMENT_200";
    public final static String USER_BUSINESS_CLONE_PAYMENT_METHOD_FAILURE                          = "USER_BUSINESS_CLONE_PAYMENT_300";
    public final static String USER_BUSINESS_CLONE_PAYMENT_METHOD_ERROR                            = "USER_BUSINESS_CLONE_PAYMENT_500";
    public final static String USER_BUSINESS_CLONE_PAYMENT_METHOD_INVALID_REQUEST                  = "USER_REQU_400";
    
}
