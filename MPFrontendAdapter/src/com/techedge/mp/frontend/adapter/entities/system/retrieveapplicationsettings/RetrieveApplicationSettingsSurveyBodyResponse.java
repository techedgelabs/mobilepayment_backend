package com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings;


public class RetrieveApplicationSettingsSurveyBodyResponse {

    private String surveyCode;

    public String getSurveyCode() {
        return surveyCode;
    }

    public void setSurveyCode(String surveyCode) {
        this.surveyCode = surveyCode;
    }

}
