package com.techedge.mp.frontend.adapter.entities.user.business.create;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.PersonalDataBusiness;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.TermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.common.business.BusinessData;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.business.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class CreateUserBusinessRequest extends AbstractRequest implements Validable {

    private Status                  status = new Status();

    private Credential              credential;
    private CreateUserBusinessBodyRequest body;

    public CreateUserBusinessRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateUserBusinessBodyRequest getBody() {
        return body;
    }

    public void setBody(CreateUserBusinessBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-USER-BUSINESS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_BUSINESS_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.USER_BUSINESS_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        CreateUserBusinessRequest createUserBusinessRequest = this;
        CreateUserBusinessResponse createUserResponse = new CreateUserBusinessResponse();

        String birthDateSting = createUserBusinessRequest.getBody().getUserData().getDateOfBirth().getYear() + "-"
                + createUserBusinessRequest.getBody().getUserData().getDateOfBirth().getMonth() + "-" + createUserBusinessRequest.getBody().getUserData().getDateOfBirth().getDay();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date birthDate;
        try {
            birthDate = sdf.parse(birthDateSting);
        }
        catch (ParseException e) {
            status = new Status();
            status.setStatusCode(StatusCode.USER_BUSINESS_CREATE_DATE_BIRTH_WRONG);
            status.setStatusMessage(prop.getProperty(status.getStatusCode()));
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setStatus(status);
            return baseResponse;
        }

        User user = new User();

        PersonalData personalData = new PersonalData();

        personalData.setFirstName(createUserBusinessRequest.getBody().getUserData().getFirstName());
        personalData.setLastName(createUserBusinessRequest.getBody().getUserData().getLastName());
        personalData.setFiscalCode(createUserBusinessRequest.getBody().getUserData().getFiscalCode());
        personalData.setBirthDate(birthDate);
        personalData.setBirthMunicipality(createUserBusinessRequest.getBody().getUserData().getBirthMunicipality());
        personalData.setBirthProvince(createUserBusinessRequest.getBody().getUserData().getBirthProvince());
        personalData.setLanguage(createUserBusinessRequest.getBody().getUserData().getLanguage());
        personalData.setSex(createUserBusinessRequest.getBody().getUserData().getSex());
        personalData.setSecurityDataEmail(createUserBusinessRequest.getBody().getUserData().getSecurityData().getEmail());
        personalData.setSecurityDataPassword(createUserBusinessRequest.getBody().getUserData().getSecurityData().getPassword());

        PersonalDataBusiness personalDataBusiness = new PersonalDataBusiness();
        BusinessData businessData = createUserBusinessRequest.getBody().getUserData().getBusinessData();
        personalDataBusiness.setAddress(businessData.getBillingAddress().getAddress());
        personalDataBusiness.setBusinessName(businessData.getBusinessName());
        personalDataBusiness.setCity(businessData.getBillingAddress().getCity());
        personalDataBusiness.setFirstName(createUserBusinessRequest.getBody().getUserData().getFirstName());
        personalDataBusiness.setLastName(createUserBusinessRequest.getBody().getUserData().getLastName());
        personalDataBusiness.setLicensePlate(businessData.getLicensePlate());
        personalDataBusiness.setProvince(businessData.getBillingAddress().getProvince());
        personalDataBusiness.setVatNumber(businessData.getVatNumber());
        personalDataBusiness.setZipCode(businessData.getBillingAddress().getZipCode());
        personalDataBusiness.setStreetNumber(businessData.getBillingAddress().getStreetNumber());
        //personalDataBusiness.setCountry(businessData.getBillingAddress().getCountry());
        personalDataBusiness.setCountry("IT");
        
        personalDataBusiness.setPecEmail(businessData.getPecEmail());
        personalDataBusiness.setSdiCode(businessData.getSdiCode());


        user.getPersonalDataBusinessList().add(personalDataBusiness);
        
        if (createUserBusinessRequest.getBody().getUserData().getTermsOfService() != null) {

            for (TermsOfServiceData termsOfServiceData : createUserBusinessRequest.getBody().getUserData().getTermsOfService()) {

                TermsOfService termsOfService = new TermsOfService();
                termsOfService.setKeyval(termsOfServiceData.getId());
                termsOfService.setAccepted(termsOfServiceData.getAccepted());
                termsOfService.setValid(Boolean.TRUE);
                personalData.getTermsOfServiceData().add(termsOfService);
            }
        }

        if (createUserBusinessRequest.getBody().getUserData().getContactData() != null) {

            for (MobilePhone mobilePhone : createUserBusinessRequest.getBody().getUserData().getContactData().getMobilePhones()) {

                user.getMobilePhoneList().add(mobilePhone);
            }
        }

        user.setPersonalData(personalData);
        user.setDeviceId(createUserBusinessRequest.getCredential().getDeviceID());
        
        if (createUserBusinessRequest.getBody().getUserData().getUserStatus() != null) {
            user.setSource(createUserBusinessRequest.getBody().getUserData().getUserStatus().getSource());
            user.setSourceToken(createUserBusinessRequest.getBody().getUserData().getUserStatus().getSourceToken());
            user.setUserStatus(createUserBusinessRequest.getBody().getUserData().getUserStatus().getStatus());
        }
        
        String response = getUserV2ServiceRemote().createUserBusiness(createUserBusinessRequest.getCredential().getTicketID(), createUserBusinessRequest.getCredential().getRequestID(), user);
        
        
        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        createUserResponse.setStatus(status);

        return createUserResponse;
    }

}
