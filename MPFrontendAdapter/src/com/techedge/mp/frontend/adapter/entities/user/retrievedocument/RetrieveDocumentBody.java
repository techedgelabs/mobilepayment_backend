package com.techedge.mp.frontend.adapter.entities.user.retrievedocument;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveDocumentBody implements Validable {
	
	private String documentId;
	
	
	@Override
	public Status check() {

		Status status = new Status();

		if(this.documentId == null) {

			status.setStatusCode(StatusCode.RETRIEVE_DOCUMENT_FAILURE);
	
			return status;

		}

		status.setStatusCode(StatusCode.RETRIEVE_DOCUMENT_SUCCESS);
		return status;
	}





	public String getDocumentId() {
		return documentId;
	}





	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}
	
	
}
