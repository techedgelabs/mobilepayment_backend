package com.techedge.mp.frontend.adapter.entities.user.retrieveactivedevice;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveActiveDeviceResponse extends BaseResponse {

    private RetrieveActiveDeviceResponseBody body;

    public RetrieveActiveDeviceResponseBody getBody() {
        return body;
    }
    public void setBody(RetrieveActiveDeviceResponseBody body) {
        this.body = body;
    }
    
}
