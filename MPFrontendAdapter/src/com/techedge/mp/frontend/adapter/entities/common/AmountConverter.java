package com.techedge.mp.frontend.adapter.entities.common;

import java.math.BigDecimal;

public class AmountConverter {

	private static Double CONV_INTERNAL_TO_MOBILE   = 100.0;
	private static Double CONV_INTERNAL_TO_MOBILE_3 = 1000.0;
	
	public AmountConverter() {}
	
	public static Integer toMobile(Double amount) {
		
	    //System.out.println("AmountConverter.toMobile - input: " + amount);
		
		if ( amount == null ) {
			return new Integer(0);
		}
		
		Double convertedAmount = (amount * CONV_INTERNAL_TO_MOBILE);
		
		Long l = Math.round(convertedAmount);
		
		//System.out.println("AmountConverter.toMobile - output: " + Integer.valueOf(l.intValue()));
		
		return Integer.valueOf(l.intValue());
	}
	
	public static Double toInternal(Integer amount) {
		
		//System.out.println("AmountConverter.toInternal - input: " + amount);
		
		if ( amount == null ) {
			return new Double(0);
		}
		
		Double convertedAmount = amount.doubleValue();
		
		//System.out.println("AmountConverter.toInternal - convertedAmount: " + convertedAmount);
		
		convertedAmount = convertedAmount / CONV_INTERNAL_TO_MOBILE;
		
		//System.out.println("AmountConverter.toInternal - output: " + convertedAmount);
		
		return convertedAmount;
	}
	
	public static Integer toMobile3(Double amount) {
		
		if ( amount == null ) {
			return new Integer(0);
		}
		
		Double convertedAmount = (amount * CONV_INTERNAL_TO_MOBILE_3);
		
		Long l = Math.round(convertedAmount);
		
		return Integer.valueOf(l.intValue());
	}
	
	public static Double toInternal3(Integer amount) {
		
		if ( amount == null ) {
			return new Double(0);
		}
		
		Double convertedAmount = amount.doubleValue();
		
		convertedAmount = convertedAmount / CONV_INTERNAL_TO_MOBILE_3;
		
		return convertedAmount;
	}
	
	public static Integer toMobile(BigDecimal amount) {
        
        //System.out.println("AmountConverter.toMobile - input: " + amount);
        
        if ( amount == null ) {
            return new Integer(0);
        }
        
        Double convertedAmount = (amount.doubleValue() * CONV_INTERNAL_TO_MOBILE);
        
        Long l = Math.round(convertedAmount);
        
        //System.out.println("AmountConverter.toMobile - output: " + Integer.valueOf(l.intValue()));
        
        return Integer.valueOf(l.intValue());
    }
}
