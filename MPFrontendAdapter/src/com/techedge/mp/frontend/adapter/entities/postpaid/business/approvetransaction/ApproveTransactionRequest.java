package com.techedge.mp.frontend.adapter.entities.postpaid.business.approvetransaction;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.business.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ApproveTransactionRequest extends AbstractRequest implements Validable {

    private ApproveTransactionCredential  credential;
    private ApproveTransactionBodyRequest body;

    public ApproveTransactionCredential getCredential() {
        return credential;
    }

    public void setCredential(ApproveTransactionCredential credential) {
        this.credential = credential;
    }

    public ApproveTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(ApproveTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("POSTPAID-APPROVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.POSTPAID_BUSINESS_APPROVE_TRANSACTION_SUCCESS);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        ApproveTransactionRequest approvePoPTransactionRequest = this;
        ApproveTransactionResponse approvePoPTransactionResponse = new ApproveTransactionResponse();

        PostPaidApproveShopTransactionResponse postPaidApproveShopTransactionResponse = getPostPaidV2TransactionServiceRemote().approvePopTransactionBusiness(
                approvePoPTransactionRequest.getCredential().getRequestID(), approvePoPTransactionRequest.getCredential().getTicketID(),
                approvePoPTransactionRequest.getBody().getTransactionID(), approvePoPTransactionRequest.getBody().getPaymentMethod().getId(),
                approvePoPTransactionRequest.getBody().getPaymentMethod().getType(), approvePoPTransactionRequest.getCredential().getPin());

        ApproveTransactionBodyResponse approvePoPTransactionBodyResponse = new ApproveTransactionBodyResponse();
        approvePoPTransactionBodyResponse.setCheckPinAttemptsLeft(postPaidApproveShopTransactionResponse.getPinCheckMaxAttempts());
        
        Status status = new Status();
        status.setStatusCode(postPaidApproveShopTransactionResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(postPaidApproveShopTransactionResponse.getStatusCode()));
        approvePoPTransactionResponse.setStatus(status);
        approvePoPTransactionResponse.setBody(approvePoPTransactionBodyResponse);

        return approvePoPTransactionResponse;
    }

}