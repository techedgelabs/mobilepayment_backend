package com.techedge.mp.frontend.adapter.entities.common;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CustomTimestamp {

    private int    year;
    private int    month;
    private int    day;
    private int    hour;
    private int    minute;
    private int    second;
    private String timezone;

    public CustomTimestamp() {}

    private CustomTimestamp(Timestamp timestamp) {

        long date = timestamp.getTime();
        String year = (new SimpleDateFormat("yyyy")).format(date);
        String month = (new SimpleDateFormat("MM")).format(date);
        String day = (new SimpleDateFormat("dd")).format(date);
        String hour = (new SimpleDateFormat("HH")).format(date);
        String minute = (new SimpleDateFormat("mm")).format(date);
        String second = (new SimpleDateFormat("ss")).format(date);
        String timezone = (new SimpleDateFormat("z")).format(date);

        this.year = new Integer(year);
        this.month = new Integer(month);
        this.day = new Integer(day);
        this.hour = new Integer(hour);
        this.minute = new Integer(minute);
        this.second = new Integer(second);
        this.timezone = timezone;

        //System.out.println( "timestamp output: " + this.year + "-" + this.month + "-" + this.day + " " + this.hour + ":" + this.minute + ":" + this.second + this.timezone);
    }

    public static CustomTimestamp createCustomTimestamp(Timestamp timestamp) {

        if (timestamp != null) {

            CustomTimestamp customTimestamp = new CustomTimestamp(timestamp);
            return customTimestamp;
        }
        else {

            return null;
        }
    }

    public static CustomTimestamp convertToCustomTimestamp(Date date) {

        if (date != null) {
            Timestamp t = new Timestamp(date.getTime());
            CustomTimestamp customTimestamp = new CustomTimestamp(t);
            return customTimestamp;
        }
        else {

            return null;
        }
    }

    public static Date convertToDate(CustomTimestamp customTimestamp) {
        int year = customTimestamp.getYear();
        int month = customTimestamp.getMonth() - 1;
        int day = customTimestamp.getDay();
        int hour = customTimestamp.getHour();
        int minute = customTimestamp.getMinute();
        int second = customTimestamp.getSecond();

        //String tz = customTimestamp.getTimezone();

        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day, hour, minute, second);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();

    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
