package com.techedge.mp.frontend.adapter.entities.common;

import com.techedge.mp.frontend.adapter.exceptions.InvalidCodeException;

public class StationPumpCode {

	private String pumpID;
	private String stationID;
	
	public StationPumpCode() {}
	
	public StationPumpCode(String code) throws InvalidCodeException {
		
		String[] result = code.split("-");
		
		if ( result.length != 2 ) {
			throw new InvalidCodeException(code);
		}
		
		this.stationID = result[0];
		this.pumpID    = result[1];
	}

	public String getPumpID() {
		return pumpID;
	}
	public void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}

	public String getStationID() {
		return stationID;
	}
	public void setStationID(String stationID) {
		this.stationID = stationID;
	}
}
