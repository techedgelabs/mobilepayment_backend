package com.techedge.mp.frontend.adapter.entities.user.insertmulticardpaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class InsertMulticardPaymentMethodResponse extends BaseResponse {
	
	
	private InsertMulticardPaymentMethodBodyResponse body;

	
	public InsertMulticardPaymentMethodBodyResponse getBody() {
		return body;
	}

	public void setBody(InsertMulticardPaymentMethodBodyResponse body) {
		this.body = body;
	}
	
}
