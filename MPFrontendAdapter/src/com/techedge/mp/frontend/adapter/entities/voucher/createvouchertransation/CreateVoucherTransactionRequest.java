package com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.voucher.CreateVoucherTransactionResult;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.FrontendParameter;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class CreateVoucherTransactionRequest extends AbstractRequest implements Validable {

    private CreateVoucherTransactionCredential  credential;
    private CreateVoucherTransactionBodyRequest body;

    public CreateVoucherTransactionRequest() {}

    public CreateVoucherTransactionCredential getCredential() {
        return credential;
    }

    public void setCredential(CreateVoucherTransactionCredential credential) {
        this.credential = credential;
    }

    public CreateVoucherTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(CreateVoucherTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-VOUCHER-TRANSACTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.CREATE_VOUCHER_TRANSACTION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        CreateVoucherTransactionRequest createVoucherTransactionRequest = this;
        CreateVoucherTransactionResponse createVoucherTransactionResponse = new CreateVoucherTransactionResponse();
        CreateVoucherTransactionBodyResponse createVoucherTransactionBodyResponse = new CreateVoucherTransactionBodyResponse();

        String requestID = createVoucherTransactionRequest.getCredential().getRequestID();
        String ticketID = createVoucherTransactionRequest.getCredential().getTicketID();
        String pin = createVoucherTransactionRequest.getCredential().getPin();
        Double amount = AmountConverter.toInternal(createVoucherTransactionRequest.getBody().getVoucherPaymentData().getAmount());
        Long paymentMethodID = createVoucherTransactionRequest.getBody().getVoucherPaymentData().getPaymentMethod().getId();
        String paymentMethodType = createVoucherTransactionRequest.getBody().getVoucherPaymentData().getPaymentMethod().getType();
        Status statusResponse = new Status();

        try {
            String voucherTransactionMaxAmount = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_VOUCHER_TRANSACTION_MAX_AMOUNT);
            String voucherTransactionMinAmount = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_VOUCHER_TRANSACTION_MIN_AMOUNT);

            if (amount < Double.valueOf(voucherTransactionMinAmount) || amount > Double.valueOf(voucherTransactionMaxAmount)) {
                BaseResponse baseResponse = new BaseResponse();
                statusResponse.setStatusCode(StatusCode.CREATE_VOUCHER_TRANSACTION_INVALID_PARAMETERS);
                statusResponse.setStatusMessage("Invalid amount");
                baseResponse.setStatus(statusResponse);

                return baseResponse;
            }
        }

        catch (ParameterNotFoundException e) {                    // TODO Auto-generated catch block
            e.printStackTrace();
        }

        CreateVoucherTransactionResult createVoucherTransactionResult = getVoucherTransactionServiceRemote().createVoucherTransaction(requestID, ticketID, pin, amount,
                paymentMethodID, paymentMethodType);

        createVoucherTransactionBodyResponse.setVoucherCode(createVoucherTransactionResult.getVoucherCode());
        createVoucherTransactionBodyResponse.setVoucherTransactionID(createVoucherTransactionResult.getVoucherTransactionID());
        createVoucherTransactionBodyResponse.setCheckPinAttemptsLeft(createVoucherTransactionResult.getPinCheckMaxAttempts());
        createVoucherTransactionResponse.setBody(createVoucherTransactionBodyResponse);

        statusResponse.setStatusCode(createVoucherTransactionResult.getStatusCode());
        statusResponse.setStatusMessage(prop.getProperty(statusResponse.getStatusCode()));
        createVoucherTransactionResponse.setStatus(statusResponse);

        return createVoucherTransactionResponse;

    }

}
