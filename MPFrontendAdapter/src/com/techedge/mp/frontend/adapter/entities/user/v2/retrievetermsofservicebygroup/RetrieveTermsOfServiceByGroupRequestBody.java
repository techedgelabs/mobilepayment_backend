package com.techedge.mp.frontend.adapter.entities.user.v2.retrievetermsofservicebygroup;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveTermsOfServiceByGroupRequestBody implements Validable {

    String group;

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.group == null) {
            
            status.setStatusCode(StatusCode.RETRIEVE_TERMS_SERVICE_FAILURE);
            
            return status;
        }
        
        status.setStatusCode(StatusCode.RETRIEVE_TERMS_SERVICE_SUCCESS);

        return status;
    }

}
