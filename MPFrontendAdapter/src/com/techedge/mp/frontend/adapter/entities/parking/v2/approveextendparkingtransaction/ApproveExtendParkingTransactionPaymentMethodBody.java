package com.techedge.mp.frontend.adapter.entities.parking.v2.approveextendparkingtransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ApproveExtendParkingTransactionPaymentMethodBody implements Validable {
	
	private Long id;
	private String type;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public Status check() {
		Status status = new Status();
		status.setStatusCode(StatusCode.POSTPAID_V2_APPROVE_TRANSACTION_SUCCESS);
		return status;
	}
}