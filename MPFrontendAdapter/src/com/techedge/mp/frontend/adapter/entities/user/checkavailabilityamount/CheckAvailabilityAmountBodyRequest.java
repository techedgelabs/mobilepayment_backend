package com.techedge.mp.frontend.adapter.entities.user.checkavailabilityamount;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CheckAvailabilityAmountBodyRequest implements Validable {

    private Integer amount;

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.amount == null) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }
        
        if (this.amount <= 0.0 ) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.CHECK_AVAILABILITY_AMOUNT_SUCCESS);

        return status;

    }

}
