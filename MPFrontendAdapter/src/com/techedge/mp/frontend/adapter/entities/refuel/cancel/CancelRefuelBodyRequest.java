package com.techedge.mp.frontend.adapter.entities.refuel.cancel;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CancelRefuelBodyRequest implements Validable {
	
	private String refuelID;
	

	public String getRefuelID() {
		return refuelID;
	}

	public void setRefuelID(String refuelID) {
		this.refuelID = refuelID;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.refuelID == null || this.refuelID.length() != 32 || this.refuelID.trim() == "") {
			
			status.setStatusCode(StatusCode.REFUEL_CANCEL_REFUEL_ID_WRONG);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.REFUEL_CANCEL_SUCCESS);
		
		return status;
	}
	
}