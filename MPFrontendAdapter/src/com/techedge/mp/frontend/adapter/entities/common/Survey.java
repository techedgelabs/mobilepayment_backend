package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;
import java.util.Date;

public class Survey {
    
    private String code;
    private String description;
    private String note;
    private Date startDate;
    private Date endDate;
    private Integer status;
    private ArrayList<SurveyQuestion> questions = new ArrayList<SurveyQuestion>(0);
    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }
    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }
    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }
    /**
     * @return the note
     */
    public String getNote() {
        return note;
    }
    /**
     * @param note the note to set
     */
    public void setNote(String note) {
        this.note = note;
    }
    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }
    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }
    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }
    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
    
    public ArrayList<SurveyQuestion> getQuestions() {
        return this.questions;
    }

    public void setQuestions(ArrayList<SurveyQuestion> questions) {
        this.questions = questions;
    }
    
    
    
    
    
    
    
    
}
