package com.techedge.mp.frontend.adapter.entities.user.v2.socialauthentication;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class SocialAuthenticationUserResponseSuccess extends BaseResponse {

    private SocialAuthenticationUserResponseBody body;

    public SocialAuthenticationUserResponseBody getBody() {
        return body;
    }

    public void setBody(SocialAuthenticationUserResponseBody body) {
        this.body = body;
    }

}
