package com.techedge.mp.frontend.adapter.entities.user.v2.retrievecities;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveCitiesRequestBody implements Validable {
	
	private String searchKey;
	
	
	public String getSearchKey() {
		return searchKey;
	}
	public void setSearchKey(String searchKey) {
		this.searchKey = searchKey;
	}


	@Override
	public Status check() {

		Status status = new Status();

		if(this.searchKey == null || this.searchKey.trim().length() > 40 || this.searchKey.trim().length() < 2) {

			status.setStatusCode(StatusCode.RETRIEVE_CITIES_FAILURE);
	
			return status;

		}

		status.setStatusCode(StatusCode.RETRIEVE_CITIES_SUCCESS);

		return status;
	}
	
	
}
