package com.techedge.mp.frontend.adapter.entities.postpaid.v2.approvemulticardtransaction;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveMulticardShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ApproveMulticardTransactionRequest extends AbstractRequest implements Validable {

    private Credential                             credential;
    private ApproveMulticardTransactionBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ApproveMulticardTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(ApproveMulticardTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("POSTPAID-MULTICARD-APPROVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        ApproveMulticardTransactionRequest approveMulticardPoPTransactionRequest = this;
        ApproveMulticardTransactionResponse approveMulticardPoPTransactionResponse = new ApproveMulticardTransactionResponse();

        PostPaidApproveMulticardShopTransactionResponse postPaidApproveMulticardShopTransactionResponse = getPostPaidV2TransactionServiceRemote().approveMulticardPopTransaction(
                approveMulticardPoPTransactionRequest.getCredential().getRequestID(), approveMulticardPoPTransactionRequest.getCredential().getTicketID(),
                approveMulticardPoPTransactionRequest.getBody().getTransactionID(), approveMulticardPoPTransactionRequest.getBody().getPaymentMethod().getId(),
                approveMulticardPoPTransactionRequest.getBody().getPaymentCryptogram());
        
        Status status = new Status();
        status.setStatusCode(postPaidApproveMulticardShopTransactionResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(postPaidApproveMulticardShopTransactionResponse.getStatusCode()));
        
        approveMulticardPoPTransactionResponse.setStatus(status);

        return approveMulticardPoPTransactionResponse;
    }

}