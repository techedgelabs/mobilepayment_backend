package com.techedge.mp.frontend.adapter.entities.user.business.retrievetermsofservice;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveTermsOfServiceRequestBody implements Validable {

    Boolean isOptional;

    public Boolean getIsOptional() {
        return isOptional;
    }

    public void setIsOptional(Boolean isOptional) {
        this.isOptional = isOptional;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.RETRIEVE_TERMS_SERVICE_SUCCESS);

        return status;
    }

}
