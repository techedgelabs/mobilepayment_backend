package com.techedge.mp.frontend.adapter.entities.user.v2.gethomepartnerlist;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.PartnerInfo;

public class GetHomePartnerListBodyResponse {

    private List<PartnerInfo> partnerList = new ArrayList<PartnerInfo>(0);

    public List<PartnerInfo> getPartnerList() {
        return partnerList;
    }

    public void setPartnerList(List<PartnerInfo> partnerList) {
        this.partnerList = partnerList;
    }
}
