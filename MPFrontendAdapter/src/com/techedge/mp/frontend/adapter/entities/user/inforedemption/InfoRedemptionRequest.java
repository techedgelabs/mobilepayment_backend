package com.techedge.mp.frontend.adapter.entities.user.inforedemption;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionDetail;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.RedemptionData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class InfoRedemptionRequest extends AbstractRequest implements Validable {

    private Status     status = new Status();
    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("INFO-REDEMPTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.GET_CREDIT_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        InfoRedemptionRequest infoRedemptionRequest = this;
        InfoRedemptionResponse infoRedemptionResponse = new InfoRedemptionResponse();
        InfoRedemptionBodyResponse infoRedemptionBodyResponse = new InfoRedemptionBodyResponse();

        String tickeID = infoRedemptionRequest.getCredential().getTicketID();
        String requestID = infoRedemptionRequest.getCredential().getRequestID();

        com.techedge.mp.core.business.interfaces.loyalty.InfoRedemptionResponse response = getUserServiceRemote().infoRedemption(tickeID, requestID);

        if (response.getStatusCode().equals(ResponseHelper.USER_INFO_REDEMPTION_SUCCESS)) {

            for (RedemptionDetail redemptionDetail : response.getRedemptions()) {
                RedemptionData redemptionData = new RedemptionData();
                redemptionData.setCode(redemptionDetail.getCode());
                redemptionData.setName(redemptionDetail.getName());
                redemptionData.setPoints(redemptionDetail.getPoints());
                Integer amount = AmountConverter.toMobile(redemptionDetail.getAmount());
                redemptionData.setAmount(amount);

                infoRedemptionBodyResponse.getRedemptions().add(redemptionData);
            }
            infoRedemptionResponse.setBody(infoRedemptionBodyResponse);

            status.setStatusCode(ResponseHelper.USER_INFO_REDEMPTION_SUCCESS);
        }
        else {
            status.setStatusCode(response.getStatusCode());
        }

        status.setStatusMessage(prop.getProperty(status.getStatusCode()));

        infoRedemptionResponse.setStatus(status);

        return infoRedemptionResponse;
    }
}
