package com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class ApproveTransactionResponse extends BaseResponse {
	
	private ApproveTransactionBodyResponse body;

    public ApproveTransactionBodyResponse getBody() {
        return body;
    }

    public void setBody(ApproveTransactionBodyResponse body) {
        this.body = body;
    }
}
