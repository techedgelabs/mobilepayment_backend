package com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievesourcedetail;

import java.sql.Timestamp;

import com.techedge.mp.core.business.interfaces.postpaid.CoreProductDetail;
import com.techedge.mp.core.business.interfaces.postpaid.GetSourceDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCartData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelData;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Cash;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidCartJsonData;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidExtendedRefuelData;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveSourceDetailRequest extends AbstractRequest implements Validable {

    private Credential                      credential;
    private RetrieveSourceDetailBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveSourceDetailBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveSourceDetailBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("POSPAID-SOURCE-DETAIL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        RetrieveSourceDetailRequest retrieveSourceDetailRequest = this;

        RetrieveSourceDetailResponse retrieveSourceDetailResponse = new RetrieveSourceDetailResponse();

        Double userPositionLatitude = null;
        Double userPositionLongitude = null;
        if (retrieveSourceDetailRequest.getBody().getUserPosition() != null) {
            userPositionLatitude = retrieveSourceDetailRequest.getBody().getUserPosition().getLatitude();
            userPositionLongitude = retrieveSourceDetailRequest.getBody().getUserPosition().getLongitude();
        }
        
        String requestID = retrieveSourceDetailRequest.getCredential().getRequestID();
        String ticketID = retrieveSourceDetailRequest.getCredential().getTicketID();
        String codeType = retrieveSourceDetailRequest.getBody().getCodeType();
        String sourceID = retrieveSourceDetailRequest.getBody().getSourceID();

        GetSourceDetailResponse getSourceDetailResponse = getPostPaidV2TransactionServiceRemote().getSourceDetail(requestID, ticketID, codeType, sourceID, 
                userPositionLatitude, userPositionLongitude);

        Status status = new Status();
        status.setStatusCode(getSourceDetailResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(getSourceDetailResponse.getStatusCode()));
        retrieveSourceDetailResponse.setStatus(status);

        RetrieveSourceDetailBodyResponse retrievePoPTransactionByObjectIDBodyResponse = new RetrieveSourceDetailBodyResponse();
        retrievePoPTransactionByObjectIDBodyResponse.setOutOfRange(getSourceDetailResponse.getOutOfRange());
        retrievePoPTransactionByObjectIDBodyResponse.setSourceType(getSourceDetailResponse.getObjectType());
        retrievePoPTransactionByObjectIDBodyResponse.setSourceStatus(getSourceDetailResponse.getObjectStatus());

        PostPaidTransactionHistoryData postPaidTransactionHistoryData = new PostPaidTransactionHistoryData();
        postPaidTransactionHistoryData.setAmount(AmountConverter.toMobile(getSourceDetailResponse.getAmount()));
        postPaidTransactionHistoryData.setTransactionID(getSourceDetailResponse.getTransactionID());
        if (getSourceDetailResponse.getTransactionData() != null) {
            postPaidTransactionHistoryData.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(
                    getSourceDetailResponse.getTransactionData().getCreationTimestamp().getTime())));
        }

        Station station = new Station();
        station.setName(getSourceDetailResponse.getStationName());
        station.setStationID(getSourceDetailResponse.getStationID());

        LocationData locationData = new LocationData();
        locationData.setAddress(getSourceDetailResponse.getStationAddress());
        locationData.setCity(getSourceDetailResponse.getStationCity());
        locationData.setCountry(getSourceDetailResponse.getStationCountry());
        locationData.setLatitude(getSourceDetailResponse.getStationLatitude());
        locationData.setLongitude(getSourceDetailResponse.getStationLongitude());
        locationData.setProvince(getSourceDetailResponse.getStationProvince());
        station.setLocationData(locationData);
        
        System.out.println("NewAcquirerEnabled: " + getSourceDetailResponse.getNewAcquirerEnabled());
        
        if(getSourceDetailResponse.getNewAcquirerEnabled()) {
            station.getEnabledServices().add(Station.PAYMENT_SERVICE);
        }
        
        System.out.println("LoyaltyEnabled: " + getSourceDetailResponse.getLoyaltyEnabled());
        
        if(getSourceDetailResponse.getLoyaltyEnabled()) {
            station.getEnabledServices().add(Station.LOYALTY_SERVICE);
        }

        retrievePoPTransactionByObjectIDBodyResponse.setStation(station);

        if (getSourceDetailResponse.getPumpInfo() != null) {

            System.out.println("Pump in sourceDetail is not null");

            Pump pump = new Pump();
            pump.setPumpID(getSourceDetailResponse.getPumpInfo().getPumpID());
            pump.setNumber(Integer.valueOf(getSourceDetailResponse.getPumpInfo().getPumpNumber()));
            pump.setStatus(getSourceDetailResponse.getPumpInfo().getPumpStatus());
            pump.setRefuelMode(getSourceDetailResponse.getPumpInfo().getRefuelMode());

            for (CoreProductDetail productDetail : getSourceDetailResponse.getPumpInfo().getProductDetails()) {
                pump.getFuelType().add(productDetail.getProductDescription());
            }

            retrievePoPTransactionByObjectIDBodyResponse.setPump(pump);
        }

        if (getSourceDetailResponse.getPostPaidCartDataList() != null) {

            PostPaidCartJsonData postPaidCartJsonData;

            for (PostPaidCartData postPaidCartData : getSourceDetailResponse.getPostPaidCartDataList()) {

                postPaidCartJsonData = new PostPaidCartJsonData();
                postPaidCartJsonData.setAmount(AmountConverter.toMobile(postPaidCartData.getAmount()));
                postPaidCartJsonData.setProductDescription(postPaidCartData.getProductDescription());
                postPaidCartJsonData.setProductId(postPaidCartData.getProductId());
                postPaidCartJsonData.setQuantity(postPaidCartData.getQuantity());

                postPaidTransactionHistoryData.getCart().add(postPaidCartJsonData);
            }
        }

        if (getSourceDetailResponse.getPostPaidRefuelDataList() != null) {

            PostPaidExtendedRefuelData postPaidRefuelJsonData;

            for (PostPaidRefuelData postPaidRefuelData : getSourceDetailResponse.getPostPaidRefuelDataList()) {

                postPaidRefuelJsonData = new PostPaidExtendedRefuelData();
                postPaidRefuelJsonData.setFuelAmount(postPaidRefuelData.getFuelAmount() * 100);
                postPaidRefuelJsonData.setFuelQuantity(postPaidRefuelData.getFuelQuantity() * 1000);
                postPaidRefuelJsonData.setFuelType(postPaidRefuelData.getFuelType());
                postPaidRefuelJsonData.setProductDescription(postPaidRefuelData.getProductDescription());
                postPaidRefuelJsonData.setProductId(postPaidRefuelData.getProductId());
                postPaidRefuelJsonData.setPumpId(postPaidRefuelData.getPumpId());

                postPaidTransactionHistoryData.getRefuel().add(postPaidRefuelJsonData);
            }
        }

        retrievePoPTransactionByObjectIDBodyResponse.setTransaction(postPaidTransactionHistoryData);

        if (getSourceDetailResponse.getCashInfo() != null) {

            Cash cash = new Cash();
            cash.setCashID(getSourceDetailResponse.getCashInfo().getCashId());
            if (getSourceDetailResponse.getCashInfo().getNumber() != null) {
                cash.setNumber(Integer.valueOf(getSourceDetailResponse.getCashInfo().getNumber()));
            }
            else {
                cash.setNumber(null);
            }
            cash.setRefuelMode("Shop");
            retrievePoPTransactionByObjectIDBodyResponse.setCash(cash);
        }

        retrieveSourceDetailResponse.setBody(retrievePoPTransactionByObjectIDBodyResponse);

        return retrieveSourceDetailResponse;
    }

}