package com.techedge.mp.frontend.adapter.entities.user.v2.insertpaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class InsertPaymentMethodBodyRequest implements Validable {

	
	private PaymentMethod paymentMethod;
	private String newPin;


	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}

	public String getNewPin() {
		return newPin;
	}
	public void setNewPin(String newPin) {
		this.newPin = newPin;
	}
	
	
	@Override
	public Status check() {

		Status status = new Status();
		
		if (this.paymentMethod == null) {
			
			status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
			
			return status;
		}
			
		if(this.paymentMethod.getType() == null ) {
			
			status.setStatusCode(StatusCode.USER_INSERT_PAYMENT_METHOD_TYPE_WRONG);
			
			return status;
		}
		
		status = Validator.checkPin("PAY-INSERT", newPin);
		
		if (!Validator.isValid(status.getStatusCode())) {
			
			return status;
		}
		
		status.setStatusCode(StatusCode.USER_INSERT_PAYMENT_METHOD_SUCCESS);
		
		return status;
		
	}
	
}
