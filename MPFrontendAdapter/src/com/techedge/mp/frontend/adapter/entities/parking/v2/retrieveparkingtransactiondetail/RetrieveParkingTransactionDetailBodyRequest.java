package com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingtransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveParkingTransactionDetailBodyRequest implements Validable {

    private String transactionID;

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.transactionID == null || this.transactionID.length() != 32 || this.transactionID.trim() == "") {

            status.setStatusCode(StatusCode.RETRIEVE_PARKING_TRANSACTION_DETAIL_INVALID_ID);

            return status;
        }

        status.setStatusCode(StatusCode.RETRIEVE_PARKING_TRANSACTION_DETAIL_SUCCESS);

        return status;
    }
}