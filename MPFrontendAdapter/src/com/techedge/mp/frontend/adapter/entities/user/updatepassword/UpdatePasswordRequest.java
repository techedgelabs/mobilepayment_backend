package com.techedge.mp.frontend.adapter.entities.user.updatepassword;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class UpdatePasswordRequest extends AbstractRequest implements Validable {

    private Status                    status = new Status();

    private Credential                credential;
    private UpdatePasswordRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdatePasswordRequestBody getBody() {
        return body;
    }

    public void setBody(UpdatePasswordRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PWD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.USER_PWD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        UpdatePasswordRequest updatePasswordRequest = this;

        String response = getUserServiceRemote().updatePassword(updatePasswordRequest.getCredential().getTicketID(), updatePasswordRequest.getCredential().getRequestID(),
                updatePasswordRequest.getBody().getOldPassword(), updatePasswordRequest.getBody().getNewPassword());

        UpdatePasswordResponse updatePasswordResponse = new UpdatePasswordResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updatePasswordResponse.setStatus(status);

        return updatePasswordResponse;
    }
}
