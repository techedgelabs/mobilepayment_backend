package com.techedge.mp.frontend.adapter.entities.user.v2.skipvirtualization;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class SkipVirtualizationRequest extends AbstractRequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("SKIP-PAYMENT-METHOD-CONFIGURATION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.SKIP_PAYMENT_METHOD_CONFIGURATION_SUCCESS);

        return status;
    }

    @Override
    public BaseResponse execute() {
        SkipVirtualizationRequest skipVirtualizationRequest = this;

        String response = getUserV2ServiceRemote().skipVirtualization(skipVirtualizationRequest.getCredential().getTicketID(),
                skipVirtualizationRequest.getCredential().getRequestID());
        
        SkipVirtualizazionResponse skipVirtualizazionResponse = new SkipVirtualizazionResponse();

        status.setStatusCode(response);

        status.setStatusMessage(prop.getProperty(response));
        
        skipVirtualizazionResponse.setStatus(status);

        return skipVirtualizazionResponse;
    }

}
