package com.techedge.mp.frontend.adapter.entities.common;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RetrieveDevice implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4218370237366906574L;

    /**
     * 
     */

    private String            statusCode;

    private List<DeviceData>    activeDevice   = new ArrayList<>(0);

    public String getStatusCode() {

        return statusCode;
    }

    public void setStatusCode(String statusCode) {

        this.statusCode = statusCode;
    }

    public List<DeviceData> getActiveDevice() {

        return activeDevice;
    }

    public void setUserDeviceList(List<DeviceData> userDeviceList) {

        this.activeDevice = userDeviceList;
    }

}
