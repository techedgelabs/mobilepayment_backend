package com.techedge.mp.frontend.adapter.entities.user.inforedemption;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class InfoRedemptionResponse extends BaseResponse {

    private InfoRedemptionBodyResponse body;

    public InfoRedemptionBodyResponse getBody() {
        return body;
    }

    public void setBody(InfoRedemptionBodyResponse body) {
        this.body = body;
    }

}
