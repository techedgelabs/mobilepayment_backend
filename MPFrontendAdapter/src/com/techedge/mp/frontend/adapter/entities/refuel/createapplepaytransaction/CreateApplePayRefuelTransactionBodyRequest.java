package com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateApplePayRefuelTransactionBodyRequest implements Validable {

    private CreateApplePayRefuelTransactionDataRequest applePayRefuelPaymentData;

    public CreateApplePayRefuelTransactionDataRequest getApplePayRefuelPaymentData() {
        return applePayRefuelPaymentData;
    }

    public void setApplePayRefuelPaymentData(CreateApplePayRefuelTransactionDataRequest applePayRefuelPaymentData) {
        this.applePayRefuelPaymentData = applePayRefuelPaymentData;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.applePayRefuelPaymentData != null) {

            status = this.applePayRefuelPaymentData.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_SUCCESS);

        return status;

    }

}
