package com.techedge.mp.frontend.adapter.entities.parking.v2.retrievependingparkingtransaction;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.parking.ParkingTransaction;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransactionItem;
import com.techedge.mp.core.business.interfaces.parking.RetrievePendingParkingTransactionResult;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZonePriceInfo;
import com.techedge.mp.frontend.adapter.entities.common.v2.PlateNumberInfo;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrievePendingParkingTransactionRequest extends AbstractRequest implements Validable {

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PENDING-PARKING-TRANSACTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        return status;

    }

    @Override
    public BaseResponse execute() {
        RetrievePendingParkingTransactionRequest retrievePendingParkingTransactionRequest = this;

        RetrievePendingParkingTransactionResult retrievePendingParkingTransactionResult = getParkingTransactionV2ServiceRemote().retrievePendingParkingTransaction(
                retrievePendingParkingTransactionRequest.getCredential().getTicketID(), retrievePendingParkingTransactionRequest.getCredential().getRequestID());

        RetrievePendingParkingTransactionResponse retrievePendingParkingTransactionResponse = new RetrievePendingParkingTransactionResponse();

        Status statusResponse = new Status();

        statusResponse.setStatusCode(retrievePendingParkingTransactionResult.getStatusCode());
        statusResponse.setStatusMessage(prop.getProperty(retrievePendingParkingTransactionResult.getStatusCode()));

        retrievePendingParkingTransactionResponse.setStatus(statusResponse);

        if (retrievePendingParkingTransactionResult.getStatusCode().equals("RETRIEVE_PENDING_PARKING_TRANSACTION_200")) {

            ParkingTransaction parkingTransaction = retrievePendingParkingTransactionResult.getParkingTransaction();
            
            RetrievePendingParkingTransactionBodyResponse retrievePendingParkingTransactionBodyResponse = new RetrievePendingParkingTransactionBodyResponse();

            // ParkingStartTime del primo item
            Date parkingStartTime = null;
            
            // ParkingEndTime dell'ultimo item
            Date parkingEndTime = null;
            
            // Somma dei price dei singoli item
            BigDecimal currentPrice = new BigDecimal("0");
            
            Long lastPaymentMethodId = null;
            
            //List<ParkingTransaction> parkingTransactionItemList = new ArrayList<ParkingTransaction>(parkingTransaction.getParkingTransactionItemList());
            
            ArrayList<ParkingTransactionItem> parkingTransactionItemList = Collections.list(Collections.enumeration(parkingTransaction.getParkingTransactionItemList()));
            
            Collections.sort(parkingTransactionItemList, new Comparator<ParkingTransactionItem>() {
                @Override
                public int compare(ParkingTransactionItem parkingTransactionItem1, ParkingTransactionItem parkingTransactionItem2) {
                    // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                    return parkingTransactionItem1.getCreationTimestamp().compareTo(parkingTransactionItem2.getCreationTimestamp());
                }
            });
            
            for(ParkingTransactionItem parkingTransactionItem : parkingTransactionItemList) {
                
                if (parkingTransactionItem.getParkingItemStatus().equals("AUTHORIZED")) {
                    
                    if (parkingStartTime == null) {
                        parkingStartTime = parkingTransactionItem.getParkingStartTime();
                    }
                    
                    parkingEndTime = parkingTransactionItem.getParkingEndTime();
                    
                    currentPrice = parkingTransactionItem.getPrice();
                    
                    lastPaymentMethodId = parkingTransactionItem.getPaymentMethodId();
                }
            }
            
            ParkingZonePriceInfo parkingZonePriceInfo = new ParkingZonePriceInfo();
            
            PlateNumberInfo plateNumberInfo = new PlateNumberInfo();
            plateNumberInfo.setPlateNumber(parkingTransaction.getPlateNumber());
            plateNumberInfo.setDescription(parkingTransaction.getPlateNumberDescription());
            parkingZonePriceInfo.setPlateNumber(plateNumberInfo);
            
            ParkingZone parkingZone = new ParkingZone();
            parkingZone.setId(parkingTransaction.getParkingZoneId());
            parkingZone.setName(parkingTransaction.getParkingZoneName());
            parkingZone.setDescription(parkingTransaction.getParkingZoneDescription());
            parkingZone.setCityId(parkingTransaction.getCityId());
            parkingZone.setCityName(parkingTransaction.getCityName());
            parkingZone.setAdministrativeAreaLevel2Code(parkingTransaction.getAdministrativeAreaLevel2Code());
            parkingZonePriceInfo.setParkingZone(parkingZone);
            
            parkingZonePriceInfo.setCurrentPrice(AmountConverter.toMobile(currentPrice));
            parkingZonePriceInfo.setFinalPrice(AmountConverter.toMobile(parkingTransaction.getFinalPrice()));
            parkingZonePriceInfo.getNotice().add(parkingTransaction.getParkingZoneNotice());
            parkingZonePriceInfo.setParkingEndTime(CustomTimestamp.convertToCustomTimestamp(parkingEndTime));
            parkingZonePriceInfo.setParkingStartTime(CustomTimestamp.convertToCustomTimestamp(parkingStartTime));
            parkingZonePriceInfo.setPrice(AmountConverter.toMobile(currentPrice));
            parkingZonePriceInfo.setPriceDifference(null);
            parkingZonePriceInfo.setStallCode(parkingTransaction.getStallCode());
            parkingZonePriceInfo.setTransactionID(parkingTransaction.getParkingTransactionId());
            
            PaymentMethod paymentMethod = new PaymentMethod();
            paymentMethod.setId(lastPaymentMethodId);
            parkingZonePriceInfo.setPaymentMethod(paymentMethod);
            
            retrievePendingParkingTransactionBodyResponse.setParkingZonePriceInfo(parkingZonePriceInfo);
            
            retrievePendingParkingTransactionBodyResponse.setStatus(parkingTransaction.getStatus());
            
            retrievePendingParkingTransactionResponse.setBody(retrievePendingParkingTransactionBodyResponse);

        }

        return retrievePendingParkingTransactionResponse;
    }

}
