package com.techedge.mp.frontend.adapter.entities.user.retrievepaymentdata;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrievePaymentDataResponse extends BaseResponse {

	
	private RetrievePaymentDataBodyResponse body;

	
	public RetrievePaymentDataBodyResponse getBody() {
		return body;
	}

	
	public void setBody(RetrievePaymentDataBodyResponse body) {
		this.body = body;
	}
	
	
	
}
