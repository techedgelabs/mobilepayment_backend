package com.techedge.mp.frontend.adapter.entities.survey.submitsurvey;

import com.techedge.mp.core.business.interfaces.SurveyAnswers;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class SubmitSurveyRequest extends AbstractRequest implements Validable {

    private Credential              credential;
    private SubmitSurveyBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public SubmitSurveyBodyRequest getBody() {
        return body;
    }

    public void setBody(SubmitSurveyBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("SUBMIT-SURVEY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.SURVEY_SUBMIT_SURVEY_SUCCESS);

        return status;
    }

    @Override
    public BaseResponse execute() {
        SubmitSurveyRequest submitSurveyRequest = this;
        SubmitSurveyResponse submitSurveyResponse = new SubmitSurveyResponse();

        SurveyAnswers coreAnswers = new SurveyAnswers();

        for (SubmitSurveyBodyRequest.Answer answer : submitSurveyRequest.getBody().getAnswers()) {
            coreAnswers.addAnswer(answer.getCode(), answer.getValue());
        }

        String response = getSurveyServiceRemote().surveySubmit(submitSurveyRequest.getCredential().getTicketID(), submitSurveyRequest.getCredential().getRequestID(),
                submitSurveyRequest.getBody().getCode(), submitSurveyRequest.getBody().getKey(), coreAnswers);

        Status statusResponse = new Status();
        statusResponse.setStatusCode(response);
        statusResponse.setStatusMessage(prop.getProperty(statusResponse.getStatusCode()));

        submitSurveyResponse.setStatus(statusResponse);

        return submitSurveyResponse;

    }

}
