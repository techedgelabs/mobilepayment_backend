package com.techedge.mp.frontend.adapter.entities.user.v2.setdefaultplatenumber;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class SetDefaultPlateNumberBodyRequest implements Validable {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.id == null) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_REMOVE_PLATE_NUMBER_SUCCESS);

        return status;

    }

}
