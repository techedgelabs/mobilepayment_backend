package com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateApplePayVoucherTransactionBodyRequest implements Validable {

    private ApplePayVoucherPaymentDataRequest applePayVoucherPaymentData;

    public CreateApplePayVoucherTransactionBodyRequest() {}

    public ApplePayVoucherPaymentDataRequest getApplePayVoucherPaymentData() {
        return applePayVoucherPaymentData;
    }

    public void setApplePayVoucherPaymentData(ApplePayVoucherPaymentDataRequest applePayVoucherPaymentData) {
        this.applePayVoucherPaymentData = applePayVoucherPaymentData;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.applePayVoucherPaymentData != null) {

            status = this.applePayVoucherPaymentData.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.CREATE_APPLE_PAY_VOUCHER_TRANSACTION_SUCCESS);

        return status;
    }

}
