package com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings;

import com.techedge.mp.frontend.adapter.entities.common.v2.AppLinkInfo;

public class RetrieveApplicationSettingsDinamycPageBodyResponse {

    private AppLinkInfo faq;
    private AppLinkInfo redemptionUrl;
    private AppLinkInfo infoTokenUrl;
    private AppLinkInfo voucherUrl;
    private AppLinkInfo loyaltyUrl;
    private AppLinkInfo missionUrl;
    private AppLinkInfo stationFinderUrl;
    private AppLinkInfo firstStartUrl;
    private AppLinkInfo parkingCoupon;
    private AppLinkInfo myCiceroLoginUrl;
    private AppLinkInfo myMulticardLoginUrl;

    public AppLinkInfo getFaq() {
        return faq;
    }

    public void setFaq(AppLinkInfo faq) {
        this.faq = faq;
    }

    public AppLinkInfo getRedemptionUrl() {
        return redemptionUrl;
    }

    public void setRedemptionUrl(AppLinkInfo redemptionUrl) {
        this.redemptionUrl = redemptionUrl;
    }

    public AppLinkInfo getInfoTokenUrl() {
        return infoTokenUrl;
    }

    public void setInfoTokenUrl(AppLinkInfo infoTokenUrl) {
        this.infoTokenUrl = infoTokenUrl;
    }

    public AppLinkInfo getVoucherUrl() {
        return voucherUrl;
    }

    public void setVoucherUrl(AppLinkInfo voucherUrl) {
        this.voucherUrl = voucherUrl;
    }

    public AppLinkInfo getLoyaltyUrl() {
        return loyaltyUrl;
    }

    public void setLoyaltyUrl(AppLinkInfo loyaltyUrl) {
        this.loyaltyUrl = loyaltyUrl;
    }

    public AppLinkInfo getMissionUrl() {
        return missionUrl;
    }

    public void setMissionUrl(AppLinkInfo missionUrl) {
        this.missionUrl = missionUrl;
    }

    public AppLinkInfo getStationFinderUrl() {
        return stationFinderUrl;
    }

    public void setStationFinderUrl(AppLinkInfo stationFinderUrl) {
        this.stationFinderUrl = stationFinderUrl;
    }

    public AppLinkInfo getFirstStartUrl() {
        return firstStartUrl;
    }

    public void setFirstStartUrl(AppLinkInfo firstStartUrl) {
        this.firstStartUrl = firstStartUrl;
    }

    public AppLinkInfo getParkingCoupon() {
        return parkingCoupon;
    }

    public void setParkingCoupon(AppLinkInfo parkingCoupon) {
        this.parkingCoupon = parkingCoupon;
    }

    public AppLinkInfo getMyCiceroLoginUrl() {
        return myCiceroLoginUrl;
    }

    public void setMyCiceroLoginUrl(AppLinkInfo myCiceroLoginUrl) {
        this.myCiceroLoginUrl = myCiceroLoginUrl;
    }

    public AppLinkInfo getMyMulticardLoginUrl() {
        return myMulticardLoginUrl;
    }

    public void setMyMulticardLoginUrl(AppLinkInfo myMulticardLoginUrl) {
        this.myMulticardLoginUrl = myMulticardLoginUrl;
    }

}
