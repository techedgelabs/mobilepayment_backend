package com.techedge.mp.frontend.adapter.entities.user.v2.setactivedpan;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class SetActiveDpanRequest extends AbstractRequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private SetActiveDpanBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public SetActiveDpanBodyRequest getBody() {
        return body;
    }

    public void setBody(SetActiveDpanBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("SET-ACTIVE-DPAN", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        
        SetActiveDpanRequest setActiveDpanRequest = this;
        SetActiveDpanResponse setActiveDpanResponse = new SetActiveDpanResponse();

        String response = getUserV2ServiceRemote().setActiveDpan(setActiveDpanRequest.getCredential().getRequestID(),
                setActiveDpanRequest.getCredential().getTicketID(), setActiveDpanRequest.getBody().getDpan(),
                setActiveDpanRequest.getBody().getStatus());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        setActiveDpanResponse.setStatus(status);

        return setActiveDpanResponse;
    }

}
