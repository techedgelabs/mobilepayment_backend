package com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ApproveTransactionPaymentMethodBody implements Validable {
	
	private Long id;
	private String type;
	
//	@Override
//	public Status check() {
//		
//		Status status = new Status();
//		
//		if(this.mpCode == null || this.mpCode.length() != 32 || this.mpCode.trim() == "") {
//			
//			status.setStatusCode(StatusCode.REFUEL_CONFIRM_REFUEL_ID_WRONG);
//			
//			return status;
//			
//		}
//		
//		status.setStatusCode(StatusCode.REFUEL_CONFIRM_SUCCESS);
//		
//		return status;
//	}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public Status check() {
		Status status = new Status();
		status.setStatusCode(StatusCode.POP_APPROVE_SUCCESS);
		return status;
	}
}