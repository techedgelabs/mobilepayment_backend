package com.techedge.mp.frontend.adapter.entities.user.validatepaymentmethod;


public class ValidatePaymentMethodBodyResponse {

	private Integer attemptsLeft;

	public Integer getCheckPinAttemptsLeft() {
		return attemptsLeft;
	}
	public void setAttemptsLeft(Integer attemptsLeft) {
		this.attemptsLeft = attemptsLeft;
	}
}
