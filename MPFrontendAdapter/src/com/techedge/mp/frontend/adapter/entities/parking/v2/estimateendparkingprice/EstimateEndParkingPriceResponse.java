package com.techedge.mp.frontend.adapter.entities.parking.v2.estimateendparkingprice;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class EstimateEndParkingPriceResponse extends BaseResponse {
    private EstimateEndParkingPriceResponseBody body;

    public EstimateEndParkingPriceResponseBody getBody() {
        return body;
    }

    public void setBody(EstimateEndParkingPriceResponseBody body) {
        this.body = body;
    }

}
