package com.techedge.mp.frontend.adapter.entities.manager.updatepassword;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ManagerUpdatePasswordRequest extends AbstractRequest implements Validable {

    private Status                           status = new Status();

    private Credential                       credential;
    private ManagerUpdatePasswordBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ManagerUpdatePasswordBodyRequest getBody() {
        return body;
    }

    public void setBody(ManagerUpdatePasswordBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PWD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.MANAGER_PWD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        ManagerUpdatePasswordRequest updatePasswordRequest = this;

        String response = getManagerServiceRemote().updatePassword(updatePasswordRequest.getCredential().getTicketID(), updatePasswordRequest.getCredential().getRequestID(),
                updatePasswordRequest.getBody().getOldPassword(), updatePasswordRequest.getBody().getNewPassword());
        ManagerUpdatePasswordResponse updatePasswordResponse = new ManagerUpdatePasswordResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updatePasswordResponse.setStatus(status);

        return updatePasswordResponse;
    }

}
