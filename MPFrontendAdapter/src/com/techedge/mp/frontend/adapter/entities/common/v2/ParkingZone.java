package com.techedge.mp.frontend.adapter.entities.common.v2;

import java.io.Serializable;
import java.util.List;

public class ParkingZone implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5894369538671332807L;
    private String            id;
    private String            name;
    private String            description;
    private List<String>      notice;
    private Boolean           stallCodeRequired;
    private String            cityId;
    private String            cityName;
    private String            administrativeAreaLevel2Code;
    private Boolean           stickerRequired;
    private String            stickerRequiredNotice;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getNotice() {
        return notice;
    }

    public void setNotice(List<String> notice) {
        this.notice = notice;
    }

    public Boolean getStallCodeRequired() {
        return stallCodeRequired;
    }

    public void setStallCodeRequired(Boolean stallCodeRequired) {
        this.stallCodeRequired = stallCodeRequired;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public Boolean getStickerRequired() {
        return stickerRequired;
    }

    public void setStickerRequired(Boolean stickerRequired) {
        this.stickerRequired = stickerRequired;
    }

    public String getStickerRequiredNotice() {
        return stickerRequiredNotice;
    }

    public void setStickerRequiredNotice(String stickerRequiredNotice) {
        this.stickerRequiredNotice = stickerRequiredNotice;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getAdministrativeAreaLevel2Code() {
        return administrativeAreaLevel2Code;
    }

    public void setAdministrativeAreaLevel2Code(String administrativeAreaLevel2Code) {
        this.administrativeAreaLevel2Code = administrativeAreaLevel2Code;
    }

}
