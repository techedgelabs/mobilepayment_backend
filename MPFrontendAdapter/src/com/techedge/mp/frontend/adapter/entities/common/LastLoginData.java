package com.techedge.mp.frontend.adapter.entities.common;


public class LastLoginData {
	
	private CustomTimestamp lastLoginTime;
	private String          lastLoginDevice;
    
    public LastLoginData(){}

	public CustomTimestamp getLastLoginTime() {
		return lastLoginTime;
	}
	public void setLastLoginTime(CustomTimestamp lastLoginTime) {
		this.lastLoginTime = lastLoginTime;
	}

	public String getLastLoginDevice() {
		return lastLoginDevice;
	}
	public void setLastLoginDevice(String lastLoginDevice) {
		this.lastLoginDevice = lastLoginDevice;
	}
}
