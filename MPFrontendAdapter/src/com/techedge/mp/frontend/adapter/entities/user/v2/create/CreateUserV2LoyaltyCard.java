package com.techedge.mp.frontend.adapter.entities.user.v2.create;

public class CreateUserV2LoyaltyCard {

    private String id;

    public CreateUserV2LoyaltyCard() {

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
