package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.user.v2.addplatenumber.AddPlateNumberRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.authentication.AuthenticationUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.create.CreateUserV2Request;
import com.techedge.mp.frontend.adapter.entities.user.v2.createuserguest.CreateUserGuestRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.externalauthentication.ExternalAuthenticationUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.externalproviderauthentication.ExternalProviderAuthenticationUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.getawardlist.GetAwardListRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.gethomepartnerlist.GetHomePartnerListRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.getlanding.GetLandingRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.getmissions.GetMissionListRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.getpartneractionurl.GetPartnerActionUrlRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.getpartnerlist.GetPartnerListRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.getpromopopup.GetPromoPopupRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.getreceipt.GetReceiptRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.getvirtualizationattemptsleft.VirtualizationAttemptsLeftActionRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.insertpaymentmethod.InsertPaymentMethodRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.loadvoucher.LoadVoucherRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.notificationvirtualization.NotificationVirtualizationRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.promo4me.Promo4MeRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.redemptionaward.GetRedemptionAwardRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.refreshuserdata.RefreshUserDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.removeplatenumber.RemovePlateNumberRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.requestresetpin.RequestResetPinRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.retrievetermsofservice.RetrieveTermsOfServiceRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.retrievetermsofservicebygroup.RetrieveTermsOfServiceByGroupRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.setactivedpan.SetActiveDpanRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.setdefaultplatenumber.SetDefaultPlateNumberRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.skipvirtualization.SkipVirtualizationRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.socialauthentication.SocialAuthenticationUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.updatepassword.UpdatePasswordRequest;
import com.techedge.mp.frontend.adapter.entities.user.v2.updateusersdata.UpdateUsersDataRequest;

public class UserV2Request extends RootRequest {

    protected AuthenticationUserRequest               authentication;
    protected SocialAuthenticationUserRequest         socialAuthentication;
    protected UpdatePasswordRequest                   updatePassword;
    protected UpdateUsersDataRequest                  updateUsersData;
    protected NotificationVirtualizationRequest       notificationVirtualization;
    protected Promo4MeRequest                         promo4Me;
    protected GetLandingRequest                       getLanding;
    protected GetPromoPopupRequest                    getPromoPopup;
    protected VirtualizationAttemptsLeftActionRequest virtualizationAttemptsLeftAction;
    protected InsertPaymentMethodRequest              insertPaymentMethod;
    protected RetrieveTermsOfServiceRequest           retrieveTermsOfService;
    protected CreateUserV2Request                     createUser;
    protected SkipVirtualizationRequest               skipVirtualization;
    protected RetrieveTermsOfServiceByGroupRequest    retrieveTermsOfServiceByGroup;
    protected LoadVoucherRequest                      loadVoucher;
    protected RefreshUserDataRequest                  refreshUserData;
    protected GetPartnerListRequest                   getPartnerList;
    protected GetHomePartnerListRequest               getHomePartnerList;
    protected GetPartnerActionUrlRequest              getPartnerActionUrl;
    protected GetAwardListRequest                     getAwardList;
    protected GetRedemptionAwardRequest               redemptionAward;
    protected GetMissionListRequest                   getMissions;
    protected AddPlateNumberRequest                   addPlateNumber;
    protected RemovePlateNumberRequest                removePlateNumber;
    protected SetDefaultPlateNumberRequest            setDefaultPlateNumber;
    protected CreateUserGuestRequest                  createUserGuest;
    protected GetReceiptRequest                       getReceipt;
    protected ExternalProviderAuthenticationUserRequest externalProviderAuthentication;
    protected RequestResetPinRequest                    requestResetPin;
    protected ExternalAuthenticationUserRequest         externalAuthenticationUser;
    protected SetActiveDpanRequest                      setActiveDpan;

    public AuthenticationUserRequest getAuthentication() {
        return authentication;
    }

    public void setAuthentication(AuthenticationUserRequest authentication) {
        this.authentication = authentication;
    }

    public UpdatePasswordRequest getUpdatePassword() {
        return updatePassword;
    }

    public void setUpdatePassword(UpdatePasswordRequest updatePassword) {
        this.updatePassword = updatePassword;
    }

    public UpdateUsersDataRequest getUpdateUsersData() {
        return updateUsersData;
    }

    public void setUpdateUsersData(UpdateUsersDataRequest updateUsersData) {
        this.updateUsersData = updateUsersData;
    }

    public NotificationVirtualizationRequest getNotificationVirtualization() {
        return notificationVirtualization;
    }

    public void setNotificationVirtualization(NotificationVirtualizationRequest notificationVirtualization) {
        this.notificationVirtualization = notificationVirtualization;
    }

    public Promo4MeRequest getPromo4Me() {
        return promo4Me;
    }

    public void setPromo4Me(Promo4MeRequest promo4Me) {
        this.promo4Me = promo4Me;
    }

    public GetLandingRequest getGetLanding() {
        return getLanding;
    }

    public void setGetLanding(GetLandingRequest getLanding) {
        this.getLanding = getLanding;
    }

    public VirtualizationAttemptsLeftActionRequest getVirtualizationAttemptsLeftAction() {
        return virtualizationAttemptsLeftAction;
    }

    public void setVirtualizationAttemptsLeftAction(VirtualizationAttemptsLeftActionRequest virtualizationAttemptsLeftAction) {
        this.virtualizationAttemptsLeftAction = virtualizationAttemptsLeftAction;
    }

    public InsertPaymentMethodRequest getInsertPaymentMethod() {
        return insertPaymentMethod;
    }

    public void setInsertPaymentMethod(InsertPaymentMethodRequest insertPaymentMethod) {
        this.insertPaymentMethod = insertPaymentMethod;
    }

    public RetrieveTermsOfServiceRequest getRetrieveTermsOfService() {
        return retrieveTermsOfService;
    }

    public void setRetrieveTermsOfService(RetrieveTermsOfServiceRequest retrieveTermsOfService) {
        this.retrieveTermsOfService = retrieveTermsOfService;
    }

    public CreateUserV2Request getCreateUser() {
        return createUser;
    }

    public void setCreateUser(CreateUserV2Request createUserV2) {
        this.createUser = createUserV2;
    }

    public SkipVirtualizationRequest getSkipVirtualization() {
        return skipVirtualization;
    }

    public void setSkipVirtualization(SkipVirtualizationRequest skipVirtualization) {
        this.skipVirtualization = skipVirtualization;
    }

    public RetrieveTermsOfServiceByGroupRequest getRetrieveTermsOfServiceByGroup() {
        return retrieveTermsOfServiceByGroup;
    }

    public void setRetrieveTermsOfServiceByGroup(RetrieveTermsOfServiceByGroupRequest retrieveTermsOfServiceByGroup) {
        this.retrieveTermsOfServiceByGroup = retrieveTermsOfServiceByGroup;
    }

    public LoadVoucherRequest getLoadVoucher() {
        return loadVoucher;
    }

    public void setLoadVoucher(LoadVoucherRequest loadVoucher) {
        this.loadVoucher = loadVoucher;
    }

    public GetPromoPopupRequest getGetPromoPopup() {
        return getPromoPopup;
    }

    public void setGetPromoPopup(GetPromoPopupRequest getPromoPopup) {
        this.getPromoPopup = getPromoPopup;
    }

	public SocialAuthenticationUserRequest getSocialAuthentication() {
        return socialAuthentication;
    }

    public void setSocialAuthentication(SocialAuthenticationUserRequest socialAuthentication) {
        this.socialAuthentication = socialAuthentication;
    }

    public RefreshUserDataRequest getRefreshUserData() {
        return refreshUserData;
    }

    public void setRefreshUserData(RefreshUserDataRequest refreshUserData) {
        this.refreshUserData = refreshUserData;
    }
    
    public GetPartnerListRequest getGetPartnerList() {
        return getPartnerList;
    }

    public void setGetPartnerList(GetPartnerListRequest getPartnerList) {
        this.getPartnerList = getPartnerList;
    }
    
    public GetPartnerActionUrlRequest getGetPartnerActionUrl() {
        return getPartnerActionUrl;
    }

    public void setGetPartnerActionUrl(GetPartnerActionUrlRequest getPartnerActionUrl) {
        this.getPartnerActionUrl = getPartnerActionUrl;
    }

    public GetAwardListRequest getGetAwardList() {
        return getAwardList;
    }

    public void setGetAwardList(GetAwardListRequest getAwardList) {
        this.getAwardList = getAwardList;
    }

    public GetRedemptionAwardRequest getRedemptionAward() {
        return redemptionAward;
    }

    public void setRedemptionAward(GetRedemptionAwardRequest redemptionAward) {
        this.redemptionAward = redemptionAward;
    }
    public GetMissionListRequest getGetMissions() {
        return getMissions;
    }

    public void setGetMissions(GetMissionListRequest getMissions) {
        this.getMissions = getMissions;
    }

	public AddPlateNumberRequest getAddPlateNumber() {
        return addPlateNumber;
    }

    public void setAddPlateNumber(AddPlateNumberRequest addPlateNumber) {
        this.addPlateNumber = addPlateNumber;
    }

    public RemovePlateNumberRequest getRemovePlateNumber() {
        return removePlateNumber;
    }

    public void setRemovePlateNumber(RemovePlateNumberRequest removePlateNumber) {
        this.removePlateNumber = removePlateNumber;
    }

    public SetDefaultPlateNumberRequest getSetDefaultPlateNumber() {
        return setDefaultPlateNumber;
    }

    public void setSetDefaultPlateNumber(SetDefaultPlateNumberRequest setDefaultPlateNumber) {
        this.setDefaultPlateNumber = setDefaultPlateNumber;
    }
    
    public GetReceiptRequest getGetReceipt() {
        return getReceipt;
    }

    public void setGetReceipt(GetReceiptRequest getReceipt) {
        this.getReceipt = getReceipt;
    }

    public CreateUserGuestRequest getCreateUserGuest() {
        return createUserGuest;
    }

    public void setCreateUserGuest(CreateUserGuestRequest createUserGuest) {
        this.createUserGuest = createUserGuest;
    }

    public GetHomePartnerListRequest getGetHomePartnerList() {
        return getHomePartnerList;
    }

    public void setGetHomePartnerList(GetHomePartnerListRequest getHomePartnerList) {
        this.getHomePartnerList = getHomePartnerList;
    }
    
    public ExternalProviderAuthenticationUserRequest getExternalProviderAuthentication() {
        return externalProviderAuthentication;
    }

    public void setExternalProviderAuthentication(ExternalProviderAuthenticationUserRequest externalProviderAuthentication) {
        this.externalProviderAuthentication = externalProviderAuthentication;
    }

    public RequestResetPinRequest getRequestResetPin() {
        return requestResetPin;
    }

    public void setRequestResetPin(RequestResetPinRequest requestResetPin) {
        this.requestResetPin = requestResetPin;
    }

    public ExternalAuthenticationUserRequest getExternalAuthenticationUser() {
        return externalAuthenticationUser;
    }

    public void setExternalAuthenticationUser(ExternalAuthenticationUserRequest externalAuthenticationUser) {
        this.externalAuthenticationUser = externalAuthenticationUser;
    }

    public SetActiveDpanRequest getSetActiveDpan() {
        return setActiveDpan;
    }

    public void setSetActiveDpan(SetActiveDpanRequest setActiveDpan) {
        this.setActiveDpan = setActiveDpan;
    }

}
