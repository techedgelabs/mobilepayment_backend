package com.techedge.mp.frontend.adapter.entities.interfaces;

import com.techedge.mp.frontend.adapter.entities.common.Status;

public interface Validable {

	public Status check();
	
}
