package com.techedge.mp.frontend.adapter.entities.manager.rescuepassword;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ManagerRescuePasswordBodyRequest implements Validable {

	private String email;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		status = Validator.checkEmail("AUTH", email);
		
		if (!Validator.isValid(status.getStatusCode())) {
			status.setStatusCode(StatusCode.MANAGER_RESCUE_PASSWORD_ERROR);
			return status;
		}
				
		return status;
	}
}