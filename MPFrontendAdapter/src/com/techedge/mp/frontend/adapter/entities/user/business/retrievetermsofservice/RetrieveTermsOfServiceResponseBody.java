package com.techedge.mp.frontend.adapter.entities.user.business.retrievetermsofservice;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.DocumentInfo;

public class RetrieveTermsOfServiceResponseBody {
	
    private String termsOfServiceInfo;
    private String termsOfServiceMessage;
	private List<DocumentInfo> termsOfServiceList = new ArrayList<DocumentInfo>(0);

	public String getTermsOfServiceInfo() {
        return termsOfServiceInfo;
    }
    public void setTermsOfServiceInfo(String termsOfServiceInfo) {
        this.termsOfServiceInfo = termsOfServiceInfo;
    }
    
    public List<DocumentInfo> getTermsOfServiceList() {
		return termsOfServiceList;
	}
	public void setTermsOfServiceList(List<DocumentInfo> termsOfServiceList) {
		this.termsOfServiceList = termsOfServiceList;
	}
	
    public String getTermsOfServiceMessage() {
        return termsOfServiceMessage;
    }
    public void setTermsOfServiceMessage(String termsOfServiceMessage) {
        this.termsOfServiceMessage = termsOfServiceMessage;
    }
}
