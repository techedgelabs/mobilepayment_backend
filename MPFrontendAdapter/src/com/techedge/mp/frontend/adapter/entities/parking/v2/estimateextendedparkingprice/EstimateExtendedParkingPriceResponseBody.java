package com.techedge.mp.frontend.adapter.entities.parking.v2.estimateextendedparkingprice;

import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZonePriceInfo;

public class EstimateExtendedParkingPriceResponseBody {
    private ParkingZonePriceInfo parkingZonePriceInfo;

    public ParkingZonePriceInfo getParkingZonePriceInfo() {
        return parkingZonePriceInfo;
    }

    public void setParkingZonePriceInfo(ParkingZonePriceInfo parkingZonePriceInfo) {
        this.parkingZonePriceInfo = parkingZonePriceInfo;
    }

}
