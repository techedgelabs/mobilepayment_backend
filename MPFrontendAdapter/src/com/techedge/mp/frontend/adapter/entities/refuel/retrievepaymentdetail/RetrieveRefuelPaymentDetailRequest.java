package com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail;

import com.techedge.mp.core.business.interfaces.PaymentRefuelDetailResponse;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Receipt;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveRefuelPaymentDetailRequest extends AbstractRequest implements Validable {

    private Credential                             credential;
    private RetrieveRefuelPaymentDetailBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveRefuelPaymentDetailBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveRefuelPaymentDetailBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DETAIL-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;

    }

    @Override
    public BaseResponse execute() {
        RetrieveRefuelPaymentDetailRequest retrieveRefuelPaymentDetailRequest = this;

        PaymentRefuelDetailResponse paymentRefuelDetailResponse = getTransactionServiceRemote().retrieveRefuelPaymentDetail(
                retrieveRefuelPaymentDetailRequest.getCredential().getRequestID(), retrieveRefuelPaymentDetailRequest.getCredential().getTicketID(),
                retrieveRefuelPaymentDetailRequest.getBody().getRefuelID());

        RetrieveRefuelPaymentDetailResponse retrieveRefuelPaymentDetailResponse = new RetrieveRefuelPaymentDetailResponse();

        Status statusResponse = new Status();

        statusResponse.setStatusCode(paymentRefuelDetailResponse.getStatusCode());
        statusResponse.setStatusMessage(prop.getProperty(paymentRefuelDetailResponse.getStatusCode()));

        retrieveRefuelPaymentDetailResponse.setStatus(statusResponse);

        if (Validator.isValid(paymentRefuelDetailResponse.getStatusCode())) {

            RetrieveRefuelPaymentDetailBodyResponse retrieveRefuelPaymentDetailBodyResponse = new RetrieveRefuelPaymentDetailBodyResponse();

            retrieveRefuelPaymentDetailBodyResponse.setRefuelID(paymentRefuelDetailResponse.getRefuelID());
            retrieveRefuelPaymentDetailBodyResponse.setStatus(paymentRefuelDetailResponse.getStatus());
            retrieveRefuelPaymentDetailBodyResponse.setSubStatus(paymentRefuelDetailResponse.getSubStatus());
            retrieveRefuelPaymentDetailBodyResponse.setUseVoucher(paymentRefuelDetailResponse.getUseVoucher());
            retrieveRefuelPaymentDetailBodyResponse.setInitialAmount(AmountConverter.toMobile(paymentRefuelDetailResponse.getInitialAmount()));

            // Valorizzazione scontrino
            Receipt receipt = new Receipt();
            receipt.setRefuelID(paymentRefuelDetailResponse.getRefuelID());
            receipt.setDate(CustomTimestamp.createCustomTimestamp(paymentRefuelDetailResponse.getDate()));
            receipt.setFinalAmount(AmountConverter.toMobile(paymentRefuelDetailResponse.getFinalAmount()));
            receipt.setFuelAmount(AmountConverter.toMobile3(paymentRefuelDetailResponse.getFuelAmount()));
            receipt.setFuelQuantity(paymentRefuelDetailResponse.getFuelQuantity());
            receipt.setBankTransactionID(paymentRefuelDetailResponse.getBankTransactionID());
            receipt.setShopLogin(paymentRefuelDetailResponse.getShopLogin());
            receipt.setCurrency(paymentRefuelDetailResponse.getCurrency());
            receipt.setMaskedPan(paymentRefuelDetailResponse.getMaskedPan());
            receipt.setAuthCode(paymentRefuelDetailResponse.getAuthCode());
            retrieveRefuelPaymentDetailBodyResponse.setReceipt(receipt);

            // Valorizzazione stazione
            LocationData locationData = new LocationData();

            String address = paymentRefuelDetailResponse.getSelectedStationAddress();
            String city = paymentRefuelDetailResponse.getSelectedStationCity();
            String province = paymentRefuelDetailResponse.getSelectedStationProvince();
            String country = paymentRefuelDetailResponse.getSelectedStationCountry();

            locationData.setAddress(address);
            locationData.setCity(city);
            locationData.setProvince(province);
            locationData.setCountry(country);
            locationData.setLatitude(paymentRefuelDetailResponse.getSelectedStationLatitude());
            locationData.setLongitude(paymentRefuelDetailResponse.getSelectedStationLongitude());

            Station station = new Station();
            station.setStationID(paymentRefuelDetailResponse.getSelectedStationID());
            station.setName(paymentRefuelDetailResponse.getSelectedStationName());
            station.setLocationData(locationData);
            retrieveRefuelPaymentDetailBodyResponse.setStation(station);

            // Valorizzazione pompa
            Pump pump = new Pump();
            pump.setPumpID(paymentRefuelDetailResponse.getSelectedPumpID());
            pump.setNumber(paymentRefuelDetailResponse.getSelectedPumpNumber());
            pump.getFuelType().add(paymentRefuelDetailResponse.getSelectedPumpFuelType());
            retrieveRefuelPaymentDetailBodyResponse.setPump(pump);

            retrieveRefuelPaymentDetailResponse.setBody(retrieveRefuelPaymentDetailBodyResponse);
        }

        return retrieveRefuelPaymentDetailResponse;
    }

}
