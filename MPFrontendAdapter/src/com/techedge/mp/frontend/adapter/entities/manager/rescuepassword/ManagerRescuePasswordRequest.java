package com.techedge.mp.frontend.adapter.entities.manager.rescuepassword;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ManagerRescuePasswordRequest extends AbstractRequest implements Validable {

    private Status                           status = new Status();

    private Credential                       credential;
    private ManagerRescuePasswordBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ManagerRescuePasswordBodyRequest getBody() {
        return body;
    }

    public void setBody(ManagerRescuePasswordBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RESCUE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.MANAGER_RESCUE_PASSWORD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        ManagerRescuePasswordRequest rescuePasswordRequest = this;

        String response = getManagerServiceRemote().rescuePassword(rescuePasswordRequest.getCredential().getTicketID(), rescuePasswordRequest.getCredential().getRequestID(),
                rescuePasswordRequest.getBody().getEmail());

        ManagerRescuePasswordResponse rescuePasswordResponse = new ManagerRescuePasswordResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        rescuePasswordResponse.setStatus(status);

        return rescuePasswordResponse;
    }

}
