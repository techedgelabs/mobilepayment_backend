package com.techedge.mp.frontend.adapter.entities.user.updatepin;

import com.techedge.mp.core.business.interfaces.user.UserUpdatePinResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class UpdatePinRequest extends AbstractRequest implements Validable {

    private Status               status = new Status();

    private Credential           credential;
    private UpdatePinRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdatePinRequestBody getBody() {
        return body;
    }

    public void setBody(UpdatePinRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PIN", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.USER_PIN_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        UpdatePinRequest updatePinRequest = this;

        Long paymentMethodId = null;
        String paymentMethodType = null;

        if (updatePinRequest.getBody().getPaymentMethod() != null) {
            paymentMethodId = updatePinRequest.getBody().getPaymentMethod().getId();
            paymentMethodType = updatePinRequest.getBody().getPaymentMethod().getType();
        }

        UserUpdatePinResponse userUpdatePinResponse = getUserServiceRemote().updatePin(updatePinRequest.getCredential().getTicketID(),
                updatePinRequest.getCredential().getRequestID(), paymentMethodId, paymentMethodType, updatePinRequest.getBody().getOldPin(), updatePinRequest.getBody().getNewPin());

        UpdatePinResponse updatePinResponse = new UpdatePinResponse();

        status.setStatusCode(userUpdatePinResponse.getStatusCode());

        status.setStatusMessage(prop.getProperty(userUpdatePinResponse.getStatusCode()));

        UpdatePinResponseBody updatePinResponseBody = new UpdatePinResponseBody();
        updatePinResponseBody.setCheckPinAttemptsLeft(userUpdatePinResponse.getPinCheckMaxAttempts());

        updatePinResponse.setStatus(status);
        updatePinResponse.setBody(updatePinResponseBody);

        return updatePinResponse;
    }

}
