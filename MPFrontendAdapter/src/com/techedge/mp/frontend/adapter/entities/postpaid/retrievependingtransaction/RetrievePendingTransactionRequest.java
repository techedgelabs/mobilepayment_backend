package com.techedge.mp.frontend.adapter.entities.postpaid.retrievependingtransaction;

import java.sql.Timestamp;

import com.techedge.mp.core.business.interfaces.postpaid.CoreProductDetail;
import com.techedge.mp.core.business.interfaces.postpaid.CorePumpDetail;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCartData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetPendingTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelData;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Cash;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.POPStatusConverter;
import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidCartJsonData;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidExtendedRefuelData;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrievePendingTransactionRequest extends AbstractRequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PENDING-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        // TODO da verificare;
        RetrievePendingTransactionRequest retrievePoPPendingTransactionRequest = this;
        RetrievePendingTransactionResponse retrievePoPPendingTransactionResponse = new RetrievePendingTransactionResponse();

        PostPaidGetPendingTransactionResponse postPaidGetPendingTransactionResponse = getPostPaidTransactionServiceRemote().retrievePoPPendingTransaction(
                retrievePoPPendingTransactionRequest.getCredential().getRequestID(), retrievePoPPendingTransactionRequest.getCredential().getTicketID(),
                retrievePoPPendingTransactionRequest.getCredential().getLoyaltySessionID());

        // TODO compilare l'output;
        status.setStatusCode(postPaidGetPendingTransactionResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(postPaidGetPendingTransactionResponse.getStatusCode()));
        retrievePoPPendingTransactionResponse.setStatus(status);

        RetrievePendingTransactionBodyResponse retrievePoPPendingTransactionBodyResponse = new RetrievePendingTransactionBodyResponse();
        retrievePoPPendingTransactionBodyResponse.setSourceType(postPaidGetPendingTransactionResponse.getObjectType());
        retrievePoPPendingTransactionBodyResponse.setSourceStatus(postPaidGetPendingTransactionResponse.getObjectStatus());

        PostPaidTransactionHistoryData postPaidTransactionHistoryData = new PostPaidTransactionHistoryData();
        postPaidTransactionHistoryData.setAmount(AmountConverter.toMobile(postPaidGetPendingTransactionResponse.getAmount()));
        postPaidTransactionHistoryData.setTransactionID(postPaidGetPendingTransactionResponse.getTransactionID());

        postPaidTransactionHistoryData.setStatus(POPStatusConverter.toAppStatus(postPaidGetPendingTransactionResponse.getTransactionStatus()));

        if (postPaidGetPendingTransactionResponse.getCreationTimestamp() != null) {
            postPaidTransactionHistoryData.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(postPaidGetPendingTransactionResponse.getCreationTimestamp().getTime())));
        }

        // postPayedTransactionJsonData.setTransactionType(getDetailForObjectIDJsonResponse.getObjectType());
        Station station = new Station();
        station.setName(postPaidGetPendingTransactionResponse.getStationName());
        station.setStationID(postPaidGetPendingTransactionResponse.getStationID());
        LocationData locationData = new LocationData();
        locationData.setAddress(postPaidGetPendingTransactionResponse.getStationAddress());
        locationData.setCity(postPaidGetPendingTransactionResponse.getStationCity());
        locationData.setCountry(postPaidGetPendingTransactionResponse.getStationCountry());
        locationData.setLatitude(postPaidGetPendingTransactionResponse.getStationLatitude());
        locationData.setLongitude(postPaidGetPendingTransactionResponse.getStationLongitude());
        locationData.setProvince(postPaidGetPendingTransactionResponse.getStationProvince());
        station.setLocationData(locationData);
        retrievePoPPendingTransactionBodyResponse.setStation(station);

        if (postPaidGetPendingTransactionResponse.getPumpInfo() != null) {

            Pump pump = new Pump();
            pump.setPumpID(postPaidGetPendingTransactionResponse.getPumpInfo().getPumpID());
            if (postPaidGetPendingTransactionResponse.getPumpInfo().getPumpNumber() != null) {
                pump.setNumber(Integer.valueOf(postPaidGetPendingTransactionResponse.getPumpInfo().getPumpNumber()));
            }
            pump.setStatus(postPaidGetPendingTransactionResponse.getPumpInfo().getPumpStatus());

            for (CoreProductDetail productDetail : postPaidGetPendingTransactionResponse.getPumpInfo().getProductDetails()) {
                pump.getFuelType().add(productDetail.getProductDescription());
            }

            retrievePoPPendingTransactionBodyResponse.setPump(pump);
        }

        if (postPaidGetPendingTransactionResponse.getPostPaidCartDataList() != null) {
            PostPaidCartJsonData postPaidCartJsonData;
            for (PostPaidCartData postPaidCartData : postPaidGetPendingTransactionResponse.getPostPaidCartDataList()) {
                postPaidCartJsonData = new PostPaidCartJsonData();
                postPaidCartJsonData.setAmount(AmountConverter.toMobile(postPaidCartData.getAmount()));
                postPaidCartJsonData.setProductDescription(postPaidCartData.getProductDescription());
                postPaidCartJsonData.setProductId(postPaidCartData.getProductId());
                postPaidCartJsonData.setQuantity(postPaidCartData.getQuantity());

                postPaidTransactionHistoryData.getCart().add(postPaidCartJsonData);
            }
        }

        if (postPaidGetPendingTransactionResponse.getPostPaidRefuelDataList() != null) {
            PostPaidExtendedRefuelData postPaidRefuelJsonData;
            for (PostPaidRefuelData postPaidRefuelData : postPaidGetPendingTransactionResponse.getPostPaidRefuelDataList()) {
                postPaidRefuelJsonData = new PostPaidExtendedRefuelData();
                postPaidRefuelJsonData.setFuelAmount(postPaidRefuelData.getFuelAmount());
                postPaidRefuelJsonData.setFuelQuantity(postPaidRefuelData.getFuelQuantity());
                postPaidRefuelJsonData.setFuelType(postPaidRefuelData.getFuelType());
                postPaidRefuelJsonData.setProductDescription(postPaidRefuelData.getProductDescription());
                postPaidRefuelJsonData.setProductId(postPaidRefuelData.getProductId());
                postPaidRefuelJsonData.setPumpId(postPaidRefuelData.getPumpId());

                postPaidTransactionHistoryData.getRefuel().add(postPaidRefuelJsonData);
            }
        }

        retrievePoPPendingTransactionBodyResponse.setTransaction(postPaidTransactionHistoryData);

        if (postPaidGetPendingTransactionResponse.getCashInfo() != null) {
            Cash cash = new Cash();
            cash.setCashID(postPaidGetPendingTransactionResponse.getCashInfo().getCashId());

            if (postPaidGetPendingTransactionResponse.getCashInfo().getNumber() != null) {
                cash.setNumber(Integer.valueOf(postPaidGetPendingTransactionResponse.getCashInfo().getNumber()));
            }

            retrievePoPPendingTransactionBodyResponse.setCash(cash);
        }
        if (postPaidGetPendingTransactionResponse.getPumpInfo() != null) {
            System.out.println("Pump in retrievePending is not null");
            CorePumpDetail corePumpDetail = postPaidGetPendingTransactionResponse.getPumpInfo();

            Pump pump = new Pump();
            pump.setStatus(postPaidGetPendingTransactionResponse.getPumpInfo().getPumpStatus());
            if (postPaidGetPendingTransactionResponse.getPumpInfo().getPumpNumber() != null) {
                pump.setNumber(Integer.valueOf(postPaidGetPendingTransactionResponse.getPumpInfo().getPumpNumber()));
            }
            pump.setPumpID(postPaidGetPendingTransactionResponse.getPumpInfo().getPumpID());

            for (CoreProductDetail coreProductDetail : corePumpDetail.getProductDetails()) {

                pump.getFuelType().add(coreProductDetail.getFuelType());

            }
            retrievePoPPendingTransactionBodyResponse.setPump(pump);
        }

        if (postPaidGetPendingTransactionResponse.getRefuelID() != null) {
            postPaidTransactionHistoryData.setTransactionID(postPaidGetPendingTransactionResponse.getRefuelID());
            postPaidTransactionHistoryData.setStatus(postPaidGetPendingTransactionResponse.getStatus());
            postPaidTransactionHistoryData.setSubStatus(postPaidGetPendingTransactionResponse.getSubStatus());
            postPaidTransactionHistoryData.setUseVoucher(postPaidGetPendingTransactionResponse.getUseVoucher());
            postPaidTransactionHistoryData.setAmount(AmountConverter.toMobile(postPaidGetPendingTransactionResponse.getInitialAmount()));
        }

        // Valorizzazione della durata del successivo intervallo di polling
        retrievePoPPendingTransactionBodyResponse.setNextPollingInterval(postPaidGetPendingTransactionResponse.getNextPollingInterval());
        
        retrievePoPPendingTransactionResponse.setBody(retrievePoPPendingTransactionBodyResponse);
        return retrievePoPPendingTransactionResponse;
    }
}