package com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievestation;

import com.techedge.mp.frontend.adapter.entities.common.EnumCodeType;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveStationBodyRequest implements Validable {

	private String codeType;
    private String beaconCode;
    private RetrieveStationUserPositionRequest userPosition;
    
    
	public String getCodeType() {
		return codeType;
	}
	public void setCodeType(String codeType) {
		this.codeType = codeType;
	}
	
	public String getBeaconCode() {
		return beaconCode;
	}
	public void setBeaconCode(String beaconCode) {
		this.beaconCode = beaconCode;
	}
	
	public RetrieveStationUserPositionRequest getUserPosition() {
		return userPosition;
	}
	public void setUserPosition(RetrieveStationUserPositionRequest userPosition) {
		this.userPosition = userPosition;
	}
	
	
	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.codeType == null || this.codeType.trim() == "" || this.codeType.length() > 40) {
			
			status.setStatusCode(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG);
			
			return status;
		}
		
		EnumCodeType codeType = EnumCodeType.valueOf(this.codeType.replaceAll("-", "_"));
		
		switch(codeType) {
		
			case GPS:
			
				if(this.beaconCode != null ) {
					
					status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
					
					return status;
				}
				
				if(this.userPosition == null) {
					
					status.setStatusCode(StatusCode.STATION_RETRIEVE_USER_POSITION_WRONG);
					
					return status;
				}
				else {
					
					status = this.userPosition.check();
					
					if(!Validator.isValid(status.getStatusCode())) {
						
						return status;
					}
				}
				
				status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);
				
				return status;
				
				
			case BEACON_CODE:
			
				if(this.beaconCode != null && this.beaconCode.length() > 20) {
				
					status.setStatusCode(StatusCode.STATION_RETRIEVE_BEACONCODE_WRONG);
					
					return status;
				}
				
				if(this.userPosition != null) {
					
					status = this.userPosition.check();
					
					if(!Validator.isValid(status.getStatusCode())) {
						
						return status;
					}
				}
				
				status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);
				
				return status;
				
			
			default: {
				
				status.setStatusCode(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG);
				
				return status;
			}
		}
	}
}
