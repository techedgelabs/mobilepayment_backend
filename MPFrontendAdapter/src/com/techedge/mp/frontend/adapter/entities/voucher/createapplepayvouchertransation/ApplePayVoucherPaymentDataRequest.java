package com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ApplePayVoucherPaymentDataRequest implements Validable {

    private String  applePayPKPaymentToken;
    private Integer amount;

    public ApplePayVoucherPaymentDataRequest() {}

    public String getApplePayPKPaymentToken() {
        return applePayPKPaymentToken;
    }

    public void setApplePayPKPaymentToken(String applePayPKPaymentToken) {
        this.applePayPKPaymentToken = applePayPKPaymentToken;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.amount == null || this.amount == 0) {

            status.setStatusCode(StatusCode.CREATE_APPLE_PAY_VOUCHER_TRANSACTION_INVALID_PARAMETERS);
            status.setStatusMessage("Invald parameters");

            return status;

        }
        
        if (this.applePayPKPaymentToken == null || this.applePayPKPaymentToken.isEmpty()) {

            status.setStatusCode(StatusCode.CREATE_APPLE_PAY_VOUCHER_TRANSACTION_INVALID_PARAMETERS);
            status.setStatusMessage("Invald parameters");

            return status;

        }

        status.setStatusCode(StatusCode.CREATE_APPLE_PAY_VOUCHER_TRANSACTION_SUCCESS);

        return status;

    }

}