package com.techedge.mp.frontend.adapter.entities.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CustomDate implements Validable {
	
	
	private Integer year;
	private Integer month;
	private Integer day;
	
	
	public CustomDate() {}
	
	private CustomDate(Date date) {
		
		Calendar calendar = Calendar.getInstance();
	    calendar.setTime(date);
	    this.year = calendar.get(Calendar.YEAR);
	    this.month = calendar.get(Calendar.MONTH) + 1;
	    this.day = calendar.get(Calendar.DAY_OF_MONTH);
	    
		
	}
	
	public static CustomDate createCustomDate(Date date) {
		
		if (date != null) {
			
			CustomDate customDate = new CustomDate(date);
			return customDate;
		}
		else {
			
			return null;
		}
	}
	
	/*public CustomDate(int year, int month, int day) {
		
		this.year = year;
		this.month = month;
		this.day = day;
		
	}*/
	
	
	public Integer getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	
	public Integer getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	
	public Integer getDay() {
		return day;
	}
	public void setDay(int day) {
		this.day = day;
	}

	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.year == null && this.month == null && this.day == null) {
			
			status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

			return status;
			
		} 
		
		if(this.year == null || this.year < 1900 || this.year > 2050) {
			
			status.setStatusCode(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG);

			return status;
			
		}
		
		if(this.month == null || this.month < 1 || this.month > 12) {
			
			status.setStatusCode(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG);

			return status;
			
		}
		
		if(this.day == null || this.day < 1 && this.day > 31) {
			
			status.setStatusCode(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG);

			return status;
			
		}
		
		status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

		return status;
	}
	
	
	public Date toDate() {
		
		Date date = null;
		
		String dateSting = this.year + "-" + this.month + "-" + this.day;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			
			date = sdf.parse(dateSting);
			
		} catch (ParseException e) {
			
			return null;
			
		}
		
		return date;
		
	}
	
}
