package com.techedge.mp.frontend.adapter.entities.eventnotification.retrievenotification;

public class RetrieveNotificationBodyResponse {

    private RetrieveNotificationData notification;

    public RetrieveNotificationData getNotification() {
        return notification;
    }

    public void setNotification(RetrieveNotificationData notification) {
        this.notification = notification;
    }

}
