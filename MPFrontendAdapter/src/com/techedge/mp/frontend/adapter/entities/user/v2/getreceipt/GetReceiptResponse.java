package com.techedge.mp.frontend.adapter.entities.user.v2.getreceipt;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetReceiptResponse extends BaseResponse {

    private GetReceiptResponseBody body;

    public GetReceiptResponseBody getBody() {
        return body;
    }

    public void setBody(GetReceiptResponseBody body) {
        this.body = body;
    }

}
