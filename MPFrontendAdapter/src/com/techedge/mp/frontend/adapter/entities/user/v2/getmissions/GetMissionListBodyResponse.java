package com.techedge.mp.frontend.adapter.entities.user.v2.getmissions;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.v2.MissionInfo;
import com.techedge.mp.frontend.adapter.entities.common.v2.PromotionInfo;

public class GetMissionListBodyResponse {

    private List<PromotionInfo> promotions   = new ArrayList<PromotionInfo>(0);
    private List<MissionInfo>          missions = new ArrayList<MissionInfo>(0);
    
    private List<MissionInfo>          completedMissions = new ArrayList<MissionInfo>(0);

    public List<PromotionInfo> getPromotions() {
        return promotions;
    }

    public void setPromotions(List<PromotionInfo> promotions) {
        this.promotions = promotions;
    }

    public List<MissionInfo> getMissions() {
        return missions;
    }

    public void setMissions(List<MissionInfo> missions) {
        this.missions = missions;
    }

    public List<MissionInfo> getCompletedMissions() {
        return completedMissions;
    }

    public void setCompletedMissions(List<MissionInfo> completedMissions) {
        this.completedMissions = completedMissions;
    }


}
