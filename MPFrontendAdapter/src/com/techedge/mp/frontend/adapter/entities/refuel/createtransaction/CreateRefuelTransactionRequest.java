package com.techedge.mp.frontend.adapter.entities.refuel.createtransaction;

import java.util.Date;

import com.techedge.mp.core.business.interfaces.CreateRefuelResponse;
import com.techedge.mp.core.business.interfaces.OperationType;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetPumpStatusResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class CreateRefuelTransactionRequest extends AbstractRequest implements Validable {

    private Status                             status = new Status();

    private CreateRefuelTransactionCredential  credential;
    private CreateRefuelTransactionBodyRequest body;

    public CreateRefuelTransactionCredential getCredential() {
        return credential;
    }

    public void setCredential(CreateRefuelTransactionCredential credential) {
        this.credential = credential;
    }

    public CreateRefuelTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(CreateRefuelTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.credential != null) {

            status = this.credential.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        CreateRefuelTransactionRequest createRefuelTransactionRequest = this;

        String response = "";

        Integer checkPinAttemptsLeft = null;
        Double maxAmount = null;
        Double thresholdAmount = null;

        response = getUserServiceRemote().checkAuthorization(createRefuelTransactionRequest.getCredential().getTicketID(), OperationType.CREATE_TRANSACTION);

        if (!response.equals(ResponseHelper.CHECK_AUTHORIZATION_SUCCESS)) {

            status.setStatusCode(response);
            status.setStatusMessage(prop.getProperty(response));
        }
        else {

            String stationID = createRefuelTransactionRequest.getBody().getRefuelPaymentData().getStationID();
            String pumpID = createRefuelTransactionRequest.getBody().getRefuelPaymentData().getPumpID();
            String requestID = String.valueOf(new Date().getTime());
            String outOfRange = createRefuelTransactionRequest.getBody().getRefuelPaymentData().getOutOfRange();

            Long paymentMethodId = null;
            String paymentMethodType = null;

            if (createRefuelTransactionRequest.getBody().getRefuelPaymentData().getPaymentMethod() != null) {

                paymentMethodId = createRefuelTransactionRequest.getBody().getRefuelPaymentData().getPaymentMethod().getId();
                paymentMethodType = createRefuelTransactionRequest.getBody().getRefuelPaymentData().getPaymentMethod().getType();
            }

            if (stationID != null && pumpID != null) {
                System.out.println(requestID + " - " + stationID + " - " + pumpID);
                if (getForecourtInfoServiceRemote() == null) {
                    System.out.println("FORECOURT NULL");
                }
                GetStationDetailsResponse getStationDetailsResponse = getForecourtInfoServiceRemote().getStationDetails(requestID, stationID, pumpID, Boolean.FALSE);

                if (getStationDetailsResponse != null) {

                    String stationStatusCode = getStationDetailsResponse.getStatusCode();

                    if (Validator.isValid(stationStatusCode)) {

                        if (!getStationDetailsResponse.getStationDetail().getPumpDetails().isEmpty()) {

                            GetPumpStatusResponse getPumpStatusResponse = getForecourtInfoServiceRemote().getPumpStatus(requestID, stationID, pumpID, null);

                            if (Validator.isValid(getPumpStatusResponse.getStatusCode())) {

                                // Il processo di rifornimento viene avviato solo se la pompa � disponibile

                                PumpDetail pumpSelected = null;

                                for (PumpDetail pumpDetail : getStationDetailsResponse.getStationDetail().getPumpDetails()) {

                                    pumpSelected = pumpDetail;
                                }

                                Integer pumpNumber = 0;
                                try {
                                    pumpNumber = Integer.parseInt(pumpSelected.getPumpNumber());
                                }
                                catch (Exception ex) {

                                    System.out.println("Pump number parsing error: " + pumpSelected.getPumpNumber());
                                }

                                String productID = "";
                                String productDescription = "";

                                String refuelMode = getPumpStatusResponse.getRefuelMode();

                                Double amount = AmountConverter.toInternal(createRefuelTransactionRequest.getBody().getRefuelPaymentData().getAmount());
                                Double amountVoucher = 0.0;

                                if (createRefuelTransactionRequest.getBody().getRefuelPaymentData().getAmountVoucher() != null) {
                                    amountVoucher = AmountConverter.toInternal(createRefuelTransactionRequest.getBody().getRefuelPaymentData().getAmountVoucher());
                                }

                                System.out.println("createRefuel");
                                System.out.println("stationID: " + stationID);
                                System.out.println("pumpID: " + pumpID);
                                System.out.println("pumpNumber: " + pumpNumber);
                                System.out.println("productID: " + productID);
                                System.out.println("productDescription: " + productDescription);
                                System.out.println("amount: " + amount);
                                System.out.println("amountVoucher: " + amountVoucher);
                                System.out.println("paymentMethodId: " + paymentMethodId);
                                System.out.println("paymentMethodType: " + paymentMethodType);
                                System.out.println("outOfRange: " + outOfRange);
                                System.out.println("refuelMode: " + refuelMode);

                                CreateRefuelResponse createRefuelResponse = getTransactionServiceRemote().createRefuel(
                                        createRefuelTransactionRequest.getCredential().getTicketID(), createRefuelTransactionRequest.getCredential().getRequestID(),
                                        createRefuelTransactionRequest.getCredential().getPin(), stationID, pumpID, pumpNumber, productID, productDescription, amount,
                                        amountVoucher, createRefuelTransactionRequest.getBody().getRefuelPaymentData().getUseVoucher(), paymentMethodId, paymentMethodType,
                                        outOfRange, refuelMode);

                                checkPinAttemptsLeft = createRefuelResponse.getPinCheckMaxAttempts();

                                System.out.println("checkPinAttemptsLeft: " + createRefuelResponse.getPinCheckMaxAttempts());

                                if (createRefuelResponse.getTransactionID() != null) {

                                    // call verso il BPEL

                                    System.out.println("BPEL starting");

                                    //System.out.println("RESPONSE SHOPLOGIN: " + createRefuelResponse.getShopLogin());
                                    //System.out.println("RESPONSE ACQUIRER_ID: " + createRefuelResponse.getAcquirerID());

                                    String tokenValue = createRefuelResponse.getTokenValue();
                                    if (tokenValue == null) {
                                        tokenValue = "";
                                    }
                                    response = getBpelServiceRemote().startRefuelingProcess(createRefuelResponse.getTransactionID(), stationID, pumpID,
                                            AmountConverter.toInternal(createRefuelTransactionRequest.getBody().getRefuelPaymentData().getAmount()),
                                            createRefuelResponse.getAcquirerID(), createRefuelResponse.getShopLogin(), createRefuelResponse.getCurrency(),
                                            createRefuelResponse.getTransactionID(), tokenValue, "", productDescription, productID);

                                    System.out.println("BPEL response: " + response);

                                    if (Validator.isValid(response)) {

                                        status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);
                                        status.setStatusMessage(prop.getProperty(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS));
                                    }
                                    else {

                                        status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);
                                        status.setStatusMessage(prop.getProperty(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS));
                                    }
                                }
                                else {

                                    maxAmount = createRefuelResponse.getMaxAmount();
                                    thresholdAmount = createRefuelResponse.getThresholdAmount();

                                    status.setStatusCode(createRefuelResponse.getStatusCode());
                                    status.setStatusMessage(prop.getProperty(createRefuelResponse.getStatusCode()));
                                }
                            }
                            else {

                                status.setStatusCode(getPumpStatusResponse.getStatusCode());
                                status.setStatusMessage(prop.getProperty(getPumpStatusResponse.getStatusCode()));
                            }
                        }
                        else {

                            status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_PUMP_NOT_FOUND);
                            status.setStatusMessage(prop.getProperty(StatusCode.REFUEL_TRANSACTION_CREATE_PUMP_NOT_FOUND));
                        }
                    }
                    else {

                        status.setStatusCode(stationStatusCode);
                        status.setStatusMessage(prop.getProperty(stationStatusCode));
                    }
                }
            }
            else {

                status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG);
                status.setStatusMessage(prop.getProperty(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG));
            }
        }

        CreateRefuelTransactionResponse createRefuelTransactionResponse = new CreateRefuelTransactionResponse();

        CreateRefuelTransactionBodyResponse createRefuelTransactionBodyResponse = new CreateRefuelTransactionBodyResponse();
        createRefuelTransactionBodyResponse.setCheckPinAttemptsLeft(checkPinAttemptsLeft);

        if (maxAmount != null) {
            Integer integerMaxAmount = AmountConverter.toMobile(maxAmount);
            createRefuelTransactionBodyResponse.setMaxAmount(integerMaxAmount);
        }
        if (thresholdAmount != null) {
            Integer integerThresholdAmount = AmountConverter.toMobile(thresholdAmount);
            createRefuelTransactionBodyResponse.setThresholdAmount(integerThresholdAmount);
        }

        createRefuelTransactionResponse.setBody(createRefuelTransactionBodyResponse);
        createRefuelTransactionResponse.setStatus(status);

        return createRefuelTransactionResponse;
    }
}