package com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class SetDefaultLoyaltyCardRequest extends AbstractRequest implements Validable {

    private Status                           status = new Status();

    private Credential                       credential;
    private SetDefaultLoyaltyCardBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public SetDefaultLoyaltyCardBodyRequest getBody() {
        return body;
    }

    public void setBody(SetDefaultLoyaltyCardBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("SET-DEFAULT-LOYALTY-CARD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        SetDefaultLoyaltyCardRequest setDefaultLoyaltyCardRequest = this;

        String response = getUserServiceRemote().setDefaultLoyaltyCard(setDefaultLoyaltyCardRequest.getCredential().getTicketID(),
                setDefaultLoyaltyCardRequest.getCredential().getRequestID(), setDefaultLoyaltyCardRequest.getBody().getPanCode(), null);

        SetDefaultLoyaltyCardResponse setDefaultLoyaltyCardResponse = new SetDefaultLoyaltyCardResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        setDefaultLoyaltyCardResponse.setStatus(status);

        return setDefaultLoyaltyCardResponse;

    }

}
