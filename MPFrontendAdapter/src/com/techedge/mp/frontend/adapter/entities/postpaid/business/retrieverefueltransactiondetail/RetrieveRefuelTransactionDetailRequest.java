package com.techedge.mp.frontend.adapter.entities.postpaid.business.retrieverefueltransactiondetail;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.techedge.mp.core.business.interfaces.PostPaidCart;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCreditsVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidRefuel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.POPStatusConverter;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetail;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetailSection;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetailTransaction;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.business.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveRefuelTransactionDetailRequest extends AbstractRequest implements Validable {
    private Status                                     status = new Status();

    private Credential                                 credential;
    private RetrieveRefuelTransactionDetailBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveRefuelTransactionDetailBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveRefuelTransactionDetailBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIVE-REFUEL-TRANSACTION-DETAIL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;
            }
        }
        else {

            status.setStatusCode(StatusCode.USER_BUSINESS_REQU_INVALID_REQUEST);
            return status;
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        RetrieveRefuelTransactionDetailRequest retrieveRefuelTransactionDetailRequest = this;
        RetrieveRefuelTransactionDetailResponse retrieveRefuelTransactionDetailResponse = new RetrieveRefuelTransactionDetailResponse();

        PostPaidGetTransactionDetailResponse postPaidGetTransactionDetailResponse = getPostPaidTransactionServiceRemote().getTransactionDetail(
                retrieveRefuelTransactionDetailRequest.getCredential().getRequestID(), retrieveRefuelTransactionDetailRequest.getCredential().getTicketID(),
                retrieveRefuelTransactionDetailRequest.getBody().getTransactionId());

        status.setStatusCode(postPaidGetTransactionDetailResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(postPaidGetTransactionDetailResponse.getStatusCode()));
        retrieveRefuelTransactionDetailResponse.setStatus(status);

        RetrieveRefuelTransactionDetailBodyResponse retrieveRefuelTransactionDetailBodyResponse = new RetrieveRefuelTransactionDetailBodyResponse();
        retrieveRefuelTransactionDetailBodyResponse.setSourceStatus(postPaidGetTransactionDetailResponse.getSourceStatus());
        retrieveRefuelTransactionDetailBodyResponse.setSourceType(postPaidGetTransactionDetailResponse.getSourceType());

        if (!status.getStatusCode().contains("200")) {
            retrieveRefuelTransactionDetailResponse.setStatus(status);
            return retrieveRefuelTransactionDetailResponse;
        }
        /*
        String iconCardUrl = "";

        try {
            iconCardUrl = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_RECEIPT_ICON_CARD_URL);
        }
        catch (ParameterNotFoundException ex) {
            getLoggerServiceRemote().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "PARAMETER NOT FOUND: " + ex.getMessage());
        }
        */
        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");

        retrieveRefuelTransactionDetailBodyResponse.setSourceStatus(postPaidGetTransactionDetailResponse.getSourceStatus());
        retrieveRefuelTransactionDetailBodyResponse.setSourceType(postPaidGetTransactionDetailResponse.getSourceType());

        Double voucherAmount = 0.0;
        Double bankAmount = 0.0;
        Double transactionAmount = 0.0;
        Integer amount = 0;

        if (postPaidGetTransactionDetailResponse.getAmount() != null) {
            amount = AmountConverter.toMobile(postPaidGetTransactionDetailResponse.getAmount());
            transactionAmount = postPaidGetTransactionDetailResponse.getAmount();
        }

        ReceiptTransactionDetailTransaction transaction = new ReceiptTransactionDetailTransaction();
        transaction.setAmount(amount);
        transaction.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(postPaidGetTransactionDetailResponse.getCreationTimestamp().getTime())));
        transaction.setTransactionID(postPaidGetTransactionDetailResponse.getMpTransactionID());
        transaction.setStatusTitle(postPaidGetTransactionDetailResponse.getStatusTitle());
        transaction.setStatusDescription(postPaidGetTransactionDetailResponse.getStatusDescription());

        // Trascodifica dello stato
        if (postPaidGetTransactionDetailResponse.getStatus() == null) {
            transaction.setStatus(null);
        }
        else {
            String localStatus = POPStatusConverter.toAppStatus(postPaidGetTransactionDetailResponse.getStatus());

            if (!localStatus.isEmpty()) {
                transaction.setStatus(localStatus);
            }
            else {
                transaction.setStatus(postPaidGetTransactionDetailResponse.getStatus());
            }
        }

        transaction.setSubStatus(postPaidGetTransactionDetailResponse.getSubStatus());

        retrieveRefuelTransactionDetailBodyResponse.setTransaction(transaction);

        ReceiptTransactionDetail receipt = new ReceiptTransactionDetail();

        ReceiptTransactionDetailSection sectionRefuel = receipt.createSection("Rifornimento", "RECEIPT", null, false);
        ReceiptTransactionDetailSection sectionPromo = receipt.createSection("Messaggio Promozionale", "PROMO", null, true);
        ReceiptTransactionDetailSection sectionTransaction = receipt.createSection("Pagamento Carta", "RECEIPT", null, true);
        ReceiptTransactionDetailSection sectionElectronicInvoice = receipt.createSection("Fattura Elettronica", "RECEIPT", null, true);
        ReceiptTransactionDetailSection sectionLoyaltyVoucher = receipt.createSection("", "RECEIPT", null, true);

        int sectionPositionRefuel = 1;
        int sectionPositionTransaction = 1;
        int sectionPositionPromo = 1;
        int sectionPositionElectronicInvoice = 1;
        int sectionPositionLoyaltyVoucher = 1;

        String creationTimestamp = new SimpleDateFormat("dd/MM/yyyy").format(postPaidGetTransactionDetailResponse.getCreationTimestamp().getTime());
        receipt.addDataToSection(sectionRefuel, "DATE", "DATA", null, creationTimestamp, sectionPositionRefuel++, null);

        receipt.addDataToSection(sectionRefuel, "STATION_ID", "CODICE PUNTO VENDITA", null, postPaidGetTransactionDetailResponse.getStation().getStationID(),
                sectionPositionRefuel++, null);

        if (!postPaidGetTransactionDetailResponse.getCartList().isEmpty()) {
            for (PostPaidCart cart : postPaidGetTransactionDetailResponse.getCartList()) {
                String cartAmount = "0.0";
                if (cart.getAmount() != null) {
                    cartAmount = currencyFormat.format(cart.getAmount());
                }

                if (postPaidGetTransactionDetailResponse.getCashID() != null && !postPaidGetTransactionDetailResponse.getCashID().equals("")
                        && !postPaidGetTransactionDetailResponse.getCashID().equals(" ")) {
                    receipt.addDataToSection(sectionRefuel, "SHOP_CASH_ID", "COD CASSA", null, postPaidGetTransactionDetailResponse.getCashID(), sectionPositionRefuel++, null);
                }
                if (postPaidGetTransactionDetailResponse.getCashNumber() != null && !postPaidGetTransactionDetailResponse.getCashID().equals("")
                        && !postPaidGetTransactionDetailResponse.getCashID().equals(" ")) {
                    receipt.addDataToSection(sectionRefuel, "SHP_CASH_NUMBER", "N CASSA", null, postPaidGetTransactionDetailResponse.getCashNumber(), sectionPositionRefuel++, null);
                }
                receipt.addDataToSection(sectionRefuel, "SHOP_AMOUNT", "IMPORTO SHOP", "�", cartAmount, sectionPositionRefuel++, null);
            }
        }

        if (!postPaidGetTransactionDetailResponse.getRefuelList().isEmpty()) {
            for (PostPaidRefuel refuel : postPaidGetTransactionDetailResponse.getRefuelList()) {
                receipt.addDataToSection(sectionRefuel, "REFUEL_PUMP_NUMBER", "EROGATORE", null, refuel.getPumpNumber().toString(), sectionPositionRefuel++, null);

                String refuelQuantity = "0.0";
                if (refuel.getFuelQuantity() != null) {
                    refuelQuantity = numberFormat.format(refuel.getFuelQuantity().doubleValue());
                }
                receipt.addDataToSection(sectionRefuel, "REFUEL_FUEL_QUANTITY", "EROGATO", "lt", refuelQuantity, sectionPositionRefuel++, null);
                receipt.addDataToSection(sectionRefuel, "REFUEL_FUEL_TYPE", "TIPO DI CARBURANTE", null, refuel.getProductDescription(), sectionPositionRefuel++, null);

                String refuelAmount = "0.0";
                if (refuel.getFuelAmount() != null) {
                    refuelAmount = currencyFormat.format(refuel.getFuelAmount());
                }
                if (refuel.getFuelAmount() != null && refuel.getFuelAmount() > 0.0) {
                    receipt.addDataToSection(sectionRefuel, "REFUEL_AMOUNT", "IMPORTO CARBURANTE", "�", refuelAmount, sectionPositionRefuel++, null);
                }
            }

        }
        
        if (!postPaidGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList().isEmpty()) {
            for (PostPaidLoadLoyaltyCredits loyaltyCredits : postPaidGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList()) {
                
                if (loyaltyCredits.getMarketingMsg() != null) {
                    String marketingMsg = loyaltyCredits.getMarketingMsg().replaceAll("[\\s\\t\\n\\r ]{2,}", " ");
                    receipt.addDataToSection(sectionPromo, "LOYALTY_MSG", "", null, marketingMsg, sectionPositionPromo++, null);
                }
                else {
                    if (loyaltyCredits.getStatusCode().equals(FidelityResponse.ENABLE_LOYALTY_CARD_MIGRATION_ERROR)
                            || loyaltyCredits.getStatusCode().equals(FidelityResponse.ENABLE_LOYALTY_CARD_MIGRATION_ERROR)) {

                        receipt.addDataToSection(sectionPromo, "LOYALTY_MSG", "", null, loyaltyCredits.getMarketingMsg(), sectionPositionPromo++, null);
                    }
                }
                
                if (!loyaltyCredits.getPostPaidLoadLoyaltyCreditsVoucherList().isEmpty()) {
                    for (PostPaidLoadLoyaltyCreditsVoucher postPaidLoadLoyaltyCreditsVoucher : loyaltyCredits.getPostPaidLoadLoyaltyCreditsVoucherList()) {
                        
                        receipt.addDataToSection(sectionLoyaltyVoucher, "LOYALTY_VOUCHER_CODE", "", null, postPaidLoadLoyaltyCreditsVoucher.getCode(), sectionPositionLoyaltyVoucher++, null);
                    }
                }
            }
        }

        System.out.println("ACQUIRER_ID: " + postPaidGetTransactionDetailResponse.getAcquirerID());

        bankAmount = transactionAmount - voucherAmount;
        String bankAmountString = currencyFormat.format(bankAmount);
        //String creationTimestamp = new SimpleDateFormat("dd/MM/yyyy").format(postPaidGetTransactionDetailResponse.getCreationTimestamp().getTime());
        //receipt.addDataToSection(sectionTransaction, "DATE", "DATA", null, creationTimestamp, sectionPositionTransaction++, null);

        receipt.addDataToSection(sectionTransaction, "AMOUNT", "IMPORTO ADDEBITATO", "�", bankAmountString, sectionPositionTransaction++, null);
        receipt.addDataToSection(sectionTransaction, "PAN", "CARTA DI PAGAMENTO", null, postPaidGetTransactionDetailResponse.getPanCode(), sectionPositionTransaction++, null);

        receipt.addDataToSection(sectionTransaction, "N_OP", "N OPERAZIONE", null, postPaidGetTransactionDetailResponse.getBankTansactionID(), sectionPositionTransaction++, null);
        receipt.addDataToSection(sectionTransaction, "C_AUTH", "COD AUTORIZZAZIONE", null, postPaidGetTransactionDetailResponse.getAuthorizationCode(),
                sectionPositionTransaction++, null);

        String invoiceId = postPaidGetTransactionDetailResponse.getGFGElectronicInvoiceID();
        if (invoiceId == null || invoiceId.isEmpty()) {
            receipt.addDataToSection(sectionElectronicInvoice, "N", "", null, "N. fattura non disponibile", sectionPositionElectronicInvoice++, null);
        }
        else {
            receipt.addDataToSection(sectionElectronicInvoice, "N", "N.", null, postPaidGetTransactionDetailResponse.getGFGElectronicInvoiceID(), sectionPositionElectronicInvoice++, null);
        }

        if (receipt.getSectionDataSize(sectionPromo) == 0) {
            receipt.removeSection(sectionPromo);
        }
        
        if (receipt.getSectionDataSize(sectionLoyaltyVoucher) == 0) {
            receipt.removeSection(sectionLoyaltyVoucher);
        }
        
        retrieveRefuelTransactionDetailBodyResponse.setReceipt(receipt);
        retrieveRefuelTransactionDetailResponse.setBody(retrieveRefuelTransactionDetailBodyResponse);
        retrieveRefuelTransactionDetailResponse.setStatus(status);

        return retrieveRefuelTransactionDetailResponse;
    }
}