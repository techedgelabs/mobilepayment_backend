package com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class VoucherPaymentDataRequest implements Validable {

    private PaymentMethod paymentMethod;
    private Integer        amount;

    public VoucherPaymentDataRequest() {}

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.paymentMethod == null || this.amount == null) {

            status.setStatusCode(StatusCode.CREATE_VOUCHER_TRANSACTION_INVALID_PARAMETERS);
            status.setStatusMessage("Invald parameters");

            return status;

        }

        if (this.paymentMethod.getId() == null || this.paymentMethod.getId().equals("") || this.paymentMethod.getType() == null || this.paymentMethod.getType().equals("")) {
            status.setStatusCode(StatusCode.CREATE_VOUCHER_TRANSACTION_INVALID_PARAMETERS);
            status.setStatusMessage("Invald parameters");

            return status;
        }

        status.setStatusCode(StatusCode.CREATE_VOUCHER_TRANSACTION_SUCCESS);

        return status;

    }

}