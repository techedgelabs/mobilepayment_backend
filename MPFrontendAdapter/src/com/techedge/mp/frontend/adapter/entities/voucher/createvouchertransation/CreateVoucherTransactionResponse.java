package com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class CreateVoucherTransactionResponse extends BaseResponse {
	
    private CreateVoucherTransactionBodyResponse body;

    public CreateVoucherTransactionBodyResponse getBody() {
        return body;
    }

    public void setBody(CreateVoucherTransactionBodyResponse body) {
        this.body = body;
    }

    
    
}
