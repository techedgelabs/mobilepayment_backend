package com.techedge.mp.frontend.adapter.entities.user.authentication;

import com.techedge.mp.frontend.adapter.entities.common.ContactData;
import com.techedge.mp.frontend.adapter.entities.common.EmailSecurityData;
import com.techedge.mp.frontend.adapter.entities.common.LastLoginData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentData;
import com.techedge.mp.frontend.adapter.entities.common.UserData;
import com.techedge.mp.frontend.adapter.entities.common.UserStatus;


public class AuthenticationUserDataResponse extends UserData {
	
	private EmailSecurityData securityData;
	private UserStatus userStatus;
	private ContactData contactData;
	private LastLoginData lastLoginData;
	private PaymentData paymentData;
	
	
	public EmailSecurityData getSecurityData() {
		return securityData;
	}
	
	public void setSecurityData(EmailSecurityData securityData) {
		this.securityData = securityData;
	}
	
	public UserStatus getUserStatus() {
		return userStatus;
	}

	public void setUserStatus(UserStatus userStatus) {
		this.userStatus = userStatus;
	}

	public ContactData getContactData() {
		return contactData;
	}
	public void setContactData(ContactData contactData) {
		this.contactData = contactData;
	}
	
	public LastLoginData getLastLoginData() {
		return lastLoginData;
	}
	public void setLastLoginData(LastLoginData lastLoginData) {
		this.lastLoginData = lastLoginData;
	}

	public PaymentData getPaymentData() {
		return paymentData;
	}
	public void setPaymentData(PaymentData paymentData) {
		this.paymentData = paymentData;
	}
}
