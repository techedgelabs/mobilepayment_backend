package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbympcode;

import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;



public class RetrievePoPTransactionByMPcodeRequest implements Validable {
	
	private Credential credential;
	private RetrievePoPTransactionByMPcodeBodyRequest body;
	
	
	public Credential getCredential() {
		return credential;
	}
	public void setCredential(Credential credential) {
		this.credential = credential;
	}
	
	public RetrievePoPTransactionByMPcodeBodyRequest getBody() {
		return body;
	}
	public void setBody(RetrievePoPTransactionByMPcodeBodyRequest body) {
		this.body = body;
	}
	
	
	@Override
	public Status check() {

		Status status = new Status();
		
		status = Validator.checkCredential("CONFIRM-REFUEL", credential);
		
		if(!Validator.isValid(status.getStatusCode())) {
			
			return status;
			
		}
		
		if(this.body != null) {
			
			status = this.body.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
			
			return status;
			
		}

		return status;
	}
	
}