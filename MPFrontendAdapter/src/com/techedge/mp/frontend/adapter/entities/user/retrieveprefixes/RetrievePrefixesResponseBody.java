package com.techedge.mp.frontend.adapter.entities.user.retrieveprefixes;

import java.util.List;

public class RetrievePrefixesResponseBody {

    private List<String> prefixes;

    public List<String> getPrefixes() {
        return prefixes;
    }

    public void setPrefixes(List<String> prefixes) {
        this.prefixes = prefixes;
    }

    
}
