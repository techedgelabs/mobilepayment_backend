package com.techedge.mp.frontend.adapter.entities.refuel.retrievepending;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Station;


public class RetrievePendingRefuelStation extends Station {
	
	
	private List<Pump> pump = new ArrayList<Pump>(0);

	public List<Pump> getPump() {
		return pump;
	}

	public void setPump(List<Pump> pump) {
		this.pump = pump;
	}	
}
