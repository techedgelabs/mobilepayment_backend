package com.techedge.mp.frontend.adapter.entities.common;

import java.util.Date;

public class SmsReport {
    private String  CorrelationId;
    private String  MessageId;
    private String  DestinationAddress;
    private Integer StatusCode;
    private Integer ReasonCode;
    private Date    OperatorTimeStamp;
    private Date    TimeStamp;
    private String  Operator;
    private String  StatusText;
    private Integer OperatorNetworkCode;

    public String getCorrelationId() {
        return CorrelationId;
    }

    public void setCorrelationId(String correlationId) {
        CorrelationId = correlationId;
    }

    public String getMessageId() {
        return MessageId;
    }

    public void setMessageId(String messageId) {
        MessageId = messageId;
    }

    public String getDestinationAddress() {
        return DestinationAddress;
    }

    public void setDestinationAddress(String destinationAddress) {
        DestinationAddress = destinationAddress;
    }

    public Integer getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(Integer statusCode) {
        StatusCode = statusCode;
    }

    public Integer getReasonCode() {
        return ReasonCode;
    }

    public void setReasonCode(Integer reasonCode) {
        ReasonCode = reasonCode;
    }

    public Date getOperatorTimeStamp() {
        return OperatorTimeStamp;
    }

    public void setOperatorTimeStamp(Date operatorTimeStamp) {
        OperatorTimeStamp = operatorTimeStamp;
    }

    public Date getTimeStamp() {
        return TimeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        TimeStamp = timeStamp;
    }

    public String getOperator() {
        return Operator;
    }

    public void setOperator(String operator) {
        Operator = operator;
    }

    public String getStatusText() {
        return StatusText;
    }

    public void setStatusText(String statusText) {
        StatusText = statusText;
    }

    public Integer getOperatorNetworkCode() {
        return OperatorNetworkCode;
    }

    public void setOperatorNetworkCode(Integer operatorNetworkCode) {
        OperatorNetworkCode = operatorNetworkCode;
    }

}
