package com.techedge.mp.frontend.adapter.entities.user.update;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class UpdateUserRequest extends AbstractRequest implements Validable {

    private Credential            credential;
    private UpdateUserRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateUserRequestBody getBody() {
        return body;
    }

    public void setBody(UpdateUserRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_UPD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        /*
         * UpdateUserRequest updateUserRequest = request.getUpdateUser();
         * 
         * Date birthDate;
         * 
         * if (updateUserRequest.getBody().getUserData().getDateOfBirth() == null) {
         * 
         * birthDate = null;
         * }
         * else {
         * 
         * String birthDateSting = updateUserRequest.getBody().getUserData().getDateOfBirth().getYear() + "-"
         * + updateUserRequest.getBody().getUserData().getDateOfBirth().getMonth() + "-" + updateUserRequest.getBody().getUserData().getDateOfBirth().getDay();
         * 
         * SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
         * 
         * try {
         * birthDate = sdf.parse(birthDateSting);
         * }
         * catch (ParseException e) {
         * status = new Status();
         * status.setStatusCode("USER-CREATE-500");
         * status.setStatusMessage("System error");
         * return Response.status(200).entity(status).build();
         * }
         * }
         * 
         * User user = new User();
         * 
         * PersonalData personalData = new PersonalData();
         * 
         * personalData.setFirstName(updateUserRequest.getBody().getUserData().getFirstName());
         * personalData.setLastName(updateUserRequest.getBody().getUserData().getLastName());
         * personalData.setFiscalCode(updateUserRequest.getBody().getUserData().getFiscalCode());
         * personalData.setBirthDate(birthDate);
         * personalData.setBirthMunicipality(updateUserRequest.getBody().getUserData().getBirthMunicipality());
         * personalData.setBirthProvince(updateUserRequest.getBody().getUserData().getBirthProvince());
         * personalData.setLanguage(updateUserRequest.getBody().getUserData().getLanguage());
         * personalData.setSex(updateUserRequest.getBody().getUserData().getSex());
         * 
         * Address address = new Address();
         * if (updateUserRequest.getBody().getUserData().getAddressData() != null) {
         * address.setCity(updateUserRequest.getBody().getUserData().getAddressData().getCity());
         * address.setCountryCodeId(updateUserRequest.getBody().getUserData().getAddressData().getCountryCodeId());
         * address.setCountryCodeName(updateUserRequest.getBody().getUserData().getAddressData().getCountryCodeName());
         * address.setHouseNumber(updateUserRequest.getBody().getUserData().getAddressData().getHouseNumber());
         * address.setRegion(updateUserRequest.getBody().getUserData().getAddressData().getRegion());
         * address.setStreet(updateUserRequest.getBody().getUserData().getAddressData().getStreet());
         * address.setZipcode(updateUserRequest.getBody().getUserData().getAddressData().getZipCode());
         * }
         * else {
         * address.setCity(null);
         * address.setCountryCodeId(null);
         * address.setCountryCodeName(null);
         * address.setHouseNumber(null);
         * address.setRegion(null);
         * address.setStreet(null);
         * address.setZipcode(null);
         * }
         * personalData.setAddress(address);
         * 
         * Address billingAddress = new Address();
         * if (updateUserRequest.getBody().getUserData().getBillingAddressData() != null) {
         * billingAddress.setCity(updateUserRequest.getBody().getUserData().getBillingAddressData().getCity());
         * billingAddress.setCountryCodeId(updateUserRequest.getBody().getUserData().getBillingAddressData().getCountryCodeId());
         * billingAddress.setCountryCodeName(updateUserRequest.getBody().getUserData().getBillingAddressData().getCountryCodeName());
         * billingAddress.setHouseNumber(updateUserRequest.getBody().getUserData().getBillingAddressData().getHouseNumber());
         * billingAddress.setRegion(updateUserRequest.getBody().getUserData().getBillingAddressData().getRegion());
         * billingAddress.setStreet(updateUserRequest.getBody().getUserData().getBillingAddressData().getStreet());
         * billingAddress.setZipcode(updateUserRequest.getBody().getUserData().getBillingAddressData().getZipCode());
         * }
         * else {
         * billingAddress.setCity(null);
         * billingAddress.setCountryCodeId(null);
         * billingAddress.setCountryCodeName(null);
         * billingAddress.setHouseNumber(null);
         * billingAddress.setRegion(null);
         * billingAddress.setStreet(null);
         * billingAddress.setZipcode(null);
         * }
         * personalData.setBillingAddress(billingAddress);
         * 
         * user.setPersonalData(personalData);
         * 
         * user.setContactDataMobilephone(updateUserRequest.getBody().getUserData().getContactData().getMobilePhone());
         * 
         * String response = userService.updateUser(updateUserRequest.getCredential().getTicketID(), updateUserRequest.getCredential().getRequestID(), user);
         * 
         * UpdateUserResponse updateUserResponse = new UpdateUserResponse();
         * 
         * status.setStatusCode(response);
         * status.setStatusMessage(prop.getProperty(response));
         * 
         * updateUserResponse.setStatus(status);
         * 
         * json = gson.toJson(updateUserResponse);
         * // logger.log(Level.INFO, "response : " + json.toString());
         * loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "userJsonHandler", null, "closing", json.toString());
         * 
         * return Response.status(200).entity(updateUserResponse).build();
         */
        return null;
    }

}
