package com.techedge.mp.frontend.adapter.entities.user.update;

import com.techedge.mp.frontend.adapter.entities.common.ContactData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UpdateUserDataRequest extends UpdateUserUserData implements Validable {

	
	private UpdateUserContactData contactData;
	
	
	public ContactData getContactData() {
		return contactData;
	}

	public void setContactData(UpdateUserContactData contactData) {
		this.contactData = contactData;
	}

	
	@Override
	public Status check() {
		
		// controllo per verificare se almeno una modifica � presente nella chiamata 
		boolean oneModify = false;
		
		Status status = new Status();
		
		status = super.check();
		
		if(!Validator.isValid(status.getStatusCode())) {
			
			return status;
			
		} else {
			
			oneModify = true;
			
		}
		
		if(this.contactData != null) {
			
			oneModify = true;
			
			status = this.contactData.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
		}
		
		
		if(oneModify == false) {
			
			status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
			
		} else {
		
			status.setStatusCode(StatusCode.USER_UPD_SUCCESS);

		}
		
		return status;
		
	}
	
}
