package com.techedge.mp.frontend.adapter.entities.user.v2.getpartnerlist;

import com.techedge.mp.core.business.interfaces.CategoryEarnDataDetail;
import com.techedge.mp.core.business.interfaces.PartnerDetailInfoData;
import com.techedge.mp.core.business.interfaces.PartnerListInfoDataResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.CategoryEarnInfo;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.PartnerInfo;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetPartnerListRequest extends AbstractRequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-PARTNER-LIST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        else {

            status.setStatusCode(StatusCode.GET_PARTNER_LIST_SUCCESS);

        }

        return status;

    }

    @Override
    public BaseResponse execute() {

        GetPartnerListBodyResponse partnerListBodyResponse = new GetPartnerListBodyResponse();
        GetPartnerListResponse getPartnerListResponse = new GetPartnerListResponse();

        PartnerListInfoDataResponse partnerListInfoDataResponse = getUserV2ServiceRemote().getPartnerListInfo(this.getCredential().getRequestID(),
                this.getCredential().getTicketID());

        if (partnerListInfoDataResponse != null && !partnerListInfoDataResponse.getPartnerDetailInfoData().isEmpty()){

            for (PartnerDetailInfoData partnerDetailInfoData : partnerListInfoDataResponse.getPartnerDetailInfoData()) {

                PartnerInfo partnerInfo = new PartnerInfo();
                partnerInfo.setCategoryId(partnerDetailInfoData.getCategoryId());
                partnerInfo.setCategory(partnerDetailInfoData.getCategory());
                partnerInfo.setDescription(partnerDetailInfoData.getDescription());
                partnerInfo.setLogic(partnerDetailInfoData.getLogic());
                partnerInfo.setLogoSmallUrl(partnerDetailInfoData.getLogoSmallUrl());
                partnerInfo.setLogoUrl(partnerDetailInfoData.getLogoUrl());
                partnerInfo.setName(partnerDetailInfoData.getName());
                partnerInfo.setPartnerId(partnerDetailInfoData.getPartnerId());
                partnerInfo.setTitle(partnerDetailInfoData.getTitle());
                partnerListBodyResponse.getPartnerList().add(partnerInfo);
            }
        }
        if (partnerListInfoDataResponse != null && !partnerListInfoDataResponse.getCategoryDataDetailList().isEmpty()) {
            
            for(CategoryEarnDataDetail categoryEarnDataDetail : partnerListInfoDataResponse.getCategoryDataDetailList()){
                CategoryEarnInfo categoryEarnInfo = new CategoryEarnInfo();
                
                categoryEarnInfo.setCategoryId(categoryEarnDataDetail.getCategoryId());
                categoryEarnInfo.setCategory(categoryEarnDataDetail.getCategory());
                categoryEarnInfo.setCategoryImageUrl(categoryEarnDataDetail.getCategoryImageUrl());
                
                partnerListBodyResponse.getCategoryEarnList().add(categoryEarnInfo);
            }

        }

        status.setStatusCode(partnerListInfoDataResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(partnerListInfoDataResponse.getStatusCode()));

        getPartnerListResponse.setStatus(status);
        getPartnerListResponse.setBody(partnerListBodyResponse);

        return getPartnerListResponse;

    }

}
