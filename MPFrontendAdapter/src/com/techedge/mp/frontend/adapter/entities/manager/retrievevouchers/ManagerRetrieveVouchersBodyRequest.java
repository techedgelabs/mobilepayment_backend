package com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers;

import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ManagerRetrieveVouchersBodyRequest implements Validable {

    private String  stationID;
    private CustomDate startDate;
    private CustomDate endDate;

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    

    public CustomDate getStartDate() {
        return startDate;
    }

    public void setStartDate(CustomDate startDate) {
        this.startDate = startDate;
    }

    public CustomDate getEndDate() {
        return endDate;
    }

    public void setEndDate(CustomDate endDate) {
        this.endDate = endDate;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.stationID == null || this.stationID.trim() == "") {

            status.setStatusCode(StatusCode.MANAGER_RETRIEVE_VOUCHERS_INVALID_PARAMETERS);

            return status;
        }

        if (this.startDate == null) {

            status.setStatusCode(StatusCode.MANAGER_RETRIEVE_VOUCHERS_INVALID_PARAMETERS);

            return status;
        }
        
        if (this.endDate == null) {

            status.setStatusCode(StatusCode.MANAGER_RETRIEVE_VOUCHERS_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.MANAGER_RETRIEVE_VOUCHERS_SUCCESS);
        return status;
    }
}
