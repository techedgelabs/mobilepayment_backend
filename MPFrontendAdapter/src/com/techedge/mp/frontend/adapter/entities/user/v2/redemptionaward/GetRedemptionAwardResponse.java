package com.techedge.mp.frontend.adapter.entities.user.v2.redemptionaward;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetRedemptionAwardResponse extends BaseResponse {
    private GetRedemptionAwardBodyResponse body;

    public GetRedemptionAwardBodyResponse getBody() {
        return body;
    }

    public void setBody(GetRedemptionAwardBodyResponse body) {
        this.body = body;
    }
    
    

}
