package com.techedge.mp.frontend.adapter.entities.user.business.retrievetermsofservice;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveTermsOfServiceResponse extends BaseResponse {

	private RetrieveTermsOfServiceResponseBody body;

	public RetrieveTermsOfServiceResponseBody getBody() {
		return body;
	}
	public void setBody(RetrieveTermsOfServiceResponseBody body) {
		this.body = body;
	}
}
