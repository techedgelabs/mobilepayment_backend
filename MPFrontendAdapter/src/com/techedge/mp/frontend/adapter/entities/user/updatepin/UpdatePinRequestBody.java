package com.techedge.mp.frontend.adapter.entities.user.updatepin;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UpdatePinRequestBody implements Validable {

	
	private PaymentMethod paymentMethod;
	private String oldPin;
	private String newPin;
	
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	public String getOldPin() {
		return oldPin;
	}
	public void setOldPin(String oldPin) {
		this.oldPin = oldPin;
	}
	
	public String getNewPin() {
		return newPin;
	}
	public void setNewPin(String newPin) {
		this.newPin = newPin;
	}
	
	
	@Override
	public Status check() {

		Status status = new Status();
		
		/* il controllo dipende dalla categoria utente, perci� viene spostato sul core
		if(this.paymentMethod == null) {
			
			status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
			
			return status;
		}
		
		if(this.paymentMethod.getId() == null) {
			
			status.setStatusCode(StatusCode.USER_PIN_METHOD_ID_WRONG);
			
			return status;
		}
		
		if(this.paymentMethod.getType() == null) {
			
			status.setStatusCode(StatusCode.USER_PIN_METHOD_TYPE_WRONG);
			
			return status;
		}
		*/
		
		status = Validator.checkPin("NEWPIN", newPin);
			
		if(!Validator.isValid(status.getStatusCode())) {
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.USER_PIN_SUCCESS);
		
		return status;
		
	}
	
	
}
