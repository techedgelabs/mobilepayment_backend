package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.parking.v2.approveextendparkingtransaction.ApproveExtendParkingTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.parking.v2.approveparkingtransaction.ApproveParkingTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.parking.v2.endparking.EndParkingRequest;
import com.techedge.mp.frontend.adapter.entities.parking.v2.estimateendparkingprice.EstimateEndParkingPriceRequest;
import com.techedge.mp.frontend.adapter.entities.parking.v2.estimateextendedparkingprice.EstimateExtendedParkingPriceRequest;
import com.techedge.mp.frontend.adapter.entities.parking.v2.estimateparkingprice.EstimateParkingPriceRequest;
import com.techedge.mp.frontend.adapter.entities.parking.v2.getcities.GetCitiesRequest;
import com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingtransactiondetail.RetrieveParkingTransactionDetailRequest;
import com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingzone.RetrieveParkingZonesRequest;
import com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingzonebycity.RetrieveParkingZonesByCityRequest;
import com.techedge.mp.frontend.adapter.entities.parking.v2.retrievependingparkingtransaction.RetrievePendingParkingTransactionRequest;

public class ParkingV2Request extends RootRequest {

    RetrieveParkingZonesRequest              retrieveParkingZones;
    RetrieveParkingZonesByCityRequest        retrieveParkingZonesByCity;
    EstimateParkingPriceRequest              estimateParkingPrice;
    ApproveParkingTransactionRequest         approveParkingTransaction;
    EndParkingRequest                        endParking;
    EstimateEndParkingPriceRequest           estimateEndParkingPrice;
    EstimateExtendedParkingPriceRequest      estimateExtendedParkingPrice;
    ApproveExtendParkingTransactionRequest   approveExtendParkingTransaction;
    RetrievePendingParkingTransactionRequest retrievePendingParkingTransaction;
    GetCitiesRequest                         getCities;
    RetrieveParkingTransactionDetailRequest  retrieveParkingTransactionDetail;

    public RetrieveParkingZonesRequest getRetrieveParkingZones() {
        return retrieveParkingZones;
    }

    public void setRetrieveParkingZones(RetrieveParkingZonesRequest retrieveParkingZones) {
        this.retrieveParkingZones = retrieveParkingZones;
    }

    public EstimateParkingPriceRequest getEstimateParkingPrice() {
        return estimateParkingPrice;
    }

    public void setEstimateParkingPrice(EstimateParkingPriceRequest estimateParkingPrice) {
        this.estimateParkingPrice = estimateParkingPrice;
    }

    public ApproveParkingTransactionRequest getApproveParkingTransaction() {
        return approveParkingTransaction;
    }

    public void setApproveParkingTransaction(ApproveParkingTransactionRequest approveParkingTransaction) {
        this.approveParkingTransaction = approveParkingTransaction;
    }

    public EndParkingRequest getEndParking() {
        return endParking;
    }

    public void setEndParking(EndParkingRequest endParking) {
        this.endParking = endParking;
    }

    public EstimateEndParkingPriceRequest getEstimateEndParkingPrice() {
        return estimateEndParkingPrice;
    }

    public void setEstimateEndParkingPrice(EstimateEndParkingPriceRequest estimateEndParkingPrice) {
        this.estimateEndParkingPrice = estimateEndParkingPrice;
    }

    public EstimateExtendedParkingPriceRequest getEstimateExtendedParkingPrice() {
        return estimateExtendedParkingPrice;
    }

    public void setEstimateExtendedParkingPrice(EstimateExtendedParkingPriceRequest estimateExtendedParkingPrice) {
        this.estimateExtendedParkingPrice = estimateExtendedParkingPrice;
    }

    public ApproveExtendParkingTransactionRequest getApproveExtendParkingTransaction() {
        return approveExtendParkingTransaction;
    }

    public void setApproveExtendParkingTransaction(ApproveExtendParkingTransactionRequest approveExtendParkingTransaction) {
        this.approveExtendParkingTransaction = approveExtendParkingTransaction;
    }

    public RetrievePendingParkingTransactionRequest getRetrievePendingParkingTransaction() {
        return retrievePendingParkingTransaction;
    }

    public void setRetrievePendingParkingTransaction(RetrievePendingParkingTransactionRequest retrievePendingParkingTransaction) {
        this.retrievePendingParkingTransaction = retrievePendingParkingTransaction;
    }

	public RetrieveParkingZonesByCityRequest getRetrieveParkingZonesByCity() {
        return retrieveParkingZonesByCity;
    }

    public void setRetrieveParkingZonesByCity(RetrieveParkingZonesByCityRequest retrieveParkingZonesByCity) {
        this.retrieveParkingZonesByCity = retrieveParkingZonesByCity;
    }

    public GetCitiesRequest getGetCities() {
        return getCities;
    }

    public void setGetCities(GetCitiesRequest getCities) {
        this.getCities = getCities;
    }

    public RetrieveParkingTransactionDetailRequest getRetrieveParkingTransactionDetail() {
        return retrieveParkingTransactionDetail;
    }

    public void setRetrieveParkingTransactionDetail(RetrieveParkingTransactionDetailRequest retrieveParkingTransactionDetail) {
        this.retrieveParkingTransactionDetail = retrieveParkingTransactionDetail;
    }
    
}
