package com.techedge.mp.frontend.adapter.entities.user.getavailableloyaltycards;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.LoyaltyCardData;

public class GetAvailableLoyaltyCardsBodyResponse {

    private Integer               balance;
    private List<LoyaltyCardData> cards = new ArrayList<LoyaltyCardData>(0);

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public List<LoyaltyCardData> getCards() {
        return cards;
    }

    public void setCards(List<LoyaltyCardData> cards) {
        this.cards = cards;
    }
}
