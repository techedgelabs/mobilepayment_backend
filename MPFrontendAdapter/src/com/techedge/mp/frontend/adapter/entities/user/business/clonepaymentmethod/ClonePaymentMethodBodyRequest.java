package com.techedge.mp.frontend.adapter.entities.user.business.clonepaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class ClonePaymentMethodBodyRequest implements Validable {

		
	@Override
	public Status check() {

		Status status = new Status();
		
		status.setStatusCode(StatusCode.USER_BUSINESS_CLONE_PAYMENT_METHOD_SUCCESS);
		
		return status;
		
	}
	
}
