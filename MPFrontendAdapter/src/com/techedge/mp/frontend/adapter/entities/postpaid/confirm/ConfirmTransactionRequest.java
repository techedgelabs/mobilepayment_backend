package com.techedge.mp.frontend.adapter.entities.postpaid.confirm;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ConfirmTransactionRequest extends AbstractRequest implements Validable {

    private Credential                    credential;
    private ConfirmTransactionBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ConfirmTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(ConfirmTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CONFIRM-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        ConfirmTransactionRequest confirmTransactionRequest = this;

        String response = getPostPaidTransactionServiceRemote().confirmTransaction(confirmTransactionRequest.getCredential().getTicketID(),
                confirmTransactionRequest.getCredential().getRequestID(), confirmTransactionRequest.getBody().getTransactionID());

        ConfirmTransactionResponse confirmTransactionResponse = new ConfirmTransactionResponse();

        Status statusResponse = new Status();

        statusResponse.setStatusCode(response);
        statusResponse.setStatusMessage(prop.getProperty(response));

        confirmTransactionResponse.setStatus(statusResponse);

        return confirmTransactionResponse;
    }

}