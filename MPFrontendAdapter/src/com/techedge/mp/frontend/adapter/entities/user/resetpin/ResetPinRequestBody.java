package com.techedge.mp.frontend.adapter.entities.user.resetpin;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ResetPinRequestBody implements Validable {

    private String password;
    private String newPin;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNewPin() {
        return newPin;
    }

    public void setNewPin(String newPin) {
        this.newPin = newPin;
    }

    @Override
    public Status check() {

        Status status = new Status();

        //status = Validator.checkPassword("PWD", password);

        //if (!Validator.isValid(status.getStatusCode())) {

        //    return status;
        //}

        if (password == null || password.trim().equals("") || password.length() > 40 || newPin == null || newPin.length() != 4) {

            status.setStatusCode(StatusCode.USER_RESET_PIN_INVALID_PASSWORD);

            return status;
        }

        status.setStatusCode(StatusCode.USER_RESET_PIN_SUCCESS);

        return status;

    }
}
