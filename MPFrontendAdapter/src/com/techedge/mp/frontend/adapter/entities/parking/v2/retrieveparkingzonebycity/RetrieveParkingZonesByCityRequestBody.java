package com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingzonebycity;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveParkingZonesByCityRequestBody implements Validable {

    private String cityId;

    @Override
    public Status check() {

        Status status = new Status();

        if (this.cityId == null || cityId.isEmpty()) {

            status.setStatusCode("TBD");

            return status;

        }

        status.setStatusCode(StatusCode.RETRIEVE_PARKING_ZONES_SUCCESS);

        return status;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

}
