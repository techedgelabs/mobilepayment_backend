package com.techedge.mp.frontend.adapter.entities.common;

public class AmountInfo {
	
	private Integer amount;		
	private String type;
	
	public AmountInfo() {}

	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
