package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation.CreateApplePayVoucherTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation.CreateVoucherTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.voucher.retrievevouchertransactiondetail.RetrieveVoucherTransactionDetailRequest;

public class VoucherRequest extends RootRequest {

    protected CreateVoucherTransactionRequest         createVoucherTransaction;
    protected CreateApplePayVoucherTransactionRequest createApplePayVoucherTransaction;
    protected RetrieveVoucherTransactionDetailRequest retrieveVoucherTransactionDetail;

    public CreateVoucherTransactionRequest getCreateVoucherTransaction() {
        return createVoucherTransaction;
    }

    public void setCreateVoucherTransaction(CreateVoucherTransactionRequest createVoucherTransaction) {
        this.createVoucherTransaction = createVoucherTransaction;
    }

    public CreateApplePayVoucherTransactionRequest getCreateApplePayVoucherTransaction() {
        return createApplePayVoucherTransaction;
    }

    public void setCreateApplePayVoucherTransaction(CreateApplePayVoucherTransactionRequest createApplePayVoucherTransaction) {
        this.createApplePayVoucherTransaction = createApplePayVoucherTransaction;
    }

    public RetrieveVoucherTransactionDetailRequest getRetrieveVoucherTransactionDetail() {
        return retrieveVoucherTransactionDetail;
    }

    public void setRetrieveVoucherTransactionDetail(RetrieveVoucherTransactionDetailRequest retrieveVoucherTransactionDetail) {
        this.retrieveVoucherTransactionDetail = retrieveVoucherTransactionDetail;
    }

}
