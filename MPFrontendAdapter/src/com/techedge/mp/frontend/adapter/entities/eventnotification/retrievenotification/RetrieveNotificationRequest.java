package com.techedge.mp.frontend.adapter.entities.eventnotification.retrievenotification;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.AppLink;
import com.techedge.mp.core.business.interfaces.ParameterNotification;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.pushnotification.NotificationResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveNotificationRequest extends AbstractRequest implements Validable {

    private Status                          status = new Status();

    private Credential                      credential;
    private RetrieveNotificationBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveNotificationBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveNotificationBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-NOTIFICATION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.RETRIEVE_NOTIFICATION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        RetrieveNotificationRequest retrieveNotificationRequest = this;
        RetrieveNotificationResponse retrieveNotificationResponse = new RetrieveNotificationResponse();

        RetrieveNotificationBodyResponse retrieveNotificationBodyResponse = new RetrieveNotificationBodyResponse();

        String ticketID = retrieveNotificationRequest.getCredential().getTicketID();
        String requestID = retrieveNotificationRequest.getCredential().getRequestID();
        Long notificationID = retrieveNotificationRequest.getBody().getNotificationID();

        NotificationResponse response = getEventNotificationServiceRemote().retrieveNotification(ticketID, requestID, notificationID);

        if (response.getStatusCode().equals(ResponseHelper.EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_SUCCESS)) {

            Timestamp sendDateTimestamp = new Timestamp(response.getSendDate().getTime());
            Timestamp expireDateTimestamp = new Timestamp(response.getExpireDate().getTime());

            RetrieveNotificationData notification = new RetrieveNotificationData();
            notification.setId(response.getId());
            notification.setSendDate(CustomTimestamp.createCustomTimestamp(sendDateTimestamp));
            notification.setExpireDate(CustomTimestamp.createCustomTimestamp(expireDateTimestamp));
            notification.setText(response.getText());
            notification.setTitle(response.getTitle());
            notification.setType(response.getSource().name());
            notification.setActionText(response.getActionText());
            notification.setShowDialog(response.getShowDialog());
            notification.setActionLocation(response.getActionLocation());
            notification.setAppLink(null);

            if (response.getAppLink() != null) {
                notification.setAppLink(new AppLink());
                if (response.getAppLink().getLocation() != null) {
                    notification.getAppLink().setLocation(response.getAppLink().getLocation());
                }
                if (response.getAppLink().getType() != null) {
                    notification.getAppLink().setType(response.getAppLink().getType());
                }
                if (response.getAppLink().getParameters() != null || !response.getAppLink().getParameters().isEmpty()) {
                    Set<ParameterNotification> parameterNotificationList = new HashSet<>(0);
                    for (ParameterNotification item : response.getAppLink().getParameters()) {
                        ParameterNotification parameterNotificationData = new ParameterNotification(item.getName(), item.getValue());
                        parameterNotificationList.add(parameterNotificationData);
                    }
                    notification.getAppLink().setParameters(parameterNotificationList);

                }

            }

            retrieveNotificationBodyResponse.setNotification(notification);
        }

        status.setStatusCode(response.getStatusCode());

        status.setStatusMessage(prop.getProperty(response.getStatusCode()));

        retrieveNotificationResponse.setStatus(status);

        retrieveNotificationResponse.setBody(retrieveNotificationBodyResponse);

        return retrieveNotificationResponse;

    }
}
