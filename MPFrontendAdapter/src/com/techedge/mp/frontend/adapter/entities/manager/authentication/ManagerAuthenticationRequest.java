package com.techedge.mp.frontend.adapter.entities.manager.authentication;

import com.techedge.mp.core.business.interfaces.ManagerAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.EmailSecurityData;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.UserStatus;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ManagerAuthenticationRequest extends AbstractRequest implements Validable {

    private Status                           status = new Status();

    private ManagerAuthenticationBodyRequest body;

    public ManagerAuthenticationBodyRequest getBody() {
        return body;
    }

    public void setBody(ManagerAuthenticationBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(ResponseHelper.MANAGER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.MANAGER_AUTH_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        ManagerAuthenticationRequest authenticationManagerRequest = this;

        ManagerAuthenticationResponse authenticationResponse = getManagerServiceRemote().authentication(authenticationManagerRequest.getBody().getUsername(),
                authenticationManagerRequest.getBody().getPassword(), authenticationManagerRequest.getBody().getRequestID(), authenticationManagerRequest.getBody().getDeviceID(),
                authenticationManagerRequest.getBody().getDeviceName());

        if (authenticationResponse.getStatusCode().equals(StatusCode.MANAGER_AUTH_SUCCESS)) {

            ManagerAuthenticationResponseSuccess authenticationManagerResponseSuccess = new ManagerAuthenticationResponseSuccess();

            status.setStatusCode(authenticationResponse.getStatusCode());
            status.setStatusMessage(prop.getProperty(authenticationResponse.getStatusCode()));
            authenticationManagerResponseSuccess.setStatus(status);

            ManagerAuthenticationBodyResponse body = new ManagerAuthenticationBodyResponse();

            /***********************************************/

            ManagerAuthenticationManagerData userData = new ManagerAuthenticationManagerData();
            userData.setUsername(authenticationResponse.getManager().getUsername());
            userData.setFirstName(authenticationResponse.getManager().getFirstName());
            userData.setLastName(authenticationResponse.getManager().getLastName());

            EmailSecurityData securityData = new EmailSecurityData();
            securityData.setEmail(authenticationResponse.getManager().getEmail());
            userData.setSecurityData(securityData);

            userData.setAddressData(null);
            userData.setBillingAddressData(null);
            userData.setLastLoginData(null);
            userData.setPaymentData(null);

            UserStatus userStatus = new UserStatus();
            userStatus.setStatus(authenticationResponse.getManager().getStatus());
            userStatus.setRegistrationCompleted(true);
            userData.setUserStatus(userStatus);

            if (authenticationResponse.getManager().getStations() != null) {
                for (com.techedge.mp.core.business.interfaces.Station stationCore : authenticationResponse.getManager().getStations()) {
                    Station station = new Station();
                    station.setName(null);
                    station.setStationID(stationCore.getStationID());
                    LocationData locationData = new LocationData();
                    locationData.setAddress(stationCore.getAddress());
                    locationData.setCity(stationCore.getCity());
                    locationData.setCountry(stationCore.getCountry());
                    locationData.setLatitude(stationCore.getLatitude());
                    locationData.setLongitude(stationCore.getLongitude());
                    locationData.setProvince(stationCore.getProvince());
                    station.setLocationData(locationData);
                    userData.getStation().add(station);
                }
            }

            body.setTicketID(authenticationResponse.getTicketId());
            body.setUserData(userData);

            authenticationManagerResponseSuccess.setBody(body);

            return authenticationManagerResponseSuccess;
        }
        else {
            ManagerAuthenticationResponseFailure authenticationManagerResponseFailure = new ManagerAuthenticationResponseFailure();

            status.setStatusCode(authenticationResponse.getStatusCode());
            status.setStatusMessage(prop.getProperty(authenticationResponse.getStatusCode()));

            authenticationManagerResponseFailure.setStatus(status);

            return authenticationManagerResponseFailure;
        }
    }

}
