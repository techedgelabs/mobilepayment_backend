package com.techedge.mp.frontend.adapter.entities.user.updatetermsofservice;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.TermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UpdateTermsOfServiceRequestBody implements Validable {

    private List<TermsOfServiceData> termsOfService = new ArrayList<TermsOfServiceData>(0);

    public List<TermsOfServiceData> getTermsOfService() {
        return termsOfService;
    }

    public void setTermsOfService(List<TermsOfServiceData> termsOfService) {
        this.termsOfService = termsOfService;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.termsOfService != null) {

            for (int i = 0; i < termsOfService.size(); i++) {

                status = this.termsOfService.get(i).check();

                if (!Validator.isValid(status.getStatusCode())) {

                    return status;

                }
            }
        }

        status.setStatusCode(StatusCode.USER_UPDATE_TERMS_OF_SERVICE_SUCCESS);

        return status;
    }

}
