package com.techedge.mp.frontend.adapter.entities.user.resendverificationcode;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ResendValidationRequest extends AbstractRequest implements Validable {

    private Status                      status = new Status();

    private Credential                  credential;
    private ResendValidationRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ResendValidationRequestBody getBody() {
        return body;
    }

    public void setBody(ResendValidationRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RESEND-VALIDATION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {
            status.setStatusCode(StatusCode.USER_RESEND_VALIDATATION_INVALID_PARAMETERS);
            return status;

        }

        status.setStatusCode(StatusCode.USER_RESEND_VALIDATATION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        ResendValidationRequest resendValidationRequest = this;
        ResendVerificationCodeResponse resendVerificationCodeResponse = new ResendVerificationCodeResponse();

        String response = getUserServiceRemote().userResendValidation(resendValidationRequest.getCredential().getTicketID(),
                resendValidationRequest.getCredential().getRequestID(), resendValidationRequest.getBody().getId(), resendValidationRequest.getBody().getType());

        status.setStatusCode(response);

        status.setStatusMessage(prop.getProperty(response));

        resendVerificationCodeResponse.setStatus(status);

        return resendVerificationCodeResponse;
    }

}
