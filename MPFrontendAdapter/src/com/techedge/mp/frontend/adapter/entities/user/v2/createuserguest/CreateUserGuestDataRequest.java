package com.techedge.mp.frontend.adapter.entities.user.v2.createuserguest;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateUserGuestDataRequest implements Validable {

    private String captchaCode;

    public String getCaptchaCode() {
        return captchaCode;
    }

    public void setCaptchaCode(String captchaCode) {
        this.captchaCode = captchaCode;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.captchaCode == null || this.captchaCode.isEmpty()) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
            return status;
        }

        status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

        return status;

    }

}