package com.techedge.mp.frontend.adapter.entities.user.v2.authentication;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class AuthenticationUserBodyRequest implements Validable {

    private String username;
    private String passwordH;
    private String passwordE;
    private String requestID;
    private String deviceID;
    private String deviceName;
    private String deviceToken;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordH() {
        return passwordH;
    }

    public void setPasswordH(String passwordH) {
        this.passwordH = passwordH;
    }

    public String getPasswordE() {
        return passwordE;
    }

    public void setPasswordE(String passwordE) {
        this.passwordE = passwordE;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.username == null || this.username.length() > 50) {

            status.setStatusCode(StatusCode.USER_V2_AUTH_EMAIL_WRONG);

            return status;

        }

        if (this.passwordH == null || this.passwordH.length() > 40) {

            status.setStatusCode(StatusCode.USER_V2_AUTH_PASSWORD_WRONG);

            return status;

        }

        if (this.requestID == null || this.requestID.length() > 20 || this.requestID.trim().isEmpty()) {

            status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

            return status;

        }
        
        if (this.requestID.startsWith("WPH")) {

            status.setStatusCode(StatusCode.USER_V2_REQU_UNSUPPORTED_DEVICE);

            return status;

        }

        if (this.deviceID != null) {

            if (this.deviceID.length() > 40 || this.deviceID.trim().isEmpty()) {

                status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

                return status;

            }
        }

        if (this.deviceName != null) {

            if (this.deviceName.length() > 100 || this.deviceName.trim().isEmpty()) {

                status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

                return status;

            }

        }

        if (this.deviceToken != null && this.deviceToken.trim().isEmpty()) {

                status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

                return status;
        }

        status.setStatusCode(StatusCode.USER_V2_AUTH_SUCCESS);

        return status;
    }

}
