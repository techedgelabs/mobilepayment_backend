package com.techedge.mp.frontend.adapter.entities.user.create;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.Address;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.TermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class CreateUserRequest extends AbstractRequest implements Validable {

    private Status                status = new Status();

    private Credential            credential;
    private CreateUserRequestBody body;

    public CreateUserRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateUserRequestBody getBody() {
        return body;
    }

    public void setBody(CreateUserRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        CreateUserRequest createUserRequest = this;

        String birthDateSting = createUserRequest.getBody().getUserData().getDateOfBirth().getYear() + "-" + createUserRequest.getBody().getUserData().getDateOfBirth().getMonth()
                + "-" + createUserRequest.getBody().getUserData().getDateOfBirth().getDay();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date birthDate;
        try {
            birthDate = sdf.parse(birthDateSting);
        }
        catch (ParseException e) {
            status = new Status();
            status.setStatusCode(StatusCode.USER_CREATE_DATE_BIRTH_WRONG);
            status.setStatusMessage(prop.getProperty(status.getStatusCode()));
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setStatus(status);
            return baseResponse;
        }

        User user = new User();

        PersonalData personalData = new PersonalData();

        personalData.setFirstName(createUserRequest.getBody().getUserData().getFirstName());
        personalData.setLastName(createUserRequest.getBody().getUserData().getLastName());
        personalData.setFiscalCode(createUserRequest.getBody().getUserData().getFiscalCode());
        personalData.setBirthDate(birthDate);
        personalData.setBirthMunicipality(createUserRequest.getBody().getUserData().getBirthMunicipality());
        personalData.setBirthProvince(createUserRequest.getBody().getUserData().getBirthProvince());
        personalData.setLanguage(createUserRequest.getBody().getUserData().getLanguage());
        personalData.setSex(createUserRequest.getBody().getUserData().getSex());
        personalData.setSecurityDataEmail(createUserRequest.getBody().getUserData().getSecurityData().getEmail());
        personalData.setSecurityDataPassword(createUserRequest.getBody().getUserData().getSecurityData().getPassword());

        if (createUserRequest.getBody().getUserData().getAddressData() != null) {
            Address address = new Address();
            address.setCity(createUserRequest.getBody().getUserData().getAddressData().getCity());
            address.setCountryCodeId(createUserRequest.getBody().getUserData().getAddressData().getCountryCodeId());
            address.setCountryCodeName(createUserRequest.getBody().getUserData().getAddressData().getCountryCodeName());
            address.setHouseNumber(createUserRequest.getBody().getUserData().getAddressData().getHouseNumber());
            address.setRegion(createUserRequest.getBody().getUserData().getAddressData().getRegion());
            address.setStreet(createUserRequest.getBody().getUserData().getAddressData().getStreet());
            address.setZipcode(createUserRequest.getBody().getUserData().getAddressData().getZipCode());
            personalData.setAddress(address);
        }
        else {
            personalData.setAddress(null);
        }

        if (createUserRequest.getBody().getUserData().getBillingAddressData() != null) {
            Address billingAddress = new Address();
            billingAddress.setCity(createUserRequest.getBody().getUserData().getBillingAddressData().getCity());
            billingAddress.setCountryCodeId(createUserRequest.getBody().getUserData().getBillingAddressData().getCountryCodeId());
            billingAddress.setCountryCodeName(createUserRequest.getBody().getUserData().getBillingAddressData().getCountryCodeName());
            billingAddress.setHouseNumber(createUserRequest.getBody().getUserData().getBillingAddressData().getHouseNumber());
            billingAddress.setRegion(createUserRequest.getBody().getUserData().getBillingAddressData().getRegion());
            billingAddress.setStreet(createUserRequest.getBody().getUserData().getBillingAddressData().getStreet());
            billingAddress.setZipcode(createUserRequest.getBody().getUserData().getBillingAddressData().getZipCode());
            personalData.setBillingAddress(billingAddress);
        }
        else {
            personalData.setBillingAddress(null);
        }

        if (createUserRequest.getBody().getUserData().getTermsOfService() != null) {

            for (TermsOfServiceData termsOfServiceData : createUserRequest.getBody().getUserData().getTermsOfService()) {

                TermsOfService termsOfService = new TermsOfService();
                termsOfService.setKeyval(termsOfServiceData.getId());
                termsOfService.setAccepted(termsOfServiceData.getAccepted());
                termsOfService.setValid(Boolean.TRUE);
                personalData.getTermsOfServiceData().add(termsOfService);
            }
        }

        if (createUserRequest.getBody().getUserData().getContactData() != null) {

            for (MobilePhone mobilePhone : createUserRequest.getBody().getUserData().getContactData().getMobilePhones()) {

                user.getMobilePhoneList().add(mobilePhone);
            }
        }
        
        if (createUserRequest.getBody().getUserData().getUserStatus() != null) {
            user.setEniStationUserType(createUserRequest.getBody().getUserData().getUserStatus().isEniStationUserType());
        }

        user.setPersonalData(personalData);
        user.setDeviceId(createUserRequest.getCredential().getDeviceID());

        Long loyaltyCardId = null;
        CreateUserLoyaltyCard createUserLoyaltyCard = createUserRequest.getBody().getUserData().getLoyaltyCard();
        CreateUserResponse createUserResponse = new CreateUserResponse();
        if (createUserLoyaltyCard != null && createUserLoyaltyCard.getId() != null) {
            loyaltyCardId = new Long(createUserLoyaltyCard.getId());
        }

        String response = getUserServiceRemote().createUser(createUserRequest.getCredential().getTicketID(), createUserRequest.getCredential().getRequestID(), user, loyaltyCardId);
        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        createUserResponse.setStatus(status);

        return createUserResponse;
    }

}
