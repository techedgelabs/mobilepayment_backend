package com.techedge.mp.frontend.adapter.entities.voucher.retrievevouchertransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveVoucherTransactionDetailBodyRequest implements Validable {

    private String voucherTransactionId;

    public RetrieveVoucherTransactionDetailBodyRequest() {}

    public String getVoucherTransactionId() {
        return voucherTransactionId;
    }

    public void setVoucherTransactionId(String voucherTransactionId) {
        this.voucherTransactionId = voucherTransactionId;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.voucherTransactionId == null || this.voucherTransactionId.equals("")) {

            status.setStatusCode(StatusCode.RETRIEVE_VOUCHER_TRANSACTION_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.RETRIEVE_VOUCHER_TRANSACTION_SUCCESS);

        return status;
    }

}
