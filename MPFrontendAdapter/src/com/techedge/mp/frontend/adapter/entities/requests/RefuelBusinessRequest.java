package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.refuel.business.createtransaction.CreateRefuelTransactionRequest;

public class RefuelBusinessRequest extends RootRequest {

    protected CreateRefuelTransactionRequest createRefuelTransaction;

    public CreateRefuelTransactionRequest getCreateRefuelTransaction() {
        return createRefuelTransaction;
    }

    public void setCreateRefuelTransaction(CreateRefuelTransactionRequest createRefuelTransaction) {
        this.createRefuelTransaction = createRefuelTransaction;
    }

}
