package com.techedge.mp.frontend.adapter.entities.parking.v2.getcities;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingCity;

public class GetCitiesResponseBody {

    private List<ParkingCity> parkingCityList = new ArrayList<ParkingCity>(0);

    public List<ParkingCity> getParkingCityList() {
        return parkingCityList;
    }

    public void setParkingCityList(List<ParkingCity> parkingCityList) {
        this.parkingCityList = parkingCityList;
    }

}
