package com.techedge.mp.frontend.adapter.entities.manager.authentication;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ManagerAuthenticationBodyRequest implements Validable {
	

	private String username;
	private String password;
	private String requestID;
	private String deviceID;
	private String deviceName;
	
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	public String getDeviceID() {
		return deviceID;
	}
	public void setDeviceID(String deviceID) {
		this.deviceID = deviceID;
	}
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	
	@Override
	public Status check() {
		
		Status status = new Status();
		
		
		if(this.username == null || this.username.length() > 50) {
			
			status.setStatusCode(StatusCode.MANAGER_AUTH_EMAIL_WRONG);
			
			return status;
			
		}

		
		if(this.password == null || this.password.length() > 40) {
			
			status.setStatusCode(StatusCode.MANAGER_AUTH_PASSWORD_WRONG);
			
			return status;
			
		}

		
		if(this.requestID == null || this.requestID.length() > 20 || this.requestID.trim() == "") {
			
			status.setStatusCode(ResponseHelper.MANAGER_REQU_INVALID_REQUEST);
			
			return status;
			
		}

		if(this.deviceID != null) {
			
			if ( this.deviceID.length() > 40 || this.deviceID.trim() == "") {
				
				status.setStatusCode(StatusCode.MANAGER_AUTH_FAILURE);

				return status;
				
			}
		}
		
		
		if(this.deviceName != null) {
			
			if ( this.deviceName.length() > 100 || this.deviceName.trim() == "") {
			
				status.setStatusCode(ResponseHelper.MANAGER_REQU_INVALID_REQUEST);
	
				return status;
			
			}
			
		}

		status.setStatusCode(StatusCode.MANAGER_AUTH_SUCCESS);

		return status;
	}
	
	
}
