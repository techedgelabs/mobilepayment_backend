package com.techedge.mp.frontend.adapter.entities.parking.v2.approveextendparkingtransaction;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class ApproveExtendParkingTransactionResponse extends BaseResponse {
    private ApproveExtendParkingTransactionResponseBody body;

    public ApproveExtendParkingTransactionResponseBody getBody() {
        return body;
    }

    public void setBody(ApproveExtendParkingTransactionResponseBody body) {
        this.body = body;
    }

}
