package com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings;

public class RetrieveApplicationSettingsStaticPageBodyResponse {

    private String faq;
    private String redemptionUrl;
    private String infoTokenUrl;
    private String stationFinderUrl;
    private String firstStartUrl;
    private String captchaUrl;

    public String getFaq() {
        return faq;
    }

    public void setFaq(String faq) {
        this.faq = faq;
    }

    public String getRedemptionUrl() {
        return redemptionUrl;
    }

    public void setRedemptionUrl(String redemptionUrl) {
        this.redemptionUrl = redemptionUrl;
    }

    public String getInfoTokenUrl() {
        return infoTokenUrl;
    }

    public void setInfoTokenUrl(String infoTokenUrl) {
        this.infoTokenUrl = infoTokenUrl;
    }

    public String getStationFinderUrl() {
        return stationFinderUrl;
    }

    public void setStationFinderUrl(String stationFinderUrl) {
        this.stationFinderUrl = stationFinderUrl;
    }

    public String getFirstStartUrl() {
        return firstStartUrl;
    }

    public void setFirstStartUrl(String firstStartUrl) {
        this.firstStartUrl = firstStartUrl;
    }

    public String getCaptchaUrl() {
        return captchaUrl;
    }

    public void setCaptchaUrl(String captchaUrl) {
        this.captchaUrl = captchaUrl;
    }

    
}
