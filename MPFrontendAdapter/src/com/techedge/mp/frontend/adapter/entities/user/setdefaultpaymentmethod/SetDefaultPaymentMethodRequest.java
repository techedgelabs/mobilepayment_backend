package com.techedge.mp.frontend.adapter.entities.user.setdefaultpaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class SetDefaultPaymentMethodRequest extends AbstractRequest implements Validable {

    private Status                             status = new Status();

    private Credential                         credential;
    private SetDefaultPaymentMethodBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public SetDefaultPaymentMethodBodyRequest getBody() {
        return body;
    }

    public void setBody(SetDefaultPaymentMethodBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PAY-SETDEFAULT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        SetDefaultPaymentMethodRequest setDefaultPaymentMethodRequest = this;

        String response = getUserServiceRemote().setDefaultPaymentMethod(setDefaultPaymentMethodRequest.getCredential().getTicketID(),
                setDefaultPaymentMethodRequest.getCredential().getRequestID(), setDefaultPaymentMethodRequest.getBody().getPaymentMethod().getId(),
                setDefaultPaymentMethodRequest.getBody().getPaymentMethod().getType());

        SetDefaultPaymentMethodResponse setDefaultPaymentMethodResponse = new SetDefaultPaymentMethodResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        setDefaultPaymentMethodResponse.setStatus(status);

        return setDefaultPaymentMethodResponse;

    }

}
