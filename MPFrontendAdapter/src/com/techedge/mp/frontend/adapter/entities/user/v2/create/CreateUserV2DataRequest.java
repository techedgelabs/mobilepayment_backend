package com.techedge.mp.frontend.adapter.entities.user.v2.create;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.ContactData;
import com.techedge.mp.frontend.adapter.entities.common.SecurityData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.TermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.common.UserStatus;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateUserV2DataRequest extends CreateUserV2UserData implements Validable {

    protected SecurityData           securityData;
    private List<TermsOfServiceData> termsOfService = new ArrayList<TermsOfServiceData>(0);
    private ContactData              contactData;
    private UserStatus               userStatus;

    public CreateUserV2DataRequest() {}

    public SecurityData getSecurityData() {
        return securityData;
    }

    public void setSecurityData(SecurityData securityData) {
        this.securityData = securityData;
    }

    public ContactData getContactData() {
        return contactData;
    }

    public void setContactData(ContactData contactData) {
        this.contactData = contactData;
    }

    public List<TermsOfServiceData> getTermsOfService() {
        return termsOfService;
    }

    public void setTermsOfService(List<TermsOfServiceData> termsOfService) {
        this.termsOfService = termsOfService;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = super.check();

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.securityData != null) {

            status = this.securityData.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        if (this.contactData != null) {

            status = this.contactData.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        /*else {
            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
            return status;
        }
        */
        if (this.termsOfService != null) {

            for (int i = 0; i < termsOfService.size(); i++) {

                status = this.termsOfService.get(i).check();

                if (!Validator.isValid(status.getStatusCode())) {

                    return status;

                }
            }
        }

        status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

        return status;

    }

}