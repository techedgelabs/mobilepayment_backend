package com.techedge.mp.frontend.adapter.entities.user.v2.updateusersdata;

import java.util.HashMap;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UpdateUsersDataBodyRequest implements Validable {
	
	private HashMap<String, Object> usersData = new HashMap<String, Object>();
		
	public HashMap<String, Object> getUsersData() {
        return usersData;
    }

    public void setUsersData(HashMap<String, Object> usersData) {
        this.usersData = usersData;
    }

    @Override
	public Status check() {

		Status status = new Status();

		if(this.usersData == null || this.usersData.size() < 0) {

			status.setStatusCode(StatusCode.USER_V2_UPDATE_USERS_DATA_INVALID_REQUEST);
	
			return status;

		}
		
		status.setStatusCode(StatusCode.USER_V2_UPDATE_USERS_DATA_SUCCESS);

		return status;
	}
	
	
}
