package com.techedge.mp.frontend.adapter.entities.common;

public class SocialData {

	private String provider;

	public String getProvider() {
		return provider;
	}

	public void setProvider(String provider) {
		this.provider = provider;
	}

}
