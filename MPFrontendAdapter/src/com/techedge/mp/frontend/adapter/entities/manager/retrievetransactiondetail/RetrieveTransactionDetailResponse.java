package com.techedge.mp.frontend.adapter.entities.manager.retrievetransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveTransactionDetailResponse extends BaseResponse {

	private RetrieveTransactionDetailBodyResponse body;

	public RetrieveTransactionDetailBodyResponse getBody() {
		return body;
	}
	public void setBody(RetrieveTransactionDetailBodyResponse body) {
		this.body = body;
	}
}
