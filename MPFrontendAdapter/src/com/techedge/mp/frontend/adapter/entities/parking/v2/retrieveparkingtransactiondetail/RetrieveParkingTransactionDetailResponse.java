package com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingtransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveParkingTransactionDetailResponse extends BaseResponse {

	private RetrieveParkingTransactionDetailBodyResponse body;

	public RetrieveParkingTransactionDetailBodyResponse getBody() {
		return body;
	}
	public void setBody(RetrieveParkingTransactionDetailBodyResponse body) {
		this.body = body;
	}
}
