package com.techedge.mp.frontend.adapter.entities.user.v2.notificationvirtualization;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class NotificationVirtualizationRequest extends AbstractRequest {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("NOTIFICATION-VIRTUALIZATION", credential);

        if (!Validator.isValid(status.getStatusCode())) {
            return status;
        }

        status.setStatusCode(StatusCode.USER_V2_NOTIFICATION_VIRTUALIZATION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        NotificationVirtualizationRequest notificationVirtualizationRequest = this;
        NotificationVirtualizationResponse notificationVirtualizationResponse = new NotificationVirtualizationResponse();

        String ticketId = notificationVirtualizationRequest.getCredential().getTicketID();
        String requestId = notificationVirtualizationRequest.getCredential().getRequestID();

        String response = getUserV2ServiceRemote().notificationVirtualization(ticketId, requestId);

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        notificationVirtualizationResponse.setStatus(status);

        return notificationVirtualizationResponse;
    }
}
