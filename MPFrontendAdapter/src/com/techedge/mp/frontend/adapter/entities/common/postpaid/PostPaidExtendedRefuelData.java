package com.techedge.mp.frontend.adapter.entities.common.postpaid;

public class PostPaidExtendedRefuelData extends PostPaidRefuelJsonData {

    private Integer pumpNumber;

    public PostPaidExtendedRefuelData() {}

    public Integer getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(Integer pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

}
