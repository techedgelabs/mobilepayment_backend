package com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings;


public class RetrieveApplicationSettingsRegistrationDepositCardBodyResponse {

	private String depositCardAnd;
	private String depositCardiOS;
	private String depositCardWP;
	
	public String getDepositCardAnd() {
		return depositCardAnd;
	}
	public void setDepositCardAnd(String depositCardAnd) {
		this.depositCardAnd = depositCardAnd;
	}
	
	public String getDepositCardiOS() {
		return depositCardiOS;
	}
	public void setDepositCardiOS(String depositCardiOS) {
		this.depositCardiOS = depositCardiOS;
	}
	
	public String getDepositCardWP() {
		return depositCardWP;
	}
	public void setDepositCardWP(String depositCardWP) {
		this.depositCardWP = depositCardWP;
	}
}
