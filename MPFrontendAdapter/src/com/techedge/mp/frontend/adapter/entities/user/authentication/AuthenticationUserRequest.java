package com.techedge.mp.frontend.adapter.entities.user.authentication;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.AuthenticationResponse;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransaction;
import com.techedge.mp.core.business.interfaces.parking.RetrievePendingParkingTransactionResult;
import com.techedge.mp.frontend.adapter.entities.common.AddressData;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.ContactData;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.EmailSecurityData;
import com.techedge.mp.frontend.adapter.entities.common.LastLoginData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.UserStatus;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.parking.v2.retrievependingparkingtransaction.RetrievePendingParkingTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class AuthenticationUserRequest extends AbstractRequest implements Validable {

    private Status                        status = new Status();

    private AuthenticationUserRequestBody body;

    public AuthenticationUserRequestBody getBody() {
        return body;
    }

    public void setBody(AuthenticationUserRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_AUTH_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        AuthenticationUserRequest authenticationUserRequest = this;

        AuthenticationResponse authenticationResponse = getUserServiceRemote().authentication(authenticationUserRequest.getBody().getUsername(),
                authenticationUserRequest.getBody().getPassword(), authenticationUserRequest.getBody().getRequestID(), authenticationUserRequest.getBody().getDeviceID(),
                authenticationUserRequest.getBody().getDeviceName(), authenticationUserRequest.getBody().getDeviceToken());

        if (authenticationResponse.getStatusCode().equals(StatusCode.USER_AUTH_SUCCESS)) {

            AuthenticationUserResponseSuccess authenticationUserResponseSuccess = new AuthenticationUserResponseSuccess();

            status.setStatusCode(authenticationResponse.getStatusCode());
            status.setStatusMessage(prop.getProperty(authenticationResponse.getStatusCode()));
            authenticationUserResponseSuccess.setStatus(status);

            AuthenticationUserResponseBody body = new AuthenticationUserResponseBody();

            AuthenticationUserDataResponse userData = new AuthenticationUserDataResponse();
            userData.setFirstName(authenticationResponse.getUser().getPersonalData().getFirstName());
            userData.setLastName(authenticationResponse.getUser().getPersonalData().getLastName());
            userData.setFiscalCode(authenticationResponse.getUser().getPersonalData().getFiscalCode());

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(authenticationResponse.getUser().getPersonalData().getBirthDate());

            CustomDate dateOfBirth = new CustomDate();
            dateOfBirth.setYear(calendar.get(Calendar.YEAR));
            dateOfBirth.setMonth(calendar.get(Calendar.MONTH) + 1);
            dateOfBirth.setDay(calendar.get(Calendar.DAY_OF_MONTH));
            userData.setDateOfBirth(dateOfBirth);

            userData.setBirthMunicipality(authenticationResponse.getUser().getPersonalData().getBirthMunicipality());
            userData.setBirthProvince(authenticationResponse.getUser().getPersonalData().getBirthProvince());
            userData.setLanguage(authenticationResponse.getUser().getPersonalData().getLanguage());
            userData.setSex(authenticationResponse.getUser().getPersonalData().getSex());

            EmailSecurityData securityData = new EmailSecurityData();
            securityData.setEmail(authenticationResponse.getUser().getPersonalData().getSecurityDataEmail());
            userData.setSecurityData(securityData);

//            Boolean contactDataFound = false;

            Date now = new Date();

            ContactData contactData = new ContactData();
            for (MobilePhone mobilePhone : authenticationResponse.getUser().getMobilePhoneList()) {

                mobilePhone.setCreationTimestamp(null);
                mobilePhone.setLastUsedTimestamp(null);
                mobilePhone.setVerificationCode(null);

                if (mobilePhone.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_PENDING) {

                    // Restituisci i metodi in stato pending che non sono scaduti
                    if (mobilePhone.getExpirationTimestamp().getTime() > now.getTime()) {
                        //System.out.println("Restituito");
                        mobilePhone.setExpirationTimestamp(null);
                        contactData.getMobilePhones().add(mobilePhone);
//                        contactDataFound = true;
                    }
                }

                if (mobilePhone.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_ACTIVE) {

                    // Restituisci i numeri attivi (dovrebbe essercene solo uno)
                    mobilePhone.setExpirationTimestamp(null);
                    contactData.getMobilePhones().add(mobilePhone);
//                    contactDataFound = true;
                }

            }
            // Modifica annullata;
            /*
             * if (contactDataFound) {
             */
            userData.setContactData(contactData);
            /*
             * }
             * else {
             * userData.setContactData(null);
             * }
             */

            AddressData addressData = null;
            if (authenticationResponse.getUser().getPersonalData().getAddress() != null) {
                addressData = new AddressData();
                addressData.setCity(authenticationResponse.getUser().getPersonalData().getAddress().getCity());
                addressData.setCountryCodeId(authenticationResponse.getUser().getPersonalData().getAddress().getCountryCodeId());
                addressData.setCountryCodeName(authenticationResponse.getUser().getPersonalData().getAddress().getCountryCodeName());
                addressData.setHouseNumber(authenticationResponse.getUser().getPersonalData().getAddress().getHouseNumber());
                addressData.setRegion(authenticationResponse.getUser().getPersonalData().getAddress().getRegion());
                addressData.setStreet(authenticationResponse.getUser().getPersonalData().getAddress().getStreet());
                addressData.setZipCode(authenticationResponse.getUser().getPersonalData().getAddress().getZipcode());
            }
            userData.setAddressData(addressData);

            AddressData billingAddressData = null;
            if (authenticationResponse.getUser().getPersonalData().getBillingAddress() != null) {
                billingAddressData = new AddressData();
                billingAddressData.setCity(authenticationResponse.getUser().getPersonalData().getBillingAddress().getCity());
                billingAddressData.setCountryCodeId(authenticationResponse.getUser().getPersonalData().getBillingAddress().getCountryCodeId());
                billingAddressData.setCountryCodeName(authenticationResponse.getUser().getPersonalData().getBillingAddress().getCountryCodeName());
                billingAddressData.setHouseNumber(authenticationResponse.getUser().getPersonalData().getBillingAddress().getHouseNumber());
                billingAddressData.setRegion(authenticationResponse.getUser().getPersonalData().getBillingAddress().getRegion());
                billingAddressData.setStreet(authenticationResponse.getUser().getPersonalData().getBillingAddress().getStreet());
                billingAddressData.setZipCode(authenticationResponse.getUser().getPersonalData().getBillingAddress().getZipcode());
            }
            userData.setBillingAddressData(billingAddressData);

            LastLoginData lastLoginData = new LastLoginData();

            String lastLoginDevice = "";
            if (authenticationResponse.getUser().getLastLoginData() != null) {
                lastLoginDevice = authenticationResponse.getUser().getLastLoginData().getDeviceName();
            }

            Timestamp lastLoginTime = null;
            if (authenticationResponse.getUser().getLastLoginData() != null) {
                lastLoginTime = authenticationResponse.getUser().getLastLoginData().getTime();
            }

            lastLoginData.setLastLoginDevice(lastLoginDevice);
            lastLoginData.setLastLoginTime(CustomTimestamp.createCustomTimestamp(lastLoginTime));
            userData.setLastLoginData(lastLoginData);

            PaymentData paymentData = new PaymentData();

            if (authenticationResponse.getUser().getPaymentData() != null) {
                for (PaymentInfo paymentInfo : authenticationResponse.getUser().getPaymentData()) {

                    PaymentMethod paymentMethod = null;
                    if (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED
                            || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                        paymentMethod = new PaymentMethod();

                        paymentMethod.setId(paymentInfo.getId());
                        paymentMethod.setType(paymentInfo.getType());
                        paymentMethod.setBrand(paymentInfo.getBrand());
                        paymentMethod.setExpirationDate(CustomDate.createCustomDate(paymentInfo.getExpirationDate()));
                        paymentMethod.setIdentifier(paymentInfo.getPan());
                        paymentMethod.setStatus(paymentInfo.getStatus());
                        paymentMethod.setDefaultMethod(paymentInfo.getDefaultMethod());

                        if (paymentInfo.getInsertTimestamp() != null) {
                            paymentMethod.setInsertDate(CustomTimestamp.createCustomTimestamp(new Timestamp(paymentInfo.getInsertTimestamp().getTime())));
                        }
                        else {
                            paymentMethod.setInsertDate(null);
                        }

                        paymentData.getPaymentMethodList().add(paymentMethod);
                    }
                }
            }

            paymentData.setResidualCap(AmountConverter.toMobile(authenticationResponse.getUser().getCapEffective()));

            if (authenticationResponse.getUser().getUseVoucher() == null) {
                paymentData.setUseVoucher(false);
            }
            else {
                paymentData.setUseVoucher(authenticationResponse.getUser().getUseVoucher());
            }

            double residualVoucher = 0.0;
            Set<Voucher> voucherList = authenticationResponse.getUser().getVoucherList();

            for (Voucher voucher : voucherList) {
                residualVoucher += voucher.getVoucherBalanceDue().doubleValue();
            }

            paymentData.setResidualVoucher(AmountConverter.toMobile(residualVoucher));

            userData.setPaymentData(paymentData);

            UserStatus userStatus = new UserStatus();
            userStatus.setStatus(authenticationResponse.getUser().getUserStatus());
            userStatus.setRegistrationCompleted(authenticationResponse.getUser().getUserStatusRegistrationCompleted());

            if (authenticationResponse.getUser().getOldUser() != null) {

                System.out.println("oldUser: " + authenticationResponse.getUser().getOldUser());
                if (authenticationResponse.getUser().getOldUser() == true) {
                    userStatus.setNewUserType(false);
                    System.out.println("setNewUserType a false");
                }
                else {
                    userStatus.setNewUserType(true);
                    System.out.println("setNewUserType a true");
                }
            }
            else {
                System.out.println("oldUser: null");
            }

            Boolean termsOfServiceAccepted = Boolean.TRUE;
            for (TermsOfService termsOfService : authenticationResponse.getUser().getPersonalData().getTermsOfServiceData()) {

                if (termsOfService.getValid() == null || !termsOfService.getValid()) {
                    termsOfServiceAccepted = Boolean.FALSE;
                    System.out.println("found termsOfService " + termsOfService.getKeyval() + " no valid");
                    break;
                }
            }
            userStatus.setTermsOfServiceAccepted(termsOfServiceAccepted);

            // Controlla se l'utente ha una sosta pending
            Boolean parkingTransactionPending = false;
            RetrievePendingParkingTransactionResult retrievePendingParkingTransactionResult = getParkingTransactionV2ServiceRemote().retrievePendingParkingTransaction(
                    authenticationResponse.getTicketId(), authenticationUserRequest.getBody().getRequestID());
            if (retrievePendingParkingTransactionResult.getStatusCode().equals("RETRIEVE_PENDING_PARKING_TRANSACTION_200")) {
                ParkingTransaction parkingTransaction = retrievePendingParkingTransactionResult.getParkingTransaction();
                if(parkingTransaction != null) {
                    parkingTransactionPending = true;
                }
            }
            userStatus.setParkingTransactionPending(parkingTransactionPending);
            
            userData.setUserStatus(userStatus);

            body.setTicketID(authenticationResponse.getTicketId());
            body.setLoyaltySessionID(authenticationResponse.getLoyaltySessionId());
            body.setUserData(userData);

            authenticationUserResponseSuccess.setBody(body);

            return authenticationUserResponseSuccess;
        }
        else {
            AuthenticationUserResponseFailure authenticationUserResponseFailure = new AuthenticationUserResponseFailure();

            status.setStatusCode(authenticationResponse.getStatusCode());
            status.setStatusMessage(prop.getProperty(authenticationResponse.getStatusCode()));

            authenticationUserResponseFailure.setStatus(status);

            return authenticationUserResponseFailure;
        }

    }

}
