package com.techedge.mp.frontend.adapter.entities.user.managepayment;

import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class ManagePaymentRequest implements Validable {
	
	
	private Credential credential;
	private ManagePaymentBodyRequest body;
	
	
	public Credential getCredential() {
		return credential;
	}
	public void setCredential(Credential credential) {
		this.credential = credential;
	}
	
	public ManagePaymentBodyRequest getBody() {
		return body;
	}
	public void setBody(ManagePaymentBodyRequest body) {
		this.body = body;
	}
	@Override
	public Status check() {

		Status status = new Status();
		
		status = Validator.checkCredential("PAY-MANAGE", credential);
		
		if(!Validator.isValid(status.getStatusCode())) {
			
			return status;
			
		}
		
		if(this.body != null) {
			
			status = this.body.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
			
			return status;
			
		}
		
		return status;
	}
	
}
