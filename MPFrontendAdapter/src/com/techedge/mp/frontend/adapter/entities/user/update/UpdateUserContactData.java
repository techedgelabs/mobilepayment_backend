package com.techedge.mp.frontend.adapter.entities.user.update;

import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.frontend.adapter.entities.common.ContactData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UpdateUserContactData extends ContactData implements Validable {

    public UpdateUserContactData() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.getMobilePhones() == null) {

            status.setStatusCode(StatusCode.USER_UPD_CONTACT_DATA_WRONG);

            return status;

        }

        for (MobilePhone mobilePhone : this.getMobilePhones()) {
            if (mobilePhone.getNumber().length() > 20) {
                status.setStatusCode(StatusCode.USER_UPD_CONTACT_DATA_WRONG);

                return status;
            }
        }

        status.setStatusCode(StatusCode.USER_UPD_SUCCESS);

        return status;

    }

}
