package com.techedge.mp.frontend.adapter.entities.voucher.retrievevouchertransactiondetail;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.voucher.RetrieveVoucherTransactionDataResponse;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.FrontendParameter;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetail;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetailSection;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetailTransaction;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveVoucherTransactionDetailRequest extends AbstractRequest implements Validable {

    private Status                                      status = new Status();

    private Credential                                  credential;
    private RetrieveVoucherTransactionDetailBodyRequest body;

    public RetrieveVoucherTransactionDetailRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveVoucherTransactionDetailBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveVoucherTransactionDetailBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-VOUCHER-TRANSACTION-DETAIL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.RETRIEVE_VOUCHER_TRANSACTION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        RetrieveVoucherTransactionDetailRequest retrieveVoucherTransactionDetailRequest = this;
        RetrieveVoucherTransactionDetailResponse retrieveVoucherTransactionDetailResponse = new RetrieveVoucherTransactionDetailResponse();

        RetrieveVoucherTransactionDataResponse retrieveVoucherTransactionDataResponse = getVoucherTransactionServiceRemote().retrieveVoucherTransactionDetail(
                retrieveVoucherTransactionDetailRequest.getCredential().getRequestID(), retrieveVoucherTransactionDetailRequest.getCredential().getTicketID(),
                retrieveVoucherTransactionDetailRequest.getBody().getVoucherTransactionId());

        status.setStatusCode(retrieveVoucherTransactionDataResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveVoucherTransactionDataResponse.getStatusCode()));
        retrieveVoucherTransactionDetailResponse.setStatus(status);

        RetrieveVoucherTransactionDetailBodyResponse retrieveVoucherTransactionDetailBodyResponse = new RetrieveVoucherTransactionDetailBodyResponse();
        retrieveVoucherTransactionDetailBodyResponse.setSourceStatus(retrieveVoucherTransactionDataResponse.getSourceStatus());
        retrieveVoucherTransactionDetailBodyResponse.setSourceType(retrieveVoucherTransactionDataResponse.getSourceType());

        if (!status.getStatusCode().contains("200")) {
            retrieveVoucherTransactionDetailResponse.setStatus(status);
            return retrieveVoucherTransactionDetailResponse;
        }

        String iconCardUrl = "";
        String iconVoucherUrl = "";

        try {
            iconCardUrl = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_RECEIPT_ICON_CARD_URL);
            iconVoucherUrl = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_RECEIPT_ICON_VOUCHER_URL);
        }
        catch (ParameterNotFoundException ex) {
            getLoggerServiceRemote().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "PARAMETER NOT FOUND: " + ex.getMessage());
        }

        retrieveVoucherTransactionDetailBodyResponse.setSourceStatus(retrieveVoucherTransactionDataResponse.getSourceStatus());
        retrieveVoucherTransactionDetailBodyResponse.setSourceType(retrieveVoucherTransactionDataResponse.getSourceType());

        ReceiptTransactionDetailTransaction transaction = new ReceiptTransactionDetailTransaction();

        Double amount = 0.0;
        if (retrieveVoucherTransactionDataResponse.getAmount() != null) {
            amount = retrieveVoucherTransactionDataResponse.getAmount();
        }
        transaction.setAmount(AmountConverter.toMobile(amount));

        transaction.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(retrieveVoucherTransactionDataResponse.getCreationTimestamp().getTime())));
        transaction.setTransactionID(retrieveVoucherTransactionDataResponse.getVocherTransactionID());
        transaction.setStatusTitle(retrieveVoucherTransactionDataResponse.getStatusTitle());
        transaction.setStatusDescription(retrieveVoucherTransactionDataResponse.getStatusDescription());

        // Trascodifica dello stato
        if (retrieveVoucherTransactionDataResponse.getStatus() == null) {
            transaction.setStatus(null);
        }
        else {
            String statusOld = retrieveVoucherTransactionDataResponse.getStatus();
            statusOld = "2" + statusOld.substring(1);
            transaction.setStatus(statusOld);
        }

        transaction.setSubStatus(retrieveVoucherTransactionDataResponse.getSubStatus());

        retrieveVoucherTransactionDetailBodyResponse.setTransaction(transaction);

        ReceiptTransactionDetail receipt = new ReceiptTransactionDetail();

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");

        ReceiptTransactionDetailSection sectionTransaction = receipt.createSection("Pagamento Carta", "RECEIPT", iconCardUrl, false);

        int sectionPositionTransaction = 1;

        String creationTimestamp = new SimpleDateFormat("dd/MM/yyyy").format(retrieveVoucherTransactionDataResponse.getCreationTimestamp().getTime());
        receipt.addDataToSection(sectionTransaction, "DATE", "DATA", null, creationTimestamp, sectionPositionTransaction++, null);

        String transactionAmount = currencyFormat.format(retrieveVoucherTransactionDataResponse.getAmount());
        receipt.addDataToSection(sectionTransaction, "AMOUNT", "IMPORTO ADDEBITATO", "�", transactionAmount, sectionPositionTransaction++, null);

        receipt.addDataToSection(sectionTransaction, "PAN", "CARTA DI PAGAMENTO", null, retrieveVoucherTransactionDataResponse.getPanCode(), sectionPositionTransaction++, null);
        receipt.addDataToSection(sectionTransaction, "N_OP", "N OPERAZIONE", null, retrieveVoucherTransactionDataResponse.getBankTansactionID(), sectionPositionTransaction++, null);
        receipt.addDataToSection(sectionTransaction, "C_AUTH", "COD AUTORIZZAZIONE", null, retrieveVoucherTransactionDataResponse.getAuthorizationCode(),
                sectionPositionTransaction++, null);

        Voucher voucher = retrieveVoucherTransactionDataResponse.getVoucher();
        receipt.addDataToSection(sectionTransaction, "VOUCHER_NAME", "TIPO VOUCHER", null, voucher.getPromoDescription(), sectionPositionTransaction++, null);

        String voucherValue = "0.0";
        if (voucher.getConsumedValue() != null) {
            voucherValue = currencyFormat.format(voucher.getValue());
        }
        receipt.addDataToSection(sectionTransaction, "VOUCHER_AMOUNT", "IMPORTO", "�", voucherValue, sectionPositionTransaction++, null);

        retrieveVoucherTransactionDetailBodyResponse.setReceipt(receipt);

        retrieveVoucherTransactionDetailResponse.setBody(retrieveVoucherTransactionDetailBodyResponse);
        retrieveVoucherTransactionDetailResponse.setStatus(status);

        return retrieveVoucherTransactionDetailResponse;
    }

}
