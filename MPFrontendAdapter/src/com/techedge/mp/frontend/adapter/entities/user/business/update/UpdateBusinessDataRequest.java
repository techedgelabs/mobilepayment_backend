package com.techedge.mp.frontend.adapter.entities.user.business.update;

import com.techedge.mp.core.business.interfaces.user.PersonalDataBusiness;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.BusinessData;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.business.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class UpdateBusinessDataRequest extends AbstractRequest implements Validable {

    private Status                       status = new Status();

    private Credential                   credential;
    private UpdateBusinessDataBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateBusinessDataBodyRequest getBody() {
        return body;
    }

    public void setBody(UpdateBusinessDataBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-BUSINESS-DATA", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {
            status.setStatusCode(StatusCode.USER_BUSINESS_REQU_INVALID_REQUEST);
            return status;
        }

        status.setStatusCode(StatusCode.USER_BUSINESS_UPDATE_BUSINESS_DATA_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        UpdateBusinessDataRequest updateBusinessDataRequest = this;
        UpdateBusinessDataResponse updateBusinessDataResponse = new UpdateBusinessDataResponse();

        String ticketId = updateBusinessDataRequest.getCredential().getTicketID();
        String requestId = updateBusinessDataRequest.getCredential().getRequestID();
        BusinessData businessData = updateBusinessDataRequest.getBody().getBusinessData();
        PersonalDataBusiness personalDataBusiness = new PersonalDataBusiness();
        personalDataBusiness.setAddress(businessData.getBillingAddress().getAddress());
        personalDataBusiness.setBusinessName(businessData.getBusinessName());
        personalDataBusiness.setCity(businessData.getBillingAddress().getCity());
        personalDataBusiness.setLicensePlate(businessData.getLicensePlate());
        personalDataBusiness.setPecEmail(businessData.getPecEmail());
        personalDataBusiness.setProvince(businessData.getBillingAddress().getProvince());
        personalDataBusiness.setSdiCode(businessData.getSdiCode());
        personalDataBusiness.setStreetNumber(businessData.getBillingAddress().getStreetNumber());
        personalDataBusiness.setVatNumber(businessData.getVatNumber());
        personalDataBusiness.setFiscalCode(businessData.getFiscalCode());
        personalDataBusiness.setZipCode(businessData.getBillingAddress().getZipCode());
        
        String response = getUserV2ServiceRemote().updateBusinessData(ticketId, requestId, personalDataBusiness);

        status.setStatusCode(response);

        status.setStatusMessage(prop.getProperty(response));

        updateBusinessDataResponse.setStatus(status);

        return updateBusinessDataResponse;
    }

}
