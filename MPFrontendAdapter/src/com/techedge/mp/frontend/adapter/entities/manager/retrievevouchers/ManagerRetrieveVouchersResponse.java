package com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class ManagerRetrieveVouchersResponse extends BaseResponse {

    private ManagerRetrieveVouchersBodyResponse body;

    public synchronized ManagerRetrieveVouchersBodyResponse getBody() {
        return body;
    }

    public synchronized void setBody(ManagerRetrieveVouchersBodyResponse body) {
        this.body = body;
    }

}
