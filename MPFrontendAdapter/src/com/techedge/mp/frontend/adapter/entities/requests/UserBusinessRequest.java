package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.user.business.addplatenumber.AddPlateNumberRequest;
import com.techedge.mp.frontend.adapter.entities.user.business.authentication.AuthenticationUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.business.clonepaymentmethod.ClonePaymentMethodRequest;
import com.techedge.mp.frontend.adapter.entities.user.business.create.CreateUserBusinessRequest;
import com.techedge.mp.frontend.adapter.entities.user.business.update.UpdateBusinessDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.business.refreshuserdata.RefreshUserDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.business.retrievetermsofservice.RetrieveTermsOfServiceRequest;
import com.techedge.mp.frontend.adapter.entities.user.business.recoverusername.RecoverUsernameRequest;
import com.techedge.mp.frontend.adapter.entities.user.business.removeplatenumber.RemovePlateNumberRequest;
import com.techedge.mp.frontend.adapter.entities.user.business.rescuepassword.RescuePasswordRequest;
import com.techedge.mp.frontend.adapter.entities.user.business.setdefaultplatenumber.SetDefaultPlateNumberRequest;

public class UserBusinessRequest extends RootRequest {

    protected AuthenticationUserRequest     authentication;
    protected CreateUserBusinessRequest     createUser;
    protected UpdateBusinessDataRequest     updateBusinessData;
    protected RefreshUserDataRequest        refreshUserData;
    protected RetrieveTermsOfServiceRequest retrieveTermsOfService;
    protected RecoverUsernameRequest        recoverUsername;
    protected RescuePasswordRequest         rescuePasswordMethod;
    protected ClonePaymentMethodRequest     clonePaymentMethod;
    protected AddPlateNumberRequest         addPlateNumber;
    protected RemovePlateNumberRequest      removePlateNumber;
    protected SetDefaultPlateNumberRequest  setDefaultPlateNumber;
    

    public AuthenticationUserRequest getAuthentication() {
        return authentication;
    }

    public void setAuthentication(AuthenticationUserRequest authentication) {
        this.authentication = authentication;
    }

    public CreateUserBusinessRequest getCreateUser() {
        return createUser;
    }

    public void setCreateUser(CreateUserBusinessRequest createUser) {
        this.createUser = createUser;
    }

    public UpdateBusinessDataRequest getUpdateBusinessData() {
        return updateBusinessData;
    }

    public void setUpdateBusinessData(UpdateBusinessDataRequest updateBusinessData) {
        this.updateBusinessData = updateBusinessData;
    }

    public RetrieveTermsOfServiceRequest getRetrieveTermsOfService() {
        return retrieveTermsOfService;
    }
    
    public void setRetrieveTermsOfService(RetrieveTermsOfServiceRequest retrieveTermsOfService) {
        this.retrieveTermsOfService = retrieveTermsOfService;
    }
    
    public RefreshUserDataRequest getRefreshUserData() {
        return refreshUserData;
    }
    
    public void setRefreshUserData(RefreshUserDataRequest refreshUserData) {
        this.refreshUserData = refreshUserData;
    }
    
    public RecoverUsernameRequest getRecoverUsername() {
        return recoverUsername;
    }
    
    public void setRecoverUsername(RecoverUsernameRequest recoverUsername) {
        this.recoverUsername = recoverUsername;
    }
    
    public RescuePasswordRequest getRescuePasswordMethod() {
        return rescuePasswordMethod;
    }
    
    public void setRescuePasswordMethod(RescuePasswordRequest rescuePasswordMethod) {
        this.rescuePasswordMethod = rescuePasswordMethod;
    }
    
    public ClonePaymentMethodRequest getClonePaymentMethod() {
        return clonePaymentMethod;
    }
    
    public void setClonePaymentMethod(ClonePaymentMethodRequest clonePaymentMethod) {
        this.clonePaymentMethod = clonePaymentMethod;
    }

    public AddPlateNumberRequest getAddPlateNumber() {
        return addPlateNumber;
    }

    public void setAddPlateNumber(AddPlateNumberRequest addPlateNumber) {
        this.addPlateNumber = addPlateNumber;
    }

    public RemovePlateNumberRequest getRemovePlateNumber() {
        return removePlateNumber;
    }

    public void setRemovePlateNumber(RemovePlateNumberRequest removePlateNumber) {
        this.removePlateNumber = removePlateNumber;
    }

    public SetDefaultPlateNumberRequest getSetDefaultPlateNumber() {
        return setDefaultPlateNumber;
    }

    public void setSetDefaultPlateNumber(SetDefaultPlateNumberRequest setDefaultPlateNumber) {
        this.setDefaultPlateNumber = setDefaultPlateNumber;
    }
    
}
