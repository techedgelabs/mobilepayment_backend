package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction.ApproveTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.confirm.ConfirmTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.notifydisplaytransaction.NotifyDisplayTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction.RejectPoPTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievependingtransaction.RetrievePendingTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail.RetrieveRefuelTransactionDetailRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievesourcedetail.RetrieveSourceDetailRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestation.RetrieveStationRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveStationListRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail.RetrieveTransactionDetailRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory.RetrieveTransactionHistoryRequest;

public class PoPRequest extends RootRequest {

    protected ApproveTransactionRequest              approvePoPTransaction;
    protected RejectPoPTransactionRequest            rejectPoPTransaction;
    protected NotifyDisplayTransactionRequest        notifyDisplayTransaction;
    protected RetrieveTransactionDetailRequest       retrieveTransactionDetail;
    protected RetrieveSourceDetailRequest            retrieveSourceDetail;
    protected RetrievePendingTransactionRequest      retrievePendingTransaction;
    protected RetrieveTransactionHistoryRequest      retrieveTransactionHistory;
    protected ConfirmTransactionRequest              confirmTransaction;
    protected RetrieveStationRequest                 retrieveStation;
    protected RetrieveStationListRequest             retrieveStationList;
    protected RetrieveRefuelTransactionDetailRequest retrieveRefuelTransactionDetail;

    public ApproveTransactionRequest getApprovePoPTransaction() {
        return approvePoPTransaction;
    }

    public void setApproveTransaction(ApproveTransactionRequest approvePoPTransaction) {
        this.approvePoPTransaction = approvePoPTransaction;
    }

    public RejectPoPTransactionRequest getRejectPoPTransaction() {
        return rejectPoPTransaction;
    }

    public void setRejectTransaction(RejectPoPTransactionRequest rejectPoPTransaction) {
        this.rejectPoPTransaction = rejectPoPTransaction;
    }

    public NotifyDisplayTransactionRequest getNotifyDisplayTransaction() {
        return notifyDisplayTransaction;
    }

    public void setNotifyDisplayTransaction(NotifyDisplayTransactionRequest notifyDisplayTransaction) {
        this.notifyDisplayTransaction = notifyDisplayTransaction;
    }

    public RetrieveTransactionDetailRequest getRetrieveTransactionDetail() {
        return retrieveTransactionDetail;
    }

    public void setRetrieveTransactionDetail(RetrieveTransactionDetailRequest retrieveTransactionDetail) {
        this.retrieveTransactionDetail = retrieveTransactionDetail;
    }

    public RetrieveSourceDetailRequest getRetrieveSourceDetail() {
        return retrieveSourceDetail;
    }

    public void setRetrieveSourceDetail(RetrieveSourceDetailRequest retrieveSourceDetail) {
        this.retrieveSourceDetail = retrieveSourceDetail;
    }

    public RetrievePendingTransactionRequest getRetrievePendingTransaction() {
        return retrievePendingTransaction;
    }

    public void setRetrievePendingTransaction(RetrievePendingTransactionRequest retrievePendingTransaction) {
        this.retrievePendingTransaction = retrievePendingTransaction;
    }

    public RetrieveTransactionHistoryRequest getRetrieveTransactionHistory() {
        return retrieveTransactionHistory;
    }

    public void setRetrieveTransactionHistory(RetrieveTransactionHistoryRequest retrieveTransactionHistory) {
        this.retrieveTransactionHistory = retrieveTransactionHistory;
    }

    public ConfirmTransactionRequest getConfirmTransaction() {
        return confirmTransaction;
    }

    public void setConfirmTransaction(ConfirmTransactionRequest confirmTransaction) {
        this.confirmTransaction = confirmTransaction;
    }

    public RetrieveStationRequest getRetrieveStation() {
        return retrieveStation;
    }

    public void setRetrieveStation(RetrieveStationRequest retrieveStation) {
        this.retrieveStation = retrieveStation;
    }

    public RetrieveStationListRequest getRetrieveStationList() {
        return retrieveStationList;
    }

    public void setRetrieveStationList(RetrieveStationListRequest retrieveStationList) {
        this.retrieveStationList = retrieveStationList;
    }

    public RetrieveRefuelTransactionDetailRequest getRetrieveRefuelTransactionDetail() {
        return retrieveRefuelTransactionDetail;
    }

    public void setRetrieveRefuelTransactionDetail(RetrieveRefuelTransactionDetailRequest retrieveRefuelTransactionDetail) {
        this.retrieveRefuelTransactionDetail = retrieveRefuelTransactionDetail;
    }

}