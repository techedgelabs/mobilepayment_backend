package com.techedge.mp.frontend.adapter.entities.user.v2.getmissions;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetMissionListResponse extends BaseResponse {

    private GetMissionListBodyResponse body;

    public GetMissionListBodyResponse getBody() {
        return body;
    }

    public void setBody(GetMissionListBodyResponse body) {
        this.body = body;
    }

}
