package com.techedge.mp.frontend.adapter.entities.eventnotification.retrievenotification;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveNotificationBodyRequest implements Validable {

    private Long notificationID;

    public Long getNotificationID() {
        return notificationID;
    }

    public void setNotificationID(Long notificationID) {
        this.notificationID = notificationID;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (notificationID == null ) {
            status.setStatusCode(StatusCode.RETRIEVE_NOTIFICATION_INVALID_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.RETRIEVE_NOTIFICATION_SUCCESS);

        return status;
    }

}
