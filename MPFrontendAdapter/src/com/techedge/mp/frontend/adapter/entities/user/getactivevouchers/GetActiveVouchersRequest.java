package com.techedge.mp.frontend.adapter.entities.user.getactivevouchers;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Locale;

import com.techedge.mp.core.business.interfaces.GetActiveVouchersData;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.VoucherData;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetActiveVouchersRequest extends AbstractRequest implements Validable {

    private Status                       status = new Status();

    private Credential                   credential;
    private GetActiveVouchersBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GetActiveVouchersBodyRequest getBody() {
        return body;
    }

    public void setBody(GetActiveVouchersBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-ACTIVE-VOUCHERS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;

    }

    @Override
    public BaseResponse execute() {
        GetActiveVouchersRequest getActiveVouchersRequest = this;

        GetActiveVouchersData getActiveVouchersData = getUserServiceRemote().getActiveVouchers(getActiveVouchersRequest.getCredential().getTicketID(),
                getActiveVouchersRequest.getCredential().getRequestID(), getActiveVouchersRequest.getBody().getRefresh(),
                getActiveVouchersRequest.getCredential().getLoyaltySessionID());

        GetActiveVouchersResponse getActiveVouchersResponse = new GetActiveVouchersResponse();

        GetActiveVouchersBodyResponse getActiveVouchersBodyResponse = new GetActiveVouchersBodyResponse();

        Boolean noDetail = getActiveVouchersRequest.getBody().getNoDetail();

        if (noDetail == null || noDetail == false) {

            for (Voucher voucher : getActiveVouchersData.getVouchers()) {

                VoucherData voucherData = new VoucherData();

                voucherData.setCode(voucher.getCode());
                voucherData.setStatus(voucher.getStatus());
                voucherData.setType(voucher.getType());
                voucherData.setValue(AmountConverter.toMobile(voucher.getValue()));
                voucherData.setInitialValue(AmountConverter.toMobile(voucher.getInitialValue()));
                voucherData.setConsumedValue(AmountConverter.toMobile(voucher.getConsumedValue()));
                voucherData.setVoucherBalanceDue(AmountConverter.toMobile(voucher.getVoucherBalanceDue()));
                voucherData.setExpirationDate(CustomTimestamp.createCustomTimestamp(new Timestamp(voucher.getExpirationDate().getTime())));
                voucherData.setPromoCode(voucher.getPromoCode());
                voucherData.setPromoDescription(voucher.getPromoDescription());
                voucherData.setPromoDoc(voucher.getPromoDoc());
                voucherData.setIcon(voucher.getIcon());
                
                // Distinzione tra voucher Credito e Prodotto
                voucherData.setClassType("CREDIT");
                //if (!voucher.getPromoCode().equals("acq") && !voucher.getPromoCode().equals("scal")) {
                //    voucherData.setClassType("PROMO");
                //}

                DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
                currencyFormat.applyPattern("##0.00");

                /*
                Double initialValue = 0.0;
                if (voucher.getInitialValue() != null) {
                    initialValue = voucher.getInitialValue();
                }
                */
                Double value = 0.0;
                if (voucher.getValue() != null) {
                    value = voucher.getValue();
                }

                Double voucherBalanceDue = 0.0;
                if (voucher.getVoucherBalanceDue() != null) {
                    voucherBalanceDue = voucher.getVoucherBalanceDue();
                }

                //voucherData.addDetail("Taglio", currencyFormat.format(initialValue) + " �");
                voucherData.addDetail("Taglio", currencyFormat.format(value) + " �");
                voucherData.addDetail("Credito Residuo", currencyFormat.format(voucherBalanceDue) + " �");
                String expirationDateString = DateFormat.getDateInstance(DateFormat.LONG, Locale.ITALIAN).format(voucher.getExpirationDate());
                expirationDateString = capitalize(expirationDateString);
                voucherData.addDetail("Scadenza", expirationDateString);

                getActiveVouchersBodyResponse.getVouchers().add(voucherData);
            }
            
            for (Voucher voucher : getActiveVouchersData.getParkingVouchers()) {

                VoucherData voucherData = new VoucherData();

                voucherData.setCode(voucher.getCode());
                voucherData.setStatus(voucher.getStatus());
                voucherData.setType(voucher.getType());
                voucherData.setValue(AmountConverter.toMobile(voucher.getValue()));
                voucherData.setInitialValue(AmountConverter.toMobile(voucher.getInitialValue()));
                voucherData.setConsumedValue(AmountConverter.toMobile(voucher.getConsumedValue()));
                voucherData.setVoucherBalanceDue(AmountConverter.toMobile(voucher.getVoucherBalanceDue()));
                voucherData.setExpirationDate(CustomTimestamp.createCustomTimestamp(new Timestamp(voucher.getExpirationDate().getTime())));
                voucherData.setPromoCode(voucher.getPromoCode());
                voucherData.setPromoDescription(voucher.getPromoDescription());
                voucherData.setPromoDoc(voucher.getPromoDoc());
                voucherData.setIcon(voucher.getIcon());
                
                // Distinzione tra voucher Credito e Prodotto
                voucherData.setClassType("CREDIT");
                //if (!voucher.getPromoCode().equals("acq") && !voucher.getPromoCode().equals("scal")) {
                //    voucherData.setClassType("PROMO");
                //}

                DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
                currencyFormat.applyPattern("##0.00");

                /*
                Double initialValue = 0.0;
                if (voucher.getInitialValue() != null) {
                    initialValue = voucher.getInitialValue();
                }
                */
                Double value = 0.0;
                if (voucher.getValue() != null) {
                    value = voucher.getValue();
                }

                Double voucherBalanceDue = 0.0;
                if (voucher.getVoucherBalanceDue() != null) {
                    voucherBalanceDue = voucher.getVoucherBalanceDue();
                }

                //voucherData.addDetail("Taglio", currencyFormat.format(initialValue) + " �");
                voucherData.addDetail("Taglio", currencyFormat.format(value) + " �");
                voucherData.addDetail("Credito Residuo", currencyFormat.format(voucherBalanceDue) + " �");
                String expirationDateString = DateFormat.getDateInstance(DateFormat.LONG, Locale.ITALIAN).format(voucher.getExpirationDate());
                expirationDateString = capitalize(expirationDateString);
                voucherData.addDetail("Scadenza", expirationDateString);

                getActiveVouchersBodyResponse.getParkingVouchers().add(voucherData);
            }
        }

        getActiveVouchersBodyResponse.setTotal(AmountConverter.toMobile(getActiveVouchersData.getTotal()));

        getActiveVouchersResponse.setBody(getActiveVouchersBodyResponse);

        status.setStatusCode(getActiveVouchersData.getStatusCode());
        status.setStatusMessage(prop.getProperty(getActiveVouchersData.getStatusCode()));

        getActiveVouchersResponse.setStatus(status);

        return getActiveVouchersResponse;
    }

    private static String capitalize(String inputString) {

        if (inputString == null || inputString.equals("")) {
            return "";
        }

        String[] arr = inputString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

}
