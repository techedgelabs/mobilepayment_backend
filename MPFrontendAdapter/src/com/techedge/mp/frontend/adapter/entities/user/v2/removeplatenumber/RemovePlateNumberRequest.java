package com.techedge.mp.frontend.adapter.entities.user.v2.removeplatenumber;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RemovePlateNumberRequest extends AbstractRequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private RemovePlateNumberBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RemovePlateNumberBodyRequest getBody() {
        return body;
    }

    public void setBody(RemovePlateNumberBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REMOVE-PLATE-NUMBER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        
        RemovePlateNumberRequest removePlateNumberRequest = this;
        RemovePlateNumberResponse removePlateNumberResponse = new RemovePlateNumberResponse();

        String response = getUserV2ServiceRemote().removePlateNumber(removePlateNumberRequest.getCredential().getRequestID(),
                removePlateNumberRequest.getCredential().getTicketID(), removePlateNumberRequest.getBody().getId());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        removePlateNumberResponse.setStatus(status);

        return removePlateNumberResponse;
    }

}
