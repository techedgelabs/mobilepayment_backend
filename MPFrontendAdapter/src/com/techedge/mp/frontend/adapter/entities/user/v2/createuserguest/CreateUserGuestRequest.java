package com.techedge.mp.frontend.adapter.entities.user.v2.createuserguest;

import com.techedge.mp.core.business.interfaces.CreateUserGuestResult;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class CreateUserGuestRequest extends AbstractRequest implements Validable {

    private Status                  status = new Status();

    private Credential              credential;
    private CreateUserGuestRequestBody body;

    public CreateUserGuestRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateUserGuestRequestBody getBody() {
        return body;
    }

    public void setBody(CreateUserGuestRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATEGUEST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        CreateUserGuestRequest createUserGuestRequest = this;

        CreateUserGuestResult createUserGuestResult = getUserV2ServiceRemote().createUserGuest(createUserGuestRequest.getCredential().getTicketID(), createUserGuestRequest.getCredential().getRequestID(), createUserGuestRequest.getCredential().getDeviceID(), createUserGuestRequest.getBody().getCaptchaCode());
        status.setStatusCode(createUserGuestResult.getStatusCode());
        status.setStatusMessage(prop.getProperty(createUserGuestResult.getStatusCode()));
        
        CreateUserGuestResponseBody createUserGuestResponseBody = new CreateUserGuestResponseBody();
        createUserGuestResponseBody.setUsername(createUserGuestResult.getUsername());
        createUserGuestResponseBody.setEncodedPassword(createUserGuestResult.getEncodedPassword());

        CreateUserGuestResponse createUserGuestResponse = new CreateUserGuestResponse();
        createUserGuestResponse.setStatus(status);
        createUserGuestResponse.setBody(createUserGuestResponseBody);
        
        return createUserGuestResponse;
    }

}
