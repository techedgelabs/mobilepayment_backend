package com.techedge.mp.frontend.adapter.entities.user.v2.externalauthentication;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.user.v2.authentication.AuthenticationUserBodyRequest;

public class ExternalAuthenticationUserBodyRequest extends
		AuthenticationUserBodyRequest implements Validable {

	private String accessToken;
	private String service;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	
	public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    @Override
	public Status check() {

		Status status = new Status();

		if (this.accessToken == null || this.accessToken.length() == 0) {

			status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

			return status;

		}

		if (this.service == null || this.service.length() == 0 || !this.service.equals("MULTICARD")) {

			status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

			return status;

		}

		status.setStatusCode(StatusCode.USER_V2_AUTH_SUCCESS);

		return status;
	}

}
