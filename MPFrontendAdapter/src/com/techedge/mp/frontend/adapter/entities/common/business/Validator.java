package com.techedge.mp.frontend.adapter.entities.common.business;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;

public class Validator {

    private final static String  regex_password_maiuscola = ".*[A-Z].*";
    private final static String  regex_password_minuscola = ".*[a-z].*";
    private final static String  regex_password_numerica  = ".*[0-9].*";

    public static Status checkPassword(String operation, String password) {

        Status status = new Status();

        if (password == null || password.length() > 40) {

            if (operation.equals("AUTH"))

                status.setStatusCode(StatusCode.USER_BUSINESS_AUTH_PASSWORD_WRONG);

            return status;

        }
        else {

            if (password.length() < 8) {

                if (operation.equals("AUTH"))

                    status.setStatusCode(StatusCode.USER_BUSINESS_AUTH_PASSWORD_SHORT);

                return status;

            }

            Pattern patternPasswordMaiuscola = Pattern.compile(regex_password_maiuscola);
            Matcher matchePasswordMaiuscola = patternPasswordMaiuscola.matcher(password);
            Pattern patternPasswordMinuscola = Pattern.compile(regex_password_minuscola);
            Matcher matchePasswordMinuscola = patternPasswordMinuscola.matcher(password);
            Pattern patternPasswordNumerica = Pattern.compile(regex_password_numerica);
            Matcher matchePasswordNumerica = patternPasswordNumerica.matcher(password);

            if (!matchePasswordMaiuscola.matches()) {

                if (operation.equals("AUTH"))

                    status.setStatusCode(StatusCode.USER_BUSINESS_AUTH_UPPER_LESS);

                return status;

            }

            if (!matchePasswordMinuscola.matches()) {

                if (operation.equals("AUTH"))

                    status.setStatusCode(StatusCode.USER_BUSINESS_AUTH_LOWER_LESS);

                return status;

            }

            if (!matchePasswordNumerica.matches()) {

                if (operation.equals("AUTH"))

                    status.setStatusCode(StatusCode.USER_BUSINESS_AUTH_NUMBER_LESS);

                return status;

            }

        }

        if (operation.equals("AUTH"))

            status.setStatusCode(StatusCode.USER_BUSINESS_AUTH_SUCCESS);

        return status;

    }


    public static Status checkCredential(String operation, Credential credential) {

        Status status = new Status();

        if (credential != null) {

            if (credential.getTicketID() == null || credential.getTicketID().length() != 32) {

                status.setStatusCode(StatusCode.USER_BUSINESS_REQU_INVALID_TICKET);

                return status;

            }

            if (credential.getRequestID() == null || credential.getRequestID().length() > 50) {

                status.setStatusCode(StatusCode.USER_BUSINESS_REQU_INVALID_REQUEST);

                return status;

            }
            
            if (credential.getRequestID().startsWith("WPH")) {

                status.setStatusCode(StatusCode.USER_BUSINESS_REQU_UNSUPPORTED_DEVICE);

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_BUSINESS_REQU_INVALID_REQUEST);

            return status;

        }
        
        if (operation.equals("CREATE-USER-BUSINESS")) {

            status.setStatusCode(StatusCode.USER_BUSINESS_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("UPDATE-BUSINESS-DATA")) {

            status.setStatusCode(StatusCode.USER_BUSINESS_UPDATE_BUSINESS_DATA_SUCCESS);

            return status;

        }

        if (operation.equals("REFRESH-USER-DATA")) {

            status.setStatusCode(StatusCode.USER_BUSINESS_AUTH_SUCCESS);

            return status;

        }
        
        if (operation.equals("POSTPAID-APPROVE")) {

            status.setStatusCode(StatusCode.POSTPAID_BUSINESS_APPROVE_TRANSACTION_SUCCESS);

            return status;

        }

        if (operation.equals("RETRIVE-REFUEL-TRANSACTION-DETAIL")) {

            status.setStatusCode(StatusCode.RETRIVE_REFUEL_TRANSACTION_DETAIL_SUCCESS);

            return status;

        }
        
        if (operation.equals("RECOVER")) {

            status.setStatusCode(StatusCode.USER_BUSINESS_RECOVER_USERNAME_SUCCESS);

            return status;

        }
        
        if (operation.equals("RESCUE")) {

            status.setStatusCode(StatusCode.USER_BUSINESS_RESCUE_PASSWORD_SUCCESS);

            return status;

        }

        if (operation.equals("CLONE-PAYMENT")) {

            status.setStatusCode(StatusCode.USER_BUSINESS_CLONE_PAYMENT_METHOD_SUCCESS);

            return status;

        }
        
        status.setStatusCode(StatusCode.USER_BUSINESS_REQU_INVALID_REQUEST);
        return status;

    }



    public static boolean isValid(String statusCode) {

        if (statusCode == null) {
            return false;
        }
        else {
            if (statusCode.contains("200")) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    

}
