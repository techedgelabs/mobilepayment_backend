package com.techedge.mp.frontend.adapter.entities.user.removepaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RemovePaymentMethodRequest extends AbstractRequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;
    private RemovePaymentMethodBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RemovePaymentMethodBodyRequest getBody() {
        return body;
    }

    public void setBody(RemovePaymentMethodBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PAY-REMOVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        RemovePaymentMethodRequest removePaymentMethodRequest = this;

        String response = getUserServiceRemote().removePaymentMethod(removePaymentMethodRequest.getCredential().getTicketID(),
                removePaymentMethodRequest.getCredential().getRequestID(), removePaymentMethodRequest.getBody().getPaymentMethod().getId(),
                removePaymentMethodRequest.getBody().getPaymentMethod().getType());

        RemovePaymentMethodResponse removePaymentMethodResponse = new RemovePaymentMethodResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        removePaymentMethodResponse.setStatus(status);

        return removePaymentMethodResponse;
    }

}
