package com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings;


public class RetrieveApplicationSettingsApplicationBodyResponse {

	private String minVersionAnd;
	private String minVersioniOS;
	private String minVersionWP;
	private String appIdAnd;
    private String appIdiOS;
    private String appIdWP;
	
	public String getMinVersionAnd() {
		return minVersionAnd;
	}
	public void setMinVersionAnd(String minVersionAnd) {
		this.minVersionAnd = minVersionAnd;
	}
	
	public String getMinVersioniOS() {
		return minVersioniOS;
	}
	public void setMinVersioniOS(String minVersioniOS) {
		this.minVersioniOS = minVersioniOS;
	}
	
	public String getMinVersionWP() {
		return minVersionWP;
	}
	public void setMinVersionWP(String minVersionWP) {
		this.minVersionWP = minVersionWP;
	}
	
    public String getAppIdAnd() {
        return appIdAnd;
    }
    public void setAppIdAnd(String appIdAnd) {
        this.appIdAnd = appIdAnd;
    }
    
    public String getAppIdiOS() {
        return appIdiOS;
    }
    public void setAppIdiOS(String appIdiOS) {
        this.appIdiOS = appIdiOS;
    }
    
    public String getAppIdWP() {
        return appIdWP;
    }
    public void setAppIdWP(String appIdWP) {
        this.appIdWP = appIdWP;
    }
}
