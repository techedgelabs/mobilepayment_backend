package com.techedge.mp.frontend.adapter.entities.survey.getsurvey;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetSurveyResponse extends BaseResponse {

    private GetSurveyBodyResponse body;

    public GetSurveyBodyResponse getBody() {
        return body;
    }

    public void setBody(GetSurveyBodyResponse body) {
        this.body = body;
    }
    
    
}
