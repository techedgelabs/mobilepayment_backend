package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;

public class ReceiptTransactionDetailSection {
    private String title;
    private String sectionType;
    private String icon;
    private Boolean separator;
    private ArrayList<ReceiptDynamicData> receiptData = new ArrayList<ReceiptDynamicData>();
    
    public String getTitle() {
        return title;
    }
    
    public void setTitle(String title) {
        this.title = title;
    }
    
    public String getSectionType() {
        return sectionType;
    }
    
    public void setSectionType(String sectionType) {
        this.sectionType = sectionType;
    }
    
    public String getIcon() {
        return icon;
    }
    
    public void setIcon(String icon) {
        this.icon = icon;
    }
    
    public Boolean getSeparator() {
        return separator;
    }

    public void setSeparator(Boolean separator) {
        this.separator = separator;
    }

    public ArrayList<ReceiptDynamicData> getReceiptData() {
        return receiptData;
    }
    
    public void addDataToSection(String key, String label, String unit, String value, int position, String link) {

        ReceiptDynamicData itemData = new ReceiptDynamicData();
        
        itemData.setKey(key);
        itemData.setLabel(label);
        itemData.setUnit(unit);
        itemData.setValue(value);
        itemData.setPosition(position);
        itemData.setLink(link);

        receiptData.add(itemData);
    }
    
}
