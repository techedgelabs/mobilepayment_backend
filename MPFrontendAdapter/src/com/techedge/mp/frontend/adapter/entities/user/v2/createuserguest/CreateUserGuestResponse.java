package com.techedge.mp.frontend.adapter.entities.user.v2.createuserguest;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class CreateUserGuestResponse extends BaseResponse {

    private CreateUserGuestResponseBody body;

    public CreateUserGuestResponseBody getBody() {
        return body;
    }

    public void setBody(CreateUserGuestResponseBody body) {
        this.body = body;
    }

}
