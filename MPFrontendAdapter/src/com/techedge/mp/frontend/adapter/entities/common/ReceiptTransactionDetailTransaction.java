package com.techedge.mp.frontend.adapter.entities.common;

public class ReceiptTransactionDetailTransaction {
    private CustomTimestamp date;
    private Integer         amount;
    private String          statusTitle;
    private String          statusDescription;
    private String          transactionID;
    private String          status;
    private String          subStatus;

    public CustomTimestamp getDate() {
        return date;
    }

    public void setDate(CustomTimestamp date) {
        this.date = date;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(String subStatus) {
        this.subStatus = subStatus;
    }
}
