package com.techedge.mp.frontend.adapter.entities.user.create;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.UserData;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateUserUserData extends UserData implements Validable {

	private CreateUserLoyaltyCard loyaltyCard;
    
	public CreateUserUserData() {

	}
		
	public CreateUserLoyaltyCard getLoyaltyCard() {
        return loyaltyCard;
    }

    public void setLoyaltyCard(CreateUserLoyaltyCard loyaltyCard) {
        this.loyaltyCard = loyaltyCard;
    }


    @Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.getFirstName() == null || this.getFirstName().length() > 40 || this.getFirstName().trim() == "") {
			
			status.setStatusCode(StatusCode.USER_CREATE_MASTER_DATA_WRONG);
			
			return status;
			
		}
		
		if(this.getLastName() == null || this.getLastName().length() > 40 || this.getLastName().trim() == "") {
			
			status.setStatusCode(StatusCode.USER_CREATE_MASTER_DATA_WRONG);
			
			return status;
			
		}
		
		if(this.getFiscalCode() == null || this.getFiscalCode().length() != 16 || this.getFiscalCode().trim() == "") {
			
			status.setStatusCode(StatusCode.USER_CREATE_MASTER_DATA_WRONG);
			
			return status;
			
		}
		
		if(this.getDateOfBirth() != null) {
			
			status = this.getDateOfBirth().check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				// nel caso si pu� aggiungere l'errore USER_CREATE_DATE_WRONG per identificare la data errata
				status.setStatusCode(StatusCode.USER_CREATE_MASTER_DATA_WRONG);
				
				return status;
			}
			
		}
		
		if(this.getBirthMunicipality() == null || this.getBirthMunicipality().length() > 40 || this.getBirthMunicipality().trim() == "") {
			
			status.setStatusCode(StatusCode.USER_CREATE_MASTER_DATA_WRONG);

			return status;
			
		}
		
		if(this.getBirthProvince() == null || this.getBirthProvince().length() > 40 || this.getBirthProvince().trim() == "") {
			
			status.setStatusCode(StatusCode.USER_CREATE_MASTER_DATA_WRONG);

			return status;
			
		}
		/*
		if(this.getLanguage() == null || this.getLanguage().trim().length() != 2) {
			
			status.setStatusCode(StatusCode.USER_CREATE_MASTER_DATA_WRONG);

			return status;
			
		}
		*/
		if(this.getLanguage() == null || this.getLanguage().trim().length() != 2) {
            
		    this.setLanguage("it");
            
        }
        
		
		if(this.getSex() == null || (!this.getSex().equals("M") && !this.getSex().equals("F"))) {
			
			status.setStatusCode(StatusCode.USER_CREATE_MASTER_DATA_WRONG);

			return status;
			
		}
		
		// Residence
		if(this.getAddressData() != null) {
			
			if(this.getAddressData().getCity() == null || this.getAddressData().getCity().length() > 40 || this.getAddressData().getCity().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG);
	
				return status;
			}
			
			if(this.getAddressData().getCountryCodeId() == null || this.getAddressData().getCountryCodeId().length() > 40 || this.getAddressData().getCountryCodeId().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG);
	
				return status;
			}
			
			if(this.getAddressData().getCountryCodeName() == null || this.getAddressData().getCountryCodeName().length() > 40 || this.getAddressData().getCountryCodeName().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG);
	
				return status;
			}
			
			if(this.getAddressData().getHouseNumber() == null || this.getAddressData().getHouseNumber().length() > 40 || this.getAddressData().getHouseNumber().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG);
	
				return status;
			}
			
			if(this.getAddressData().getRegion() == null || this.getAddressData().getRegion().length() > 40 || this.getAddressData().getRegion().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG);
	
				return status;
			}
			
			if(this.getAddressData().getStreet() == null || this.getAddressData().getStreet().length() > 50 || this.getAddressData().getStreet().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG);
	
				return status;
			}
			
			if(this.getAddressData().getZipCode() == null || this.getAddressData().getZipCode().trim().length() != 5) {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_RESIDENCE_WRONG);
	
				return status;
			}
		
		}
		
		// Billing
		if(this.getBillingAddressData() != null) {
		
			if(this.getBillingAddressData().getCity() == null || this.getBillingAddressData().getCity().length() > 40 || this.getBillingAddressData().getCity().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_BILLING_WRONG);
	
				return status;
			}
			
			if(this.getBillingAddressData().getCountryCodeId() == null || this.getBillingAddressData().getCountryCodeId().length() > 40 || this.getBillingAddressData().getCountryCodeId().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_BILLING_WRONG);
	
				return status;
			}
			
			if(this.getBillingAddressData().getCountryCodeName() == null || this.getBillingAddressData().getCountryCodeName().length() > 40 || this.getBillingAddressData().getCountryCodeName().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_BILLING_WRONG);
	
				return status;
			}
			
			if(this.getBillingAddressData().getHouseNumber() == null || this.getBillingAddressData().getHouseNumber().length() > 40 || this.getBillingAddressData().getHouseNumber().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_BILLING_WRONG);
	
				return status;
			}
			
			if(this.getBillingAddressData().getRegion() == null || this.getBillingAddressData().getRegion().length() > 40 || this.getBillingAddressData().getRegion().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_BILLING_WRONG);
	
				return status;
			}
			
			if(this.getBillingAddressData().getStreet() == null || this.getBillingAddressData().getStreet().length() > 50 || this.getBillingAddressData().getStreet().trim() == "") {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_BILLING_WRONG);
	
				return status;
			}
			
			if(this.getBillingAddressData().getZipCode() == null || this.getBillingAddressData().getZipCode().trim().length() != 5) {
				
				status.setStatusCode(StatusCode.USER_CREATE_ADDRESS_BILLING_WRONG);
	
				return status;
			}
			
		}
		
		if ( this.loyaltyCard != null ) {
		    
		    if(this.loyaltyCard.getId() == null || this.loyaltyCard.getId().length() > 20) {
                
                status.setStatusCode(StatusCode.USER_CREATE_MASTER_DATA_WRONG);
    
                return status;
            }
		}
		

		status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);
		
		return status;
	}

}
