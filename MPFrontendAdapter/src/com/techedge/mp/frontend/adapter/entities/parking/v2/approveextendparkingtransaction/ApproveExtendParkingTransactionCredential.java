package com.techedge.mp.frontend.adapter.entities.parking.v2.approveextendparkingtransaction;

import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ApproveExtendParkingTransactionCredential extends Credential implements Validable {

    private String pin;

    public String getPin() {
        return pin;
    }

    public void setPin(String pin) {
        this.pin = pin;
    }

    @Override
    public Status check() {

        Status status = new Status();

        Credential credential = new Credential();
        credential.setTicketID(this.getTicketID());
        credential.setRequestID(this.getRequestID());

        if (credential != null) {

            if (credential.getTicketID() == null || credential.getTicketID().length() != 32) {

                status.setStatusCode(StatusCode.APPROVE_PARKING_TRANSACTION_INVALID_REQUEST);

                return status;

            }

            if (credential.getRequestID() == null || credential.getRequestID().length() > 50) {

                status.setStatusCode(StatusCode.APPROVE_PARKING_TRANSACTION_INVALID_REQUEST);

                return status;

            }

            if (this.pin == null || this.pin.isEmpty()) {

                status.setStatusCode(StatusCode.APPROVE_PARKING_TRANSACTION_INVALID_REQUEST);

                return status;

            }

        }

        status.setStatusCode(StatusCode.APPROVE_PARKING_TRANSACTION_SUCCESS);

        return status;

    }

}