package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.survey.getsurvey.GetSurveyRequest;
import com.techedge.mp.frontend.adapter.entities.survey.submitsurvey.SubmitSurveyRequest;

public class SurveyRequest extends RootRequest {

    protected GetSurveyRequest    getSurvey;
    protected SubmitSurveyRequest submitSurvey;

    public GetSurveyRequest getGetSurveyRequest() {
        return getSurvey;
    }

    public void setGetSurveyRequest(GetSurveyRequest getSurvey) {
        this.getSurvey = getSurvey;
    }

    public SubmitSurveyRequest getSubmitSurveyRequest() {
        return submitSurvey;
    }

    public void setSubmitSurveyRequest(SubmitSurveyRequest submitSurvey) {
        this.submitSurvey = submitSurvey;
    }

}
