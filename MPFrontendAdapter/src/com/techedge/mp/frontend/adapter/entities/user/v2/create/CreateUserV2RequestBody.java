package com.techedge.mp.frontend.adapter.entities.user.v2.create;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateUserV2RequestBody implements Validable {

    private CreateUserV2DataRequest userData;
    private String                  accessToken;
    private String                  socialProvider;

    public CreateUserV2RequestBody() {}

    public CreateUserV2DataRequest getUserData() {
        return userData;
    }

    public void setUserData(CreateUserV2DataRequest userData) {
        this.userData = userData;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getSocialProvider() {
        return socialProvider;
    }

    public void setSocialProvider(String socialProvider) {
        this.socialProvider = socialProvider;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.userData != null) {

            status = this.userData.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

        return status;
    }

}
