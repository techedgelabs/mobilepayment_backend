package com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ApproveTransactionRequest extends AbstractRequest implements Validable {

    private Status                                  status = new Status();

    private ApproveTransactionTransactionCredential credential;
    private ApproveTransactionBodyRequest           body;

    public ApproveTransactionTransactionCredential getCredential() {
        return credential;
    }

    public void setCredential(ApproveTransactionTransactionCredential credential) {
        this.credential = credential;
    }

    public ApproveTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(ApproveTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CONFIRM-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        ApproveTransactionRequest approvePoPTransactionRequest = this;
        ApproveTransactionResponse approvePoPTransactionResponse = new ApproveTransactionResponse();

        Boolean useVoucher = approvePoPTransactionRequest.getBody().getUseVoucher();

        PostPaidApproveShopTransactionResponse postPaidApproveShopTransactionResponse = getPostPaidTransactionServiceRemote().approveShopTransaction(
                approvePoPTransactionRequest.getCredential().getRequestID(), approvePoPTransactionRequest.getCredential().getTicketID(),
                approvePoPTransactionRequest.getBody().getTransactionID(), approvePoPTransactionRequest.getBody().getPaymentMethod().getId(),
                approvePoPTransactionRequest.getBody().getPaymentMethod().getType(), approvePoPTransactionRequest.getCredential().getPin(), useVoucher);

        ApproveTransactionBodyResponse approvePoPTransactionBodyResponse = new ApproveTransactionBodyResponse();

        approvePoPTransactionBodyResponse.setCheckPinAttemptsLeft(postPaidApproveShopTransactionResponse.getPinCheckMaxAttempts());

        status.setStatusCode(postPaidApproveShopTransactionResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(postPaidApproveShopTransactionResponse.getStatusCode()));

        approvePoPTransactionResponse.setStatus(status);
        approvePoPTransactionResponse.setBody(approvePoPTransactionBodyResponse);

        return approvePoPTransactionResponse;
    }

}