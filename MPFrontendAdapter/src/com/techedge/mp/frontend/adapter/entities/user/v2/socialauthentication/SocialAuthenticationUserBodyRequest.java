package com.techedge.mp.frontend.adapter.entities.user.v2.socialauthentication;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.user.v2.authentication.AuthenticationUserBodyRequest;

public class SocialAuthenticationUserBodyRequest extends
		AuthenticationUserBodyRequest implements Validable {

	private String accessToken;
	private String social;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getSocial() {
		return social;
	}

	public void setSocial(String social) {
		this.social = social;
	}

	@Override
	public Status check() {

		Status status = new Status();

		if (this.accessToken == null || this.accessToken.length() == 0) {

			status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

			return status;

		}

		if (this.social == null || this.social.length() == 0) {

			status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

			return status;

		}

		status.setStatusCode(StatusCode.USER_V2_AUTH_SUCCESS);

		return status;
	}

}
