package com.techedge.mp.frontend.adapter.entities.user.v2.getpartnerlist;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetPartnerListResponse extends BaseResponse {
    
    private GetPartnerListBodyResponse body;

    public GetPartnerListBodyResponse getBody() {
        return body;
    }

    public void setBody(GetPartnerListBodyResponse body) {
        this.body = body;
    }

}
