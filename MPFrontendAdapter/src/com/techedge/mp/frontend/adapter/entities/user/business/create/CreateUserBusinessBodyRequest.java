package com.techedge.mp.frontend.adapter.entities.user.business.create;

import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.Validator;
import com.techedge.mp.frontend.adapter.entities.common.business.UserBusinessData;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateUserBusinessBodyRequest implements Validable {

    private UserBusinessData userData;

    public UserBusinessData getUserData() {
        return userData;
    }
    
    public void setUserData(UserBusinessData userData) {
        this.userData = userData;
    }
    
    public CreateUserBusinessBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.userData != null) {

            status = this.userData.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_BUSINESS_AUTH_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_BUSINESS_CREATE_SUCCESS);

        return status;
    }

}
