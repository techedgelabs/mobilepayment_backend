package com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings;

import com.techedge.mp.frontend.adapter.entities.common.Credential;

public class RetrieveApplicationSettingsCredential extends Credential {

    private String appVersion;

    public RetrieveApplicationSettingsCredential() {}

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

}
