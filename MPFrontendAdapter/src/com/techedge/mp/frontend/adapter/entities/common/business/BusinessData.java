package com.techedge.mp.frontend.adapter.entities.common.business;

import com.techedge.mp.frontend.adapter.entities.common.BillingAddressData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class BusinessData implements Validable {

    private BillingAddressData billingAddress;
    private String             businessName;
    private String             fiscalCode;
    private String             vatNumber;
    private String             pecEmail;
    private String             sdiCode;
    private String             licensePlate;

    public BillingAddressData getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddressData billingAddress) {
        this.billingAddress = billingAddress;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getVatNumber() {
        return vatNumber;
    }

    public void setVatNumber(String vatNumber) {
        this.vatNumber = vatNumber;
    }

    public String getPecEmail() {
        return pecEmail;
    }

    public void setPecEmail(String pecEmail) {
        this.pecEmail = pecEmail;
    }

    public String getSdiCode() {
        return sdiCode;
    }

    public void setSdiCode(String sdicode) {
        this.sdiCode = sdicode;
    }
    
    public String getLicensePlate() {
        return licensePlate;
    }
    
    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (billingAddress == null) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BILLING_ADDRESS_DATA_REQUIRED);
            return status;
        }

        status = this.billingAddress.check();

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (businessName == null || businessName.isEmpty() || businessName.length() > 80) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BUSINESS_NAME_INVALID);
            return status;
        }
        
        //if (fiscalCode != null && fiscalCode.length() != 16) {
        //    status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_FISCAL_CODE_INVALID);
        //    return status;
        //}

        if (vatNumber == null || vatNumber.isEmpty() || vatNumber.length() > 11) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_VAT_NUMBER_INVALID);
            return status;
        }

        if ((pecEmail == null || pecEmail.isEmpty()) && (sdiCode == null || sdiCode.isEmpty())) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_PEC_EMAIL_OR_SDI_CODE_REQUIRED);
            return status;
        }

        if (pecEmail != null && pecEmail.length() > 256) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_PEC_EMAIL_INVALID);
            return status;
        }

        if (sdiCode != null) {
            
            String trimSdiCode = sdiCode.trim();
            if (trimSdiCode.length() != 7) {
                status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_SDI_CODE_INVALID);
                return status;
            }
        }

        if (licensePlate != null && !licensePlate.isEmpty() && (licensePlate.length() > 20 || !licensePlate.matches("^[a-zA-Z0-9]+$"))) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_LICENSE_PLATE_INVALID);
            return status;
        }
        
        status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_SUCCESS);
        return status;
    }
}
