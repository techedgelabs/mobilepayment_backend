package com.techedge.mp.frontend.adapter.entities.common;

public class StationLocationData extends LocationData {

    private Double distance;

    public StationLocationData() {
        super();
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }
    
}
