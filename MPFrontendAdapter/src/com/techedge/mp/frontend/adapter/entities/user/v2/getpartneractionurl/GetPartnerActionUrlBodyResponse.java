package com.techedge.mp.frontend.adapter.entities.user.v2.getpartneractionurl;


public class GetPartnerActionUrlBodyResponse {

    private String url;
    private String key;
    private int actionForwardId;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getActionForwardId() {
        return actionForwardId;
    }

    public void setActionForwardId(int actionForwardId) {
        this.actionForwardId = actionForwardId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

   

}
