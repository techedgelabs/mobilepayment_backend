package com.techedge.mp.frontend.adapter.entities.user.v2.getreceipt;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class GetReceiptRequestBody implements Validable {

    private String type;
    private String transactionId;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public GetReceiptRequestBody() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (type == null || type.trim().equals("")) {

            status.setStatusCode(StatusCode.GET_RECEIPT_INVALID_PARAMETERS);

            return status;
        }
        
        if(! (type.equalsIgnoreCase("PRE-PAY")|| type.equalsIgnoreCase("POST-PAY"))){
            
            status.setStatusCode(StatusCode.GET_RECEIPT_INVALID_PARAMETERS);

            return status;
        }

        if (transactionId == null || transactionId.trim().equals("")) {

            status.setStatusCode(StatusCode.GET_RECEIPT_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.GET_RECEIPT_SUCCESS);

        return status;
    }

}
