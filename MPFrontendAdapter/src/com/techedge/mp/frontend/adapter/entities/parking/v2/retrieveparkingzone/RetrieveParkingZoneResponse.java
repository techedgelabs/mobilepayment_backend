package com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingzone;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveParkingZoneResponse extends BaseResponse {
    private RetrieveParkingZonesResponseBody body;

    public RetrieveParkingZonesResponseBody getBody() {
        return body;
    }

    public void setBody(RetrieveParkingZonesResponseBody body) {
        this.body = body;
    }

    
}
