package com.techedge.mp.frontend.adapter.entities.postpaid.business.retrieverefueltransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveRefuelTransactionDetailBodyRequest implements Validable {

	private String transactionId;

	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.transactionId == null || this.transactionId.length() != 32 || this.transactionId.trim() == "") {
			
			status.setStatusCode(StatusCode.RETRIVE_REFUEL_TRANSACTION_DETAIL_INVALID_PARAMETERS);
			
			return status;			
		}
		
		status.setStatusCode(StatusCode.RETRIVE_REFUEL_TRANSACTION_DETAIL_SUCCESS);
		
		return status;
	}
}