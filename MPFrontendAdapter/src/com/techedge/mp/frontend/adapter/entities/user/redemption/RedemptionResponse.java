package com.techedge.mp.frontend.adapter.entities.user.redemption;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RedemptionResponse extends BaseResponse {

    private RedemptionBodyResponse body;

    public RedemptionBodyResponse getBody() {
        return body;
    }

    public void setBody(RedemptionBodyResponse body) {
        this.body = body;
    }

    
}
