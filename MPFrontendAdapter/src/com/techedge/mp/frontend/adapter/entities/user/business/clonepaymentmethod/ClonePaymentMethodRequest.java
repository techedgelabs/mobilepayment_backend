package com.techedge.mp.frontend.adapter.entities.user.business.clonepaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ClonePaymentMethodRequest extends AbstractRequest implements Validable {

    private ClonePaymentMethodCredential  credential;
    private ClonePaymentMethodBodyRequest body;

    public ClonePaymentMethodCredential getCredential() {
        return credential;
    }

    public void setCredential(ClonePaymentMethodCredential credential) {
        this.credential = credential;
    }

    public ClonePaymentMethodBodyRequest getBody() {
        return body;
    }

    public void setBody(ClonePaymentMethodBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CLONE-PAYMENT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {

        ClonePaymentMethodRequest clonePaymentMethodRequest = this;
        ClonePaymentMethodResponse clonePaymentMethodResponse = new ClonePaymentMethodResponse();

        String response = getUserV2ServiceRemote().cloneUserPaymentMethod(clonePaymentMethodRequest.getCredential().getTicketID(),
                clonePaymentMethodRequest.getCredential().getRequestID(), clonePaymentMethodRequest.getCredential().getPin());

        Status status = new Status();
        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        clonePaymentMethodResponse.setStatus(status);

        return clonePaymentMethodResponse;
    }

}
