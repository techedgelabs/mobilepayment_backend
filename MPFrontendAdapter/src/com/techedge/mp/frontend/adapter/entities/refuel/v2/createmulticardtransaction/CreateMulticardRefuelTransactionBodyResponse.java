package com.techedge.mp.frontend.adapter.entities.refuel.v2.createmulticardtransaction;


public class CreateMulticardRefuelTransactionBodyResponse {

	private Integer maxAmount;
    private Integer thresholdAmount;
	
    public Integer getMaxAmount() {
        return maxAmount;
    }
    public void setMaxAmount(Integer maxAmount) {
        this.maxAmount = maxAmount;
    }
    
    public Integer getThresholdAmount() {
        return thresholdAmount;
    }
    public void setThresholdAmount(Integer thresholdAmount) {
        this.thresholdAmount = thresholdAmount;
    }
}
