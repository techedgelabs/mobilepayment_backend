package com.techedge.mp.frontend.adapter.entities.user.v2.getreceipt;

import com.techedge.mp.core.business.interfaces.GetReceiptDataResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetReceiptRequest extends AbstractRequest implements Validable {

    private Status                status = new Status();

    private Credential            credential;
    private GetReceiptRequestBody body;

    public GetReceiptRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GetReceiptRequestBody getBody() {
        return body;
    }

    public void setBody(GetReceiptRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {
        
        Status status = new Status();

        status = Validator.checkCredential("GET_RECEIPT", credential);

        if (!Validator.isValid(status.getStatusCode())) {
            
            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {
            
            status.setStatusCode(StatusCode.GET_RECEIPT_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.GET_RECEIPT_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        
        System.out.println("Execute receipt request");

        GetReceiptRequest getReceiptRequest = this;
        
        GetReceiptDataResponse getReceiptDataResponse = getUserV2ServiceRemote().getReceiptResponse(getReceiptRequest.getCredential().getRequestID(), getReceiptRequest.getCredential().getTicketID(), getReceiptRequest.getBody().getType(),getReceiptRequest.getBody().getTransactionId());
        getReceiptDataResponse.setStatusCode(getReceiptDataResponse.getStatusCode());
        
        status.setStatusCode(getReceiptDataResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(getReceiptDataResponse.getStatusCode()));

        GetReceiptResponseBody getReceiptResponseBody = new GetReceiptResponseBody();
        if(getReceiptDataResponse.getGetReceiptDataDetail()!= null){
            
            getReceiptResponseBody.setDimension(getReceiptDataResponse.getGetReceiptDataDetail().getDimension());
            getReceiptResponseBody.setDocumentBase64(getReceiptDataResponse.getGetReceiptDataDetail().getDocumentBase64());
            getReceiptResponseBody.setFilename(getReceiptDataResponse.getGetReceiptDataDetail().getFilename());
        }
        
        GetReceiptResponse getReceiptResponse = new GetReceiptResponse();
        
        getReceiptResponse.setStatus(status);
        if(getReceiptResponseBody!=null){
            getReceiptResponse.setBody(getReceiptResponseBody);
        }

        return getReceiptResponse;
    }

}
