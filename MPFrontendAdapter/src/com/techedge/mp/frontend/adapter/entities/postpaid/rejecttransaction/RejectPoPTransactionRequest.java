package com.techedge.mp.frontend.adapter.entities.postpaid.rejecttransaction;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRejectShopTransactionResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RejectPoPTransactionRequest extends AbstractRequest implements Validable {

    private Status                          status = new Status();

    private Credential                      credential;
    private RejectPoPTransactionBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RejectPoPTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(RejectPoPTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CONFIRM-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;
        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;
            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
            return status;
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        RejectPoPTransactionRequest rejectPoPTransactionRequest = this;
        RejectPoPTransactionResponse rejectPoPTransactionResponse = new RejectPoPTransactionResponse();

        PostPaidRejectShopTransactionResponse postPaidRejectShopTransactionResponse = getPostPaidTransactionServiceRemote().rejectShopTransaction(
                rejectPoPTransactionRequest.getCredential().getRequestID(), rejectPoPTransactionRequest.getCredential().getTicketID(),
                rejectPoPTransactionRequest.getBody().getTransactionId());

        status.setStatusCode(postPaidRejectShopTransactionResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(postPaidRejectShopTransactionResponse.getStatusCode()));
        rejectPoPTransactionResponse.setStatus(status);

        return rejectPoPTransactionResponse;

    }
}