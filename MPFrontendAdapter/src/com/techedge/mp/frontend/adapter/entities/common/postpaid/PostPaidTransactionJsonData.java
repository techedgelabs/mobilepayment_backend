package com.techedge.mp.frontend.adapter.entities.common.postpaid;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class PostPaidTransactionJsonData {

	
	private String transactionID;
	private String status;
	private String subStatus;
	private Boolean useVoucher;
//	private String type; // (shop/fuel)
	private Integer amount;
	private Integer voucherAmount;
	private Integer loyaltyBalance;
//	private Station station;
	private Date creationTimestamp;
	private Set<PostPaidCartJsonData> cart = new HashSet<PostPaidCartJsonData>(0);  
	private Set<PostPaidExtendedRefuelData> refuel = new HashSet<PostPaidExtendedRefuelData>(0);  
	//

	public PostPaidTransactionJsonData() {}

  public String getTransactionID() {
    return transactionID;
  }

  public void setTransactionID(String transactionID) {
    this.transactionID = transactionID;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public String getSubStatus() {
    return subStatus;
  }

  public void setSubStatus(String subStatus) {
    this.subStatus = subStatus;
  }

  public Boolean getUseVoucher() {
    return useVoucher;
  }

  public void setUseVoucher(Boolean useVoucher) {
    this.useVoucher = useVoucher;
  }

  public Integer getAmount() {
    return amount;
  }

  public void setAmount(Integer amount) {
    this.amount = amount;
  }

  public Integer getVoucherAmount() {
    return voucherAmount;
  }

  public void setVoucherAmount(Integer voucherAmount) {
    this.voucherAmount = voucherAmount;
  }

  public Integer getLoyaltyBalance() {
    return loyaltyBalance;
  }

  public void setLoyaltyBalance(Integer loyaltyBalance) {
    this.loyaltyBalance = loyaltyBalance;
  }

  public Date getCreationTimestamp() {
    return creationTimestamp;
  }

  public void setCreationTimestamp(Date creationTimestamp) {
    this.creationTimestamp = creationTimestamp;
  }

  public Set<PostPaidCartJsonData> getCart() {
    return cart;
  }

  public void setCart(Set<PostPaidCartJsonData> cart) {
    this.cart = cart;
  }

  public Set<PostPaidExtendedRefuelData> getRefuel() {
    return refuel;
  }

  public void setRefuel(Set<PostPaidExtendedRefuelData> refuel) {
    this.refuel = refuel;
  }

  


}
