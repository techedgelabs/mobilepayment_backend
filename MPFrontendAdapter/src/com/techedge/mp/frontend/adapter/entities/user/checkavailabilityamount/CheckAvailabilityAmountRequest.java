package com.techedge.mp.frontend.adapter.entities.user.checkavailabilityamount;

import com.techedge.mp.core.business.interfaces.CheckAvailabilityAmountData;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class CheckAvailabilityAmountRequest extends AbstractRequest implements Validable {

    private Status                             status = new Status();

    private Credential                         credential;
    private CheckAvailabilityAmountBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CheckAvailabilityAmountBodyRequest getBody() {
        return body;
    }

    public void setBody(CheckAvailabilityAmountBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CHECK-AVAILABLE-AMOUNT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;

    }

    @Override
    public BaseResponse execute() {
        CheckAvailabilityAmountRequest checkAvailabilityAmountRequest = this;
        CheckAvailabilityAmountResponse checkAvailabilityAmountResponse = new CheckAvailabilityAmountResponse();
        CheckAvailabilityAmountBodyResponse checkAvailabilityAmountBodyResponse = new CheckAvailabilityAmountBodyResponse();

        Double amount = AmountConverter.toInternal(checkAvailabilityAmountRequest.getBody().getAmount());

        CheckAvailabilityAmountData checkAvailabilityAmountData = getUserServiceRemote().userCheckAvailabilityAmount(checkAvailabilityAmountRequest.getCredential().getTicketID(),
                checkAvailabilityAmountRequest.getCredential().getRequestID(), amount, checkAvailabilityAmountRequest.getCredential().getLoyaltySessionID());

        status.setStatusCode(checkAvailabilityAmountData.getStatusCode());
        status.setStatusMessage(prop.getProperty(checkAvailabilityAmountData.getStatusCode()));
        checkAvailabilityAmountResponse.setStatus(status);

        Integer maxAmount = AmountConverter.toMobile(checkAvailabilityAmountData.getMaxAmount());
        Integer thresholdAmount = AmountConverter.toMobile(checkAvailabilityAmountData.getThresholdAmount());
        Boolean allowResize = checkAvailabilityAmountData.getAllowResize();

        checkAvailabilityAmountBodyResponse.setMaxAmount(maxAmount);
        checkAvailabilityAmountBodyResponse.setThresholdAmount(thresholdAmount);
        checkAvailabilityAmountBodyResponse.setAllowResize(allowResize);

        checkAvailabilityAmountResponse.setBody(checkAvailabilityAmountBodyResponse);

        return checkAvailabilityAmountResponse;
    }
}
