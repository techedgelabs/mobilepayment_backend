package com.techedge.mp.frontend.adapter.entities.user.retrievepaymentdata;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class RetrievePaymentDataBodyRequest implements Validable {

	
	private PaymentMethod paymentMethod;

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}


	@Override
	public Status check() {

		Status status = new Status();
		
		if(this.paymentMethod == null) {
			
			status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
			
			return status;
		}
		
		if(this.paymentMethod.getId() == null) {
			
			status.setStatusCode(StatusCode.USER_RETRIEVE_PAYMENT_DATA_ID_WRONG);
			
			return status;
		}
		
		if(this.paymentMethod.getType() == null) {
			
			status.setStatusCode(StatusCode.USER_RETRIEVE_PAYMENT_DATA_TYPE_WRONG);
			
			return status;
		}
		
		status.setStatusCode(StatusCode.USER_RETRIEVE_PAYMENT_DATA_SUCCESS);
		
		return status;
		
	}
	
}
