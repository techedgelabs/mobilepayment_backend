package com.techedge.mp.frontend.adapter.entities.user.v2.addplatenumber;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class AddPlateNumberRequest extends AbstractRequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private AddPlateNumberBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AddPlateNumberBodyRequest getBody() {
        return body;
    }

    public void setBody(AddPlateNumberBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADD-PLATE-NUMBER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        
        AddPlateNumberRequest addPlateNumberRequest = this;
        AddPlateNumberResponse addPlateNumberResponse = new AddPlateNumberResponse();

        String response = getUserV2ServiceRemote().addPlateNumber(addPlateNumberRequest.getCredential().getRequestID(), addPlateNumberRequest.getCredential().getTicketID(),
                addPlateNumberRequest.getBody().getPlateNumber(), addPlateNumberRequest.getBody().getDescription());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        addPlateNumberResponse.setStatus(status);

        return addPlateNumberResponse;
    }

}
