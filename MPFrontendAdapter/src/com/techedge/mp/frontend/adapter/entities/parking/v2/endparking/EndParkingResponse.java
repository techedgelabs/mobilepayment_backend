package com.techedge.mp.frontend.adapter.entities.parking.v2.endparking;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class EndParkingResponse extends BaseResponse {
    private EndParkingResponseBody body;

    public EndParkingResponseBody getBody() {
        return body;
    }

    public void setBody(EndParkingResponseBody body) {
        this.body = body;
    }

}
