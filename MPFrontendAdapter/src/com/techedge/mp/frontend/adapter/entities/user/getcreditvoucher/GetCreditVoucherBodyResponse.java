package com.techedge.mp.frontend.adapter.entities.user.getcreditvoucher;

import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetCreditVoucherBodyResponse extends BaseResponse {
    private String                             voucher;
    private List<GetCreditVoucherDataResponse> vouchers;
    private List<GetCreditVoucherDataResponse> given;

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public List<GetCreditVoucherDataResponse> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<GetCreditVoucherDataResponse> vouchers) {
        this.vouchers = vouchers;
    }

    public List<GetCreditVoucherDataResponse> getGiven() {
        return given;
    }

    public void setGiven(List<GetCreditVoucherDataResponse> given) {
        this.given = given;
    }

}
