package com.techedge.mp.frontend.adapter.entities.postpaid.retrievependingtransaction;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrievePendingTransactionResponse extends BaseResponse {
	
	private RetrievePendingTransactionBodyResponse body;

	public RetrievePendingTransactionBodyResponse getBody() {
		return body;
	}
	public void setBody(RetrievePendingTransactionBodyResponse body) {
		this.body = body;
	}
}
