package com.techedge.mp.frontend.adapter.entities.postpaid.notifydisplaytransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class NotifyDisplayTransactionBodyRequest implements Validable {
	
	private String mpCode;

	public String getMPcode() {
		return mpCode;
	}
	public void setMPCode(String mpCode) {
		this.mpCode = mpCode;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.mpCode == null || this.mpCode.length() != 32 || this.mpCode.trim() == "") {
			
			status.setStatusCode(StatusCode.REFUEL_CONFIRM_REFUEL_ID_WRONG);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.REFUEL_CONFIRM_SUCCESS);
		
		return status;
	}
	
}