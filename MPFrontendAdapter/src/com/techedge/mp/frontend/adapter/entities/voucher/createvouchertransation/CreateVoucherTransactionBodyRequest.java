package com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateVoucherTransactionBodyRequest implements Validable {

    private VoucherPaymentDataRequest voucherPaymentData;

    public CreateVoucherTransactionBodyRequest() {}

    public VoucherPaymentDataRequest getVoucherPaymentData() {
        return voucherPaymentData;
    }

    public void setVoucherPaymentData(VoucherPaymentDataRequest voucherPaymentData) {
        this.voucherPaymentData = voucherPaymentData;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.voucherPaymentData != null) {

            status = this.voucherPaymentData.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.CREATE_VOUCHER_TRANSACTION_SUCCESS);

        return status;
    }

}
