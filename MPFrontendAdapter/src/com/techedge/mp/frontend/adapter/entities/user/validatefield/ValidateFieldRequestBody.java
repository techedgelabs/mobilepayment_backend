package com.techedge.mp.frontend.adapter.entities.user.validatefield;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ValidateFieldRequestBody implements Validable {
    private String type;
    private String verificationField;
    private String verificationCode;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getVerificationField() {
        return verificationField;
    }

    public void setVerificationField(String verificationField) {
        this.verificationField = verificationField;
    }

    public String getVerificationCode() {
        return verificationCode;
    }

    public void setVerificationCode(String verificationCode) {
        this.verificationCode = verificationCode;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.type == null || this.type.trim().length() > 50) {

            status.setStatusCode(StatusCode.USER_VALID_FIELD_WRONG);

            return status;

        }
        if (this.verificationField == null || this.verificationField.trim().length() > 50) {

            status.setStatusCode(StatusCode.USER_VALID_FIELD_WRONG);

            return status;

        }

        if (this.verificationCode == null || this.verificationCode.trim().length() > 50) {

            status.setStatusCode(StatusCode.USER_VALID_CODE_WRONG);

            return status;

        }

        status.setStatusCode(StatusCode.USER_VALID_SUCCESS);

        return status;
    }

}
