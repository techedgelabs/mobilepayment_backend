package com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingzone;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone;

public class RetrieveParkingZonesResponseBody {

    private List<ParkingZone> parkingZones = new ArrayList<ParkingZone>(0);

    public List<ParkingZone> getParkingZones() {
        return parkingZones;
    }

    public void setParkingZones(List<ParkingZone> parkingZones) {
        this.parkingZones = parkingZones;
    }

}
