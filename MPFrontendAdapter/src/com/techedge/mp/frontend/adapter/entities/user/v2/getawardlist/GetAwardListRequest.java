package com.techedge.mp.frontend.adapter.entities.user.v2.getawardlist;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.techedge.mp.core.business.interfaces.AwardDetailData;
import com.techedge.mp.core.business.interfaces.AwardDetailDataResponse;
import com.techedge.mp.core.business.interfaces.BrandDataDetail;
import com.techedge.mp.core.business.interfaces.CategoryBurnDataDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionDetail;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.AwardData;
import com.techedge.mp.frontend.adapter.entities.common.AwardGroup;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.BrandData;
import com.techedge.mp.frontend.adapter.entities.common.CategoryBurnInfo;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.RedemptionData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetAwardListRequest extends AbstractRequest implements Validable {

    private Status     status = new Status();
    
    private static final String BRAND_ENI_GAS_E_LUCE_ORIGINAL = "Eni gas e luce";
    private static final String BRAND_ENI_GAS_E_LUCE_UPDATED  = "Eni Gas&Luce";

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-AWARD-LIST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        else {

            status.setStatusCode(StatusCode.GET_PARTNER_LIST_SUCCESS);

        }

        return status;

    }

    @Override
    public BaseResponse execute() {

        GetAwardListBodyResponse awardListBodyResponse = new GetAwardListBodyResponse();
        GetAwardListResponse getAwardListResponse = new GetAwardListResponse();

        AwardDetailDataResponse awardResponse = getUserV2ServiceRemote().getAwardList(this.getCredential().getRequestID(), this.getCredential().getTicketID());
        
      
        //START dati Stub 
//        AwardDetailDataResponse awardResponse = new AwardDetailDataResponse();
//
//        
//        List<AwardDetailData>        awardDetailList            = new ArrayList<AwardDetailData>(0);
//        
//        AwardDetailData awardDetailData1 = new AwardDetailData();
//        AwardDetailData awardDetailData2 = new AwardDetailData();
//        AwardDetailData awardDetailData3 = new AwardDetailData();
//        AwardDetailData awardDetailData4 = new AwardDetailData();
//        AwardDetailData awardDetailData5 = new AwardDetailData();
//        AwardDetailData awardDetailData6 = new AwardDetailData();
//        AwardDetailData awardDetailData7 = new AwardDetailData();
//        AwardDetailData awardDetailData8 = new AwardDetailData();
//        AwardDetailData awardDetailData9 = new AwardDetailData();
//        AwardDetailData awardDetailData10 = new AwardDetailData();
//        
//        awardDetailData1.setAwardId(1);
//        awardDetailData1.setBrand("Chicco");
//        awardDetailData1.setBrandId(1);
//        awardDetailData1.setCancelable(false);
//        awardDetailData1.setCategory("INFANZIA");
//        awardDetailData1.setCategoryId(1);
//        awardDetailData1.setContribution(new BigDecimal("12"));
//        awardDetailData1.setDateStartRedemption(new Date().getTime());
//        awardDetailData1.setDateStopRedemption(new Date().getTime());
//        awardDetailData1.setDescription("Prima infanzia ");
//        awardDetailData1.setDescriptionShort("Prima infanzia short");
//        awardDetailData1.setPoint(345);
//        awardDetailData1.setValue(new BigDecimal("1200"));
//        awardDetailData1.setRedeemable(true);
//        awardDetailData1.setTangible(true);
//        awardDetailData1.setType("V");
//        awardDetailData1.setUrlImageAward("www.chicco.it");
//        awardDetailData1.setUrlImageBrand("www.chicco.it");
//        
//        awardDetailData2.setAwardId(2);
//        awardDetailData2.setBrand("Ferrero cinemas");
//        awardDetailData2.setBrandId(2);
//        awardDetailData2.setCancelable(false);
//        awardDetailData2.setCategory("CINEMA");
//        awardDetailData2.setCategoryId(2);
//        awardDetailData2.setContribution(new BigDecimal("5"));
//        awardDetailData2.setDateStartRedemption(new Date().getTime());
//        awardDetailData2.setDateStopRedemption(new Date().getTime());
//        awardDetailData2.setDescription("CINEMA svago ");
//        awardDetailData2.setDescriptionShort("Promo cinema 2x1");
//        awardDetailData2.setPoint(567);
//        awardDetailData5.setValue(new BigDecimal("1400"));
//        awardDetailData2.setRedeemable(true);
//        awardDetailData2.setTangible(true);
//        awardDetailData2.setType("V");
//        awardDetailData2.setUrlImageAward("www.ferrerocinemas.it");
//        awardDetailData2.setUrlImageBrand("www.ferrerocinemas.it");
//        
//        awardDetailData3.setAwardId(3);
//        awardDetailData3.setBrand("Fastweb");
//        awardDetailData3.setBrandId(3);
//        awardDetailData3.setCancelable(false);
//        awardDetailData3.setCategory("FASTWEB RETE");
//        awardDetailData3.setCategoryId(3);
//        awardDetailData3.setContribution(new BigDecimal("25"));
//        awardDetailData3.setDateStartRedemption(new Date().getTime());
//        awardDetailData3.setDateStopRedemption(new Date().getTime());
//        awardDetailData3.setDescription("FASTWEB WI FI ");
//        awardDetailData3.setDescriptionShort("Promo rete Dati");
//        awardDetailData3.setPoint(3500);
//        awardDetailData3.setValue(new BigDecimal("1500"));
//        awardDetailData3.setRedeemable(false);
//        awardDetailData3.setTangible(true);
//        awardDetailData3.setType("V");
//        awardDetailData3.setUrlImageAward("www.fastweb.it");
//        awardDetailData3.setUrlImageBrand("www.fastweb.it");
//        
//        awardDetailData4.setAwardId(4);
//        awardDetailData4.setBrand("Prenatal");
//        awardDetailData4.setBrandId(4);
//        awardDetailData4.setCancelable(false);
//        awardDetailData4.setCategory("INFANZIA");
//        awardDetailData4.setCategoryId(4);
//        awardDetailData4.setContribution(new BigDecimal("15"));
//        awardDetailData4.setDateStartRedemption(new Date().getTime());
//        awardDetailData4.setDateStopRedemption(new Date().getTime());
//        awardDetailData4.setDescription("PRENATAL ");
//        awardDetailData4.setDescriptionShort("Buono  Prenatal 20� su 70� di spesa");
//        awardDetailData4.setPoint(789);
//        awardDetailData4.setValue(new BigDecimal("7500"));
//        awardDetailData4.setRedeemable(false);
//        awardDetailData4.setTangible(true);
//        awardDetailData4.setType("V");
//        awardDetailData4.setUrlImageAward("www.prenatal.it");
//        awardDetailData4.setUrlImageBrand("www.prenatal.it");
//        
//        awardDetailData5.setAwardId(4);
//        awardDetailData5.setBrand("Buono La Feltrinelli.it 15� su 79� di spesa");
//        awardDetailData5.setBrandId(4);
//        awardDetailData5.setCancelable(false);
//        awardDetailData5.setCategory("EDITORIA");
//        awardDetailData5.setCategoryId(4);
//        awardDetailData5.setContribution(new BigDecimal("15"));
//        awardDetailData5.setDateStartRedemption(new Date().getTime());
//        awardDetailData5.setDateStopRedemption(new Date().getTime());
//        awardDetailData5.setDescription("Buono La Feltrinelli.it 15� su 79� di spesa ");
//        awardDetailData5.setDescriptionShort("15� - spesa minima 79�");
//        awardDetailData5.setPoint(5000);
//        awardDetailData5.setValue(new BigDecimal("1500"));
//        awardDetailData5.setRedeemable(false);
//        awardDetailData5.setTangible(true);
//        awardDetailData5.setType("V");
//        awardDetailData5.setUrlImageAward("www.lafeltrinelli.it");
//        awardDetailData5.setUrlImageBrand("www.lafeltrinelli.it");
//        
//        awardDetailData6.setAwardId(1);
//        awardDetailData6.setBrand("Io bimbo");
//        awardDetailData6.setBrandId(1);
//        awardDetailData6.setCancelable(false);
//        awardDetailData6.setCategory("INFANZIA");
//        awardDetailData6.setCategoryId(1);
//        awardDetailData6.setContribution(new BigDecimal("12"));
//        awardDetailData6.setDateStartRedemption(new Date().getTime());
//        awardDetailData6.setDateStopRedemption(new Date().getTime());
//        awardDetailData6.setDescription("Prima infanzia ");
//        awardDetailData6.setDescriptionShort("Buono Io Bimbo 10� su 59� di spesa");
//        awardDetailData6.setPoint(345);
//        awardDetailData6.setValue(new BigDecimal("1900"));
//        awardDetailData6.setRedeemable(true);
//        awardDetailData6.setTangible(true);
//        awardDetailData6.setType("V");
//        awardDetailData6.setUrlImageAward("www.iobimbo.it");
//        awardDetailData6.setUrlImageBrand("www.iobimbo.it");
//        
//        awardDetailData7.setAwardId(1);
//        awardDetailData7.setBrand("Io bimbo");
//        awardDetailData7.setBrandId(1);
//        awardDetailData7.setCancelable(false);
//        awardDetailData7.setCategory("SPORT Bimbo");
//        awardDetailData7.setCategoryId(1);
//        awardDetailData7.setContribution(new BigDecimal("19"));
//        awardDetailData7.setDateStartRedemption(new Date().getTime());
//        awardDetailData7.setDateStopRedemption(new Date().getTime());
//        awardDetailData7.setDescription("Sport infanzia ");
//        awardDetailData7.setDescriptionShort("Buono Io Bimbo 25� su 69� di spesa");
//        awardDetailData7.setPoint(543);
//        awardDetailData7.setValue(new BigDecimal("1945"));
//        awardDetailData7.setRedeemable(true);
//        awardDetailData7.setTangible(true);
//        awardDetailData7.setType("V");
//        awardDetailData7.setUrlImageAward("www.iobimbo.it");
//        awardDetailData7.setUrlImageBrand("www.iobimbo.it");
//        
//        awardDetailData8.setAwardId(1);
//        awardDetailData8.setBrand("Io bimbo");
//        awardDetailData8.setBrandId(3);
//        awardDetailData8.setCancelable(false);
//        awardDetailData8.setCategory("Tempo Libero");
//        awardDetailData8.setCategoryId(1);
//        awardDetailData8.setContribution(new BigDecimal("12"));
//        awardDetailData8.setDateStartRedemption(new Date().getTime());
//        awardDetailData8.setDateStopRedemption(new Date().getTime());
//        awardDetailData8.setDescription("Giochi infanzia ");
//        awardDetailData8.setDescriptionShort("Buono Io Bimbo 10� su 59� di spesa");
//        awardDetailData8.setPoint(345);
//        awardDetailData8.setValue(new BigDecimal("1900"));
//        awardDetailData8.setRedeemable(false);
//        awardDetailData8.setTangible(true);
//        awardDetailData8.setType("V");
//        awardDetailData8.setUrlImageAward("www.iobimbo.it");
//        awardDetailData8.setUrlImageBrand("www.iobimbo.it");
//        
//        awardDetailData9.setAwardId(1);
//        awardDetailData9.setBrand("Tifoshop");
//        awardDetailData9.setBrandId(10);
//        awardDetailData9.setCancelable(false);
//        awardDetailData9.setCategory("Tempo Libero");
//        awardDetailData9.setCategoryId(1);
//        awardDetailData9.setContribution(new BigDecimal("12"));
//        awardDetailData9.setDateStartRedemption(new Date().getTime());
//        awardDetailData9.setDateStopRedemption(new Date().getTime());
//        awardDetailData9.setDescription("Buono Tifoshop 15,00 � su 80,00� di spesa");
//        awardDetailData9.setDescriptionShort("15 � - spesa minima 80�");
//        awardDetailData9.setPoint(3435);
//        awardDetailData9.setValue(new BigDecimal("1934"));
//        awardDetailData9.setRedeemable(true);
//        awardDetailData9.setTangible(true);
//        awardDetailData9.setType("V");
//        awardDetailData9.setUrlImageAward("http://enistation-411377.s3-eu-west-1.amazonaws.com/logo_menu_it.png");
//        awardDetailData9.setUrlImageBrand("http://enistation-411377.s3-eu-west-1.amazonaws.com/logo_menu_it.png");
//        
//        awardDetailData10.setAwardId(1);
//        awardDetailData10.setBrand("Tifoshop");
//        awardDetailData10.setBrandId(10);
//        awardDetailData10.setCancelable(false);
//        awardDetailData10.setCategory("Tempo Libero");
//        awardDetailData10.setCategoryId(1);
//        awardDetailData10.setContribution(new BigDecimal("12"));
//        awardDetailData10.setDateStartRedemption(new Date().getTime());
//        awardDetailData10.setDateStopRedemption(new Date().getTime());
//        awardDetailData10.setDescription("Buono Tifoshop 15,00 � su 80,00� di spesa");
//        awardDetailData10.setDescriptionShort("15 � - spesa minima 80�");
//        awardDetailData10.setPoint(3435);
//        awardDetailData10.setValue(new BigDecimal("1934"));
//        awardDetailData10.setRedeemable(false);
//        awardDetailData10.setTangible(true);
//        awardDetailData10.setType("V");
//        awardDetailData10.setUrlImageAward("http://enistation-411377.s3-eu-west-1.amazonaws.com/logo_menu_it.png");
//        awardDetailData10.setUrlImageBrand("http://enistation-411377.s3-eu-west-1.amazonaws.com/logo_menu_it.png");
//        
//        awardDetailList.add(awardDetailData1);
//        awardDetailList.add(awardDetailData2);
//        awardDetailList.add(awardDetailData3);
//        awardDetailList.add(awardDetailData4);
//        awardDetailList.add(awardDetailData5);
//        awardDetailList.add(awardDetailData6);
//        awardDetailList.add(awardDetailData7);
//        awardDetailList.add(awardDetailData8);
//        awardDetailList.add(awardDetailData9);
//        awardDetailList.add(awardDetailData10);
//        
//        awardResponse.setAwardDetailList(awardDetailList);
        //END dati Stub 
        
        
        if (awardResponse != null && !awardResponse.getAwardDetailList().isEmpty()) {

            for (AwardDetailData awardDataDetail : awardResponse.getAwardDetailList()) {

                AwardData awardData = new AwardData();

                awardData.setAwardId(awardDataDetail.getAwardId());
                awardData.setBrandId(awardDataDetail.getBrandId());
                // Mapping del brand Eni gase e luce per attivare la visualizzazione del form con il codice partner su app
                if (awardDataDetail.getBrand().equals(BRAND_ENI_GAS_E_LUCE_ORIGINAL)) {
                    awardData.setBrand(BRAND_ENI_GAS_E_LUCE_UPDATED);
                }
                else {
                    awardData.setBrand(awardDataDetail.getBrand());
                }
                awardData.setCancelable(awardDataDetail.getCancelable());
                awardData.setContribution(AmountConverter.toMobile(awardDataDetail.getContribution()));
                awardData.setCategoryId(awardDataDetail.getCategoryId());
                awardData.setCategory(awardDataDetail.getCategory());
                awardData.setDateStartRedemption(CustomDate.createCustomDate(new Date(awardDataDetail.getDateStartRedemption())));
                awardData.setDateStopRedemption(CustomDate.createCustomDate(new Date(awardDataDetail.getDateStopRedemption())));
                awardData.setDescription(awardDataDetail.getDescription());
                awardData.setDescriptionShort(awardDataDetail.getDescriptionShort());
                awardData.setPoint(awardDataDetail.getPoint());
                awardData.setValue(AmountConverter.toMobile(awardDataDetail.getValue()));
                awardData.setRedeemable(awardDataDetail.getRedeemable());
                awardData.setTangible(awardDataDetail.getTangible());
                awardData.setType(awardDataDetail.getType());
                awardData.setUrlImageAward(awardDataDetail.getUrlImageAward());
                awardData.setUrlImageBrand(awardDataDetail.getUrlImageBrand());
                awardData.setPointPartner(awardDataDetail.getPointPartner());
                awardData.setChannelRedemption(awardDataDetail.getChannelRedemption());
                awardData.setTypePointPartner(awardDataDetail.getTypePointPartner());
                
                if(awardDataDetail.getRedeemable()){
                    
                    AwardGroup awardGroupFound = null;
                    
                    for(AwardGroup awardGroupDetail : awardListBodyResponse.getRedeemableAwardGroupList()){
                        
                        if(awardGroupDetail.getBrandId() == awardDataDetail.getBrandId()){
                            
                            awardGroupFound = awardGroupDetail;
                            break;
                        }
                        
                    }
                    
                    if(awardGroupFound == null){
                        
                        //System.out.println("Award group redeemable not found, new award group created");
                        awardGroupFound = new AwardGroup();
                        
                        awardGroupFound.setBrandId(awardDataDetail.getBrandId());
                        
                        awardGroupFound.getAwardDataList().add(awardData);
                        
                        awardListBodyResponse.getRedeemableAwardGroupList().add(awardGroupFound);
                        
                    }
                    else{
                        //System.out.println("Award group redeemable founded added to existing award group");
                        awardGroupFound.getAwardDataList().add(awardData);
                    }
                            
                    
                }
                
                else if(! awardDataDetail.getRedeemable()){
                    
                    AwardGroup awardGroupNotRedeemableFound =null;
                    
                    for(AwardGroup awardNotRedeemableGroupDetail : awardListBodyResponse.getNotRedeemableAwardGroupList()){
                        
                        if(awardNotRedeemableGroupDetail.getBrandId() == awardDataDetail.getBrandId()){
                            
                            awardGroupNotRedeemableFound = awardNotRedeemableGroupDetail;
                            break;
                        }
                        
                    }
                    
                    if(awardGroupNotRedeemableFound == null){
                        
                        //System.out.println("Award group not redeemable not found, new award group created");
                        
                        awardGroupNotRedeemableFound = new AwardGroup();
                        
                        awardGroupNotRedeemableFound.setBrandId(awardDataDetail.getBrandId());
                        
                        awardGroupNotRedeemableFound.getAwardDataList().add(awardData);
                        
                        awardListBodyResponse.getNotRedeemableAwardGroupList().add(awardGroupNotRedeemableFound);
                    }
                    else{
                        
                        //System.out.println("Award group not redeemable founded added to existing award group");
                        
                        awardGroupNotRedeemableFound.getAwardDataList().add(awardData);
                    }
                            
                }

            }
        }

        if(!awardResponse.getBrandDataDetailList().isEmpty()){
            for (BrandDataDetail brandDataDetail : awardResponse.getBrandDataDetailList()) {

                BrandData brandData = new BrandData();

                brandData.setBrandId(brandDataDetail.getBrandId());
                brandData.setBrand(brandDataDetail.getBrand());
                brandData.setBrandUrl(brandDataDetail.getBrandUrl());

                awardListBodyResponse.getBrandDataList().add(brandData);

            }

        }
            
        if(!awardResponse.getCategoryBurnDataDetailList().isEmpty()){
            for (CategoryBurnDataDetail categoryBurnDataDetail : awardResponse.getCategoryBurnDataDetailList()) {
                CategoryBurnInfo categoryBurnInfo = new CategoryBurnInfo();

                categoryBurnInfo.setCategoryId(categoryBurnDataDetail.getCategoryId());
                categoryBurnInfo.setCategory(categoryBurnDataDetail.getCategory());
                categoryBurnInfo.setCategoryImageUrl(categoryBurnDataDetail.getCategoryImageUrl());

                awardListBodyResponse.getCategoryBurnList().add(categoryBurnInfo);
            }
        }

        if(! awardResponse.getRedemptionDetailList().isEmpty()){
            for (RedemptionDetail redemptionDetail : awardResponse.getRedemptionDetailList()) {
                RedemptionData redemptionData = new RedemptionData();

                Integer amount = AmountConverter.toMobile(redemptionDetail.getAmount());
                redemptionData.setAmount(amount);
                redemptionData.setCode(redemptionDetail.getCode());
                redemptionData.setName(redemptionDetail.getName());
                redemptionData.setPoints(redemptionDetail.getPoints());
                
                awardListBodyResponse.getRedemptions().add(redemptionData);

            }
        }
        
        if(! awardResponse.getParkingRedemptionDetailList().isEmpty()){
            for (RedemptionDetail parkingRedemptionDetail : awardResponse.getParkingRedemptionDetailList()) {
                RedemptionData parkingRedemptionData = new RedemptionData();

                Integer amount = AmountConverter.toMobile(parkingRedemptionDetail.getAmount());
                parkingRedemptionData.setAmount(amount);
                parkingRedemptionData.setCode(parkingRedemptionDetail.getCode());
                parkingRedemptionData.setName(parkingRedemptionDetail.getName());
                parkingRedemptionData.setPoints(parkingRedemptionDetail.getPoints());
                
                awardListBodyResponse.getParkingRedemptions().add(parkingRedemptionData);

            }
        }
         
        status.setStatusCode(awardResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(awardResponse.getStatusCode()));
        
        //Stub status
//        status.setStatusCode("200");
//        status.setStatusMessage("Award list restituita con successo");
        //end stub status

        getAwardListResponse.setStatus(status);
        getAwardListResponse.setBody(awardListBodyResponse);

        return getAwardListResponse;

    }

}
