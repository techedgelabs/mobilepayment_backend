package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.ProductStatistics;

public class RetrieveTransactionHistoryBodyResponse {

    private int                      pageOffset;
    private int                      pagesTotal;
    private List<ProductStatistics>  statistics     = new ArrayList<ProductStatistics>(0);
    private List<TransactionHistory> paymentHistory = new ArrayList<TransactionHistory>(0);
    private List<ParkingTransactionHistory> parkingHistory = new ArrayList<ParkingTransactionHistory>(0);

    public List<TransactionHistory> getPaymentHistory() {
        return paymentHistory;
    }

    public void setPaymentHistory(List<TransactionHistory> paymentHistory) {
        this.paymentHistory = paymentHistory;
    }

    public int getPageOffset() {
        return pageOffset;
    }

    public void setPageOffset(int pageOffset) {
        this.pageOffset = pageOffset;
    }

    public int getPagesTotal() {
        return pagesTotal;
    }

    public void setPagesTotal(int pagesTotal) {
        this.pagesTotal = pagesTotal;
    }

    public List<ProductStatistics> getStatistics() {
        return statistics;
    }

    public void setStatistics(List<ProductStatistics> statistics) {
        this.statistics = statistics;
    }

    public List<ParkingTransactionHistory> getParkingHistory() {
        return parkingHistory;
    }

    public void setParkingHistory(List<ParkingTransactionHistory> parkingHistory) {
        this.parkingHistory = parkingHistory;
    }
    
}
