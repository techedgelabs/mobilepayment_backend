package com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.AmountInfo;


public class RetrieveApplicationSettingsRefuelBodyResponse {

	private Integer minAmount;
	private Integer maxAmount;
	private List<AmountInfo> defaultAmount = new ArrayList<AmountInfo>(0);
	
	public Integer getMinAmount() {
		return minAmount;
	}
	public void setMinAmount(Integer minAmount) {
		this.minAmount = minAmount;
	}
	
	public Integer getMaxAmount() {
		return maxAmount;
	}
	public void setMaxAmount(Integer maxAmount) {
		this.maxAmount = maxAmount;
	}
	
	public List<AmountInfo> getDefaultAmount() {
		return defaultAmount;
	}
	public void setDefaultAmount(List<AmountInfo> defaultAmount) {
		this.defaultAmount = defaultAmount;
	}
}
