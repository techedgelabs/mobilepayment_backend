package com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.GetStationInfo;
import com.techedge.mp.core.business.interfaces.GetStationListResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.StationLocationData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveStationListRequest extends AbstractRequest implements Validable {

    private Credential                     credential;
    private RetrieveStationListBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveStationListBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveStationListBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("STATION-LIST-RETRIEVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveStationListRequest retrieveStationListRequest = this;

        com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveListResponse retrieveListResponse = new com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveListResponse();

        Status statusResponse = new Status();

        //Controllo che la posizione esista
        Double userPositionLatitude = 0d;
        Double userPositionLongitude = 0d;

        if (retrieveStationListRequest.getBody().getUserPosition() != null) {

            userPositionLatitude = retrieveStationListRequest.getBody().getUserPosition().getLatitude();
            userPositionLongitude = retrieveStationListRequest.getBody().getUserPosition().getLongitude();
        }

        Credential retrieveStationListRequestCredential = retrieveStationListRequest.getCredential();
        GetStationListResponse getStationListResponse = getPostPaidTransactionServiceRemote().getStationList(retrieveStationListRequestCredential.getRequestID(),
                retrieveStationListRequestCredential.getTicketID(), userPositionLatitude, userPositionLongitude);

        if (!getStationListResponse.getStatusCode().equals(StatusCode.STATION_LIST_RETRIEVE_SUCCESS)) {

            statusResponse.setStatusCode(getStationListResponse.getStatusCode());
            statusResponse.setStatusMessage(prop.getProperty(getStationListResponse.getStatusCode()));
        }
        else {

            com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveBodyResponse retrieveBodyResponse = new com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveBodyResponse();

            if (!getStationListResponse.getStationInfoList().isEmpty()) {

                //setto la lista di stazioni
                composeStationListResponse(getStationListResponse, retrieveBodyResponse);

            }

            if (!getStationListResponse.getActiveCityInfoList().isEmpty()) {

                //setto la lista delle citt� attive
                composeActiveCityListResponse(getStationListResponse, retrieveBodyResponse);
            }

            retrieveListResponse.setBody(retrieveBodyResponse);

            statusResponse.setStatusCode(StatusCode.STATION_LIST_RETRIEVE_SUCCESS);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_LIST_RETRIEVE_SUCCESS));

            retrieveListResponse.setStatus(statusResponse);
        }
        retrieveListResponse.setStatus(statusResponse);

        return retrieveListResponse;
    }

    private void composeStationListResponse(GetStationListResponse getStationListResponse,
            com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveBodyResponse retrieveBodyResponse) {

        for (GetStationInfo stationInfo : getStationListResponse.getStationInfoList()) {

            //System.out.println("station: " + stationInfo.getStationId());

            com.techedge.mp.frontend.adapter.entities.common.Station station = new com.techedge.mp.frontend.adapter.entities.common.Station();

            station.setStationID(stationInfo.getStationId());
            if (stationInfo.getName() != null) {

                station.setName(stationInfo.getName());
            }
            else {

                station.setName("");
            }
            StationLocationData locationData = composeLocatinData(stationInfo);
            station.setLocationData(locationData);

            // Funzionalit� disattivata
            //List<String> mode = composeModeActive(stationInfo);

            List<String> mode = new ArrayList<String>(0);
            station.setMode(mode);

            retrieveBodyResponse.getStations().add(station);

        }
    }

    private StationLocationData composeLocatinData(GetStationInfo stationInfo) {

        StationLocationData stationLocationData = new StationLocationData();

        String address = stationInfo.getAddress();
        String city = stationInfo.getCity();
        String province = stationInfo.getProvince();
        String country = stationInfo.getCountry();

        stationLocationData.setAddress(address);
        stationLocationData.setCity(city);
        stationLocationData.setProvince(province);
        stationLocationData.setCountry(country);

        try {
            stationLocationData.setLatitude(new Double(stationInfo.getLatitude()));
            stationLocationData.setLongitude(new Double(stationInfo.getLongitude()));
        }
        catch (Exception ex) {

            stationLocationData.setLatitude(0.0);
            stationLocationData.setLongitude(0.0);
        }

        if (stationInfo.getDistance() != null) {
            stationLocationData.setDistance(formatDouble(stationInfo.getDistance()));
        }
        else {
            stationLocationData.setDistance(null);
        }

        return stationLocationData;
    }

    private Double formatDouble(Double distance) {

        DecimalFormat formatter = new DecimalFormat("#0.00");

        //System.out.println("distance: " + distance);

        String doubleString = formatter.format(distance);
        String replaced = doubleString.replace(",", ".");

        Double distanceFormatted = Double.parseDouble(replaced);

        return distanceFormatted;
    }

    private void composeActiveCityListResponse(GetStationListResponse getStationListResponse,
            com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist.RetrieveBodyResponse retrieveBodyResponse) {

        for (com.techedge.mp.core.business.interfaces.ActiveCityInfo activeCityInfo : getStationListResponse.getActiveCityInfoList()) {

            System.out.println("City: " + activeCityInfo.getCity());
            com.techedge.mp.frontend.adapter.entities.common.ActiveCities activeCity = new com.techedge.mp.frontend.adapter.entities.common.ActiveCities();

            activeCity.setCurrentCity(activeCityInfo.getCurrentCity());
            activeCity.setStationCount(String.valueOf(activeCityInfo.getStationCount()));

            LocationData locationData = composeLocationData(activeCityInfo);
            activeCity.setLocationData(locationData);

            retrieveBodyResponse.getActiveCities().add(activeCity);
        }
    }

    private LocationData composeLocationData(com.techedge.mp.core.business.interfaces.ActiveCityInfo activeCityInfo) {

        LocationData locationData = new LocationData();

        locationData.setCity(activeCityInfo.getCity());

        if (activeCityInfo.getProvince() != null) {

            locationData.setProvince(activeCityInfo.getProvince());
        }
        else {

            locationData.setProvince("");
        }

        locationData.setCountry(activeCityInfo.getCountry());
        locationData.setLatitude(Double.parseDouble(activeCityInfo.getLatitude()));
        locationData.setLongitude(Double.parseDouble(activeCityInfo.getLongitude()));
        locationData.setAddress("");

        return locationData;
    }

}
