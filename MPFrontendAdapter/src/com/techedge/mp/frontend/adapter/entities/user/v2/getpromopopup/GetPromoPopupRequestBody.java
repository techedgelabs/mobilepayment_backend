package com.techedge.mp.frontend.adapter.entities.user.v2.getpromopopup;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class GetPromoPopupRequestBody implements Validable {

    private String sectionId;

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (sectionId == null || sectionId.trim() == "") {

            status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_V2_GETLANDING_SUCCESS);

        return status;
    }

}
