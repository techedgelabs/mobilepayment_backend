package com.techedge.mp.frontend.adapter.entities.user.insertmulticardpaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;

public class InsertMulticardPaymentMethodBodyResponse {

    private PaymentMethod paymentMethod;
    private String operationId;
    
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	public String getOperationId() {
		return operationId;
	}
	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

    

}
