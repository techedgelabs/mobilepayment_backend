package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.refuel.v2.createmulticardtransaction.CreateMulticardRefuelTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.v2.createtransaction.CreateRefuelTransactionRequest;

public class RefuelV2Request extends RootRequest {

    protected CreateRefuelTransactionRequest createRefuelTransaction;
    protected CreateMulticardRefuelTransactionRequest createMulticardRefuelTransaction;

    public CreateRefuelTransactionRequest getCreateRefuelTransaction() {
        return createRefuelTransaction;
    }

    public void setCreateRefuelTransaction(CreateRefuelTransactionRequest createRefuelTransaction) {
        this.createRefuelTransaction = createRefuelTransaction;
    }

    public CreateMulticardRefuelTransactionRequest getCreateMulticardRefuelTransaction() {
        return createMulticardRefuelTransaction;
    }

    public void setCreateMulticardRefuelTransaction(CreateMulticardRefuelTransactionRequest createMulticardRefuelTransaction) {
        this.createMulticardRefuelTransaction = createMulticardRefuelTransaction;
    }

}
