package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PostPaidCart;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.PostPaidRefuel;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Cash;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.POPStatusConverter;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptDynamic;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidExtendedRefuelData;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveTransactionDetailRequest extends AbstractRequest implements Validable {

    private Status                               status = new Status();

    private Credential                           credential;
    private RetrieveTransactionDetailBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveTransactionDetailBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveTransactionDetailBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DETAIL-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;
            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
            return status;
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail.RetrieveTransactionDetailRequest retrievePopTransactionDetailRequest = this;

        com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail.RetrieveTransactionDetailResponse retrievePopTransactionDetailResponse = new com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail.RetrieveTransactionDetailResponse();

        PostPaidGetTransactionDetailResponse postPaidGetTransactionDetailResponse = getPostPaidTransactionServiceRemote().getTransactionDetail(
                retrievePopTransactionDetailRequest.getCredential().getRequestID(), retrievePopTransactionDetailRequest.getCredential().getTicketID(),
                retrievePopTransactionDetailRequest.getBody().getTransactionId());

        status.setStatusCode(postPaidGetTransactionDetailResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(postPaidGetTransactionDetailResponse.getStatusCode()));
        retrievePopTransactionDetailResponse.setStatus(status);

        com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail.RetrieveTransactionDetailBodyResponse retrieveTransactionDetailBodyResponse = new com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail.RetrieveTransactionDetailBodyResponse();

        retrieveTransactionDetailBodyResponse.setSourceStatus(postPaidGetTransactionDetailResponse.getSourceStatus());
        retrieveTransactionDetailBodyResponse.setSourceType(postPaidGetTransactionDetailResponse.getSourceType());

        if (!status.getStatusCode().contains("200")) {
            retrievePopTransactionDetailResponse.setStatus(status);
            return retrievePopTransactionDetailResponse;
        }

        // station
        Station station = new Station();
        station.setStationID(postPaidGetTransactionDetailResponse.getStation().getStationID());

        LocationData locationData = new LocationData();
        locationData.setAddress(postPaidGetTransactionDetailResponse.getStation().getAddress());
        locationData.setCity(postPaidGetTransactionDetailResponse.getStation().getCity());
        locationData.setCountry(postPaidGetTransactionDetailResponse.getStation().getCountry());
        locationData.setLatitude(postPaidGetTransactionDetailResponse.getStation().getLatitude());
        locationData.setLongitude(postPaidGetTransactionDetailResponse.getStation().getLongitude());
        locationData.setProvince(postPaidGetTransactionDetailResponse.getStation().getProvince());
        station.setLocationData(locationData);

        retrieveTransactionDetailBodyResponse.setStation(station);
        // pump
        /*
         * if (postPaidGetTransactionDetailResponse.getPumpID() != null) {
         * 
         * Pump pump = new Pump();
         * 
         * pump.setPumpID(postPaidGetTransactionDetailResponse.getPumpID());
         * pump.setStatus(postPaidGetTransactionDetailResponse.getPumpStatus());
         * pump.setNumber(Integer.valueOf(postPaidGetTransactionDetailResponse.getPumpNumber()));
         * pump.getFuelType().add(postPaidGetTransactionDetailResponse.getPumpFuelType());
         * 
         * retrievePopTransactionDetailBodyResponse.setPump(pump);
         * }
         */

        // cash
        if (postPaidGetTransactionDetailResponse.getCashID() != null) {

            Cash cash = new Cash();

            cash.setCashID(postPaidGetTransactionDetailResponse.getCashID());
            if (postPaidGetTransactionDetailResponse.getCashNumber() != null) {
                cash.setNumber(Integer.valueOf(postPaidGetTransactionDetailResponse.getCashNumber()));
            }

            retrieveTransactionDetailBodyResponse.setCash(cash);
        }

        // transaction
        PostPaidTransactionHistoryData transaction = new PostPaidTransactionHistoryData();
        ReceiptDynamic receipt = new ReceiptDynamic();

        transaction.setAmount(AmountConverter.toMobile(postPaidGetTransactionDetailResponse.getAmount()));
        transaction.setCreationTimestamp(null);
        transaction.setStatusTitle(postPaidGetTransactionDetailResponse.getStatusTitle());
        transaction.setStatusDescription(postPaidGetTransactionDetailResponse.getStatusDescription());

        if (postPaidGetTransactionDetailResponse.getCreationTimestamp() != null) {
            transaction.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(postPaidGetTransactionDetailResponse.getCreationTimestamp().getTime())));
        }

        // Trascodifica dello stato
        if (postPaidGetTransactionDetailResponse.getStatus() == null) {
            transaction.setStatus(null);
        }
        else {
            String localStatus = POPStatusConverter.toAppStatus(postPaidGetTransactionDetailResponse.getStatus());

            if (!localStatus.isEmpty()) {
                transaction.setStatus(localStatus);
            }
            else {
                transaction.setStatus(postPaidGetTransactionDetailResponse.getStatus());
            }
        }

        transaction.setSubStatus(postPaidGetTransactionDetailResponse.getSubStatus());
        transaction.setTransactionID(postPaidGetTransactionDetailResponse.getMpTransactionID());
        transaction.setUseVoucher(postPaidGetTransactionDetailResponse.getUseVoucher());

        Double voucherAmount = 0.0;
        int position = 1;
        int parentIndex = -1;
        int positionPromo = 1;

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");

        if (!postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList().isEmpty()) {
            for (PostPaidConsumeVoucher voucherConsume : postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList()) {
                for (PostPaidConsumeVoucherDetail voucherDetail : voucherConsume.getPostPaidConsumeVoucherDetail()) {
                    parentIndex = receipt.addToVoucherData(-1, "VOUCHER_NAME", "PROMO", null, voucherDetail.getPromoDescription(), position++, null);
                    double consumedValue = 0.0;
                    if (voucherDetail.getConsumedValue() != null) {
                        voucherAmount += (voucherDetail.getConsumedValue() * 100);
                        consumedValue = voucherDetail.getConsumedValue().doubleValue();
                    }
                    receipt.addToVoucherData(parentIndex, "VOUCHER_AMOUNT", "IMPORTO", "�", currencyFormat.format(consumedValue), position++, null);

                    if (voucherConsume.getMarketingMsg() != null && !voucherConsume.getMarketingMsg().isEmpty()) {
                        receipt.addToPromoData(-1, "VOUCHER_MSG", "", null, voucherConsume.getMarketingMsg(), positionPromo++, null);
                        //receipt.addToVoucherData(parentIndex, "MESSAGE", "", null, voucherConsume.getMarketingMsg(), position2++, null);
                    }
                }
            }
        }

        if (!postPaidGetTransactionDetailResponse.getPrePaidConsumeVoucherList().isEmpty()) {
            System.out.println("found consume voucher");
            for (PrePaidConsumeVoucher prePaidVoucherConsume : postPaidGetTransactionDetailResponse.getPrePaidConsumeVoucherList()) {
                System.out.println("found consume voucher detail");
                for (PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetail : prePaidVoucherConsume.getPrePaidConsumeVoucherDetail()) {
                    parentIndex = receipt.addToVoucherData(-1, "VOUCHER_NAME", "PROMO", null, prePaidConsumeVoucherDetail.getPromoDescription(), position++, null);
                    double consumedValue = 0.0;
                    if (prePaidConsumeVoucherDetail.getConsumedValue() != null) {
                        voucherAmount += (prePaidConsumeVoucherDetail.getConsumedValue() * 100);
                        consumedValue = prePaidConsumeVoucherDetail.getConsumedValue().doubleValue();
                    }
                    receipt.addToVoucherData(parentIndex, "VOUCHER_AMOUNT", "IMPORTO", "�", currencyFormat.format(consumedValue), position++, null);

                    if (prePaidVoucherConsume.getMarketingMsg() != null && !prePaidVoucherConsume.getMarketingMsg().isEmpty()) {
                        receipt.addToPromoData(-1, "VOUCHER_MSG", "", null, prePaidVoucherConsume.getMarketingMsg(), positionPromo++, null);
                        //receipt.addToVoucherData(parentIndex, "MESSAGE", "", null, prePaidVoucherConsume.getMarketingMsg(), position2++, null);
                    }
                }
            }
        }

        transaction.setVoucherAmount(voucherAmount.intValue());

        Integer loyaltyBalance = 0;
        position = 1;
        parentIndex = -1;
        if (!postPaidGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList().isEmpty()) {
            for (PostPaidLoadLoyaltyCredits loyaltyCredits : postPaidGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList()) {
                parentIndex = receipt.addToLoyaltyData(-1, "LOYALTY_PAN", "CODICE", null, loyaltyCredits.getEanCode(), position++, null);
                String credits = "0";
                String balanceAmount = "0";

                if (loyaltyCredits.getCredits() != null) {
                    loyaltyBalance += loyaltyCredits.getCredits();
                    credits = loyaltyCredits.getCredits().toString();
                }

                if (loyaltyCredits.getBalance() != null) {
                    balanceAmount = loyaltyCredits.getBalance().toString();
                }

                if (loyaltyCredits.getMarketingMsg() != null) {
                    if (receipt.getPromoData().size() > 0) {
                        receipt.addToPromoData(-1, "SEPARATOR", "", null, null, positionPromo++, null);
                    }
                    receipt.addToPromoData(-1, "LOYALTY_MSG", "", null, loyaltyCredits.getMarketingMsg(), positionPromo++, null);
                }
                else {
                    if (loyaltyCredits.getStatusCode().equals(FidelityResponse.ENABLE_LOYALTY_CARD_MIGRATION_ERROR)
                            || loyaltyCredits.getStatusCode().equals(FidelityResponse.ENABLE_LOYALTY_CARD_MIGRATION_ERROR)) {
                        if (receipt.getPromoData().size() > 0) {
                            receipt.addToPromoData(-1, "SEPARATOR", "", null, null, positionPromo++, null);
                        }
                        receipt.addToPromoData(-1, "LOYALTY_MSG", "", null, loyaltyCredits.getMessageCode(), positionPromo++, null);
                    }
                }

                receipt.addToLoyaltyData(parentIndex, "LOYALTY_BALANCE", "PUNTI FEDELTA'", null, credits, position++, null);
                receipt.addToLoyaltyData(parentIndex, "LOYALTY_TOTAL", "SALDO PUNTI", null, balanceAmount, position++, null);

                if (loyaltyCredits.getWarningMsg() != null) {
                    receipt.addToLoyaltyData(-1, "LOYALTY_MSG", "", null, loyaltyCredits.getWarningMsg(), position++, null);
                }

                /*
                 * if (loyaltyCredits.getMarketingMsg() != null) {
                 * //receipt.addToPromoData(-1, "LOYALTY_MARKETING_MSG", "", null, loyaltyCredits.getMarketingMsg(), positionPromo++, null);
                 * receipt.addToLoyaltyData(parentIndex, "LOYALTY_MARKETING_MSG", "", null, loyaltyCredits.getMarketingMsg(), position++, null);
                 * }
                 */
            }
        }

        transaction.setLoyaltyBalance(loyaltyBalance);

        position = 1;
        if (postPaidGetTransactionDetailResponse.getSourceStatus().equals("POST-PAY") && postPaidGetTransactionDetailResponse.getCartList() != null) {
            double amountCart = 0.0;
            for (PostPaidCart cartData : postPaidGetTransactionDetailResponse.getCartList()) {
                if (cartData.getAmount() != null) {
                    amountCart += cartData.getAmount();
                }
            }

            if (postPaidGetTransactionDetailResponse.getCashID() != null && !postPaidGetTransactionDetailResponse.getCashID().equals("")
                    && !postPaidGetTransactionDetailResponse.getCashID().equals(" ")) {
                System.out.println("CASSA: " + postPaidGetTransactionDetailResponse.getCashID());
                receipt.addToShopData(-1, "SHOP_CASH_ID", "COD CASSA", null, postPaidGetTransactionDetailResponse.getCashID(), position++, null);
            }
            if (postPaidGetTransactionDetailResponse.getCashNumber() != null && !postPaidGetTransactionDetailResponse.getCashID().equals("")
                    && !postPaidGetTransactionDetailResponse.getCashID().equals(" ")) {
                System.out.println("CASSA: " + postPaidGetTransactionDetailResponse.getCashNumber());
                receipt.addToShopData(-1, "SHP_CASH_NUMBER", "N CASSA", null, postPaidGetTransactionDetailResponse.getCashNumber(), position++, null);
            }
            if (amountCart > 0.0) {
                receipt.addToShopData(-1, "SHOP_AMOUNT", "IMPORTO SHOP", "�", currencyFormat.format(amountCart), position++, null);
            }
        }

        position = 1;
        if (postPaidGetTransactionDetailResponse.getRefuelList() != null) {
            for (PostPaidRefuel postPaidRefuel : postPaidGetTransactionDetailResponse.getRefuelList()) {
                String amount = null;
                String quantity = null;
                PostPaidExtendedRefuelData postPaidRefuelJsonData = new PostPaidExtendedRefuelData();
                postPaidRefuelJsonData.setFuelAmount(postPaidRefuel.getFuelAmount());
                postPaidRefuelJsonData.setFuelQuantity(postPaidRefuel.getFuelQuantity());
                postPaidRefuelJsonData.setFuelType(postPaidRefuel.getFuelType());
                postPaidRefuelJsonData.setProductDescription(postPaidRefuel.getProductDescription());
                postPaidRefuelJsonData.setProductId(postPaidRefuel.getProductId());
                postPaidRefuelJsonData.setPumpId(postPaidRefuel.getPumpId());
                postPaidRefuelJsonData.setPumpNumber(postPaidRefuel.getPumpNumber());

                transaction.getRefuel().add(postPaidRefuelJsonData);

                parentIndex = receipt.addToRefuelData(-1, "REFUEL_PUMP_ID", "COD EROGATORE", null, postPaidRefuel.getPumpId(), position++, null);

                if (postPaidRefuel.getPumpNumber() != null) {
                    receipt.addToRefuelData(-1, "REFUEL_PUMP_NUMBER", "N EROGATORE", null, postPaidRefuel.getPumpNumber().toString(), position++, null);
                }

                if (postPaidRefuel.getFuelQuantity() != null) {
                    quantity = numberFormat.format(postPaidRefuel.getFuelQuantity().doubleValue());
                    receipt.addToRefuelData(-1, "REFUEL_FUEL_QUANTITY", "EROGATO", "lt", quantity, position++, null);
                }

                if (postPaidRefuel.getFuelType() != null && !postPaidRefuel.getFuelType().equals("")) {
                    receipt.addToRefuelData(-1, "REFUEL_FUEL_TYPE", "TIPO DI CARBURANTE", null, postPaidRefuel.getFuelType(), position++, null);
                }

                if (postPaidRefuel.getFuelAmount() != null) {
                    amount = currencyFormat.format(postPaidRefuel.getFuelAmount().doubleValue());
                    receipt.addToRefuelData(-1, "REFUEL_AMOUNT", "IMPORTO CARBURANTE", "�", amount, position++, null);
                }

                /*
                 * if (!postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList().isEmpty()
                 * && postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList().size() >= (index - 1)) {
                 * 
                 * Object[] voucherConsumeList = postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList().toArray();
                 * PostPaidConsumeVoucher voucherConsume = (PostPaidConsumeVoucher) voucherConsumeList[index - 1];
                 * receipt.addToRefuelData(-1, "REFUEL_WARNING", "", null, voucherConsume.getWarningMsg(), position++, null);
                 * }
                 */

                //receipt.addToRefuelData(rootIndex, -1, "REFUEL_WARNING_" + index, "", null, "Non so cosa passarti...", position++, null);
                receipt.addToRefuelData(-1, "SEPARATOR", "", null, null, position++, null);

            }
        }

        position = 1;
        parentIndex = receipt.addToPaymentData(-1, "STATION_ID", "CODICE PV", "", postPaidGetTransactionDetailResponse.getStation().getStationID(), position++, null);

        String data = "";
        if (postPaidGetTransactionDetailResponse.getCreationTimestamp() != null) {
            data = new SimpleDateFormat("dd/MM/yyyy").format(postPaidGetTransactionDetailResponse.getCreationTimestamp().getTime());
        }

        Double amount = postPaidGetTransactionDetailResponse.getAmount();

        if (amount == null) {
            amount = 0.0;
        }

        receipt.addToPaymentData(-1, "DATE", "DATA", "", data, position++, null);

        double bankAmount = amount.doubleValue() - (voucherAmount.doubleValue() / 100);

        if ((postPaidGetTransactionDetailResponse.getPaymentMethodType() == null || postPaidGetTransactionDetailResponse.getPaymentMethodType().equals(
                PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD))
                && bankAmount > 0.0) {

            receipt.addToPaymentData(-1, "AMOUNT", "IMPORTO ADDEBITATO", "�", currencyFormat.format(bankAmount), position++, null);

            receipt.addToPaymentData(-1, "PAN", "CARTA DI PAGAMENTO", "", postPaidGetTransactionDetailResponse.getPanCode(), position++, null);

            if (postPaidGetTransactionDetailResponse.getAuthorizationCode() != null && !postPaidGetTransactionDetailResponse.getAuthorizationCode().equals("")) {
                receipt.addToPaymentData(-1, "C_AUTH", "COD AUTORIZZAZIONE", "", postPaidGetTransactionDetailResponse.getAuthorizationCode(), position++, null);
            }

            if (postPaidGetTransactionDetailResponse.getBankTansactionID() != null && !postPaidGetTransactionDetailResponse.getBankTansactionID().equals("")) {
                receipt.addToPaymentData(-1, "N_OP", "N OPERAZIONE", "", postPaidGetTransactionDetailResponse.getBankTansactionID(), position++, null);
            }
        }

        receipt.addToPaymentData(-1, "SEPARATOR", "", "", null, position++, null);

        if (postPaidGetTransactionDetailResponse.getSurveyUrl() != null) {
            if (receipt.getPromoData().size() > 0) {
                receipt.addToPromoData(-1, "SEPARATOR", "", null, null, positionPromo++, null);
            }

            receipt.addToPromoData(-1, "SURVEY", "", null, "Valuta il servizio offerto", positionPromo++, postPaidGetTransactionDetailResponse.getSurveyUrl());
        }

        retrieveTransactionDetailBodyResponse.setTransaction(transaction);
        retrieveTransactionDetailBodyResponse.setReceipt(receipt);

        retrievePopTransactionDetailResponse.setBody(retrieveTransactionDetailBodyResponse);
        retrievePopTransactionDetailResponse.setStatus(status);

        return retrievePopTransactionDetailResponse;
    }
}