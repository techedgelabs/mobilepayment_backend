package com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveStationListBodyRequest implements Validable {

    private RetrieveStationUserPositionRequest userPosition;

    public RetrieveStationUserPositionRequest getUserPosition() {
        return userPosition;
    }

    public void setUserPosition(RetrieveStationUserPositionRequest userPosition) {
        this.userPosition = userPosition;
    }

    @Override
    public Status check() {

        Status status = new Status();
        status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);
        
        if (this.userPosition == null) {

            status.setStatusCode(StatusCode.STATION_RETRIEVE_USER_POSITION_WRONG);

        }

        return status;

    }
}
