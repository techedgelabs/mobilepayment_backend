package com.techedge.mp.frontend.adapter.entities.requests;

import java.lang.reflect.Field;

public class RootRequest {

    public AbstractRequest getAbstractRequest() throws IllegalArgumentException, IllegalAccessException {

        Field fieldFound = null;

        for (Field field : this.getClass().getDeclaredFields()) {
            if (field.get(this) != null) {
                fieldFound = field;
                break;
            }
        }

        if (fieldFound == null) {
            return null;
        }
        
        AbstractRequest request = (AbstractRequest) fieldFound.get(this);

        return request;
    }
    
    public String getRequestName() {
        String request = null;
        Field fieldFound = null;

        try {
            for (Field field : this.getClass().getDeclaredFields()) {
                if (field.get(this) != null) {
                    fieldFound = field;
                    break;
                }
            }
        }
        catch (Exception ex) {
            System.err.println("Errore nel recupero del nome della richiesta: " + ex.getMessage());
        }

        if (fieldFound != null) {
            request = fieldFound.getName();
        }

        return request;
        
    }
}
