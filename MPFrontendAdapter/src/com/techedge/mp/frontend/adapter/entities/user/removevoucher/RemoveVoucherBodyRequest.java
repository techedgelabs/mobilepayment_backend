package com.techedge.mp.frontend.adapter.entities.user.removevoucher;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RemoveVoucherBodyRequest implements Validable {

    private String voucherCode;

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.voucherCode == null || this.voucherCode.trim().isEmpty()) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        if (this.voucherCode.length() > 16) {

            status.setStatusCode(StatusCode.USER_REMOVE_VOUCHER_CODE_WRONG);

            return status;
        }

        status.setStatusCode(StatusCode.USER_REMOVE_VOUCHER_SUCCESS);

        return status;

    }

}
