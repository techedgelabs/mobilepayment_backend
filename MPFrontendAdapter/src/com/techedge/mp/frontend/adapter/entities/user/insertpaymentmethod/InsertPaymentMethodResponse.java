package com.techedge.mp.frontend.adapter.entities.user.insertpaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class InsertPaymentMethodResponse extends BaseResponse {
	
	
	private InsertPaymentMethodBodyResponse body;

	
	public InsertPaymentMethodBodyResponse getBody() {
		return body;
	}

	public void setBody(InsertPaymentMethodBodyResponse body) {
		this.body = body;
	}
	
}
