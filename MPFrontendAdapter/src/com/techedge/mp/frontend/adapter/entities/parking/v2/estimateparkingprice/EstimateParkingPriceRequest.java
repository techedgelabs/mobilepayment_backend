package com.techedge.mp.frontend.adapter.entities.parking.v2.estimateparkingprice;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.EstimateParkingPriceResult;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZonePriceInfo;
import com.techedge.mp.frontend.adapter.entities.common.v2.PlateNumberInfo;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class EstimateParkingPriceRequest extends AbstractRequest implements Validable {

    private Status                          status = new Status();

    private Credential                      credential;
    private EstimateParkingPriceRequestBody body;

    public EstimateParkingPriceRequestBody getBody() {
        return body;
    }

    public void setBody(EstimateParkingPriceRequestBody body) {
        this.body = body;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_AUTH_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        EstimateParkingPriceRequest estimateParkingPriceRequest = this;
        
        EstimateParkingPriceRequestBody estimateParkingPriceRequestBody = estimateParkingPriceRequest.getBody();
      //Secondi impostati a zero per evitare l'arrotondamento effettuato dai servizi MyCicero
        estimateParkingPriceRequestBody.getRequestedEndTime().setSecond(0);
        EstimateParkingPriceResult result = getParkingTransactionV2ServiceRemote().estimateParkingPrice(estimateParkingPriceRequest.getCredential().getTicketID(),
                estimateParkingPriceRequest.getCredential().getRequestID(), estimateParkingPriceRequestBody.getPlateNumber(),
                CustomTimestamp.convertToDate(estimateParkingPriceRequestBody.getRequestedEndTime()), estimateParkingPriceRequestBody.getParkingZoneId(),
                estimateParkingPriceRequestBody.getStallCode(), estimateParkingPriceRequestBody.getCityId());

        EstimateParkingPriceResponse response = new EstimateParkingPriceResponse();
        
        if (result != null && result.getStatusCode().equals(ResponseHelper.PARKING_ESTIMATE_PARKING_PRICE_SUCCESS)) {
            
            EstimateParkingPriceResponseBody responseBody = new EstimateParkingPriceResponseBody();
            
            ParkingZonePriceInfo parkingZonePriceInfo = new ParkingZonePriceInfo();
    
            PlateNumberInfo plateNumberInfo = new PlateNumberInfo();
            plateNumberInfo.setPlateNumber(result.getPlateNumber());
            plateNumberInfo.setDescription(result.getPlateNumberDescription());
            parkingZonePriceInfo.setPlateNumber(plateNumberInfo);
            
            parkingZonePriceInfo.setPrice(AmountConverter.toMobile(result.getPrice()));
            parkingZonePriceInfo.setStallCode(result.getStallCode());
            parkingZonePriceInfo.setNotice(result.getNotice());
            
            ParkingZone parkingZone = new ParkingZone();
            parkingZone.setId(result.getParkingZoneId());
            parkingZone.setName(result.getParkingZoneName());
            parkingZone.setDescription(result.getParkingZoneDescription());
            parkingZone.setCityId(result.getCityId());
            parkingZone.setCityName(result.getCityName());
            parkingZone.setAdministrativeAreaLevel2Code(result.getAdministrativeAreaLevel2Code());
            parkingZonePriceInfo.setParkingZone(parkingZone);
            
            parkingZonePriceInfo.setParkingStartTime(CustomTimestamp.convertToCustomTimestamp(result.getParkingStartTime()));
            parkingZonePriceInfo.setParkingEndTime(CustomTimestamp.convertToCustomTimestamp(result.getParkingEndTime()));
            parkingZonePriceInfo.setTransactionID(result.getTransactionId());
    
            responseBody.setParkingZonePriceInfo(parkingZonePriceInfo);
            
            response.setBody(responseBody);
        }
        
        Status status = new Status();
        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(prop.getProperty(result.getStatusCode()));
        
        response.setStatus(status);

        return response;

    }

}
