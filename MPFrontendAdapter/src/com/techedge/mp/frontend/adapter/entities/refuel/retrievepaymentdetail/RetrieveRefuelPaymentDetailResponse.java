package com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class RetrieveRefuelPaymentDetailResponse extends BaseResponse {

	
	private RetrieveRefuelPaymentDetailBodyResponse body;

	public RetrieveRefuelPaymentDetailBodyResponse getBody() {
		return body;
	}
	public void setBody(RetrieveRefuelPaymentDetailBodyResponse body) {
		this.body = body;
	}
	
}
