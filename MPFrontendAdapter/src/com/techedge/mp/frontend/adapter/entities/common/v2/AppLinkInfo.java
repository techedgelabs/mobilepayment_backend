package com.techedge.mp.frontend.adapter.entities.common.v2;

import com.techedge.mp.core.business.interfaces.AppLink;

public class AppLinkInfo extends AppLink {

    /**
     * 
     */
    private static final long serialVersionUID = -4683644136884024445L;
    
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    
}
