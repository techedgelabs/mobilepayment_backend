package com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers;

import java.util.ArrayList;
import java.util.List;

public class ManagerRetrieveVouchersBodyResponse {

    private List<ManagerRetrieveVouchersBodyDataResponse> voucherHistory = new ArrayList<ManagerRetrieveVouchersBodyDataResponse>(0);

    public List<ManagerRetrieveVouchersBodyDataResponse> getVoucherHistory() {
        return voucherHistory;
    }

    public void setVoucherHistory(List<ManagerRetrieveVouchersBodyDataResponse> voucherHistory) {
        this.voucherHistory = voucherHistory;
    }

}
