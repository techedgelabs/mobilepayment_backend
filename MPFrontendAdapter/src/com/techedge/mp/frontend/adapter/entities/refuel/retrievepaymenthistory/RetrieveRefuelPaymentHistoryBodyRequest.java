package com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory;

import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class RetrieveRefuelPaymentHistoryBodyRequest implements Validable {

	
	private CustomDate startDate;
	private CustomDate endDate;
	private Integer itemsLimit;
	private Integer pageOffset;
	private Boolean details;
	
	
	public CustomDate getStartDate() {
		return startDate;
	}
	public void setStartDate(CustomDate startDate) {
		this.startDate = startDate;
	}
	
	public CustomDate getEndDate() {
		return endDate;
	}
	public void setEndDate(CustomDate endDate) {
		this.endDate = endDate;
	}
	
	
	/**
	 * @return the itemsLimit
	 */
	public Integer getItemsLimit() {
		return itemsLimit;
	}
	/**
	 * @param itemsLimit the itemsLimit to set
	 */
	public void setItemsLimit(Integer itemsLimit) {
		this.itemsLimit = itemsLimit;
	}
	/**
	 * @return the pageOffset
	 */
	public Integer getPageOffset() {
		return pageOffset;
	}
	/**
	 * @param pageOffset the pageOffset to set
	 */
	public void setPageOffset(Integer pageOffset) {
		this.pageOffset = pageOffset;
	}
	
	public Boolean getDetails() {
		return details;
	}
	public void setDetails(Boolean details) {
		this.details = details;
	}
	
	
	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.itemsLimit != null) {
			
			status = Validator.checkHistoryLimit("PAY-HISTORY-LIMIT", this.itemsLimit);
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.PAYMENT_RETRIEVE_HISTORY_LIMIT_WRONG);
			
			return status;
			
		}
		
		if(this.startDate != null) {
		
			status = Validator.checkCustomDate("PAY-HISTORY-START", this.startDate);
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.PAYMENT_RETRIEVE_HISTORY_STARTDATE_WRONG);
			
			return status;
			
		}
		
		if(this.endDate != null) {
		
			status = Validator.checkCustomDate("PAY-HISTORY-END", this.endDate);
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
		
		} else {
			
			status.setStatusCode(StatusCode.PAYMENT_RETRIEVE_HISTORY_ENDDATE_WRONG);
			
			return status;
			
		}
		
		if(this.pageOffset != null) {
			
			status = Validator.checkHistoryPageOffset("PAY-HISTORY-PAGEOFFSET", this.pageOffset);
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.PAYMENT_RETRIEVE_HISTORY_PAGEOFFSET_WRONG);
			
			return status;
			
		}
		
		return status;
		
	}
	
}
