package com.techedge.mp.frontend.adapter.entities.user.redemption;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Locale;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.VoucherData;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RedemptionRequest extends AbstractRequest implements Validable {

    private Status                status = new Status();

    private RedemptionCredential  credential;
    private RedemptionBodyRequest body;

    public RedemptionCredential getCredential() {
        return credential;
    }

    public void setCredential(RedemptionCredential credential) {
        this.credential = credential;
    }

    public RedemptionBodyRequest getBody() {
        return body;
    }

    public void setBody(RedemptionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REDEMPTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;

    }

    @Override
    public BaseResponse execute() {
        RedemptionRequest redemptionRequest = this;
        RedemptionResponse redemptionResponse = new RedemptionResponse();
        RedemptionBodyResponse redemptionBodyResponse = new RedemptionBodyResponse();
        String ticketID = redemptionRequest.getCredential().getTicketID();
        String requestID = redemptionRequest.getCredential().getRequestID();
        Integer redemptionCode = redemptionRequest.getBody().getRedemptionCode();
        String pin = redemptionRequest.getCredential().getPin();

        com.techedge.mp.core.business.interfaces.loyalty.RedemptionResponse response = getUserServiceRemote().redemption(ticketID, requestID, redemptionCode, pin);

        if (response.getStatusCode().equals(ResponseHelper.USER_REDEMPTION_SUCCESS)) {
            VoucherDetail voucher = response.getVoucher();
            VoucherData voucherData = new VoucherData();

            voucherData.setCode(voucher.getVoucherCode());
            voucherData.setStatus(voucher.getVoucherStatus());
            voucherData.setType(voucher.getVoucherType());
            voucherData.setValue(AmountConverter.toMobile(voucher.getVoucherValue()));
            voucherData.setInitialValue(AmountConverter.toMobile(voucher.getInitialValue()));
            voucherData.setConsumedValue(AmountConverter.toMobile(voucher.getConsumedValue()));
            voucherData.setVoucherBalanceDue(AmountConverter.toMobile(voucher.getVoucherBalanceDue()));
            voucherData.setExpirationDate(CustomTimestamp.createCustomTimestamp(new Timestamp(voucher.getExpirationDate().getTime())));
            voucherData.setPromoCode(voucher.getPromoCode());
            voucherData.setPromoDescription(voucher.getPromoDescription());
            voucherData.setPromoDoc(voucher.getPromoDoc());
            voucherData.setIcon(voucher.getIcon());

            DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
            currencyFormat.applyPattern("##0.00");

            Double initialValue = 0.0;
            if (voucher.getInitialValue() != null) {
                initialValue = voucher.getInitialValue();
            }

            Double voucherBalanceDue = 0.0;
            if (voucher.getVoucherBalanceDue() != null) {
                voucherBalanceDue = voucher.getVoucherBalanceDue();
            }

            voucherData.addDetail("Taglio", currencyFormat.format(initialValue) + " �");
            voucherData.addDetail("Credito Residuo", currencyFormat.format(voucherBalanceDue) + " �");
            String expirationDateString = DateFormat.getDateInstance(DateFormat.LONG, Locale.ITALIAN).format(voucher.getExpirationDate());
            expirationDateString = capitalize(expirationDateString);
            voucherData.addDetail("Scadenza", expirationDateString);
            redemptionBodyResponse.setVoucher(voucherData);

        }
        redemptionBodyResponse.setCheckPinAttemptsLeft(response.getCheckPinAttemptsLeft());
        redemptionResponse.setBody(redemptionBodyResponse);

        status.setStatusCode(response.getStatusCode());

        status.setStatusMessage(prop.getProperty(response.getStatusCode()));

        redemptionResponse.setStatus(status);

        return redemptionResponse;
    }

    private static String capitalize(String inputString) {

        if (inputString == null || inputString.equals("")) {
            return "";
        }

        String[] arr = inputString.split(" ");
        StringBuffer sb = new StringBuffer();

        for (int i = 0; i < arr.length; i++) {
            sb.append(Character.toUpperCase(arr[i].charAt(0))).append(arr[i].substring(1)).append(" ");
        }
        return sb.toString().trim();
    }

}
