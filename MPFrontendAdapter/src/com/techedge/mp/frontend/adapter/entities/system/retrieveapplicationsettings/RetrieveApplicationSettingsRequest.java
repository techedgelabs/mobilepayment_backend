package com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveApplicationSettingsRequest implements Validable {

    private RetrieveApplicationSettingsCredential credential;

    public RetrieveApplicationSettingsCredential getCredential() {
        return credential;
    }

    public void setCredential(RetrieveApplicationSettingsCredential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("APPLICATION-SYSTEM", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        return status;

    }

}
