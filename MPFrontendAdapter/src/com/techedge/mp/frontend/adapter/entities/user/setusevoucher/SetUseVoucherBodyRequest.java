package com.techedge.mp.frontend.adapter.entities.user.setusevoucher;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class SetUseVoucherBodyRequest implements Validable {

    private boolean useVoucher;

    public boolean getUseVoucher() {
        return useVoucher;
    }

    public void setUseVoucher(boolean useVoucher) {
        this.useVoucher = useVoucher;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.USER_SET_USE_VOUCHER_SUCCESS);

        return status;

    }

}
