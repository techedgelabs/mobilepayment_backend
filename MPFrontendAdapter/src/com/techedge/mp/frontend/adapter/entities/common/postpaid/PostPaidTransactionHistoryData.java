package com.techedge.mp.frontend.adapter.entities.common.postpaid;

import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;

public class PostPaidTransactionHistoryData extends PostPaidTransactionJsonData {

    private CustomTimestamp date;
    private CustomTimestamp systemDate;
    private String          statusTitle;
    private String          statusDescription;

    public CustomTimestamp getDate() {
        return date;
    }

    public void setDate(CustomTimestamp date) {
        this.date = date;
    }

    public CustomTimestamp getSystemDate() {
        return systemDate;
    }

    public void setSystemDate(CustomTimestamp systemDate) {
        this.systemDate = systemDate;
    }

    public String getStatusTitle() {
        return statusTitle;
    }

    public void setStatusTitle(String statusTitle) {
        this.statusTitle = statusTitle;
    }

    public String getStatusDescription() {
        return statusDescription;
    }

    public void setStatusDescription(String statusDescription) {
        this.statusDescription = statusDescription;
    }

}
