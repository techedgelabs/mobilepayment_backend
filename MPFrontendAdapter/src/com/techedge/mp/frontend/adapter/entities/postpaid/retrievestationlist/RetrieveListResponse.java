package com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveListResponse extends BaseResponse {

	
	private RetrieveBodyResponse body;
	
	public synchronized RetrieveBodyResponse getBody() {
		return body;
	}

	public synchronized void setBody(RetrieveBodyResponse body) {
		this.body = body;
	}
	
	
	
}
