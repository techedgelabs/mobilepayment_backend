package com.techedge.mp.frontend.adapter.entities.user.v2.authentication;

public class AuthenticationUserBodyResponse {

	private String ticketID;
	private AuthenticationUserDataResponse userData;
	
	public String getTicketID() {
		return ticketID;
	}

	public void setTicketID(String ticketID) {
		this.ticketID = ticketID;
	}

	public AuthenticationUserDataResponse getUserData() {
		return userData;
	}
	
	public void setUserData(AuthenticationUserDataResponse userData) {
		this.userData = userData;
	}
}
