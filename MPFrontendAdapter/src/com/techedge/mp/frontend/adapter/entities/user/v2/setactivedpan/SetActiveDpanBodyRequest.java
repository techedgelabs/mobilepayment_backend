package com.techedge.mp.frontend.adapter.entities.user.v2.setactivedpan;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class SetActiveDpanBodyRequest implements Validable {

    private String dpan;
    private String status;

    public String getDpan() {
        return dpan;
    }

    public void setDpan(String dpan) {
        this.dpan = dpan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.dpan == null || this.dpan.isEmpty()) {

            status.setStatusCode(StatusCode.USER_SET_ACTIVE_DPAN_WRONG);

            return status;
        }
        
        if (this.status == null || this.status.isEmpty()) {

            status.setStatusCode(StatusCode.USER_SET_ACTIVE_DPAN_WRONG);

            return status;
        }

        status.setStatusCode(StatusCode.USER_SET_ACTIVE_DPAN_SUCCESS);

        return status;

    }

}
