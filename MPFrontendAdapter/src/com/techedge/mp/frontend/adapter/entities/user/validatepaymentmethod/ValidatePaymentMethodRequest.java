package com.techedge.mp.frontend.adapter.entities.user.validatepaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ValidatePaymentMethodRequest extends AbstractRequest implements Validable {

    private Status                           status = new Status();

    private Credential                       credential;
    private ValidatePaymentMethodBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ValidatePaymentMethodBodyRequest getBody() {
        return body;
    }

    public void setBody(ValidatePaymentMethodBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("VALID-PAY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        ValidatePaymentMethodRequest validatePaymentMethodRequest = this;

        com.techedge.mp.core.business.interfaces.ValidatePaymentMethodResponse validatePaymentMethodCoreResponse = getUserServiceRemote().validatePaymentMethod(
                validatePaymentMethodRequest.getCredential().getTicketID(), validatePaymentMethodRequest.getCredential().getRequestID(),
                validatePaymentMethodRequest.getBody().getPaymentMethod().getId(), validatePaymentMethodRequest.getBody().getPaymentMethod().getType(),
                AmountConverter.toInternal(validatePaymentMethodRequest.getBody().getVerificationAmount()));

        status.setStatusCode(validatePaymentMethodCoreResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(validatePaymentMethodCoreResponse.getStatusCode()));

        ValidatePaymentMethodResponse validatePaymentMethodResponse = new ValidatePaymentMethodResponse();

        ValidatePaymentMethodBodyResponse validatePaymentMethodBodyResponse = new ValidatePaymentMethodBodyResponse();
        validatePaymentMethodBodyResponse.setAttemptsLeft(validatePaymentMethodCoreResponse.getAttemptsLeft());

        validatePaymentMethodResponse.setBody(validatePaymentMethodBodyResponse);
        validatePaymentMethodResponse.setStatus(status);

        return validatePaymentMethodResponse;
    }

}
