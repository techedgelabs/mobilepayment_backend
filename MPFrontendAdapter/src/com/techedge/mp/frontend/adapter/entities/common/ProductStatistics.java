package com.techedge.mp.frontend.adapter.entities.common;

public class ProductStatistics {

    private String  productDescription;
    private Integer quantity;
    private Integer previousQuantity;
    private Integer amount;
    private Integer previousAmount;

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getPreviousQuantity() {
        return previousQuantity;
    }

    public void setPreviousQuantity(Integer previousQuantity) {
        this.previousQuantity = previousQuantity;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Integer getPreviousAmount() {
        return previousAmount;
    }

    public void setPreviousAmount(Integer previousAmount) {
        this.previousAmount = previousAmount;
    }

}
