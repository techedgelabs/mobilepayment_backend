package com.techedge.mp.frontend.adapter.entities.common.v2;

import java.io.Serializable;

public class ParkingCity implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2643222065739261611L;
    /**
     * 
     */
    

    private String            id;
    private String            cityId;
    private String            name;
    private String            administrativeAreaLevel2Code;
    private double            latitude;
    private double            longitude;
    private Boolean           stickerRequired;
    private String            stickerRequiredNotice;
    private String            url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAdministrativeAreaLevel2Code() {
        return administrativeAreaLevel2Code;
    }

    public void setAdministrativeAreaLevel2Code(String administrativeAreaLevel2Code) {
        this.administrativeAreaLevel2Code = administrativeAreaLevel2Code;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public Boolean getStickerRequired() {
        return stickerRequired;
    }

    public void setStickerRequired(Boolean stickerRequired) {
        this.stickerRequired = stickerRequired;
    }

    public String getStickerRequiredNotice() {
        return stickerRequiredNotice;
    }

    public void setStickerRequiredNotice(String stickerRequiredNotice) {
        this.stickerRequiredNotice = stickerRequiredNotice;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    

}
