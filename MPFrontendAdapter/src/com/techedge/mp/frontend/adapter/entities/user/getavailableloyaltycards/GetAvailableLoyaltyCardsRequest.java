package com.techedge.mp.frontend.adapter.entities.user.getavailableloyaltycards;

import com.techedge.mp.core.business.interfaces.GetAvailableLoyaltyCardsData;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.utilities.LoyaltyCardTypeConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.LoyaltyCardData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetAvailableLoyaltyCardsRequest extends AbstractRequest implements Validable {

    private Status                              status = new Status();

    private Credential                          credential;
    private GetAvailableLoyaltyCardsBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GetAvailableLoyaltyCardsBodyRequest getBody() {
        return body;
    }

    public void setBody(GetAvailableLoyaltyCardsBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-AVAILABLE-LOYALTY-CARDS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        GetAvailableLoyaltyCardsRequest getAvailableLoyaltyCardsRequest = this;

        GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = getUserServiceRemote().getAvailableLoyaltyCards(getAvailableLoyaltyCardsRequest.getCredential().getTicketID(),
                getAvailableLoyaltyCardsRequest.getCredential().getRequestID(), getAvailableLoyaltyCardsRequest.getBody().getFiscalcode());

        GetAvailableLoyaltyCardsResponse getAvailableLoyaltyCardsResponse = new GetAvailableLoyaltyCardsResponse();
        GetAvailableLoyaltyCardsBodyResponse getAvailableLoyaltyCardsBodyResponse = new GetAvailableLoyaltyCardsBodyResponse();

        status.setStatusCode(getAvailableLoyaltyCardsData.getStatusCode());
        status.setStatusMessage(prop.getProperty(getAvailableLoyaltyCardsData.getStatusCode()));

        if (status.getStatusCode().equals(StatusCode.USER_GET_AVAILABLE_LOYALTY_CARDS_SUCCESS)) {
            getAvailableLoyaltyCardsBodyResponse.setBalance(getAvailableLoyaltyCardsData.getBalance());

            for (LoyaltyCard loyaltyCard : getAvailableLoyaltyCardsData.getLoyaltyCards()) {
                LoyaltyCardData loyaltyCardData = new LoyaltyCardData();
                loyaltyCardData.setId(loyaltyCard.getId());
                loyaltyCardData.setEanCode(loyaltyCard.getEanCode());
                loyaltyCardData.setPanCode(loyaltyCard.getPanCode());
                loyaltyCardData.setType(LoyaltyCardTypeConverter.toFrontendAdapter(loyaltyCard.getType()));
                loyaltyCardData.setDefaultCard(loyaltyCard.getDefaultCard());
                getAvailableLoyaltyCardsBodyResponse.getCards().add(loyaltyCardData);
            }

            getAvailableLoyaltyCardsResponse.setBody(getAvailableLoyaltyCardsBodyResponse);
        }

        getAvailableLoyaltyCardsResponse.setStatus(status);

        return getAvailableLoyaltyCardsResponse;

    }

}
