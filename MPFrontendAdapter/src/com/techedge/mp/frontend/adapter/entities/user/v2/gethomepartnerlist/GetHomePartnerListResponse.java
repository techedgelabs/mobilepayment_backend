package com.techedge.mp.frontend.adapter.entities.user.v2.gethomepartnerlist;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetHomePartnerListResponse extends BaseResponse {
    
    private GetHomePartnerListBodyResponse body;

    public GetHomePartnerListBodyResponse getBody() {
        return body;
    }

    public void setBody(GetHomePartnerListBodyResponse body) {
        this.body = body;
    }

}
