package com.techedge.mp.frontend.adapter.entities.user.managepayment;



public class ManagePaymentBodyResponse {

	
	private String shopLogin;
	private String secureString;
	
	
	public String getShopLogin() {
		return shopLogin;
	}
	public void setShopLogin(String shopLogin) {
		this.shopLogin = shopLogin;
	}
	
	public String getSecureString() {
		return secureString;
	}
	public void setSecureString(String secureString) {
		this.secureString = secureString;
	}
	
}
