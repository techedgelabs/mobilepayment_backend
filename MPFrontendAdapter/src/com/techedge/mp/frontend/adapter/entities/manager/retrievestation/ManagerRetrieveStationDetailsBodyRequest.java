package com.techedge.mp.frontend.adapter.entities.manager.retrievestation;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ManagerRetrieveStationDetailsBodyRequest implements Validable {

	private String stationID;
    
    
	public String getStationID() {
		return stationID;
	}
	public void setStationID(String stationID) {
		this.stationID = stationID;
	}
	
	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.stationID == null || this.stationID.trim() == "") {
			
			status.setStatusCode(ResponseHelper.MANAGER_STATION_INVALID_STATION_ID);
			
			return status;
		}
		
        status.setStatusCode(ResponseHelper.MANAGER_STATION_SUCCESS);       
        return status;
	}
}
