package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.Cash;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptDynamic;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;


public class RetrieveTransactionDetailBodyResponse {

	private String sourceType;
	private String sourceStatus;
    private ReceiptDynamic receipt;
    private Station station;
    //private Pump pump;
    private Cash cash;
    private PostPaidTransactionHistoryData transaction;
	
    public String getSourceType() {
		return sourceType;
	}
	public void setSourceType(String sourceType) {
		this.sourceType = sourceType;
	}
	
	public String getSourceStatus() {
		return sourceStatus;
	}
	public void setSourceStatus(String sourceStatus) {
		this.sourceStatus = sourceStatus;
	}
	
	public ReceiptDynamic getReceipt() {
		return receipt;
	}
	public void setReceipt(ReceiptDynamic receipt) {
		this.receipt = receipt;
	}
	
	public Station getStation() {
		return station;
	}
	public void setStation(Station station) {
		this.station = station;
	}
	
	/*public Pump getPump() {
		return pump;
	}
	public void setPump(Pump pump) {
		this.pump = pump;
	}*/
	
	public Cash getCash() {
		return cash;
	}
	public void setCash(Cash cash) {
		this.cash = cash;
	}
	
	public PostPaidTransactionHistoryData getTransaction() {
		return transaction;
	}
	public void setTransaction(PostPaidTransactionHistoryData transaction) {
		this.transaction = transaction;
	}
}