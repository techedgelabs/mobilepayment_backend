package com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ManagerRetrieveTransactionsBodyRequest implements Validable {

    private String  stationID;
    private Integer pumpMaxTransactions;

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public Integer getPumpMaxTransactions() {
        return pumpMaxTransactions;
    }

    public void setPumpMaxTransactions(Integer pumpMaxTransactions) {
        this.pumpMaxTransactions = pumpMaxTransactions;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.stationID == null || this.stationID.trim() == "") {

            status.setStatusCode(StatusCode.MANAGER_RETRIEVE_TRANSACTIONS_INVALID_PARAMETERS);

            return status;
        }

        if (this.pumpMaxTransactions == null) {

            status.setStatusCode(StatusCode.MANAGER_RETRIEVE_TRANSACTIONS_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.MANAGER_RETRIEVE_VOUCHERS_SUCCESS);
        return status;
    }
}
