package com.techedge.mp.frontend.adapter.entities.refuel.v2.createmulticardtransaction;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class CreateMulticardRefuelTransactionResponse extends BaseResponse {

	private CreateMulticardRefuelTransactionBodyResponse body;

	public CreateMulticardRefuelTransactionBodyResponse getBody() {
		return body;
	}
	public void setBody(CreateMulticardRefuelTransactionBodyResponse body) {
		this.body = body;
	}
}
