package com.techedge.mp.frontend.adapter.entities.common;

import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class SecurityData extends EmailSecurityData implements Validable {
	
	private String password;
	
	
	public SecurityData(){}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public Status check() {
		
		Status status = super.check();
		
		if (!Validator.isValid(status.getStatusCode())) {
			
			return status;
		}
		
		status = Validator.checkPassword("CREATE", password);
		
		if (!Validator.isValid(status.getStatusCode())) {
			
			return status;
		}
		
		if(password.equals(getEmail())) {
			
			status.setStatusCode(StatusCode.USER_CREATE_USERNAME_PASSWORD_EQUALS);
			
			return status;
		}
		
				
		status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);
		
		return status;
	}
	
	
}
