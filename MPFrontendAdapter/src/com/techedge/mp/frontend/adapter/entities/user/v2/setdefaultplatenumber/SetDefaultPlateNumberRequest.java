package com.techedge.mp.frontend.adapter.entities.user.v2.setdefaultplatenumber;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class SetDefaultPlateNumberRequest extends AbstractRequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private SetDefaultPlateNumberBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public SetDefaultPlateNumberBodyRequest getBody() {
        return body;
    }

    public void setBody(SetDefaultPlateNumberBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("SET-DEFAULT-PLATE-NUMBER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        
        SetDefaultPlateNumberRequest setDefaultPlateNumberRequest = this;
        SetDefaultPlateNumberResponse setDefaultPlateNumberResponse = new SetDefaultPlateNumberResponse();

        String response = getUserV2ServiceRemote().setDefaultPlateNumber(setDefaultPlateNumberRequest.getCredential().getRequestID(),
                setDefaultPlateNumberRequest.getCredential().getTicketID(), setDefaultPlateNumberRequest.getBody().getId());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        setDefaultPlateNumberResponse.setStatus(status);

        return setDefaultPlateNumberResponse;
    }

}
