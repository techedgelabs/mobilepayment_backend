package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.v2.PlateNumberInfo;

public class UserData {

	private String firstName;
	private String lastName;
	private String fiscalCode;
	private CustomDate dateOfBirth;
	private String birthMunicipality;
	private String birthProvince;
	private String language;
	private String sex;
	private AddressData addressData;
	private AddressData billingAddressData;
	private List<SocialData> socialDataList = new ArrayList<SocialData>(0);
	private List<PlateNumberInfo> plateNumberList = new ArrayList<PlateNumberInfo>(0);
    private String  source;
    private String externalUserId;
    private String contactKey;

	public UserData() {
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public CustomDate getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(CustomDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getBirthMunicipality() {
		return birthMunicipality;
	}

	public void setBirthMunicipality(String birthMunicipality) {
		this.birthMunicipality = birthMunicipality;
	}

	public String getBirthProvince() {
		return birthProvince;
	}

	public void setBirthProvince(String birthProvince) {
		this.birthProvince = birthProvince;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public AddressData getAddressData() {
		return addressData;
	}

	public void setAddressData(AddressData addressData) {
		this.addressData = addressData;
	}

	public AddressData getBillingAddressData() {
		return billingAddressData;
	}

	public void setBillingAddressData(AddressData billingAddressData) {
		this.billingAddressData = billingAddressData;
	}

	public List<SocialData> getSocialDataList() {
		return socialDataList;
	}

	public void setSocialDataList(List<SocialData> socialDataList) {
		this.socialDataList = socialDataList;
	}

    public List<PlateNumberInfo> getPlateNumberList() {
        return plateNumberList;
    }

    public void setPlateNumberList(List<PlateNumberInfo> plateNumberList) {
        this.plateNumberList = plateNumberList;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getExternalUserId() {
        return externalUserId;
    }

    public void setExternalUserId(String externalUserId) {
        this.externalUserId = externalUserId;
    }

    public String getContactKey() {
        return contactKey;
    }

    public void setContactKey(String contactKey) {
        this.contactKey = contactKey;
    }
    
}
