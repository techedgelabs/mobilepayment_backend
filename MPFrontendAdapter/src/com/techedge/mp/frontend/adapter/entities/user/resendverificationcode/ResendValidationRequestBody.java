package com.techedge.mp.frontend.adapter.entities.user.resendverificationcode;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ResendValidationRequestBody implements Validable {

    private String id;
    private String type;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (id == null || (id.isEmpty()) || id.equals("0")) {

            status.setStatusCode(StatusCode.USER_RESEND_VALIDATATION_INVALID_PARAMETERS);
            return status;

        }

        if (type == null || type.isEmpty()) {

            status.setStatusCode(StatusCode.USER_RESEND_VALIDATATION_INVALID_PARAMETERS);
            return status;

        }

        status.setStatusCode(StatusCode.USER_RESEND_VALIDATATION_SUCCESS);

        return status;

    }
}
