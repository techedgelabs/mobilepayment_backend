package com.techedge.mp.frontend.adapter.entities.manager.retrievetransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveTransactionDetailBodyRequest implements Validable {

	private String transactionID;

	public String getTransactionId() {
		return transactionID;
	}
	public void setTransactionId(String mpCode) {
		this.transactionID = mpCode;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.transactionID == null || this.transactionID.length() != 32 || this.transactionID.trim() == "") {
			
			status.setStatusCode(StatusCode.POP_DETAIL_ID_WRONG);
			
			return status;			
		}
		
		status.setStatusCode(StatusCode.POP_DETAIL_SUCCESS);
		
		return status;
	}
}