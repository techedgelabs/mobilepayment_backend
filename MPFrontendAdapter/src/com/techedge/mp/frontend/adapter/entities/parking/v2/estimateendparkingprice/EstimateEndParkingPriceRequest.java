package com.techedge.mp.frontend.adapter.entities.parking.v2.estimateendparkingprice;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.EstimateEndParkingPriceResult;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZonePriceInfo;
import com.techedge.mp.frontend.adapter.entities.common.v2.PlateNumberInfo;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class EstimateEndParkingPriceRequest extends AbstractRequest implements Validable {

    private Status                          status = new Status();

    private Credential                      credential;
    private EstimateEndParkingPriceRequestBody body;

    public EstimateEndParkingPriceRequestBody getBody() {
        return body;
    }

    public void setBody(EstimateEndParkingPriceRequestBody body) {
        this.body = body;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_AUTH_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        EstimateEndParkingPriceRequest estimateEndParkingPriceRequest = this;
        
        EstimateEndParkingPriceRequestBody estimateEndParkingPriceRequestBody = estimateEndParkingPriceRequest.getBody();
        
        EstimateEndParkingPriceResult result = getParkingTransactionV2ServiceRemote().estimateEndParkingPrice(estimateEndParkingPriceRequest.getCredential().getTicketID(),
                estimateEndParkingPriceRequest.getCredential().getRequestID(), estimateEndParkingPriceRequestBody.getTransactionID());

        EstimateEndParkingPriceResponse response = new EstimateEndParkingPriceResponse();
        
        EstimateEndParkingPriceResponseBody responseBody = new EstimateEndParkingPriceResponseBody();
        
        ParkingZonePriceInfo parkingZonePriceInfo = new ParkingZonePriceInfo();

        if (result != null && result.getStatusCode().equals(ResponseHelper.PARKING_ESTIMATE_END_PARKING_PRICE_SUCCESS)) {
            
            PlateNumberInfo plateNumberInfo = new PlateNumberInfo();
            plateNumberInfo.setPlateNumber(result.getPlateNumber());
            plateNumberInfo.setDescription(result.getPlateNumberDescription());
            parkingZonePriceInfo.setPlateNumber(plateNumberInfo);
            
            parkingZonePriceInfo.setPrice(AmountConverter.toMobile(result.getPrice()));
            parkingZonePriceInfo.setStallCode(result.getStallCode());
            parkingZonePriceInfo.setNotice(result.getNotice());
            
            ParkingZone parkingZone = new ParkingZone();
            parkingZone.setId(result.getParkingZoneId());
            parkingZone.setName(result.getParkingZoneName());
            parkingZone.setDescription(result.getParkingZoneDescription());
            parkingZone.setCityId(result.getCityId());
            parkingZone.setCityName(result.getCityName());
            parkingZone.setAdministrativeAreaLevel2Code(result.getAdministrativeAreaLevel2Code());
            parkingZonePriceInfo.setParkingZone(parkingZone);
            
            if (result.getCurrentPrice() != null) {
                parkingZonePriceInfo.setCurrentPrice(result.getCurrentPrice().intValue());
            }
            if (result.getPriceDifference() != null) {
                parkingZonePriceInfo.setPriceDifference(result.getPriceDifference().intValue());
            }
            
            parkingZonePriceInfo.setParkingStartTime(CustomTimestamp.convertToCustomTimestamp(result.getParkingStartTime()));
            parkingZonePriceInfo.setParkingEndTime(CustomTimestamp.convertToCustomTimestamp(result.getParkingEndTime()));
            parkingZonePriceInfo.setTransactionID(result.getTransactionId());
    
            PaymentMethod paymentMethod = new PaymentMethod();
            paymentMethod.setId(result.getPaymentMethodId());
            parkingZonePriceInfo.setPaymentMethod(paymentMethod);
            
            responseBody.setParkingZonePriceInfo(parkingZonePriceInfo);
            
            response.setBody(responseBody);
        }
        
        Status status = new Status();
        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(prop.getProperty(result.getStatusCode()));
        
        response.setStatus(status);

        return response;

    }

}
