package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.refuel.cancel.CancelRefuelRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.confirm.ConfirmRefuelRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction.CreateApplePayRefuelTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.createtransaction.CreateRefuelTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail.RetrieveRefuelPaymentDetailRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory.RetrieveRefuelPaymentHistoryRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievepending.RetrievePendingRefuelRequest;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationRequest;

public class RefuelRequest extends RootRequest {

    protected CancelRefuelRequest                    cancelRefuel;
    protected ConfirmRefuelRequest                   confirmRefuel;
    protected CreateRefuelTransactionRequest         createRefuelTransaction;
    protected RetrieveRefuelPaymentDetailRequest     retrieveRefuelPaymentDetail;
    protected RetrieveRefuelPaymentHistoryRequest    retrieveRefuelPaymentHistory;
    protected RetrievePendingRefuelRequest           retrievePendingRefuel;
    protected RetrieveStationRequest                 retrieveStation;
    protected CreateApplePayRefuelTransactionRequest createApplePayRefuelTransaction;

    public CancelRefuelRequest getCancelRefuel() {
        return cancelRefuel;
    }

    public void setCancelRefuel(CancelRefuelRequest cancelRefuel) {
        this.cancelRefuel = cancelRefuel;
    }

    public ConfirmRefuelRequest getConfirmRefuel() {
        return confirmRefuel;
    }

    public void setConfirmRefuel(ConfirmRefuelRequest confirmRefuel) {
        this.confirmRefuel = confirmRefuel;
    }

    public CreateRefuelTransactionRequest getCreateRefuelTransaction() {
        return createRefuelTransaction;
    }

    public void setCreateRefuelTransaction(CreateRefuelTransactionRequest createRefuelTransaction) {
        this.createRefuelTransaction = createRefuelTransaction;
    }

    public RetrieveRefuelPaymentDetailRequest getRetrieveRefuelPaymentDetail() {
        return retrieveRefuelPaymentDetail;
    }

    public void setRetrieveRefuelPaymentDetail(RetrieveRefuelPaymentDetailRequest retrieveRefuelPaymentDetail) {
        this.retrieveRefuelPaymentDetail = retrieveRefuelPaymentDetail;
    }

    public RetrieveRefuelPaymentHistoryRequest getRetrieveRefuelPaymentHistory() {
        return retrieveRefuelPaymentHistory;
    }

    public void setRetrieveRefuelPaymentHistory(RetrieveRefuelPaymentHistoryRequest retrieveRefuelPaymentHistory) {
        this.retrieveRefuelPaymentHistory = retrieveRefuelPaymentHistory;
    }

    public RetrievePendingRefuelRequest getRetrievePendingRefuel() {
        return retrievePendingRefuel;
    }

    public void setRetrievePendingRefuel(RetrievePendingRefuelRequest retrievePendingRefuel) {
        this.retrievePendingRefuel = retrievePendingRefuel;
    }

    public RetrieveStationRequest getRetrieveStation() {
        return retrieveStation;
    }

    public void setRetrieveStation(RetrieveStationRequest retrieveStation) {
        this.retrieveStation = retrieveStation;
    }

    public CreateApplePayRefuelTransactionRequest getCreateApplePayRefuelTransaction() {
        return createApplePayRefuelTransaction;
    }

    public void setCreateApplePayRefuelTransaction(CreateApplePayRefuelTransactionRequest createApplePayRefuelTransaction) {
        this.createApplePayRefuelTransaction = createApplePayRefuelTransaction;
    }

}
