package com.techedge.mp.frontend.adapter.entities.common.v2;

public class PlateNumberInfo {

    private Long   id;
    private String plateNumber;
    private String description;
    private boolean defaultPlateNumber;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isDefaultPlateNumber() {
        return defaultPlateNumber;
    }

    public void setDefaultPlateNumber(boolean defaultPlateNumber) {
        this.defaultPlateNumber = defaultPlateNumber;
    }

}
