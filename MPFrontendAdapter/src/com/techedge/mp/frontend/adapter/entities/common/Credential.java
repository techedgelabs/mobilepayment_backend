package com.techedge.mp.frontend.adapter.entities.common;


public class Credential {
	
	
	private String ticketID;
	private String requestID;
	private String deviceID;
	private String loyaltySessionID;
	private String deviceName;
	
	public Credential(){}
	
	public String getTicketID() {
		return ticketID;
	}

	public void setTicketID(String ticketID) {
		this.ticketID = ticketID;
	}

    public String getLoyaltySessionID() {
        return loyaltySessionID;
    }

    public void setLoyaltySessionID(String loyaltySessionID) {
        this.loyaltySessionID = loyaltySessionID;
    }

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceId) {
        this.deviceID = deviceId;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}
