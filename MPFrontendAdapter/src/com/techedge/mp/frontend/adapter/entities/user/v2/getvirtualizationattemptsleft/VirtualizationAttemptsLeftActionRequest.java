package com.techedge.mp.frontend.adapter.entities.user.v2.getvirtualizationattemptsleft;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class VirtualizationAttemptsLeftActionRequest extends AbstractRequest {

    private Status                                      status = new Status();

    private Credential                                  credential;
    private VirtualizationAttemptsLeftActionRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public VirtualizationAttemptsLeftActionRequestBody getBody() {
        return body;
    }

    public void setBody(VirtualizationAttemptsLeftActionRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("VIRTUALIZATIONATTEMPTSLEFTACTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.USER_V2_GETLANDING_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        VirtualizationAttemptsLeftActionRequest virtualizationAttemptsLeftActionRequest = this;
        VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
        VirtualizationAttemptsLeftActionResponseBody virtualizationAttemptsLeftActionResponseBody = new VirtualizationAttemptsLeftActionResponseBody();

        String ticketId = virtualizationAttemptsLeftActionRequest.getCredential().getTicketID();
        String requestId = virtualizationAttemptsLeftActionRequest.getCredential().getRequestID();

        com.techedge.mp.core.business.interfaces.VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionData = getUserV2ServiceRemote().virtualizationAttemptsLeftAction(
                ticketId, requestId, virtualizationAttemptsLeftActionRequest.getBody().getAction());

        virtualizationAttemptsLeftActionResponseBody.setAttemptsLeft(virtualizationAttemptsLeftActionData.getAttemptsLeft());

        status.setStatusCode(virtualizationAttemptsLeftActionData.getStatusCode());
        status.setStatusMessage(prop.getProperty(virtualizationAttemptsLeftActionData.getStatusCode()));

        virtualizationAttemptsLeftActionResponse.setStatus(status);
        virtualizationAttemptsLeftActionResponse.setBody(virtualizationAttemptsLeftActionResponseBody);

        return virtualizationAttemptsLeftActionResponse;

    }

}
