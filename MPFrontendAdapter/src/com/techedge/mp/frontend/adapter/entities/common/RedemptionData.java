
package com.techedge.mp.frontend.adapter.entities.common;

import java.io.Serializable;


public class RedemptionData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5421158432655338995L;
    private Integer code;
    private String name;
    private Integer amount;
    private Integer points;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer value) {
        this.code = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer value) {
        this.amount = value;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer value) {
        this.points = value;
    }
    
}
