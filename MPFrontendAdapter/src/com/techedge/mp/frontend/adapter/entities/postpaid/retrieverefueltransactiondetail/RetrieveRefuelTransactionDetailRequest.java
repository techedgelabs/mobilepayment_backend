package com.techedge.mp.frontend.adapter.entities.postpaid.retrieverefueltransactiondetail;

import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Locale;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PostPaidCart;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCreditsVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidRefuel;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionAdditionalData;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.FrontendParameter;
import com.techedge.mp.frontend.adapter.entities.common.POPStatusConverter;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetail;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetailSection;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetailTransaction;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveRefuelTransactionDetailRequest extends AbstractRequest implements Validable {
    private Status                                     status = new Status();

    private Credential                                 credential;
    private RetrieveRefuelTransactionDetailBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveRefuelTransactionDetailBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveRefuelTransactionDetailBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIVE-REFUEL-TRANSACTION-DETAIL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;
            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
            return status;
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        RetrieveRefuelTransactionDetailRequest retrieveRefuelTransactionDetailRequest = this;
        RetrieveRefuelTransactionDetailResponse retrieveRefuelTransactionDetailResponse = new RetrieveRefuelTransactionDetailResponse();

        PostPaidGetTransactionDetailResponse postPaidGetTransactionDetailResponse = getPostPaidTransactionServiceRemote().getTransactionDetail(
                retrieveRefuelTransactionDetailRequest.getCredential().getRequestID(), retrieveRefuelTransactionDetailRequest.getCredential().getTicketID(),
                retrieveRefuelTransactionDetailRequest.getBody().getTransactionId());

        status.setStatusCode(postPaidGetTransactionDetailResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(postPaidGetTransactionDetailResponse.getStatusCode()));
        retrieveRefuelTransactionDetailResponse.setStatus(status);

        RetrieveRefuelTransactionDetailBodyResponse retrieveRefuelTransactionDetailBodyResponse = new RetrieveRefuelTransactionDetailBodyResponse();
        retrieveRefuelTransactionDetailBodyResponse.setSourceStatus(postPaidGetTransactionDetailResponse.getSourceStatus());
        retrieveRefuelTransactionDetailBodyResponse.setSourceType(postPaidGetTransactionDetailResponse.getSourceType());

        if (!status.getStatusCode().contains("200")) {
            retrieveRefuelTransactionDetailResponse.setStatus(status);
            return retrieveRefuelTransactionDetailResponse;
        }

        String iconCardUrl = "";
        String iconVoucherUrl = "";
        String iconLoyaltyUrl = "";

        try {
            iconCardUrl = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_RECEIPT_ICON_CARD_URL);
            iconVoucherUrl = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_RECEIPT_ICON_VOUCHER_URL);
            iconLoyaltyUrl = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_RECEIPT_ICON_LOYALTY_URL);
        }
        catch (ParameterNotFoundException ex) {
            getLoggerServiceRemote().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "PARAMETER NOT FOUND: " + ex.getMessage());
        }

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");

        retrieveRefuelTransactionDetailBodyResponse.setSourceStatus(postPaidGetTransactionDetailResponse.getSourceStatus());
        retrieveRefuelTransactionDetailBodyResponse.setSourceType(postPaidGetTransactionDetailResponse.getSourceType());

        Double voucherAmount = 0.0;
        Double bankAmount = 0.0;
        Double transactionAmount = 0.0;
        Integer amount = 0;

        if (postPaidGetTransactionDetailResponse.getAmount() != null) {
            amount = AmountConverter.toMobile(postPaidGetTransactionDetailResponse.getAmount());
            transactionAmount = postPaidGetTransactionDetailResponse.getAmount();
        }

        ReceiptTransactionDetailTransaction transaction = new ReceiptTransactionDetailTransaction();
        transaction.setAmount(amount);
        transaction.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(postPaidGetTransactionDetailResponse.getCreationTimestamp().getTime())));
        transaction.setTransactionID(postPaidGetTransactionDetailResponse.getMpTransactionID());
        transaction.setStatusTitle(postPaidGetTransactionDetailResponse.getStatusTitle());
        transaction.setStatusDescription(postPaidGetTransactionDetailResponse.getStatusDescription());

        // Trascodifica dello stato
        if (postPaidGetTransactionDetailResponse.getStatus() == null) {
            transaction.setStatus(null);
        }
        else {
            String localStatus = POPStatusConverter.toAppStatus(postPaidGetTransactionDetailResponse.getStatus());

            if (!localStatus.isEmpty()) {
                transaction.setStatus(localStatus);
            }
            else {
                transaction.setStatus(postPaidGetTransactionDetailResponse.getStatus());
            }
        }

        transaction.setSubStatus(postPaidGetTransactionDetailResponse.getSubStatus());

        retrieveRefuelTransactionDetailBodyResponse.setTransaction(transaction);

        retrieveRefuelTransactionDetailBodyResponse.setSurveyUrl(postPaidGetTransactionDetailResponse.getSurveyUrl());

        System.out.println("SURVEY URL settata: " + postPaidGetTransactionDetailResponse.getSurveyUrl());

        ReceiptTransactionDetail receipt = new ReceiptTransactionDetail();

        ReceiptTransactionDetailSection sectionRefuel = receipt.createSection("Rifornimento", "RECEIPT", null, false);
        ReceiptTransactionDetailSection sectionPromo = receipt.createSection("Messaggio Promozionale", "PROMO", null, true);
        ReceiptTransactionDetailSection sectionTransaction = receipt.createSection("Pagamento Carta", "RECEIPT", null, true);
        ReceiptTransactionDetailSection sectionMulticard = receipt.createSection("Pagamento Multicard", "RECEIPT", null, true);
        ReceiptTransactionDetailSection sectionVoucher = receipt.createSection("Pagamento Voucher", "RECEIPT", null, true);
        ReceiptTransactionDetailSection sectionLoyalty = receipt.createSection("You&Eni", "RECEIPT", null, true);
        ReceiptTransactionDetailSection sectionLoyaltyVoucher = receipt.createSection("", "RECEIPT", null, true);

        int sectionPositionRefuel = 1;
        int sectionPositionTransaction = 1;
        int sectionPositionPromo = 1;
        int sectionPositionVoucher = 1;
        int sectionPositionLoyalty = 1;
        int sectionPositionMulticard = 1;
        int sectionPositionLoyaltyVoucher = 1;

        String creationTimestamp = new SimpleDateFormat("dd/MM/yyyy").format(postPaidGetTransactionDetailResponse.getCreationTimestamp().getTime());
        receipt.addDataToSection(sectionRefuel, "DATE", "DATA", null, creationTimestamp, sectionPositionRefuel++, null);

        receipt.addDataToSection(sectionRefuel, "STATION_ID", "CODICE PUNTO VENDITA", null, postPaidGetTransactionDetailResponse.getStation().getStationID(),
                sectionPositionRefuel++, null);

        if (!postPaidGetTransactionDetailResponse.getCartList().isEmpty()) {
            for (PostPaidCart cart : postPaidGetTransactionDetailResponse.getCartList()) {
                String cartAmount = "0.0";
                if (cart.getAmount() != null) {
                    cartAmount = currencyFormat.format(cart.getAmount());
                }

                if (postPaidGetTransactionDetailResponse.getCashID() != null && !postPaidGetTransactionDetailResponse.getCashID().equals("")
                        && !postPaidGetTransactionDetailResponse.getCashID().equals(" ")) {
                    receipt.addDataToSection(sectionRefuel, "SHOP_CASH_ID", "COD CASSA", null, postPaidGetTransactionDetailResponse.getCashID(), sectionPositionRefuel++, null);
                }
                if (postPaidGetTransactionDetailResponse.getCashNumber() != null && !postPaidGetTransactionDetailResponse.getCashID().equals("")
                        && !postPaidGetTransactionDetailResponse.getCashID().equals(" ")) {
                    receipt.addDataToSection(sectionRefuel, "SHP_CASH_NUMBER", "N CASSA", null, postPaidGetTransactionDetailResponse.getCashNumber(), sectionPositionRefuel++, null);
                }
                receipt.addDataToSection(sectionRefuel, "SHOP_AMOUNT", "IMPORTO SHOP", "�", cartAmount, sectionPositionRefuel++, null);
            }
        }

        if (!postPaidGetTransactionDetailResponse.getRefuelList().isEmpty()) {
            for (PostPaidRefuel refuel : postPaidGetTransactionDetailResponse.getRefuelList()) {
                receipt.addDataToSection(sectionRefuel, "REFUEL_PUMP_NUMBER", "EROGATORE", null, refuel.getPumpNumber().toString(), sectionPositionRefuel++, null);

                String refuelQuantity = "0.0";
                if (refuel.getFuelQuantity() != null) {
                    refuelQuantity = numberFormat.format(refuel.getFuelQuantity().doubleValue());
                }
                receipt.addDataToSection(sectionRefuel, "REFUEL_FUEL_QUANTITY", "EROGATO", "lt", refuelQuantity, sectionPositionRefuel++, null);
                receipt.addDataToSection(sectionRefuel, "REFUEL_FUEL_TYPE", "TIPO DI CARBURANTE", null, refuel.getProductDescription(), sectionPositionRefuel++, null);

                String refuelAmount = "0.0";
                if (refuel.getFuelAmount() != null) {
                    refuelAmount = currencyFormat.format(refuel.getFuelAmount());
                }
                if (refuel.getFuelAmount() != null && refuel.getFuelAmount() > 0.0) {
                    receipt.addDataToSection(sectionRefuel, "REFUEL_AMOUNT", "IMPORTO CARBURANTE", "�", refuelAmount, sectionPositionRefuel++, null);
                }
                
                String refuelUnitPrice = "0.0";
                if (refuel.getUnitPrice() != null) {
                    refuelUnitPrice = currencyFormat.format(refuel.getUnitPrice());
                }
                if (refuel.getUnitPrice() != null && refuel.getUnitPrice() > 0.0) {
                    receipt.addDataToSection(sectionRefuel, "REFUEL_AMOUNT", "PREZZO PER LITRO", "�", refuelUnitPrice, sectionPositionRefuel++, null);
                }
            }

        }

        System.out.println("ACQUIRER_ID: " + postPaidGetTransactionDetailResponse.getAcquirerID());

        if (!postPaidGetTransactionDetailResponse.getPrePaidConsumeVoucherList().isEmpty()) {

            for (PrePaidConsumeVoucher prePaidConsumeVoucher : postPaidGetTransactionDetailResponse.getPrePaidConsumeVoucherList()) {
                if (prePaidConsumeVoucher.getOperationType().equals("CONSUME") && prePaidConsumeVoucher.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {

                    if (postPaidGetTransactionDetailResponse.getAcquirerID().equals("QUENIT")) {
                        receipt.addDataToSection(sectionVoucher, "N_OP", "N OPERAZIONE", null, postPaidGetTransactionDetailResponse.getBankTansactionID(),
                                sectionPositionVoucher++, null);
                        receipt.addDataToSection(sectionVoucher, "C_AUTH", "COD AUTORIZZAZIONE", null, postPaidGetTransactionDetailResponse.getAuthorizationCode(),
                                sectionPositionVoucher++, null);
                    }

                    for (PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetail : prePaidConsumeVoucher.getPrePaidConsumeVoucherDetail()) {
                        receipt.addDataToSection(sectionVoucher, "VOUCHER_NAME", "TIPO VOUCHER", null, prePaidConsumeVoucherDetail.getPromoDescription(), sectionPositionVoucher++,
                                null);

                        String consumedValue = "0.0";
                        if (prePaidConsumeVoucherDetail.getConsumedValue() != null) {
                            consumedValue = currencyFormat.format(prePaidConsumeVoucherDetail.getConsumedValue());
                            if (prePaidConsumeVoucherDetail.getConsumedValue() > 0.0) {
                                voucherAmount += prePaidConsumeVoucherDetail.getConsumedValue();
                            }
                        }

                        receipt.addDataToSection(sectionVoucher, "VOUCHER_AMOUNT", "IMPORTO", "�", consumedValue, sectionPositionVoucher++, null);
                    }

                    if (prePaidConsumeVoucher.getMarketingMsg() != null && !prePaidConsumeVoucher.getMarketingMsg().isEmpty()) {
                        receipt.addDataToSection(sectionPromo, "VOUCHER_MSG", "", null, prePaidConsumeVoucher.getMarketingMsg(), sectionPositionPromo++, null);
                    }

                }
            }
        }

        if (!postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList().isEmpty()) {

            for (PostPaidConsumeVoucher postPaidConsumeVoucher : postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList()) {
                if (postPaidConsumeVoucher.getOperationType().equals("CONSUME") && postPaidConsumeVoucher.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {

//                    if (postPaidGetTransactionDetailResponse.getAcquirerID().equals("QUENIT")) {
//                        receipt.addDataToSection(sectionVoucher, "N_OP", "N OPERAZIONE", null, postPaidGetTransactionDetailResponse.getBankTansactionID(),
//                                sectionPositionVoucher++, null);
//                        receipt.addDataToSection(sectionVoucher, "C_AUTH", "COD AUTORIZZAZIONE", null, postPaidGetTransactionDetailResponse.getAuthorizationCode(),
//                                sectionPositionVoucher++, null);
//                    }

                    for (PostPaidConsumeVoucherDetail postPaidConsumeVoucherDetail : postPaidConsumeVoucher.getPostPaidConsumeVoucherDetail()) {
                        receipt.addDataToSection(sectionVoucher, "VOUCHER_NAME", "TIPO VOUCHER", null, postPaidConsumeVoucherDetail.getPromoDescription(),
                                sectionPositionVoucher++, null);

                        String consumedValue = "0.0";
                        if (postPaidConsumeVoucherDetail.getConsumedValue() != null) {
                            consumedValue = currencyFormat.format(postPaidConsumeVoucherDetail.getConsumedValue());
                            if (postPaidConsumeVoucherDetail.getConsumedValue() > 0.0) {
                                voucherAmount += postPaidConsumeVoucherDetail.getConsumedValue();
                            }
                        }
                        receipt.addDataToSection(sectionVoucher, "VOUCHER_AMOUNT", "IMPORTO", "�", consumedValue, sectionPositionVoucher++, null);
                    }

                    if (postPaidConsumeVoucher.getMarketingMsg() != null && !postPaidConsumeVoucher.getMarketingMsg().isEmpty()) {
                        receipt.addDataToSection(sectionPromo, "VOUCHER_MSG", "", null, postPaidConsumeVoucher.getMarketingMsg(), sectionPositionPromo++, null);
                    }

                }
            }
        }

        if (!postPaidGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList().isEmpty()) {
            for (PostPaidLoadLoyaltyCredits loyaltyCredits : postPaidGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList()) {
                String panCode = null;
                String credits = "0";
                String balanceAmount = "0";

                if (loyaltyCredits.getEanCode() != null && !loyaltyCredits.getEanCode().isEmpty()) {
                    panCode = loyaltyCredits.getEanCode();
                }

                if (loyaltyCredits.getCredits() != null) {
                    credits = loyaltyCredits.getCredits().toString();
                }

                if (loyaltyCredits.getBalance() != null) {
                    balanceAmount = loyaltyCredits.getBalance().toString();
                }

                if (panCode != null) {
                    //receipt.addDataToSection(sectionLoyalty, "LOYALTY_PAN", "CODICE", null, loyaltyCredits.getEanCode(), sectionPositionLoyalty++, null);
                    receipt.addDataToSection(sectionLoyalty, "LOYALTY_BALANCE", "PUNTI", null, credits, sectionPositionLoyalty++, null);
                    receipt.addDataToSection(sectionLoyalty, "LOYALTY_TOTAL", "SALDO PUNTI", null, balanceAmount, sectionPositionLoyalty++, null);
                }

                if (loyaltyCredits.getMarketingMsg() != null) {
                    String marketingMsg = loyaltyCredits.getMarketingMsg().replaceAll("[\\s\\t\\n\\r ]{2,}", " ");
                    receipt.addDataToSection(sectionPromo, "LOYALTY_MSG", "", null, marketingMsg, sectionPositionPromo++, null);
                }
                else {
                    if (loyaltyCredits.getStatusCode().equals(FidelityResponse.ENABLE_LOYALTY_CARD_MIGRATION_ERROR)
                            || loyaltyCredits.getStatusCode().equals(FidelityResponse.ENABLE_LOYALTY_CARD_MIGRATION_ERROR)) {

                        receipt.addDataToSection(sectionPromo, "LOYALTY_MSG", "", null, loyaltyCredits.getMarketingMsg(), sectionPositionPromo++, null);
                    }
                }

                if (loyaltyCredits.getWarningMsg() != null) {
                    String warningMsg = loyaltyCredits.getWarningMsg().replaceAll("[\\s\\t\\n\\r ]{2,}", " ");
                    receipt.addDataToSection(sectionLoyalty, "LOYALTY_MSG", "", null, warningMsg, sectionPositionLoyalty++, null);
                }

                if (!loyaltyCredits.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {
                    if (loyaltyCredits.getMessageCode() != null && !loyaltyCredits.getStatusCode().equals(StatusHelper.LOYALTY_STATUS_TO_BE_VERIFIED)) {
                        receipt.addDataToSection(sectionLoyalty, "LOYALTY_MSG", "", null, loyaltyCredits.getMessageCode(), sectionPositionLoyalty++, null);
                    }

                    /*if (loyaltyCredits.getWarningMsg() != null) {
                        receipt.addDataToSection(sectionLoyalty, "LOYALTY_MSG", "", null, loyaltyCredits.getWarningMsg(), sectionPositionLoyalty++, null);
                    }*/
                }
                
                if (!loyaltyCredits.getPostPaidLoadLoyaltyCreditsVoucherList().isEmpty()) {
                    for (PostPaidLoadLoyaltyCreditsVoucher postPaidLoadLoyaltyCreditsVoucher : loyaltyCredits.getPostPaidLoadLoyaltyCreditsVoucherList()) {
                        
                        receipt.addDataToSection(sectionLoyaltyVoucher, "LOYALTY_VOUCHER_CODE", "", null, postPaidLoadLoyaltyCreditsVoucher.getCode(), sectionPositionLoyaltyVoucher++, null);
                    }
                }
            }
        }

        bankAmount = transactionAmount - voucherAmount;
        String bankAmountString = currencyFormat.format(bankAmount);
        //String creationTimestamp = new SimpleDateFormat("dd/MM/yyyy").format(postPaidGetTransactionDetailResponse.getCreationTimestamp().getTime());
        //receipt.addDataToSection(sectionTransaction, "DATE", "DATA", null, creationTimestamp, sectionPositionTransaction++, null);

        receipt.addDataToSection(sectionTransaction, "AMOUNT", "IMPORTO ADDEBITATO", "�", bankAmountString, sectionPositionTransaction++, null);
        receipt.addDataToSection(sectionTransaction, "PAN", "CARTA DI PAGAMENTO", null, postPaidGetTransactionDetailResponse.getPanCode(), sectionPositionTransaction++, null);
        receipt.addDataToSection(sectionTransaction, "N_OP", "N OPERAZIONE", null, postPaidGetTransactionDetailResponse.getBankTansactionID(), sectionPositionTransaction++, null);
        receipt.addDataToSection(sectionTransaction, "C_AUTH", "COD AUTORIZZAZIONE", null, postPaidGetTransactionDetailResponse.getAuthorizationCode(),
                sectionPositionTransaction++, null);
        
        
        receipt.addDataToSection(sectionMulticard, "AMOUNT", "IMPORTO ADDEBITATO", "�", bankAmountString, sectionPositionMulticard++, null);
        //receipt.addDataToSection(sectionMulticard, "PAN", "MULTICARD", null, postPaidGetTransactionDetailResponse.getPanCode(), sectionPositionMulticard++, null);
        receipt.addDataToSection(sectionMulticard, "N_OP", "N OPERAZIONE", null, postPaidGetTransactionDetailResponse.getBankTansactionID(), sectionPositionMulticard++, null);
        receipt.addDataToSection(sectionMulticard, "C_AUTH", "COD AUTORIZZAZIONE", null, postPaidGetTransactionDetailResponse.getAuthorizationCode(),
                sectionPositionMulticard++, null);
        
        if (!postPaidGetTransactionDetailResponse.getPostPaidTransactionAdditionalDataList().isEmpty()) {
            for (PostPaidTransactionAdditionalData postPaidTransactionAdditionalData : postPaidGetTransactionDetailResponse.getPostPaidTransactionAdditionalDataList()) {
                String dataKey   = postPaidTransactionAdditionalData.getDataKey();
                String dataValue = postPaidTransactionAdditionalData.getDataValue();
                if (dataKey.equalsIgnoreCase("merchantAddress") ||
                    dataKey.equalsIgnoreCase("cardType") ||
                    dataKey.equalsIgnoreCase("merchantName") ||
                    dataKey.equalsIgnoreCase("cardExpDt") ||
                    dataKey.equalsIgnoreCase("productIDListString")) {
                }
                else {
                    
                    String dataLabel = dataKey;
                    if (dataKey.equalsIgnoreCase("mcMaskedPan")) {
                        dataLabel = "PAN";
                    }
                    else if (dataKey.equalsIgnoreCase("addInfo")) {
                        dataLabel = "INFO";
                    }
                    else if (dataKey.equalsIgnoreCase("km")) {
                        dataLabel = "KM";
                    }
                    
                    receipt.addDataToSection(sectionMulticard, dataKey, dataLabel, null, dataValue, sectionPositionMulticard++, null);
                }
            }
        }
        
        

        if (receipt.getSectionDataSize(sectionPromo) == 0) {
            receipt.removeSection(sectionPromo);
        }

        if (receipt.getSectionDataSize(sectionLoyalty) == 0) {
            receipt.removeSection(sectionLoyalty);
        }

        if (postPaidGetTransactionDetailResponse.getAcquirerID().equals("QUENIT") || postPaidGetTransactionDetailResponse.getAcquirerID().equals("MULTICARD") || bankAmount == 0.0) {
            receipt.removeSection(sectionTransaction);
        }

        if (voucherAmount == 0.0) {
            receipt.removeSection(sectionVoucher);
        }
        
        if (!postPaidGetTransactionDetailResponse.getAcquirerID().equals("MULTICARD") || bankAmount == 0.0) {
            receipt.removeSection(sectionMulticard);
        }
        
        if (receipt.getSectionDataSize(sectionLoyaltyVoucher) == 0) {
            receipt.removeSection(sectionLoyaltyVoucher);
        }

        retrieveRefuelTransactionDetailBodyResponse.setReceipt(receipt);
        retrieveRefuelTransactionDetailResponse.setBody(retrieveRefuelTransactionDetailBodyResponse);
        retrieveRefuelTransactionDetailResponse.setStatus(status);

        return retrieveRefuelTransactionDetailResponse;
    }
}