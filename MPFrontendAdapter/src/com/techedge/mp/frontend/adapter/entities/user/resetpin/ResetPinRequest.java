package com.techedge.mp.frontend.adapter.entities.user.resetpin;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ResetPinRequest extends AbstractRequest implements Validable {

    private Status              status = new Status();

    private Credential          credential;
    private ResetPinRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ResetPinRequestBody getBody() {
        return body;
    }

    public void setBody(ResetPinRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PIN", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        else {

            status.setStatusCode(StatusCode.USER_PIN_FAILURE);
            return status;

        }

        status.setStatusCode(StatusCode.USER_PIN_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        ResetPinRequest resetPinRequest = this;

        String response = getUserServiceRemote().resetPin(resetPinRequest.getCredential().getTicketID(), resetPinRequest.getCredential().getRequestID(),
                resetPinRequest.getBody().getPassword(), resetPinRequest.getBody().getNewPin());

        ResetPinResponse resetPinResponse = new ResetPinResponse();

        status.setStatusCode(response);

        status.setStatusMessage(prop.getProperty(response));

        resetPinResponse.setStatus(status);

        return resetPinResponse;
    }

}
