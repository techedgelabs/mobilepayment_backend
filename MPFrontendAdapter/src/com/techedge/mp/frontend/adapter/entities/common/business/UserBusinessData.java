package com.techedge.mp.frontend.adapter.entities.common.business;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import com.techedge.mp.frontend.adapter.entities.common.ContactData;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.SecurityData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.TermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UserBusinessData implements Validable {

    private String                   firstName;
    private String                   lastName;
    private String                   fiscalCode;
    private CustomDate               dateOfBirth;
    private String                   birthMunicipality;
    private String                   birthProvince;
    private String                   language;
    private String                   sex;
    private BusinessData             businessData;
    private SecurityData             securityData;
    private List<TermsOfServiceData> termsOfService = new ArrayList<TermsOfServiceData>(0);
    private ContactData              contactData;
    private UserStatus               userStatus;

    public UserBusinessData() {}

    @Override
    public Status check() {
        Status status = new Status();
        
        if (firstName == null || firstName.isEmpty() || firstName.length() > 60) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_FIRST_NAME_REQUIRED);
            return status;
        }
        else {
            if (Pattern.compile("[0-9]").matcher(firstName).find()) {
                status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_FIRST_NAME_INVALID);
                return status;
            }
        }
        
        if (lastName == null || lastName.isEmpty() || lastName.length() > 60) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_LAST_NAME_REQUIRED);
            return status;
        }
        else {
            if (Pattern.compile("[0-9]").matcher(lastName).find()) {
                status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_LAST_NAME_INVALID);
                return status;
            }
        }
        
        if (fiscalCode == null || fiscalCode.isEmpty() || fiscalCode.length() != 16) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_FISCALCODE_REQUIRED);
            return status;
        }

        if (dateOfBirth == null) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_DATE_OF_BIRTH_REQUIRED);
            return status;
        }
        
        if (birthMunicipality == null || birthMunicipality.isEmpty()) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BIRTH_MUNICIPALITY_REQUIRED);
            return status;
        }

        if (birthProvince == null || birthProvince.isEmpty()) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BIRTH_PROVINCE_REQUIRED);
            return status;
        }

        if (sex == null || sex.isEmpty()) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_SEX_REQUIRED);
            return status;
        }
        
        if (this.businessData != null) {
            status = this.businessData.check();
            if (!Validator.isValid(status.getStatusCode())) {
                return status;
            }
        }
        else {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BILLING_ADDRESS_DATA_REQUIRED);
            return status;
        }
        
        if (this.securityData != null) {

            status = this.securityData.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_SECURITY_DATA_REQUIRED);
            return status;
        }
        
        status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_SUCCESS);
        return status;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public CustomDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(CustomDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getBirthMunicipality() {
        return birthMunicipality;
    }

    public void setBirthMunicipality(String birthMunicipality) {
        this.birthMunicipality = birthMunicipality;
    }

    public String getBirthProvince() {
        return birthProvince;
    }

    public void setBirthProvince(String birthProvince) {
        this.birthProvince = birthProvince;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public BusinessData getBusinessData() {
        return businessData;
    }

    public void setBusinessData(BusinessData businessData) {
        this.businessData = businessData;
    }

    public SecurityData getSecurityData() {
        return securityData;
    }

    public void setSecurityData(SecurityData securityData) {
        this.securityData = securityData;
    }

    public List<TermsOfServiceData> getTermsOfService() {
        return termsOfService;
    }

    public void setTermsOfService(List<TermsOfServiceData> termsOfService) {
        this.termsOfService = termsOfService;
    }

    public ContactData getContactData() {
        return contactData;
    }

    public void setContactData(ContactData contactData) {
        this.contactData = contactData;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }
    
    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }
}
