package com.techedge.mp.frontend.adapter.entities.user.v2.getpartneractionurl;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetPartnerActionUrlResponse extends BaseResponse {

    private GetPartnerActionUrlBodyResponse body;

    public GetPartnerActionUrlBodyResponse getBody() {
        return body;
    }

    public void setBody(GetPartnerActionUrlBodyResponse body) {
        this.body = body;
    }


}
