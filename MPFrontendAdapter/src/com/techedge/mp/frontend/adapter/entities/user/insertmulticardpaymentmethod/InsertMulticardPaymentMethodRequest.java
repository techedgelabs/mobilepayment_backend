package com.techedge.mp.frontend.adapter.entities.user.insertmulticardpaymentmethod;

import com.techedge.mp.core.business.interfaces.MulticardPaymentResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class InsertMulticardPaymentMethodRequest extends AbstractRequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("MULTICARD-PAY-INSERT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }
        
        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.USER_INSERT_MULTICARD_PAYMENT_METHOD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        InsertMulticardPaymentMethodRequest insertPaymentMethodRequest = this;

        MulticardPaymentResponse paymentResponse = getUserServiceRemote().insertMulticardPaymentMethod(insertPaymentMethodRequest.getCredential().getTicketID(),
                insertPaymentMethodRequest.getCredential().getRequestID());

        InsertMulticardPaymentMethodResponse insertPaymentMethodResponse = new InsertMulticardPaymentMethodResponse();

        status.setStatusCode(paymentResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(paymentResponse.getStatusCode()));

        insertPaymentMethodResponse.setStatus(status);

        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(paymentResponse.getPaymentMethodId());
        paymentMethod.setType(paymentResponse.getPaymentMethodType());

        InsertMulticardPaymentMethodBodyResponse insertPaymentMethodBodyResponse = new InsertMulticardPaymentMethodBodyResponse();
        insertPaymentMethodBodyResponse.setPaymentMethod(paymentMethod);
        insertPaymentMethodBodyResponse.setOperationId(paymentResponse.getOperationID());

        insertPaymentMethodResponse.setBody(insertPaymentMethodBodyResponse);

        return insertPaymentMethodResponse;
    }

}
