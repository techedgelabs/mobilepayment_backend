package com.techedge.mp.frontend.adapter.entities.parking.v2.estimateendparkingprice;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class EstimateEndParkingPriceRequestBody implements Validable {

    private String transactionID;

    @Override
    public Status check() {

        Status status = new Status();

        if (this.transactionID == null || this.transactionID.isEmpty()) {

            status.setStatusCode("TBD");

            return status;

        }

        status.setStatusCode(StatusCode.POSTPAID_V2_APPROVE_TRANSACTION_SUCCESS);

        return status;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

}
