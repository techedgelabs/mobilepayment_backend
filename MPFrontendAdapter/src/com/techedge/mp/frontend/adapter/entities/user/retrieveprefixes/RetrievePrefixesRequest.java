package com.techedge.mp.frontend.adapter.entities.user.retrieveprefixes;

import com.techedge.mp.core.business.interfaces.PrefixNumberResult;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrievePrefixesRequest extends AbstractRequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-PREFIXES", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.RETRIEVE_PREFIXES_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        RetrievePrefixesRequest retrieveDocumentRequest = this;
        PrefixNumberResult retrievePrefixData = getUserServiceRemote().retrieveAllPrefixNumber(retrieveDocumentRequest.getCredential().getTicketID(),
                retrieveDocumentRequest.getCredential().getRequestID());

        RetrievePrefixesResponse retrievePrefixNumberResponse = new RetrievePrefixesResponse();
        RetrievePrefixesResponseBody retrievePrefixNumberBodyResponse = new RetrievePrefixesResponseBody();

        retrievePrefixNumberBodyResponse.setPrefixes(retrievePrefixData.getListPrefix());

        status.setStatusCode(retrievePrefixData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrievePrefixData.getStatusCode()));
        retrievePrefixNumberResponse.setStatus(status);

        retrievePrefixNumberResponse.setBody(retrievePrefixNumberBodyResponse);

        return retrievePrefixNumberResponse;
    }

}
