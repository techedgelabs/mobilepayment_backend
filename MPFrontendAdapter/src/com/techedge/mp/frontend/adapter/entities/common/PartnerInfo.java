
package com.techedge.mp.frontend.adapter.entities.common;



public class PartnerInfo {

    private Integer partnerId;
    private Integer categoryId;
    private String category;
    private String name;
    private String description;
    private String title;
    private String logic;
    private String logoUrl;
    private String logoSmallUrl;
    
    
    public Integer getPartnerId() {
        return partnerId;
    }
    public void setPartnerId(Integer partnerId) {
        this.partnerId = partnerId;
    }
    public Integer getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getLogic() {
        return logic;
    }
    public void setLogic(String logic) {
        this.logic = logic;
    }
    public String getLogoUrl() {
        return logoUrl;
    }
    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }
    public String getLogoSmallUrl() {
        return logoSmallUrl;
    }
    public void setLogoSmallUrl(String logoSmallUrl) {
        this.logoSmallUrl = logoSmallUrl;
    }
    
}
