package com.techedge.mp.frontend.adapter.entities.refuel.cancel;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class CancelRefuelRequest extends AbstractRequest implements Validable {

    private Credential              credential;
    private CancelRefuelBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CancelRefuelBodyRequest getBody() {
        return body;
    }

    public void setBody(CancelRefuelBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CANCEL-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        CancelRefuelRequest cancelRefuelRequest = this;

        String response = getTransactionServiceRemote().persistUndoRefuelStatus(cancelRefuelRequest.getCredential().getTicketID(),
                cancelRefuelRequest.getCredential().getRequestID(), cancelRefuelRequest.getBody().getRefuelID());

        CancelRefuelResponse cancelRefuelResponse = new CancelRefuelResponse();

        Status statusResponse = new Status();

        statusResponse.setStatusCode(response);
        statusResponse.setStatusMessage(prop.getProperty(response));

        cancelRefuelResponse.setStatus(statusResponse);

        return cancelRefuelResponse;
    }

}