package com.techedge.mp.frontend.adapter.entities.user.v2.getlanding;

import com.techedge.mp.core.business.interfaces.LandingDataResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetLandingRequest extends AbstractRequest {

    private Status                status = new Status();

    private Credential            credential;
    private GetLandingRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GetLandingRequestBody getBody() {
        return body;
    }

    public void setBody(GetLandingRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GETLANDING", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.USER_V2_GETLANDING_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        GetLandingResponse getLandingResponse = new GetLandingResponse();
        GetLandingResponseBody getLandingResponseBody = new GetLandingResponseBody();

        LandingDataResponse response = getUserV2ServiceRemote().getLandingData(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getSectionId());

//        String title = "Titolo";
//        String url = "https://www.google.com";
//        Boolean showAlways = Boolean.FALSE;
//
//        if (getLandingRequest.getBody() != null) {
//
//            String sectionId = getLandingRequest.getBody().getSectionId();
//
//            if (sectionId.equals("REGISTRATION")) {
//                title = "Registrazione";
//                url = "http://trustmyphone-01.mashfrog.com/enipay/webpages/eni_preRegistra/";
//                showAlways = Boolean.TRUE;
//            }
//
//            if (sectionId.equals("REGISTRATION-ENIPAY")) {
//
//                title = "Titolo";
//                url = "http://mpdev.r53.techedgegroup.com:8080/MPFrontendServlet/landing/preRegistraMigEniPay/index.html";
//                showAlways = Boolean.TRUE;
//            }
//
//            if (sectionId.equals("REGISTRATION-ENISTATION")) {
//
//                title = "Titolo";
//                url = "http://mpdev.r53.techedgegroup.com:8080/MPFrontendServlet/landing/preRegistraMigYouEni/index.html";
//                showAlways = Boolean.TRUE;
//            }
//        }

        if (response != null && response.getLandingData() != null) {
            getLandingResponseBody.setTitle(response.getLandingData().getTitle());
            getLandingResponseBody.setUrl(response.getLandingData().getUrl());
            getLandingResponseBody.setShowAlways(response.getLandingData().getShowAlways());
        }

        status.setStatusCode(response.getStatusCode());
        status.setStatusMessage(prop.getProperty(response.getStatusCode()));

        getLandingResponse.setStatus(status);
        getLandingResponse.setBody(getLandingResponseBody);

        return getLandingResponse;
    }

}
