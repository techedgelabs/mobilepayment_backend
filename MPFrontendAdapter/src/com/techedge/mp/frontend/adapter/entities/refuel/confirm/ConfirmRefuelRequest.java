package com.techedge.mp.frontend.adapter.entities.refuel.confirm;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ConfirmRefuelRequest extends AbstractRequest implements Validable {

    private Credential               credential;
    private ConfirmRefuelBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ConfirmRefuelBodyRequest getBody() {
        return body;
    }

    public void setBody(ConfirmRefuelBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CONFIRM-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        ConfirmRefuelRequest confirmRefuelRequest = this;

        String response = getTransactionServiceRemote().confirmRefuel(confirmRefuelRequest.getCredential().getTicketID(), confirmRefuelRequest.getCredential().getRequestID(),
                confirmRefuelRequest.getBody().getRefuelID());

        ConfirmRefuelResponse confirmRefuelResponse = new ConfirmRefuelResponse();

        Status statusResponse = new Status();

        statusResponse.setStatusCode(response);
        statusResponse.setStatusMessage(prop.getProperty(response));

        confirmRefuelResponse.setStatus(statusResponse);

        return confirmRefuelResponse;
    }

}