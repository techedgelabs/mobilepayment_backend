package com.techedge.mp.frontend.adapter.entities.parking.v2.retrievependingparkingtransaction;

import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZonePriceInfo;

public class RetrievePendingParkingTransactionBodyResponse {

    private ParkingZonePriceInfo parkingZonePriceInfo;
    private String               status;

    public ParkingZonePriceInfo getParkingZonePriceInfo() {
        return parkingZonePriceInfo;
    }

    public void setParkingZonePriceInfo(ParkingZonePriceInfo parkingZonePriceInfo) {
        this.parkingZonePriceInfo = parkingZonePriceInfo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
