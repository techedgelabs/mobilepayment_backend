package com.techedge.mp.frontend.adapter.entities.user.retrievepaymentdata;

import com.techedge.mp.frontend.adapter.entities.common.PaymentData;

public class RetrievePaymentDataBodyResponse {
	
	private PaymentData paymentData;

	public PaymentData getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(PaymentData paymentData) {
		this.paymentData = paymentData;
	}
}
