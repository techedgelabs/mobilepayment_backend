package com.techedge.mp.frontend.adapter.entities.manager.retrievestation;

import com.techedge.mp.core.business.interfaces.ExtendedPumpInfo;
import com.techedge.mp.core.business.interfaces.GetStationInfo;
import com.techedge.mp.core.business.interfaces.ManagerStationDetailsResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.ManagerStationDetails;
import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ManagerRetrieveStationDetailsRequest extends AbstractRequest implements Validable {

    private Status                                   status = new Status();

    private Credential                               credential;
    private ManagerRetrieveStationDetailsBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ManagerRetrieveStationDetailsBodyRequest getBody() {
        return body;
    }

    public void setBody(ManagerRetrieveStationDetailsBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("STATION-RETRIEVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(ResponseHelper.MANAGER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(ResponseHelper.MANAGER_STATION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        ManagerRetrieveStationDetailsRequest stationDetailsRequest = this;
        ManagerRetrieveStationDetailsResponse stationDetailsResponse = new ManagerRetrieveStationDetailsResponse();

        String ticketId = stationDetailsRequest.getCredential().getTicketID();
        String requestId = stationDetailsRequest.getCredential().getRequestID();
        String stationId = stationDetailsRequest.getBody().getStationID();
        ManagerStationDetailsResponse stationDetails = getManagerServiceRemote().stationDetails(ticketId, requestId, stationId);
        GetStationInfo stationInfo = stationDetails.getStationInfo();

        if (stationInfo == null) {
            BaseResponse baseResponse = new BaseResponse();
            Status statusResponse = new Status();
            statusResponse.setStatusCode(stationDetails.getStatusCode());
            statusResponse.setStatusMessage(prop.getProperty(stationDetails.getStatusCode()));
            baseResponse.setStatus(statusResponse);

            return baseResponse;
        }

        ManagerRetrieveStationDetailsBodyResponse bodyResponse = new ManagerRetrieveStationDetailsBodyResponse();
        ManagerStationDetails station = new ManagerStationDetails();
        LocationData locationData = new LocationData();
        locationData.setAddress(stationInfo.getAddress());
        locationData.setCity(stationInfo.getCity());
        locationData.setCountry(stationInfo.getCountry());
        locationData.setProvince(stationInfo.getProvince());
        locationData.setLatitude(Double.valueOf(stationInfo.getLatitude()));
        locationData.setLongitude(Double.valueOf(stationInfo.getLongitude()));
        station.setLocationData(locationData);
        station.setName(null);
        station.setStationID(stationInfo.getStationId());

        if (!stationInfo.getPumpList().isEmpty()) {

            for (ExtendedPumpInfo pumpInfo : stationInfo.getPumpList()) {
                Pump pump = new Pump();
                pump.setNumber(Integer.valueOf(pumpInfo.getNumber()));
                pump.setPumpID(pumpInfo.getPumpId());
                pump.setStatus(pumpInfo.getStatus());
                station.getPumpList().add(pump);
            }

        }

        bodyResponse.setStation(station);
        stationDetailsResponse.setBody(bodyResponse);
        status.setStatusCode(stationDetails.getStatusCode());
        status.setStatusMessage(prop.getProperty(stationDetails.getStatusCode()));

        stationDetailsResponse.setStatus(status);

        return stationDetailsResponse;
    }

}
