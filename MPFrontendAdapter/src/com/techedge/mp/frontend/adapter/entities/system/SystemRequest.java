package com.techedge.mp.frontend.adapter.entities.system;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings.RetrieveApplicationSettingsRequest;


public class SystemRequest implements Validable {

	
	protected RetrieveApplicationSettingsRequest retrieveApplicationSettings;
	
	public RetrieveApplicationSettingsRequest getRetrieveApplicationSettings() {
		return retrieveApplicationSettings;
	}

	public void setRetrieveApplicationSettings(
			RetrieveApplicationSettingsRequest retrieveApplicationSettings) {
		this.retrieveApplicationSettings = retrieveApplicationSettings;
	}

	@Override
	public Status check() {
		
		boolean serviceFound = false;
		
		Status status = new Status();
		
		if(this.retrieveApplicationSettings != null) {
			
			serviceFound = true;
			
			status = this.retrieveApplicationSettings.check();
			
			return status;
				
		}
		
		if(!serviceFound) {
			
			status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
			
			return status;
		}
		
		return status;

	}

}
