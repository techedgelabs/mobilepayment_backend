package com.techedge.mp.frontend.adapter.entities.requests;

import java.util.Properties;

import com.techedge.mp.bpel.operation.refuel.business.BPELServiceRemote;
import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.EventNotificationServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ManagerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.ParkingTransactionV2ServiceRemote;
import com.techedge.mp.core.business.PostPaidTransactionServiceRemote;
import com.techedge.mp.core.business.PostPaidV2TransactionServiceRemote;
import com.techedge.mp.core.business.RefuelingServiceRemote;
import com.techedge.mp.core.business.SurveyServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.TransactionV2ServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.UserV2ServiceRemote;
import com.techedge.mp.core.business.VoucherTransactionServiceRemote;
import com.techedge.mp.fidelity.adapter.business.AuthorizationPlusServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
//import com.techedge.mp.frontend.adapter.business.BPELServiceLocal;
//import com.techedge.mp.frontend.adapter.business.ForecourtServiceLocal;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;

public abstract class AbstractRequest {

    protected Properties                       prop = null;

    private LoggerServiceRemote                loggerServiceRemote;
    private UserV2ServiceRemote                userV2ServiceRemote;
    private UserServiceRemote                  userServiceRemote;
    private EventNotificationServiceRemote     eventNotificationServiceRemote;
    private CRMServiceRemote                   crmServiceRemote;
    private ParametersServiceRemote            parameterServiceRemote;
    private VoucherTransactionServiceRemote    voucherTransactionServiceRemote;
    private SurveyServiceRemote                surveyServiceRemote;
    private ManagerServiceRemote               managerServiceRemote;
    private PostPaidTransactionServiceRemote   postPaidTransactionServiceRemote;
    private TransactionServiceRemote           transactionServiceRemote;
    private TransactionV2ServiceRemote         transactionV2ServiceRemote;
    private ForecourtPostPaidServiceRemote     forecourtPostPaidServiceRemote;
    private ForecourtInfoServiceRemote         forecourtInfoServiceRemote;
    private BPELServiceRemote                  bpelServiceRemote;
    private PostPaidV2TransactionServiceRemote postPaidV2TransactionServiceRemote;
    private ParkingTransactionV2ServiceRemote  parkingTransactionV2ServiceRemote;
    private RefuelingServiceRemote             refuelingServiceRemote;
    private AuthorizationPlusServiceRemote authorizationPlusServiceRemote;
    public abstract Status check();

    public abstract BaseResponse execute();

    public Properties getProp() {
        return prop;
    }

    public void setProp(Properties prop) {
        this.prop = prop;
    }

    public LoggerServiceRemote getLoggerServiceRemote() {
        return loggerServiceRemote;
    }

    public void setLoggerServiceRemote(LoggerServiceRemote loggerServiceRemote) {
        this.loggerServiceRemote = loggerServiceRemote;
    }

    public UserV2ServiceRemote getUserV2ServiceRemote() {
        return userV2ServiceRemote;
    }

    public void setUserV2ServiceRemote(UserV2ServiceRemote userV2ServiceRemote) {
        this.userV2ServiceRemote = userV2ServiceRemote;
    }

    public UserServiceRemote getUserServiceRemote() {
        return userServiceRemote;
    }

    public void setUserServiceRemote(UserServiceRemote userServiceRemote) {
        this.userServiceRemote = userServiceRemote;
    }

    public EventNotificationServiceRemote getEventNotificationServiceRemote() {
        return eventNotificationServiceRemote;
    }

    public void setEventNotificationServiceRemote(EventNotificationServiceRemote eventNotificationServiceRemote) {
        this.eventNotificationServiceRemote = eventNotificationServiceRemote;
    }

    public CRMServiceRemote getCrmServiceRemote() {
        return crmServiceRemote;
    }

    public void setCrmServiceRemote(CRMServiceRemote crmServiceRemote) {
        this.crmServiceRemote = crmServiceRemote;
    }

    public ParametersServiceRemote getParameterServiceRemote() {
        return parameterServiceRemote;
    }

    public void setParameterServiceRemote(ParametersServiceRemote parameterServiceRemote) {
        this.parameterServiceRemote = parameterServiceRemote;
    }

    public VoucherTransactionServiceRemote getVoucherTransactionServiceRemote() {
        return voucherTransactionServiceRemote;
    }

    public void setVoucherTransactionServiceRemote(VoucherTransactionServiceRemote voucherTransactionServiceRemote) {
        this.voucherTransactionServiceRemote = voucherTransactionServiceRemote;
    }

    public SurveyServiceRemote getSurveyServiceRemote() {
        return surveyServiceRemote;
    }

    public void setSurveyServiceRemote(SurveyServiceRemote surveyServiceRemote) {
        this.surveyServiceRemote = surveyServiceRemote;
    }

    public ManagerServiceRemote getManagerServiceRemote() {
        return managerServiceRemote;
    }

    public void setManagerServiceRemote(ManagerServiceRemote managerServiceRemote) {
        this.managerServiceRemote = managerServiceRemote;
    }

    public PostPaidTransactionServiceRemote getPostPaidTransactionServiceRemote() {
        return postPaidTransactionServiceRemote;
    }

    public void setPostPaidTransactionServiceRemote(PostPaidTransactionServiceRemote postPaidTransactionServiceRemote) {
        this.postPaidTransactionServiceRemote = postPaidTransactionServiceRemote;
    }

    public TransactionServiceRemote getTransactionServiceRemote() {
        return transactionServiceRemote;
    }

    public void setTransactionServiceRemote(TransactionServiceRemote transactionServiceRemote) {
        this.transactionServiceRemote = transactionServiceRemote;
    }

    public TransactionV2ServiceRemote getTransactionV2ServiceRemote() {
        return transactionV2ServiceRemote;
    }

    public void setTransactionV2ServiceRemote(TransactionV2ServiceRemote transactionV2ServiceRemote) {
        this.transactionV2ServiceRemote = transactionV2ServiceRemote;
    }

    public ForecourtPostPaidServiceRemote getForecourtPostPaidServiceRemote() {
        return forecourtPostPaidServiceRemote;
    }

    public void setForecourtPostPaidServiceRemote(ForecourtPostPaidServiceRemote forecourtPostPaidServiceRemote) {
        this.forecourtPostPaidServiceRemote = forecourtPostPaidServiceRemote;
    }

    public ForecourtInfoServiceRemote getForecourtInfoServiceRemote() {
        return forecourtInfoServiceRemote;
    }

    public void setForecourtInfoServiceRemote(ForecourtInfoServiceRemote forecourtInfoServiceRemote) {
        this.forecourtInfoServiceRemote = forecourtInfoServiceRemote;
    }

    public BPELServiceRemote getBpelServiceRemote() {
        return bpelServiceRemote;
    }

    public void setBpelServiceRemote(BPELServiceRemote bpelServiceRemote) {
        this.bpelServiceRemote = bpelServiceRemote;
    }

    public PostPaidV2TransactionServiceRemote getPostPaidV2TransactionServiceRemote() {
        return postPaidV2TransactionServiceRemote;
    }

    public void setPostPaidV2TransactionServiceRemote(PostPaidV2TransactionServiceRemote postPaidV2TransactionServiceRemote) {
        this.postPaidV2TransactionServiceRemote = postPaidV2TransactionServiceRemote;
    }

    public ParkingTransactionV2ServiceRemote getParkingTransactionV2ServiceRemote() {
        return parkingTransactionV2ServiceRemote;
    }

    public void setParkingTransactionV2ServiceRemote(ParkingTransactionV2ServiceRemote parkingTransactionV2ServiceRemote) {
        this.parkingTransactionV2ServiceRemote = parkingTransactionV2ServiceRemote;
    }
    
    public RefuelingServiceRemote getRefuelingServiceRemote() {
        return refuelingServiceRemote;
    }

    public void setRefuelingServiceRemote(RefuelingServiceRemote refuelingServiceRemote) {
        this.refuelingServiceRemote = refuelingServiceRemote;
    }
    
    public AuthorizationPlusServiceRemote getAuthorizationPlusServiceRemote() {
        return authorizationPlusServiceRemote;
    }

    public void setAuthorizationPlusServiceRemote(
            AuthorizationPlusServiceRemote authorizationPlusServiceRemote) {
        this.authorizationPlusServiceRemote = authorizationPlusServiceRemote;
    }
}
