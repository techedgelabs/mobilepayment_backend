package com.techedge.mp.frontend.adapter.entities.user.insertpaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;

public class InsertPaymentMethodBodyResponse {

    private String        shopLogin;
    private String        secureString;
    private PaymentMethod paymentMethod;
    private Integer       checkPinAttemptsLeft;

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getSecureString() {
        return secureString;
    }

    public void setSecureString(String secureString) {
        this.secureString = secureString;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getCheckPinAttemptsLeft() {
        return checkPinAttemptsLeft;
    }

    public void setCheckPinAttemptsLeft(Integer checkPinAttemptsLeft) {
        this.checkPinAttemptsLeft = checkPinAttemptsLeft;
    }

}
