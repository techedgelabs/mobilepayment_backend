package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbyqrcode;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class RetrievePoPTransactionByQRcodeResponse extends BaseResponse {
	
	private RetrievePoPTransactionByQRcodeBodyResponse body;

	public RetrievePoPTransactionByQRcodeBodyResponse getBody() {
		return body;
	}

	public void setBody(RetrievePoPTransactionByQRcodeBodyResponse body) {
		this.body = body;
	}


}
