package com.techedge.mp.frontend.adapter.entities.user.redemption;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.VoucherData;

public class RedemptionBodyResponse extends BaseResponse {
    private VoucherData voucher;
    private Integer     checkPinAttemptsLeft;

    public VoucherData getVoucher() {
        return voucher;
    }

    public void setVoucher(VoucherData voucher) {
        this.voucher = voucher;
    }

    public Integer getCheckPinAttemptsLeft() {
        return checkPinAttemptsLeft;
    }

    public void setCheckPinAttemptsLeft(Integer checkPinAttemptsLeft) {
        this.checkPinAttemptsLeft = checkPinAttemptsLeft;
    }

}
