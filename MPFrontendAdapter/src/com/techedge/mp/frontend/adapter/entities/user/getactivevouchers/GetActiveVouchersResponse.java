package com.techedge.mp.frontend.adapter.entities.user.getactivevouchers;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetActiveVouchersResponse extends BaseResponse {

	
	private GetActiveVouchersBodyResponse body;

	
	public GetActiveVouchersBodyResponse getBody() {
		return body;
	}

	
	public void setBody(GetActiveVouchersBodyResponse body) {
		this.body = body;
	}
	
	
	
}
