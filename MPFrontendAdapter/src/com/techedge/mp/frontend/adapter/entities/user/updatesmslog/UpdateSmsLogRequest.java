package com.techedge.mp.frontend.adapter.entities.user.updatesmslog;

import java.net.URLDecoder;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class UpdateSmsLogRequest extends AbstractRequest implements Validable {

    private Status                  status = new Status();

    private Credential              credential;
    private UpdateSmsLogBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateSmsLogBodyRequest getBody() {
        return body;
    }

    public void setBody(UpdateSmsLogBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPD-SMS-LOG", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {
            status.setStatusCode(StatusCode.UPDATE_SMS_LOG_FAILURE);
            return status;
        }

        status.setStatusCode(StatusCode.UPDATE_SMS_LOG_SUCCESS);
        status.setStatusMessage("SMS Log updated");

        return status;

    }

    @Override
    public BaseResponse execute() {
        UpdateSmsLogRequest updateSmsLogRequest = this;
        UpdateSmsLogResponse updateSmsLogResponse = new UpdateSmsLogResponse();

//        String ticketId = updateSmsLogRequest.getCredential().getTicketID();
//        String requestId = updateSmsLogRequest.getCredential().getRequestID();
//        String correlationID = updateSmsLogRequest.getBody().getCorrelationId();
//        String mtMessageID = updateSmsLogRequest.getBody().getMessageId();
//        String message = null;

        try {
            if (updateSmsLogRequest.getBody().getMessage() != null) {
//                message = URLDecoder.decode(updateSmsLogRequest.getBody().getMessage(), "UTF-8");
            }
        }
        catch (Exception e) {
            System.err.println("Errore nel decode del campo message: " + e.getMessage());
        }

//        Integer statusCode = updateSmsLogRequest.getBody().getStatusCode();
//        Integer reasonCode = updateSmsLogRequest.getBody().getReasonCode();
//        Date operatorTimestamp = null;
//        Date providerTimestamp = null;
        String timestamp;

        try {
            timestamp = updateSmsLogRequest.getBody().getOperatorTimeStamp();
            if (timestamp != null && !timestamp.isEmpty()) {
                timestamp = URLDecoder.decode(timestamp, "UTF-8");
                System.out.println("operatorTimestamp:" + timestamp);
//                operatorTimestamp = new SimpleDateFormat("yyyyMMdd HH:mm:ss").parse(timestamp);
            }
        }
        catch (Exception e) {
            System.err.println("Errore nel parsing della data operatorTimestamp: " + e.getMessage());
        }

        try {
            timestamp = updateSmsLogRequest.getBody().getTimeStamp();
            if (timestamp != null && !timestamp.isEmpty()) {
                timestamp = URLDecoder.decode(timestamp, "UTF-8");
                System.out.println("providerTimestamp:" + timestamp);
//                providerTimestamp = new SimpleDateFormat("yyyyMMdd HH:mm:ss").parse(timestamp);
            }
        }
        catch (Exception e) {
            System.err.println("Errore nel parsing della data providerTimestamp: " + e.getMessage());
        }

//        String operator = updateSmsLogRequest.getBody().getOperator();
        /*
         * String response = userService.updateSmsLog(correlationID, mtMessageID, message, statusCode, reasonCode,
         * operatorTimestamp, providerTimestamp, operator);
         */
        String response = ResponseHelper.USER_UPDATE_SMS_LOG_SUCCESS;
        status.setStatusCode(response);

        status.setStatusMessage(prop.getProperty(response));

        updateSmsLogResponse.setStatus(status);

        return updateSmsLogResponse;
    }

}
