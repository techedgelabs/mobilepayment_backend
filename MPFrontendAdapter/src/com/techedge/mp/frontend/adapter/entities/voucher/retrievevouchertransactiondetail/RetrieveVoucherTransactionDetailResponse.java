package com.techedge.mp.frontend.adapter.entities.voucher.retrievevouchertransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveVoucherTransactionDetailResponse extends BaseResponse {

    private RetrieveVoucherTransactionDetailBodyResponse body;

    public RetrieveVoucherTransactionDetailBodyResponse getBody() {
        return body;
    }

    public void setBody(RetrieveVoucherTransactionDetailBodyResponse body) {
        this.body = body;
    }

}
