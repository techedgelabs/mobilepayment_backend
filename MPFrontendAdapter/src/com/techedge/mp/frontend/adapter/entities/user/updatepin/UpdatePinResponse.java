package com.techedge.mp.frontend.adapter.entities.user.updatepin;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class UpdatePinResponse extends BaseResponse{
	
	private UpdatePinResponseBody body;

	public UpdatePinResponseBody getBody() {
		return body;
	}
	public void setBody(UpdatePinResponseBody body) {
		this.body = body;
	}
}
