package com.techedge.mp.frontend.adapter.entities.eventnotification.retrievenotification;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveNotificationResponse extends BaseResponse {
    
    private RetrieveNotificationBodyResponse body;
    

    public RetrieveNotificationBodyResponse getBody() {
        return body;
    }

    public void setBody(RetrieveNotificationBodyResponse body) {
        this.body = body;
    }
    
    

}
