package com.techedge.mp.frontend.adapter.entities.user.retrieveprefixes;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrievePrefixesResponse extends BaseResponse {

    private RetrievePrefixesResponseBody body;

    public RetrievePrefixesResponseBody getBody() {
        return body;
    }
    public void setBody(RetrievePrefixesResponseBody body) {
        this.body = body;
    }
    
}
