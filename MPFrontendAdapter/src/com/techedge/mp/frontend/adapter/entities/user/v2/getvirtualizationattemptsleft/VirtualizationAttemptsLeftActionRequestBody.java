package com.techedge.mp.frontend.adapter.entities.user.v2.getvirtualizationattemptsleft;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class VirtualizationAttemptsLeftActionRequestBody implements Validable {

    private String action;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (action == null || action.trim() == "") {

            status.setStatusCode(StatusCode.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_INVALID_REQUEST);

            return status;
        }
        
        if (action.equals("RETIREVE") && action.equals("DECREMENT")) {
            
            status.setStatusCode(StatusCode.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_V2_GETLANDING_SUCCESS);

        return status;
    }

}
