package com.techedge.mp.frontend.adapter.entities.user.setusevoucher;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class SetUseVoucherRequest extends AbstractRequest implements Validable {

    private Status                   status = new Status();

    private Credential               credential;
    private SetUseVoucherBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public SetUseVoucherBodyRequest getBody() {
        return body;
    }

    public void setBody(SetUseVoucherBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("SET-USE-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        SetUseVoucherRequest setUseVoucherRequest = this;
        SetUseVoucherResponse setUseVoucherResponse = new SetUseVoucherResponse();

        String response = getUserServiceRemote().setUseVoucher(setUseVoucherRequest.getCredential().getTicketID(), setUseVoucherRequest.getCredential().getRequestID(),
                setUseVoucherRequest.getBody().getUseVoucher());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        setUseVoucherResponse.setStatus(status);

        return setUseVoucherResponse;
    }

}
