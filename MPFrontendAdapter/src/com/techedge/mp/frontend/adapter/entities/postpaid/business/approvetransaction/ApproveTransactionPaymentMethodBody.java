package com.techedge.mp.frontend.adapter.entities.postpaid.business.approvetransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ApproveTransactionPaymentMethodBody implements Validable {
	
	private Long id;
	private String type;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	@Override
	public Status check() {
		Status status = new Status();
		status.setStatusCode(StatusCode.POSTPAID_BUSINESS_APPROVE_TRANSACTION_SUCCESS);
		return status;
	}
}