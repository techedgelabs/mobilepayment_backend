package com.techedge.mp.frontend.adapter.entities.user.logout;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class LogoutUserRequest extends AbstractRequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("LOGOUT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.USER_LOGOUT_SUCCESS);

        return status;
    }

    @Override
    public BaseResponse execute() {
        LogoutUserRequest logoutUserRequest = this;

        String response = getUserServiceRemote().logout(logoutUserRequest.getCredential().getTicketID(), logoutUserRequest.getCredential().getRequestID());

        LogoutUserResponse logoutUserResponse = new LogoutUserResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        logoutUserResponse.setStatus(status);

        return logoutUserResponse;

    }

}
