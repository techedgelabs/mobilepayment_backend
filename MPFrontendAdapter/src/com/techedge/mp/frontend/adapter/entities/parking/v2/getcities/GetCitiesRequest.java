package com.techedge.mp.frontend.adapter.entities.parking.v2.getcities;

import com.techedge.mp.core.business.interfaces.parking.GetCitiesResult;
import com.techedge.mp.core.business.interfaces.parking.ParkingCity;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetCitiesRequest extends AbstractRequest implements Validable {

    private Status               status = new Status();
    private Credential           credential;
    private GetCitiesRequestBody body;

    public GetCitiesRequestBody getBody() {
        return body;
    }

    public void setBody(GetCitiesRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.GETCITIES_SUCCESS);

        return status;

    }

    public Credential getCredential() {
        return credential;
    }

    @Override
    public BaseResponse execute() {

        GetCitiesRequest getCitiesRequest = this;

        GetCitiesResponse getCitiesResponse = new GetCitiesResponse();

        GetCitiesResponseBody getCitiesResponseBody = new GetCitiesResponseBody();

        GetCitiesResult result = getParkingTransactionV2ServiceRemote().getCities(getCitiesRequest.getCredential().getTicketID(), getCitiesRequest.getCredential().getRequestID());

        for (ParkingCity parkingCity : result.getParkingCities()) {

            com.techedge.mp.frontend.adapter.entities.common.v2.ParkingCity parkingCity2 = new com.techedge.mp.frontend.adapter.entities.common.v2.ParkingCity();
            parkingCity2.setAdministrativeAreaLevel2Code(parkingCity.getAdministrativeAreaLevel2Code());
            parkingCity2.setCityId(parkingCity.getCityId());
            parkingCity2.setId(parkingCity.getId());
            parkingCity2.setLatitude(parkingCity.getLatitude());
            parkingCity2.setLongitude(parkingCity.getLongitude());
            parkingCity2.setName(parkingCity.getName());
            parkingCity2.setStickerRequired(parkingCity.getStickerRequired());
            parkingCity2.setStickerRequiredNotice(parkingCity.getStickerRequiredNotice());
            parkingCity2.setUrl(parkingCity.getUrl());

            getCitiesResponseBody.getParkingCityList().add(parkingCity2);

        }

        getCitiesResponse.setBody(getCitiesResponseBody);

        Status status = new Status();
        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(prop.getProperty(result.getStatusCode()));
        getCitiesResponse.setStatus(status);

        return getCitiesResponse;

    }
}
