package com.techedge.mp.frontend.adapter.entities.refuel.retrievestation;

import com.techedge.mp.bpel.operation.refuel.business.interfaces.StationInfoRequest;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.GeoDataUtility;
import com.techedge.mp.core.business.interfaces.OperationType;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.FrontendParameter;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveStationRequest extends AbstractRequest implements Validable {

    private Credential                 credential;
    private RetrieveStationBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveStationBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveStationBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("STATION-RETRIEVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        boolean isStationID = false;

        RetrieveStationRequest retrieveStationRequest = this;

        RetrieveStationResponse retrieveStationResponse = new RetrieveStationResponse();

        Status statusResponse = new Status();

        // Verifica se l'utente pu� effettuare una richiesta al servizio retrieveStation
        String response = getUserServiceRemote().checkAuthorization(retrieveStationRequest.getCredential().getTicketID(), OperationType.RETRIEVE_STATION);

        if (!response.equals(ResponseHelper.CHECK_AUTHORIZATION_SUCCESS)) {

            statusResponse.setStatusCode(response);
            statusResponse.setStatusMessage(prop.getProperty(response));
        }
        else {

            // call verso il ForecourtAdapter

            StationInfoRequest stationDetailsRequest = new StationInfoRequest();

            String code = retrieveStationRequest.getBody().getCode();
            String codeType = retrieveStationRequest.getBody().getCodeType();
            String beaconCode = retrieveStationRequest.getBody().getBeaconCode();
            RetrieveStationUserPositionRequest userPosition = retrieveStationRequest.getBody().getUserPosition();

            Boolean gpsError = false;
            Boolean beaconError = false;

            if (codeType.equals("GPS")) {

                // Recupera l'impianto pi� vicino alle coordinate inserite dall'utente
                String proxStationId = getTransactionServiceRemote().retrieveStationIdByCoords(userPosition.getLatitude(), userPosition.getLongitude());

                if (proxStationId == null) {

                    statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_STATION_PUMP_NOT_FOUND);
                    statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_RETRIEVE_STATION_PUMP_NOT_FOUND));

                    gpsError = true;
                }

                stationDetailsRequest.setPumpID(null);
                stationDetailsRequest.setRequestID(retrieveStationRequest.getCredential().getRequestID());
                stationDetailsRequest.setStationID(proxStationId);
                stationDetailsRequest.setPumpDetailsReq(false);

            }
            else {

                if (codeType.equals("BEACON-CODE")) {

                    String stationId = getTransactionServiceRemote().retrieveStationId(code);

                    if (stationId == null) {

                        statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_BEACON_CODE_NOT_FOUND);
                        statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_RETRIEVE_BEACON_CODE_NOT_FOUND));

                        beaconError = true;
                    }
                    else {

                        // Il servizio � stato richiamato passando in input il codice di una stazione
                        stationDetailsRequest.setPumpID(null);
                        stationDetailsRequest.setRequestID(retrieveStationRequest.getCredential().getRequestID());
                        stationDetailsRequest.setStationID(stationId);
                        stationDetailsRequest.setPumpDetailsReq(false);
                        isStationID = true;
                    }
                }
                else {

                    // Il servizio � stato richiamato passando in input il codice di un erogatore
                    stationDetailsRequest.setPumpID(code);
                    stationDetailsRequest.setRequestID(retrieveStationRequest.getCredential().getRequestID());
                    stationDetailsRequest.setStationID(null);
                    stationDetailsRequest.setPumpDetailsReq(true);
                    isStationID = false;
                }
            }

            if (!gpsError && !beaconError) {

                GetStationDetailsResponse getStationDetailsResponse = getForecourtInfoServiceRemote().getStationDetails(stationDetailsRequest.getRequestID(),
                        stationDetailsRequest.getStationID(), stationDetailsRequest.getPumpID(), stationDetailsRequest.getPumpDetailsReq());

                statusResponse.setStatusCode(getStationDetailsResponse.getStatusCode());
                statusResponse.setStatusMessage(getStationDetailsResponse.getMessageCode());

                if (getStationDetailsResponse.getStationDetail() != null) {

                    RetrieveStationBodyStationResponse retrieveStationBodyStationResponse = new RetrieveStationBodyStationResponse();

                    retrieveStationBodyStationResponse.setStationID(getStationDetailsResponse.getStationDetail().getStationID());

                    LocationData locationData = new LocationData();

                    String address = getStationDetailsResponse.getStationDetail().getAddress();
                    String city = getStationDetailsResponse.getStationDetail().getCity();
                    String province = getStationDetailsResponse.getStationDetail().getProvince();
                    String country = getStationDetailsResponse.getStationDetail().getCountry();

                    locationData.setAddress(address);
                    locationData.setCity(city);
                    locationData.setProvince(province);
                    locationData.setCountry(country);

                    try {
                        locationData.setLatitude(new Double(getStationDetailsResponse.getStationDetail().getLatitude()));
                        locationData.setLongitude(new Double(getStationDetailsResponse.getStationDetail().getLongitude()));
                    }
                    catch (Exception ex) {

                        locationData.setLatitude(0.0);
                        locationData.setLongitude(0.0);
                    }
                    retrieveStationBodyStationResponse.setLocationData(locationData);

                    retrieveStationBodyStationResponse.setName("");

                    RetrieveStationBodyResponse retrieveStationBodyResponse = new RetrieveStationBodyResponse();
                    retrieveStationBodyResponse.setStation(retrieveStationBodyStationResponse);
                    retrieveStationBodyResponse.setSelectedPumpID(code);

                    String outRangeThreshold;
                    try {
                        outRangeThreshold = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_RANGE_THRESHOLD);
                    }
                    catch (ParameterNotFoundException e) {
                        outRangeThreshold = "-1";
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    if (retrieveStationRequest.getBody().getUserPosition() != null) {

                        System.out.println("Distanza in metri: "
                                + GeoDataUtility.distance(retrieveStationRequest.getBody().getUserPosition().getLatitude(),
                                        retrieveStationRequest.getBody().getUserPosition().getLongitude(), locationData.getLatitude(), locationData.getLongitude(), "K") * 1000);

                        if (GeoDataUtility.distance(retrieveStationRequest.getBody().getUserPosition().getLatitude(),
                                retrieveStationRequest.getBody().getUserPosition().getLongitude(), locationData.getLatitude(), locationData.getLongitude(), "K") * 1000 > Double.valueOf(outRangeThreshold)) {
                            retrieveStationBodyResponse.setOutOfRange("true");
                        }
                        else {
                            retrieveStationBodyResponse.setOutOfRange("false");
                        }
                    }
                    else {
                        retrieveStationBodyResponse.setOutOfRange("true");
                    }

                    for (PumpDetail pumpDetail : getStationDetailsResponse.getStationDetail().getPumpDetails()) {

                        Pump pump = new Pump();

                        try {
                            if (!isStationID || (isStationID && !pumpDetail.getPumpStatus().contains("NOT"))) {
                                pump.setPumpID(pumpDetail.getPumpID());
                                pump.setStatus(pumpDetail.getPumpStatus());
                                pump.setNumber(new Integer(pumpDetail.getPumpNumber()));
                                for (ProductDetail productDetail : pumpDetail.getProductDetails()) {
                                    pump.getFuelType().add(productDetail.getProductDescription());
                                }
                                retrieveStationBodyStationResponse.getPumpList().add(pump);
                            }
                        }
                        catch (Exception ex) {

                            // TODO gestire l'eccezione
                            ex.printStackTrace();
                        }

                    }

                    retrieveStationResponse.setBody(retrieveStationBodyResponse);

                    // Aggiornamento delle informazioni sulla stazione sul backend
                    String result = getTransactionServiceRemote().refreshStationInfo(retrieveStationRequest.getCredential().getTicketID(),
                            getStationDetailsResponse.getStationDetail().getStationID(), getStationDetailsResponse.getStationDetail().getAddress(),
                            getStationDetailsResponse.getStationDetail().getCity(), getStationDetailsResponse.getStationDetail().getCountry(),
                            getStationDetailsResponse.getStationDetail().getProvince(), locationData.getLatitude(), locationData.getLongitude());

                    System.out.println("Refresh station info response: " + result);

                    Boolean rangeThresholdBlocking = null;

                    try {
                        rangeThresholdBlocking = Boolean.valueOf(getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_RANGE_THRESHOLD_BLOCKING));
                    }
                    catch (ParameterNotFoundException e) {
                        rangeThresholdBlocking = true;
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    if (rangeThresholdBlocking) {

                        System.out.println("Blocco utenti non in range attivo");

                        if (beaconCode == null || beaconCode.equals("")) {

                            System.out.println("beaconCode null or empty");

                            if (retrieveStationBodyResponse.getOutOfRange().equals("true")) {

                                System.out.println("user not in range");

                                statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE);
                                statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE));
                            }
                            else {

                                System.out.println("user in range");

                                retrieveStationResponse.getBody().setOutOfRange("false");
                            }
                        }
                        else {

                            System.out.println("beaconCode not null");

                            String stationIdFromBeaconCode = getTransactionServiceRemote().retrieveStationId(beaconCode);

                            if (stationIdFromBeaconCode == null) {

                                System.out.println("stationIdFromBeaconCode null");

                                System.out.println("user not in range");

                                statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE);
                                statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE));
                            }
                            else {

                                if (codeType.equals("BEACON-CODE")) {

                                    if (beaconCode.equals(code)) {

                                        System.out.println("beaconCode == code");

                                        if (retrieveStationRequest.getBody().getUserPosition() == null) {

                                            System.out.println("userPosition null");

                                            System.out.println("user in range");

                                            retrieveStationResponse.getBody().setOutOfRange("false");
                                        }
                                        else {

                                            System.out.println("userPosition not null");

                                            if (retrieveStationBodyResponse.getOutOfRange().equals("true")) {

                                                System.out.println("user not in range");

                                                statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE);
                                                statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE));
                                            }
                                            else {

                                                System.out.println("user in range");

                                                retrieveStationResponse.getBody().setOutOfRange("false");
                                            }
                                        }
                                    }
                                    else {

                                        System.out.println("beaconCode != code");

                                        System.out.println("user not in range");

                                        statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE);
                                        statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE));
                                    }
                                }
                                else {

                                    String stationIdFromCode = getStationDetailsResponse.getStationDetail().getStationID();

                                    System.out.println("stationIdFromCode: " + stationIdFromCode);

                                    if (stationIdFromBeaconCode.equals(stationIdFromCode)) {

                                        System.out.println("stationIdFromBeaconCode == code");

                                        if (retrieveStationRequest.getBody().getUserPosition() == null) {

                                            System.out.println("userPosition null");

                                            System.out.println("user in range");

                                            retrieveStationResponse.getBody().setOutOfRange("false");
                                        }
                                        else {

                                            System.out.println("userPosition not null");

                                            if (retrieveStationBodyResponse.getOutOfRange().equals("true")) {

                                                System.out.println("user not in range");

                                                statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE);
                                                statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE));
                                            }
                                            else {

                                                System.out.println("user in range");

                                                retrieveStationResponse.getBody().setOutOfRange("false");
                                            }
                                        }
                                    }
                                    else {

                                        System.out.println("stationIdFromBeaconCode != code");
                                        System.out.println("user not in range");

                                        statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE);
                                        statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_RETRIEVE_USER_NOT_IN_RANGE));
                                    }
                                }
                            }

                        }
                    }
                    else {

                        System.out.println("Blocco utenti non in range non attivo");
                    }

                    if (result.equals("REFRESH_STATION_400")) {
                        statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_STATION_PUMP_NOT_FOUND);
                    }
                }
            }
        }

        retrieveStationResponse.setStatus(statusResponse);

        return retrieveStationResponse;
    }

}
