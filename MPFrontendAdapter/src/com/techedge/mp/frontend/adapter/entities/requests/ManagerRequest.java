package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.manager.authentication.ManagerAuthenticationRequest;
import com.techedge.mp.frontend.adapter.entities.manager.logout.ManagerLogoutRequest;
import com.techedge.mp.frontend.adapter.entities.manager.rescuepassword.ManagerRescuePasswordRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievestation.ManagerRetrieveStationDetailsRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievetransactiondetail.RetrieveTransactionDetailRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions.ManagerRetrieveTransactionsRequest;
import com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers.ManagerRetrieveVouchersRequest;
import com.techedge.mp.frontend.adapter.entities.manager.updatepassword.ManagerUpdatePasswordRequest;

public class ManagerRequest extends RootRequest {

    protected ManagerAuthenticationRequest         authentication;
    protected ManagerLogoutRequest                 logout;
    protected ManagerRescuePasswordRequest         rescuePassword;
    protected ManagerUpdatePasswordRequest         updatePassword;
    protected ManagerRetrieveStationDetailsRequest retrieveStationDetails;
    protected ManagerRetrieveTransactionsRequest   retrieveTransactions;
    protected RetrieveTransactionDetailRequest     retrieveTransactionDetail;
    protected ManagerRetrieveVouchersRequest       retrieveVouchers;

    public ManagerRescuePasswordRequest getRescuePassword() {
        return rescuePassword;
    }

    public void setRescuePassword(ManagerRescuePasswordRequest rescuePassword) {
        this.rescuePassword = rescuePassword;
    }

    public ManagerUpdatePasswordRequest getUpdatePassword() {
        return updatePassword;
    }

    public void setUpdatePassword(ManagerUpdatePasswordRequest updatePassword) {
        this.updatePassword = updatePassword;
    }

    public ManagerAuthenticationRequest getAuthentication() {
        return authentication;
    }

    public void setAuthentication(ManagerAuthenticationRequest authentication) {
        this.authentication = authentication;
    }

    public ManagerLogoutRequest getLogout() {
        return logout;
    }

    public void setLogout(ManagerLogoutRequest logout) {
        this.logout = logout;
    }

    public ManagerRetrieveStationDetailsRequest getRetrieveStationDetails() {
        return retrieveStationDetails;
    }

    public void setRetrieveStationDetails(ManagerRetrieveStationDetailsRequest retrieveStationDetails) {
        this.retrieveStationDetails = retrieveStationDetails;
    }

    public void setRetrieveTransactions(ManagerRetrieveTransactionsRequest retrieveTransactions) {
        this.retrieveTransactions = retrieveTransactions;
    }

    public ManagerRetrieveTransactionsRequest getRetrieveTransactions() {
        return retrieveTransactions;
    }

    public RetrieveTransactionDetailRequest getRetrieveTransactionDetail() {
        return retrieveTransactionDetail;
    }

    public void setRetrieveTransactionDetail(RetrieveTransactionDetailRequest retrieveTransactionDetail) {
        this.retrieveTransactionDetail = retrieveTransactionDetail;
    }

    public ManagerRetrieveVouchersRequest getRetrieveVouchers() {
        return retrieveVouchers;
    }

    public void setRetrieveVouchers(ManagerRetrieveVouchersRequest retrieveVouchers) {
        this.retrieveVouchers = retrieveVouchers;
    }
}
