package com.techedge.mp.frontend.adapter.entities.user.v2.loadvoucher;

import com.techedge.mp.core.business.interfaces.LoadVoucherEventCampaignResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class LoadVoucherRequest extends AbstractRequest implements Validable {

    private Status                 status = new Status();

    private Credential             credential;
    private LoadVoucherBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public LoadVoucherBodyRequest getBody() {
        return body;
    }

    public void setBody(LoadVoucherBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("LOAD-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.USER_V2_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        LoadVoucherRequest loadVoucherRequest = this;

        String ticketID = loadVoucherRequest.getCredential().getTicketID();
        String requestID = loadVoucherRequest.getCredential().getRequestID();
        String voucherCode = loadVoucherRequest.getBody().getVoucherCode();
        String response = null;
        
        if (voucherCode.matches("^[a-zA-Z]+-(.)*")) {
            
            LoadVoucherEventCampaignResponse loadVoucherEventCampaignResponse = getUserV2ServiceRemote().loadVoucherEventCampaign(ticketID, requestID, voucherCode);
            
            if (loadVoucherEventCampaignResponse.getStatusCode().equals(ResponseHelper.USER_LOAD_VOUCHER_VALID)) {
                
                String message = ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_SUCCESS;
                
                if (message.contains("%PROMO_NAME%")) {
                    message.replace("%PROMO_NAME%", loadVoucherEventCampaignResponse.getPromoName());
                }
                else {
                    message = prop.getProperty(ResponseHelper.USER_LOAD_VOUCHER_VALID);
                }
                
                status.setStatusCode(loadVoucherEventCampaignResponse.getStatusCode());
                status.setStatusMessage(message);
            }
            else {
                
                status.setStatusCode(loadVoucherEventCampaignResponse.getStatusCode());
                status.setStatusMessage(prop.getProperty(loadVoucherEventCampaignResponse.getStatusCode()));
            }
            
            LoadVoucherResponse loadVoucherResponse = new LoadVoucherResponse();

            loadVoucherResponse.setStatus(status);

            return loadVoucherResponse;
        }
        else {
            
            if (voucherCode.length() > 16) {

                LoadVoucherResponse loadVoucherResponse = new LoadVoucherResponse();
                
                status.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_NOT_VALID);
                status.setStatusMessage(prop.getProperty(ResponseHelper.USER_LOAD_VOUCHER_NOT_VALID));
                
                loadVoucherResponse.setStatus(status);
                
                return loadVoucherResponse;
            }
            
            response = getUserServiceRemote().loadVoucher(ticketID, requestID, voucherCode);
            
            LoadVoucherResponse loadVoucherResponse = new LoadVoucherResponse();

            status.setStatusCode(response);
            status.setStatusMessage(prop.getProperty(response));

            loadVoucherResponse.setStatus(status);

            return loadVoucherResponse;
        }
    }
}
