package com.techedge.mp.frontend.adapter.entities.user.updatetermsofservice;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.TermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class UpdateTermsOfServiceRequest extends AbstractRequest implements Validable {

    private Status                          status = new Status();

    private Credential                      credential;
    private UpdateTermsOfServiceRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateTermsOfServiceRequestBody getBody() {
        return body;
    }

    public void setBody(UpdateTermsOfServiceRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PWD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.USER_UPDATE_TERMS_OF_SERVICE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        UpdateTermsOfServiceRequest updateTermsOfServiceRequest = this;
        String response;
        List<TermsOfService> termsOfServiceList = new ArrayList<TermsOfService>(0);
        if (updateTermsOfServiceRequest.getBody() == null) {
            response = StatusCode.USER_UPDATE_TERMS_OF_SERVICE_INVALID_REQUEST;
        }
        else {
            for (TermsOfServiceData termsOfServiceData : updateTermsOfServiceRequest.getBody().getTermsOfService()) {

                TermsOfService termsOfService = new TermsOfService();
                termsOfService.setAccepted(termsOfServiceData.getAccepted());
                termsOfService.setKeyval(termsOfServiceData.getId());
                termsOfServiceList.add(termsOfService);
            }

            response = getUserServiceRemote().updateTermsOfService(updateTermsOfServiceRequest.getCredential().getTicketID(),
                    updateTermsOfServiceRequest.getCredential().getRequestID(), termsOfServiceList);
        }
        UpdateTermsOfServiceResponse updateTermsOfServiceResponse = new UpdateTermsOfServiceResponse();

        status.setStatusCode(response);

        status.setStatusMessage(prop.getProperty(response));

        updateTermsOfServiceResponse.setStatus(status);

        return updateTermsOfServiceResponse;
    }
}
