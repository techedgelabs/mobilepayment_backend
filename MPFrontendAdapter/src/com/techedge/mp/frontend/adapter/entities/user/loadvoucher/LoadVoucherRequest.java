package com.techedge.mp.frontend.adapter.entities.user.loadvoucher;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class LoadVoucherRequest extends AbstractRequest implements Validable {

    private Status                 status = new Status();

    private Credential             credential;
    private LoadVoucherBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public LoadVoucherBodyRequest getBody() {
        return body;
    }

    public void setBody(LoadVoucherBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("LOAD-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        LoadVoucherRequest loadVoucherRequest = this;

        String response = getUserServiceRemote().loadVoucher(loadVoucherRequest.getCredential().getTicketID(), loadVoucherRequest.getCredential().getRequestID(),
                loadVoucherRequest.getBody().getVoucherCode());

        LoadVoucherResponse loadVoucherResponse = new LoadVoucherResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        loadVoucherResponse.setStatus(status);

        return loadVoucherResponse;

    }

}
