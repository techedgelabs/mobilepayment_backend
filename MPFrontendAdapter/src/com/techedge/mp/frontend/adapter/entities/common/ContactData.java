package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ContactData implements Validable {

    private List<MobilePhone> mobilePhones = new ArrayList<MobilePhone>(0);

    public ContactData() {}

    public List<MobilePhone> getMobilePhones() {
        return mobilePhones;
    }

    public void setMobilePhones(List<MobilePhone> mobilePhones) {
        this.mobilePhones = mobilePhones;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (mobilePhones.isEmpty()) {
            status.setStatusCode(StatusCode.USER_CHECK_MOBILE_PHONE_REQUIRED);
            return status;
        }
        else {
            for (MobilePhone mobilePhone : mobilePhones) {
                if (mobilePhone.getNumber() == null || mobilePhone.getPrefix() == null ) {
                    status.setStatusCode(StatusCode.USER_CHECK_MOBILE_PHONE_WRONG);
                    return status;
                }
                if (mobilePhone.getNumber().length() > 20 || mobilePhone.getPrefix().length() > 10 || mobilePhone.getPrefix().contains("[a-zA-Z]+") == true
                        || mobilePhone.getNumber().matches("[a-zA-Z]+") == true) {
                    status.setStatusCode(StatusCode.USER_CHECK_MOBILE_PHONE_WRONG);
                    return status;
                }
            }
        }

        status.setStatusCode(StatusCode.USER_CHECK_SUCCESS);

        return status;
    }

}
