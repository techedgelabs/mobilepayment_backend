package com.techedge.mp.frontend.adapter.entities.user.update;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class UpdateUserPersonalDataRequest extends AbstractRequest implements Validable {

    private Status                            status = new Status();

    private Credential                        credential;
    private UpdateUserPersonalDataRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateUserPersonalDataRequestBody getBody() {
        return body;
    }

    public void setBody(UpdateUserPersonalDataRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_UPD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        UpdateUserPersonalDataRequest updateUserRequest = this;
        UpdateUserResponse updateUserResponse = new UpdateUserResponse();
        String response = null;
        Date birthDate = null;

        if (updateUserRequest.getBody().getUserPersonalData().getDateOfBirth() != null) {

            String birthDateSting = updateUserRequest.getBody().getUserPersonalData().getDateOfBirth().getYear() + "-"
                    + updateUserRequest.getBody().getUserPersonalData().getDateOfBirth().getMonth() + "-"
                    + updateUserRequest.getBody().getUserPersonalData().getDateOfBirth().getDay();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

            try {
                birthDate = sdf.parse(birthDateSting);

                User user = new User();

                PersonalData personalData = new PersonalData();

                personalData.setFirstName(updateUserRequest.getBody().getUserPersonalData().getFirstName());
                personalData.setLastName(updateUserRequest.getBody().getUserPersonalData().getLastName());
                personalData.setBirthDate(birthDate);
                personalData.setBirthMunicipality(updateUserRequest.getBody().getUserPersonalData().getBirthMunicipality());
                personalData.setBirthProvince(updateUserRequest.getBody().getUserPersonalData().getBirthProvince());
                personalData.setLanguage(updateUserRequest.getBody().getUserPersonalData().getLanguage());
                personalData.setSex(updateUserRequest.getBody().getUserPersonalData().getSex());

                user.setPersonalData(personalData);

                response = getUserServiceRemote().updateUser(updateUserRequest.getCredential().getTicketID(), updateUserRequest.getCredential().getRequestID(), user);

                status.setStatusCode(response);
                status.setStatusMessage(prop.getProperty(response));

            }
            catch (ParseException e) {
                getLoggerServiceRemote().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "userJsonHandler", null, "parse exception", e.getMessage());
                response = StatusCode.USER_UPD_SYSTEM_ERROR;
            }
        }

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        updateUserResponse.setStatus(status);

        return updateUserResponse;

    }

}
