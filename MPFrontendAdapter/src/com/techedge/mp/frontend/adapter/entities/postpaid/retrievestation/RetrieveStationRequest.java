package com.techedge.mp.frontend.adapter.entities.postpaid.retrievestation;

import java.text.DecimalFormat;

import com.techedge.mp.core.business.interfaces.CashInfo;
import com.techedge.mp.core.business.interfaces.ExtendedPumpInfo;
import com.techedge.mp.core.business.interfaces.GetStationInfo;
import com.techedge.mp.core.business.interfaces.GetStationResponse;
import com.techedge.mp.core.business.interfaces.OperationType;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Cash;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.StationLocationData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveStationRequest extends AbstractRequest implements Validable {

    private Credential                 credential;
    private RetrieveStationBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveStationBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveStationBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("STATION-RETRIEVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        RetrieveStationRequest retrieveStationRequest = this;

        RetrieveStationResponse retrieveStationResponse = new RetrieveStationResponse();

        Status statusResponse = new Status();

        // Verifica se l'utente pu� effettuare una richiesta al servizio
        // retrieveStation
        String response = getUserServiceRemote().checkAuthorization(retrieveStationRequest.getCredential().getTicketID(), OperationType.RETRIEVE_STATION);

        if (!response.equals(ResponseHelper.CHECK_AUTHORIZATION_SUCCESS)) {

            statusResponse.setStatusCode(response);
            statusResponse.setStatusMessage(prop.getProperty(response));
        }
        else {

            Double userPositionLatitude = null;
            Double userPositionLongitude = null;

            if (retrieveStationRequest.getBody().getUserPosition() != null) {

                userPositionLatitude = retrieveStationRequest.getBody().getUserPosition().getLatitude();
                userPositionLongitude = retrieveStationRequest.getBody().getUserPosition().getLongitude();
            }

            GetStationResponse getStationsResponse = getPostPaidTransactionServiceRemote().getStation(retrieveStationRequest.getCredential().getRequestID(),
                    retrieveStationRequest.getCredential().getTicketID(), retrieveStationRequest.getBody().getCodeType(), retrieveStationRequest.getBody().getBeaconCode(),
                    userPositionLatitude, userPositionLongitude);

            if (!getStationsResponse.getStatusCode().equals("STATION_RETRIEVE_200")) {

                statusResponse.setStatusCode(getStationsResponse.getStatusCode());
                statusResponse.setStatusMessage(prop.getProperty(getStationsResponse.getStatusCode()));
            }
            else {

                RetrieveStationBodyResponse retrieveStationBodyResponse = new RetrieveStationBodyResponse();

                if (!getStationsResponse.getStationInfoList().isEmpty()) {

                    for (GetStationInfo stationInfo : getStationsResponse.getStationInfoList()) {

                        //System.out.println("station: " + stationInfo.getStationId());
                        //System.out.println("distance: " + stationInfo.getDistance());

                        RetrieveStationBodyStationResponse station = new RetrieveStationBodyStationResponse();

                        station.setStationID(stationInfo.getStationId());

                        LocationData locationData = composeLocatinData(stationInfo);
                        station.setLocationData(locationData);

                        for (ExtendedPumpInfo extendedPumpInfo : stationInfo.getPumpList()) {

                            Pump pump = new Pump();

                            pump.setPumpID(extendedPumpInfo.getPumpId());
                            pump.setStatus(null);
                            pump.setNumber(new Integer(extendedPumpInfo.getNumber()));

                            for (String fuelType : extendedPumpInfo.getFuelType()) {

                                pump.getFuelType().add(fuelType);
                            }

                            station.getPumpList().add(pump);

                        }

                        for (CashInfo cashInfo : stationInfo.getCashList()) {

                            Cash cash = new Cash();

                            cash.setCashID(cashInfo.getCashId());
                            cash.setNumber(new Integer(cashInfo.getNumber()));

                            station.getCashList().add(cash);

                        }

                        retrieveStationBodyResponse.getStations().add(station);

                    }

                }

                if (getStationsResponse.getOutOfRange()) {
                    retrieveStationBodyResponse.setOutOfRange("true");
                }
                else {
                    retrieveStationBodyResponse.setOutOfRange("false");
                }

                retrieveStationResponse.setBody(retrieveStationBodyResponse);

                statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);
                statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_RETRIEVE_SUCCESS));

                retrieveStationResponse.setStatus(statusResponse);
            }
        }

        retrieveStationResponse.setStatus(statusResponse);

        return retrieveStationResponse;
    }

    private StationLocationData composeLocatinData(GetStationInfo stationInfo) {

        StationLocationData stationLocationData = new StationLocationData();

        String address = stationInfo.getAddress();
        String city = stationInfo.getCity();
        String province = stationInfo.getProvince();
        String country = stationInfo.getCountry();

        stationLocationData.setAddress(address);
        stationLocationData.setCity(city);
        stationLocationData.setProvince(province);
        stationLocationData.setCountry(country);

        try {
            stationLocationData.setLatitude(new Double(stationInfo.getLatitude()));
            stationLocationData.setLongitude(new Double(stationInfo.getLongitude()));
        }
        catch (Exception ex) {

            stationLocationData.setLatitude(0.0);
            stationLocationData.setLongitude(0.0);
        }

        if (stationInfo.getDistance() != null) {
            stationLocationData.setDistance(formatDouble(stationInfo.getDistance()));
        }
        else {
            stationLocationData.setDistance(null);
        }

        return stationLocationData;
    }

    private Double formatDouble(Double distance) {

        DecimalFormat formatter = new DecimalFormat("#0.00");

        //System.out.println("distance: " + distance);

        String doubleString = formatter.format(distance);
        String replaced = doubleString.replace(",", ".");

        Double distanceFormatted = Double.parseDouble(replaced);

        return distanceFormatted;
    }
}
