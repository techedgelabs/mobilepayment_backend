package com.techedge.mp.frontend.adapter.entities.common;

import java.util.List;
import java.util.ArrayList;


public class ReceiptDynamicData {

	private String label;
	private String unit;
	private String key;
	private String value;
	private Integer position;
	private List<ReceiptDynamicData> child = new ArrayList<ReceiptDynamicData>(0);
	private String link;
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getKey() {
		return key;
	}
	public void setKey(String key) {
		this.key = key;
	}
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	public Integer getPosition() {
		return position;
	}
	public void setPosition(Integer position) {
		this.position = position;
	}
	
	public String getLink() {
		return link;
	}
	public void setLink(String link) {
		this.link = link;
	}
    public List<ReceiptDynamicData> getChild() {
        return child;
    }
    public void setChild(List<ReceiptDynamicData> child) {
        this.child = child;
    }
}
