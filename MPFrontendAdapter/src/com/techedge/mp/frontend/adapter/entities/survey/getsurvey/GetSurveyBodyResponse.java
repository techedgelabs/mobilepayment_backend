package com.techedge.mp.frontend.adapter.entities.survey.getsurvey;

import com.techedge.mp.frontend.adapter.entities.common.Survey;

public class GetSurveyBodyResponse {

    private Survey survey;

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }
}
