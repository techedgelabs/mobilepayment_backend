package com.techedge.mp.frontend.adapter.entities.user.v2.redemptionaward;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class GetRedemptionAwardBodyRequest implements Validable {

    private String awardId;
    private String cardPartner;

    public String getAwardId() {
        return awardId;
    }

    public void setAwardId(String awardId) {
        this.awardId = awardId;
    }

    public String getCardPartner() {
        return cardPartner;
    }

    public void setCardPartner(String cardPartner) {
        this.cardPartner = cardPartner;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.awardId == null || this.awardId.isEmpty()) {

            status.setStatusCode(StatusCode.USER_REDEMPTION_AWARD_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.USER_REDEMPTION_AWARD_SUCCESS);

        return status;
    }

}
