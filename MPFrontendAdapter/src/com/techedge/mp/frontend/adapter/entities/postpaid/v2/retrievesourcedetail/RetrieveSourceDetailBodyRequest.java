package com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievesourcedetail;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.refuel.retrievestation.RetrieveStationUserPositionRequest;

public class RetrieveSourceDetailBodyRequest implements Validable {

    private String                             codeType;
    private String                             sourceID;
    private RetrieveStationUserPositionRequest userPosition;

    public String getCodeType() {
        return codeType;
    }

    public void setCodeType(String codeType) {
        this.codeType = codeType;
    }

    public String getSourceID() {
        return sourceID;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    public RetrieveStationUserPositionRequest getUserPosition() {
        return userPosition;
    }

    public void setUserPosition(RetrieveStationUserPositionRequest userPosition) {
        this.userPosition = userPosition;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.codeType == null || this.codeType.trim() == "" || this.codeType.length() > 40) {

            status.setStatusCode(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG);

            return status;
        }

        if (!this.codeType.equals("QR-CODE") && !this.codeType.equals("PLAIN-CODE")) {

            status.setStatusCode(StatusCode.STATION_RETRIEVE_CODE_TYPE_WRONG);

            return status;
        }

        if (this.sourceID == null || this.sourceID.trim() == "" || this.sourceID.length() > 40) {

            status.setStatusCode(StatusCode.STATION_RETRIEVE_CODE_WRONG);

            return status;
        }

        if (this.userPosition != null) {

            status = this.userPosition.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }

        status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);

        return status;
    }

}