package com.techedge.mp.frontend.adapter.entities.user.authentication;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class AuthenticationUserRequestBody implements Validable {

    private String username;
    private String password;
    private String requestID;
    private String deviceID;
    private String deviceName;
    private String deviceToken;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getDeviceID() {
        return deviceID;
    }

    public void setDeviceID(String deviceID) {
        this.deviceID = deviceID;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.username == null || this.username.length() > 50) {

            status.setStatusCode(StatusCode.USER_AUTH_EMAIL_WRONG);

            return status;

        }

        if (this.password == null || this.password.length() > 40) {

            status.setStatusCode(StatusCode.USER_AUTH_PASSWORD_WRONG);

            return status;

        }

        if (this.requestID == null || this.requestID.length() > 20 || this.requestID.trim() == "") {

            status.setStatusCode(StatusCode.USER_AUTH_FAILURE);

            return status;

        }

        if (this.deviceID != null) {

            if (this.deviceID.length() > 40 || this.deviceID.trim() == "") {

                status.setStatusCode(StatusCode.USER_AUTH_FAILURE);

                return status;

            }
        }

        if (this.deviceName != null) {

            if (this.deviceName.length() > 100 || this.deviceName.trim() == "") {

                status.setStatusCode(StatusCode.USER_AUTH_FAILURE);

                return status;

            }
        }

        if (this.deviceToken != null && this.deviceToken.isEmpty()) {

                status.setStatusCode(StatusCode.USER_AUTH_FAILURE);

                return status;
        }
        
        status.setStatusCode(StatusCode.USER_AUTH_SUCCESS);

        return status;
    }

}
