package com.techedge.mp.frontend.adapter.entities.user.removeactivedevice;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RemoveActiveDeviceBodyRequest implements Validable {

    private String userDeviceID;

    public String getUserDeviceID() {
        return userDeviceID;
    }

    public void setUserDeviceID(String userDeviceID) {
        this.userDeviceID = userDeviceID;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.userDeviceID == null) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_REMOVE_DEVICE_SUCCESS);

        return status;

    }

}
