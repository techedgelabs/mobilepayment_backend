package com.techedge.mp.frontend.adapter.entities.user.v2.getawardlist;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetAwardListResponse extends BaseResponse {

    private GetAwardListBodyResponse body;

    public GetAwardListBodyResponse getBody() {
        return body;
    }

    public void setBody(GetAwardListBodyResponse body) {
        this.body = body;
    }

}
