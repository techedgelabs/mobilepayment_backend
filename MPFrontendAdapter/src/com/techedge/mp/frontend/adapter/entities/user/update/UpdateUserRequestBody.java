package com.techedge.mp.frontend.adapter.entities.user.update;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class UpdateUserRequestBody implements Validable {

	private UpdateUserDataRequest userData;

	public UpdateUserDataRequest getUserData() {
		return userData;
	}

	public void setUserData(UpdateUserDataRequest userData) {
		this.userData = userData;
	}

	@Override
	public Status check() {

		Status status = new Status();
		
		if(this.userData != null) {
			
			status = this.userData.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
		} else {
			
			status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.USER_UPD_SUCCESS);
		
		return status;
		
	}
	
}
