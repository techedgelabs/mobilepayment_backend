package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbyqrcode;

import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;



public class RetrievePoPTransactionByQRcodeRequest implements Validable {
	
	private Credential credential;
	private RetrievePoPTransactionByQRcodeBodyRequest body;
	
	
	public Credential getCredential() {
		return credential;
	}
	public void setCredential(Credential credential) {
		this.credential = credential;
	}
	
	public RetrievePoPTransactionByQRcodeBodyRequest getBody() {
		return body;
	}
	public void setBody(RetrievePoPTransactionByQRcodeBodyRequest body) {
		this.body = body;
	}
	
	
	@Override
	public Status check() {

		Status status = new Status();
		
		status = Validator.checkCredential("CONFIRM-REFUEL", credential);
		
		if(!Validator.isValid(status.getStatusCode())) {
			
			return status;
			
		}
		
		if(this.body != null) {
			
			status = this.body.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
			
			return status;
			
		}

		return status;
	}
	
}