package com.techedge.mp.frontend.adapter.entities.common;

public class RetrieveStationCodeHelper {
	
	public static final Integer CODE_TYPE_STATION = 0;
	public static final Integer CODE_TYPE_PUMP    = 1;
	
	private static final String PUMP_PREFIX = "P";
	
	public static Integer getType(String code) {
		
		if ( code == null )
			return null;
		
		if ( code.startsWith( PUMP_PREFIX ) ) {	
			return CODE_TYPE_PUMP;
		}
		
		return CODE_TYPE_STATION;
	}
}
