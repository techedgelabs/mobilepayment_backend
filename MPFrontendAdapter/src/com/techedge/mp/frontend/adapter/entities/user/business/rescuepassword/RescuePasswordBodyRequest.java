package com.techedge.mp.frontend.adapter.entities.user.business.rescuepassword;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RescuePasswordBodyRequest implements Validable {

	private String email;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		status = Validator.checkEmail("AUTH", email);
		
		if (!Validator.isValid(status.getStatusCode())) {
			status.setStatusCode(StatusCode.USER_BUSINESS_RESCUE_PASSWORD_ERROR);
			return status;
		}
				
		return status;
	}
}