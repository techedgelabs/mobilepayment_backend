package com.techedge.mp.frontend.adapter.entities.refuel.v2.createmulticardtransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateMulticardRefuelTransactionBodyRequest implements Validable {

    private CreateMulticardRefuelTransactionDataRequest multicardRefuelPaymentData;

    public CreateMulticardRefuelTransactionDataRequest getMulticardRefuelPaymentData() {
        return multicardRefuelPaymentData;
    }

    public void setMulticardRefuelPaymentData(CreateMulticardRefuelTransactionDataRequest multicardRefuelPaymentData) {
        this.multicardRefuelPaymentData = multicardRefuelPaymentData;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.multicardRefuelPaymentData != null) {

            status = this.multicardRefuelPaymentData.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.MULTICARD_REFUEL_TRANSACTION_CREATE_SUCCESS);

        return status;

    }

}
