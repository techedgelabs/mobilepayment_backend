package com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail;

import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Receipt;
import com.techedge.mp.frontend.adapter.entities.common.Station;


public class RetrieveRefuelPaymentDetailBodyResponse {

	private String refuelID;
    private String status;
    private String subStatus;
    private Boolean useVoucher;
    private Integer initialAmount;
    private Receipt receipt;
    private Station station;
    private Pump pump;
	
    public String getRefuelID() {
		return refuelID;
	}
	public void setRefuelID(String refuelID) {
		this.refuelID = refuelID;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public String getSubStatus() {
		return subStatus;
	}
	public void setSubStatus(String subStatus) {
		this.subStatus = subStatus;
	}
	
	public Boolean getUseVoucher() {
		return useVoucher;
	}
	public void setUseVoucher(Boolean useVoucher) {
		this.useVoucher = useVoucher;
	}
	
	public Integer getInitialAmount() {
		return initialAmount;
	}
	public void setInitialAmount(Integer initialAmount) {
		this.initialAmount = initialAmount;
	}
	
	public Receipt getReceipt() {
		return receipt;
	}
	public void setReceipt(Receipt receipt) {
		this.receipt = receipt;
	}
	
	public Station getStation() {
		return station;
	}
	public void setStation(Station station) {
		this.station = station;
	}
	
	public Pump getPump() {
		return pump;
	}
	public void setPump(Pump pump) {
		this.pump = pump;
	}
}
