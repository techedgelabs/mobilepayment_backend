package com.techedge.mp.frontend.adapter.entities.manager.retrievetransactiondetail;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import com.techedge.mp.core.business.interfaces.PostPaidCart;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.PostPaidRefuel;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Cash;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.POPStatusConverter;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptDynamic;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidExtendedRefuelData;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveTransactionDetailRequest extends AbstractRequest implements Validable {

    private Status                               status = new Status();

    private Credential                           credential;
    private RetrieveTransactionDetailBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveTransactionDetailBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveTransactionDetailBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DETAIL-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;
            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
            return status;
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        com.techedge.mp.frontend.adapter.entities.manager.retrievetransactiondetail.RetrieveTransactionDetailRequest retrievePopTransactionDetailRequest = this;
        com.techedge.mp.frontend.adapter.entities.manager.retrievetransactiondetail.RetrieveTransactionDetailResponse retrievePopTransactionDetailResponse = new com.techedge.mp.frontend.adapter.entities.manager.retrievetransactiondetail.RetrieveTransactionDetailResponse();

        PostPaidGetTransactionDetailResponse postPaidGetTransactionDetailResponse = getManagerServiceRemote().getTransactionDetail(
                retrievePopTransactionDetailRequest.getCredential().getRequestID(), retrievePopTransactionDetailRequest.getCredential().getTicketID(),
                retrievePopTransactionDetailRequest.getBody().getTransactionId());
        // TODO compilare l'output;
        status.setStatusCode(postPaidGetTransactionDetailResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(postPaidGetTransactionDetailResponse.getStatusCode()));
        retrievePopTransactionDetailResponse.setStatus(status);

        // TODO compilare l'output;
        com.techedge.mp.frontend.adapter.entities.manager.retrievetransactiondetail.RetrieveTransactionDetailBodyResponse retrievePopTransactionDetailBodyResponse = new com.techedge.mp.frontend.adapter.entities.manager.retrievetransactiondetail.RetrieveTransactionDetailBodyResponse();
        retrievePopTransactionDetailBodyResponse.setSourceStatus(postPaidGetTransactionDetailResponse.getSourceStatus());
        retrievePopTransactionDetailBodyResponse.setSourceType(postPaidGetTransactionDetailResponse.getSourceType());

        if (!status.getStatusCode().contains("200")) {
            retrievePopTransactionDetailResponse.setStatus(status);
            return retrievePopTransactionDetailResponse;
        }

        // station
        Station station = new Station();
        station.setStationID(postPaidGetTransactionDetailResponse.getStation().getStationID());

        LocationData locationData = new LocationData();
        locationData.setAddress(postPaidGetTransactionDetailResponse.getStation().getAddress());
        locationData.setCity(postPaidGetTransactionDetailResponse.getStation().getCity());
        locationData.setCountry(postPaidGetTransactionDetailResponse.getStation().getCountry());
        locationData.setLatitude(postPaidGetTransactionDetailResponse.getStation().getLatitude());
        locationData.setLongitude(postPaidGetTransactionDetailResponse.getStation().getLongitude());
        locationData.setProvince(postPaidGetTransactionDetailResponse.getStation().getProvince());
        station.setLocationData(locationData);

        retrievePopTransactionDetailBodyResponse.setStation(station);
        // pump
        /*
         * if (postPaidGetTransactionDetailResponse.getPumpID() != null) {
         * 
         * Pump pump = new Pump();
         * 
         * pump.setPumpID(postPaidGetTransactionDetailResponse.getPumpID());
         * pump.setStatus(postPaidGetTransactionDetailResponse.getPumpStatus());
         * pump.setNumber(Integer.valueOf(postPaidGetTransactionDetailResponse.getPumpNumber()));
         * pump.getFuelType().add(postPaidGetTransactionDetailResponse.getPumpFuelType());
         * 
         * retrievePopTransactionDetailBodyResponse.setPump(pump);
         * }
         */

        // cash
        if (postPaidGetTransactionDetailResponse.getCashID() != null) {

            Cash cash = new Cash();

            cash.setCashID(postPaidGetTransactionDetailResponse.getCashID());
            if (postPaidGetTransactionDetailResponse.getCashNumber() != null) {
                cash.setNumber(Integer.valueOf(postPaidGetTransactionDetailResponse.getCashNumber()));
            }

            retrievePopTransactionDetailBodyResponse.setCash(cash);
        }

        // transaction
        PostPaidTransactionHistoryData transaction = new PostPaidTransactionHistoryData();
        ReceiptDynamic receipt = new ReceiptDynamic();

        transaction.setAmount(AmountConverter.toMobile(postPaidGetTransactionDetailResponse.getAmount()));
        transaction.setCreationTimestamp(null);

        if (postPaidGetTransactionDetailResponse.getCreationTimestamp() != null) {
            transaction.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(postPaidGetTransactionDetailResponse.getCreationTimestamp().getTime())));
        }

        // Trascodifica dello stato
        if (postPaidGetTransactionDetailResponse.getStatus() == null) {
            transaction.setStatus(null);
        }
        else {
            String localStatus = POPStatusConverter.toAppStatus(postPaidGetTransactionDetailResponse.getStatus());

            if (!localStatus.isEmpty()) {
                transaction.setStatus(localStatus);
            }
            else {
                transaction.setStatus(postPaidGetTransactionDetailResponse.getStatus());
            }
        }

        transaction.setSubStatus(postPaidGetTransactionDetailResponse.getSubStatus());
        transaction.setTransactionID(postPaidGetTransactionDetailResponse.getMpTransactionID());
        transaction.setUseVoucher(postPaidGetTransactionDetailResponse.getUseVoucher());

        Double voucherAmount = 0.0;
        int position = 1;
        int position2 = 1;
        int parentIndex = -1;
        if (!postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList().isEmpty()) {
            for (PostPaidConsumeVoucher voucherConsume : postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList()) {
                for (PostPaidConsumeVoucherDetail voucherDetail : voucherConsume.getPostPaidConsumeVoucherDetail()) {
                    parentIndex = receipt.addToVoucherData(-1, "VOUCHER_NAME", "NOME", null, voucherDetail.getPromoDescription(), position++, null);
                    String consumedValue = "0.0";
                    if (voucherDetail.getConsumedValue() != null) {
                        voucherAmount += (voucherDetail.getConsumedValue() * 100);
                        consumedValue = voucherDetail.getConsumedValue().toString();
                    }
                    receipt.addToVoucherData(parentIndex, "VOUCHER_AMOUNT", "IMPORTO", "�", consumedValue, position++, null);
                    receipt.addToVoucherData(parentIndex, "MESSAGE", "", null, voucherDetail.getPromoDescription(), position2++, null);
                }
            }
        }

        transaction.setVoucherAmount(voucherAmount.intValue());

        Integer loyaltyBalance = 0;
        position = 1;
        parentIndex = -1;
        if (!postPaidGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList().isEmpty()) {
            for (PostPaidLoadLoyaltyCredits loyaltyCredits : postPaidGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList()) {
                parentIndex = receipt.addToLoyaltyData(-1, "LOYALTY_PAN", "PAN", null, loyaltyCredits.getEanCode(), position++, null);
                String credits = "0";
                String balanceAmount = "0";
                if (loyaltyCredits.getCredits() != null) {
                    loyaltyBalance += loyaltyCredits.getCredits();
                    credits = loyaltyCredits.getCredits().toString();
                }

                if (loyaltyCredits.getBalance() != null) {
                    balanceAmount = loyaltyCredits.getBalance().toString();
                }

                receipt.addToLoyaltyData(parentIndex, "LOYALTY_BALANCE", "PUNTI FEDELTA'", null, credits, position++, null);
                receipt.addToLoyaltyData(parentIndex, "LOYALTY_TOTAL", "SALDO PUNTI", null, balanceAmount, position++, null);
            }
        }

        transaction.setLoyaltyBalance(loyaltyBalance);

        position = 1;
        if (postPaidGetTransactionDetailResponse.getCartList() != null) {
            for (PostPaidCart cartData : postPaidGetTransactionDetailResponse.getCartList()) {
                String amount = "0.0";
                if (cartData.getAmount() != null) {
                    amount = cartData.getAmount().toString();
                }
                receipt.addToShopData(-1, "SHOP_AMOUNT", "IMPORTO", "�", amount, position++, null);
            }
        }

        position = 1;
        if (postPaidGetTransactionDetailResponse.getRefuelList() != null) {
            int index = 1;
            for (PostPaidRefuel postPaidRefuel : postPaidGetTransactionDetailResponse.getRefuelList()) {
                String amount = "0.0";
                String quantity = "0.0";
                PostPaidExtendedRefuelData postPaidRefuelJsonData = new PostPaidExtendedRefuelData();
                postPaidRefuelJsonData.setFuelAmount(postPaidRefuel.getFuelAmount());
                postPaidRefuelJsonData.setFuelQuantity(postPaidRefuel.getFuelQuantity());
                postPaidRefuelJsonData.setFuelType(postPaidRefuel.getFuelType());
                postPaidRefuelJsonData.setProductDescription(postPaidRefuel.getProductDescription());
                postPaidRefuelJsonData.setProductId(postPaidRefuel.getProductId());
                postPaidRefuelJsonData.setPumpId(postPaidRefuel.getPumpId());

                transaction.getRefuel().add(postPaidRefuelJsonData);

                if (postPaidRefuel.getFuelQuantity() != null) {
                    quantity = postPaidRefuel.getFuelQuantity().toString();
                }

                receipt.addToRefuelData(-1, "REFUEL_FUEL_QUANTITY", "EROGATO", "lt", quantity, position++, null);
                receipt.addToRefuelData(-1, "REFUEL_FUEL_TYPE", "TIPO DI CARBURANTE", null, postPaidRefuel.getFuelType(), position++, null);
                receipt.addToRefuelData(-1, "REFUEL_PUMP_NUMBER", "EROGATORE", null, postPaidRefuel.getPumpId(), position++, null);
                receipt.addToRefuelData(-1, "REFUEL_FUEL_QUANTITY", "EROGATO", "lt", quantity, position++, null);

                if (postPaidRefuel.getFuelAmount() != null) {
                    amount = postPaidRefuel.getFuelAmount().toString();
                }

                receipt.addToRefuelData(-1, "REFUEL_AMOUNT", "IMPORTO", "�", amount, position++, null);

                if (!postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList().isEmpty()
                        && postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList().size() >= (index - 1)) {

                    Object[] voucherConsumeList = postPaidGetTransactionDetailResponse.getPostPaidConsumeVoucherList().toArray();
                    PostPaidConsumeVoucher voucherConsume = (PostPaidConsumeVoucher) voucherConsumeList[index - 1];
                    receipt.addToRefuelData(-1, "REFUEL_WARNING", "", null, voucherConsume.getWarningMsg(), position++, null);
                }

                //receipt.addToRefuelData(rootIndex, -1, "REFUEL_WARNING_" + index, "", null, "Non so cosa passarti...", position++, null);
                receipt.addToRefuelData(-1, "SEPARATOR", "", null, null, position++, null);

                index++;
            }
        }

        position = 1;
        receipt.addToPaymentData(-1, "STATION_ID", "CODICE PV", "", postPaidGetTransactionDetailResponse.getStation().getStationID(), position++, null);
        receipt.addToPaymentData(-1, "C_AUTH", "COD AUTORIZZAZIONE", "", postPaidGetTransactionDetailResponse.getAuthorizationCode(), position++, null);

        String data = "";
        if (postPaidGetTransactionDetailResponse.getCreationTimestamp() != null) {
            data = new SimpleDateFormat("dd/MM/yyyy").format(postPaidGetTransactionDetailResponse.getCreationTimestamp().getTime());
        }

        String amount = "0.0";

        if (postPaidGetTransactionDetailResponse.getAmount() != null) {
            amount = postPaidGetTransactionDetailResponse.getAmount().toString();
        }

        receipt.addToPaymentData(-1, "DATE", "DATA", "", data, position++, null);
        receipt.addToPaymentData(-1, "AMOUNT", "IMPORTO", "", amount, position++, null);
        receipt.addToPaymentData(-1, "PAN", "PAN", "", postPaidGetTransactionDetailResponse.getPanCode(), position++, null);
        receipt.addToPaymentData(-1, "N_OP", "N OPERAZIONE", "", postPaidGetTransactionDetailResponse.getBankTansactionID(), position++, null);
        receipt.addToPaymentData(-1, "SEPARATOR", "", "", null, position++, null);

        retrievePopTransactionDetailBodyResponse.setTransaction(transaction);
        retrievePopTransactionDetailBodyResponse.setReceipt(receipt);

        retrievePopTransactionDetailResponse.setBody(retrievePopTransactionDetailBodyResponse);
        retrievePopTransactionDetailResponse.setStatus(status);

        return retrievePopTransactionDetailResponse;
    }
}