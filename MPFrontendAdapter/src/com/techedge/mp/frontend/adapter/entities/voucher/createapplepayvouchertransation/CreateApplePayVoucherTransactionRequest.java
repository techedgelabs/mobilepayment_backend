package com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.voucher.CreateApplePayVoucherTransactionResult;
import com.techedge.mp.core.business.interfaces.voucher.CreateVoucherTransactionResult;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.FrontendParameter;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;
import com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation.CreateVoucherTransactionBodyResponse;
import com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation.CreateVoucherTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.voucher.createvouchertransation.CreateVoucherTransactionResponse;

public class CreateApplePayVoucherTransactionRequest extends AbstractRequest implements Validable {

    private Credential  credential;
    private CreateApplePayVoucherTransactionBodyRequest body;

    public CreateApplePayVoucherTransactionRequest() {}

    public Credential getCredential() {
        return credential;
    }
    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateApplePayVoucherTransactionBodyRequest getBody() {
        return body;
    }
    public void setBody(CreateApplePayVoucherTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("VALID", credential);

        if(!Validator.isValid(status.getStatusCode())) {
            
            return status;
            
        }
        
        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.CREATE_APPLE_PAY_VOUCHER_TRANSACTION_SUCCESS);

        return status;

    }
    
    
    @Override
    public BaseResponse execute() {
        CreateApplePayVoucherTransactionRequest createApplePayVoucherTransactionRequest = this;
        CreateApplePayVoucherTransactionResponse createApplePayVoucherTransactionResponse = new CreateApplePayVoucherTransactionResponse();
        CreateApplePayVoucherTransactionBodyResponse createApplePayVoucherTransactionBodyResponse = new CreateApplePayVoucherTransactionBodyResponse();

        String requestID              = createApplePayVoucherTransactionRequest.getCredential().getRequestID();
        String ticketID               = createApplePayVoucherTransactionRequest.getCredential().getTicketID();
        Double amount                 = AmountConverter.toInternal(createApplePayVoucherTransactionRequest.getBody().getApplePayVoucherPaymentData().getAmount());
        String applePayPKPaymentToken = createApplePayVoucherTransactionRequest.getBody().getApplePayVoucherPaymentData().getApplePayPKPaymentToken();
        
        Status statusResponse = new Status();

        try {
            String voucherTransactionMaxAmount = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_VOUCHER_TRANSACTION_MAX_AMOUNT);
            String voucherTransactionMinAmount = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_VOUCHER_TRANSACTION_MIN_AMOUNT);

            if (amount < Double.valueOf(voucherTransactionMinAmount) || amount > Double.valueOf(voucherTransactionMaxAmount)) {
                BaseResponse baseResponse = new BaseResponse();
                statusResponse.setStatusCode(StatusCode.CREATE_VOUCHER_TRANSACTION_INVALID_PARAMETERS);
                statusResponse.setStatusMessage("Invalid amount");
                baseResponse.setStatus(statusResponse);

                return baseResponse;
            }
        }

        catch (ParameterNotFoundException e) {                    // TODO Auto-generated catch block
            e.printStackTrace();
        }

        CreateApplePayVoucherTransactionResult createApplePayVoucherTransactionResult = getVoucherTransactionServiceRemote().createApplePayVoucherTransaction(requestID, ticketID, amount, applePayPKPaymentToken);

        createApplePayVoucherTransactionBodyResponse.setVoucherCode(createApplePayVoucherTransactionResult.getVoucherCode());
        createApplePayVoucherTransactionBodyResponse.setVoucherTransactionID(createApplePayVoucherTransactionResult.getVoucherTransactionID());
        createApplePayVoucherTransactionResponse.setBody(createApplePayVoucherTransactionBodyResponse);

        statusResponse.setStatusCode(createApplePayVoucherTransactionResult.getStatusCode());
        statusResponse.setStatusMessage(prop.getProperty(statusResponse.getStatusCode()));
        createApplePayVoucherTransactionResponse.setStatus(statusResponse);

        return createApplePayVoucherTransactionResponse;

    }

}
