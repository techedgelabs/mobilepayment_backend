package com.techedge.mp.frontend.adapter.entities.user.retrieveactivedevice;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.DeviceData;

public class RetrieveActiveDeviceResponseBody {

    private List<DeviceData> activeDevice = new ArrayList<>(0);

    public List<DeviceData> getActiveDevice() {
        return activeDevice;
    }

    public void setActiveDevice(List<DeviceData> activeDevice) {
        this.activeDevice = activeDevice;
    }

}
