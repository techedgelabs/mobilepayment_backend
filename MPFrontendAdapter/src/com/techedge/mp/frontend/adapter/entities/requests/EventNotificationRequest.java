package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.eventnotification.retrievenotification.RetrieveNotificationRequest;
import com.techedge.mp.frontend.adapter.entities.eventnotification.retrievetransactiondetail.RetrieveLoyaltyTransactionDetailRequest;

public class EventNotificationRequest extends RootRequest {

    protected RetrieveLoyaltyTransactionDetailRequest retrieveLoyaltyTransactionDetail;
    protected RetrieveNotificationRequest             retrieveNotification;

    public RetrieveLoyaltyTransactionDetailRequest getRetrieveLoyaltyTransactionDetail() {
        return retrieveLoyaltyTransactionDetail;
    }

    public void setRetrieveLoyaltyTransactionDetail(RetrieveLoyaltyTransactionDetailRequest retrieveTransactionDetail) {
        this.retrieveLoyaltyTransactionDetail = retrieveTransactionDetail;
    }

    public RetrieveNotificationRequest getRetrieveNotification() {
        return retrieveNotification;
    }

    public void setRetrieveNotification(RetrieveNotificationRequest retrieveNotification) {
        this.retrieveNotification = retrieveNotification;
    }

}
