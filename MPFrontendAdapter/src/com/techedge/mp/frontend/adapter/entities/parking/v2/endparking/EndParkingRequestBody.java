package com.techedge.mp.frontend.adapter.entities.parking.v2.endparking;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class EndParkingRequestBody implements Validable {

    //private String          plateNumber;
    //private CustomTimestamp requestedEndTime;
    //private String          parkingZoneId;
    //private String          stallCode;
    private String          transactionID;

    //    private String          cityId;
    //    private Boolean         stickerRequired;
    //    private String          stickerRequiredNotice;

    @Override
    public Status check() {

        Status status = new Status();
        try {
            //Date reqEndTime = CustomTimestamp.convertToDate(requestedEndTime);
        }
        catch (Exception e) {
            status.setStatusCode(StatusCode.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_FAILURE);

            return status;
        }

//        if (this.plateNumber == null || this.plateNumber.isEmpty()) {
//
//            status.setStatusCode("TBD");
//
//            return status;
//
//        }
//
//        if (this.parkingZoneId == null || this.parkingZoneId.isEmpty()) {
//
//            status.setStatusCode("TBD");
//
//            return status;
//
//        }

        status.setStatusCode(StatusCode.POSTPAID_V2_APPROVE_TRANSACTION_SUCCESS);

        return status;
    }

    

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

}
