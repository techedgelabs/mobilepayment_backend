package com.techedge.mp.frontend.adapter.entities.user.removevoucher;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RemoveVoucherRequest extends AbstractRequest implements Validable {

    private Status                   status = new Status();

    private Credential               credential;
    private RemoveVoucherBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RemoveVoucherBodyRequest getBody() {
        return body;
    }

    public void setBody(RemoveVoucherBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REMOVE-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        RemoveVoucherRequest removeVoucherRequest = this;
        RemoveVoucherResponse removeVoucherResponse = new RemoveVoucherResponse();

        String response = getUserServiceRemote().removeVoucher(removeVoucherRequest.getCredential().getTicketID(), removeVoucherRequest.getCredential().getRequestID(),
                removeVoucherRequest.getBody().getVoucherCode());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        removeVoucherResponse.setStatus(status);

        return removeVoucherResponse;
    }

}
