package com.techedge.mp.frontend.adapter.entities.postpaid.v2.createtransaction;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class CreateTransactionResponse extends BaseResponse {
	
	private CreateTransactionBodyResponse body;

    public CreateTransactionBodyResponse getBody() {
        return body;
    }

    public void setBody(CreateTransactionBodyResponse body) {
        this.body = body;
    }

}
