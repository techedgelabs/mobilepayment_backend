package com.techedge.mp.frontend.adapter.entities.user.checkavailabilityamount;

public class CheckAvailabilityAmountBodyResponse {

    private Integer maxAmount;
    private Integer thresholdAmount;
    private Boolean allowResize;

    public Integer getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Integer maxAmount) {
        this.maxAmount = maxAmount;
    }

    public Integer getThresholdAmount() {
        return thresholdAmount;
    }

    public void setThresholdAmount(Integer thresholdAmount) {
        this.thresholdAmount = thresholdAmount;
    }

    public Boolean getAllowResize() {
        return allowResize;
    }

    public void setAllowResize(Boolean allowResize) {
        this.allowResize = allowResize;
    }
}
