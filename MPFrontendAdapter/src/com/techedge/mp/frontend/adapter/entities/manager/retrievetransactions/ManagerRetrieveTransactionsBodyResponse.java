package com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions;

import java.util.ArrayList;
import java.util.List;

public class ManagerRetrieveTransactionsBodyResponse {

    private List<ManagerRetrieveTransactionsBodyDataResponse> transactionHistory = new ArrayList<ManagerRetrieveTransactionsBodyDataResponse>(0);

    public List<ManagerRetrieveTransactionsBodyDataResponse> getTransactionHistory() {
        return transactionHistory;
    }

    public void setTransactionHistory(List<ManagerRetrieveTransactionsBodyDataResponse> transactionHistory) {
        this.transactionHistory = transactionHistory;
    }

}
