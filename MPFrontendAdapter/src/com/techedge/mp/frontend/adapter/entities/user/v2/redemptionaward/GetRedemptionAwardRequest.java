package com.techedge.mp.frontend.adapter.entities.user.v2.redemptionaward;

import com.techedge.mp.core.business.interfaces.RedemptionAwardResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;
import com.techedge.mp.frontend.adapter.entities.user.redemption.RedemptionCredential;

public class GetRedemptionAwardRequest extends AbstractRequest implements Validable {

    private Status                        status = new Status();

    private RedemptionCredential          credential;

    private GetRedemptionAwardBodyRequest body;

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public GetRedemptionAwardBodyRequest getBody() {
        return body;
    }

    public void setBody(GetRedemptionAwardBodyRequest body) {
        this.body = body;
    }

    public RedemptionCredential getCredential() {
        return credential;
    }

    public void setCredential(RedemptionCredential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-REDEMPTION-AWARD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        else {

            status.setStatusCode(StatusCode.USER_REDEMPTION_AWARD_SUCCESS);

        }

        return status;

    }

    @Override
    public BaseResponse execute() {

        GetRedemptionAwardResponse response = new GetRedemptionAwardResponse();
        GetRedemptionAwardBodyResponse bodyResponse = new GetRedemptionAwardBodyResponse();
        GetRedemptionAwardRequest redemptionAwardRequest = this;

        RedemptionAwardResponse redemptionAwardResponse = getUserV2ServiceRemote().getRedemptionAward(this.getCredential().getRequestID(), this.getCredential().getTicketID(),
                redemptionAwardRequest.getBody().getAwardId(), redemptionAwardRequest.getBody().getCardPartner(), redemptionAwardRequest.getCredential().getPin());

        if (redemptionAwardResponse != null && redemptionAwardResponse.getStatusCode()!=null && !redemptionAwardResponse.getStatusCode().isEmpty()) {
            
            status.setStatusCode(redemptionAwardResponse.getStatusCode());
            
            if(redemptionAwardResponse.getMessageCode()!=null){
                status.setStatusMessage(redemptionAwardResponse.getMessageCode());
            }else{
                status.setStatusMessage(prop.getProperty(redemptionAwardResponse.getStatusCode()));
            }
            bodyResponse.setOrderId(redemptionAwardResponse.getOrderId());
            bodyResponse.setVoucher(redemptionAwardResponse.getVoucher());
            response.setBody(bodyResponse);
        }
        
        response.setStatus(status);
        
        return response;

    }

}
