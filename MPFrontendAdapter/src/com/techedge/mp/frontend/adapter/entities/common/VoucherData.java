package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;

public class VoucherData {

    private String                  code;
    private String                  status;
    private String                  type;
    private Integer                 value;
    private Integer                 initialValue;
    private Integer                 consumedValue;
    private Integer                 voucherBalanceDue;
    private CustomTimestamp         expirationDate;
    private String                  promoCode;
    private String                  promoDescription;
    private String                  promoDoc;
    private String                  icon;
    private String                  classType;
    private List<VoucherDataDetail> details = new ArrayList<VoucherDataDetail>();

    public VoucherData() {}

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public Integer getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Integer initialValue) {
        this.initialValue = initialValue;
    }

    public Integer getConsumedValue() {
        return consumedValue;
    }

    public void setConsumedValue(Integer consumedValue) {
        this.consumedValue = consumedValue;
    }

    public Integer getVoucherBalanceDue() {
        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(Integer voucherBalanceDue) {
        this.voucherBalanceDue = voucherBalanceDue;
    }

    public CustomTimestamp getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(CustomTimestamp expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoDoc() {
        return promoDoc;
    }

    public void setPromoDoc(String promoDoc) {
        this.promoDoc = promoDoc;
    }

    public void addDetail(String label, String value) {
        VoucherDataDetail detail = new VoucherDataDetail();
        detail.setLabel(label);
        detail.setValue(value);
        this.details.add(detail);
    }

    public List<VoucherDataDetail> getDetails() {
        return details;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getClassType() {
        return classType;
    }

    public void setClassType(String classType) {
        this.classType = classType;
    }

}
