package com.techedge.mp.frontend.adapter.entities.user.getavailableloyaltycards;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class GetAvailableLoyaltyCardsBodyRequest implements Validable {

    private String fiscalcode;

    public String getFiscalcode() {
        return fiscalcode;
    }

    public void setFiscalcode(String fiscalcode) {
        this.fiscalcode = fiscalcode;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.fiscalcode != null && this.fiscalcode.length() != 16) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_GET_AVAILABLE_LOYALTY_CARDS_SUCCESS);

        return status;

    }

}
