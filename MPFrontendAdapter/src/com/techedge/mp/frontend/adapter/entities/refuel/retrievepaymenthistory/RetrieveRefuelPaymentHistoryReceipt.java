package com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory;

import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;


public class RetrieveRefuelPaymentHistoryReceipt {

	
	private CustomTimestamp date;
//    private Integer fuelAmount;
//    private Integer fuelQuantity;
    private Integer finalAmount;
    
    
	public CustomTimestamp getDate() {
		return date;
	}
	public void setDate(CustomTimestamp date) {
		this.date = date;
	}
	
//	public Integer getFuelAmount() {
//		return fuelAmount;
//	}
//	public void setFuelAmount(Integer fuelAmount) {
//		this.fuelAmount = fuelAmount;
//	}
//	
//	public Integer getFuelQuantity() {
//		return fuelQuantity;
//	}
//	public void setFuelQuantity(Integer fuelQuantity) {
//		this.fuelQuantity = fuelQuantity;
//	}
	
	public Integer getFinalAmount() {
		return finalAmount;
	}
	public void setFinalAmount(Integer finalAmount) {
		this.finalAmount = finalAmount;
	}
    
	
}
