package com.techedge.mp.frontend.adapter.entities.common;

public class PaymentMethod {
	
	private Long id;
	private String type;
	private String brand;
	private CustomDate expirationDate;
	private String identifier;
    private Integer status;
    private String message;
    private Boolean defaultMethod;
    private CustomTimestamp insertDate;
    
    
    public PaymentMethod(){}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}

	public CustomDate getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(CustomDate expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getIdentifier() {
		return identifier;
	}
	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public Boolean getDefaultMethod() {
		return defaultMethod;
	}
	public void setDefaultMethod(Boolean defaultMethod) {
		this.defaultMethod = defaultMethod;
	}

	public CustomTimestamp getInsertDate() {
		return insertDate;
	}
	public void setInsertDate(CustomTimestamp insertDate) {
		this.insertDate = insertDate;
	}
}
