package com.techedge.mp.frontend.adapter.entities.common;

public class Receipt {

	private String refuelID;
	private CustomTimestamp date;
	private Integer fuelAmount;
	private Double fuelQuantity;
	private Integer finalAmount;
	private String bankTransactionID;
	private String shopLogin;
	private String currency;
	private String maskedPan;
	private String authCode;
	
	public String getRefuelID() {
		return refuelID;
	}
	public void setRefuelID(String refuelID) {
		this.refuelID = refuelID;
	}
	
	public CustomTimestamp getDate() {
		return date;
	}
	public void setDate(CustomTimestamp date) {
		this.date = date;
	}
	
	public Integer getFuelAmount() {
		return fuelAmount;
	}
	public void setFuelAmount(Integer fuelAmount) {
		this.fuelAmount = fuelAmount;
	}
	
	public Double getFuelQuantity() {
		return fuelQuantity;
	}
	public void setFuelQuantity(Double fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}
	
	public Integer getFinalAmount() {
		return finalAmount;
	}
	public void setFinalAmount(Integer finalAmount) {
		this.finalAmount = finalAmount;
	}
	
	public String getBankTransactionID() {
		return bankTransactionID;
	}
	public void setBankTransactionID(String bankTransactionID) {
		this.bankTransactionID = bankTransactionID;
	}
	
	public String getShopLogin() {
		return shopLogin;
	}
	public void setShopLogin(String shopLogin) {
		this.shopLogin = shopLogin;
	}
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	
	public String getMaskedPan() {
		return maskedPan;
	}
	public void setMaskedPan(String maskedPan) {
		this.maskedPan = maskedPan;
	}
	
	public String getAuthCode() {
		return authCode;
	}
	public void setAuthCode(String authCode) {
		this.authCode = authCode;
	}
}
