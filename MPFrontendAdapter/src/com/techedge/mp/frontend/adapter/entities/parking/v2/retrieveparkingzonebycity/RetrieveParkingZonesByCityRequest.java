package com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingzonebycity;

import java.util.LinkedList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.parking.ParkingCity;
import com.techedge.mp.core.business.interfaces.parking.ParkingCityZoneData;
import com.techedge.mp.core.business.interfaces.parking.ParkingZone;
import com.techedge.mp.core.business.interfaces.parking.RetrieveParkingZonesByCityResult;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveParkingZonesByCityRequest extends AbstractRequest implements Validable {

    private Status                                status = new Status();
    private Credential                            credential;
    private RetrieveParkingZonesByCityRequestBody body;

    public RetrieveParkingZonesByCityRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveParkingZonesByCityRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.RETRIEVE_PARKING_ZONES_SUCCESS);

        return status;

    }

    public Credential getCredential() {
        return credential;
    }

    @Override
    public BaseResponse execute() {

        RetrieveParkingZonesByCityRequest retrieveParkingZonesByCityRequest = this;

        RetrieveParkingZonesByCityResponse retrieveParkingZoneByCityResponse = new RetrieveParkingZonesByCityResponse();

        RetrieveParkingZonesByCityResponseBody retrieveParkingZonesByCityResponseBody = new RetrieveParkingZonesByCityResponseBody();

        RetrieveParkingZonesByCityResult result = getParkingTransactionV2ServiceRemote().retrieveParkingZonesByCity(
                retrieveParkingZonesByCityRequest.getCredential().getTicketID(), retrieveParkingZonesByCityRequest.getCredential().getRequestID(),
                retrieveParkingZonesByCityRequest.getBody().getCityId());

        for (ParkingCityZoneData parkingCityZoneData : result.getParkingCityZoneData()) {

            com.techedge.mp.frontend.adapter.entities.common.v2.ParkingCityZoneData parkingCityZoneData2 = new com.techedge.mp.frontend.adapter.entities.common.v2.ParkingCityZoneData();
            ParkingCity parkingCity = parkingCityZoneData.getParkingCity();
            com.techedge.mp.frontend.adapter.entities.common.v2.ParkingCity parkingCity2 = new com.techedge.mp.frontend.adapter.entities.common.v2.ParkingCity();
            parkingCity2.setAdministrativeAreaLevel2Code(parkingCity.getAdministrativeAreaLevel2Code());
            parkingCity2.setCityId(parkingCity.getCityId());
            parkingCity2.setId(parkingCity.getId());
            parkingCity2.setLatitude(parkingCity.getLatitude());
            parkingCity2.setLongitude(parkingCity.getLongitude());
            parkingCity2.setName(parkingCity.getName());
            parkingCity2.setStickerRequired(parkingCity.getStickerRequired());
            parkingCity2.setStickerRequiredNotice(parkingCity.getStickerRequiredNotice());
            parkingCity2.setUrl(parkingCity.getUrl());
            parkingCityZoneData2.setParkingCity(parkingCity2);

            List<ParkingZone> parkingZones = parkingCityZoneData.getParkingZoneList();
            //List<com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone> parkingZoneList = new LinkedList<com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone>();

            for (ParkingZone parkingZone : parkingZones) {
                com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone parkingZone2 = new com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone();
                parkingZone2.setCityId(parkingZone.getCityId());
                parkingZone2.setDescription(parkingZone.getDescription());
                parkingZone2.setId(parkingZone.getId());
                parkingZone2.setName(parkingZone.getName());
                parkingZone2.setNotice(parkingZone.getNotice());
                parkingZone2.setStallCodeRequired(parkingZone.getStallCodeRequired());
                parkingZone2.setStickerRequired(parkingZone.getStickerRequired());
                parkingZone2.setStickerRequiredNotice(parkingZone.getStickerRequiredNotice());
                parkingZone2.setCityName(parkingCity.getName());
                parkingZone2.setAdministrativeAreaLevel2Code(parkingCity.getAdministrativeAreaLevel2Code());
                //parkingZoneList.add(parkingZone2);
                retrieveParkingZonesByCityResponseBody.getParkingZones().add(parkingZone2);

            }
            //parkingCityZoneData2.setParkingZones(parkingZoneList);
            //retrieveParkingZonesByCityResponseBody.getParkingZones().add(parkingCityZoneData2);

        }

        retrieveParkingZoneByCityResponse.setBody(retrieveParkingZonesByCityResponseBody);

        Status status = new Status();
        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(prop.getProperty(result.getStatusCode()));
        retrieveParkingZoneByCityResponse.setStatus(status);

        return retrieveParkingZoneByCityResponse;

    }
}
