package com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings;

import com.techedge.mp.frontend.adapter.entities.common.v2.AppLinkInfo;

public class RetrieveApplicationSettingsDinamycPageBodyResponse {

    private AppLinkInfo faq;
    private AppLinkInfo redemptionUrl;
    private AppLinkInfo infoTokenUrl;
    private AppLinkInfo stationFinderUrl;
    private AppLinkInfo firstStartUrl;
    private AppLinkInfo businessInfoUrl;

    public AppLinkInfo getFaq() {
        return faq;
    }

    public void setFaq(AppLinkInfo faq) {
        this.faq = faq;
    }

    public AppLinkInfo getRedemptionUrl() {
        return redemptionUrl;
    }

    public void setRedemptionUrl(AppLinkInfo redemptionUrl) {
        this.redemptionUrl = redemptionUrl;
    }

    public AppLinkInfo getInfoTokenUrl() {
        return infoTokenUrl;
    }

    public void setInfoTokenUrl(AppLinkInfo infoTokenUrl) {
        this.infoTokenUrl = infoTokenUrl;
    }

    public AppLinkInfo getStationFinderUrl() {
        return stationFinderUrl;
    }

    public void setStationFinderUrl(AppLinkInfo stationFinderUrl) {
        this.stationFinderUrl = stationFinderUrl;
    }

    public AppLinkInfo getFirstStartUrl() {
        return firstStartUrl;
    }

    public void setFirstStartUrl(AppLinkInfo firstStartUrl) {
        this.firstStartUrl = firstStartUrl;
    }

    public AppLinkInfo getBusinessInfoUrl() {
        return businessInfoUrl;
    }

    public void setBusinessInfoUrl(AppLinkInfo businessInfoUrl) {
        this.businessInfoUrl = businessInfoUrl;
    }

}
