package com.techedge.mp.frontend.adapter.entities.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {

    private final static Integer MAX_HISTORY_ENTRIES      = 20;

    private final static String  regex_password_maiuscola = ".*[A-Z].*";
    private final static String  regex_password_minuscola = ".*[a-z].*";
    private final static String  regex_password_numerica  = ".*[0-9].*";

    private final static String  regex_email              = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    public static Status checkPassword(String operation, String password) {

        Status status = new Status();
        
        if (operation.equals("CREATE") && (password == null || password.isEmpty())) {

            status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);
            
            return status;
        }
        

        if (password == null || password.length() > 40) {

            if (operation.equals("CREATE"))

                status.setStatusCode(StatusCode.USER_CREATE_PASSWORD_WRONG);

            if (operation.equals("AUTH"))

                status.setStatusCode(StatusCode.USER_AUTH_PASSWORD_WRONG);

            if (operation.equals("PWD"))

                status.setStatusCode(StatusCode.USER_PWD_OLD_PASSWORD_WRONG);

            return status;

        }
        else {

            if (password.length() < 8) {

                if (operation.equals("CREATE"))

                    status.setStatusCode(StatusCode.USER_CREATE_PASSWORD_SHORT);

                if (operation.equals("AUTH"))

                    status.setStatusCode(StatusCode.USER_AUTH_PASSWORD_SHORT);

                if (operation.equals("PWD"))

                    status.setStatusCode(StatusCode.USER_PWD_PASSWORD_SHORT);

                return status;

            }

            Pattern patternPasswordMaiuscola = Pattern.compile(regex_password_maiuscola);
            Matcher matchePasswordMaiuscola = patternPasswordMaiuscola.matcher(password);
            Pattern patternPasswordMinuscola = Pattern.compile(regex_password_minuscola);
            Matcher matchePasswordMinuscola = patternPasswordMinuscola.matcher(password);
            Pattern patternPasswordNumerica = Pattern.compile(regex_password_numerica);
            Matcher matchePasswordNumerica = patternPasswordNumerica.matcher(password);

            if (!matchePasswordMaiuscola.matches()) {

                if (operation.equals("CREATE"))

                    status.setStatusCode(StatusCode.USER_CREATE_UPPER_LESS);

                if (operation.equals("AUTH"))

                    status.setStatusCode(StatusCode.USER_AUTH_UPPER_LESS);

                if (operation.equals("PWD"))

                    status.setStatusCode(StatusCode.USER_PWD_UPPER_LESS);

                return status;

            }

            if (!matchePasswordMinuscola.matches()) {

                if (operation.equals("CREATE"))

                    status.setStatusCode(StatusCode.USER_CREATE_LOWER_LESS);

                if (operation.equals("AUTH"))

                    status.setStatusCode(StatusCode.USER_AUTH_LOWER_LESS);

                if (operation.equals("PWD"))

                    status.setStatusCode(StatusCode.USER_PWD_LOWER_LESS);

                return status;

            }

            if (!matchePasswordNumerica.matches()) {

                if (operation.equals("CREATE"))

                    status.setStatusCode(StatusCode.USER_CREATE_NUMBER_LESS);

                if (operation.equals("AUTH"))

                    status.setStatusCode(StatusCode.USER_AUTH_NUMBER_LESS);

                if (operation.equals("PWD"))

                    status.setStatusCode(StatusCode.USER_PWD_NUMBER_LESS);

                return status;

            }

        }

        if (operation.equals("CREATE"))

            status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

        if (operation.equals("AUTH"))

            status.setStatusCode(StatusCode.USER_AUTH_SUCCESS);

        if (operation.equals("PWD"))

            status.setStatusCode(StatusCode.USER_PWD_SUCCESS);

        return status;

    }

    public static Status checkPin(String operation, String pin) {

        Status status = new Status();

        if (pin == null || pin.length() > 4 || pin.trim() == "") {

            if (operation.equals("OLDPIN"))

                status.setStatusCode(StatusCode.USER_PIN_OLD_WRONG);

            if (operation.equals("NEWPIN"))

                status.setStatusCode(StatusCode.USER_PIN_NEW_WRONG);

            if (operation.equals("PAY-INSERT"))

                status.setStatusCode(StatusCode.USER_INSERT_PAYMENT_METHOD_PIN_WRONG);

            if (operation.equals("MULTICARD-PAY-INSERT"))

                status.setStatusCode(StatusCode.USER_INSERT_MULTICARD_PAYMENT_METHOD_WRONG_PIN);
            
            return status;

        }

        status.setStatusCode(StatusCode.USER_PIN_SUCCESS);

        return status;
    }

    public static Status checkEmail(String operation, String email) {

        Status status = new Status();

        if (email == null || email.length() > 50) {

            if (operation.equals("CREATE"))

                status.setStatusCode(StatusCode.USER_CREATE_EMAIL_WRONG);

            if (operation.equals("AUTH"))

                status.setStatusCode(StatusCode.USER_AUTH_EMAIL_WRONG);

            return status;

        }
        else {

            Pattern patternEmail = Pattern.compile(regex_email);
            Matcher matcherEmail = patternEmail.matcher(email);

            if (!matcherEmail.matches()) {

                if (operation.equals("CREATE")) {

                    status.setStatusCode(StatusCode.USER_CREATE_EMAIL_WRONG);

                    return status;

                }

            }

        }

        if (operation.equals("CREATE"))

            status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

        if (operation.equals("AUTH"))

            status.setStatusCode(StatusCode.USER_AUTH_SUCCESS);

        return status;

    }

    public static Status checkCredential(String operation, Credential credential) {

        Status status = new Status();

        if (credential != null) {

            if (credential.getTicketID() == null || credential.getTicketID().length() != 32) {

                status.setStatusCode(StatusCode.USER_REQU_INVALID_TICKET);

                return status;

            }

            if (credential.getRequestID() == null || credential.getRequestID().length() > 50) {

                status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

                return status;

            }
            
            if (credential.getRequestID().startsWith("WPH")) {

                status.setStatusCode(StatusCode.USER_REQU_UNSUPPORTED_DEVICE);

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        if (operation.equals("CREATE")) {

            status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("LOGOUT")) {

            status.setStatusCode(StatusCode.USER_LOGOUT_SUCCESS);

            return status;

        }

        if (operation.equals("UPD")) {

            status.setStatusCode(StatusCode.USER_UPD_SUCCESS);

            return status;

        }

        if (operation.equals("PWD")) {

            status.setStatusCode(StatusCode.USER_PWD_SUCCESS);

            return status;

        }

        if (operation.equals("RESCUE")) {

            status.setStatusCode(StatusCode.USER_RESCUE_PASSWORD_SUCCESS);

            return status;

        }

        if (operation.equals("RECOVER")) {

            status.setStatusCode(StatusCode.USER_RECOVER_USERNAME_SUCCESS);

            return status;

        }

        if (operation.equals("PIN")) {

            status.setStatusCode(StatusCode.USER_PIN_SUCCESS);

            return status;

        }

        if (operation.equals("VALID")) {

            status.setStatusCode(StatusCode.USER_VALID_SUCCESS);

            return status;

        }

        if (operation.equals("VALID-PAY")) {

            status.setStatusCode(StatusCode.USER_VALIDATE_PAYMENT_METHOD_SUCCESS);

            return status;

        }

        if (operation.equals("CANCEL-REFUEL")) {

            status.setStatusCode(StatusCode.REFUEL_CANCEL_SUCCESS);

            return status;

        }

        if (operation.equals("CONFIRM-REFUEL")) {

            status.setStatusCode(StatusCode.REFUEL_CONFIRM_SUCCESS);

            return status;

        }

        if (operation.equals("REFUEL-CREATE")) {

            status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("DETAIL-REFUEL")) {

            status.setStatusCode(StatusCode.RETRIEVE_PAYMENT_DETAIL_SUCCESS);

            return status;

        }

        if (operation.equals("PAY-HISTORY")) {

            status.setStatusCode(StatusCode.REFUEL_EXEC_PAY_HISTORY_SUCCESS);

            return status;

        }

        if (operation.equals("PAY-INSERT")) {

            status.setStatusCode(StatusCode.USER_INSERT_PAYMENT_METHOD_SUCCESS);

            return status;

        }

        if (operation.equals("PENDING-REFUEL")) {

            status.setStatusCode(StatusCode.REFUEL_PENDING_SUCCESS);

            return status;

        }

        if (operation.equals("VOUCHER-DEPOSIT")) {

            status.setStatusCode(StatusCode.VOUCHER_DEPOSIT_SUCCESS);

            return status;

        }

        if (operation.equals("VOUCHER-RETRIEVE")) {

            status.setStatusCode(StatusCode.VOUCHER_RETRIEVE_HISTORY_SUCCESS);

            return status;

        }

        if (operation.equals("STATION-RETRIEVE")) {

            status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);

            return status;

        }

        if (operation.equals("STATION-LIST-RETRIEVE")) {

            status.setStatusCode(StatusCode.STATION_LIST_RETRIEVE_SUCCESS);

            return status;

        }

        if (operation.equals("PAY-RETRIEVE-DATA")) {

            status.setStatusCode(StatusCode.PAYMENT_RETRIEVE_DATA_SUCCESS);

            return status;

        }

        if (operation.equals("PAY-RETRIEVE")) {

            status.setStatusCode(StatusCode.PAYMENT_RETRIEVE_SUCCESS);

            return status;

        }

        if (operation.equals("REFUEL-CREATE")) {

            status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("CITIES")) {

            status.setStatusCode(StatusCode.RETRIEVE_CITIES_SUCCESS);

            return status;

        }

        if (operation.equals("APPLICATION-SYSTEM")) {

            status.setStatusCode(StatusCode.RETRIEVE_APPLICATION_SETTINGS_SUCCESS);

            return status;

        }

        if (operation.equals("PAY-SETDEFAULT")) {

            status.setStatusCode(StatusCode.USER_SET_DEFAULT_PAYMENT_METHOD_SUCCESS);

            return status;

        }

        if (operation.equals("PAY-REMOVE")) {

            status.setStatusCode(StatusCode.USER_REMOVE_PAYMENT_METHOD_SUCCESS);

            return status;

        }

        if (operation.equals("TERMS_SERVICE")) {

            status.setStatusCode(StatusCode.RETRIEVE_TERMS_SERVICE_SUCCESS);

            return status;

        }

        if (operation.equals("TRANSACTION-HISTORY")) {

            status.setStatusCode(StatusCode.RETRIEVE_TRANSACTION_HISTORY_SUCCESS);

            return status;

        }

        if (operation.equals("MANAGER-RETRIEVE-TRANSACTIONS")) {

            status.setStatusCode(StatusCode.MANAGER_RETRIEVE_TRANSACTIONS_SUCCESS);

            return status;

        }

        if (operation.equals("MANAGER-RETRIEVE-VOUCHERS")) {

            status.setStatusCode(StatusCode.MANAGER_RETRIEVE_VOUCHERS_SUCCESS);

            return status;

        }

        if (operation.equals("LOAD-VOUCHER")) {

            status.setStatusCode(StatusCode.USER_LOAD_VOUCHER_SUCCESS);

            return status;

        }

        if (operation.equals("GET-ACTIVE-VOUCHERS")) {

            status.setStatusCode(StatusCode.USER_GET_ACTIVE_VOUCHERS_SUCCESS);

            return status;

        }

        if (operation.equals("SET-DEFAULT-LOYALTY-CARD")) {

            status.setStatusCode(StatusCode.USER_SET_DEFAULT_LOYALTY_CARD_SUCCESS);

            return status;

        }

        if (operation.equals("GET-AVAILABLE-LOYALTY-CARDS")) {

            status.setStatusCode(StatusCode.USER_GET_AVAILABLE_LOYALTY_CARDS_SUCCESS);

            return status;

        }

        if (operation.equals("SET-USE-VOUCHER")) {

            status.setStatusCode(StatusCode.USER_GET_AVAILABLE_LOYALTY_CARDS_SUCCESS);

            return status;

        }

        if (operation.equals("REMOVE-VOUCHER")) {

            status.setStatusCode(StatusCode.USER_REMOVE_VOUCHER_SUCCESS);

            return status;

        }

        if (operation.equals("GET-SURVEY")) {

            status.setStatusCode(StatusCode.SURVEY_GET_SURVEY_SUCCESS);

            return status;

        }

        if (operation.equals("SUBMIT-SURVEY")) {

            status.setStatusCode(StatusCode.SURVEY_SUBMIT_SURVEY_SUCCESS);

            return status;

        }

        if (operation.equals("SKIP-PAYMENT-METHOD-CONFIGURATION")) {

            status.setStatusCode(StatusCode.SKIP_PAYMENT_METHOD_CONFIGURATION_SUCCESS);

            return status;

        }
        

        if (operation.equals("UPD-MOBILE-PHONE")) {

            status.setStatusCode(StatusCode.UPD_MOBILE_PHONE_SUCCESS);
            status.setStatusMessage("Mobile phone number updated");

            return status;

        }

        if (operation.equals("CANCEL-MOBILE-PHONE")) {

            status.setStatusCode(StatusCode.CANCEL_MOBILE_PHONE_SUCCESS);
            status.setStatusMessage("Mobile phone number cancelled");

            return status;

        }

        if (operation.equals("RESEND-VALIDATION")) {

            status.setStatusCode(StatusCode.USER_RESEND_VALIDATATION_SUCCESS);

            return status;

        }

        if (operation.equals("RETRIEVE-PREFIXES")) {

            status.setStatusCode(StatusCode.USER_RESEND_VALIDATATION_SUCCESS);

            return status;

        }

        if (operation.equals("GET-CREDIT-VOUCHER")) {

            status.setStatusCode(StatusCode.GET_CREDIT_VOUCHER_SUCCESS);

            return status;

        }
        
        if (operation.equals("CREATE-VOUCHER-TRANSACTION")) {

            status.setStatusCode(StatusCode.CREATE_VOUCHER_TRANSACTION_SUCCESS);

            return status;

        }
        
        if (operation.equals("RETRIEVE-VOUCHER-TRANSACTION-DETAIL")) {

            status.setStatusCode(StatusCode.RETRIEVE_VOUCHER_TRANSACTION_SUCCESS);

            return status;

        }
        
        if (operation.equals("CHECK-AVAILABLE-AMOUNT")) {

            status.setStatusCode(StatusCode.CHECK_AVAILABILITY_AMOUNT_SUCCESS);

            return status;

        }

        if (operation.equals("RETRIVE-REFUEL-TRANSACTION-DETAIL")) {

            status.setStatusCode(StatusCode.RETRIVE_REFUEL_TRANSACTION_DETAIL_SUCCESS);

            return status;

        }
        
        if (operation.equals("RETRIVE-NOTIFICATION-TRANSACTION-DETAIL")) {

            status.setStatusCode(StatusCode.RETRIEVE_EVENT_NOTIFICATION_TRANSACTION_DETAIL_SUCCESS);

            return status;

        }
        
        if (operation.equals("UPD-SMS-LOG")) {

            status.setStatusCode(StatusCode.UPDATE_SMS_LOG_SUCCESS);

            return status;

        }

        if (operation.equals("INFO-REDEMPTION")) {

            status.setStatusCode(StatusCode.USER_INFO_REDEMPTION_SUCCESS);

            return status;

        }
        
        if (operation.equals("REDEMPTION")) {

            status.setStatusCode(StatusCode.USER_REDEMPTION_SUCCESS);

            return status;

        }
        
        if (operation.equals("RETRIEVE-NOTIFICATION")) {

            status.setStatusCode(StatusCode.RETRIEVE_NOTIFICATION_SUCCESS);

            return status;

        }

        if (operation.equals("RETRIEVE-ACTIVE-DEVICE")) {

            status.setStatusCode(StatusCode.USER_RETRIEVE_ACTIVE_DEVICE_SUCCESS);

            return status;

        }
 
        if (operation.equals("REMOVE-ACTIVE-DEVICE")) {

            status.setStatusCode(StatusCode.USER_RETRIEVE_ACTIVE_DEVICE_SUCCESS);

            return status;

        }

        if (operation.equals("NOTIFICATION-VIRTUALIZATION")) {

            status.setStatusCode(StatusCode.USER_V2_NOTIFICATION_VIRTUALIZATION_SUCCESS);

            return status;

        }

        
        if (operation.equals("APPLE-PAY-CREATE-REFUEL")) {

            status.setStatusCode(StatusCode.UPDATE_SMS_LOG_SUCCESS);

            return status;

        }
        
        if (operation.equals("CREATEV2")) {

            status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

            return status;

        }
        
        if (operation.equals("CREATEGUEST")) {

            status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

            return status;

        }
        
        if (operation.equals("GET-PARTNER-LIST")) {

            status.setStatusCode(StatusCode.GET_PARTNER_LIST_SUCCESS);

            return status;

        }
        
        if (operation.equals("GET-CATEGORY-LIST")) {

            status.setStatusCode(StatusCode.GET_CATEGORY_LIST_SUCCESS);

            return status;

        }
        
        if (operation.equals("GET-BRAND-LIST")) {

            status.setStatusCode(StatusCode.GET_BRAND_LIST_SUCCESS);

            return status;

        }
        
        if (operation.equals("GET-AWARD-LIST")) {

            status.setStatusCode(StatusCode.GET_AWARD_LIST_SUCCESS);

            return status;

        }
        
        if (operation.equals("GET-REDEMPTION-AWARD")) {

            status.setStatusCode(StatusCode.GET_AWARD_LIST_SUCCESS);

            return status;

        }
        
        if (operation.equals("GET-ACTION-URL")) {

            status.setStatusCode(StatusCode.GET_ACTION_URL_SUCCESS);

            return status;

        }
        
        if (operation.equals("GET-MISSION-LIST")) {

            status.setStatusCode(StatusCode.GET_MISSION_LIST_SUCCESS);

            return status;

        }
        
        if (operation.equals("PENDING-PARKING-TRANSACTION")) {

            status.setStatusCode(StatusCode.RETRIEVE_PENDING_PARKING_TRANSACTION_SUCCESS);

            return status;

        }
        
        if (operation.equals("ADD-PLATE-NUMBER")) {

            status.setStatusCode(StatusCode.USER_ADD_PLATE_NUMBER_SUCCESS);

            return status;

        }
        
        if (operation.equals("REMOVE-PLATE-NUMBER")) {

            status.setStatusCode(StatusCode.USER_REMOVE_PLATE_NUMBER_SUCCESS);

            return status;

        }
        
        if (operation.equals("SET-DEFAULT-PLATE-NUMBER")) {

            status.setStatusCode(StatusCode.USER_SET_DEFAULT_PLATE_NUMBER_SUCCESS);

            return status;

        }
        
        if (operation.equals("DETAIL-PARKING-TRANSACTION")) {

            status.setStatusCode(StatusCode.RETRIEVE_PARKING_TRANSACTION_DETAIL_SUCCESS);

            return status;

        }
        
        if (operation.equals("GET_RECEIPT")) {
            
            status.setStatusCode(StatusCode.GET_RECEIPT_SUCCESS);

            return status;

        }
        
        if (operation.equals("MULTICARD-PAY-INSERT")) {

            status.setStatusCode(StatusCode.USER_INSERT_PAYMENT_METHOD_SUCCESS);

            return status;

        }
        
        if (operation.equals("MULTICARD-CREATE-REFUEL")) {
            
            status.setStatusCode(StatusCode.MULTICARD_REFUEL_TRANSACTION_CREATE_SUCCESS);

            return status;

        }
        
        if (operation.equals("SET-ACTIVE-DPAN")) {
            
            status.setStatusCode(StatusCode.USER_SET_ACTIVE_DPAN_SUCCESS);

            return status;

        }
        
        status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
        return status;

    }

    public static Status checkCustomDate(String operation, CustomDate date) {

        Status status = new Status();

        if (date != null) {

            if (date.getYear() == null || date.getYear() < 1900 || date.getYear() > 2050) {

                status = identifycheckCustomDate(operation);

                return status;

            }

            if (date.getMonth() == null || date.getMonth() < 1 || date.getMonth() > 12) {

                status = identifycheckCustomDate(operation);

                return status;

            }

            if (date.getDay() == null || date.getDay() < 1 || date.getDay() > 31) {

                status = identifycheckCustomDate(operation);

                return status;

            }

        }

        if (operation.equals("CREATE")) {

            status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("LOGOUT")) {

            status.setStatusCode(StatusCode.USER_LOGOUT_SUCCESS);

            return status;

        }

        if (operation.equals("UPD")) {

            status.setStatusCode(StatusCode.USER_UPD_SUCCESS);

            return status;

        }

        if (operation.equals("PWD")) {

            status.setStatusCode(StatusCode.USER_PWD_SUCCESS);

            return status;

        }

        if (operation.equals("PIN")) {

            status.setStatusCode(StatusCode.USER_PIN_SUCCESS);

            return status;

        }

        if (operation.equals("VALID")) {

            status.setStatusCode(StatusCode.USER_VALID_SUCCESS);

            return status;

        }

        if (operation.equals("CONFIRM-REFUEL")) {

            status.setStatusCode(StatusCode.REFUEL_CONFIRM_SUCCESS);

            return status;

        }

        if (operation.equals("REFUEL-CREATE")) {

            status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("PAY-HISTORY-START") || operation.equals("PAY-HISTORY-END")) {

            status.setStatusCode(StatusCode.REFUEL_EXEC_PAY_HISTORY_SUCCESS);

            return status;

        }

        if (operation.equals("TRANSACTION-HISTORY-START") || operation.equals("TRANSACTION-HISTORY-END")) {

            status.setStatusCode(StatusCode.RETRIEVE_TRANSACTION_HISTORY_SUCCESS);

            return status;

        }

        if (operation.equals("PENDING-REFUEL")) {

            status.setStatusCode(StatusCode.REFUEL_PENDING_SUCCESS);

            return status;

        }

        if (operation.equals("VOU-HISTORY-START") || operation.equals("VOU-HISTORY-END")) {

            status.setStatusCode(StatusCode.VOUCHER_RETRIEVE_HISTORY_SUCCESS);

            return status;

        }

        status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
        return status;

    }

    private static Status identifycheckCustomDate(String operation) {

        Status status = new Status();

        if (operation.equals("PAY-HISTORY-START")) {

            status.setStatusCode(StatusCode.REFUEL_EXEC_PAY_HISTORY_STARTDATE_WRONG);

            return status;

        }

        if (operation.equals("PAY-HISTORY-END")) {

            status.setStatusCode(StatusCode.REFUEL_EXEC_PAY_HISTORY_ENDDATE_WRONG);

            return status;

        }

        if (operation.equals("VOU-HISTORY-START")) {

            status.setStatusCode(StatusCode.REFUEL_EXEC_PAY_HISTORY_STARTDATE_WRONG);

            return status;

        }

        if (operation.equals("VOU-HISTORY-END")) {

            status.setStatusCode(StatusCode.REFUEL_EXEC_PAY_HISTORY_ENDDATE_WRONG);

            return status;

        }

        status.setStatusCode(StatusCode.USER_CREATE_FIDELITY_DATA_WRONG);

        return status;
    }

    public static Status checkHistoryLimit(String operation, Integer limit) {

        Status status = new Status();

        if (limit > Validator.MAX_HISTORY_ENTRIES) {

            status.setStatusCode(StatusCode.RETRIEVE_TRANSACTION_HISTORY_LIMIT_WRONG);

            return status;
        }

        status.setStatusCode(StatusCode.RETRIEVE_TRANSACTION_HISTORY_SUCCESS);

        return status;
    }

    public static Status checkHistoryPageOffset(String operation, Integer pageOffset) {

        Status status = new Status();

        if (pageOffset < 0) {

            status.setStatusCode(StatusCode.RETRIEVE_TRANSACTION_HISTORY_PAGEOFFSET_WRONG);

            return status;
        }

        status.setStatusCode(StatusCode.RETRIEVE_TRANSACTION_HISTORY_SUCCESS);

        return status;
    }

    public static boolean isValid(String statusCode) {

        if (statusCode == null) {
            return false;
        }
        else {
            if (statusCode.contains("200")) {
                return true;
            }
            else {
                return false;
            }
        }
    }
    

}
