package com.techedge.mp.frontend.adapter.entities.postpaid.business.retrieverefueltransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveRefuelTransactionDetailResponse extends BaseResponse {

	private RetrieveRefuelTransactionDetailBodyResponse body;

	public RetrieveRefuelTransactionDetailBodyResponse getBody() {
		return body;
	}
	public void setBody(RetrieveRefuelTransactionDetailBodyResponse body) {
		this.body = body;
	}
}
