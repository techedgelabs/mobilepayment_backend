package com.techedge.mp.frontend.adapter.entities.user.validatepaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class ValidatePaymentMethodResponse extends BaseResponse {

	private ValidatePaymentMethodBodyResponse body;

	public ValidatePaymentMethodBodyResponse getBody() {
		return body;
	}
	public void setBody(ValidatePaymentMethodBodyResponse body) {
		this.body = body;
	}
}
