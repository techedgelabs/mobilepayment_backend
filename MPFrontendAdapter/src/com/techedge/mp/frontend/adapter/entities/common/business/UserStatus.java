package com.techedge.mp.frontend.adapter.entities.common.business;

public class UserStatus {

    private int     status;
    private boolean registrationCompleted;
    private boolean newUserType;
    private boolean termsOfServiceAccepted;
    private boolean eniStationUserType;
    private boolean DepositCardStepCompleted;
    private String  type;
    private String  sourceToken;
    private boolean sourcePaymentMethodFound;
    private String  source;
    

    public UserStatus() {}

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isRegistrationCompleted() {
        return registrationCompleted;
    }

    public void setRegistrationCompleted(boolean registrationCompleted) {
        this.registrationCompleted = registrationCompleted;
    }

    public boolean isNewUserType() {
        return newUserType;
    }

    public void setNewUserType(boolean newUserType) {
        this.newUserType = newUserType;
    }

    public boolean isTermsOfServiceAccepted() {
        return termsOfServiceAccepted;
    }

    public void setTermsOfServiceAccepted(boolean termsOfServiceAccepted) {
        this.termsOfServiceAccepted = termsOfServiceAccepted;
    }

    public boolean isEniStationUserType() {
        return eniStationUserType;
    }

    public void setEniStationUserType(boolean eniStationUserType) {
        this.eniStationUserType = eniStationUserType;
    }

    public boolean isDepositCardStepCompleted() {
        return DepositCardStepCompleted;
    }

    public void setDepositCardStepCompleted(boolean depositCardStepCompleted) {
        DepositCardStepCompleted = depositCardStepCompleted;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    public String getSourceToken() {
        return sourceToken;
    }
    
    public void setSourceToken(String sourceToken) {
        this.sourceToken = sourceToken;
    }
    
    public boolean isSourcePaymentMethodFound() {
        return sourcePaymentMethodFound;
    }
    
    public void setSourcePaymentMethodFound(boolean sourcePaymentMethodFound) {
        this.sourcePaymentMethodFound = sourcePaymentMethodFound;
    }
    
    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }
    
}
