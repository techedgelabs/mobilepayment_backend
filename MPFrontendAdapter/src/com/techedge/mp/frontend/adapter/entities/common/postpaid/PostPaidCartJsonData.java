package com.techedge.mp.frontend.adapter.entities.common.postpaid;


public class PostPaidCartJsonData  {

	private String productId;
	private String productDescription;
	private Integer amount;
	private Integer quantity;

	public PostPaidCartJsonData() {}	

	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
}