package com.techedge.mp.frontend.adapter.entities.postpaid.retrievependingtransaction;

import com.techedge.mp.frontend.adapter.entities.common.Cash;
import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;

public class RetrievePendingTransactionBodyResponse {

    private String                         sourceType;
    private String                         sourceStatus;
    private Pump                           pump;
    private Station                        station;
    private Cash                           cash;
    private PostPaidTransactionHistoryData transaction;
    private Integer                        nextPollingInterval;

    //private PendingRefuel pendingRefuel;

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getSourceStatus() {
        return sourceStatus;
    }

    public void setSourceStatus(String sourceStatus) {
        this.sourceStatus = sourceStatus;
    }

    public Pump getPump() {
        return pump;
    }

    public void setPump(Pump pump) {
        this.pump = pump;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Cash getCash() {
        return cash;
    }

    public void setCash(Cash cash) {
        this.cash = cash;
    }

    public PostPaidTransactionHistoryData getTransaction() {
        return transaction;
    }

    public void setTransaction(PostPaidTransactionHistoryData transaction) {
        this.transaction = transaction;
    }

    public Integer getNextPollingInterval() {
        return nextPollingInterval;
    }

    public void setNextPollingInterval(Integer nextPollingInterval) {
        this.nextPollingInterval = nextPollingInterval;
    }

}
