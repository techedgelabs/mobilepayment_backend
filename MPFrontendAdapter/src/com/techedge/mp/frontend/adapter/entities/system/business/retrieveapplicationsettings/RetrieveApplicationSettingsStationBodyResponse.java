package com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings;


public class RetrieveApplicationSettingsStationBodyResponse {

	private String gpsEnabled;
	
	public String getGpsEnabled() {
		return gpsEnabled;
	}
	public void setGpsEnabled(String gpsEnabled) {
		this.gpsEnabled = gpsEnabled;
	}
	
}
