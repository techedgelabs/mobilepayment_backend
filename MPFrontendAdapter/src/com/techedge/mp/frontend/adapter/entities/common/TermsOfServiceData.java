package com.techedge.mp.frontend.adapter.entities.common;

import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class TermsOfServiceData implements Validable {
	
	private String id;		
	private Boolean accepted;
	
	public TermsOfServiceData(){}

	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.id == null ||
		   ( !this.id.equals("PRIVACY_1") && !this.id.equals("INVIO_COMUNICAZIONI_ENI_1") && !this.id.equals("TERMS_CONDITIONS_1" ) && !this.id.equals("CLAUSOLE_VESSATORIE_1" ) && 
		           !this.id.equals("INVIO_COMUNICAZIONI_PARTNER_1" ) &&
		     !this.id.equals("PRIVACY_NEW_1") && !this.id.equals("INVIO_COMUNICAZIONI_ENI_NEW_1") && !this.id.equals("TERMS_CONDITIONS_NEW_1" ) && 
		     !this.id.equals("TERMS_CONDITIONS_NEW_GUEST_1" ) && !this.id.equals("CLAUSOLE_VESSATORIE_NEW_1" ) && !this.id.equals("CLAUSOLE_VESSATORIE_NEW_GUEST_1" ) && 
		     !this.id.equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1" ) &&
             !this.id.equals("REGOLAMENTO_YOUANDENI_NEW_1") && !this.id.equals("INVIO_COMUNICAZIONI_NEW_1") && !this.id.equals("GEOLOCALIZZAZIONE_NEW_1") && 
             !this.id.equals("NOTIFICATION_NEW_1")  && !this.id.equals("TRATTAMENTO_DATI_PERSONALI_NEW_1") &&
             !this.id.equals("PRIVACY_BUSINESS_1")  && !this.id.equals("TERMS_CONDITIONS_BUSINESS_1")  && !this.id.equals("CLAUSOLE_VESSATORIE_BUSINESS_1")  && 
             !this.id.equals("INVIO_COMUNICAZIONI_ENI_BUSINESS_1")  && !this.id.equals("INVIO_COMUNICAZIONI_PARTNER_BUSINESS_1") &&
             !this.id.equals("TERMS_CONDITIONS_NEW_MULTICARD_1")  && !this.id.equals("CLAUSOLE_VESSATORIE_NEW_MULTICARD_1")
		           ) ) {
		    
			status.setStatusCode(StatusCode.USER_CREATE_PRIVACY_DATA_WRONG);
			
			return status;
		}
		
		if(this.accepted == null || (this.accepted != Boolean.TRUE && this.accepted != Boolean.FALSE)) {
			
			status.setStatusCode(StatusCode.USER_CREATE_PRIVACY_DATA_WRONG);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

		return status;
		
	}



	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public Boolean getAccepted() {
		return accepted;
	}



	public void setAccepted(Boolean accepted) {
		this.accepted = accepted;
	}
	
	
}
