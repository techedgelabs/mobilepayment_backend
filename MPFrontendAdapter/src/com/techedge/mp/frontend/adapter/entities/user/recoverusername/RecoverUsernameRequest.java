package com.techedge.mp.frontend.adapter.entities.user.recoverusername;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RecoverUsernameRequest extends AbstractRequest implements Validable {

    private Status                     status = new Status();

    private Credential                 credential;
    private RecoverUsernameRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RecoverUsernameRequestBody getBody() {
        return body;
    }

    public void setBody(RecoverUsernameRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RECOVER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.USER_RECOVER_USERNAME_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        RecoverUsernameRequest recoverUsernameRequest = this;

        String response = getUserServiceRemote().recoverUsername(recoverUsernameRequest.getCredential().getTicketID(), recoverUsernameRequest.getCredential().getRequestID(),
                recoverUsernameRequest.getBody().getFiscalCode());

        RecoverUsernameResponse recoverUsernameResponse = new RecoverUsernameResponse();

        if (response.contains("USER_RECOVER_USERNAME")) {
            response = StatusCode.USER_RECOVER_USERNAME_SUCCESS;
        }

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        recoverUsernameResponse.setStatus(status);

        return recoverUsernameResponse;
    }

}
