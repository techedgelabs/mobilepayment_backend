package com.techedge.mp.frontend.adapter.entities.user.v2.retrievetermsofservicebygroup;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.DocumentCheckInfo;
import com.techedge.mp.core.business.interfaces.DocumentInfo;
import com.techedge.mp.core.business.interfaces.RetrieveTermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.FrontendParameter;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveTermsOfServiceByGroupRequest extends AbstractRequest implements Validable {

    private Status                            status = new Status();

    private Credential                        credential;
    private RetrieveTermsOfServiceByGroupRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveTermsOfServiceByGroupRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveTermsOfServiceByGroupRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("TERMS_SERVICE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        // Lettura parametri da db
        String termsOfServiceInfo           = null;
        String termsOfServiceInfoLoyalty    = null;
        String termsOfServiceMessage        = null;
        String termsOfServiceMessageLoyalty = null;
        try {
            termsOfServiceInfo           = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_TERMS_OF_SERVICE_INFO_NEW_ACQUIRER);
            termsOfServiceInfoLoyalty    = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_TERMS_OF_SERVICE_INFO_LOYALTY);
            termsOfServiceMessage        = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_TERMS_OF_SERVICE_MESSAGE_NEW_ACQUIRER);
            termsOfServiceMessageLoyalty = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_TERMS_OF_SERVICE_MESSAGE_LOYALTY);
        }
        catch (ParameterNotFoundException e) {

            // TODO Auto-generated catch block
            e.printStackTrace();

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);
            return baseResponse;
        }
        RetrieveTermsOfServiceByGroupRequest retrieveTermsOfServiceByGroupRequest = this;
        String group = "";
        
        if (retrieveTermsOfServiceByGroupRequest.getBody() != null) {
            group = retrieveTermsOfServiceByGroupRequest.getBody().getGroup();
        }
        
        RetrieveTermsOfServiceData retrieveDocumentsData = getUserV2ServiceRemote().retrieveTermsOfServiceByGroup(retrieveTermsOfServiceByGroupRequest.getCredential().getTicketID(),
                retrieveTermsOfServiceByGroupRequest.getCredential().getRequestID(), group);

        RetrieveTermsOfServiceByGroupResponse retrieveTermsOfServiceByGroupResponse = new RetrieveTermsOfServiceByGroupResponse();

        status.setStatusCode(retrieveDocumentsData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveDocumentsData.getStatusCode()));

        RetrieveTermsOfServiceByGroupResponseBody retrieveTermsOfServiceByGroupResponseBody = new RetrieveTermsOfServiceByGroupResponseBody();

        for(DocumentInfo documentInfo : retrieveDocumentsData.getDocuments()) {
            
            System.out.println("documentInfo: " + documentInfo.getTitle());
            
            Boolean nullCheckList = true;
            
            for(DocumentCheckInfo documentCheckInfo : documentInfo.getCheckList()) {
                
                if ( (documentCheckInfo.getConditionText() != null && !documentCheckInfo.getConditionText().isEmpty()) ||
                     (documentCheckInfo.getSubTitle()      != null && !documentCheckInfo.getSubTitle().isEmpty()) ) {
                    
                    nullCheckList = false;
                }
            }
            
            if (nullCheckList) {
                
                documentInfo.setCheckList(null);
            }
            
            retrieveTermsOfServiceByGroupResponseBody.getTermsOfServiceList().add(documentInfo);
        }
        
        retrieveTermsOfServiceByGroupResponseBody.setTermsOfServiceMessage(termsOfServiceMessage);
        
        if (group.equals("LOYALTY")) {
            retrieveTermsOfServiceByGroupResponseBody.setTermsOfServiceInfo(termsOfServiceInfo);
            retrieveTermsOfServiceByGroupResponseBody.setTermsOfServiceMessage(termsOfServiceMessageLoyalty);
        }

        retrieveTermsOfServiceByGroupResponse.setBody(retrieveTermsOfServiceByGroupResponseBody);
        retrieveTermsOfServiceByGroupResponse.setStatus(status);

        return retrieveTermsOfServiceByGroupResponse;
    }

}
