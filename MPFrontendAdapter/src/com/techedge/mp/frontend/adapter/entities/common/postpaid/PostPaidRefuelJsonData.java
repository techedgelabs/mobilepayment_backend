package com.techedge.mp.frontend.adapter.entities.common.postpaid;



public class PostPaidRefuelJsonData {

	
	private String pumpId;
	private String productId;
	private String productDescription;
	private String fuelType;
	private Double fuelQuantity;
	private Double fuelAmount;
//	private PostPaidTransactionJsonData transactionData;
	
	
	public PostPaidRefuelJsonData() {}


	public String getFuelType() {
		return fuelType;
	}


	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}




	public String getProductId() {
		return productId;
	}




	public void setProductId(String productId) {
		this.productId = productId;
	}




	public String getProductDescription() {
		return productDescription;
	}




	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}




	public Double getFuelQuantity() {
		return fuelQuantity;
	}




	public void setFuelQuantity(Double fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}


	public Double getFuelAmount() {
		return fuelAmount;
	}


	public void setFuelAmount(Double fuelAmount) {
		this.fuelAmount = fuelAmount;
	}


	public String getPumpId() {
		return pumpId;
	}


	public void setPumpId(String pumpId) {
		this.pumpId = pumpId;
	}




//	public PostPaidTransactionJsonData getTransactionData() {
//		return transactionData;
//	}
//
//
//
//
//	public void setTransactionData(PostPaidTransactionJsonData transactionData) {
//		this.transactionData = transactionData;
//	}
	
	
	
	
	
}
