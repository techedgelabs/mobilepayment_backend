package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;

public class ReceiptDynamic {

    private List<ReceiptDynamicData> paymentData = new ArrayList<ReceiptDynamicData>();
    private List<ReceiptDynamicData> refuelData  = new ArrayList<ReceiptDynamicData>();
    private List<ReceiptDynamicData> shopData    = new ArrayList<ReceiptDynamicData>();
    private List<ReceiptDynamicData> loyaltyData = new ArrayList<ReceiptDynamicData>();
    private List<ReceiptDynamicData> voucherData = new ArrayList<ReceiptDynamicData>();
    private List<ReceiptDynamicData> promoData   = new ArrayList<ReceiptDynamicData>();

    public List<ReceiptDynamicData> getPaymentData() {
        return paymentData;
    }

    public List<ReceiptDynamicData> getRefuelData() {
        return refuelData;
    }

    public List<ReceiptDynamicData> getShopData() {
        return shopData;
    }

    public List<ReceiptDynamicData> getLoyaltyData() {
        return loyaltyData;
    }

    public List<ReceiptDynamicData> getVoucherData() {
        return voucherData;
    }

    public List<ReceiptDynamicData> getPromoData() {
        return promoData;
    }

    public int addToPaymentData(int parentIndex, String key, String label, String unit, String value, int position, String link) {
        return addToReceiptDynamicData(paymentData, parentIndex, key, label, unit, value, position, link);
    }

    public int addToRefuelData(int parentIndex, String key, String label, String unit, String value, int position, String link) {
        return addToReceiptDynamicData(refuelData, parentIndex, key, label, unit, value, position, link);
    }

    public int addToShopData(int parentIndex, String key, String label, String unit, String value, int position, String link) {
        return addToReceiptDynamicData(shopData, parentIndex, key, label, unit, value, position, link);
    }

    public int addToLoyaltyData(int parentIndex, String key, String label, String unit, String value, int position, String link) {
        return addToReceiptDynamicData(loyaltyData, parentIndex, key, label, unit, value, position, link);
    }

    public int addToVoucherData(int parentIndex, String key, String label, String unit, String value, int position, String link) {
        return addToReceiptDynamicData(voucherData, parentIndex, key, label, unit, value, position, link);
    }

    public int addToPromoData(int parentIndex, String key, String label, String unit, String value, int position, String link) {
        return addToReceiptDynamicData(promoData, parentIndex, key, label, unit, value, position, link);
    }

    private int addToReceiptDynamicData(List<ReceiptDynamicData> receiptData, int parentIndex, String key, String label, String unit, String value, int position, String link) {

        ReceiptDynamicData itemData = new ReceiptDynamicData();
        int index = -1;

        itemData.setLabel(label);
        itemData.setKey(key);
        itemData.setUnit(unit);
        itemData.setValue(value);
        itemData.setPosition(position);
        itemData.setLink(link);

        if (parentIndex != -1) {
            ReceiptDynamicData parentData = receiptData.get(parentIndex);
            parentData.getChild().add(itemData);
            receiptData.set(parentIndex, parentData);
            index = parentIndex;
        }
        else {
            if (receiptData.add(itemData)) {
                index = (receiptData.size() - 1);
            }
        }

        return index;
    }

    
}
