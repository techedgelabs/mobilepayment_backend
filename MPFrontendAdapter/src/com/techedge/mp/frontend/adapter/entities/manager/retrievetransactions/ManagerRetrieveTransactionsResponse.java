package com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class ManagerRetrieveTransactionsResponse extends BaseResponse {

    private ManagerRetrieveTransactionsBodyResponse body;

    public synchronized ManagerRetrieveTransactionsBodyResponse getBody() {
        return body;
    }

    public synchronized void setBody(ManagerRetrieveTransactionsBodyResponse body) {
        this.body = body;
    }

}
