package com.techedge.mp.frontend.adapter.entities.parking.v2.approveparkingtransaction;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.ApproveParkingTransactionResult;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZonePriceInfo;
import com.techedge.mp.frontend.adapter.entities.common.v2.PlateNumberInfo;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ApproveParkingTransactionRequest extends AbstractRequest implements Validable {

    private ApproveParkingTransactionCredential  credential;

    private ApproveParkingtransactionRequestBody body;

    public ApproveParkingtransactionRequestBody getBody() {
        return body;
    }

    public void setBody(ApproveParkingtransactionRequestBody body) {
        this.body = body;
    }

    public ApproveParkingTransactionCredential getCredential() {
        return credential;
    }

    public void setCredential(ApproveParkingTransactionCredential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.credential != null) {
            
            status = this.credential.check();
        
            if(!status.getStatusCode().equals(StatusCode.APPROVE_PARKING_TRANSACTION_SUCCESS))
                return status;
        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_AUTH_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        ApproveParkingTransactionRequest approveParkingTransactionRequestRequest = this;

        ApproveParkingtransactionRequestBody approveParkingtransactionRequestBody = approveParkingTransactionRequestRequest.getBody();

        ApproveParkingTransactionResult result = getParkingTransactionV2ServiceRemote().approveParkingTransaction(approveParkingTransactionRequestRequest.getCredential().getTicketID(),
                approveParkingTransactionRequestRequest.getCredential().getRequestID(), approveParkingtransactionRequestBody.getTransactionID(),
                approveParkingtransactionRequestBody.getPaymentMethod().getId(), approveParkingtransactionRequestBody.getPaymentMethod().getType(),
                approveParkingTransactionRequestRequest.getCredential().getPin());

        ApproveParkingTransactionResponse response = new ApproveParkingTransactionResponse();

        if (result != null && result.getStatusCode().equals(ResponseHelper.PARKING_APPROVE_PARKING_TRANSACTION_SUCCESS)) {
            
            ApproveParkingtransactionResponseBody responseBody = new ApproveParkingtransactionResponseBody();

            ParkingZonePriceInfo parkingZonePriceInfo = new ParkingZonePriceInfo();
            
            PlateNumberInfo plateNumberInfo = new PlateNumberInfo();
            plateNumberInfo.setPlateNumber(result.getPlateNumber());
            plateNumberInfo.setDescription(result.getPlateNumberDescription());
            parkingZonePriceInfo.setPlateNumber(plateNumberInfo);
            
            parkingZonePriceInfo.setPrice(AmountConverter.toMobile(result.getPrice()));
            parkingZonePriceInfo.setStallCode(result.getStallCode());
            parkingZonePriceInfo.setNotice(result.getNotice());
            
            ParkingZone parkingZone = new ParkingZone();
            parkingZone.setId(result.getParkingZoneId());
            parkingZone.setName(result.getParkingZoneName());
            parkingZone.setDescription(result.getParkingZoneDescription());
            parkingZone.setCityId(result.getCityId());
            parkingZone.setCityName(result.getCityName());
            parkingZone.setAdministrativeAreaLevel2Code(result.getAdministrativeAreaLevel2Code());
            parkingZonePriceInfo.setParkingZone(parkingZone);

            parkingZonePriceInfo.setParkingStartTime(CustomTimestamp.convertToCustomTimestamp(result.getParkingStartTime()));
            parkingZonePriceInfo.setParkingEndTime(CustomTimestamp.convertToCustomTimestamp(result.getParkingEndTime()));
            parkingZonePriceInfo.setTransactionID(result.getTransactionId());

            PaymentMethod paymentMethod = new PaymentMethod();
            paymentMethod.setId(result.getPaymentMethodId());
            parkingZonePriceInfo.setPaymentMethod(paymentMethod);
            
            responseBody.setParkingZonePriceInfo(parkingZonePriceInfo);

            response.setBody(responseBody);
        }
        
        Status status = new Status();
        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(prop.getProperty(result.getStatusCode()));

        response.setStatus(status);

        return response;

    }

}
