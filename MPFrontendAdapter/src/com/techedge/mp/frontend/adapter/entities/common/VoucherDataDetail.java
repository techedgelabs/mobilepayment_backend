package com.techedge.mp.frontend.adapter.entities.common;

public class VoucherDataDetail {
    
    private String label;
    
    private String value;
    
    public String getLabel() {
        return label;
    }
    public void setLabel(String label) {
        this.label = label;
    }
    public String getValue() {
        return value;
    }
    public void setValue(String value) {
        this.value = value;
    }
    
    
}
