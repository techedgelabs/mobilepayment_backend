package com.techedge.mp.frontend.adapter.entities.common;

public class ActiveCities {

    private LocationData locationData;
    private String stationCount;
    private Boolean currentCity;
    
    public LocationData getLocationData() {
        return locationData;
    }
    
    public void setLocationData(LocationData locationData) {
        this.locationData = locationData;
    }
    
    public String getStationCount() {
        return stationCount;
    }
    
    public void setStationCount(String stationCount) {
        this.stationCount = stationCount;
    }
    
    public Boolean getCurrentCity() {
        return currentCity;
    }

    public void setCurrentCity(Boolean currentCity) {
        this.currentCity = currentCity;
    }
    
    
}
