package com.techedge.mp.frontend.adapter.entities.refuel.v2.createmulticardtransaction;

import java.util.Date;

import com.techedge.mp.bpel.operation.refuel.business.interfaces.StationInfoRequest;
import com.techedge.mp.core.business.interfaces.CreateMulticardRefuelResponse;
import com.techedge.mp.core.business.interfaces.OperationType;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetPumpStatusResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class CreateMulticardRefuelTransactionRequest extends AbstractRequest implements Validable {

    private Status                                      status = new Status();

    private Credential                                  credential;
    private CreateMulticardRefuelTransactionBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateMulticardRefuelTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(CreateMulticardRefuelTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("MULTICARD-CREATE-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.MULTICARD_REFUEL_TRANSACTION_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String response = "";

        Double maxAmount = null;
        Double thresholdAmount = null;

        response = getUserServiceRemote().checkAuthorization(this.getCredential().getTicketID(), OperationType.CREATE_TRANSACTION);

        if (!response.equals(ResponseHelper.CHECK_AUTHORIZATION_SUCCESS)) {

            status.setStatusCode(response);
            status.setStatusMessage(prop.getProperty(response));
        }
        else {

            String stationID = this.getBody().getMulticardRefuelPaymentData().getStationID();
            String pumpID = this.getBody().getMulticardRefuelPaymentData().getPumpID();
            String requestID = String.valueOf(new Date().getTime());
            String outOfRange = this.getBody().getMulticardRefuelPaymentData().getOutOfRange();
            PaymentMethod paymentMethod = this.getBody().getMulticardRefuelPaymentData().getPaymentMethod();
            String paymentCryptogram = this.getBody().getMulticardRefuelPaymentData().getPaymentCryptogram();

            if (stationID != null && pumpID != null) {

                System.out.println(requestID + " - " + stationID + " - " + pumpID);
                if (getForecourtInfoServiceRemote() == null) {
                    System.out.println("FORECOURT NULL");
                }
                GetStationDetailsResponse getStationDetailsResponse = getForecourtInfoServiceRemote().getStationDetails(requestID, stationID, pumpID, Boolean.FALSE);

                if (getStationDetailsResponse != null) {
                    
                    String stationStatusCode = getStationDetailsResponse.getStatusCode();
    
                    if (Validator.isValid(stationStatusCode)) {
    
                        if (!getStationDetailsResponse.getStationDetail().getPumpDetails().isEmpty()) {
    
                            GetPumpStatusResponse pumpStatusResponse = getForecourtInfoServiceRemote().getPumpStatus(requestID, stationID, pumpID, null);
    
                            if (Validator.isValid(pumpStatusResponse.getStatusCode())) {
    
                                // Il processo di rifornimento viene avviato solo se la pompa � disponibile
                                
                                PumpDetail pumpSelected = null;
    
                                for (PumpDetail pumpDetail : getStationDetailsResponse.getStationDetail().getPumpDetails()) {
    
                                    pumpSelected = pumpDetail;
                                }
    
                                Integer pumpNumber = 0;
                                try {
                                    pumpNumber = Integer.parseInt(pumpSelected.getPumpNumber());
                                }
                                catch (Exception ex) {
    
                                    System.out.println("Pump number parsing error: " + pumpSelected.getPumpNumber());
                                }
                                
                                String productID = "";
                                String productDescription = "";
    
                                String refuelMode = pumpStatusResponse.getRefuelMode();
    
                                Double amount = AmountConverter.toInternal(this.getBody().getMulticardRefuelPaymentData().getAmount());
                                Double amountVoucher = 0.0;
    
                                System.out.println("createRefuel");
                                System.out.println("stationID: " + stationID);
                                System.out.println("pumpID: " + pumpID);
                                System.out.println("pumpNumber: " + pumpNumber);
                                System.out.println("productID: " + productID);
                                System.out.println("productDescription: " + productDescription);
                                System.out.println("amount: " + amount);
                                System.out.println("amountVoucher: " + amountVoucher);
                                System.out.println("paymentCryptogram: " + paymentCryptogram);
                                System.out.println("outOfRange: " + outOfRange);
                                System.out.println("refuelMode: " + refuelMode);
    
                                CreateMulticardRefuelResponse createMulticardRefuelResponse = getTransactionV2ServiceRemote().createMulticardRefuel(this.getCredential().getTicketID(),
                                        this.getCredential().getRequestID(), stationID, pumpID, pumpNumber, productID, productDescription, amount, paymentMethod.getId(), paymentCryptogram, outOfRange, refuelMode);
    
                                if (createMulticardRefuelResponse.getStatusCode() != null && createMulticardRefuelResponse.getStatusCode().equals(ResponseHelper.REFUEL_TRANSACTION_CREATE_SUCCESS)) {
    
                                    // call verso il BPEL
    
                                    System.out.println("BPEL starting");
    
                                    //System.out.println("RESPONSE SHOPLOGIN: " + createRefuelResponse.getShopLogin());
                                    //System.out.println("RESPONSE ACQUIRER_ID: " + createRefuelResponse.getAcquirerID());
    
                                    String tokenValue = createMulticardRefuelResponse.getTokenValue();
                                    if (tokenValue == null) {
                                        tokenValue = "";
                                    }
                                    
                                    Double amountAuthorizedDouble = createMulticardRefuelResponse.getAmountAuthorized();
    
                                    response = getBpelServiceRemote().startRefuelingProcess(createMulticardRefuelResponse.getTransactionID(), stationID, pumpID,
                                            amountAuthorizedDouble, createMulticardRefuelResponse.getAcquirerID(),
                                            createMulticardRefuelResponse.getShopLogin(), createMulticardRefuelResponse.getCurrency(), createMulticardRefuelResponse.getTransactionID(),
                                            tokenValue, "", productDescription, productID);
    
                                    System.out.println("BPEL response: " + response);
    
                                    if (Validator.isValid(response)) {
    
                                        status.setStatusCode(StatusCode.MULTICARD_REFUEL_TRANSACTION_CREATE_SUCCESS);
                                        status.setStatusMessage(prop.getProperty(StatusCode.MULTICARD_REFUEL_TRANSACTION_CREATE_SUCCESS));
                                    }
                                    else {
    
                                        status.setStatusCode(StatusCode.MULTICARD_REFUEL_TRANSACTION_CREATE_SUCCESS);
                                        status.setStatusMessage(prop.getProperty(StatusCode.MULTICARD_REFUEL_TRANSACTION_CREATE_SUCCESS));
                                    }
                                }
                                else {
    
                                    maxAmount = createMulticardRefuelResponse.getMaxAmount();
                                    thresholdAmount = createMulticardRefuelResponse.getThresholdAmount();
    
                                    status.setStatusCode(createMulticardRefuelResponse.getStatusCode());
                                    status.setStatusMessage(prop.getProperty(createMulticardRefuelResponse.getStatusCode()));
                                }
                            }
                            else {
    
                                status.setStatusCode(pumpStatusResponse.getStatusCode());
                                status.setStatusMessage(prop.getProperty(pumpStatusResponse.getStatusCode()));
                            }
                        }
                        else {
    
                            status.setStatusCode(StatusCode.MULTICARD_REFUEL_TRANSACTION_CREATE_PUMP_NOT_FOUND);
                            status.setStatusMessage(prop.getProperty(StatusCode.MULTICARD_REFUEL_TRANSACTION_CREATE_PUMP_NOT_FOUND));
                        }
                    }
                    else {
    
                        status.setStatusCode(stationStatusCode);
                        status.setStatusMessage(prop.getProperty(stationStatusCode));
                    }
                }
            }
            else {

                status.setStatusCode(StatusCode.MULTICARD_REFUEL_TRANSACTION_CREATE_DATA_WRONG);
                status.setStatusMessage(prop.getProperty(StatusCode.MULTICARD_REFUEL_TRANSACTION_CREATE_DATA_WRONG));
            }
        }

        CreateMulticardRefuelTransactionResponse createMulticardRefuelTransactionResponse = new CreateMulticardRefuelTransactionResponse();

        CreateMulticardRefuelTransactionBodyResponse createMulticardRefuelTransactionBodyResponse = new CreateMulticardRefuelTransactionBodyResponse();

        if (maxAmount != null) {
            Integer integerMaxAmount = AmountConverter.toMobile(maxAmount);
            createMulticardRefuelTransactionBodyResponse.setMaxAmount(integerMaxAmount);
        }
        if (thresholdAmount != null) {
            Integer integerThresholdAmount = AmountConverter.toMobile(thresholdAmount);
            createMulticardRefuelTransactionBodyResponse.setThresholdAmount(integerThresholdAmount);
        }

        createMulticardRefuelTransactionResponse.setBody(createMulticardRefuelTransactionBodyResponse);
        createMulticardRefuelTransactionResponse.setStatus(status);

        return createMulticardRefuelTransactionResponse;

    }
}