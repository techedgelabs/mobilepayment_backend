package com.techedge.mp.frontend.adapter.entities.user.v2.externalauthentication;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class ExternalAuthenticationUserResponseSuccess extends BaseResponse {

    private ExternalAuthenticationUserResponseBody body;

    public ExternalAuthenticationUserResponseBody getBody() {
        return body;
    }

    public void setBody(ExternalAuthenticationUserResponseBody body) {
        this.body = body;
    }

}
