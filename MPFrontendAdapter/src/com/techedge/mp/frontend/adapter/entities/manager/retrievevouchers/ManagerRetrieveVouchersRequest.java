package com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers;

import java.sql.Timestamp;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.postpaid.VoucherHistory;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.VoucherData;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ManagerRetrieveVouchersRequest extends AbstractRequest implements Validable {

    private Credential                         credential;
    private ManagerRetrieveVouchersBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ManagerRetrieveVouchersBodyRequest getBody() {
        return body;
    }

    public void setBody(ManagerRetrieveVouchersBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("MANAGER-RETRIEVE-VOUCHERS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(ResponseHelper.MANAGER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.MANAGER_RETRIEVE_VOUCHERS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        ManagerRetrieveVouchersRequest retrieveVouchersRequest = this;

        ManagerRetrieveVouchersResponse retrieveVouchersResponse = new ManagerRetrieveVouchersResponse();
        ManagerRetrieveVouchersBodyResponse retrieveVouchersBodyResponse = new ManagerRetrieveVouchersBodyResponse();

        String ticketID = retrieveVouchersRequest.getCredential().getTicketID();
        String requestID = retrieveVouchersRequest.getCredential().getRequestID();
        String stationID = retrieveVouchersRequest.getBody().getStationID();
        CustomDate startDate = retrieveVouchersRequest.getBody().getStartDate();
        CustomDate endDate = retrieveVouchersRequest.getBody().getEndDate();

        com.techedge.mp.core.business.interfaces.ManagerRetrieveVouchersResponse managerRetrieveVouchersResponse = getManagerServiceRemote().retrieveVouchers(ticketID, requestID,
                stationID, startDate.toDate(), endDate.toDate());

        if (!managerRetrieveVouchersResponse.getStatusCode().equals(ResponseHelper.MANAGER_RETRIEVE_VOUCHERS_SUCCESS)) {

            Status statusResponse = new Status();
            statusResponse.setStatusCode(managerRetrieveVouchersResponse.getStatusCode());
            statusResponse.setStatusMessage("");

            retrieveVouchersResponse.setStatus(statusResponse);
        }
        else {

            Date systemDate = new Date();

            for (VoucherHistory voucherHistory : managerRetrieveVouchersResponse.getVoucherHistory()) {

                ManagerRetrieveVouchersBodyDataResponse managerRetrieveVouchersBodyDataResponse = new ManagerRetrieveVouchersBodyDataResponse();

                managerRetrieveVouchersBodyDataResponse.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(voucherHistory.getDate().getTime())));
                managerRetrieveVouchersBodyDataResponse.setStationID(voucherHistory.getStationID());
                managerRetrieveVouchersBodyDataResponse.setPumpNumber(voucherHistory.getPumpNumber());
                managerRetrieveVouchersBodyDataResponse.setSystemDate(CustomTimestamp.createCustomTimestamp(new Timestamp(systemDate.getTime())));
                managerRetrieveVouchersBodyDataResponse.setTotalConsumed(AmountConverter.toMobile(voucherHistory.getTotalConsumed()));

                for (Voucher voucher : voucherHistory.getVoucherList()) {

                    VoucherData voucherData = new VoucherData();

                    voucherData.setCode(voucher.getCode());
                    voucherData.setConsumedValue(AmountConverter.toMobile(voucher.getConsumedValue()));
                    voucherData.setExpirationDate(CustomTimestamp.createCustomTimestamp(new Timestamp(voucher.getExpirationDate().getTime())));
                    voucherData.setInitialValue(AmountConverter.toMobile(voucher.getInitialValue()));
                    voucherData.setPromoCode(voucher.getPromoCode());
                    voucherData.setPromoDescription(voucher.getPromoDescription());
                    voucherData.setPromoDoc(voucher.getPromoDoc());
                    voucherData.setStatus(voucher.getStatus());
                    voucherData.setType(voucher.getType());
                    voucherData.setValue(AmountConverter.toMobile(voucher.getValue()));
                    voucherData.setVoucherBalanceDue(AmountConverter.toMobile(voucher.getVoucherBalanceDue()));

                    managerRetrieveVouchersBodyDataResponse.getVouchers().add(voucherData);
                }

                retrieveVouchersBodyResponse.getVoucherHistory().add(managerRetrieveVouchersBodyDataResponse);
            }

            retrieveVouchersResponse.setBody(retrieveVouchersBodyResponse);

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.MANAGER_RETRIEVE_VOUCHERS_SUCCESS);
            statusResponse.setStatusMessage(prop.getProperty("OK"));

            retrieveVouchersResponse.setStatus(statusResponse);
        }

        return retrieveVouchersResponse;
    }

}
