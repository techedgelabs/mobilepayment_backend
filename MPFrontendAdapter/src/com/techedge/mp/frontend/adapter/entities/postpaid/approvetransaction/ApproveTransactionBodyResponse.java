package com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction;



public class ApproveTransactionBodyResponse {
	
	private Integer checkPinAttemptsLeft;

	public Integer getCheckPinAttemptsLeft() {
		return checkPinAttemptsLeft;
	}

	public void setCheckPinAttemptsLeft(Integer checkPinAttemptsLeft) {
		this.checkPinAttemptsLeft = checkPinAttemptsLeft;
	}
}
