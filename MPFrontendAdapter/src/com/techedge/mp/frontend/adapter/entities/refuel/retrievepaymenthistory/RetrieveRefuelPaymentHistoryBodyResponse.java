package com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory;

import java.util.ArrayList;
import java.util.List;


public class RetrieveRefuelPaymentHistoryBodyResponse {

	private int pageOffset;
	private int pagesTotal;
	private List<RetrieveRefuelPaymentHistoryListResponse> paymentHistory = new ArrayList<RetrieveRefuelPaymentHistoryListResponse>(0);
	
	

	public List<RetrieveRefuelPaymentHistoryListResponse> getPaymentHistory() {
		return paymentHistory;
	}

	public void setPaymentHistory(
			List<RetrieveRefuelPaymentHistoryListResponse> paymentHistory) {
		this.paymentHistory = paymentHistory;
	}

	public int getPageOffset() {
		return pageOffset;
	}

	public void setPageOffset(int pageOffset) {
		this.pageOffset = pageOffset;
	}

	public int getPagesTotal() {
		return pagesTotal;
	}

	public void setPagesTotal(int pagesTotal) {
		this.pagesTotal = pagesTotal;
	}
	
	
	
	
}
