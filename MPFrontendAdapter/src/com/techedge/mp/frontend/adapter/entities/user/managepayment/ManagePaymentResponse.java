package com.techedge.mp.frontend.adapter.entities.user.managepayment;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class ManagePaymentResponse extends BaseResponse {
	
	
	private ManagePaymentBodyResponse body;

	
	public ManagePaymentBodyResponse getBody() {
		return body;
	}

	public void setBody(ManagePaymentBodyResponse body) {
		this.body = body;
	}
	
}
