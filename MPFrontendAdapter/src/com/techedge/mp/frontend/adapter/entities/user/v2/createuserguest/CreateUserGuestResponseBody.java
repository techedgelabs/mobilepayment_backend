package com.techedge.mp.frontend.adapter.entities.user.v2.createuserguest;

import java.io.Serializable;

public class CreateUserGuestResponseBody implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3367489987400244385L;
    
    private String username;
    private String encodedPassword;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEncodedPassword() {
        return encodedPassword;
    }

    public void setEncodedPassword(String encodedPassword) {
        this.encodedPassword = encodedPassword;
    }

}
