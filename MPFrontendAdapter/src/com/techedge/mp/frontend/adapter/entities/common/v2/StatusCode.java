package com.techedge.mp.frontend.adapter.entities.common.v2;

public class StatusCode {

    public final static String SYSTEM_ERROR                                              = "SYSTEM_ERROR_500";

    public final static String USER_V2_AUTH_SUCCESS                                         = "USER_AUTH_200";
    public final static String USER_V2_AUTH_FAILURE                                         = "USER_AUTH_300";
    public final static String USER_V2_AUTH_LOGIN_ERROR                                     = "USER_AUTH_301";
    public final static String USER_V2_AUTH_EMAIL_WRONG                                     = "USER_AUTH_401";
    public final static String USER_V2_AUTH_PASSWORD_WRONG                                  = "USER_AUTH_402";
    public final static String USER_V2_AUTH_PASSWORD_SHORT                                  = "USER_AUTH_409";
    public final static String USER_V2_AUTH_NUMBER_LESS                                     = "USER_AUTH_410";
    public final static String USER_V2_AUTH_LOWER_LESS                                      = "USER_AUTH_411";
    public final static String USER_V2_AUTH_UPPER_LESS                                      = "USER_AUTH_412";
    public final static String USER_V2_AUTH_INVALID_REQUEST                                 = "USER_REQU_400";
    public final static String USER_V2_AUTH_TICKETID_WRONG                                  = "USER_AUTH_413";

    public final static String USER_V2_UPDATE_USERS_DATA_SUCCESS                            = "USER_UPDATE_USERS_DATA_200";
    public final static String USER_V2_UPDATE_USERS_DATA_FAILURE                            = "USER_UPDATE_USERS_DATA_300";
    public final static String USER_V2_UPDATE_USERS_DATA_INVALID_REQUEST                    = "USER_REQU_400";
    public final static String USER_V2_UPDATE_USERS_DATA_UNAUTHORIZED                       = "USER_REQU_403";
    
    public final static String USER_V2_LOGOUT_SUCCESS                                       = "USER_LOGOUT_200";
    public final static String USER_V2_LOGOUT_FAILURE                                       = "USER_LOGOUT_300";
    public final static String USER_V2_LOGOUT_ERROR                                         = "USER_LOGOUT_301";

    public final static String USER_V2_REQU_INVALID_REQUEST                                 = "USER_REQU_400";
    public final static String USER_V2_REQU_INVALID_TICKET                                  = "USER_REQU_401";
    public final static String USER_V2_REQU_JSON_SYNTAX_ERROR                               = "USER_REQU_402";
    public final static String USER_V2_REQU_UNAUTHORIZED                                    = "USER_REQU_403";
    public final static String USER_V2_REQU_UNSUPPORTED_DEVICE                              = "USER_REQU_405";
    
    public final static String USER_V2_PROMO4ME_SUCCESS                                     = "USER_PROMO4ME_200";
    public final static String USER_V2_PROMO4ME_FAILURE                                     = "USER_PROMO4ME_300";
    public final static String USER_V2_PROMO4ME_INVALID_REQUEST                             = "USER_REQU_400";
    public final static String USER_V2_PROMO4ME_UNAUTHORIZED                                = "USER_REQU_403";
    
    public final static String USER_V2_GETLANDING_SUCCESS                                   = "USER_GETLANDING_200";
    public final static String USER_V2_GETLANDING_FAILURE                                   = "USER_GETLANDING_300";
    public final static String USER_V2_GETLANDING_INVALID_REQUEST                           = "USER_REQU_400";
    public final static String USER_V2_GETLANDING_UNAUTHORIZED                              = "USER_REQU_403";
    
    public final static String USER_V2_GETPROMOPOPUP_SUCCESS                                = "USER_GETPROMOPOPUP_200";
    public final static String USER_V2_GETPROMOPOPUP_FAILURE                                = "USER_GETPROMOPOPUP_300";
    public final static String USER_V2_GETPROMOPOPUP_PROMO_NOT_FOUND                        = "USER_GETPROMOPOPUP_400";
    public final static String USER_V2_GETPROMOPOPUP_INVALID_REQUEST                        = "USER_REQU_400";
    public final static String USER_V2_GETPROMOPOPUP_UNAUTHORIZED                           = "USER_REQU_403";
    
    public final static String USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_SUCCESS             = "USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_200";
    public final static String USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_FAILURE             = "USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_300";
    public final static String USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_INVALID_REQUEST     = "USER_REQU_400";
    public final static String USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_UNAUTHORIZED        = "USER_REQU_403";
    
    public final static String POSTPAID_V2_APPROVE_TRANSACTION_SUCCESS                       = "POSTPAID_V2_APPROVE_TRANSACTION_200";
    public final static String POSTPAID_V2_APPROVE_TRANSACTION_FAILURE                       = "POSTPAID_V2_APPROVE_TRANSACTION_300";
    public final static String POSTPAID_V2_APPROVE_TRANSACTION_INVALID_REQUEST               = "USER_REQU_400";
    public final static String POSTPAID_V2_APPROVE_TRANSACTION_UNAUTHORIZED                  = "USER_REQU_403";
    public final static String POSTPAID_V2_APPROVE_TRANSACTION_PIN_WRONG                     = "POSTPAID_V2_APPROVE_TRANSACTION_301";
    public final static String POSTPAID_V2_APPROVE_TRANSACTION_ID_WRONG                      = "POSTPAID_V2_APPROVE_TRANSACTION_302";
    
    public final static String POSTPAID_V2_CREATE_TRANSACTION_SUCCESS                        = "POSTPAID_V2_CREATE_TRANSACTION_200";
    public final static String POSTPAID_V2_CREATE_TRANSACTION_FAILURE                        = "POSTPAID_V2_CREATE_TRANSACTION_300";
    public final static String POSTPAID_V2_CREATE_TRANSACTION_INVALID_REQUEST                = "USER_REQU_400";
    public final static String POSTPAID_V2_CREATE_TRANSACTION_UNAUTHORIZED                   = "USER_REQU_403";

    public final static String POSTPAID_V2_GET_SOURCE_DEATAIL_SUCCESS                        = "POSTPAID_V2_GET_SOURCE_DEATAIL_200";
    public final static String POSTPAID_V2_GET_SOURCE_DEATAIL_FAILURE                        = "POSTPAID_V2_GET_SOURCE_DEATAIL_300";
    
    public final static String USER_LOAD_V2_VOUCHER_SUCCESS                                  = "USER_LOAD_VOUCHER_200";
    public final static String USER_LOAD_V2_VOUCHER_FAILURE                                  = "USER_LOAD_VOUCHER_300";
    
    public final static String USER_V2_SOCIAL_AUTH_SUCCESS                                  = "SOCIAL_USER_AUTH_200";
    public final static String USER_V2_SOCIAL_AUTH_FAILURE                                  = "SOCIAL_USER_AUTH_300";
    public final static String USER_V2_SOCIAL_AUTH_LOGIN_ERROR                              = "SOCIAL_USER_AUTH_400";
    public final static String USER_V2_SOCIAL_AUTH_MISSING_EMAIL                            = "SOCIAL_USER_AUTH_401";
    public final static String USER_V2_SOCIAL_AUTH_WRONG_SOCIAL                             = "SOCIAL_USER_AUTH_402";
    public final static String USER_V2_SOCIAL_AUTH_NEW_USER                                 = "SOCIAL_USER_AUTH_201";
    
    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_SUCCESS                       = "EXTERNAL_PROVIDER_USER_AUTH_200";
    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_FAILURE                       = "EXTERNAL_PROVIDER_USER_AUTH_300";
    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_LOGIN_ERROR                   = "EXTERNAL_PROVIDER_USER_AUTH_400";
    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_MISSING_EMAIL                 = "EXTERNAL_PROVIDER_USER_AUTH_401";
    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_WRONG_SOCIAL                  = "EXTERNAL_PROVIDER_USER_AUTH_402";
    public final static String USER_V2_EXTERNAL_PROVIDER_AUTH_NEW_USER                      = "EXTERNAL_PROVIDER_USER_AUTH_201";
    
    public final static String USER_V2_GETPARTNER_ACTION_URL_SUCCESS                        = "GETPARTNER_ACTION_URL_200";
    public final static String USER_V2_GETPARTNER_ACTION_URL_FAILURE                        = "GETPARTNER_ACTION_URL_300";
    public final static String USER_V2_GETPARTNER_ACTION_URL_INVALID_REQUEST                = "USER_REQU_400";
    public final static String USER_V2_GETPARTNER_ACTION_URL_UNAUTHORIZED                   = "USER_REQU_403";

    public final static String USER_V2_EXTERNAL_AUTH_SUCCESS                                = "EXTERNAL_USER_AUTH_200";
    public final static String USER_V2_EXTERNAL_AUTH_FAILURE                                = "EXTERNAL_USER_AUTH_300";
    public final static String USER_V2_EXTERNAL_AUTH_INVALID_TICKET                         = "USER_REQU_401";
    public final static String USER_V2_EXTERNAL_AUTH_LOGIN_ERROR                            = "EXTERNAL_USER_AUTH_400";
    public final static String USER_V2_EXTERNAL_AUTH_MISSING_EMAIL                          = "EXTERNAL_USER_AUTH_401";
    public final static String USER_V2_EXTERNAL_AUTH_WRONG_EXTERNAL                         = "EXTERNAL_USER_AUTH_402";
    public final static String USER_V2_EXTERNAL_AUTH_NEW_USER                               = "EXTERNAL_USER_AUTH_201";
    
}
