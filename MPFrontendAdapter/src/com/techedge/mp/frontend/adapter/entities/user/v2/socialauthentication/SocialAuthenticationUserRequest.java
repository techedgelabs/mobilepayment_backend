package com.techedge.mp.frontend.adapter.entities.user.v2.socialauthentication;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PlateNumber;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SocialAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.UserSocialData;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransaction;
import com.techedge.mp.core.business.interfaces.parking.RetrievePendingParkingTransactionResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontend.adapter.entities.common.AddressData;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.ContactData;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.EmailSecurityData;
import com.techedge.mp.frontend.adapter.entities.common.LastLoginData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.SocialData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.UserStatus;
import com.techedge.mp.frontend.adapter.entities.common.v2.PlateNumberInfo;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class SocialAuthenticationUserRequest extends AbstractRequest {

	private Status status = new Status();

	private SocialAuthenticationUserBodyRequest body;

	public SocialAuthenticationUserBodyRequest getBody() {
		return body;
	}

	public void setBody(SocialAuthenticationUserBodyRequest body) {
		this.body = body;
	}

	@Override
	public Status check() {

		Status status = new Status();

		if (this.body != null) {

			status = this.body.check();

			if (!Validator.isValid(status.getStatusCode())) {

				return status;

			}
		} else {

			status.setStatusCode(StatusCode.USER_V2_REQU_INVALID_REQUEST);

			return status;
		}

		status.setStatusCode(StatusCode.USER_V2_AUTH_SUCCESS);

		return status;

	}

	@Override
	public BaseResponse execute() {

		SocialAuthenticationUserRequest socialAuthenticationUserRequest = this;

		String accessToken = socialAuthenticationUserRequest.getBody().getAccessToken();
		String socialProviderRequestString = socialAuthenticationUserRequest.getBody().getSocial().toUpperCase();

		SocialAuthenticationResponse socialAuthenticationResponse = getUserV2ServiceRemote().socialAuthentication(accessToken, socialProviderRequestString, socialAuthenticationUserRequest.getBody().getRequestID(), socialAuthenticationUserRequest.getBody().getDeviceID(), socialAuthenticationUserRequest.getBody().getDeviceName(), socialAuthenticationUserRequest.getBody().getDeviceToken());

		if (socialAuthenticationResponse == null || socialAuthenticationResponse.getStatusCode() == null) {
		    
		    SocialAuthenticationUserResponseFailure socialAuthenticationUserResponseFailure = new SocialAuthenticationUserResponseFailure();
		    
            status.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_FAILURE);
            status.setStatusMessage(prop.getProperty(ResponseHelper.USER_V2_SOCIAL_AUTH_FAILURE));

            socialAuthenticationUserResponseFailure.setStatus(status);

            return socialAuthenticationUserResponseFailure;
		}
		
		if (socialAuthenticationResponse.getStatusCode().equals(StatusCode.USER_V2_SOCIAL_AUTH_SUCCESS)) {

		    System.out.println("SUCCESS");
		    
			SocialAuthenticationUserResponseSuccess socialAuthenticationUserResponseSuccess = new SocialAuthenticationUserResponseSuccess();

			status.setStatusCode(socialAuthenticationResponse.getStatusCode());
			status.setStatusMessage(prop.getProperty(socialAuthenticationResponse.getStatusCode()));
			socialAuthenticationUserResponseSuccess.setStatus(status);

			SocialAuthenticationUserResponseBody body = new SocialAuthenticationUserResponseBody();

			SocialAuthenticationUserDataResponse userData = new SocialAuthenticationUserDataResponse();
			userData.setFirstName(socialAuthenticationResponse.getUser().getPersonalData().getFirstName());
			userData.setLastName(socialAuthenticationResponse.getUser().getPersonalData().getLastName());
			userData.setFiscalCode(socialAuthenticationResponse.getUser().getPersonalData().getFiscalCode());

			Calendar calendar = new GregorianCalendar();
			calendar.setTime(socialAuthenticationResponse.getUser().getPersonalData().getBirthDate());

			CustomDate dateOfBirth = new CustomDate();
			dateOfBirth.setYear(calendar.get(Calendar.YEAR));
			dateOfBirth.setMonth(calendar.get(Calendar.MONTH) + 1);
			dateOfBirth.setDay(calendar.get(Calendar.DAY_OF_MONTH));
			userData.setDateOfBirth(dateOfBirth);

			userData.setBirthMunicipality(socialAuthenticationResponse.getUser().getPersonalData().getBirthMunicipality());
			userData.setBirthProvince(socialAuthenticationResponse.getUser().getPersonalData().getBirthProvince());
			userData.setLanguage(socialAuthenticationResponse.getUser().getPersonalData().getLanguage());
			userData.setSex(socialAuthenticationResponse.getUser().getPersonalData().getSex());
			userData.setContactKey(socialAuthenticationResponse.getUser().getContactKey());

			EmailSecurityData securityData = new EmailSecurityData();
			securityData.setEmail(socialAuthenticationResponse.getUser().getPersonalData().getSecurityDataEmail());
			securityData.setStatus(1);
			userData.setSecurityData(securityData);

			Date now = new Date();

			ContactData contactData = new ContactData();
			for (MobilePhone mobilePhone : socialAuthenticationResponse.getUser().getMobilePhoneList()) {

				mobilePhone.setCreationTimestamp(null);
				mobilePhone.setLastUsedTimestamp(null);
				mobilePhone.setVerificationCode(null);

				if (mobilePhone.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_PENDING) {

					// Restituisci i metodi in stato pending che non sono
					// scaduti
					if (mobilePhone.getExpirationTimestamp().getTime() > now.getTime()) {
						// System.out.println("Restituito");
						mobilePhone.setExpirationTimestamp(null);
						contactData.getMobilePhones().add(mobilePhone);

					}
				}

				if (mobilePhone.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_ACTIVE) {

					// Restituisci i numeri attivi (dovrebbe essercene solo uno)
					mobilePhone.setExpirationTimestamp(null);
					contactData.getMobilePhones().add(mobilePhone);

				}

			}

			userData.setContactData(contactData);

			AddressData addressData = null;
			if (socialAuthenticationResponse.getUser().getPersonalData().getAddress() != null) {
				addressData = new AddressData();
				addressData.setCity(socialAuthenticationResponse.getUser().getPersonalData().getAddress().getCity());
				addressData.setCountryCodeId(socialAuthenticationResponse.getUser().getPersonalData().getAddress().getCountryCodeId());
				addressData.setCountryCodeName(socialAuthenticationResponse.getUser().getPersonalData().getAddress().getCountryCodeName());
				addressData.setHouseNumber(socialAuthenticationResponse.getUser().getPersonalData().getAddress().getHouseNumber());
				addressData.setRegion(socialAuthenticationResponse.getUser().getPersonalData().getAddress().getRegion());
				addressData.setStreet(socialAuthenticationResponse.getUser().getPersonalData().getAddress().getStreet());
				addressData.setZipCode(socialAuthenticationResponse.getUser().getPersonalData().getAddress().getZipcode());
			}
			userData.setAddressData(addressData);

			AddressData billingAddressData = null;
			if (socialAuthenticationResponse.getUser().getPersonalData().getBillingAddress() != null) {
				billingAddressData = new AddressData();
				billingAddressData.setCity(socialAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getCity());
				billingAddressData.setCountryCodeId(socialAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getCountryCodeId());
				billingAddressData.setCountryCodeName(socialAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getCountryCodeName());
				billingAddressData.setHouseNumber(socialAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getHouseNumber());
				billingAddressData.setRegion(socialAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getRegion());
				billingAddressData.setStreet(socialAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getStreet());
				billingAddressData.setZipCode(socialAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getZipcode());
			}
			userData.setBillingAddressData(billingAddressData);

			LastLoginData lastLoginData = new LastLoginData();

			String lastLoginDevice = "";
			if (socialAuthenticationResponse.getUser().getLastLoginData() != null) {
				lastLoginDevice = socialAuthenticationResponse.getUser().getLastLoginData().getDeviceName();
			}

			Timestamp lastLoginTime = null;
			if (socialAuthenticationResponse.getUser().getLastLoginData() != null) {
				lastLoginTime = socialAuthenticationResponse.getUser().getLastLoginData().getTime();
			}

			lastLoginData.setLastLoginDevice(lastLoginDevice);
			lastLoginData.setLastLoginTime(CustomTimestamp.createCustomTimestamp(lastLoginTime));
			userData.setLastLoginData(lastLoginData);

			PaymentData paymentData = new PaymentData();

			if (socialAuthenticationResponse.getUser().getPaymentData() != null) {

				// Inserimento del metodo di default in prima posizione
				for (PaymentInfo paymentInfo : socialAuthenticationResponse.getUser().getPaymentData()) {

					PaymentMethod paymentMethod = null;
					if (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

						if (paymentInfo.getDefaultMethod().equals(Boolean.TRUE)) {

							paymentMethod = new PaymentMethod();

							paymentMethod.setId(paymentInfo.getId());
							paymentMethod.setType(paymentInfo.getType());
							paymentMethod.setBrand(paymentInfo.getBrand());
							paymentMethod.setExpirationDate(CustomDate.createCustomDate(paymentInfo.getExpirationDate()));
							paymentMethod.setIdentifier(paymentInfo.getPan());
							paymentMethod.setStatus(paymentInfo.getStatus());
							paymentMethod.setDefaultMethod(paymentInfo.getDefaultMethod());

							if (paymentInfo.getInsertTimestamp() != null) {
								paymentMethod.setInsertDate(CustomTimestamp.createCustomTimestamp(new Timestamp(paymentInfo.getInsertTimestamp().getTime())));
							} else {
								paymentMethod.setInsertDate(null);
							}

							paymentData.getPaymentMethodList().add(paymentMethod);

							break;
						}
					}
				}

				// Inserimento degli altri metodi di pagamento
				for (PaymentInfo paymentInfo : socialAuthenticationResponse.getUser().getPaymentData()) {

					PaymentMethod paymentMethod = null;
					if (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

						if (paymentInfo.getDefaultMethod().equals(Boolean.FALSE)) {

							paymentMethod = new PaymentMethod();

							paymentMethod.setId(paymentInfo.getId());
							paymentMethod.setType(paymentInfo.getType());
							paymentMethod.setBrand(paymentInfo.getBrand());
							paymentMethod.setExpirationDate(CustomDate.createCustomDate(paymentInfo.getExpirationDate()));
							paymentMethod.setIdentifier(paymentInfo.getPan());
							paymentMethod.setStatus(paymentInfo.getStatus());
							paymentMethod.setDefaultMethod(paymentInfo.getDefaultMethod());

							if (paymentInfo.getInsertTimestamp() != null) {
								paymentMethod.setInsertDate(CustomTimestamp.createCustomTimestamp(new Timestamp(paymentInfo.getInsertTimestamp().getTime())));
							} else {
								paymentMethod.setInsertDate(null);
							}

							paymentData.getPaymentMethodList().add(paymentMethod);
						}
					}
				}
			}

			paymentData.setResidualCap(AmountConverter.toMobile(socialAuthenticationResponse.getUser().getCapEffective()));

			if (socialAuthenticationResponse.getUser().getUseVoucher() == null) {
				paymentData.setUseVoucher(false);
			} else {
				paymentData.setUseVoucher(socialAuthenticationResponse.getUser().getUseVoucher());
			}

			double residualVoucher = 0.0;
			Set<Voucher> voucherList = socialAuthenticationResponse.getUser().getVoucherList();

			for (Voucher voucher : voucherList) {
				residualVoucher += voucher.getVoucherBalanceDue().doubleValue();
			}

			paymentData.setResidualVoucher(AmountConverter.toMobile(residualVoucher));

			userData.setPaymentData(paymentData);

			UserStatus userStatus = new UserStatus();
			userStatus.setStatus(socialAuthenticationResponse.getUser().getUserStatus());
			userStatus.setRegistrationCompleted(socialAuthenticationResponse.getUser().getUserStatusRegistrationCompleted());

			if (socialAuthenticationResponse.getUser().getOldUser() != null) {

				System.out.println("oldUser: " + socialAuthenticationResponse.getUser().getOldUser());
				if (socialAuthenticationResponse.getUser().getOldUser() == true) {
					userStatus.setNewUserType(false);
					System.out.println("setNewUserType a false");
				} else {
					userStatus.setNewUserType(true);
					System.out.println("setNewUserType a true");
				}
			} else {
				System.out.println("oldUser: null");
			}

			Boolean termsOfServiceAccepted = Boolean.TRUE;
			for (TermsOfService termsOfService : socialAuthenticationResponse.getUser().getPersonalData().getTermsOfServiceData()) {

				if (termsOfService.getValid() == null || !termsOfService.getValid()) {
					termsOfServiceAccepted = Boolean.FALSE;
					System.out.println("found termsOfService " + termsOfService.getKeyval() + " no valid");
					break;
				}
			}
			userStatus.setTermsOfServiceAccepted(termsOfServiceAccepted);

			if (socialAuthenticationResponse.getUser().getDepositCardStepCompleted() != null) {
				userStatus.setDepositCardStepCompleted(socialAuthenticationResponse.getUser().getDepositCardStepCompleted());
			}

			if (socialAuthenticationResponse.getUser().getVirtualizationCompleted() != null) {
				userStatus.setVirtualizationCompleted(socialAuthenticationResponse.getUser().getVirtualizationCompleted());
			}

			Boolean eniStationUserType = socialAuthenticationResponse.getUser().getEniStationUserType();
			if (eniStationUserType == null) {
				userStatus.setEniStationUserType(false);
			} else {
				userStatus.setEniStationUserType(eniStationUserType);
			}

			Boolean loyaltyCheckEnabled = socialAuthenticationResponse.getUser().getLoyaltyCheckEnabled();
			if (loyaltyCheckEnabled == null) {
				userStatus.setLoyaltyCheckEnabled(false);
			} else {
				userStatus.setLoyaltyCheckEnabled(loyaltyCheckEnabled);
			}
			
			System.out.println("UserType: " + socialAuthenticationResponse.getUser().getUserType());
            String type = "CUSTOMER";
            if(socialAuthenticationResponse.getUser().getUserType() == User.USER_TYPE_GUEST) {
                type = "GUEST";
            }
            userStatus.setType(type);
            
            Boolean updatedMission = socialAuthenticationResponse.getUser().getUpdatedMission();
            if ( updatedMission == null ) {
                userStatus.setUpdatedMission(false);
            }
            else {
                userStatus.setUpdatedMission(updatedMission);
            }
			
			// Ricerca transazione di sosta pending
            boolean parkingTransactionPending = false;
            RetrievePendingParkingTransactionResult retrievePendingParkingTransactionResult = getParkingTransactionV2ServiceRemote().retrievePendingParkingTransaction(
                    socialAuthenticationResponse.getTicketId(), socialAuthenticationUserRequest.getBody().getRequestID());
            if (retrievePendingParkingTransactionResult.getStatusCode().equals("RETRIEVE_PENDING_PARKING_TRANSACTION_200")) {

                ParkingTransaction parkingTransaction = retrievePendingParkingTransactionResult.getParkingTransaction();
                if (parkingTransaction != null) {
                    parkingTransactionPending = true;
                }
            }
            userStatus.setParkingTransactionPending(parkingTransactionPending);

			userData.setUserStatus(userStatus);
			
			String socialProvider = null;
			if (!socialAuthenticationResponse.getUser().getUserSocialData().isEmpty()) {
    			for(UserSocialData userSocialData : socialAuthenticationResponse.getUser().getUserSocialData()) {
    			    socialProvider = userSocialData.getProvider();
    			}
			}
			
			if (socialProvider != null) {
    			SocialData socialData = new SocialData();
                socialData.setProvider(socialProvider);
                userData.getSocialDataList().add(socialData);
			}
			
			if (!socialAuthenticationResponse.getUser().getPlateNumberList().isEmpty()) {
                for(PlateNumber plateNumber : socialAuthenticationResponse.getUser().getPlateNumberList()) {
                    PlateNumberInfo plateNumberInfo = new PlateNumberInfo();
                    plateNumberInfo.setId(plateNumber.getId());
                    plateNumberInfo.setPlateNumber(plateNumber.getPlateNumber());
                    plateNumberInfo.setDescription(plateNumber.getDescription());
                    plateNumberInfo.setDefaultPlateNumber(plateNumber.getDefaultPlateNumber());
                    userData.getPlateNumberList().add(plateNumberInfo);
                }
            }

			body.setTicketID(socialAuthenticationResponse.getTicketId());
			body.setLoyaltySessionID(socialAuthenticationResponse.getLoyaltySessionId());
			
			String externalUserId = socialAuthenticationResponse.getUser().getExternalUserId();
            userData.setExternalUserId(externalUserId);
			
			body.setUserData(userData);

			socialAuthenticationUserResponseSuccess.setBody(body);

			return socialAuthenticationUserResponseSuccess;
		}
		
	    if (socialAuthenticationResponse.getStatusCode().equals(StatusCode.USER_V2_SOCIAL_AUTH_NEW_USER)) {

			SocialAuthenticationUserResponseSuccess socialAuthenticationUserResponseSuccess = new SocialAuthenticationUserResponseSuccess();

			status.setStatusCode(socialAuthenticationResponse.getStatusCode());
			status.setStatusMessage(prop.getProperty(socialAuthenticationResponse.getStatusCode()));
			socialAuthenticationUserResponseSuccess.setStatus(status);

			SocialAuthenticationUserResponseBody body = new SocialAuthenticationUserResponseBody();

			SocialAuthenticationUserDataResponse userData = new SocialAuthenticationUserDataResponse();
			userData.setFirstName(socialAuthenticationResponse.getUser().getPersonalData().getFirstName());
			userData.setLastName(socialAuthenticationResponse.getUser().getPersonalData().getLastName());
			// userData.setFiscalCode(authenticationResponse.getUser().getPersonalData().getFiscalCode());

			if (socialAuthenticationResponse.getUser().getPersonalData().getBirthDate() != null) {
				Calendar calendar = new GregorianCalendar();
				calendar.setTime(socialAuthenticationResponse.getUser().getPersonalData().getBirthDate());
				CustomDate dateOfBirth = new CustomDate();
				dateOfBirth.setYear(calendar.get(Calendar.YEAR));
				dateOfBirth.setMonth(calendar.get(Calendar.MONTH) + 1);
				dateOfBirth.setDay(calendar.get(Calendar.DAY_OF_MONTH));
				userData.setDateOfBirth(dateOfBirth);
			}
			
			SocialData socialData = new SocialData();
			socialData.setProvider(socialProviderRequestString);
			userData.getSocialDataList().add(socialData);
			
			
			UserStatus userStatus = new UserStatus();
			userStatus.setStatus(socialAuthenticationResponse.getUser().getUserStatus());
			userStatus.setRegistrationCompleted(socialAuthenticationResponse.getUser().getUserStatusRegistrationCompleted());

			Boolean updatedMission = socialAuthenticationResponse.getUser().getUpdatedMission();
            if ( updatedMission == null ) {
                userStatus.setUpdatedMission(false);
            }
            else {
                userStatus.setUpdatedMission(updatedMission);
            }
            
			userData.setUserStatus(userStatus);
			userData.setSex(socialAuthenticationResponse.getUser().getPersonalData().getSex());

			EmailSecurityData securityData = new EmailSecurityData();
			securityData.setEmail(socialAuthenticationResponse.getUser().getPersonalData().getSecurityDataEmail());
			securityData.setStatus(1);
			userData.setSecurityData(securityData);
			body.setUserData(userData);
			if(socialAuthenticationResponse.getUser().getPersonalData().getSecurityDataEmail() != null && !socialAuthenticationResponse.getUser().getPersonalData().getSecurityDataEmail().isEmpty())
			{
			    body.getNotEditableFields().add("email");
			}
			
			socialAuthenticationUserResponseSuccess.setBody(body);
			
			status.setStatusCode(StatusCode.USER_V2_SOCIAL_AUTH_SUCCESS);
			status.setStatusMessage(prop.getProperty(StatusCode.USER_V2_SOCIAL_AUTH_SUCCESS));
			
			socialAuthenticationUserResponseSuccess.setStatus(status);
			
			return socialAuthenticationUserResponseSuccess;
		}


		SocialAuthenticationUserResponseFailure socialAuthenticationUserResponseFailure = new SocialAuthenticationUserResponseFailure();

		status.setStatusCode(socialAuthenticationResponse.getStatusCode());
		status.setStatusMessage(prop.getProperty(socialAuthenticationResponse.getStatusCode()));

		socialAuthenticationUserResponseFailure.setStatus(status);

		return socialAuthenticationUserResponseFailure;
	}

}
