package com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class RetrieveApplicationSettingsResponse extends BaseResponse {

	
	private RetrieveApplicationSettingsBodyResponse body;

	public RetrieveApplicationSettingsBodyResponse getBody() {
		return body;
	}

	public void setBody(RetrieveApplicationSettingsBodyResponse body) {
		this.body = body;
	}
	
}
