package com.techedge.mp.frontend.adapter.entities.common;

import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class BillingAddressData  implements Validable {

    private String            address;
    private String            streetNumber;
    private String            province;
    private String            city;
    private String            zipCode;
    private String            country;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber;
    }

    public String getCountry() {
        return country;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    
    @Override
    public Status check() {
        Status status = new Status();
        String regexCheckNumbers    = "\\d+";
        String regexCheckLetters    = "[a-zA-Z]+";
        String regexContainsNumbers = ".*\\d+.*";
        
        if (address == null || address.isEmpty() || address.length() > 60) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BILLING_ADDRESS_ADDRESS_INVALID);
            return status;
        }
        
        if (province == null || province.isEmpty() || province.length() != 2) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BILLING_ADDRESS_PROVINCE_INVALID);
            return status;
        }
        
        if (city == null || city.isEmpty() || city.length() > 60 || (city.matches(regexContainsNumbers))) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BILLING_ADDRESS_CITY_INVALID);
            return status;
        }

        if (zipCode == null || zipCode.isEmpty() || zipCode.length() != 5 || (!zipCode.matches(regexCheckNumbers))) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BILLING_ADDRESS_ZIPCODE_INVALID);
            return status;
        }
        
        if (streetNumber != null && !streetNumber.isEmpty() && streetNumber.length() > 8) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BILLING_ADDRESS_STREET_NUMBER_INVALID);
            return status;
        }
        
        int addressLength      = address.length();
        int streetNumberLength = 0;
        if (streetNumber != null && !streetNumber.isEmpty()) {
            streetNumberLength = streetNumber.length();
        }
        int addressTotalLength = addressLength + streetNumberLength;
        
        System.out.println("address length:       " + addressLength);
        System.out.println("street number length: " + streetNumberLength);
        System.out.println("address total length: " + addressTotalLength);
        
        if (addressTotalLength > 59) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BILLING_ADDRESS_LENGTH_INVALID);
            return status;
        }
        
        /*
        if (country == null || country.isEmpty()) {
            status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_BILLING_ADDRESS_COUNTRY_REQUIRED);
            return status;
        }
        */
        status.setStatusCode(StatusCode.USER_BUSINESS_CHECK_SUCCESS);
        return status;
    }
}
