package com.techedge.mp.frontend.adapter.entities.user.update;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UpdateUserPersonalDataRequestBody implements Validable {

    private UpdateUserUserData userPersonalData;

    public UpdateUserUserData getUserPersonalData() {
        return userPersonalData;
    }

    public void setUserPersonalData(UpdateUserUserData userPersonalData) {
        this.userPersonalData = userPersonalData;
    }

    @Override
    public Status check() {

        Status status = new Status();
        
        if(this.userPersonalData != null) {
            
            status = this.userPersonalData.check();
            
            if(!Validator.isValid(status.getStatusCode())) {
                
                return status;
                
            }
        } else {
            
            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
            
            return status;
            
        }
        
        status.setStatusCode(StatusCode.USER_UPD_SUCCESS);
        
        return status;
        
    }
    
}
