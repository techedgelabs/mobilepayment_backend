package com.techedge.mp.frontend.adapter.entities.common;

import java.io.Serializable;

public class DeviceData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3581769861919986017L;

    /**
     * 
     */

    private String            deviceName;

    private String            deviceID;

    private CustomTimestamp   lastLoginTime;

    private CustomTimestamp   creationTime;

    private CustomTimestamp   associationTime;

    public String getDeviceName() {

        return deviceName;
    }

    public void setDeviceName(String deviceName) {

        this.deviceName = deviceName;
    }

    public String getDeviceID() {

        return deviceID;
    }

    public void setDeviceID(String deviceID) {

        this.deviceID = deviceID;
    }

    public CustomTimestamp getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(CustomTimestamp lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }

    public CustomTimestamp getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(CustomTimestamp creationTime) {
        this.creationTime = creationTime;
    }

    public CustomTimestamp getAssociationTime() {
        return associationTime;
    }

    public void setAssociationTime(CustomTimestamp associationTime) {
        this.associationTime = associationTime;
    }

}
