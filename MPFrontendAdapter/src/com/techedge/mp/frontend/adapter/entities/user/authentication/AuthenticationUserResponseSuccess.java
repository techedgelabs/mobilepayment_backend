package com.techedge.mp.frontend.adapter.entities.user.authentication;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class AuthenticationUserResponseSuccess extends BaseResponse {

    private AuthenticationUserResponseBody body;

    public AuthenticationUserResponseBody getBody() {
        return body;
    }

    public void setBody(AuthenticationUserResponseBody body) {
        this.body = body;
    }
}
