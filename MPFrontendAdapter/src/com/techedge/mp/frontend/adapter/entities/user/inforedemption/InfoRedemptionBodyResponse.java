package com.techedge.mp.frontend.adapter.entities.user.inforedemption;

import java.util.ArrayList;
import java.util.List;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.RedemptionData;

public class InfoRedemptionBodyResponse extends BaseResponse {
    private List<RedemptionData> redemptions   = new ArrayList<RedemptionData>();

    public List<RedemptionData> getRedemptions() {
        return redemptions;
    }

    public void setRedemptions(List<RedemptionData> redemptions) {
        this.redemptions = redemptions;
    }
}
