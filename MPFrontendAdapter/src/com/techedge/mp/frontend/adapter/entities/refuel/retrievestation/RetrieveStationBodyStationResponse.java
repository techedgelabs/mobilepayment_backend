package com.techedge.mp.frontend.adapter.entities.refuel.retrievestation;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Station;

public class RetrieveStationBodyStationResponse extends Station {

	private List<Pump> pumpList = new ArrayList<Pump>(0);

	public List<Pump> getPumpList() {
		return pumpList;
	}

	public void setPumpList(List<Pump> pumpList) {
		this.pumpList = pumpList;
	}
}
