package com.techedge.mp.frontend.adapter.entities.manager.retrievestation;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class ManagerRetrieveStationDetailsResponse extends BaseResponse {

	
	private ManagerRetrieveStationDetailsBodyResponse body;

	
	public synchronized ManagerRetrieveStationDetailsBodyResponse getBody() {
		return body;
	}

	public synchronized void setBody(ManagerRetrieveStationDetailsBodyResponse body) {
		this.body = body;
	}
	
	
	
}
