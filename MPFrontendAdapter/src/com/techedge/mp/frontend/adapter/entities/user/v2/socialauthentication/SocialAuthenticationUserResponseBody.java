package com.techedge.mp.frontend.adapter.entities.user.v2.socialauthentication;

import java.util.ArrayList;
import java.util.List;

public class SocialAuthenticationUserResponseBody {

    private String                               ticketID;
    private String                               loyaltySessionID;
    private SocialAuthenticationUserDataResponse userData;
    private List<String>                         notEditableFields = new ArrayList<String>(0);

    public String getTicketID() {
        return ticketID;
    }

    public void setTicketID(String ticketID) {
        this.ticketID = ticketID;
    }

    public String getLoyaltySessionID() {
        return loyaltySessionID;
    }

    public void setLoyaltySessionID(String loyaltySessionID) {
        this.loyaltySessionID = loyaltySessionID;
    }

    public SocialAuthenticationUserDataResponse getUserData() {
        return userData;
    }

    public void setUserData(SocialAuthenticationUserDataResponse userData) {
        this.userData = userData;
    }

    public List<String> getNotEditableFields() {
        return notEditableFields;
    }

    public void setNotEditableFields(List<String> notEditableFields) {
        this.notEditableFields = notEditableFields;
    }

}
