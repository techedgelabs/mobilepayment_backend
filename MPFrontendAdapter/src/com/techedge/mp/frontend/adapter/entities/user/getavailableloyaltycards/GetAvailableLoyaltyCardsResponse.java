package com.techedge.mp.frontend.adapter.entities.user.getavailableloyaltycards;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetAvailableLoyaltyCardsResponse extends BaseResponse {

	
	private GetAvailableLoyaltyCardsBodyResponse body;

	
	public GetAvailableLoyaltyCardsBodyResponse getBody() {
		return body;
	}

	
	public void setBody(GetAvailableLoyaltyCardsBodyResponse body) {
		this.body = body;
	}
	
	
	
}
