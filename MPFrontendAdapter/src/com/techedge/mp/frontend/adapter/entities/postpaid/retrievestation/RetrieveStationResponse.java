package com.techedge.mp.frontend.adapter.entities.postpaid.retrievestation;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveStationResponse extends BaseResponse {

	
	private RetrieveStationBodyResponse body;

	
	public synchronized RetrieveStationBodyResponse getBody() {
		return body;
	}

	public synchronized void setBody(RetrieveStationBodyResponse body) {
		this.body = body;
	}
	
	
	
}
