package com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings;

public class RetrieveApplicationSettingsSecurityBodyResponse {

    private String publicKey;
    private String captchaPublicKeyAndroid;
    private String captchaPublicKeyIOS;

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getCaptchaPublicKeyAndroid() {
        return captchaPublicKeyAndroid;
    }

    public void setCaptchaPublicKeyAndroid(String captchaPublicKeyAndroid) {
        this.captchaPublicKeyAndroid = captchaPublicKeyAndroid;
    }

    public String getCaptchaPublicKeyIOS() {
        return captchaPublicKeyIOS;
    }

    public void setCaptchaPublicKeyIOS(String captchaPublicKeyIOS) {
        this.captchaPublicKeyIOS = captchaPublicKeyIOS;
    }

}
