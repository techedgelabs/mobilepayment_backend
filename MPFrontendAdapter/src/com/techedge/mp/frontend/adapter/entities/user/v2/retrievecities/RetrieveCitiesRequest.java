package com.techedge.mp.frontend.adapter.entities.user.v2.retrievecities;

import com.techedge.mp.core.business.interfaces.CityInfo;
import com.techedge.mp.core.business.interfaces.RetrieveCitiesData;
import com.techedge.mp.core.business.utilities.StringUtils;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.v2.City;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveCitiesRequest extends AbstractRequest implements Validable {

    private Credential                credential;
    private RetrieveCitiesRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveCitiesRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveCitiesRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CITIES", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        RetrieveCitiesRequest retrieveCitiesRequest = this;

        RetrieveCitiesData retrieveCitiesData = getUserServiceRemote().retrieveCities(retrieveCitiesRequest.getCredential().getTicketID(),
                retrieveCitiesRequest.getCredential().getRequestID(), retrieveCitiesRequest.getBody().getSearchKey());

        RetrieveCitiesResponse retrieveCitiesResponse = new RetrieveCitiesResponse();

        Status status = new Status();
        status.setStatusCode(retrieveCitiesData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveCitiesData.getStatusCode()));

        RetrieveCitiesResponseBody retrieveCitiesResponseBody = new RetrieveCitiesResponseBody();

        if (!retrieveCitiesData.getCities().isEmpty()) {

            for (CityInfo cityInfo : retrieveCitiesData.getCities()) {

                City city = new City();
                city.setCityName(StringUtils.capitalizeString(cityInfo.getName()));
                city.setCityProvince(cityInfo.getProvince());
                city.setCityFiscalCode(cityInfo.getCode());
                retrieveCitiesResponseBody.getCitiesList().add(city);
            }
        }

        retrieveCitiesResponse.setBody(retrieveCitiesResponseBody);

        retrieveCitiesResponse.setStatus(status);

        return retrieveCitiesResponse;
    }

}
