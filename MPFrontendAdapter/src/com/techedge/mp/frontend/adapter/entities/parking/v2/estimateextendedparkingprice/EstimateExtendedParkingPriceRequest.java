package com.techedge.mp.frontend.adapter.entities.parking.v2.estimateextendedparkingprice;

import com.techedge.mp.core.business.interfaces.parking.EstimateExtendedParkingPriceResult;
import com.techedge.mp.core.business.interfaces.parking.EstimateParkingPriceResult;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZonePriceInfo;
import com.techedge.mp.frontend.adapter.entities.common.v2.PlateNumberInfo;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class EstimateExtendedParkingPriceRequest extends AbstractRequest implements Validable {

    private Status                          status = new Status();

    private Credential                      credential;
    private EstimateExtendedParkingPriceRequestBody body;

    public EstimateExtendedParkingPriceRequestBody getBody() {
        return body;
    }

    public void setBody(EstimateExtendedParkingPriceRequestBody body) {
        this.body = body;
    }

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_AUTH_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        EstimateExtendedParkingPriceRequest estimateExtendedParkingPriceRequest = this;
        
        EstimateExtendedParkingPriceRequestBody estimateExtendedParkingPriceRequestBody = estimateExtendedParkingPriceRequest.getBody();
      //Secondi impostati a zero per evitare l'arrotondamento effettuato dai servizi MyCicero
        estimateExtendedParkingPriceRequestBody.getRequestedEndTime().setSecond(0);
        EstimateExtendedParkingPriceResult result = getParkingTransactionV2ServiceRemote().estimateExtendedParkingPrice(estimateExtendedParkingPriceRequest.getCredential().getTicketID(),
                estimateExtendedParkingPriceRequest.getCredential().getRequestID(), estimateExtendedParkingPriceRequestBody.getTransactionID(),
                CustomTimestamp.convertToDate(estimateExtendedParkingPriceRequestBody.getRequestedEndTime()));

        EstimateExtendedParkingPriceResponse response = new EstimateExtendedParkingPriceResponse();
        
        EstimateExtendedParkingPriceResponseBody responseBody = new EstimateExtendedParkingPriceResponseBody();
        
        ParkingZonePriceInfo parkingZonePriceInfo = new ParkingZonePriceInfo();
        
        PlateNumberInfo plateNumberInfo = new PlateNumberInfo();
        plateNumberInfo.setPlateNumber(result.getPlateNumber());
        plateNumberInfo.setDescription(result.getPlateNumberDescription());
        parkingZonePriceInfo.setPlateNumber(plateNumberInfo);
        
        parkingZonePriceInfo.setPrice(AmountConverter.toMobile(result.getPrice()));
        parkingZonePriceInfo.setStallCode(result.getStallCode());
        parkingZonePriceInfo.setNotice(result.getNotice());
        
        ParkingZone parkingZone = new ParkingZone();
        parkingZone.setId(result.getParkingZoneId());
        parkingZone.setName(result.getParkingZoneName());
        parkingZone.setDescription(result.getParkingZoneDescription());
        parkingZone.setCityId(result.getCityId());
        parkingZone.setCityName(result.getCityName());
        parkingZone.setAdministrativeAreaLevel2Code(result.getAdministrativeAreaLevel2Code());
        parkingZonePriceInfo.setParkingZone(parkingZone);

        parkingZonePriceInfo.setParkingStartTime(CustomTimestamp.convertToCustomTimestamp(result.getParkingStartTime()));
        parkingZonePriceInfo.setParkingEndTime(CustomTimestamp.convertToCustomTimestamp(result.getParkingEndTime()));
        parkingZonePriceInfo.setTransactionID(result.getTransactionId());

        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(result.getPaymentMethodId());
        parkingZonePriceInfo.setPaymentMethod(paymentMethod);
        
        responseBody.setParkingZonePriceInfo(parkingZonePriceInfo);
        
        response.setBody(responseBody);
        Status status = new Status();
        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(prop.getProperty(result.getStatusCode()));
        
        response.setStatus(status);

        return response;

    }

}
