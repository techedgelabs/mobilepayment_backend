package com.techedge.mp.frontend.adapter.entities.user.business.recoverusername;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.business.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RecoverUsernameRequest extends AbstractRequest implements Validable {

    private Status                     status = new Status();

    private Credential                 credential;
    private RecoverUsernameBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RecoverUsernameBodyRequest getBody() {
        return body;
    }

    public void setBody(RecoverUsernameBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RECOVER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.USER_BUSINESS_RECOVER_USERNAME_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        RecoverUsernameRequest recoverUsernameRequest = this;

        String response = getUserV2ServiceRemote().recoverUsernameBusiness(recoverUsernameRequest.getCredential().getTicketID(), recoverUsernameRequest.getCredential().getRequestID(),
                recoverUsernameRequest.getBody().getFiscalCode());

        RecoverUsernameResponse recoverUsernameResponse = new RecoverUsernameResponse();

        if (response.contains("USER_RECOVER_USERNAME")) {
            response = StatusCode.USER_BUSINESS_RECOVER_USERNAME_SUCCESS;
        }

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        recoverUsernameResponse.setStatus(status);

        return recoverUsernameResponse;
    }

}
