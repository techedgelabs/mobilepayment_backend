package com.techedge.mp.frontend.adapter.entities.user.retrievepaymentdata;

import java.sql.Timestamp;

import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PaymentInfoResponse;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.PaymentData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrievePaymentDataRequest extends AbstractRequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;
    private RetrievePaymentDataBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrievePaymentDataBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrievePaymentDataBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PAY-RETRIEVE-DATA", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;

    }

    @Override
    public BaseResponse execute() {
        RetrievePaymentDataRequest retrievePaymentDataRequest = this;

        PaymentInfoResponse paymentInfoResponse = getUserServiceRemote().retrievePaymentData(retrievePaymentDataRequest.getCredential().getTicketID(),
                retrievePaymentDataRequest.getCredential().getRequestID(), retrievePaymentDataRequest.getBody().getPaymentMethod().getId(),
                retrievePaymentDataRequest.getBody().getPaymentMethod().getType());

        RetrievePaymentDataResponse retrievePaymentDataResponse = new RetrievePaymentDataResponse();

        status.setStatusCode(paymentInfoResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(paymentInfoResponse.getStatusCode()));
        retrievePaymentDataResponse.setStatus(status);

        if (paymentInfoResponse.getPaymentInfo() != null) {

            PaymentInfo paymentInfo = paymentInfoResponse.getPaymentInfo();

            PaymentData paymentData = new PaymentData();

            if (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED
                    || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_ERROR) {

                PaymentMethod paymentMethod = new PaymentMethod();

                paymentMethod.setId(paymentInfo.getId());
                paymentMethod.setType(paymentInfo.getType());
                paymentMethod.setBrand(paymentInfo.getBrand());
                paymentMethod.setExpirationDate(CustomDate.createCustomDate(paymentInfo.getExpirationDate()));
                paymentMethod.setIdentifier(paymentInfo.getPan());
                paymentMethod.setStatus(paymentInfo.getStatus());
                paymentMethod.setMessage(paymentInfo.getMessage());
                paymentMethod.setDefaultMethod(paymentInfo.getDefaultMethod());

                if (paymentInfo.getInsertTimestamp() != null) {
                    paymentMethod.setInsertDate(CustomTimestamp.createCustomTimestamp(new Timestamp(paymentInfo.getInsertTimestamp().getTime())));
                }
                else {
                    paymentMethod.setInsertDate(null);
                }

                paymentData.getPaymentMethodList().add(paymentMethod);
            }

            RetrievePaymentDataBodyResponse body = new RetrievePaymentDataBodyResponse();
            body.setPaymentData(paymentData);

            retrievePaymentDataResponse.setBody(body);
        }
        else {
            retrievePaymentDataResponse.setBody(null);
        }

        return retrievePaymentDataResponse;
    }

}
