package com.techedge.mp.frontend.adapter.entities.user.v2.getpromopopup;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetPromoPopupResponse extends BaseResponse {

    private GetPromoPopupResponseBody body;

    public GetPromoPopupResponseBody getBody() {
        return body;
    }

    public void setBody(GetPromoPopupResponseBody body) {
        this.body = body;
    }
}
