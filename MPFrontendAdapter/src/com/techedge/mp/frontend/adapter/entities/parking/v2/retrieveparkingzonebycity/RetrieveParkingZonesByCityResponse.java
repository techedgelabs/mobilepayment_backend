package com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingzonebycity;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveParkingZonesByCityResponse extends BaseResponse {
    private RetrieveParkingZonesByCityResponseBody body;

    public RetrieveParkingZonesByCityResponseBody getBody() {
        return body;
    }

    public void setBody(RetrieveParkingZonesByCityResponseBody body) {
        this.body = body;
    }

    
}
