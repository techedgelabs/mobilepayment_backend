package com.techedge.mp.frontend.adapter.entities.parking.v2.approveparkingtransaction;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class ApproveParkingTransactionResponse extends BaseResponse {
    private ApproveParkingtransactionResponseBody body;

    public ApproveParkingtransactionResponseBody getBody() {
        return body;
    }

    public void setBody(ApproveParkingtransactionResponseBody body) {
        this.body = body;
    }

}
