package com.techedge.mp.frontend.adapter.entities.user.redemption;

import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RedemptionCredential extends Credential implements Validable {
	

	private String pin;

	
	public String getPin() {
		return pin;
	}
	public void setPin(String pin) {
		this.pin = pin;
	}

	@Override
	public Status check() {
		
		Status status = new Status();
		
		Credential credential = new Credential();
		credential.setTicketID(this.getTicketID());
		credential.setRequestID(this.getRequestID());
		
		status = Validator.checkCredential("CREATE-VOUCHER-TRANSACTION", credential);
		
		if(!Validator.isValid(status.getStatusCode())) {
			
			return status;
			
		}
		
		if(this.pin == null) {
			
			status.setStatusCode(StatusCode.CREATE_VOUCHER_TRANSACTION_PIN_WRONG);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.CREATE_VOUCHER_TRANSACTION_SUCCESS);
		
		return status;
		
	}
	
	
}
