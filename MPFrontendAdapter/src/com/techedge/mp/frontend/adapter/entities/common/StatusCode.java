package com.techedge.mp.frontend.adapter.entities.common;

public class StatusCode {

    public final static String SYSTEM_ERROR                                                      = "SYSTEM_ERROR_500";

    public final static String USER_CREATE_SUCCESS                                               = "USER_CREATE_200";
    public final static String USER_CREATE_FAILURE                                               = "USER_CREATE_300";
    public final static String USER_CREATE_EMAIL_WRONG                                           = "USER_CREATE_401";
    public final static String USER_CREATE_PASSWORD_WRONG                                        = "USER_CREATE_402";
    public final static String USER_CREATE_MASTER_DATA_WRONG                                     = "USER_CREATE_403";
    public final static String USER_CREATE_ADDRESS_RESIDENCE_WRONG                               = "USER_CREATE_404";
    public final static String USER_CREATE_ADDRESS_BILLING_WRONG                                 = "USER_CREATE_405";
    public final static String USER_CREATE_FIDELITY_DATA_WRONG                                   = "USER_CREATE_406";
    public final static String USER_CREATE_PRIVACY_DATA_WRONG                                    = "USER_CREATE_407";
    public final static String USER_CREATE_USERNAME_PASSWORD_EQUALS                              = "USER_CREATE_408";
    public final static String USER_CREATE_PASSWORD_SHORT                                        = "USER_CREATE_409";
    public final static String USER_CREATE_NUMBER_LESS                                           = "USER_CREATE_410";
    public final static String USER_CREATE_LOWER_LESS                                            = "USER_CREATE_411";
    public final static String USER_CREATE_UPPER_LESS                                            = "USER_CREATE_412";
    public final static String USER_CREATE_DATE_BIRTH_WRONG                                      = "USER_CREATE_413";
    public final static String USER_CREATE_CONTACT_DATA_WRONG                                    = "USER_CREATE_414";
    public final static String USER_CREATE_SYSTEM_ERROR                                          = "USER_CREATE_500";
    public final static String USER_CREATE_MAIL_EXIST                                            = "USER_CREATE_501";

    public final static String USER_CHECK_SUCCESS                                                = "USER_CHECK_200";
    public final static String USER_CHECK_FAILURE                                                = "USER_CHECK_300";
    public final static String USER_CHECK_CODE_WRONG                                             = "USER_CHECK_400";
    public final static String USER_CHECK_MOBILE_PHONE_WRONG                                     = "USER_CHECK_401";
    public final static String USER_CHECK_MOBILE_PHONE_REQUIRED                                  = "USER_CHECK_402";

    public final static String USER_AUTH_SUCCESS                                                 = "USER_AUTH_200";
    public final static String USER_AUTH_FAILURE                                                 = "USER_AUTH_300";
    public final static String USER_AUTH_LOGIN_ERROR                                             = "USER_AUTH_301";
    public final static String USER_AUTH_EMAIL_WRONG                                             = "USER_AUTH_401";
    public final static String USER_AUTH_PASSWORD_WRONG                                          = "USER_AUTH_402";
    public final static String USER_AUTH_PASSWORD_SHORT                                          = "USER_AUTH_409";
    public final static String USER_AUTH_NUMBER_LESS                                             = "USER_AUTH_410";
    public final static String USER_AUTH_LOWER_LESS                                              = "USER_AUTH_411";
    public final static String USER_AUTH_UPPER_LESS                                              = "USER_AUTH_412";
    public final static String USER_AUTH_INVALID_REQUEST                                         = "USER_REQU_400";

    public final static String USER_LOGOUT_SUCCESS                                               = "USER_LOGOUT_200";
    public final static String USER_LOGOUT_FAILURE                                               = "USER_LOGOUT_300";
    public final static String USER_LOGOUT_ERROR                                                 = "USER_LOGOUT_301";

    public final static String USER_RESCUE_PASSWORD_SUCCESS                                      = "USER_RESCUE_200";
    public final static String USER_RESCUE_PASSWORD_FAILURE                                      = "USER_RESCUE_300";
    public final static String USER_RESCUE_PASSWORD_ERROR                                        = "USER_RESCUE_301";

    public final static String USER_UPD_SUCCESS                                                  = "USER_UPD_200";
    public final static String USER_UPD_FAILURE                                                  = "USER_UPD_300";
    public final static String USER_UPD_MASTER_DATA_WRONG                                        = "USER_UPD_401";
    public final static String USER_UPD_CONTACT_DATA_WRONG                                       = "USER_UPD_402";
    public final static String USER_UPD_ADDRESS_RESIDENCE_WRONG                                  = "USER_UPD_403";
    public final static String USER_UPD_ADDRESS_BILLING_WRONG                                    = "USER_UPD_404";
    public final static String USER_UPD_FIDELITY_DATA_WRONG                                      = "USER_UPD_405";
    public final static String USER_UPD_DATE_BIRTH_WRONG                                         = "USER_UPD_406";
    public final static String USER_UPD_PRIVACY_DATA_WRONG                                       = "USER_UPD_407";
    public final static String USER_UPD_EXPIRATION_DATA_LOYALTY_WRONG                            = "USER_UPD_413";
    public final static String USER_UPD_SYSTEM_ERROR                                             = "USER_UPD_500";

    public final static String USER_PWD_SUCCESS                                                  = "USER_PWD_200";
    public final static String USER_PWD_FAILURE                                                  = "USER_PWD_300";
    public final static String USER_PWD_NEW_PASSWORD_WRONG                                       = "USER_PWD_401";
    public final static String USER_PWD_OLD_PASSWORD_WRONG                                       = "USER_PWD_402";
    public final static String USER_PWD_USERNAME_PASSWORD_EQUALS                                 = "USER_PWD_403";
    public final static String USER_PWD_PASSWORD_SHORT                                           = "USER_PWD_409";
    public final static String USER_PWD_NUMBER_LESS                                              = "USER_PWD_410";
    public final static String USER_PWD_LOWER_LESS                                               = "USER_PWD_411";
    public final static String USER_PWD_UPPER_LESS                                               = "USER_PWD_412";
    public final static String USER_PWD_OLD_NEW_EQUALS                                           = "USER_PWD_413";
    public final static String USER_PWD_OLD_ERROR                                                = "USER_PWD_414";
    public final static String USER_PWD_HISTORY_NEW_EQUALS                                       = "USER_PWD_415";

    public final static String USER_PIN_SUCCESS                                                  = "USER_PIN_200";
    public final static String USER_PIN_FAILURE                                                  = "USER_PIN_300";
    public final static String USER_PIN_OLD_LESS                                                 = "USER_PIN_400";
    public final static String USER_PIN_NEW_WRONG                                                = "USER_PIN_401";
    public final static String USER_PIN_OLD_WRONG                                                = "USER_PIN_402";
    public final static String USER_PIN_OLD_DB_LESS                                              = "USER_PIN_403";
    public final static String USER_PIN_OLD_NEW_EQUALS                                           = "USER_PIN_404";
    public final static String USER_PIN_OLD_ERROR                                                = "USER_PIN_405";
    public final static String USER_PIN_METHOD_NOT_FOUND                                         = "USER_PIN_406";
    public final static String USER_PIN_STATUS_ERROR                                             = "USER_PIN_407";
    public final static String USER_PIN_METHOD_ID_WRONG                                          = "USER_PIN_408";
    public final static String USER_PIN_METHOD_TYPE_WRONG                                        = "USER_PIN_409";

    public final static String USER_RESET_PIN_SUCCESS                                            = "USER_RESET_PIN_200";
    public final static String USER_RESET_PIN_FAILURE                                            = "USER_RESET_PIN_300";
    public final static String USER_RESET_PIN_INVALID_PASSWORD                                   = "USER_RESET_PIN_401";

    public final static String USER_VALID_SUCCESS                                                = "USER_VALID_200";
    public final static String USER_VALID_FAILURE                                                = "USER_VALID_300";
    public final static String USER_VALID_TYPE_WRONG                                             = "USER_VALID_400";
    public final static String USER_VALID_FIELD_WRONG                                            = "USER_VALID_401";
    public final static String USER_VALID_CODE_WRONG                                             = "USER_VALID_402";

    public final static String USER_VALIDATE_PAYMENT_METHOD_SUCCESS                              = "USER_VALPAY_200";
    public final static String USER_VALIDATE_PAYMENT_METHOD_FAILURE                              = "USER_VALPAY_300";
    public final static String USER_VALIDATE_PAYMENT_METHOD_ID_ERROR                             = "USER_VALPAY_400";
    public final static String USER_VALIDATE_PAYMENT_METHOD_TYPE_ERROR                           = "USER_VALPAY_401";
    public final static String USER_VALIDATE_PAYMENT_METHOD_AMOUNT_ERROR                         = "USER_VALPAY_402";
    public final static String USER_VALIDATE_PAYMENT_METHOD_NOT_FOUND                            = "USER_VALPAY_403";
    public final static String USER_VALIDATE_PAYMENT_METHOD_STATUS_NOT_VALID                     = "USER_VALPAY_404";

    public final static String USER_SET_DEFAULT_PAYMENT_METHOD_SUCCESS                           = "USER_DEFPAY_200";
    public final static String USER_SET_DEFAULT_PAYMENT_METHOD_FAILURE                           = "USER_DEFPAY_300";
    public final static String USER_SET_DEFAULT_PAYMENT_METHOD_ID_WRONG                          = "USER_DEFPAY_400";
    public final static String USER_SET_DEFAULT_PAYMENT_METHOD_TYPE_WRONG                        = "USER_DEFPAY_401";
    public final static String USER_SET_DEFAULT_PAYMENT_METHOD_NOT_FOUND                         = "USER_DEFPAY_404";

    public final static String USER_INSERT_PAYMENT_METHOD_SUCCESS                                = "USER_INSPAY_200";
    public final static String USER_INSERT_PAYMENT_METHOD_FAILURE                                = "USER_INSPAY_300";
    public final static String USER_INSERT_PAYMENT_METHOD_TYPE_WRONG                             = "USER_INSPAY_400";
    public final static String USER_INSERT_PAYMENT_METHOD_PIN_WRONG                              = "USER_INSPAY_401";
    public final static String USER_INSERT_PAYMENT_METHOD_NOT_FOUND                              = "USER_INSPAY_404";
    public final static String USER_INSERT_PAYMENT_METHOD_CONNECTION_ERROR                       = "USER_INSPAY_405";

    public final static String USER_REMOVE_PAYMENT_METHOD_SUCCESS                                = "USER_REMPAY_200";
    public final static String USER_REMOVE_PAYMENT_METHOD_FAILURE                                = "USER_REMPAY_300";
    public final static String USER_REMOVE_PAYMENT_METHOD_ID_WRONG                               = "USER_REMPAY_400";
    public final static String USER_REMOVE_PAYMENT_METHOD_TYPE_WRONG                             = "USER_REMPAY_401";
    public final static String USER_REMOVE_PAYMENT_METHOD_NOT_FOUND                              = "USER_REMPAY_404";
    public final static String USER_REMOVE_PAYMENT_METHOD_CONNECTION_ERROR                       = "USER_REMPAY_405";

    public final static String USER_RETRIEVE_PAYMENT_DATA_SUCCESS                                = "USER_RETPAYDATA_200";
    public final static String USER_RETRIEVE_PAYMENT_DATA_FAILURE                                = "USER_RETPAYDATA_300";
    public final static String USER_RETRIEVE_PAYMENT_DATA_ID_WRONG                               = "USER_RETPAYDATA_400";
    public final static String USER_RETRIEVE_PAYMENT_DATA_TYPE_WRONG                             = "USER_RETPAYDATA_401";

    public final static String USER_RETRIEVE_ACTIVE_DEVICE_SUCCESS                               = "USER_RETACTDEV_200";
    public final static String USER_RETRIEVE_ACTIVE_DEVICE_FAILURE                               = "USER_RETACTDEV_300";
    public final static String USER_RETRIEVE_ACTIVE_DEVICE_ID_WRONG                              = "USER_RETACTDEV_400";
    public final static String USER_RETRIEVE_ACTIVE_DEVICE_TYPE_WRONG                            = "USER_RETACTDEV_401";

    public final static String USER_LOAD_VOUCHER_SUCCESS                                         = "USER_LOAD_VOUCHER_200";
    public final static String USER_LOAD_VOUCHER_FAILURE                                         = "USER_LOAD_VOUCHER_300";
    public final static String USER_LOAD_VOUCHER_CODE_WRONG                                      = "USER_LOAD_VOUCHER_400";

    public final static String USER_REMOVE_VOUCHER_SUCCESS                                       = "USER_REMOVE_VOUCHER_200";
    public final static String USER_REMOVE_VOUCHER_FAILURE                                       = "USER_REMOVE_VOUCHER_300";
    public final static String USER_REMOVE_VOUCHER_CODE_WRONG                                    = "USER_REMOVE_VOUCHER_400";

    public final static String USER_REMOVE_DEVICE_SUCCESS                                        = "USER_REMOVE_DEVICE_200";
    public final static String USER_REMOVE_DEVICE_FAILURE                                        = "USER_REMOVE_DEVICE_300";
    public final static String USER_REMOVE_DEVICE_CODE_WRONG                                     = "USER_REMOVE_DEVICE_400";

    public final static String USER_SET_USE_VOUCHER_SUCCESS                                      = "USER_SET_USE_VOUCHER_200";

    public final static String USER_GET_ACTIVE_VOUCHERS_SUCCESS                                  = "USER_GET_ACTIVE_VOUCHERS_200";
    public final static String USER_GET_ACTIVE_VOUCHERS_FAILURE                                  = "USER_GET_ACTIVE_VOUCHERS_300";
    //public final static String USER_GET_ACTIVE_VOUCHERS_CODE_WRONG           = "USER_GETACTIVEVOUCHERS_400";

    public final static String USER_SET_DEFAULT_LOYALTY_CARD_SUCCESS                             = "USER_SET_DEFAULT_LOYALTY_CARD_200";
    public final static String USER_SET_DEFAULT_LOYALTY_CARD_FAILURE                             = "USER_SET_DEFAULT_LOYALTY_CARD_300";
    public final static String USER_SET_DEFAULT_LOYALTY_CARD_PANCODE_WRONG                       = "USER_SET_DEFAULT_LOYALTY_CARD_400";

    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_SUCCESS                          = "USER_GET_AVAILABLE_LOYALTY_CARDS_200";
    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_INVALID_TICKET                   = "USER_REQU_401";
    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_UNAUTHORIZED                     = "USER_REQU_403";
    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_CF_NOT_FOUND                     = "USER_GET_AVAILABLE_LOYALTY_CARDS_300";
    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_GENERIC_ERROR                    = "USER_GET_AVAILABLE_LOYALTY_CARDS_301";
    public final static String USER_GET_AVAILABLE_LOYALTY_CARDS_MIGRATION_ERROR                  = "USER_GET_AVAILABLE_LOYALTY_CARDS_400";

    public final static String USER_RECOVER_USERNAME_SUCCESS                                     = "USER_RECOVER_USERNAME_200";
    public final static String USER_RECOVER_USERNAME_FAILURE                                     = "USER_RECOVER_USERNAME_300";
    public final static String USER_RECOVER_USERNAME_INVALID_TAX_CODE                            = "USER_RECOVER_USERNAME_301";
    public final static String USER_RECOVER_USERNAME_INVALID_REQUEST                             = "USER_REQU_400";
    public final static String USER_RECOVER_USERNAME_UNAUTHORIZED                                = "USER_REQU_403";

    public final static String UPDATE_SMS_LOG_SUCCESS                                            = "UPDATE_SMS_LOG_200";
    public final static String UPDATE_SMS_LOG_FAILURE                                            = "UPDATE_SMS_LOG_300";
    public final static String UPDATE_SMS_LOG_INVALID_REQUEST                                    = "UPDATE_SMS_LOG_400";

    public final static String USER_REQU_INVALID_REQUEST                                         = "USER_REQU_400";
    public final static String USER_REQU_INVALID_TICKET                                          = "USER_REQU_401";
    public final static String USER_REQU_JSON_SYNTAX_ERROR                                       = "USER_REQU_402";
    public final static String USER_REQU_UNAUTHORIZED                                            = "USER_REQU_403";
    public final static String USER_REQU_UNSUPPORTED_DEVICE                                      = "USER_REQU_405";

    public final static String REFUEL_CANCEL_SUCCESS                                             = "REFUEL_CANCEL_200";
    public final static String REFUEL_CANCEL_FAILURE                                             = "REFUEL_CANCEL_300";
    public final static String REFUEL_CANCEL_REFUEL_ID_WRONG                                     = "REFUEL_CANCEL_400";

    public final static String REFUEL_CONFIRM_SUCCESS                                            = "REFUEL_CONFIRM_200";
    public final static String REFUEL_CONFIRM_FAILURE                                            = "REFUEL_CONFIRM_300";
    public final static String REFUEL_CONFIRM_REFUEL_ID_WRONG                                    = "REFUEL_CONFIRM_400";

    public final static String REFUEL_TRANSACTION_CREATE_SUCCESS                                 = "REFUEL_TRANSACTION_CREATE_200";
    public final static String REFUEL_TRANSACTION_CREATE_FAILURE                                 = "REFUEL_TRANSACTION_CREATE_300";
    public final static String REFUEL_TRANSACTION_CREATE_DATA_WRONG                              = "REFUEL_TRANSACTION_CREATE_400";
    public final static String REFUEL_TRANSACTION_CREATE_PIN_WRONG                               = "REFUEL_TRANSACTION_CREATE_401";
    public final static String REFUEL_SYSTEM_ERROR                                               = "REFUEL_SYSTEM_ERROR_500";
    public final static String REFUEL_TRANSACTION_CREATE_PUMP_NOT_FOUND                          = "PUMP_NOT_FOUND_404";
    public final static String REFUEL_TRANSACTION_CREATE_STATION_NOT_FOUND                       = "STATION_PUMP_NOT_FOUND_404";

    public final static String APPLE_PAY_REFUEL_TRANSACTION_CREATE_SUCCESS                       = "APPLE_PAY_REFUEL_TRANSACTION_CREATE_200";
    public final static String APPLE_PAY_REFUEL_TRANSACTION_CREATE_FAILURE                       = "APPLE_PAY_REFUEL_TRANSACTION_CREATE_300";
    public final static String APPLE_PAY_REFUEL_TRANSACTION_CREATE_DATA_WRONG                    = "APPLE_PAY_REFUEL_TRANSACTION_CREATE_400";
    public final static String APPLE_PAY_REFUEL_TRANSACTION_CREATE_PIN_WRONG                     = "APPLE_PAY_REFUEL_TRANSACTION_CREATE_401";
    public final static String APPLE_PAY_REFUEL_SYSTEM_ERROR                                     = "APPLE_PAY_REFUEL_SYSTEM_ERROR_500";
    public final static String APPLE_PAY_REFUEL_TRANSACTION_CREATE_PUMP_NOT_FOUND                = "PUMP_NOT_FOUND_404";
    public final static String APPLE_PAY_REFUEL_TRANSACTION_CREATE_STATION_NOT_FOUND             = "STATION_PUMP_NOT_FOUND_404";

    public final static String REFUEL_EXEC_PAY_HISTORY_SUCCESS                                   = "REFUEL_EXEC_PAY_HISTORY_200";
    public final static String REFUEL_EXEC_PAY_HISTORY_FAILURE                                   = "REFUEL_EXEC_PAY_HISTORY_300";
    public final static String REFUEL_EXEC_PAY_HISTORY_STARTDATE_WRONG                           = "REFUEL_EXEC_PAY_HISTORY_400";
    public final static String REFUEL_EXEC_PAY_HISTORY_ENDDATE_WRONG                             = "REFUEL_EXEC_PAY_HISTORY_401";

    public final static String REFUEL_PENDING_SUCCESS                                            = "REFUEL_PENDING_200";
    public final static String REFUEL_PENDING_FAILURE                                            = "REFUEL_PENDING_300";

    public final static String VOUCHER_DEPOSIT_SUCCESS                                           = "VOUCHER_DEPOSIT_200";
    public final static String VOUCHER_DEPOSIT_FAILURE                                           = "VOUCHER_DEPOSIT_300";
    public final static String VOUCHER_DEPOSIT_VOUCHER_CODE_WRONG                                = "VOUCHER_DEPOSIT_400";

    public final static String VOUCHER_RETRIEVE_HISTORY_SUCCESS                                  = "VOUCHER_RETRIEVE_HISTORY_200";
    public final static String VOUCHER_RETRIEVE_HISTORY_FAILURE                                  = "VOUCHER_RETRIEVE_HISTORY_300";
    public final static String VOUCHER_RETRIEVE_HISTORY_STARTDATE_WRONG                          = "VOUCHER_RETRIEVE_HISTORY_400";
    public final static String VOUCHER_RETRIEVE_HISTORY_ENDDATE_WRONG                            = "VOUCHER_RETRIEVE_HISTORY_401";

    public final static String PAYMENT_RETRIEVE_HISTORY_SUCCESS                                  = "PAYMENT_RETRIEVE_HISTORY_200";
    public final static String PAYMENT_RETRIEVE_HISTORY_FAILURE                                  = "PAYMENT_RETRIEVE_HISTORY_300";
    public final static String PAYMENT_RETRIEVE_HISTORY_STARTDATE_WRONG                          = "PAYMENT_RETRIEVE_HISTORY_400";
    public final static String PAYMENT_RETRIEVE_HISTORY_ENDDATE_WRONG                            = "PAYMENT_RETRIEVE_HISTORY_401";
    public final static String PAYMENT_RETRIEVE_HISTORY_LIMIT_WRONG                              = "PAYMENT_RETRIEVE_HISTORY_402";
    public final static String PAYMENT_RETRIEVE_HISTORY_PAGEOFFSET_WRONG                         = "PAYMENT_RETRIEVE_HISTORY_403";

    public final static String RETRIEVE_TRANSACTION_HISTORY_SUCCESS                              = "RETRIEVE_TRANSACTION_HISTORY_200";
    public final static String RETRIEVE_TRANSACTION_HISTORY_FAILURE                              = "RETRIEVE_TRANSACTION_HISTORY_300";
    public final static String RETRIEVE_TRANSACTION_HISTORY_STARTDATE_WRONG                      = "RETRIEVE_TRANSACTION_HISTORY_400";
    public final static String RETRIEVE_TRANSACTION_HISTORY_ENDDATE_WRONG                        = "RETRIEVE_TRANSACTION_HISTORY_401";
    public final static String RETRIEVE_TRANSACTION_HISTORY_LIMIT_WRONG                          = "RETRIEVE_TRANSACTION_HISTORY_402";
    public final static String RETRIEVE_TRANSACTION_HISTORY_PAGEOFFSET_WRONG                     = "RETRIEVE_TRANSACTION_HISTORY_403";

    public static final String RETRIEVE_PAYMENT_DETAIL_SUCCESS                                   = "RETRIEVE_PAYMENT_DETAIL_200";
    public static final String RETRIEVE_PAYMENT_DETAIL_FAILURE                                   = "RETRIEVE_PAYMENT_DETAIL_300";
    public static final String RETRIEVE_PAYMENT_DETAIL_REFUELID_WRONG                            = "RETRIEVE_PAYMENT_DETAIL_400";

    public final static String STATION_RETRIEVE_SUCCESS                                          = "STATION_RETRIEVE_200";
    public final static String STATION_RETRIEVE_FAILURE                                          = "STATION_RETRIEVE_300";
    public final static String STATION_RETRIEVE_CODE_TYPE_WRONG                                  = "STATION_RETRIEVE_400";
    public final static String STATION_RETRIEVE_CODE_WRONG                                       = "STATION_RETRIEVE_401";
    public final static String STATION_RETRIEVE_USER_POSITION_WRONG                              = "STATION_RETRIEVE_402";
    public final static String STATION_RETRIEVE_BEACON_CODE_NOT_FOUND                            = "STATION_RETRIEVE_403";
    public final static String STATION_RETRIEVE_BEACONCODE_WRONG                                 = "STATION_RETRIEVE_404";
    public final static String STATION_RETRIEVE_USER_NOT_IN_RANGE                                = "STATION_RETRIEVE_405";
    public final static String STATION_RETRIEVE_STATION_PUMP_NOT_FOUND                           = "STATION_PUMP_NOT_FOUND_404";
    public final static String STATION_RETRIEVE_STATION_NOT_FOUND                                = "STATION_NOT_FOUND_404";
    public final static String STATION_RETRIEVE_ID_CODE_NOT_FOUND                                = "STATION_RETRIEVE_403";
    public final static String STATION_RETRIEVE_IDCODE_WRONG                                     = "STATION_RETRIEVE_404";

    public final static String STATION_LIST_RETRIEVE_SUCCESS                                     = "STATION_LIST_RETRIEVE_200";
    public final static String STATION_LIST_RETRIEVE_FAILURE                                     = "STATION_LIST_RETRIEVE_300";
    public final static String STATION_LIST_USER_POSITION_WRONG                                  = "STATION_LIST_RETRIEVE_301";

    public final static String PUMP_STATUS_RETRIEVE_FAILURE                                      = "PUMP_STATUS_RETRIEVE_500";

    public final static String PAYMENT_RETRIEVE_SUCCESS                                          = "PAYMENT_RETRIEVE_200";
    public final static String PAYMENT_RETRIEVE_FAILURE                                          = "PAYMENT_RETRIEVE_300";
    public final static String PAYMENT_RETRIEVE_REFUEL_ID_WRONG                                  = "PAYMENT_RETRIEVE_400";

    public final static String PAYMENT_RETRIEVE_DATA_SUCCESS                                     = "PAYMENT_RETRIEVEDATA_200";
    public final static String PAYMENT_RETRIEVE_DATA_FAILURE                                     = "PAYMENT_RETRIEVEDATA_300";
    public final static String PAYMENT_RETRIEVE_DATA_REQUEST_TYPE_WRONG                          = "PAYMENT_RETRIEVEDATA_400";

    public final static String PAYMENT_CONFIRM_SUCCESS                                           = "PAYMENT_CONFIRM_200";
    public final static String PAYMENT_CONFIRM_FAILURE                                           = "PAYMENT_CONFIRM_300";

    public final static String REFUEL_CONFIRM_SYSTEM_ERROR                                       = "REFUEL_CONFIRM_500";
    public final static String PAYMENT_RETRIEVE_SYSTEM_ERROR                                     = "PAYMENT_RETRIEVE_500";
    public final static String PAYMENT_RETRIEVE_HISTORY_SYSTEM_ERROR                             = "PAYMENT_RETRIEVE_HISTORY_500";
    public final static String REFUEL_PENDING_SYSTEM_SUCCESS                                     = "REFUEL_PENDING_200";
    public final static String REFUEL_PENDING_SYSTEM_ERROR                                       = "REFUEL_PENDING_500";
    public final static String RETRIEVE_STATION_SYSTEM_ERROR                                     = "RETRIEVE_STATION_500";

    public final static String RETRIEVE_CITIES_SUCCESS                                           = "RET_CIT_200";
    public final static String RETRIEVE_CITIES_FAILURE                                           = "RET_CIT_300";

    public final static String RETRIEVE_APPLICATION_SETTINGS_SUCCESS                             = "RETRIEVE_APPLICATION_SETTINGS_200";
    public final static String RETRIEVE_APPLICATION_SETTINGS_FAILURE                             = "RETRIEVE_APPLICATION_SETTINGS_300";
    public final static String RETRIEVE_APPLICATION_SETTINGS_SYSTEM_ERROR                        = "RETRIEVE_APPLICATION_SETTINGS_500";

    public final static String RETRIEVE_TERMS_SERVICE_SUCCESS                                    = "RETRIEVE_TERMS_SERVICE_200";
    public final static String RETRIEVE_TERMS_SERVICE_FAILURE                                    = "RETRIEVE_TERMS_SERVICE_300";

    public final static String RETRIEVE_DOCUMENT_SUCCESS                                         = "RETRIEVE_DOCUMENT_200";
    public final static String RETRIEVE_DOCUMENT_FAILURE                                         = "RETRIEVE_DOCUMENT_300";

    public final static String RETRIEVE_SYSTEM_PARAMETERS_ERROR                                  = "SYSTEM_ERROR_500";

    public final static String POP_APPROVE_SUCCESS                                               = "POP_APPROVE_200";
    public final static String POP_APPROVE_FAILURE                                               = "POP_APPROVE_300";
    public final static String POP_APPROVE_PIN_WRONG                                             = "POP_APPROVE_301";
    public final static String POP_APPROVE_ID_WRONG                                              = "POP_APPROVE_400";

    public final static String POP_GET_SUCCESS                                                   = "POP_GET_200";
    public final static String POP_GET_FAILURE                                                   = "POP_GET_300";
    public final static String POP_GET_ID_WRONG                                                  = "POP_GET_400";

    public final static String POP_REJECT_SUCCESS                                                = "POP_REJECT_200";
    public final static String POP_REJECT_FAILURE                                                = "POP_REJECT_300";
    public final static String POP_REJECT_ID_WRONG                                               = "POP_REJECT_400";

    public final static String POP_DETAIL_SUCCESS                                                = "POP_DETAIL_200";
    public final static String POP_DETAIL_FAILURE                                                = "POP_DETAIL_300";
    public final static String POP_DETAIL_ID_WRONG                                               = "POP_DETAIL_400";

    public final static String POP_CONFIRM_TRANSACTION_SUCCESS                                   = "POP_CONFIRM_200";
    public final static String POP_CONFIRM_TRANSACTION_FAILURE                                   = "POP_CONFIRM_300";
    public final static String POP_CONFIRM_TRANSACTION_WRONG_ID                                  = "POP_CONFIRM_400";

    public final static String MANAGER_AUTH_SUCCESS                                              = "MANAGER_AUTH_200";
    public final static String MANAGER_AUTH_FAILURE                                              = "MANAGER_AUTH_300";
    public final static String MANAGER_AUTH_LOGIN_ERROR                                          = "MANAGER_AUTH_301";
    public final static String MANAGER_AUTH_EMAIL_WRONG                                          = "MANAGER_AUTH_401";
    public final static String MANAGER_AUTH_PASSWORD_WRONG                                       = "MANAGER_AUTH_402";
    public final static String MANAGER_AUTH_PASSWORD_SHORT                                       = "MANAGER_AUTH_409";
    public final static String MANAGER_AUTH_NUMBER_LESS                                          = "MANAGER_AUTH_410";
    public final static String MANAGER_AUTH_LOWER_LESS                                           = "MANAGER_AUTH_411";
    public final static String MANAGER_AUTH_UPPER_LESS                                           = "MANAGER_AUTH_412";

    public final static String MANAGER_LOGOUT_SUCCESS                                            = "MANAGER_LOGOUT_200";
    public final static String MANAGER_LOGOUT_FAILURE                                            = "MANAGER_LOGOUT_300";
    public final static String MANAGER_LOGOUT_ERROR                                              = "MANAGER_LOGOUT_301";

    public final static String MANAGER_RESCUE_PASSWORD_SUCCESS                                   = "MANAGER_RESCUE_200";
    public final static String MANAGER_RESCUE_PASSWORD_FAILURE                                   = "MANAGER_RESCUE_300";
    public final static String MANAGER_RESCUE_PASSWORD_ERROR                                     = "MANAGER_RESCUE_301";

    public final static String MANAGER_PWD_SUCCESS                                               = "MANAGER_PWD_200";
    public final static String MANAGER_PWD_FAILURE                                               = "MANAGER_PWD_300";
    public final static String MANAGER_PWD_NEW_PASSWORD_WRONG                                    = "MANAGER_PWD_401";
    public final static String MANAGER_PWD_OLD_PASSWORD_WRONG                                    = "MANAGER_PWD_402";
    public final static String MANAGER_PWD_USERNAME_PASSWORD_EQUALS                              = "MANAGER_PWD_403";
    public final static String MANAGER_PWD_PASSWORD_SHORT                                        = "MANAGER_PWD_409";
    public final static String MANAGER_PWD_NUMBER_LESS                                           = "MANAGER_PWD_410";
    public final static String MANAGER_PWD_LOWER_LESS                                            = "MANAGER_PWD_411";
    public final static String MANAGER_PWD_UPPER_LESS                                            = "MANAGER_PWD_412";
    public final static String MANAGER_PWD_OLD_NEW_EQUALS                                        = "MANAGER_PWD_413";
    public final static String MANAGER_PWD_OLD_ERROR                                             = "MANAGER_PWD_414";
    public final static String MANAGER_PWD_HISTORY_NEW_EQUALS                                    = "MANAGER_PWD_415";

    public final static String MANAGER_RETRIEVE_TRANSACTIONS_SUCCESS                             = "MANAGER_RETRIEVE_TRANSACTIONS_200";
    public final static String MANAGER_RETRIEVE_TRANSACTIONS_FAILURE                             = "MANAGER_RETRIEVE_TRANSACTIONS_300";
    public final static String MANAGER_RETRIEVE_TRANSACTIONS_INVALID_PARAMETERS                  = "MANAGER_RETRIEVE_TRANSACTIONS_301";
    public final static String MANAGER_RETRIEVE_TRANSACTIONS_INVALID_PUMPMAXTRANSACTIONS         = "MANAGER_RETRIEVE_TRANSACTIONS_302";

    public final static String MANAGER_RETRIEVE_VOUCHERS_SUCCESS                                 = "MANAGER_RETRIEVE_VOUCHERS_200";
    public final static String MANAGER_RETRIEVE_VOUCHERS_FAILURE                                 = "MANAGER_RETRIEVE_VOUCHERS_300";
    public final static String MANAGER_RETRIEVE_VOUCHERS_INVALID_PARAMETERS                      = "MANAGER_RETRIEVE_VOUCHERS_301";

    public final static String SURVEY_REQU_INVALID_REQUEST                                       = "USER_REQU_400";
    public final static String SURVEY_REQU_INVALID_TICKET                                        = "USER_REQU_401";
    public final static String SURVEY_REQU_JSON_SYNTAX_ERROR                                     = "USER_REQU_402";
    public final static String SURVEY_REQU_UNAUTHORIZED                                          = "USER_REQU_403";

    public final static String SURVEY_GET_SURVEY_SUCCESS                                         = "SURVEY_GET_SURVEY_200";

    public final static String SURVEY_SUBMIT_SURVEY_SUCCESS                                      = "SURVEY_SUBMIT_SURVEY_200";

    public final static String SKIP_PAYMENT_METHOD_CONFIGURATION_SUCCESS                         = "USER_SKIP_PAYMENT_CONF_200";
    public final static String SKIP_PAYMENT_METHOD_CONFIGURATION_FAILURE                         = "USER_SKIP_PAYMENT_CONF_300";
    public final static String SKIP_PAYMENT_METHOD_CONFIGURATION_UNAUTHORIZED                    = "USER_REQU_403";

    public final static String UPD_MOBILE_PHONE_SUCCESS                                          = "UPD_MOBILE_PHONE_200";
    public final static String UPD_MOBILE_PHONE_FAILURE                                          = "UPD_MOBILE_PHONE_300";
    public final static String UPD_MOBILE_PHONE_INVALID_PARAMETERS                               = "UPD_MOBILE_PHONE_301";

    public final static String CANCEL_MOBILE_PHONE_SUCCESS                                       = "CANCEL_MOBILE_PHONE_200";
    public final static String CANCEL_MOBILE_PHONE_FAILURE                                       = "CANCEL_MOBILE_PHONE_300";
    public final static String CANCEL_MOBILE_PHONE_INVALID_PARAMETERS                            = "CANCEL_MOBILE_PHONE_301";

    public final static String USER_RESEND_VALIDATATION_SUCCESS                                  = "USER_RESEND_VALIDATATION_200";
    public final static String USER_RESEND_VALIDATATION_FAILURE                                  = "USER_RESEND_VALIDATATION_300";
    public final static String USER_RESEND_VALIDATATION_INVALID_PARAMETERS                       = "USER_RESEND_VALIDATATION_301";

    public final static String RETRIEVE_PREFIXES_SUCCESS                                         = "RETRIEVE_PREFIXES_200";
    public final static String RETRIEVE_PREFIXES_FAILURE                                         = "RETRIEVE_PREFIXES_300";
    public final static String RETRIEVE_PREFIXES_INVALID_PARAMETERS                              = "RETRIEVE_PREFIXES_301";

    public final static String GET_CREDIT_VOUCHER_SUCCESS                                        = "GET_CREDIT_VOUCHER_200";
    public final static String GET_CREDIT_VOUCHER_FAILURE                                        = "GET_CREDIT_VOUCHER_300";
    public final static String GET_CREDIT_VOUCHER_INVALID_PARAMETERS                             = "GET_CREDIT_VOUCHER_301";

    public final static String GET_PARTNER_LIST_SUCCESS                                          = "GET_PARTNER_LIST_200";
    public final static String GET_PARTNER_LIST_FAILURE                                          = "GET_PARTNER_LIST_300";

    public final static String GET_CATEGORY_LIST_SUCCESS                                         = "GET_CATEGORY_LIST_200";
    public final static String GET_CATEGORY_LIST_FAILURE                                         = "GET_CATEGORY_LIST_300";

    public final static String GET_BRAND_LIST_SUCCESS                                            = "GET_CBRAND_LIST_200";
    public final static String GET_BRAND_LIST_FAILURE                                            = "GET_BRAND_LIST_300";

    public final static String GET_ACTION_URL_SUCCESS                                            = "GET_ACTION_URL_200";
    public final static String GET_ACTION_URL_FAILURE                                            = "GET_ACTION_URL_300";

    public final static String VOUCHER_REQU_JSON_SYNTAX_ERROR                                    = "VOUCHER_REQU_402";

    public final static String CREATE_VOUCHER_TRANSACTION_SUCCESS                                = "CREATE_VOUCHER_TRANSACTION_200";
    public final static String CREATE_VOUCHER_TRANSACTION_FAILURE                                = "CREATE_VOUCHER_TRANSACTION_300";
    public final static String CREATE_VOUCHER_TRANSACTION_INVALID_PARAMETERS                     = "CREATE_VOUCHER_TRANSACTION_301";
    public final static String CREATE_VOUCHER_TRANSACTION_PIN_WRONG                              = "CREATE_VOUCHER_307";

    public final static String CREATE_APPLE_PAY_VOUCHER_TRANSACTION_SUCCESS                      = "CREATE_APPLE_PAY_VOUCHER_TRANSACTION_200";
    public final static String CREATE_APPLE_PAY_VOUCHER_TRANSACTION_FAILURE                      = "CREATE_APPLE_PAY_VOUCHER_TRANSACTION_300";
    public final static String CREATE_APPLE_PAY_VOUCHER_TRANSACTION_INVALID_PARAMETERS           = "CREATE_APPLE_PAY_VOUCHER_TRANSACTION_301";

    public final static String RETRIEVE_VOUCHER_TRANSACTION_SUCCESS                              = "RETRIEVE_VOUCHER_TRANSACTION_200";
    public final static String RETRIEVE_VOUCHER_TRANSACTION_FAILURE                              = "RETRIEVE_VOUCHER_TRANSACTION_300";
    public final static String RETRIEVE_VOUCHER_TRANSACTION_INVALID_PARAMETERS                   = "RETRIEVE_VOUCHER_TRANSACTION_301";

    public final static String CHECK_AVAILABILITY_AMOUNT_SUCCESS                                 = "CHECK_AVAILABILITY_AMOUNT_200";
    public final static String CHECK_AVAILABILITY_AMOUNT_FAILURE                                 = "CHECK_AVAILABILITY_AMOUNT_300";

    public final static String RETRIVE_REFUEL_TRANSACTION_DETAIL_SUCCESS                         = "RETRIVE_REFUEL_TRANSACTION_DETAIL_200";
    public final static String RETRIVE_REFUEL_TRANSACTION_DETAIL_FAILURE                         = "RETRIVE_REFUEL_TRANSACTION_DETAIL_300";
    public final static String RETRIVE_REFUEL_TRANSACTION_DETAIL_INVALID_PARAMETERS              = "RETRIVE_REFUEL_TRANSACTION_DETAIL_301";

    public final static String USER_UPDATE_TERMS_OF_SERVICE_SUCCESS                              = "USER_UPDATE_TERMS_OF_SERVICE_200";
    public final static String USER_UPDATE_TERMS_OF_SERVICE_INVALID_REQUEST                      = "USER_REQU_400";

    public final static String RETRIEVE_NOTIFICATION_SUCCESS                                     = "RETRIEVE_NOTIFICATION_200";
    public final static String RETRIEVE_NOTIFICATION_FAILURE                                     = "RETRIEVE_NOTIFICATION_300";
    public final static String RETRIEVE_NOTIFICATION_INVALID_PARAMETERS                          = "RETRIEVE_NOTIFICATION_301";

    public final static String EVENT_NOTIFICATION_REQU_INVALID_REQUEST                           = "USER_REQU_400";
    public final static String EVENT_NOTIFICATION_REQU_INVALID_TICKET                            = "USER_REQU_401";
    public final static String EVENT_NOTIFICATION_REQU_JSON_SYNTAX_ERROR                         = "USER_REQU_402";
    public final static String EVENT_NOTIFICATION_REQU_UNAUTHORIZED                              = "USER_REQU_403";

    public final static String RETRIEVE_EVENT_NOTIFICATION_TRANSACTION_DETAIL_SUCCESS            = "RETRIVE_EVENT_NOTIFICATION_TRANSACTION_DETAIL_200";
    public final static String RETRIVE_EVENT_NOTIFICATION_TRANSACTION_DETAIL_FAILURE             = "RETRIVE_EVENT_NOTIFICATION_TRANSACTION_DETAIL_300";
    public final static String RETRIEVE_EVENT_NOTIFICATION_TRANSACTION_DETAIL_INVALID_PARAMETERS = "RETRIVE_EVENT_NOTIFICATION_TRANSACTION_DETAIL_301";

    public final static String USER_INFO_REDEMPTION_SUCCESS                                      = "USER_INFO_REDEMPTION_200";
    public final static String USER_INFO_REDEMPTION_FAILURE                                      = "USER_INFO_REDEMPTION_300";
    public final static String USER_INFO_REDEMPTION_INVALID_PARAMETERS                           = "USER_INFO_REDEMPTION_301";
    public final static String USER_INFO_REDEMPTION_INVALID_REQUEST                              = "USER_REQU_400";

    public final static String USER_REDEMPTION_SUCCESS                                           = "USER_GET_REDEMPTION_200";
    public final static String USER_REDEMPTION_FAILURE                                           = "USER_GET_REDEMPTION_300";
    public final static String USER_REDEMPTION_INVALID_PARAMETERS                                = "USER_GET_REDEMPTION_301";
    public final static String USER_REDEMPTION_INVALID_REQUEST                                   = "USER_REQU_400";

    public final static String GET_AWARD_LIST_SUCCESS                                            = "GET_AWARD_LIST_200";
    public final static String GET_AWARD_LIST_FAILURE                                            = "GET_AWARD_LIST_300";

    public final static String USER_REDEMPTION_AWARD_SUCCESS                                     = "USER_GET_REDEMPTION_AWARD_200";
    public final static String USER_REDEMPTION_AWARD_FAILURE                                     = "USER_GET_REDEMPTION_AWARD_300";
    public final static String USER_REDEMPTION_AWARD_INVALID_PARAMETERS                          = "USER_GET_REDEMPTION_AWARD_301";
    public final static String USER_REDEMPTION_AWARD_INVALID_REQUEST                             = "USER_REQU_400";
    
    public final static String GET_MISSION_LIST_SUCCESS                                          = "GET_MISSION_LIST_200";
    public final static String GET_MISSION_LIST_FAILURE                                          = "GET_MISSION_LIST_300";
    
    public final static String GET_RECEIPT_SUCCESS                                               = "GET_RECEIPT_200";
    public final static String GET_RECEIPT_FAILURE                                               = "USER_V2_GET_RECEIPT_300";
    public final static String GET_RECEIPT_INVALID_PARAMETERS                                    = "USER_V2_GET_RECEIPT_301";                        

    public final static String USER_V2_NOTIFICATION_VIRTUALIZATION_SUCCESS                       = "USER_V2_NOTIFICATION_VIRTUALIZATION_200";
    public final static String USER_V2_NOTIFICATION_VIRTUALIZATION_FAILURE                       = "USER_V2_NOTIFICATION_VIRTUALIZATION_300";

    public final static String ABSTRACT_REQUEST_ERROR                                            = "ABSTRACT_REQUEST_500";

    public static final String ESTIMATE_PARKING_PRICE_SUCCESS                                    = "PARKING_ESTIMATE_PARKING_PRICE_200";
    public static final String ESTIMATE_PARKING_PRICE_FAILURE                                    = "PARKING_ESTIMATE_PARKING_PRICE_300";
    public static final String ESTIMATE_PARKING_PRICE_INVALID_REQUEST                            = "USER_REQU_400";
    
	public final static String APPROVE_PARKING_TRANSACTION_SUCCESS                               = "PARKING_APPROVE_PARKING_TRANSACTION_200";
    public final static String APPROVE_PARKING_TRANSACTION_INVALID_REQUEST                       = "USER_REQU_400";
    
    public final static String RETRIEVE_PENDING_PARKING_TRANSACTION_SUCCESS                      = "RETRIEVE_PENDING_PARKING_TRANSACTION_200";
    public final static String RETRIEVE_PENDING_PARKING_TRANSACTION_INVALID_REQUEST              = "USER_REQU_400";
    
    public final static String RETRIEVE_PARKING_ZONES_SUCCESS                                    = "RETRIEVE_PARKING_ZONES_200";
    public final static String GETCITIES_SUCCESS                                                 = "GETCITIES_200";
    
    public final static String RETRIEVE_PARKING_TRANSACTION_DETAIL_SUCCESS                       = "RETRIEVE_PARKING_TRANSACTION_DETAIL_200";
    public final static String RETRIEVE_PARKING_TRANSACTION_DETAIL_FAILURE                       = "RETRIEVE_PARKING_TRANSACTION_DETAIL_300";
    public final static String RETRIEVE_PARKING_TRANSACTION_DETAIL_INVALID_ID                    = "RETRIEVE_PARKING_TRANSACTION_DETAIL_400";
    
    public static final String ESTIMATE_END_PARKING_PRICE_SUCCESS                                = "PARKING_ESTIMATE_END_PARKING_PRICE_200";
    public static final String ESTIMATE_END_PARKING_PRICE_FAILURE                                = "PARKING_ESTIMATE_END_PARKING_PRICE_300";
    
    public final static String USER_ADD_PLATE_NUMBER_SUCCESS                                     = "USER_ADD_PLATE_NUMBER_200";
    public final static String USER_ADD_PLATE_NUMBER_FAILURE                                     = "USER_ADD_PLATE_NUMBER_300";
    public final static String USER_ADD_PLATE_NUMBER_WRONG                                       = "USER_ADD_PLATE_NUMBER_400";
    
    public final static String USER_REMOVE_PLATE_NUMBER_SUCCESS                                  = "USER_REMOVE_PLATE_NUMBER_200";
    public final static String USER_REMOVE_PLATE_NUMBER_FAILURE                                  = "USER_REMOVE_PLATE_NUMBER_300";
    public final static String USER_REMOVE_PLATE_NUMBER_WRONG                                    = "USER_REMOVE_PLATE_NUMBER_400";
    
    public final static String USER_SET_DEFAULT_PLATE_NUMBER_SUCCESS                             = "USER_SET_DEFAULT_PLATE_NUMBER_200";
    public final static String USER_SET_DEFAULT_PLATE_NUMBER_FAILURE                             = "USER_SET_DEFAULT_PLATE_NUMBER_300";
    public final static String USER_SET_DEFAULT_PLATE_NUMBER_WRONG                               = "USER_SET_DEFAULT_PLATE_NUMBER_400";
    
    public final static String USER_REQUEST_RESEND_PIN_SUCCESS                                   = "USER_REQUEST_RESEND_PIN_200";
    public final static String USER_REQUEST_RESEND_PIN_FAILURE                                   = "USER_REQUEST_RESEND_PIN_300";
    
    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_SUCCESS                                                = "USER_MULTICARD_INSPAY_200";
    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_FAILED                                                 = "USER_MULTICARD_INSPAY_300";
    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_NOT_FOUND                                              = "USER_MULTICARD_INSPAY_404";
    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_CONNECTION_ERROR                                       = "USER_MULTICARD_INSPAY_405";
    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_INVALID_PIN                                            = "USER_MULTICARD_INSPAY_406";
    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_WRONG_PIN                                              = "USER_MULTICARD_INSPAY_407";
    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_UNAUTHORIZED                                           = "USER_REQU_403";
    public final static String USER_INSERT_MULTICARD_PAYMENT_METHOD_INVALID_TICKET                                         = "USER_REQU_401";
    
    public final static String MULTICARD_REFUEL_TRANSACTION_CREATE_SUCCESS                       = "MULTICARD_REFUEL_TRANSACTION_CREATE_200";
    public final static String MULTICARD_REFUEL_TRANSACTION_CREATE_FAILURE                       = "MULTICARD_REFUEL_TRANSACTION_CREATE_300";
    public final static String MULTICARD_REFUEL_TRANSACTION_CREATE_DATA_WRONG                    = "MULTICARD_REFUEL_TRANSACTION_CREATE_400";
    public final static String MULTICARD_REFUEL_TRANSACTION_CREATE_PIN_WRONG                     = "MULTICARD_REFUEL_TRANSACTION_CREATE_401";
    public final static String MULTICARD_REFUEL_SYSTEM_ERROR                                     = "MULTICARD_REFUEL_SYSTEM_ERROR_500";
    public final static String MULTICARD_REFUEL_TRANSACTION_CREATE_PUMP_NOT_FOUND                = "PUMP_NOT_FOUND_404";
    public final static String MULTICARD_REFUEL_TRANSACTION_CREATE_STATION_NOT_FOUND             = "STATION_PUMP_NOT_FOUND_404";
    
    public final static String USER_SET_ACTIVE_DPAN_SUCCESS                                      = "USER_SET_ACTIVE_DPAN_200";
    public final static String USER_SET_ACTIVE_DPAN_FAILURE                                      = "USER_SET_ACTIVE_DPAN_300";
    public final static String USER_SET_ACTIVE_DPAN_WRONG                                        = "USER_SET_ACTIVE_DPAN_400";
}
