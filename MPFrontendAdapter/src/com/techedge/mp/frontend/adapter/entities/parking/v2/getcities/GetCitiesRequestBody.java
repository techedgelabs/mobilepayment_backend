package com.techedge.mp.frontend.adapter.entities.parking.v2.getcities;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class GetCitiesRequestBody implements Validable {

    private double latitude;
    private double longitude;

    @Override
    public Status check() {

        Status status = new Status();

        /*
         * if (this.latitude == null) {
         * 
         * status.setStatusCode(StatusCode.USER_AUTH_EMAIL_WRONG);
         * 
         * return status;
         * 
         * }
         * 
         * if (this.longitude == null) {
         * 
         * status.setStatusCode(StatusCode.USER_AUTH_PASSWORD_WRONG);
         * 
         * return status;
         * 
         * }
         */

        status.setStatusCode(StatusCode.RETRIEVE_PARKING_ZONES_SUCCESS);

        return status;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}
