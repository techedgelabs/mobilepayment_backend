package com.techedge.mp.frontend.adapter.entities.user.v2.externalauthentication;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.ExternalAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PlateNumber;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.UserSocialData;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransaction;
import com.techedge.mp.core.business.interfaces.parking.RetrievePendingParkingTransactionResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontend.adapter.entities.common.AddressData;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.ContactData;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.EmailSecurityData;
import com.techedge.mp.frontend.adapter.entities.common.LastLoginData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.SecurityData;
import com.techedge.mp.frontend.adapter.entities.common.SocialData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.UserStatus;
import com.techedge.mp.frontend.adapter.entities.common.v2.PlateNumberInfo;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ExternalAuthenticationUserRequest extends AbstractRequest {

    private Status                                status = new Status();

    private ExternalAuthenticationUserBodyRequest body;

    public ExternalAuthenticationUserBodyRequest getBody() {
        return body;
    }

    public void setBody(ExternalAuthenticationUserBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.USER_V2_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_V2_AUTH_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        ExternalAuthenticationUserRequest externalAuthenticationUserRequest = this;

        String accessToken = externalAuthenticationUserRequest.getBody().getAccessToken();
        String socialProviderRequestString = externalAuthenticationUserRequest.getBody().getService().toUpperCase();

        ExternalAuthenticationResponse externalAuthenticationResponse = getUserV2ServiceRemote().externalAuthentication(accessToken, socialProviderRequestString,
                externalAuthenticationUserRequest.getBody().getRequestID(), externalAuthenticationUserRequest.getBody().getDeviceID(),
                externalAuthenticationUserRequest.getBody().getDeviceName(), externalAuthenticationUserRequest.getBody().getDeviceToken());

        if (externalAuthenticationResponse == null || externalAuthenticationResponse.getStatusCode() == null) {

            ExternalAuthenticationUserResponseFailure externalAuthenticationUserResponseFailure = new ExternalAuthenticationUserResponseFailure();

            status.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_AUTH_FAILURE);
            status.setStatusMessage(prop.getProperty(ResponseHelper.USER_V2_EXTERNAL_AUTH_FAILURE));

            externalAuthenticationUserResponseFailure.setStatus(status);

            return externalAuthenticationUserResponseFailure;
        }

        if (externalAuthenticationResponse.getStatusCode().equals(StatusCode.USER_V2_EXTERNAL_AUTH_SUCCESS)) {

            System.out.println("SUCCESS");

            ExternalAuthenticationUserResponseSuccess socialAuthenticationUserResponseSuccess = new ExternalAuthenticationUserResponseSuccess();

            status.setStatusCode(externalAuthenticationResponse.getStatusCode());
            status.setStatusMessage(prop.getProperty(externalAuthenticationResponse.getStatusCode()));
            socialAuthenticationUserResponseSuccess.setStatus(status);

            ExternalAuthenticationUserResponseBody body = new ExternalAuthenticationUserResponseBody();

            ExternalAuthenticationUserDataResponse userData = new ExternalAuthenticationUserDataResponse();
            userData.setFirstName(externalAuthenticationResponse.getUser().getPersonalData().getFirstName());
            userData.setLastName(externalAuthenticationResponse.getUser().getPersonalData().getLastName());
            userData.setFiscalCode(externalAuthenticationResponse.getUser().getPersonalData().getFiscalCode());
            Calendar calendar = new GregorianCalendar();
            calendar.setTime(externalAuthenticationResponse.getUser().getPersonalData().getBirthDate());

            CustomDate dateOfBirth = new CustomDate();
            dateOfBirth.setYear(calendar.get(Calendar.YEAR));
            dateOfBirth.setMonth(calendar.get(Calendar.MONTH) + 1);
            dateOfBirth.setDay(calendar.get(Calendar.DAY_OF_MONTH));
            userData.setDateOfBirth(dateOfBirth);

            userData.setBirthMunicipality(externalAuthenticationResponse.getUser().getPersonalData().getBirthMunicipality());
            userData.setBirthProvince(externalAuthenticationResponse.getUser().getPersonalData().getBirthProvince());
            userData.setLanguage(externalAuthenticationResponse.getUser().getPersonalData().getLanguage());
            userData.setSex(externalAuthenticationResponse.getUser().getPersonalData().getSex());
            userData.setContactKey(externalAuthenticationResponse.getUser().getContactKey());

            SecurityData securityData = new SecurityData();
            securityData.setEmail(externalAuthenticationResponse.getUser().getPersonalData().getSecurityDataEmail());
            securityData.setPassword(externalAuthenticationResponse.getUser().getPersonalData().getSecurityDataPassword());
            securityData.setStatus(1);
            userData.setSecurityData(securityData);

            Date now = new Date();

            ContactData contactData = new ContactData();
            for (MobilePhone mobilePhone : externalAuthenticationResponse.getUser().getMobilePhoneList()) {

                mobilePhone.setCreationTimestamp(null);
                mobilePhone.setLastUsedTimestamp(null);
                mobilePhone.setVerificationCode(null);

                if (mobilePhone.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_PENDING) {

                    // Restituisci i metodi in stato pending che non sono
                    // scaduti
                    if (mobilePhone.getExpirationTimestamp().getTime() > now.getTime()) {
                        // System.out.println("Restituito");
                        mobilePhone.setExpirationTimestamp(null);
                        contactData.getMobilePhones().add(mobilePhone);

                    }
                }

                if (mobilePhone.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_ACTIVE) {

                    // Restituisci i numeri attivi (dovrebbe essercene solo uno)
                    mobilePhone.setExpirationTimestamp(null);
                    contactData.getMobilePhones().add(mobilePhone);

                }

            }

            userData.setContactData(contactData);

            AddressData addressData = null;
            if (externalAuthenticationResponse.getUser().getPersonalData().getAddress() != null) {
                addressData = new AddressData();
                addressData.setCity(externalAuthenticationResponse.getUser().getPersonalData().getAddress().getCity());
                addressData.setCountryCodeId(externalAuthenticationResponse.getUser().getPersonalData().getAddress().getCountryCodeId());
                addressData.setCountryCodeName(externalAuthenticationResponse.getUser().getPersonalData().getAddress().getCountryCodeName());
                addressData.setHouseNumber(externalAuthenticationResponse.getUser().getPersonalData().getAddress().getHouseNumber());
                addressData.setRegion(externalAuthenticationResponse.getUser().getPersonalData().getAddress().getRegion());
                addressData.setStreet(externalAuthenticationResponse.getUser().getPersonalData().getAddress().getStreet());
                addressData.setZipCode(externalAuthenticationResponse.getUser().getPersonalData().getAddress().getZipcode());
            }
            userData.setAddressData(addressData);

            AddressData billingAddressData = null;
            if (externalAuthenticationResponse.getUser().getPersonalData().getBillingAddress() != null) {
                billingAddressData = new AddressData();
                billingAddressData.setCity(externalAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getCity());
                billingAddressData.setCountryCodeId(externalAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getCountryCodeId());
                billingAddressData.setCountryCodeName(externalAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getCountryCodeName());
                billingAddressData.setHouseNumber(externalAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getHouseNumber());
                billingAddressData.setRegion(externalAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getRegion());
                billingAddressData.setStreet(externalAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getStreet());
                billingAddressData.setZipCode(externalAuthenticationResponse.getUser().getPersonalData().getBillingAddress().getZipcode());
            }
            userData.setBillingAddressData(billingAddressData);

            LastLoginData lastLoginData = new LastLoginData();

            String lastLoginDevice = "";
            if (externalAuthenticationResponse.getUser().getLastLoginData() != null) {
                lastLoginDevice = externalAuthenticationResponse.getUser().getLastLoginData().getDeviceName();
            }

            Timestamp lastLoginTime = null;
            if (externalAuthenticationResponse.getUser().getLastLoginData() != null) {
                lastLoginTime = externalAuthenticationResponse.getUser().getLastLoginData().getTime();
            }

            lastLoginData.setLastLoginDevice(lastLoginDevice);
            lastLoginData.setLastLoginTime(CustomTimestamp.createCustomTimestamp(lastLoginTime));
            userData.setLastLoginData(lastLoginData);

            PaymentData paymentData = new PaymentData();

            if (externalAuthenticationResponse.getUser().getPaymentData() != null) {

                // Inserimento del metodo di default in prima posizione
                for (PaymentInfo paymentInfo : externalAuthenticationResponse.getUser().getPaymentData()) {

                    PaymentMethod paymentMethod = null;
                    if (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED
                            || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                        if (paymentInfo.getDefaultMethod().equals(Boolean.TRUE)) {

                            paymentMethod = new PaymentMethod();

                            paymentMethod.setId(paymentInfo.getId());
                            paymentMethod.setType(paymentInfo.getType());
                            paymentMethod.setBrand(paymentInfo.getBrand());
                            paymentMethod.setExpirationDate(CustomDate.createCustomDate(paymentInfo.getExpirationDate()));
                            paymentMethod.setIdentifier(paymentInfo.getPan());
                            paymentMethod.setStatus(paymentInfo.getStatus());
                            paymentMethod.setDefaultMethod(paymentInfo.getDefaultMethod());

                            if (paymentInfo.getInsertTimestamp() != null) {
                                paymentMethod.setInsertDate(CustomTimestamp.createCustomTimestamp(new Timestamp(paymentInfo.getInsertTimestamp().getTime())));
                            }
                            else {
                                paymentMethod.setInsertDate(null);
                            }

                            paymentData.getPaymentMethodList().add(paymentMethod);

                            break;
                        }
                    }
                }

                // Inserimento degli altri metodi di pagamento
                for (PaymentInfo paymentInfo : externalAuthenticationResponse.getUser().getPaymentData()) {

                    PaymentMethod paymentMethod = null;
                    if (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED
                            || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                        if (paymentInfo.getDefaultMethod().equals(Boolean.FALSE)) {

                            paymentMethod = new PaymentMethod();

                            paymentMethod.setId(paymentInfo.getId());
                            paymentMethod.setType(paymentInfo.getType());
                            paymentMethod.setBrand(paymentInfo.getBrand());
                            paymentMethod.setExpirationDate(CustomDate.createCustomDate(paymentInfo.getExpirationDate()));
                            paymentMethod.setIdentifier(paymentInfo.getPan());
                            paymentMethod.setStatus(paymentInfo.getStatus());
                            paymentMethod.setDefaultMethod(paymentInfo.getDefaultMethod());

                            if (paymentInfo.getInsertTimestamp() != null) {
                                paymentMethod.setInsertDate(CustomTimestamp.createCustomTimestamp(new Timestamp(paymentInfo.getInsertTimestamp().getTime())));
                            }
                            else {
                                paymentMethod.setInsertDate(null);
                            }

                            paymentData.getPaymentMethodList().add(paymentMethod);
                        }
                    }
                }
            }

            paymentData.setResidualCap(AmountConverter.toMobile(externalAuthenticationResponse.getUser().getCapEffective()));

            if (externalAuthenticationResponse.getUser().getUseVoucher() == null) {
                paymentData.setUseVoucher(false);
            }
            else {
                paymentData.setUseVoucher(externalAuthenticationResponse.getUser().getUseVoucher());
            }

            double residualVoucher = 0.0;
            Set<Voucher> voucherList = externalAuthenticationResponse.getUser().getVoucherList();

            for (Voucher voucher : voucherList) {
                residualVoucher += voucher.getVoucherBalanceDue().doubleValue();
            }

            paymentData.setResidualVoucher(AmountConverter.toMobile(residualVoucher));

            userData.setPaymentData(paymentData);

            UserStatus userStatus = new UserStatus();
            userStatus.setStatus(externalAuthenticationResponse.getUser().getUserStatus());
            userStatus.setRegistrationCompleted(externalAuthenticationResponse.getUser().getUserStatusRegistrationCompleted());

            if (externalAuthenticationResponse.getUser().getOldUser() != null) {

                System.out.println("oldUser: " + externalAuthenticationResponse.getUser().getOldUser());
                if (externalAuthenticationResponse.getUser().getOldUser() == true) {
                    userStatus.setNewUserType(false);
                    System.out.println("setNewUserType a false");
                }
                else {
                    userStatus.setNewUserType(true);
                    System.out.println("setNewUserType a true");
                }
            }
            else {
                System.out.println("oldUser: null");
            }

            Boolean termsOfServiceAccepted = Boolean.TRUE;
            for (TermsOfService termsOfService : externalAuthenticationResponse.getUser().getPersonalData().getTermsOfServiceData()) {

                if (termsOfService.getValid() == null || !termsOfService.getValid()) {
                    termsOfServiceAccepted = Boolean.FALSE;
                    System.out.println("found termsOfService " + termsOfService.getKeyval() + " no valid");
                    break;
                }
            }
            userStatus.setTermsOfServiceAccepted(termsOfServiceAccepted);

            if (externalAuthenticationResponse.getUser().getDepositCardStepCompleted() != null) {
                userStatus.setDepositCardStepCompleted(externalAuthenticationResponse.getUser().getDepositCardStepCompleted());
            }

            if (externalAuthenticationResponse.getUser().getVirtualizationCompleted() != null) {
                userStatus.setVirtualizationCompleted(externalAuthenticationResponse.getUser().getVirtualizationCompleted());
            }

            Boolean eniStationUserType = externalAuthenticationResponse.getUser().getEniStationUserType();
            if (eniStationUserType == null) {
                userStatus.setEniStationUserType(false);
            }
            else {
                userStatus.setEniStationUserType(eniStationUserType);
            }

            Boolean loyaltyCheckEnabled = externalAuthenticationResponse.getUser().getLoyaltyCheckEnabled();
            if (loyaltyCheckEnabled == null) {
                userStatus.setLoyaltyCheckEnabled(false);
            }
            else {
                userStatus.setLoyaltyCheckEnabled(loyaltyCheckEnabled);
            }

            System.out.println("UserType: " + externalAuthenticationResponse.getUser().getUserType());
            String type = "CUSTOMER";
            if (externalAuthenticationResponse.getUser().getUserType() == User.USER_TYPE_MULTICARD) {
                type = "MULTICARD";
            }
            userStatus.setType(type);

            Boolean updatedMission = externalAuthenticationResponse.getUser().getUpdatedMission();
            if (updatedMission == null) {
                userStatus.setUpdatedMission(false);
            }
            else {
                userStatus.setUpdatedMission(updatedMission);
            }

            // Ricerca transazione di sosta pending
            boolean parkingTransactionPending = false;
            RetrievePendingParkingTransactionResult retrievePendingParkingTransactionResult = getParkingTransactionV2ServiceRemote().retrievePendingParkingTransaction(
                    externalAuthenticationResponse.getTicketId(), externalAuthenticationUserRequest.getBody().getRequestID());
            if (retrievePendingParkingTransactionResult.getStatusCode().equals("RETRIEVE_PENDING_PARKING_TRANSACTION_200")) {

                ParkingTransaction parkingTransaction = retrievePendingParkingTransactionResult.getParkingTransaction();
                if (parkingTransaction != null) {
                    parkingTransactionPending = true;
                }
            }
            userStatus.setParkingTransactionPending(parkingTransactionPending);

            userData.setUserStatus(userStatus);

            String socialProvider = null;
            if (!externalAuthenticationResponse.getUser().getUserSocialData().isEmpty()) {
                for (UserSocialData userSocialData : externalAuthenticationResponse.getUser().getUserSocialData()) {
                    socialProvider = userSocialData.getProvider();
                }
            }

            if (socialProvider != null) {
                SocialData socialData = new SocialData();
                socialData.setProvider(socialProvider);
                userData.getSocialDataList().add(socialData);
            }

            if (!externalAuthenticationResponse.getUser().getPlateNumberList().isEmpty()) {
                for (PlateNumber plateNumber : externalAuthenticationResponse.getUser().getPlateNumberList()) {
                    PlateNumberInfo plateNumberInfo = new PlateNumberInfo();
                    plateNumberInfo.setId(plateNumber.getId());
                    plateNumberInfo.setPlateNumber(plateNumber.getPlateNumber());
                    plateNumberInfo.setDescription(plateNumber.getDescription());
                    plateNumberInfo.setDefaultPlateNumber(plateNumber.getDefaultPlateNumber());
                    userData.getPlateNumberList().add(plateNumberInfo);
                }
            }

            body.setTicketID(externalAuthenticationResponse.getTicketId());
            body.setLoyaltySessionID(null);
            
            String externalUserId = externalAuthenticationResponse.getUser().getExternalUserId();
            userData.setExternalUserId(externalUserId);
            
            body.setUserData(userData);

            socialAuthenticationUserResponseSuccess.setBody(body);

            return socialAuthenticationUserResponseSuccess;
        }


        ExternalAuthenticationUserResponseFailure socialAuthenticationUserResponseFailure = new ExternalAuthenticationUserResponseFailure();

        status.setStatusCode(externalAuthenticationResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(externalAuthenticationResponse.getStatusCode()));

        socialAuthenticationUserResponseFailure.setStatus(status);

        return socialAuthenticationUserResponseFailure;
    }

}
