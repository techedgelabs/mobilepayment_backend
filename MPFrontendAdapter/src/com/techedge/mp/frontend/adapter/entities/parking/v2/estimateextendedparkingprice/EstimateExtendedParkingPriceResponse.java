package com.techedge.mp.frontend.adapter.entities.parking.v2.estimateextendedparkingprice;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class EstimateExtendedParkingPriceResponse extends BaseResponse {
    private EstimateExtendedParkingPriceResponseBody body;

    public EstimateExtendedParkingPriceResponseBody getBody() {
        return body;
    }

    public void setBody(EstimateExtendedParkingPriceResponseBody body) {
        this.body = body;
    }

}
