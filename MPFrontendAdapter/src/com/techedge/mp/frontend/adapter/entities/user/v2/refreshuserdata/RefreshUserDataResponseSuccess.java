package com.techedge.mp.frontend.adapter.entities.user.v2.refreshuserdata;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RefreshUserDataResponseSuccess extends BaseResponse {

	private RefreshUserDataBodyResponse body;

	public RefreshUserDataBodyResponse getBody() {
		return body;
	}

	public void setBody(RefreshUserDataBodyResponse body) {
		this.body = body;
	}
}
