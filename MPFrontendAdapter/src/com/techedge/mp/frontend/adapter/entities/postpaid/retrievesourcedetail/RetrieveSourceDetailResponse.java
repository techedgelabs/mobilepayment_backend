package com.techedge.mp.frontend.adapter.entities.postpaid.retrievesourcedetail;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class RetrieveSourceDetailResponse extends BaseResponse {
	
	private RetrieveSourceDetailBodyResponse body;

	public RetrieveSourceDetailBodyResponse getBody() {
		return body;
	}

	public void setBody(RetrieveSourceDetailBodyResponse body) {
		this.body = body;
	}


}
