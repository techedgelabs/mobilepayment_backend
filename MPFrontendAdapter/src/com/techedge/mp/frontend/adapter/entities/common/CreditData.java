package com.techedge.mp.frontend.adapter.entities.common;

public class CreditData {
	
	
	private String voucher;

	public CreditData(){}
	
	public String getVoucher() {
		return voucher;
	}

	public void setVoucher(String voucher) {
		this.voucher = voucher;
	}
	
}
