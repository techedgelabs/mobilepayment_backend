package com.techedge.mp.frontend.adapter.entities.user.v2.getreceipt;

import java.io.Serializable;

public class GetReceiptResponseBody implements Serializable {

  
    /**
     * 
     */
    private static final long serialVersionUID = 1509487747936242043L;
    
    private String documentBase64;
    private String filename;
    private int dimension;
    
    public String getDocumentBase64() {
        return documentBase64;
    }
    public void setDocumentBase64(String documentBase64) {
        this.documentBase64 = documentBase64;
    }
    public String getFilename() {
        return filename;
    }
    public void setFilename(String filename) {
        this.filename = filename;
    }
    public int getDimension() {
        return dimension;
    }
    public void setDimension(int dimension) {
        this.dimension = dimension;
    }


}
