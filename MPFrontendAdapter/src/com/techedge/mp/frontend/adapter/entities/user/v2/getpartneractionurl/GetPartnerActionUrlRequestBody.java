package com.techedge.mp.frontend.adapter.entities.user.v2.getpartneractionurl;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class GetPartnerActionUrlRequestBody implements Validable {

    private String partnerId;


    public String getPartnerId() {
        return partnerId;
    }


    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }


    @Override
    public Status check() {

        Status status = new Status();

        if (partnerId == null || partnerId.trim().equalsIgnoreCase("")) {

            status.setStatusCode(StatusCode.USER_V2_GETPARTNER_ACTION_URL_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_V2_GETPARTNER_ACTION_URL_SUCCESS);

        return status;
    }

}
