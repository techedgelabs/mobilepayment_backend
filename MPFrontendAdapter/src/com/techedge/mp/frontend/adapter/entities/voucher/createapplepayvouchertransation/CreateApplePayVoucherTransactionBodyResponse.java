package com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation;

public class CreateApplePayVoucherTransactionBodyResponse {

    private String voucherTransactionID;
    private String voucherCode;

    public String getVoucherTransactionID() {

        return voucherTransactionID;
    }

    public void setVoucherTransactionID(String voucherTransactionID) {

        this.voucherTransactionID = voucherTransactionID;
    }

    public String getVoucherCode() {

        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {

        this.voucherCode = voucherCode;
    }
}
