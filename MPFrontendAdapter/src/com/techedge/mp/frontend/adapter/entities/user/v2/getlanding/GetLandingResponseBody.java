package com.techedge.mp.frontend.adapter.entities.user.v2.getlanding;

import java.io.Serializable;

import com.techedge.mp.core.business.interfaces.user.UserCategory;

public class GetLandingResponseBody implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6277018799357256280L;

    private String            title;
    private String            url;
    private Boolean           showAlways;
    private Integer            status;
    private UserCategory      userCategory;
    private String            sectionID;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean getShowAlways() {
        return showAlways;
    }

    public void setShowAlways(Boolean showAlways) {
        this.showAlways = showAlways;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public UserCategory getUserCategory() {
        return userCategory;
    }

    public void setUserCategory(UserCategory userCategory) {
        this.userCategory = userCategory;
    }

    public String getSectionID() {
        return sectionID;
    }

    public void setSectionID(String sectionID) {
        this.sectionID = sectionID;
    }

}
