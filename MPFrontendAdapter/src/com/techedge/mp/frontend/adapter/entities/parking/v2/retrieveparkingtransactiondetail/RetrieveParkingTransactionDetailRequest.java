package com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingtransactiondetail;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransaction;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionNonOil;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionRefuel;
import com.techedge.mp.core.business.interfaces.loyalty.RewardTransactionParameter;
import com.techedge.mp.core.business.interfaces.parking.GetParkingTransactionDetailResult;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransaction;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransactionItem;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransactionItemEvent;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.FrontendParameter;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptDynamic;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetail;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetailSection;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetailTransaction;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveParkingTransactionDetailRequest extends AbstractRequest implements Validable {

    private Status                               status = new Status();

    private Credential                           credential;
    private RetrieveParkingTransactionDetailBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveParkingTransactionDetailBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveParkingTransactionDetailBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DETAIL-PARKING-TRANSACTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;
            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
            return status;
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        
        RetrieveParkingTransactionDetailRequest retrieveParkingTransactionDetailRequest = this;

        RetrieveParkingTransactionDetailResponse retrieveParkingTransactionDetailResponse = new RetrieveParkingTransactionDetailResponse();
        RetrieveParkingTransactionDetailBodyResponse retrieveParkingTransactionDetailBodyResponse = new RetrieveParkingTransactionDetailBodyResponse();

        GetParkingTransactionDetailResult getParkingTransactionDetailResult = getParkingTransactionV2ServiceRemote().getParkingTransactionDetail(
                retrieveParkingTransactionDetailRequest.getCredential().getTicketID(), retrieveParkingTransactionDetailRequest.getCredential().getRequestID(),
                retrieveParkingTransactionDetailRequest.getBody().getTransactionID());

        status.setStatusCode(getParkingTransactionDetailResult.getStatusCode());
        //status.setStatusCode(StatusCode.RETRIEVE_EVENT_NOTIFICATION_TRANSACTION_DETAIL_SUCCESS);
        //status.setStatusMessage("STATUS MESSAGE TEST");
        status.setStatusMessage(prop.getProperty(status.getStatusCode()));
        retrieveParkingTransactionDetailResponse.setStatus(status);

        if (!status.getStatusCode().contains("200")) {
            retrieveParkingTransactionDetailResponse.setStatus(status);
            return retrieveParkingTransactionDetailResponse;
        }
        
        String iconCardUrl = "";

        try {
            iconCardUrl = getParameterServiceRemote().getParamValue(FrontendParameter.PARAM_RECEIPT_ICON_CARD_URL);
        }
        catch (ParameterNotFoundException ex) {
            getLoggerServiceRemote().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "PARAMETER NOT FOUND: " + ex.getMessage());
        }
        
        ParkingTransaction parkingTransaction = getParkingTransactionDetailResult.getParkingTransaction();

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");

        retrieveParkingTransactionDetailBodyResponse.setSourceStatus("SourceStatus");
        retrieveParkingTransactionDetailBodyResponse.setSourceType("SourceType");

        Integer amount = AmountConverter.toMobile(parkingTransaction.getFinalPrice());

        ReceiptTransactionDetailTransaction transaction = new ReceiptTransactionDetailTransaction();
        transaction.setAmount(amount);
        transaction.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(new Date().getTime())));

        ReceiptTransactionDetail receipt = new ReceiptTransactionDetail();

        ReceiptTransactionDetailSection sectionParking = receipt.createSection("Sosta", "RECEIPT", null, false);
        //ReceiptTransactionDetailSection sectionPromo = receipt.createSection("Messaggio Promozionale", "PROMO", null, true);
        //ReceiptTransactionDetailSection sectionLoyalty = receipt.createSection("Punti Loyalty", "RECEIPT", null, false);
        //ReceiptTransactionDetailSection sectionReward = receipt.createSection("Premio", "RECEIPT", null, false);

        int sectionPositionParking = 1;
        int sectionPositionPromo = 1;
        int sectionPositionLoyalty = 1;
        int sectionPositionReward = 1;

        // SECTION PARKING

        // ParkingStartTime del primo item
        Date parkingStartTime = null;
        
        // ParkingEndTime dell'ultimo item
        Date parkingEndTime = parkingTransaction.getFinalParkingEndTime();
        
        ArrayList<ParkingTransactionItem> parkingTransactionItemList = Collections.list(Collections.enumeration(parkingTransaction.getParkingTransactionItemList()));
        
        Collections.sort(parkingTransactionItemList, new Comparator<ParkingTransactionItem>() {
            @Override
            public int compare(ParkingTransactionItem parkingTransactionItem1, ParkingTransactionItem parkingTransactionItem2) {
                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                return parkingTransactionItem1.getCreationTimestamp().compareTo(parkingTransactionItem2.getCreationTimestamp());
            }
        });
        
        for(ParkingTransactionItem parkingTransactionItem : parkingTransactionItemList) {
            
            if (parkingTransactionItem.getParkingItemStatus().equals("SETTLED")) {
                
                if (parkingStartTime == null) {
                    parkingStartTime = parkingTransactionItem.getParkingStartTime();
                    break;
                }
            }
        }
        
        if (parkingStartTime == null) {
            
            for(ParkingTransactionItem parkingTransactionItem : parkingTransactionItemList) {
                
                if (parkingTransactionItem.getParkingItemStatus().equals("AUTHORIZED")) {
                    
                    parkingStartTime = parkingTransactionItem.getParkingStartTime();
                    break;
                }
            }
        }
        
        if (parkingStartTime == null) {
            
            for(ParkingTransactionItem parkingTransactionItem : parkingTransactionItemList) {
                
                if (parkingTransactionItem.getParkingItemStatus().equals("CANCELED")) {
                    
                    parkingStartTime = parkingTransactionItem.getParkingStartTime();
                    break;
                }
            }
        }
        
        transaction.setTransactionID(parkingTransaction.getParkingTransactionId());

        transaction.setStatusTitle(null);        //TBD
        transaction.setStatusDescription(null); //TBD

        // Trascodifica dello stato
        transaction.setStatus(null);            //TBD

        transaction.setSubStatus(null);         //TBD

        retrieveParkingTransactionDetailBodyResponse.setTransaction(transaction);
        
        String zoneId      = parkingTransaction.getParkingZoneId();
        String zoneName    = parkingTransaction.getParkingZoneName();
        String cityName    = parkingTransaction.getCityName();
        String plateNumber = parkingTransaction.getPlateNumber();

        String startTimestamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(parkingStartTime);
        String endTimestamp   = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(parkingEndTime);
        //receipt.addDataToSection(sectionParking, "ZONE_ID", "CODICE ZONA", null, zoneId, sectionPositionParking++, null);
        receipt.addDataToSection(sectionParking, "CITY_NAME", "COMUNE", null, cityName, sectionPositionParking++, null);
        receipt.addDataToSection(sectionParking, "ZONE_NAME", "AREA DI SOSTA", null, zoneName, sectionPositionParking++, null);
        receipt.addDataToSection(sectionParking, "PLATE_NUMBER", "TARGA VEICOLO", null, plateNumber, sectionPositionParking++, null);
        receipt.addDataToSection(sectionParking, "DATE_START", "INIZIO SOSTA", null, startTimestamp, sectionPositionParking++, null);
        receipt.addDataToSection(sectionParking, "DATE_END", "FINE SOSTA", null, endTimestamp, sectionPositionParking++, null);
        receipt.addDataToSection(sectionParking, "PARKING_ID", "CODICE SOSTA", null, parkingTransaction.getParkingId(), sectionPositionParking++, null);

        /*
        // START SECTION LOYALTY
        String panCode = null;
        String credits = "0";
        String balanceAmount = "0";

        if (eventNotificationResponse.getPanCode() != null) {
            panCode = eventNotificationResponse.getPanCode();
        }

        if (eventNotificationResponse.getCredits() != null) {
            credits = eventNotificationResponse.getCredits().toString();
        }

        if (eventNotificationResponse.getBalance() != null) {
            balanceAmount = eventNotificationResponse.getBalance().toString();
        }

        if (panCode != null) {
            receipt.addDataToSection(sectionLoyalty, "LOYALTY_PAN", "CODICE", null, eventNotificationResponse.getPanCode(), sectionPositionLoyalty++, null);
        }

        receipt.addDataToSection(sectionLoyalty, "LOYALTY_CREDITS", "PUNTI FEDELTA'", null, credits, sectionPositionLoyalty++, null);
        receipt.addDataToSection(sectionLoyalty, "LOYALTY_BALANCE", "SALDO PUNTI", null, balanceAmount, sectionPositionLoyalty++, null);

        if (eventNotificationResponse.getMarketingMsg() != null) {
            String marketingMsg = eventNotificationResponse.getMarketingMsg().replaceAll("[\\s\\t\\n\\r ]{2,}", " ");
            receipt.addDataToSection(sectionPromo, "LOYALTY_MSG", "", null, marketingMsg, sectionPositionPromo++, null);
        }

        if (eventNotificationResponse.getWarningMsg() != null) {
            String warningMsg = eventNotificationResponse.getWarningMsg().replaceAll("[\\s\\t\\n\\r ]{2,}", " ");
            receipt.addDataToSection(sectionLoyalty, "LOYALTY_MSG", "", null, warningMsg, sectionPositionLoyalty++, null);
        }

        // END SECTION LOYALTY
        */
        
        for(ParkingTransactionItem parkingTransactionItem : parkingTransactionItemList) {

            if (parkingTransactionItem.getParkingItemStatus().equals("SETTLED")) {
                
                double bankAmount = parkingTransactionItem.getPrice().doubleValue();
                
                if ((parkingTransactionItem.getPaymentMethodType() == null || parkingTransactionItem.getPaymentMethodType().equals(
                        PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD))
                        && bankAmount > 0.0) {
                    
                    ReceiptTransactionDetailSection sectionParkingPaymentDetail = receipt.createSection("Pagamento Carta", "RECEIPT", null, true);
                    
                    String bankAmountString     = "";
                    
                    for(ParkingTransactionItemEvent parkingTransactionItemEvent : parkingTransactionItem.getParkingTransactionItemEventList()) {
                        System.out.println("Trovato evento " + parkingTransactionItemEvent.getEventType());
                        if(parkingTransactionItemEvent.getEventType().equals("MOV")) {
                            bankAmountString = currencyFormat.format(parkingTransactionItemEvent.getEventAmount());
                        }
                    }
                    
                    sectionPositionParking = 1;
                    
                    receipt.addDataToSection(sectionParkingPaymentDetail, "AMOUNT", "IMPORTO ADDEBITATO", "�", bankAmountString, sectionPositionParking++, null);
                    receipt.addDataToSection(sectionParkingPaymentDetail, "PAN", "CARTA DI PAGAMENTO", null, parkingTransactionItem.getPan(), sectionPositionParking++, null);
    
                    receipt.addDataToSection(sectionParkingPaymentDetail, "N_OP", "N OPERAZIONE", null, parkingTransactionItem.getBankTansactionID(), sectionPositionParking++, null);
                    receipt.addDataToSection(sectionParkingPaymentDetail, "C_AUTH", "COD AUTORIZZAZIONE", null, parkingTransactionItem.getAuthorizationCode(),
                            sectionPositionParking++, null);
                    
                }
            }
        }

        /*
        // START REWARD
        if (eventNotificationResponse.getRewardTransactionDetail() != null) {
            for (RewardTransactionParameter rewardParameter : eventNotificationResponse.getRewardTransactionDetail().getParameterDetails()) {
                String label = rewardParameter.getParameterID().toUpperCase();
                String key = rewardParameter.getParameterID().toUpperCase().replaceAll(" ", "_");

                if (label.equals("TESTO DA DWH")) {
                    label = null;
                    continue;
                }

                receipt.addDataToSection(sectionReward, key, label, null, rewardParameter.getParameterValue(), sectionPositionReward++, null);
            }
        }
        else {
            receipt.removeSection(sectionReward);
        }
        // END REWARD
        */
        

        retrieveParkingTransactionDetailBodyResponse.setReceipt(receipt);
        
        retrieveParkingTransactionDetailResponse.setBody(retrieveParkingTransactionDetailBodyResponse);
        retrieveParkingTransactionDetailResponse.setStatus(status);

        return retrieveParkingTransactionDetailResponse;

    }
}