package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbyqrcode;

public class RetrievePoPTransactionByQRcodeBodyResponse {
	
	private String mpTransaction;

	public String getMpTransaction() {
		return mpTransaction;
	}

	public void setMpTransaction(String mpTransaction) {
		this.mpTransaction = mpTransaction;
	}

}
