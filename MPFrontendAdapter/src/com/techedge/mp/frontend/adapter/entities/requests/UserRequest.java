package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.user.authentication.AuthenticationUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.cancelmobilephone.CancelMobilePhoneRequest;
import com.techedge.mp.frontend.adapter.entities.user.checkavailabilityamount.CheckAvailabilityAmountRequest;
import com.techedge.mp.frontend.adapter.entities.user.create.CreateUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.getactivevouchers.GetActiveVouchersRequest;
import com.techedge.mp.frontend.adapter.entities.user.getavailableloyaltycards.GetAvailableLoyaltyCardsRequest;
import com.techedge.mp.frontend.adapter.entities.user.inforedemption.InfoRedemptionRequest;
import com.techedge.mp.frontend.adapter.entities.user.insertmulticardpaymentmethod.InsertMulticardPaymentMethodRequest;
import com.techedge.mp.frontend.adapter.entities.user.insertpaymentmethod.InsertPaymentMethodRequest;
import com.techedge.mp.frontend.adapter.entities.user.loadvoucher.LoadVoucherRequest;
import com.techedge.mp.frontend.adapter.entities.user.logout.LogoutUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.recoverusername.RecoverUsernameRequest;
import com.techedge.mp.frontend.adapter.entities.user.redemption.RedemptionRequest;
import com.techedge.mp.frontend.adapter.entities.user.removeactivedevice.RemoveActiveDeviceRequest;
import com.techedge.mp.frontend.adapter.entities.user.removepaymentmethod.RemovePaymentMethodRequest;
import com.techedge.mp.frontend.adapter.entities.user.removevoucher.RemoveVoucherRequest;
import com.techedge.mp.frontend.adapter.entities.user.rescuepassword.RescuePasswordRequest;
import com.techedge.mp.frontend.adapter.entities.user.resendverificationcode.ResendValidationRequest;
import com.techedge.mp.frontend.adapter.entities.user.resetpin.ResetPinRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrieveactivedevice.RetrieveActiveDeviceRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrievecities.RetrieveCitiesRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrievedocument.RetrieveDocumentRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrievepaymentdata.RetrievePaymentDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrieveprefixes.RetrievePrefixesRequest;
import com.techedge.mp.frontend.adapter.entities.user.retrievetermsofservice.RetrieveTermsOfServiceRequest;
import com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard.SetDefaultLoyaltyCardRequest;
import com.techedge.mp.frontend.adapter.entities.user.setdefaultpaymentmethod.SetDefaultPaymentMethodRequest;
import com.techedge.mp.frontend.adapter.entities.user.setusevoucher.SetUseVoucherRequest;
import com.techedge.mp.frontend.adapter.entities.user.skippaymentmethodconfiguration.SkipPaymentMethodConfigurationRequest;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserPersonalDataRequest;
import com.techedge.mp.frontend.adapter.entities.user.update.UpdateUserRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatemobilephone.UpdateMobilePhoneRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatepassword.UpdatePasswordRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatepin.UpdatePinRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatesmslog.UpdateSmsLogRequest;
import com.techedge.mp.frontend.adapter.entities.user.updatetermsofservice.UpdateTermsOfServiceRequest;
import com.techedge.mp.frontend.adapter.entities.user.validatefield.ValidateFieldRequest;
import com.techedge.mp.frontend.adapter.entities.user.validatepaymentmethod.ValidatePaymentMethodRequest;

public class UserRequest extends RootRequest {

    protected CreateUserRequest                     createUser;
    protected UpdateUserRequest                     updateUser;
    protected UpdateUserPersonalDataRequest         updateUserPersonalData;
    protected AuthenticationUserRequest             authentication;
    protected LogoutUserRequest                     logout;
    protected UpdatePasswordRequest                 updatePassword;
    protected UpdatePinRequest                      updatePin;
    protected ValidateFieldRequest                  validateField;
    protected ValidatePaymentMethodRequest          validatePaymentMethod;
    protected InsertPaymentMethodRequest            insertPaymentMethod;
    protected RetrievePaymentDataRequest            retrievePaymentData;
    protected RetrieveCitiesRequest                 retrieveCities;
    protected SetDefaultPaymentMethodRequest        setDefaultPaymentMethod;
    protected RemovePaymentMethodRequest            removePaymentMethod;
    protected RescuePasswordRequest                 rescuePasswordMethod;
    protected RecoverUsernameRequest                recoverUsername;
    protected RetrieveTermsOfServiceRequest         retrieveTermsOfService;
    protected RetrieveDocumentRequest               retrieveDocument;
    protected LoadVoucherRequest                    loadVoucher;
    protected GetActiveVouchersRequest              getActiveVouchers;
    protected SetDefaultLoyaltyCardRequest          setDefaultLoyaltyCard;
    protected GetAvailableLoyaltyCardsRequest       getAvailableLoyaltyCards;
    protected SetUseVoucherRequest                  setUseVoucher;
    protected RemoveVoucherRequest                  removeVoucher;
    protected ResetPinRequest                       resetPin;
    protected SkipPaymentMethodConfigurationRequest skipPaymentMethodConfiguration;
    protected RetrievePrefixesRequest               retrievePrefixNumber;
    protected CancelMobilePhoneRequest              cancelMobilePhoneUpdate;
    protected UpdateMobilePhoneRequest              updateMobilePhone;
    protected ResendValidationRequest               resendVerificationCode;
    protected CheckAvailabilityAmountRequest        checkAvailabilityAmount;
    protected UpdateTermsOfServiceRequest           updateTermsOfService;
    protected UpdateSmsLogRequest                   updateSmsLog;
    protected InfoRedemptionRequest                 infoRedemption;
    protected RedemptionRequest                     redemption;
    protected RetrieveActiveDeviceRequest           retrieveActiveDevice;
    protected RemoveActiveDeviceRequest             removeActiveUserDevice;
    protected InsertMulticardPaymentMethodRequest   insertMulticard;

    public CreateUserRequest getCreateUser() {
        return createUser;
    }

    public void setCreateUser(CreateUserRequest createUser) {
        this.createUser = createUser;
    }

    public UpdateUserRequest getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(UpdateUserRequest updateUser) {
        this.updateUser = updateUser;
    }

    public UpdateUserPersonalDataRequest getUpdateUserPersonalData() {
        return updateUserPersonalData;
    }

    public void setUpdateUserPersonalData(UpdateUserPersonalDataRequest updateUserPersonalData) {
        this.updateUserPersonalData = updateUserPersonalData;
    }

    public AuthenticationUserRequest getAuthentication() {
        return authentication;
    }

    public void setAuthentication(AuthenticationUserRequest authentication) {
        this.authentication = authentication;
    }

    public LogoutUserRequest getLogout() {
        return logout;
    }

    public void setLogout(LogoutUserRequest logout) {
        this.logout = logout;
    }

    public ValidateFieldRequest getValidateField() {
        return validateField;
    }

    public void setValidateField(ValidateFieldRequest validateField) {
        this.validateField = validateField;
    }

    public ValidatePaymentMethodRequest getValidatePaymentMethod() {
        return validatePaymentMethod;
    }

    public void setValidatePaymentMethod(ValidatePaymentMethodRequest validatePaymentMethod) {
        this.validatePaymentMethod = validatePaymentMethod;
    }

    public UpdatePasswordRequest getUpdatePassword() {
        return updatePassword;
    }

    public void setUpdatePassword(UpdatePasswordRequest updatePassword) {
        this.updatePassword = updatePassword;
    }

    public UpdatePinRequest getUpdatePin() {
        return updatePin;
    }

    public void setUpdatePin(UpdatePinRequest updatePin) {
        this.updatePin = updatePin;
    }

    public InsertPaymentMethodRequest getInsertPaymentMethod() {
        return insertPaymentMethod;
    }

    public void setInsertPaymentMethod(InsertPaymentMethodRequest insertPaymentMethod) {
        this.insertPaymentMethod = insertPaymentMethod;
    }

    public RetrievePaymentDataRequest getRetrievePaymentData() {
        return retrievePaymentData;
    }

    public void setRetrievePaymentData(RetrievePaymentDataRequest retrievePaymentData) {
        this.retrievePaymentData = retrievePaymentData;
    }

    public RetrieveCitiesRequest getRetrieveCities() {
        return retrieveCities;
    }

    public void setRetrieveCities(RetrieveCitiesRequest retrieveCities) {
        this.retrieveCities = retrieveCities;
    }

    public SetDefaultPaymentMethodRequest getSetDefaultPaymentMethod() {
        return setDefaultPaymentMethod;
    }

    public void setSetDefaultPaymentMethod(SetDefaultPaymentMethodRequest setDefaultPaymentMethod) {
        this.setDefaultPaymentMethod = setDefaultPaymentMethod;
    }

    public RemovePaymentMethodRequest getRemovePaymentMethod() {
        return removePaymentMethod;
    }

    public void setRemovePaymentMethod(RemovePaymentMethodRequest removePaymentMethod) {
        this.removePaymentMethod = removePaymentMethod;
    }

    public RescuePasswordRequest getRescuePasswordMethod() {
        return rescuePasswordMethod;
    }

    public void setRescuePasswordMethod(RescuePasswordRequest rescuePasswordMethod) {
        this.rescuePasswordMethod = rescuePasswordMethod;
    }

    public RecoverUsernameRequest getRecoverUsername() {
        return recoverUsername;
    }

    public void setRecoverUsername(RecoverUsernameRequest recoverUsername) {
        this.recoverUsername = recoverUsername;
    }

    public RetrieveTermsOfServiceRequest getRetrieveTermsOfService() {
        return retrieveTermsOfService;
    }

    public void setRetrieveTermsOfService(RetrieveTermsOfServiceRequest retrieveTermsOfService) {
        this.retrieveTermsOfService = retrieveTermsOfService;
    }

    public RetrieveDocumentRequest getRetrieveDocument() {
        return retrieveDocument;
    }

    public void setRetrieveDocument(RetrieveDocumentRequest retrieveDocument) {
        this.retrieveDocument = retrieveDocument;
    }

    public LoadVoucherRequest getLoadVoucher() {
        return loadVoucher;
    }

    public void setLoadVoucher(LoadVoucherRequest loadVoucher) {
        this.loadVoucher = loadVoucher;
    }

    public GetActiveVouchersRequest getGetActiveVouchers() {
        return getActiveVouchers;
    }

    public void setGetActiveVouchers(GetActiveVouchersRequest getActiveVouchers) {
        this.getActiveVouchers = getActiveVouchers;
    }

    public SetDefaultLoyaltyCardRequest getSetDefaultLoyaltyCard() {
        return setDefaultLoyaltyCard;
    }

    public void setSetDefaultLoyaltyCard(SetDefaultLoyaltyCardRequest setDefaultLoyaltyCard) {
        this.setDefaultLoyaltyCard = setDefaultLoyaltyCard;
    }

    public GetAvailableLoyaltyCardsRequest getGetAvailableLoyaltyCards() {
        return getAvailableLoyaltyCards;
    }

    public void setGetAvailableLoyaltyCards(GetAvailableLoyaltyCardsRequest getAvailableLoyaltyCards) {
        this.getAvailableLoyaltyCards = getAvailableLoyaltyCards;
    }

    public SetUseVoucherRequest getSetUseVoucher() {
        return setUseVoucher;
    }

    public void setSetUseVoucher(SetUseVoucherRequest setUseVoucher) {
        this.setUseVoucher = setUseVoucher;
    }

    public RemoveVoucherRequest getRemoveVoucher() {
        return removeVoucher;
    }

    public void setRemoveVoucher(RemoveVoucherRequest removeVoucher) {
        this.removeVoucher = removeVoucher;
    }

    public ResetPinRequest getResetPin() {
        return resetPin;
    }

    public void setResetPin(ResetPinRequest resetPin) {
        this.resetPin = resetPin;
    }

    public SkipPaymentMethodConfigurationRequest getSkipPaymentMethodConfiguration() {
        return skipPaymentMethodConfiguration;
    }

    public void setSkipPaymentMethodConfiguration(SkipPaymentMethodConfigurationRequest skipPaymentMethodConfiguration) {
        this.skipPaymentMethodConfiguration = skipPaymentMethodConfiguration;
    }

    public RetrievePrefixesRequest getRetrievePrefixNumber() {
        return retrievePrefixNumber;
    }

    public void setRetrievePrefixNumber(RetrievePrefixesRequest retrievePrefixNumber) {
        this.retrievePrefixNumber = retrievePrefixNumber;
    }

    public CancelMobilePhoneRequest getCancelMobilePhoneUpdate() {
        return cancelMobilePhoneUpdate;
    }

    public void setCancelMobilePhoneUpdate(CancelMobilePhoneRequest cancelMobilePhoneUpdate) {
        this.cancelMobilePhoneUpdate = cancelMobilePhoneUpdate;
    }

    public UpdateMobilePhoneRequest getUpdateMobilePhone() {
        return updateMobilePhone;
    }

    public void setUpdateMobilePhone(UpdateMobilePhoneRequest updateMobilePhone) {
        this.updateMobilePhone = updateMobilePhone;
    }

    public ResendValidationRequest getResendVerificationCode() {
        return resendVerificationCode;
    }

    public void setResendVerificationCode(ResendValidationRequest resendVerificationCode) {
        this.resendVerificationCode = resendVerificationCode;
    }

    public CheckAvailabilityAmountRequest getCheckAvailabilityAmount() {
        return checkAvailabilityAmount;
    }

    public void setCheckAvailabilityAmount(CheckAvailabilityAmountRequest checkAvailabilityAmount) {
        this.checkAvailabilityAmount = checkAvailabilityAmount;
    }

    public UpdateTermsOfServiceRequest getUpdateTermsOfService() {
        return updateTermsOfService;
    }

    public void setUpdateTermsOfService(UpdateTermsOfServiceRequest updateTermsOfService) {
        this.updateTermsOfService = updateTermsOfService;
    }

    public UpdateSmsLogRequest getUpdateSmsLog() {
        return updateSmsLog;
    }

    public void setUpdateSmsLog(UpdateSmsLogRequest updateSmsLog) {
        this.updateSmsLog = updateSmsLog;
    }

    public InfoRedemptionRequest getInfoRedemption() {
        return infoRedemption;
    }

    public void setInfoRedemption(InfoRedemptionRequest infoRedemption) {
        this.infoRedemption = infoRedemption;
    }

    public RedemptionRequest getRedemption() {
        return redemption;
    }

    public void setRedemption(RedemptionRequest redemption) {
        this.redemption = redemption;
    }

    public RetrieveActiveDeviceRequest getRetrieveActiveDevice() {
        return retrieveActiveDevice;
    }

    public void setRetrieveActiveDevice(RetrieveActiveDeviceRequest retrieveActiveDevice) {
        this.retrieveActiveDevice = retrieveActiveDevice;
    }

    public RemoveActiveDeviceRequest getRemoveActiveUserDevice() {
        return removeActiveUserDevice;
    }

    public void setRemoveActiveUserDevice(RemoveActiveDeviceRequest removeActiveUserDevice) {
        this.removeActiveUserDevice = removeActiveUserDevice;
    }

	public InsertMulticardPaymentMethodRequest getInsertMulticard() {
		return insertMulticard;
	}

	public void setInsertMulticard(
			InsertMulticardPaymentMethodRequest insertMulticard) {
		this.insertMulticard = insertMulticard;
	}

}
