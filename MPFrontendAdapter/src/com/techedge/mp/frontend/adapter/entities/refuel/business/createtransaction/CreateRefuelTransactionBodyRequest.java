package com.techedge.mp.frontend.adapter.entities.refuel.business.createtransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateRefuelTransactionBodyRequest implements Validable {

	
	private CreateRefuelTransactionDataRequest refuelPaymentData;

	
	public CreateRefuelTransactionDataRequest getRefuelPaymentData() {
		return refuelPaymentData;
	}
	public void setRefuelPaymentData(CreateRefuelTransactionDataRequest refuelPaymentData) {
		this.refuelPaymentData = refuelPaymentData;
	}

	@Override
	public Status check() {

		Status status = new Status();
		
		if(this.refuelPaymentData != null) {
			
			status = this.refuelPaymentData.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);
		
		return status;
		
	}
	
}
