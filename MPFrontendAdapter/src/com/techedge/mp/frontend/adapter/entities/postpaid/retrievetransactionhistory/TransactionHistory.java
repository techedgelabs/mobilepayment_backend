package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory;

import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;

public class TransactionHistory {

  private String sourceType;
  private String sourceStatus;
  private PostPaidTransactionHistoryData transaction;
  private Station station;


  public String getSourceType() {
    return sourceType;
  }

  public void setSourceType(String sourceType) {
    this.sourceType = sourceType;
  }

  public String getSourceStatus() {
    return sourceStatus;
  }

  public void setSourceStatus(String sourceStatus) {
    this.sourceStatus = sourceStatus;
  }

  public PostPaidTransactionHistoryData getTransaction() {
    return transaction;
  }

  public void setTransaction(PostPaidTransactionHistoryData transaction) {
    this.transaction = transaction;
  }

  public Station getStation() {
    return station;
  }

  public void setStation(Station station) {
    this.station = station;
  }  
}
