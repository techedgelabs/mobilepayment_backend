package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbyqrcode;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrievePoPTransactionByQRcodeBodyRequest implements Validable {
	
	private String code;

	public String getQRCode() {
		return code;
	}

	public void setQRCode(String qrCode) {
		this.code = qrCode;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.code == null || this.code.length() != 32 || this.code.trim() == "") {
			
			status.setStatusCode(StatusCode.POP_GET_ID_WRONG);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.POP_GET_SUCCESS);
		
		return status;
	}
	
}