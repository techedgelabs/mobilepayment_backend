package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class RetrieveTransactionHistoryResponse extends BaseResponse {

	
	private RetrieveTransactionHistoryBodyResponse body;

	
	public RetrieveTransactionHistoryBodyResponse getBody() {
		return body;
	}

	public void setBody(RetrieveTransactionHistoryBodyResponse body) {
		this.body = body;
	}

	
}
