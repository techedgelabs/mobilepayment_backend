package com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymenthistory;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class RetrieveRefuelPaymentHistoryResponse extends BaseResponse {

	
	private RetrieveRefuelPaymentHistoryBodyResponse body;

	
	public RetrieveRefuelPaymentHistoryBodyResponse getBody() {
		return body;
	}

	public void setBody(RetrieveRefuelPaymentHistoryBodyResponse body) {
		this.body = body;
	}

	
}
