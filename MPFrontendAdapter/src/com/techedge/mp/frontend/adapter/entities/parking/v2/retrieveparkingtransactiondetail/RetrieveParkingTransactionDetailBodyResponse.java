package com.techedge.mp.frontend.adapter.entities.parking.v2.retrieveparkingtransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetail;
import com.techedge.mp.frontend.adapter.entities.common.ReceiptTransactionDetailTransaction;

public class RetrieveParkingTransactionDetailBodyResponse {

    private String                              sourceType;
    private String                              sourceStatus;
    private ReceiptTransactionDetail            receipt;
    private ReceiptTransactionDetailTransaction transaction;
    private String                              surveyUrl;

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getSourceStatus() {
        return sourceStatus;
    }

    public void setSourceStatus(String sourceStatus) {
        this.sourceStatus = sourceStatus;
    }

    public ReceiptTransactionDetail getReceipt() {
        return receipt;
    }

    public void setReceipt(ReceiptTransactionDetail receipt) {
        this.receipt = receipt;
    }

    public ReceiptTransactionDetailTransaction getTransaction() {
        return transaction;
    }

    public void setTransaction(ReceiptTransactionDetailTransaction transaction) {
        this.transaction = transaction;
    }

    public String getSurveyUrl() {
        return surveyUrl;
    }

    public void setSurveyUrl(String surveyUrl) {
        this.surveyUrl = surveyUrl;
    }

}