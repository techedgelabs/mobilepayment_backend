package com.techedge.mp.frontend.adapter.entities.postpaid.v2.createtransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateTransactionBodyRequest implements Validable {

    private String                              sourceID;

    @Override
    public Status check() {

        Status status = new Status();

        if (this.sourceID == null || this.sourceID.trim() == "") {

            status.setStatusCode(StatusCode.POSTPAID_V2_CREATE_TRANSACTION_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.POSTPAID_V2_CREATE_TRANSACTION_SUCCESS);

        return status;
    }

    public String getSourceID() {
        return sourceID;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

}