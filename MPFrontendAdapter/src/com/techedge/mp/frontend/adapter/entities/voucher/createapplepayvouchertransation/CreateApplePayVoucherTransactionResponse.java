package com.techedge.mp.frontend.adapter.entities.voucher.createapplepayvouchertransation;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class CreateApplePayVoucherTransactionResponse extends BaseResponse {

    private CreateApplePayVoucherTransactionBodyResponse body;

    public CreateApplePayVoucherTransactionBodyResponse getBody() {
        return body;
    }

    public void setBody(CreateApplePayVoucherTransactionBodyResponse body) {
        this.body = body;
    }

}
