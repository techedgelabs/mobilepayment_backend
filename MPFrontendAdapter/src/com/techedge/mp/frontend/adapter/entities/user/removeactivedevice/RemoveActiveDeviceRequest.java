package com.techedge.mp.frontend.adapter.entities.user.removeactivedevice;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RemoveActiveDeviceRequest extends AbstractRequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private RemoveActiveDeviceBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RemoveActiveDeviceBodyRequest getBody() {
        return body;
    }

    public void setBody(RemoveActiveDeviceBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REMOVE-ACTIVE-DEVICE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        RemoveActiveDeviceRequest removeActiveDeviceRequest = this;
        RemoveActiveDeviceResponse removeActiveDeviceResponse = new RemoveActiveDeviceResponse();

        String response = getUserServiceRemote().removeActiveUserDevice(removeActiveDeviceRequest.getCredential().getTicketID(),
                removeActiveDeviceRequest.getCredential().getRequestID(), removeActiveDeviceRequest.getBody().getUserDeviceID());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        removeActiveDeviceResponse.setStatus(status);

        return removeActiveDeviceResponse;
    }

}
