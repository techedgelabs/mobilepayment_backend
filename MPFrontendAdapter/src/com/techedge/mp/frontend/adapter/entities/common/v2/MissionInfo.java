package com.techedge.mp.frontend.adapter.entities.common.v2;

import com.techedge.mp.core.business.interfaces.AppLink;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;

public class MissionInfo {

    private String     description;
    private String     code;
    private String     name;
    private String     type;
    private Integer    stepCompleted;
    private Integer    stepObjective;
    private AppLink    appLink;
    private CustomDate startDate;
    private CustomDate endDate;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getStepCompleted() {
        return stepCompleted;
    }

    public void setStepCompleted(Integer stepCompleted) {
        this.stepCompleted = stepCompleted;
    }

    public Integer getStepObjective() {
        return stepObjective;
    }

    public void setStepObjective(Integer stepObjective) {
        this.stepObjective = stepObjective;
    }

    public AppLink getAppLink() {
        return appLink;
    }

    public void setAppLink(AppLink appLink) {
        this.appLink = appLink;
    }

    public CustomDate getStartDate() {
        return startDate;
    }

    public void setStartDate(CustomDate startDate) {
        this.startDate = startDate;
    }

    public CustomDate getEndDate() {
        return endDate;
    }

    public void setEndDate(CustomDate endDate) {
        this.endDate = endDate;
    }

}
