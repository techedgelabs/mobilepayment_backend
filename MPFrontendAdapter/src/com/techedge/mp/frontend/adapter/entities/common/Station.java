package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;


public class Station {

    public static final String PAYMENT_SERVICE  = "PAYMENT";
    public static final String LOYALTY_SERVICE  = "LOYALTY";
    public static final String BUSINESS_SERVICE = "BUSINESS";
    
	private String stationID;
	private String name;
	private LocationData locationData;
	private List<String> mode;
    private List<String> enabledServices = new ArrayList<String>(0);

    public String getStationID() {
		return stationID;
	}
	public void setStationID(String stationID) {
		this.stationID = stationID;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public LocationData getLocationData() {
		return locationData;
	}
	public void setLocationData(
			LocationData locationData) {
		this.locationData = locationData;
	}

    public List<String> getMode() {
        return mode;
    }

    public void setMode(List<String> mode) {
        this.mode = mode;
    }
	
    public List<String> getEnabledServices() {
        return enabledServices;
    }

    public void setEnabledServices(List<String> enabledServices) {
        this.enabledServices = enabledServices;
    }
}
