package com.techedge.mp.frontend.adapter.entities.eventnotification.retrievetransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveLoyaltyTransactionDetailResponse extends BaseResponse{
    
    private RetrieveLoyaltyTransactionDetailBodyResponse body;

    public RetrieveLoyaltyTransactionDetailBodyResponse getBody() {
        return body;
    }

    public void setBody(RetrieveLoyaltyTransactionDetailBodyResponse body) {
        this.body = body;
    }
    
}
