package com.techedge.mp.frontend.adapter.entities.user.redemption;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RedemptionBodyRequest implements Validable {

    private Integer redemptionCode;

    public Integer getRedemptionCode() {
        return redemptionCode;
    }

    public void setRedemptionCode(Integer redemptionCode) {
        this.redemptionCode = redemptionCode;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.redemptionCode == null) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_REDEMPTION_SUCCESS);

        return status;

    }

}
