package com.techedge.mp.frontend.adapter.entities.parking.v2.approveparkingtransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.postpaid.v2.approvetransaction.ApproveTransactionPaymentMethodBody;

public class ApproveParkingtransactionRequestBody implements Validable {

    //private String          plateNumber;
    //private CustomTimestamp requestedEndTime;
    //private String          parkingZoneId;
    //private String          stallCode;
    private String          transactionID;
    private ApproveTransactionPaymentMethodBody paymentMethod;

    //    private String          cityId;
    //    private Boolean         stickerRequired;
    //    private String          stickerRequiredNotice;

    @Override
    public Status check() {

        Status status = new Status();
        
        if (this.paymentMethod != null) {

            status = this.paymentMethod.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.APPROVE_PARKING_TRANSACTION_INVALID_REQUEST);

            return status;
        }

        if (this.transactionID == null || this.transactionID.isEmpty()) {

            status.setStatusCode(StatusCode.APPROVE_PARKING_TRANSACTION_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.APPROVE_PARKING_TRANSACTION_SUCCESS);

        return status;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public ApproveTransactionPaymentMethodBody getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(ApproveTransactionPaymentMethodBody paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

}
