package com.techedge.mp.frontend.adapter.entities.user.business.addplatenumber;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class AddPlateNumberBodyRequest implements Validable {

    private String plateNumber;
    private String description;

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.plateNumber == null && !this.plateNumber.isEmpty()) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }
        
        if (this.description == null && !this.description.isEmpty()) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_ADD_PLATE_NUMBER_SUCCESS);

        return status;

    }

}
