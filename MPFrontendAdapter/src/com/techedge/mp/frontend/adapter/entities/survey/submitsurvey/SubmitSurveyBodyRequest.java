package com.techedge.mp.frontend.adapter.entities.survey.submitsurvey;

import java.util.ArrayList;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class SubmitSurveyBodyRequest implements Validable {

    private String code;
    private String key;
    private ArrayList<Answer> answers;

    public void addAnswer(String code, String value) {
        this.answers.add(new Answer(code, value));
    }
    
    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.code == null || this.code.isEmpty()) {
            status.setStatusCode(StatusCode.SURVEY_REQU_INVALID_REQUEST);
            return status;
        }

        if (this.key == null || this.key.isEmpty()) {
            status.setStatusCode(StatusCode.SURVEY_REQU_INVALID_REQUEST);
            return status;
        }

        status.setStatusCode(StatusCode.SURVEY_SUBMIT_SURVEY_SUCCESS);

        return status;

    }

    public class Answer {
        
        private String value;        
        private String code;        

        public Answer(String code, String value) {
            this.code = code;
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }

        public String getCode() {
            return code;
        }
    }
    
}
