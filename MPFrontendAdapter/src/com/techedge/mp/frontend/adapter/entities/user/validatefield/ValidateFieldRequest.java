package com.techedge.mp.frontend.adapter.entities.user.validatefield;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ValidateFieldRequest extends AbstractRequest implements Validable {

    private Status                   status = new Status();

    private Credential               credential;
    private ValidateFieldRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ValidateFieldRequestBody getBody() {
        return body;
    }

    public void setBody(ValidateFieldRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("VALID", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        ValidateFieldRequest validateFieldRequest = this;

        if (!validateFieldRequest.getBody().getType().equals("email_address") && !validateFieldRequest.getBody().getType().equals("phone_number")
                && !validateFieldRequest.getBody().getType().equals("device_number")) {

            String response = ResponseHelper.USER_CHECK_INVALID_REQUEST;
            status.setStatusCode(response);
            status.setStatusMessage(prop.getProperty(response));
        }
        else {

            String response = getUserServiceRemote().validateField(validateFieldRequest.getCredential().getTicketID(), validateFieldRequest.getCredential().getRequestID(),
                    validateFieldRequest.getCredential().getDeviceID(), validateFieldRequest.getBody().getType(), validateFieldRequest.getBody().getVerificationField(),
                    validateFieldRequest.getBody().getVerificationCode());

            status.setStatusCode(response);
            status.setStatusMessage(prop.getProperty(response));
        }

        ValidateFieldResponse validateFieldResponse = new ValidateFieldResponse();

        validateFieldResponse.setStatus(status);

        return validateFieldResponse;
    }
}
