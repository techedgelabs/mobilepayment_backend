package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;

public class PaymentData {

    private List<PaymentMethod> paymentMethodList = new ArrayList<PaymentMethod>(0);
    private Integer             residualCap;
    private Boolean             useVoucher;
    private Integer             residualVoucher;

    public PaymentData() {}

    public List<PaymentMethod> getPaymentMethodList() {
        return paymentMethodList;
    }

    public void setPaymentMethodList(List<PaymentMethod> paymentMethodList) {
        this.paymentMethodList = paymentMethodList;
    }

    public Integer getResidualCap() {
        return residualCap;
    }

    public void setResidualCap(Integer residualCap) {
        this.residualCap = residualCap;
    }

    public Boolean getUseVoucher() {
        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {
        this.useVoucher = useVoucher;
    }

    public Integer getResidualVoucher() {
        return residualVoucher;
    }

    public void setResidualVoucher(Integer residualVoucher) {
        this.residualVoucher = residualVoucher;
    }

}
