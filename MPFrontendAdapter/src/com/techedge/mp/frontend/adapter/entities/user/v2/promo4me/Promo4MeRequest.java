package com.techedge.mp.frontend.adapter.entities.user.v2.promo4me;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import com.techedge.mp.core.business.interfaces.ParameterNotification;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.Offer;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.CrmOffers;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class Promo4MeRequest extends AbstractRequest {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PROMO4ME", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.USER_V2_PROMO4ME_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        Promo4MeRequest promo4MeRequest = this;
        Promo4MeResponse promo4MeResponse = new Promo4MeResponse();
        Promo4MeBodyResponse bodyResponse = new Promo4MeBodyResponse();

        String ticketId = promo4MeRequest.getCredential().getTicketID();
        String requestId = promo4MeRequest.getCredential().getRequestID();

        com.techedge.mp.core.business.interfaces.crm.Promo4MeResponse response = getCrmServiceRemote().getPromo4Me(ticketId, requestId);

        if (response.getStatusCode().equals(ResponseHelper.CRM_PROMO4ME_SUCCESS)) {
            for (Offer offer : response.getOffersList()) {
                CrmOffers crmOffer = new CrmOffers();
                HashMap<String, Object> offerParams = offer.getAdditionalAttributes();
                if (offer.getDescription() != null) {
                    crmOffer.setDescription(offer.getDescription());
                }
                for (String key : offerParams.keySet()) {
                    Object value = offerParams.get(key);
                    String valueToString = value != null ? value.toString() : "";
                    if (valueToString.equals("n/a")) {
                        value = "";
                    }
                    if (key.matches("^C_[0-9]+$")) {
                        crmOffer.getAppLink().getParameters().add(new ParameterNotification(key, valueToString));
                    }
                    if (key.equals("UrlApp") && !valueToString.isEmpty()) {
                        crmOffer.getAppLink().setLocation(valueToString.trim());
                        crmOffer.getAppLink().setType(PushNotificationContentType.INTERNAL);
                    }
                    else if (key.equals("textAPP") && !valueToString.isEmpty()) {
                        crmOffer.setDescription(valueToString);
                    }
                    else if (key.equals("ExpirationDate") && !valueToString.isEmpty()) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        
                        if (value.getClass().equals(Date.class)) {
                            crmOffer.setDate(CustomDate.createCustomDate((Date) value)); 
                        }
                        else {
                            try {
                                crmOffer.setDate(CustomDate.createCustomDate(sdf.parse((String) value)));
                            }
                            catch (ParseException e) {
                                System.err.println("Errore nel parsing della data: " + e.getMessage());
                                crmOffer.setDate(null);
                            }
                        }
                        
                    }
                }

                crmOffer.setName(offer.getOfferName());
                crmOffer.setCode(offer.getOfferCodeToString("-"));
                bodyResponse.getOffers().add(crmOffer);
            }
        }

        promo4MeResponse.setBody(bodyResponse);

        status.setStatusCode(response.getStatusCode());
        status.setStatusMessage(prop.getProperty(response.getStatusCode()));

        promo4MeResponse.setStatus(status);

        return promo4MeResponse;

    }
}
