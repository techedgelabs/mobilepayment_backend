package com.techedge.mp.frontend.adapter.entities.user.getcreditvoucher;

import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class GetCreditVoucherRequest implements Validable {

    private Credential                  credential;
    private GetCreditVoucherRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GetCreditVoucherRequestBody getBody() {
        return body;
    }

    public void setBody(GetCreditVoucherRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-CREDIT-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        else {
            
            status.setStatusCode(StatusCode.GET_CREDIT_VOUCHER_FAILURE);
            return status;

        }

        status.setStatusCode(StatusCode.GET_CREDIT_VOUCHER_SUCCESS);

        return status;

    }

}
