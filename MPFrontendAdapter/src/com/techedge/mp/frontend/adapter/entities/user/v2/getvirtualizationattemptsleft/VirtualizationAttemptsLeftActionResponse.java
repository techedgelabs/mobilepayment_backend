package com.techedge.mp.frontend.adapter.entities.user.v2.getvirtualizationattemptsleft;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class VirtualizationAttemptsLeftActionResponse extends BaseResponse {

    private VirtualizationAttemptsLeftActionResponseBody body;

    public VirtualizationAttemptsLeftActionResponseBody getBody() {
        return body;
    }

    public void setBody(VirtualizationAttemptsLeftActionResponseBody body) {
        this.body = body;
    }
}
