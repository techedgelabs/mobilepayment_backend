package com.techedge.mp.frontend.adapter.entities.manager.retrievevouchers;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.VoucherData;

public class ManagerRetrieveVouchersBodyDataResponse {

    private String            stationID;
    private String            pumpNumber;
    private CustomTimestamp   date;
    private CustomTimestamp   systemDate;
    private Integer           totalConsumed;
    private List<VoucherData> vouchers = new ArrayList<VoucherData>(0);

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getPumpNumber() {
        return pumpNumber;
    }

    public void setPumpNumber(String pumpNumber) {
        this.pumpNumber = pumpNumber;
    }

    public CustomTimestamp getDate() {
        return date;
    }

    public void setDate(CustomTimestamp date) {
        this.date = date;
    }

    public CustomTimestamp getSystemDate() {
        return systemDate;
    }

    public void setSystemDate(CustomTimestamp systemDate) {
        this.systemDate = systemDate;
    }

    public Integer getTotalConsumed() {
        return totalConsumed;
    }

    public void setTotalConsumed(Integer totalConsumed) {
        this.totalConsumed = totalConsumed;
    }

    public List<VoucherData> getVouchers() {
        return vouchers;
    }

    public void setVouchers(List<VoucherData> vouchers) {
        this.vouchers = vouchers;
    }

}
