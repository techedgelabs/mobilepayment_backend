package com.techedge.mp.frontend.adapter.entities.user.v2.externalproviderauthentication;

import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.user.v2.authentication.AuthenticationUserBodyRequest;

public class ExternalProviderAuthenticationUserBodyRequest extends
		AuthenticationUserBodyRequest implements Validable {

	private String accessToken;
	private String password;
	private String provider;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}


	public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
	public Status check() {

		Status status = new Status();
		
		if(this.provider != null && this.provider.equals(User.USER_SOURCE_MYCICERO)){
		    if(this.accessToken == null || this.accessToken.isEmpty()){
		        status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

	            return status;
		    }
		}
		
		else if(this.provider != null && this.provider.equals(User.USER_SOURCE_ENJOY)){
            if( this.getUsername() == null || this.getUsername().isEmpty() || this.password == null || this.password.isEmpty() ){
                status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

                return status;
            }
        }
    
		

		else if (this.provider == null || this.provider.length() == 0) {

			status.setStatusCode(StatusCode.USER_V2_AUTH_INVALID_REQUEST);

			return status;

		}
		
		status.setStatusCode(StatusCode.USER_V2_AUTH_SUCCESS);

		return status;
	}

}
