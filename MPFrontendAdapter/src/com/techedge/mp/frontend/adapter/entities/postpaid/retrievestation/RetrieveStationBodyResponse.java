package com.techedge.mp.frontend.adapter.entities.postpaid.retrievestation;

import java.util.ArrayList;
import java.util.List;

public class RetrieveStationBodyResponse {

	
	private String outOfRange;
	private List<RetrieveStationBodyStationResponse> stations = new ArrayList<RetrieveStationBodyStationResponse>(0);
	
	public List<RetrieveStationBodyStationResponse> getStations() {
		return stations;
	}
	public void setStations(List<RetrieveStationBodyStationResponse> stations) {
		this.stations = stations;
	}
	public String getOutOfRange() {
		return outOfRange;
	}
	public void setOutOfRange(String outOfRange) {
		this.outOfRange = outOfRange;
	}
	
}
