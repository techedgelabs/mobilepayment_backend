package com.techedge.mp.frontend.adapter.entities.user.v2.requestresetpin;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RequestResetPinRequest extends AbstractRequest implements Validable {

    private Status                     status = new Status();

    private Credential                 credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }


    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RECOVER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.USER_REQUEST_RESEND_PIN_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        RequestResetPinRequest requestResetPinRequest = this;

        String response = getUserV2ServiceRemote().requestResetPin(requestResetPinRequest.getCredential().getTicketID(), requestResetPinRequest.getCredential().getRequestID());

        RequestResetPinResponse recoverUsernameResponse = new RequestResetPinResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        recoverUsernameResponse.setStatus(status);

        return recoverUsernameResponse;
    }

}
