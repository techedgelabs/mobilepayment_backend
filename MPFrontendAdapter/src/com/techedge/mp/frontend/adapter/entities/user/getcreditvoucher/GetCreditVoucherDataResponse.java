package com.techedge.mp.frontend.adapter.entities.user.getcreditvoucher;

import java.util.List;

import com.techedge.mp.bpel.operation.refuel.business.interfaces.DetailsData;
import com.techedge.mp.bpel.operation.refuel.business.interfaces.ExpirationDate;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetCreditVoucherDataResponse extends BaseResponse {
    private String            voucherCode;
    private String            voucherValue;
    private String            initialValue;
    private String            consumedValue;
    private String            voucherBalanceDue;
    private ExpirationDate    expirationDate;
    private List<DetailsData> details;

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(String voucherValue) {
        this.voucherValue = voucherValue;
    }

    public String getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(String initialValue) {
        this.initialValue = initialValue;
    }

    public String getConsumedValue() {
        return consumedValue;
    }

    public void setConsumedValue(String consumedValue) {
        this.consumedValue = consumedValue;
    }

    public String getVoucherBalanceDue() {
        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(String voucherBalanceDue) {
        this.voucherBalanceDue = voucherBalanceDue;
    }

    public ExpirationDate getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(ExpirationDate expirationDate) {
        this.expirationDate = expirationDate;
    }

    public List<DetailsData> getDetails() {
        return details;
    }

    public void setDetails(List<DetailsData> details) {
        this.details = details;
    }

}
