package com.techedge.mp.frontend.adapter.entities.user.v2.insertpaymentmethod;

import com.techedge.mp.core.business.interfaces.PaymentV2Response;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class InsertPaymentMethodRequest extends AbstractRequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;
    private InsertPaymentMethodBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public InsertPaymentMethodBodyRequest getBody() {
        return body;
    }

    public void setBody(InsertPaymentMethodBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PAY-INSERT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
        InsertPaymentMethodRequest insertPaymentMethodRequest = this;

        PaymentV2Response paymentResponse = getUserV2ServiceRemote().insertPaymentMethod(insertPaymentMethodRequest.getCredential().getTicketID(),
                insertPaymentMethodRequest.getCredential().getRequestID(), insertPaymentMethodRequest.getBody().getPaymentMethod().getType(),
                insertPaymentMethodRequest.getBody().getNewPin());

        InsertPaymentMethodResponse insertPaymentMethodResponse = new InsertPaymentMethodResponse();

        status.setStatusCode(paymentResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(paymentResponse.getStatusCode()));

        insertPaymentMethodResponse.setStatus(status);

        PaymentMethod paymentMethod = new PaymentMethod();
        paymentMethod.setId(paymentResponse.getPaymentMethodId());
        paymentMethod.setType(paymentResponse.getPaymentMethodType());

        InsertPaymentMethodBodyResponse insertPaymentMethodBodyResponse = new InsertPaymentMethodBodyResponse();
        insertPaymentMethodBodyResponse.setRedirectUrl(paymentResponse.getRedirectUrl());
        insertPaymentMethodBodyResponse.setShopLogin(paymentResponse.getShopLogin());
        insertPaymentMethodBodyResponse.setPaymentMethod(paymentMethod);
        insertPaymentMethodBodyResponse.setCheckPinAttemptsLeft(paymentResponse.getPinCheckAttemptsLeft());
        insertPaymentMethodResponse.setBody(insertPaymentMethodBodyResponse);

        return insertPaymentMethodResponse;
    }

}
