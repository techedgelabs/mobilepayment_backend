package com.techedge.mp.frontend.adapter.entities.common;



public class Cash {

	private String cashID;
	private Integer number;
	private String refuelMode;
	public String getCashID() {
		return cashID;
	}
	public void setCashID(String cashID) {
		this.cashID = cashID;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
    public String getRefuelMode() {
        return refuelMode;
    }
    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }
}
