package com.techedge.mp.frontend.adapter.entities.common;


public class City {

	private String cityName;
	private String cityProvince;
	
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	public String getCityProvince() {
		return cityProvince;
	}
	public void setCityProvince(String cityProvince) {
		this.cityProvince = cityProvince;
	}
}
