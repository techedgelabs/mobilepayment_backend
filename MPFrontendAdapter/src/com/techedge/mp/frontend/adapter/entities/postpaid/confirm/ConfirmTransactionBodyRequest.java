package com.techedge.mp.frontend.adapter.entities.postpaid.confirm;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ConfirmTransactionBodyRequest implements Validable {
	
	private String transactionID;

	public String getTransactionID() {
		return transactionID;
	}

	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.transactionID == null || this.transactionID.length() != 32 || this.transactionID.trim() == "") {
			
			status.setStatusCode(StatusCode.POP_CONFIRM_TRANSACTION_WRONG_ID);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.POP_CONFIRM_TRANSACTION_SUCCESS);
		
		return status;
	}
	
}