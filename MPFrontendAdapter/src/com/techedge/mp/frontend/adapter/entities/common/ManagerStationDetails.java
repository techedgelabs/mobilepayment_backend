package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;

public class ManagerStationDetails extends Station {

    private List<Pump> pumpList = new ArrayList<Pump>(0);

    public List<Pump> getPumpList() {
        return pumpList;
    }

    public void setPumpList(List<Pump> pumpList) {
        this.pumpList = pumpList;
    }

}
