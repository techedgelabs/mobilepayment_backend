package com.techedge.mp.frontend.adapter.entities.common.v2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;

public class ParkingZonePriceInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 5894369538671332807L;
    //private String            parkingZoneId;
    //private String            parkingZoneName;
    //private String            parkingZoneDescription;
    private ParkingZone       parkingZone;
    private String            stallCode;
    //private String            plateNumber;
    private PlateNumberInfo   plateNumber;
    private CustomTimestamp   parkingStartTime;
    private CustomTimestamp   parkingEndTime;
    private Integer           price;
    private Integer           finalPrice;
    private Integer           currentPrice;
    private Integer           priceDifference;
    private PaymentMethod     paymentMethod;

    private List<String>      notice = new ArrayList<String>(0);
    private String            transactionID;

    
    public ParkingZone getParkingZone() {
        return parkingZone;
    }

    public void setParkingZone(ParkingZone parkingZone) {
        this.parkingZone = parkingZone;
    }

    public PlateNumberInfo getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(PlateNumberInfo plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getStallCode() {
        return stallCode;
    }

    public void setStallCode(String stallCode) {
        this.stallCode = stallCode;
    }

    public CustomTimestamp getParkingStartTime() {
        return parkingStartTime;
    }

    public void setParkingStartTime(CustomTimestamp parkingStartTime) {
        this.parkingStartTime = parkingStartTime;
    }

    public CustomTimestamp getParkingEndTime() {
        return parkingEndTime;
    }

    public void setParkingEndTime(CustomTimestamp parkingEndTime) {
        this.parkingEndTime = parkingEndTime;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public List<String> getNotice() {
        return notice;
    }

    public void setNotice(List<String> notice) {
        this.notice = notice;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public Integer getFinalPrice() {
        return finalPrice;
    }

    public void setFinalPrice(Integer finalPrice) {
        this.finalPrice = finalPrice;
    }

    public Integer getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(Integer currentPrice) {
        this.currentPrice = currentPrice;
    }

    public Integer getPriceDifference() {
        return priceDifference;
    }

    public void setPriceDifference(Integer priceDifference) {
        this.priceDifference = priceDifference;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

}
