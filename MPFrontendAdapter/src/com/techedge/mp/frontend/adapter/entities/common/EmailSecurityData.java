package com.techedge.mp.frontend.adapter.entities.common;

import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class EmailSecurityData implements Validable {
	
	private String email;
	private Integer status;
	
	
	public EmailSecurityData(){}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
	public Status check() {
		
		Status status = new Status();
		
		status = Validator.checkEmail("CREATE", email);
		
		if(!Validator.isValid(status.getStatusCode())) {
			
			status.setStatusCode(StatusCode.USER_CREATE_EMAIL_WRONG);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);
		
		return status;
	}
	
	
}
