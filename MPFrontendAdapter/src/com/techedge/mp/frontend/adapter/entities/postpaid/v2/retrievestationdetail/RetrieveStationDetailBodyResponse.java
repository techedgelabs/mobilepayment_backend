package com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievestationdetail;

import java.util.ArrayList;
import java.util.List;

public class RetrieveStationDetailBodyResponse {

	
	private String outOfRange;
	private List<RetrieveStationDetailBodyStationResponse> stations = new ArrayList<RetrieveStationDetailBodyStationResponse>(0);
	
	public List<RetrieveStationDetailBodyStationResponse> getStations() {
		return stations;
	}
	public void setStations(List<RetrieveStationDetailBodyStationResponse> stations) {
		this.stations = stations;
	}
	public String getOutOfRange() {
		return outOfRange;
	}
	public void setOutOfRange(String outOfRange) {
		this.outOfRange = outOfRange;
	}
	
}
