package com.techedge.mp.frontend.adapter.entities.user.business.clonepaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class ClonePaymentMethodResponse extends BaseResponse {
	
	
	private ClonePaymentMethodBodyResponse body;

	
	public ClonePaymentMethodBodyResponse getBody() {
		return body;
	}

	public void setBody(ClonePaymentMethodBodyResponse body) {
		this.body = body;
	}
	
}
