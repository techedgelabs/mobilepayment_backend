package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory;

import java.sql.Timestamp;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ConstantsHelper;
import com.techedge.mp.core.business.interfaces.ProductStatisticsInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransaction;
import com.techedge.mp.core.business.interfaces.postpaid.PaymentHistory;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCartData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelData;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.POPStatusConverter;
import com.techedge.mp.frontend.adapter.entities.common.ProductStatistics;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidCartJsonData;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidExtendedRefuelData;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;
import com.techedge.mp.frontend.adapter.entities.common.v2.ParkingZone;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveTransactionHistoryRequest extends AbstractRequest implements Validable {

    private Status                                status = new Status();

    private Credential                            credential;
    private RetrieveTransactionHistoryBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveTransactionHistoryBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveTransactionHistoryBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("TRANSACTION-HISTORY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.RETRIEVE_TRANSACTION_HISTORY_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        RetrieveTransactionHistoryRequest retrieveTransactionHistoryRequest = this;

        com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory.RetrieveTransactionHistoryResponse retrieveTransactionHistoryResponse = new com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionhistory.RetrieveTransactionHistoryResponse();

        com.techedge.mp.core.business.interfaces.postpaid.RetrieveTransactionHistoryResponse coreRetrieveTransactionHistoryResponse = getPostPaidTransactionServiceRemote().retrieveTransactionHistory(
                retrieveTransactionHistoryRequest.getCredential().getRequestID(), retrieveTransactionHistoryRequest.getCredential().getTicketID(),
                retrieveTransactionHistoryRequest.getBody().getStartDate().toDate(), retrieveTransactionHistoryRequest.getBody().getEndDate().toDate(),
                retrieveTransactionHistoryRequest.getBody().getItemsLimit(), retrieveTransactionHistoryRequest.getBody().getPageOffset());

        if (!coreRetrieveTransactionHistoryResponse.getStatusCode().equals(ResponseHelper.RETRIEVE_TRANSACTION_HISTORY_SUCCESS)) {
            RetrieveTransactionHistoryResponse response = new RetrieveTransactionHistoryResponse();
            status.setStatusCode(coreRetrieveTransactionHistoryResponse.getStatusCode());
            response.setStatus(status);

            return response;
        }

        RetrieveTransactionHistoryBodyResponse retrieveTransactionHistoryBodyResponse = new RetrieveTransactionHistoryBodyResponse();

        status.setStatusCode(coreRetrieveTransactionHistoryResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(status.getStatusCode()));

        retrieveTransactionHistoryResponse.setStatus(status);

        retrieveTransactionHistoryBodyResponse.setPageOffset(coreRetrieveTransactionHistoryResponse.getPageOffset());
        retrieveTransactionHistoryBodyResponse.setPagesTotal(coreRetrieveTransactionHistoryResponse.getPagesTotal());

        // Valorizzazione storico rifornimenti
        for (PaymentHistory paymentHistory : coreRetrieveTransactionHistoryResponse.getPaymentHistory()) {
            TransactionHistory transactionHistory = new TransactionHistory();
            transactionHistory.setSourceType(paymentHistory.getSourceType());
            transactionHistory.setSourceStatus(paymentHistory.getSourceStatus());

            PostPaidTransactionHistoryData postPaidTransactionHistoryData = new PostPaidTransactionHistoryData();
            postPaidTransactionHistoryData.setTransactionID(paymentHistory.getTransaction().getMpTransactionID());

            // Trascodifica dello stato
            if (paymentHistory.getSourceType().equals(ConstantsHelper.TRANSACTION_SOURCE_TYPE_VOUCHER)) {

                if (paymentHistory.getTransaction().getStatus() == null) {
                    postPaidTransactionHistoryData.setStatus(null);
                }
                else {
                    String statusOld = paymentHistory.getTransaction().getStatus();
                    statusOld = "2" + statusOld.substring(1);
                    postPaidTransactionHistoryData.setStatus(statusOld);
                }
            }
            else {
                if (paymentHistory.getTransaction().getStatus() == null) {
                    postPaidTransactionHistoryData.setStatus(null);
                }
                else {

                    String localStatus = POPStatusConverter.toAppStatus(paymentHistory.getTransaction().getStatus());

                    if (!localStatus.isEmpty()) {
                        postPaidTransactionHistoryData.setStatus(localStatus);
                    }
                    else {
                        postPaidTransactionHistoryData.setStatus(paymentHistory.getTransaction().getStatus());
                    }
                }
            }

            postPaidTransactionHistoryData.setSubStatus(paymentHistory.getTransaction().getSubStatus());
            postPaidTransactionHistoryData.setAmount(AmountConverter.toMobile(paymentHistory.getTransaction().getAmount()));
            postPaidTransactionHistoryData.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(paymentHistory.getTransaction().getCreationTimestamp().getTime())));
            postPaidTransactionHistoryData.setStatusTitle(paymentHistory.getTransaction().getStatusTitle());
            postPaidTransactionHistoryData.setStatusDescription(paymentHistory.getTransaction().getStatusDescription());

            if (paymentHistory.getTransaction().getCart() != null) {
                for (PostPaidCartData postPaidCartData : paymentHistory.getTransaction().getCart()) {
                    PostPaidCartJsonData postPaidCartJsonData = new PostPaidCartJsonData();
                    postPaidCartJsonData.setProductId(postPaidCartData.getProductId());
                    postPaidCartJsonData.setProductDescription(postPaidCartData.getProductDescription());
                    postPaidCartJsonData.setAmount(AmountConverter.toMobile(postPaidCartData.getAmount()));
                    postPaidCartJsonData.setQuantity(postPaidCartData.getQuantity());
                    postPaidTransactionHistoryData.getCart().add(postPaidCartJsonData);
                }
            }

            if (paymentHistory.getTransaction().getRefuel() != null) {
                for (PostPaidRefuelData postPaidRefuelData : paymentHistory.getTransaction().getRefuel()) {
                    PostPaidExtendedRefuelData postPaidRefuelJsonData = new PostPaidExtendedRefuelData();
                    postPaidRefuelJsonData.setPumpId(postPaidRefuelData.getPumpId());
                    postPaidRefuelJsonData.setProductId(postPaidRefuelData.getProductId());
                    postPaidRefuelJsonData.setProductDescription(postPaidRefuelData.getProductDescription());
                    postPaidRefuelJsonData.setFuelType(postPaidRefuelData.getFuelType());
                    postPaidRefuelJsonData.setFuelQuantity(postPaidRefuelData.getFuelQuantity());
                    postPaidRefuelJsonData.setFuelAmount(postPaidRefuelData.getFuelAmount());
                    postPaidTransactionHistoryData.getRefuel().add(postPaidRefuelJsonData);
                }
            }

            transactionHistory.setTransaction(postPaidTransactionHistoryData);

            if (paymentHistory.getTransaction().getStation() != null) {

                Station station = new Station();
                LocationData locationData = new LocationData();
                station.setStationID(paymentHistory.getTransaction().getStation().getStationID());
                station.setName(null);
                locationData.setAddress(paymentHistory.getTransaction().getStation().getAddress());
                locationData.setCity(paymentHistory.getTransaction().getStation().getCity());
                locationData.setProvince(paymentHistory.getTransaction().getStation().getProvince());
                locationData.setCountry(paymentHistory.getTransaction().getStation().getCountry());
                locationData.setLatitude(paymentHistory.getTransaction().getStation().getLatitude());
                locationData.setLongitude(paymentHistory.getTransaction().getStation().getLongitude());
                station.setLocationData(locationData);
                transactionHistory.setStation(station);
            }

            // Inserire la transazione solo se si � conclusa positivamente
            if (postPaidTransactionHistoryData.getStatus().equals("0013") || postPaidTransactionHistoryData.getStatus().equals("1001")
                    || postPaidTransactionHistoryData.getStatus().equals("2007")) {

                retrieveTransactionHistoryBodyResponse.getPaymentHistory().add(transactionHistory);
            }
        }
        
        // Valorizzazione storico soste
        for (ParkingTransaction parkingTransaction : coreRetrieveTransactionHistoryResponse.getParkingHistory()) {
            
            System.out.println("ParkingTransactionId: " + parkingTransaction.getParkingTransactionId());
            
            ParkingTransactionHistory parkingTransactionHistory = new ParkingTransactionHistory();
            parkingTransactionHistory.setSourceType("PARKING");
            parkingTransactionHistory.setSourceStatus("");

            PostPaidTransactionHistoryData postPaidTransactionHistoryData = new PostPaidTransactionHistoryData();
            postPaidTransactionHistoryData.setTransactionID(parkingTransaction.getParkingTransactionId());

            // Trascodifica dello stato
            postPaidTransactionHistoryData.setStatus("ENDED");
            
            postPaidTransactionHistoryData.setSubStatus("");
            postPaidTransactionHistoryData.setAmount(AmountConverter.toMobile(parkingTransaction.getFinalPrice()));
            postPaidTransactionHistoryData.setDate(CustomTimestamp.createCustomTimestamp(new Timestamp(parkingTransaction.getFinalParkingEndTime().getTime())));
            postPaidTransactionHistoryData.setStatusTitle("Transazione Completata");
            postPaidTransactionHistoryData.setStatusDescription("Transazione Completata");

            parkingTransactionHistory.setTransaction(postPaidTransactionHistoryData);

            ParkingZone parkingZone = new ParkingZone();
            parkingZone.setAdministrativeAreaLevel2Code(parkingTransaction.getAdministrativeAreaLevel2Code());
            parkingZone.setCityId(parkingTransaction.getCityId());
            parkingZone.setCityName(parkingTransaction.getCityName());
            parkingZone.setDescription(parkingTransaction.getParkingZoneDescription());
            parkingZone.setName(parkingTransaction.getParkingZoneName());
            //parkingZone.setNotice(parkingTransaction.getParkingZoneNotice());
                
            parkingTransactionHistory.setParkingZone(parkingZone);
            
            // Inserire la transazione solo se si � conclusa positivamente
            retrieveTransactionHistoryBodyResponse.getParkingHistory().add(parkingTransactionHistory);
        }

        List<ProductStatisticsInfo> productStatisticsInfoList = coreRetrieveTransactionHistoryResponse.getStatistics();

        Double quantityAll         = 0.0;
        Double previousQuantityAll = 0.0;
        Double amountAll           = 0.0;
        Double previousAmountAll   = 0.0;
        
        for (ProductStatisticsInfo productStatisticsInfo : productStatisticsInfoList) {

            ProductStatistics productStatistics = new ProductStatistics();

            productStatistics.setProductDescription(productStatisticsInfo.getProductDescription());

            Double quantity = 0.0;
            if (productStatisticsInfo.getQuantity() != null) {
                quantity = productStatisticsInfo.getQuantity();
            }
            productStatistics.setQuantity(AmountConverter.toMobile3(quantity));

            Double previousQuantity = 0.0;
            if (productStatisticsInfo.getPreviousQuantity() != null) {
                previousQuantity = productStatisticsInfo.getPreviousQuantity();
            }
            productStatistics.setPreviousQuantity(AmountConverter.toMobile3(previousQuantity));
            
            Double amount = 0.0;
            if (productStatisticsInfo.getAmount() != null) {
                amount = productStatisticsInfo.getAmount();
            }
            productStatistics.setAmount(AmountConverter.toMobile(amount));

            Double previousAmount = 0.0;
            if (productStatisticsInfo.getPreviousAmount() != null) {
                previousAmount = productStatisticsInfo.getPreviousAmount();
            }
            productStatistics.setPreviousAmount(AmountConverter.toMobile(previousAmount));

            if (productStatistics.getQuantity() > 0) {
                
                retrieveTransactionHistoryBodyResponse.getStatistics().add(productStatistics);
                
                quantityAll         += quantity;
                previousQuantityAll += previousQuantity;
                amountAll           += amount;
                previousAmountAll   += previousAmount;
                
                //System.out.println("QuantityAll: " + quantityAll);
            }
        }
        
        if (quantityAll > 0) {
            
            ProductStatistics productStatisticsAll = new ProductStatistics();
            
            productStatisticsAll.setQuantity(AmountConverter.toMobile3(quantityAll));
            productStatisticsAll.setPreviousQuantity(AmountConverter.toMobile3(previousQuantityAll));
            productStatisticsAll.setAmount(AmountConverter.toMobile(amountAll));
            productStatisticsAll.setPreviousAmount(AmountConverter.toMobile(previousAmountAll));
            productStatisticsAll.setProductDescription("All");
            
            retrieveTransactionHistoryBodyResponse.getStatistics().add(productStatisticsAll);
        }

        retrieveTransactionHistoryResponse.setBody(retrieveTransactionHistoryBodyResponse);

        return retrieveTransactionHistoryResponse;
    }

}
