package com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.AmountInfo;

public class RetrieveApplicationSettingsBodyResponse {

    private String                                              depositCardAnd;
    private String                                              depositCardiOS;
    private String                                              depositCardWP;

    private String                                              robotUrl;
    
    private Integer                                             minAmount;
    private Integer                                             maxAmount;
    private List<AmountInfo>                                    defaultAmount = new ArrayList<AmountInfo>(0);

    private RetrieveApplicationSettingsApplicationBodyResponse  application;
    private RetrieveApplicationSettingsRegistrationBodyResponse registration;
    private RetrieveApplicationSettingsHowToBodyResponse        howTo;
    private RetrieveApplicationSettingsRefuelBodyResponse       refuel;
    private RetrieveApplicationSettingsRefuelBodyResponse       refuelNewFlow;
    private RetrieveApplicationSettingsStationBodyResponse      station;
    private RetrieveApplicationSettingsSupportBodyResponse      support;
    private RetrieveApplicationSettingsStaticPageBodyResponse   staticPage;
    private RetrieveApplicationSettingsDinamycPageBodyResponse  dinamycPage;
    private RetrieveApplicationSettingsFirstStartBodyResponse   firstStart;
    private RetrieveApplicationSettingsSecurityBodyResponse     security;
    

    //private Boolean loyaltyRequired;

    public String getDepositCardAnd() {
        return depositCardAnd;
    }

    public void setDepositCardAnd(String depositCardAnd) {
        this.depositCardAnd = depositCardAnd;
    }

    public String getDepositCardiOS() {
        return depositCardiOS;
    }

    public void setDepositCardiOS(String depositCardiOS) {
        this.depositCardiOS = depositCardiOS;
    }

    public String getDepositCardWP() {
        return depositCardWP;
    }

    public void setDepositCardWP(String depositCardWP) {
        this.depositCardWP = depositCardWP;
    }

    public String getRobotUrl() {
        return robotUrl;
    }

    public void setRobotUrl(String robotUrl) {
        this.robotUrl = robotUrl;
    }

    public Integer getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Integer minAmount) {
        this.minAmount = minAmount;
    }

    public Integer getMaxAmount() {
        return maxAmount;
    }

    public void setMaxAmount(Integer maxAmount) {
        this.maxAmount = maxAmount;
    }

    public List<AmountInfo> getDefaultAmount() {
        return defaultAmount;
    }

    public void setDefaultAmount(List<AmountInfo> defaultAmount) {
        this.defaultAmount = defaultAmount;
    }

    public RetrieveApplicationSettingsApplicationBodyResponse getApplication() {
        return application;
    }

    public void setApplication(RetrieveApplicationSettingsApplicationBodyResponse application) {
        this.application = application;
    }

    public RetrieveApplicationSettingsRegistrationBodyResponse getRegistration() {
        return registration;
    }

    public void setRegistration(RetrieveApplicationSettingsRegistrationBodyResponse registration) {
        this.registration = registration;
    }

    public RetrieveApplicationSettingsHowToBodyResponse getHowTo() {
        return howTo;
    }

    public void setHowTo(RetrieveApplicationSettingsHowToBodyResponse howTo) {
        this.howTo = howTo;
    }

    public RetrieveApplicationSettingsRefuelBodyResponse getRefuel() {
        return refuel;
    }

    public void setRefuel(RetrieveApplicationSettingsRefuelBodyResponse refuel) {
        this.refuel = refuel;
    }

    public RetrieveApplicationSettingsStationBodyResponse getStation() {
        return station;
    }

    public void setStation(RetrieveApplicationSettingsStationBodyResponse station) {
        this.station = station;
    }

    public RetrieveApplicationSettingsSupportBodyResponse getSupport() {
        return support;
    }

    public void setSupport(RetrieveApplicationSettingsSupportBodyResponse support) {
        this.support = support;
    }

    public RetrieveApplicationSettingsStaticPageBodyResponse getStaticPage() {
        return staticPage;
    }

    public void setStaticPage(RetrieveApplicationSettingsStaticPageBodyResponse staticPage) {
        this.staticPage = staticPage;
    }

    public RetrieveApplicationSettingsDinamycPageBodyResponse getDinamycPage() {
        return dinamycPage;
    }

    public void setDinamycPage(RetrieveApplicationSettingsDinamycPageBodyResponse dinamycPage) {
        this.dinamycPage = dinamycPage;
    }

    public RetrieveApplicationSettingsFirstStartBodyResponse getFirstStart() {
        return firstStart;
    }

    public void setFirstStart(RetrieveApplicationSettingsFirstStartBodyResponse firstStart) {
        this.firstStart = firstStart;
    }

    public RetrieveApplicationSettingsRefuelBodyResponse getRefuelNewFlow() {
        return refuelNewFlow;
    }

    public void setRefuelNewFlow(RetrieveApplicationSettingsRefuelBodyResponse refuelNewFlow) {
        this.refuelNewFlow = refuelNewFlow;
    }

    public RetrieveApplicationSettingsSecurityBodyResponse getSecurity() {
        return security;
    }
    
    public void setSecurity(RetrieveApplicationSettingsSecurityBodyResponse security) {
        this.security = security;
    }

}
