package com.techedge.mp.frontend.adapter.entities.user.business.authentication;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.techedge.mp.core.business.interfaces.AuthenticationBusinessResponse;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PlateNumber;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.PersonalDataBusiness;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.BillingAddressData;
import com.techedge.mp.frontend.adapter.entities.common.ContactData;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.EmailSecurityData;
import com.techedge.mp.frontend.adapter.entities.common.LastLoginData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.BusinessData;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.business.UserStatus;
import com.techedge.mp.frontend.adapter.entities.common.business.Validator;
import com.techedge.mp.frontend.adapter.entities.common.v2.PlateNumberInfo;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class AuthenticationUserRequest extends AbstractRequest {

    private AuthenticationUserBodyRequest body;

    public AuthenticationUserBodyRequest getBody() {
        return body;
    }

    public void setBody(AuthenticationUserBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.USER_BUSINESS_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_BUSINESS_AUTH_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        AuthenticationUserRequest authenticationUserRequest = this;

        String passwordHash = authenticationUserRequest.getBody().getPasswordH();
        String source = authenticationUserRequest.getBody().getSource();

        AuthenticationBusinessResponse authenticationResponse = getUserV2ServiceRemote().authenticationBusiness(authenticationUserRequest.getBody().getUsername(), passwordHash, 
                authenticationUserRequest.getBody().getRequestID(), authenticationUserRequest.getBody().getDeviceID(), authenticationUserRequest.getBody().getDeviceName(), source);

        Status status = new Status();

        if (authenticationResponse.getStatusCode().equals(StatusCode.USER_BUSINESS_AUTH_SUCCESS)) {

            AuthenticationUserResponseSuccess authenticationUserResponseSuccess = new AuthenticationUserResponseSuccess();

            status.setStatusCode(authenticationResponse.getStatusCode());
            status.setStatusMessage(prop.getProperty(authenticationResponse.getStatusCode()));
            authenticationUserResponseSuccess.setStatus(status);

            AuthenticationUserBodyResponse body = new AuthenticationUserBodyResponse();

            AuthenticationUserDataResponse userData = new AuthenticationUserDataResponse();
            userData.setFirstName(authenticationResponse.getUser().getPersonalData().getFirstName());
            userData.setLastName(authenticationResponse.getUser().getPersonalData().getLastName());
            userData.setFiscalCode(authenticationResponse.getUser().getPersonalData().getFiscalCode());

            Calendar calendar = new GregorianCalendar();
            calendar.setTime(authenticationResponse.getUser().getPersonalData().getBirthDate());

            CustomDate dateOfBirth = new CustomDate();
            dateOfBirth.setYear(calendar.get(Calendar.YEAR));
            dateOfBirth.setMonth(calendar.get(Calendar.MONTH) + 1);
            dateOfBirth.setDay(calendar.get(Calendar.DAY_OF_MONTH));
            userData.setDateOfBirth(dateOfBirth);

            userData.setBirthMunicipality(authenticationResponse.getUser().getPersonalData().getBirthMunicipality());
            userData.setBirthProvince(authenticationResponse.getUser().getPersonalData().getBirthProvince());
            userData.setLanguage(authenticationResponse.getUser().getPersonalData().getLanguage());
            userData.setSex(authenticationResponse.getUser().getPersonalData().getSex());

            EmailSecurityData securityData = new EmailSecurityData();
            securityData.setEmail(authenticationResponse.getUser().getPersonalData().getSecurityDataEmail());
            securityData.setStatus(1);
            userData.setSecurityData(securityData);

            Date now = new Date();

            ContactData contactData = new ContactData();
            for (MobilePhone mobilePhone : authenticationResponse.getUser().getMobilePhoneList()) {

                mobilePhone.setCreationTimestamp(null);
                mobilePhone.setLastUsedTimestamp(null);
                mobilePhone.setVerificationCode(null);

                if (mobilePhone.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_PENDING) {

                    // Restituisci i metodi in stato pending che non sono scaduti
                    if (mobilePhone.getExpirationTimestamp().getTime() > now.getTime()) {
                        //System.out.println("Restituito");
                        mobilePhone.setExpirationTimestamp(null);
                        contactData.getMobilePhones().add(mobilePhone);
                    }
                }

                if (mobilePhone.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_ACTIVE) {

                    // Restituisci i numeri attivi (dovrebbe essercene solo uno)
                    mobilePhone.setExpirationTimestamp(null);
                    contactData.getMobilePhones().add(mobilePhone);
                }

            }

            userData.setContactData(contactData);

            LastLoginData lastLoginData = new LastLoginData();

            String lastLoginDevice = "";
            if (authenticationResponse.getUser().getLastLoginData() != null) {
                lastLoginDevice = authenticationResponse.getUser().getLastLoginData().getDeviceName();
            }

            Timestamp lastLoginTime = null;
            if (authenticationResponse.getUser().getLastLoginData() != null) {
                lastLoginTime = authenticationResponse.getUser().getLastLoginData().getTime();
            }

            lastLoginData.setLastLoginDevice(lastLoginDevice);
            lastLoginData.setLastLoginTime(CustomTimestamp.createCustomTimestamp(lastLoginTime));
            userData.setLastLoginData(lastLoginData);

            PaymentData paymentData = new PaymentData();

            if (authenticationResponse.getUser().getPaymentData() != null) {
                
                // Inserimento del metodo di default in prima posizione
                for (PaymentInfo paymentInfo : authenticationResponse.getUser().getPaymentData()) {

                    PaymentMethod paymentMethod = null;
                    if (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED
                            || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                        if (paymentInfo.getDefaultMethod().equals(Boolean.TRUE)) {
                            
                            paymentMethod = new PaymentMethod();
    
                            paymentMethod.setId(paymentInfo.getId());
                            paymentMethod.setType(paymentInfo.getType());
                            paymentMethod.setBrand(paymentInfo.getBrand());
                            paymentMethod.setExpirationDate(CustomDate.createCustomDate(paymentInfo.getExpirationDate()));
                            paymentMethod.setIdentifier(paymentInfo.getPan());
                            paymentMethod.setStatus(paymentInfo.getStatus());
                            paymentMethod.setDefaultMethod(paymentInfo.getDefaultMethod());
    
                            if (paymentInfo.getInsertTimestamp() != null) {
                                paymentMethod.setInsertDate(CustomTimestamp.createCustomTimestamp(new Timestamp(paymentInfo.getInsertTimestamp().getTime())));
                            }
                            else {
                                paymentMethod.setInsertDate(null);
                            }
    
                            paymentData.getPaymentMethodList().add(paymentMethod);
                            
                            break;
                        }
                    }
                }
                
            // Inserimento degli altri metodi di pagamento
                for (PaymentInfo paymentInfo : authenticationResponse.getUser().getPaymentData()) {

                    PaymentMethod paymentMethod = null;
                    if (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED
                            || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                        if (paymentInfo.getDefaultMethod().equals(Boolean.FALSE)) {
                            
                            paymentMethod = new PaymentMethod();
    
                            paymentMethod.setId(paymentInfo.getId());
                            paymentMethod.setType(paymentInfo.getType());
                            paymentMethod.setBrand(paymentInfo.getBrand());
                            paymentMethod.setExpirationDate(CustomDate.createCustomDate(paymentInfo.getExpirationDate()));
                            paymentMethod.setIdentifier(paymentInfo.getPan());
                            paymentMethod.setStatus(paymentInfo.getStatus());
                            paymentMethod.setDefaultMethod(paymentInfo.getDefaultMethod());
    
                            if (paymentInfo.getInsertTimestamp() != null) {
                                paymentMethod.setInsertDate(CustomTimestamp.createCustomTimestamp(new Timestamp(paymentInfo.getInsertTimestamp().getTime())));
                            }
                            else {
                                paymentMethod.setInsertDate(null);
                            }
    
                            paymentData.getPaymentMethodList().add(paymentMethod);
                        }
                    }
                }
            }

            paymentData.setResidualCap(AmountConverter.toMobile(authenticationResponse.getUser().getCapEffective()));

            if (authenticationResponse.getUser().getUseVoucher() == null) {
                paymentData.setUseVoucher(false);
            }
            else {
                paymentData.setUseVoucher(authenticationResponse.getUser().getUseVoucher());
            }

            userData.setPaymentData(paymentData);

            UserStatus userStatus = new UserStatus();
            userStatus.setStatus(authenticationResponse.getUser().getUserStatus());
            userStatus.setRegistrationCompleted(authenticationResponse.getUser().getUserStatusRegistrationCompleted());
            userStatus.setSourceToken(authenticationResponse.getSourceToken());
            
            if (source != null && source.equals("ENISTATION+")) {
                
                body.getNotEditableFields().add("email");
                body.getNotEditableFields().add("fiscalCode");
                body.getNotEditableFields().add("firstName");
                body.getNotEditableFields().add("lastName");
                body.getNotEditableFields().add("sex");
                body.getNotEditableFields().add("birthCity");
                body.getNotEditableFields().add("birthMunicipality");
                body.getNotEditableFields().add("dateOfBirth");
                body.getNotEditableFields().add("phoneNumber");
            }
            
            Boolean termsOfServiceAccepted = Boolean.TRUE;
            for (TermsOfService termsOfService : authenticationResponse.getUser().getPersonalData().getTermsOfServiceData()) {

                if (termsOfService.getValid() == null || !termsOfService.getValid()) {
                    termsOfServiceAccepted = Boolean.FALSE;
                    //System.out.println("found termsOfService " + termsOfService.getKeyval() + " no valid");
                    break;
                }
            }
            
            boolean paymentMethodFound = false;
            
            if (authenticationResponse.getUser().getSourcePaymentMethodFound() != null) {
                paymentMethodFound = authenticationResponse.getUser().getSourcePaymentMethodFound();
            }
            
            userStatus.setSourcePaymentMethodFound(paymentMethodFound);
            userStatus.setTermsOfServiceAccepted(termsOfServiceAccepted);

            if (authenticationResponse.getUser().getDepositCardStepCompleted() != null) {
                userStatus.setDepositCardStepCompleted(authenticationResponse.getUser().getDepositCardStepCompleted());
            }
            
            String type = "BUSINESS";
            userStatus.setType(type);
            userStatus.setSource(authenticationResponse.getUser().getSource());
            userData.setUserStatus(userStatus);
            
            if (authenticationResponse.getUser().getPersonalDataBusinessList() != null && authenticationResponse.getUser().getPersonalDataBusinessList().size() > 0) {
                PersonalDataBusiness personalDataBusiness = authenticationResponse.getUser().getPersonalDataBusinessList().get(0);
                
                BusinessData businessData = new BusinessData();
                BillingAddressData businessBillingAddressData = new BillingAddressData();
                businessBillingAddressData.setAddress(personalDataBusiness.getAddress());
                businessBillingAddressData.setCity(personalDataBusiness.getCity());
                businessBillingAddressData.setProvince(personalDataBusiness.getProvince());
                businessBillingAddressData.setZipCode(personalDataBusiness.getZipCode());
                businessBillingAddressData.setStreetNumber(personalDataBusiness.getStreetNumber());
                businessData.setBillingAddress(businessBillingAddressData);
                
                businessData.setBusinessName(personalDataBusiness.getBusinessName());
                businessData.setPecEmail(personalDataBusiness.getPecEmail());
                businessData.setSdiCode(personalDataBusiness.getSdiCode());
                businessData.setVatNumber(personalDataBusiness.getVatNumber());
                businessData.setFiscalCode(personalDataBusiness.getFiscalCode());
                businessData.setLicensePlate(personalDataBusiness.getLicensePlate());
                
                userData.setBusinessData(businessData);
            }
            
            if (!authenticationResponse.getUser().getPlateNumberList().isEmpty()) {
                for(PlateNumber plateNumber : authenticationResponse.getUser().getPlateNumberList()) {
                    PlateNumberInfo plateNumberInfo = new PlateNumberInfo();
                    plateNumberInfo.setId(plateNumber.getId());
                    plateNumberInfo.setPlateNumber(plateNumber.getPlateNumber());
                    plateNumberInfo.setDescription(plateNumber.getDescription());
                    plateNumberInfo.setDefaultPlateNumber(plateNumber.getDefaultPlateNumber());
                    userData.getPlateNumberList().add(plateNumberInfo);
                }
            }
            
            body.setTicketID(authenticationResponse.getTicketId());
            body.setUserData(userData);

            authenticationUserResponseSuccess.setBody(body);

            return authenticationUserResponseSuccess;
        }
        else {
            AuthenticationUserResponseFailure authenticationUserResponseFailure = new AuthenticationUserResponseFailure();

            status.setStatusCode(authenticationResponse.getStatusCode());
            status.setStatusMessage(prop.getProperty(authenticationResponse.getStatusCode()));

            authenticationUserResponseFailure.setStatus(status);

            return authenticationUserResponseFailure;
        }
    }

}
