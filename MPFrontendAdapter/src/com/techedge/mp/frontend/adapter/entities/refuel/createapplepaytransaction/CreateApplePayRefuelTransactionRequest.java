package com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction;

import java.util.Date;

import com.techedge.mp.bpel.operation.refuel.business.interfaces.StationInfoRequest;
import com.techedge.mp.core.business.interfaces.CreateApplePayRefuelResponse;
import com.techedge.mp.core.business.interfaces.OperationType;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetPumpStatusResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class CreateApplePayRefuelTransactionRequest extends AbstractRequest implements Validable {

    private Status                                     status = new Status();

    private Credential                                 credential;
    private CreateApplePayRefuelTransactionBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateApplePayRefuelTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(CreateApplePayRefuelTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("APPLE-PAY-CREATE-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String response = "";

        Double maxAmount = null;
        Double thresholdAmount = null;

        response = getUserServiceRemote().checkAuthorization(this.getCredential().getTicketID(), OperationType.CREATE_TRANSACTION);

        if (!response.equals(ResponseHelper.CHECK_AUTHORIZATION_SUCCESS)) {

            status.setStatusCode(response);
            status.setStatusMessage(prop.getProperty(response));
        }
        else {

            String stationID = this.getBody().getApplePayRefuelPaymentData().getStationID();
            String pumpID = this.getBody().getApplePayRefuelPaymentData().getPumpID();
            String requestID = String.valueOf(new Date().getTime());
            String outOfRange = this.getBody().getApplePayRefuelPaymentData().getOutOfRange();
            String applePayPKPaymentToken = this.getBody().getApplePayRefuelPaymentData().getApplePayPKPaymentToken();

            if (stationID != null && pumpID != null) {

                // String pumpStatusCode =
                // forecourtService.getPumpStatus(stationID,
                // pumpID);

                StationInfoRequest stationInfoRequest = new StationInfoRequest();

                stationInfoRequest.setRequestID(requestID);
                stationInfoRequest.setStationID(stationID);
                stationInfoRequest.setPumpID(pumpID);
                stationInfoRequest.setPumpDetailsReq(false);

                GetStationDetailsResponse stationInfoResponse = getForecourtInfoServiceRemote().getStationDetails(requestID, stationID, pumpID, false);

                String stationStatusCode = stationInfoResponse.getStatusCode();

                if (Validator.isValid(stationStatusCode)) {

                    if (!stationInfoResponse.getStationDetail().getPumpDetails().isEmpty()) {

                        GetPumpStatusResponse pumpStatusResponse = getForecourtInfoServiceRemote().getPumpStatus(requestID, stationID, pumpID, null);

                        if (Validator.isValid(pumpStatusResponse.getStatusCode())) {

                            // Il processo di rifornimento viene avviato solo se la pompa � disponibile

                            PumpDetail pumpSelected = null;

                            for (PumpDetail pumpDetail : stationInfoResponse.getStationDetail().getPumpDetails()) {

                                pumpSelected = pumpDetail;
                            }

                            Integer pumpNumber = 0;
                            try {
                                pumpNumber = Integer.parseInt(pumpSelected.getPumpNumber());
                            }
                            catch (Exception ex) {

                                System.out.println("Pump number parsing error: " + pumpSelected.getPumpNumber());
                            }

                            String productID = "";
                            String productDescription = "";

                            String refuelMode = pumpStatusResponse.getRefuelMode();

                            Double amount = AmountConverter.toInternal(this.getBody().getApplePayRefuelPaymentData().getAmount());
                            Double amountVoucher = 0.0;

                            if (this.getBody().getApplePayRefuelPaymentData().getAmountVoucher() != null) {
                                amountVoucher = AmountConverter.toInternal(this.getBody().getApplePayRefuelPaymentData().getAmountVoucher());
                            }

                            System.out.println("createRefuel");
                            System.out.println("stationID: " + stationID);
                            System.out.println("pumpID: " + pumpID);
                            System.out.println("pumpNumber: " + pumpNumber);
                            System.out.println("productID: " + productID);
                            System.out.println("productDescription: " + productDescription);
                            System.out.println("amount: " + amount);
                            System.out.println("amountVoucher: " + amountVoucher);
                            System.out.println("applePayPKPaymentToken: " + applePayPKPaymentToken);
                            System.out.println("outOfRange: " + outOfRange);
                            System.out.println("refuelMode: " + refuelMode);

                            CreateApplePayRefuelResponse createApplePayRefuelResponse = getTransactionServiceRemote().createApplePayRefuel(this.getCredential().getTicketID(),
                                    this.getCredential().getRequestID(), stationID, pumpID, pumpNumber, productID, productDescription, amount, amountVoucher,
                                    this.getBody().getApplePayRefuelPaymentData().getUseVoucher(), applePayPKPaymentToken, outOfRange, refuelMode);

                            if (createApplePayRefuelResponse.getTransactionID() != null) {

                                // call verso il BPEL

                                System.out.println("BPEL starting");

                                //System.out.println("RESPONSE SHOPLOGIN: " + createRefuelResponse.getShopLogin());
                                //System.out.println("RESPONSE ACQUIRER_ID: " + createRefuelResponse.getAcquirerID());

                                String tokenValue = createApplePayRefuelResponse.getTokenValue();
                                if (tokenValue == null) {
                                    tokenValue = "";
                                }

                                response = getBpelServiceRemote().startRefuelingProcess(createApplePayRefuelResponse.getTransactionID(), stationID, pumpID,
                                        AmountConverter.toInternal(this.getBody().getApplePayRefuelPaymentData().getAmount()), createApplePayRefuelResponse.getAcquirerID(),
                                        createApplePayRefuelResponse.getShopLogin(), createApplePayRefuelResponse.getCurrency(), createApplePayRefuelResponse.getTransactionID(),
                                        tokenValue, "", productDescription, productID);

                                System.out.println("BPEL response: " + response);

                                if (Validator.isValid(response)) {

                                    status.setStatusCode(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_SUCCESS);
                                    status.setStatusMessage(prop.getProperty(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_SUCCESS));
                                }
                                else {

                                    status.setStatusCode(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_SUCCESS);
                                    status.setStatusMessage(prop.getProperty(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_SUCCESS));
                                }
                            }
                            else {

                                maxAmount = createApplePayRefuelResponse.getMaxAmount();
                                thresholdAmount = createApplePayRefuelResponse.getThresholdAmount();

                                status.setStatusCode(createApplePayRefuelResponse.getStatusCode());
                                status.setStatusMessage(prop.getProperty(createApplePayRefuelResponse.getStatusCode()));
                            }
                        }
                        else {

                            status.setStatusCode(pumpStatusResponse.getStatusCode());
                            status.setStatusMessage(prop.getProperty(pumpStatusResponse.getStatusCode()));
                        }
                    }
                    else {

                        status.setStatusCode(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_PUMP_NOT_FOUND);
                        status.setStatusMessage(prop.getProperty(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_PUMP_NOT_FOUND));
                    }
                }
                else {

                    status.setStatusCode(stationStatusCode);
                    status.setStatusMessage(prop.getProperty(stationStatusCode));
                }
            }
            else {

                status.setStatusCode(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_DATA_WRONG);
                status.setStatusMessage(prop.getProperty(StatusCode.APPLE_PAY_REFUEL_TRANSACTION_CREATE_DATA_WRONG));
            }
        }

        CreateApplePayRefuelTransactionResponse createApplePayRefuelTransactionResponse = new CreateApplePayRefuelTransactionResponse();

        CreateApplePayRefuelTransactionBodyResponse createApplePayRefuelTransactionBodyResponse = new CreateApplePayRefuelTransactionBodyResponse();

        if (maxAmount != null) {
            Integer integerMaxAmount = AmountConverter.toMobile(maxAmount);
            createApplePayRefuelTransactionBodyResponse.setMaxAmount(integerMaxAmount);
        }
        if (thresholdAmount != null) {
            Integer integerThresholdAmount = AmountConverter.toMobile(thresholdAmount);
            createApplePayRefuelTransactionBodyResponse.setThresholdAmount(integerThresholdAmount);
        }

        createApplePayRefuelTransactionResponse.setBody(createApplePayRefuelTransactionBodyResponse);
        createApplePayRefuelTransactionResponse.setStatus(status);

        return createApplePayRefuelTransactionResponse;

    }
}