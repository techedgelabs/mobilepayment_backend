package com.techedge.mp.frontend.adapter.entities.postpaid.retrievetransactionbympcode;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class RetrievePoPTransactionByMPcodeResponse extends BaseResponse {
	
	private RetrievePoPTransactionByMPcodeBodyResponse body;

	public RetrievePoPTransactionByMPcodeBodyResponse getBody() {
		return body;
	}

	public void setBody(RetrievePoPTransactionByMPcodeBodyResponse body) {
		this.body = body;
	}


}
