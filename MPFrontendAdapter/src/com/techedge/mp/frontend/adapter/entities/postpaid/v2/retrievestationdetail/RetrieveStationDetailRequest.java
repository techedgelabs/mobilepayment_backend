package com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievestationdetail;

import java.text.DecimalFormat;

import com.techedge.mp.core.business.interfaces.CashInfo;
import com.techedge.mp.core.business.interfaces.ExtendedPumpInfo;
import com.techedge.mp.core.business.interfaces.GetStationInfo;
import com.techedge.mp.core.business.interfaces.GetStationResponse;
import com.techedge.mp.core.business.interfaces.OperationType;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Cash;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.StationLocationData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveStationDetailRequest extends AbstractRequest implements Validable {

    private Credential                 credential;
    private RetrieveStationDetailBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveStationDetailBodyRequest getBody() {
        return body;
    }

    public void setBody(RetrieveStationDetailBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("STATION-RETRIEVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        RetrieveStationDetailRequest retrieveStationDetailRequest = this;

        RetrieveStationDetailResponse retrieveStationDetailResponse = new RetrieveStationDetailResponse();

        Status statusResponse = new Status();
        
        Boolean refresh = retrieveStationDetailRequest.getBody().getRefresh();
        
        // Disattivazione verifica di prossimit� in caso di refresh == false
        if (refresh != null && refresh == Boolean.FALSE) {
            
            System.out.println("Rilevato parametro refresh = false -> disattivazione controllo di prossimit� per caricamento punti da POS");
            
            RetrieveStationDetailBodyResponse retrieveStationBodyResponse = new RetrieveStationDetailBodyResponse();

            RetrieveStationDetailBodyStationResponse station = new RetrieveStationDetailBodyStationResponse();

            station.setStationID("00000");

            String address = "address";
            String city = "city";
            String province = "XX";
            String country = "Italia";

            LocationData locationData = new LocationData();
            locationData.setAddress(address);
            locationData.setCity(city);
            locationData.setProvince(province);
            locationData.setCountry(country);
            locationData.setLatitude(0.0);
            locationData.setLongitude(0.0);
            
            station.setLocationData(locationData);
            
            /*
            for (ExtendedPumpInfo extendedPumpInfo : stationInfo.getPumpList()) {
                
                Pump pump = new Pump();

                pump.setPumpID(extendedPumpInfo.getPumpId());
                pump.setStatus(null);
                pump.setNumber(new Integer(extendedPumpInfo.getNumber()));

                for (String fuelType : extendedPumpInfo.getFuelType()) {

                    pump.getFuelType().add(fuelType);
                }
                
                pump.setRefuelMode(extendedPumpInfo.getRefuelMode());

                station.getPumpList().add(pump);

            }

            for (CashInfo cashInfo : stationInfo.getCashList()) {

                Cash cash = new Cash();

                cash.setCashID(cashInfo.getCashId());
                cash.setNumber(new Integer(cashInfo.getNumber()));

                station.getCashList().add(cash);

            }
            */      
            
            //station.getEnabledServices().add(Station.PAYMENT_SERVICE);
            station.getEnabledServices().add(Station.LOYALTY_SERVICE);
            
            retrieveStationBodyResponse.getStations().add(station);
            
            retrieveStationBodyResponse.setOutOfRange("false");
            
            retrieveStationDetailResponse.setBody(retrieveStationBodyResponse);

            statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_RETRIEVE_SUCCESS));

            retrieveStationDetailResponse.setStatus(statusResponse);

            return retrieveStationDetailResponse;
        }
        
        
        // Verifica se l'utente pu� effettuare una richiesta al servizio
        // retrieveStation
        String response = getUserServiceRemote().checkAuthorization(retrieveStationDetailRequest.getCredential().getTicketID(), OperationType.RETRIEVE_STATION);

        if (!response.equals(ResponseHelper.CHECK_AUTHORIZATION_SUCCESS)) {

            statusResponse.setStatusCode(response);
            statusResponse.setStatusMessage(prop.getProperty(response));
        }
        else {

            Double userPositionLatitude = null;
            Double userPositionLongitude = null;

            if (retrieveStationDetailRequest.getBody().getUserPosition() != null) {

                userPositionLatitude = retrieveStationDetailRequest.getBody().getUserPosition().getLatitude();
                userPositionLongitude = retrieveStationDetailRequest.getBody().getUserPosition().getLongitude();
            }
            
            refresh = retrieveStationDetailRequest.getBody().getRefresh();
            
            if (refresh == null) {
                
                // Se il campo refresh non � valorizzato utilizza il valore di default TRUE
                refresh = Boolean.TRUE;
            }
            
            // Se valorizzato, formatta lo stationID aggiungendo gli 0 a sinistra
            String stationID = retrieveStationDetailRequest.getBody().getStationID();
            if (stationID != null) {
                System.out.println("StationID before padding: " + stationID);
                stationID = String.format("%05d", Integer.parseInt(stationID));
                System.out.println("StationID after padding:  " + stationID);
            }
            
            GetStationResponse getStationsResponse = getPostPaidV2TransactionServiceRemote().getStation(retrieveStationDetailRequest.getCredential().getRequestID(),
                    retrieveStationDetailRequest.getCredential().getTicketID(), userPositionLatitude, userPositionLongitude,
                    stationID, retrieveStationDetailRequest.getBody().getClearTransaction(), refresh );

            if (!getStationsResponse.getStatusCode().equals(ResponseHelper.PP_TRANSACTION_GETSTATIONS_SUCCESS)) {

                statusResponse.setStatusCode(getStationsResponse.getStatusCode());
                statusResponse.setStatusMessage(prop.getProperty(getStationsResponse.getStatusCode()));
            }
            else {

                RetrieveStationDetailBodyResponse retrieveStationBodyResponse = new RetrieveStationDetailBodyResponse();

                if (!getStationsResponse.getStationInfoList().isEmpty()) {

                    for (GetStationInfo stationInfo : getStationsResponse.getStationInfoList()) {

                        //System.out.println("station: " + stationInfo.getStationId());
                        //System.out.println("distance: " + stationInfo.getDistance());

                        RetrieveStationDetailBodyStationResponse station = new RetrieveStationDetailBodyStationResponse();

                        station.setStationID(stationInfo.getStationId());

                        LocationData locationData = composeLocatinData(stationInfo);
                        station.setLocationData(locationData);

                        for (ExtendedPumpInfo extendedPumpInfo : stationInfo.getPumpList()) {
                            
                            Pump pump = new Pump();

                            pump.setPumpID(extendedPumpInfo.getPumpId());
                            pump.setStatus(null);
                            pump.setNumber(new Integer(extendedPumpInfo.getNumber()));

                            for (String fuelType : extendedPumpInfo.getFuelType()) {

                                pump.getFuelType().add(fuelType);
                            }
                            
                            pump.setRefuelMode(extendedPumpInfo.getRefuelMode());

                            station.getPumpList().add(pump);

                        }

                        for (CashInfo cashInfo : stationInfo.getCashList()) {

                            Cash cash = new Cash();

                            cash.setCashID(cashInfo.getCashId());
                            cash.setNumber(new Integer(cashInfo.getNumber()));

                            station.getCashList().add(cash);

                        }
                        
                        //System.out.println("NewAcquirerEnabled: " + stationInfo.getNewAcquirerEnabled());
                        
                        if(stationInfo.getNewAcquirerEnabled()) {
                            station.getEnabledServices().add(Station.PAYMENT_SERVICE);
                        }
                        
                        //System.out.println("LoyaltyEnabled: " + stationInfo.getLoyaltyEnabled());
                        
                        if(stationInfo.getLoyaltyEnabled()) {
                            station.getEnabledServices().add(Station.LOYALTY_SERVICE);
                        }
                        
                        //System.out.println("BusinessEnabled: " + stationInfo.getBusinessEnabled());
                        
                        if(stationInfo.getBusinessEnabled()) {
                            station.getEnabledServices().add(Station.BUSINESS_SERVICE);
                        }

                        retrieveStationBodyResponse.getStations().add(station);

                    }

                }
                
                if (getStationsResponse.getOutOfRange()) {
                    retrieveStationBodyResponse.setOutOfRange("true");
                }
                else {
                    retrieveStationBodyResponse.setOutOfRange("false");
                }

                retrieveStationDetailResponse.setBody(retrieveStationBodyResponse);

                statusResponse.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);
                statusResponse.setStatusMessage(prop.getProperty(StatusCode.STATION_RETRIEVE_SUCCESS));

                retrieveStationDetailResponse.setStatus(statusResponse);
            }
        }

        retrieveStationDetailResponse.setStatus(statusResponse);

        return retrieveStationDetailResponse;
    }

    private StationLocationData composeLocatinData(GetStationInfo stationInfo) {

        StationLocationData stationLocationData = new StationLocationData();

        String address = stationInfo.getAddress();
        String city = stationInfo.getCity();
        String province = stationInfo.getProvince();
        String country = stationInfo.getCountry();

        stationLocationData.setAddress(address);
        stationLocationData.setCity(city);
        stationLocationData.setProvince(province);
        stationLocationData.setCountry(country);

        try {
            stationLocationData.setLatitude(new Double(stationInfo.getLatitude()));
            stationLocationData.setLongitude(new Double(stationInfo.getLongitude()));
        }
        catch (Exception ex) {

            stationLocationData.setLatitude(0.0);
            stationLocationData.setLongitude(0.0);
        }

        if (stationInfo.getDistance() != null) {
            stationLocationData.setDistance(formatDouble(stationInfo.getDistance()));
        }
        else {
            stationLocationData.setDistance(null);
        }

        return stationLocationData;
    }

    private Double formatDouble(Double distance) {

        DecimalFormat formatter = new DecimalFormat("#0.00");

        //System.out.println("distance: " + distance);

        String doubleString = formatter.format(distance);
        String replaced = doubleString.replace(",", ".");

        Double distanceFormatted = Double.parseDouble(replaced);

        return distanceFormatted;
    }
}
