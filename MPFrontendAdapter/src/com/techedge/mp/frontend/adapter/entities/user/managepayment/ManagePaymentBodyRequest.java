package com.techedge.mp.frontend.adapter.entities.user.managepayment;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class ManagePaymentBodyRequest implements Validable {

	
	private Integer requestType;
	

	public int getRequestType() {
		return requestType;
	}

	public void setRequestType(int requestType) {
		this.requestType = requestType;
	}

	
	@Override
	public Status check() {

		Status status = new Status();
		
		if(this.requestType == null || ( this.requestType != 0 && this.requestType != 1 && this.requestType != 2 ) ) {
			
			//status.setStatusCode(StatusCode.PAYMENT_MANAGE_REQUEST_TYPE_WRONG);
			
		}
		
		//status.setStatusCode(StatusCode.PAYMENT_MANAGE_SUCCESS);
		
		return status;
		
	}
	
}
