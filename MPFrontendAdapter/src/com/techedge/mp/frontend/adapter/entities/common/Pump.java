package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;


public class Pump {

	private String pumpID;
	private String status;
    private Integer number;
    private String refuelMode;
    private List<String> fuelType = new ArrayList<String>(0);
	
    public String getPumpID() {
		return pumpID;
	}
	public void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	
	public List<String> getFuelType() {
		return fuelType;
	}
	public void setFuelType(List<String> fuelType) {
		this.fuelType = fuelType;
	}
	
    public String getRefuelMode() {
        return refuelMode;
    }
    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }
}
