package com.techedge.mp.frontend.adapter.entities.user.updatepassword;

import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UpdatePasswordRequestBody implements Validable {

	
	private String oldPassword;
	private String newPassword;
	
	
	public String getOldPassword() {
		return oldPassword;
	}
	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}
	
	public String getNewPassword() {
		return newPassword;
	}
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}
	
	@Override
	public Status check() {
		
		Status status = new Status();
		
		status = Validator.checkPassword("PWD", newPassword);
		
		if (!Validator.isValid(status.getStatusCode())) {
			
			return status;
		}
		
		
		if(oldPassword == null || oldPassword.trim().equals("")) {
		
			status.setStatusCode(StatusCode.USER_PWD_OLD_PASSWORD_WRONG);
			
			return status;
		}
		
		status.setStatusCode(StatusCode.USER_PWD_SUCCESS);
		
		return status;
	}
	
	
}
