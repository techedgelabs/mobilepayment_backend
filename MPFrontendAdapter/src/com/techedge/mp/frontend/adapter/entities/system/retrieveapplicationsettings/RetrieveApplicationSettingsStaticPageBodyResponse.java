package com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings;

public class RetrieveApplicationSettingsStaticPageBodyResponse {

    private String faq;
    /*private String landingHome;
    private String landingRegistration;
    private String landingHomeTitle;
    private String landingRegistrationTitle;
    private String tutorial;*/
    private String redemptionUrl;
    private String infoTokenUrl;
    private String voucherUrl;
    private String loyaltyUrl;
    private String missionUrl;
    private String stationFinderUrl;
    private String firstStartUrl;
    private String captchaUrl;

    public String getFaq() {
        return faq;
    }

    public void setFaq(String faq) {
        this.faq = faq;
    }

    /*public String getLandingHome() {
        return landingHome;
    }

    public void setLandingHome(String landingHome) {
        this.landingHome = landingHome;
    }

    public String getLandingRegistration() {
        return landingRegistration;
    }

    public void setLandingRegistration(String landingRegistration) {
        this.landingRegistration = landingRegistration;
    }

    public String getLandingHomeTitle() {
        return landingHomeTitle;
    }

    public void setLandingHomeTitle(String landingHomeTitle) {
        this.landingHomeTitle = landingHomeTitle;
    }

    public String getLandingRegistrationTitle() {
        return landingRegistrationTitle;
    }

    public void setLandingRegistrationTitle(String landingRegistrationTitle) {
        this.landingRegistrationTitle = landingRegistrationTitle;
    }

    public String getTutorial() {
        return tutorial;
    }

    public void setTutorial(String tutorial) {
        this.tutorial = tutorial;
    }*/

    public String getRedemptionUrl() {
        return redemptionUrl;
    }

    public void setRedemptionUrl(String redemptionUrl) {
        this.redemptionUrl = redemptionUrl;
    }

    public String getInfoTokenUrl() {
        return infoTokenUrl;
    }

    public void setInfoTokenUrl(String infoTokenUrl) {
        this.infoTokenUrl = infoTokenUrl;
    }

    public String getVoucherUrl() {
        return voucherUrl;
    }

    public void setVoucherUrl(String voucherUrl) {
        this.voucherUrl = voucherUrl;
    }

    public String getLoyaltyUrl() {
        return loyaltyUrl;
    }

    public void setLoyaltyUrl(String loyaltyUrl) {
        this.loyaltyUrl = loyaltyUrl;
    }

    public String getMissionUrl() {
        return missionUrl;
    }

    public void setMissionUrl(String missionUrl) {
        this.missionUrl = missionUrl;
    }

    public String getStationFinderUrl() {
        return stationFinderUrl;
    }

    public void setStationFinderUrl(String stationFinderUrl) {
        this.stationFinderUrl = stationFinderUrl;
    }

    public String getFirstStartUrl() {
        return firstStartUrl;
    }

    public void setFirstStartUrl(String firstStartUrl) {
        this.firstStartUrl = firstStartUrl;
    }

    public String getCaptchaUrl() {
        return captchaUrl;
    }

    public void setCaptchaUrl(String captchaUrl) {
        this.captchaUrl = captchaUrl;
    }

    
}
