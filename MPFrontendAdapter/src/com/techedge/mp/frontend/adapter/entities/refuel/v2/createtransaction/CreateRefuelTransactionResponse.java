package com.techedge.mp.frontend.adapter.entities.refuel.v2.createtransaction;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class CreateRefuelTransactionResponse extends BaseResponse {

	private CreateRefuelTransactionBodyResponse body;

	public CreateRefuelTransactionBodyResponse getBody() {
		return body;
	}
	public void setBody(CreateRefuelTransactionBodyResponse body) {
		this.body = body;
	}
}
