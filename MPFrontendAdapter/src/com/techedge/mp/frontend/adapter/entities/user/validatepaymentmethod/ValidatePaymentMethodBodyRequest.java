package com.techedge.mp.frontend.adapter.entities.user.validatepaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ValidatePaymentMethodBodyRequest implements Validable {
	
	private PaymentMethod paymentMethod;
	private Integer verificationAmount;

	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	public Integer getVerificationAmount() {
		return verificationAmount;
	}
	public void setVerificationAmount(Integer verificationAmount) {
		this.verificationAmount = verificationAmount;
	}


	@Override
	public Status check() {

		Status status = new Status();
		
		if(this.paymentMethod == null) {

			status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);
	
			return status;

		}
		
		if(this.paymentMethod.getId() == null) {

			status.setStatusCode(StatusCode.USER_VALIDATE_PAYMENT_METHOD_ID_ERROR);
	
			return status;

		}

		if(this.paymentMethod.getType() == null || this.paymentMethod.getType().trim().length() > 50) {

			status.setStatusCode(StatusCode.USER_VALIDATE_PAYMENT_METHOD_TYPE_ERROR);
	
			return status;

		}

		if(this.verificationAmount == null || this.verificationAmount < 0) {

			status.setStatusCode(StatusCode.USER_VALIDATE_PAYMENT_METHOD_AMOUNT_ERROR);
	
			return status;

		}

		status.setStatusCode(StatusCode.USER_VALIDATE_PAYMENT_METHOD_SUCCESS);

		return status;
	}
	
	
}
