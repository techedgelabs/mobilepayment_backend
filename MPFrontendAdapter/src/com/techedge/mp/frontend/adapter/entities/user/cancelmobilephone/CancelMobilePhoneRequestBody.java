package com.techedge.mp.frontend.adapter.entities.user.cancelmobilephone;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CancelMobilePhoneRequestBody implements Validable {

    private Long mobilePhoneId;

    public Long getMobilePhoneId() {
        return mobilePhoneId;
    }

    public void setMobilePhoneId(Long mobilePhoneId) {
        this.mobilePhoneId = mobilePhoneId;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (mobilePhoneId == null || (mobilePhoneId != null && mobilePhoneId.equals(""))) {

            status.setStatusCode(StatusCode.CANCEL_MOBILE_PHONE_INVALID_PARAMETERS);
            return status;

        }

        status.setStatusCode(StatusCode.CANCEL_MOBILE_PHONE_SUCCESS);

        return status;

    }
}
