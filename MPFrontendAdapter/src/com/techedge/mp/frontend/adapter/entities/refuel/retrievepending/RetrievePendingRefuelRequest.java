package com.techedge.mp.frontend.adapter.entities.refuel.retrievepending;

import com.techedge.mp.core.business.interfaces.PendingRefuelResponse;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.LocationData;
import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Receipt;
import com.techedge.mp.frontend.adapter.entities.common.Station;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrievePendingRefuelRequest extends AbstractRequest implements Validable {

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PENDING-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        return status;

    }

    @Override
    public BaseResponse execute() {
        RetrievePendingRefuelRequest retrievePendingRefuel = this;

        PendingRefuelResponse pendingRefuelResponse = getTransactionServiceRemote().retrievePendingRefuel(retrievePendingRefuel.getCredential().getRequestID(),
                retrievePendingRefuel.getCredential().getTicketID());

        RetrievePendingRefuelResponse retrievePendingRefuelResponse = new RetrievePendingRefuelResponse();

        Status statusResponse = new Status();

        statusResponse.setStatusCode(pendingRefuelResponse.getStatusCode());
        statusResponse.setStatusMessage(prop.getProperty(pendingRefuelResponse.getStatusCode()));

        retrievePendingRefuelResponse.setStatus(statusResponse);

        if (pendingRefuelResponse.getStatusCode().equals("REFUEL_PENDING_200")) {

            RetrievePendingRefuelBodyResponse retrievePendingRefuelBodyResponse = new RetrievePendingRefuelBodyResponse();

            retrievePendingRefuelBodyResponse.setRefuelID(pendingRefuelResponse.getRefuelID());
            retrievePendingRefuelBodyResponse.setStatus(pendingRefuelResponse.getStatus());
            retrievePendingRefuelBodyResponse.setSubStatus(pendingRefuelResponse.getSubStatus());
            retrievePendingRefuelBodyResponse.setUseVoucher(pendingRefuelResponse.getUseVoucher());
            retrievePendingRefuelBodyResponse.setInitialAmount(AmountConverter.toMobile(pendingRefuelResponse.getInitialAmount()));
            retrievePendingRefuelBodyResponse.setTimestamp(pendingRefuelResponse.getDate().getTime());

            // Valorizzazione scontrino
            Receipt receipt = new Receipt();
            receipt.setDate(CustomTimestamp.createCustomTimestamp(pendingRefuelResponse.getDate()));
            receipt.setFinalAmount(AmountConverter.toMobile(pendingRefuelResponse.getFinalAmount()));
            receipt.setFuelAmount(AmountConverter.toMobile3(pendingRefuelResponse.getFuelAmount()));
            receipt.setFuelQuantity(pendingRefuelResponse.getFuelQuantity());
            retrievePendingRefuelBodyResponse.setReceipt(receipt);

            // Valorizzazione stazione
            LocationData locationData = new LocationData();

            String address = pendingRefuelResponse.getSelectedStationAddress();
            String city = pendingRefuelResponse.getSelectedStationCity();
            String province = pendingRefuelResponse.getSelectedStationProvince();
            String country = pendingRefuelResponse.getSelectedStationCountry();

            locationData.setAddress(address);
            locationData.setCity(city);
            locationData.setProvince(province);
            locationData.setCountry(country);
            locationData.setLatitude(pendingRefuelResponse.getSelectedStationLatitude());
            locationData.setLongitude(pendingRefuelResponse.getSelectedStationLongitude());

            Station station = new Station();
            station.setStationID(pendingRefuelResponse.getSelectedStationID());
            station.setName(pendingRefuelResponse.getSelectedStationName());
            station.setLocationData(locationData);
            retrievePendingRefuelBodyResponse.setStation(station);

            // Valorizzazione pompa
            Pump pump = new Pump();
            pump.setPumpID(pendingRefuelResponse.getSelectedPumpID());
            pump.setNumber(pendingRefuelResponse.getSelectedPumpNumber());
            pump.getFuelType().add(pendingRefuelResponse.getSelectedPumpFuelType());
            retrievePendingRefuelBodyResponse.setPump(pump);

            retrievePendingRefuelResponse.setBody(retrievePendingRefuelBodyResponse);

        }

        return retrievePendingRefuelResponse;
    }

}
