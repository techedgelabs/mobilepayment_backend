package com.techedge.mp.frontend.adapter.entities.parking.v2.getcities;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetCitiesResponse extends BaseResponse {
    private GetCitiesResponseBody body;

    public GetCitiesResponseBody getBody() {
        return body;
    }

    public void setBody(GetCitiesResponseBody body) {
        this.body = body;
    }

    
}
