package com.techedge.mp.frontend.adapter.entities.postpaid.notifydisplaytransaction;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class NotifyDisplayTransactionRequest extends AbstractRequest implements Validable {

    private Status                              status = new Status();

    private Credential                          credential;
    private NotifyDisplayTransactionBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public NotifyDisplayTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(NotifyDisplayTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CONFIRM-REFUEL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {
//        NotifyDisplayTransactionRequest notifyDisplayPoPTransactionRequest = this;
        NotifyDisplayTransactionResponse notifyDisplayPoPTransactionResponse = new NotifyDisplayTransactionResponse();

        String response = "200";

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        notifyDisplayPoPTransactionResponse.setStatus(status);

        return notifyDisplayPoPTransactionResponse;
    }

}