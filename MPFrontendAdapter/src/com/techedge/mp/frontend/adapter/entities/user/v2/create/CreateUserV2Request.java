package com.techedge.mp.frontend.adapter.entities.user.v2.create;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.Address;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.TermsOfServiceData;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class CreateUserV2Request extends AbstractRequest implements Validable {

    private Status                  status = new Status();

    private Credential              credential;
    private CreateUserV2RequestBody body;

    public CreateUserV2Request() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateUserV2RequestBody getBody() {
        return body;
    }

    public void setBody(CreateUserV2RequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATEV2", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }
        else {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.USER_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        CreateUserV2Request createUserV2Request = this;

        String birthDateSting = createUserV2Request.getBody().getUserData().getDateOfBirth().getYear() + "-"
                + createUserV2Request.getBody().getUserData().getDateOfBirth().getMonth() + "-" + createUserV2Request.getBody().getUserData().getDateOfBirth().getDay();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date birthDate;
        try {
            birthDate = sdf.parse(birthDateSting);
        }
        catch (ParseException e) {
            status = new Status();
            status.setStatusCode(StatusCode.USER_CREATE_DATE_BIRTH_WRONG);
            status.setStatusMessage(prop.getProperty(status.getStatusCode()));
            BaseResponse baseResponse = new BaseResponse();
            baseResponse.setStatus(status);
            return baseResponse;
        }

        User user = new User();

        PersonalData personalData = new PersonalData();

        personalData.setFirstName(createUserV2Request.getBody().getUserData().getFirstName());
        personalData.setLastName(createUserV2Request.getBody().getUserData().getLastName());
        personalData.setFiscalCode(createUserV2Request.getBody().getUserData().getFiscalCode());
        personalData.setBirthDate(birthDate);
        personalData.setBirthMunicipality(createUserV2Request.getBody().getUserData().getBirthMunicipality());
        personalData.setBirthProvince(createUserV2Request.getBody().getUserData().getBirthProvince());
        personalData.setLanguage(createUserV2Request.getBody().getUserData().getLanguage());
        personalData.setSex(createUserV2Request.getBody().getUserData().getSex());
        personalData.setSecurityDataEmail(createUserV2Request.getBody().getUserData().getSecurityData().getEmail());
        personalData.setSecurityDataPassword(createUserV2Request.getBody().getUserData().getSecurityData().getPassword());

        if (createUserV2Request.getBody().getUserData().getAddressData() != null) {
            Address address = new Address();
            address.setCity(createUserV2Request.getBody().getUserData().getAddressData().getCity());
            address.setCountryCodeId(createUserV2Request.getBody().getUserData().getAddressData().getCountryCodeId());
            address.setCountryCodeName(createUserV2Request.getBody().getUserData().getAddressData().getCountryCodeName());
            address.setHouseNumber(createUserV2Request.getBody().getUserData().getAddressData().getHouseNumber());
            address.setRegion(createUserV2Request.getBody().getUserData().getAddressData().getRegion());
            address.setStreet(createUserV2Request.getBody().getUserData().getAddressData().getStreet());
            address.setZipcode(createUserV2Request.getBody().getUserData().getAddressData().getZipCode());
            personalData.setAddress(address);
        }
        else {
            personalData.setAddress(null);
        }

        if (createUserV2Request.getBody().getUserData().getBillingAddressData() != null) {
            Address billingAddress = new Address();
            billingAddress.setCity(createUserV2Request.getBody().getUserData().getBillingAddressData().getCity());
            billingAddress.setCountryCodeId(createUserV2Request.getBody().getUserData().getBillingAddressData().getCountryCodeId());
            billingAddress.setCountryCodeName(createUserV2Request.getBody().getUserData().getBillingAddressData().getCountryCodeName());
            billingAddress.setHouseNumber(createUserV2Request.getBody().getUserData().getBillingAddressData().getHouseNumber());
            billingAddress.setRegion(createUserV2Request.getBody().getUserData().getBillingAddressData().getRegion());
            billingAddress.setStreet(createUserV2Request.getBody().getUserData().getBillingAddressData().getStreet());
            billingAddress.setZipcode(createUserV2Request.getBody().getUserData().getBillingAddressData().getZipCode());
            personalData.setBillingAddress(billingAddress);
        }
        else {
            personalData.setBillingAddress(null);
        }

        if (createUserV2Request.getBody().getUserData().getTermsOfService() != null) {

            for (TermsOfServiceData termsOfServiceData : createUserV2Request.getBody().getUserData().getTermsOfService()) {

                TermsOfService termsOfService = new TermsOfService();
                termsOfService.setKeyval(termsOfServiceData.getId());
                termsOfService.setAccepted(termsOfServiceData.getAccepted());
                termsOfService.setValid(Boolean.TRUE);
                personalData.getTermsOfServiceData().add(termsOfService);
            }
        }

        if (createUserV2Request.getBody().getUserData().getContactData() != null) {

            for (MobilePhone mobilePhone : createUserV2Request.getBody().getUserData().getContactData().getMobilePhones()) {

                user.getMobilePhoneList().add(mobilePhone);
            }
        }

        if (createUserV2Request.getBody().getUserData().getUserStatus() != null) {
            user.setEniStationUserType(createUserV2Request.getBody().getUserData().getUserStatus().isEniStationUserType());
        }

        user.setPersonalData(personalData);
        user.setDeviceId(createUserV2Request.getCredential().getDeviceID());
        
        //Se il campo source � vuoto utilizza il campo type
        if(createUserV2Request.getBody().getUserData().getSource() == null && createUserV2Request.getBody().getUserData().getUserStatus() != null && 
                createUserV2Request.getBody().getUserData().getUserStatus().getType() != null && !createUserV2Request.getBody().getUserData().getUserStatus().getType().equals("CUSTOMER")) {
            
            createUserV2Request.getBody().getUserData().setSource(createUserV2Request.getBody().getUserData().getUserStatus().getType());
        }
        //Nuovo campo source che discrimina il tipo di utente che si sta creando (ENJOY/MYCICERO)
        if(createUserV2Request.getBody().getUserData().getSource()!= null && !createUserV2Request.getBody().getUserData().getSource().isEmpty()){
            user.setSource(createUserV2Request.getBody().getUserData().getSource());
            System.out.println("Source: " + user.getSource());
        }

        Long loyaltyCardId = null;
        CreateUserV2LoyaltyCard createUserLoyaltyCard = createUserV2Request.getBody().getUserData().getLoyaltyCard();
        CreateUserV2Response createUserResponse = new CreateUserV2Response();
        if (createUserLoyaltyCard != null && createUserLoyaltyCard.getId() != null) {
            loyaltyCardId = new Long(createUserLoyaltyCard.getId());
        }

//        if (createUserV2Request.getBody().getUserData().getSocialDataList() != null) {
//
//            for (SocialData socialData : createUserV2Request.getBody().getUserData().getSocialDataList()) {
//
//                UserSocialData userSocialData = new UserSocialData();
//                userSocialData.setProvider(socialData.getProvider());
//                userSocialData.setUser(user);
//                user.getUserSocialData().add(userSocialData);
//            }
//        }

        String response = null;
        
        String accessToken    = createUserV2Request.getBody().getAccessToken();
        String socialProvider = createUserV2Request.getBody().getSocialProvider();

        System.out.println("accessToken:    " + accessToken);
        System.out.println("socialProvider: " + socialProvider);
        
        if (accessToken != null && socialProvider != null)
            response = getUserV2ServiceRemote().createSocialUser(accessToken, socialProvider,
                    createUserV2Request.getCredential().getTicketID(), createUserV2Request.getCredential().getRequestID(), user, loyaltyCardId);
        else
            response = getUserV2ServiceRemote().createUser(createUserV2Request.getCredential().getTicketID(), createUserV2Request.getCredential().getRequestID(), user, loyaltyCardId);
        
        
        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        createUserResponse.setStatus(status);

        return createUserResponse;
    }

}
