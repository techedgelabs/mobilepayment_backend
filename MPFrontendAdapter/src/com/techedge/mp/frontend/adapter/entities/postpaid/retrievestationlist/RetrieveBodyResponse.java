package com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.ActiveCities;
import com.techedge.mp.frontend.adapter.entities.common.Station;

public class RetrieveBodyResponse {

	
	private List<ActiveCities> activeCities = new ArrayList<ActiveCities>(0);
	private List<Station> stations = new ArrayList<Station>(0);

	public List<ActiveCities> getActiveCities() {
	    return activeCities;
	}
	
	public void setActiveCities(List<ActiveCities> activeCities) {
	    this.activeCities = activeCities;
	}
	
	public List<Station> getStations() {
		return stations;
	}
	

    public void setStations(List<Station> stations) {
		this.stations = stations;
	}
	
	
}
