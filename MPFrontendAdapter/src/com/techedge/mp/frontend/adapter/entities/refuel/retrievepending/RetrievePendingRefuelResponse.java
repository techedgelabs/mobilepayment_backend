package com.techedge.mp.frontend.adapter.entities.refuel.retrievepending;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class RetrievePendingRefuelResponse extends BaseResponse {

	
	private RetrievePendingRefuelBodyResponse body;

	
	public RetrievePendingRefuelBodyResponse getBody() {
		return body;
	}

	public void setBody(RetrievePendingRefuelBodyResponse body) {
		this.body = body;
	}
	
	
}
