package com.techedge.mp.frontend.adapter.entities.common.v2;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ParkingCityZoneData implements Serializable {

    private static final long serialVersionUID = 1L;

    private ParkingCity       parkingCity;
    private List<ParkingZone> parkingZones     = new ArrayList<ParkingZone>(0);

    public ParkingCity getParkingCity() {
        return parkingCity;
    }

    public void setParkingCity(ParkingCity parkingCity) {
        this.parkingCity = parkingCity;
    }

    public List<ParkingZone> getParkingZones() {
        return parkingZones;
    }

    public void setParkingZones(List<ParkingZone> parkingZones) {
        this.parkingZones = parkingZones;
    }

}
