package com.techedge.mp.frontend.adapter.entities.postpaid.approvetransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ApproveTransactionBodyRequest implements Validable {

    private String                              transactionID;
    private ApproveTransactionPaymentMethodBody paymentMethod;
    private Boolean                             useVoucher;

    @Override
    public Status check() {

        Status status = new Status();

        if (this.transactionID == null || this.transactionID.length() != 32 || this.transactionID.trim() == "") {

            status.setStatusCode(StatusCode.POP_APPROVE_ID_WRONG);

            return status;

        }

        status.setStatusCode(StatusCode.POP_APPROVE_SUCCESS);

        return status;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public ApproveTransactionPaymentMethodBody getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(ApproveTransactionPaymentMethodBody paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Boolean getUseVoucher() {
        return useVoucher;
    }

    public void setUseVoucher(Boolean useVoucher) {
        this.useVoucher = useVoucher;
    }
}