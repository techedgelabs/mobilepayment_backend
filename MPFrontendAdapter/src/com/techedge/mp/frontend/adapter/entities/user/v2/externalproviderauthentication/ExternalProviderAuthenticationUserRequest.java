package com.techedge.mp.frontend.adapter.entities.user.v2.externalproviderauthentication;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.ExternalProviderAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PlateNumber;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SocialAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.UserSocialData;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransaction;
import com.techedge.mp.core.business.interfaces.parking.RetrievePendingParkingTransactionResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontend.adapter.entities.common.AddressData;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.ContactData;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.EmailSecurityData;
import com.techedge.mp.frontend.adapter.entities.common.LastLoginData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentData;
import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.SocialData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.UserStatus;
import com.techedge.mp.frontend.adapter.entities.common.v2.PlateNumberInfo;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ExternalProviderAuthenticationUserRequest extends AbstractRequest {

	private Status status = new Status();

	private ExternalProviderAuthenticationUserBodyRequest body;

	public ExternalProviderAuthenticationUserBodyRequest getBody() {
		return body;
	}

	public void setBody(ExternalProviderAuthenticationUserBodyRequest body) {
		this.body = body;
	}

	@Override
	public Status check() {

		Status status = new Status();

		if (this.body != null) {

			status = this.body.check();

			if (!Validator.isValid(status.getStatusCode())) {

				return status;

			}
		} else {

			status.setStatusCode(StatusCode.USER_V2_REQU_INVALID_REQUEST);

			return status;
		}

		status.setStatusCode(StatusCode.USER_V2_AUTH_SUCCESS);

		return status;

	}

	@Override
	public BaseResponse execute() {

		ExternalProviderAuthenticationUserRequest externalProviderAuthenticationUserRequest = this;

		String accessToken = externalProviderAuthenticationUserRequest.getBody().getAccessToken();
		String username = externalProviderAuthenticationUserRequest.getBody().getUsername();
		String password = externalProviderAuthenticationUserRequest.getBody().getPassword();
		String externalProviderRequestString = externalProviderAuthenticationUserRequest.getBody().getProvider().toUpperCase();

		ExternalProviderAuthenticationResponse externalProviderAuthenticationResponse = getUserV2ServiceRemote().externalProviderAuthentication(username,password,accessToken, externalProviderRequestString, externalProviderAuthenticationUserRequest.getBody().getRequestID(), externalProviderAuthenticationUserRequest.getBody().getDeviceID(), externalProviderAuthenticationUserRequest.getBody().getDeviceName(), externalProviderAuthenticationUserRequest.getBody().getDeviceToken());

		if (externalProviderAuthenticationResponse == null || externalProviderAuthenticationResponse.getStatusCode() == null) {
		    
		    ExternalProviderAuthenticationUserResponseFailure socialAuthenticationUserResponseFailure = new ExternalProviderAuthenticationUserResponseFailure();
		    
            status.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_FAILURE);
            status.setStatusMessage(prop.getProperty(ResponseHelper.USER_V2_SOCIAL_AUTH_FAILURE));

            socialAuthenticationUserResponseFailure.setStatus(status);

            return socialAuthenticationUserResponseFailure;
		}

	    if (externalProviderAuthenticationResponse.getStatusCode().equals(StatusCode.USER_V2_EXTERNAL_PROVIDER_AUTH_NEW_USER)) {

			ExternalProviderAuthenticationUserResponseSuccess externalProviderAuthenticationUserResponseSuccess = new ExternalProviderAuthenticationUserResponseSuccess();

			status.setStatusCode(externalProviderAuthenticationResponse.getStatusCode());
			status.setStatusMessage(prop.getProperty(externalProviderAuthenticationResponse.getStatusCode()));
			externalProviderAuthenticationUserResponseSuccess.setStatus(status);

			ExternalProviderAuthenticationUserResponseBody body = new ExternalProviderAuthenticationUserResponseBody();

			ExternalProviderAuthenticationUserDataResponse userData = new ExternalProviderAuthenticationUserDataResponse();
			userData.setFirstName(externalProviderAuthenticationResponse.getUser().getPersonalData().getFirstName());
			userData.setLastName(externalProviderAuthenticationResponse.getUser().getPersonalData().getLastName());
			userData.setFiscalCode(externalProviderAuthenticationResponse.getUser().getPersonalData().getFiscalCode());
            userData.setBirthMunicipality(externalProviderAuthenticationResponse.getUser().getPersonalData().getBirthMunicipality());
            userData.setBirthProvince(externalProviderAuthenticationResponse.getUser().getPersonalData().getBirthProvince());
            userData.setSex(externalProviderAuthenticationResponse.getUser().getPersonalData().getSex());
			userData.setSource(externalProviderRequestString);


			if (externalProviderAuthenticationResponse.getUser().getPersonalData().getBirthDate() != null) {
				Calendar calendar = new GregorianCalendar();
				calendar.setTime(externalProviderAuthenticationResponse.getUser().getPersonalData().getBirthDate());
				CustomDate dateOfBirth = new CustomDate();
				dateOfBirth.setYear(calendar.get(Calendar.YEAR));
				dateOfBirth.setMonth(calendar.get(Calendar.MONTH) + 1);
				dateOfBirth.setDay(calendar.get(Calendar.DAY_OF_MONTH));
				userData.setDateOfBirth(dateOfBirth);
			}
			
			System.out.println("DEBUG ************** Size of getMobilePhoneList:  "+externalProviderAuthenticationResponse.getUser().getMobilePhoneList().size());
			if(externalProviderAuthenticationResponse.getUser().getMobilePhoneList()!= null && ! externalProviderAuthenticationResponse.getUser().getMobilePhoneList().isEmpty()){
			   
			    String prefixNumberFromMyCicero=externalProviderAuthenticationResponse.getUser().getMobilePhoneList().get(0).getPrefix();
	            String mobileNumberFromMyCicero = externalProviderAuthenticationResponse.getUser().getMobilePhoneList().get(0).getNumber();
	            System.out.println("INFO     ExternaProviderAuthenticationUserRequest......Prefix Nunmber::"+prefixNumberFromMyCicero+"      mobile Number From MyCicero:: "+mobileNumberFromMyCicero);
	            MobilePhone mobilePhone = checkMobilePhonFromMyCicero(prefixNumberFromMyCicero, mobileNumberFromMyCicero);
	            
	            if(mobilePhone!=null){
	                mobilePhone.setStatus(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE);
	                ContactData contactData = new ContactData();
	                contactData.getMobilePhones().add(mobilePhone);
	                userData.setContactData(contactData);
	            }
			}
            
//			SocialData socialData = new SocialData();
//			socialData.setProvider(externalProviderRequestString);
//			userData.getSocialDataList().add(socialData);
			
			
			UserStatus userStatus = new UserStatus();
			if(externalProviderAuthenticationResponse.getUser().getUserStatus()!=null){
			    userStatus.setStatus(externalProviderAuthenticationResponse.getUser().getUserStatus());
			}
			
			if(externalProviderAuthenticationResponse.getUser().getUserStatusRegistrationCompleted()!=null){
			    userStatus.setRegistrationCompleted(externalProviderAuthenticationResponse.getUser().getUserStatusRegistrationCompleted());
			}

			Boolean updatedMission = externalProviderAuthenticationResponse.getUser().getUpdatedMission();
            if ( updatedMission == null ) {
                userStatus.setUpdatedMission(false);
            }
            else {
                userStatus.setUpdatedMission(updatedMission);
            }
            
            String source = externalProviderAuthenticationResponse.getUser().getSource();
            System.out.println("Source: " + source);
            if (source != null) {
                userStatus.setType(source);
            }
            
			userData.setUserStatus(userStatus);
			userData.setSex(externalProviderAuthenticationResponse.getUser().getPersonalData().getSex());

			EmailSecurityData securityData = new EmailSecurityData();
			securityData.setEmail(externalProviderAuthenticationResponse.getUser().getPersonalData().getSecurityDataEmail());
			userData.setSecurityData(securityData);
			body.setUserData(userData);
			if(externalProviderAuthenticationResponse.getUser().getPersonalData().getSecurityDataEmail() != null && !externalProviderAuthenticationResponse.getUser().getPersonalData().getSecurityDataEmail().isEmpty())
			{
			    body.getNotEditableFields().add("email");
			}
			if(source != null && source.equals("MYCICERO") && !externalProviderAuthenticationResponse.getUser().getMobilePhoneList().isEmpty()) {
			    body.getNotEditableFields().add("phoneNumber");
			}
			
			externalProviderAuthenticationUserResponseSuccess.setBody(body);
			
			status.setStatusCode(StatusCode.USER_V2_SOCIAL_AUTH_SUCCESS);
			status.setStatusMessage(prop.getProperty(StatusCode.USER_V2_SOCIAL_AUTH_SUCCESS));
			
			externalProviderAuthenticationUserResponseSuccess.setStatus(status);
			
			return externalProviderAuthenticationUserResponseSuccess;
		}


		ExternalProviderAuthenticationUserResponseFailure externalProviderAuthenticationUserResponseFailure = new ExternalProviderAuthenticationUserResponseFailure();

		status.setStatusCode(externalProviderAuthenticationResponse.getStatusCode());
		status.setStatusMessage(prop.getProperty(externalProviderAuthenticationResponse.getStatusCode()));

		externalProviderAuthenticationUserResponseFailure.setStatus(status);

		return externalProviderAuthenticationUserResponseFailure;
	}
	
	
	private MobilePhone checkMobilePhonFromMyCicero(String prefix,String number){
	  
	    MobilePhone mobilePhone = null; 
	    
        if(prefix!=null && !prefix.isEmpty()) {
            
            if (! prefix.equals("+39")){
                return null;
            }
            else {
                if(number!= null && !number.isEmpty()) {
                    
                    if(!number.startsWith("+39")){
                        mobilePhone = new MobilePhone();
                        mobilePhone.setPrefix(prefix);
                        mobilePhone.setNumber(number);
                        return mobilePhone;
                    }
                    else {
                        mobilePhone = new MobilePhone();
                        mobilePhone.setPrefix("+39");
                        mobilePhone.setNumber(number.replace("+39", ""));
                        return mobilePhone;
                    }
                }
            }
        }
        else {
            
            if(number!= null && !number.isEmpty()) {
                
                if (number.startsWith("+39")){
                    mobilePhone = new MobilePhone();
                    mobilePhone.setPrefix("+39");
                    mobilePhone.setNumber(number.replace("+39", ""));
                    return mobilePhone;
                }
    
                if(number.startsWith("+")){
                   return null;
                }
                
                mobilePhone = new MobilePhone();
                mobilePhone.setNumber(number);
                return mobilePhone;
            }
        }
	    
	    return mobilePhone;
	}

}
