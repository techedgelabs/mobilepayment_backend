package com.techedge.mp.frontend.adapter.entities.postpaid.v2.approvemulticardtransaction;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ApproveMulticardTransactionBodyRequest implements Validable {

    private String                              transactionID;
    private String                              paymentCryptogram;
    private ApproveTransactionPaymentMethodBody paymentMethod;

    @Override
    public Status check() {

        Status status = new Status();

        if (this.transactionID == null || this.transactionID.length() != 32 || this.transactionID.trim() == "") {

            status.setStatusCode(StatusCode.POSTPAID_V2_APPROVE_TRANSACTION_ID_WRONG);

            return status;

        }
        
        if (this.paymentCryptogram == null || this.paymentCryptogram.trim() == "") {

            status.setStatusCode(StatusCode.POSTPAID_V2_APPROVE_TRANSACTION_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.POSTPAID_V2_APPROVE_TRANSACTION_SUCCESS);

        return status;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public String getPaymentCryptogram() {
        return paymentCryptogram;
    }

    public void setPaymentCryptogram(String paymentCryptogram) {
        this.paymentCryptogram = paymentCryptogram;
    }

    public ApproveTransactionPaymentMethodBody getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(ApproveTransactionPaymentMethodBody paymentMethod) {
        this.paymentMethod = paymentMethod;
    }
}