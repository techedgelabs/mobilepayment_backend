package com.techedge.mp.frontend.adapter.entities.user.business.update;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.BusinessData;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.business.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class UpdateBusinessDataBodyRequest implements Validable {

    private BusinessData businessData;

    public BusinessData getBusinessData() {
        return businessData;
    }

    public void setBusinessData(BusinessData businessData) {
        this.businessData = businessData;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.businessData != null) {

            status = this.businessData.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.USER_BUSINESS_REQU_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.USER_BUSINESS_UPDATE_BUSINESS_DATA_SUCCESS);

        return status;

    }

}
