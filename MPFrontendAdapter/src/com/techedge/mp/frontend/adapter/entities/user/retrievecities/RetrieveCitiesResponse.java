package com.techedge.mp.frontend.adapter.entities.user.retrievecities;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveCitiesResponse extends BaseResponse {

	private RetrieveCitiesResponseBody body;

	public RetrieveCitiesResponseBody getBody() {
		return body;
	}
	public void setBody(RetrieveCitiesResponseBody body) {
		this.body = body;
	}
}
