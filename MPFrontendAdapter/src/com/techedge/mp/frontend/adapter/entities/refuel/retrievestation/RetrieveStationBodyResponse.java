package com.techedge.mp.frontend.adapter.entities.refuel.retrievestation;

public class RetrieveStationBodyResponse {

	
	private String selectedPumpID;
	private RetrieveStationBodyStationResponse station;
	private String outOfRange;
	
	public String getSelectedPumpID() {
		return selectedPumpID;
	}
	public void setSelectedPumpID(String selectedPumpID) {
		this.selectedPumpID = selectedPumpID;
	}
	
	public RetrieveStationBodyStationResponse getStation() {
		return station;
	}
	public void setStation(RetrieveStationBodyStationResponse station) {
		this.station = station;
	}
	public String getOutOfRange() {
		return outOfRange;
	}
	public void setOutOfRange(String outOfRange) {
		this.outOfRange = outOfRange;
	}
	
}
