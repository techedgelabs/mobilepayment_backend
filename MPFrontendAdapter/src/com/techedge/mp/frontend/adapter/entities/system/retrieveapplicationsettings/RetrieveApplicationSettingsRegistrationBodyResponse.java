package com.techedge.mp.frontend.adapter.entities.system.retrieveapplicationsettings;


public class RetrieveApplicationSettingsRegistrationBodyResponse {

	private RetrieveApplicationSettingsRegistrationDepositCardBodyResponse depositCard;

	public RetrieveApplicationSettingsRegistrationDepositCardBodyResponse getDepositCard() {
		return depositCard;
	}
	public void setDepositCard(
			RetrieveApplicationSettingsRegistrationDepositCardBodyResponse depositCard) {
		this.depositCard = depositCard;
	}
}
