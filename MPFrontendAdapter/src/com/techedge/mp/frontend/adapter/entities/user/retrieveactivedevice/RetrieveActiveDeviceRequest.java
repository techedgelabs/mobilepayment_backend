package com.techedge.mp.frontend.adapter.entities.user.retrieveactivedevice;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.Device;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveUserDeviceData;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.DeviceData;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RetrieveActiveDeviceRequest extends AbstractRequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-ACTIVE-DEVICE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.USER_RETRIEVE_ACTIVE_DEVICE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        RetrieveActiveDeviceRequest retrieveActiveDeviceRequest = this;
        RetrieveActiveDeviceResponse retrieveActiveDeviceResponse = new RetrieveActiveDeviceResponse();

        RetrieveUserDeviceData response = getUserServiceRemote().retrieveActiveDevice(retrieveActiveDeviceRequest.getCredential().getTicketID(),
                retrieveActiveDeviceRequest.getCredential().getRequestID());

        if (response.getStatusCode().equals(ResponseHelper.USER_RETRIEVE_ACTIVE_DEVICE_SUCCESS)) {
            retrieveActiveDeviceResponse.setBody(new RetrieveActiveDeviceResponseBody());
            List<DeviceData> listDeviceData = new ArrayList<DeviceData>(0);
            for (Device item : response.getDeviceList()) {
                DeviceData deviceData = new DeviceData();
                if (item.getAssociationTime() != null) {
                    deviceData.setAssociationTime(CustomTimestamp.createCustomTimestamp(new Timestamp(item.getAssociationTime().getTime())));
                }
                if (item.getCreationTime() != null) {
                    deviceData.setCreationTime(CustomTimestamp.createCustomTimestamp(new Timestamp(item.getCreationTime().getTime())));
                }
                deviceData.setDeviceID(item.getDeviceID());
                deviceData.setDeviceName(item.getDeviceName());
                if (item.getLastLoginTime() != null) {
                    deviceData.setLastLoginTime(CustomTimestamp.createCustomTimestamp(new Timestamp(item.getLastLoginTime().getTime())));
                }
                listDeviceData.add(deviceData);
            }
            retrieveActiveDeviceResponse.getBody().setActiveDevice(listDeviceData);
        }

        status.setStatusCode(response.getStatusCode());

        status.setStatusMessage(prop.getProperty(response.getStatusCode()));

        retrieveActiveDeviceResponse.setStatus(status);

        return retrieveActiveDeviceResponse;
    }

}
