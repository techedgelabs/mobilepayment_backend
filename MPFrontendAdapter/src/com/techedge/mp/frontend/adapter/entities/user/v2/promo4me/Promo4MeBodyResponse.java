package com.techedge.mp.frontend.adapter.entities.user.v2.promo4me;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.v2.CrmOffers;

public class Promo4MeBodyResponse implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = -8455744578136344806L;
    
    List<CrmOffers> offers = new ArrayList<CrmOffers>();

    public List<CrmOffers> getOffers() {
        return offers;
    }

    public void setOffers(List<CrmOffers> offers) {
        this.offers = offers;
    }

    
}
