package com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions;

import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;

public class ManagerRetrieveTransactionsBodyDataResponse {

    private String                         sourceType;
    private String                         sourceStatus;
    private PostPaidTransactionHistoryData transaction;

    public String getSourceType() {
        return sourceType;
    }

    public void setSourceType(String sourceType) {
        this.sourceType = sourceType;
    }

    public String getSourceStatus() {
        return sourceStatus;
    }

    public void setSourceStatus(String sourceStatus) {
        this.sourceStatus = sourceStatus;
    }

    public PostPaidTransactionHistoryData getTransaction() {
        return transaction;
    }

    public void setTransaction(PostPaidTransactionHistoryData transaction) {
        this.transaction = transaction;
    }

}
