package com.techedge.mp.frontend.adapter.entities.user.v2.getpromopopup;

import com.techedge.mp.core.business.interfaces.AppLink;
import com.techedge.mp.core.business.interfaces.GetPromoPopupDataResponse;
import com.techedge.mp.core.business.interfaces.LandingDataResponse;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.v2.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.v2.Validator;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetPromoPopupRequest extends AbstractRequest {

    private Status                   status = new Status();

    private Credential               credential;
    private GetPromoPopupRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GetPromoPopupRequestBody getBody() {
        return body;
    }

    public void setBody(GetPromoPopupRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GETPROMOPOPUP", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.USER_V2_GETLANDING_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        
        GetPromoPopupResponse getPromoPopupResponse = new GetPromoPopupResponse();
        GetPromoPopupResponseBody getPromoPopupResponseBody = new GetPromoPopupResponseBody();

        GetPromoPopupDataResponse getPromoPopupDataResponse = getUserV2ServiceRemote().getPromoPopupData(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getSectionId());


        /*
        String url = "https://www.google.com";

        
        String  type       = "info";
        String  body       = "";
        String  bannerUrl  = "";
        Boolean showAlways = Boolean.FALSE;
        String  promoId    = "";

        if (this.getBody() != null) {

            String sectionId = this.getBody().getSectionId();

            if (sectionId.equals("HOME_REFUELING")) {
                
                url        = "https://www.google.com";
                type       = "info";
                body       = "<b>Test promo HOME_REFUEL</b><i>Messaggio test promo (i)</i>";
                bannerUrl  = "http://mpdev.r53.techedgegroup.com:8080/MPFrontendServlet/img/banner/banner.png";
                showAlways = Boolean.TRUE;
                promoId    = "CINEMA_2X";
                
                AppLink appLink    = new AppLink();
                appLink.setLocation(url);
                appLink.setType(PushNotificationContentType.INTERNAL);
                
                getPromoPopupResponseBody.setAppLink(appLink);
                getPromoPopupResponseBody.setBannerUrl(bannerUrl);
                getPromoPopupResponseBody.setBody(body);
                getPromoPopupResponseBody.setPromoId(promoId);
                getPromoPopupResponseBody.setShowAlways(showAlways);
                getPromoPopupResponseBody.setType(type);
                
                status.setStatusCode(StatusCode.USER_V2_GETPROMOPOPUP_SUCCESS);
                status.setStatusMessage(prop.getProperty(StatusCode.USER_V2_GETPROMOPOPUP_SUCCESS));
                
                getPromoPopupResponse.setStatus(status);
                getPromoPopupResponse.setBody(getPromoPopupResponseBody);
                
                return getPromoPopupResponse;
            }

            if (sectionId.equals("HOME_LOYALTY")) {

                url        = "https://www.google.com";
                type       = "info";
                body       = "<b>Test promo HOME_LOYALTY</b>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Ut odio. Nam sed est. Nam a risus et est iaculis adipiscing. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Integer ut justo. In tincidunt viverra nisl. Donec dictum malesuada magna. Curabitur id nibh auctor tellus adipiscing pharetra. Fusce vel justo non orci semper feugiat. Cras eu leo at purus ultrices tristique.";
                bannerUrl  = "http://mpdev.r53.techedgegroup.com:8080/MPFrontendServlet/img/banner/banner.png";
                showAlways = Boolean.TRUE;
                promoId    = "CINEMA_2X";
                
                AppLink appLink    = new AppLink();
                appLink.setLocation(url);
                appLink.setType(PushNotificationContentType.INTERNAL);
                
                getPromoPopupResponseBody.setAppLink(appLink);
                getPromoPopupResponseBody.setBannerUrl(bannerUrl);
                getPromoPopupResponseBody.setBody(body);
                getPromoPopupResponseBody.setPromoId(promoId);
                getPromoPopupResponseBody.setShowAlways(showAlways);
                getPromoPopupResponseBody.setType(type);
                
                status.setStatusCode(StatusCode.USER_V2_GETPROMOPOPUP_SUCCESS);
                status.setStatusMessage(prop.getProperty(StatusCode.USER_V2_GETPROMOPOPUP_SUCCESS));
                
                getPromoPopupResponse.setStatus(status);
                getPromoPopupResponse.setBody(getPromoPopupResponseBody);
                
                return getPromoPopupResponse;
            }

            status.setStatusCode(StatusCode.USER_V2_GETPROMOPOPUP_PROMO_NOT_FOUND);
            status.setStatusMessage(prop.getProperty(StatusCode.USER_V2_GETPROMOPOPUP_PROMO_NOT_FOUND));
            
            getPromoPopupResponse.setStatus(status);

            return getPromoPopupResponse;
        }
        else {
            
            status.setStatusCode(StatusCode.USER_V2_GETPROMOPOPUP_INVALID_REQUEST);
            status.setStatusMessage(prop.getProperty(StatusCode.USER_V2_GETPROMOPOPUP_INVALID_REQUEST));
            
            getPromoPopupResponse.setStatus(status);

            return getPromoPopupResponse;
        }
        */

        
        if (getPromoPopupDataResponse != null && getPromoPopupDataResponse.getPromoPopupData() != null) {
            
            String url         = getPromoPopupDataResponse.getPromoPopupData().getUrl();
            String type        = getPromoPopupDataResponse.getPromoPopupData().getType();
            String body        = getPromoPopupDataResponse.getPromoPopupData().getBody();
            String bannerUrl   = getPromoPopupDataResponse.getPromoPopupData().getBannerUrl();
            Boolean showAlways = getPromoPopupDataResponse.getPromoPopupData().getShowAlways();
            String promoId     = getPromoPopupDataResponse.getPromoPopupData().getPromoId();
            
            AppLink appLink    = new AppLink();
            appLink.setLocation(url);
            appLink.setType(PushNotificationContentType.INTERNAL);
            
            getPromoPopupResponseBody.setAppLink(appLink);
            getPromoPopupResponseBody.setBannerUrl(bannerUrl);
            getPromoPopupResponseBody.setBody(body);
            getPromoPopupResponseBody.setPromoId(promoId);
            getPromoPopupResponseBody.setShowAlways(showAlways);
            getPromoPopupResponseBody.setType(type);
        }
        
        status.setStatusCode(getPromoPopupDataResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(getPromoPopupDataResponse.getStatusCode()));

        getPromoPopupResponse.setStatus(status);
        getPromoPopupResponse.setBody(getPromoPopupResponseBody);

        return getPromoPopupResponse;

    }

}
