package com.techedge.mp.frontend.adapter.entities.manager.retrievetransactions;

import java.sql.Timestamp;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PaymentHistory;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCartData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionInfo;
import com.techedge.mp.frontend.adapter.entities.common.AmountConverter;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomTimestamp;
import com.techedge.mp.frontend.adapter.entities.common.POPStatusConverter;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidCartJsonData;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidExtendedRefuelData;
import com.techedge.mp.frontend.adapter.entities.common.postpaid.PostPaidTransactionHistoryData;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class ManagerRetrieveTransactionsRequest extends AbstractRequest implements Validable {

    private Credential                             credential;
    private ManagerRetrieveTransactionsBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ManagerRetrieveTransactionsBodyRequest getBody() {
        return body;
    }

    public void setBody(ManagerRetrieveTransactionsBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("MANAGER-RETRIEVE-TRANSACTIONS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(ResponseHelper.MANAGER_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.MANAGER_RETRIEVE_TRANSACTIONS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        ManagerRetrieveTransactionsRequest retrieveTransactionsRequest = this;
        ManagerRetrieveTransactionsResponse retrieveTransactionsResponse = new ManagerRetrieveTransactionsResponse();
        ManagerRetrieveTransactionsBodyResponse retrieveTransactionsBodyResponse = new ManagerRetrieveTransactionsBodyResponse();

        String ticketID = retrieveTransactionsRequest.getCredential().getTicketID();
        String requestID = retrieveTransactionsRequest.getCredential().getRequestID();
        String stationID = retrieveTransactionsRequest.getBody().getStationID();
        Integer pumpMaxTransactions = retrieveTransactionsRequest.getBody().getPumpMaxTransactions();

        com.techedge.mp.core.business.interfaces.ManagerRetrieveTransactionsResponse coreResponse;
        coreResponse = getManagerServiceRemote().retrieveTransactions(ticketID, requestID, stationID, pumpMaxTransactions);

        Date systemDate = new Date();

        if (coreResponse.getTransactionHistory() != null && !coreResponse.getTransactionHistory().isEmpty()) {
            for (PaymentHistory coreTransactionHistory : coreResponse.getTransactionHistory()) {
                PostPaidTransactionInfo transactionInfo = coreTransactionHistory.getTransaction();
                ManagerRetrieveTransactionsBodyDataResponse transactionHistory = new ManagerRetrieveTransactionsBodyDataResponse();
                PostPaidTransactionHistoryData transaction = new PostPaidTransactionHistoryData();
                transactionHistory.setSourceStatus(coreTransactionHistory.getSourceStatus());
                transactionHistory.setSourceType(coreTransactionHistory.getSourceType());

                // Trascodifica dello stato
                if (transactionInfo.getStatus() == null) {
                    transaction.setStatus(null);
                }
                else {
                    String localStatus = POPStatusConverter.toAppStatus(transactionInfo.getStatus());

                    if (!localStatus.isEmpty()) {
                        transaction.setStatus(localStatus);
                    }
                    else {
                        transaction.setStatus(transactionInfo.getStatus());
                    }
                }

                transaction.setSubStatus(transactionInfo.getSubStatus());
                transaction.setTransactionID(transactionInfo.getTransactionID());
                transaction.setAmount(AmountConverter.toMobile(transactionInfo.getAmount()));
                transaction.setVoucherAmount(AmountConverter.toMobile(transactionInfo.getVoucherAmount()));
                transaction.setLoyaltyBalance(transactionInfo.getLoyaltyBalance());

                if (transactionInfo.getCreationTimestamp() != null) {
                    transaction.setDate(CustomTimestamp.createCustomTimestamp(Timestamp.valueOf(transactionInfo.getCreationTimestamp().toString())));
                }

                transaction.setSystemDate(CustomTimestamp.createCustomTimestamp(new Timestamp(systemDate.getTime())));

                if (transactionInfo.getCart() != null && !transactionInfo.getCart().isEmpty()) {
                    for (PostPaidCartData coreCart : transactionInfo.getCart()) {
                        PostPaidCartJsonData cart = new PostPaidCartJsonData();
                        cart.setProductId(coreCart.getProductId());
                        cart.setProductDescription(coreCart.getProductDescription());
                        cart.setAmount(AmountConverter.toMobile(coreCart.getAmount()));
                        cart.setQuantity(coreCart.getQuantity());
                        transaction.getCart().add(cart);
                    }
                }

                if (transactionInfo.getRefuel() != null && !transactionInfo.getRefuel().isEmpty()) {
                    for (PostPaidRefuelData coreRefuel : transactionInfo.getRefuel()) {
                        PostPaidExtendedRefuelData refuel = new PostPaidExtendedRefuelData();
                        refuel.setPumpId(coreRefuel.getPumpId());
                        refuel.setPumpNumber(coreRefuel.getPumpNumber());
                        refuel.setProductId(coreRefuel.getProductId());
                        refuel.setProductDescription(coreRefuel.getProductDescription());
                        refuel.setFuelType(coreRefuel.getFuelType());
                        refuel.setFuelAmount(coreRefuel.getFuelAmount());
                        refuel.setFuelQuantity(coreRefuel.getFuelQuantity());
                        transaction.getRefuel().add(refuel);
                    }
                }

                transactionHistory.setTransaction(transaction);
                retrieveTransactionsBodyResponse.getTransactionHistory().add(transactionHistory);
            }
        }

        retrieveTransactionsResponse.setBody(retrieveTransactionsBodyResponse);
        Status statusResponse = new Status();
        statusResponse.setStatusCode(coreResponse.getStatusCode());
        statusResponse.setStatusMessage(prop.getProperty(coreResponse.getStatusCode()));
        retrieveTransactionsResponse.setStatus(statusResponse);

        return retrieveTransactionsResponse;
    }

}
