package com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievestationdetail;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveStationDetailBodyRequest implements Validable {

    private RetrieveStationDetailUserPositionRequest userPosition;
    private String stationID;
    private Boolean clearTransaction;
    private Boolean refresh;
    
	public RetrieveStationDetailUserPositionRequest getUserPosition() {
		return userPosition;
	}
	public void setUserPosition(RetrieveStationDetailUserPositionRequest userPosition) {
		this.userPosition = userPosition;
	}
	
	public String getStationID() {
        return stationID;
    }
    public void setStationID(String stationID) {
        this.stationID = stationID;
    }
    
    public Boolean getClearTransaction() {
        return clearTransaction;
    }
    public void setClearTransaction(Boolean clearTransaction) {
        this.clearTransaction = clearTransaction;
    }
    
    public Boolean getRefresh() {
        return refresh;
    }
    public void setRefresh(Boolean refresh) {
        this.refresh = refresh;
    }
    @Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.userPosition == null && (this.stationID == null || this.stationID.trim().isEmpty())) {
			
			status.setStatusCode(StatusCode.STATION_RETRIEVE_STATION_NOT_FOUND);
			
			return status;
		}

        status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);
        
        return status;
    }
}
