package com.techedge.mp.frontend.adapter.entities.manager.authentication;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class ManagerAuthenticationResponse extends BaseResponse {

  private ManagerAuthenticationBodyResponse body;

  public ManagerAuthenticationBodyResponse getBody() {
    return body;
  }

  public void setBody(ManagerAuthenticationBodyResponse body) {
    this.body = body;
  }
  
  
  
  
}
