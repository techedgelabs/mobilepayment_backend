package com.techedge.mp.frontend.adapter.entities.user.v2.externalproviderauthentication;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class ExternalProviderAuthenticationUserResponseSuccess extends BaseResponse {

    private ExternalProviderAuthenticationUserResponseBody body;

    public ExternalProviderAuthenticationUserResponseBody getBody() {
        return body;
    }

    public void setBody(ExternalProviderAuthenticationUserResponseBody body) {
        this.body = body;
    }

}
