package com.techedge.mp.frontend.adapter.entities.user.v2.gethomepartnerlist;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.techedge.mp.core.business.interfaces.GetHomePartnerListResult;
import com.techedge.mp.core.business.interfaces.HomePartner;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.PartnerInfo;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetHomePartnerListRequest extends AbstractRequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-PARTNER-LIST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        else {

            status.setStatusCode(StatusCode.GET_PARTNER_LIST_SUCCESS);

        }

        return status;

    }

    @Override
    public BaseResponse execute() {

        GetHomePartnerListBodyResponse getHomePartnerListBodyResponse = new GetHomePartnerListBodyResponse();
        GetHomePartnerListResponse getHomePartnerListResponse = new GetHomePartnerListResponse();

        GetHomePartnerListResult getHomePartnerListResult = getUserV2ServiceRemote().getHomePartnerListInfo(this.getCredential().getRequestID(), this.getCredential().getTicketID());

        if (getHomePartnerListResult != null && getHomePartnerListResult.getHomePartnerList() != null && !getHomePartnerListResult.getHomePartnerList().isEmpty()) {

            List<HomePartner> homePartnerList = getHomePartnerListResult.getHomePartnerList();

            // Ordinamento
            Collections.sort(homePartnerList, new Comparator<HomePartner>() {
                @Override
                public int compare(HomePartner homePartner1, HomePartner homePartner2) {

                    return (homePartner1.getSequence().compareTo(homePartner2.getSequence()));
                }
            });

            for (HomePartner homePartner : homePartnerList) {

                PartnerInfo partnerInfo = new PartnerInfo();
                partnerInfo.setLogoSmallUrl(homePartner.getName());
                getHomePartnerListBodyResponse.getPartnerList().add(partnerInfo);
            }
        }

        status.setStatusCode(getHomePartnerListResult.getStatusCode());
        status.setStatusMessage(prop.getProperty(getHomePartnerListResult.getStatusCode()));

        // *********** Start stub section ************************
        /*
         * List<String> partnerUrlList = new ArrayList<String>(0);
         * 
         * Date now = new Date();
         * Long maxTimestamp = 1546297200000l;
         * 
         * // Versione immagini 160
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/enjoy_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/ventis_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/cisalfa_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/volagratis_160.png");
         * //partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/gnv_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/lafeltrinelli_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/leroymerlin_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/musement_160.png");
         * //partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/mymovies_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/tifoshop_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/eprice_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/chicco_160.png");
         * //partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/kiko_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/danordafood_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/going_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/zalando_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/yamamay_160.png");
         * //partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/unieuro_160.png");
         * //partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/prenatal_160.png");
         * //partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/monclick_160.png");
         * //partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/justeat_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/guess_160.png");
         * //partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/gas_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/expedia_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/edreams_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/douglas_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/disneystore_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/ciam_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/carpisa_160.png");
         * //partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/bookingcom_160.png");
         * //partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/stefanel_160.png");
         * partnerUrlList.add("http://djyajlmeyzzai.cloudfront.net/brand/hotelscom_160.png");
         * 
         * for(String partnerUrl : partnerUrlList) {
         * PartnerInfo partnerInfo = new PartnerInfo();
         * partnerInfo.setLogoSmallUrl(partnerUrl);
         * getHomePartnerListBodyResponse.getPartnerList().add(partnerInfo);
         * }
         * 
         * status.setStatusCode("USER_V2_GET_PARTNER_LIST_200");
         * status.setStatusMessage("Lista partner restituita con successo (0400)");
         */
        //**************** END stub Section   ***************************************

        getHomePartnerListResponse.setStatus(status);
        getHomePartnerListResponse.setBody(getHomePartnerListBodyResponse);

        return getHomePartnerListResponse;

    }

}
