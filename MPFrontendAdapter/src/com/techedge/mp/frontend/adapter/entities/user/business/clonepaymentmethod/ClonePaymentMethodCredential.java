package com.techedge.mp.frontend.adapter.entities.user.business.clonepaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.business.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class ClonePaymentMethodCredential extends Credential implements Validable {
    
    private String pin;

    public String getPin() {
        return pin;
    }
    public void setPin(String pin) {
        this.pin = pin;
    }

    @Override
    public Status check() {
        
        Status status = new Status();
        
        Credential credential = new Credential();
        credential.setTicketID(this.getTicketID());
        credential.setRequestID(this.getRequestID());
        
        status = Validator.checkCredential("POSTPAID-APPROVE", credential);
        
        if(!Validator.isValid(status.getStatusCode())) {
            
            return status;
            
        }
        
        if(this.pin == null || this.pin.trim().isEmpty()) {
            
            status.setStatusCode(StatusCode.USER_BUSINESS_CLONE_PAYMENT_METHOD_INVALID_REQUEST);
            
            return status;
            
        }
        
        status.setStatusCode(StatusCode.USER_BUSINESS_CLONE_PAYMENT_METHOD_SUCCESS);
        
        return status;
        
    }   
}
