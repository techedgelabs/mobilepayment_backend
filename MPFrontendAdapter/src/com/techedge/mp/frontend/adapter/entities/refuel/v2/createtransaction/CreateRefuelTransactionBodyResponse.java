package com.techedge.mp.frontend.adapter.entities.refuel.v2.createtransaction;


public class CreateRefuelTransactionBodyResponse {

	private Integer checkPinAttemptsLeft;

	public Integer getCheckPinAttemptsLeft() {
		return checkPinAttemptsLeft;
	}
	public void setCheckPinAttemptsLeft(Integer checkPinAttemptsLeft) {
		this.checkPinAttemptsLeft = checkPinAttemptsLeft;
	}

}
