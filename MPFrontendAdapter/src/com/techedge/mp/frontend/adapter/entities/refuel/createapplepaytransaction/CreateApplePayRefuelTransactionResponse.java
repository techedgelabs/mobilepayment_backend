package com.techedge.mp.frontend.adapter.entities.refuel.createapplepaytransaction;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class CreateApplePayRefuelTransactionResponse extends BaseResponse {

	private CreateApplePayRefuelTransactionBodyResponse body;

	public CreateApplePayRefuelTransactionBodyResponse getBody() {
		return body;
	}
	public void setBody(CreateApplePayRefuelTransactionBodyResponse body) {
		this.body = body;
	}
}
