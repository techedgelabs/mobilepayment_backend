package com.techedge.mp.frontend.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;

public class ManagerData {

  private String username;
  private String password;
  private String email;
  private String firstName;
  private String lastName;
  private Integer type;
  private Integer status;
  private List<Station> station = new ArrayList<Station>(0);


  public ManagerData() {}

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public Integer getStatus() {
    return status;
  }

  public void setStatus(Integer status) {
    this.status = status;
  }

  public List<Station> getStation() {
    return station;
  }

  public void setStation(List<Station> station) {
    this.station = station;
  }

  public Integer getType() {
    return type;
  }

  public void setType(Integer type) {
    this.type = type;
  }

  public String getFirstName() {
    return firstName;
  }

  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  public String getLastName() {
    return lastName;
  }

  public void setLastName(String lastName) {
    this.lastName = lastName;
  }

}
