package com.techedge.mp.frontend.adapter.entities.user.setdefaultloyaltycard;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class SetDefaultLoyaltyCardBodyRequest implements Validable {

    private String panCode;

    public String getPanCode() {
        return panCode;
    }

    public void setPanCode(String panCode) {
        this.panCode = panCode;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.panCode == null || this.panCode.trim().isEmpty()) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        if (this.panCode.length() > 19) {

            status.setStatusCode(StatusCode.USER_SET_DEFAULT_LOYALTY_CARD_PANCODE_WRONG);

            return status;
        }

        status.setStatusCode(StatusCode.USER_SET_DEFAULT_LOYALTY_CARD_SUCCESS);

        return status;

    }

}
