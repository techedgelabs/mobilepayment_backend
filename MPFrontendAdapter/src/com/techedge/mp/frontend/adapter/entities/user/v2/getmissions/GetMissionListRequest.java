package com.techedge.mp.frontend.adapter.entities.user.v2.getmissions;

import com.techedge.mp.core.business.interfaces.AppLink;
import com.techedge.mp.core.business.interfaces.MissionDetailDataResponse;
import com.techedge.mp.core.business.interfaces.MissionInfoDataDetail;
import com.techedge.mp.core.business.interfaces.PromotionInfoDataDetail;
import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.CustomDate;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.common.v2.AppLinkInfo;
import com.techedge.mp.frontend.adapter.entities.common.v2.MissionInfo;
import com.techedge.mp.frontend.adapter.entities.common.v2.PromotionInfo;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class GetMissionListRequest extends AbstractRequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-MISSION-LIST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        else {

            status.setStatusCode(StatusCode.GET_MISSION_LIST_SUCCESS);

        }

        return status;

    }

    @Override
    public BaseResponse execute() {

        GetMissionListBodyResponse missionListBodyResponse = new GetMissionListBodyResponse();
        GetMissionListResponse getMissionListResponse = new GetMissionListResponse();

        MissionDetailDataResponse missionDetailDataResponse = getUserV2ServiceRemote().getMissionList(this.getCredential().getRequestID(), this.getCredential().getTicketID());

        if (missionDetailDataResponse != null && !missionDetailDataResponse.getPromotionList().isEmpty()) {
            
            for(PromotionInfoDataDetail promotionInfoDataDetail : missionDetailDataResponse.getPromotionList()){
                
                PromotionInfo promotionInfo = new PromotionInfo();
                
                AppLink appLink = new AppLink();
                appLink.setLocation(promotionInfoDataDetail.getAppLinkInfo().getLocation());
                appLink.setParameters(promotionInfoDataDetail.getAppLinkInfo().getParameters());
                appLink.setType(promotionInfoDataDetail.getAppLinkInfo().getType());
                
                CustomDate startDate = CustomDate.createCustomDate(promotionInfoDataDetail.getStartDate());
                CustomDate endDate   = CustomDate.createCustomDate(promotionInfoDataDetail.getEndDate());
                
                promotionInfo.setAppLink(appLink);
                promotionInfo.setCode(promotionInfoDataDetail.getCode());
                promotionInfo.setImageUrl(promotionInfoDataDetail.getImageUrl());
                promotionInfo.setDescription(promotionInfoDataDetail.getDescription());
                promotionInfo.setName(promotionInfoDataDetail.getName());
                promotionInfo.setStartDate(startDate);
                promotionInfo.setEndDate(endDate);
                
                missionListBodyResponse.getPromotions().add(promotionInfo);
            }
        }
        
        if (missionDetailDataResponse != null && !missionDetailDataResponse.getMissionDetailList().isEmpty()) {

            for (MissionInfoDataDetail missionInfoDataDetail : missionDetailDataResponse.getMissionDetailList()) {

                MissionInfo missionInfo = new MissionInfo();

                AppLink appLink = new AppLink();
                appLink.setLocation(missionInfoDataDetail.getAppLink().getLocation());
                appLink.setType((missionInfoDataDetail.getAppLink().getType()));
                appLink.setParameters(missionInfoDataDetail.getAppLink().getParameters());

                CustomDate startDate = CustomDate.createCustomDate(missionInfoDataDetail.getStartDate());
                CustomDate endDate   = CustomDate.createCustomDate(missionInfoDataDetail.getEndDate());
                
                missionInfo.setAppLink(appLink);
                missionInfo.setCode(missionInfoDataDetail.getCode());
                missionInfo.setDescription(missionInfoDataDetail.getDescription());
                missionInfo.setName(missionInfoDataDetail.getName());
                missionInfo.setType(missionInfoDataDetail.getType());
                missionInfo.setStepCompleted(missionInfoDataDetail.getStepCompleted());
                missionInfo.setStepObjective(missionInfoDataDetail.getStepObjective());
                missionInfo.setStartDate(startDate);
                missionInfo.setEndDate(endDate);
                missionListBodyResponse.getMissions().add(missionInfo);
            }
        }
        
        if (missionDetailDataResponse != null && !missionDetailDataResponse.getCompletedMissionDetailList().isEmpty()) {
            
            for (MissionInfoDataDetail missionInfoDataDetail : missionDetailDataResponse.getCompletedMissionDetailList()) {
                
                MissionInfo missionInfo = new MissionInfo();

                AppLink appLink = new AppLink();
                appLink.setLocation(missionInfoDataDetail.getAppLink().getLocation());
                appLink.setType((missionInfoDataDetail.getAppLink().getType()));
                appLink.setParameters(missionInfoDataDetail.getAppLink().getParameters());

                CustomDate startDate = CustomDate.createCustomDate(missionInfoDataDetail.getStartDate());
                CustomDate endDate   = CustomDate.createCustomDate(missionInfoDataDetail.getEndDate());
                
                missionInfo.setAppLink(appLink);
                missionInfo.setCode(missionInfoDataDetail.getCode());
                missionInfo.setDescription(missionInfoDataDetail.getDescription());
                missionInfo.setName(missionInfoDataDetail.getName());
                missionInfo.setType(missionInfoDataDetail.getType());
                missionInfo.setStepCompleted(missionInfoDataDetail.getStepCompleted());
                missionInfo.setStepObjective(missionInfoDataDetail.getStepObjective());
                missionInfo.setStartDate(startDate);
                missionInfo.setEndDate(endDate);
                missionListBodyResponse.getCompletedMissions().add(missionInfo);
            }
        }

        status.setStatusCode(missionDetailDataResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(missionDetailDataResponse.getStatusCode()));

        getMissionListResponse.setStatus(status);
        getMissionListResponse.setBody(missionListBodyResponse);

        return getMissionListResponse;

    }

}
