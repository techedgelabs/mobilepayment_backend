package com.techedge.mp.frontend.adapter.entities.user.updatepin;


public class UpdatePinResponseBody {
	
	private Integer checkPinAttemptsLeft;

	public Integer getCheckPinAttemptsLeft() {
		return checkPinAttemptsLeft;
	}
	public void setCheckPinAttemptsLeft(Integer checkPinAttemptsLeft) {
		this.checkPinAttemptsLeft = checkPinAttemptsLeft;
	}
	
}
