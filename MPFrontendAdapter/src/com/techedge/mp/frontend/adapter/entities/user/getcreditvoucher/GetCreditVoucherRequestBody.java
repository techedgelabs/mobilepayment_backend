package com.techedge.mp.frontend.adapter.entities.user.getcreditvoucher;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class GetCreditVoucherRequestBody implements Validable {

    private Boolean refresh;

    public Boolean getRefresh() {
        return refresh;
    }

    public void setRefresh(Boolean refresh) {
        this.refresh = refresh;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (refresh == null) {

            status.setStatusCode(StatusCode.GET_CREDIT_VOUCHER_INVALID_PARAMETERS);
            return status;

        }

        status.setStatusCode(StatusCode.GET_CREDIT_VOUCHER_SUCCESS);

        return status;

    }
}
