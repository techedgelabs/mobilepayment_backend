package com.techedge.mp.frontend.adapter.entities.user.v2.retrievetermsofservicebygroup;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class RetrieveTermsOfServiceByGroupResponse extends BaseResponse {

	private RetrieveTermsOfServiceByGroupResponseBody body;

	public RetrieveTermsOfServiceByGroupResponseBody getBody() {
		return body;
	}
	public void setBody(RetrieveTermsOfServiceByGroupResponseBody body) {
		this.body = body;
	}
}
