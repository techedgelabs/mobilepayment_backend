package com.techedge.mp.frontend.adapter.entities.parking.v2.estimateparkingprice;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class EstimateParkingPriceResponse extends BaseResponse {
    private EstimateParkingPriceResponseBody body;

    public EstimateParkingPriceResponseBody getBody() {
        return body;
    }

    public void setBody(EstimateParkingPriceResponseBody body) {
        this.body = body;
    }

}
