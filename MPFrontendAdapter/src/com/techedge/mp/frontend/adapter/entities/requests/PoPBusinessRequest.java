package com.techedge.mp.frontend.adapter.entities.requests;

import com.techedge.mp.frontend.adapter.entities.postpaid.business.approvetransaction.ApproveTransactionRequest;
import com.techedge.mp.frontend.adapter.entities.postpaid.business.retrieverefueltransactiondetail.RetrieveRefuelTransactionDetailRequest;

public class PoPBusinessRequest extends RootRequest {

    protected ApproveTransactionRequest    approvePoPTransaction;
    protected RetrieveRefuelTransactionDetailRequest retrieveRefuelTransactionDetail;
    
    public ApproveTransactionRequest getApprovePoPTransaction() {
        return approvePoPTransaction;
    }

    public void setApprovePoPTransaction(ApproveTransactionRequest approvePoPTransaction) {
        this.approvePoPTransaction = approvePoPTransaction;
    }

    public RetrieveRefuelTransactionDetailRequest getRetrieveRefuelTransactionDetail() {
        return retrieveRefuelTransactionDetail;
    }
    
    public void setRetrieveRefuelTransactionDetail(RetrieveRefuelTransactionDetailRequest retrieveRefuelTransactionDetail) {
        this.retrieveRefuelTransactionDetail = retrieveRefuelTransactionDetail;
    }
}