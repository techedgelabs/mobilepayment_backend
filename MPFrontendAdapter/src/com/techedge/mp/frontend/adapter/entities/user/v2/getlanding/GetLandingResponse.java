package com.techedge.mp.frontend.adapter.entities.user.v2.getlanding;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetLandingResponse extends BaseResponse {

    private GetLandingResponseBody body;

    public GetLandingResponseBody getBody() {
        return body;
    }

    public void setBody(GetLandingResponseBody body) {
        this.body = body;
    }
}
