package com.techedge.mp.frontend.adapter.entities.postpaid.retrievestationlist;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;


public class RetrieveStationUserPositionRequest implements Validable {

	
	private Double latitude;
	private Double longitude;
	
	
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	
	@Override
	@Deprecated
	public Status check() {
		
		Status status = new Status();
		
	
		status.setStatusCode(StatusCode.STATION_RETRIEVE_SUCCESS);
		
		return status;
		
	}
	
	
}
