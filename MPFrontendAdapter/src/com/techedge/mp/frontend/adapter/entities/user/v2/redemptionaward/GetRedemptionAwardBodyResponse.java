package com.techedge.mp.frontend.adapter.entities.user.v2.redemptionaward;

public class GetRedemptionAwardBodyResponse {
    
    private String voucher;
    private Long orderId;
    
    public String getVoucher() {
        return voucher;
    }
    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }
    public Long getOrderId() {
        return orderId;
    }
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

}
