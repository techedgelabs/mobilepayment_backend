package com.techedge.mp.frontend.adapter.entities.user.getcreditvoucher;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;

public class GetCreditVoucherResponse extends BaseResponse {
    private GetCreditVoucherBodyResponse body;

    public GetCreditVoucherBodyResponse getBody() {
        return body;
    }

    public void setBody(GetCreditVoucherBodyResponse body) {
        this.body = body;
    }

}
