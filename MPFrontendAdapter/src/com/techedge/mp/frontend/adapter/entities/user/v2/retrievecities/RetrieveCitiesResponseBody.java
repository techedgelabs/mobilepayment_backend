package com.techedge.mp.frontend.adapter.entities.user.v2.retrievecities;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.v2.City;

public class RetrieveCitiesResponseBody {
	
	private List<City> citiesList = new ArrayList<City>(0);

	public List<City> getCitiesList() {
		return citiesList;
	}
	public void setCitiesList(List<City> citiesList) {
		this.citiesList = citiesList;
	}
	
}
