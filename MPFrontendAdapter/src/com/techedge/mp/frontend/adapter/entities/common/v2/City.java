package com.techedge.mp.frontend.adapter.entities.common.v2;


public class City {

	private String cityName;
	private String cityProvince;
	private String cityFiscalCode;
	
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	
	public String getCityProvince() {
		return cityProvince;
	}
	public void setCityProvince(String cityProvince) {
		this.cityProvince = cityProvince;
	}
	
	public String getCityFiscalCode() {
        return cityFiscalCode;
    }
	
	public void setCityFiscalCode(String cityFiscalCode) {
        this.cityFiscalCode = cityFiscalCode;
    }
}
