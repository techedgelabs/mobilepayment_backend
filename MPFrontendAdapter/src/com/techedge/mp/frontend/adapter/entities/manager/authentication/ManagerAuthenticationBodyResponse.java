package com.techedge.mp.frontend.adapter.entities.manager.authentication;

public class ManagerAuthenticationBodyResponse {

    private String                           ticketID;
    private ManagerAuthenticationManagerData userData;

    public String getTicketID() {
        return ticketID;
    }

    public void setTicketID(String ticketID) {
        this.ticketID = ticketID;
    }

    public ManagerAuthenticationManagerData getUserData() {
        return userData;
    }

    public void setUserData(ManagerAuthenticationManagerData userData) {
        this.userData = userData;
    }
}
