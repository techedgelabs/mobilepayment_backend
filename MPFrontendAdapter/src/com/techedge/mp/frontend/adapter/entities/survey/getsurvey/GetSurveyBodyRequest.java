package com.techedge.mp.frontend.adapter.entities.survey.getsurvey;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class GetSurveyBodyRequest implements Validable {

	private String code;

	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public Status check() {

		Status status = new Status();
		
		if (this.code == null || this.code.isEmpty()) {
			status.setStatusCode(StatusCode.SURVEY_REQU_INVALID_REQUEST);			
			return status;
		}
		
		status.setStatusCode(StatusCode.SURVEY_GET_SURVEY_SUCCESS);
		
		return status;
		
	}
	
}
