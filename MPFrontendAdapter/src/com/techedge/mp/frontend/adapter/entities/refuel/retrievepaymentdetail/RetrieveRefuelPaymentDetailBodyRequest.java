package com.techedge.mp.frontend.adapter.entities.refuel.retrievepaymentdetail;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveRefuelPaymentDetailBodyRequest implements Validable {

	private String refuelID;
	
	public String getRefuelID() {
		return refuelID;
	}

	public void setRefuelID(String refuelID) {
		this.refuelID = refuelID;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.refuelID == null || this.refuelID.length() != 32 || this.refuelID.trim() == "") {
			
			status.setStatusCode(StatusCode.RETRIEVE_PAYMENT_DETAIL_REFUELID_WRONG);
			return status;
		}
		
		status.setStatusCode(StatusCode.RETRIEVE_PAYMENT_DETAIL_SUCCESS);
		
		return status;
		
	}
}
