package com.techedge.mp.frontend.adapter.entities.eventnotification.retrievetransactiondetail;

import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RetrieveLoyaltyTransactionDetailBodyRequest implements Validable{
    
    private Long eventNotificationID;
    
    public Long getEventNotificationID() {
        return eventNotificationID;
    }

    public void setEventNotificationID(Long eventNotificationID) {
        this.eventNotificationID = eventNotificationID;
    }



    @Override
    public Status check() {
        
        Status status = new Status();
        
        if(this.eventNotificationID == null) {
            
            status.setStatusCode(StatusCode.RETRIEVE_EVENT_NOTIFICATION_TRANSACTION_DETAIL_INVALID_PARAMETERS);
            
            return status;          
        }
        
        status.setStatusCode(StatusCode.RETRIEVE_EVENT_NOTIFICATION_TRANSACTION_DETAIL_SUCCESS);
        
        return status;
    }

}
