package com.techedge.mp.frontend.adapter.entities.refuel.business.createtransaction;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class CreateRefuelTransactionDataRequest implements Validable {

	private String stationID;
	private String pumpID;
    private Integer amount;
	private PaymentMethod paymentMethod;
	private String outOfRange;
    
	public String getStationID() {
		return stationID;
	}
	public void setStationID(String stationID) {
		this.stationID = stationID;
	}
	
	public String getPumpID() {
		return pumpID;
	}
	public void setPumpID(String pumpID) {
		this.pumpID = pumpID;
	}
	
	public Integer getAmount() {
		return amount;
	}
	public void setAmount(Integer amount) {
		this.amount = amount;
	}
	
	public PaymentMethod getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(PaymentMethod paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
	
	public String getOutOfRange() {
		return outOfRange;
	}
	public void setOutOfRange(String outOfRange) {
		this.outOfRange = outOfRange;
	}
	
	
	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.stationID == null || this.stationID.length() > 10 || this.stationID.trim() == "") {
			
			status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG);
			
			return status;
			
		}
		
		if(this.pumpID == null || this.pumpID.length() != 9 || this.pumpID.trim() == "") {
			
			status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG);
			
			return status;
			
		}
		
		if(this.amount == null) {
			
			status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG);
			
			return status;
			
		}
		
		if(this.outOfRange != null && !this.outOfRange.equals("true") && !this.outOfRange.equals("false")) {
			
			status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_DATA_WRONG);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.REFUEL_TRANSACTION_CREATE_SUCCESS);
		
		return status;
		
	}
	
}
