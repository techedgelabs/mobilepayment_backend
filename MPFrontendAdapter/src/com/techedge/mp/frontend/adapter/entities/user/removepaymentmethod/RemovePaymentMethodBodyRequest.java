package com.techedge.mp.frontend.adapter.entities.user.removepaymentmethod;

import com.techedge.mp.frontend.adapter.entities.common.PaymentMethod;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;

public class RemovePaymentMethodBodyRequest implements Validable {

    private PaymentMethod paymentMethod;

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.paymentMethod == null) {

            status.setStatusCode(StatusCode.USER_REQU_INVALID_REQUEST);

            return status;
        }

        if (this.paymentMethod.getId() == null) {

            status.setStatusCode(StatusCode.USER_REMOVE_PAYMENT_METHOD_ID_WRONG);
            return status;
        }

        if (this.paymentMethod.getType() == null) {

            status.setStatusCode(StatusCode.USER_REMOVE_PAYMENT_METHOD_TYPE_WRONG);
            return status;
        }

        status.setStatusCode(StatusCode.USER_REMOVE_PAYMENT_METHOD_SUCCESS);

        return status;

    }

}
