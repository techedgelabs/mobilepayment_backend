package com.techedge.mp.frontend.adapter.entities.user.business.rescuepassword;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.business.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.business.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class RescuePasswordRequest extends AbstractRequest implements Validable {

    private Status                    status = new Status();

    private Credential                credential;
    private RescuePasswordBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RescuePasswordBodyRequest getBody() {
        return body;
    }

    public void setBody(RescuePasswordBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RESCUE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.USER_BUSINESS_RESCUE_PASSWORD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        RescuePasswordRequest rescuePasswordRequest = this;

        String response = getUserV2ServiceRemote().rescuePasswordBusiness(rescuePasswordRequest.getCredential().getTicketID(), rescuePasswordRequest.getCredential().getRequestID(),
                rescuePasswordRequest.getBody().getEmail());

        RescuePasswordResponse rescuePasswordResponse = new RescuePasswordResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        rescuePasswordResponse.setStatus(status);

        return rescuePasswordResponse;
    }

}
