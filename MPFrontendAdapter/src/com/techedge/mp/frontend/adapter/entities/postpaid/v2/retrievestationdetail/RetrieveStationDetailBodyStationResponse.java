package com.techedge.mp.frontend.adapter.entities.postpaid.v2.retrievestationdetail;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontend.adapter.entities.common.Cash;
import com.techedge.mp.frontend.adapter.entities.common.Pump;
import com.techedge.mp.frontend.adapter.entities.common.Station;

public class RetrieveStationDetailBodyStationResponse extends Station {

	private List<Pump> pumpList = new ArrayList<Pump>(0);
	private List<Cash> cashList = new ArrayList<Cash>(0);

	public List<Pump> getPumpList() {
		return pumpList;
	}
	public void setPumpList(List<Pump> pumpList) {
		this.pumpList = pumpList;
	}
	
	public List<Cash> getCashList() {
		return cashList;
	}
	public void setCashList(List<Cash> cashList) {
		this.cashList = cashList;
	}
}
