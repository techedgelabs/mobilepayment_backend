package com.techedge.mp.frontend.adapter.entities.system.business.retrieveapplicationsettings;


public class RetrieveApplicationSettingsSupportBodyResponse {

    private String callCenter;
    private String callCenterReal;
    private String callCenterText;

    public String getCallCenter() {
        return callCenter;
    }

    public void setCallCenter(String callCenter) {
        this.callCenter = callCenter;
    }

    public String getCallCenterReal() {
        return callCenterReal;
    }

    public void setCallCenterReal(String callCenterReal) {
        this.callCenterReal = callCenterReal;
    }

    public String getCallCenterText() {
        return callCenterText;
    }

    public void setCallCenterText(String callCenterText) {
        this.callCenterText = callCenterText;
    }
}
