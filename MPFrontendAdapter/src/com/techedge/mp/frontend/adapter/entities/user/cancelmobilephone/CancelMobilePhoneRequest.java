package com.techedge.mp.frontend.adapter.entities.user.cancelmobilephone;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontend.adapter.entities.common.Credential;
import com.techedge.mp.frontend.adapter.entities.common.Status;
import com.techedge.mp.frontend.adapter.entities.common.StatusCode;
import com.techedge.mp.frontend.adapter.entities.common.Validator;
import com.techedge.mp.frontend.adapter.entities.interfaces.Validable;
import com.techedge.mp.frontend.adapter.entities.requests.AbstractRequest;

public class CancelMobilePhoneRequest extends AbstractRequest implements Validable {

    private Status                       status = new Status();

    private Credential                   credential;
    private CancelMobilePhoneRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CancelMobilePhoneRequestBody getBody() {
        return body;
    }

    public void setBody(CancelMobilePhoneRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CANCEL-MOBILE-PHONE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        else {

            status.setStatusCode(StatusCode.CANCEL_MOBILE_PHONE_FAILURE);
            return status;

        }

        status.setStatusCode(StatusCode.CANCEL_MOBILE_PHONE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        CancelMobilePhoneRequest cancelMobilePhoneRequest = this;

        String response = getUserServiceRemote().cancelMobilePhoneUpdate(cancelMobilePhoneRequest.getCredential().getTicketID(),
                cancelMobilePhoneRequest.getCredential().getRequestID(), cancelMobilePhoneRequest.getBody().getMobilePhoneId());

        CancelMobilePhoneResponse cancelMobilePhoneResponse = new CancelMobilePhoneResponse();

        status.setStatusCode(response);

        status.setStatusMessage(prop.getProperty(response));

        cancelMobilePhoneResponse.setStatus(status);

        return cancelMobilePhoneResponse;
    }

}
