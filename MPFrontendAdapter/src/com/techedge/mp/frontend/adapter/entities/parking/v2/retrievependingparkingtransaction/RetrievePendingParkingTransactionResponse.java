package com.techedge.mp.frontend.adapter.entities.parking.v2.retrievependingparkingtransaction;

import com.techedge.mp.frontend.adapter.entities.common.BaseResponse;


public class RetrievePendingParkingTransactionResponse extends BaseResponse {

	
	private RetrievePendingParkingTransactionBodyResponse body;

	
	public RetrievePendingParkingTransactionBodyResponse getBody() {
		return body;
	}

	public void setBody(RetrievePendingParkingTransactionBodyResponse body) {
		this.body = body;
	}
	
	
}
