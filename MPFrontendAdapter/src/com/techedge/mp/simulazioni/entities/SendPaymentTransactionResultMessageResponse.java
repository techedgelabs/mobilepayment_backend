package com.techedge.mp.simulazioni.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendPaymentTransactionResultMessageResponse", propOrder = {
	"statusCode",
	"messageCode"
})
public class SendPaymentTransactionResultMessageResponse {

	
	@XmlElement(required=true, type=String.class)
	private ENUM_SEND_PAYMENT_TRANSACTION_RESULT_STATUS_RESPONSE statusCode;
	@XmlElement(required=true)
	private String messageCode;
	
	
	public ENUM_SEND_PAYMENT_TRANSACTION_RESULT_STATUS_RESPONSE getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(ENUM_SEND_PAYMENT_TRANSACTION_RESULT_STATUS_RESPONSE statusCode) {
		this.statusCode = statusCode;
	}
	
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	
	
}
