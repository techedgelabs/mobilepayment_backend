package com.techedge.mp.simulazioni.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendPaymentTransactionResultMessageRequest", propOrder = {
	"requestID",
	"transactionID",
	"paymentTransactionResult"
})
public class SendPaymentTransactionResultMessageRequest {

	
	@XmlElement(required=true)
	private String requestID;
	@XmlElement(required=true)
	private String transactionID;
	@XmlElement(required=true)
	private Boolean paymentTransactionResult;
	
	
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	public Boolean getPaymentTransactionResult() {
		return paymentTransactionResult;
	}
	public void setPaymentTransactionResult(Boolean paymentTransactionResult) {
		this.paymentTransactionResult = paymentTransactionResult;
	}
	
	
}
