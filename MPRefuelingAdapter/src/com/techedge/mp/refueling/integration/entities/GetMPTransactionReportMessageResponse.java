package com.techedge.mp.refueling.integration.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMPTransactionReportMessageResponse", propOrder = { "statusCode", "messageCode" })
public class GetMPTransactionReportMessageResponse {

    @XmlElement(required = true, nillable = true)
    protected EnumGetMPTransactionReportStatusCode statusCode;
    @XmlElement(required = true, nillable = true)
    protected String                               messageCode;

    public EnumGetMPTransactionReportStatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(EnumGetMPTransactionReportStatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

}