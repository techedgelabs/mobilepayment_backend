package com.techedge.mp.refueling.integration.entities;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(String.class)
public enum EnumCreateMPTransactionStatusCode {

    TRANSACTION_CREATED_200, TRANSACTION_NOT_CREATED_500, STATION_PUMP_NOT_FOUND_404, TOKEN_INVALID_500
}