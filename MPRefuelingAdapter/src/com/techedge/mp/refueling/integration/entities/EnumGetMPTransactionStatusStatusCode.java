package com.techedge.mp.refueling.integration.entities;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(String.class)
public enum EnumGetMPTransactionStatusStatusCode {

    MESSAGE_RECEIVED_200, TRANSACTION_NOT_RECOGNIZED_400, TICKET_INVALID_500, TOKEN_INVALID_500
}