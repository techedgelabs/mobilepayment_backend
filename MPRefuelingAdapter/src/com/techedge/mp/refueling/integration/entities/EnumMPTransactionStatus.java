package com.techedge.mp.refueling.integration.entities;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(String.class)
public enum EnumMPTransactionStatus {

    START_REFUELING ("START_REFUELING"),
    END_REFUELING ("END_REFUELING"), 
    REFUELING_IN_PROGRESS ("REFUELING_IN_PROGRESS"), 
    ERROR ("ERROR"), 
    AUTH_FAILED_451 ("AUTH_FAILED_451"), 
    AUTH_FAILED_462 ("AUTH_FAILED_462"), 
    AUTH_FAILED_500 ("AUTH_FAILED_500"), 
    RECONCILIATION ("RECONCILIATION");
    
    private final String status;
    
    private EnumMPTransactionStatus (String status) {
        
        this.status = status;
    }
    
    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : status.equals(otherName);
    }

    public String toString() {
       return this.status;
    }
    
    public String getValue() {
        return this.status;
    }
}