package com.techedge.mp.refueling.integration.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "refuelDetail", propOrder = { "timestampStartRefuel", "timestampEndRefuel", "authorizationCode", "amount", "fuelQuantity", "productID", "productDescription", "fuelType" })
public class RefuelDetail {

    @XmlElement(required = true)
    protected String        timestampStartRefuel;
    @XmlElement(required = true)
    protected String        timestampEndRefuel;
    @XmlElement(required = false)
    protected String        authorizationCode;
    @XmlElement(required = true)
    protected Double        amount;
    @XmlElement(required = false)
    protected Double        fuelQuantity;
    @XmlElement(required = false)
    protected EnumProductID productID;
    @XmlElement(required = false)
    protected String        productDescription;
    @XmlElement(required = false)
    protected String        fuelType;

    public String getTimestampStartRefuel() {
        return timestampStartRefuel;
    }

    public void setTimestampStartRefuel(String timestampStartRefuel) {
        this.timestampStartRefuel = timestampStartRefuel;
    }

    public String getTimestampEndRefuel() {
        return timestampEndRefuel;
    }

    public void setTimestampEndRefuel(String timestampEndRefuel) {
        this.timestampEndRefuel = timestampEndRefuel;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Double getFuelQuantity() {
        return fuelQuantity;
    }

    public void setFuelQuantity(Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }

    public EnumProductID getProductID() {
        return productID;
    }

    public void setProductID(EnumProductID productID) {
        this.productID = productID;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

}