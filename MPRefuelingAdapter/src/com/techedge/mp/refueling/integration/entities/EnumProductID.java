package com.techedge.mp.refueling.integration.entities;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(String.class)
public enum EnumProductID {

    SP, BS, GG, BD, MT, GP, AD
}