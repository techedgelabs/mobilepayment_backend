package com.techedge.mp.refueling.integration.entities;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(String.class)
public enum EnumGetMPTransactionReportStatusCode {

    MESSAGE_RECEIVED_200, PERIOD_INVALID_500, TOKEN_INVALID_500
}