package com.techedge.mp.refueling.integration.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMPTokenMessageResponse", propOrder = { "statusCode", "messageCode", "mpToken" })
public class GetMPTokenMessageResponse {

    @XmlElement(required = true, nillable = true)
    protected EnumGetMPTokenStatusCode statusCode;
    @XmlElement(required = true, nillable = true)
    protected String                        messageCode;
    @XmlElement(required = false, nillable = true)
    protected String                        mpToken;

    public EnumGetMPTokenStatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(EnumGetMPTokenStatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMpToken() {
        return mpToken;
    }

    public void setMpToken(String mpToken) {
        this.mpToken = mpToken;
    }

}