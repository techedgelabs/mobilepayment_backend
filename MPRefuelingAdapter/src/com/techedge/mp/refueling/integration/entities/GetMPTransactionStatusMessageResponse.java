package com.techedge.mp.refueling.integration.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getMPTransactionStatusMessageResponse", propOrder = { "statusCode", "messageCode", "mpTransactionID", "mpTransactionStatus", "refuelDetail" })
public class GetMPTransactionStatusMessageResponse {

    @XmlElement(required = true, nillable = true)
    protected EnumGetMPTransactionStatusStatusCode statusCode;
    @XmlElement(required = true, nillable = true)
    protected String                                     messageCode;
    @XmlElement(required = false, nillable = true)
    protected String                                     mpTransactionID;
    @XmlElement(required = false, nillable = true)
    protected EnumMPTransactionStatus                 mpTransactionStatus;
    @XmlElement(required = false, nillable = true)
    protected RefuelDetail                               refuelDetail;

    public EnumGetMPTransactionStatusStatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(EnumGetMPTransactionStatusStatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public EnumMPTransactionStatus getMpTransactionStatus() {
        return mpTransactionStatus;
    }

    public void setMpTransactionStatus(EnumMPTransactionStatus mpTransactionStatus) {
        this.mpTransactionStatus = mpTransactionStatus;
    }

    public RefuelDetail getRefuelDetail() {
        return refuelDetail;
    }

    public void setRefuelDetail(RefuelDetail refuelDetail) {
        this.refuelDetail = refuelDetail;
    }

}