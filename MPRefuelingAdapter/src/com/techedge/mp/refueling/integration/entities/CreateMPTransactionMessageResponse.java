package com.techedge.mp.refueling.integration.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "createMPTransactionMessageResponse", propOrder = { "statusCode", "messageCode", "mpTransactionID", "mpCreationTimestamp" })
public class CreateMPTransactionMessageResponse {

    @XmlElement(required = true, nillable = true)
    protected EnumCreateMPTransactionStatusCode statusCode;
    @XmlElement(required = true, nillable = true)
    protected String                                 messageCode;
    @XmlElement(required = false, nillable = true)
    protected String                                 mpTransactionID;
    @XmlElement(required = true, nillable = true)
    protected String                                 mpCreationTimestamp;

    public EnumCreateMPTransactionStatusCode getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(EnumCreateMPTransactionStatusCode statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public String getMpCreationTimestamp() {
        return mpCreationTimestamp;
    }

    public void setMpCreationTimestamp(String mpCreationTimestamp) {
        this.mpCreationTimestamp = mpCreationTimestamp;
    }

}