package com.techedge.mp.refueling.integration.utility.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.techedge.mp.refueling.integration.entities.GetMPTokenMessageRequest;
import com.techedge.mp.refueling.integration.utility.UtilityCheck;

public class TestGetMPTokenMessageRequest extends TestAbstractRequest {

    GetMPTokenMessageRequest request;
    
    @Override
    public void CreateInstance() {

        request  = new GetMPTokenMessageRequest();
        
    }
    
    @Test
    public void testRequiredFileds() {
        
        request.setPassword("123456");
        request.setRequestID("456576");
        request.setUsername("Andrea");
        
        assertTrue(UtilityCheck.isRequired(request));
    }
    
    @Test
    public void testRequiredFiledsExceptionRequestID() {

        request.setPassword("123456");
        request.setUsername("Andrea");
        
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Error in SOAP request. requestID is missing");
        
        UtilityCheck.isRequired(request);
    }
    
    @Test
    public void testRequiredFiledsExceptionRequestIDEmpty() {

        request.setPassword("123456");
        request.setRequestID("");
        request.setUsername("Andrea");
        
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Error in SOAP request. requestID is missing");
        
        UtilityCheck.isRequired(request);
    }

    @Test
    public void testRequestID() {
        
        request.setRequestID("ENJ-1234563456");

        super.testRequestIDValid(request.getRequestID());
    }
    
    @Test
    public void testRequestIDInvalid() {
        
        request.setRequestID("123456");

        super.testRequestIDInvalid(request.getRequestID());
    }
    
    @Test
    public void testUsernameLength(){
        
        String username = "test_user";
        
        assertTrue(UtilityCheck.isValidLength(username, 0, 50));
    }
    
    @Test
    public void testUsernameLengthFail(){
        
        String username = "test_user";
        
        assertFalse(UtilityCheck.isValidLength(username, 0, 3));
    }
    
}
