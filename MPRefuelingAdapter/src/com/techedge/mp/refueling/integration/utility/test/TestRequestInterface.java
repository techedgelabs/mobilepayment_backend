package com.techedge.mp.refueling.integration.utility.test;

public interface TestRequestInterface {
    
    public void testRequiredFileds();
    public void testRequiredFiledsExceptionRequestID();
    public void testRequiredFiledsExceptionRequestIDEmpty();
    public void testRequestIDValid(String requestID);
    public void testRequestIDInvalid(String requestID);

}
