package com.techedge.mp.refueling.integration.utility.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import com.techedge.mp.refueling.integration.utility.UtilityCheck;

public abstract class TestAbstractRequest implements TestRequestInterface {

    private static final String ENJ_SUFFIX = "ENJ-";
    
    @Rule
    public ExpectedException thrown = ExpectedException.none();
    
    
    @Before
    public abstract void CreateInstance();


    @Override
    public void testRequestIDValid(String requestID) {

        assertTrue(UtilityCheck.isValidRequestID(requestID, ENJ_SUFFIX));
    }


    @Override
    public void testRequestIDInvalid(String requestID) {

        assertFalse(UtilityCheck.isValidRequestID(requestID, ENJ_SUFFIX));
    }  
    
    
}
