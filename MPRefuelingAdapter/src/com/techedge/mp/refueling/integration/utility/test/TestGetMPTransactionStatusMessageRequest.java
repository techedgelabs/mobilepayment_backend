package com.techedge.mp.refueling.integration.utility.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.techedge.mp.refueling.integration.entities.GetMPTransactionStatusMessageRequest;
import com.techedge.mp.refueling.integration.utility.UtilityCheck;

public class TestGetMPTransactionStatusMessageRequest extends TestAbstractRequest{

    GetMPTransactionStatusMessageRequest request;
    
    @Override
    public void CreateInstance() {

        request = new GetMPTransactionStatusMessageRequest();        
    }
    
    @Test
    public void testRequiredFileds() {
        
        request.setMpToken("123456");
        request.setRequestID("456576");
        request.setSrcTransactionID("98765");
        request.setMpTransactionID("222");
        
        assertTrue(UtilityCheck.isRequired(request));
        
    }
    
    @Test
    public void testRequiredFiledsExceptionRequestID() {
        
        request.setMpToken("123456");
        request.setSrcTransactionID("98765");
        request.setMpTransactionID("222");
        
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Error in SOAP request. requestID is missing");
        
        UtilityCheck.isRequired(request);
    }
    
    @Test
    public void testRequiredFiledsExceptionRequestIDEmpty() {
        
        request.setMpToken("123456");
        request.setRequestID("");
        request.setSrcTransactionID("98765");
        request.setMpTransactionID("222");
        
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Error in SOAP request. requestID is missing");
        
        UtilityCheck.isRequired(request);
    }

    @Test
    public void testRequestID() {
        
        request.setRequestID("ENJ-1234567890");

        super.testRequestIDValid(request.getRequestID());
    }
    
    @Test
    public void testRequestIDInvalid() {
        
        request.setRequestID("123456");

        super.testRequestIDInvalid(request.getRequestID());
    }
}
