package com.techedge.mp.refueling.integration.utility.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.techedge.mp.refueling.integration.entities.GetMPTransactionReportMessageRequest;
import com.techedge.mp.refueling.integration.utility.UtilityCheck;

public class TestGetMPTransactionReportMessageRequest extends TestAbstractRequest{

    GetMPTransactionReportMessageRequest request;
    

    @Override
    public void CreateInstance() {

        request = new GetMPTransactionReportMessageRequest();
    }
    
    @Test
    public void testRequiredFileds() {

        request.setMpToken("123456");
        request.setRequestID("456576");
        request.setEndDate("20161202");
        request.setStartDate("20161101");
        request.setMpTransactionID("222");
        
        assertTrue(UtilityCheck.isRequired(request));
        
    }
    
    @Test
    public void testRequiredFiledsExceptionRequestID() {

        request.setMpToken("123456");
        request.setEndDate("20161202");
        request.setStartDate("20161101");
        request.setMpTransactionID("222");
        
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Error in SOAP request. requestID is missing");
        
        UtilityCheck.isRequired(request);
    }
    
    @Test
    public void testRequiredFiledsExceptionRequestIDEmpty() {

        request.setMpToken("123456");
        request.setRequestID("");
        request.setEndDate("20161202");
        request.setStartDate("20161101");
        request.setMpTransactionID("222");
        
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Error in SOAP request. requestID is missing");
        
        UtilityCheck.isRequired(request);
    }


    @Test(expected=ParseException.class)
    public void testCheckConvertDateFail() throws ParseException {

        request.setMpToken("123456");
        request.setRequestID("456576");
        request.setEndDate("2016-12-02");
        request.setStartDate("11-01-2016");
        request.setMpTransactionID("222");
        
        UtilityCheck.convertStringToDate(request.getStartDate());
        
    }
    
    @Test
    public void testCheckConvertDate() throws ParseException {

        request.setMpToken("123456");
        request.setRequestID("456576");
        request.setEndDate("2016-12-02");
        request.setStartDate("2016-11-01");
        request.setMpTransactionID("222");
        
        Date javaData = new Date();

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        format.setLenient(false);

        javaData = format.parse(request.getStartDate());
        
        Date returnedDate = UtilityCheck.convertStringToDate(request.getStartDate());
        
        assertEquals(javaData, returnedDate);
    }
    
    @Test
    public void testCheckDateFormat() {

        request.setMpToken("123456");
        request.setRequestID("456576");
        request.setEndDate("2016-12-02");
        request.setStartDate("2016-11-01");
        request.setMpTransactionID("222");
        
        assertTrue(UtilityCheck.isValidDate(request.getStartDate()));
    }
    
    @Test
    public void testCheckDateFormatFail() {

        request.setMpToken("123456");
        request.setRequestID("456576");
        request.setEndDate("2016-12-02");
        request.setStartDate("11-01-2016");
        request.setMpTransactionID("222");
        
        assertFalse(UtilityCheck.isValidDate(request.getStartDate()));
    }
    
    @Test
    public void testCheckStartDateBeforeEndDate() throws ParseException {

        request.setMpToken("123456");
        request.setRequestID("456576");
        request.setEndDate("2016-12-02");
        request.setStartDate("2016-11-02");
        request.setMpTransactionID("222");
        
        Date startDate = UtilityCheck.convertStringToDate(request.getStartDate());
        Date endDate = UtilityCheck.convertStringToDate(request.getEndDate());
        
        assertTrue(UtilityCheck.isValidDateRange(startDate, endDate));
    }
    
    @Test
    public void testCheckStartDateEndDateEquals() throws ParseException {

        request.setMpToken("123456");
        request.setRequestID("456576");
        request.setEndDate("2016-12-02");
        request.setStartDate("2016-12-02");
        request.setMpTransactionID("222");
        
        Date startDate = UtilityCheck.convertStringToDate(request.getStartDate());
        Date endDate = UtilityCheck.convertStringToDate(request.getEndDate());
        
        assertTrue(UtilityCheck.isValidDateRange(startDate, endDate));
    }
    
    @Test
    public void testCheckEndDateBeforeStartDataFail() throws ParseException {

        request.setMpToken("123456");
        request.setRequestID("456576");
        request.setEndDate("2015-12-02");
        request.setStartDate("2016-12-02");
        request.setMpTransactionID("222");
        
        Date startDate = UtilityCheck.convertStringToDate(request.getStartDate());
        Date endDate = UtilityCheck.convertStringToDate(request.getEndDate());
        
        assertFalse(UtilityCheck.isValidDateRange(startDate, endDate));
    }
    
    @Test
    public void testCheckEndDateNull() throws ParseException {

        request.setMpToken("123456");
        request.setRequestID("456576");
        request.setStartDate("2016-12-02");
        request.setMpTransactionID("222");
        
        Date startDate = UtilityCheck.convertStringToDate(request.getStartDate());
        
        assertFalse(UtilityCheck.isValidDateRange(startDate, null));
    }
    
    @Test
    public void testRequestID() {
        
        request.setRequestID("ENJ-1234567890");

        super.testRequestIDValid(request.getRequestID());
    }
    
    @Test
    public void testRequestIDInvalid() {
        
        request.setRequestID("123456");

        super.testRequestIDInvalid(request.getRequestID());
    }
    
    @Test
    public void testCheckValidDateRangeNotValid(){
        
        request.setEndDate("2014-01-25");
        request.setStartDate("2016-01-25");

        assertFalse(UtilityCheck.checkValidDate(request.getStartDate(), request.getEndDate()));
    }
    
    @Test
    public void testCheckValidDate(){
        
        request.setEndDate("2017-01-25");
        request.setStartDate("2016-01-25");
        
//        thrown.expect(RuntimeException.class);  
        
        assertTrue(UtilityCheck.checkValidDate(request.getStartDate(), request.getEndDate()));
    }
}
