package com.techedge.mp.refueling.integration.utility.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestCreateMPTransactionMessageRequest.class, TestGetMPTokenMessageRequest.class, TestGetMPTransactionReportMessageRequest.class,
        TestGetMPTransactionStatusMessageRequest.class })
public class AllTests {

}
