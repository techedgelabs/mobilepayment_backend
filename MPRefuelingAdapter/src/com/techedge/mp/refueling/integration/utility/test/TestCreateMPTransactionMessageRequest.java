package com.techedge.mp.refueling.integration.utility.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.techedge.mp.refueling.integration.entities.CreateMPTransactionMessageRequest;
import com.techedge.mp.refueling.integration.utility.UtilityCheck;

public class TestCreateMPTransactionMessageRequest extends TestAbstractRequest {
    
    CreateMPTransactionMessageRequest request;
    
    @Override
    public void CreateInstance() {

        request = new CreateMPTransactionMessageRequest();
    }
    
    @Test
    public void testRequiredFileds() {
        
        request.setAmount(1234.56);
        request.setCurrency("Andrea");
        request.setMpToken("123456");
        request.setPumpID("01");
        request.setRequestID("456576");
        request.setSrcTransactionID("98765");
        request.setStationID("222");
        
        assertTrue(UtilityCheck.isRequired(request));
        
    }
    
    @Test
    public void testRequiredFiledsExceptionRequestID() {
        
        request.setAmount(1234.56);
        request.setCurrency("Andrea");
        request.setMpToken("123456");
        request.setPumpID("01");
        request.setSrcTransactionID("98765");
        request.setStationID("222");
        
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Error in SOAP request. requestID is missing");
        
        UtilityCheck.isRequired(request);
    }
    
    @Test
    public void testRequiredFiledsExceptionRequestIDEmpty() {
        
        request.setAmount(1234.56);
        request.setCurrency("Andrea");
        request.setMpToken("123456");
        request.setPumpID("01");
        request.setSrcTransactionID("98765");
        request.setStationID("222");
        request.setRequestID("");
        
        thrown.expect(RuntimeException.class);
        thrown.expectMessage("Error in SOAP request. requestID is missing");
        
        UtilityCheck.isRequired(request);
    }

    @Test
    public void testRequiredFiledsExceptionCastAmount() {
        
        request.setRequestID("456576");
        request.setAmount(1234.56);
        request.setCurrency("Andrea");
        request.setMpToken("123456");
        request.setPumpID("01");
        request.setSrcTransactionID("98765");
        request.setStationID("222");
        
        thrown.expect(ClassCastException.class);
        
        UtilityCheck.isRequiredCastException(request);
    }
    
    @Test
    public void testRequestID() {
        
        request.setRequestID("ENJ-1234567890");

        super.testRequestIDValid(request.getRequestID());
    }
    
    @Test
    public void testRequestIDChar() {
        
        request.setRequestID("ENJ-123456789a");

        super.testRequestIDInvalid(request.getRequestID());
    }
    
    @Test
    public void testRequestIDInvalid() {
        
        request.setRequestID("123456");

        super.testRequestIDInvalid(request.getRequestID());
    }
}
