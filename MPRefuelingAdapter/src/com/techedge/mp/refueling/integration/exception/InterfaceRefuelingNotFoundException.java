package com.techedge.mp.refueling.integration.exception;

public class InterfaceRefuelingNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8830944981038276518L;
	
	private String jndiString;
	
	public InterfaceRefuelingNotFoundException(String jndiString) {
		
		this.jndiString = jndiString;
	}
	
	public String getJndiString()
	{
		return jndiString;
	}
}
