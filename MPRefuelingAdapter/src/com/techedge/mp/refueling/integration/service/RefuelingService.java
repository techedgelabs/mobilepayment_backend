package com.techedge.mp.refueling.integration.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.naming.Context;
import javax.xml.bind.annotation.XmlElement;

import com.techedge.mp.bpel.operation.refuel.business.BPELServiceRemote;
import com.techedge.mp.bpel.operation.refuel.business.interfaces.StationInfoRequest;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.RefuelingServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingCreateMPTransactionResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTokenResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTransactionReportResponse;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTransactionStatusResponse;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetPumpStatusResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail;
import com.techedge.mp.refueling.integration.entities.CreateMPTransactionMessageRequest;
import com.techedge.mp.refueling.integration.entities.CreateMPTransactionMessageResponse;
import com.techedge.mp.refueling.integration.entities.EnumCreateMPTransactionStatusCode;
import com.techedge.mp.refueling.integration.entities.EnumGetMPTokenStatusCode;
import com.techedge.mp.refueling.integration.entities.EnumGetMPTransactionReportStatusCode;
import com.techedge.mp.refueling.integration.entities.EnumGetMPTransactionStatusStatusCode;
import com.techedge.mp.refueling.integration.entities.EnumMPTransactionStatus;
import com.techedge.mp.refueling.integration.entities.EnumProductID;
import com.techedge.mp.refueling.integration.entities.GetMPTokenMessageRequest;
import com.techedge.mp.refueling.integration.entities.GetMPTokenMessageResponse;
import com.techedge.mp.refueling.integration.entities.GetMPTransactionReportMessageRequest;
import com.techedge.mp.refueling.integration.entities.GetMPTransactionReportMessageResponse;
import com.techedge.mp.refueling.integration.entities.GetMPTransactionStatusMessageRequest;
import com.techedge.mp.refueling.integration.entities.GetMPTransactionStatusMessageResponse;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;
import com.techedge.mp.refueling.integration.exception.InterfaceRefuelingNotFoundException;
import com.techedge.mp.refueling.integration.utility.UtilityCheck;

@WebService()
public class RefuelingService {

    private static final String        PARAM_REFUELING_CURRENCY = "REFUELING_CURRENCY";

    private static final String        SUCCESS_CODE             = "_200";

    private static final int           MAX_PASSWORD_LENGTH      = 40;

    private static final int           MAX_USERNAME_LENGTH      = 50;

    private static final int           MIN_LENGTH               = 0;

    private static final String        ENJ_SUFFIX               = "ENJ-";

    private Properties                 prop                     = new Properties();

    private LoggerServiceRemote        loggerService;
    private RefuelingServiceRemote     refuelingService;
    private ForecourtInfoServiceRemote forecourtInfoService;
    private BPELServiceRemote          bpelService;
    private ParametersServiceRemote    parametersService;

    public RefuelingService() {

        loadFileProperty();
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    @WebMethod(operationName = "getMPToken")
    @WebResult(name = "getMPTokenResponse")
    public @XmlElement(required = true)
    GetMPTokenMessageResponse getMPToken(@XmlElement(required = true) @WebParam(name = "getMPTokenRequest") GetMPTokenMessageRequest getMPTokenMessageRequest) {

        GetMPTokenMessageResponse getMPTokenMessageResponse = new GetMPTokenMessageResponse();
        RefuelingGetMPTokenResponse refuelingTokenReponse = new RefuelingGetMPTokenResponse();

        String requestID = getMPTokenMessageRequest.getRequestID();
        String username = getMPTokenMessageRequest.getUsername();
        String password = getMPTokenMessageRequest.getPassword();

        UtilityCheck.isRequired(getMPTokenMessageRequest);

        checkRequestID(requestID);

        if (!UtilityCheck.isValidLength(username, MIN_LENGTH, MAX_USERNAME_LENGTH) || !UtilityCheck.isValidLength(password, 0, MAX_PASSWORD_LENGTH)) {

            throw new RuntimeException("Invalid username or password length!");
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("username", username));
        inputParameters.add(new Pair<String, String>("password", password));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMPToken", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        try {

            this.refuelingService = EJBHomeCache.getInstance().getRefuelingService();
            refuelingTokenReponse = this.refuelingService.getMPToken(requestID, username, password);

        }
        catch (InterfaceRefuelingNotFoundException e) {

            e.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getMPToken", requestID, null, "Exception message: " + e.getMessage());

            getMPTokenMessageResponse.setStatusCode(EnumGetMPTokenStatusCode.USER_AUTH_500);
            getMPTokenMessageResponse.setMessageCode(prop.getProperty(refuelingTokenReponse.getStatusCode()));
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", refuelingTokenReponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", refuelingTokenReponse.getMessageCode()));
        outputParameters.add(new Pair<String, String>("mpToken", refuelingTokenReponse.getMpToken()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMPToken", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        if (refuelingTokenReponse.getStatusCode().equals(ResponseHelper.USER_AUTH_FAILED)) {

            getMPTokenMessageResponse.setStatusCode(EnumGetMPTokenStatusCode.USER_AUTH_500);
            getMPTokenMessageResponse.setMessageCode(prop.getProperty(refuelingTokenReponse.getStatusCode()));

        }
        else {

            getMPTokenMessageResponse.setStatusCode(EnumGetMPTokenStatusCode.USER_AUTH_200);
            getMPTokenMessageResponse.setMessageCode(prop.getProperty(refuelingTokenReponse.getStatusCode()));
            getMPTokenMessageResponse.setMpToken(refuelingTokenReponse.getMpToken());
        }

        return getMPTokenMessageResponse;

    }

    @WebMethod(operationName = "createMPTransaction")
    @WebResult(name = "createMPTransactionResponse")
    public @XmlElement(required = true)
    CreateMPTransactionMessageResponse createMPTransaction(
            @XmlElement(required = true) @WebParam(name = "createMPTransactionRequest") CreateMPTransactionMessageRequest createMPTransactionMessageRequest) {

        UtilityCheck.isRequired(createMPTransactionMessageRequest);

        String requestID = createMPTransactionMessageRequest.getRequestID();
        checkRequestID(requestID);

        String mpToken = createMPTransactionMessageRequest.getMpToken();
        String srcTransactionID = createMPTransactionMessageRequest.getSrcTransactionID();
        String stationID = createMPTransactionMessageRequest.getStationID();
        String pumpID = createMPTransactionMessageRequest.getPumpID();
        Double amount = createMPTransactionMessageRequest.getAmount();
        String currentCurrency = createMPTransactionMessageRequest.getCurrency();

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpToken", mpToken));
        inputParameters.add(new Pair<String, String>("srcTransactionID", srcTransactionID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
        inputParameters.add(new Pair<String, String>("amount", String.valueOf(amount)));
        inputParameters.add(new Pair<String, String>("currency", currentCurrency));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMPTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateMPTransactionMessageResponse createMPTransactionMessageResponse = new CreateMPTransactionMessageResponse();
        RefuelingCreateMPTransactionResponse refuelingResponse = new RefuelingCreateMPTransactionResponse();

        try {

            this.parametersService = EJBHomeCache.getInstance().getParametersService();
            String currency = parametersService.getParamValue(PARAM_REFUELING_CURRENCY);
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "createMPTransaction", requestID, null, "Retrieved Currency : " + currency);

            this.forecourtInfoService = EJBHomeCache.getInstance().getForecourtInfoService();
            StationInfoRequest stationInfoRequest = new StationInfoRequest();

            stationInfoRequest.setRequestID(requestID);
            stationInfoRequest.setStationID(stationID);
            stationInfoRequest.setPumpID(pumpID);
            stationInfoRequest.setPumpDetailsReq(false);

            GetStationDetailsResponse getStationDetailsResponse = forecourtInfoService.getStationDetails(requestID, stationID, pumpID, Boolean.FALSE);

            String stationStatusCode = getStationDetailsResponse.getStatusCode();

            if (isValidCode(stationStatusCode, refuelingResponse)) {

                if (!isPumpListEmpty(getStationDetailsResponse.getStationDetail().getPumpDetails(), refuelingResponse)) {

                    GetPumpStatusResponse getPumpStatusResponse = forecourtInfoService.getPumpStatus(requestID, stationID, pumpID, null);

                    if (isValidCode(getPumpStatusResponse.getStatusCode(), refuelingResponse)) {

                        Integer pumpNumber = extractPumpNumber(getStationDetailsResponse);
                        String refuelMode = getPumpStatusResponse.getRefuelMode();

                        Set<Pair<String, String>> inputParametersCreateTransaction = new HashSet<Pair<String, String>>();
                        inputParameters.add(new Pair<String, String>("requestID", requestID));
                        inputParameters.add(new Pair<String, String>("stationID", stationID));
                        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
                        inputParameters.add(new Pair<String, String>("pumpNumber", String.valueOf(pumpNumber)));
                        inputParameters.add(new Pair<String, String>("amount", String.valueOf(amount)));
                        inputParameters.add(new Pair<String, String>("refuelMode", refuelMode));

                        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMPTransaction", requestID, null,
                                ActivityLog.createLogMessage(inputParametersCreateTransaction));

                        refuelingResponse = callRefuelingServiceCreateTransaction(requestID, mpToken, srcTransactionID, stationID, pumpID, amount, currency, pumpNumber, refuelMode);

                        if (!isTransactionIDNull(refuelingResponse)) {

                            callBPELService(stationID, pumpID, amount, currency, refuelingResponse);
                        }
                    }
                }
            }
        }
        catch (InterfaceRefuelingNotFoundException e) {

            e.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createMPTransaction", requestID, null, "Exception message: " + e.getMessage());

            createMPTransactionMessageResponse.setStatusCode(EnumCreateMPTransactionStatusCode.TRANSACTION_NOT_CREATED_500);
            createMPTransactionMessageResponse.setMessageCode(prop.getProperty(refuelingResponse.getStatusCode()));
        }
        catch (ParameterNotFoundException ex) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, ex.getMessage());
            createMPTransactionMessageResponse.setStatusCode(EnumCreateMPTransactionStatusCode.TRANSACTION_NOT_CREATED_500);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", refuelingResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", refuelingResponse.getMessageCode()));
        outputParameters.add(new Pair<String, String>("mpTransactionID", refuelingResponse.getMpTransactionID()));
        outputParameters.add(new Pair<String, String>("mpCreationTimestamp", refuelingResponse.getMpCreationTimestamp()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMPTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        if (refuelingResponse != null && !refuelingResponse.getStatusCode().contains(SUCCESS_CODE)) {

            createMPTransactionMessageResponse.setStatusCode(EnumCreateMPTransactionStatusCode.valueOf(refuelingResponse.getStatusCode()));
            createMPTransactionMessageResponse.setMessageCode(prop.getProperty(refuelingResponse.getStatusCode()));

        }
        else {

            createMPTransactionMessageResponse.setStatusCode(EnumCreateMPTransactionStatusCode.valueOf(refuelingResponse.getStatusCode()));
            createMPTransactionMessageResponse.setMessageCode(prop.getProperty(refuelingResponse.getStatusCode()));
            createMPTransactionMessageResponse.setMpTransactionID(refuelingResponse.getMpTransactionID());
            createMPTransactionMessageResponse.setMpCreationTimestamp(refuelingResponse.getMpCreationTimestamp());
        }

        return createMPTransactionMessageResponse;
    }

    private boolean isValidCode(String code, RefuelingCreateMPTransactionResponse refuelingResponse) {

        boolean statusValid = true;

        if (code.equals("PUMP_NOT_FOUND_404")) {

            setStatusCodeStationPumpNotFound(refuelingResponse);
            statusValid = false;
        }
        else {

            if (!code.contains("200")) {

                setStatusCodeFailure(refuelingResponse);
                statusValid = false;
            }
        }

        return statusValid;

    }

    private RefuelingCreateMPTransactionResponse callRefuelingServiceCreateTransaction(String requestID, String mpToken, String srcTransactionID, String stationID, String pumpID,
            Double amount, String currency, Integer pumpNumber, String refuelMode) throws InterfaceRefuelingNotFoundException {

        RefuelingCreateMPTransactionResponse refuelingResponse;

        this.refuelingService = EJBHomeCache.getInstance().getRefuelingService();
        refuelingResponse = this.refuelingService.createMPTransaction(requestID, mpToken, srcTransactionID, stationID, pumpID, amount, currency, pumpNumber, refuelMode);

        return refuelingResponse;
    }

    private Integer extractPumpNumber(GetStationDetailsResponse getStationDetailsResponse) {

        // Il processo di rifornimento viene avviato solo se la pompa � disponibile
        PumpDetail pumpSelected = null;
        for (PumpDetail pumpDetail : getStationDetailsResponse.getStationDetail().getPumpDetails()) {

            pumpSelected = pumpDetail;
        }

        Integer pumpNumber = 0;
        try {
            pumpNumber = Integer.parseInt(pumpSelected.getPumpNumber());
        }
        catch (Exception ex) {

            System.out.println("Pump number parsing error: " + pumpSelected.getPumpNumber());
        }
        return pumpNumber;
    }

    private void setStatusCodeFailure(RefuelingCreateMPTransactionResponse refuelingResponse) {

        refuelingResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
    }

    private void setStatusCodeStationPumpNotFound(RefuelingCreateMPTransactionResponse refuelingResponse) {

        refuelingResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_STATION_PUMP_NOT_FOUND);
    }

    private boolean isTransactionIDNull(RefuelingCreateMPTransactionResponse refuelingResponse) {

        boolean isNull = false;

        if (refuelingResponse.getMpTransactionID() == null) {
            setStatusCodeFailure(refuelingResponse);
            isNull = true;
        }

        return isNull;
    }

    private boolean isPumpListEmpty(List<PumpDetail> pumpList, RefuelingCreateMPTransactionResponse refuelingResponse) {

        boolean isEmplyList = false;

        if (pumpList.isEmpty()) {
            setStatusCodeFailure(refuelingResponse);
            isEmplyList = true;
        }

        return isEmplyList;
    }

    private void callBPELService(String stationID, String pumpID, Double amount, String currency, RefuelingCreateMPTransactionResponse refuelingResponse)
            throws InterfaceRefuelingNotFoundException {

        this.bpelService = EJBHomeCache.getInstance().getBpelService();

        //mpToken da passare token associato 
        String response = bpelService.startRefuelingProcess(refuelingResponse.getMpTransactionID(), stationID, pumpID, amount, refuelingResponse.getMpAcquirerID(),
                refuelingResponse.getMpShopLogin(), refuelingResponse.getCurrency(), refuelingResponse.getMpTransactionID(), refuelingResponse.getToken(), "", "", "");

        System.out.println("BPEL response: " + response);

        if (response.contains("200")) {

            refuelingResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_SUCCESS);
        }
        else {

            setStatusCodeFailure(refuelingResponse);
        }
    }

    @WebMethod(operationName = "getMPTransactionStatus")
    @WebResult(name = "getMPTransactionStatusResponse")
    public @XmlElement(required = true)
    GetMPTransactionStatusMessageResponse getMPTransactionStatus(
            @XmlElement(required = true) @WebParam(name = "getMPTransactionStatusRequest") GetMPTransactionStatusMessageRequest getMPTransactionStatusMessageRequest) {

        UtilityCheck.isRequired(getMPTransactionStatusMessageRequest);

        String requestID = getMPTransactionStatusMessageRequest.getRequestID();
        checkRequestID(requestID);

        String mpTransactionID = getMPTransactionStatusMessageRequest.getMpTransactionID();
        if (mpTransactionID != null && !mpTransactionID.equals("")) {

            checkLength(mpTransactionID);
        }

        String mpToken = getMPTransactionStatusMessageRequest.getMpToken();
        String srcTransactionID = getMPTransactionStatusMessageRequest.getSrcTransactionID();

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpToken", mpToken));
        inputParameters.add(new Pair<String, String>("srcTransactionID", srcTransactionID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMPTransactionStatus", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetMPTransactionStatusMessageResponse getMPTransactionStatusMessageResponse = new GetMPTransactionStatusMessageResponse();
        RefuelingGetMPTransactionStatusResponse refuelingGetMPTransactionStatusResponse = new RefuelingGetMPTransactionStatusResponse();

        try {

            this.refuelingService = EJBHomeCache.getInstance().getRefuelingService();
            refuelingGetMPTransactionStatusResponse = this.refuelingService.getMPTransactionStatus(requestID, mpToken, srcTransactionID, mpTransactionID);

        }
        catch (InterfaceRefuelingNotFoundException e) {

            e.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createMPTransaction", requestID, null, "Exception message: " + e.getMessage());

            getMPTransactionStatusMessageResponse.setStatusCode(EnumGetMPTransactionStatusStatusCode.TICKET_INVALID_500);
            getMPTransactionStatusMessageResponse.setMessageCode(refuelingGetMPTransactionStatusResponse.getMessageCode());
        }

        if (!refuelingGetMPTransactionStatusResponse.getStatusCode().contains(SUCCESS_CODE)) {

            getMPTransactionStatusMessageResponse.setStatusCode(EnumGetMPTransactionStatusStatusCode.valueOf(refuelingGetMPTransactionStatusResponse.getStatusCode()));
            getMPTransactionStatusMessageResponse.setMessageCode(prop.getProperty(refuelingGetMPTransactionStatusResponse.getStatusCode()));
        }
        else {

            getMPTransactionStatusMessageResponse.setStatusCode(EnumGetMPTransactionStatusStatusCode.valueOf(refuelingGetMPTransactionStatusResponse.getStatusCode()));
            getMPTransactionStatusMessageResponse.setMessageCode(prop.getProperty(refuelingGetMPTransactionStatusResponse.getStatusCode()));
            getMPTransactionStatusMessageResponse.setMpTransactionID(refuelingGetMPTransactionStatusResponse.getMpTransactionID());

            try {
                if (EnumMPTransactionStatus.valueOf(refuelingGetMPTransactionStatusResponse.getMpTransactionStatus()) != null) {
                    getMPTransactionStatusMessageResponse.setMpTransactionStatus(EnumMPTransactionStatus.valueOf(refuelingGetMPTransactionStatusResponse.getMpTransactionStatus()));
                }
            }
            catch (Exception ex) {
                System.out.println("Valore non presente in EnumMPTransactionStatus");
            }
            RefuelDetail refuelDetail = convertRefuelDetail(refuelingGetMPTransactionStatusResponse.getRefuelDetail());

            getMPTransactionStatusMessageResponse.setRefuelDetail(refuelDetail);

        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", refuelingGetMPTransactionStatusResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", refuelingGetMPTransactionStatusResponse.getMessageCode()));
        outputParameters.add(new Pair<String, String>("mpTransactionID", refuelingGetMPTransactionStatusResponse.getMpTransactionID()));
        outputParameters.add(new Pair<String, String>("mpTransactionStatus", refuelingGetMPTransactionStatusResponse.getMpTransactionStatus()));

        if (refuelingGetMPTransactionStatusResponse.getRefuelDetail() != null) {

            outputParameters.add(new Pair<String, String>("refuelDetail", refuelingGetMPTransactionStatusResponse.getRefuelDetail().toString()));
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMPTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getMPTransactionStatusMessageResponse;
    }

    private RefuelDetail convertRefuelDetail(com.techedge.mp.core.business.interfaces.refueling.RefuelDetail refuelDetail) {

        if (refuelDetail == null) {
            return null;
        }

        RefuelDetail returnedRefuelDetail = new RefuelDetail();

        returnedRefuelDetail.setTimestampStartRefuel(refuelDetail.getTimestampStartRefuel());
        returnedRefuelDetail.setTimestampEndRefuel(refuelDetail.getTimestampEndRefuel());
        returnedRefuelDetail.setAuthorizationCode(refuelDetail.getAuthorizationCode());
        returnedRefuelDetail.setAmount(refuelDetail.getAmount());
        returnedRefuelDetail.setFuelQuantity(refuelDetail.getFuelQuantity());
        returnedRefuelDetail.setProductDescription(refuelDetail.getProductDescription());
        returnedRefuelDetail.setFuelType(refuelDetail.getFuelType());
        returnedRefuelDetail.setProductID(EnumProductID.valueOf(refuelDetail.getProductID()));

        return returnedRefuelDetail;
    }

    @WebMethod(operationName = "getMPTransactionReport")
    @WebResult(name = "getMPTransactionReportResponse")
    public @XmlElement(required = true)
    GetMPTransactionReportMessageResponse getMPTransactionReport(
            @XmlElement(required = true) @WebParam(name = "getMPTransactionReportRequest") GetMPTransactionReportMessageRequest getMPTransactionReportMessageRequest) {

        String startDate = "";
        String endDate = "";
        String requestID = "";

        requestID = getMPTransactionReportMessageRequest.getRequestID();
        checkRequestID(requestID);

        try {
            UtilityCheck.isRequired(getMPTransactionReportMessageRequest);

            startDate = getMPTransactionReportMessageRequest.getStartDate();
            endDate = getMPTransactionReportMessageRequest.getEndDate();

            checkValidDate(startDate, endDate);
        }
        catch (RuntimeException e) {

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createMPTransaction", requestID, null, "Exception message: " + e.getMessage());
            GetMPTransactionReportMessageResponse getMPTransactionReportMessageResponse = new GetMPTransactionReportMessageResponse();

            getMPTransactionReportMessageResponse.setStatusCode(EnumGetMPTransactionReportStatusCode.PERIOD_INVALID_500);
            return getMPTransactionReportMessageResponse;

        }

        String mpTransactionID = getMPTransactionReportMessageRequest.getMpTransactionID();
        if (mpTransactionID != null && !mpTransactionID.equals("")) {

            checkLength(mpTransactionID);
        }

        String mpToken = getMPTransactionReportMessageRequest.getMpToken();

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpToken", mpToken));
        inputParameters.add(new Pair<String, String>("startDate", startDate));
        inputParameters.add(new Pair<String, String>("endDate", endDate));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMPTransactionReport", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetMPTransactionReportMessageResponse getMPTransactionReportMessageResponse = new GetMPTransactionReportMessageResponse();
        RefuelingGetMPTransactionReportResponse refuelingGetMPTransactionReportResponse = new RefuelingGetMPTransactionReportResponse();

        try {

            this.refuelingService = EJBHomeCache.getInstance().getRefuelingService();
            refuelingGetMPTransactionReportResponse = this.refuelingService.getMPTransactionReport(requestID, mpToken, startDate, endDate, mpTransactionID);

        }
        catch (InterfaceRefuelingNotFoundException e) {

            e.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createMPTransaction", requestID, null, "Exception message: " + e.getMessage());

            getMPTransactionReportMessageResponse.setStatusCode(EnumGetMPTransactionReportStatusCode.TOKEN_INVALID_500);
            getMPTransactionReportMessageResponse.setMessageCode(refuelingGetMPTransactionReportResponse.getMessageCode());
        }

        if (!refuelingGetMPTransactionReportResponse.getStatusCode().contains(SUCCESS_CODE)) {

            getMPTransactionReportMessageResponse.setStatusCode(EnumGetMPTransactionReportStatusCode.valueOf(refuelingGetMPTransactionReportResponse.getStatusCode()));
            getMPTransactionReportMessageResponse.setMessageCode(prop.getProperty(refuelingGetMPTransactionReportResponse.getStatusCode()));
        }
        else {

            getMPTransactionReportMessageResponse.setStatusCode(EnumGetMPTransactionReportStatusCode.valueOf(refuelingGetMPTransactionReportResponse.getStatusCode()));
            getMPTransactionReportMessageResponse.setMessageCode(prop.getProperty(refuelingGetMPTransactionReportResponse.getStatusCode()));
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getMPTransactionReportMessageResponse.getStatusCode().toString()));
        outputParameters.add(new Pair<String, String>("statusMessage", getMPTransactionReportMessageResponse.getMessageCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMPTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getMPTransactionReportMessageResponse;
    }

    private void checkLength(String mpTransactionID) {

        if (!UtilityCheck.isEquals(mpTransactionID, 32)) {
            throw new RuntimeException("TransactionID not correct!");
        }

    }

    private void checkValidDate(String startDate, String endDate) {

        if (!UtilityCheck.checkValidDate(startDate, endDate)) {
            throw new RuntimeException(EnumGetMPTransactionReportStatusCode.PERIOD_INVALID_500.toString());
        }
    }

    private void checkRequestID(String requestID) {

        if (!UtilityCheck.isValidRequestID(requestID, ENJ_SUFFIX)) {

            throw new RuntimeException("Invalid requestID!");
        }
    }

    private void loadFileProperty() {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("response_service.properties");
        try {
            prop.load(inputStream);
            if (inputStream == null) {
                throw new FileNotFoundException("property file response_service.properties not found in the classpath");
            }
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }
}
