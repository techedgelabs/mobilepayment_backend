package com.techedge.mp.refueling.integration.service;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.techedge.mp.bpel.operation.refuel.business.BPELServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.RefuelingServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.refueling.integration.exception.InterfaceRefuelingNotFoundException;

public class EJBHomeCache {

    final String                         loggerServiceRemoteJndi        = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
    final String                         refuelingServiceRemoteJndi     = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/RefuelingService!com.techedge.mp.core.business.RefuelingServiceRemote";
    final String                         forecourtServiceRemoteJndi     = "java:global/MPFrontendAdapterEAR/MPFrontendAdapterEJB/ForecourtService!com.techedge.mp.frontend.adapter.business.ForecourtServiceRemote";
    final String                         forecourtInfoServiceRemoteJndi = "java:global/MPForecourtAdapterEAR/MPForecourtAdapterEJB/ForecourtInfoService!com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote";
    final String                         bpelServiceRemoteJndi          = "java:global/MPBPELOperationRefuelEAR/MPBPELOperationRefuelEJB/BPELService!com.techedge.mp.bpel.operation.refuel.business.BPELServiceRemote";
    final String                         parametersServiceRemoteJndi    = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";

    private static EJBHomeCache          instance;

    protected Context                    context                        = null;

    protected LoggerServiceRemote        loggerService                  = null;
    protected RefuelingServiceRemote     refuelingService               = null;
    protected ForecourtInfoServiceRemote forecourtInfoService           = null;
    protected BPELServiceRemote          bpelService                    = null;
    protected ParametersServiceRemote    parametersService              = null;

    private EJBHomeCache() throws InterfaceRefuelingNotFoundException {

        final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();

        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

        try {
            context = new InitialContext(jndiProperties);
        }
        catch (NamingException e) {

            throw new InterfaceRefuelingNotFoundException("Naming exception: " + e.getMessage());
        }

        try {
            loggerService = (LoggerServiceRemote) context.lookup(loggerServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceRefuelingNotFoundException(loggerServiceRemoteJndi);
        }

        try {
            refuelingService = (RefuelingServiceRemote) context.lookup(refuelingServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceRefuelingNotFoundException(refuelingServiceRemoteJndi);
        }
        try {
            forecourtInfoService = (ForecourtInfoServiceRemote) context.lookup(forecourtInfoServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceRefuelingNotFoundException(forecourtInfoServiceRemoteJndi);
        }
        try {
            bpelService = (BPELServiceRemote) context.lookup(bpelServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceRefuelingNotFoundException(bpelServiceRemoteJndi);
        }
        try {
            parametersService = (ParametersServiceRemote) context.lookup(parametersServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceRefuelingNotFoundException(parametersServiceRemoteJndi);
        }

    }

    public static synchronized EJBHomeCache getInstance() throws InterfaceRefuelingNotFoundException {
        if (instance == null)
            instance = new EJBHomeCache();

        return instance;
    }

    public RefuelingServiceRemote getRefuelingService() {
        return refuelingService;
    }

    public LoggerServiceRemote getLoggerService() {
        return loggerService;
    }

    public BPELServiceRemote getBpelService() {

        return bpelService;
    }

    public ParametersServiceRemote getParametersService() {

        return parametersService;
    }

    public ForecourtInfoServiceRemote getForecourtInfoService() {
        return forecourtInfoService;
    }

    public void setForecourtInfoService(ForecourtInfoServiceRemote forecourtInfoService) {
        this.forecourtInfoService = forecourtInfoService;
    }

}
