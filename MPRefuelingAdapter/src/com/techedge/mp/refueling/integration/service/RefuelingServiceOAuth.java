package com.techedge.mp.refueling.integration.service;

import java.util.Hashtable;

import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.bind.annotation.XmlElement;

import com.techedge.mp.core.business.RefuelingServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.entities.GetMPTokenMessageRequest;
import com.techedge.mp.refueling.integration.entities.GetMPTokenMessageResponse;
import com.techedge.mp.refueling.integration.exception.InterfaceRefuelingNotFoundException;

@WebService()
//@HandlerChain(file = "handler-chain.xml")
public class RefuelingServiceOAuth extends RefuelingService {

    private RefuelingNotificationServiceOAuth2     refuelingServiceOAuth2;

    @WebMethod(operationName = "testOAuth2")
    @WebResult(name = "getMPTokenResponse")
    public @XmlElement(required = true)
    GetMPTokenMessageResponse testOAuth2() {

        GetMPTokenMessageResponse getMPTokenMessageResponse = new GetMPTokenMessageResponse();
        
        refuelingServiceOAuth2 = getRefuelingNotificationServiceOAuth2();
        
        refuelingServiceOAuth2.testOAuth2();
        
        
        return getMPTokenMessageResponse;
    }
    
    private RefuelingNotificationServiceOAuth2 getRefuelingNotificationServiceOAuth2() {

        //final String jndi = "java:global/MPRefuelingAdapterEAR/MPRefuelingAdapterEJB/RefuelingNotificationServiceOAuth2!com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote";
        final String jndi = "java:global/MPRefuelingAdapterEAR/MPRefuelingAdapterEJB/RefuelingNotificationServiceOAuth2!com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2";
        RefuelingNotificationServiceOAuth2 remote;
        Context context;
            try {
                
                final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();

                jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

                try {
                    context = new InitialContext(jndiProperties);
                }
                catch (NamingException e) {

                    throw new InterfaceRefuelingNotFoundException("Naming exception: " + e.getMessage());
                }
                
                remote = (RefuelingNotificationServiceOAuth2) context.lookup(jndi);
            }
            catch (Exception ex) {

                return null;
            }

        return remote;
    }
    
}
