package com.techedge.mp.refueling.integration.authorization;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Date;

import sun.misc.BASE64Encoder;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.techedge.mp.core.business.utilities.EncryptionRSA;

public class TokenGenerator {

    private final EncryptionRSA encryptionRSA = new EncryptionRSA();


    public void loadCertificates(byte[] privateKey, byte[] publicKey) throws TokenGenerator.Exception {
        
        try {
            String pemPrivateKey = new String(privateKey, "UTF-8");
            String pemPublicKey = new String(publicKey, "UTF-8");
            encryptionRSA.loadPrivateKey(pemPrivateKey);
            encryptionRSA.loadPublicKey(pemPublicKey);
        }
        catch (IOException ex) {
            String message = "Error reading file certificate: " + ex.getMessage();
            throw new TokenGenerator.Exception(message, "read_pem");
        }
        catch (InvalidKeyException | InvalidKeySpecException | NoSuchAlgorithmException ex) {
            String message = "Error load keys: " + ex.getMessage();
            throw new TokenGenerator.Exception(message, "load_keys");
        }
        catch (java.lang.Exception ex) {
            String message = "Error load keys: " + ex.getMessage();
            throw new TokenGenerator.Exception(message, "load_keys");
        }
    }

    public String getJWTToken(String issuer, String audience, int expireIn) throws TokenGenerator.Exception {

        RSAPublicKey publicKey = encryptionRSA.getPublicKey();
        RSAPrivateKey privateKey = encryptionRSA.getPrivateKey();

        Algorithm algorithm = Algorithm.RSA256(publicKey, privateKey);
        String token = null;
        
        try {
            Date now = new Date();
            Date expire = new Date(now.getTime() + expireIn);
            System.out.println("token expire: " + expire.toString());
            System.out.println("token created: " + now.toString());
            
            token = JWT.create()
                    .withIssuer(issuer)
                    .withAudience(audience)
                    .withExpiresAt(expire)
                    .withIssuedAt(now)
                    .sign(algorithm);
        }
        catch (JWTCreationException ex) {
            String message = "Error creating jwt token: " + ex.getMessage();
            throw new TokenGenerator.Exception(message, "create_token");
        }
        
        return token;
    }
    
    public DecodedJWT verifyJWTToken(String jwtToken, String issuer, String audience) throws TokenGenerator.Exception {
        RSAPublicKey publicKey = encryptionRSA.getPublicKey();
        RSAPrivateKey privateKey = encryptionRSA.getPrivateKey();

        try {
            Algorithm algorithm = Algorithm.RSA256(publicKey, privateKey);
            JWTVerifier verifier = JWT.require(algorithm)
                    .withIssuer(issuer)
                    .withAudience(audience)
                .build(); //Reusable verifier instance
            DecodedJWT jwt = verifier.verify(jwtToken);
            return jwt;
        } catch (JWTVerificationException ex) {
            String message = "Error verifing jwt token: " + ex.getMessage();
            throw new TokenGenerator.Exception(message, "verify_token");
        }
    }

    public String getJWTToken(String teamID, String appKeyID, byte[] keyBytes) throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {
        String jwtToken = JWTA.getToken(teamID, appKeyID, keyBytes);
        return jwtToken;
    }

    private static class JWTA {

        private static BASE64Encoder encoder = new BASE64Encoder();
        //private static BASE64Decoder decoder = new BASE64Decoder();
        /**
         * Generates a JWT token as per Apple's specifications.
         *
         * @param teamID The team ID (found in the member center)
         * @param keyID  The key ID (found when generating your private key)
         * @param secret The private key (excluding the header and the footer)
         *
         * @return The resulting token, which will be valid for one hour
         *
         * @throws InvalidKeySpecException  if the key is incorrect
         * @throws NoSuchAlgorithmException if the key algo failed to load
         * @throws InvalidKeyException      if the key is invalid
         * @throws SignatureException       if this signature object is not initialized properly.
         */
        public static String getToken(final String teamID, final String keyID, final byte[] secret)
                throws InvalidKeySpecException, NoSuchAlgorithmException, InvalidKeyException, SignatureException {

            final int now = (int) (System.currentTimeMillis() / 1000);
            final String header = "{\"alg\":\"ES256\",\"kid\":\"" + keyID + "\"}";
            final String payload = "{\"iss\":\"" + teamID + "\",\"iat\":" + now + "}";

            final String part1 = encoder.encode(header.getBytes(StandardCharsets.UTF_8))
                    + "."
                    + encoder.encode(payload.getBytes(StandardCharsets.UTF_8));

            return part1 + "." + ES256(secret, part1);
        }

        /**
         * @param secret The secret
         * @param data   The data to be encoded
         *
         * @return The encoded token
         *
         * @throws InvalidKeySpecException  if the key is incorrect
         * @throws NoSuchAlgorithmException if the key algo failed to load
         * @throws InvalidKeyException      if the key is invalid
         * @throws SignatureException       if this signature object is not initialized properly.
         */
        private static String ES256(final byte[] secret, final String data)
                throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, SignatureException {

            KeyFactory kf = KeyFactory.getInstance("EC");
            KeySpec keySpec = new PKCS8EncodedKeySpec(secret);
            PrivateKey key = kf.generatePrivate(keySpec);

            final Signature sha256withECDSA = Signature.getInstance("SHA256withECDSA");
            sha256withECDSA.initSign(key);

            sha256withECDSA.update(data.getBytes(StandardCharsets.UTF_8));

            final byte[] signed = sha256withECDSA.sign();
            return encoder.encode(signed);
        }
    }
    
    public static class Exception extends java.lang.Exception {

        /**
         * 
         */
        private static final long serialVersionUID = -7136629033765762335L;
        private final String step;
        
        public Exception(String message, String step) {
            super(message);
            this.step = step;
        }

        public String getStep() {
            return step;
        }
    }
}
