package com.techedge.mp.refueling.integration.authorization;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.soap.Detail;
import javax.xml.soap.DetailEntry;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import com.auth0.jwt.interfaces.DecodedJWT;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.refueling.integration.exception.InterfaceRefuelingNotFoundException;
import com.techedge.mp.refueling.integration.service.EJBHomeCache;

public class OAuth2Handler implements SOAPHandler<SOAPMessageContext> {

    private final static String     PARAM_SECURITY_RSA_PRIVATE_PEM_PATH = "REFUELING_SECURITY_RSA_PRIVATE_PEM_PATH";
    private final static String     PARAM_SECURITY_RSA_PUBLIC_PEM_PATH  = "REFUELING_SECURITY_RSA_PUBLIC_PEM_PATH";

    private final static String     PARAM_OAUTH2_JWT_ISSUER             = "REFUELING_OAUTH2_JWT_ISSUER";
    private final static String     PARAM_OAUTH2_JWT_AUDICENCE          = "REFUELING_OAUTH2_JWT_AUDICENCE";

    private ParametersServiceRemote parametersService                   = null;

    @Override
    public boolean handleFault(SOAPMessageContext messageContext) {
        //System.out.println("HandlerChain [OAuthValidator]: handleFault()......");
        return true;
    }

    @Override
    public Set<QName> getHeaders() {
        //System.out.println("HandlerChain [OAuthValidator]: getHeaders()......");
        return null;
    }

    @Override
    public void close(MessageContext messageContext) {
        //System.out.println("HandlerChain [OAuthValidator]: close()......");

        String reponseContent = null;
        try {
            SOAPMessageContext soapMessageContext = (SOAPMessageContext) messageContext;
            SOAPMessage soapMessage = soapMessageContext.getMessage();
            soapMessage.getSOAPHeader().detachNode();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            soapMessage.writeTo(baos);
            reponseContent = baos.toString();
            /*
             * NodeList soapBody = soapMessage.getSOAPHeader().rgetSOAPBody().getChildNodes();
             * DOMSource source = new DOMSource();
             * StringWriter writer = new StringWriter();
             * StreamResult result = new StreamResult(writer);
             * Transformer transformer = TransformerFactory.newInstance().newTransformer();
             * transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
             * 
             * for (int i = 0; i < soapBody.getLength(); ++i) {
             * source.setNode(soapBody.item(i));
             * transformer.transform(source, result);
             * }
             * 
             * requestContent = writer.toString();
             */

            System.out.println("=========== SOAP RESPONSE ===========");
            System.out.println(reponseContent.replaceAll("/>", "/>\n"));
            System.out.println("=========== END ===========");
        }
        catch (Exception ex) {
            System.err.println("ERRORE nella chiamata close(): " + ex.getMessage());
        }
    }

    @Override
    public boolean handleMessage(SOAPMessageContext messageContext) {

        //System.out.println("HandlerChain [OAuthValidator]: handleMessage()......");

        Boolean isRequest = (Boolean) messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        //for response message only, true for outbound messages, false for inbound
        if (!isRequest) {
            try {
                TokenGenerator tokenGenerator = buildToken();
                verifyOAuth(messageContext, tokenGenerator);
            }
            catch (ParameterNotFoundException ex) {
                System.err.println("Parameter not found: " + ex.getMessage());
                messageContext.setMessage(generateSOAPFaultMessage("Service is not available", ex.getMessage()));
                sendErrorResponse(messageContext, HttpURLConnection.HTTP_INTERNAL_ERROR, "Service is not available");
                return false;
            }
            catch (InterfaceRefuelingNotFoundException ex) {
                System.err.println("Service is not available: " + ex.getMessage());
                messageContext.setMessage(generateSOAPFaultMessage("Service is not available", ex.getMessage()));
                sendErrorResponse(messageContext, HttpURLConnection.HTTP_INTERNAL_ERROR, "Service is not available");        
                return false;
            }
            catch (TokenGenerator.Exception ex) {
                System.err.println("Token Generator error: " + ex.getMessage());
                messageContext.setMessage(generateSOAPFaultMessage("Invalid Token", ex.getMessage()));
                sendErrorResponse(messageContext, HttpURLConnection.HTTP_UNAUTHORIZED, "Invalid Authorization");
                return false;
            }
            catch (IOException ex) {
                System.err.println("IO error: " + ex.getMessage());
                messageContext.setMessage(generateSOAPFaultMessage("Service is not available", ex.getMessage()));
                sendErrorResponse(messageContext, HttpURLConnection.HTTP_INTERNAL_ERROR, "Service is not available");
                return false;
            }
        }
        //continue other handler chain
        return true;
    }
    
    private void sendErrorResponse(SOAPMessageContext messageContext, int httpStatusCode, String httpStatusMessage) {
        HttpServletResponse response = (HttpServletResponse) messageContext.get(MessageContext.SERVLET_RESPONSE);
        try {
            response.sendError(httpStatusCode, httpStatusMessage);
        }
        catch (IOException e) {
            System.err.println("Error while sending http response: " + e.getMessage());
        }        
    }

    private TokenGenerator buildToken() throws InterfaceRefuelingNotFoundException, ParameterNotFoundException, IOException, TokenGenerator.Exception {

        if (parametersService == null) {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }

        File filePrivatePEM = new File(parametersService.getParamValue(PARAM_SECURITY_RSA_PRIVATE_PEM_PATH));
        FileInputStream input = new FileInputStream(filePrivatePEM);
        byte[] privateKey = new byte[input.available()];
        input.read(privateKey);
        input.close();

        File filepublicPEM = new File(parametersService.getParamValue(PARAM_SECURITY_RSA_PUBLIC_PEM_PATH));
        input = new FileInputStream(filepublicPEM);
        byte[] publicKey = new byte[input.available()];
        input.read(publicKey);
        input.close();

        TokenGenerator tokenGenerator = new TokenGenerator();
        tokenGenerator.loadCertificates(privateKey, publicKey);

        return tokenGenerator;
    }

    @SuppressWarnings({ "unchecked", "unused" })
    private void verifyOAuth(SOAPMessageContext messageContext, TokenGenerator tokenGenerator) throws ParameterNotFoundException, 
            InterfaceRefuelingNotFoundException, TokenGenerator.Exception {

        if (parametersService == null) {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }

        String issuer = parametersService.getParamValue(PARAM_OAUTH2_JWT_ISSUER);
        String audience = parametersService.getParamValue(PARAM_OAUTH2_JWT_AUDICENCE);

        Map<String, Object> httpHeaders = (Map<String, Object>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
        ArrayList<String> authorizationList = (ArrayList<String>) httpHeaders.get("Authorization");

        if (authorizationList == null || authorizationList.isEmpty()) {
            System.err.println("Http Header Authorization is null or is empty");
            throw new TokenGenerator.Exception("Http Header Authorization is null or is empty", null);
        }

        String bearerAuthorization = authorizationList.get(0).replaceAll("BEARER|Bearer", "").trim();

        try {
            DecodedJWT token = tokenGenerator.verifyJWTToken(bearerAuthorization, issuer, audience);
        }
        catch (TokenGenerator.Exception ex) {
            System.err.println("Failed to verify token: " + ex.getMessage() + "  " + ex.getStep());
            throw ex;
        }
    }

    private SOAPMessage generateSOAPFaultMessage(String error, String errorDescription) {
        SOAPMessage soapMessage = null;
        try {
            soapMessage = MessageFactory.newInstance().createMessage();
            soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ns1", "http://service.adapter.refueling.mp.techedge.com");
            SOAPFault soapFault = soapMessage.getSOAPBody().addFault();
            QName faultName = new QName(SOAPConstants.URI_NS_SOAP_ENVELOPE, "Server");
            soapFault.setFaultCode(faultName);
            soapFault.setFaultString(error);
            Detail detail = soapFault.addDetail();
            QName entryName = new QName("http://service.adapter.refueling.mp.techedge.com", "OAuthAuthorizationException", "ns1");
            DetailEntry entry = detail.addDetailEntry(entryName);
            entry.addTextNode((errorDescription != null) ? errorDescription : error);
        }
        catch (SOAPException ex) {
            System.err.println("Errore nella creazione del messaggio soap: " + ex.getMessage());
        }

        return soapMessage;
    }

}
