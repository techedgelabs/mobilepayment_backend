package com.techedge.mp.refueling.integration.authorization;

public class OAuthAuthorizationException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 3685231397172066332L;
    
    public OAuthAuthorizationException(String message) {
        super(message);
    }

}
