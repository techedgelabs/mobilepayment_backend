
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per DpanDetailEntityType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="DpanDetailEntityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mcCardDpan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mcCardPan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it}EnumStatusMcCardType"/>
 *         &lt;element name="mcCardKmRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="mcCardInfoRequired" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="mcCardServiceCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DpanDetailEntityType", propOrder = {
    "mcCardDpan",
    "mcCardPan",
    "status",
    "mcCardKmRequired",
    "mcCardInfoRequired",
    "mcCardServiceCode"
})
public class DpanDetailEntityType {

    @XmlElement(required = true)
    protected String mcCardDpan;
    @XmlElement(required = true)
    protected String mcCardPan;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected EnumStatusMcCardType status;
    protected boolean mcCardKmRequired;
    protected boolean mcCardInfoRequired;
    @XmlElement(required = true)
    protected String mcCardServiceCode;

    /**
     * Recupera il valore della proprietÓ mcCardDpan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcCardDpan() {
        return mcCardDpan;
    }

    /**
     * Imposta il valore della proprietÓ mcCardDpan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcCardDpan(String value) {
        this.mcCardDpan = value;
    }

    /**
     * Recupera il valore della proprietÓ mcCardPan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcCardPan() {
        return mcCardPan;
    }

    /**
     * Imposta il valore della proprietÓ mcCardPan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcCardPan(String value) {
        this.mcCardPan = value;
    }

    /**
     * Recupera il valore della proprietÓ status.
     * 
     * @return
     *     possible object is
     *     {@link EnumStatusMcCardType }
     *     
     */
    public EnumStatusMcCardType getStatus() {
        return status;
    }

    /**
     * Imposta il valore della proprietÓ status.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumStatusMcCardType }
     *     
     */
    public void setStatus(EnumStatusMcCardType value) {
        this.status = value;
    }

    /**
     * Recupera il valore della proprietÓ mcCardKmRequired.
     * 
     */
    public boolean isMcCardKmRequired() {
        return mcCardKmRequired;
    }

    /**
     * Imposta il valore della proprietÓ mcCardKmRequired.
     * 
     */
    public void setMcCardKmRequired(boolean value) {
        this.mcCardKmRequired = value;
    }

    /**
     * Recupera il valore della proprietÓ mcCardInfoRequired.
     * 
     */
    public boolean isMcCardInfoRequired() {
        return mcCardInfoRequired;
    }

    /**
     * Imposta il valore della proprietÓ mcCardInfoRequired.
     * 
     */
    public void setMcCardInfoRequired(boolean value) {
        this.mcCardInfoRequired = value;
    }

    /**
     * Recupera il valore della proprietÓ mcCardServiceCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcCardServiceCode() {
        return mcCardServiceCode;
    }

    /**
     * Imposta il valore della proprietÓ mcCardServiceCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcCardServiceCode(String value) {
        this.mcCardServiceCode = value;
    }

}
