
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per McCardEnjoyAuthorizeRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="McCardEnjoyAuthorizeRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}mcPaymentApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mcCardDpan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serverSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paymentMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "McCardEnjoyAuthorizeRequest", propOrder = {
    "userId",
    "mcCardDpan",
    "serverSerialNumber",
    "amount",
    "currencyCode",
    "paymentMode"
})
@XmlRootElement(name = "mcCardEnjoyAuthorizeRequest")
public class McCardEnjoyAuthorizeRequest
    extends McPaymentApiRequestContext
{

    @XmlElement(required = true)
    protected String userId;
    @XmlElement(required = true)
    protected String mcCardDpan;
    @XmlElement(required = true)
    protected String serverSerialNumber;
    protected int amount;
    @XmlElement(required = true)
    protected String currencyCode;
    protected String paymentMode;

    /**
     * Recupera il valore della proprietÓ userId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Imposta il valore della proprietÓ userId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Recupera il valore della proprietÓ mcCardDpan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcCardDpan() {
        return mcCardDpan;
    }

    /**
     * Imposta il valore della proprietÓ mcCardDpan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcCardDpan(String value) {
        this.mcCardDpan = value;
    }

    /**
     * Recupera il valore della proprietÓ serverSerialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServerSerialNumber() {
        return serverSerialNumber;
    }

    /**
     * Imposta il valore della proprietÓ serverSerialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServerSerialNumber(String value) {
        this.serverSerialNumber = value;
    }

    /**
     * Recupera il valore della proprietÓ amount.
     * 
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Imposta il valore della proprietÓ amount.
     * 
     */
    public void setAmount(int value) {
        this.amount = value;
    }

    /**
     * Recupera il valore della proprietÓ currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Imposta il valore della proprietÓ currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Recupera il valore della proprietÓ paymentMode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * Imposta il valore della proprietÓ paymentMode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMode(String value) {
        this.paymentMode = value;
    }

}
