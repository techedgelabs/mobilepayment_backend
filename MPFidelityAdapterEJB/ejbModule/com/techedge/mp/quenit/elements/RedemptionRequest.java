
package com.techedge.mp.quenit.elements;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiRequestContext"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fiscalCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="redemptionCode" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fiscalCode",
    "redemptionCode"
})
@XmlRootElement(name = "redemptionRequest")
public class RedemptionRequest
    extends LoyaltyApiRequestContext
{

    @XmlElement(required = true)
    protected String fiscalCode;
    @XmlElement(required = true)
    protected BigInteger redemptionCode;

    /**
     * Recupera il valore della proprietÓ fiscalCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiscalCode() {
        return fiscalCode;
    }

    /**
     * Imposta il valore della proprietÓ fiscalCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiscalCode(String value) {
        this.fiscalCode = value;
    }

    /**
     * Recupera il valore della proprietÓ redemptionCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRedemptionCode() {
        return redemptionCode;
    }

    /**
     * Imposta il valore della proprietÓ redemptionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRedemptionCode(BigInteger value) {
        this.redemptionCode = value;
    }

}
