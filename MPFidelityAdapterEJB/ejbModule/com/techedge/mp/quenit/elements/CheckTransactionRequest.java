
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for checkTransactionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="checkTransactionRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="operationIDtoCheck" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "checkTransactionRequest", propOrder = {
    "operationIDtoCheck"
})
@XmlSeeAlso({
    CheckLoadLoyaltyCreditsTransactionRequest.class,
    CheckConsumeVoucherTransactionRequest.class
})
public class CheckTransactionRequest
    extends LoyaltyApiRequestContext
{

    @XmlElement(required = true)
    protected String operationIDtoCheck;

    /**
     * Gets the value of the operationIDtoCheck property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationIDtoCheck() {
        return operationIDtoCheck;
    }

    /**
     * Sets the value of the operationIDtoCheck property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationIDtoCheck(String value) {
        this.operationIDtoCheck = value;
    }

}
