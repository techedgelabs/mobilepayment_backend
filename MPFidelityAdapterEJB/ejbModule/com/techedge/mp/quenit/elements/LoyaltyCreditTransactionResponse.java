
package com.techedge.mp.quenit.elements;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.techedge.mp.quenit.types.Voucher;


/**
 * <p>Java class for loyaltyCreditTransactionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loyaltyCreditTransactionResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiResponseContext">
 *       &lt;sequence>
 *         &lt;element name="credits" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="balance" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="balanceAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="cardCodeIssuer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="eanCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cardStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cardType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cardClassification" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="marketingMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="warningMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;sequence>
 *           &lt;element name="voucherList" type="{http://servizi.infogroup.it}voucher" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loyaltyCreditTransactionResponse", propOrder = {
    "credits",
    "balance",
    "balanceAmount",
    "cardCodeIssuer",
    "eanCode",
    "cardStatus",
    "cardType",
    "cardClassification",
    "marketingMsg",
    "warningMsg",
    "voucherList"
})
@XmlSeeAlso({
    CheckLoadLoyaltyCreditsTransactionResponse.class,
    LoadLoyaltyCreditsResponse.class
})
public class LoyaltyCreditTransactionResponse
    extends LoyaltyApiResponseContext
{

    @XmlElement(required = true)
    protected BigInteger credits;
    @XmlElement(required = true)
    protected BigInteger balance;
    @XmlElement(required = true)
    protected BigDecimal balanceAmount;
    @XmlElement(required = true)
    protected String cardCodeIssuer;
    @XmlElement(required = true)
    protected String eanCode;
    @XmlElement(required = true)
    protected String cardStatus;
    @XmlElement(required = true, nillable = true)
    protected String cardType;
    @XmlElement(required = true)
    protected String cardClassification;
    @XmlElement(required = true, nillable = true)
    protected String marketingMsg;
    @XmlElement(required = false, nillable = true)
    protected String warningMsg;
    protected List<Voucher> voucherList;

    /**
     * Gets the value of the credits property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCredits() {
        return credits;
    }

    /**
     * Sets the value of the credits property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCredits(BigInteger value) {
        this.credits = value;
    }

    /**
     * Gets the value of the balance property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBalance() {
        return balance;
    }

    /**
     * Sets the value of the balance property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBalance(BigInteger value) {
        this.balance = value;
    }

    /**
     * Gets the value of the balanceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBalanceAmount() {
        return balanceAmount;
    }

    /**
     * Sets the value of the balanceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBalanceAmount(BigDecimal value) {
        this.balanceAmount = value;
    }

    /**
     * Gets the value of the cardCodeIssuer property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardCodeIssuer() {
        return cardCodeIssuer;
    }

    /**
     * Sets the value of the cardCodeIssuer property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardCodeIssuer(String value) {
        this.cardCodeIssuer = value;
    }

    /**
     * Gets the value of the eanCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEanCode() {
        return eanCode;
    }

    /**
     * Sets the value of the eanCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEanCode(String value) {
        this.eanCode = value;
    }

    /**
     * Gets the value of the cardStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardStatus() {
        return cardStatus;
    }

    /**
     * Sets the value of the cardStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardStatus(String value) {
        this.cardStatus = value;
    }

    /**
     * Gets the value of the cardType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * Sets the value of the cardType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardType(String value) {
        this.cardType = value;
    }

    /**
     * Gets the value of the cardClassification property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardClassification() {
        return cardClassification;
    }

    /**
     * Sets the value of the cardClassification property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardClassification(String value) {
        this.cardClassification = value;
    }

    /**
     * Gets the value of the marketingMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketingMsg() {
        return marketingMsg;
    }

    /**
     * Sets the value of the marketingMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketingMsg(String value) {
        this.marketingMsg = value;
    }
    
    /**
     * Gets the value of the warningMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarningMsg() {
        return warningMsg;
    }

    /**
     * Sets the value of the warningMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarningMsg(String value) {
        this.warningMsg = value;
    }
    
    /**
     * Gets the value of the voucherList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucherList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucherList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Voucher }
     * 
     * 
     */
    public List<Voucher> getVoucherList() {
        if (voucherList == null) {
            voucherList = new ArrayList<Voucher>();
        }
        return this.voucherList;
    }

}
