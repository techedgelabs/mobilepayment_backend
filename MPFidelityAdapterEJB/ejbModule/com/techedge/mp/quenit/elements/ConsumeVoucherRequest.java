package com.techedge.mp.quenit.elements;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.techedge.mp.quenit.types.Product;
import com.techedge.mp.quenit.types.VoucherCode;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="voucherType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refuelMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mpTransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;sequence>
 *           &lt;element name="productsList" type="{http://servizi.infogroup.it}product" maxOccurs="2" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="voucherList" type="{http://servizi.infogroup.it}voucherCode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;element name="consumeType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="preAuthOperationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "voucherType", "stationID", "refuelMode", "paymentMode", "language", 
        "mpTransactionID", "productsList", "voucherList", "consumeType", "preAuthOperationID" })
@XmlRootElement(name = "consumeVoucherRequest")
public class ConsumeVoucherRequest extends LoyaltyApiRequestContext {

    @XmlElement(required = true)
    protected String            voucherType;
    @XmlElement(required = true)
    protected String            stationID;
    @XmlElement(required = true)
    protected String            refuelMode;
    @XmlElement(required = true)
    protected String            paymentMode;
    protected String            language;
    @XmlElement(required = true)
    protected String            mpTransactionID;
    @XmlElement(required = true, nillable = true)
    protected List<Product>     productsList;
    protected List<VoucherCode> voucherList;
    @XmlElement(required = true, nillable = true)
    protected String            consumeType;
    @XmlElement(required = true, nillable = true)
    protected String            preAuthOperationID;

    /**
     * Gets the value of the voucherType property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getVoucherType() {
        return voucherType;
    }

    /**
     * Sets the value of the voucherType property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setVoucherType(String value) {
        this.voucherType = value;
    }

    /**
     * Gets the value of the stationID property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getStationID() {
        return stationID;
    }

    /**
     * Sets the value of the stationID property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setStationID(String value) {
        this.stationID = value;
    }

    /**
     * Gets the value of the refuelMode property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getRefuelMode() {
        return refuelMode;
    }

    /**
     * Sets the value of the refuelMode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setRefuelMode(String value) {
        this.refuelMode = value;
    }

    /**
     * Gets the value of the paymentMode property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setLanguage(String value) {
        this.language = value;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the paymentMode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setPaymentMode(String value) {
        this.paymentMode = value;
    }

    /**
     * Gets the value of the mpTransactionID property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getMpTransactionID() {
        return mpTransactionID;
    }

    /**
     * Sets the value of the mpTransactionID property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setMpTransactionID(String value) {
        this.mpTransactionID = value;
    }

    /**
     * Gets the value of the productsList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the productsList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getProductsList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link Product }
     * 
     * 
     */
    public List<Product> getProductsList() {
        if (productsList == null) {
            productsList = new ArrayList<Product>();
        }
        return this.productsList;
    }

    /**
     * Gets the value of the voucherList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucherList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getVoucherList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link VoucherCode }
     * 
     * 
     */
    public List<VoucherCode> getVoucherList() {
        if (voucherList == null) {
            voucherList = new ArrayList<VoucherCode>();
        }
        return this.voucherList;
    }

    /**
     * Gets the value of the consumeType property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getConsumeType() {
        return consumeType;
    }

    /**
     * Sets the value of the consumeType property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setConsumeType(String consumeType) {
        this.consumeType = consumeType;
    }

    /**
     * Gets the value of the preAuthOperationID property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getPreAuthOperationID() {
        return preAuthOperationID;
    }

    /**
     * Sets the value of the preAuthOperationID property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setPreAuthOperationID(String preAuthOperationID) {
        this.preAuthOperationID = preAuthOperationID;
    }

}
