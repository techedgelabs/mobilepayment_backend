
package com.techedge.mp.quenit.elements;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.techedge.mp.quenit.types.Voucher;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiResponseContext">
 *       &lt;sequence>
 *         &lt;sequence>
 *           &lt;element name="voucherList" type="{http://servizi.infogroup.it}voucher" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "voucherList"
})
@XmlRootElement(name = "checkVoucherResponse")
public class CheckVoucherResponse
    extends LoyaltyApiResponseContext
{

    protected List<Voucher> voucherList;

    /**
     * Gets the value of the voucherList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucherList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucherList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Voucher }
     * 
     * 
     */
    public List<Voucher> getVoucherList() {
        if (voucherList == null) {
            voucherList = new ArrayList<Voucher>();
        }
        return this.voucherList;
    }

}
