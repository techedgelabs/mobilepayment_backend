
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per McCardEnjoyAuthorizeResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="McCardEnjoyAuthorizeResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}ResultEntityType"/>
 *         &lt;element name="authCryptogram" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "McCardEnjoyAuthorizeResponse", propOrder = {
    "result",
    "authCryptogram"
})
@XmlRootElement(name = "mcCardEnjoyAuthorizeResponse", namespace = "http://mcPaymentAPI.services.multicardPaymentGatewayServer.it")
public class McCardEnjoyAuthorizeResponse {

    @XmlElement(required = true)
    protected ResultEntityType result;
    protected String authCryptogram;

    /**
     * Recupera il valore della proprietÓ result.
     * 
     * @return
     *     possible object is
     *     {@link ResultEntityType }
     *     
     */
    public ResultEntityType getResult() {
        return result;
    }

    /**
     * Imposta il valore della proprietÓ result.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultEntityType }
     *     
     */
    public void setResult(ResultEntityType value) {
        this.result = value;
    }

    /**
     * Recupera il valore della proprietÓ authCryptogram.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthCryptogram() {
        return authCryptogram;
    }

    /**
     * Imposta il valore della proprietÓ authCryptogram.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthCryptogram(String value) {
        this.authCryptogram = value;
    }

}
