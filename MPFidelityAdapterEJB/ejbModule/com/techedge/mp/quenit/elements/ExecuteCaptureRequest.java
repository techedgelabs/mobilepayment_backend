
package com.techedge.mp.quenit.elements;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ExecuteCaptureRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ExecuteCaptureRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}mcPaymentApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="mcCardDpan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="retrievalRefNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="authCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refuelMode" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}EnumRefuelModeType"/>
 *         &lt;element name="refuelInfo" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}RefuelInfoEntityType" maxOccurs="3" minOccurs="0"/>
 *         &lt;element name="messageReasonCode" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}string4" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExecuteCaptureRequest", propOrder = {
    "mcCardDpan",
    "retrievalRefNumber",
    "authCode",
    "amount",
    "currencyCode",
    "refuelMode",
    "refuelInfo",
    "messageReasonCode"
})
@XmlRootElement(name = "executeCaptureRequest")
public class ExecuteCaptureRequest
    extends McPaymentApiRequestContext
{

    @XmlElement(required = true)
    protected String mcCardDpan;
    @XmlElement(required = true)
    protected String retrievalRefNumber;
    @XmlElement(required = true)
    protected String authCode;
    protected int amount;
    @XmlElement(required = true)
    protected String currencyCode;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected EnumRefuelModeType refuelMode;
    protected List<RefuelInfoEntityType> refuelInfo;
    protected String messageReasonCode;

    /**
     * Recupera il valore della proprietÓ mcCardDpan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcCardDpan() {
        return mcCardDpan;
    }

    /**
     * Imposta il valore della proprietÓ mcCardDpan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcCardDpan(String value) {
        this.mcCardDpan = value;
    }

    /**
     * Recupera il valore della proprietÓ retrievalRefNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetrievalRefNumber() {
        return retrievalRefNumber;
    }

    /**
     * Imposta il valore della proprietÓ retrievalRefNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetrievalRefNumber(String value) {
        this.retrievalRefNumber = value;
    }

    /**
     * Recupera il valore della proprietÓ authCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * Imposta il valore della proprietÓ authCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthCode(String value) {
        this.authCode = value;
    }

    /**
     * Recupera il valore della proprietÓ amount.
     * 
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Imposta il valore della proprietÓ amount.
     * 
     */
    public void setAmount(int value) {
        this.amount = value;
    }

    /**
     * Recupera il valore della proprietÓ currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Imposta il valore della proprietÓ currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Recupera il valore della proprietÓ refuelMode.
     * 
     * @return
     *     possible object is
     *     {@link EnumRefuelModeType }
     *     
     */
    public EnumRefuelModeType getRefuelMode() {
        return refuelMode;
    }

    /**
     * Imposta il valore della proprietÓ refuelMode.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumRefuelModeType }
     *     
     */
    public void setRefuelMode(EnumRefuelModeType value) {
        this.refuelMode = value;
    }

    /**
     * Gets the value of the refuelInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refuelInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefuelInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefuelInfoEntityType }
     * 
     * 
     */
    public List<RefuelInfoEntityType> getRefuelInfo() {
        if (refuelInfo == null) {
            refuelInfo = new ArrayList<RefuelInfoEntityType>();
        }
        return this.refuelInfo;
    }

    /**
     * Recupera il valore della proprietÓ messageReasonCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageReasonCode() {
        return messageReasonCode;
    }

    /**
     * Imposta il valore della proprietÓ messageReasonCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageReasonCode(String value) {
        this.messageReasonCode = value;
    }

}
