
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ExecuteReversalRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ExecuteReversalRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}mcPaymentApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="mcCardDpan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="retrievalRefNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="authCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="shopCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="messageReasonCode" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}string4"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExecuteReversalRequest", propOrder = {
    "mcCardDpan",
    "retrievalRefNumber",
    "authCode",
    "shopCode",
    "amount",
    "currencyCode",
    "messageReasonCode"
})
@XmlRootElement(name = "executeReversalRequest")
public class ExecuteReversalRequest
    extends McPaymentApiRequestContext
{

    @XmlElement(required = true)
    protected String mcCardDpan;
    @XmlElement(required = true)
    protected String retrievalRefNumber;
    @XmlElement(required = true)
    protected String authCode;
    @XmlElement(required = true)
    protected String shopCode;
    protected int amount;
    @XmlElement(required = true)
    protected String currencyCode;
    @XmlElement(required = true)
    protected String messageReasonCode;

    /**
     * Recupera il valore della proprietÓ mcCardDpan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcCardDpan() {
        return mcCardDpan;
    }

    /**
     * Imposta il valore della proprietÓ mcCardDpan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcCardDpan(String value) {
        this.mcCardDpan = value;
    }

    /**
     * Recupera il valore della proprietÓ retrievalRefNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetrievalRefNumber() {
        return retrievalRefNumber;
    }

    /**
     * Imposta il valore della proprietÓ retrievalRefNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetrievalRefNumber(String value) {
        this.retrievalRefNumber = value;
    }

    /**
     * Recupera il valore della proprietÓ authCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * Imposta il valore della proprietÓ authCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthCode(String value) {
        this.authCode = value;
    }

    /**
     * Recupera il valore della proprietÓ shopCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopCode() {
        return shopCode;
    }

    /**
     * Imposta il valore della proprietÓ shopCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopCode(String value) {
        this.shopCode = value;
    }

    /**
     * Recupera il valore della proprietÓ amount.
     * 
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Imposta il valore della proprietÓ amount.
     * 
     */
    public void setAmount(int value) {
        this.amount = value;
    }

    /**
     * Recupera il valore della proprietÓ currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Imposta il valore della proprietÓ currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Recupera il valore della proprietÓ messageReasonCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageReasonCode() {
        return messageReasonCode;
    }

    /**
     * Imposta il valore della proprietÓ messageReasonCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageReasonCode(String value) {
        this.messageReasonCode = value;
    }

}
