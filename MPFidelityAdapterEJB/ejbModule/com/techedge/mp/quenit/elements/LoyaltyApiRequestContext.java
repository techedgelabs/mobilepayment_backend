
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loyaltyApiRequestContext complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loyaltyApiRequestContext">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="partnerType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="requestTimestamp" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loyaltyApiRequestContext", propOrder = {
    "operationID",
    "partnerType",
    "requestTimestamp"
})
@XmlSeeAlso({
    GetLoyaltyCardsListRequest.class,
    CheckTransactionRequest.class,
    CheckVoucherRequest.class,
    ReverseTransactionRequest.class,
    ConsumeVoucherRequest.class,
    EnableLoyaltyCardRequest.class,
    LoadLoyaltyCreditsRequest.class,
    InfoRedemptionRequest.class
})
public class LoyaltyApiRequestContext {

    @XmlElement(required = true)
    protected String operationID;
    @XmlElement(required = true)
    protected String partnerType;
    protected long requestTimestamp;

    /**
     * Gets the value of the operationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationID() {
        return operationID;
    }

    /**
     * Sets the value of the operationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationID(String value) {
        this.operationID = value;
    }

    /**
     * Gets the value of the partnerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerType() {
        return partnerType;
    }

    /**
     * Sets the value of the partnerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerType(String value) {
        this.partnerType = value;
    }

    /**
     * Gets the value of the requestTimestamp property.
     * 
     */
    public long getRequestTimestamp() {
        return requestTimestamp;
    }

    /**
     * Sets the value of the requestTimestamp property.
     * 
     */
    public void setRequestTimestamp(long value) {
        this.requestTimestamp = value;
    }

}
