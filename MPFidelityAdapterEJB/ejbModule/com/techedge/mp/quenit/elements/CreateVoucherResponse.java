package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.techedge.mp.quenit.types.Voucher;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiResponseContext">
 *       &lt;sequence>
 *         &lt;sequence>
 *           &lt;element name="voucher" type="{http://servizi.infogroup.it}voucher" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "voucher" })
@XmlRootElement(name = "createVoucherResponse")
public class CreateVoucherResponse extends LoyaltyApiResponseContext {

    protected Voucher voucher;

    /**
     * Gets the value of the voucher property.
     * 
     * @return
     *         possible object is {@link Voucher }
     * 
     */
    public Voucher getVoucher() {
        return this.voucher;
    }

    /**
     * Sets the value of the voucher property.
     * 
     * @param value
     *            allowed object is {@link Voucher }
     * 
     */
    public void setVoucher(Voucher value) {
        this.voucher = value;
    }

}
