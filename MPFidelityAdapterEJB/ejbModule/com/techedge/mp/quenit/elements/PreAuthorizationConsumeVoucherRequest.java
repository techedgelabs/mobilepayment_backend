package com.techedge.mp.quenit.elements;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.techedge.mp.quenit.types.VoucherCode;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="tns:loyaltyApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="voucherType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;sequence>
 *           &lt;element maxOccurs="unbounded" minOccurs="0" name="voucherList" type="{http://servizi.infogroup.it}voucherCode"/>
 *         &lt;/sequence>
 *         &lt;element name="mpTransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="language" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "voucherType", "stationID", "amount", "voucherList", "mpTransactionID", "language", "productType" })
@XmlRootElement(name = "preAuthorizationConsumeVoucherRequest")
public class PreAuthorizationConsumeVoucherRequest extends LoyaltyApiRequestContext {

    @XmlElement(required = true)
    protected String            voucherType;
    
    @XmlElement(required = true)
    protected String            stationID;
    
    @XmlElement(required = true)
    protected BigDecimal        amount;
    
    @XmlElement(required = true, nillable = true)
    protected String            language;
    
    @XmlElement(required = true)
    protected String            mpTransactionID;
    
    @XmlElement(required = true)
    protected List<VoucherCode> voucherList;
    
    @XmlElement(required = true)
    protected String            productType;

    /**
     * Gets the value of the voucherType property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getVoucherType() {
        return voucherType;
    }

    /**
     * Sets the value of the voucherType property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setVoucherType(String value) {
        this.voucherType = value;
    }

    /**
     * Gets the value of the stationID property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getStationID() {
        return stationID;
    }

    /**
     * Sets the value of the stationID property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setStationID(String value) {
        this.stationID = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *         possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * Gets the value of the language property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getLanguage() {
        return language;
    }

    /**
     * Sets the value of the language property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setLanguage(String language) {
        this.language = language;
    }
    
    /**
     * Gets the value of the mpTransactionID property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getMpTransactionID() {
        return mpTransactionID;
    }

    /**
     * Sets the value of the mpTransactionID property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setMpTransactionID(String value) {
        this.mpTransactionID = value;
    }

    /**
     * Gets the value of the voucherList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list, not a snapshot. Therefore any modification you make to the returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucherList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * 
     * <pre>
     * getVoucherList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list {@link VoucherCode }
     * 
     * 
     */
    public List<VoucherCode> getVoucherList() {
        if (voucherList == null) {
            voucherList = new ArrayList<VoucherCode>();
        }
        return this.voucherList;
    }

    /**
     * Sets the value of the voucherList property.
     * 
     * @param value
     *            allowed object is {@link List<VoucherCode> }
     * 
     */
    public void setVoucherList(List<VoucherCode> voucherList) {
        this.voucherList = voucherList;
    }

    /**
     * Gets the value of the productType property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getProductType() {
        return productType;
    }

    /**
     * Sets the value of the productType property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }

}
