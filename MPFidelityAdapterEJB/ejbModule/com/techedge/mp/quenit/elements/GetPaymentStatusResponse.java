
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per GetPaymentStatusResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="GetPaymentStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}ResultEntityType"/>
 *         &lt;element name="executePaymentResponse" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}ExecutePaymentResponse" minOccurs="0"/>
 *         &lt;element name="executeAuthorizationResponse" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}ExecuteAuthorizationResponse" minOccurs="0"/>
 *         &lt;element name="executeCaptureResponse" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}ExecuteCaptureResponse" minOccurs="0"/>
 *         &lt;element name="executeReversalResponse" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}ExecuteReversalResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPaymentStatusResponse", propOrder = {
    "result",
    "executePaymentResponse",
    "executeAuthorizationResponse",
    "executeCaptureResponse",
    "executeReversalResponse"
})
@XmlRootElement(name = "getPaymentStatusResponse", namespace = "http://mcPaymentAPI.services.multicardPaymentGatewayServer.it")
public class GetPaymentStatusResponse {

    @XmlElement(required = true)
    protected ResultEntityType result;
    protected ExecutePaymentResponse executePaymentResponse;
    protected ExecuteAuthorizationResponse executeAuthorizationResponse;
    protected ExecuteCaptureResponse executeCaptureResponse;
    protected ExecuteReversalResponse executeReversalResponse;

    /**
     * Recupera il valore della proprietÓ result.
     * 
     * @return
     *     possible object is
     *     {@link ResultEntityType }
     *     
     */
    public ResultEntityType getResult() {
        return result;
    }

    /**
     * Imposta il valore della proprietÓ result.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultEntityType }
     *     
     */
    public void setResult(ResultEntityType value) {
        this.result = value;
    }

    /**
     * Recupera il valore della proprietÓ executePaymentResponse.
     * 
     * @return
     *     possible object is
     *     {@link ExecutePaymentResponse }
     *     
     */
    public ExecutePaymentResponse getExecutePaymentResponse() {
        return executePaymentResponse;
    }

    /**
     * Imposta il valore della proprietÓ executePaymentResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecutePaymentResponse }
     *     
     */
    public void setExecutePaymentResponse(ExecutePaymentResponse value) {
        this.executePaymentResponse = value;
    }

    /**
     * Recupera il valore della proprietÓ executeAuthorizationResponse.
     * 
     * @return
     *     possible object is
     *     {@link ExecuteAuthorizationResponse }
     *     
     */
    public ExecuteAuthorizationResponse getExecuteAuthorizationResponse() {
        return executeAuthorizationResponse;
    }

    /**
     * Imposta il valore della proprietÓ executeAuthorizationResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecuteAuthorizationResponse }
     *     
     */
    public void setExecuteAuthorizationResponse(ExecuteAuthorizationResponse value) {
        this.executeAuthorizationResponse = value;
    }

    /**
     * Recupera il valore della proprietÓ executeCaptureResponse.
     * 
     * @return
     *     possible object is
     *     {@link ExecuteCaptureResponse }
     *     
     */
    public ExecuteCaptureResponse getExecuteCaptureResponse() {
        return executeCaptureResponse;
    }

    /**
     * Imposta il valore della proprietÓ executeCaptureResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecuteCaptureResponse }
     *     
     */
    public void setExecuteCaptureResponse(ExecuteCaptureResponse value) {
        this.executeCaptureResponse = value;
    }

    /**
     * Recupera il valore della proprietÓ executeReversalResponse.
     * 
     * @return
     *     possible object is
     *     {@link ExecuteReversalResponse }
     *     
     */
    public ExecuteReversalResponse getExecuteReversalResponse() {
        return executeReversalResponse;
    }

    /**
     * Imposta il valore della proprietÓ executeReversalResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link ExecuteReversalResponse }
     *     
     */
    public void setExecuteReversalResponse(ExecuteReversalResponse value) {
        this.executeReversalResponse = value;
    }

}
