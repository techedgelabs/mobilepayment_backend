package com.techedge.mp.quenit.elements;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiResponseContext">
 *       &lt;sequence>
 *         &lt;element name="marketingMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="preAuthOperationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "marketingMsg", "amount", "preAuthOperationID" })
@XmlRootElement(name = "preAuthorizationConsumeVoucherResponse")
public class PreAuthorizationConsumeVoucherResponse extends LoyaltyApiResponseContext {
    @XmlElement(required = true)
    protected String     marketingMsg;

    @XmlElement(required = true, nillable = true)
    protected BigDecimal amount;

    @XmlElement(required = true, nillable = true)
    protected String     preAuthOperationID;

    /**
     * Gets the value of the marketingMsg property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getMarketingMsg() {
        return marketingMsg;
    }

    /**
     * Sets the value of the marketingMsg property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    /**
     * Gets the value of the amount property.
     * 
     * @return
     *         possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * Gets the value of the preAuthOperationID property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getPreAuthOperationID() {
        return preAuthOperationID;
    }

    /**
     * Sets the value of the preAuthOperationID property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setPreAuthOperationID(String preAuthOperationID) {
        this.preAuthOperationID = preAuthOperationID;
    }

}
