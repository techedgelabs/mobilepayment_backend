
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per DeleteMcCardRefuelingRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="DeleteMcCardRefuelingRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it}mcCardInfoApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="userId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="mcCardDpan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="serverSerialNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeleteMcCardRefuelingRequest", propOrder = {
    "userId",
    "mcCardDpan",
    "serverSerialNumber"
})
@XmlRootElement(name = "deleteMcCardRefuelingRequest")
public class DeleteMcCardRefuelingRequest
    extends McCardInfoApiRequestContext
{

    @XmlElement(required = true)
    protected String userId;
    @XmlElement(required = true)
    protected String mcCardDpan;
    @XmlElement(required = true)
    protected String serverSerialNumber;

    /**
     * Recupera il valore della proprietÓ userId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserId() {
        return userId;
    }

    /**
     * Imposta il valore della proprietÓ userId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserId(String value) {
        this.userId = value;
    }

    /**
     * Recupera il valore della proprietÓ mcCardDpan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcCardDpan() {
        return mcCardDpan;
    }

    /**
     * Imposta il valore della proprietÓ mcCardDpan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcCardDpan(String value) {
        this.mcCardDpan = value;
    }

    /**
     * Recupera il valore della proprietÓ serverSerialNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServerSerialNumber() {
        return serverSerialNumber;
    }

    /**
     * Imposta il valore della proprietÓ serverSerialNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServerSerialNumber(String value) {
        this.serverSerialNumber = value;
    }

}
