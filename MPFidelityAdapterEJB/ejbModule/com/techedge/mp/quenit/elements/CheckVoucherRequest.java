
package com.techedge.mp.quenit.elements;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.techedge.mp.quenit.types.VoucherCode;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="voucherType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;sequence>
 *           &lt;element name="voucherList" type="{http://servizi.infogroup.it}voucherCode" maxOccurs="unbounded" minOccurs="1"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "voucherType",
    "voucherList"
})
@XmlRootElement(name = "checkVoucherRequest")
public class CheckVoucherRequest
    extends LoyaltyApiRequestContext
{

    @XmlElement(required = true)
    protected String voucherType;
    @XmlElement(required = true)
    protected List<VoucherCode> voucherList;

    /**
     * Gets the value of the voucherType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherType() {
        return voucherType;
    }

    /**
     * Sets the value of the voucherType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherType(String value) {
        this.voucherType = value;
    }

    /**
     * Gets the value of the voucherList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucherList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucherList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link VoucherCode }
     * 
     * 
     */
    public List<VoucherCode> getVoucherList() {
        if (voucherList == null) {
            voucherList = new ArrayList<VoucherCode>();
        }
        return this.voucherList;
    }

}
