
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per GetPaymentStatusRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="GetPaymentStatusRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}mcPaymentApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="paymentOperationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetPaymentStatusRequest", propOrder = {
    "paymentOperationId"
})
@XmlRootElement(name = "getPaymentStatusRequest")
public class GetPaymentStatusRequest
    extends McPaymentApiRequestContext
{

    @XmlElement(required = true)
    protected String paymentOperationId;

    /**
     * Recupera il valore della proprietÓ paymentOperationId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentOperationId() {
        return paymentOperationId;
    }

    /**
     * Imposta il valore della proprietÓ paymentOperationId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentOperationId(String value) {
        this.paymentOperationId = value;
    }

}
