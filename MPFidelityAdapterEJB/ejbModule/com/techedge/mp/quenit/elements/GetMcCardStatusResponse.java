
package com.techedge.mp.quenit.elements;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per GetMcCardStatusResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="GetMcCardStatusResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it}ResultEntityType"/>
 *         &lt;element name="dpanLists" type="{http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it}DpanDetailEntityType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetMcCardStatusResponse", propOrder = {
    "result",
    "dpanLists"
})
@XmlRootElement(name = "getMcCardStatusResponse", namespace = "http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it")
public class GetMcCardStatusResponse {

    @XmlElement(required = true)
    protected ResultEntityType result;
    protected List<DpanDetailEntityType> dpanLists;

    /**
     * Recupera il valore della proprietÓ result.
     * 
     * @return
     *     possible object is
     *     {@link ResultEntityType }
     *     
     */
    public ResultEntityType getResult() {
        return result;
    }

    /**
     * Imposta il valore della proprietÓ result.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultEntityType }
     *     
     */
    public void setResult(ResultEntityType value) {
        this.result = value;
    }

    /**
     * Gets the value of the dpanLists property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dpanLists property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDpanLists().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DpanDetailEntityType }
     * 
     * 
     */
    public List<DpanDetailEntityType> getDpanLists() {
        if (dpanLists == null) {
            dpanLists = new ArrayList<DpanDetailEntityType>();
        }
        return this.dpanLists;
    }

}
