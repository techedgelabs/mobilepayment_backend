
package com.techedge.mp.quenit.elements;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.techedge.mp.quenit.types.Voucher;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyRedemptionTransactionResponse"&gt;
 *       &lt;sequence&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "")
@XmlRootElement(name = "redemptionResponse")
public class RedemptionResponse extends LoyaltyApiResponseContext {

    @XmlElement(required = true)
    protected BigInteger redemptionCode;
    @XmlElement(required = true, nillable = true)
    protected Voucher voucher;
    @XmlElement(required = true, nillable = true)
    protected BigInteger credits;
    @XmlElement(required = true, nillable = true)
    protected BigInteger balance;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal balanceAmount;
    @XmlElement(required = true, nillable = true)
    protected String marketingMsg;
    @XmlElement(required = true, nillable = true)
    protected String warningMsg;

    /**
     * Recupera il valore della proprietÓ redemptionCode.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRedemptionCode() {
        return redemptionCode;
    }

    /**
     * Imposta il valore della proprietÓ redemptionCode.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRedemptionCode(BigInteger value) {
        this.redemptionCode = value;
    }

    /**
     * Recupera il valore della proprietÓ voucher.
     * 
     * @return
     *     possible object is
     *     {@link Voucher }
     *     
     */
    public Voucher getVoucher() {
        return voucher;
    }

    /**
     * Imposta il valore della proprietÓ voucher.
     * 
     * @param value
     *     allowed object is
     *     {@link Voucher }
     *     
     */
    public void setVoucher(Voucher value) {
        this.voucher = value;
    }

    /**
     * Recupera il valore della proprietÓ credits.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCredits() {
        return credits;
    }

    /**
     * Imposta il valore della proprietÓ credits.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCredits(BigInteger value) {
        this.credits = value;
    }

    /**
     * Recupera il valore della proprietÓ balance.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getBalance() {
        return balance;
    }

    /**
     * Imposta il valore della proprietÓ balance.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setBalance(BigInteger value) {
        this.balance = value;
    }

    /**
     * Recupera il valore della proprietÓ balanceAmount.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBalanceAmount() {
        return balanceAmount;
    }

    /**
     * Imposta il valore della proprietÓ balanceAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBalanceAmount(BigDecimal value) {
        this.balanceAmount = value;
    }

    /**
     * Recupera il valore della proprietÓ marketingMsg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketingMsg() {
        return marketingMsg;
    }

    /**
     * Imposta il valore della proprietÓ marketingMsg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketingMsg(String value) {
        this.marketingMsg = value;
    }

    /**
     * Recupera il valore della proprietÓ warningMsg.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarningMsg() {
        return warningMsg;
    }

    /**
     * Imposta il valore della proprietÓ warningMsg.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarningMsg(String value) {
        this.warningMsg = value;
    }

}
