
package com.techedge.mp.quenit.elements;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;

import com.techedge.mp.quenit.types.Voucher;


/**
 * <p>Java class for voucherTransactionResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="voucherTransactionResponse">
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiResponseContext">
 *       &lt;sequence>
 *         &lt;element name="marketingMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="warningMsg" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;sequence>
 *           &lt;element name="voucherList" type="{http://servizi.infogroup.it}voucher" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "voucherTransactionResponse", propOrder = {
    "marketingMsg",
    "warningMsg",
    "voucherList"
})
@XmlSeeAlso({
    ConsumeVoucherResponse.class,
    CheckConsumeVoucherTransactionResponse.class
})
public class VoucherTransactionResponse
    extends LoyaltyApiResponseContext
{

    @XmlElement(required = true)
    protected String marketingMsg;
    @XmlElement(required = false, nillable = true)
    protected String warningMsg;
    protected List<Voucher> voucherList;

    /**
     * Gets the value of the marketingMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarketingMsg() {
        return marketingMsg;
    }

    /**
     * Sets the value of the marketingMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarketingMsg(String value) {
        this.marketingMsg = value;
    }
    
    /**
     * Gets the value of the warningMsg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWarningMsg() {
        return warningMsg;
    }

    /**
     * Sets the value of the warningMsg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWarningMsg(String value) {
        this.warningMsg = value;
    }

    /**
     * Gets the value of the voucherList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucherList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucherList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Voucher }
     * 
     * 
     */
    public List<Voucher> getVoucherList() {
        if (voucherList == null) {
            voucherList = new ArrayList<Voucher>();
        }
        return this.voucherList;
    }

}
