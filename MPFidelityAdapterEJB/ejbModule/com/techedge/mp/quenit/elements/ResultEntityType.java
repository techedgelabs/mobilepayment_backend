
package com.techedge.mp.quenit.elements;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

import com.techedge.mp.fidelity.adapter.business.interfaces.CodeEnum;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnumStatusType;
import com.techedge.mp.fidelity.adapter.business.interfaces.KeyValueInfo;
import com.techedge.mp.fidelity.adapter.business.interfaces.ResultDetail;


/**
 * <p>Classe Java per ResultEntityType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ResultEntityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="status" type="{http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it}EnumStatusType"/>
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="params" type="{http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it}KeyValueEntityType" maxOccurs="100" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ResultEntityType", propOrder = {
    "transactionId",
    "status",
    "code",
    "message",
    "params"
})
public class ResultEntityType {

    @XmlElement(required = true)
    protected String transactionId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected EnumStatusType status;
    @XmlElement(required = true)
    protected String code;
    protected String message;
    protected List<KeyValueEntityType> params;

    /**
     * Recupera il valore della proprietÓ transactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Imposta il valore della proprietÓ transactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionId(String value) {
        this.transactionId = value;
    }

    /**
     * Recupera il valore della proprietÓ status.
     * 
     * @return
     *     possible object is
     *     {@link EnumStatusType }
     *     
     */
    public EnumStatusType getStatus() {
        return status;
    }

    /**
     * Imposta il valore della proprietÓ status.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumStatusType }
     *     
     */
    public void setStatus(EnumStatusType value) {
        this.status = value;
    }

    /**
     * Recupera il valore della proprietÓ code.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCode() {
        return code;
    }

    /**
     * Imposta il valore della proprietÓ code.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCode(String value) {
        this.code = value;
    }

    /**
     * Recupera il valore della proprietÓ message.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Imposta il valore della proprietÓ message.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the params property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the params property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParams().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KeyValueEntityType }
     * 
     * 
     */
    public List<KeyValueEntityType> getParams() {
        if (params == null) {
            params = new ArrayList<KeyValueEntityType>();
        }
        return this.params;
    }

    public ResultDetail toResultDetail() {
        
        ResultDetail resultDetail = new ResultDetail();
        resultDetail.setCode(CodeEnum.getCodeEnum(this.code));
        resultDetail.setMessage(this.message);
        resultDetail.setStatus(this.status);
        resultDetail.setTransactionId(this.transactionId);
        
        if (this.params != null && !this.params.isEmpty()) {
            for (KeyValueEntityType keyValueEntityType : this.params) {
                KeyValueInfo keyValueInfo = new KeyValueInfo();
                keyValueInfo.setKey(keyValueEntityType.getKey());
                keyValueInfo.setValue(keyValueEntityType.getValue());
                resultDetail.getParams().add(keyValueInfo);
            }
        }
        
        return resultDetail;
    }
}
