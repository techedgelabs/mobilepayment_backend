
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per EnumStatusMcCardType.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="EnumStatusMcCardType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PRE_ENROLLED"/>
 *     &lt;enumeration value="ACTIVATED"/>
 *     &lt;enumeration value="CANCELED"/>
 *     &lt;enumeration value="BLOCKED"/>
 *     &lt;enumeration value="REASSIGNED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EnumStatusMcCardType")
@XmlEnum
public enum EnumStatusMcCardType {

    PRE_ENROLLED,
    ACTIVATED,
    CANCELED,
    BLOCKED,
    REASSIGNED;

    public String value() {
        return name();
    }

    public static EnumStatusMcCardType fromValue(String v) {
        return valueOf(v);
    }

}
