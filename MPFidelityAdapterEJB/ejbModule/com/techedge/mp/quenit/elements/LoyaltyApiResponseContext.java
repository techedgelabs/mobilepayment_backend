
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loyaltyApiResponseContext complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loyaltyApiResponseContext">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="csTransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="statusCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="messageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loyaltyApiResponseContext", propOrder = {
    "csTransactionID",
    "statusCode",
    "messageCode"
})
@XmlSeeAlso({
    ReverseLoadLoyaltyCreditsTransactionResponse.class,
    GetLoyaltyCardsListResponse.class,
    ReverseConsumeVoucherTransactionResponse.class,
    CheckVoucherResponse.class,
    VoucherTransactionResponse.class,
    EnableLoyaltyCardResponse.class,
    LoyaltyCreditTransactionResponse.class,
    CreateVoucherResponse.class,
    InfoRedemptionResponse.class,
    CreateVoucherPromotionalResponse.class
})
public class LoyaltyApiResponseContext {

    @XmlElement(required = true)
    protected String csTransactionID;
    @XmlElement(required = true)
    protected String statusCode;
    @XmlElement(required = true)
    protected String messageCode;

    /**
     * Gets the value of the csTransactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCsTransactionID() {
        return csTransactionID;
    }

    /**
     * Sets the value of the csTransactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCsTransactionID(String value) {
        this.csTransactionID = value;
    }

    /**
     * Gets the value of the statusCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusCode() {
        return statusCode;
    }

    /**
     * Sets the value of the statusCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    /**
     * Gets the value of the messageCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Sets the value of the messageCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }

}
