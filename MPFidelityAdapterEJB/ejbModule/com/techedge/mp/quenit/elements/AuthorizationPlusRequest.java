
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AuthorizationPlus" type="{http://pojo.ws.multicard.eni}AuthorizationPlusRequestPojo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "authorizationPlus"
})
@XmlRootElement(name = "authorizationPlusRequest", namespace = "http://service.ws.multicard.eni")
public class AuthorizationPlusRequest {

    @XmlElement(name = "AuthorizationPlus", namespace = "http://service.ws.multicard.eni", required = true)
    protected AuthorizationPlusRequestPojo authorizationPlus;

    /**
     * Recupera il valore della proprietÓ authorizationPlus.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationPlusRequestPojo }
     *     
     */
    public AuthorizationPlusRequestPojo getAuthorizationPlus() {
        return authorizationPlus;
    }

    /**
     * Imposta il valore della proprietÓ authorizationPlus.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationPlusRequestPojo }
     *     
     */
    public void setAuthorizationPlus(AuthorizationPlusRequestPojo value) {
        this.authorizationPlus = value;
    }

}
