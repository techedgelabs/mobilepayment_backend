
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for reverseTransactionRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="reverseTransactionRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="operationIDtoReverse" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "reverseTransactionRequest", propOrder = {
    "operationIDtoReverse"
})
@XmlSeeAlso({
    ReverseConsumeVoucherTransactionRequest.class,
    ReverseLoadLoyaltyCreditsTransactionRequest.class
})
public class ReverseTransactionRequest
    extends LoyaltyApiRequestContext
{

    @XmlElement(required = true)
    protected String operationIDtoReverse;

    /**
     * Gets the value of the operationIDtoReverse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationIDtoReverse() {
        return operationIDtoReverse;
    }

    /**
     * Sets the value of the operationIDtoReverse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationIDtoReverse(String value) {
        this.operationIDtoReverse = value;
    }

}
