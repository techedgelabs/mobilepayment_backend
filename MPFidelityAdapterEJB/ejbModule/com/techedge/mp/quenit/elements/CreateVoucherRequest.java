package com.techedge.mp.quenit.elements;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="voucherType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="totalAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="bankTransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="shopTransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="authorizationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "voucherType", "totalAmount", "bankTransactionID", "shopTransactionID", "authorizationCode" })
@XmlRootElement(name = "createVoucherRequest")
public class CreateVoucherRequest extends LoyaltyApiRequestContext {

    @XmlElement(required = true)
    protected String     voucherType;
    @XmlElement(required = true)
    protected BigDecimal totalAmount;
    @XmlElement(required = true)
    protected String     bankTransactionID;
    @XmlElement(required = true)
    protected String     shopTransactionID;
    @XmlElement(required = true)
    protected String     authorizationCode;

    /**
     * Gets the value of the voucherType property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getVoucherType() {
        return voucherType;
    }

    /**
     * Sets the value of the voucherType property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setVoucherType(String value) {
        this.voucherType = value;
    }

    /**
     * Gets the value of the totalAmount property.
     * 
     * @return
     *         possible object is {@link BigDecimal }
     * 
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     * 
     * @param value
     *            allowed object is {@link BigDecimal }
     * 
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * Gets the value of the bankTransactionID property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getBankTransactionID() {
        return bankTransactionID;
    }

    /**
     * Sets the value of the bankTransactionID property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setBankTransactionID(String bankTransactionID) {
        this.bankTransactionID = bankTransactionID;
    }

    /**
     * Gets the value of the shopTransactionID property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getShopTransactionID() {
        return shopTransactionID;
    }

    /**
     * Sets the value of the shopTransactionID property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setShopTransactionID(String shopTransactionID) {
        this.shopTransactionID = shopTransactionID;
    }

    /**
     * Gets the value of the authorizationCode property.
     * 
     * @return
     *         possible object is {@link String }
     * 
     */
    public String getAuthorizationCode() {
        return authorizationCode;
    }

    /**
     * Sets the value of the authorizationCode property.
     * 
     * @param value
     *            allowed object is {@link String }
     * 
     */
    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

}
