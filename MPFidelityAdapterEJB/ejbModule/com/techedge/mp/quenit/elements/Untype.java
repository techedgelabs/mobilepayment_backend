
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per Untype complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Untype">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="digit" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Untype", propOrder = {
    "digit",
    "value"
})
public class Untype {

    protected int digit;
    protected int value;

    /**
     * Recupera il valore della proprietÓ digit.
     * 
     */
    public int getDigit() {
        return digit;
    }

    /**
     * Imposta il valore della proprietÓ digit.
     * 
     */
    public void setDigit(int value) {
        this.digit = value;
    }

    /**
     * Recupera il valore della proprietÓ value.
     * 
     */
    public int getValue() {
        return value;
    }

    /**
     * Imposta il valore della proprietÓ value.
     * 
     */
    public void setValue(int value) {
        this.value = value;
    }

}
