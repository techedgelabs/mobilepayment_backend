
package com.techedge.mp.quenit.elements;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ExecuteAuthorizationResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ExecuteAuthorizationResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}ResultEntityType"/>
 *         &lt;element name="retrievalRefNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="authCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fuelEnabledProducts" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}ProductDatailEntityType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="receiptElements" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}KeyValueEntityType" maxOccurs="20" minOccurs="0"/>
 *         &lt;element name="amountAuthorized" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExecuteAuthorizationResponse", propOrder = {
    "result",
    "retrievalRefNumber",
    "authCode",
    "fuelEnabledProducts",
    "receiptElements",
    "amountAuthorized",
    "currencyCode"
})
@XmlRootElement(name = "executeAuthorizationResponse", namespace = "http://mcPaymentAPI.services.multicardPaymentGatewayServer.it")
public class ExecuteAuthorizationResponse {

    @XmlElement(required = true)
    protected ResultEntityType result;
    protected String retrievalRefNumber;
    protected String authCode;
    protected List<ProductDatailEntityType> fuelEnabledProducts;
    protected List<KeyValueEntityType> receiptElements;
    protected Integer amountAuthorized;
    protected String currencyCode;

    /**
     * Recupera il valore della proprietÓ result.
     * 
     * @return
     *     possible object is
     *     {@link ResultEntityType }
     *     
     */
    public ResultEntityType getResult() {
        return result;
    }

    /**
     * Imposta il valore della proprietÓ result.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultEntityType }
     *     
     */
    public void setResult(ResultEntityType value) {
        this.result = value;
    }

    /**
     * Recupera il valore della proprietÓ retrievalRefNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetrievalRefNumber() {
        return retrievalRefNumber;
    }

    /**
     * Imposta il valore della proprietÓ retrievalRefNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetrievalRefNumber(String value) {
        this.retrievalRefNumber = value;
    }

    /**
     * Recupera il valore della proprietÓ authCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * Imposta il valore della proprietÓ authCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthCode(String value) {
        this.authCode = value;
    }

    /**
     * Gets the value of the fuelEnabledProducts property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fuelEnabledProducts property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFuelEnabledProducts().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProductDatailEntityType }
     * 
     * 
     */
    public List<ProductDatailEntityType> getFuelEnabledProducts() {
        if (fuelEnabledProducts == null) {
            fuelEnabledProducts = new ArrayList<ProductDatailEntityType>();
        }
        return this.fuelEnabledProducts;
    }

    /**
     * Gets the value of the receiptElements property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the receiptElements property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReceiptElements().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KeyValueEntityType }
     * 
     * 
     */
    public List<KeyValueEntityType> getReceiptElements() {
        if (receiptElements == null) {
            receiptElements = new ArrayList<KeyValueEntityType>();
        }
        return this.receiptElements;
    }

    /**
     * Recupera il valore della proprietÓ amountAuthorized.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAmountAuthorized() {
        return amountAuthorized;
    }

    /**
     * Imposta il valore della proprietÓ amountAuthorized.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAmountAuthorized(Integer value) {
        this.amountAuthorized = value;
    }

    /**
     * Recupera il valore della proprietÓ currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Imposta il valore della proprietÓ currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

}
