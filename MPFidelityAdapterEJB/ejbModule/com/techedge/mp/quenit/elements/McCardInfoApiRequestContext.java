
package com.techedge.mp.quenit.elements;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per mcCardInfoApiRequestContext complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="mcCardInfoApiRequestContext">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operationId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="partnerType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="requestTimestamp" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *         &lt;element name="params" type="{http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it}KeyValueEntityType" maxOccurs="100" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "mcCardInfoApiRequestContext", propOrder = {
    "operationId",
    "partnerType",
    "requestTimestamp",
    "params"
})
@XmlSeeAlso({
    GetMcCardStatusRequest.class
})
public class McCardInfoApiRequestContext {

    @XmlElement(required = true)
    protected String operationId;
    @XmlElement(required = true)
    protected String partnerType;
    protected long requestTimestamp;
    protected List<KeyValueEntityType> params;

    /**
     * Recupera il valore della proprietÓ operationId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperationId() {
        return operationId;
    }

    /**
     * Imposta il valore della proprietÓ operationId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperationId(String value) {
        this.operationId = value;
    }

    /**
     * Recupera il valore della proprietÓ partnerType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPartnerType() {
        return partnerType;
    }

    /**
     * Imposta il valore della proprietÓ partnerType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPartnerType(String value) {
        this.partnerType = value;
    }

    /**
     * Recupera il valore della proprietÓ requestTimestamp.
     * 
     */
    public long getRequestTimestamp() {
        return requestTimestamp;
    }

    /**
     * Imposta il valore della proprietÓ requestTimestamp.
     * 
     */
    public void setRequestTimestamp(long value) {
        this.requestTimestamp = value;
    }

    /**
     * Gets the value of the params property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the params property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParams().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link KeyValueEntityType }
     * 
     * 
     */
    public List<KeyValueEntityType> getParams() {
        if (params == null) {
            params = new ArrayList<KeyValueEntityType>();
        }
        return this.params;
    }

}
