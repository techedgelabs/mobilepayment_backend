
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ExecuteAuthorizationRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ExecuteAuthorizationRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}mcPaymentApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="mcCardDpan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="authCryptogram" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="shopCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExecuteAuthorizationRequest", propOrder = {
    "mcCardDpan",
    "authCryptogram",
    "shopCode",
    "amount",
    "currencyCode"
})
@XmlRootElement(name = "executeAuthorizationRequest")
public class ExecuteAuthorizationRequest
    extends McPaymentApiRequestContext
{

    @XmlElement(required = true)
    protected String mcCardDpan;
    @XmlElement(required = true)
    protected String authCryptogram;
    @XmlElement(required = true)
    protected String shopCode;
    protected int amount;
    @XmlElement(required = true)
    protected String currencyCode;

    /**
     * Recupera il valore della proprietÓ mcCardDpan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcCardDpan() {
        return mcCardDpan;
    }

    /**
     * Imposta il valore della proprietÓ mcCardDpan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcCardDpan(String value) {
        this.mcCardDpan = value;
    }

    /**
     * Recupera il valore della proprietÓ authCryptogram.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthCryptogram() {
        return authCryptogram;
    }

    /**
     * Imposta il valore della proprietÓ authCryptogram.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthCryptogram(String value) {
        this.authCryptogram = value;
    }

    /**
     * Recupera il valore della proprietÓ shopCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopCode() {
        return shopCode;
    }

    /**
     * Imposta il valore della proprietÓ shopCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopCode(String value) {
        this.shopCode = value;
    }

    /**
     * Recupera il valore della proprietÓ amount.
     * 
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Imposta il valore della proprietÓ amount.
     * 
     */
    public void setAmount(int value) {
        this.amount = value;
    }

    /**
     * Recupera il valore della proprietÓ currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Imposta il valore della proprietÓ currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

}
