
package com.techedge.mp.quenit.elements;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ExecutePaymentRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ExecutePaymentRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}mcPaymentApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="mcCardDpan" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="authCryptogram" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="shopCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refuelMode" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}EnumRefuelModeType" minOccurs="0"/>
 *         &lt;element name="refuelInfo" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}RefuelInfoEntityType" maxOccurs="3"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExecutePaymentRequest", propOrder = {
    "mcCardDpan",
    "authCryptogram",
    "shopCode",
    "amount",
    "currencyCode",
    "refuelMode",
    "refuelInfo"
})
@XmlRootElement(name = "executePaymentRequest")
public class ExecutePaymentRequest
    extends McPaymentApiRequestContext
{

    @XmlElement(required = true)
    protected String mcCardDpan;
    @XmlElement(required = true)
    protected String authCryptogram;
    @XmlElement(required = true)
    protected String shopCode;
    protected int amount;
    @XmlElement(required = true)
    protected String currencyCode;
    @XmlSchemaType(name = "string")
    protected EnumRefuelModeType refuelMode;
    @XmlElement(required = true)
    protected List<RefuelInfoEntityType> refuelInfo;

    /**
     * Recupera il valore della proprietÓ mcCardDpan.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMcCardDpan() {
        return mcCardDpan;
    }

    /**
     * Imposta il valore della proprietÓ mcCardDpan.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMcCardDpan(String value) {
        this.mcCardDpan = value;
    }

    /**
     * Recupera il valore della proprietÓ authCryptogram.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthCryptogram() {
        return authCryptogram;
    }

    /**
     * Imposta il valore della proprietÓ authCryptogram.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthCryptogram(String value) {
        this.authCryptogram = value;
    }

    /**
     * Recupera il valore della proprietÓ shopCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopCode() {
        return shopCode;
    }

    /**
     * Imposta il valore della proprietÓ shopCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopCode(String value) {
        this.shopCode = value;
    }

    /**
     * Recupera il valore della proprietÓ amount.
     * 
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Imposta il valore della proprietÓ amount.
     * 
     */
    public void setAmount(int value) {
        this.amount = value;
    }

    /**
     * Recupera il valore della proprietÓ currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Imposta il valore della proprietÓ currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

    /**
     * Recupera il valore della proprietÓ refuelMode.
     * 
     * @return
     *     possible object is
     *     {@link EnumRefuelModeType }
     *     
     */
    public EnumRefuelModeType getRefuelMode() {
        return refuelMode;
    }

    /**
     * Imposta il valore della proprietÓ refuelMode.
     * 
     * @param value
     *     allowed object is
     *     {@link EnumRefuelModeType }
     *     
     */
    public void setRefuelMode(EnumRefuelModeType value) {
        this.refuelMode = value;
    }

    /**
     * Gets the value of the refuelInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the refuelInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRefuelInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefuelInfoEntityType }
     * 
     * 
     */
    public List<RefuelInfoEntityType> getRefuelInfo() {
        if (refuelInfo == null) {
            refuelInfo = new ArrayList<RefuelInfoEntityType>();
        }
        return this.refuelInfo;
    }

}
