
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiRequestContext">
 *       &lt;sequence>
 *         &lt;element name="fiscalCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="panCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="enable" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fiscalCode",
    "panCode",
    "enable"
})
@XmlRootElement(name = "enableLoyaltyCardRequest")
public class EnableLoyaltyCardRequest
    extends LoyaltyApiRequestContext
{

    @XmlElement(required = true)
    protected String fiscalCode;
    @XmlElement(required = true)
    protected String panCode;
    protected boolean enable;

    /**
     * Gets the value of the fiscalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiscalCode() {
        return fiscalCode;
    }

    /**
     * Sets the value of the fiscalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiscalCode(String value) {
        this.fiscalCode = value;
    }

    /**
     * Gets the value of the panCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPanCode() {
        return panCode;
    }

    /**
     * Sets the value of the panCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPanCode(String value) {
        this.panCode = value;
    }

    /**
     * Gets the value of the enable property.
     * 
     */
    public boolean isEnable() {
        return enable;
    }

    /**
     * Sets the value of the enable property.
     * 
     */
    public void setEnable(boolean value) {
        this.enable = value;
    }

}
