package com.techedge.mp.quenit.elements;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import com.techedge.mp.quenit.types.CategoryRedemption;
import com.techedge.mp.quenit.types.Redemption;

/**
 * <p>
 * Java class for anonymous complex type.
 * 
 * <p>
 * The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;extension base="{http://servizi.infogroup.it}loyaltyApiResponseContext">
 *       &lt;sequence>
 *         &lt;element name="redemptionList" type="{http://servizi.infogroup.it}redemption" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="categoryRedemptionList" type="{http://servizi.infogroup.it}categoryRedemption" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "infoRedemptionResponse")
public class InfoRedemptionResponse extends LoyaltyApiResponseContext {
    protected List<Redemption> redemptionList;
    protected List<CategoryRedemption> categoryRedemptionList;

    /**
     * Gets the value of the redemptionList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the redemptionList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRedemptionList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Redemption }
     * 
     * 
     */
    public List<Redemption> getRedemptionList() {
        if (redemptionList == null) {
            redemptionList = new ArrayList<Redemption>();
        }
        return this.redemptionList;
    }

    public List<CategoryRedemption> getCategoryRedemptionList() {
        if (categoryRedemptionList == null) {
            categoryRedemptionList = new ArrayList<CategoryRedemption>();
        }
        return this.categoryRedemptionList;
    }

}
