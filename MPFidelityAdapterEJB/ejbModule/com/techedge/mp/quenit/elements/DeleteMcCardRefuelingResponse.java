
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per DeleteMcCardRefuelingResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="DeleteMcCardRefuelingResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="result" type="{http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it}ResultEntityType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DeleteMcCardRefuelingResponse", propOrder = {
    "result"
})
@XmlRootElement(name = "deleteMcCardRefuelingResponse", namespace = "http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it")
public class DeleteMcCardRefuelingResponse {

    @XmlElement(required = true)
    protected ResultEntityType result;

    /**
     * Recupera il valore della proprietÓ result.
     * 
     * @return
     *     possible object is
     *     {@link ResultEntityType }
     *     
     */
    public ResultEntityType getResult() {
        return result;
    }

    /**
     * Imposta il valore della proprietÓ result.
     * 
     * @param value
     *     allowed object is
     *     {@link ResultEntityType }
     *     
     */
    public void setResult(ResultEntityType value) {
        this.result = value;
    }

}
