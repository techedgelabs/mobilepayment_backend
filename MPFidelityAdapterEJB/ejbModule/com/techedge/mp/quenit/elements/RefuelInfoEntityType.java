
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per RefuelInfoEntityType complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="RefuelInfoEntityType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="productCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="unitPrice" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}Untype"/>
 *         &lt;element name="quantity" type="{http://mcPaymentAPI.services.multicardPaymentGatewayServer.it}Untype"/>
 *         &lt;element name="totAmnt" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="currencyCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RefuelInfoEntityType", propOrder = {
    "productCode",
    "unitPrice",
    "quantity",
    "totAmnt",
    "currencyCode"
})
public class RefuelInfoEntityType {

    @XmlElement(required = true)
    protected String productCode;
    @XmlElement(required = true)
    protected Untype unitPrice;
    @XmlElement(required = true)
    protected Untype quantity;
    protected int totAmnt;
    @XmlElement(required = true)
    protected String currencyCode;

    /**
     * Recupera il valore della proprietÓ productCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Imposta il valore della proprietÓ productCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCode(String value) {
        this.productCode = value;
    }

    /**
     * Recupera il valore della proprietÓ unitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Untype }
     *     
     */
    public Untype getUnitPrice() {
        return unitPrice;
    }

    /**
     * Imposta il valore della proprietÓ unitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Untype }
     *     
     */
    public void setUnitPrice(Untype value) {
        this.unitPrice = value;
    }

    /**
     * Recupera il valore della proprietÓ quantity.
     * 
     * @return
     *     possible object is
     *     {@link Untype }
     *     
     */
    public Untype getQuantity() {
        return quantity;
    }

    /**
     * Imposta il valore della proprietÓ quantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Untype }
     *     
     */
    public void setQuantity(Untype value) {
        this.quantity = value;
    }

    /**
     * Recupera il valore della proprietÓ totAmnt.
     * 
     */
    public int getTotAmnt() {
        return totAmnt;
    }

    /**
     * Imposta il valore della proprietÓ totAmnt.
     * 
     */
    public void setTotAmnt(int value) {
        this.totAmnt = value;
    }

    /**
     * Recupera il valore della proprietÓ currencyCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrencyCode() {
        return currencyCode;
    }

    /**
     * Imposta il valore della proprietÓ currencyCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrencyCode(String value) {
        this.currencyCode = value;
    }

}
