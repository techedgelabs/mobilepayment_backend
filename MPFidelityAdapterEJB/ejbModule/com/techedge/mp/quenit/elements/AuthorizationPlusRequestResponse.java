
package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="authorizationPlusResponse" type="{http://pojo.ws.multicard.eni}AuthorizationPlusResponsePojo"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "authorizationPlusResponse"
})
@XmlRootElement(name = "authorizationPlusRequestResponse", namespace = "http://service.ws.multicard.eni")
public class AuthorizationPlusRequestResponse {

    @XmlElement(namespace = "http://service.ws.multicard.eni", required = true)
    protected AuthorizationPlusResponsePojo authorizationPlusResponse;

    /**
     * Recupera il valore della proprietÓ authorizationPlusResponse.
     * 
     * @return
     *     possible object is
     *     {@link AuthorizationPlusResponsePojo }
     *     
     */
    public AuthorizationPlusResponsePojo getAuthorizationPlusResponse() {
        return authorizationPlusResponse;
    }

    /**
     * Imposta il valore della proprietÓ authorizationPlusResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link AuthorizationPlusResponsePojo }
     *     
     */
    public void setAuthorizationPlusResponse(AuthorizationPlusResponsePojo value) {
        this.authorizationPlusResponse = value;
    }

}
