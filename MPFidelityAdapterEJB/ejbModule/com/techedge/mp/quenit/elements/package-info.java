@XmlSchema(
        namespace = "http://servizi.infogroup.it",
        elementFormDefault = XmlNsForm.QUALIFIED, 
        xmlns = { 
                @XmlNs(
                        prefix = "soap-env", 
                        namespaceURI = "http://schemas.xmlsoap.org/soap/envelope"
                 ),
                 @XmlNs(
                         prefix = "ser", 
                         namespaceURI = "http://servizi.infogroup.it"
                  )
})

package com.techedge.mp.quenit.elements;

import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;

