package com.techedge.mp.quenit.service;

import org.scribe.builder.ServiceBuilder;
import org.scribe.exceptions.OAuthConnectionException;
import org.scribe.model.Token;
import org.scribe.oauth.OAuthService;

import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.quenit.client.EniOAuthApi;
import com.techedge.mp.quenit.model.EniOAuthRequest;
import com.techedge.mp.quenit.model.EniResponse;

public class EniOAuthService {

    private final String USER_AGENT           = "Mozilla/5.0";
    //private final String CHARSET          = "UTF-8";
    // private final String CONTENT_TYPE_FORM = "application/x-www-form-urlencoded";
    //private final String CONTENT_TYPE_XML = "text/xml";

    private String       requestURL           = null;
    private String       consumerKey          = null;
    private String       consumerSecret       = null;
    private String       proxyHost            = null;
    private String       proxyPort            = null;
    private String       proxyNoHosts         = null;
    private boolean      debugSOAP            = false;
    private String       debugSSL             = null;
    private String       keyStore             = null;
    private String       keyStorePassword     = null;
    private String       keyPassword          = null;

    public EniOAuthService(String consumerKey, String consumerSecret, String requestURL, String keyStore, String keyStorePasswordword, String keyPassword) {
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
        this.requestURL = requestURL;
        this.keyStore = keyStore;
        this.keyStorePassword = keyStorePasswordword;
        this.keyPassword = keyPassword;
    }

    public void setProxy(String proxyHost, String proxyPort, String proxyNoHosts) {
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.proxyNoHosts = proxyNoHosts;
    }

    public void setDebug(boolean debugSOAP, boolean debugSSL) {
        this.debugSOAP = debugSOAP;
        if (debugSSL) {
            this.debugSSL = "ssl";
        }
    }

    public EniResponse execute(String body) throws OAuthConnectionException {

        Proxy proxy = new Proxy(proxyHost, proxyPort, proxyNoHosts);
        
        proxy.setHttp();
        
        if (debugSSL != null) {

            System.setProperty("javax.net.debug", debugSSL);
        }
        else {

            System.clearProperty("javax.net.debug");

        }

        OAuthService service = buildService(consumerKey, consumerSecret);

        EniOAuthRequest request = new EniOAuthRequest(requestURL);
        request.addHeader("SOAPAction", "\"\"");
        request.addHeader("User-Agent", USER_AGENT);
        //request.addHeader("Content-type", CONTENT_TYPE_XML + "; charset=" + CHARSET);

        request.addBodyParameter("request_body", body);
        request.addPayload(body);

        service.signRequest(Token.empty(), request);
        EniResponse response = request.send(keyStore, keyStorePassword, keyPassword);

        proxy.unsetHttp();
        
        return response;
    }

    public boolean inDebug() {
        return debugSOAP;
    }
    
    private OAuthService buildService(String consumerKey, String consumerSecret) {
        ServiceBuilder serviceBuilder = new ServiceBuilder();

        if (debugSOAP) {
            serviceBuilder.debug();
        }

        return serviceBuilder.provider(EniOAuthApi.class).apiKey(consumerKey).apiSecret(consumerSecret).build();
    }

}
