package com.techedge.mp.quenit.exception;


public class EniWsException extends Exception {
    
    /**
     * 
     */
    private static final long serialVersionUID = -593042575365657066L;

    public EniWsException(String message, Throwable cause) {
        super(message, cause);
    }

    public EniWsException(String message) {
        this(message, null);
    }



}
