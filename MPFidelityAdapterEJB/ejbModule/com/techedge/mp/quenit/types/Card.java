
package com.techedge.mp.quenit.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for card complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="card">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="panCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="eanCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="cardCoverType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="virtual" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "card", propOrder = {
    "panCode",
    "eanCode",
    "cardCoverType",
    "virtual"
})
public class Card {

    @XmlElement(required = true)
    protected String panCode;
    @XmlElement(required = true, nillable = true)
    protected String eanCode;
    @XmlElement(required = true)
    protected String cardCoverType;
    @XmlElement(required = true)
    protected String virtual;
    /**
     * Gets the value of the panCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPanCode() {
        return panCode;
    }

    /**
     * Sets the value of the panCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPanCode(String value) {
        this.panCode = value;
    }

    /**
     * Gets the value of the eanCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEanCode() {
        return eanCode;
    }

    /**
     * Sets the value of the eanCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEanCode(String value) {
        this.eanCode = value;
    }

    /**
     * Gets the value of the cardCoverType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCardCoverType() {
        return cardCoverType;
    }

    /**
     * Sets the value of the cardCoverType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCardCoverType(String value) {
        this.cardCoverType = value;
    }
    
    /**
     * Gets the value of the virtual property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVirtual() {
        return virtual;
    }

    /**
     * Sets the value of the virtual property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVirtual(String value) {
        this.virtual = value;
    }
    
}
