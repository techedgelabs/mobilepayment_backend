
package com.techedge.mp.quenit.types;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for card complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="redemption">
 *   &lt;complexContent>
 *     &lt;sequence>
 *       &lt;element name="categoryCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;element name="redemptionList" type="{http://servizi.infogroup.it}redemption" maxOccurs="unbounded" minOccurs="0"/>
 *     &lt;/sequence>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "categoryRedemption", propOrder = {
    "categoryCode",
    "redemptionList"
})
public class CategoryRedemption {

    @XmlElement(required = true)
    protected String categoryCode;
    protected List<Redemption> redemptionList;
    
    public String getCategoryCode() {
        return categoryCode;
    }
    
    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public List<Redemption> getRedemptionList() {
        return redemptionList;
    }

    public void setRedemptionList(List<Redemption> redemptionList) {
        this.redemptionList = redemptionList;
    }
    
}
