package com.techedge.mp.quenit.types;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



/**
 * <p>Java class for voucherCode complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="refuelMode">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="refuelMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "refuelMode", propOrder = {
    "refuelMode"
})

public class RefuelMode {

    @XmlElement(required = true)
    protected String refuelMode;

    /**
     * Gets the value of the refuelMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefuelMode() {
        return refuelMode;
    }

    /**
     * Sets the value of the refuelMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefuelMode(String value) {
        this.refuelMode = value;
    }
    
}
