
package com.techedge.mp.quenit.types;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for voucher complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="voucher">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="voucherCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="voucherStatus" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="voucherType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="voucherValue">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="initialValue">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="consumedValue">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="voucherBalanceDue">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="expirationDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="promoCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="promoDescrition" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="promoDoc" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="minQuantity">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="minAmount">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;sequence>
 *           &lt;element name="validProduct" type="{http://servizi.infogroup.it}product" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;element name="validPV" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;sequence>
 *           &lt;element name="validRefuelMode" type="{http://servizi.infogroup.it}refuelMode" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;/sequence>
 *         &lt;element name="isCombinable" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="promoPartner" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "voucher", propOrder = {
    "voucherCode",
    "voucherStatus",
    "voucherType",
    "voucherValue",
    "initialValue",
    "consumedValue",
    "voucherBalanceDue",
    "expirationDate",
    "promoCode",
    "promoDescrition",
    "promoDoc",
    "minQuantity",
    "minAmount",
    //"validProduct",
    //"validRefuelMode",
    "validPV",
    "isCombinable",
    "promoPartner"
    
})
public class Voucher {

    @XmlElement(required = true)
    protected String voucherCode;
    @XmlElement(required = true, nillable = true)
    protected String voucherStatus;
    @XmlElement(required = true, nillable = true)
    protected String voucherType;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal voucherValue;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal initialValue;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal consumedValue;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal voucherBalanceDue;
    @XmlElement(required = true, nillable = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar expirationDate;
    @XmlElement(required = true, nillable = true)
    protected String promoCode;
    @XmlElement(required = true, nillable = true)
    protected String promoDescrition;
    @XmlElement(required = true, nillable = true)
    protected String promoDoc;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal minQuantity;
    @XmlElement(required = true, nillable = true)
    protected BigDecimal minAmount;
    /*@XmlElement(required = true, nillable = true)
    protected List<Product> validProduct;
    @XmlElement(required = true, nillable = true)
    protected List<RefuelMode> validRefuelMode;*/
    @XmlElement(required = true, nillable = true)
    protected String validPV;
    @XmlElement(required = true, nillable = true)
    protected String isCombinable;
    @XmlElement(required = true, nillable = true)
    protected String promoPartner;
    

    /**
     * Gets the value of the voucherCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     * Sets the value of the voucherCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherCode(String value) {
        this.voucherCode = value;
    }

    /**
     * Gets the value of the voucherStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherStatus() {
        return voucherStatus;
    }

    /**
     * Sets the value of the voucherStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherStatus(String value) {
        this.voucherStatus = value;
    }

    /**
     * Gets the value of the voucherType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherType() {
        return voucherType;
    }

    /**
     * Sets the value of the voucherType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherType(String value) {
        this.voucherType = value;
    }

    /**
     * Gets the value of the voucherValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVoucherValue() {
        return voucherValue;
    }

    /**
     * Sets the value of the voucherValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVoucherValue(BigDecimal value) {
        this.voucherValue = value;
    }

    /**
     * Gets the value of the initialValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getInitialValue() {
        return initialValue;
    }

    /**
     * Sets the value of the initialValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setInitialValue(BigDecimal value) {
        this.initialValue = value;
    }

    /**
     * Gets the value of the consumedValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getConsumedValue() {
        return consumedValue;
    }

    /**
     * Sets the value of the consumedValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setConsumedValue(BigDecimal value) {
        this.consumedValue = value;
    }

    /**
     * Gets the value of the voucherBalanceDue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getVoucherBalanceDue() {
        return voucherBalanceDue;
    }

    /**
     * Sets the value of the voucherBalanceDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setVoucherBalanceDue(BigDecimal value) {
        this.voucherBalanceDue = value;
    }

    /**
     * Gets the value of the expirationDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExpirationDate() {
        return expirationDate;
    }

    /**
     * Sets the value of the expirationDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExpirationDate(XMLGregorianCalendar value) {
        this.expirationDate = value;
    }

    /**
     * Gets the value of the promoCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoCode() {
        return promoCode;
    }

    /**
     * Sets the value of the promoCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoCode(String value) {
        this.promoCode = value;
    }

    /**
     * Gets the value of the promoDescrition property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoDescrition() {
        return promoDescrition;
    }

    /**
     * Sets the value of the promoDescrition property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoDescrition(String value) {
        this.promoDescrition = value;
    }

    /**
     * Gets the value of the promoDoc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoDoc() {
        return promoDoc;
    }

    /**
     * Sets the value of the promoDoc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoDoc(String value) {
        this.promoDoc = value;
    }

    /**
     * Gets the value of the minQuantity property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinQuantity() {
        return minQuantity;
    }

    /**
     * Sets the value of the minQuantity property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinQuantity(BigDecimal value) {
        this.minQuantity = value;
    }

    /**
     * Gets the value of the minAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMinAmount() {
        return minAmount;
    }

    /**
     * Sets the value of the minAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMinAmount(BigDecimal value) {
        this.minAmount = value;
    }
    
    /**
     * Gets the value of the validProduct property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucherList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValidProduct().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Product }
     * 
     * 
     */
    /*public List<Product> getValidProduct() {
        if (validProduct == null) {
            validProduct = new ArrayList<Product>();
        }
        return this.validProduct;
    }*/
    
    /**
     * Gets the value of the validPV property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidPV() {
        return validPV;
    }

    /**
     * Sets the value of the validPV property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidPV(String value) {
        this.validPV = value;
    }
    
    /**
     * Gets the value of the validRefuelMode property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucherList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValidRefuelMode().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RefuelMode }
     * 
     * 
     */
    /*public List<RefuelMode> getValidRefuelMode() {
        if (validRefuelMode == null) {
            validRefuelMode = new ArrayList<RefuelMode>();
        }
        return this.validRefuelMode;
    }*/
 
    /**
     * Gets the value of the isCombinable property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsCombinable() {
        return isCombinable;
    }

    /**
     * Sets the value of the isCombinable property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsCombinable(String value) {
        this.isCombinable = value;
    }

    /**
     * Gets the value of the promoPartner property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoPartner() {
        return promoPartner;
    }

    /**
     * Sets the value of the promoPartner property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoPartner(String value) {
        this.promoPartner = value;
    }
    
}
