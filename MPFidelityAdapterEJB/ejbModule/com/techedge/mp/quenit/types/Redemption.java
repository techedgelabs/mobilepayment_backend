
package com.techedge.mp.quenit.types;

import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for card complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="redemption">
 *   &lt;complexContent>
 *     &lt;sequence>
 *       &lt;element name="redemptionCode" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *       &lt;element name="redemptionName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;element name="redemptionAmount">
 *         &lt;simpleType>
 *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *             &lt;fractionDigits value="2"/>
 *           &lt;/restriction>
 *         &lt;/simpleType>
 *       &lt;/element>
 *       &lt;element name="redemptionPoints" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *     &lt;/sequence>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "redemption", propOrder = {
    "redemptionCode",
    "redemptionName",
    "redemptionAmount",
    "redemptionPoints"
})
public class Redemption {

    @XmlElement(required = true)
    protected BigInteger redemptionCode;
    @XmlElement(required = true)
    protected String redemptionName;
    @XmlElement(required = true)
    protected BigDecimal redemptionAmount;
    @XmlElement(required = true)
    protected BigInteger redemptionPoints;
    /**
     * Gets the value of the redemptionCode property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRedemptionCode() {
        return redemptionCode;
    }

    /**
     * Sets the value of the redemptionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRedemptionCode(BigInteger value) {
        this.redemptionCode = value;
    }

    /**
     * Gets the value of the redemptionName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRedemptionName() {
        return redemptionName;
    }

    /**
     * Sets the value of the redemptionName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRedemptionName(String value) {
        this.redemptionName = value;
    }

    /**
     * Gets the value of the redemptionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRedemptionAmount() {
        return redemptionAmount;
    }

    /**
     * Sets the value of the redemptionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRedemptionAmount(BigDecimal value) {
        this.redemptionAmount = value;
    }

    /**
     * Gets the value of the redemptionPoints property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRedemptionPoints() {
        return redemptionPoints;
    }

    /**
     * Sets the value of the redemptionPoints property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRedemptionPoints(BigInteger value) {
        this.redemptionPoints = value;
    }
    
}
