package com.techedge.mp.quenit.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.techedge.mp.quenit.elements.AuthorizationPlusRequest;
import com.techedge.mp.quenit.elements.AuthorizationPlusRequestResponse;
import com.techedge.mp.quenit.exception.EniWsException;

public class EniWsAuthorizationPlusImpl extends EniWsAbstract {

    /*
     * private static ENIWsDWHImpl instance = null;
     * 
     * private ENIWsDWHImpl() {
     * // Exists only to defeat instantiation.
     * }
     * 
     * public static ENIWsDWHImpl getInstance() {
     * if (instance == null) {
     * instance = new ENIWsDWHImpl();
     * }
     * return instance;
     * }
     */

    public EniWsAuthorizationPlusImpl(String consumerKey, String consumerSecret, String requestURL, String proxyHost, String proxyPort, String proxyNoHosts, String keyStore, String keyStorePassword, String keyPassword, boolean debugSOAP, boolean debugSSL) {
        super(consumerKey, consumerSecret, requestURL, proxyHost, proxyPort, proxyNoHosts, keyStore, keyStorePassword, keyPassword, debugSOAP, debugSSL);
    }

    public EniWsAuthorizationPlusImpl(String consumerKey, String consumerSecret, String requestURL, String keyStore, String keyStorePassword) {
        super(consumerKey, consumerSecret, requestURL, keyStore, keyStorePassword);
    }

    public AuthorizationPlusRequestResponse authorizationPlus(AuthorizationPlusRequest authorizationPlusRequest) throws EniWsException {
        return (AuthorizationPlusRequestResponse) sendSoapRequest(authorizationPlusRequest, AuthorizationPlusRequestResponse.class);
    }

    protected String marshalSoapRequest(Object request) throws JAXBException, ParserConfigurationException, SOAPException, IOException {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Marshaller marshaller = JAXBContext.newInstance(request.getClass()).createMarshaller();
        marshaller.marshal(request, document);
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
        //soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ser", "http://servizi.infogroup.it");
        //soapMessage.getSOAPBody().addNamespaceDeclaration("ser", "http://servizi.infogroup.it");
        soapMessage.getSOAPBody().addDocument(document);
        //((SOAPElement) soapMessage.getSOAPBody().getFirstChild()).addNamespaceDeclaration("ser", "http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it");
        /*
        NodeList nodeList = soapMessage.getSOAPBody().getElementsByTagName("*");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getPrefix() == null) {
                ((SOAPElement) node).setElementQName(new QName("http://servizi.infogroup.it", node.getNodeName(), "ser"));
            }

        }
        */

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        soapMessage.writeTo(outputStream);
        String output = new String(outputStream.toByteArray());
        return output;
    }

    @SuppressWarnings("rawtypes")
    protected Object unmarshalSoapRequest(String soapResponseString, Class soapResponseClass) throws JAXBException, SOAPException, IOException {
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage(new MimeHeaders(), new ByteArrayInputStream(soapResponseString.getBytes(Charset.forName("UTF-8"))));
        //soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ns2", "http://servizi.infogroup.it");
        //soapMessage.getSOAPBody().addNamespaceDeclaration("ns2", "http://servizi.infogroup.it");
        //soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ns2", "http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it");
        //((SOAPElement) soapMessage.getSOAPBody().getFirstChild()).addNamespaceDeclaration("ns2", "http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it");
        JAXBContext jaxbContext = JAXBContext.newInstance(soapResponseClass);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return jaxbUnmarshaller.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument());
    }

}
