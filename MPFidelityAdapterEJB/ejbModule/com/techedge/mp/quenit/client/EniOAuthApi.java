package com.techedge.mp.quenit.client;

import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.Token;
import org.scribe.services.SignatureService;
import org.scribe.services.TimestampService;

import com.techedge.mp.quenit.service.EniOAuthApiTimestampService;
import com.techedge.mp.quenit.service.HMACSha256SignatureService;

public class EniOAuthApi extends DefaultApi10a {
    private static String NOT_USED_STRING = "Not used since ENI CS supports 2-legged authentication";

    @Override
    public String getRequestTokenEndpoint() {
        return NOT_USED_STRING;
    }

    @Override
    public String getAccessTokenEndpoint() {
        return NOT_USED_STRING;
    }

    @Override
    public String getAuthorizationUrl(Token requestToken) {
        return NOT_USED_STRING;
    }

    @Override
    public SignatureService getSignatureService() {
        return new HMACSha256SignatureService();
    }

    @Override
    public TimestampService getTimestampService() {
        return new EniOAuthApiTimestampService();
    }

}
