package com.techedge.mp.quenit.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.SOAPException;

import com.techedge.mp.quenit.exception.EniWsException;
import com.techedge.mp.quenit.model.EniResponse;
import com.techedge.mp.quenit.service.EniOAuthService;

public abstract class EniWsAbstract {

    private EniOAuthService eniService;

    /*
     * private static ENIWsDWHImpl instance = null;
     * 
     * private ENIWsDWHImpl() {
     * // Exists only to defeat instantiation.
     * }
     * 
     * public static ENIWsDWHImpl getInstance() {
     * if (instance == null) {
     * instance = new ENIWsDWHImpl();
     * }
     * return instance;
     * }
     */

    public EniWsAbstract(String consumerKey, String consumerSecret, String requestURL, String proxyHost, String proxyPort, String proxyNoHosts, String keyStore, String keyStorePassword, String keyPassword, boolean debugSOAP, boolean debugSSL) {
        eniService = new EniOAuthService(consumerKey, consumerSecret, requestURL, keyStore, keyStorePassword, keyPassword);
        eniService.setProxy(proxyHost, proxyPort, proxyNoHosts);
        eniService.setDebug(debugSOAP, debugSSL);
    }

    public EniWsAbstract(String consumerKey, String consumerSecret, String requestURL, String keyStore, String keyStorePassword) {
        this(consumerKey, consumerSecret, requestURL, null, null, null, keyStore, keyStorePassword, keyStorePassword, false, false);
    }

    @SuppressWarnings("rawtypes")
    protected Object sendSoapRequest(Object soapRequest, Class soapResponse) throws EniWsException {
        try {
            printDebug("=========== SOAP REQUEST ===========");
            String soapEnvelope = marshalSoapRequest(soapRequest);
            printDebug(soapEnvelope.replaceAll("/>", "/>\n"));
            printDebug("=========== END ===========");
            EniResponse response = eniService.execute(soapEnvelope);
            printDebug("=========== SOAP RESPONSE ===========");
            String soapResponseContent = readContentFromResponse(response);
            printDebug(soapResponseContent.replaceAll("/>", "/>\n"));
            printDebug("=========== END ===========");
            /*
             * soapResponseContent = "<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\">"
             * + "<SOAP-ENV:Header/><SOAP-ENV:Body>"
             * + "<ns2:getLoyaltyCardsListResponse xmlns:ns2=\"http://servizi.infogroup.it\">"
             * + "<ns2:csTransactionID>MP-1F95-00000000000017438582</ns2:csTransactionID>"
             * + "<ns2:statusCode>00</ns2:statusCode><ns2:messageCode>TRANSAZIONE ESEGUITA</ns2:messageCode>"
             * + "<ns2:balance>0</ns2:balance><ns2:cardsList><ns2:panCode>5362000047703853366</ns2:panCode>"
             * + "<ns2:eanCode>0477038533662</ns2:eanCode></ns2:cardsList></ns2:getLoyaltyCardsListResponse>"
             * + "</SOAP-ENV:Body></SOAP-ENV:Envelope>";
             * printDebug("SOAP Content:"+soapResponseContent);
             */
            return unmarshalSoapRequest(soapResponseContent, soapResponse);
        }
        catch (Exception ex) {
            //System.out.println(ex.getMessage());
            throw new EniWsException(ex.getMessage(), ex);
        }
    }

    protected String readContentFromResponse(EniResponse response) throws IOException {
        String content = "";
        StringBuilder buffer = new StringBuilder();
        InputStream in = response.getStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        String line;
        while ((line = reader.readLine()) != null) {
            buffer.append(line);
        }

        in.close();
        // reqBytes = buffer.toString().getBytes();
        content = buffer.toString();
        return content;
    }

    protected abstract String marshalSoapRequest(Object request) throws JAXBException, ParserConfigurationException, SOAPException, IOException;

    /*
     * private String marshalSoapRequest(Object request) throws JAXBException, ParserConfigurationException, SOAPException, IOException {
     * Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
     * Marshaller marshaller = JAXBContext.newInstance(request.getClass()).createMarshaller();
     * marshaller.marshal(request, document);
     * SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
     * //soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ser", "http://servizi.infogroup.it");
     * //soapMessage.getSOAPBody().addNamespaceDeclaration("ser", "http://servizi.infogroup.it");
     * soapMessage.getSOAPBody().addDocument(document);
     * //((SOAPElement) soapMessage.getSOAPBody().getFirstChild()).addNamespaceDeclaration("ser", "http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it");
     * NodeList nodeList = soapMessage.getSOAPBody().getElementsByTagName("*");
     * for (int i = 0; i < nodeList.getLength(); i++) {
     * Node node = nodeList.item(i);
     * if (node.getPrefix() == null) {
     * ((SOAPElement) node).setElementQName(new QName("http://servizi.infogroup.it", node.getNodeName(), "ser"));
     * }
     * 
     * }
     * 
     * ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
     * soapMessage.writeTo(outputStream);
     * String output = new String(outputStream.toByteArray());
     * return output;
     * }
     */

    protected abstract Object unmarshalSoapRequest(String soapResponseString, Class soapResponseClass) throws JAXBException, SOAPException, IOException;

    /*
     * @SuppressWarnings("rawtypes")
     * private Object unmarshalSoapRequest(String soapResponseString, Class soapResponseClass) throws JAXBException, SOAPException, IOException {
     * SOAPMessage soapMessage = MessageFactory.newInstance().createMessage(new MimeHeaders(), new ByteArrayInputStream(soapResponseString.getBytes(Charset.forName("UTF-8"))));
     * soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ns2", "http://servizi.infogroup.it");
     * soapMessage.getSOAPBody().addNamespaceDeclaration("ns2", "http://servizi.infogroup.it");
     * //soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ns2", "http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it");
     * //((SOAPElement) soapMessage.getSOAPBody().getFirstChild()).addNamespaceDeclaration("ns2", "http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it");
     * JAXBContext jaxbContext = JAXBContext.newInstance(soapResponseClass);
     * Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
     * return jaxbUnmarshaller.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument());
     * }
     */

    public String marshalResponse(Object response) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(response.getClass());
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        StringWriter sw = new StringWriter();
        jaxbMarshaller.marshal(response, sw);
        return sw.toString();

    }

    protected void printDebug(String debug) {
        if (eniService.inDebug()) {
            System.out.println(debug);
        }
    }

}
