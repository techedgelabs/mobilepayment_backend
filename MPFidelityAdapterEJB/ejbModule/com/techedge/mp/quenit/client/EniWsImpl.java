package com.techedge.mp.quenit.client;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.techedge.mp.quenit.elements.CancelPreAuthorizationConsumeVoucherRequest;
import com.techedge.mp.quenit.elements.CancelPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.quenit.elements.CheckConsumeVoucherTransactionRequest;
import com.techedge.mp.quenit.elements.CheckConsumeVoucherTransactionResponse;
import com.techedge.mp.quenit.elements.CheckLoadLoyaltyCreditsTransactionRequest;
import com.techedge.mp.quenit.elements.CheckLoadLoyaltyCreditsTransactionResponse;
import com.techedge.mp.quenit.elements.CheckVoucherRequest;
import com.techedge.mp.quenit.elements.CheckVoucherResponse;
import com.techedge.mp.quenit.elements.ConsumeVoucherRequest;
import com.techedge.mp.quenit.elements.ConsumeVoucherResponse;
import com.techedge.mp.quenit.elements.CreateVoucherPromotionalRequest;
import com.techedge.mp.quenit.elements.CreateVoucherPromotionalResponse;
import com.techedge.mp.quenit.elements.CreateVoucherRequest;
import com.techedge.mp.quenit.elements.CreateVoucherResponse;
import com.techedge.mp.quenit.elements.DeleteVoucherRequest;
import com.techedge.mp.quenit.elements.DeleteVoucherResponse;
import com.techedge.mp.quenit.elements.EnableLoyaltyCardRequest;
import com.techedge.mp.quenit.elements.EnableLoyaltyCardResponse;
import com.techedge.mp.quenit.elements.GetLoyaltyCardsListRequest;
import com.techedge.mp.quenit.elements.GetLoyaltyCardsListResponse;
import com.techedge.mp.quenit.elements.InfoRedemptionRequest;
import com.techedge.mp.quenit.elements.InfoRedemptionResponse;
import com.techedge.mp.quenit.elements.LoadLoyaltyCreditsRequest;
import com.techedge.mp.quenit.elements.LoadLoyaltyCreditsResponse;
import com.techedge.mp.quenit.elements.PreAuthorizationConsumeVoucherRequest;
import com.techedge.mp.quenit.elements.PreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.quenit.elements.RedemptionRequest;
import com.techedge.mp.quenit.elements.RedemptionResponse;
import com.techedge.mp.quenit.elements.ReverseConsumeVoucherTransactionRequest;
import com.techedge.mp.quenit.elements.ReverseConsumeVoucherTransactionResponse;
import com.techedge.mp.quenit.elements.ReverseLoadLoyaltyCreditsTransactionRequest;
import com.techedge.mp.quenit.elements.ReverseLoadLoyaltyCreditsTransactionResponse;
import com.techedge.mp.quenit.exception.EniWsException;

public class EniWsImpl extends EniWsAbstract {

    /*
     * private static ENIWsDWHImpl instance = null;
     * 
     * private ENIWsDWHImpl() {
     * // Exists only to defeat instantiation.
     * }
     * 
     * public static ENIWsDWHImpl getInstance() {
     * if (instance == null) {
     * instance = new ENIWsDWHImpl();
     * }
     * return instance;
     * }
     */

    public EniWsImpl(String consumerKey, String consumerSecret, String requestURL, String proxyHost, String proxyPort, String proxyNoHosts, String keyStore, String keyStorePassword, String keyPassword, boolean debugSOAP, boolean debugSSL) {
        super(consumerKey, consumerSecret, requestURL, proxyHost, proxyPort, proxyNoHosts, keyStore, keyStorePassword, keyPassword, debugSOAP, debugSSL);
    }

    public EniWsImpl(String consumerKey, String consumerSecret, String requestURL, String keyStore, String keyStorePassword) {
        super(consumerKey, consumerSecret, requestURL, keyStore, keyStorePassword);
    }

    public ReverseConsumeVoucherTransactionResponse reverseConsumeVoucherTransaction(ReverseConsumeVoucherTransactionRequest reverseConsumeVoucherTransactionRequest)
            throws EniWsException {
        return (ReverseConsumeVoucherTransactionResponse) sendSoapRequest(reverseConsumeVoucherTransactionRequest, ReverseConsumeVoucherTransactionResponse.class);
    }

    public GetLoyaltyCardsListResponse getLoyaltyCardsList(GetLoyaltyCardsListRequest getLoyaltyCardsListRequest) throws EniWsException {
        return (GetLoyaltyCardsListResponse) sendSoapRequest(getLoyaltyCardsListRequest, GetLoyaltyCardsListResponse.class);
    }

    public LoadLoyaltyCreditsResponse loadLoyaltyCredits(LoadLoyaltyCreditsRequest loadLoyaltyCreditsRequest) throws EniWsException {
        return (LoadLoyaltyCreditsResponse) sendSoapRequest(loadLoyaltyCreditsRequest, LoadLoyaltyCreditsResponse.class);
    }

    public ConsumeVoucherResponse consumeVoucher(ConsumeVoucherRequest consumeVoucherRequest) throws EniWsException {
        return (ConsumeVoucherResponse) sendSoapRequest(consumeVoucherRequest, ConsumeVoucherResponse.class);
    }

    public CheckVoucherResponse checkVoucher(CheckVoucherRequest checkVoucherRequest) throws EniWsException {
        return (CheckVoucherResponse) sendSoapRequest(checkVoucherRequest, CheckVoucherResponse.class);
    }

    public CheckLoadLoyaltyCreditsTransactionResponse checkLoadLoyaltyCreditsTransaction(CheckLoadLoyaltyCreditsTransactionRequest checkLoadLoyaltyCreditsTransactionRequest)
            throws EniWsException {
        return (CheckLoadLoyaltyCreditsTransactionResponse) sendSoapRequest(checkLoadLoyaltyCreditsTransactionRequest, CheckLoadLoyaltyCreditsTransactionResponse.class);
    }

    public ReverseLoadLoyaltyCreditsTransactionResponse reverseLoadLoyaltyCreditsTransaction(ReverseLoadLoyaltyCreditsTransactionRequest reverseLoadLoyaltyCreditsTransactionRequest)
            throws EniWsException {
        return (ReverseLoadLoyaltyCreditsTransactionResponse) sendSoapRequest(reverseLoadLoyaltyCreditsTransactionRequest, ReverseLoadLoyaltyCreditsTransactionResponse.class);
    }

    public CheckConsumeVoucherTransactionResponse checkConsumeVoucherTransaction(CheckConsumeVoucherTransactionRequest checkConsumeVoucherTransactionRequest) throws EniWsException {
        return (CheckConsumeVoucherTransactionResponse) sendSoapRequest(checkConsumeVoucherTransactionRequest, CheckConsumeVoucherTransactionResponse.class);
    }

    public EnableLoyaltyCardResponse enableLoyaltyCard(EnableLoyaltyCardRequest enableLoyaltyCardRequest) throws EniWsException {
        return (EnableLoyaltyCardResponse) sendSoapRequest(enableLoyaltyCardRequest, EnableLoyaltyCardResponse.class);
    }

    public PreAuthorizationConsumeVoucherResponse preAuthorizationConsumeVoucher(PreAuthorizationConsumeVoucherRequest preAuthorizationConsumeVoucherRequest) throws EniWsException {
        return (PreAuthorizationConsumeVoucherResponse) sendSoapRequest(preAuthorizationConsumeVoucherRequest, PreAuthorizationConsumeVoucherResponse.class);
    }

    public CancelPreAuthorizationConsumeVoucherResponse cancelPreAuthorizationConsumeVoucher(CancelPreAuthorizationConsumeVoucherRequest cancelPreAuthorizationConsumeVoucherRequest)
            throws EniWsException {
        return (CancelPreAuthorizationConsumeVoucherResponse) sendSoapRequest(cancelPreAuthorizationConsumeVoucherRequest, CancelPreAuthorizationConsumeVoucherResponse.class);
    }

    public CreateVoucherResponse createVoucher(CreateVoucherRequest createVoucherRequest) throws EniWsException {
        return (CreateVoucherResponse) sendSoapRequest(createVoucherRequest, CreateVoucherResponse.class);
    }

    public DeleteVoucherResponse deleteVoucher(DeleteVoucherRequest deleteVoucherRequest) throws EniWsException {
        return (DeleteVoucherResponse) sendSoapRequest(deleteVoucherRequest, DeleteVoucherResponse.class);
    }

    public InfoRedemptionResponse infoRedemption(InfoRedemptionRequest infoRedemptionRequest) throws EniWsException {
        return (InfoRedemptionResponse) sendSoapRequest(infoRedemptionRequest, InfoRedemptionResponse.class);
    }

    public RedemptionResponse redemption(RedemptionRequest redemptionRequest) throws EniWsException {
        return (RedemptionResponse) sendSoapRequest(redemptionRequest, RedemptionResponse.class);
    }

    public CreateVoucherPromotionalResponse createVoucherPromotional(CreateVoucherPromotionalRequest createVoucherPromotionalRequest) throws EniWsException {
        return (CreateVoucherPromotionalResponse) sendSoapRequest(createVoucherPromotionalRequest, CreateVoucherPromotionalResponse.class);
    }

    protected String marshalSoapRequest(Object request) throws JAXBException, ParserConfigurationException, SOAPException, IOException {
        Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
        Marshaller marshaller = JAXBContext.newInstance(request.getClass()).createMarshaller();
        marshaller.marshal(request, document);
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage();
        //soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ser", "http://servizi.infogroup.it");
        //soapMessage.getSOAPBody().addNamespaceDeclaration("ser", "http://servizi.infogroup.it");
        soapMessage.getSOAPBody().addDocument(document);
        //((SOAPElement) soapMessage.getSOAPBody().getFirstChild()).addNamespaceDeclaration("ser", "http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it");
        NodeList nodeList = soapMessage.getSOAPBody().getElementsByTagName("*");
        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getPrefix() == null) {
                ((SOAPElement) node).setElementQName(new QName("http://servizi.infogroup.it", node.getNodeName(), "ser"));
            }

        }

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        soapMessage.writeTo(outputStream);
        String output = new String(outputStream.toByteArray());
        return output;
    }

    @SuppressWarnings("rawtypes")
    protected Object unmarshalSoapRequest(String soapResponseString, Class soapResponseClass) throws JAXBException, SOAPException, IOException {
        SOAPMessage soapMessage = MessageFactory.newInstance().createMessage(new MimeHeaders(), new ByteArrayInputStream(soapResponseString.getBytes(Charset.forName("UTF-8"))));
        soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ns2", "http://servizi.infogroup.it");
        soapMessage.getSOAPBody().addNamespaceDeclaration("ns2", "http://servizi.infogroup.it");
        //soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ns2", "http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it");
        //((SOAPElement) soapMessage.getSOAPBody().getFirstChild()).addNamespaceDeclaration("ns2", "http://mcCardInfoAPI.services.multicardPaymentGatewayServer.it");
        JAXBContext jaxbContext = JAXBContext.newInstance(soapResponseClass);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return jaxbUnmarshaller.unmarshal(soapMessage.getSOAPBody().extractContentAsDocument());
    }

}
