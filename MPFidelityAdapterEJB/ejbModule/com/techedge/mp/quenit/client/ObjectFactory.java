package com.techedge.mp.quenit.client;

import javax.xml.bind.annotation.XmlRegistry;

import com.techedge.mp.quenit.elements.CheckConsumeVoucherTransactionRequest;
import com.techedge.mp.quenit.elements.CheckConsumeVoucherTransactionResponse;
import com.techedge.mp.quenit.elements.CheckLoadLoyaltyCreditsTransactionRequest;
import com.techedge.mp.quenit.elements.CheckLoadLoyaltyCreditsTransactionResponse;
import com.techedge.mp.quenit.elements.CheckTransactionRequest;
import com.techedge.mp.quenit.elements.CheckVoucherRequest;
import com.techedge.mp.quenit.elements.CheckVoucherResponse;
import com.techedge.mp.quenit.elements.ConsumeVoucherRequest;
import com.techedge.mp.quenit.elements.ConsumeVoucherResponse;
import com.techedge.mp.quenit.elements.EnableLoyaltyCardRequest;
import com.techedge.mp.quenit.elements.EnableLoyaltyCardResponse;
import com.techedge.mp.quenit.elements.GetLoyaltyCardsListRequest;
import com.techedge.mp.quenit.elements.GetLoyaltyCardsListResponse;
import com.techedge.mp.quenit.elements.LoadLoyaltyCreditsRequest;
import com.techedge.mp.quenit.elements.LoadLoyaltyCreditsResponse;
import com.techedge.mp.quenit.elements.LoyaltyApiRequestContext;
import com.techedge.mp.quenit.elements.LoyaltyApiResponseContext;
import com.techedge.mp.quenit.elements.LoyaltyCreditTransactionResponse;
import com.techedge.mp.quenit.elements.ReverseConsumeVoucherTransactionRequest;
import com.techedge.mp.quenit.elements.ReverseConsumeVoucherTransactionResponse;
import com.techedge.mp.quenit.elements.ReverseLoadLoyaltyCreditsTransactionRequest;
import com.techedge.mp.quenit.elements.ReverseLoadLoyaltyCreditsTransactionResponse;
import com.techedge.mp.quenit.elements.ReverseTransactionRequest;
import com.techedge.mp.quenit.elements.VoucherTransactionResponse;
import com.techedge.mp.quenit.types.Card;
import com.techedge.mp.quenit.types.Product;
import com.techedge.mp.quenit.types.Voucher;
import com.techedge.mp.quenit.types.VoucherCode;

/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.techedge.test package.
 * <p>
 * An ObjectFactory allows you to programatically construct new instances of the Java representation for XML content. The Java representation of XML content can consist of schema
 * derived interfaces and classes representing the binding of schema type definitions, element declarations and model groups. Factory methods for each of these are provided in this
 * class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.techedge.test
     * 
     */
    public ObjectFactory() {}

    /**
     * Create an instance of {@link LoadLoyaltyCreditsRequest }
     * 
     */
    public LoadLoyaltyCreditsRequest createLoadLoyaltyCreditsRequest() {
        return new LoadLoyaltyCreditsRequest();
    }

    /**
     * Create an instance of {@link LoyaltyApiRequestContext }
     * 
     */
    public LoyaltyApiRequestContext createLoyaltyApiRequestContext() {
        return new LoyaltyApiRequestContext();
    }

    /**
     * Create an instance of {@link Product }
     * 
     */
    public Product createProduct() {
        return new Product();
    }

    /**
     * Create an instance of {@link LoadLoyaltyCreditsResponse }
     * 
     */
    public LoadLoyaltyCreditsResponse createLoadLoyaltyCreditsResponse() {
        return new LoadLoyaltyCreditsResponse();
    }

    /**
     * Create an instance of {@link LoyaltyCreditTransactionResponse }
     * 
     */
    public LoyaltyCreditTransactionResponse createLoyaltyCreditTransactionResponse() {
        return new LoyaltyCreditTransactionResponse();
    }

    /**
     * Create an instance of {@link LoyaltyApiResponseContext }
     * 
     */
    public LoyaltyApiResponseContext createLoyaltyApiResponseContext() {
        return new LoyaltyApiResponseContext();
    }

    /**
     * Create an instance of {@link EnableLoyaltyCardRequest }
     * 
     */
    public EnableLoyaltyCardRequest createEnableLoyaltyCardRequest() {
        return new EnableLoyaltyCardRequest();
    }

    /**
     * Create an instance of {@link ConsumeVoucherRequest }
     * 
     */
    public ConsumeVoucherRequest createConsumeVoucherRequest() {
        return new ConsumeVoucherRequest();
    }

    /**
     * Create an instance of {@link VoucherCode }
     * 
     */
    public VoucherCode createVoucherCode() {
        return new VoucherCode();
    }

    /**
     * Create an instance of {@link EnableLoyaltyCardResponse }
     * 
     */
    public EnableLoyaltyCardResponse createEnableLoyaltyCardResponse() {
        return new EnableLoyaltyCardResponse();
    }

    /**
     * Create an instance of {@link CheckConsumeVoucherTransactionResponse }
     * 
     */
    public CheckConsumeVoucherTransactionResponse createCheckConsumeVoucherTransactionResponse() {
        return new CheckConsumeVoucherTransactionResponse();
    }

    /**
     * Create an instance of {@link VoucherTransactionResponse }
     * 
     */
    public VoucherTransactionResponse createVoucherTransactionResponse() {
        return new VoucherTransactionResponse();
    }

    /**
     * Create an instance of {@link Voucher }
     * 
     */
    public Voucher createVoucher() {
        return new Voucher();
    }

    /**
     * Create an instance of {@link CheckVoucherResponse }
     * 
     */
    public CheckVoucherResponse createCheckVoucherResponse() {
        return new CheckVoucherResponse();
    }

    /**
     * Create an instance of {@link ReverseLoadLoyaltyCreditsTransactionRequest }
     * 
     */
    public ReverseLoadLoyaltyCreditsTransactionRequest createReverseLoadLoyaltyCreditsTransactionRequest() {
        return new ReverseLoadLoyaltyCreditsTransactionRequest();
    }

    /**
     * Create an instance of {@link ReverseTransactionRequest }
     * 
     */
    public ReverseTransactionRequest createReverseTransactionRequest() {
        return new ReverseTransactionRequest();
    }

    /**
     * Create an instance of {@link CheckVoucherRequest }
     * 
     */
    public CheckVoucherRequest createCheckVoucherRequest() {
        return new CheckVoucherRequest();
    }

    /**
     * Create an instance of {@link CheckConsumeVoucherTransactionRequest }
     * 
     */
    public CheckConsumeVoucherTransactionRequest createCheckConsumeVoucherTransactionRequest() {
        return new CheckConsumeVoucherTransactionRequest();
    }

    /**
     * Create an instance of {@link CheckTransactionRequest }
     * 
     */
    public CheckTransactionRequest createCheckTransactionRequest() {
        return new CheckTransactionRequest();
    }

    /**
     * Create an instance of {@link ReverseConsumeVoucherTransactionResponse }
     * 
     */
    public ReverseConsumeVoucherTransactionResponse createReverseConsumeVoucherTransactionResponse() {
        return new ReverseConsumeVoucherTransactionResponse();
    }

    /**
     * Create an instance of {@link ConsumeVoucherResponse }
     * 
     */
    public ConsumeVoucherResponse createConsumeVoucherResponse() {
        return new ConsumeVoucherResponse();
    }

    /**
     * Create an instance of {@link GetLoyaltyCardsListResponse }
     * 
     */
    public GetLoyaltyCardsListResponse createGetLoyaltyCardsListResponse() {
        return new GetLoyaltyCardsListResponse();
    }

    /**
     * Create an instance of {@link Card }
     * 
     */
    public Card createCard() {
        return new Card();
    }

    /**
     * Create an instance of {@link CheckLoadLoyaltyCreditsTransactionResponse }
     * 
     */
    public CheckLoadLoyaltyCreditsTransactionResponse createCheckLoadLoyaltyCreditsTransactionResponse() {
        return new CheckLoadLoyaltyCreditsTransactionResponse();
    }

    /**
     * Create an instance of {@link CheckLoadLoyaltyCreditsTransactionRequest }
     * 
     */
    public CheckLoadLoyaltyCreditsTransactionRequest createCheckLoadLoyaltyCreditsTransactionRequest() {
        return new CheckLoadLoyaltyCreditsTransactionRequest();
    }

    /**
     * Create an instance of {@link ReverseLoadLoyaltyCreditsTransactionResponse }
     * 
     */
    public ReverseLoadLoyaltyCreditsTransactionResponse createReverseLoadLoyaltyCreditsTransactionResponse() {
        return new ReverseLoadLoyaltyCreditsTransactionResponse();
    }

    /**
     * Create an instance of {@link GetLoyaltyCardsListRequest }
     * 
     */
    public GetLoyaltyCardsListRequest createGetLoyaltyCardsListRequest() {
        return new GetLoyaltyCardsListRequest();
    }

    /**
     * Create an instance of {@link ReverseConsumeVoucherTransactionRequest }
     * 
     */
    public ReverseConsumeVoucherTransactionRequest createReverseConsumeVoucherTransactionRequest() {
        return new ReverseConsumeVoucherTransactionRequest();
    }

}
