package com.techedge.mp.fidelity.adapter.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.AuthorizationPlusResponsePojo;
import com.techedge.mp.fidelity.adapter.business.interfaces.AuthorizationPlusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.Customer;
import com.techedge.mp.fidelity.adapter.business.interfaces.Esito;
import com.techedge.mp.fidelity.adapter.business.interfaces.User;
import com.techedge.mp.quenit.client.EniWsAuthorizationPlusImpl;
import com.techedge.mp.quenit.elements.AuthorizationPlusRequest;
import com.techedge.mp.quenit.elements.AuthorizationPlusRequestPojo;
import com.techedge.mp.quenit.elements.AuthorizationPlusRequestResponse;


/**
 * Session Bean implementation class AuthorizationPlusService
 */
@Stateless
@LocalBean
public class AuthorizationPlusService implements AuthorizationPlusServiceRemote, AuthorizationPlusServiceLocal {

    private final static String        PARAM_MULTICARD_AUTHORIZATIONPLUS_WSDL = "MULTICARD_AUTHORIZATIONPLUS_WSDL";
    private final static String        PARAM_MULTICARD_OAUTH_CONSUMER_KEY     = "MULTICARD_OAUTH_CONSUMER_KEY";
    //private final static String     PARAM_MULTICARD_OAUTH_CONSUMER_SECRET = "MULTICARD_OAUTH_CONSUMER_SECRET";
    private final static String        PARAM_PROXY_HOST                       = "PROXY_HOST";
    private final static String        PARAM_PROXY_PORT                       = "PROXY_PORT";
    private final static String        PARAM_PROXY_NO_HOSTS                   = "PROXY_NO_HOSTS";

    private final static String        PARAM_KEYSTORE_PATH                    = "MULTICARD_KEYSTORE_PATH";
    private final static String        PARAM_KEYSTORE_PASSWORD                = "MULTICARD_KEYSTORE_PASSWORD";
    private final static String        PARAM_KEY_PASSWORD                     = "MULTICARD_KEY_PASSWORD";

    private final static String        PARAM_DEBUG_SOAP                       = "FIDELITY_DEBUG_SOAP";
    private final static String        PARAM_DEBUG_SSL                        = "FIDELITY_DEBUG_SSL";

    private final static String        PARAM_SIMULATION_ACTIVE                = "SIMULATION_ACTIVE";

    private ParametersServiceRemote    parametersService                      = null;
    private LoggerServiceRemote        loggerService                          = null;
    private EniWsAuthorizationPlusImpl eniWsAuthorizationPlusImpl             = null;
    private boolean                    simulationActive                       = false;
    
    public AuthorizationPlusService() {
        // TODO Auto-generated constructor stub
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    private EniWsAuthorizationPlusImpl getEniWsAuthorizationPlusImpl() throws Exception {

        if (this.eniWsAuthorizationPlusImpl == null) {

            try {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
                String wsdlString = parametersService.getParamValue(AuthorizationPlusService.PARAM_MULTICARD_AUTHORIZATIONPLUS_WSDL);
                String consumerKey = parametersService.getParamValue(AuthorizationPlusService.PARAM_MULTICARD_OAUTH_CONSUMER_KEY);
                String consumerSecret = "";
                String proxyHost = parametersService.getParamValue(AuthorizationPlusService.PARAM_PROXY_HOST);
                String proxyPort = parametersService.getParamValue(AuthorizationPlusService.PARAM_PROXY_PORT);
                String proxyNoHosts = parametersService.getParamValue(AuthorizationPlusService.PARAM_PROXY_NO_HOSTS);
                String keyStore = parametersService.getParamValue(AuthorizationPlusService.PARAM_KEYSTORE_PATH);
                String keyStorePasswordword = parametersService.getParamValue(AuthorizationPlusService.PARAM_KEYSTORE_PASSWORD);
                String keyPassword = parametersService.getParamValue(AuthorizationPlusService.PARAM_KEY_PASSWORD);
                boolean debugSOAP = false;
                boolean debugSSL = false;
                String debugSOAPString = parametersService.getParamValue(AuthorizationPlusService.PARAM_DEBUG_SOAP);
                String debugSSLString = parametersService.getParamValue(AuthorizationPlusService.PARAM_DEBUG_SSL);
                this.simulationActive = Boolean.parseBoolean(parametersService.getParamValue(AuthorizationPlusService.PARAM_SIMULATION_ACTIVE));

                if (debugSOAPString != null) {
                    debugSOAP = Boolean.parseBoolean(debugSOAPString);
                }

                if (debugSSLString != null) {
                    debugSSL = Boolean.parseBoolean(debugSSLString);
                }

                File fileCert = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "oauth" + File.separator + "authorizationplus.oauth");
                FileInputStream input = new FileInputStream(fileCert);

                InputStreamReader is = new InputStreamReader(input);
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);
                String read;

                while ((read = br.readLine()) != null) {
                    //System.out.println(read);
                    if (read.contains("BEGIN CERTIFICATE") || read.contains("END CERTIFICATE"))
                        continue;

                    sb.append(read);
                }

                br.close();
                consumerSecret = sb.toString();

                this.eniWsAuthorizationPlusImpl = new EniWsAuthorizationPlusImpl(consumerKey, consumerSecret, wsdlString, proxyHost, proxyPort, proxyNoHosts, keyStore, keyStorePasswordword, keyPassword, debugSOAP,
                        debugSSL);
            }
            catch (ParameterNotFoundException e) {

                e.printStackTrace();

                throw new Exception("Parameter for Eni Web Service not found: " + e.getMessage());
            }
            catch (InterfaceNotFoundException e) {

                e.printStackTrace();

                throw new Exception("InterfaceNotFoundException for jndi " + e.getJndiString());
            }
        }

        if (simulationActive) {
            System.out.println("Simulazione AuthorizationPlus Service attiva");
        }

        return this.eniWsAuthorizationPlusImpl;
    }
    
    @Override
    public AuthorizationPlusResult authorizationPlus(String accessToken) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("accessToken", accessToken));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "authorizationPlus", accessToken, "opening", ActivityLog.createLogMessage(inputParameters));

        AuthorizationPlusResult authorizationPlusResult = new AuthorizationPlusResult();
        AuthorizationPlusRequest authorizationPlusRequest = new AuthorizationPlusRequest();
        AuthorizationPlusRequestResponse authorizationPlusResponse = new AuthorizationPlusRequestResponse();

        AuthorizationPlusRequestPojo authorizationPlusRequestPojo = new AuthorizationPlusRequestPojo();
        authorizationPlusRequestPojo.setAccessToken(accessToken);
        
        authorizationPlusRequest.setAuthorizationPlus(authorizationPlusRequestPojo);
        
        try {
        	EniWsAuthorizationPlusImpl getEniWsAuthorizationPlusImpl = getEniWsAuthorizationPlusImpl();

            authorizationPlusResponse = getEniWsAuthorizationPlusImpl.authorizationPlus(authorizationPlusRequest); // authorizationPlus(authorizationPlusRequest);
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "authorizationPlus", accessToken, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }
        
        //Esito
        Esito esito = null;
        if (authorizationPlusResponse.getAuthorizationPlusResponse().getEsito() != null) {
            String esitoData = authorizationPlusResponse.getAuthorizationPlusResponse().getEsito().getEsito();
            String messaggioErrore = authorizationPlusResponse.getAuthorizationPlusResponse().getEsito().getMessaggioErrore();
            esito = new Esito();
            esito.setEsito(esitoData);
            esito.setMessaggioErrore(messaggioErrore);
        }
        else {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "authorizationPlus", accessToken, "closing", "Campo esito null");
            throw new FidelityServiceException("Campo esito null", null);
        }
        //esito.setEsito("OK");
        //esito.setMessaggioErrore("Nessun errore");
        
        //Customer
        Customer customer = null;
        if (authorizationPlusResponse.getAuthorizationPlusResponse().getCustomer() != null) {
            String businessName = authorizationPlusResponse.getAuthorizationPlusResponse().getCustomer().getBusinessName();
            String code = authorizationPlusResponse.getAuthorizationPlusResponse().getCustomer().getCode();
            String vatNumber = authorizationPlusResponse.getAuthorizationPlusResponse().getCustomer().getVatNumber();
            String fiscalCode = authorizationPlusResponse.getAuthorizationPlusResponse().getCustomer().getFiscalCode();
            customer = new Customer();
            customer.setBusinessName(businessName);
            customer.setCode(code);
            customer.setFiscalCode(fiscalCode);
            customer.setVatNumber(vatNumber);
        }
        
        //User
        User user = null;
        if (authorizationPlusResponse.getAuthorizationPlusResponse().getUser() != null) {
            String emailAddress = authorizationPlusResponse.getAuthorizationPlusResponse().getUser().getEmailAddress();
            String firstName = authorizationPlusResponse.getAuthorizationPlusResponse().getUser().getFirstName();
            String lastName = authorizationPlusResponse.getAuthorizationPlusResponse().getUser().getLastName();
            String telephoneNumber = authorizationPlusResponse.getAuthorizationPlusResponse().getUser().getTelephoneNumber();
            user = new User();
            user.setEmailAddress(emailAddress);
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setTelephoneNumber(telephoneNumber);
        }
        
        //user.setEmailAddress("mariorossi@aaaaa1.com");
        //user.setFirstName("Mario");
        //user.setLastName("Test");
        //user.setTelephoneNumber("3334444555");
        
        //AuthorizationPlusResponsePojo
        AuthorizationPlusResponsePojo response = new AuthorizationPlusResponsePojo();
        response.setCustomer(customer);
        response.setEsito(esito);
        response.setUser(user);
        
        //AuthorizationPlusResult
        authorizationPlusResult.setRequest(response);
        
        
        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();

        if (authorizationPlusResult.getRequest() != null) {
            if (authorizationPlusResult.getRequest().getUser() != null) {
                outputParameters.add(new Pair<String, String>("user.emailAddress", authorizationPlusResult.getRequest().getUser().getEmailAddress()));
                outputParameters.add(new Pair<String, String>("user.firstName", authorizationPlusResult.getRequest().getUser().getFirstName()));
                outputParameters.add(new Pair<String, String>("user.lastName", authorizationPlusResult.getRequest().getUser().getLastName()));
                outputParameters.add(new Pair<String, String>("user.telephoneNumber", authorizationPlusResult.getRequest().getUser().getTelephoneNumber()));
            }
            if (authorizationPlusResult.getRequest().getCustomer() != null) {
                outputParameters.add(new Pair<String, String>("customer.businessName", authorizationPlusResult.getRequest().getCustomer().getBusinessName()));
                outputParameters.add(new Pair<String, String>("customer.code", authorizationPlusResult.getRequest().getCustomer().getCode()));
                outputParameters.add(new Pair<String, String>("customer.fiscalCode", authorizationPlusResult.getRequest().getCustomer().getFiscalCode()));
                outputParameters.add(new Pair<String, String>("customer.vatNumber", authorizationPlusResult.getRequest().getCustomer().getVatNumber()));
            }
            if (authorizationPlusResult.getRequest().getEsito() != null) {
                outputParameters.add(new Pair<String, String>("esito.esito", authorizationPlusResult.getRequest().getEsito().getEsito()));
                outputParameters.add(new Pair<String, String>("esito.messaggioErrore", authorizationPlusResult.getRequest().getEsito().getMessaggioErrore()));
            }
        }
        else {
            outputParameters.add(new Pair<String, String>("result", "null"));
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "authorizationPlus", accessToken, "closing", ActivityLog.createLogMessage(outputParameters));

        return authorizationPlusResult;
    }

}
