package com.techedge.mp.fidelity.adapter.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteAuthorizationResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteCaptureResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecutePaymentResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteReversalResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetPaymentStatusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.KeyValueInfo;
import com.techedge.mp.fidelity.adapter.business.interfaces.McCardEnjoyAuthorizeResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PaymentMode;
import com.techedge.mp.fidelity.adapter.business.interfaces.PaymentRefuelMode;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductCodeEnum;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetailInfo;
import com.techedge.mp.quenit.client.EniWsPaymentImpl;
import com.techedge.mp.quenit.elements.EnumRefuelModeType;
import com.techedge.mp.quenit.elements.ExecuteAuthorizationRequest;
import com.techedge.mp.quenit.elements.ExecuteAuthorizationResponse;
import com.techedge.mp.quenit.elements.ExecuteCaptureRequest;
import com.techedge.mp.quenit.elements.ExecuteCaptureResponse;
import com.techedge.mp.quenit.elements.ExecutePaymentRequest;
import com.techedge.mp.quenit.elements.ExecutePaymentResponse;
import com.techedge.mp.quenit.elements.ExecuteReversalRequest;
import com.techedge.mp.quenit.elements.ExecuteReversalResponse;
import com.techedge.mp.quenit.elements.GetPaymentStatusRequest;
import com.techedge.mp.quenit.elements.GetPaymentStatusResponse;
import com.techedge.mp.quenit.elements.KeyValueEntityType;
import com.techedge.mp.quenit.elements.McCardEnjoyAuthorizeRequest;
import com.techedge.mp.quenit.elements.McCardEnjoyAuthorizeResponse;
import com.techedge.mp.quenit.elements.ProductDatailEntityType;
import com.techedge.mp.quenit.elements.RefuelInfoEntityType;
import com.techedge.mp.quenit.elements.ResultEntityType;
import com.techedge.mp.quenit.elements.Untype;

/**
 * Session Bean implementation class PaymentService
 */
@Stateless
@LocalBean
public class PaymentService implements PaymentServiceRemote, PaymentServiceLocal {

    private final static String     PARAM_MULTICARD_PAYMENT_WSDL       = "MULTICARD_PAYMENT_WSDL";
    private final static String     PARAM_MULTICARD_OAUTH_CONSUMER_KEY = "MULTICARD_OAUTH_CONSUMER_KEY";
    //private final static String     PARAM_MULTICARD_OAUTH_CONSUMER_SECRET = "MULTICARD_OAUTH_CONSUMER_SECRET";
    private final static String     PARAM_PROXY_HOST                   = "PROXY_HOST";
    private final static String     PARAM_PROXY_PORT                   = "PROXY_PORT";
    private final static String     PARAM_PROXY_NO_HOSTS               = "PROXY_NO_HOSTS";

    private final static String     PARAM_KEYSTORE_PATH                = "MULTICARD_KEYSTORE_PATH";
    private final static String     PARAM_KEYSTORE_PASSWORD            = "MULTICARD_KEYSTORE_PASSWORD";
    private final static String     PARAM_KEY_PASSWORD                 = "MULTICARD_KEY_PASSWORD";

    private final static String     PARAM_DEBUG_SOAP                   = "FIDELITY_DEBUG_SOAP";
    private final static String     PARAM_DEBUG_SSL                    = "FIDELITY_DEBUG_SSL";

    private final static String     PARAM_SIMULATION_ACTIVE            = "SIMULATION_ACTIVE";

    private ParametersServiceRemote parametersService                  = null;
    private LoggerServiceRemote     loggerService                      = null;
    private EniWsPaymentImpl        eniWsPaymentImpl                   = null;
    private boolean                 simulationActive                   = false;
    

    public PaymentService() {
        // TODO Auto-generated constructor stub
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    private EniWsPaymentImpl getEniWsPaymentImpl() throws Exception {

        if (this.eniWsPaymentImpl == null) {

            try {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
                String wsdlString = parametersService.getParamValue(PaymentService.PARAM_MULTICARD_PAYMENT_WSDL);
                String consumerKey = parametersService.getParamValue(PaymentService.PARAM_MULTICARD_OAUTH_CONSUMER_KEY);
                String consumerSecret = "";
                String proxyHost = parametersService.getParamValue(PaymentService.PARAM_PROXY_HOST);
                String proxyPort = parametersService.getParamValue(PaymentService.PARAM_PROXY_PORT);
                String proxyNoHosts = parametersService.getParamValue(PaymentService.PARAM_PROXY_NO_HOSTS);
                String keyStore = parametersService.getParamValue(PaymentService.PARAM_KEYSTORE_PATH);
                String keyStorePasswordword = parametersService.getParamValue(PaymentService.PARAM_KEYSTORE_PASSWORD);
                String keyPassword = parametersService.getParamValue(PaymentService.PARAM_KEY_PASSWORD);
                boolean debugSOAP = false;
                boolean debugSSL = false;
                String debugSOAPString = parametersService.getParamValue(PaymentService.PARAM_DEBUG_SOAP);
                String debugSSLString = parametersService.getParamValue(PaymentService.PARAM_DEBUG_SSL);
                this.simulationActive = Boolean.parseBoolean(parametersService.getParamValue(PaymentService.PARAM_SIMULATION_ACTIVE));

                if (debugSOAPString != null) {
                    debugSOAP = Boolean.parseBoolean(debugSOAPString);
                }

                if (debugSSLString != null) {
                    debugSSL = Boolean.parseBoolean(debugSSLString);
                }

                File fileCert = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "oauth" + File.separator + "multicard.oauth");
                FileInputStream input = new FileInputStream(fileCert);

                InputStreamReader is = new InputStreamReader(input);
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);
                String read;

                while ((read = br.readLine()) != null) {
                    //System.out.println(read);
                    if (read.contains("BEGIN CERTIFICATE") || read.contains("END CERTIFICATE"))
                        continue;

                    sb.append(read);
                }

                br.close();
                consumerSecret = sb.toString();

                this.eniWsPaymentImpl = new EniWsPaymentImpl(consumerKey, consumerSecret, wsdlString, proxyHost, proxyPort, proxyNoHosts, keyStore, keyStorePasswordword,
                        keyPassword, debugSOAP, debugSSL);
            }
            catch (ParameterNotFoundException e) {

                e.printStackTrace();

                throw new Exception("Parameter for Eni Web Service not found: " + e.getMessage());
            }
            catch (InterfaceNotFoundException e) {

                e.printStackTrace();

                throw new Exception("InterfaceNotFoundException for jndi " + e.getJndiString());
            }
        }

        if (simulationActive) {
            System.out.println("Simulazione Payment Service attiva");
        }

        return this.eniWsPaymentImpl;
    }

    @Override
    public ExecutePaymentResult executePayment(String operationID, Integer amount, ProductCodeEnum productCode, Integer quantity, Integer unitPrice, String authCryptogram, String currencyCode, String mcCardDpan, PaymentRefuelMode refuelMode,
            String shopCode, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("amount", String.valueOf(amount)));
        inputParameters.add(new Pair<String, String>("authCryptogram", authCryptogram));
        inputParameters.add(new Pair<String, String>("currencyCode", currencyCode));
        inputParameters.add(new Pair<String, String>("mcCardDPan", mcCardDpan));
        inputParameters.add(new Pair<String, String>("refuelMode", refuelMode.getValue()));
        inputParameters.add(new Pair<String, String>("shopCode", shopCode));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "executePayment", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        ExecutePaymentResult executePaymentResult = new ExecutePaymentResult();
        ExecutePaymentRequest executePaymentRequest = new ExecutePaymentRequest();
        ExecutePaymentResponse executePaymentResponse = new ExecutePaymentResponse();

        executePaymentRequest.setAmount(amount);
        executePaymentRequest.setAuthCryptogram(authCryptogram);
        executePaymentRequest.setCurrencyCode(currencyCode);
        executePaymentRequest.setMcCardDpan(mcCardDpan);
        executePaymentRequest.setOperationId(operationID);
        executePaymentRequest.setPartnerType(partnerType.getValue());
        executePaymentRequest.setRefuelMode(EnumRefuelModeType.fromValue(refuelMode.getValue()));
        executePaymentRequest.setRequestTimestamp(requestTimestamp);
        executePaymentRequest.setShopCode(shopCode);

        Untype untypeQuantity = new Untype();
        untypeQuantity.setDigit(3);
        untypeQuantity.setValue(quantity);
        
        Untype untypeUnitPrice = new Untype();
        untypeUnitPrice.setDigit(3);
        untypeUnitPrice.setValue(unitPrice);
        
        RefuelInfoEntityType refuelInfoEntityType = new RefuelInfoEntityType();
        refuelInfoEntityType.setCurrencyCode(currencyCode);
        refuelInfoEntityType.setProductCode(productCode.getValue());
        refuelInfoEntityType.setQuantity(untypeQuantity);
        refuelInfoEntityType.setTotAmnt(amount);
        refuelInfoEntityType.setUnitPrice(untypeUnitPrice);
        executePaymentRequest.getRefuelInfo().add(refuelInfoEntityType);
        
        try {
            EniWsPaymentImpl eniWsPaymentImpl = getEniWsPaymentImpl();

            executePaymentResponse = eniWsPaymentImpl.executePayment(executePaymentRequest);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "executePayment", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        String authCode = executePaymentResponse.getAuthCode();
        ResultEntityType result = executePaymentResponse.getResult();
        String retrievalRefNumber = executePaymentResponse.getRetrievalRefNumber();

        executePaymentResult.setAuthCode(authCode);
        executePaymentResult.setResult(result.toResultDetail());
        executePaymentResult.setRetrievalRefNumber(retrievalRefNumber);

        List<ProductDatailEntityType> fuelEnabledProductList = executePaymentResponse.getFuelEnabledProducts();
        if (fuelEnabledProductList != null && !fuelEnabledProductList.isEmpty()) {

            for (ProductDatailEntityType productDatailEntityType : fuelEnabledProductList) {

                ProductDetailInfo productDetailInfo = new ProductDetailInfo();
                productDetailInfo.setProductCode(productDatailEntityType.getProductCode());
                productDetailInfo.setProductDescription(productDatailEntityType.getProductDescription());
                executePaymentResult.getFuelEnabledProductList().add(productDetailInfo);
            }
        }

        List<KeyValueEntityType> receiptElementList = executePaymentResponse.getReceiptElements();
        if (receiptElementList != null && !receiptElementList.isEmpty()) {

            for (KeyValueEntityType KeyValueEntityType : receiptElementList) {

                KeyValueInfo keyValueInfo = new KeyValueInfo();
                keyValueInfo.setKey(KeyValueEntityType.getKey());
                keyValueInfo.setValue(KeyValueEntityType.getValue());
                executePaymentResult.getReceiptElementList().add(keyValueInfo);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("authCode", executePaymentResult.getAuthCode()));

        if (executePaymentResult.getResult() != null) {
            outputParameters.add(new Pair<String, String>("result.code", executePaymentResult.getResult().getCode().getValue()));
            outputParameters.add(new Pair<String, String>("result.message", executePaymentResult.getResult().getMessage()));
            outputParameters.add(new Pair<String, String>("result.value", executePaymentResult.getResult().getStatus().value()));
            outputParameters.add(new Pair<String, String>("result.transactionId", executePaymentResult.getResult().getTransactionId()));
        }
        else {
            outputParameters.add(new Pair<String, String>("result", "null"));
        }

        outputParameters.add(new Pair<String, String>("retrievalRefNumber", executePaymentResult.getRetrievalRefNumber()));

        String fuelEnabledProductListString = "";
        if (!executePaymentResult.getFuelEnabledProductList().isEmpty()) {

            for (ProductDetailInfo productDetailInfo : executePaymentResult.getFuelEnabledProductList()) {

                String productDetailInfoString = "{ " + "productCode: " + productDetailInfo.getProductCode() + "  productDescription: " + productDetailInfo.getProductDescription()
                        + " }";

                fuelEnabledProductListString = fuelEnabledProductListString + productDetailInfoString;
            }
        }
        outputParameters.add(new Pair<String, String>("fuelEnabledProductList", fuelEnabledProductListString));

        String receiptElementListString = "";
        if (!executePaymentResult.getReceiptElementList().isEmpty()) {

            for (KeyValueInfo keyValueInfo : executePaymentResult.getReceiptElementList()) {

                String productDetailInfoString = "{ " + "key: " + keyValueInfo.getKey() + "  value: " + keyValueInfo.getValue() + " }";

                receiptElementListString = receiptElementListString + productDetailInfoString;
            }
        }
        outputParameters.add(new Pair<String, String>("receiptElementList", receiptElementListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "executePayment", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return executePaymentResult;
    }
    
    
    @Override
    public ExecuteAuthorizationResult executeAuthorization(String operationID, Integer amount, String authCryptogram, String currencyCode, String mcCardDpan,
            String shopCode, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("amount", String.valueOf(amount)));
        inputParameters.add(new Pair<String, String>("authCryptogram", authCryptogram));
        inputParameters.add(new Pair<String, String>("currencyCode", currencyCode));
        inputParameters.add(new Pair<String, String>("mcCardDpan", mcCardDpan));
        inputParameters.add(new Pair<String, String>("shopCode", shopCode));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "executeAuthorization", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        ExecuteAuthorizationResult executeAuthorizationResult = new ExecuteAuthorizationResult();
        ExecuteAuthorizationRequest executeAuthorizationRequest = new ExecuteAuthorizationRequest();
        ExecuteAuthorizationResponse executeAuthorizationResponse = new ExecuteAuthorizationResponse();
        
        executeAuthorizationRequest.setAmount(amount);
        executeAuthorizationRequest.setAuthCryptogram(authCryptogram);
        executeAuthorizationRequest.setCurrencyCode(currencyCode);
        executeAuthorizationRequest.setMcCardDpan(mcCardDpan);
        executeAuthorizationRequest.setOperationId(operationID);
        executeAuthorizationRequest.setPartnerType(partnerType.getValue());
        executeAuthorizationRequest.setRequestTimestamp(requestTimestamp);
        executeAuthorizationRequest.setShopCode(shopCode);
        
        try {
            EniWsPaymentImpl eniWsPaymentImpl = getEniWsPaymentImpl();

            executeAuthorizationResponse = eniWsPaymentImpl.executeAuthorization(executeAuthorizationRequest);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "executeAuthorization", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }
        
        Integer amountAuthorized = executeAuthorizationResponse.getAmountAuthorized();
        String authCode = executeAuthorizationResponse.getAuthCode();
        ResultEntityType result = executeAuthorizationResponse.getResult();
        String retrievalRefNumber = executeAuthorizationResponse.getRetrievalRefNumber();

        executeAuthorizationResult.setAmountAuthorized(amountAuthorized);
        executeAuthorizationResult.setAuthCode(authCode);
        executeAuthorizationResult.setResult(result.toResultDetail());
        executeAuthorizationResult.setRetrievalRefNumber(retrievalRefNumber);

        List<ProductDatailEntityType> fuelEnabledProductList = executeAuthorizationResponse.getFuelEnabledProducts();
        if (fuelEnabledProductList != null && !fuelEnabledProductList.isEmpty()) {

            for (ProductDatailEntityType productDatailEntityType : fuelEnabledProductList) {

                ProductDetailInfo productDetailInfo = new ProductDetailInfo();
                productDetailInfo.setProductCode(productDatailEntityType.getProductCode());
                productDetailInfo.setProductDescription(productDatailEntityType.getProductDescription());
                executeAuthorizationResult.getFuelEnabledProductList().add(productDetailInfo);
            }
        }

        List<KeyValueEntityType> receiptElementList = executeAuthorizationResponse.getReceiptElements();
        if (receiptElementList != null && !receiptElementList.isEmpty()) {

            for (KeyValueEntityType KeyValueEntityType : receiptElementList) {

                KeyValueInfo keyValueInfo = new KeyValueInfo();
                keyValueInfo.setKey(KeyValueEntityType.getKey());
                keyValueInfo.setValue(KeyValueEntityType.getValue());
                executeAuthorizationResult.getReceiptElementList().add(keyValueInfo);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("authCode", executeAuthorizationResult.getAuthCode()));

        if (executeAuthorizationResult.getResult() != null) {
            outputParameters.add(new Pair<String, String>("result.code", executeAuthorizationResult.getResult().getCode().getValue()));
            outputParameters.add(new Pair<String, String>("result.message", executeAuthorizationResult.getResult().getMessage()));
            outputParameters.add(new Pair<String, String>("result.value", executeAuthorizationResult.getResult().getStatus().value()));
            outputParameters.add(new Pair<String, String>("result.transactionId", executeAuthorizationResult.getResult().getTransactionId()));
        }
        else {
            outputParameters.add(new Pair<String, String>("result", "null"));
        }

        outputParameters.add(new Pair<String, String>("retrievalRefNumber", executeAuthorizationResult.getRetrievalRefNumber()));

        String fuelEnabledProductListString = "";
        if (!executeAuthorizationResult.getFuelEnabledProductList().isEmpty()) {

            for (ProductDetailInfo productDetailInfo : executeAuthorizationResult.getFuelEnabledProductList()) {

                String productDetailInfoString = "{ " + "productCode: " + productDetailInfo.getProductCode() + "  productDescription: " + productDetailInfo.getProductDescription()
                        + " }";

                fuelEnabledProductListString = fuelEnabledProductListString + productDetailInfoString;
            }
        }
        outputParameters.add(new Pair<String, String>("fuelEnabledProductList", fuelEnabledProductListString));

        String receiptElementListString = "";
        if (!executeAuthorizationResult.getReceiptElementList().isEmpty()) {

            for (KeyValueInfo keyValueInfo : executeAuthorizationResult.getReceiptElementList()) {

                String productDetailInfoString = "{ " + "key: " + keyValueInfo.getKey() + "  value: " + keyValueInfo.getValue() + " }";

                receiptElementListString = receiptElementListString + productDetailInfoString;
            }
        }
        outputParameters.add(new Pair<String, String>("receiptElementList", receiptElementListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "executeAuthorization", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return executeAuthorizationResult;
    }

    @Override
    public ExecuteCaptureResult executeCapture( String mcCardDpan, String messageReasonCode, String retrievalRefNumber,  String authCode,  String currencyCode, PaymentRefuelMode refuelMode, String operationID, Integer amount, ProductCodeEnum productCode, Integer quantity, Integer unitPrice, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("amount", String.valueOf(amount)));
        inputParameters.add(new Pair<String, String>("retrievalRefNumber", retrievalRefNumber));
        inputParameters.add(new Pair<String, String>("authCode", authCode));
        inputParameters.add(new Pair<String, String>("currencyCode", currencyCode));
        inputParameters.add(new Pair<String, String>("mcCardDpan", mcCardDpan));
        if (refuelMode != null) {
            inputParameters.add(new Pair<String, String>("refuelMode", refuelMode.getValue()));
        }
        else {
            inputParameters.add(new Pair<String, String>("refuelMode", "null"));
        }
        if (partnerType != null) {
            inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        }
        else {
            inputParameters.add(new Pair<String, String>("partnerType", "null"));
        }
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        if (messageReasonCode != null) {
            inputParameters.add(new Pair<String, String>("messageReasonCode", messageReasonCode));
        }
        else {
            inputParameters.add(new Pair<String, String>("messageReasonCode", "null"));
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "executeCapure", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        ExecuteCaptureResult executeCaptureResult = new ExecuteCaptureResult();
        ExecuteCaptureRequest executeCaptureRequest = new ExecuteCaptureRequest();
        ExecuteCaptureResponse executeCaptureResponse = new ExecuteCaptureResponse();

        executeCaptureRequest.setAmount(amount);
        executeCaptureRequest.setCurrencyCode(currencyCode);
        executeCaptureRequest.setMcCardDpan(mcCardDpan);
        executeCaptureRequest.setOperationId(operationID);
        executeCaptureRequest.setPartnerType(partnerType.getValue());
        executeCaptureRequest.setRefuelMode(EnumRefuelModeType.fromValue(refuelMode.getValue()));
        executeCaptureRequest.setRequestTimestamp(requestTimestamp);
        executeCaptureRequest.setAuthCode(authCode);
        executeCaptureRequest.setRetrievalRefNumber(retrievalRefNumber);
        executeCaptureRequest.setMessageReasonCode(messageReasonCode);
        
        if (productCode != null) {
            Untype untypeQuantity = new Untype();
            untypeQuantity.setDigit(3);
            untypeQuantity.setValue(quantity);
            
            Untype untypeUnitPrice = new Untype();
            untypeUnitPrice.setDigit(3);
            untypeUnitPrice.setValue(unitPrice);
            
            RefuelInfoEntityType refuelInfoEntityType = new RefuelInfoEntityType();
            refuelInfoEntityType.setCurrencyCode(currencyCode);
            refuelInfoEntityType.setProductCode(productCode.getValue());
            refuelInfoEntityType.setQuantity(untypeQuantity);
            refuelInfoEntityType.setTotAmnt(amount);
            refuelInfoEntityType.setUnitPrice(untypeUnitPrice);
            executeCaptureRequest.getRefuelInfo().add(refuelInfoEntityType);
        }
        
        try {
            EniWsPaymentImpl eniWsPaymentImpl = getEniWsPaymentImpl();

            executeCaptureResponse = eniWsPaymentImpl.executeCapture(executeCaptureRequest);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "executeCapure", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        ResultEntityType result = executeCaptureResponse.getResult();
        String retrievalRefNumberResult = executeCaptureResponse.getRetrievalRefNumber();

        executeCaptureResult.setResult(result.toResultDetail());
        executeCaptureResult.setRetrievalRefNumber(retrievalRefNumberResult);


        List<KeyValueEntityType> receiptElementList = executeCaptureResponse.getReceiptElements();
        if (receiptElementList != null && !receiptElementList.isEmpty()) {

            for (KeyValueEntityType KeyValueEntityType : receiptElementList) {

                KeyValueInfo keyValueInfo = new KeyValueInfo();
                keyValueInfo.setKey(KeyValueEntityType.getKey());
                keyValueInfo.setValue(KeyValueEntityType.getValue());
                executeCaptureResult.getReceiptElementList().add(keyValueInfo);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();

        if (executeCaptureResult.getResult() != null) {
            outputParameters.add(new Pair<String, String>("result.code", executeCaptureResult.getResult().getCode().getValue()));
            outputParameters.add(new Pair<String, String>("result.message", executeCaptureResult.getResult().getMessage()));
            outputParameters.add(new Pair<String, String>("result.value", executeCaptureResult.getResult().getStatus().value()));
            outputParameters.add(new Pair<String, String>("result.transactionId", executeCaptureResult.getResult().getTransactionId()));
        }
        else {
            outputParameters.add(new Pair<String, String>("result", "null"));
        }

        outputParameters.add(new Pair<String, String>("retrievalRefNumber", executeCaptureResult.getRetrievalRefNumber()));

        String receiptElementListString = "";
        if (!executeCaptureResult.getReceiptElementList().isEmpty()) {

            for (KeyValueInfo keyValueInfo : executeCaptureResult.getReceiptElementList()) {

                String productDetailInfoString = "{ " + "key: " + keyValueInfo.getKey() + "  value: " + keyValueInfo.getValue() + " }";

                receiptElementListString = receiptElementListString + productDetailInfoString;
            }
        }
        outputParameters.add(new Pair<String, String>("receiptElementList", receiptElementListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "executeCapture", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return executeCaptureResult;
    }

    @Override
    public ExecuteReversalResult executeReversal( String mcCardDpan, String messageReasonCode, String retrievalRefNumber,  String authCode, String shopCode, String currencyCode, String operationID, Integer amount, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("amount", String.valueOf(amount)));
        inputParameters.add(new Pair<String, String>("retrievalRefNumber", retrievalRefNumber));
        inputParameters.add(new Pair<String, String>("authCode", authCode));
        inputParameters.add(new Pair<String, String>("currencyCode", currencyCode));
        inputParameters.add(new Pair<String, String>("shopCode", shopCode));
        inputParameters.add(new Pair<String, String>("mcCardDpan", mcCardDpan));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "executeReversal", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        ExecuteReversalResult executeReversalResult = new ExecuteReversalResult();
        ExecuteReversalRequest executeReversalRequest = new ExecuteReversalRequest();
        ExecuteReversalResponse executeReversalResponse = new ExecuteReversalResponse();

        executeReversalRequest.setAmount(amount);
        executeReversalRequest.setCurrencyCode(currencyCode);
        executeReversalRequest.setMcCardDpan(mcCardDpan);
        executeReversalRequest.setShopCode(shopCode);
        executeReversalRequest.setOperationId(operationID);
        executeReversalRequest.setPartnerType(partnerType.getValue());
        executeReversalRequest.setRequestTimestamp(requestTimestamp);
        executeReversalRequest.setAuthCode(authCode);
        executeReversalRequest.setRetrievalRefNumber(retrievalRefNumber);
        executeReversalRequest.setMessageReasonCode(messageReasonCode);

      
        try {
            EniWsPaymentImpl eniWsPaymentImpl = getEniWsPaymentImpl();

            executeReversalResponse = eniWsPaymentImpl.executeReversal(executeReversalRequest);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "executeReversal", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }
        
        ResultEntityType result = executeReversalResponse.getResult();

        executeReversalResult.setResult(result.toResultDetail());


        List<KeyValueEntityType> receiptElementList = executeReversalResponse.getReceiptElements();
        if (receiptElementList != null && !receiptElementList.isEmpty()) {

            for (KeyValueEntityType KeyValueEntityType : receiptElementList) {

                KeyValueInfo keyValueInfo = new KeyValueInfo();
                keyValueInfo.setKey(KeyValueEntityType.getKey());
                keyValueInfo.setValue(KeyValueEntityType.getValue());
                executeReversalResult.getReceiptElementList().add(keyValueInfo);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();

        if (executeReversalResult.getResult() != null) {
            outputParameters.add(new Pair<String, String>("result.code", executeReversalResult.getResult().getCode().getValue()));
            outputParameters.add(new Pair<String, String>("result.message", executeReversalResult.getResult().getMessage()));
            outputParameters.add(new Pair<String, String>("result.value", executeReversalResult.getResult().getStatus().value()));
            outputParameters.add(new Pair<String, String>("result.transactionId", executeReversalResult.getResult().getTransactionId()));
        }
        else {
            outputParameters.add(new Pair<String, String>("result", "null"));
        }


        String receiptElementListString = "";
        if (!executeReversalResult.getReceiptElementList().isEmpty()) {

            for (KeyValueInfo keyValueInfo : executeReversalResult.getReceiptElementList()) {

                String productDetailInfoString = "{ " + "key: " + keyValueInfo.getKey() + "  value: " + keyValueInfo.getValue() + " }";

                receiptElementListString = receiptElementListString + productDetailInfoString;
            }
        }
        outputParameters.add(new Pair<String, String>("receiptElementList", receiptElementListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "executeReversal", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return executeReversalResult;
    }
    
    @Override
    public GetPaymentStatusResult getPaymentStatus(String paymentOperationId, String operationID, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("paymentOperationId", paymentOperationId));
        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPaymentStatus", paymentOperationId, "opening", ActivityLog.createLogMessage(inputParameters));

        GetPaymentStatusResult getPaymentStatusResult = new GetPaymentStatusResult();
        GetPaymentStatusRequest getPaymentStatusRequest = new GetPaymentStatusRequest();
        GetPaymentStatusResponse getPaymentStatusResponse = new GetPaymentStatusResponse();

        getPaymentStatusRequest.setOperationId(operationID);
        getPaymentStatusRequest.setPartnerType(partnerType.getValue());
        getPaymentStatusRequest.setRequestTimestamp(requestTimestamp);
        getPaymentStatusRequest.setPaymentOperationId(paymentOperationId);
      
        try {
            EniWsPaymentImpl eniWsPaymentImpl = getEniWsPaymentImpl();

            getPaymentStatusResponse = eniWsPaymentImpl.getPaymentStatus(getPaymentStatusRequest);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getPaymentStatus", paymentOperationId, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }
        ResultEntityType result = getPaymentStatusResponse.getResult();

        getPaymentStatusResult.setResult(result.toResultDetail());
        
        ExecuteAuthorizationResult executeAuthorizationResult = null;
        ExecuteCaptureResult executeCaptureResult = null;
        ExecutePaymentResult executePaymentResult = null;
        ExecuteReversalResult executeReversalResult = null;

        if (getPaymentStatusResponse.getExecuteAuthorizationResponse() != null) {
        	executeAuthorizationResult = new ExecuteAuthorizationResult();	
        	 Integer amountAuthorized = getPaymentStatusResponse.getExecuteAuthorizationResponse().getAmountAuthorized();
             String authCode = getPaymentStatusResponse.getExecuteAuthorizationResponse().getAuthCode();
             ResultEntityType resultAuth = getPaymentStatusResponse.getExecuteAuthorizationResponse().getResult();
             String retrievalRefNumber = getPaymentStatusResponse.getExecuteAuthorizationResponse().getRetrievalRefNumber();

             executeAuthorizationResult.setAmountAuthorized(amountAuthorized);
             executeAuthorizationResult.setAuthCode(authCode);
             executeAuthorizationResult.setResult(resultAuth.toResultDetail());
             executeAuthorizationResult.setRetrievalRefNumber(retrievalRefNumber);

             List<ProductDatailEntityType> fuelEnabledProductList = getPaymentStatusResponse.getExecuteAuthorizationResponse().getFuelEnabledProducts();
             if (fuelEnabledProductList != null && !fuelEnabledProductList.isEmpty()) {

                 for (ProductDatailEntityType productDatailEntityType : fuelEnabledProductList) {

                     ProductDetailInfo productDetailInfo = new ProductDetailInfo();
                     productDetailInfo.setProductCode(productDatailEntityType.getProductCode());
                     productDetailInfo.setProductDescription(productDatailEntityType.getProductDescription());
                     executeAuthorizationResult.getFuelEnabledProductList().add(productDetailInfo);
                 }
             }

             List<KeyValueEntityType> receiptElementList = getPaymentStatusResponse.getExecuteAuthorizationResponse().getReceiptElements();
             if (receiptElementList != null && !receiptElementList.isEmpty()) {

                 for (KeyValueEntityType KeyValueEntityType : receiptElementList) {

                     KeyValueInfo keyValueInfo = new KeyValueInfo();
                     keyValueInfo.setKey(KeyValueEntityType.getKey());
                     keyValueInfo.setValue(KeyValueEntityType.getValue());
                     executeAuthorizationResult.getReceiptElementList().add(keyValueInfo);
                 }
             }
             
             getPaymentStatusResult.setExecuteAuthorizationResult(executeAuthorizationResult);
        }
        
        if (getPaymentStatusResponse.getExecuteCaptureResponse() != null) {
        	executeCaptureResult = new ExecuteCaptureResult();	

            ResultEntityType resultCapture = getPaymentStatusResponse.getExecuteCaptureResponse().getResult();
            String retrievalRefNumberResult = getPaymentStatusResponse.getExecuteCaptureResponse().getRetrievalRefNumber();

            executeCaptureResult.setResult(resultCapture.toResultDetail());
            executeCaptureResult.setRetrievalRefNumber(retrievalRefNumberResult);


            List<KeyValueEntityType> receiptElementList = getPaymentStatusResponse.getExecuteCaptureResponse().getReceiptElements();
            if (receiptElementList != null && !receiptElementList.isEmpty()) {

                for (KeyValueEntityType KeyValueEntityType : receiptElementList) {

                    KeyValueInfo keyValueInfo = new KeyValueInfo();
                    keyValueInfo.setKey(KeyValueEntityType.getKey());
                    keyValueInfo.setValue(KeyValueEntityType.getValue());
                    executeCaptureResult.getReceiptElementList().add(keyValueInfo);
                }
            }
            getPaymentStatusResult.setExecuteCaptureResult(executeCaptureResult);
        }
        
        if (getPaymentStatusResponse.getExecutePaymentResponse() != null) {
        	executePaymentResult = new ExecutePaymentResult();	

            String authCode = getPaymentStatusResponse.getExecutePaymentResponse().getAuthCode();
            ResultEntityType resultPayment = getPaymentStatusResponse.getExecutePaymentResponse().getResult();
            String retrievalRefNumber = getPaymentStatusResponse.getExecutePaymentResponse().getRetrievalRefNumber();

            executePaymentResult.setAuthCode(authCode);
            executePaymentResult.setResult(resultPayment.toResultDetail());
            executePaymentResult.setRetrievalRefNumber(retrievalRefNumber);

            List<ProductDatailEntityType> fuelEnabledProductList = getPaymentStatusResponse.getExecutePaymentResponse().getFuelEnabledProducts();
            if (fuelEnabledProductList != null && !fuelEnabledProductList.isEmpty()) {

                for (ProductDatailEntityType productDatailEntityType : fuelEnabledProductList) {

                    ProductDetailInfo productDetailInfo = new ProductDetailInfo();
                    productDetailInfo.setProductCode(productDatailEntityType.getProductCode());
                    productDetailInfo.setProductDescription(productDatailEntityType.getProductDescription());
                    executePaymentResult.getFuelEnabledProductList().add(productDetailInfo);
                }
            }

            List<KeyValueEntityType> receiptElementList = getPaymentStatusResponse.getExecutePaymentResponse().getReceiptElements();
            if (receiptElementList != null && !receiptElementList.isEmpty()) {

                for (KeyValueEntityType KeyValueEntityType : receiptElementList) {

                    KeyValueInfo keyValueInfo = new KeyValueInfo();
                    keyValueInfo.setKey(KeyValueEntityType.getKey());
                    keyValueInfo.setValue(KeyValueEntityType.getValue());
                    executePaymentResult.getReceiptElementList().add(keyValueInfo);
                }
            }
            getPaymentStatusResult.setExecutePaymentResult(executePaymentResult);
        }

        if (getPaymentStatusResponse.getExecuteReversalResponse() != null) {
        	executeReversalResult = new ExecuteReversalResult();
        	ResultEntityType resultReversal = getPaymentStatusResponse.getExecuteReversalResponse().getResult();

            executeReversalResult.setResult(resultReversal.toResultDetail());


            List<KeyValueEntityType> receiptElementList = getPaymentStatusResponse.getExecuteReversalResponse().getReceiptElements();
            if (receiptElementList != null && !receiptElementList.isEmpty()) {

                for (KeyValueEntityType KeyValueEntityType : receiptElementList) {

                    KeyValueInfo keyValueInfo = new KeyValueInfo();
                    keyValueInfo.setKey(KeyValueEntityType.getKey());
                    keyValueInfo.setValue(KeyValueEntityType.getValue());
                    executeReversalResult.getReceiptElementList().add(keyValueInfo);
                }
            }
            getPaymentStatusResult.setExecuteReversalResult(executeReversalResult);
        }
        
       
        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();

        if (getPaymentStatusResult.getResult() != null) {
            outputParameters.add(new Pair<String, String>("result.code", getPaymentStatusResult.getResult().getCode().getValue()));
            outputParameters.add(new Pair<String, String>("result.message", getPaymentStatusResult.getResult().getMessage()));
            outputParameters.add(new Pair<String, String>("result.value", getPaymentStatusResult.getResult().getStatus().value()));
            outputParameters.add(new Pair<String, String>("result.transactionId", getPaymentStatusResult.getResult().getTransactionId()));
        }
        else {
            outputParameters.add(new Pair<String, String>("result", "null"));
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPaymentStatus", paymentOperationId, "closing", ActivityLog.createLogMessage(outputParameters));

        return getPaymentStatusResult;
    }
    
    @Override
    public McCardEnjoyAuthorizeResult mcCardEnjoyAuthorize(String operationID, Integer amount, String currencyCode, String mcCardDpan,
            PartnerType partnerType, PaymentMode paymentMode, Long requestTimestamp, String serverSerialNumber, String userId) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("amount", String.valueOf(amount)));
        inputParameters.add(new Pair<String, String>("currencyCode", currencyCode));
        inputParameters.add(new Pair<String, String>("mcCardDpan", mcCardDpan));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("paymentMode", paymentMode.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("serverSerialNumber", serverSerialNumber));
        inputParameters.add(new Pair<String, String>("userId", userId));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "mcCardEnjoyAuthorize", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        McCardEnjoyAuthorizeResult mcCardEnjoyAuthorizeResult = new McCardEnjoyAuthorizeResult();
        McCardEnjoyAuthorizeRequest mcCardEnjoyAuthorizeRequest = new McCardEnjoyAuthorizeRequest();
        McCardEnjoyAuthorizeResponse mcCardEnjoyAuthorizeResponse = new McCardEnjoyAuthorizeResponse();
        
        mcCardEnjoyAuthorizeRequest.setAmount(amount);
        mcCardEnjoyAuthorizeRequest.setCurrencyCode(currencyCode);
        mcCardEnjoyAuthorizeRequest.setMcCardDpan(mcCardDpan);
        mcCardEnjoyAuthorizeRequest.setOperationId(operationID);
        mcCardEnjoyAuthorizeRequest.setPartnerType(partnerType.getValue());
        mcCardEnjoyAuthorizeRequest.setPaymentMode(paymentMode.getValue());
        mcCardEnjoyAuthorizeRequest.setRequestTimestamp(requestTimestamp);
        mcCardEnjoyAuthorizeRequest.setServerSerialNumber(serverSerialNumber);
        mcCardEnjoyAuthorizeRequest.setUserId(userId);
        
        try {
            EniWsPaymentImpl eniWsPaymentImpl = getEniWsPaymentImpl();

            mcCardEnjoyAuthorizeResponse = eniWsPaymentImpl.mcCardEnjoyAuthorize(mcCardEnjoyAuthorizeRequest);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "executeAuthorization", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }
        
        String authCryptogram = mcCardEnjoyAuthorizeResponse.getAuthCryptogram();
        ResultEntityType result = mcCardEnjoyAuthorizeResponse.getResult();

        mcCardEnjoyAuthorizeResult.setResult(result.toResultDetail());
        mcCardEnjoyAuthorizeResult.setAuthCryptogram(authCryptogram);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("authCryptogram", mcCardEnjoyAuthorizeResult.getAuthCryptogram()));

        if (mcCardEnjoyAuthorizeResult.getResult() != null) {
            outputParameters.add(new Pair<String, String>("result.code", mcCardEnjoyAuthorizeResult.getResult().getCode().getValue()));
            outputParameters.add(new Pair<String, String>("result.message", mcCardEnjoyAuthorizeResult.getResult().getMessage()));
            outputParameters.add(new Pair<String, String>("result.value", mcCardEnjoyAuthorizeResult.getResult().getStatus().value()));
            outputParameters.add(new Pair<String, String>("result.transactionId", mcCardEnjoyAuthorizeResult.getResult().getTransactionId()));
        }
        else {
            outputParameters.add(new Pair<String, String>("result", "null"));
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "mcCardEnjoyAuthorize", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return mcCardEnjoyAuthorizeResult;
    }
    
}
