package com.techedge.mp.fidelity.adapter.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.DeleteMcCardRefuelingResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.DpanDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnumStatusType;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetMcCardStatusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.quenit.client.EniWsCardInfoImpl;
import com.techedge.mp.quenit.elements.DeleteMcCardRefuelingRequest;
import com.techedge.mp.quenit.elements.DeleteMcCardRefuelingResponse;
import com.techedge.mp.quenit.elements.DpanDetailEntityType;
import com.techedge.mp.quenit.elements.GetMcCardStatusRequest;
import com.techedge.mp.quenit.elements.GetMcCardStatusResponse;
import com.techedge.mp.quenit.elements.ResultEntityType;

/**
 * Session Bean implementation class CardInfoService
 */
@Stateless
@LocalBean
public class CardInfoService implements CardInfoServiceRemote, CardInfoServiceLocal {

    private final static String     PARAM_MULTICARD_CARDINFO_WSDL      = "MULTICARD_CARDINFO_WSDL";
    private final static String     PARAM_MULTICARD_OAUTH_CONSUMER_KEY = "MULTICARD_OAUTH_CONSUMER_KEY";
    //private final static String     PARAM_MULTICARD_OAUTH_CONSUMER_SECRET = "MULTICARD_OAUTH_CONSUMER_SECRET";
    private final static String     PARAM_PROXY_HOST                   = "PROXY_HOST";
    private final static String     PARAM_PROXY_PORT                   = "PROXY_PORT";
    private final static String     PARAM_PROXY_NO_HOSTS               = "PROXY_NO_HOSTS";

    private final static String     PARAM_KEYSTORE_PATH                = "MULTICARD_KEYSTORE_PATH";
    private final static String     PARAM_KEYSTORE_PASSWORD            = "MULTICARD_KEYSTORE_PASSWORD";
    private final static String     PARAM_KEY_PASSWORD                 = "MULTICARD_KEY_PASSWORD";

    private final static String     PARAM_DEBUG_SOAP                   = "FIDELITY_DEBUG_SOAP";
    private final static String     PARAM_DEBUG_SSL                    = "FIDELITY_DEBUG_SSL";

    private final static String     PARAM_SIMULATION_ACTIVE            = "SIMULATION_ACTIVE";

    private ParametersServiceRemote parametersService                  = null;
    private LoggerServiceRemote     loggerService                      = null;
    private EniWsCardInfoImpl       eniWsCardInfoImpl                  = null;
    private boolean                 simulationActive                   = false;

    public CardInfoService() {
        // TODO Auto-generated constructor stub
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    private EniWsCardInfoImpl getEniWsCardInfoImpl() throws Exception {

        if (this.eniWsCardInfoImpl == null) {

            try {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
                String wsdlString = parametersService.getParamValue(CardInfoService.PARAM_MULTICARD_CARDINFO_WSDL);
                String consumerKey = parametersService.getParamValue(CardInfoService.PARAM_MULTICARD_OAUTH_CONSUMER_KEY);
                String consumerSecret = "";
                String proxyHost = parametersService.getParamValue(CardInfoService.PARAM_PROXY_HOST);
                String proxyPort = parametersService.getParamValue(CardInfoService.PARAM_PROXY_PORT);
                String proxyNoHosts = parametersService.getParamValue(CardInfoService.PARAM_PROXY_NO_HOSTS);
                String keyStore = parametersService.getParamValue(CardInfoService.PARAM_KEYSTORE_PATH);
                String keyStorePasswordword = parametersService.getParamValue(CardInfoService.PARAM_KEYSTORE_PASSWORD);
                String keyPassword = parametersService.getParamValue(CardInfoService.PARAM_KEY_PASSWORD);
                boolean debugSOAP = false;
                boolean debugSSL = false;
                String debugSOAPString = parametersService.getParamValue(CardInfoService.PARAM_DEBUG_SOAP);
                String debugSSLString = parametersService.getParamValue(CardInfoService.PARAM_DEBUG_SSL);
                this.simulationActive = Boolean.parseBoolean(parametersService.getParamValue(CardInfoService.PARAM_SIMULATION_ACTIVE));

                if (debugSOAPString != null) {
                    debugSOAP = Boolean.parseBoolean(debugSOAPString);
                }

                if (debugSSLString != null) {
                    debugSSL = Boolean.parseBoolean(debugSSLString);
                }

                File fileCert = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "oauth" + File.separator + "multicard.oauth");
                FileInputStream input = new FileInputStream(fileCert);

                InputStreamReader is = new InputStreamReader(input);
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);
                String read;

                while ((read = br.readLine()) != null) {
                    //System.out.println(read);
                    if (read.contains("BEGIN CERTIFICATE") || read.contains("END CERTIFICATE"))
                        continue;

                    sb.append(read);
                }

                br.close();
                consumerSecret = sb.toString();

                this.eniWsCardInfoImpl = new EniWsCardInfoImpl(consumerKey, consumerSecret, wsdlString, proxyHost, proxyPort, proxyNoHosts, keyStore, keyStorePasswordword, keyPassword, debugSOAP,
                        debugSSL);
            }
            catch (ParameterNotFoundException e) {

                e.printStackTrace();

                throw new Exception("Parameter for Eni Web Service not found: " + e.getMessage());
            }
            catch (InterfaceNotFoundException e) {

                e.printStackTrace();

                throw new Exception("InterfaceNotFoundException for jndi " + e.getJndiString());
            }
        }

        if (simulationActive) {
            System.out.println("Simulazione CardInfo Service attiva");
        }

        return this.eniWsCardInfoImpl;
    }

    @Override
    public GetMcCardStatusResult getMcCardStatus(String operationID, String userId, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("userId", userId));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMcCardStatus", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetMcCardStatusResult getMcCardStatusResult = new GetMcCardStatusResult();
        GetMcCardStatusRequest getMcCardStatusRequest = new GetMcCardStatusRequest();
        GetMcCardStatusResponse getLoyaltyCardsListResponse = new GetMcCardStatusResponse();

        getMcCardStatusRequest.setOperationId(operationID);
        getMcCardStatusRequest.setUserId(userId);
        getMcCardStatusRequest.setPartnerType(partnerType.getValue());
        getMcCardStatusRequest.setRequestTimestamp(requestTimestamp);

        try {
            EniWsCardInfoImpl eniWsCardInfoImpl = getEniWsCardInfoImpl();

            getLoyaltyCardsListResponse = eniWsCardInfoImpl.getMcCardStatus(getMcCardStatusRequest);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getMcCardStatus", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        ResultEntityType resultEntityType = getLoyaltyCardsListResponse.getResult();
        String code = resultEntityType.getCode();
        String message = resultEntityType.getMessage();
        EnumStatusType status = resultEntityType.getStatus();
        String transactionId = resultEntityType.getTransactionId();

        getMcCardStatusResult.setCode(code);
        getMcCardStatusResult.setMessage(message);
        getMcCardStatusResult.setStatus(status.value());
        getMcCardStatusResult.setTransactionId(transactionId);

        List<DpanDetailEntityType> dpanDetailEntityTypeList = getLoyaltyCardsListResponse.getDpanLists();
        if (dpanDetailEntityTypeList != null && !dpanDetailEntityTypeList.isEmpty()) {

            for (DpanDetailEntityType dpanDetailEntityType : dpanDetailEntityTypeList) {

                DpanDetail dpanDetail = new DpanDetail();
                dpanDetail.setMcCardDpan(dpanDetailEntityType.getMcCardDpan());
                dpanDetail.setMcCardInfoRequired(dpanDetailEntityType.isMcCardInfoRequired());
                dpanDetail.setMcCardKmRequired(dpanDetailEntityType.isMcCardKmRequired());
                dpanDetail.setMcCardPan(dpanDetailEntityType.getMcCardPan());
                dpanDetail.setMcCardServiceCode(dpanDetailEntityType.getMcCardServiceCode());
                dpanDetail.setStatus(dpanDetailEntityType.getStatus().value());
                getMcCardStatusResult.getDpanDetailList().add(dpanDetail);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("transactionID", getMcCardStatusResult.getTransactionId()));
        outputParameters.add(new Pair<String, String>("code", getMcCardStatusResult.getCode()));
        outputParameters.add(new Pair<String, String>("message", getMcCardStatusResult.getMessage()));
        outputParameters.add(new Pair<String, String>("status", getMcCardStatusResult.getStatus()));

        String dpanListString = "";

        if (!getMcCardStatusResult.getDpanDetailList().isEmpty()) {

            for (DpanDetail dpanDetail : getMcCardStatusResult.getDpanDetailList()) {

                String dpanString = "{ " + "dpan: " + dpanDetail.getMcCardDpan() + "  infoRequired: " + dpanDetail.getMcCardInfoRequired() + "  kmRequired: "
                        + dpanDetail.getMcCardKmRequired() + "  pan: " + dpanDetail.getMcCardPan() + "  serviceCode: " + dpanDetail.getMcCardServiceCode() + "  status: "
                        + dpanDetail.getStatus() + " }";

                dpanListString = dpanListString + dpanString;
            }
        }

        outputParameters.add(new Pair<String, String>("dpanList", dpanListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMcCardStatus", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getMcCardStatusResult;
    }
    
    
    @Override
    public DeleteMcCardRefuelingResult deleteMcCardRefueling(String operationID, String userId, PartnerType partnerType, Long requestTimestamp, String mcCardDpan, String serverSerialNumber) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("userId", userId));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("mcCardDpan", mcCardDpan));
        inputParameters.add(new Pair<String, String>("serverSerialNumber", serverSerialNumber));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "deleteMcCardRefueling", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        DeleteMcCardRefuelingResult deleteMcCardRefuelingResult = new DeleteMcCardRefuelingResult();
        DeleteMcCardRefuelingRequest deleteMcCardRefuelingRequest = new DeleteMcCardRefuelingRequest();
        DeleteMcCardRefuelingResponse deleteMcCardRefuelingResponse = new DeleteMcCardRefuelingResponse();

        deleteMcCardRefuelingRequest.setOperationId(operationID);
        deleteMcCardRefuelingRequest.setUserId(userId);
        deleteMcCardRefuelingRequest.setPartnerType(partnerType.getValue());
        deleteMcCardRefuelingRequest.setRequestTimestamp(requestTimestamp);
        deleteMcCardRefuelingRequest.setMcCardDpan(mcCardDpan);
        deleteMcCardRefuelingRequest.setServerSerialNumber(serverSerialNumber);

        try {
            EniWsCardInfoImpl eniWsCardInfoImpl = getEniWsCardInfoImpl();

            deleteMcCardRefuelingResponse = eniWsCardInfoImpl.deleteMcCardRefueling(deleteMcCardRefuelingRequest);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "deleteMcCardRefueling", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        ResultEntityType resultEntityType = deleteMcCardRefuelingResponse.getResult();
        String code = resultEntityType.getCode();
        String message = resultEntityType.getMessage();
        EnumStatusType status = resultEntityType.getStatus();
        String transactionId = resultEntityType.getTransactionId();

        deleteMcCardRefuelingResult.setCode(code);
        deleteMcCardRefuelingResult.setMessage(message);
        deleteMcCardRefuelingResult.setStatus(status.value());
        deleteMcCardRefuelingResult.setTransactionId(transactionId);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("transactionID", deleteMcCardRefuelingResult.getTransactionId()));
        outputParameters.add(new Pair<String, String>("code", deleteMcCardRefuelingResult.getCode()));
        outputParameters.add(new Pair<String, String>("message", deleteMcCardRefuelingResult.getMessage()));
        outputParameters.add(new Pair<String, String>("status", deleteMcCardRefuelingResult.getStatus()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "deleteMcCardRefueling", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return deleteMcCardRefuelingResult;
    }
}
