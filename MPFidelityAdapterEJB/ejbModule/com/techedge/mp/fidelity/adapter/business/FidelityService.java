package com.techedge.mp.fidelity.adapter.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CancelPreAuthorizationConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CardDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.CategoryRedemptionDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CreateVoucherPromotionalResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CreateVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.DeleteVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnableLoyaltyCardResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetLoyaltyCardListResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.InfoRedemptionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.LoadLoyaltyCreditsResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PreAuthorizationConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductType;
import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.fidelity.adapter.business.utilities.AmountConverter;
import com.techedge.mp.quenit.client.EniWsImpl;
import com.techedge.mp.quenit.elements.CancelPreAuthorizationConsumeVoucherRequest;
import com.techedge.mp.quenit.elements.CancelPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.quenit.elements.CheckConsumeVoucherTransactionRequest;
import com.techedge.mp.quenit.elements.CheckConsumeVoucherTransactionResponse;
import com.techedge.mp.quenit.elements.CheckLoadLoyaltyCreditsTransactionRequest;
import com.techedge.mp.quenit.elements.CheckLoadLoyaltyCreditsTransactionResponse;
import com.techedge.mp.quenit.elements.CheckVoucherRequest;
import com.techedge.mp.quenit.elements.CheckVoucherResponse;
import com.techedge.mp.quenit.elements.ConsumeVoucherRequest;
import com.techedge.mp.quenit.elements.ConsumeVoucherResponse;
import com.techedge.mp.quenit.elements.CreateVoucherPromotionalRequest;
import com.techedge.mp.quenit.elements.CreateVoucherPromotionalResponse;
import com.techedge.mp.quenit.elements.CreateVoucherRequest;
import com.techedge.mp.quenit.elements.CreateVoucherResponse;
import com.techedge.mp.quenit.elements.DeleteVoucherRequest;
import com.techedge.mp.quenit.elements.DeleteVoucherResponse;
import com.techedge.mp.quenit.elements.EnableLoyaltyCardRequest;
import com.techedge.mp.quenit.elements.EnableLoyaltyCardResponse;
import com.techedge.mp.quenit.elements.GetLoyaltyCardsListRequest;
import com.techedge.mp.quenit.elements.GetLoyaltyCardsListResponse;
import com.techedge.mp.quenit.elements.InfoRedemptionRequest;
import com.techedge.mp.quenit.elements.InfoRedemptionResponse;
import com.techedge.mp.quenit.elements.LoadLoyaltyCreditsRequest;
import com.techedge.mp.quenit.elements.LoadLoyaltyCreditsResponse;
import com.techedge.mp.quenit.elements.PreAuthorizationConsumeVoucherRequest;
import com.techedge.mp.quenit.elements.PreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.quenit.elements.RedemptionRequest;
import com.techedge.mp.quenit.elements.RedemptionResponse;
import com.techedge.mp.quenit.elements.ReverseConsumeVoucherTransactionRequest;
import com.techedge.mp.quenit.elements.ReverseConsumeVoucherTransactionResponse;
import com.techedge.mp.quenit.elements.ReverseLoadLoyaltyCreditsTransactionRequest;
import com.techedge.mp.quenit.elements.ReverseLoadLoyaltyCreditsTransactionResponse;
import com.techedge.mp.quenit.types.Card;
import com.techedge.mp.quenit.types.CategoryRedemption;
import com.techedge.mp.quenit.types.Product;
import com.techedge.mp.quenit.types.Redemption;
import com.techedge.mp.quenit.types.Voucher;
import com.techedge.mp.quenit.types.VoucherCode;

/**
 * Session Bean implementation class FidelityService
 */
@Stateless
@LocalBean
public class FidelityService implements FidelityServiceRemote, FidelityServiceLocal {

    private final static String     PARAM_FIDELITY_WSDL                             = "FIDELITY_WSDL";
    private final static String     PARAM_FIDELITY_OAUTH_CONSUMER_KEY               = "FIDELITY_OAUTH_CONSUMER_KEY";
    //private final static String     PARAM_FIDELITY_OAUTH_CONSUMER_SECRET = "FIDELITY_OAUTH_CONSUMER_SECRET";
    private final static String     PARAM_PROXY_HOST                                = "PROXY_HOST";
    private final static String     PARAM_PROXY_PORT                                = "PROXY_PORT";
    private final static String     PARAM_PROXY_NO_HOSTS                            = "PROXY_NO_HOSTS";

    private final static String     PARAM_KEYSTORE_PATH                             = "FIDELITY_KEYSTORE_PATH";
    private final static String     PARAM_KEYSTORE_PASSWORD                         = "FIDELITY_KEYSTORE_PASSWORD";
    private final static String     PARAM_KEY_PASSWORD                              = "FIDELITY_KEY_PASSWORD";

    private final static String     PARAM_DEBUG_SOAP                                = "FIDELITY_DEBUG_SOAP";
    private final static String     PARAM_DEBUG_SSL                                 = "FIDELITY_DEBUG_SSL";

    private final static String     PARAM_SIMULATION_ACTIVE                         = "SIMULATION_ACTIVE";
    private final static String     PARAM_SIMULATION_CONSUMEVOUCHER_ERROR           = "SIMULATION_FIDELITY_CONSUMEVOUCHER_ERROR";
    private final static String     PARAM_SIMULATION_LOADLOYALTYCREDITS_ERROR       = "SIMULATION_FIDELITY_LOADLOYALTYCREDITS_ERROR";
    private final static String     PARAM_SIMULATION_PREAUTH_CONSUME_VOUCHER_ERROR  = "SIMULATION_FIDELITY_PREAUTH_CONSUME_VOUCHER_ERROR";
    private final static String     PARAM_SIMULATION_PREAUTH_CONSUME_VOUCHER_KO     = "SIMULATION_FIDELITY_PREAUTH_CONSUME_VOUCHER_KO";

    private final static String     PARAM_SIMULATION_CREATEVOUCHER_ERROR            = "SIMULATION_FIDELITY_CREATEVOUCHER_ERROR";
    private final static String     PARAM_SIMULATION_DELETEVOUCHER_ERROR            = "SIMULATION_FIDELITY_DELETEVOUCHER_ERROR";
    private final static String     PARAM_SIMULATION_REVERSECONSUMEVOUCHER_ERROR    = "SIMULATION_FIDELITY_REVERSECONSUMEVOUCHER_ERROR";
    private final static String     PARAM_SIMULATION_CREATEVOUCHERPROMOTIONAL_ERROR = "SIMULATION_FIDELITY_CREATEVOUCHERPROMOTIONAL_ERROR";

    private final static String     PARAM_SIMULATION_CONSUMEVOUCHER_KO              = "SIMULATION_FIDELITY_CONSUMEVOUCHER_KO";
    private final static String     PARAM_SIMULATION_LOADLOYALTYCREDITS_KO          = "SIMULATION_FIDELITY_LOADLOYALTYCREDITS_KO";
    private final static String     PARAM_SIMULATION_CREATEVOUCHER_KO               = "SIMULATION_FIDELITY_CREATEVOUCHER_KO";
    private final static String     PARAM_SIMULATION_DELETEVOUCHER_KO               = "SIMULATION_FIDELITY_DELETEVOUCHER_KO";
    private final static String     PARAM_SIMULATION_REVERSECONSUMEVOUCHER_KO       = "SIMULATION_FIDELITY_REVERSECONSUMEVOUCHER_KO";
    private final static String     PARAM_SIMULATION_CREATEVOUCHERPROMOTIONAL_KO    = "SIMULATION_FIDELITY_CREATEVOUCHERPROMOTIONAL_KO";

    private ParametersServiceRemote parametersService                               = null;
    private LoggerServiceRemote     loggerService                                   = null;
    private EniWsImpl               eniWsImpl                                       = null;
    private boolean                 simulationActive                                = false;

    public FidelityService() {

    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    private EniWsImpl getEniWsImpl() throws Exception {

        if (this.eniWsImpl == null) {

            try {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
                String wsdlString = parametersService.getParamValue(FidelityService.PARAM_FIDELITY_WSDL);
                String consumerKey = parametersService.getParamValue(FidelityService.PARAM_FIDELITY_OAUTH_CONSUMER_KEY);
                //String consumerSecret = parametersService.getParamValue(FidelityService.PARAM_FIDELITY_OAUTH_CONSUMER_SECRET);
                String consumerSecret = "";
                String proxyHost = parametersService.getParamValue(FidelityService.PARAM_PROXY_HOST);
                String proxyPort = parametersService.getParamValue(FidelityService.PARAM_PROXY_PORT);
                String proxyNoHosts = parametersService.getParamValue(FidelityService.PARAM_PROXY_NO_HOSTS);

                String keyStore = parametersService.getParamValue(FidelityService.PARAM_KEYSTORE_PATH);
                //File keyStoreFile = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "ssl" + File.separator + "enimp");
                //String keyStore = keyStoreFile.getAbsolutePath();

                String keyStorePasswordword = parametersService.getParamValue(FidelityService.PARAM_KEYSTORE_PASSWORD);
                String keyPassword = parametersService.getParamValue(FidelityService.PARAM_KEY_PASSWORD);
                boolean debugSOAP = false;
                boolean debugSSL = false;
                String debugSOAPString = parametersService.getParamValue(FidelityService.PARAM_DEBUG_SOAP);
                String debugSSLString = parametersService.getParamValue(FidelityService.PARAM_DEBUG_SSL);
                this.simulationActive = Boolean.parseBoolean(parametersService.getParamValue(FidelityService.PARAM_SIMULATION_ACTIVE));

                if (debugSOAPString != null) {
                    debugSOAP = Boolean.parseBoolean(debugSOAPString);
                }

                if (debugSSLString != null) {
                    debugSSL = Boolean.parseBoolean(debugSSLString);
                }

                File fileCert = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "oauth" + File.separator + "enimp.oauth");
                FileInputStream input = new FileInputStream(fileCert);

                InputStreamReader is = new InputStreamReader(input);
                StringBuilder sb = new StringBuilder();
                BufferedReader br = new BufferedReader(is);
                String read;

                while ((read = br.readLine()) != null) {
                    //System.out.println(read);
                    if (read.contains("BEGIN CERTIFICATE") || read.contains("END CERTIFICATE"))
                        continue;

                    sb.append(read);
                }

                br.close();
                consumerSecret = sb.toString();

                this.eniWsImpl = new EniWsImpl(consumerKey, consumerSecret, wsdlString, proxyHost, proxyPort, proxyNoHosts, keyStore, keyStorePasswordword, keyPassword, debugSOAP,
                        debugSSL);
            }
            catch (ParameterNotFoundException e) {

                e.printStackTrace();

                throw new Exception("Parameter for Eni Web Service not found: " + e.getMessage());
            }
            catch (InterfaceNotFoundException e) {

                e.printStackTrace();

                throw new Exception("InterfaceNotFoundException for jndi " + e.getJndiString());
            }
        }

        if (simulationActive) {
            System.out.println("Simulazione Adapter Fidelity attiva");
        }

        return this.eniWsImpl;
    }

    @Override
    public CheckConsumeVoucherTransactionResult checkConsumeVoucherTransaction(String operationID, String operationIDtoCheck, PartnerType partnerType, Long requestTimestamp)
            throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("operationIDtoCheck", operationIDtoCheck));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkConsumeVoucherTransaction", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        CheckConsumeVoucherTransactionResult checkConsumeVoucherTransactionResult = new CheckConsumeVoucherTransactionResult();
        CheckConsumeVoucherTransactionRequest checkConsumeVoucherTransactionRequest = new CheckConsumeVoucherTransactionRequest();
        CheckConsumeVoucherTransactionResponse checkConsumeVoucherTransactionResponse = new CheckConsumeVoucherTransactionResponse();

        checkConsumeVoucherTransactionRequest.setOperationID(operationID);
        checkConsumeVoucherTransactionRequest.setOperationIDtoCheck(operationIDtoCheck);
        checkConsumeVoucherTransactionRequest.setPartnerType(partnerType.getValue());
        checkConsumeVoucherTransactionRequest.setRequestTimestamp(requestTimestamp);

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            checkConsumeVoucherTransactionResponse = eniWsImpl.checkConsumeVoucherTransaction(checkConsumeVoucherTransactionRequest);
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "checkConsumeVoucherTransaction", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        checkConsumeVoucherTransactionResult.setCsTransactionID(checkConsumeVoucherTransactionResponse.getCsTransactionID());
        checkConsumeVoucherTransactionResult.setMarketingMsg(checkConsumeVoucherTransactionResponse.getMarketingMsg());
        checkConsumeVoucherTransactionResult.setMessageCode(checkConsumeVoucherTransactionResponse.getMessageCode());
        checkConsumeVoucherTransactionResult.setStatusCode(checkConsumeVoucherTransactionResponse.getStatusCode());

        if (checkConsumeVoucherTransactionResponse.getVoucherList() != null) {
            for (Voucher voucher : checkConsumeVoucherTransactionResponse.getVoucherList()) {
                VoucherDetail voucherDetail = new VoucherDetail();
                voucherDetail.setConsumedValue(AmountConverter.toInternal(voucher.getConsumedValue()));
                voucherDetail.setExpirationDate(voucher.getExpirationDate().toGregorianCalendar().getTime());
                voucherDetail.setInitialValue(AmountConverter.toInternal(voucher.getInitialValue()));
                voucherDetail.setPromoCode(voucher.getPromoCode());
                voucherDetail.setPromoDescription(voucher.getPromoDescrition());
                voucherDetail.setPromoDoc(voucher.getPromoDoc());
                voucherDetail.setVoucherBalanceDue(AmountConverter.toInternal(voucher.getVoucherBalanceDue()));
                voucherDetail.setVoucherCode(voucher.getVoucherCode());
                voucherDetail.setVoucherStatus(voucher.getVoucherStatus());
                voucherDetail.setVoucherType(voucher.getVoucherType());
                voucherDetail.setVoucherValue(AmountConverter.toInternal(voucher.getVoucherValue()));
                checkConsumeVoucherTransactionResult.getVoucherList().add(voucherDetail);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", checkConsumeVoucherTransactionResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("marketingMsg", checkConsumeVoucherTransactionResult.getMarketingMsg()));
        outputParameters.add(new Pair<String, String>("messageCode", checkConsumeVoucherTransactionResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", checkConsumeVoucherTransactionResult.getStatusCode()));

        String voucherListString = "";

        if (!checkConsumeVoucherTransactionResult.getVoucherList().isEmpty()) {

            for (VoucherDetail voucherDetail : checkConsumeVoucherTransactionResult.getVoucherList()) {

                String voucherString = "{" + " consumedValue: " + voucherDetail.getConsumedValue() + ", expirationDate: " + voucherDetail.getExpirationDate().toString()
                        + ", initialValue: " + voucherDetail.getInitialValue().toString() + ", promoCode: " + voucherDetail.getPromoCode() + ", promoDescription: "
                        + voucherDetail.getPromoDescription() + ", promoDoc: " + voucherDetail.getPromoDoc() + ", voucherBalanceDue: "
                        + voucherDetail.getVoucherBalanceDue().toString() + ", voucherCode: " + voucherDetail.getVoucherCode() + ", voucherStatus: "
                        + voucherDetail.getVoucherStatus() + ", voucherType: " + voucherDetail.getVoucherType() + ", voucherValue: " + voucherDetail.getVoucherValue().toString()
                        + " }";

                voucherListString = voucherListString + voucherString;
            }
        }

        outputParameters.add(new Pair<String, String>("voucherList", voucherListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkConsumeVoucherTransaction", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return checkConsumeVoucherTransactionResult;
    }

    @Override
    public CheckLoadLoyaltyCreditsTransactionResult checkLoadLoyaltyCreditsTransaction(String operationID, String operationIDtoCheck, PartnerType partnerType, Long requestTimestamp)
            throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("operationIDtoCheck", operationIDtoCheck));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkLoadLoyaltyCreditsTransaction", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        CheckLoadLoyaltyCreditsTransactionResult checkLoadLoyaltyCreditsTransactionResult = new CheckLoadLoyaltyCreditsTransactionResult();
        CheckLoadLoyaltyCreditsTransactionRequest checkLoadLoyaltyCreditsTransactionRequest = new CheckLoadLoyaltyCreditsTransactionRequest();
        CheckLoadLoyaltyCreditsTransactionResponse checkLoadLoyaltyCreditsTransactionResponse = new CheckLoadLoyaltyCreditsTransactionResponse();

        checkLoadLoyaltyCreditsTransactionRequest.setOperationID(operationID);
        checkLoadLoyaltyCreditsTransactionRequest.setOperationIDtoCheck(operationIDtoCheck);
        checkLoadLoyaltyCreditsTransactionRequest.setPartnerType(partnerType.getValue());
        checkLoadLoyaltyCreditsTransactionRequest.setRequestTimestamp(requestTimestamp);

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            checkLoadLoyaltyCreditsTransactionResponse = eniWsImpl.checkLoadLoyaltyCreditsTransaction(checkLoadLoyaltyCreditsTransactionRequest);
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "checkConsumeVoucherTransaction", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        checkLoadLoyaltyCreditsTransactionResult.setBalance(AmountConverter.toInternal(checkLoadLoyaltyCreditsTransactionResponse.getBalance()));
        checkLoadLoyaltyCreditsTransactionResult.setBalanceAmount(AmountConverter.toInternal(checkLoadLoyaltyCreditsTransactionResponse.getBalanceAmount()));
        checkLoadLoyaltyCreditsTransactionResult.setCardClassification(checkLoadLoyaltyCreditsTransactionResponse.getCardClassification());
        checkLoadLoyaltyCreditsTransactionResult.setCardCodeIssuer(checkLoadLoyaltyCreditsTransactionResponse.getCardCodeIssuer());
        checkLoadLoyaltyCreditsTransactionResult.setCardStatus(checkLoadLoyaltyCreditsTransactionResponse.getCardStatus());
        checkLoadLoyaltyCreditsTransactionResult.setCardType(checkLoadLoyaltyCreditsTransactionResponse.getCardType());
        checkLoadLoyaltyCreditsTransactionResult.setCredits(AmountConverter.toInternal(checkLoadLoyaltyCreditsTransactionResponse.getCredits()));
        checkLoadLoyaltyCreditsTransactionResult.setCsTransactionID(checkLoadLoyaltyCreditsTransactionResponse.getCsTransactionID());
        checkLoadLoyaltyCreditsTransactionResult.setEanCode(checkLoadLoyaltyCreditsTransactionResponse.getEanCode());
        checkLoadLoyaltyCreditsTransactionResult.setMarketingMsg(checkLoadLoyaltyCreditsTransactionResponse.getMarketingMsg());
        checkLoadLoyaltyCreditsTransactionResult.setMessageCode(checkLoadLoyaltyCreditsTransactionResponse.getMessageCode());
        checkLoadLoyaltyCreditsTransactionResult.setStatusCode(checkLoadLoyaltyCreditsTransactionResponse.getStatusCode());

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("balance", checkLoadLoyaltyCreditsTransactionResult.getBalance().toString()));
        outputParameters.add(new Pair<String, String>("balanceAmount", checkLoadLoyaltyCreditsTransactionResult.getBalanceAmount().toString()));
        outputParameters.add(new Pair<String, String>("cardClassification", checkLoadLoyaltyCreditsTransactionResult.getCardClassification()));
        outputParameters.add(new Pair<String, String>("cardCodeIssuer", checkLoadLoyaltyCreditsTransactionResult.getCardCodeIssuer()));
        outputParameters.add(new Pair<String, String>("cardStrtus", checkLoadLoyaltyCreditsTransactionResult.getCardStatus()));
        outputParameters.add(new Pair<String, String>("cardType", checkLoadLoyaltyCreditsTransactionResult.getCardType()));
        outputParameters.add(new Pair<String, String>("credits", checkLoadLoyaltyCreditsTransactionResult.getCredits().toString()));
        outputParameters.add(new Pair<String, String>("csTransactionID", checkLoadLoyaltyCreditsTransactionResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("eanCode", checkLoadLoyaltyCreditsTransactionResult.getEanCode()));
        outputParameters.add(new Pair<String, String>("marketingMsg", checkLoadLoyaltyCreditsTransactionResult.getMarketingMsg()));
        outputParameters.add(new Pair<String, String>("messageCode", checkLoadLoyaltyCreditsTransactionResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", checkLoadLoyaltyCreditsTransactionResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkLoadLoyaltyCreditsTransaction", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return checkLoadLoyaltyCreditsTransactionResult;
    }

    @Override
    public CheckVoucherResult checkVoucher(String operationID, VoucherConsumerType voucherType, PartnerType partnerType, Long requestTimestamp,
            List<VoucherCodeDetail> voucherCodeDetailList) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("voucherType", voucherType.getValue()));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        String voucherCodeListString = "";
        CheckVoucherResult checkVoucherResult = new CheckVoucherResult();
        CheckVoucherRequest checkVoucherRequest = new CheckVoucherRequest();
        CheckVoucherResponse checkVoucherResponse = new CheckVoucherResponse();

        checkVoucherRequest.setOperationID(operationID);
        checkVoucherRequest.setVoucherType(voucherType.getValue());
        checkVoucherRequest.setPartnerType(partnerType.getValue());
        checkVoucherRequest.setRequestTimestamp(requestTimestamp);

        for (VoucherCodeDetail voucherCodeDetail : voucherCodeDetailList) {

            System.out.println("Fidelity Service add voucher " + voucherCodeDetail.getVoucherCode());
            VoucherCode voucherCode = new VoucherCode();
            voucherCode.setVoucherCode(voucherCodeDetail.getVoucherCode());
            checkVoucherRequest.getVoucherList().add(voucherCode);

            String voucherCodeString = "{" + " voucherCode: " + voucherCodeDetail.getVoucherCode() + " }";

            voucherCodeListString = voucherCodeListString + voucherCodeString;
        }

        inputParameters.add(new Pair<String, String>("voucherList", voucherCodeListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkVoucher", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        if (checkVoucherRequest.getVoucherList().isEmpty()) {

            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "checkVoucher", operationID, "closing", "No voucherList present");
            throw new FidelityServiceException("No voucherList present");
        }

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            checkVoucherResponse = eniWsImpl.checkVoucher(checkVoucherRequest);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        checkVoucherResult.setCsTransactionID(checkVoucherResponse.getCsTransactionID());
        checkVoucherResult.setMessageCode(checkVoucherResponse.getMessageCode());
        checkVoucherResult.setStatusCode(checkVoucherResponse.getStatusCode());

        if (checkVoucherResponse.getVoucherList() != null) {
            for (Voucher voucher : checkVoucherResponse.getVoucherList()) {
                VoucherDetail voucherDetail = new VoucherDetail();
                voucherDetail.setConsumedValue(AmountConverter.toInternal(voucher.getConsumedValue()));

                Date expirationDate = Calendar.getInstance().getTime();

                if (voucher.getExpirationDate() != null) {
                    expirationDate = voucher.getExpirationDate().toGregorianCalendar().getTime();
                }

                voucherDetail.setExpirationDate(expirationDate);
                voucherDetail.setInitialValue(AmountConverter.toInternal(voucher.getInitialValue()));
                voucherDetail.setPromoCode(voucher.getPromoCode());
                voucherDetail.setPromoDescription(voucher.getPromoDescrition());
                voucherDetail.setPromoDoc(voucher.getPromoDoc());
                voucherDetail.setVoucherBalanceDue(AmountConverter.toInternal(voucher.getVoucherBalanceDue()));
                voucherDetail.setVoucherCode(voucher.getVoucherCode());
                voucherDetail.setVoucherStatus(voucher.getVoucherStatus());
                voucherDetail.setVoucherType(voucher.getVoucherType());
                voucherDetail.setVoucherValue(AmountConverter.toInternal(voucher.getVoucherValue()));
                checkVoucherResult.getVoucherList().add(voucherDetail);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", checkVoucherResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("messageCode", checkVoucherResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", checkVoucherResult.getStatusCode()));

        String voucherListString = "";

        if (!checkVoucherResult.getVoucherList().isEmpty()) {

            for (VoucherDetail voucherDetail : checkVoucherResult.getVoucherList()) {

                String voucherString = "{ " + "consumedValue: " + voucherDetail.getConsumedValue() + ", expirationDate: " + voucherDetail.getExpirationDate().toString()
                        + ", initialValue: " + voucherDetail.getInitialValue().toString() + ", promoCode: " + voucherDetail.getPromoCode() + ", promoDescription: "
                        + voucherDetail.getPromoDescription() + ", promoDoc: " + voucherDetail.getPromoDoc() + ", voucherBalanceDue: "
                        + voucherDetail.getVoucherBalanceDue().toString() + ", voucherCode: " + voucherDetail.getVoucherCode() + ", voucherStatus: "
                        + voucherDetail.getVoucherStatus() + ", voucherType: " + voucherDetail.getVoucherType() + ", voucherValue: " + voucherDetail.getVoucherValue().toString()
                        + " }";

                voucherListString = voucherListString + voucherString;
            }
        }

        outputParameters.add(new Pair<String, String>("voucherList", voucherListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkVoucher", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return checkVoucherResult;
    }

    @Override
    public ConsumeVoucherResult consumeVoucher(String operationID, String mpTransactionID, VoucherConsumerType voucherType, String stationID, String refuelMode,
            String paymentMode, String language, PartnerType partnerType, Long requestTimestamp, List<ProductDetail> productList, List<VoucherCodeDetail> voucherCodeList,
            String consumeType, String preAuthOperationID) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));
        inputParameters.add(new Pair<String, String>("voucherType", voucherType.getValue()));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("refuelMode", refuelMode));
        inputParameters.add(new Pair<String, String>("paymentMode", paymentMode));
        inputParameters.add(new Pair<String, String>("language", language));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("consumeType", consumeType));
        inputParameters.add(new Pair<String, String>("preAuthOperationID", preAuthOperationID));

        ConsumeVoucherResult consumeVoucherResult = new ConsumeVoucherResult();
        ConsumeVoucherRequest consumeVoucherRequest = new ConsumeVoucherRequest();
        ConsumeVoucherResponse consumeVoucherResponse = new ConsumeVoucherResponse();

        consumeVoucherRequest.setOperationID(operationID);
        consumeVoucherRequest.setMpTransactionID(mpTransactionID);
        consumeVoucherRequest.setVoucherType(voucherType.getValue());
        consumeVoucherRequest.setPartnerType(partnerType.getValue());
        consumeVoucherRequest.setRequestTimestamp(requestTimestamp);
        consumeVoucherRequest.setPaymentMode(paymentMode);
        consumeVoucherRequest.setLanguage(language);
        consumeVoucherRequest.setRefuelMode(refuelMode);
        consumeVoucherRequest.setStationID(stationID);
        consumeVoucherRequest.setConsumeType(consumeType);
        consumeVoucherRequest.setPreAuthOperationID(preAuthOperationID);

        String productListString = "";

        if (!productList.isEmpty()) {

            for (ProductDetail productDetail : productList) {

                Product product = new Product();
                product.setAmount(AmountConverter.toInternal(productDetail.getAmount()));
                product.setProductCode(productDetail.getProductCode());
                product.setQuantity(AmountConverter.toInternal(productDetail.getQuantity()));
                consumeVoucherRequest.getProductsList().add(product);

                String productString = "{" + " amount: " + productDetail.getAmount() + ", productCode: " + productDetail.getProductCode() + ", quantity: "
                        + productDetail.getQuantity() + " }";

                productListString = productListString + productString;
            }
        }

        inputParameters.add(new Pair<String, String>("productList", productListString));

        String voucherCodeListString = "";

        if (voucherCodeList != null && !voucherCodeList.isEmpty()) {

            for (VoucherCodeDetail voucherCodeDetail : voucherCodeList) {

                VoucherCode voucherCode = new VoucherCode();
                voucherCode.setVoucherCode(voucherCodeDetail.getVoucherCode());
                consumeVoucherRequest.getVoucherList().add(voucherCode);

                String voucherCodeString = "{" + " voucherCode: " + voucherCodeDetail.getVoucherCode() + " }";

                voucherCodeListString = voucherCodeListString + voucherCodeString;
            }
        }

        inputParameters.add(new Pair<String, String>("voucherCodeList", voucherCodeListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "consumeVoucher", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        String simulationConsumeVoucherError = null;
        boolean simulationConsumeVoucherKO = false;

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            if (simulationActive) {

                simulationConsumeVoucherError = parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_CONSUMEVOUCHER_ERROR);
                simulationConsumeVoucherKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_CONSUMEVOUCHER_KO));

                if (simulationConsumeVoucherError != null && simulationConsumeVoucherError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }

                if (simulationConsumeVoucherKO) {
                    consumeVoucherResult.setStatusCode("9999");
                    consumeVoucherResult.setMessageCode("Simulazione di KO");
                    return consumeVoucherResult;
                }
            }

            consumeVoucherResponse = eniWsImpl.consumeVoucher(consumeVoucherRequest);

            if (simulationActive) {
                if (simulationConsumeVoucherError.equalsIgnoreCase("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "consumeVoucher", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        consumeVoucherResult.setCsTransactionID(consumeVoucherResponse.getCsTransactionID());
        consumeVoucherResult.setMarketingMsg(consumeVoucherResponse.getMarketingMsg());
        consumeVoucherResult.setMessageCode(consumeVoucherResponse.getMessageCode());
        consumeVoucherResult.setStatusCode(consumeVoucherResponse.getStatusCode());
        consumeVoucherResult.setWarningMsg(consumeVoucherResponse.getWarningMsg());

        if (consumeVoucherResponse.getVoucherList() != null) {
            for (Voucher voucher : consumeVoucherResponse.getVoucherList()) {
                VoucherDetail voucherDetail = new VoucherDetail();
                voucherDetail.setConsumedValue(AmountConverter.toInternal(voucher.getConsumedValue()));
                voucherDetail.setExpirationDate(voucher.getExpirationDate().toGregorianCalendar().getTime());
                voucherDetail.setInitialValue(AmountConverter.toInternal(voucher.getInitialValue()));
                voucherDetail.setPromoCode(voucher.getPromoCode());
                voucherDetail.setPromoDescription(voucher.getPromoDescrition());
                voucherDetail.setPromoDoc(voucher.getPromoDoc());
                voucherDetail.setVoucherBalanceDue(AmountConverter.toInternal(voucher.getVoucherBalanceDue()));
                voucherDetail.setVoucherCode(voucher.getVoucherCode());
                voucherDetail.setVoucherStatus(voucher.getVoucherStatus());
                voucherDetail.setVoucherType(voucher.getVoucherType());
                voucherDetail.setVoucherValue(AmountConverter.toInternal(voucher.getVoucherValue()));
                consumeVoucherResult.getVoucherList().add(voucherDetail);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", consumeVoucherResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("marketingMsg", consumeVoucherResult.getMarketingMsg()));
        outputParameters.add(new Pair<String, String>("messageCode", consumeVoucherResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", consumeVoucherResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("warningMsg", consumeVoucherResult.getWarningMsg()));

        String voucherListString = "";

        if (!consumeVoucherResult.getVoucherList().isEmpty()) {

            for (VoucherDetail voucherDetail : consumeVoucherResult.getVoucherList()) {

                String voucherString = "{ " + ", consumedValue: " + voucherDetail.getConsumedValue() + ", expirationDate: " + voucherDetail.getExpirationDate().toString()
                        + ", initialValue: " + voucherDetail.getInitialValue().toString() + ", promoCode: " + voucherDetail.getPromoCode() + ", promoDescription: "
                        + voucherDetail.getPromoDescription() + ", promoDoc: " + voucherDetail.getPromoDoc() + ", voucherBalanceDue: "
                        + voucherDetail.getVoucherBalanceDue().toString() + ", voucherCode: " + voucherDetail.getVoucherCode() + ", voucherStatus: "
                        + voucherDetail.getVoucherStatus() + ", voucherType: " + voucherDetail.getVoucherType() + ", voucherValue: " + voucherDetail.getVoucherValue().toString()
                        + " }";

                voucherListString = voucherListString + voucherString;
            }
        }

        outputParameters.add(new Pair<String, String>("voucherList", voucherListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "consumeVoucher", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return consumeVoucherResult;
    }

    @Override
    public EnableLoyaltyCardResult enableLoyaltyCard(String operationID, String fiscalCode, String panCode, Boolean enable, PartnerType partnerType, Long requestTimestamp)
            throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));
        inputParameters.add(new Pair<String, String>("panCode", panCode));
        inputParameters.add(new Pair<String, String>("enable", enable.toString()));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "enableLoyaltyCard", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        EnableLoyaltyCardResult enableLoyaltyCardResult = new EnableLoyaltyCardResult();
        EnableLoyaltyCardRequest enableLoyaltyCardRequest = new EnableLoyaltyCardRequest();
        EnableLoyaltyCardResponse enableLoyaltyCardResponse = new EnableLoyaltyCardResponse();

        enableLoyaltyCardRequest.setOperationID(operationID);
        enableLoyaltyCardRequest.setFiscalCode(fiscalCode);
        enableLoyaltyCardRequest.setPanCode(panCode);
        enableLoyaltyCardRequest.setPartnerType(partnerType.getValue());
        enableLoyaltyCardRequest.setRequestTimestamp(requestTimestamp);
        enableLoyaltyCardRequest.setEnable(enable);

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            enableLoyaltyCardResponse = eniWsImpl.enableLoyaltyCard(enableLoyaltyCardRequest);
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "enableLoyaltyCard", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        enableLoyaltyCardResult.setCsTransactionID(enableLoyaltyCardResponse.getCsTransactionID());
        enableLoyaltyCardResult.setStatusCode(enableLoyaltyCardResponse.getStatusCode());
        enableLoyaltyCardResult.setMessageCode(enableLoyaltyCardResponse.getMessageCode());

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", enableLoyaltyCardResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("messageCode", enableLoyaltyCardResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", enableLoyaltyCardResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "enableLoyaltyCard", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return enableLoyaltyCardResult;
    }

    @Override
    public GetLoyaltyCardListResult getLoyaltyCardList(String operationID, String fiscalCode, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getLoyaltyCardsList", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetLoyaltyCardListResult getLoyaltyCardListResult = new GetLoyaltyCardListResult();
        GetLoyaltyCardsListRequest getLoyaltyCardsListRequest = new GetLoyaltyCardsListRequest();
        GetLoyaltyCardsListResponse getLoyaltyCardsListResponse = new GetLoyaltyCardsListResponse();

        getLoyaltyCardsListRequest.setOperationID(operationID);
        getLoyaltyCardsListRequest.setFiscalCode(fiscalCode);
        getLoyaltyCardsListRequest.setPartnerType(partnerType.getValue());
        getLoyaltyCardsListRequest.setRequestTimestamp(requestTimestamp);

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            getLoyaltyCardsListResponse = eniWsImpl.getLoyaltyCardsList(getLoyaltyCardsListRequest);
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getLoyaltyCardsList", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        getLoyaltyCardListResult.setBalance(AmountConverter.toInternal(getLoyaltyCardsListResponse.getBalance()));
        getLoyaltyCardListResult.setCsTransactionID(getLoyaltyCardsListResponse.getCsTransactionID());
        getLoyaltyCardListResult.setMessageCode(getLoyaltyCardsListResponse.getMessageCode());
        getLoyaltyCardListResult.setStatusCode(getLoyaltyCardsListResponse.getStatusCode());

        if (getLoyaltyCardsListResponse.getCardsList() != null) {
            for (Card card : getLoyaltyCardsListResponse.getCardsList()) {
                CardDetail cardDetail = new CardDetail();
                cardDetail.setEanCode(card.getEanCode());
                cardDetail.setPanCode(card.getPanCode());
                cardDetail.setVirtual(card.getVirtual());
                if (card.getCardCoverType() == null) {
                    cardDetail.setCardType("G");
                }
                else {
                    cardDetail.setCardType(card.getCardCoverType());
                }

                getLoyaltyCardListResult.getCardList().add(cardDetail);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", getLoyaltyCardListResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("messageCode", getLoyaltyCardListResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", getLoyaltyCardListResult.getStatusCode()));

        String cardListString = "";

        if (!getLoyaltyCardListResult.getCardList().isEmpty()) {

            for (CardDetail cardDetail : getLoyaltyCardListResult.getCardList()) {

                String cardString = "{ " + "eanCode: " + cardDetail.getEanCode() + "  panCode: " + cardDetail.getPanCode() + "  cardType: " + cardDetail.getCardType() + " }";

                cardListString = cardListString + cardString;
            }
        }

        outputParameters.add(new Pair<String, String>("cardList", cardListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getLoyaltyCardList", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getLoyaltyCardListResult;
    }

    @Override
    public LoadLoyaltyCreditsResult loadLoyaltyCredits(String operationID, String mpTransactionID, String stationID, String panCode, String BIN, String refuelMode,
            String paymentMode, String language, PartnerType partnerType, Long requestTimestamp, String fiscalCode, List<ProductDetail> productList) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("panCode", panCode));
        inputParameters.add(new Pair<String, String>("BIN", BIN));
        inputParameters.add(new Pair<String, String>("refuelMode", refuelMode));
        inputParameters.add(new Pair<String, String>("paymentMode", paymentMode));
        inputParameters.add(new Pair<String, String>("language", language));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));

        LoadLoyaltyCreditsResult loadLoyaltyCreditsResult = new LoadLoyaltyCreditsResult();
        LoadLoyaltyCreditsRequest loadLoyaltyCreditsRequest = new LoadLoyaltyCreditsRequest();
        LoadLoyaltyCreditsResponse loadLoyaltyCreditsResponse = new LoadLoyaltyCreditsResponse();

        String productListString = "";

        if (!productList.isEmpty()) {

            for (ProductDetail productDetail : productList) {

                Product product = new Product();
                product.setAmount(AmountConverter.toInternal(productDetail.getAmount()));
                product.setProductCode(productDetail.getProductCode());
                product.setQuantity(AmountConverter.toInternal(productDetail.getQuantity()));
                loadLoyaltyCreditsRequest.getProductsList().add(product);

                String productString = "{" + " amount: " + productDetail.getAmount() + ", productCode: " + productDetail.getProductCode() + ", quantity: "
                        + productDetail.getQuantity() + " }";

                productListString = productListString + productString;
            }
        }

        inputParameters.add(new Pair<String, String>("productList", productListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "loadLoyaltyCredits", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        loadLoyaltyCreditsRequest.setOperationID(operationID);
        loadLoyaltyCreditsRequest.setMpTransactionID(mpTransactionID);
        loadLoyaltyCreditsRequest.setStationID(stationID);
        loadLoyaltyCreditsRequest.setPanCode(panCode);
        loadLoyaltyCreditsRequest.setBIN(BIN);
        loadLoyaltyCreditsRequest.setRefuelMode(refuelMode);
        loadLoyaltyCreditsRequest.setPaymentMode(paymentMode);
        loadLoyaltyCreditsRequest.setLanguage(language);
        loadLoyaltyCreditsRequest.setPartnerType(partnerType.getValue());
        loadLoyaltyCreditsRequest.setRequestTimestamp(requestTimestamp);
        loadLoyaltyCreditsRequest.setFiscalCode(fiscalCode);

        String simulationLoadLoyaltyCreditsError = null;
        boolean simulationLoadLoyaltyCreditsKO = false;

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            if (simulationActive) {

                simulationLoadLoyaltyCreditsError = parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_LOADLOYALTYCREDITS_ERROR);
                simulationLoadLoyaltyCreditsKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_LOADLOYALTYCREDITS_KO));

                if (simulationLoadLoyaltyCreditsError != null && simulationLoadLoyaltyCreditsError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }

                if (simulationLoadLoyaltyCreditsKO) {
                    loadLoyaltyCreditsResult.setStatusCode("9999");
                    loadLoyaltyCreditsResult.setMessageCode("Simulazione di KO");
                    return loadLoyaltyCreditsResult;
                }
            }

            loadLoyaltyCreditsResponse = eniWsImpl.loadLoyaltyCredits(loadLoyaltyCreditsRequest);

            if (simulationActive) {
                if (simulationLoadLoyaltyCreditsError.equals("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "loadLoyaltyCredits", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        loadLoyaltyCreditsResult.setBalance(AmountConverter.toInternal(loadLoyaltyCreditsResponse.getBalance()));
        loadLoyaltyCreditsResult.setBalanceAmount(AmountConverter.toInternal(loadLoyaltyCreditsResponse.getBalanceAmount()));
        loadLoyaltyCreditsResult.setCardClassification(loadLoyaltyCreditsResponse.getCardClassification());
        loadLoyaltyCreditsResult.setCardCodeIssuer(loadLoyaltyCreditsResponse.getCardCodeIssuer());
        loadLoyaltyCreditsResult.setCardStatus(loadLoyaltyCreditsResponse.getCardStatus());

        if (loadLoyaltyCreditsResponse.getCardType() == null) {
            loadLoyaltyCreditsResult.setCardType("G");
        }
        else {
            loadLoyaltyCreditsResult.setCardType(loadLoyaltyCreditsResponse.getCardType());
        }

        loadLoyaltyCreditsResult.setCredits(AmountConverter.toInternal(loadLoyaltyCreditsResponse.getCredits()));
        loadLoyaltyCreditsResult.setCsTransactionID(loadLoyaltyCreditsResponse.getCsTransactionID());
        loadLoyaltyCreditsResult.setEanCode(loadLoyaltyCreditsResponse.getEanCode());
        loadLoyaltyCreditsResult.setMarketingMsg(loadLoyaltyCreditsResponse.getMarketingMsg());
        loadLoyaltyCreditsResult.setMessageCode(loadLoyaltyCreditsResponse.getMessageCode());
        loadLoyaltyCreditsResult.setStatusCode(loadLoyaltyCreditsResponse.getStatusCode());
        loadLoyaltyCreditsResult.setStatusCode(loadLoyaltyCreditsResponse.getStatusCode());
        loadLoyaltyCreditsResult.setWarningMsg(loadLoyaltyCreditsResponse.getWarningMsg());
        
        if (loadLoyaltyCreditsResponse.getVoucherList() != null && !loadLoyaltyCreditsResponse.getVoucherList().isEmpty()) {
            for(Voucher voucher : loadLoyaltyCreditsResponse.getVoucherList()) {
                VoucherDetail voucherDetail = new VoucherDetail();
                voucherDetail.setConsumedValue(AmountConverter.toInternal(voucher.getConsumedValue()));
                voucherDetail.setExpirationDate(voucher.getExpirationDate().toGregorianCalendar().getTime());
                voucherDetail.setInitialValue(AmountConverter.toInternal(voucher.getInitialValue()));
                voucherDetail.setPromoCode(voucher.getPromoCode());
                voucherDetail.setPromoDescription(voucher.getPromoDescrition());
                voucherDetail.setPromoDoc(voucher.getPromoDoc());
                voucherDetail.setVoucherBalanceDue(AmountConverter.toInternal(voucher.getVoucherBalanceDue()));
                voucherDetail.setVoucherCode(voucher.getVoucherCode());
                voucherDetail.setVoucherStatus(voucher.getVoucherStatus());
                voucherDetail.setVoucherType(voucher.getVoucherType());
                voucherDetail.setVoucherValue(AmountConverter.toInternal(voucher.getVoucherValue()));
                loadLoyaltyCreditsResult.getVoucherList().add(voucherDetail);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("balance", loadLoyaltyCreditsResult.getBalance().toString()));
        outputParameters.add(new Pair<String, String>("balanceAmount", loadLoyaltyCreditsResult.getBalanceAmount().toString()));
        outputParameters.add(new Pair<String, String>("cardClassification", loadLoyaltyCreditsResult.getCardClassification()));
        outputParameters.add(new Pair<String, String>("cardCodeIssuer", loadLoyaltyCreditsResult.getCardCodeIssuer()));
        outputParameters.add(new Pair<String, String>("cardStatus", loadLoyaltyCreditsResult.getCardStatus()));
        outputParameters.add(new Pair<String, String>("cardType", loadLoyaltyCreditsResult.getCardType()));
        outputParameters.add(new Pair<String, String>("credits", loadLoyaltyCreditsResult.getCredits().toString()));
        outputParameters.add(new Pair<String, String>("csTransactionID", loadLoyaltyCreditsResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("eanCode", loadLoyaltyCreditsResult.getEanCode()));
        outputParameters.add(new Pair<String, String>("marketingMsg", loadLoyaltyCreditsResult.getMarketingMsg()));
        outputParameters.add(new Pair<String, String>("messageCode", loadLoyaltyCreditsResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", loadLoyaltyCreditsResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("warningMsg", loadLoyaltyCreditsResult.getWarningMsg()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "loadLoyaltyCredits", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return loadLoyaltyCreditsResult;
    }

    @Override
    public ReverseConsumeVoucherTransactionResult reverseConsumeVoucherTransaction(String operationID, String operationIDtoReverse, PartnerType partnerType, Long requestTimestamp)
            throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("operationIDtoReverse", operationIDtoReverse));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reverseConsumeVoucherTransaction", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        ReverseConsumeVoucherTransactionResult reverseConsumeVoucherTransactionResult = new ReverseConsumeVoucherTransactionResult();
        ReverseConsumeVoucherTransactionRequest reverseConsumeVoucherTransactionRequest = new ReverseConsumeVoucherTransactionRequest();
        ReverseConsumeVoucherTransactionResponse reverseConsumeVoucherTransactionResponse = new ReverseConsumeVoucherTransactionResponse();

        reverseConsumeVoucherTransactionRequest.setOperationID(operationID);
        reverseConsumeVoucherTransactionRequest.setOperationIDtoReverse(operationIDtoReverse);
        reverseConsumeVoucherTransactionRequest.setPartnerType(partnerType.getValue());
        reverseConsumeVoucherTransactionRequest.setRequestTimestamp(requestTimestamp);

        String simulationReverseConsumeVoucherError = null;
        boolean simulationReverseConsumeVoucherKO = false;

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            if (simulationActive) {
                simulationReverseConsumeVoucherError = parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_REVERSECONSUMEVOUCHER_ERROR);
                simulationReverseConsumeVoucherKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_REVERSECONSUMEVOUCHER_KO));

                if (simulationReverseConsumeVoucherError != null && simulationReverseConsumeVoucherError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }

                if (simulationReverseConsumeVoucherKO) {
                    reverseConsumeVoucherTransactionResult.setStatusCode("9999");
                    reverseConsumeVoucherTransactionResult.setMessageCode("Simulazione di KO");
                    return reverseConsumeVoucherTransactionResult;
                }
            }

            reverseConsumeVoucherTransactionResponse = eniWsImpl.reverseConsumeVoucherTransaction(reverseConsumeVoucherTransactionRequest);

            if (simulationActive) {
                if (simulationReverseConsumeVoucherError != null && simulationReverseConsumeVoucherError.equalsIgnoreCase("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "reverseConsumeVoucherTransaction", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        reverseConsumeVoucherTransactionResult.setCsTransactionID(reverseConsumeVoucherTransactionResponse.getCsTransactionID());
        reverseConsumeVoucherTransactionResult.setMessageCode(reverseConsumeVoucherTransactionResponse.getMessageCode());
        reverseConsumeVoucherTransactionResult.setStatusCode(reverseConsumeVoucherTransactionResponse.getStatusCode());

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", reverseConsumeVoucherTransactionResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("messageCode", reverseConsumeVoucherTransactionResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", reverseConsumeVoucherTransactionResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reverseConsumeVoucherTransaction", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return reverseConsumeVoucherTransactionResult;
    }

    @Override
    public ReverseLoadLoyaltyCreditsTransactionResult reverseLoadLoyaltyCreditsTransaction(String operationID, String operationIDtoReverse, PartnerType partnerType,
            Long requestTimestamp) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("operationIDtoReverse", operationIDtoReverse));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reverseLoadLoyaltyCreditsTransaction", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        ReverseLoadLoyaltyCreditsTransactionResult reverseLoadLoyaltyCreditsTransactionResult = new ReverseLoadLoyaltyCreditsTransactionResult();
        ReverseLoadLoyaltyCreditsTransactionRequest reverseLoadLoyaltyCreditsTransactionRequest = new ReverseLoadLoyaltyCreditsTransactionRequest();
        ReverseLoadLoyaltyCreditsTransactionResponse reverseLoadLoyaltyCreditsTransactionResponse = new ReverseLoadLoyaltyCreditsTransactionResponse();

        reverseLoadLoyaltyCreditsTransactionRequest.setOperationID(operationID);
        reverseLoadLoyaltyCreditsTransactionRequest.setOperationIDtoReverse(operationIDtoReverse);
        reverseLoadLoyaltyCreditsTransactionRequest.setPartnerType(partnerType.getValue());
        reverseLoadLoyaltyCreditsTransactionRequest.setRequestTimestamp(requestTimestamp);

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            reverseLoadLoyaltyCreditsTransactionResponse = eniWsImpl.reverseLoadLoyaltyCreditsTransaction(reverseLoadLoyaltyCreditsTransactionRequest);

        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "reverseLoadLoyaltyCreditsTransaction", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        reverseLoadLoyaltyCreditsTransactionResult.setCsTransactionID(reverseLoadLoyaltyCreditsTransactionResponse.getCsTransactionID());
        reverseLoadLoyaltyCreditsTransactionResult.setMessageCode(reverseLoadLoyaltyCreditsTransactionResponse.getMessageCode());
        reverseLoadLoyaltyCreditsTransactionResult.setStatusCode(reverseLoadLoyaltyCreditsTransactionResponse.getStatusCode());

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", reverseLoadLoyaltyCreditsTransactionResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("messageCode", reverseLoadLoyaltyCreditsTransactionResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", reverseLoadLoyaltyCreditsTransactionResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reverseLoadLoyaltyCreditsTransaction", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return reverseLoadLoyaltyCreditsTransactionResult;
    }

    @Override
    public PreAuthorizationConsumeVoucherResult preAuthorizationConsumeVoucher(String operationID, String mpTransactionID, VoucherConsumerType voucherType, String stationID,
            Double amount, List<VoucherCodeDetail> voucherCodeList, PartnerType partnerType, Long requestTimestamp, String language, ProductType productType)
            throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        String voucherCodeListString = "";

        for (VoucherCodeDetail voucherCodeDetail : voucherCodeList) {
            voucherCodeListString = voucherCodeListString + "{ \"voucherCode\":" + voucherCodeDetail.getVoucherCode() + " }";
        }

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));
        inputParameters.add(new Pair<String, String>("voucherType", voucherType.getValue()));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        inputParameters.add(new Pair<String, String>("voucherCodeList", voucherCodeListString));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("language", language));
        inputParameters.add(new Pair<String, String>("productType", productType.getValue()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "preAuthorizationConsumeVoucher", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        PreAuthorizationConsumeVoucherResult preAuthorizationConsumeVoucherResult = new PreAuthorizationConsumeVoucherResult();
        PreAuthorizationConsumeVoucherRequest preAuthorizationConsumeVoucherRequest = new PreAuthorizationConsumeVoucherRequest();
        PreAuthorizationConsumeVoucherResponse preAuthorizationConsumeVoucherResponse = new PreAuthorizationConsumeVoucherResponse();

        ArrayList<VoucherCode> vouchers = new ArrayList<VoucherCode>();

        for (VoucherCodeDetail voucherCodeDetail : voucherCodeList) {
            VoucherCode voucherCode = new VoucherCode();
            voucherCode.setVoucherCode(voucherCodeDetail.getVoucherCode());
            vouchers.add(voucherCode);
        }

        preAuthorizationConsumeVoucherRequest.setOperationID(operationID);
        preAuthorizationConsumeVoucherRequest.setMpTransactionID(mpTransactionID);
        preAuthorizationConsumeVoucherRequest.setVoucherType(voucherType.getValue());
        preAuthorizationConsumeVoucherRequest.setStationID(stationID);
        preAuthorizationConsumeVoucherRequest.setAmount(new BigDecimal(amount.toString()));
        preAuthorizationConsumeVoucherRequest.setVoucherList(vouchers);
        preAuthorizationConsumeVoucherRequest.setPartnerType(partnerType.getValue());
        preAuthorizationConsumeVoucherRequest.setRequestTimestamp(requestTimestamp);
        preAuthorizationConsumeVoucherRequest.setLanguage(language);
        preAuthorizationConsumeVoucherRequest.setProductType(productType.getValue());

        String simulationPreauthVoucherError = null;
        boolean simulationPreauthVoucherKO = false;

        
        try {
            EniWsImpl eniWsImpl = getEniWsImpl();
            
            
            if (simulationActive) {

                simulationPreauthVoucherError = parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_PREAUTH_CONSUME_VOUCHER_ERROR);
                simulationPreauthVoucherKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_PREAUTH_CONSUME_VOUCHER_KO));

                if (simulationPreauthVoucherError != null && simulationPreauthVoucherError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }

                if (simulationPreauthVoucherKO) {
                    preAuthorizationConsumeVoucherResult.setStatusCode("9999");
                    preAuthorizationConsumeVoucherResult.setMessageCode("Simulazione di KO");
                    return preAuthorizationConsumeVoucherResult;
                }
            }

            preAuthorizationConsumeVoucherResponse = eniWsImpl.preAuthorizationConsumeVoucher(preAuthorizationConsumeVoucherRequest);
            
            if (simulationActive) {
                if (simulationPreauthVoucherError.equalsIgnoreCase("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }

        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "preAuthorizationConsumeVoucher", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        preAuthorizationConsumeVoucherResult.setCsTransactionID(preAuthorizationConsumeVoucherResponse.getCsTransactionID());
        preAuthorizationConsumeVoucherResult.setMessageCode(preAuthorizationConsumeVoucherResponse.getMessageCode());
        preAuthorizationConsumeVoucherResult.setStatusCode(preAuthorizationConsumeVoucherResponse.getStatusCode());
        preAuthorizationConsumeVoucherResult.setMarketingMsg(preAuthorizationConsumeVoucherResponse.getMarketingMsg());
        if (preAuthorizationConsumeVoucherResponse.getAmount() != null) {
            preAuthorizationConsumeVoucherResult.setAmount(preAuthorizationConsumeVoucherResponse.getAmount().doubleValue());
        }
        preAuthorizationConsumeVoucherResult.setPreAuthOperationID(preAuthorizationConsumeVoucherResponse.getPreAuthOperationID());

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", preAuthorizationConsumeVoucherResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("messageCode", preAuthorizationConsumeVoucherResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", preAuthorizationConsumeVoucherResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("marketingMsg", preAuthorizationConsumeVoucherResult.getMarketingMsg()));
        if (preAuthorizationConsumeVoucherResult.getAmount() != null) {
            outputParameters.add(new Pair<String, String>("amount", preAuthorizationConsumeVoucherResult.getAmount().toString()));
        }
        else {
            outputParameters.add(new Pair<String, String>("amount", "null"));
        }
        outputParameters.add(new Pair<String, String>("preAuthOperationID", preAuthorizationConsumeVoucherResult.getPreAuthOperationID()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "preAuthorizationConsumeVoucher", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return preAuthorizationConsumeVoucherResult;

    }

    @Override
    public CancelPreAuthorizationConsumeVoucherResult cancelPreAuthorizationConsumeVoucher(String operationID, String operationIDtoReverse, PartnerType partnerType,
            Long requestTimestamp) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("operationIDtoReverse", operationIDtoReverse));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "cancelPreAuthorizationConsumeVoucher", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        CancelPreAuthorizationConsumeVoucherResult cancelPreAuthorizationConsumeVoucherResult = new CancelPreAuthorizationConsumeVoucherResult();
        CancelPreAuthorizationConsumeVoucherRequest cancelPreAuthorizationConsumeVoucherRequest = new CancelPreAuthorizationConsumeVoucherRequest();
        CancelPreAuthorizationConsumeVoucherResponse cancelPreAuthorizationConsumeVoucherResponse = new CancelPreAuthorizationConsumeVoucherResponse();

        cancelPreAuthorizationConsumeVoucherRequest.setOperationID(operationID);
        cancelPreAuthorizationConsumeVoucherRequest.setOperationIDtoReverse(operationIDtoReverse);
        cancelPreAuthorizationConsumeVoucherRequest.setPartnerType(partnerType.getValue());
        cancelPreAuthorizationConsumeVoucherRequest.setRequestTimestamp(requestTimestamp);

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();
            cancelPreAuthorizationConsumeVoucherResponse = eniWsImpl.cancelPreAuthorizationConsumeVoucher(cancelPreAuthorizationConsumeVoucherRequest);

        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "cancelPreAuthorizationConsumeVoucher", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        cancelPreAuthorizationConsumeVoucherResult.setCsTransactionID(cancelPreAuthorizationConsumeVoucherResponse.getCsTransactionID());
        cancelPreAuthorizationConsumeVoucherResult.setMessageCode(cancelPreAuthorizationConsumeVoucherResponse.getMessageCode());
        cancelPreAuthorizationConsumeVoucherResult.setStatusCode(cancelPreAuthorizationConsumeVoucherResponse.getStatusCode());

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", cancelPreAuthorizationConsumeVoucherResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("messageCode", cancelPreAuthorizationConsumeVoucherResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", cancelPreAuthorizationConsumeVoucherResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "cancelPreAuthorizationConsumeVoucher", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return cancelPreAuthorizationConsumeVoucherResult;

    }

    @Override
    public CreateVoucherResult createVoucher(VoucherConsumerType voucherType, BigDecimal totalAmount, String bankTransactionID, String shopTransactionID, String authorizationCode,
            String operationID, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("voucherType", voucherType.getValue()));
        inputParameters.add(new Pair<String, String>("totalAmount", totalAmount.toString()));
        inputParameters.add(new Pair<String, String>("bankTransactionID", bankTransactionID));
        inputParameters.add(new Pair<String, String>("shopTransactionID", shopTransactionID));
        inputParameters.add(new Pair<String, String>("authorizationCode", authorizationCode));
        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createVoucher", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateVoucherResult createVoucherResult = new CreateVoucherResult();
        CreateVoucherRequest createVoucherRequest = new CreateVoucherRequest();
        CreateVoucherResponse createVoucherResponse = new CreateVoucherResponse();

        createVoucherRequest.setVoucherType(voucherType.getValue());
        createVoucherRequest.setTotalAmount(totalAmount);
        createVoucherRequest.setBankTransactionID(bankTransactionID);
        createVoucherRequest.setShopTransactionID(shopTransactionID);
        createVoucherRequest.setOperationID(operationID);
        createVoucherRequest.setPartnerType(partnerType.getValue());
        createVoucherRequest.setRequestTimestamp(requestTimestamp);
        createVoucherRequest.setAuthorizationCode(authorizationCode);

        String simulationCreateVoucherError = null;
        boolean simulationCreateVoucherKO = false;

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            if (simulationActive) {

                simulationCreateVoucherError = parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_CREATEVOUCHER_ERROR);
                simulationCreateVoucherKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_CREATEVOUCHER_KO));

                if (simulationCreateVoucherError != null && simulationCreateVoucherError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }
                if (simulationCreateVoucherKO) {
                    createVoucherResult.setStatusCode(FidelityResponse.CREATE_VOUCHER_MAX_LIMIT_REACHED);
                    createVoucherResult.setMessageCode("Simulazione di KO");
                    return createVoucherResult;
                }
            }

            createVoucherResponse = eniWsImpl.createVoucher(createVoucherRequest);

            if (simulationActive) {
                if (simulationCreateVoucherError != null && simulationCreateVoucherError.equals("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createVoucher", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }
        VoucherDetail voucherDetail = new VoucherDetail();

        if (createVoucherResponse != null) {

            createVoucherResult.setCsTransactionID(createVoucherResponse.getCsTransactionID());
            createVoucherResult.setMessageCode(createVoucherResponse.getMessageCode());
            createVoucherResult.setStatusCode(createVoucherResponse.getStatusCode());

            if (createVoucherResponse.getVoucher() != null) {
                voucherDetail.setConsumedValue(createVoucherResponse.getVoucher().getConsumedValue().doubleValue());
                voucherDetail.setExpirationDate(createVoucherResponse.getVoucher().getExpirationDate().toGregorianCalendar().getTime());
                voucherDetail.setInitialValue(createVoucherResponse.getVoucher().getInitialValue().doubleValue());
                voucherDetail.setIsCombinable(createVoucherResponse.getVoucher().getIsCombinable());
                voucherDetail.setMinAmount(createVoucherResponse.getVoucher().getMinAmount().doubleValue());
                voucherDetail.setMinQuantity(createVoucherResponse.getVoucher().getMinQuantity().doubleValue());
                voucherDetail.setPromoCode(createVoucherResponse.getVoucher().getPromoCode());
                voucherDetail.setPromoDescription(createVoucherResponse.getVoucher().getPromoDescrition());
                voucherDetail.setPromoDoc(createVoucherResponse.getVoucher().getPromoDoc());
                voucherDetail.setPromoPartner(createVoucherResponse.getVoucher().getPromoPartner());
                /*
                 * List<ProductDetail> productDetailList = new ArrayList<ProductDetail>(0);
                 * for (Product item : createVoucherResponse.getVoucher().getValidProduct()) {
                 * ProductDetail productDetail = new ProductDetail();
                 * productDetail.setAmount(item.getAmount().doubleValue());
                 * productDetail.setProductCode(item.getProductCode());
                 * productDetail.setQuantity(item.getQuantity().doubleValue());
                 * productDetailList.add(productDetail);
                 * }
                 * 
                 * voucherDetail.setValidProduct(productDetailList);
                 */
                voucherDetail.setValidPV(createVoucherResponse.getVoucher().getValidPV());
                voucherDetail.setVoucherBalanceDue(createVoucherResponse.getVoucher().getVoucherBalanceDue().doubleValue());
                voucherDetail.setVoucherCode(createVoucherResponse.getVoucher().getVoucherCode());
                voucherDetail.setVoucherStatus(createVoucherResponse.getVoucher().getVoucherStatus());
                voucherDetail.setVoucherType(createVoucherResponse.getVoucher().getVoucherType());
                voucherDetail.setVoucherValue(createVoucherResponse.getVoucher().getVoucherValue().doubleValue());
                /*
                 * List<RefuelMode> refuelModeList = new ArrayList<RefuelMode>(0);
                 * 
                 * for (com.techedge.mp.quenit.types.RefuelMode item : createVoucherResponse.getVoucher().getValidRefuelMode()) {
                 * RefuelMode refuelMode = RefuelMode.valueOf(item.getRefuelMode());
                 * refuelModeList.add(refuelMode);
                 * }
                 * 
                 * voucherDetail.setValidRefuelMode(refuelModeList);
                 */
            }
            createVoucherResult.setVoucher(voucherDetail);

        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", createVoucherResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("statusCode", createVoucherResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("messageCode", createVoucherResult.getMessageCode()));

        String voucherToString = "";
        VoucherDetail voucher = createVoucherResult.getVoucher();

        if (voucher != null) {
            voucherToString = "{";
            voucherToString += "VoucherCode: " + voucher.getVoucherCode();
            voucherToString += "  VoucherStatus: " + voucher.getVoucherStatus();
            voucherToString += "  VoucherType: " + voucher.getVoucherType();
            voucherToString += "  VoucherStatus: " + voucher.getVoucherStatus();
            voucherToString += "  ExpirationDate: " + voucher.getExpirationDate();
            voucherToString += "  VoucherBalanceDue: " + voucher.getVoucherBalanceDue();
            voucherToString += "  InitialValue: " + voucher.getInitialValue();
            voucherToString += "  VoucherValue: " + voucher.getVoucherValue();
            voucherToString += "  MinAmount: " + voucher.getMinAmount();
            voucherToString += "  MinQuantity: " + voucher.getMinQuantity();
            voucherToString += "  ConsumedValue: " + voucher.getConsumedValue();
            voucherToString += "  IsCombinable: " + voucher.getIsCombinable();
            voucherToString += "  PromoCode: " + voucher.getPromoCode();
            voucherToString += "  PromoDescription: " + voucher.getPromoDescription();
            voucherToString += "  PromoDoc: " + voucher.getPromoDoc();
            voucherToString += "  PromoPartner: " + voucher.getPromoPartner();
            voucherToString += "}";
        }

        outputParameters.add(new Pair<String, String>("voucher", voucherToString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createVoucher", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createVoucherResult;

    }

    @Override
    public DeleteVoucherResult deleteVoucher(String operationIDtoReverse, String operationID, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("operationIDtoReverse", operationIDtoReverse));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "deleteVoucher", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        DeleteVoucherResult deleteVoucherResult = new DeleteVoucherResult();
        DeleteVoucherRequest deleteVoucherRequest = new DeleteVoucherRequest();
        DeleteVoucherResponse deleteVoucherResponse = new DeleteVoucherResponse();

        deleteVoucherRequest.setOperationID(operationID);
        deleteVoucherRequest.setOperationIDtoReverse(operationIDtoReverse);
        deleteVoucherRequest.setPartnerType(partnerType.getValue());
        deleteVoucherRequest.setRequestTimestamp(requestTimestamp);

        String simulationDeleteVoucherError = null;
        boolean simulationDeleteVoucherKO = false;

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            if (simulationActive) {

                simulationDeleteVoucherError = parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_DELETEVOUCHER_ERROR);
                simulationDeleteVoucherKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_DELETEVOUCHER_KO));

                if (simulationDeleteVoucherError != null && simulationDeleteVoucherError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }

                if (simulationDeleteVoucherKO) {
                    deleteVoucherResult.setStatusCode(FidelityResponse.DELETE_VOUCHER_MAX_LIMIT_REACHED);
                    deleteVoucherResult.setMessageCode("Simulazione di KO");
                    return deleteVoucherResult;
                }
            }

            deleteVoucherResponse = eniWsImpl.deleteVoucher(deleteVoucherRequest);

            if (simulationActive) {
                if (simulationDeleteVoucherError != null && simulationDeleteVoucherError.equalsIgnoreCase("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "deleteVoucher", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        deleteVoucherResult.setCsTransactionID(deleteVoucherResponse.getCsTransactionID());
        deleteVoucherResult.setMessageCode(deleteVoucherResponse.getMessageCode());
        deleteVoucherResult.setStatusCode(deleteVoucherResponse.getStatusCode());

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", deleteVoucherResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("messageCode", deleteVoucherResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", deleteVoucherResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "deleteVoucher", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return deleteVoucherResult;
    }

    public InfoRedemptionResult infoRedemption(String operationID, PartnerType partnerType, Long requestTimestamp, String fiscalCode) throws FidelityServiceException {
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "infoRedemption", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        InfoRedemptionRequest infoRedemptionRequest = new InfoRedemptionRequest();
        InfoRedemptionResponse infoRedemptionResponse = new InfoRedemptionResponse();
        InfoRedemptionResult infoRedemptionResult = new InfoRedemptionResult();

        infoRedemptionRequest.setOperationID(operationID);
        infoRedemptionRequest.setFiscalCode(fiscalCode);
        infoRedemptionRequest.setPartnerType(partnerType.getValue());
        infoRedemptionRequest.setRequestTimestamp(requestTimestamp);

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            infoRedemptionResponse = eniWsImpl.infoRedemption(infoRedemptionRequest);
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "infoRedemption", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        infoRedemptionResult.setCsTransactionID(infoRedemptionResponse.getCsTransactionID());
        infoRedemptionResult.setMessageCode(infoRedemptionResponse.getMessageCode());
        infoRedemptionResult.setStatusCode(infoRedemptionResponse.getStatusCode());

        if (infoRedemptionResponse.getRedemptionList() != null) {
            for (Redemption redemption : infoRedemptionResponse.getRedemptionList()) {
                RedemptionDetail redemptionDetail = new RedemptionDetail();
                redemptionDetail.setAmount(new Double(redemption.getRedemptionAmount().toString()));
                redemptionDetail.setCode(new Integer(redemption.getRedemptionCode().toString()));
                redemptionDetail.setName(redemption.getRedemptionName());
                redemptionDetail.setPoints(new Integer(redemption.getRedemptionPoints().toString()));
                infoRedemptionResult.getRedemptionList().add(redemptionDetail);
            }
        }
        
        if (infoRedemptionResponse.getCategoryRedemptionList() != null) {
            for(CategoryRedemption categoryRedemption : infoRedemptionResponse.getCategoryRedemptionList()) {
                CategoryRedemptionDetail categoryRedemptionDetail = new CategoryRedemptionDetail();
                categoryRedemptionDetail.setCategoryCode(categoryRedemption.getCategoryCode());
                for (Redemption redemption : categoryRedemption.getRedemptionList()) {
                    RedemptionDetail redemptionDetail = new RedemptionDetail();
                    redemptionDetail.setAmount(new Double(redemption.getRedemptionAmount().toString()));
                    redemptionDetail.setCode(new Integer(redemption.getRedemptionCode().toString()));
                    redemptionDetail.setName(redemption.getRedemptionName());
                    redemptionDetail.setPoints(new Integer(redemption.getRedemptionPoints().toString()));
                    categoryRedemptionDetail.getRedemptionList().add(redemptionDetail);
                }
                infoRedemptionResult.getCategoryRedemptionList().add(categoryRedemptionDetail);
            }
            
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", infoRedemptionResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("messageCode", infoRedemptionResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", infoRedemptionResult.getStatusCode()));

        String redemptionListString = "";

        for (RedemptionDetail redemptionDetail : infoRedemptionResult.getRedemptionList()) {
            String redemptionString = "{ code: " + redemptionDetail.getCode() + ", amount: " + redemptionDetail.getAmount() + ", name: " + redemptionDetail.getName()
                    + ", points: " + redemptionDetail.getPoints() + " }";

            redemptionListString = redemptionListString + redemptionString;
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "infoRedemption", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return infoRedemptionResult;
    }

    @Override
    public RedemptionResult redemption(String operationID, PartnerType partnerType, Long requestTimestamp, String fiscalCode, Integer redemptionCode)
            throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));
        inputParameters.add(new Pair<String, String>("redemptionCode", redemptionCode != null ? redemptionCode.toString() : ""));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "redemption", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        RedemptionRequest redemptionRequest = new RedemptionRequest();
        RedemptionResponse redemptionResponse = new RedemptionResponse();
        RedemptionResult redemptionResult = new RedemptionResult();

        redemptionRequest.setOperationID(operationID);
        redemptionRequest.setFiscalCode(fiscalCode);
        redemptionRequest.setPartnerType(partnerType.getValue());
        redemptionRequest.setRequestTimestamp(requestTimestamp);

        if (redemptionCode != null) {
            redemptionRequest.setRedemptionCode(new BigInteger(redemptionCode.toString()));
        }

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            redemptionResponse = eniWsImpl.redemption(redemptionRequest);
        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "redemption", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }

        redemptionResult.setCsTransactionID(redemptionResponse.getCsTransactionID());
        redemptionResult.setMessageCode(redemptionResponse.getMessageCode());
        redemptionResult.setStatusCode(redemptionResponse.getStatusCode());
        redemptionResult.setWarningMsg(redemptionResponse.getWarningMsg());
        redemptionResult.setMarketingMsg(redemptionResponse.getMarketingMsg());

        if (redemptionResponse.getBalance() != null) {
            redemptionResult.setBalance(new Double(redemptionResponse.getBalance().toString()));
        }

        if (redemptionResponse.getBalanceAmount() != null) {
            redemptionResult.setBalanceAmount(new Double(redemptionResponse.getBalanceAmount().toString()));
        }

        if (redemptionResponse.getCredits() != null) {
            redemptionResult.setCredits(new Integer(redemptionResponse.getCredits().toString()));
        }

        if (redemptionResponse.getRedemptionCode() != null) {
            redemptionResult.setRedemptionCode(new Integer(redemptionResponse.getRedemptionCode().toString()));
        }

        if (redemptionResponse.getVoucher() != null) {
            VoucherDetail voucherDetail = new VoucherDetail();
            Voucher voucher = redemptionResponse.getVoucher();

            voucherDetail.setIsCombinable(voucher.getIsCombinable());
            voucherDetail.setPromoCode(voucher.getPromoCode());
            voucherDetail.setPromoDescription(voucher.getPromoDescrition());
            voucherDetail.setPromoDoc(voucher.getPromoDoc());
            voucherDetail.setPromoPartner(voucher.getPromoPartner());
            voucherDetail.setValidPV(voucher.getValidPV());
            voucherDetail.setVoucherCode(voucher.getVoucherCode());
            voucherDetail.setVoucherStatus(voucher.getVoucherStatus());
            voucherDetail.setVoucherType(voucher.getVoucherType());

            if (voucher.getConsumedValue() != null) {
                voucherDetail.setConsumedValue(new Double(voucher.getConsumedValue().toString()));
            }

            if (voucher.getExpirationDate() != null) {
                voucherDetail.setExpirationDate(voucher.getExpirationDate().toGregorianCalendar().getTime());
            }

            if (voucher.getInitialValue() != null) {
                voucherDetail.setInitialValue(new Double(voucher.getInitialValue().toString()));
            }

            if (voucher.getMinAmount() != null) {}
            voucherDetail.setMinAmount(new Double(voucher.getMinAmount().toString()));

            if (voucher.getMinQuantity() != null) {
                voucherDetail.setMinQuantity(new Double(voucher.getMinQuantity().toString()));
            }

            if (voucher.getPromoCode() != null) {
                voucherDetail.setMinQuantity(new Double(voucher.getMinQuantity().toString()));
            }

            if (voucher.getVoucherBalanceDue() != null) {
                voucherDetail.setVoucherBalanceDue(new Double(voucher.getVoucherBalanceDue().toString()));
            }

            if (voucher.getVoucherValue() != null) {
                voucherDetail.setVoucherValue(new Double(voucher.getVoucherValue().toString()));
            }

            redemptionResult.setVoucher(voucherDetail);
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", redemptionResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("messageCode", redemptionResult.getMessageCode()));
        outputParameters.add(new Pair<String, String>("statusCode", redemptionResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("warningMsg", redemptionResult.getWarningMsg()));

        String voucherString = "";

        if (redemptionResult.getVoucher() != null) {
            VoucherDetail voucherDetail = redemptionResult.getVoucher();

            voucherString = "{ consumedValue: " + voucherDetail.getConsumedValue() + ", expirationDate: " + voucherDetail.getExpirationDate().toString() + ", initialValue: "
                    + voucherDetail.getInitialValue().toString() + ", promoCode: " + voucherDetail.getPromoCode() + ", promoDescription: " + voucherDetail.getPromoDescription()
                    + ", promoDoc: " + voucherDetail.getPromoDoc() + ", voucherBalanceDue: " + voucherDetail.getVoucherBalanceDue().toString() + ", voucherCode: "
                    + voucherDetail.getVoucherCode() + ", voucherStatus: " + voucherDetail.getVoucherStatus() + ", voucherType: " + voucherDetail.getVoucherType()
                    + ", voucherValue: " + voucherDetail.getVoucherValue().toString() + " }";
        }

        outputParameters.add(new Pair<String, String>("voucher", voucherString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "redemption", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return redemptionResult;
    }

    public CreateVoucherPromotionalResult createVoucherPromotional(String operationID, VoucherConsumerType voucherType, PartnerType partnerType, Long requestTimestamp,
            String fiscalCode, String promoCode, BigDecimal totalAmount) throws FidelityServiceException {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("voucherType", voucherType.getValue()));
        inputParameters.add(new Pair<String, String>("totalAmount", totalAmount.toString()));
        inputParameters.add(new Pair<String, String>("operationID", operationID));
        inputParameters.add(new Pair<String, String>("partnerType", partnerType.getValue()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));
        inputParameters.add(new Pair<String, String>("promoCode", promoCode));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createVoucherPromotional", operationID, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateVoucherPromotionalResult createVoucherResult = new CreateVoucherPromotionalResult();
        CreateVoucherPromotionalRequest createVoucherRequest = new CreateVoucherPromotionalRequest();
        CreateVoucherPromotionalResponse createVoucherResponse = new CreateVoucherPromotionalResponse();

        createVoucherRequest.setVoucherType(voucherType.getValue());
        createVoucherRequest.setTotalAmount(totalAmount);
        createVoucherRequest.setOperationID(operationID);
        createVoucherRequest.setPartnerType(partnerType.getValue());
        createVoucherRequest.setRequestTimestamp(requestTimestamp);
        createVoucherRequest.setFiscalCode(fiscalCode);
        createVoucherRequest.setPromoCode(promoCode);

        String simulationCreateVoucherPromotionalError = null;
        boolean simulationCreateVoucherPromotionalKO = false;

        try {
            EniWsImpl eniWsImpl = getEniWsImpl();

            if (simulationActive) {

                simulationCreateVoucherPromotionalError = parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_CREATEVOUCHERPROMOTIONAL_ERROR);
                simulationCreateVoucherPromotionalKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(FidelityService.PARAM_SIMULATION_CREATEVOUCHERPROMOTIONAL_KO));

                if (simulationCreateVoucherPromotionalError != null && simulationCreateVoucherPromotionalError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }

                if (simulationCreateVoucherPromotionalKO) {
                    createVoucherResult.setStatusCode(FidelityResponse.CREATE_VOUCHER_PROMOTIONAL_GENERIC_ERROR);
                    createVoucherResult.setMessageCode("Simulazione di KO");
                    return createVoucherResult;
                }
            }

            createVoucherResponse = eniWsImpl.createVoucherPromotional(createVoucherRequest);

            if (simulationActive) {
                if (simulationCreateVoucherPromotionalError != null && simulationCreateVoucherPromotionalError.equalsIgnoreCase("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }

        }
        catch (Exception ex) {
            loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createVoucherPromotional", operationID, "closing", ex.getMessage());
            throw new FidelityServiceException(ex.getMessage(), ex);
        }
        VoucherDetail voucherDetail = new VoucherDetail();

        if (createVoucherResponse != null) {

            createVoucherResult.setCsTransactionID(createVoucherResponse.getCsTransactionID());
            createVoucherResult.setMessageCode(createVoucherResponse.getMessageCode());
            createVoucherResult.setStatusCode(createVoucherResponse.getStatusCode());

            if (createVoucherResponse.getVoucher() != null) {
                voucherDetail.setConsumedValue(createVoucherResponse.getVoucher().getConsumedValue().doubleValue());
                voucherDetail.setExpirationDate(createVoucherResponse.getVoucher().getExpirationDate().toGregorianCalendar().getTime());
                voucherDetail.setInitialValue(createVoucherResponse.getVoucher().getInitialValue().doubleValue());
                voucherDetail.setIsCombinable(createVoucherResponse.getVoucher().getIsCombinable());
                voucherDetail.setMinAmount(createVoucherResponse.getVoucher().getMinAmount().doubleValue());
                voucherDetail.setMinQuantity(createVoucherResponse.getVoucher().getMinQuantity().doubleValue());
                voucherDetail.setPromoCode(createVoucherResponse.getVoucher().getPromoCode());
                voucherDetail.setPromoDescription(createVoucherResponse.getVoucher().getPromoDescrition());
                voucherDetail.setPromoDoc(createVoucherResponse.getVoucher().getPromoDoc());
                voucherDetail.setPromoPartner(createVoucherResponse.getVoucher().getPromoPartner());
                /*
                 * List<ProductDetail> productDetailList = new ArrayList<ProductDetail>(0);
                 * for (Product item : createVoucherResponse.getVoucher().getValidProduct()) {
                 * ProductDetail productDetail = new ProductDetail();
                 * productDetail.setAmount(item.getAmount().doubleValue());
                 * productDetail.setProductCode(item.getProductCode());
                 * productDetail.setQuantity(item.getQuantity().doubleValue());
                 * productDetailList.add(productDetail);
                 * }
                 * 
                 * voucherDetail.setValidProduct(productDetailList);
                 */
                voucherDetail.setValidPV(createVoucherResponse.getVoucher().getValidPV());
                voucherDetail.setVoucherBalanceDue(createVoucherResponse.getVoucher().getVoucherBalanceDue().doubleValue());
                voucherDetail.setVoucherCode(createVoucherResponse.getVoucher().getVoucherCode());
                voucherDetail.setVoucherStatus(createVoucherResponse.getVoucher().getVoucherStatus());
                voucherDetail.setVoucherType(createVoucherResponse.getVoucher().getVoucherType());
                voucherDetail.setVoucherValue(createVoucherResponse.getVoucher().getVoucherValue().doubleValue());
                /*
                 * List<RefuelMode> refuelModeList = new ArrayList<RefuelMode>(0);
                 * 
                 * for (com.techedge.mp.quenit.types.RefuelMode item : createVoucherResponse.getVoucher().getValidRefuelMode()) {
                 * RefuelMode refuelMode = RefuelMode.valueOf(item.getRefuelMode());
                 * refuelModeList.add(refuelMode);
                 * }
                 * 
                 * voucherDetail.setValidRefuelMode(refuelModeList);
                 */
            }
            createVoucherResult.setVoucher(voucherDetail);

        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("csTransactionID", createVoucherResult.getCsTransactionID()));
        outputParameters.add(new Pair<String, String>("statusCode", createVoucherResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("messageCode", createVoucherResult.getMessageCode()));

        String voucherToString = "";
        VoucherDetail voucher = createVoucherResult.getVoucher();

        if (voucher != null) {
            voucherToString = "{";
            voucherToString += "VoucherCode: " + voucher.getVoucherCode();
            voucherToString += "  VoucherStatus: " + voucher.getVoucherStatus();
            voucherToString += "  VoucherType: " + voucher.getVoucherType();
            voucherToString += "  VoucherStatus: " + voucher.getVoucherStatus();
            voucherToString += "  ExpirationDate: " + voucher.getExpirationDate();
            voucherToString += "  VoucherBalanceDue: " + voucher.getVoucherBalanceDue();
            voucherToString += "  InitialValue: " + voucher.getInitialValue();
            voucherToString += "  VoucherValue: " + voucher.getVoucherValue();
            voucherToString += "  MinAmount: " + voucher.getMinAmount();
            voucherToString += "  MinQuantity: " + voucher.getMinQuantity();
            voucherToString += "  ConsumedValue: " + voucher.getConsumedValue();
            voucherToString += "  IsCombinable: " + voucher.getIsCombinable();
            voucherToString += "  PromoCode: " + voucher.getPromoCode();
            voucherToString += "  PromoDescription: " + voucher.getPromoDescription();
            voucherToString += "  PromoDoc: " + voucher.getPromoDoc();
            voucherToString += "  PromoPartner: " + voucher.getPromoPartner();
            voucherToString += "}";
        }

        outputParameters.add(new Pair<String, String>("voucher", voucherToString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createVoucherPromotional", operationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return createVoucherResult;
    }

}
