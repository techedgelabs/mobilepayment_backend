
/**
 * ParkingService_EstimateEndParkingPrice_InvalidFormatFault_FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

package com.techedge.mp.parking.integration.wsclient;

public class ParkingService_EstimateEndParkingPrice_InvalidFormatFault_FaultMessage extends java.lang.Exception{

    private static final long serialVersionUID = 1523263023357L;
    
    private com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.InvalidFormatFaultE faultMessage;

    
        public ParkingService_EstimateEndParkingPrice_InvalidFormatFault_FaultMessage() {
            super("ParkingService_EstimateEndParkingPrice_InvalidFormatFault_FaultMessage");
        }

        public ParkingService_EstimateEndParkingPrice_InvalidFormatFault_FaultMessage(java.lang.String s) {
           super(s);
        }

        public ParkingService_EstimateEndParkingPrice_InvalidFormatFault_FaultMessage(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ParkingService_EstimateEndParkingPrice_InvalidFormatFault_FaultMessage(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.InvalidFormatFaultE msg){
       faultMessage = msg;
    }
    
    public com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.InvalidFormatFaultE getFaultMessage(){
       return faultMessage;
    }
}
    