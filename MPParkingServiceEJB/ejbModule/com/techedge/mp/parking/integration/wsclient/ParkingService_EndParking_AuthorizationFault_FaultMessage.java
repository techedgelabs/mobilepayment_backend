
/**
 * ParkingService_EndParking_AuthorizationFault_FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

package com.techedge.mp.parking.integration.wsclient;

public class ParkingService_EndParking_AuthorizationFault_FaultMessage extends java.lang.Exception{

    private static final long serialVersionUID = 1523263023244L;
    
    private com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.AuthorizationFaultE faultMessage;

    
        public ParkingService_EndParking_AuthorizationFault_FaultMessage() {
            super("ParkingService_EndParking_AuthorizationFault_FaultMessage");
        }

        public ParkingService_EndParking_AuthorizationFault_FaultMessage(java.lang.String s) {
           super(s);
        }

        public ParkingService_EndParking_AuthorizationFault_FaultMessage(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ParkingService_EndParking_AuthorizationFault_FaultMessage(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.AuthorizationFaultE msg){
       faultMessage = msg;
    }
    
    public com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.AuthorizationFaultE getFaultMessage(){
       return faultMessage;
    }
}
    