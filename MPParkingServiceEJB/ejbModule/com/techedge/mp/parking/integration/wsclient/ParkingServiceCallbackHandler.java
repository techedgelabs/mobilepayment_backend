
/**
 * ParkingServiceCallbackHandler.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

    package com.techedge.mp.parking.integration.wsclient;

    /**
     *  ParkingServiceCallbackHandler Callback class, Users can extend this class and implement
     *  their own receiveResult and receiveError methods.
     */
    public abstract class ParkingServiceCallbackHandler{



    protected Object clientData;

    /**
    * User can pass in any object that needs to be accessed once the NonBlocking
    * Web service call is finished and appropriate method of this CallBack is called.
    * @param clientData Object mechanism by which the user can pass in user data
    * that will be avilable at the time this callback is called.
    */
    public ParkingServiceCallbackHandler(Object clientData){
        this.clientData = clientData;
    }

    /**
    * Please use this constructor if you don't want to set any clientData
    */
    public ParkingServiceCallbackHandler(){
        this.clientData = null;
    }

    /**
     * Get the client data
     */

     public Object getClientData() {
        return clientData;
     }

        
           /**
            * auto generated Axis2 call back method for extendParking method
            * override this method for handling normal response from extendParking operation
            */
           public void receiveResultextendParking(
                    com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.ExtendParkingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from extendParking operation
           */
            public void receiveErrorextendParking(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for estimateExtendedParkingPrice method
            * override this method for handling normal response from estimateExtendedParkingPrice operation
            */
           public void receiveResultestimateExtendedParkingPrice(
                    com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateExtendedParkingPriceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from estimateExtendedParkingPrice operation
           */
            public void receiveErrorestimateExtendedParkingPrice(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for endParking method
            * override this method for handling normal response from endParking operation
            */
           public void receiveResultendParking(
                    com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EndParkingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from endParking operation
           */
            public void receiveErrorendParking(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for startParking method
            * override this method for handling normal response from startParking operation
            */
           public void receiveResultstartParking(
                    com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.StartParkingResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from startParking operation
           */
            public void receiveErrorstartParking(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getCities method
            * override this method for handling normal response from getCities operation
            */
           public void receiveResultgetCities(
                    com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetCitiesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getCities operation
           */
            public void receiveErrorgetCities(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for estimateParkingPrice method
            * override this method for handling normal response from estimateParkingPrice operation
            */
           public void receiveResultestimateParkingPrice(
                    com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateParkingPriceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from estimateParkingPrice operation
           */
            public void receiveErrorestimateParkingPrice(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getParkingInfoHere method
            * override this method for handling normal response from getParkingInfoHere operation
            */
           public void receiveResultgetParkingInfoHere(
                    com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetParkingInfoHereResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getParkingInfoHere operation
           */
            public void receiveErrorgetParkingInfoHere(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for getParkingZones method
            * override this method for handling normal response from getParkingZones operation
            */
           public void receiveResultgetParkingZones(
                    com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetParkingZonesResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from getParkingZones operation
           */
            public void receiveErrorgetParkingZones(java.lang.Exception e) {
            }
                
           /**
            * auto generated Axis2 call back method for estimateEndParkingPrice method
            * override this method for handling normal response from estimateEndParkingPrice operation
            */
           public void receiveResultestimateEndParkingPrice(
                    com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateEndParkingPriceResponse result
                        ) {
           }

          /**
           * auto generated Axis2 Error handler
           * override this method for handling error response from estimateEndParkingPrice operation
           */
            public void receiveErrorestimateEndParkingPrice(java.lang.Exception e) {
            }
                


    }
    