
/**
 * ParkingService_EstimateParkingPrice_NotImplementedFault_FaultMessage.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.6.4  Built on : Dec 28, 2015 (10:03:39 GMT)
 */

package com.techedge.mp.parking.integration.wsclient;

public class ParkingService_EstimateParkingPrice_NotImplementedFault_FaultMessage extends java.lang.Exception{

    private static final long serialVersionUID = 1523263023107L;
    
    private com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.NotImplementedFaultE faultMessage;

    
        public ParkingService_EstimateParkingPrice_NotImplementedFault_FaultMessage() {
            super("ParkingService_EstimateParkingPrice_NotImplementedFault_FaultMessage");
        }

        public ParkingService_EstimateParkingPrice_NotImplementedFault_FaultMessage(java.lang.String s) {
           super(s);
        }

        public ParkingService_EstimateParkingPrice_NotImplementedFault_FaultMessage(java.lang.String s, java.lang.Throwable ex) {
          super(s, ex);
        }

        public ParkingService_EstimateParkingPrice_NotImplementedFault_FaultMessage(java.lang.Throwable cause) {
            super(cause);
        }
    

    public void setFaultMessage(com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.NotImplementedFaultE msg){
       faultMessage = msg;
    }
    
    public com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.NotImplementedFaultE getFaultMessage(){
       return faultMessage;
    }
}
    