package com.techedge.mp.parking.integration.business.auth;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Calendar;
import java.util.Date;

public class Oauth2ParkingServiceTokenExchanger implements Serializable {

    /**
     * 
     */
    private static final long                         serialVersionUID = -4301349399646367863L;
    private static Oauth2ParkingServiceJWTExchanger   oauth2ParkingServiceJWTExchanger;
    private static Oauth2RequestTokenHelper           oauth2RequestTokenHelper;

    private static Oauth2ParkingServiceTokenExchanger instance         = null;

    private String                                    certificateFilePath;
    private String                                    certificatePassword;
    private String                                    issuer;
    private String                                    subject;
    private String                                    audience;
    private int                                       expirationInSeconds;
    private String                                    oauth2TokenEndpoint;
    private String                                    proxyHost;
    private String                                    proxyPort;

    public void setCertificateFilePath(String certificateFilePath) {
        this.certificateFilePath = certificateFilePath;
    }

    public void setCertificatePassword(String certificatePassword) {
        this.certificatePassword = certificatePassword;
    }

    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public void setExpirationInSeconds(int expirationInSeconds) {
        this.expirationInSeconds = expirationInSeconds;
    }

    public void setOauth2TokenEndpoint(String oauth2TokenEndpoint) {
        this.oauth2TokenEndpoint = oauth2TokenEndpoint;
    }

    public String getOauth2TokenEndpoint() {
        return oauth2TokenEndpoint;
    }

    public String getProxyHost() {
        return proxyHost;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public String getProxyPort() {
        return proxyPort;
    }

    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }

    protected Oauth2ParkingServiceTokenExchanger() {

    }

    public static Oauth2ParkingServiceTokenExchanger getInstance(String certificateFilePath, String certificatePassword, String KEY_ALIAS, String issuer, String subject,
            String audience, int expirationInSeconds, String oauth2TokenEndpoint, String proxyHost, String proxyPort) {
        if (instance == null) {
            instance = new Oauth2ParkingServiceTokenExchanger();
            instance.setAudience(audience);
            instance.setCertificateFilePath(certificateFilePath);
            instance.setCertificatePassword(certificatePassword);
            instance.setExpirationInSeconds(expirationInSeconds);
            instance.setIssuer(issuer);
            instance.setSubject(subject);
            instance.setOauth2TokenEndpoint(oauth2TokenEndpoint);
            instance.setProxyHost(proxyHost);
            instance.setProxyPort(proxyPort);

            oauth2ParkingServiceJWTExchanger = Oauth2ParkingServiceJWTExchanger.getInstance(certificateFilePath, certificatePassword, KEY_ALIAS);
            oauth2RequestTokenHelper = Oauth2RequestTokenHelper.getInstance(proxyHost, Integer.valueOf(proxyPort));

        }
        return instance;
    }

    public Authentication retrieveAuthenticationData() throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException, CertificateException, FileNotFoundException,
            IOException {

        Authentication authentication = null;

        Date issuedAt = Calendar.getInstance().getTime();
        authentication = oauth2RequestTokenHelper.checkAccessTokenState();
        if (authentication != null /*&& !authentication.isExpired()*/) {
            return authentication;
        }
        else {
            String jWTCompactString = oauth2ParkingServiceJWTExchanger.getJWTSigned(issuer, subject, audience, expirationInSeconds, issuedAt);
            Oauth2RequestTokenHelper.setOauth2TokenEndPoint(instance.getOauth2TokenEndpoint());
            authentication = Oauth2RequestTokenHelper.getAuthenticationData(jWTCompactString, instance.getProxyHost(), instance.getProxyPort());
            return authentication;
        }

    }

    public Authentication refreshAuthenticationData(String refreshToken) {
        Date issuedAt = Calendar.getInstance().getTime();
        Authentication authentication = null;
        String jWTCompactString;
        try {
            jWTCompactString = oauth2ParkingServiceJWTExchanger.getJWTSigned(issuer, subject, audience, expirationInSeconds, issuedAt);
            Oauth2RequestTokenHelper.setOauth2TokenEndPoint(instance.getOauth2TokenEndpoint());
            authentication = Oauth2RequestTokenHelper.refreshAuthenticationData(jWTCompactString, refreshToken, instance.getProxyHost(), instance.getProxyPort());

        }
        catch (UnrecoverableKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (KeyStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (CertificateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return authentication;
    }

}
