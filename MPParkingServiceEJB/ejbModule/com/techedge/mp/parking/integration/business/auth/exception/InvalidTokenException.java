package com.techedge.mp.parking.integration.business.auth.exception;

public class InvalidTokenException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    private String            errorCode;
    private String            errorDescription;

    public InvalidTokenException() {
        super();
    }

    public InvalidTokenException(String errorCode, String errorDescription) {
        super();
        this.errorCode = errorCode;
        this.errorDescription = errorDescription;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }

}
