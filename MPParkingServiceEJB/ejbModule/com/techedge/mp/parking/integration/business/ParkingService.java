package com.techedge.mp.parking.integration.business;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.net.ssl.SSLContext;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.core.business.utilities.StringUtils;
import com.techedge.mp.parking.integration.business.auth.Authentication;
import com.techedge.mp.parking.integration.business.auth.Oauth2ParkingServiceTokenExchanger;
import com.techedge.mp.parking.integration.business.auth.exception.MyCiceroTokenException;
import com.techedge.mp.parking.integration.business.auth.exception.InvalidStallCodeException;
import com.techedge.mp.parking.integration.business.auth.exception.InvalidTokenException;
import com.techedge.mp.parking.integration.business.auth.exception.ParkingNotFoundException;
import com.techedge.mp.parking.integration.business.auth.exception.PlateAlreadyInUseException;
import com.techedge.mp.parking.integration.business.interfaces.CitiesParkingZone;
import com.techedge.mp.parking.integration.business.interfaces.EndParkingResult;
import com.techedge.mp.parking.integration.business.interfaces.EstimateEndParkingPriceResult;
import com.techedge.mp.parking.integration.business.interfaces.EstimateExtendedParkingPriceResult;
import com.techedge.mp.parking.integration.business.interfaces.EstimateParkingPriceResult;
import com.techedge.mp.parking.integration.business.interfaces.ExtendParkingResult;
import com.techedge.mp.parking.integration.business.interfaces.GetCitiesResult;
import com.techedge.mp.parking.integration.business.interfaces.GetParkingZonesByCityResult;
import com.techedge.mp.parking.integration.business.interfaces.ParkingCity;
import com.techedge.mp.parking.integration.business.interfaces.ParkingZone;
import com.techedge.mp.parking.integration.business.interfaces.RetrieveParkingZonesResult;
import com.techedge.mp.parking.integration.business.interfaces.StartParkingResult;
import com.techedge.mp.parking.integration.business.interfaces.StatusCodeHelper;
import com.techedge.mp.parking.integration.business.interfaces.UserMyCiceroDataResult;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.City;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.CityParkingZones;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EndParking;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EndParkingInput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EndParkingOutput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EndParkingResponse;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateEndParkingPrice;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateEndParkingPriceInput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateEndParkingPriceOutput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateEndParkingPriceResponse;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateExtendedParkingPrice;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateExtendedParkingPriceInput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateExtendedParkingPriceOutput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateExtendedParkingPriceResponse;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateParkingPrice;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateParkingPriceInput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateParkingPriceOutput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.EstimateParkingPriceResponse;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.ExtendParking;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.ExtendParkingInput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.ExtendParkingOutput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.ExtendParkingResponse;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetCities;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetCitiesInput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetCitiesOutput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetCitiesResponse;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetParkingInfoHere;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetParkingInfoHereInput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetParkingInfoHereOutput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetParkingInfoHereResponse;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetParkingZones;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetParkingZonesInput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetParkingZonesOutput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.GetParkingZonesResponse;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.ParkingZonesList;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.StartParking;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.StartParkingInput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.StartParkingOutput;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.StartParkingResponse;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub.Zone;

/**
 * Session Bean implementation class ParkingService
 */
@Stateless
@LocalBean
public class ParkingService implements ParkingServiceRemote, ParkingServiceLocal {

    private ParametersServiceRemote            parametersService                           = null;
    private LoggerServiceRemote                loggerService                               = null;

    private final static String                PARAM_PROXY_HOST                            = "PROXY_HOST";
    private final static String                PARAM_PROXY_PORT                            = "PROXY_PORT";
    private final static String                PARAM_PROXY_NO_HOSTS                        = "PROXY_NO_HOSTS";
    private final static String                PARAM_PARKING_USERINFO_MYCICERO_ENDPOINT    ="PARKING_USERINFO_MYCICERO_ENDPOINT";
    private final static String                PARAM_CRM_URL_SERVER_INTERACT               = "CRM_URL_SERVER_INTERACT";
    private final static String                PARAM_CRM_DEBUG                             = "CRM_DEBUG";

    private final static String                PARAM_PARKING_CERT_FILE_PATH                = "PARKING_CERT_FILE_PATH";
    private final static String                PARAM_PARKING_JWT_ISSUER                    = "PARKING_JWT_ISSUER";
    private final static String                PARAM_PARKING_JWT_SUBJECT                   = "PARKING_JWT_SUBJECT";
    private final static String                PARAM_PARKING_JWT_AUDIENCE                  = "PARKING_JWT_AUDIENCE";
    private final static String                PARAM_PARKING_JWT_EXPIRATION_SECS           = "PARKING_JWT_EXPIRATION_SECS";
    private final static String                PARAM_PARKING_SECURITY_KEY_ALIAS            = "PARKING_SECURITY_KEY_ALIAS";
    private final static String                PARAM_OAUTH2_VAULT_BLOCK                    = "OAUTH2_VAULT_BLOCK";
    private final static String                PARAM_PARKING_OAUTH2_TOKEN_ENDPOINT         = "PARKING_OAUTH2_TOKEN_ENDPOINT";
    private final static String                PARAM_PARKING_RESOURCE_SERVER_ENDPOINT      = "PARKING_RESOURCE_SERVER_ENDPOINT";

    private final static String                PARAM_OAUTH2_VAULT_BLOCK_ATTRIBUTE_PASSWORD = "OAUTH2_VAULT_BLOCK_ATTRIBUTE_PASSWORD";
    private String                             certificatePassword;

    private String                             proxyHost                                   = "sprite.techedge.corp";
    private String                             proxyPort                                   = "3128";
    private String                             proxyNoHosts                                = "localhost|127.0.0.1|*.enimp.pri";
    private String                             myCicieroUserInfoEndPoint;

    private SSLContext                         sslContext;
    private Oauth2ParkingServiceTokenExchanger oauth2ParkingServiceTokenExchanger;
    private JsonClient                         jsonClient;
    private String                             resourceServerEndpoint;

    /**
     * Default constructor.
     */
    public ParkingService() {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            throw new EJBException(e);
        }

        try {
            this.proxyHost = parametersService.getParamValue(PARAM_PROXY_HOST);
            this.proxyPort = parametersService.getParamValue(PARAM_PROXY_PORT);
            this.proxyNoHosts = parametersService.getParamValue(PARAM_PROXY_NO_HOSTS);
            this.myCicieroUserInfoEndPoint = parametersService.getParamValue(PARAM_PARKING_USERINFO_MYCICERO_ENDPOINT);

            //this.URLEndpoint = parametersService.getParamValue(PARAM_CRM_URL_SERVER_INTERACT);
        }
        catch (ParameterNotFoundException ex) {
            System.err.println("Errore nella lettura dei parametri: " + ex.getMessage());
            /*
             * }
             * catch (Exception ex) {
             * System.err.println("Errore nella creazione della istanza  al server interact: " + ex.getMessage());
             */
        }

        try {
            jsonClient = JsonClient.getInstance(this.proxyHost, this.proxyPort, this.myCicieroUserInfoEndPoint);
        }
        catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }

        String certificateFilePath;
        try {
            certificateFilePath = parametersService.getParamValue(PARAM_PARKING_CERT_FILE_PATH);
            String issuer = parametersService.getParamValue(PARAM_PARKING_JWT_ISSUER);
            String subject = parametersService.getParamValue(PARAM_PARKING_JWT_SUBJECT);
            String audience = parametersService.getParamValue(PARAM_PARKING_JWT_AUDIENCE);
            int expirationInSeconds = Integer.valueOf(parametersService.getParamValue(PARAM_PARKING_JWT_EXPIRATION_SECS));
            String keyAlias = parametersService.getParamValue(PARAM_PARKING_SECURITY_KEY_ALIAS);
            String oauth2TokenEndpoint = parametersService.getParamValue(PARAM_PARKING_OAUTH2_TOKEN_ENDPOINT);
            resourceServerEndpoint = parametersService.getParamValue(PARAM_PARKING_RESOURCE_SERVER_ENDPOINT);

            String vaultBlock = "";
            String vaultBlockAttributeCryptKey = "";

            try {

                vaultBlock = parametersService.getParamValue(PARAM_OAUTH2_VAULT_BLOCK);
                vaultBlockAttributeCryptKey = parametersService.getParamValue(PARAM_OAUTH2_VAULT_BLOCK_ATTRIBUTE_PASSWORD);

            }
            catch (ParameterNotFoundException e) {
                System.err.println("Parameter not found: " + e.getMessage());
            }

            try {
                PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
                System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

                if (securityVault.isInitialized()) {

                    try {
                        this.certificatePassword = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                    }
                    catch (IllegalArgumentException ex) {
                        System.err.println("Error in security vault: " + ex.getMessage());
                    }
                }
                else {
                    System.err.println("VAULT NOT INITIALIZED!!!");
                }
            }
            catch (SecurityVaultException e) {
                System.err.println("Error in security vault: " + e.getMessage());
                //e.printStackTrace();
            }

            System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
            System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeCryptKey + "'");
            System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + this.certificatePassword + "'");

            oauth2ParkingServiceTokenExchanger = Oauth2ParkingServiceTokenExchanger.getInstance(certificateFilePath, certificatePassword, keyAlias, issuer, subject, audience,
                    expirationInSeconds, oauth2TokenEndpoint, proxyHost, proxyPort);
        }
        catch (ParameterNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @Override
    public GetCitiesResult getCities(String lang) {

        Date now = new Date();
        String requestId = String.valueOf(now.getTime());

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("lang", lang));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCities", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        GetCitiesResult getCitiesResult = new GetCitiesResult();

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        proxy.setHttp();
        Authentication auth = null;
        try {
            auth = this.oauth2ParkingServiceTokenExchanger.retrieveAuthenticationData();
            ParkingServiceStub stub = new ParkingServiceStub(resourceServerEndpoint);
            GetCities citiesRequest = new GetCities();
            GetCitiesInput input = new GetCitiesInput();
            input.setLang(lang);
            citiesRequest.setInput(input);

            GetCitiesResponse response = stub.getCities(auth.getAccess_token(), citiesRequest);
            GetCitiesOutput output = response.getGetCitiesResult();
            List<ParkingCity> parkingCities = new LinkedList<ParkingCity>();
            List<City> cities = new LinkedList<ParkingServiceStub.City>(Arrays.asList(output.getCity()));
            for (City city : cities) {
                ParkingCity parkingCity = new ParkingCity();

                parkingCity.setAdministrativeAreaLevel2Code(city.getAdministrativeAreaLevel2Code());
                parkingCity.setCityId(city.getId());
                parkingCity.setId(city.getId());
                parkingCity.setLatitude(city.getLatitude());
                parkingCity.setLongitude(city.getLongitude());
                parkingCity.setName(city.getName());
                parkingCity.setStickerRequired(city.getStickerRequired());
                parkingCity.setUrl(city.getUrl());
                parkingCities.add(parkingCity);
            }

            proxy.unsetHttp();
            getCitiesResult.setParkingCityList(parkingCities);

        }

        catch (InvalidTokenException invalidTokenException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getCities", "", "", "accessToken " + auth.getAccess_token() + " expired ");
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getCities", "", "", "requesting new accessToken ... at " + Calendar.getInstance().getTime());
            Authentication refreshedAuthentication = oauth2ParkingServiceTokenExchanger.refreshAuthenticationData(auth.getRefresh_token());
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getCities", "", "", "got new accessToken ... at " + refreshedAuthentication.getAccess_token());
            return getCities(lang);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getCities", null, "", ex.getMessage());

        }

        proxy.unsetHttp();

        String statusCode = StatusCodeHelper.RETRIEVE_PARKING_ZONES_OK;
        getCitiesResult.setStatusCode(statusCode);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", statusCode));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCities", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return getCitiesResult;
    }

    @Override
    public GetParkingZonesByCityResult getParkingZonesByCityResult(String lang, String cityId) {

        Date now = new Date();
        String requestId = String.valueOf(now.getTime());

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("lang", lang));
        inputParameters.add(new Pair<String, String>("cityId", cityId));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getParkingZonesByCityResult", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        GetParkingZonesByCityResult getParkingZonesByCityResult = new GetParkingZonesByCityResult();

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        proxy.setHttp();
        Authentication auth = null;
        try {
            auth = this.oauth2ParkingServiceTokenExchanger.retrieveAuthenticationData();
            ParkingServiceStub stub = new ParkingServiceStub(resourceServerEndpoint);

            GetParkingZones getParkingZonesRequest = new GetParkingZones();
            GetParkingZonesInput input = new GetParkingZonesInput();
            input.setCityId(cityId);
            input.setLang(lang);
            input.setIncludeShapes(false);
            getParkingZonesRequest.setInput(input);

            GetParkingZonesResponse response = stub.getParkingZones(auth.getAccess_token(), getParkingZonesRequest);
            GetParkingZonesOutput output = response.getGetParkingZonesResult();

            CitiesParkingZone citiesParkingZone = new CitiesParkingZone();
            City city = output.getCity();

            ParkingCity parkingCity = new ParkingCity();
            parkingCity.setAdministrativeAreaLevel2Code(city.getAdministrativeAreaLevel2Code());
            parkingCity.setCityId(city.getId());
            parkingCity.setId(city.getId());
            parkingCity.setLatitude(city.getLatitude());
            parkingCity.setLongitude(city.getLongitude());
            parkingCity.setName(city.getName());
            parkingCity.setStickerRequired(city.getStickerRequired());
            parkingCity.setUrl(city.getUrl());
            citiesParkingZone.setCity(parkingCity);

            if (output.getParkingZones() != null && output.getParkingZones().getZone() != null && output.getParkingZones().getZone().length > 0) {

                List<ParkingZone> parkingZones = new LinkedList<ParkingZone>();
                List<Zone> result = new LinkedList<Zone>(Arrays.asList(output.getParkingZones().getZone()));

                for (Zone zone : result) {
                    ParkingZone parkingZone = new ParkingZone();
                    parkingZone.setCityId(zone.getCityId());
                    parkingZone.setDescription(zone.getDescription());
                    parkingZone.setId(zone.getId());
                    parkingZone.setName(zone.getName());
                    parkingZone.setNotice(zone.getNotices().getString() != null ? new LinkedList<String>(Arrays.asList(zone.getNotices().getString())) : null);
                    parkingZone.setStallCodeRequired(zone.getStallCodeRequired());
                    parkingZone.setVendorId(zone.getVendorId());
                    parkingZone.setVendorName(zone.getVendorName());
                    parkingZone.setVendorUrl(zone.getVendorUrl());
                    parkingZones.add(parkingZone);

                }
                citiesParkingZone.setParkingZoneList(parkingZones);
                getParkingZonesByCityResult.getCitiesParkingZones().add(citiesParkingZone);
            }

        }
        catch (InvalidTokenException invalidTokenException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getParkingZonesByCityResult", "", "", "accessToken " + auth.getAccess_token() + " expired ");
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getParkingZonesByCityResult", "", "", "requesting new accessToken ... at "
                    + Calendar.getInstance().getTime());
            Authentication refreshedAuthentication = oauth2ParkingServiceTokenExchanger.refreshAuthenticationData(auth.getRefresh_token());
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getParkingZonesByCityResult", "", "",
                    "got new accessToken ... at " + refreshedAuthentication.getAccess_token());
            return getParkingZonesByCityResult(lang, cityId);
        }
        catch (Exception ex) {
            ex.printStackTrace();

            proxy.unsetHttp();

            getParkingZonesByCityResult.setStatusCode(StatusCodeHelper.RETRIEVE_PARKING_ZONES_KO);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", getParkingZonesByCityResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getParkingZonesByCityResult", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

            return getParkingZonesByCityResult;
        }
        proxy.unsetHttp();

        String statusCode = StatusCodeHelper.RETRIEVE_PARKING_ZONES_OK;
        getParkingZonesByCityResult.setStatusCode(statusCode);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", statusCode));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getParkingZonesByCityResult", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return getParkingZonesByCityResult;
    }

    @Override
    public RetrieveParkingZonesResult retrieveParkingZones(String lang, BigDecimal latitude, BigDecimal longitude, BigDecimal accuracy) {

        Date now = new Date();
        String requestId = String.valueOf(now.getTime());

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("lang", lang));
        inputParameters.add(new Pair<String, String>("latitude", String.valueOf(latitude)));
        inputParameters.add(new Pair<String, String>("longitude", String.valueOf(longitude)));
        inputParameters.add(new Pair<String, String>("accuracy", String.valueOf(accuracy)));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveParkingZones", requestId, "opening", ActivityLog.createLogMessage(inputParameters));
        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        proxy.setHttp();

        Authentication auth = null;
        RetrieveParkingZonesResult retrieveParkingZonesResult = new RetrieveParkingZonesResult();
        try {
            auth = this.oauth2ParkingServiceTokenExchanger.retrieveAuthenticationData();
            ParkingServiceStub stub = new ParkingServiceStub(resourceServerEndpoint);

            GetParkingInfoHere requestObj = new GetParkingInfoHere();
            GetParkingInfoHereInput requestInputObj = new GetParkingInfoHereInput();
            requestInputObj.setAccuracy(accuracy.doubleValue());
            requestInputObj.setLang(lang);
            requestInputObj.setLatitude(latitude.doubleValue());
            requestInputObj.setLongitude(longitude.doubleValue());
            requestObj.setInput(requestInputObj);
            GetParkingInfoHereResponse response = stub.getParkingInfoHere(auth.getAccess_token(), requestObj);
            GetParkingInfoHereOutput output = response.getGetParkingInfoHereResult();
            List<CityParkingZones> result = output.getCityParkingZones() != null ? new LinkedList<CityParkingZones>(Arrays.asList(output.getCityParkingZones()))
                    : new LinkedList<CityParkingZones>();
            for (CityParkingZones cityParkingZone : result) {
                City city = cityParkingZone.getCity();
                ParkingCity parkingCity = new ParkingCity();
                parkingCity.setCityId(city.getId());
                parkingCity.setId(city.getId());
                parkingCity.setAdministrativeAreaLevel2Code(city.getAdministrativeAreaLevel2Code());
                parkingCity.setLatitude(city.getLatitude());
                parkingCity.setLongitude(city.getLongitude());
                parkingCity.setName(city.getName());
                parkingCity.setStickerRequired(city.getStickerRequired());
                parkingCity.setUrl(city.getUrl());
                CitiesParkingZone citiesParkingZone = new CitiesParkingZone();
                citiesParkingZone.setCity(parkingCity);
                ParkingZonesList parkingZonesList = cityParkingZone.getParkingZones();
                Zone[] zoneList = parkingZonesList.getZone();
                LinkedList<Zone> zoneAsList = new LinkedList<Zone>(Arrays.asList(zoneList));
                LinkedList<ParkingZone> parkingZones = new LinkedList<ParkingZone>();
                for (Zone zone : zoneAsList) {
                    ParkingZone parkingZone = new ParkingZone();
                    parkingZone.setCityId(zone.getCityId());
                    parkingZone.setDescription(zone.getDescription());
                    parkingZone.setId(zone.getId());
                    parkingZone.setName(zone.getName());

                    parkingZone.setNotice(zone.getNotices().getString() != null ? new LinkedList<String>(Arrays.asList(zone.getNotices().getString())) : new LinkedList<String>());
                    parkingZone.setStallCodeRequired(zone.getStallCodeRequired());
                    parkingZone.setVendorId(zone.getVendorId());
                    parkingZone.setVendorName(zone.getVendorName());
                    parkingZone.setVendorUrl(zone.getVendorUrl());
                    parkingZones.add(parkingZone);

                }
                citiesParkingZone.setParkingZoneList(parkingZones);
                retrieveParkingZonesResult.getCitiesParkingZones().add(citiesParkingZone);

            }
            proxy.unsetHttp();

        }
        catch (InvalidTokenException invalidTokenException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "retrieveParkingZones", "", "", "accessToken " + auth.getAccess_token() + " expired ");
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "retrieveParkingZones", "", "", "requesting new accessToken ... at " + Calendar.getInstance().getTime());
            Authentication refreshedAuthentication = oauth2ParkingServiceTokenExchanger.refreshAuthenticationData(auth.getRefresh_token());
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "retrieveParkingZones", "", "", "got new accessToken ... at " + refreshedAuthentication.getAccess_token());
            return retrieveParkingZones(lang, latitude, longitude, accuracy);
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();

            proxy.unsetHttp();

            retrieveParkingZonesResult.setStatusCode(StatusCodeHelper.RETRIEVE_PARKING_ZONES_KO);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", retrieveParkingZonesResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveParkingZones", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

            return retrieveParkingZonesResult;
        }

        String statusCode = StatusCodeHelper.RETRIEVE_PARKING_ZONES_OK;
        retrieveParkingZonesResult.setStatusCode(statusCode);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", statusCode));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveParkingZones", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return retrieveParkingZonesResult;
    }

    @Override
    public EstimateParkingPriceResult estimateParkingPrice(String lang, String plateNumber, Date requestedEndTime, String parkingZoneId, String stallCode) {

        Date now = new Date();
        String requestId = String.valueOf(now.getTime());

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("lang", lang));
        inputParameters.add(new Pair<String, String>("plateNumber", plateNumber));
        inputParameters.add(new Pair<String, String>("requestedEndTime", String.valueOf(requestedEndTime)));
        inputParameters.add(new Pair<String, String>("parkingZoneId", parkingZoneId));
        inputParameters.add(new Pair<String, String>("stallCode", stallCode));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateParkingPrice", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        EstimateParkingPriceResult estimateParkingPriceResult = new EstimateParkingPriceResult();

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        proxy.setHttp();
        Authentication auth = null;
        try {
            auth = this.oauth2ParkingServiceTokenExchanger.retrieveAuthenticationData();
            ParkingServiceStub stub = new ParkingServiceStub(resourceServerEndpoint);
            EstimateParkingPrice estimateParkingPrice = new EstimateParkingPrice();
            EstimateParkingPriceInput input = new EstimateParkingPriceInput();
            input.setLang(lang);
            input.setParkingZoneId(parkingZoneId);
            input.setPlateNumber(plateNumber);
            Calendar cal = Calendar.getInstance();
            cal.setTime(requestedEndTime);
            input.setRequestedEndTime(cal);
            input.setStallCode(stallCode);
            estimateParkingPrice.setInput(input);

            EstimateParkingPriceResponse estimateParkingPriceResponse = stub.estimateParkingPrice(auth.getAccess_token(), estimateParkingPrice);
            EstimateParkingPriceOutput output = estimateParkingPriceResponse.getEstimateParkingPriceResult();

            estimateParkingPriceResult.setParkingEndTime(output.getParkingEndTime().getTime());
            estimateParkingPriceResult.setParkingStartTime(output.getParkingStartTime().getTime());
            estimateParkingPriceResult.setParkingZoneId(output.getParkingZoneId());
            estimateParkingPriceResult.setRequestedEndTime(output.getRequestedEndTime().getTime());
            estimateParkingPriceResult.setPlateNumber(output.getPlateNumber());
            estimateParkingPriceResult.setPrice(output.getPrice());
            estimateParkingPriceResult.setStallCode(output.getStallCode());
            estimateParkingPriceResult.setNotice(new LinkedList<String>(Arrays.asList(output.getNotices().getString())));
            estimateParkingPriceResult.setNotice(output.getNotices().getString() != null ? new LinkedList<String>(Arrays.asList(output.getNotices().getString()))
                    : new LinkedList<String>());

        }
        catch (InvalidTokenException invalidTokenException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "estimateParkingPrice", "", "", "accessToken " + auth.getAccess_token() + " expired ");
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "estimateParkingPrice", "", "", "requesting new accessToken ... at " + Calendar.getInstance().getTime());
            Authentication refreshedAuthentication = oauth2ParkingServiceTokenExchanger.refreshAuthenticationData(auth.getRefresh_token());
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "estimateParkingPrice", "", "", "got new accessToken ... at " + refreshedAuthentication.getAccess_token());
            return estimateParkingPrice(lang, plateNumber, requestedEndTime, parkingZoneId, stallCode);
        }
        catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();

            proxy.unsetHttp();

            estimateParkingPriceResult.setStatusCode(StatusCodeHelper.ESTIMATE_PARKING_PRICE_KO);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", estimateParkingPriceResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateParkingPrice", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

            return estimateParkingPriceResult;
        }

        estimateParkingPriceResult.setStatusCode(StatusCodeHelper.ESTIMATE_PARKING_PRICE_OK);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("parkingZoneId", estimateParkingPriceResult.getParkingZoneId()));
        outputParameters.add(new Pair<String, String>("stallCode", estimateParkingPriceResult.getStallCode()));
        outputParameters.add(new Pair<String, String>("plateNumber", estimateParkingPriceResult.getPlateNumber()));
        outputParameters.add(new Pair<String, String>("requestedEndTime", String.valueOf(estimateParkingPriceResult.getRequestedEndTime())));
        outputParameters.add(new Pair<String, String>("parkingStartTime", String.valueOf(estimateParkingPriceResult.getParkingStartTime())));
        outputParameters.add(new Pair<String, String>("parkingEndTime", String.valueOf(estimateParkingPriceResult.getParkingEndTime())));
        outputParameters.add(new Pair<String, String>("parkingTimeCorrection", String.valueOf(estimateParkingPriceResult.getParkingTimeCorrection())));
        outputParameters.add(new Pair<String, String>("price", String.valueOf(estimateParkingPriceResult.getPrice())));
        outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(estimateParkingPriceResult.getNotice(), "|")));
        outputParameters.add(new Pair<String, String>("statusCode", estimateParkingPriceResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateParkingPrice", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return estimateParkingPriceResult;
    }

    @Override
    public StartParkingResult startParking(String lang, String plateNumber, Date requestedEndTime, String parkingZoneId, String stallCode, String clientOperationID) {

        Date now = new Date();

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("lang", lang));
        inputParameters.add(new Pair<String, String>("plateNumber", plateNumber));
        inputParameters.add(new Pair<String, String>("requestedEndTime", String.valueOf(requestedEndTime)));
        inputParameters.add(new Pair<String, String>("parkingZoneId", parkingZoneId));
        inputParameters.add(new Pair<String, String>("stallCode", stallCode));
        inputParameters.add(new Pair<String, String>("clientOperationID", clientOperationID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "startParking", clientOperationID, "opening", ActivityLog.createLogMessage(inputParameters));

        StartParkingResult startParkingResult = new StartParkingResult();

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        proxy.setHttp();
        Authentication auth = null;
        try {
            auth = this.oauth2ParkingServiceTokenExchanger.retrieveAuthenticationData();
            ParkingServiceStub stub = new ParkingServiceStub(resourceServerEndpoint);
            StartParking startParkingRequest = new StartParking();
            StartParkingInput input = new StartParkingInput();
            input.setLang(lang);
            //TODO gestire Client Operation ID
            input.setClientOperationId(clientOperationID);
            input.setParkingZoneId(parkingZoneId);
            input.setPlateNumber(plateNumber);
            Calendar cal = Calendar.getInstance();
            cal.setTime(requestedEndTime);
            input.setRequestedEndTime(cal);
            input.setStallCode(stallCode);
            startParkingRequest.setInput(input);

            StartParkingResponse response = stub.startParking(auth.getAccess_token(), startParkingRequest);
            StartParkingOutput startParkingOutput = response.getStartParkingResult();

            startParkingResult.setParkingId(startParkingOutput.getParkingId());
            startParkingResult.setParkingZoneId(startParkingOutput.getParkingZoneId());
            startParkingResult.setStallCode(startParkingOutput.getStallCode());
            startParkingResult.setPlateNumber(startParkingOutput.getPlateNumber());
            startParkingResult.setRequestedEndTime(startParkingOutput.getRequestedEndTime().getTime());
            startParkingResult.setParkingStartTime(startParkingOutput.getParkingStartTime().getTime());
            startParkingResult.setParkingEndTime(startParkingOutput.getParkingEndTime().getTime());
            startParkingResult.setParkingTimeCorrection(startParkingOutput.getParkingTimeCorrection().getValue());

            startParkingResult.setPrice(startParkingOutput.getPrice());
            startParkingResult.setNotice(startParkingOutput.getNotices().getString() != null ? new LinkedList<String>(Arrays.asList(startParkingOutput.getNotices().getString()))
                    : new LinkedList<String>());
        }

        catch (InvalidTokenException invalidTokenException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "startParking", "", "", "accessToken " + auth.getAccess_token() + " expired ");
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "startParking", "", "", "requesting new accessToken ... at " + Calendar.getInstance().getTime());
            Authentication refreshedAuthentication = oauth2ParkingServiceTokenExchanger.refreshAuthenticationData(auth.getRefresh_token());
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "startParking", "", "", "got new accessToken ... at " + refreshedAuthentication.getAccess_token());
            return startParking(lang, plateNumber, requestedEndTime, parkingZoneId, stallCode, clientOperationID);
        }

        catch (InvalidStallCodeException invalidStallCodeException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "startParking", "", "", "stallCode " + stallCode + " is invalid.. ");
            proxy.unsetHttp();
            startParkingResult.setStatusCode(StatusCodeHelper.START_PARKING_INVALID_STALL_CODE);
            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", startParkingResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "startParking", clientOperationID, "closing", ActivityLog.createLogMessage(outputParameters));

            return startParkingResult;

        }

        catch (PlateAlreadyInUseException plateAlreadyInUseException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "startParking", "", "", "plate " + plateNumber + " is already in use ");
            proxy.unsetHttp();
            startParkingResult.setStatusCode(StatusCodeHelper.START_PARKING_PLATE_NUMBER_IN_USE);
            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", startParkingResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "startParking", clientOperationID, "closing", ActivityLog.createLogMessage(outputParameters));

            return startParkingResult;

        }

        catch (Exception ex) {
            ex.printStackTrace();

            proxy.unsetHttp();
            startParkingResult.setStatusCode(StatusCodeHelper.START_PARKING_KO);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", startParkingResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "startParking", clientOperationID, "closing", ActivityLog.createLogMessage(outputParameters));

            return startParkingResult;
        }

        startParkingResult.setStatusCode(StatusCodeHelper.START_PARKING_OK);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("parkingId", startParkingResult.getParkingId()));
        outputParameters.add(new Pair<String, String>("parkingZoneId", startParkingResult.getParkingZoneId()));
        outputParameters.add(new Pair<String, String>("stallCode", startParkingResult.getStallCode()));
        outputParameters.add(new Pair<String, String>("plateNumber", startParkingResult.getPlateNumber()));
        outputParameters.add(new Pair<String, String>("requestedEndTime", String.valueOf(startParkingResult.getRequestedEndTime())));
        outputParameters.add(new Pair<String, String>("parkingStartTime", String.valueOf(startParkingResult.getParkingStartTime())));
        outputParameters.add(new Pair<String, String>("parkingEndTime", String.valueOf(startParkingResult.getParkingEndTime())));
        outputParameters.add(new Pair<String, String>("parkingTimeCorrection", String.valueOf(startParkingResult.getParkingTimeCorrection())));
        outputParameters.add(new Pair<String, String>("price", String.valueOf(startParkingResult.getPrice())));
        outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(startParkingResult.getNotice(), "|")));
        outputParameters.add(new Pair<String, String>("statusCode", startParkingResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "startParking", clientOperationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return startParkingResult;
    }

    @Override
    public EstimateExtendedParkingPriceResult estimateExtendedParkingPrice(String lang, String parkingId, Date requestedEndTime) {

        Date now = new Date();
        String requestId = String.valueOf(now.getTime());

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("lang", lang));
        inputParameters.add(new Pair<String, String>("parkingId", parkingId));
        inputParameters.add(new Pair<String, String>("requestedEndTime", String.valueOf(requestedEndTime)));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateExtendedParkingPrice", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        EstimateExtendedParkingPriceResult estimateExtendedParkingPriceResult = new EstimateExtendedParkingPriceResult();

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        proxy.setHttp();
        Authentication auth = null;
        try {
            auth = this.oauth2ParkingServiceTokenExchanger.retrieveAuthenticationData();
            ParkingServiceStub stub = new ParkingServiceStub(resourceServerEndpoint);
            EstimateExtendedParkingPrice estimateExtendedParkingPriceRequest = new EstimateExtendedParkingPrice();
            EstimateExtendedParkingPriceInput input = new EstimateExtendedParkingPriceInput();
            input.setLang(lang);
            input.setParkingId(parkingId);
            Calendar cal = Calendar.getInstance();
            cal.setTime(requestedEndTime);
            input.setRequestedEndTime(cal);
            estimateExtendedParkingPriceRequest.setInput(input);
            EstimateExtendedParkingPriceResponse response = stub.estimateExtendedParkingPrice(auth.getAccess_token(), estimateExtendedParkingPriceRequest);
            EstimateExtendedParkingPriceOutput estimateExtendedParkingPriceOutput = response.getEstimateExtendedParkingPriceResult();

            estimateExtendedParkingPriceResult.setParkingId(estimateExtendedParkingPriceOutput.getParkingId());
            estimateExtendedParkingPriceResult.setParkingZoneId(estimateExtendedParkingPriceOutput.getParkingZoneId());
            estimateExtendedParkingPriceResult.setStallCode(estimateExtendedParkingPriceOutput.getStallCode());
            estimateExtendedParkingPriceResult.setPlateNumber(estimateExtendedParkingPriceOutput.getPlateNumber());
            estimateExtendedParkingPriceResult.setRequestedEndTime(estimateExtendedParkingPriceOutput.getRequestedEndTime().getTime());
            estimateExtendedParkingPriceResult.setParkingStartTime(estimateExtendedParkingPriceOutput.getParkingStartTime().getTime());
            estimateExtendedParkingPriceResult.setParkingEndTime(estimateExtendedParkingPriceOutput.getParkingEndTime().getTime());
            estimateExtendedParkingPriceResult.setParkingTimeCorrection(estimateExtendedParkingPriceOutput.getParkingTimeCorrection().getValue());

            estimateExtendedParkingPriceResult.setPrice(estimateExtendedParkingPriceOutput.getPrice());
            estimateExtendedParkingPriceResult.setCurrentPrice(estimateExtendedParkingPriceOutput.getCurrentPrice());
            estimateExtendedParkingPriceResult.setPriceDifference(estimateExtendedParkingPriceOutput.getPriceDifference());
            estimateExtendedParkingPriceResult.setNotice(estimateExtendedParkingPriceOutput.getNotices().getString() != null ? new LinkedList<String>(
                    Arrays.asList(estimateExtendedParkingPriceOutput.getNotices().getString())) : new LinkedList<String>());

        }
        catch (InvalidTokenException invalidTokenException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "estimateExtendedParkingPrice", "", "", "accessToken " + auth.getAccess_token() + " expired ");
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "estimateExtendedParkingPrice", "", "", "requesting new accessToken ... at "
                    + Calendar.getInstance().getTime());
            Authentication refreshedAuthentication = oauth2ParkingServiceTokenExchanger.refreshAuthenticationData(auth.getRefresh_token());
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "estimateExtendedParkingPrice", "", "",
                    "got new accessToken ... at " + refreshedAuthentication.getAccess_token());
            return estimateExtendedParkingPrice(lang, parkingId, requestedEndTime);
        }

        catch (com.techedge.mp.parking.integration.wsclient.ParkingService_EstimateExtendedParkingPrice_ServiceFault_FaultMessage serviceFaultException) {

            String message = serviceFaultException.getMessage();

            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "estimateExtendedParkingPrice", requestId, "", "serviceFaultException: " + message);

            proxy.unsetHttp();

            if (message.equals("Could not end parking with parkingId=" + parkingId) || message.equals("Could not find parking with parkingId=" + parkingId)) {

                // La sosta � gi� terminata
                estimateExtendedParkingPriceResult.setStatusCode(StatusCodeHelper.ESTIMATE_EXTEND_PARKING_PRICE_CLOSED);
            }
            else {

                serviceFaultException.printStackTrace();
                estimateExtendedParkingPriceResult.setStatusCode(StatusCodeHelper.ESTIMATE_EXTEND_PARKING_PRICE_KO);
            }

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", estimateExtendedParkingPriceResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateExtendedParkingPrice", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

            return estimateExtendedParkingPriceResult;

        }

        catch (Exception ex) {
            ex.printStackTrace();

            proxy.unsetHttp();

            estimateExtendedParkingPriceResult.setStatusCode(StatusCodeHelper.ESTIMATE_EXTEND_PARKING_PRICE_KO);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", estimateExtendedParkingPriceResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateExtendedParkingPrice", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

            return estimateExtendedParkingPriceResult;
        }

        proxy.unsetHttp();
        estimateExtendedParkingPriceResult.setStatusCode("OK");

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("parkingId", estimateExtendedParkingPriceResult.getParkingId()));
        outputParameters.add(new Pair<String, String>("parkingZoneId", estimateExtendedParkingPriceResult.getParkingZoneId()));
        outputParameters.add(new Pair<String, String>("stallCode", estimateExtendedParkingPriceResult.getStallCode()));
        outputParameters.add(new Pair<String, String>("plateNumber", estimateExtendedParkingPriceResult.getPlateNumber()));
        outputParameters.add(new Pair<String, String>("requestedEndTime", String.valueOf(estimateExtendedParkingPriceResult.getRequestedEndTime())));
        outputParameters.add(new Pair<String, String>("parkingStartTime", String.valueOf(estimateExtendedParkingPriceResult.getParkingStartTime())));
        outputParameters.add(new Pair<String, String>("parkingEndTime", String.valueOf(estimateExtendedParkingPriceResult.getParkingEndTime())));
        outputParameters.add(new Pair<String, String>("parkingTimeCorrection", String.valueOf(estimateExtendedParkingPriceResult.getParkingTimeCorrection())));
        outputParameters.add(new Pair<String, String>("price", String.valueOf(estimateExtendedParkingPriceResult.getPrice())));
        outputParameters.add(new Pair<String, String>("currentPrice", String.valueOf(estimateExtendedParkingPriceResult.getCurrentPrice())));
        outputParameters.add(new Pair<String, String>("priceDifference", String.valueOf(estimateExtendedParkingPriceResult.getPriceDifference())));
        outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(estimateExtendedParkingPriceResult.getNotice(), "|")));
        outputParameters.add(new Pair<String, String>("statusCode", estimateExtendedParkingPriceResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateExtendedParkingPrice", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return estimateExtendedParkingPriceResult;
    }

    @Override
    public ExtendParkingResult extendParking(String lang, String parkingId, Date requestedEndTime, String clientOperationID) {

        Date now = new Date();

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("lang", lang));
        inputParameters.add(new Pair<String, String>("parkingId", parkingId));
        inputParameters.add(new Pair<String, String>("requestedEndTime", String.valueOf(requestedEndTime)));
        inputParameters.add(new Pair<String, String>("clientOperationID", clientOperationID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "extendParking", clientOperationID, "opening", ActivityLog.createLogMessage(inputParameters));

        ExtendParkingResult extendParkingResult = new ExtendParkingResult();

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        proxy.setHttp();
        Authentication auth = null;
        try {
            auth = this.oauth2ParkingServiceTokenExchanger.retrieveAuthenticationData();
            ParkingServiceStub stub = new ParkingServiceStub(resourceServerEndpoint);
            ExtendParking extendParkingRequest = new ExtendParking();
            ExtendParkingInput input = new ExtendParkingInput();
            input.setClientOperationId(clientOperationID);
            input.setLang(lang);
            input.setParkingId(parkingId);
            Calendar cal = Calendar.getInstance();
            cal.setTime(requestedEndTime);
            input.setRequestedEndTime(cal);
            extendParkingRequest.setInput(input);
            ExtendParkingResponse extendParkingResponse = stub.extendParking(auth.getAccess_token(), extendParkingRequest);
            ExtendParkingOutput extendParkingOutput = extendParkingResponse.getExtendParkingResult();
            extendParkingResult.setParkingId(extendParkingOutput.getParkingId());
            extendParkingResult.setParkingZoneId(extendParkingOutput.getParkingZoneId());
            extendParkingResult.setStallCode(extendParkingOutput.getStallCode());
            extendParkingResult.setPlateNumber(extendParkingOutput.getPlateNumber());
            extendParkingResult.setRequestedEndTime(extendParkingOutput.getRequestedEndTime().getTime());
            extendParkingResult.setParkingStartTime(extendParkingOutput.getParkingStartTime().getTime());
            extendParkingResult.setParkingEndTime(extendParkingOutput.getParkingEndTime().getTime());
            extendParkingResult.setParkingTimeCorrection(extendParkingOutput.getParkingTimeCorrection().getValue());

            extendParkingResult.setPrice(extendParkingOutput.getPrice());
            extendParkingResult.setPreviousPrice(extendParkingOutput.getPreviousPrice());
            extendParkingResult.setPriceDifference(extendParkingOutput.getPriceDifference());

            extendParkingResult.setNotice(extendParkingOutput.getNotices().getString() != null ? new LinkedList<String>(Arrays.asList(extendParkingOutput.getNotices().getString()))
                    : new LinkedList<String>());
        }
        catch (InvalidTokenException invalidTokenException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "extendParking", "", "", "accessToken " + auth.getAccess_token() + " expired ");
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "extendParking", "", "", "requesting new accessToken ... at " + Calendar.getInstance().getTime());
            Authentication refreshedAuthentication = oauth2ParkingServiceTokenExchanger.refreshAuthenticationData(auth.getRefresh_token());
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "extendParking", "", "", "got new accessToken ... at " + refreshedAuthentication.getAccess_token());
            return extendParking(lang, parkingId, requestedEndTime, clientOperationID);
        }
        catch (ParkingNotFoundException parkingNotFoundException) {
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "extendParking", "", "", parkingNotFoundException.getErrorDescription());
            extendParkingResult.setStatusCode(parkingNotFoundException.getErrorCode());
            return extendParkingResult;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "extendParking", null, "", ex.getMessage());

        }
        proxy.unsetHttp();

        extendParkingResult.setStatusCode("OK");

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("parkingId", extendParkingResult.getParkingId()));
        outputParameters.add(new Pair<String, String>("parkingZoneId", extendParkingResult.getParkingZoneId()));
        outputParameters.add(new Pair<String, String>("stallCode", extendParkingResult.getStallCode()));
        outputParameters.add(new Pair<String, String>("plateNumber", extendParkingResult.getPlateNumber()));
        outputParameters.add(new Pair<String, String>("requestedEndTime", String.valueOf(extendParkingResult.getRequestedEndTime())));
        outputParameters.add(new Pair<String, String>("parkingStartTime", String.valueOf(extendParkingResult.getParkingStartTime())));
        outputParameters.add(new Pair<String, String>("parkingEndTime", String.valueOf(extendParkingResult.getParkingEndTime())));
        outputParameters.add(new Pair<String, String>("parkingTimeCorrection", String.valueOf(extendParkingResult.getParkingTimeCorrection())));
        outputParameters.add(new Pair<String, String>("price", String.valueOf(extendParkingResult.getPrice())));
        outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(extendParkingResult.getNotice(), "|")));
        outputParameters.add(new Pair<String, String>("statusCode", extendParkingResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "extendParking", clientOperationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return extendParkingResult;
    }

    @Override
    public EstimateEndParkingPriceResult estimateEndParkingPrice(String lang, String parkingId) {

        Date now = new Date();
        String requestId = String.valueOf(now.getTime());

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("lang", lang));
        inputParameters.add(new Pair<String, String>("parkingId", parkingId));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateEndParkingPrice", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        EstimateEndParkingPriceResult estimateEndParkingPriceResult = new EstimateEndParkingPriceResult();

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        proxy.setHttp();
        Authentication auth = null;
        try {
            auth = this.oauth2ParkingServiceTokenExchanger.retrieveAuthenticationData();
            ParkingServiceStub stub = new ParkingServiceStub(resourceServerEndpoint);
            EstimateEndParkingPrice estimateEndParkingPriceRequest = new EstimateEndParkingPrice();
            EstimateEndParkingPriceInput input = new EstimateEndParkingPriceInput();
            input.setLang(lang);
            input.setParkingId(parkingId);
            estimateEndParkingPriceRequest.setInput(input);
            EstimateEndParkingPriceResponse response = stub.estimateEndParkingPrice(auth.getAccess_token(), estimateEndParkingPriceRequest);
            EstimateEndParkingPriceOutput estimateEndParkingPriceOutput = response.getEstimateEndParkingPriceResult();

            estimateEndParkingPriceResult.setParkingId(estimateEndParkingPriceOutput.getParkingId());
            estimateEndParkingPriceResult.setParkingZoneId(estimateEndParkingPriceOutput.getParkingZoneId());
            estimateEndParkingPriceResult.setStallCode(estimateEndParkingPriceOutput.getStallCode());
            estimateEndParkingPriceResult.setPlateNumber(estimateEndParkingPriceOutput.getPlateNumber());
            estimateEndParkingPriceResult.setParkingStartTime(estimateEndParkingPriceOutput.getParkingStartTime().getTime());
            estimateEndParkingPriceResult.setParkingEndTime(estimateEndParkingPriceOutput.getParkingEndTime().getTime());
            estimateEndParkingPriceResult.setParkingTimeCorrection(estimateEndParkingPriceOutput.getParkingTimeCorrection().getValue());
            estimateEndParkingPriceResult.setPrice(estimateEndParkingPriceOutput.getPrice());
            estimateEndParkingPriceResult.setNotice(estimateEndParkingPriceOutput.getNotices().getString() != null ? new LinkedList<String>(
                    Arrays.asList(estimateEndParkingPriceOutput.getNotices().getString())) : new LinkedList<String>());
        }
        catch (InvalidTokenException invalidTokenException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "estimateEndParkingPrice", "", "", "accessToken " + auth.getAccess_token() + " expired ");
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "estimateEndParkingPrice", "", "", "requesting new accessToken ... at " + Calendar.getInstance().getTime());
            Authentication refreshedAuthentication = oauth2ParkingServiceTokenExchanger.refreshAuthenticationData(auth.getRefresh_token());
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "estimateEndParkingPrice", "", "", "got new accessToken ... at " + refreshedAuthentication.getAccess_token());
            return estimateEndParkingPrice(lang, parkingId);
        }
        catch (com.techedge.mp.parking.integration.wsclient.ParkingService_EstimateEndParkingPrice_ServiceFault_FaultMessage serviceFaultException) {

            String message = serviceFaultException.getMessage();

            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "endParking", requestId, "", "serviceFaultException: " + message);

            proxy.unsetHttp();

            if (message.equals("Could not end parking with parkingId=" + parkingId) || message.equals("Could not find parking with parkingId=" + parkingId)) {

                // La sosta � gi� terminata
                estimateEndParkingPriceResult.setStatusCode(StatusCodeHelper.ESTIMATE_END_PARKING_PRICE_CLOSED);
            }
            else {

                serviceFaultException.printStackTrace();
                estimateEndParkingPriceResult.setStatusCode(StatusCodeHelper.ESTIMATE_END_PARKING_PRICE_KO);
            }

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", estimateEndParkingPriceResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "endParking", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

            return estimateEndParkingPriceResult;

        }
        catch (Exception ex) {
            ex.printStackTrace();

            proxy.unsetHttp();

            estimateEndParkingPriceResult.setStatusCode(StatusCodeHelper.ESTIMATE_END_PARKING_PRICE_KO);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", estimateEndParkingPriceResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateEndParkingPrice", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

            return estimateEndParkingPriceResult;
        }
        proxy.unsetHttp();

        estimateEndParkingPriceResult.setStatusCode(StatusCodeHelper.ESTIMATE_END_PARKING_PRICE_OK);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("parkingId", estimateEndParkingPriceResult.getParkingId()));
        outputParameters.add(new Pair<String, String>("parkingZoneId", estimateEndParkingPriceResult.getParkingZoneId()));
        outputParameters.add(new Pair<String, String>("stallCode", estimateEndParkingPriceResult.getStallCode()));
        outputParameters.add(new Pair<String, String>("plateNumber", estimateEndParkingPriceResult.getPlateNumber()));
        outputParameters.add(new Pair<String, String>("requestedEndTime", String.valueOf(estimateEndParkingPriceResult.getRequestedEndTime())));
        outputParameters.add(new Pair<String, String>("parkingStartTime", String.valueOf(estimateEndParkingPriceResult.getParkingStartTime())));
        outputParameters.add(new Pair<String, String>("parkingEndTime", String.valueOf(estimateEndParkingPriceResult.getParkingEndTime())));
        outputParameters.add(new Pair<String, String>("parkingTimeCorrection", String.valueOf(estimateEndParkingPriceResult.getParkingTimeCorrection())));
        outputParameters.add(new Pair<String, String>("price", String.valueOf(estimateEndParkingPriceResult.getPrice())));
        outputParameters.add(new Pair<String, String>("currentPrice", String.valueOf(estimateEndParkingPriceResult.getCurrentPrice())));
        outputParameters.add(new Pair<String, String>("priceDifference", String.valueOf(estimateEndParkingPriceResult.getPriceDifference())));
        outputParameters.add(new Pair<String, String>("statusCode", estimateEndParkingPriceResult.getStatusCode()));
        outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(estimateEndParkingPriceResult.getNotice(), "|")));
        outputParameters.add(new Pair<String, String>("statusCode", estimateEndParkingPriceResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "estimateEndParkingPrice", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return estimateEndParkingPriceResult;
    }

    @Override
    public EndParkingResult endParking(String lang, String parkingId, String clientOperationID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("lang", lang));
        inputParameters.add(new Pair<String, String>("parkingId", parkingId));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "endParking", clientOperationID, "opening", ActivityLog.createLogMessage(inputParameters));

        EndParkingResult endParkingResult = new EndParkingResult();

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        proxy.setHttp();
        Authentication auth = null;
        try {
            auth = this.oauth2ParkingServiceTokenExchanger.retrieveAuthenticationData();
            ParkingServiceStub stub = new ParkingServiceStub(resourceServerEndpoint);
            EndParking endParkingRequest = new EndParking();
            EndParkingInput input = new EndParkingInput();
            input.setClientOperationId(clientOperationID);
            input.setLang(lang);
            input.setParkingId(parkingId);
            endParkingRequest.setInput(input);
            EndParkingResponse response = stub.endParking(auth.getAccess_token(), endParkingRequest);
            EndParkingOutput endParkingOutput = response.getEndParkingResult();

            endParkingResult.setParkingId(endParkingOutput.getParkingId());
            endParkingResult.setParkingZoneId(endParkingOutput.getParkingZoneId());
            endParkingResult.setStallCode(endParkingOutput.getStallCode());
            endParkingResult.setPlateNumber(endParkingOutput.getPlateNumber());
            endParkingResult.setParkingStartTime(endParkingOutput.getParkingStartTime().getTime());
            endParkingResult.setParkingEndTime(endParkingOutput.getParkingEndTime().getTime());
            endParkingResult.setPrice(endParkingOutput.getPrice());
            endParkingResult.setNotice(endParkingOutput.getNotices().getString() != null ? new LinkedList<String>(Arrays.asList(endParkingOutput.getNotices().getString())) : null);

        }
        catch (InvalidTokenException invalidTokenException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "endParking", clientOperationID, "", "accessToken " + auth.getAccess_token() + " expired ");
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "endParking", clientOperationID, "", "requesting new accessToken ... at " + Calendar.getInstance().getTime());
            Authentication refreshedAuthentication = oauth2ParkingServiceTokenExchanger.refreshAuthenticationData(auth.getRefresh_token());
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "endParking", clientOperationID, "",
                    "got new accessToken ... at " + refreshedAuthentication.getAccess_token());
            return endParking(lang, parkingId, clientOperationID);
        }
        catch (com.techedge.mp.parking.integration.wsclient.ParkingService_EndParking_ServiceFault_FaultMessage serviceFaultException) {

            String message = serviceFaultException.getMessage();

            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "endParking", clientOperationID, "", "serviceFaultException: " + message);

            proxy.unsetHttp();

            if (message.equals("Could not end parking with parkingId=" + parkingId) || message.equals("Could not find parking with parkingId=" + parkingId)) {

                // La sosta � gi� terminata
                endParkingResult.setStatusCode(StatusCodeHelper.END_PARKING_CLOSED);
            }
            else {

                serviceFaultException.printStackTrace();
                endParkingResult.setStatusCode(StatusCodeHelper.END_PARKING_KO);
            }

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", endParkingResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "endParking", clientOperationID, "closing", ActivityLog.createLogMessage(outputParameters));

            return endParkingResult;

        }
        catch (Exception ex) {
            ex.printStackTrace();

            proxy.unsetHttp();

            endParkingResult.setStatusCode(StatusCodeHelper.END_PARKING_KO);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", endParkingResult.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "endParking", clientOperationID, "closing", ActivityLog.createLogMessage(outputParameters));

            return endParkingResult;

        }

        proxy.unsetHttp();

        endParkingResult.setStatusCode(StatusCodeHelper.END_PARKING_OK);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("parkingId", endParkingResult.getParkingId()));
        outputParameters.add(new Pair<String, String>("parkingZoneId", endParkingResult.getParkingZoneId()));
        outputParameters.add(new Pair<String, String>("stallCode", endParkingResult.getStallCode()));
        outputParameters.add(new Pair<String, String>("plateNumber", endParkingResult.getPlateNumber()));
        outputParameters.add(new Pair<String, String>("parkingStartTime", String.valueOf(endParkingResult.getParkingStartTime())));
        outputParameters.add(new Pair<String, String>("parkingEndTime", String.valueOf(endParkingResult.getParkingEndTime())));
        outputParameters.add(new Pair<String, String>("price", String.valueOf(endParkingResult.getPrice())));
        outputParameters.add(new Pair<String, String>("notice", StringUtils.convertStringArrayToString(endParkingResult.getNotice(), "|")));
        outputParameters.add(new Pair<String, String>("statusCode", endParkingResult.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "endParking", clientOperationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return endParkingResult;
    }

    @Override
    public UserMyCiceroDataResult retrieveUserData(String logonToken) {

        String clientOperationID = null;
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("token", logonToken));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveUserData", logonToken, "opening", ActivityLog.createLogMessage(inputParameters));

        UserMyCiceroDataResult userData = null;

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        proxy.setHttp();
        Authentication auth = null;
        try {
            auth = this.oauth2ParkingServiceTokenExchanger.retrieveAuthenticationData();
            userData = jsonClient.retrieveUserData(auth.getAccess_token(), logonToken);
            
        }

        catch (MyCiceroTokenException invalidTokenException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "retrieveUserData", clientOperationID, "", "MyCicero logon token " + logonToken + " expired ");
            userData = new UserMyCiceroDataResult();
            userData.setInternalStatusCode(invalidTokenException.getErrorCode());
            return userData;
        }

        catch (InvalidTokenException invalidTokenException) {
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "retrieveUserData", clientOperationID, "", "accessToken " + auth.getAccess_token() + " expired ");
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "retrieveUserData", clientOperationID, "", "requesting new accessToken ... at "
                    + Calendar.getInstance().getTime());
            Authentication refreshedAuthentication = oauth2ParkingServiceTokenExchanger.refreshAuthenticationData(auth.getRefresh_token());
            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "retrieveUserData", clientOperationID, "",
                    "got new accessToken ... at " + refreshedAuthentication.getAccess_token());
            return retrieveUserData(logonToken);
        }

        catch (Exception ex) {
            ex.printStackTrace();

            proxy.unsetHttp();

            userData.setInternalStatusCode(StatusCodeHelper.RETRIEVE_USER_DATA_KO);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", userData.getInternalStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveUserData", clientOperationID, "closing", ActivityLog.createLogMessage(outputParameters));

            return userData;

        }

        proxy.unsetHttp();

        userData.setStatusCode(StatusCodeHelper.RETRIEVE_USER_DATA_OK);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("email", userData.getEmail()));
        outputParameters.add(new Pair<String, String>("name", userData.getName()));
        outputParameters.add(new Pair<String, String>("surname", userData.getSurname()));
        outputParameters.add(new Pair<String, String>("phoneNumber", userData.getPhoneNumber()));
        outputParameters.add(new Pair<String, String>("statusCode", userData.getStatusCode()));
        outputParameters.add(new Pair<String, String>("verifiedEmail", String.valueOf(userData.getVerifiedEmail())));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "retrieveUserData", clientOperationID, "closing", ActivityLog.createLogMessage(outputParameters));

        return userData;
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {

            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

}
