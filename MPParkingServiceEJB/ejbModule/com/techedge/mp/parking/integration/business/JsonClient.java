package com.techedge.mp.parking.integration.business;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.ws.rs.core.MediaType;

import org.apache.http.HttpStatus;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.client.urlconnection.HTTPSProperties;
import com.sun.jersey.client.urlconnection.HttpURLConnectionFactory;
import com.sun.jersey.client.urlconnection.URLConnectionClientHandler;
import com.techedge.mp.parking.integration.business.auth.exception.MyCiceroTokenException;
import com.techedge.mp.parking.integration.business.auth.exception.InvalidTokenException;
import com.techedge.mp.parking.integration.business.interfaces.UserMyCiceroDataResult;
import com.techedge.mp.parking.integration.wsclient.ParkingServiceStub;

public class JsonClient implements HttpURLConnectionFactory {

    private static JsonClient instance = null;
    private String            proxyHost;
    private String            proxyPort;
    private SSLContext        sslContext;
    private Proxy             proxy;
    private static final java.lang.String                  AXIS_FAULT_UNAUTHORIZED_MESSAGE      = "Transport error: 401 Error: Unauthorized";
    private static final java.lang.String                  AXIS_FAULT_CONNECTION_CLOSED_MESSAGE = "javax.net.ssl.SSLHandshakeException: Remote host closed connection during handshake";
    private static WebResource resource;

    protected JsonClient() {

    }

    public static JsonClient getInstance(String _proxyHost, String _proxyPort, String userInfoMyCiceroEndPoint) {
        if (instance == null) {
            instance = new JsonClient();
            instance.setProxyHost(_proxyHost);
            instance.setProxyPort(_proxyPort);
            resource = Client.create(new DefaultClientConfig()).resource(userInfoMyCiceroEndPoint);


        }
        return instance;
    }

    @Override
    public HttpURLConnection getHttpURLConnection(URL url) throws IOException {
        initializeProxy();
        HttpURLConnection con = (HttpURLConnection) url.openConnection(proxy);
        if (con instanceof HttpsURLConnection) {
            HttpsURLConnection httpsCon = (HttpsURLConnection) url.openConnection(proxy);
            httpsCon.setHostnameVerifier(getHostnameVerifier());
            httpsCon.setSSLSocketFactory(getSslContext().getSocketFactory());
            return httpsCon;
        }
        else {
            return con;
        }

    }

    public SSLContext getSslContext() {
        try {
            sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, null);
            SSLContext.setDefault(sslContext);

        }
        catch (NoSuchAlgorithmException ex) {
            //TODO Gestire eccezioni
        }
        catch (KeyManagementException ex) {
            //TODO Gestire eccezioni
        }
        return sslContext;
    }

    private HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                return true;
            }
        };
    }

    private void initializeProxy() {
        proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, Integer.parseInt(proxyPort)));
    }

    public UserMyCiceroDataResult retrieveUserData(String accessToken, String myCiceroLogonToken) throws InvalidTokenException, MyCiceroTokenException {
        Gson gson = new Gson();
        UserMyCiceroDataResult userDataResult = null;
        String resp = null;

        
       try {
            HostnameVerifier hostnameVerifier = HttpsURLConnection.getDefaultHostnameVerifier();

            URLConnectionClientHandler cc = new URLConnectionClientHandler();
            ClientConfig config = new DefaultClientConfig();
            config.getProperties().put(HTTPSProperties.PROPERTY_HTTPS_PROPERTIES, new HTTPSProperties(hostnameVerifier, getSslContext()));
            Client client = new Client(cc, config);
            client.setConnectTimeout(2000000);
            
            WebResource.Builder builder = resource.getRequestBuilder();
            builder.accept(MediaType.APPLICATION_JSON).type(MediaType.APPLICATION_JSON).header("Authorization", "Bearer " + accessToken);

            String queryParam = "{ \"input\": {\"token\":\""+myCiceroLogonToken+"\" }}";
            ClientResponse response = builder.post(ClientResponse.class, queryParam);
            if(response!=null && response.getStatusInfo()!=null && response.getStatusInfo().getStatusCode()==HttpStatus.SC_UNAUTHORIZED)
                throw new InvalidTokenException();
            resp = response.getEntity(String.class);
            userDataResult = gson.fromJson(resp, UserMyCiceroDataResult.class);
            System.out.println(resp);
            if(userDataResult.getMessaggioErrore().equals("Token scaduto"))
                throw new MyCiceroTokenException(userDataResult.getStatusCode(), userDataResult.getMessaggioErrore());
        }
       
       catch(MyCiceroTokenException myCyceroTokenException)
       {
           throw myCyceroTokenException;
       }
        
       
       catch(InvalidTokenException invalidTokenException)
       {
           throw new InvalidTokenException();
       }
        catch (Exception ex) {
            if (ex!=null && ex.getMessage()!=null && (ex.getMessage().equals(AXIS_FAULT_CONNECTION_CLOSED_MESSAGE) || ex.getMessage().equals(AXIS_FAULT_CONNECTION_CLOSED_MESSAGE)))
            throw new InvalidTokenException();
            
            System.out.println(ex.getLocalizedMessage());
        }
        userDataResult = gson.fromJson(resp, UserMyCiceroDataResult.class);
        return userDataResult;
    }

    public void setProxyHost(String proxyHost) {
        this.proxyHost = proxyHost;
    }

    public void setProxyPort(String proxyPort) {
        this.proxyPort = proxyPort;
    }

}
