package com.techedge.mp.parking.integration.business.auth;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import com.google.gson.Gson;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.client.urlconnection.HttpURLConnectionFactory;
import com.sun.jersey.client.urlconnection.URLConnectionClientHandler;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParkingTransactionV2ServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.parking.integration.business.EJBHomeCache;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class Oauth2RequestTokenHelper implements HttpURLConnectionFactory {

    private Proxy                                    proxy;

    private String                                   proxyHost;

    private Integer                                  proxyPort;

    private SSLContext                               sslContext;

    private static String                            oauth2TokenEndPoint;

    private static Oauth2RequestTokenHelper          instance = null;

    @Resource
    private static EJBContext                        context;

    private static ParkingTransactionV2ServiceRemote parkingTransactionV2Service;
    private LoggerServiceRemote                      loggerService;

    public static void setOauth2TokenEndPoint(String _oauth2TokenEndPoint) {
        oauth2TokenEndPoint = _oauth2TokenEndPoint;
    }

    public Oauth2RequestTokenHelper() {

    }

    public static Oauth2RequestTokenHelper getInstance(String proxyHost, Integer proxyPort) {
        if (instance == null) {
            instance = new Oauth2RequestTokenHelper(proxyHost, proxyPort);
        }
        return instance;
    }

    private Oauth2RequestTokenHelper(String proxyHost, Integer proxyPort) {
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get LoggerService object");
            throw new EJBException(e);
        }

        try {
            parkingTransactionV2Service = EJBHomeCache.getInstance().getParkingTransactionV2Service();
        }
        catch (InterfaceNotFoundException e) {

            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), null, null, null, "Unable to get ParkingTransactionV2Service object");

            throw new EJBException(e);
        }
    }

    private void initializeProxy() {
        proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(this.proxyHost, this.proxyPort));
    }

    @Override
    public HttpURLConnection getHttpURLConnection(URL url) throws IOException {
        initializeProxy();
        HttpURLConnection con = (HttpURLConnection) url.openConnection(proxy);
        if (con instanceof HttpsURLConnection) {
            HttpsURLConnection httpsCon = (HttpsURLConnection) url.openConnection(proxy);
            httpsCon.setHostnameVerifier(getHostnameVerifier());
            httpsCon.setSSLSocketFactory(getSslContext().getSocketFactory());
            return httpsCon;
        }
        else {
            return con;
        }

    }

    public SSLContext getSslContext() {
        try {
            sslContext = SSLContext.getInstance("TLSv1.2");
            sslContext.init(null, null, null);
            SSLContext.setDefault(sslContext);

        }
        catch (NoSuchAlgorithmException ex) {
            //TODO Gestire eccezioni
        }
        catch (KeyManagementException ex) {
            //TODO Gestire eccezioni
        }
        return sslContext;
    }

    private HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                return true;
            }
        };
    }

    public static Authentication getAuthenticationData(String jWTCompact, String proxyHost, String proxyPort) {

        URLConnectionClientHandler cc = new URLConnectionClientHandler(instance);
        Client client = new Client(cc);
        client.setConnectTimeout(2000000);
        WebResource resource = client.resource(oauth2TokenEndPoint);
        resource.type(MediaType.APPLICATION_JSON);

        MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
        queryParams.add("grant_type", "client_credentials");
        queryParams.add("client_assertion", jWTCompact);
        queryParams.add("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer");
        resource.accept(MediaType.APPLICATION_FORM_URLENCODED);
        ClientResponse response = resource.post(ClientResponse.class, queryParams);
        String resp = response.getEntity(String.class);
        Gson gson = new Gson();
        Authentication auth = gson.fromJson(resp, Authentication.class);

        Authentication resfreshedAuth = persistAuthToken(auth);

        System.out.println(resp);
        return resfreshedAuth;
    }
    
    public static Authentication refreshAuthenticationData(String jWTCompact, String refreshToken, String proxyHost, String proxyPort) {

        URLConnectionClientHandler cc = new URLConnectionClientHandler(instance);
        Client client = new Client(cc);
        client.setConnectTimeout(2000000);
        WebResource resource = client.resource(oauth2TokenEndPoint);
        resource.type(MediaType.APPLICATION_JSON);

        MultivaluedMap<String, String> queryParams = new MultivaluedMapImpl();
        queryParams.add("refresh_token", refreshToken);
        queryParams.add("grant_type", "refresh_token");
        queryParams.add("client_assertion", jWTCompact);
        queryParams.add("client_assertion_type", "urn:ietf:params:oauth:client-assertion-type:jwt-bearer");
        resource.accept(MediaType.APPLICATION_FORM_URLENCODED);
        ClientResponse response = resource.post(ClientResponse.class, queryParams);
        String resp = response.getEntity(String.class);
        Gson gson = new Gson();
        Authentication auth = gson.fromJson(resp, Authentication.class);
        System.out.println(resp);
        //REFRESH TOKEN SCADUTO
        if(auth.getAccess_token()==null)
        {
            return auth = getAuthenticationData(jWTCompact, proxyHost, proxyPort); 
        }
        Authentication resfreshedAuth = persistAuthToken(auth);

        System.out.println(resp);
        return resfreshedAuth;
    }

    protected static Authentication persistAuthToken(Authentication auth) {

        com.techedge.mp.core.business.interfaces.parking.Authentication authenticationRemote = new com.techedge.mp.core.business.interfaces.parking.Authentication();
        Authentication authentication2Out = null;
        try {
            authenticationRemote.setAccess_token(auth.getAccess_token());
            authenticationRemote.setRefresh_token(auth.getRefresh_token());
            authenticationRemote.setExpires_in(auth.getExpires_in());
            authenticationRemote.setRefresh_expires_in(auth.getRefresh_expires_in());
            authenticationRemote.setExpired(auth.isExpired());
            com.techedge.mp.core.business.interfaces.parking.Authentication authenticationOut = parkingTransactionV2Service.persistAuthToken(authenticationRemote);
            authentication2Out = new Authentication();
            authentication2Out.setAccess_token(authenticationOut.getAccess_token());
            authentication2Out.setRefresh_token(authenticationOut.getRefresh_token());
            authentication2Out.setExpired(authenticationOut.isExpired());
            authentication2Out.setExpires_in(authenticationOut.getExpires_in());
            authentication2Out.setRefresh_expires_in(authenticationOut.getRefresh_expires_in());

        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return authentication2Out;
    }

    protected Authentication checkAccessTokenState() {
        Authentication authentication = null;

        try {
            com.techedge.mp.core.business.interfaces.parking.Authentication authenticationRemote = parkingTransactionV2Service.checkAccessTokenState();
            if(authenticationRemote==null)
                return null;
            authentication = new Authentication();
            authentication.setAccess_token(authenticationRemote.getAccess_token());
            authentication.setRefresh_token(authenticationRemote.getRefresh_token());
            authentication.setExpired(authenticationRemote.isExpired());
            authentication.setExpires_in(authenticationRemote.getExpires_in());
            authentication.setRefresh_expires_in(authenticationRemote.getRefresh_expires_in());

        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return authentication;
    }
}
