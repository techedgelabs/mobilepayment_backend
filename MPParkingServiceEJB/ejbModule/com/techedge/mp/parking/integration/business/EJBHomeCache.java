package com.techedge.mp.parking.integration.business;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.ParkingTransactionV2ServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;

public class EJBHomeCache {

    final String                                parametersServiceRemoteJndi          = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
    final String                                loggerServiceRemoteJndi              = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
    final String                                parkingTansactionV2ServiceRemoteJndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParkingTransactionV2Service!com.techedge.mp.core.business.ParkingTransactionV2ServiceRemote";

    private static EJBHomeCache                 instance;

    protected Context                           context                              = null;

    protected ParametersServiceRemote           parametersService                    = null;
    protected LoggerServiceRemote               loggerService                        = null;
    protected ParkingTransactionV2ServiceRemote parkingTransactionV2Service          = null;

    private EJBHomeCache() throws InterfaceNotFoundException {

        final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();

        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

        try {
            context = new InitialContext(jndiProperties);
        }
        catch (NamingException e) {

            throw new InterfaceNotFoundException("Naming exception: " + e.getMessage());
        }

        try {
            parametersService = (ParametersServiceRemote) context.lookup(parametersServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(parametersServiceRemoteJndi);
        }

        try {
            loggerService = (LoggerServiceRemote) context.lookup(loggerServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(loggerServiceRemoteJndi);
        }

        try {
            parkingTransactionV2Service = (ParkingTransactionV2ServiceRemote) context.lookup(parkingTansactionV2ServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(parkingTansactionV2ServiceRemoteJndi);
        }

    }

    public static synchronized EJBHomeCache getInstance() throws InterfaceNotFoundException {
        if (instance == null)

            instance = new EJBHomeCache();

        return instance;
    }

    public ParametersServiceRemote getParametersService() {
        return parametersService;
    }

    public LoggerServiceRemote getLoggerService() {
        return loggerService;
    }

    public ParkingTransactionV2ServiceRemote getParkingTransactionV2Service() {
        return parkingTransactionV2Service;
    }

}
