package com.techedge.mp.parking.integration.business.auth;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

public class Oauth2ParkingServiceJWTExchanger {

    private static final String                     KEYSTORE_TYPE = "PKCS12";

    private static Oauth2ParkingServiceJWTExchanger instance      = null;
    private static InputStream                      certFile;
    private static PrivateKey                       privateKey;

    protected Oauth2ParkingServiceJWTExchanger() {

    }

    public static Oauth2ParkingServiceJWTExchanger getInstance(String certificateFilePath, String certificatePassword, String KEY_ALIAS) {
        if (instance == null) {
            instance = new Oauth2ParkingServiceJWTExchanger();
            File cert = new File(certificateFilePath);

            try {
                certFile = new FileInputStream(cert);
                System.out.println("file size " + certFile.available());
                KeyStore keystore = KeyStore.getInstance(KEYSTORE_TYPE);
                keystore.load(certFile, certificatePassword.toCharArray());
                privateKey = (PrivateKey) keystore.getKey(KEY_ALIAS, certificatePassword.toCharArray());

            }
            catch (FileNotFoundException e) {

                System.err.println("MYCICERO SERVICE - CERTIFICATE FILE NOT FOUND OR CORRUPTED ...");
            }
            catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (KeyStoreException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (NoSuchAlgorithmException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (CertificateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (UnrecoverableKeyException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        return instance;
    }

    public String getJWTSigned(String issuer, String subject, String audience, int expirationInSeconds, Date issuedAt) throws KeyStoreException, NoSuchAlgorithmException,
            CertificateException, FileNotFoundException, IOException, UnrecoverableKeyException

    {

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, expirationInSeconds);

        String compactJws = Jwts.builder().setIssuer(issuer).setSubject(subject).setAudience(audience).setHeaderParam("typ", "JWT").setId(UUID.randomUUID().toString()).setIssuedAt(
                Calendar.getInstance().getTime()).setExpiration(cal.getTime()).signWith(SignatureAlgorithm.RS256, privateKey).compact();

        return compactJws;
    }

}
