package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class PreAuthorizationConsumeVoucherResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7740814006378979458L;
  
    private String              csTransactionID;
    private String              statusCode;
    private String              messageCode;
    private String              marketingMsg;
    private Double              amount;
    private String              preAuthOperationID;

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }    

    public String getPreAuthOperationID() {
        return preAuthOperationID;
    }

    public void setPreAuthOperationID(String preAuthOperationID) {
        this.preAuthOperationID = preAuthOperationID;
    }


}
