package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class McCardEnjoyAuthorizeResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 4978636584558331361L;
    
    private ResultDetail result;
    private String       authCryptogram;

    public ResultDetail getResult() {
        return result;
    }

    public void setResult(ResultDetail result) {
        this.result = result;
    }

    public String getAuthCryptogram() {
        return authCryptogram;
    }

    public void setAuthCryptogram(String authCryptogram) {
        this.authCryptogram = authCryptogram;
    }

}
