package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class InfoRedemptionResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2724243807965044985L;
    private String                         csTransactionID;
    private String                         statusCode;
    private String                         messageCode;
    private List<RedemptionDetail>         redemptionList   = new ArrayList<RedemptionDetail>();
    private List<CategoryRedemptionDetail> categoryRedemptionList   = new ArrayList<CategoryRedemptionDetail>();

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String value) {
        this.csTransactionID = value;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    public List<RedemptionDetail> getRedemptionList() {
        return redemptionList;
    }

    public void setRedemptionList(List<RedemptionDetail> redemptionList) {
        this.redemptionList = redemptionList;
    }

    public List<CategoryRedemptionDetail> getCategoryRedemptionList() {
        return categoryRedemptionList;
    }

    public void setCategoryRedemptionList(List<CategoryRedemptionDetail> categoryRedemptionList) {
        this.categoryRedemptionList = categoryRedemptionList;
    }

}
