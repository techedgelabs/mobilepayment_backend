package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;


public class User implements Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = 6966100703229389249L;
	
	private String emailAddress;
    private String firstName;
    private String lastName;
    private String telephoneNumber;
	public String getEmailAddress() {
		return emailAddress;
	}
	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getTelephoneNumber() {
		return telephoneNumber;
	}
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
    

    
    
}
