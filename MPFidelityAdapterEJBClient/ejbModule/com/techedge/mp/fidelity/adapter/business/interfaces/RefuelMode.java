package com.techedge.mp.fidelity.adapter.business.interfaces;

public enum RefuelMode {

    F("F"), //Iperself Postpay
    S("S"), //Servito
    C("C"); //Iperself Prepay
    
    private final String value;

    RefuelMode(final String value) {
        this.value = value;
    }

    public String getValue() { 
    
        return value; 
    }

}
