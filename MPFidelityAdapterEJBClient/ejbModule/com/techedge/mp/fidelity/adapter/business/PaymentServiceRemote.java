package com.techedge.mp.fidelity.adapter.business;

import javax.ejb.Remote;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteAuthorizationResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteCaptureResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecutePaymentResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteReversalResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetPaymentStatusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.McCardEnjoyAuthorizeResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PaymentMode;
import com.techedge.mp.fidelity.adapter.business.interfaces.PaymentRefuelMode;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductCodeEnum;

@Remote
public interface PaymentServiceRemote {

    public ExecutePaymentResult executePayment(String operationID, Integer amount, ProductCodeEnum productCode, Integer quantity, Integer unitPrice, String authCryptogram, String currencyCode, String mcCardDpan, PaymentRefuelMode refuelMode,
            String shopCode, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException;
    
    public ExecuteAuthorizationResult executeAuthorization(String operationID, Integer amount, String authCryptogram, String currencyCode, String mcCardDpan,
            String shopCode, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException;
    
    public ExecuteCaptureResult executeCapture(String mcCardDpan, String messageReasonCode, String retrievalRefNumber,  String authCode,  String currencyCode, PaymentRefuelMode refuelMode, String operationID, Integer amount, ProductCodeEnum productCode, Integer quantity, Integer unitPrice,
           PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException;
    
    public ExecuteReversalResult executeReversal( String mcCardDpan, String messageReasonCode, String retrievalRefNumber,  String authCode, String shopCode, String currencyCode, String operationID, Integer amount, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException;

    public GetPaymentStatusResult getPaymentStatus(String paymentOperationId, String operationID, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException;
    
    public McCardEnjoyAuthorizeResult mcCardEnjoyAuthorize(String operationID, Integer amount, String currencyCode, String mcCardDpan,
            PartnerType partnerType, PaymentMode paymentMode, Long requestTimestamp, String serverSerialNumber, String userId) throws FidelityServiceException;
}
