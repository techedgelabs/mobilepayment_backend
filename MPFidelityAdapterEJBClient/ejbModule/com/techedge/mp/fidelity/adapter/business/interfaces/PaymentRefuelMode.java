package com.techedge.mp.fidelity.adapter.business.interfaces;

public enum PaymentRefuelMode {

    F("F"), // Servito
    S("S"), // Iperself postpaid
    I("I"); // Iperself prepaid
    
    private final String value;

    PaymentRefuelMode(final String value) {
        this.value = value;
    }

    public String getValue() { 
    
        return value; 
    }

}
