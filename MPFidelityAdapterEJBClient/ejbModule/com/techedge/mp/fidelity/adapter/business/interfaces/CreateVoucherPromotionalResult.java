package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class CreateVoucherPromotionalResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2270541884437768396L;
    private VoucherDetail     voucher;
    private String            csTransactionID;
    private String            statusCode;
    private String            messageCode;

    public VoucherDetail getVoucher() {
        return voucher;
    }

    public void setVoucher(VoucherDetail voucher) {
        this.voucher = voucher;
    }

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }


}
