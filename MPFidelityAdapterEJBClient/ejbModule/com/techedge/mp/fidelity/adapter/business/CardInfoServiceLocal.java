package com.techedge.mp.fidelity.adapter.business;

import javax.ejb.Local;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.DeleteMcCardRefuelingResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetMcCardStatusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Local
public interface CardInfoServiceLocal {

    public GetMcCardStatusResult getMcCardStatus(String operationID, String userId, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException;
    public DeleteMcCardRefuelingResult deleteMcCardRefueling(String operationID, String userId, PartnerType partnerType, Long requestTimestamp, String mcCardDpan, String serverSerialNumber) throws FidelityServiceException;
}
