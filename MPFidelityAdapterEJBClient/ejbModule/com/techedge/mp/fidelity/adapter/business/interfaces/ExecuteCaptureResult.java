package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ExecuteCaptureResult implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8405421876091060530L;
	private List<KeyValueInfo>      receiptElementList = new ArrayList<KeyValueInfo>(0);
    private ResultDetail            result;
    private String                  retrievalRefNumber;


    public List<KeyValueInfo> getReceiptElementList() {
        return receiptElementList;
    }

    public void setReceiptElementList(List<KeyValueInfo> receiptElementList) {
        this.receiptElementList = receiptElementList;
    }

    public ResultDetail getResult() {
        return result;
    }

    public void setResult(ResultDetail result) {
        this.result = result;
    }

    public String getRetrievalRefNumber() {
        return retrievalRefNumber;
    }

    public void setRetrievalRefNumber(String retrievalRefNumber) {
        this.retrievalRefNumber = retrievalRefNumber;
    }

}
