package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ExecutePaymentResult implements Serializable {

    /**
     * 
     */
    private static final long       serialVersionUID = 3940207337031046528L;
    private String                  authCode;
    private List<ProductDetailInfo> fuelEnabledProductList = new ArrayList<ProductDetailInfo>(0);
    private List<KeyValueInfo>      receiptElementList = new ArrayList<KeyValueInfo>(0);
    private ResultDetail            result;
    private String                  retrievalRefNumber;

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public List<ProductDetailInfo> getFuelEnabledProductList() {
        return fuelEnabledProductList;
    }

    public void setFuelEnabledProductList(List<ProductDetailInfo> fuelEnabledProductList) {
        this.fuelEnabledProductList = fuelEnabledProductList;
    }

    public List<KeyValueInfo> getReceiptElementList() {
        return receiptElementList;
    }

    public void setReceiptElementList(List<KeyValueInfo> receiptElementList) {
        this.receiptElementList = receiptElementList;
    }

    public ResultDetail getResult() {
        return result;
    }

    public void setResult(ResultDetail result) {
        this.result = result;
    }

    public String getRetrievalRefNumber() {
        return retrievalRefNumber;
    }

    public void setRetrievalRefNumber(String retrievalRefNumber) {
        this.retrievalRefNumber = retrievalRefNumber;
    }

}
