package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class DeleteMcCardRefuelingResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3146657316448061495L;
    
    private String            transactionId;
    private String            code;
    private String            message;
    private String            status;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
