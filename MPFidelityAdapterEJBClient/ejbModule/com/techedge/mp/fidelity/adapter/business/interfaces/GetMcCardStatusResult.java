package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetMcCardStatusResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1662382128793698000L;

    private String            transactionId;
    private String            code;
    private String            message;
    private String            status;
    private List<DpanDetail>  dpanDetailList   = new ArrayList<DpanDetail>(0);

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<DpanDetail> getDpanDetailList() {
        return dpanDetailList;
    }

    public void setDpanDetailList(List<DpanDetail> dpanDetailList) {
        this.dpanDetailList = dpanDetailList;
    }

}
