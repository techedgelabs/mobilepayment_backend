package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class VoucherCodeDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -895589050037909791L;

    private String            voucherCode;

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }
}
