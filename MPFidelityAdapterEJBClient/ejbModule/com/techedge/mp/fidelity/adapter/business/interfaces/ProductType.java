package com.techedge.mp.fidelity.adapter.business.interfaces;

public enum ProductType {

    OIL("O"),
    WASH("L"),
    PARKING("S");
    private final String value;

    ProductType(final String value) {
        this.value = value;
    }

    public String getValue() { 
    
        return value; 
    }

}
