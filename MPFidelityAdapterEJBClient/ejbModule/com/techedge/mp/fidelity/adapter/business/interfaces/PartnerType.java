package com.techedge.mp.fidelity.adapter.business.interfaces;

public enum PartnerType {

    MP("MP"),
    EM("EM");
    
    private final String value;

    PartnerType(final String value) {
        this.value = value;
    }

    public String getValue() { 
    
        return value; 
    }

}
