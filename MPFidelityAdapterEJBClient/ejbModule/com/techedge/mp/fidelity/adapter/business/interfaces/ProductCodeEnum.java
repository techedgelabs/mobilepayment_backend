package com.techedge.mp.fidelity.adapter.business.interfaces;


public enum ProductCodeEnum {

    /*
        021 27101245 BENZSP          L
        022 27101245 BENZSP          L
        023 27101249 SP98            L
        029 27101249 SP98            L
        030 27102011 DIESEL          L
        031 27102011 DIESEL          L
        032 27101943 DIESL+          L
        033 27101943 DIESL+          L
        034 ENI0034 G.P.L.           L
        039 ENI0039 METANO           L
     */
    
    SP("021"),
    GG("030"),
    BS("023"),
    BD("032"),
    MT("039"),
    GP("034"),
    AD("");
    
    private final String value;

    ProductCodeEnum(final String value) {
        this.value = value;
    }

    public String getValue() { 
    
        return value; 
    }

    public static ProductCodeEnum getCommand(String value) {
        for (ProductCodeEnum productCodeEnum : ProductCodeEnum.values()) {
            if (productCodeEnum.name().equals(value)) {
                return productCodeEnum;
            }
        }
        
        return null;
    }
}
