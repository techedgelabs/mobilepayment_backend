package com.techedge.mp.fidelity.adapter.business.interfaces;

public enum PaymentMode {

    AUTH_AND_MOVE("AUTH_AND_MOVE"),
    AUTH_ONLY("AUTH_ONLY");
    
    private final String value;

    PaymentMode(final String value) {
        this.value = value;
    }

    public String getValue() { 
    
        return value; 
    }

}
