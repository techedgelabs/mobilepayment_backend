package com.techedge.mp.fidelity.adapter.business.utilities;

import java.math.BigDecimal;
import java.math.BigInteger;

public class AmountConverter {

    private static Double CONV_INTERNAL_TO_MOBILE   = 100.0;
    private static Double CONV_INTERNAL_TO_MOBILE_3 = 1000.0;

    public AmountConverter() {}

    public static Integer toMobile(Double amount) {

        //System.out.println("AmountConverter.toMobile - input: " + amount);

        if (amount == null) {
            return new Integer(0);
        }

        Double convertedAmount = (amount * CONV_INTERNAL_TO_MOBILE);

        Long l = Math.round(convertedAmount);

        //System.out.println("AmountConverter.toMobile - output: " + Integer.valueOf(l.intValue()));

        return Integer.valueOf(l.intValue());
    }
    
    public static Integer toMobile3(Double amount) {

        //System.out.println("AmountConverter.toMobile - input: " + amount);

        if (amount == null) {
            return new Integer(0);
        }

        Double convertedAmount = (amount * CONV_INTERNAL_TO_MOBILE_3);

        Long l = Math.round(convertedAmount);

        //System.out.println("AmountConverter.toMobile - output: " + Integer.valueOf(l.intValue()));

        return Integer.valueOf(l.intValue());
    }

    public static Double toInternal(Integer amount) {

        //System.out.println("AmountConverter.toInternal - input: " + amount);

        if (amount == null) {
            return new Double(0);
        }

        Double convertedAmount = amount.doubleValue();

        //System.out.println("AmountConverter.toInternal - convertedAmount: " + convertedAmount);

        convertedAmount = convertedAmount / CONV_INTERNAL_TO_MOBILE;

        //System.out.println("AmountConverter.toInternal - output: " + convertedAmount);

        return convertedAmount;
    }

    public static Integer toInternal(BigInteger amount) {

        //System.out.println("AmountConverter.toInternal - input: " + amount);

        if (amount == null) {
            return new Integer(0);
        }

        Integer convertedAmount = amount.intValue();

        //System.out.println("AmountConverter.toInternal - convertedAmount: " + convertedAmount);

        return convertedAmount;
    }

    public static Double toInternal(BigDecimal amount) {
        //System.out.println("AmountConverter.toInternal - input: " + amount);

        if (amount == null) {
            return new Double(0);
        }

        Double convertedAmount = amount.doubleValue();

        //System.out.println("AmountConverter.toInternal - convertedAmount: " + convertedAmount);

        return convertedAmount;

    }

    public static BigDecimal toInternal(Double amount) {
        //System.out.println("AmountConverter.toInternal - input: " + amount);

        if (amount == null) {
            return new BigDecimal(0);
        }

        Double convertedDouble = (amount * 100);

        //System.out.println("AmountConverter.toInternal - convertedDouble: " + convertedDouble);

        Long convertedLong = Math.round(convertedDouble);

        //System.out.println("AmountConverter.toInternal - convertedLong: " + convertedLong);

        BigDecimal convertedAmount = new BigDecimal(convertedLong);

        //System.out.println("AmountConverter.toInternal - convertedAmount before: " + convertedAmount);

        convertedAmount = convertedAmount.divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_EVEN);

        //System.out.println("AmountConverter.toInternal - convertedAmount after: " + convertedAmount);

        return convertedAmount;

    }
}
