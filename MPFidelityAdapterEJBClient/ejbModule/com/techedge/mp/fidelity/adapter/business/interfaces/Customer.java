package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class Customer implements Serializable {


	/**
	 * 
	 */
	private static final long serialVersionUID = 7108449179687132560L;
	
    private String businessName;
    private String code;
    private String vatNumber;
    private String fiscalCode;
    
	public String getBusinessName() {
		return businessName;
	}
	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getVatNumber() {
		return vatNumber;
	}
	public void setVatNumber(String vatNumber) {
		this.vatNumber = vatNumber;
	}
	public String getFiscalCode() {
		return fiscalCode;
	}
	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}
    
    
}
