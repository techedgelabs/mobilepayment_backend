package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class DeleteVoucherResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7815223629865580850L;

    private String            csTransactionID;
    private String            statusCode;
    private String            messageCode;

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

}
