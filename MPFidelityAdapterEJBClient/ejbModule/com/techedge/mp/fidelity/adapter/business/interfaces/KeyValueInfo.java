package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class KeyValueInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2344792071092458102L;
    protected String          key;
    protected String          value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
