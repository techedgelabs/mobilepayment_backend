package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CheckConsumeVoucherTransactionResult implements Serializable {

    /**
     * 
     */
    private static final long   serialVersionUID = -886332931407180933L;

    private String              csTransactionID;
    private String              statusCode;
    private String              messageCode;
    private String              marketingMsg;
    private List<VoucherDetail> voucherList      = new ArrayList<VoucherDetail>(0);

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public List<VoucherDetail> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<VoucherDetail> voucherList) {
        this.voucherList = voucherList;
    }

}
