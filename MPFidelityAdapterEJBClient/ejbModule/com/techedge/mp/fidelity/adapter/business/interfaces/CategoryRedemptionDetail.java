package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CategoryRedemptionDetail implements Serializable {

    /**
     * 
     */
    private static final long      serialVersionUID = 1606539946762008607L;

    private String                 categoryCode;
    private List<RedemptionDetail> redemptionList   = new ArrayList<RedemptionDetail>();

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public List<RedemptionDetail> getRedemptionList() {
        return redemptionList;
    }

    public void setRedemptionList(List<RedemptionDetail> redemptionList) {
        this.redemptionList = redemptionList;
    }

}
