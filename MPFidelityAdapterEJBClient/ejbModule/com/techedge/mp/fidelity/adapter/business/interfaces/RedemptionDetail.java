
package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;


public class RedemptionDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5421158432655338995L;
    private Integer code;
    private String name;
    private Double amount;
    private Integer points;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer value) {
        this.code = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double value) {
        this.amount = value;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer value) {
        this.points = value;
    }
    
}
