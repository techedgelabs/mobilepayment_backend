package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ExecuteReversalResult implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3928038839802615923L;

    private List<KeyValueInfo>      receiptElementList = new ArrayList<KeyValueInfo>(0);
    private ResultDetail            result;

    public List<KeyValueInfo> getReceiptElementList() {
        return receiptElementList;
    }

    public void setReceiptElementList(List<KeyValueInfo> receiptElementList) {
        this.receiptElementList = receiptElementList;
    }

    public ResultDetail getResult() {
        return result;
    }

    public void setResult(ResultDetail result) {
        this.result = result;
    }

}
