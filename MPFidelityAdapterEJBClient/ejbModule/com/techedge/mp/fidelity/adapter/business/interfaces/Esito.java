package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class Esito implements Serializable {


    /**
	 * 
	 */
	private static final long serialVersionUID = -1038016749137314847L;
	private String esito;
    private String messaggioErrore;
    
	public String getEsito() {
		return esito;
	}
	public void setEsito(String esito) {
		this.esito = esito;
	}
	public String getMessaggioErrore() {
		return messaggioErrore;
	}
	public void setMessaggioErrore(String messaggioErrore) {
		this.messaggioErrore = messaggioErrore;
	}
    
}
