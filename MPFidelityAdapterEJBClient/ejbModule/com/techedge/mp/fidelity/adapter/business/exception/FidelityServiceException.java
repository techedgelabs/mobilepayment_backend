package com.techedge.mp.fidelity.adapter.business.exception;

public class FidelityServiceException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -7543183125480034594L;

    
    public FidelityServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public FidelityServiceException(String message) {
        super(message);
    }



}
