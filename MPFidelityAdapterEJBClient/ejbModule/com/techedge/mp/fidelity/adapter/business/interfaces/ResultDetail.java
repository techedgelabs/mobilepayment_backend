package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ResultDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3764224455481563130L;

    public String             transactionId;
    public EnumStatusType     status;
    public CodeEnum           code;
    public String             message;
    public List<KeyValueInfo> params = new ArrayList<KeyValueInfo>(0);

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public EnumStatusType getStatus() {
        return status;
    }

    public void setStatus(EnumStatusType status) {
        this.status = status;
    }

    public CodeEnum getCode() {
        return code;
    }

    public void setCode(CodeEnum code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<KeyValueInfo> getParams() {
        return params;
    }

    public void setParams(List<KeyValueInfo> params) {
        this.params = params;
    }

}
