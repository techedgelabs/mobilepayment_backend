package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;
import java.util.Date;

public class VoucherDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6641110890653731480L;

    private String            voucherCode;
    private String            voucherStatus;
    private String            voucherType;
    private Double            voucherValue;
    private Double            initialValue;
    private Double            consumedValue;
    private Double            voucherBalanceDue;
    private Date              expirationDate;
    private String            promoCode;
    private String            promoDescription;
    private String            promoDoc;
    private Double            minQuantity;
    private Double            minAmount;
    //private List<ProductDetail> validProduct     = new ArrayList<ProductDetail>(0);
    //private List<RefuelMode>    validRefuelMode  = new ArrayList<RefuelMode>(0);
    private String            validPV;
    private String            isCombinable;
    private String            promoPartner;
    private String            icon;

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public Double getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(Double voucherValue) {
        this.voucherValue = voucherValue;
    }

    public Double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Double initialValue) {
        this.initialValue = initialValue;
    }

    public Double getConsumedValue() {
        return consumedValue;
    }

    public void setConsumedValue(Double consumedValue) {
        this.consumedValue = consumedValue;
    }

    public Double getVoucherBalanceDue() {
        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(Double voucherBalanceDue) {
        this.voucherBalanceDue = voucherBalanceDue;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoDoc() {
        return promoDoc;
    }

    public void setPromoDoc(String promoDoc) {
        this.promoDoc = promoDoc;
    }

    public Double getMinQuantity() {
        return minQuantity;
    }

    public void setMinQuantity(Double minQuantity) {
        this.minQuantity = minQuantity;
    }

    public Double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(Double minAmount) {
        this.minAmount = minAmount;
    }

    /*
     * public List<ProductDetail> getValidProduct() {
     * return validProduct;
     * }
     * 
     * public void setValidProduct(List<ProductDetail> validProduct) {
     * this.validProduct = validProduct;
     * }
     * 
     * public List<RefuelMode> getValidRefuelMode() {
     * return validRefuelMode;
     * }
     * 
     * public void setValidRefuelMode(List<RefuelMode> validRefuelMode) {
     * this.validRefuelMode = validRefuelMode;
     * }
     */
    public String getValidPV() {
        return validPV;
    }

    public void setValidPV(String validPV) {
        this.validPV = validPV;
    }

    public String getIsCombinable() {
        return isCombinable;
    }

    public void setIsCombinable(String isCombinable) {
        this.isCombinable = isCombinable;
    }

    public String getPromoPartner() {
        return promoPartner;
    }

    public void setPromoPartner(String promoPartner) {
        this.promoPartner = promoPartner;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

}
