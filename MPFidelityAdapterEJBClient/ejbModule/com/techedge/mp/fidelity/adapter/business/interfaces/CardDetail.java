package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class CardDetail implements Serializable {

    /**
     * 
     */

    private static final long serialVersionUID = -688500564465926204L;
    private String            panCode;
    private String            eanCode;
    private String            cardType;
    private String            virtual;

    public String getPanCode() {
        return panCode;
    }

    public void setPanCode(String panCode) {
        this.panCode = panCode;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getVirtual() {
        return virtual;
    }

    public void setVirtual(String virtual) {
        this.virtual = virtual;
    }

}
