package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LoadLoyaltyCreditsResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3607813193366208737L;

    private String            csTransactionID;
    private String            statusCode;
    private String            messageCode;
    private Integer           credits;
    private Integer           balance;
    private Double            balanceAmount;
    private String            cardCodeIssuer;
    private String            eanCode;
    private String            cardStatus;
    private String            cardType;
    private String            cardClassification;
    private String            marketingMsg;
    private String            warningMsg;
    private List<VoucherDetail> voucherList      = new ArrayList<VoucherDetail>(0);

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getCardCodeIssuer() {
        return cardCodeIssuer;
    }

    public void setCardCodeIssuer(String cardCodeIssuer) {
        this.cardCodeIssuer = cardCodeIssuer;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardClassification() {
        return cardClassification;
    }

    public void setCardClassification(String cardClassification) {
        this.cardClassification = cardClassification;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public List<VoucherDetail> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<VoucherDetail> voucherList) {
        this.voucherList = voucherList;
    }
}
