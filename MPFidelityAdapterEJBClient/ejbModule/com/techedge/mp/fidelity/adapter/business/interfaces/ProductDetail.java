package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class ProductDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 561526689499092479L;

    private String            productCode;
    private Double            quantity;
    private Double            amount;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Double getQuantity() {
        return quantity;
    }

    public void setQuantity(Double quantity) {
        this.quantity = quantity;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
