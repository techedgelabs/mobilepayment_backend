package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class AuthorizationPlusResult implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4814544185044431874L;
	private AuthorizationPlusResponsePojo request;

	public AuthorizationPlusResponsePojo getRequest() {
		return request;
	}

	public void setRequest(AuthorizationPlusResponsePojo request) {
		this.request = request;
	}

}
