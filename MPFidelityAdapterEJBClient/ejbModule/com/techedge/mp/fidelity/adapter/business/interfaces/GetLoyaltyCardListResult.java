package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class GetLoyaltyCardListResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 7570034821745975185L;

    private String            csTransactionID;
    private String            statusCode;
    private String            messageCode;
    private Integer           balance;
    private List<CardDetail>  cardList         = new ArrayList<CardDetail>(0);

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public List<CardDetail> getCardList() {
        return cardList;
    }

    public void setCardList(List<CardDetail> cardList) {
        this.cardList = cardList;
    }

}
