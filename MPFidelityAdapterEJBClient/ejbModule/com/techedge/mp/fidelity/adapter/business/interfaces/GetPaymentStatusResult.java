package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class GetPaymentStatusResult implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6842132219111408452L;
	private ResultDetail            result;
    private ExecutePaymentResult executePaymentResult;
    private ExecuteAuthorizationResult executeAuthorizationResult;
    private ExecuteCaptureResult executeCaptureResult;
    private ExecuteReversalResult executeReversalResult;

    
    public ResultDetail getResult() {
        return result;
    }

    public void setResult(ResultDetail result) {
        this.result = result;
    }

	public ExecutePaymentResult getExecutePaymentResult() {
		return executePaymentResult;
	}

	public void setExecutePaymentResult(ExecutePaymentResult executePaymentResult) {
		this.executePaymentResult = executePaymentResult;
	}

	public ExecuteAuthorizationResult getExecuteAuthorizationResult() {
		return executeAuthorizationResult;
	}

	public void setExecuteAuthorizationResult(
			ExecuteAuthorizationResult executeAuthorizationResult) {
		this.executeAuthorizationResult = executeAuthorizationResult;
	}

	public ExecuteCaptureResult getExecuteCaptureResult() {
		return executeCaptureResult;
	}

	public void setExecuteCaptureResult(ExecuteCaptureResult executeCaptureResult) {
		this.executeCaptureResult = executeCaptureResult;
	}

	public ExecuteReversalResult getExecuteReversalResult() {
		return executeReversalResult;
	}

	public void setExecuteReversalResult(ExecuteReversalResult executeReversalResult) {
		this.executeReversalResult = executeReversalResult;
	}

}
