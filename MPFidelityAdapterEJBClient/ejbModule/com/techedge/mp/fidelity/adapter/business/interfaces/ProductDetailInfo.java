package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class ProductDetailInfo implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7525503623967816090L;
    private String            productCode;
    private String            productDescription;

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

}
