package com.techedge.mp.fidelity.adapter.business;

import javax.ejb.Remote;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.DeleteMcCardRefuelingResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetMcCardStatusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Remote
public interface CardInfoServiceRemote {

    public GetMcCardStatusResult getMcCardStatus(String operationID, String userId, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException;
    public DeleteMcCardRefuelingResult deleteMcCardRefueling(String operationID, String userId, PartnerType partnerType, Long requestTimestamp, String mcCardDpan, String serverSerialNumber) throws FidelityServiceException;
}
