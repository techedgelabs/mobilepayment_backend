package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;

public class DpanDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -5187380217363165784L;

    protected String          mcCardDpan;
    protected String          mcCardPan;
    protected String          status;
    protected Boolean         mcCardKmRequired;
    protected Boolean         mcCardInfoRequired;
    protected String          mcCardServiceCode;

    public String getMcCardDpan() {
        return mcCardDpan;
    }

    public void setMcCardDpan(String mcCardDpan) {
        this.mcCardDpan = mcCardDpan;
    }

    public String getMcCardPan() {
        return mcCardPan;
    }

    public void setMcCardPan(String mcCardPan) {
        this.mcCardPan = mcCardPan;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getMcCardKmRequired() {
        return mcCardKmRequired;
    }

    public void setMcCardKmRequired(Boolean mcCardKmRequired) {
        this.mcCardKmRequired = mcCardKmRequired;
    }

    public Boolean getMcCardInfoRequired() {
        return mcCardInfoRequired;
    }

    public void setMcCardInfoRequired(Boolean mcCardInfoRequired) {
        this.mcCardInfoRequired = mcCardInfoRequired;
    }

    public String getMcCardServiceCode() {
        return mcCardServiceCode;
    }

    public void setMcCardServiceCode(String mcCardServiceCode) {
        this.mcCardServiceCode = mcCardServiceCode;
    }

}
