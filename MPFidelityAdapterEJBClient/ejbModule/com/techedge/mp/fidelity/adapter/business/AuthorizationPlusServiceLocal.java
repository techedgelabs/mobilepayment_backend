package com.techedge.mp.fidelity.adapter.business;

import javax.ejb.Local;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.AuthorizationPlusResult;

@Local
public interface AuthorizationPlusServiceLocal {

	public AuthorizationPlusResult authorizationPlus(String accessToken) throws FidelityServiceException;
	
}
