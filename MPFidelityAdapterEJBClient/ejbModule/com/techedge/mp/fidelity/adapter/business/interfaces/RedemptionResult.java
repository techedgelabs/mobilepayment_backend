
package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;


public class RedemptionResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7300113966780979573L;
    private Integer redemptionCode;
    private VoucherDetail voucher;
    private Integer credits;
    private Double balance;
    private Double balanceAmount;
    private String marketingMsg;
    private String warningMsg;
    private String csTransactionID;
    private String statusCode;
    private String messageCode;

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String value) {
        this.csTransactionID = value;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    public Integer getRedemptionCode() {
        return redemptionCode;
    }

    public void setRedemptionCode(Integer value) {
        this.redemptionCode = value;
    }

    public VoucherDetail getVoucher() {
        return voucher;
    }

    public void setVoucher(VoucherDetail value) {
        this.voucher = value;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer value) {
        this.credits = value;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double value) {
        this.balance = value;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double value) {
        this.balanceAmount = value;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String value) {
        this.marketingMsg = value;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String value) {
        this.warningMsg = value;
    }

}
