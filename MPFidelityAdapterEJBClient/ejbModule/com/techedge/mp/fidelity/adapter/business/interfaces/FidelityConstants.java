package com.techedge.mp.fidelity.adapter.business.interfaces;

public class FidelityConstants {

    //public final static String VOUCHER_TYPE                 = "ENI";

    //public final static String PARTNER_TYPE                 = "MP";

    public final static String PAYMENT_METHOD_MULTICARD     = "M";
    public final static String PAYMENT_METHOD_VOUCHER       = "V";
    public final static String PAYMENT_METHOD_ENI_CARD      = "P";
    public final static String PAYMENT_METHOD_OTHER         = "A";

    public final static String REFUEL_MODE_IPERSELF_POSTPAY = "F";
    public final static String REFUEL_MODE_SERVITO          = "S";
    public final static String REFUEL_MODE_IPERSELF_PREPAY  = "C";
    public final static String REFUEL_MODE_PARKING          = "C";
    public final static String REFUEL_MODE_ENI_CAFE_ONLY    = "0";

    public final static String PRODUCT_CODE_GASOLIO         = "11";
    public final static String PRODUCT_CODE_SP              = "24";
    public final static String PRODUCT_CODE_BLUE_DIESEL     = "32";
    public final static String PRODUCT_CODE_GPL             = "26";
    public final static String PRODUCT_CODE_BLUE_SUPER      = "29";
    public final static String PRODUCT_CODE_METANO          = "39";
    public final static String PRODUCT_CODE_NON_OIL         = "70";
    public final static String PRODUCT_CODE_PARKING         = "73";

    public final static String LANGUAGE_ITALIAN             = "it";

    public final static String CONSUME_TYPE_PARTIAL         = "P";
    public final static String CONSUME_TYPE_TOTAL           = "T";

}
