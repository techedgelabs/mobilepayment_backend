package com.techedge.mp.fidelity.adapter.business.interfaces;

public enum VoucherType {
    
    SCALARE("SCALARE"),
    INTERO("INTERO");
    
    private final String value;

    VoucherType(final String value) {
        this.value = value;
    }

    public String getValue() { 
        
        return value; 
    }
    
    

}
