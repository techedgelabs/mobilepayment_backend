package com.techedge.mp.fidelity.adapter.business.interfaces;

public enum ConsumeType {

    T("T"),
    P("P");
    
    private final String value;

    ConsumeType(final String value) {
        this.value = value;
    }

    public String getValue() { 
    
        return value; 
    }

}
