package com.techedge.mp.fidelity.adapter.business.interfaces;

public enum VoucherConsumerType {
    
    ENI("ENI");
    
    private final String value;

    VoucherConsumerType(final String value) {
        this.value = value;
    }

    public String getValue() { 
        
        return value; 
    }
    
    

}
