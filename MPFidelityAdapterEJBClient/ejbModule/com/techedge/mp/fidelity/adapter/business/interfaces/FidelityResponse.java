package com.techedge.mp.fidelity.adapter.business.interfaces;

public class FidelityResponse {

    public final static String ENABLE_LOYALTY_CARD_OK                                         = "00";
    public final static String ENABLE_LOYALTY_CARD_GENERIC_ERROR                              = "9997";
    public final static String ENABLE_LOYALTY_CARD_NOT_FOUND                                  = "0131";
    public final static String ENABLE_LOYALTY_CARD_USER_NOT_MATCHES                           = "3";
    public final static String ENABLE_LOYALTY_CARD_BLACKLIST                                  = "0132";
    public final static String ENABLE_LOYALTY_CARD_MIGRATION_ONLINE_ERROR                     = "7033";
    public final static String ENABLE_LOYALTY_CARD_MIGRATION_ERROR                            = "7034";

    public final static String GET_AVAILABLE_LOYALTY_CARD_OK                                  = "00";
    public final static String GET_AVAILABLE_LOYALTY_CARD_GENERIC_ERROR                       = "9997";
    public final static String GET_AVAILABLE_LOYALTY_CARD_CF_NOT_FOUND                        = "2";
    public final static String GET_AVAILABLE_LOYALTY_CARD_MIGRATION_ERROR                     = "7034";

    public final static String CHECK_VOUCHER_OK                                               = "00";
    public final static String CHECK_VOUCHER_EMPTY_LIST                                       = "7030";     //Lista Voucher vuota
    public final static String CHECK_VOUCHER_MAX_LIMIT_REACHED                                = "7031";     //Numero Voucher > 200
    public final static String CHECK_VOUCHER_GENERIC_ERROR                                    = "9997";     //Errore generico

    public final static String CONSUME_VOUCHER_OK                                             = "00";
    public final static String CONSUME_VOUCHER_MAX_LIMIT_REACHED                              = "7031";
    public final static String CONSUME_VOUCHER_GENERIC_ERROR                                  = "9997";
    public final static String CONSUME_VOUCHER_REFUEL_MOD_NOT_MANAGED                         = "4001";
    public final static String CONSUME_VOUCHER_PAYMENT_MOD_NOT_MANAGED                        = "4003";
    public final static String CONSUME_VOUCHER_REFUEL_MOD_NOT_ENABLED                         = "4006";
    public final static String CONSUME_VOUCHER_PRODUCT_NOT_ENABLED                            = "4004";
    public final static String CONSUME_VOUCHER_INACTIVE_PV                                    = "0112";
    public final static String CONSUME_VOUCHER_PV_NOT_ENABLED_PRODUCT                         = "4005";
    public final static String CONSUME_VOUCHER_POINT_CALCULATION_WARNING                      = "4000";
    public final static String CONSUME_VOUCHER_PV_NOT_FOUND                                   = "0111";
    public final static String CONSUME_VOUCHER_PV_NOT_ENABLED_OPERATION                       = "0114";
    public final static String CONSUME_VOUCHER_PV_NOT_ENABLED                                 = "4007";
    public final static String CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER                    = "7040";    //Importo Voucher insufficiente
    public final static String CONSUME_VOUCHER_INVALID_VOUCHER                                = "5525";    //Nessun voucher valido
    public final static String CONSUME_VOUCHER_AUTH_NOT_FOUND                                 = "7037";    //Codice preautorizzazione inesistente
    public final static String CONSUME_VOUCHER_AUTH_ALREADY_USED                              = "7039";    //Codice preautorizzazione gi� utilizzato
    public final static String CONSUME_VOUCHER_AMOUNT_MORE_THEN_PRE_AUTH_AMOUNT               = "7038";    //Importo superiore a preautorizzazione
    
    
    public final static String REVERSE_CONSUME_VOUCHER_TRANSACTION_OK                         = "00";
    /*
    public final static String CREATE_AND_CONSUME_VOUCHER_OK                                             = "00";
    public final static String CREATE_AND_CONSUME_VOUCHER_MAX_LIMIT_REACHED                              = "7031";
    public final static String CREATE_AND_CONSUME_VOUCHER_GENERIC_ERROR                                  = "9997";
    public final static String CREATE_AND_CONSUME_VOUCHER_REFUEL_MOD_NOT_MANAGED                         = "4001";
    public final static String CREATE_AND_CONSUME_VOUCHER_PAYMENT_MOD_NOT_MANAGED                        = "4003";
    public final static String CREATE_AND_CONSUME_VOUCHER_REFUEL_MOD_NOT_ENABLED                         = "4006";
    public final static String CREATE_AND_CONSUME_VOUCHER_PRODUCT_NOT_ENABLED                            = "4004";
    public final static String CREATE_AND_CONSUME_VOUCHER_INACTIVE_PV                                    = "0112";
    public final static String CREATE_AND_CONSUME_VOUCHER_POINT_CALCULATION_WARNING                      = "4000";
    public final static String CREATE_AND_CONSUME_VOUCHER_PV_NOT_FOUND                                   = "0111";
    public final static String CREATE_AND_CONSUME_VOUCHER_PV_NOT_ENABLED_OPERATION                       = "0114";


    public final static String REVERSE_CREATE_AND_CONSUME_VOUCHER_OK                                     = "00";
    public final static String REVERSE_CREATE_AND_CONSUME_VOUCHER_GENERIC_ERROR                          = "9997";
    public final static String REVERSE_CREATE_AND_CONSUME_VOUCHER_AUTH_NOT_FOUND                         = "5529";    //Autorizzazione da cancellare inesistente
    public final static String REVERSE_CREATE_AND_CONSUME_VOUCHER_AUTH_ALREADY_CANCELLED                 = "5530";    //Autorizzazione gi� cancellata
    public final static String REVERSE_CREATE_AND_CONSUME_VOUCHER_ID_TO_CANCELL_NOT_FOUND                = "5531";    //createAndConsumeVoucherOperationIDToCancel  mancante
    */
    
    public final static String CREATE_VOUCHER_OK                                              = "00";
    public final static String CREATE_VOUCHER_MAX_LIMIT_REACHED                               = "7031";
    public final static String CREATE_VOUCHER_GENERIC_ERROR                                   = "9997";
    public final static String CREATE_VOUCHER_SYSTEM_ERROR                                    = "9999";
    
    public final static String DELETE_VOUCHER_OK                                              = "00";
    public final static String DELETE_VOUCHER_MAX_LIMIT_REACHED                               = "7031";
    public final static String DELETE_VOUCHER_TRANSACTION_NOT_FOUND                  = "7025";    //Transazione da stornare inesistente
    public final static String DELETE_VOUCHER_TRANSACTION_NOT_REVERSABLE             = "7026";    //Transazione non stornabile
    public final static String DELETE_VOUCHER_TRANSACTION_EXPIRED                    = "7027";    //Transazione da stornare scaduta
    public final static String DELETE_VOUCHER_TRANSACTION_ALREADY_DELETED           = "7028";    //Transazione gi� cancellata
    public final static String DELETE_VOUCHER_OPERATION_ID_MISSING                   = "7029";    //OperationIDtoReverse mancante
    
    public final static String DELETE_VOUCHER_GENERIC_ERROR                                   = "9997";
    //public final static String DELETE_VOUCHER_TRANSACTION_NOT_FOUND                           = "5529";     //Autorizzazione da cancellare inesistente
    //public final static String DELETE_VOUCHER_TRANSACTION_ALREADY_DELETED                     = "5530";     //Autorizzazione gi� cancellata
    //public final static String DELETE_VOUCHER_OPERATION_ID_MISSING                            = "5531";     //OperationIDtoReverse mancante
    public final static String DELETE_VOUCHER_SYSTEM_ERROR                                    = "9999";     
    

    public final static String LOAD_LOYALTY_CREDITS_OK                                        = "00";
    public final static String LOAD_LOYALTY_CREDITS_MIGRATION_ERROR                           = "7034";
    public final static String LOAD_LOYALTY_CREDITS_PV_NOT_ENABLED                            = "0115";     //Pv non abilitato SAP

    public final static String REVERSE_LOAD_LOYALTY_CREDITS_OK                                  = "00";
    public final static String REVERSE_LOAD_GENERIC_ERROR                                       = "9997";     //Errore generico
    public final static String REVERSE_LOAD_TRANSACTION_NOT_FOUND                               = "7025";    //Transazione da stornare inesistente
    public final static String REVERSE_LOAD_TRANSACTION_NOT_REVERSABLE                          = "7026";    //Transazione non stornabile
    public final static String REVERSE_LOAD_TRANSACTION_EXPIRED                                 = "7027";    //Transazione da stornare scaduta
    public final static String REVERSE_LOAD_TRANSACTION_ALREADY_DELETED                         = "7028";    //Transazione gi� stornata
    public final static String REVERSE_LOAD_OPERATION_ID_MISSING                                = "7029";    //OperationIDtoReverse mancante
    
    public final static String REVERSE_CONSUME_VOUCHER_OK                                     = "00";
    public final static String REVERSE_CONSUME_VOUCHER_TRANSACTION_NOT_FOUND                  = "7025";    //Transazione da stornare inesistente
    public final static String REVERSE_CONSUME_VOUCHER_TRANSACTION_NOT_REVERSABLE             = "7026";    //Transazione non stornabile
    public final static String REVERSE_CONSUME_VOUCHER_TRANSACTION_EXPIRED                    = "7027";    //Transazione da stornare scaduta
    public final static String REVERSE_CONSUME_VOUCHER_TRANSACTION_ALREADY_REVERSED           = "7028";    //Transazione gi� stornata
    public final static String REVERSE_CONSUME_VOUCHER_OPERATION_ID_MISSING                   = "7029";    //OperationIDtoReverse mancante

    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_OK                           = "00";      //OK
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_GENERIC_ERROR                = "9997";    //Errore generico
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_MAX_LIMIT_REACHED            = "7031";    //Numero Voucher > 200
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_REFUEL_MOD_NOT_MANAGED       = "4001";    //Mod rifornimento non gestita
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_PAYMENT_MOD_NOT_MANAGED      = "4003";    //Mod pagamento non gestita
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_REFUEL_MOD_NOT_ENABLED       = "4006";    //Mod rifornimento non abilitata
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_INACTIVE_PV                  = "0112";    //PV bloccato o non attivo
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_POINT_CALCULATION_WARNING    = "4000";    //Errore su calcolo voucher
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_PV_NOT_FOUND                 = "0111";    //Pv non presente
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_PV_NOT_ENABLED_OPERATION     = "0114";    //Pv non abilitato all�operazione
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_INVALID_VOUCHER              = "5525";    //Nessun voucher valido
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER  = "5526";    //Importo Voucher insufficiente
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_INSUFFICIENT_AMOUNT_VOUCHER2 = "7040";    //Importo Voucher insufficiente
    public final static String PRE_AUTHORIZATION_CONSUME_VOUCHER_PV_NOT_ENABLED               = "4007";    //Terminale non abilitato a prodotto

    public final static String CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_OK                    = "00";       //OK
    public final static String CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_GENERIC_ERROR         = "9997";     //Errore generico
    public final static String CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_AUTH_NOT_FOUND        = "5529";     //Autorizzazione da cancellare inesistente
    public final static String CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_AUTH_ALREADY_CANCELED = "5530";     //Autorizzazione gi� cancellata
    public final static String CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_OPERATION_ID_MISSING  = "5531";     //preAuthOperationIDToCancelmancante

    public final static String INFO_REDEMPTION_OK                                             = "00";       //OK
    public final static String INFO_REDEMPTION_GENERIC_ERROR                                  = "9997";     //Errore generico
    public final static String INFO_REDEMPTION_CUSTOMER_BLOCKED                               = "0133";     //Cliente bloccato
    public final static String INFO_REDEMPTION_INVALID_FISCAL_CODE                            = "7005";     //C.F. Non valido
    public final static String INFO_REDEMPTION_INVALID_BALANCE                                = "5505";     //Saldo insufficiente

    public final static String REDEMPTION_OK                                                  = "00";       //OK
    public final static String REDEMPTION_GENERIC_ERROR                                       = "9997";     //Errore generico
    public final static String REDEMPTION_CUSTOMER_BLOCKED                                    = "0133";     //Cliente bloccato
    public final static String REDEMPTION_INVALID_FISCAL_CODE                                 = "7005";     //C.F. Non valido
    public final static String REDEMPTION_INVALID_BALANCE                                     = "5505";     //Saldo insufficiente
    
    public final static String CREATE_VOUCHER_PROMOTIONAL_OK                                              = "00";   //OK
    public final static String CREATE_VOUCHER_PROMOTIONAL_GENERIC_ERROR                                   = "9997"; //Errore generico
    public final static String CREATE_VOUCHER_PROMOTIONAL_FISCAL_CODE_NOT_FOUND                           = "7001";  //C.F. Non presente
    public final static String CREATE_VOUCHER_PROMOTIONAL_INVALID_FISCAL_CODE                             = "7005";     //C.F. Non valido
    public final static String CREATE_VOUCHER_PROMOTIONAL_FISCAL_CODE_NOT_ASSOCIATE                       = "7020";     //C.F. Non associato al cliente
    public final static String CREATE_VOUCHER_PROMOTIONAL_INVALID_PROMO_CODE                              = "7054"; //Codice Promozione non esistente
    
    public final static String CHECK_LOAD_LOYALTY_CREDITS_TRANSACTION_OK                      = "00";   //OK
    public final static String CHECK_LOAD_LOYALTY_CREDITS_TRANSACTION_ID_REQUEST_NOT_FOUND    = "7004"; //ID RICHIESTA NON PRESENTE
}
