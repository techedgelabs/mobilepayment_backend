package com.techedge.mp.fidelity.adapter.business.utilities;

public class StatusCode {

    public final static String SYSTEM_ERROR                         = "SYSTEM_ERROR_500";

    public final static String FIDELITY_CHECK_VOUCHER_SUCCESS       = "FIDELITY_CHECK_VOUCHER_200";
    public final static String FIDELITY_CHECK_VOUCHER_FAILURE       = "FIDELITY_CHECK_VOUCHER_300";

    public final static String FIDELITY_ENABLE_LOYALTY_CARD_SUCCESS = "FIDELITY_ENABLE_LOYALTY_CARD_200";
    public final static String FIDELITY_ENABLE_LOYALTY_CARD_FAILURE = "FIDELITY_ENABLE_LOYALTY_CARD_300";

}
