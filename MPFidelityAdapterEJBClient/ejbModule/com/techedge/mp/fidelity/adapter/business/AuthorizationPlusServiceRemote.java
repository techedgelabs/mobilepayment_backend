package com.techedge.mp.fidelity.adapter.business;

import javax.ejb.Remote;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.AuthorizationPlusResult;

@Remote
public interface AuthorizationPlusServiceRemote {
	public AuthorizationPlusResult authorizationPlus(String accessToken) throws FidelityServiceException;

}
