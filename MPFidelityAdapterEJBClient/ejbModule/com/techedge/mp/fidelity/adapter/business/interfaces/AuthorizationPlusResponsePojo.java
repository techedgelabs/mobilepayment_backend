package com.techedge.mp.fidelity.adapter.business.interfaces;

import java.io.Serializable;


public class AuthorizationPlusResponsePojo implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6972893550596958439L;

	private Esito esito;
	private User user;
	private Customer customer;
	public Esito getEsito() {
		return esito;
	}
	public void setEsito(Esito esito) {
		this.esito = esito;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	

}
