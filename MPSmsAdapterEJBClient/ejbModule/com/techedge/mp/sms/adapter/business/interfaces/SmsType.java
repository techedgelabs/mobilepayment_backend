package com.techedge.mp.sms.adapter.business.interfaces;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public enum SmsType {
    MOBILE_PHONE_VERIFICATION("mobile_phone_verification.txt"),
    MOBILE_PHONE_VERIFICATION_V2("v2/mobile_phone_verification.txt"),
    MOBILE_PHONE_VERIFICATION_BUSINESS("v2/mobile_phone_verification_business.txt"),
    DEVICE_VERIFICATION_V2("v2/device_verification.txt"),
    DEVICE_VERIFICATION_BUSINESS("v2/device_verification_business.txt");
    
    private final String template;

    SmsType(final String template) {
        this.template = template;
    }

    public String getTemplate() {
        return template; 
    }

    public String getTemplateContent() {
        
        BufferedReader br = null;
        String currentLine = "";
        String content = "";
        
        String urlTemplate = System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "sms" + File.separator + this.template;
        
        try {
            br = new BufferedReader(new FileReader(urlTemplate));
            
            while ((currentLine = br.readLine()) != null) {
                content = content + currentLine;
            }
        } catch (IOException e) {
            System.out.println("Error opening template mail file: " + urlTemplate + " message: " + e.getMessage());
        } finally {
            try {
                if ( br != null) {
                    br.close();
                }
            } catch (IOException e) {
                System.out.println("Error closing template mail file");
            }
        }
        
       return content;
    }
    
    
    
}
