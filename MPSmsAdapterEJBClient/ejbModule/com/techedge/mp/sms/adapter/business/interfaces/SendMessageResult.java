package com.techedge.mp.sms.adapter.business.interfaces;

import java.io.Serializable;

public class SendMessageResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7355996410110971066L;
    private String correlationId;
    private String messageId;
    private Integer responseCode;
    private Integer reasonCode;
    private String responseMessage;
    private Boolean temporaryError;
    private Integer billingStatus;
    private Double vat;

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String value) {
        this.correlationId = value;
    }

    public String getMessageId() {
        return messageId;
    }

    public void setMessageId(String value) {
        this.messageId = value;
    }

    public Integer getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(Integer value) {
        this.responseCode = value;
    }

    public Integer getReasonCode() {
        return reasonCode;
    }

    public void setReasonCode(Integer value) {
        this.reasonCode = value;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String value) {
        this.responseMessage = value;
    }

    public Boolean getTemporaryError() {
        return temporaryError;
    }

    public void setTemporaryError(Boolean value) {
        this.temporaryError = value;
    }

    public Integer getBillingStatus() {
        return billingStatus;
    }

    public void setBillingStatus(Integer value) {
        this.billingStatus = value;
    }

    public Double getVAT() {
        return vat;
    }

    public void setVAT(Double value) {
        this.vat = value;
    }    
}
