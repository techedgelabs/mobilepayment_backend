package com.techedge.mp.sms.adapter.business;

import java.util.List;

import javax.ejb.Local;

import com.techedge.mp.sms.adapter.business.interfaces.Parameter;
import com.techedge.mp.sms.adapter.business.interfaces.SendMessageResult;
import com.techedge.mp.sms.adapter.business.interfaces.SmsType;

@Local
public interface SmsServiceLocal {

    public SendMessageResult sendShortMessage(SmsType smsType, String destinationAddress, List<Parameter> parameters, String messageId);
    
    public SendMessageResult sendShortMessage(SmsType smsType, String senderAlias, String destinationAddress, List<Parameter> parameters, String messageId);

    public SendMessageResult sendShortMessage(String senderAlias, String destinationAddress, String message, String messageId);
    
    public SendMessageResult sendShortMessage(String destinationAddress, String message, String messageId);

    public String getSender();
    
}
