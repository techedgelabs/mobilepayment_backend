package com.techedge.mp.sms.adapter.business.interfaces;

public enum ResponseCodeType {
    /*0("Success"),
    1("Invalid login or un-authorized API usage"),
    2("Consumer is blocked by Netsize"),
    3("Operation is not provisioned by Netsize"),
    4("The Consumer is unknown to Netsize"),
    5("Consumer has blocked this service in Netsize"),
    6("The originating address is not supported"),
    7("Alpha originating address not supported by account"),
    8("MSISDN originating address not supported by account"),
    9("GSM extended not supported by account"),
    10("Unicode not supported by account"),
    11("Status report not supported by account"),
    12("Required capability not supported"),
    13("Could not route message"),
    14("The content provider max throttling rate is exceeded"),
    15("The account max throttling rate is exceeded"),
    16("Protocol ID not supported by account"),
    17("The service provider charging frequency is exceeded"),
    18("The service is blocked"),
    19("Operation is not provisioned for the operator"),
    20("Prohibited time period"),
    50("Partial success: (<1>;<2>;<n>)"),
    99("Internal server error"),
    100("Invalid destination address"),
    101("Invalid tariff class (price)"),
    102("Invalid referenced (linked) ID"),
    103("Invalid account name"),
    104("Invalid service category"),
    105("Invalid service meta data"),
    106("Invalid originating address"),
    107("Invalid alphanumeric originating address"),
    108("Invalid validity time"),
    109("Invalid delivery time"),
    110("Invalid message content/user data"),
    111("Invalid message length"),
    112("Invalid user data header"),
    113("Invalid data coding scheme"),
    114("Invalid protocol ID"),
    115("Invalid status report flags"),
    116("Invalid TON"),
    117("Invalid VAT"),
    118("Invalid campaign name"),
    119("Invalid service name"),
    120("Invalid service ID"),
    200("Operator integration error"),
    201("Communication problems"),
    202("Read timeout"),
    299("Integration error");
    
    private final String message;
    
    ResponseCodeType(String message) {
        this.message = message;
    }
    
    public String getMessage() {
        return message;
    }*/
}
