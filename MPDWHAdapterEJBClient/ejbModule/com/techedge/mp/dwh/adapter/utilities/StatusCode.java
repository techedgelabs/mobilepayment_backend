package com.techedge.mp.dwh.adapter.utilities;

public class StatusCode {

    //Operazione eseguita con successo
    public final static String DWH_ADAPTER_RESULT_SUCCESS                  = "DWH_ADAPTER_RESULT_200";
    
    //Utente esistente con registrazione lite
    public final static String DWH_ADAPTER_RESULT_REG_LITE                 = "DWH_ADAPTER_RESULT_201";

    //Chiamata formata in modo errato
    public final static String DWH_ADAPTER_RESULT_BAD_REQUEST              = "DWH_ADAPTER_RESULT_400";
    
    //Parametri obbligatori mancanti
    public final static String DWH_ADAPTER_RESULT_MISSING_PARAMETERS       = "DWH_ADAPTER_RESULT_401";

    //Utente non trovato con mail fornita
    public final static String DWH_ADAPTER_RESULT_USER_NOT_FOUND           = "DWH_ADAPTER_RESULT_402";

    //Credenziali non corrette
    public final static String DWH_ADAPTER_RESULT_BAD_CREDENTIALS          = "DWH_ADAPTER_RESULT_403";
    
    //RequestId esistente
    public final static String DWH_ADAPTER_RESULT_REQUESTID_ALREADY_EXISTS = "DWH_ADAPTER_RESULT_404";
    
    //Punti insufficienti
    public final static String DWH_ADAPTER_RESULT_INSUFFICIENT_POINT       = "DWH_ADAPTER_RESULT_405";
    
    //Premio non trovato
    public final static String DWH_ADAPTER_RESULT_AWARD_NOT_FOUND          = "DWH_ADAPTER_RESULT_406";
    
    //Premio scaduto
    public final static String DWH_ADAPTER_RESULT_AWARD_OUT_OF_DATE        = "DWH_ADAPTER_RESULT_407";

    //Errore generico del servizio
    public final static String DWH_ADAPTER_RESULT_SYSTEM_ERROR             = "DWH_ADAPTER_RESULT_500";

    public final static String DWH_ADAPTER_RESULT_UNKNOWN                  = "DWH_ADAPTER_RESULT_300";

}
