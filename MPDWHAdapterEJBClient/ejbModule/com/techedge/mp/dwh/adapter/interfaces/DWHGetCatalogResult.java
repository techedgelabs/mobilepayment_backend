package com.techedge.mp.dwh.adapter.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DWHGetCatalogResult implements Serializable {

    /**
     * 
     */

    private static final long        serialVersionUID       = 7256210044098337994L;

    private String                   statusCode;
    private String                   messageCode;
    private List<AwardDetail>        awardDetailList        = new ArrayList<AwardDetail>();

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    public List<AwardDetail> getAwardDetailList() {
        return awardDetailList;
    }

    public void setAwardDetailList(List<AwardDetail> awardDetailList) {
        this.awardDetailList = awardDetailList;
    }

}
