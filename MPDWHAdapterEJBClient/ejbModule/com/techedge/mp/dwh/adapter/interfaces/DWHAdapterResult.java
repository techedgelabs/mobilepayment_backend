package com.techedge.mp.dwh.adapter.interfaces;

import java.io.Serializable;

public class DWHAdapterResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -8435281735388053973L;
    private UserDetail userDetail;
    private String statusCode;
    private String messageCode;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    public UserDetail getUserDetail() {
        return userDetail;
    }

    public void setUserDetail(UserDetail userDetail) {
        this.userDetail = userDetail;
    }
    

}
