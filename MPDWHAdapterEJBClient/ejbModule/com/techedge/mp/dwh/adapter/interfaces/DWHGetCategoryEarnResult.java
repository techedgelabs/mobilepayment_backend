package com.techedge.mp.dwh.adapter.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DWHGetCategoryEarnResult implements Serializable {

    /**
     * 
     */
    private static final long        serialVersionUID = -6382608426386234203L;

    private String                   statusCode;
    private String                   messageCode;
    private List<CategoryEarnDetail> categoryEarnList = new ArrayList<CategoryEarnDetail>(0);

    public List<CategoryEarnDetail> getCategoryEarnList() {
        return categoryEarnList;
    }

    public void setCategoryEarnList(List<CategoryEarnDetail> categoryEarnList) {
        this.categoryEarnList = categoryEarnList;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

}
