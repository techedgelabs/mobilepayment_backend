package com.techedge.mp.dwh.adapter.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DWHGetPartnerResult implements Serializable {

    /**
     * 
     */
    private static final long    serialVersionUID   = -8435281735388053973L;
    private List<PartnerDetail>  partnerDetailList  = new ArrayList<PartnerDetail>(0);
    
    private String               statusCode;
    private String               messageCode;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    public List<PartnerDetail> getPartnerDetailList() {
        return partnerDetailList;
    }

    public void setPartnerDetailList(List<PartnerDetail> partnerDetailList) {
        this.partnerDetailList = partnerDetailList;
    }

}
