package com.techedge.mp.dwh.adapter.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class DWHGetBrandResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6382608426386234203L;

    private String            statusCode;
    private String            messageCode;
    private List<BrandDetail> brandDetailList = new ArrayList<BrandDetail>(0);
    public String getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
    public String getMessageCode() {
        return messageCode;
    }
    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }
    public List<BrandDetail> getBrandDetailList() {
        return brandDetailList;
    }
    public void setBrandDetailList(List<BrandDetail> brandDetailList) {
        this.brandDetailList = brandDetailList;
    }
    
}
