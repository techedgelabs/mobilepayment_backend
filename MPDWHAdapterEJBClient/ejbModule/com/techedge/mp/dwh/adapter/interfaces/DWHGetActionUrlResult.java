package com.techedge.mp.dwh.adapter.interfaces;

import java.io.Serializable;

public class DWHGetActionUrlResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -7301337965257311829L;

    private String            actionUrl;
    private String            key;
    private int               actionForwardId;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public int getActionForwardId() {
        return actionForwardId;
    }

    public void setActionForwardId(int actionForwardId) {
        this.actionForwardId = actionForwardId;
    }

    private String statusCode;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String value) {
        this.statusCode = value;
    }

    public String getActionUrl() {
        return actionUrl;
    }

    public void setActionUrl(String actionUrl) {
        this.actionUrl = actionUrl;
    }

}
