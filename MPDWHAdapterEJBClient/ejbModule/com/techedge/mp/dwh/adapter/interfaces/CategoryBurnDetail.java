package com.techedge.mp.dwh.adapter.interfaces;

import java.io.Serializable;

public class CategoryBurnDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 8957309111183280711L;
    private Integer           categoryId;
    private String            category;
    private String            categoryImageUrl;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryImageUrl() {
        return categoryImageUrl;
    }

    public void setCategoryImageUrl(String categoryImageUrl) {
        this.categoryImageUrl = categoryImageUrl;
    }

}
