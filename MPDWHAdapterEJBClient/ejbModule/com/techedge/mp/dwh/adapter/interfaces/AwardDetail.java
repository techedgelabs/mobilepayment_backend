
package com.techedge.mp.dwh.adapter.interfaces;

import java.io.Serializable;
import java.math.BigDecimal;


public class AwardDetail implements Serializable {

    /**
     * 
     */
    
    private static final long serialVersionUID = -919851355046062657L;
    
    private Integer awardId;
    private Integer categoryId;
    private String category;
    private Integer brandId;
    private String  brand;
    private String description;
    private String descriptionShort;
    private Integer point;
    private BigDecimal value;
    private BigDecimal contribution;
    private Long dateStartRedemption;
    private Long dateStopRedemption;
    private Boolean cancelable;
    private Boolean tangible;
    private String type;
    private Boolean redeemable;
    private String urlImageAward;
    private String urlImageBrand;
    private Integer pointPartner;
    private String channelRedemption;
    private String typePointPartner;
    
    public Integer getAwardId() {
        return awardId;
    }
    public void setAwardId(Integer awardId) {
        this.awardId = awardId;
    }
    public Integer getCategoryId() {
        return categoryId;
    }
    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }
    public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    public Integer getBrandId() {
        return brandId;
    }
    public void setBrandId(Integer brandId) {
        this.brandId = brandId;
    }
    public String getBrand() {
        return brand;
    }
    public void setBrand(String brand) {
        this.brand = brand;
    }
    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }
    public String getDescriptionShort() {
        return descriptionShort;
    }
    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }
    public Integer getPoint() {
        return point;
    }
    public void setPoint(Integer point) {
        this.point = point;
    }
    public BigDecimal getValue() {
        return value;
    }
    public void setValue(BigDecimal value) {
        this.value = value;
    }
    public BigDecimal getContribution() {
        return contribution;
    }
    public void setContribution(BigDecimal contribution) {
        this.contribution = contribution;
    }
    public Long getDateStartRedemption() {
        return dateStartRedemption;
    }
    public void setDateStartRedemption(Long dateStartRedemption) {
        this.dateStartRedemption = dateStartRedemption;
    }
    public Long getDateStopRedemption() {
        return dateStopRedemption;
    }
    public void setDateStopRedemption(Long dateStopRedemption) {
        this.dateStopRedemption = dateStopRedemption;
    }
    public Boolean getCancelable() {
        return cancelable;
    }
    public void setCancelable(Boolean cancelable) {
        this.cancelable = cancelable;
    }
    public Boolean getTangible() {
        return tangible;
    }
    public void setTangible(Boolean tangible) {
        this.tangible = tangible;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public Boolean getRedeemable() {
        return redeemable;
    }
    public void setRedeemable(Boolean redeemable) {
        this.redeemable = redeemable;
    }
    public String getUrlImageAward() {
        return urlImageAward;
    }
    public void setUrlImageAward(String urlImageAward) {
        this.urlImageAward = urlImageAward;
    }
    public String getUrlImageBrand() {
        return urlImageBrand;
    }
    public void setUrlImageBrand(String urlImageBrand) {
        this.urlImageBrand = urlImageBrand;
    }
    public Integer getPointPartner() {
        return pointPartner;
    }
    public void setPointPartner(Integer pointPartner) {
        this.pointPartner = pointPartner;
    }
    public String getChannelRedemption() {
        return channelRedemption;
    }
    public void setChannelRedemption(String channelRedemption) {
        this.channelRedemption = channelRedemption;
    }
    public String getTypePointPartner() {
        return typePointPartner;
    }
    public void setTypePointPartner(String typePointPartner) {
        this.typePointPartner = typePointPartner;
    }
    
}
