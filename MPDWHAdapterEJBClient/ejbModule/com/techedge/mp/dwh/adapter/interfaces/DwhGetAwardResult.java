package com.techedge.mp.dwh.adapter.interfaces;

import java.io.Serializable;

public class DwhGetAwardResult implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2139218192205205548L;

    private String            statusCode;
    private String            messageCode;
    private String            voucher;
    private Long           orderId;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

}
