
package com.techedge.mp.dwh.adapter.interfaces;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;


public class UserDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3846033292843674249L;
    private String name;
    private String surname;
    private Date dateOfBirth;
    private String gender;
    private String fiscalCode;
    private String cityOfBirth;
    private String countryOfBirth;
    private String mobilePhoneNumber;
    private HashMap<String, Boolean> flagPrivacy = new HashMap<String, Boolean>();
    private String codCarta;
    private Boolean flagBlackList;
    
    public String getName() {
        return name;
    }

    public void setName(String value) {
        this.name = value;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String value) {
        this.surname = value;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date value) {
        this.dateOfBirth = value;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String value) {
        this.gender = value;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String value) {
        this.fiscalCode = value;
    }

    public String getCityOfBirth() {
        return cityOfBirth;
    }

    public void setCityOfBirth(String value) {
        this.cityOfBirth = value;
    }

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String value) {
        this.countryOfBirth = value;
    }

    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String value) {
        this.mobilePhoneNumber = value;
    }

    public HashMap<String, Boolean> getFlagPrivacy() {
        return this.flagPrivacy;
    }

    public String getCodCarta() {
        return codCarta;
    }

    public void setCodCarta(String value) {
        this.codCarta = value;
    }

    public Boolean getFlagBlackList() {
        return flagBlackList;
    }

    public void setFlagBlackList(Boolean flagBlackList) {
        this.flagBlackList = flagBlackList;
    }

}
