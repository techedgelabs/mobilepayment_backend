package com.techedge.mp.dwh.adapter.interfaces;

import java.io.Serializable;

public class BrandDetail implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 6502112549279049589L;

    private Integer           brandId;

    private String            brand;

    private String            brandUrl;

    public String getBrandUrl() {
        return brandUrl;
    }

    public void setBrandUrl(String brandUrl) {
        this.brandUrl = brandUrl;
    }

    public Integer getBrandId() {

        return brandId;
    }

    public void setBrandId(Integer brandId) {

        this.brandId = brandId;
    }

    public String getBrand() {

        return brand;
    }

    public void setBrand(String brand) {

        this.brand = brand;
    }

}
