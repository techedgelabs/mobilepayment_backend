package com.techedge.mp.dwh.adapter.business;

import java.util.Date;
import java.util.HashMap;

import javax.ejb.Remote;

import com.techedge.mp.dwh.adapter.interfaces.DWHAdapterResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetActionUrlResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetBrandResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCatalogResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCategoryBurnResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCategoryEarnResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetPartnerResult;
import com.techedge.mp.dwh.adapter.interfaces.DwhGetAwardResult;

@Remote
public interface DWHAdapterServiceRemote {

    public DWHAdapterResult checkLegacyPasswordPlus(String email, String password, String requestId);

    public DWHAdapterResult setUserDataPlus(String email, String password, String name, String surname, String gender, String fiscalCode, Date dateOfBirth, String cityOfBirth,
            String countryOfBirth, String mobilePhoneNumber, String codCarta, HashMap<String, Boolean> flagPrivacy, String requestId, Long requestTimestamp, Boolean flgBlacklist,
            String campaignType, Boolean isSocial, String socialType, String idSocial);

    public DWHAdapterResult changeUserPasswordPlus(String email, String password, String requestId, Long requestTimestamp);

    public DWHGetPartnerResult getPartnerList();

    public DWHGetActionUrlResult getActionUrl(String partnerId, String fiscalCode, String eanCode);

    public DWHGetCatalogResult getCatalog(String card, String requestId, Long requestTimestamp);

    public DWHGetBrandResult getBrandList(String requestId, Long requestTimestamp);
    
    public DWHGetCategoryEarnResult getCategoryEarnList();
    
    public DWHGetCategoryBurnResult getCategoryBurnList(String requestId, Long requestTimestamp);
    
    public DwhGetAwardResult getAwardResult(Integer awardId, String card, String cardPartner, String requestId, Long requestTimestamp);
}
