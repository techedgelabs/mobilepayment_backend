
package com.techedgegroup.mprefuelingprocess;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.techedgegroup.mprefuelingprocess package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _LocalContextData_QNAME = new QName("http://www.techedgegroup.com/MPRefuelingProcess/", "LocalContextData");
    private final static QName _ExtensionFieldList_QNAME = new QName("http://www.techedgegroup.com/MPRefuelingProcess/", "ExtensionFieldList");
    private final static QName _BankDetail_QNAME = new QName("http://www.techedgegroup.com/MPRefuelingProcess/", "BankDetail");
    private final static QName _RefuelDetail_QNAME = new QName("http://www.techedgegroup.com/MPRefuelingProcess/", "RefuelDetail");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.techedgegroup.mprefuelingprocess
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ExtensionFieldList }
     * 
     */
    public ExtensionFieldList createExtensionFieldList() {
        return new ExtensionFieldList();
    }

    /**
     * Create an instance of {@link StartRefuelingProcessResponse }
     * 
     */
    public StartRefuelingProcessResponse createStartRefuelingProcessResponse() {
        return new StartRefuelingProcessResponse();
    }

    /**
     * Create an instance of {@link StartRefuelingProcessRequest }
     * 
     */
    public StartRefuelingProcessRequest createStartRefuelingProcessRequest() {
        return new StartRefuelingProcessRequest();
    }

    /**
     * Create an instance of {@link BankDetail }
     * 
     */
    public BankDetail createBankDetail() {
        return new BankDetail();
    }

    /**
     * Create an instance of {@link RefuelDetail }
     * 
     */
    public RefuelDetail createRefuelDetail() {
        return new RefuelDetail();
    }

    /**
     * Create an instance of {@link LocalContextData }
     * 
     */
    public LocalContextData createLocalContextData() {
        return new LocalContextData();
    }

    /**
     * Create an instance of {@link StartRefuelResponse }
     * 
     */
    public StartRefuelResponse createStartRefuelResponse() {
        return new StartRefuelResponse();
    }

    /**
     * Create an instance of {@link StartRefuelRequest }
     * 
     */
    public StartRefuelRequest createStartRefuelRequest() {
        return new StartRefuelRequest();
    }

    /**
     * Create an instance of {@link EndRefuelResponse }
     * 
     */
    public EndRefuelResponse createEndRefuelResponse() {
        return new EndRefuelResponse();
    }

    /**
     * Create an instance of {@link EndRefuelRequest }
     * 
     */
    public EndRefuelRequest createEndRefuelRequest() {
        return new EndRefuelRequest();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LocalContextData }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.techedgegroup.com/MPRefuelingProcess/", name = "LocalContextData")
    public JAXBElement<LocalContextData> createLocalContextData(LocalContextData value) {
        return new JAXBElement<LocalContextData>(_LocalContextData_QNAME, LocalContextData.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ExtensionFieldList }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.techedgegroup.com/MPRefuelingProcess/", name = "ExtensionFieldList")
    public JAXBElement<ExtensionFieldList> createExtensionFieldList(ExtensionFieldList value) {
        return new JAXBElement<ExtensionFieldList>(_ExtensionFieldList_QNAME, ExtensionFieldList.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BankDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.techedgegroup.com/MPRefuelingProcess/", name = "BankDetail")
    public JAXBElement<BankDetail> createBankDetail(BankDetail value) {
        return new JAXBElement<BankDetail>(_BankDetail_QNAME, BankDetail.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RefuelDetail }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.techedgegroup.com/MPRefuelingProcess/", name = "RefuelDetail")
    public JAXBElement<RefuelDetail> createRefuelDetail(RefuelDetail value) {
        return new JAXBElement<RefuelDetail>(_RefuelDetail_QNAME, RefuelDetail.class, null, value);
    }

}
