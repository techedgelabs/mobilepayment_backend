package com.techedgegroup.mprefuelingprocess.clientsample;

import com.techedgegroup.mprefuelingprocess.*;

public class ClientSample {

	public static void main(String[] args) {
	        System.out.println("***********************");
	        System.out.println("Create Web Service Client...");
	        MPRefuelingProcess_Service service1 = new MPRefuelingProcess_Service();
	        System.out.println("Create Web Service...");
	        MPRefuelingProcess port1 = service1.getMPRefuelingProcessSOAP();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port1.startRefuel(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.endRefuel(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.startRefuelingProcess(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Create Web Service...");
	        MPRefuelingProcess port2 = service1.getMPRefuelingProcessSOAP();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port2.startRefuel(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.endRefuel(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.startRefuelingProcess(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("***********************");
	        System.out.println("Call Over!");
	}
}
