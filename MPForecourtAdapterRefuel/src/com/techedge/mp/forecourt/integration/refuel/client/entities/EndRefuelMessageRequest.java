/**
 * EndRefuelMessageRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client.entities;

public class EndRefuelMessageRequest  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -8151128945335883746L;

    private java.lang.String requestID;

    private java.lang.String transactionID;

    private com.techedge.mp.forecourt.integration.refuel.client.entities.RefuelDetail refuelDetail;

    public EndRefuelMessageRequest() {
    }

    public EndRefuelMessageRequest(
           java.lang.String requestID,
           java.lang.String transactionID,
           com.techedge.mp.forecourt.integration.refuel.client.entities.RefuelDetail refuelDetail) {
           this.requestID = requestID;
           this.transactionID = transactionID;
           this.refuelDetail = refuelDetail;
    }


    /**
     * Gets the requestID value for this EndRefuelMessageRequest.
     * 
     * @return requestID
     */
    public java.lang.String getRequestID() {
        return requestID;
    }


    /**
     * Sets the requestID value for this EndRefuelMessageRequest.
     * 
     * @param requestID
     */
    public void setRequestID(java.lang.String requestID) {
        this.requestID = requestID;
    }


    /**
     * Gets the transactionID value for this EndRefuelMessageRequest.
     * 
     * @return transactionID
     */
    public java.lang.String getTransactionID() {
        return transactionID;
    }


    /**
     * Sets the transactionID value for this EndRefuelMessageRequest.
     * 
     * @param transactionID
     */
    public void setTransactionID(java.lang.String transactionID) {
        this.transactionID = transactionID;
    }


    /**
     * Gets the refuelDetail value for this EndRefuelMessageRequest.
     * 
     * @return refuelDetail
     */
    public com.techedge.mp.forecourt.integration.refuel.client.entities.RefuelDetail getRefuelDetail() {
        return refuelDetail;
    }


    /**
     * Sets the refuelDetail value for this EndRefuelMessageRequest.
     * 
     * @param refuelDetail
     */
    public void setRefuelDetail(com.techedge.mp.forecourt.integration.refuel.client.entities.RefuelDetail refuelDetail) {
        this.refuelDetail = refuelDetail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof EndRefuelMessageRequest)) return false;
        EndRefuelMessageRequest other = (EndRefuelMessageRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.requestID==null && other.getRequestID()==null) || 
             (this.requestID!=null &&
              this.requestID.equals(other.getRequestID()))) &&
            ((this.transactionID==null && other.getTransactionID()==null) || 
             (this.transactionID!=null &&
              this.transactionID.equals(other.getTransactionID()))) &&
            ((this.refuelDetail==null && other.getRefuelDetail()==null) || 
             (this.refuelDetail!=null &&
              this.refuelDetail.equals(other.getRefuelDetail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getRequestID() != null) {
            _hashCode += getRequestID().hashCode();
        }
        if (getTransactionID() != null) {
            _hashCode += getTransactionID().hashCode();
        }
        if (getRefuelDetail() != null) {
            _hashCode += getRefuelDetail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EndRefuelMessageRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://refuel.integration.forecourt.mp.techedge.com/", "endRefuelMessageRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("requestID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "requestID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("refuelDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("", "refuelDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://refuel.integration.forecourt.mp.techedge.com/", "refuelDetail"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
