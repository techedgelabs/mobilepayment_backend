/**
 * Voucher.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client.entities;

public class Voucher  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -8521266228127423282L;

    private double voucherAmount;

    private java.lang.String voucherCode;

    private java.lang.String promoCode;

    private java.lang.String promoDescription;

    public Voucher() {
    }

    public Voucher(
           double voucherAmount,
           java.lang.String voucherCode,
           java.lang.String promoCode,
           java.lang.String promoDescription) {
           this.voucherAmount = voucherAmount;
           this.voucherCode = voucherCode;
           this.promoCode = promoCode;
           this.promoDescription = promoDescription;
    }


    /**
     * Gets the voucherAmount value for this Voucher.
     * 
     * @return voucherAmount
     */
    public double getVoucherAmount() {
        return voucherAmount;
    }


    /**
     * Sets the voucherAmount value for this Voucher.
     * 
     * @param voucherAmount
     */
    public void setVoucherAmount(double voucherAmount) {
        this.voucherAmount = voucherAmount;
    }


    /**
     * Gets the voucherCode value for this Voucher.
     * 
     * @return voucherCode
     */
    public java.lang.String getVoucherCode() {
        return voucherCode;
    }


    /**
     * Sets the voucherCode value for this Voucher.
     * 
     * @param voucherCode
     */
    public void setVoucherCode(java.lang.String voucherCode) {
        this.voucherCode = voucherCode;
    }


    /**
     * Gets the promoCode value for this Voucher.
     * 
     * @return promoCode
     */
    public java.lang.String getPromoCode() {
        return promoCode;
    }


    /**
     * Sets the promoCode value for this Voucher.
     * 
     * @param promoCode
     */
    public void setPromoCode(java.lang.String promoCode) {
        this.promoCode = promoCode;
    }


    /**
     * Gets the promoDescription value for this Voucher.
     * 
     * @return promoDescription
     */
    public java.lang.String getPromoDescription() {
        return promoDescription;
    }


    /**
     * Sets the promoDescription value for this Voucher.
     * 
     * @param promoDescription
     */
    public void setPromoDescription(java.lang.String promoDescription) {
        this.promoDescription = promoDescription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Voucher)) return false;
        Voucher other = (Voucher) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.voucherAmount == other.getVoucherAmount() &&
            ((this.voucherCode==null && other.getVoucherCode()==null) || 
             (this.voucherCode!=null &&
              this.voucherCode.equals(other.getVoucherCode()))) &&
            ((this.promoCode==null && other.getPromoCode()==null) || 
             (this.promoCode!=null &&
              this.promoCode.equals(other.getPromoCode()))) &&
            ((this.promoDescription==null && other.getPromoDescription()==null) || 
             (this.promoDescription!=null &&
              this.promoDescription.equals(other.getPromoDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += new Double(getVoucherAmount()).hashCode();
        if (getVoucherCode() != null) {
            _hashCode += getVoucherCode().hashCode();
        }
        if (getPromoCode() != null) {
            _hashCode += getPromoCode().hashCode();
        }
        if (getPromoDescription() != null) {
            _hashCode += getPromoDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Voucher.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://refuel.integration.forecourt.mp.techedge.com/", "voucher"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voucherAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VoucherAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voucherCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "VoucherCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PromoCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("promoDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "PromoDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
