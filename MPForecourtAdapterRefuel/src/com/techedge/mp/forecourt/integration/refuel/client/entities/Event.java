/**
 * Event.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client.entities;

public class Event  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -4442405405792353495L;

    private java.lang.String sequence;

    private com.techedge.mp.forecourt.integration.refuel.client.entities.EnumEVENTTYPE eventType;

    private java.lang.Double eventAmount;

    private com.techedge.mp.forecourt.integration.refuel.client.entities.EnumTRANSACTIONRESULT transactionResult;

    private java.lang.String errorCode;

    private java.lang.String errorDescription;

    public Event() {
    }

    public Event(
           java.lang.String sequence,
           com.techedge.mp.forecourt.integration.refuel.client.entities.EnumEVENTTYPE eventType,
           java.lang.Double eventAmount,
           com.techedge.mp.forecourt.integration.refuel.client.entities.EnumTRANSACTIONRESULT transactionResult,
           java.lang.String errorCode,
           java.lang.String errorDescription) {
           this.sequence = sequence;
           this.eventType = eventType;
           this.eventAmount = eventAmount;
           this.transactionResult = transactionResult;
           this.errorCode = errorCode;
           this.errorDescription = errorDescription;
    }


    /**
     * Gets the sequence value for this Event.
     * 
     * @return sequence
     */
    public java.lang.String getSequence() {
        return sequence;
    }


    /**
     * Sets the sequence value for this Event.
     * 
     * @param sequence
     */
    public void setSequence(java.lang.String sequence) {
        this.sequence = sequence;
    }


    /**
     * Gets the eventType value for this Event.
     * 
     * @return eventType
     */
    public com.techedge.mp.forecourt.integration.refuel.client.entities.EnumEVENTTYPE getEventType() {
        return eventType;
    }


    /**
     * Sets the eventType value for this Event.
     * 
     * @param eventType
     */
    public void setEventType(com.techedge.mp.forecourt.integration.refuel.client.entities.EnumEVENTTYPE eventType) {
        this.eventType = eventType;
    }


    /**
     * Gets the eventAmount value for this Event.
     * 
     * @return eventAmount
     */
    public java.lang.Double getEventAmount() {
        return eventAmount;
    }


    /**
     * Sets the eventAmount value for this Event.
     * 
     * @param eventAmount
     */
    public void setEventAmount(java.lang.Double eventAmount) {
        this.eventAmount = eventAmount;
    }


    /**
     * Gets the transactionResult value for this Event.
     * 
     * @return transactionResult
     */
    public com.techedge.mp.forecourt.integration.refuel.client.entities.EnumTRANSACTIONRESULT getTransactionResult() {
        return transactionResult;
    }


    /**
     * Sets the transactionResult value for this Event.
     * 
     * @param transactionResult
     */
    public void setTransactionResult(com.techedge.mp.forecourt.integration.refuel.client.entities.EnumTRANSACTIONRESULT transactionResult) {
        this.transactionResult = transactionResult;
    }


    /**
     * Gets the errorCode value for this Event.
     * 
     * @return errorCode
     */
    public java.lang.String getErrorCode() {
        return errorCode;
    }


    /**
     * Sets the errorCode value for this Event.
     * 
     * @param errorCode
     */
    public void setErrorCode(java.lang.String errorCode) {
        this.errorCode = errorCode;
    }


    /**
     * Gets the errorDescription value for this Event.
     * 
     * @return errorDescription
     */
    public java.lang.String getErrorDescription() {
        return errorDescription;
    }


    /**
     * Sets the errorDescription value for this Event.
     * 
     * @param errorDescription
     */
    public void setErrorDescription(java.lang.String errorDescription) {
        this.errorDescription = errorDescription;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Event)) return false;
        Event other = (Event) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sequence==null && other.getSequence()==null) || 
             (this.sequence!=null &&
              this.sequence.equals(other.getSequence()))) &&
            ((this.eventType==null && other.getEventType()==null) || 
             (this.eventType!=null &&
              this.eventType.equals(other.getEventType()))) &&
            ((this.eventAmount==null && other.getEventAmount()==null) || 
             (this.eventAmount!=null &&
              this.eventAmount.equals(other.getEventAmount()))) &&
            ((this.transactionResult==null && other.getTransactionResult()==null) || 
             (this.transactionResult!=null &&
              this.transactionResult.equals(other.getTransactionResult()))) &&
            ((this.errorCode==null && other.getErrorCode()==null) || 
             (this.errorCode!=null &&
              this.errorCode.equals(other.getErrorCode()))) &&
            ((this.errorDescription==null && other.getErrorDescription()==null) || 
             (this.errorDescription!=null &&
              this.errorDescription.equals(other.getErrorDescription())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSequence() != null) {
            _hashCode += getSequence().hashCode();
        }
        if (getEventType() != null) {
            _hashCode += getEventType().hashCode();
        }
        if (getEventAmount() != null) {
            _hashCode += getEventAmount().hashCode();
        }
        if (getTransactionResult() != null) {
            _hashCode += getTransactionResult().hashCode();
        }
        if (getErrorCode() != null) {
            _hashCode += getErrorCode().hashCode();
        }
        if (getErrorDescription() != null) {
            _hashCode += getErrorDescription().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Event.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://entities.client.refuel.integration.forecourt.mp.techedge.com/", "event"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sequence");
        elemField.setXmlName(new javax.xml.namespace.QName("", "sequence"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eventType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://refuel.integration.forecourt.mp.techedge.com/", "enumEVENTTYPE"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventAmount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "eventAmount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transactionResult");
        elemField.setXmlName(new javax.xml.namespace.QName("", "transactionResult"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://refuel.integration.forecourt.mp.techedge.com/", "enumTRANSACTIONRESULT"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "errorCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "errorDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
