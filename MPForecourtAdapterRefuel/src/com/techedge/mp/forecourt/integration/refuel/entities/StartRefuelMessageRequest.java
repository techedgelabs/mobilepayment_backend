package com.techedge.mp.forecourt.integration.refuel.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "startRefuelMessageRequest", propOrder = {
	"requestID",
	"transactionID"
})
public class StartRefuelMessageRequest {

	
	@XmlElement(required=true)
	protected String requestID;
	@XmlElement(required=true)
	protected String transactionID;
	
	
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	
	public String getTransactionID() {
		return transactionID;
	}
	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	
	
}
