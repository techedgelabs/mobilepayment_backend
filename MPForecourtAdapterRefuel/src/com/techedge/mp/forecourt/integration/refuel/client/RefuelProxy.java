package com.techedge.mp.forecourt.integration.refuel.client;

public class RefuelProxy implements com.techedge.mp.forecourt.integration.refuel.client.Refuel {
    private String                                                     _endpoint = null;
    private com.techedge.mp.forecourt.integration.refuel.client.Refuel refuel    = null;

    public RefuelProxy() {
        _initRefuelProxy();
    }

    public RefuelProxy(String endpoint) {
        _endpoint = endpoint;
        _initRefuelProxy();
    }

    private void _initRefuelProxy() {
        try {
            refuel = (new com.techedge.mp.forecourt.integration.refuel.client.RefuelServiceLocator()).getRefuelPort();
            if (refuel != null) {
                if (_endpoint != null)
                    ((javax.xml.rpc.Stub) refuel)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
                else
                    _endpoint = (String) ((javax.xml.rpc.Stub) refuel)._getProperty("javax.xml.rpc.service.endpoint.address");
            }

        }
        catch (javax.xml.rpc.ServiceException serviceException) {}
    }

    public String getEndpoint() {
        return _endpoint;
    }

    public void setEndpoint(String endpoint) {
        _endpoint = endpoint;
        if (refuel != null)
            ((javax.xml.rpc.Stub) refuel)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

    }

    public com.techedge.mp.forecourt.integration.refuel.client.Refuel getRefuel() {
        if (refuel == null)
            _initRefuelProxy();
        return refuel;
    }

    public com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageResponse startRefuel(
            com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageRequest startRefuelRequest) throws java.rmi.RemoteException {
        if (refuel == null)
            _initRefuelProxy();
        return refuel.startRefuel(startRefuelRequest);
    }

    public com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusResponse getPaymentTransactionStatus(
            com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusRequest paymentTransactionStatusRequest) throws java.rmi.RemoteException {
        if (refuel == null)
            _initRefuelProxy();
        return refuel.getPaymentTransactionStatus(paymentTransactionStatusRequest);
    }

    public com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageResponse endRefuel(
            com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageRequest endRefuelRequest) throws java.rmi.RemoteException {
        if (refuel == null)
            _initRefuelProxy();
        return refuel.endRefuel(endRefuelRequest);
    }

    public com.techedge.mp.forecourt.integration.refuel.client.entities.SendStationListMessageResponse sendStationList(
            com.techedge.mp.forecourt.integration.refuel.client.entities.SendStationListMessageRequest sendStationListMessageRequest) throws java.rmi.RemoteException {
        if (refuel == null)
            _initRefuelProxy();
        return refuel.sendStationList(sendStationListMessageRequest);
    }
}