package com.techedge.mp.forecourt.integration.refuel.entities;


import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paymentTransactionStatusResponse", propOrder = {
	"statusCode",
	"messageCode",
	"paymentTransactionStatus",
	"voucher"
})

public class PaymentTransactionStatusMessageResponse {
	
	@XmlElement(required = true, nillable = true)
	protected ENUM_STATUS_CODE statusCode;
	@XmlElement(required = true, nillable = true)
	protected String messageCode;
	protected PaymentTransactionStatus paymentTransactionStatus;
	@XmlElement(name = "Voucher")
	protected List<Voucher> voucher;
	/**
	 * @return the statusCode
	 */
	public ENUM_STATUS_CODE getStatusCode() {
		return statusCode;
	}
	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(ENUM_STATUS_CODE statusCode) {
		this.statusCode = statusCode;
	}
	/**
	 * @return the messageCode
	 */
	public String getMessageCode() {
		return messageCode;
	}
	/**
	 * @param messageCode the messageCode to set
	 */
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}
	/**
	 * @return the paymentTransactionStatus
	 */
	public PaymentTransactionStatus getPaymentTransactionStatus() {
		return paymentTransactionStatus;
	}
	/**
	 * @param paymentTransactionStatus the paymentTransactionStatus to set
	 */
	public void setPaymentTransactionStatus(
			PaymentTransactionStatus paymentTransactionStatus) {
		this.paymentTransactionStatus = paymentTransactionStatus;
	}
	
	
	

    /**
     * Gets the value of the voucher property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucher property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucher().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Voucher }
     * 
     * 
     */
    public List<Voucher> getVoucher() {
        if (voucher == null) {
            voucher = new ArrayList<Voucher>();
        }
        return this.voucher;
    }

	

}
