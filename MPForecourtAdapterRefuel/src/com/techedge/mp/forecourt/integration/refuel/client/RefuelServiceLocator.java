/**
 * RefuelServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client;

public class RefuelServiceLocator extends org.apache.axis.client.Service implements com.techedge.mp.forecourt.integration.refuel.client.RefuelService {

    /**
     * 
     */
    private static final long serialVersionUID = 2978388935993472387L;

    public RefuelServiceLocator() {
    }


    public RefuelServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public RefuelServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for RefuelPort
    private java.lang.String RefuelPort_address = "http://localhost:8080/MPForecourtAdapterRefuel/Refuel";

    public java.lang.String getRefuelPortAddress() {
        return RefuelPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String RefuelPortWSDDServiceName = "RefuelPort";

    public java.lang.String getRefuelPortWSDDServiceName() {
        return RefuelPortWSDDServiceName;
    }

    public void setRefuelPortWSDDServiceName(java.lang.String name) {
        RefuelPortWSDDServiceName = name;
    }

    public com.techedge.mp.forecourt.integration.refuel.client.Refuel getRefuelPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(RefuelPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getRefuelPort(endpoint);
    }

    public com.techedge.mp.forecourt.integration.refuel.client.Refuel getRefuelPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.techedge.mp.forecourt.integration.refuel.client.RefuelServiceSoapBindingStub _stub = new com.techedge.mp.forecourt.integration.refuel.client.RefuelServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getRefuelPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setRefuelPortEndpointAddress(java.lang.String address) {
        RefuelPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.techedge.mp.forecourt.integration.refuel.client.Refuel.class.isAssignableFrom(serviceEndpointInterface)) {
                com.techedge.mp.forecourt.integration.refuel.client.RefuelServiceSoapBindingStub _stub = new com.techedge.mp.forecourt.integration.refuel.client.RefuelServiceSoapBindingStub(new java.net.URL(RefuelPort_address), this);
                _stub.setPortName(getRefuelPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("RefuelPort".equals(inputPortName)) {
            return getRefuelPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://client.refuel.integration.forecourt.mp.techedge.com/", "RefuelService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://client.refuel.integration.forecourt.mp.techedge.com/", "RefuelPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("RefuelPort".equals(portName)) {
            setRefuelPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
