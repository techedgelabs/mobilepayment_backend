package com.techedge.mp.forecourt.integration.refuel.exception;

public class RefuelException extends Exception {
    

    /**
     * 
     */
    private static final long serialVersionUID = -56140767981321728L;

    public RefuelException(String message, Throwable cause) {
        super(message, cause);
    }

    public RefuelException(String message) {
        this(message, null);
    }
}
