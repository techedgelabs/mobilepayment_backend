package com.techedge.mp.forecourt.integration.refuel.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;

@XmlAccessorType(XmlAccessType.FIELD )
@XmlType(name = "StationDetail", propOrder = {
        "stationId",
        "validityDateDetails",
        "address",
        "city",
        "province",
        "country",
        "latitude",
        "longitude",
        "loyalty",
        "business",
        "voucherPayment",
        "alias",
        "macKey",
        "temporarilyClosed"
})
public class StationDetail {
    @XmlElement(required=true)
    private String stationId;
    
    @XmlElement(required=true)
    private XMLGregorianCalendar validityDateDetails;
    
    @XmlElement(required=true)
    private String address;
    
    @XmlElement(required=true)
    private String  city;
    
    @XmlElement(required=true)
    private String province;
    
    @XmlElement(required=true)
    private String country;
    
    @XmlElement(required=true)
    private String latitude;
    
    @XmlElement(required=true)
    private String longitude;
    
    @XmlElement(required=true)
    private Boolean loyalty;
    
    @XmlElement(required=true)
    private Boolean business;
    
    @XmlElement(required=true)
    private Boolean voucherPayment;
    
    @XmlElement(required=true)
    private String alias;
    
    @XmlElement(required=true)
    private String macKey;
    
    @XmlElement(required=true)
    private Boolean temporarilyClosed;

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public XMLGregorianCalendar getValidityDateDetails() {
        return validityDateDetails;
    }

    public void setValidityDateDetails(XMLGregorianCalendar validityDateDetails) {
        this.validityDateDetails = validityDateDetails;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public Boolean getLoyalty() {
        return loyalty;
    }

    public void setLoyalty(Boolean loyalty) {
        this.loyalty = loyalty;
    }

    public Boolean getBusiness() {
        return business;
    }

    public void setBusiness(Boolean business) {
        this.business = business;
    }

    public Boolean getVoucherPayment() {
        return voucherPayment;
    }

    public void setVoucherPayment(Boolean voucherPayment) {
        this.voucherPayment = voucherPayment;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getMacKey() {
        return macKey;
    }

    public void setMacKey(String macKey) {
        this.macKey = macKey;
    }

    public Boolean getTemporarilyClosed() {
        return temporarilyClosed;
    }

    public void setTemporarilyClosed(Boolean temporarilyClosed) {
        this.temporarilyClosed = temporarilyClosed;
    }
    
    
}
