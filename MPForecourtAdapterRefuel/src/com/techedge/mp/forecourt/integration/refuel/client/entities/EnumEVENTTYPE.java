/**
 * EnumEVENTTYPE.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client.entities;

public class EnumEVENTTYPE implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -3047165989820859761L;
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EnumEVENTTYPE(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _AUT = "AUT";
    public static final java.lang.String _MOV = "MOV";
    public static final java.lang.String _CAN = "CAN";
    public static final EnumEVENTTYPE AUT = new EnumEVENTTYPE(_AUT);
    public static final EnumEVENTTYPE MOV = new EnumEVENTTYPE(_MOV);
    public static final EnumEVENTTYPE CAN = new EnumEVENTTYPE(_CAN);
    public java.lang.String getValue() { return _value_;}
    public static EnumEVENTTYPE fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        EnumEVENTTYPE enumeration = (EnumEVENTTYPE)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static EnumEVENTTYPE fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EnumEVENTTYPE.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://refuel.integration.forecourt.mp.techedge.com/", "enumEVENTTYPE"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
