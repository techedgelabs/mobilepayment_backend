package com.techedge.mp.forecourt.integration.refuel.entities;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(String.class)
public enum ENUM_STATUS_CODE {

	MESSAGE_RECEIVED_200,
	TRANSACTION_NOT_RECOGNIZED_400,
	PARAMETER_NOT_FOUND_400,
	REQUEST_NOT_VALID_400,
	REQUEST_ID_NOT_VALID_400,
	STATIONS_LIST_EMPTY_400
	
}
