package com.techedge.mp.forecourt.integration.refuel.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "startRefuelMessageResponse", propOrder = {
	"statusCode",
	"messageCode"
})

public class StartRefuelMessageResponse {

	
	@XmlElement(required = true, nillable = true)
	protected ENUM_STATUS_CODE statusCode;
	@XmlElement(required = true, nillable = true)
	protected String messageCode;
	
	
	public ENUM_STATUS_CODE getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(ENUM_STATUS_CODE statusCode) {
		this.statusCode = statusCode;
	}
	public String getMessageCode() {
		return messageCode;
	}
	public void setMessageCode(String messageCode) {
		this.messageCode = messageCode;
	}

	
}