/**
 * RefuelDetail.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client.entities;

public class RefuelDetail  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 5245438214724847705L;

    private java.lang.String timestampEndRefuel;

    private double amount;

    private java.lang.String fuelType;

    private java.lang.Double fuelQuantity;

    private java.lang.String productID;

    private java.lang.String productDescription;
    
    private java.lang.Double unitPrice;

    public RefuelDetail() {
    }

    public RefuelDetail(
           java.lang.String timestampEndRefuel,
           double amount,
           java.lang.String fuelType,
           java.lang.Double fuelQuantity,
           java.lang.String productID,
           java.lang.String productDescription,
           java.lang.Double unitPrice) {
           this.timestampEndRefuel = timestampEndRefuel;
           this.amount = amount;
           this.fuelType = fuelType;
           this.fuelQuantity = fuelQuantity;
           this.productID = productID;
           this.productDescription = productDescription;
           this.unitPrice = unitPrice;
    }


    /**
     * Gets the timestampEndRefuel value for this RefuelDetail.
     * 
     * @return timestampEndRefuel
     */
    public java.lang.String getTimestampEndRefuel() {
        return timestampEndRefuel;
    }


    /**
     * Sets the timestampEndRefuel value for this RefuelDetail.
     * 
     * @param timestampEndRefuel
     */
    public void setTimestampEndRefuel(java.lang.String timestampEndRefuel) {
        this.timestampEndRefuel = timestampEndRefuel;
    }


    /**
     * Gets the amount value for this RefuelDetail.
     * 
     * @return amount
     */
    public double getAmount() {
        return amount;
    }


    /**
     * Sets the amount value for this RefuelDetail.
     * 
     * @param amount
     */
    public void setAmount(double amount) {
        this.amount = amount;
    }


    /**
     * Gets the fuelType value for this RefuelDetail.
     * 
     * @return fuelType
     */
    public java.lang.String getFuelType() {
        return fuelType;
    }


    /**
     * Sets the fuelType value for this RefuelDetail.
     * 
     * @param fuelType
     */
    public void setFuelType(java.lang.String fuelType) {
        this.fuelType = fuelType;
    }


    /**
     * Gets the fuelQuantity value for this RefuelDetail.
     * 
     * @return fuelQuantity
     */
    public java.lang.Double getFuelQuantity() {
        return fuelQuantity;
    }


    /**
     * Sets the fuelQuantity value for this RefuelDetail.
     * 
     * @param fuelQuantity
     */
    public void setFuelQuantity(java.lang.Double fuelQuantity) {
        this.fuelQuantity = fuelQuantity;
    }


    /**
     * Gets the productID value for this RefuelDetail.
     * 
     * @return productID
     */
    public java.lang.String getProductID() {
        return productID;
    }


    /**
     * Sets the productID value for this RefuelDetail.
     * 
     * @param productID
     */
    public void setProductID(java.lang.String productID) {
        this.productID = productID;
    }


    /**
     * Gets the productDescription value for this RefuelDetail.
     * 
     * @return productDescription
     */
    public java.lang.String getProductDescription() {
        return productDescription;
    }


    /**
     * Sets the productDescription value for this RefuelDetail.
     * 
     * @param productDescription
     */
    public void setProductDescription(java.lang.String productDescription) {
        this.productDescription = productDescription;
    }
    
    
    /**
    * Gets the unitPrice value for this RefuelDetail.
    * 
    * @return unitPrice
    */
    public java.lang.Double getUnitPrice() {
        return unitPrice;
    }

    
    /**
     * Sets the unitPrice value for this RefuelDetail.
     * 
     * @param unitPrice
     */
    public void setUnitPrice(java.lang.Double unitPrice) {
        this.unitPrice = unitPrice;
    }



    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RefuelDetail)) return false;
        RefuelDetail other = (RefuelDetail) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.timestampEndRefuel==null && other.getTimestampEndRefuel()==null) || 
             (this.timestampEndRefuel!=null &&
              this.timestampEndRefuel.equals(other.getTimestampEndRefuel()))) &&
            this.amount == other.getAmount() &&
            ((this.fuelType==null && other.getFuelType()==null) || 
             (this.fuelType!=null &&
              this.fuelType.equals(other.getFuelType()))) &&
            ((this.fuelQuantity==null && other.getFuelQuantity()==null) || 
             (this.fuelQuantity!=null &&
              this.fuelQuantity.equals(other.getFuelQuantity()))) &&
            ((this.productID==null && other.getProductID()==null) || 
             (this.productID!=null &&
              this.productID.equals(other.getProductID()))) &&
            ((this.productDescription==null && other.getProductDescription()==null) || 
             (this.productDescription!=null &&
              this.productDescription.equals(other.getProductDescription()))) &&
            this.unitPrice == other.getUnitPrice();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getTimestampEndRefuel() != null) {
            _hashCode += getTimestampEndRefuel().hashCode();
        }
        _hashCode += new Double(getAmount()).hashCode();
        if (getFuelType() != null) {
            _hashCode += getFuelType().hashCode();
        }
        if (getFuelQuantity() != null) {
            _hashCode += getFuelQuantity().hashCode();
        }
        if (getProductID() != null) {
            _hashCode += getProductID().hashCode();
        }
        if (getProductDescription() != null) {
            _hashCode += getProductDescription().hashCode();
        }
        if (getUnitPrice() != null) {
            _hashCode += new Double(getUnitPrice()).hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RefuelDetail.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://refuel.integration.forecourt.mp.techedge.com/", "refuelDetail"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timestampEndRefuel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "timestampEndRefuel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("amount");
        elemField.setXmlName(new javax.xml.namespace.QName("", "amount"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fuelType");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fuelType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fuelQuantity");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fuelQuantity"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("productDescription");
        elemField.setXmlName(new javax.xml.namespace.QName("", "productDescription"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("unitPrice");
        elemField.setXmlName(new javax.xml.namespace.QName("", "unitPrice"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
