package com.techedge.mp.forecourt.integration.refuel;

import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.ws.BindingProvider;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.StationServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.EventInfo;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveTransactionEventsData;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.TransactionExistsResponse;
import com.techedge.mp.core.business.interfaces.station.StationUpdateDetail;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.forecourt.integration.refuel.client.RefuelProxy;
import com.techedge.mp.forecourt.integration.refuel.entities.ENUM_EVENT_TYPE;
import com.techedge.mp.forecourt.integration.refuel.entities.ENUM_STATUS_CODE;
import com.techedge.mp.forecourt.integration.refuel.entities.ENUM_TRANSACTION_RESULT;
import com.techedge.mp.forecourt.integration.refuel.entities.EndRefuelMessageRequest;
import com.techedge.mp.forecourt.integration.refuel.entities.EndRefuelMessageResponse;
import com.techedge.mp.forecourt.integration.refuel.entities.Event;
import com.techedge.mp.forecourt.integration.refuel.entities.PaymentTransactionStatus;
import com.techedge.mp.forecourt.integration.refuel.entities.PaymentTransactionStatusMessageRequest;
import com.techedge.mp.forecourt.integration.refuel.entities.PaymentTransactionStatusMessageResponse;
import com.techedge.mp.forecourt.integration.refuel.entities.SendStationListMessageRequest;
import com.techedge.mp.forecourt.integration.refuel.entities.SendStationListMessageResponse;
import com.techedge.mp.forecourt.integration.refuel.entities.StartRefuelMessageRequest;
import com.techedge.mp.forecourt.integration.refuel.entities.StartRefuelMessageResponse;
import com.techedge.mp.forecourt.integration.refuel.entities.StationDetail;
import com.techedge.mp.forecourt.integration.refuel.entities.Voucher;
import com.techedgegroup.mprefuelingprocess.EndRefuelRequest;
import com.techedgegroup.mprefuelingprocess.EndRefuelResponse;
import com.techedgegroup.mprefuelingprocess.MPRefuelingProcess;
import com.techedgegroup.mprefuelingprocess.MPRefuelingProcess_Service;
import com.techedgegroup.mprefuelingprocess.StartRefuelRequest;
import com.techedgegroup.mprefuelingprocess.StartRefuelResponse;

@WebService()
public class Refuel {

    private final static String      PARAM_WSDL_PREFIX                = "WSDL_PREFIX";
    private final static String      PARAM_REFUEL_WSDL_SUFFIX         = "REFUEL_WSDL_SUFFIX";
    private final static String      PARAM_SERVER_NAME                = "SERVER_NAME";
    private final static String      PARAM_REFUEL_ADAPTER_WSDL_SUFFIX = "REFUEL_ADAPTER_WSDL_SUFFIX";
    private final static String      PARAM_BPEL_EVENT_TIMEOUT         = "BPEL_EVENT_TIMEOUT";

    private Properties               prop                             = new Properties();

    private TransactionServiceRemote transactionService               = null;
    private ParametersServiceRemote  parametersService                = null;
    private LoggerServiceRemote      loggerService                    = null;
    private StationServiceRemote     stationService                   = null;
    

    private String                   wsdlStringPrefix                 = "";
    private String                   wsdlStringSuffix                 = "";

    public Refuel() {

    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    private URL getMPReuelingProcessUrl(String serverName) {

        if (this.wsdlStringPrefix == null || this.wsdlStringSuffix == null || this.wsdlStringPrefix.equals("") || this.wsdlStringSuffix.equals("")) {

            try {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
                this.wsdlStringPrefix = parametersService.getParamValue(Refuel.PARAM_WSDL_PREFIX);
                this.wsdlStringSuffix = parametersService.getParamValue(Refuel.PARAM_REFUEL_WSDL_SUFFIX);

                System.out.println("prefix: " + this.wsdlStringPrefix);
                System.out.println("suffix: " + this.wsdlStringSuffix);
            }
            catch (ParameterNotFoundException e) {

                e.printStackTrace();
            }
            catch (InterfaceNotFoundException e) {

                e.printStackTrace();
            }
        }

        String wsdlString = this.wsdlStringPrefix + serverName + this.wsdlStringSuffix;

        try {
            return new URL(wsdlString);
        }
        catch (MalformedURLException e) {

            System.out.println("Malformed URL: " + wsdlString);

            e.printStackTrace();
        }

        return null;
    }

    private URL getRefuelAdpterUrl(String serverName) {

        String wsdlString = null;
        //String wsdlServerName = null;
        String wsdlStringPrefix = null;
        String wsdlStringSuffix = null;

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
            wsdlStringPrefix = parametersService.getParamValue(Refuel.PARAM_WSDL_PREFIX);
            wsdlStringSuffix = parametersService.getParamValue(Refuel.PARAM_REFUEL_ADAPTER_WSDL_SUFFIX);
            //wsdlServerName = parametersService.getParamValue(Refuel.PARAM_SERVER_NAME);

            System.out.println("prefix: " + wsdlStringPrefix);
            System.out.println("suffix: " + wsdlStringSuffix);

            wsdlString = wsdlStringPrefix + serverName + wsdlStringSuffix;

            return new URL(wsdlString);
        }
        catch (ParameterNotFoundException e) {

            e.printStackTrace();
        }
        catch (InterfaceNotFoundException e) {

            e.printStackTrace();
        }
        catch (MalformedURLException e) {

            System.out.println("Malformed URL: " + wsdlString);

            e.printStackTrace();
        }

        return null;
    }

    @WebMethod(operationName = "startRefuel")
    @WebResult(name = "startRefuelResponse")
    public @XmlElement(required = true)
    StartRefuelMessageResponse startRefuel(@XmlElement(required = true) @WebParam(name = "startRefuelRequest") StartRefuelMessageRequest srmRequest) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", srmRequest.getRequestID()));
        inputParameters.add(new Pair<String, String>("transactionID", srmRequest.getTransactionID()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "startRefuel", srmRequest.getRequestID(), "opening", ActivityLog.createLogMessage(inputParameters));

        StartRefuelMessageResponse srmResponse = new StartRefuelMessageResponse();

        Boolean errorFound = false;

        if (srmRequest.getRequestID() == null || srmRequest.getRequestID().trim().isEmpty()) {

            srmResponse.setStatusCode(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400);
            srmResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (srmRequest.getTransactionID() == null || srmRequest.getTransactionID().trim().isEmpty()) {

            srmResponse.setStatusCode(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400);
            srmResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (!errorFound) {

            try {

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                TransactionExistsResponse transactionExistsResponse = transactionService.transactionExists(srmRequest.getTransactionID());

                if (transactionExistsResponse != null) {

                    // Verifica dello stato della transazione

                    // Il comando di start com.techedge.mp.forecourt.integration.refuel.client deve essere inviato solo se non � gi� stato inviato

                    String status = transactionExistsResponse.getStatus();

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "startRefuel", srmRequest.getRequestID(), null, "TransactionID found. Status: " + status);

                    if (status.equals(StatusHelper.STATUS_PUMP_ENABLED)) {

                        String serverName = transactionExistsResponse.getServerName();

                        URL url = this.getMPReuelingProcessUrl(serverName);

                        if (url == null) {

                            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "startRefuel", srmRequest.getRequestID(), null, "Unable to call RefuelImpl service");

                            srmResponse.setStatusCode(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200);
                            srmResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200.name()));
                        }
                        else {

                            srmResponse.setStatusCode(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200);
                            srmResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200.name()));

                            String requestID = srmRequest.getRequestID();
                            String transactionID = srmRequest.getTransactionID();

                            String response = transactionService.persistStartRefuelReceivedStatus(requestID, transactionID, "startRefuel");

                            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "startRefuel", srmRequest.getRequestID(), null, "persistStartRefuelReceivedStatus response:"
                                    + response);

                            /*
                             * Eliminazione gestione EndRefuel con thread esterno
                             * 
                             * Runnable r = new StartRefuelSender(transactionID, url, this.loggerService);
                             * 
                             * new Thread(r).start();
                             */

                            new Proxy().unsetHttp();

                            StartRefuelResponse startRefuelResponse = new StartRefuelResponse();

                            if (!parametersService.getParamValue(Refuel.PARAM_SERVER_NAME).equals(serverName)) {

                                url = getRefuelAdpterUrl(serverName);

                                System.out.println("Chiamata al servizio di Refuel Adapter (startRefuel): " + url.toString());

                                RefuelProxy refuelProxy = new RefuelProxy();
                                refuelProxy.setEndpoint(url.toString());

                                com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageRequest startRefuelMessageRequestClient = new com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageRequest();
                                startRefuelMessageRequestClient.setRequestID(srmRequest.getRequestID());
                                startRefuelMessageRequestClient.setTransactionID(srmRequest.getTransactionID());

                                com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageResponse startRefuelMessageResponseClient = refuelProxy.startRefuel(startRefuelMessageRequestClient);

                                startRefuelResponse.setMessageCode(startRefuelMessageResponseClient.getMessageCode());
                                startRefuelResponse.setStatusCode(startRefuelMessageResponseClient.getStatusCode().toString());
                            }
                            else {

                                System.out.println("Chiamata al servizio di MPRefuelProcess (startRefuel): " + url.toString());

                                StartRefuelRequest startRefuelRequest = new StartRefuelRequest();

                                startRefuelRequest.setTransactionID(transactionID);

                                MPRefuelingProcess_Service service1 = new MPRefuelingProcess_Service(url);

                                MPRefuelingProcess port1 = service1.getMPRefuelingProcessSOAP();

                                String bpelEventTimeoutString = parametersService.getParamValue(Refuel.PARAM_BPEL_EVENT_TIMEOUT);
                                Integer bpelEventTimeout = Integer.valueOf(bpelEventTimeoutString);

                                //System.out.println("########### TEST Connection Timeout startRefuel ##############");
                                
                                System.out.println("timeout: " + bpelEventTimeout.toString());

                                ((BindingProvider) port1).getRequestContext().put("javax.xml.ws.client.connectionTimeout", bpelEventTimeout);
                                ((BindingProvider) port1).getRequestContext().put("javax.xml.ws.client.receiveTimeout",    bpelEventTimeout);

                                try {
                                    startRefuelResponse = port1.startRefuel(startRefuelRequest);
                                }
                                catch (Exception ex) {
                                    System.out.println("##### Errore in startRefuel: " + ex.getMessage() + " ############");
                                }

                                //System.out.println("###################################################");
                            }

                        }
                    }
                    else {

                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "startRefuel", srmRequest.getRequestID(), null,
                                "StartRefuel command processed in previous phase");

                        srmResponse.setStatusCode(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200);
                        srmResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200.name()));
                    }
                }
                else {

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "startRefuel", srmRequest.getRequestID(), null, "TransactionID not found");

                    srmResponse.setStatusCode(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400);
                    srmResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400.name()));

                }
            }
            catch (Exception ex) {

                ex.printStackTrace();

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "startRefuel", srmRequest.getRequestID(), null, "Exception message: " + ex.getMessage());

                srmResponse.setStatusCode(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400);
                srmResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400.name()));

            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", srmResponse.getStatusCode().toString()));
        outputParameters.add(new Pair<String, String>("statusMessage", srmResponse.getMessageCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "startRefuel", srmRequest.getRequestID(), "closing", ActivityLog.createLogMessage(outputParameters));

        return srmResponse;

    }

    @WebMethod(operationName = "endRefuel")
    @WebResult(name = "endRefuelResponse")
    public @XmlElement(required = true)
    EndRefuelMessageResponse endRefuel(@XmlElement(required = true) @WebParam(name = "endRefuelRequest") EndRefuelMessageRequest ermRequest) {
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", ermRequest.getRequestID()));
        inputParameters.add(new Pair<String, String>("transactionID", ermRequest.getTransactionID()));
        inputParameters.add(new Pair<String, String>("amount: ", ermRequest.getRefuelDetail().getAmount().toString()));
        inputParameters.add(new Pair<String, String>("fuel quantity: ", ermRequest.getRefuelDetail().getFuelQuantity().toString()));
        inputParameters.add(new Pair<String, String>("fuel type: ", ermRequest.getRefuelDetail().getFuelType()));
        inputParameters.add(new Pair<String, String>("product description: ", ermRequest.getRefuelDetail().getProductDescription()));
        inputParameters.add(new Pair<String, String>("product id: ", ermRequest.getRefuelDetail().getProductID()));
        if (ermRequest.getRefuelDetail().getUnitPrice() != null) {
            inputParameters.add(new Pair<String, String>("unit price: ", ermRequest.getRefuelDetail().getUnitPrice().toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("unit price: ", "null"));
        }
        inputParameters.add(new Pair<String, String>("timestamp end com.techedge.mp.forecourt.integration.refuel.client: ", ermRequest.getRefuelDetail().getTimestampEndRefuel()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "endRefuel", ermRequest.getRequestID(), "opening", ActivityLog.createLogMessage(inputParameters));

        EndRefuelMessageResponse ermResponse = new EndRefuelMessageResponse();

        Boolean errorFound = false;

        if (ermRequest.getRequestID() == null || ermRequest.getRequestID().trim().isEmpty()) {

            ermResponse.setStatusCode(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400);
            ermResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (ermRequest.getTransactionID() == null || ermRequest.getTransactionID().trim().isEmpty()) {

            ermResponse.setStatusCode(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400);
            ermResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (ermRequest.getRefuelDetail() != null) {

            if (ermRequest.getRefuelDetail().getTimestampEndRefuel() == null) {

                ermResponse.setStatusCode(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400);
                ermResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400.name()));

                errorFound = true;

            }

            if (ermRequest.getRefuelDetail().getAmount() == null) {

                ermResponse.setStatusCode(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400);
                ermResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400.name()));

                errorFound = true;

            }

        }
        else {

            ermResponse.setStatusCode(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400);
            ermResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (!errorFound) {

            try {

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                TransactionExistsResponse transactionExistsResponse = transactionService.transactionExists(ermRequest.getTransactionID());

                if (transactionExistsResponse != null) {

                    // Verifica dello stato della transazione

                    // Il comando di end com.techedge.mp.forecourt.integration.refuel.client deve essere inviato solo se non � gi� stato inviato

                    String status = transactionExistsResponse.getStatus();

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "endRefuel", ermRequest.getRequestID(), null, "TransactionID found. Status: " + status);

                    if (status.equals(StatusHelper.STATUS_PUMP_ENABLED) || status.equals(StatusHelper.STATUS_START_REFUEL_RECEIVED)) {

                        String serverName = transactionExistsResponse.getServerName();

                        URL url = this.getMPReuelingProcessUrl(serverName);

                        if (url == null) {

                            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "endRefuel", ermRequest.getRequestID(), null, "Unable to call RefuelImpl service");

                            ermResponse.setStatusCode(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200);
                            ermResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200.name()));
                        }
                        else {

                            ermResponse.setStatusCode(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200);
                            ermResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200.name()));

                            String requestID = ermRequest.getRequestID();
                            String transactionID = ermRequest.getTransactionID();
                            Double amount = ermRequest.getRefuelDetail().getAmount();
                            Double fuelQuantity = ermRequest.getRefuelDetail().getFuelQuantity();
                            String fuelType = ermRequest.getRefuelDetail().getFuelType();
                            String productDescription = ermRequest.getRefuelDetail().getProductDescription();
                            String productID = ermRequest.getRefuelDetail().getProductID();
                            String timestampEndRefuel = ermRequest.getRefuelDetail().getTimestampEndRefuel();
                            Double unitPrice = ermRequest.getRefuelDetail().getUnitPrice();

                            /* Spostamento codice per essere eseguito solo se la transazione � stata eseguita sulla macchina */
                            /*
                            String response = transactionService.persistEndRefuelReceivedStatus(requestID, transactionID, amount, fuelQuantity, fuelType, productDescription,
                                    productID, timestampEndRefuel, "endRefuel");

                            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "endRefuel", ermRequest.getRequestID(), null, "persistEndRefuelReceivedStatus response:"
                                    + response);
                            */
                            
                            /*
                             * Eliminazione gestione EndRefuel con thread esterno
                             * 
                             * Runnable r = new EndRefuelSender(transactionID, amount, url, loggerService);
                             * 
                             * new Thread(r).start();
                             */
                            try {

                                EndRefuelRequest endRefuelRequest = new EndRefuelRequest();

                                endRefuelRequest.setTransactionID(transactionID);
                                endRefuelRequest.setAmount(amount);

                                EndRefuelResponse endRefuelResponse = new EndRefuelResponse();

                                new Proxy().unsetHttp();

                                if (!parametersService.getParamValue(Refuel.PARAM_SERVER_NAME).equals(serverName)) {

                                    url = getRefuelAdpterUrl(serverName);

                                    System.out.println("Chiamata al servizio di Refuel Adapter (endRefuel): " + url.toString());

                                    RefuelProxy refuelProxy = new RefuelProxy();
                                    refuelProxy.setEndpoint(url.toString());

                                    com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageRequest endRefuelMessageRequestClient = new com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageRequest();
                                    endRefuelMessageRequestClient.setRequestID(ermRequest.getRequestID());
                                    endRefuelMessageRequestClient.setTransactionID(ermRequest.getTransactionID());

                                    com.techedge.mp.forecourt.integration.refuel.client.entities.RefuelDetail refuelDetail = new com.techedge.mp.forecourt.integration.refuel.client.entities.RefuelDetail();
                                    refuelDetail.setAmount(ermRequest.getRefuelDetail().getAmount());
                                    refuelDetail.setFuelQuantity(ermRequest.getRefuelDetail().getFuelQuantity());
                                    refuelDetail.setFuelType(ermRequest.getRefuelDetail().getFuelType());
                                    refuelDetail.setProductDescription(ermRequest.getRefuelDetail().getProductDescription());
                                    refuelDetail.setProductID(ermRequest.getRefuelDetail().getProductID());
                                    refuelDetail.setTimestampEndRefuel(ermRequest.getRefuelDetail().getTimestampEndRefuel());
                                    refuelDetail.setUnitPrice(ermRequest.getRefuelDetail().getUnitPrice());

                                    endRefuelMessageRequestClient.setRefuelDetail(refuelDetail);

                                    com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageResponse endRefuelMessageResponseClient = refuelProxy.endRefuel(endRefuelMessageRequestClient);

                                    endRefuelResponse.setMessageCode(endRefuelMessageResponseClient.getMessageCode());
                                    endRefuelResponse.setStatusCode(endRefuelMessageResponseClient.getStatusCode().toString());
                                }
                                else {

                                    String response = transactionService.persistEndRefuelReceivedStatus(requestID, transactionID, amount, fuelQuantity, fuelType, productDescription,
                                            productID, timestampEndRefuel, unitPrice, "endRefuel");

                                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "endRefuel", ermRequest.getRequestID(), null, "persistEndRefuelReceivedStatus response:"
                                            + response);
                                    
                                    System.out.println("Chiamata al servizio di MPRefuelProcess (endRefuel): " + url.toString());

                                    MPRefuelingProcess_Service service1 = new MPRefuelingProcess_Service(url);

                                    MPRefuelingProcess port1 = service1.getMPRefuelingProcessSOAP();

                                    String bpelEventTimeoutString = parametersService.getParamValue(Refuel.PARAM_BPEL_EVENT_TIMEOUT);
                                    Integer bpelEventTimeout = Integer.valueOf(bpelEventTimeoutString);

                                    //System.out.println("########### TEST Connection Timeout endRefuel ##############");
                                    
                                    System.out.println("timeout: " + bpelEventTimeout.toString());

                                    ((BindingProvider) port1).getRequestContext().put("javax.xml.ws.client.connectionTimeout", bpelEventTimeout);
                                    ((BindingProvider) port1).getRequestContext().put("javax.xml.ws.client.receiveTimeout",    bpelEventTimeout);

                                    try {
                                        endRefuelResponse = port1.endRefuel(endRefuelRequest);
                                    }
                                    catch (Exception ex) {
                                        System.out.println("##### Errore in endRefuel: " + ex.getMessage() + " ############");
                                    }

                                    //System.out.println("###################################################");

                                }

                            }
                            catch (Exception ex) {

                                ex.printStackTrace();

                                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "endRefuel", transactionID, null, "Exception message: " + ex.getMessage());

                            }
                        }

                    }
                    else {

                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "endRefuel", ermRequest.getRequestID(), null, "EndRefuel command processed in previous phase");

                        ermResponse.setStatusCode(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200);
                        ermResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200.name()));
                    }

                }
                else {

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "endRefuel", ermRequest.getRequestID(), null, "transactionID not found");

                    ermResponse.setStatusCode(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400);
                    ermResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400.name()));
                }
            }
            catch (Exception ex) {

                ex.printStackTrace();

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "endRefuel", ermRequest.getRequestID(), null, "Exception message: " + ex.getMessage());

                ermResponse.setStatusCode(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400);
                ermResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400.name()));

            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", ermResponse.getStatusCode().toString()));
        outputParameters.add(new Pair<String, String>("statusMessage", ermResponse.getMessageCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "endRefuel", ermRequest.getRequestID(), "closing", ActivityLog.createLogMessage(outputParameters));

        return ermResponse;

    }

    @WebMethod(operationName = "getPaymentTransactionStatus")
    @WebResult(name = "paymentTransactionStatusResponse")
    public @XmlElement(required = true)
    PaymentTransactionStatusMessageResponse getPaymentTransactionStatus(
            @XmlElement(required = true) @WebParam(name = "paymentTransactionStatusRequest") PaymentTransactionStatusMessageRequest ptsRequest) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", ptsRequest.getRequestID()));
        inputParameters.add(new Pair<String, String>("transactionID", ptsRequest.getTransactionID()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPaymentTransactionStatus", ptsRequest.getRequestID(), "opening",
                ActivityLog.createLogMessage(inputParameters));

        PaymentTransactionStatusMessageResponse ptsResponse = new PaymentTransactionStatusMessageResponse();

        Boolean errorFound = false;

        if (ptsRequest.getRequestID() == null || ptsRequest.getRequestID().trim().isEmpty()) {

            ptsResponse.setStatusCode(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400);
            ptsResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        if (ptsRequest.getTransactionID() == null || ptsRequest.getTransactionID().trim().isEmpty()) {

            ptsResponse.setStatusCode(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400);
            ptsResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.PARAMETER_NOT_FOUND_400.name()));

            errorFound = true;

        }

        //TODO completare la gestione della risposta
        if (!errorFound) {

            try {

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                Transaction transactionDetail = transactionService.getTransactionDetail(ptsRequest.getRequestID(), ptsRequest.getTransactionID());
                RetrieveTransactionEventsData response = transactionService.retrieveEventList(ptsRequest.getRequestID(), ptsRequest.getTransactionID());

                if (response != null && response.getStatusCode().contains("200")) {

                    ptsResponse.setMessageCode(response.getMessageCode());
                    ptsResponse.setStatusCode(ENUM_STATUS_CODE.valueOf(response.getStatusCode()));
                    PaymentTransactionStatus ptsobject = new PaymentTransactionStatus();
                    ptsobject.setAcquirerID(response.getAcquirerID());
                    ptsobject.setAuthorizationCode(response.getPaymentRefuelDetailResponse().getAuthCode());
                    ptsobject.setBankTransactionID(response.getPaymentRefuelDetailResponse().getBankTransactionID());
                    ptsobject.setCurrency(response.getPaymentRefuelDetailResponse().getCurrency());
                    ptsobject.setPaymentMode(response.getPaymentMode());
                    ptsobject.setShopLogin(response.getPaymentRefuelDetailResponse().getShopLogin());
                    ptsobject.setShopTransactionID(ptsRequest.getTransactionID());

                    List<EventInfo> transactionEventList = response.getTransactionEvents();
                    Event event;
                    List<Event> transactionEvents = new ArrayList<Event>(0);
                    Event[] event_array = new Event[0];
                    for (EventInfo eventInfo : transactionEventList) {
                        event = new Event();
                        event.setErrorCode(eventInfo.getErrorCode());
                        event.setErrorDescription(eventInfo.getErrorDescription());
                        event.setEventAmount(eventInfo.getEventAmount());
                        
                        if ( transactionDetail.getNewPaymentFlow() ) {
                            if ( eventInfo.getEventType().equals("MOV") ) {
                                event.setEventType(ENUM_EVENT_TYPE.CAN);
                            }
                            else {
                                event.setEventType(ENUM_EVENT_TYPE.valueOf(eventInfo.getEventType()));
                            }
                        }
                        else {
                            event.setEventType(ENUM_EVENT_TYPE.valueOf(eventInfo.getEventType()));
                        }
                        
                        event.setSequence(eventInfo.getSequence());
                        event.setTransactionResult(ENUM_TRANSACTION_RESULT.valueOf(eventInfo.getTransactionResult()));
                        transactionEvents.add(event);

                    }

                    ptsobject.setEventList(transactionEvents.toArray(event_array));
                    ptsResponse.setPaymentTransactionStatus(ptsobject);

                    new Proxy().unsetHttp();

                    // Estrazione informazioni consumo voucher
                    Transaction transactionResponse = transactionService.getTransactionDetail(ptsRequest.getRequestID(), ptsRequest.getTransactionID());

                    if (transactionResponse != null) {

                        if (transactionResponse.getPrePaidConsumeVoucherList().size() > 0) {

                            this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getPaymentTransactionStatus", ptsRequest.getRequestID(), null,
                                    "Consume voucher header found");

                            for (PrePaidConsumeVoucher prePaidVoucherConsume : transactionResponse.getPrePaidConsumeVoucherList()) {

                                if (prePaidVoucherConsume.getOperationType().equals("CONSUME") && prePaidVoucherConsume.getPrePaidConsumeVoucherDetail().size() > 0) {

                                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getPaymentTransactionStatus", ptsRequest.getRequestID(), null,
                                            "Consume voucher found");

                                    for (PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetail : prePaidVoucherConsume.getPrePaidConsumeVoucherDetail()) {

                                        Voucher voucher = new Voucher();
                                        voucher.setPromoCode(prePaidConsumeVoucherDetail.getPromoCode());
                                        voucher.setPromoDescription(prePaidConsumeVoucherDetail.getPromoDescription());
                                        voucher.setVoucherAmount(prePaidConsumeVoucherDetail.getConsumedValue());
                                        voucher.setVoucherCode(prePaidConsumeVoucherDetail.getVoucherCode());

                                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getPaymentTransactionStatus", ptsRequest.getRequestID(), null,
                                                "Voucher: code: " + voucher.getVoucherCode() + ", amount: " + voucher.getVoucherAmount());

                                        ptsResponse.getVoucher().add(voucher);
                                    }
                                }
                            }
                        }
                    }
                }

                else {
                    ptsResponse.setStatusCode(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400);
                    ptsResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400.name()));

                    return ptsResponse;
                }
            }
            catch (Exception ex) {

                ex.printStackTrace();

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getPaymentTransactionStatus", ptsRequest.getRequestID(), null, "Exception message: " + ex.getMessage());

                ptsResponse.setStatusCode(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400);
                ptsResponse.setMessageCode(prop.getProperty(ENUM_STATUS_CODE.TRANSACTION_NOT_RECOGNIZED_400.name()));

            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", ptsResponse.getStatusCode().toString()));
        outputParameters.add(new Pair<String, String>("statusMessage", ptsResponse.getMessageCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPaymentTransactionStatus", ptsRequest.getRequestID(), "closing",
                ActivityLog.createLogMessage(outputParameters));

        return ptsResponse;

    }
    
    @WebMethod(operationName = "sendStationList")
    @WebResult(name = "sendStationListResponse")
    public @XmlElement(required = true)
    SendStationListMessageResponse sendStationList(
            @XmlElement(required = true) @WebParam(name = "sendStationListRequest") SendStationListMessageRequest stationListRequest) {
        
        SendStationListMessageResponse stationListResponse = new SendStationListMessageResponse();

        if (stationListRequest.getRequestID() == null || stationListRequest.getRequestID().isEmpty()) {
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendStationList", null, null, "RequestID null or empty");
            stationListResponse.setStatusCode(ENUM_STATUS_CODE.REQUEST_NOT_VALID_400);
            return stationListResponse;
        }
        
        for (StationDetail stationDetail : stationListRequest.getStationList()) {
            Field[] fields = stationDetail.getClass().getDeclaredFields();
            for (Field field : fields) {
                if (!field.getName().equals("alias") || !field.getName().equals("macKey")) {
                    try {
                        boolean accessible = field.isAccessible();
                        field.setAccessible(true);


                        if (field.get(stationDetail) == null) {
                            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendStationList", null, null, "The parameter " + field.getName() + " is null");
                            stationListResponse.setStatusCode(ENUM_STATUS_CODE.REQUEST_NOT_VALID_400);
                            return stationListResponse;
                        }
                        
                        field.setAccessible(accessible);

                    } catch (IllegalAccessException ex) {
                        this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendStationList", null, null, "Request exception: " + ex.getMessage());
                    }
                }
            }
        }
        
        String stationListString = "";
        
        if (stationListRequest.getStationList() != null) {
            int index = 1;
            for (StationDetail stationDetail : stationListRequest.getStationList()) {
                stationListString += "{ ";
                stationListString += "stationID: " + stationDetail.getStationId();
                stationListString += " }";
                index++;
                
                if (index < stationListRequest.getStationList().length) {
                    stationListString += ",";
                }
            }
        }
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", stationListRequest.getRequestID()));
        inputParameters.add(new Pair<String, String>("stationList", stationListString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendStationList", stationListRequest.getRequestID(), "opening",
                ActivityLog.createLogMessage(inputParameters));

        
        try {
            ArrayList<StationUpdateDetail> stationList = new ArrayList<>();
            
            if (stationListRequest.getStationList() != null) {
                for (StationDetail stationDetail : stationListRequest.getStationList()) {
                    StationUpdateDetail stationUpdateDetail = new StationUpdateDetail();
                    stationUpdateDetail.setAddress(stationDetail.getAddress());
                    stationUpdateDetail.setAlias(stationDetail.getAlias());
                    stationUpdateDetail.setBusiness(stationDetail.getBusiness());
                    stationUpdateDetail.setCity(stationDetail.getCity());
                    stationUpdateDetail.setCountry(stationDetail.getCountry());
                    stationUpdateDetail.setLatitude(new Double(stationDetail.getLatitude()));
                    stationUpdateDetail.setLongitude(new Double(stationDetail.getLongitude()));
                    stationUpdateDetail.setLoyalty(stationDetail.getLoyalty());
                    stationUpdateDetail.setMacKey(stationDetail.getMacKey());
                    stationUpdateDetail.setProvince(stationDetail.getProvince());
                    stationUpdateDetail.setStationID(stationDetail.getStationId());
                    stationUpdateDetail.setTemporarilyClosed(stationDetail.getTemporarilyClosed());
                    stationUpdateDetail.setVoucherPayment(stationDetail.getVoucherPayment());
                    
                    if (stationDetail.getValidityDateDetails() != null) {
                        stationUpdateDetail.setValidityDateDetails(stationDetail.getValidityDateDetails().toGregorianCalendar().getTime());    
                    }
                    
                    stationList.add(stationUpdateDetail);
                }
            }            
            
            stationService = EJBHomeCache.getInstance().getStationService();
            String response = stationService.updateStaging(stationListRequest.getRequestID(), stationList);
            
            if (Objects.equals(response, ResponseHelper.STATION_UPDATE_STAGING_SUCCESS)) {
                stationListResponse.setStatusCode(ENUM_STATUS_CODE.MESSAGE_RECEIVED_200);
            }
            else if (Objects.equals(response, ResponseHelper.STATION_UPDATE_STAGING_REQUEST_ID_ALREADY_EXISTS)) {
                stationListResponse.setStatusCode(ENUM_STATUS_CODE.REQUEST_ID_NOT_VALID_400);
            }
            else if (Objects.equals(response, ResponseHelper.STATION_UPDATE_STAGING_STATIONS_LIST_EMPTY)) {
                stationListResponse.setStatusCode(ENUM_STATUS_CODE.STATIONS_LIST_EMPTY_400);
            }
            else {
                stationListResponse.setStatusCode(ENUM_STATUS_CODE.REQUEST_NOT_VALID_400);
            }
        }
        catch (InterfaceNotFoundException ex) {
            
        }
        
        
        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", stationListResponse.getStatusCode().toString()));
        outputParameters.add(new Pair<String, String>("statusMessage", stationListResponse.getMessageCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendStationList", stationListRequest.getRequestID(), "closing",
                ActivityLog.createLogMessage(outputParameters));

        return stationListResponse;
        
    }

}
