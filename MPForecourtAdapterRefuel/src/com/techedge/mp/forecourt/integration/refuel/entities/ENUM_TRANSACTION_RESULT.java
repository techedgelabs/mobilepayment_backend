package com.techedge.mp.forecourt.integration.refuel.entities;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;

@XmlType
@XmlEnum(String.class)
public enum ENUM_TRANSACTION_RESULT {
	OK,
	KO

}
