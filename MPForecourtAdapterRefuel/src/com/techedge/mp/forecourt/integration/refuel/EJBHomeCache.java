package com.techedge.mp.forecourt.integration.refuel;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.StationServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;

public class EJBHomeCache {

    final String                       transactionServiceRemoteJndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/TransactionService!com.techedge.mp.core.business.TransactionServiceRemote";
    final String                       parametersServiceRemoteJndi  = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
    final String                       loggerServiceRemoteJndi      = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
    final String                       stationServiceRemoteJndi     = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/StationService!com.techedge.mp.core.business.StationServiceRemote";

    private static EJBHomeCache        instance;

    protected Context                  context                      = null;

    protected TransactionServiceRemote transactionService           = null;
    protected ParametersServiceRemote  parametersService            = null;
    protected LoggerServiceRemote      loggerService                = null;
    protected StationServiceRemote     stationService               = null;

    private EJBHomeCache() throws InterfaceNotFoundException {

        final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();

        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

        try {
            context = new InitialContext(jndiProperties);
        }
        catch (NamingException e) {

            throw new InterfaceNotFoundException("Naming exception: " + e.getMessage());
        }

        try {
            transactionService = (TransactionServiceRemote) context.lookup(transactionServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(transactionServiceRemoteJndi);
        }

        try {
            parametersService = (ParametersServiceRemote) context.lookup(parametersServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(parametersServiceRemoteJndi);
        }

        try {
            loggerService = (LoggerServiceRemote) context.lookup(loggerServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(loggerServiceRemoteJndi);
        }

        try {
            stationService = (StationServiceRemote) context.lookup(stationServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(stationServiceRemoteJndi);
        }
    }

    public static synchronized EJBHomeCache getInstance() throws InterfaceNotFoundException {
        if (instance == null)
            instance = new EJBHomeCache();

        return instance;
    }

    public TransactionServiceRemote getTransactionService() {
        return transactionService;
    }

    public ParametersServiceRemote getParametersService() {
        return parametersService;
    }

    public LoggerServiceRemote getLoggerService() {
        return loggerService;
    }
    
    public StationServiceRemote getStationService() {
        return stationService;
    }
    
}
