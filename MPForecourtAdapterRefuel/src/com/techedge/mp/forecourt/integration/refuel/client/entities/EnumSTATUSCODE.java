/**
 * EnumSTATUSCODE.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client.entities;

public class EnumSTATUSCODE implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 2874177922863643213L;
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected EnumSTATUSCODE(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _MESSAGE_RECEIVED_200 = "MESSAGE_RECEIVED_200";
    public static final java.lang.String _TRANSACTION_NOT_RECOGNIZED_400 = "TRANSACTION_NOT_RECOGNIZED_400";
    public static final java.lang.String _PARAMETER_NOT_FOUND_400 = "PARAMETER_NOT_FOUND_400";
    public static final EnumSTATUSCODE MESSAGE_RECEIVED_200 = new EnumSTATUSCODE(_MESSAGE_RECEIVED_200);
    public static final EnumSTATUSCODE TRANSACTION_NOT_RECOGNIZED_400 = new EnumSTATUSCODE(_TRANSACTION_NOT_RECOGNIZED_400);
    public static final EnumSTATUSCODE PARAMETER_NOT_FOUND_400 = new EnumSTATUSCODE(_PARAMETER_NOT_FOUND_400);
    public java.lang.String getValue() { return _value_;}
    public static EnumSTATUSCODE fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        EnumSTATUSCODE enumeration = (EnumSTATUSCODE)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static EnumSTATUSCODE fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(EnumSTATUSCODE.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://entities.client.refuel.integration.forecourt.mp.techedge.com/", "enumSTATUSCODE"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
