/**
 * Refuel.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client;

public interface Refuel extends java.rmi.Remote {
    public com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageResponse startRefuel(com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageRequest startRefuelRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusResponse getPaymentTransactionStatus(com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusRequest paymentTransactionStatusRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageResponse endRefuel(com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageRequest endRefuelRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.forecourt.integration.refuel.client.entities.SendStationListMessageResponse sendStationList(com.techedge.mp.forecourt.integration.refuel.client.entities.SendStationListMessageRequest sendStationListMessageRequest) throws java.rmi.RemoteException;
}
