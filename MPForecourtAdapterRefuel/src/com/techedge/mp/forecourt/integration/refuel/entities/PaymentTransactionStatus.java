package com.techedge.mp.forecourt.integration.refuel.entities;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paymentTransactionStatus", propOrder = {
    "shopLogin",
    "acquirerID",
    "paymentMode",
    "shopTransactionID",
    "bankTransactionID",
    "authorizationCode",
    "currency",
    "eventList"
})






public class PaymentTransactionStatus {
	@XmlElement(required = true, nillable = true)
    protected String shopLogin;
    @XmlElement(required = true, nillable = true)
    protected String acquirerID;
    @XmlElement(required = true, nillable = true)
    protected String paymentMode;
    @XmlElement(required = true, nillable = true)
    protected String shopTransactionID;
    @XmlElement(required = true, nillable = true)
    protected String bankTransactionID;
    @XmlElement(required = true, nillable = true)
    protected String authorizationCode;
    protected String currency;
    @XmlElement(namespace = "http://entities.refuel.integration.forecourt.mp.techedge.com/", required = true)
    protected Event [] eventList;
	/**
	 * @return the shopLogin
	 */
	public String getShopLogin() {
		return shopLogin;
	}
	/**
	 * @param shopLogin the shopLogin to set
	 */
	public void setShopLogin(String shopLogin) {
		this.shopLogin = shopLogin;
	}
	/**
	 * @return the acquirerID
	 */
	public String getAcquirerID() {
		return acquirerID;
	}
	/**
	 * @param acquirerID the acquirerID to set
	 */
	public void setAcquirerID(String acquirerID) {
		this.acquirerID = acquirerID;
	}
	/**
	 * @return the paymentMode
	 */
	public String getPaymentMode() {
		return paymentMode;
	}
	/**
	 * @param paymentMode the paymentMode to set
	 */
	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}
	/**
	 * @return the shopTransactionID
	 */
	public String getShopTransactionID() {
		return shopTransactionID;
	}
	/**
	 * @param shopTransactionID the shopTransactionID to set
	 */
	public void setShopTransactionID(String shopTransactionID) {
		this.shopTransactionID = shopTransactionID;
	}
	/**
	 * @return the bankTransactionID
	 */
	public String getBankTransactionID() {
		return bankTransactionID;
	}
	/**
	 * @param bankTransactionID the bankTransactionID to set
	 */
	public void setBankTransactionID(String bankTransactionID) {
		this.bankTransactionID = bankTransactionID;
	}
	/**
	 * @return the authorizationCode
	 */
	public String getAuthorizationCode() {
		return authorizationCode;
	}
	/**
	 * @param authorizationCode the authorizationCode to set
	 */
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}
	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}
	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	/**
	 * @return the eventList
	 */
	public Event[] getEventList() {
		return eventList;
	}
	/**
	 * @param eventList the eventList to set
	 */
	public void setEventList(Event[] eventList) {
		this.eventList = eventList;
	}
	
	
    
    

}
