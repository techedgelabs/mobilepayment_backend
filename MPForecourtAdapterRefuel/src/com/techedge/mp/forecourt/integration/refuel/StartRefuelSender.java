package com.techedge.mp.forecourt.integration.refuel;

import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedgegroup.mprefuelingprocess.MPRefuelingProcess;
import com.techedgegroup.mprefuelingprocess.MPRefuelingProcess_Service;
import com.techedgegroup.mprefuelingprocess.StartRefuelRequest;
import com.techedgegroup.mprefuelingprocess.StartRefuelResponse;

public class StartRefuelSender implements Runnable {

	private String transactionID;
	private URL url;
	private LoggerServiceRemote loggerService;
	
	public StartRefuelSender(String transactionID, URL url, LoggerServiceRemote loggerService) {
		
		this.transactionID = transactionID;
		this.url           = url;
		this.loggerService = loggerService;
	}
	
	
	private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {
		
		try {
			this.loggerService = EJBHomeCache.getInstance().getLoggerService();
			this.loggerService.log(level, className, methodName, groupId, phaseId, message);
		}
		catch (Exception ex) {
			
			System.out.println("LoggerService is not available: " + ex.getMessage());
		}
	}
	
	
	@Override
	public void run() {
		
		Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
		inputParameters.add(new Pair<String,String>("transactionID",  this.transactionID));
		
		this.log( ErrorLevel.DEBUG, this.getClass().getSimpleName(), "run", this.transactionID, "opening", ActivityLog.createLogMessage(inputParameters));
		
		
		try {
			
			StartRefuelRequest startRefuelRequest = new StartRefuelRequest();
			
			startRefuelRequest.setTransactionID(this.transactionID);
			
			MPRefuelingProcess_Service service1 = new MPRefuelingProcess_Service(this.url);

			MPRefuelingProcess port1 = service1.getMPRefuelingProcessSOAP();
	        		        
	        StartRefuelResponse startRefuelResponse = new StartRefuelResponse();
		
			startRefuelResponse = port1.startRefuel(startRefuelRequest);
			
			Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
			outputParameters.add(new Pair<String,String>("statusCode", startRefuelResponse.getStatusCode()));
			outputParameters.add(new Pair<String,String>("messageCode", startRefuelResponse.getMessageCode()));
			
			this.log( ErrorLevel.DEBUG, this.getClass().getSimpleName(), "run", this.transactionID, "closing", ActivityLog.createLogMessage(outputParameters));
			
		} catch (Exception ex) {
			
			ex.printStackTrace();
			
			this.log( ErrorLevel.ERROR, this.getClass().getSimpleName(), "run", this.transactionID, null, "Exception message: " + ex.getMessage());
        	
		}
	}
}
