
package com.techedge.mp.forecourt.integration.refuel.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for voucher complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="voucher">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VoucherAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="VoucherCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PromoCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PromoDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "voucher", propOrder = {
    "voucherAmount",
    "voucherCode",
    "promoCode",
    "promoDescription"
})
public class Voucher {

    @XmlElement(name = "VoucherAmount")
    protected double voucherAmount;
    @XmlElement(name = "VoucherCode", required = true, nillable = true)
    protected String voucherCode;
    @XmlElement(name = "PromoCode", required = true, nillable = true)
    protected String promoCode;
    @XmlElement(name = "PromoDescription", required = true, nillable = true)
    protected String promoDescription;

    /**
     * Gets the value of the voucherAmount property.
     * 
     */
    public double getVoucherAmount() {
        return voucherAmount;
    }

    /**
     * Sets the value of the voucherAmount property.
     * 
     */
    public void setVoucherAmount(double value) {
        this.voucherAmount = value;
    }

    /**
     * Gets the value of the voucherCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     * Sets the value of the voucherCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherCode(String value) {
        this.voucherCode = value;
    }

    /**
     * Gets the value of the promoCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoCode() {
        return promoCode;
    }

    /**
     * Sets the value of the promoCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoCode(String value) {
        this.promoCode = value;
    }

    /**
     * Gets the value of the promoDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoDescription() {
        return promoDescription;
    }

    /**
     * Sets the value of the promoDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoDescription(String value) {
        this.promoDescription = value;
    }

}
