package com.techedge.mp.forecourt.integration.refuel.entities;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;



@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "event", propOrder = {
    "sequence",
    "eventType",
    "eventAmount",
    "transactionResult",
    "errorCode",
    "errorDescription"
})


public class Event {
	
	@XmlElement(required = true, nillable = true)
    protected String sequence;
	@XmlElement(required = true, nillable = true)
    protected ENUM_EVENT_TYPE eventType;
  	@XmlElement(required = true, nillable = true)
    protected Double eventAmount;
    @XmlElement(required = true, nillable = true)
    protected ENUM_TRANSACTION_RESULT transactionResult;
    protected String errorCode;
    protected String errorDescription;
    /**
	 * @return the sequence
	 */
	public String getSequence() {
		return sequence;
	}
	/**
	 * @param sequence the sequence to set
	 */
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	
	/**
	 * @return the eventType
	 */
	public ENUM_EVENT_TYPE getEventType() {
		return eventType;
	}
	/**
	 * @param eventType the eventType to set
	 */
	public void setEventType(ENUM_EVENT_TYPE eventType) {
		this.eventType = eventType;
	}
	/**
	 * @return the eventAmount
	 */
	public Double getEventAmount() {
		return eventAmount;
	}
	/**
	 * @param eventAmount the eventAmount to set
	 */
	public void setEventAmount(Double eventAmount) {
		this.eventAmount = eventAmount;
	}
	/**
	 * @return the transactionResult
	 */
	public ENUM_TRANSACTION_RESULT getTransactionResult() {
		return transactionResult;
	}
	/**
	 * @param transactionResult the transactionResult to set
	 */
	public void setTransactionResult(ENUM_TRANSACTION_RESULT transactionResult) {
		this.transactionResult = transactionResult;
	}
	/**
	 * @return the errorCode
	 */
	public String getErrorCode() {
		return errorCode;
	}
	/**
	 * @param errorCode the errorCode to set
	 */
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	/**
	 * @return the errorDescription
	 */
	public String getErrorDescription() {
		return errorDescription;
	}
	/**
	 * @param errorDescription the errorDescription to set
	 */
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	
    

}
