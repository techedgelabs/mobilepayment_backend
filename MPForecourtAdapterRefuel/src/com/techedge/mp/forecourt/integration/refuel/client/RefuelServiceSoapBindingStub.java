/**
 * RefuelServiceSoapBindingStub.java
 * 
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client;

public class RefuelServiceSoapBindingStub extends org.apache.axis.client.Stub implements com.techedge.mp.forecourt.integration.refuel.client.Refuel {
    private java.util.Vector                           cachedSerClasses     = new java.util.Vector();
    private java.util.Vector                           cachedSerQNames      = new java.util.Vector();
    private java.util.Vector                           cachedSerFactories   = new java.util.Vector();
    private java.util.Vector                           cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc[] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[3];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1() {
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("startRefuel");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "startRefuelRequest"), org.apache.axis.description.ParameterDesc.IN,
                new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "startRefuelMessageRequest"),
                com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "startRefuelMessageResponse"));
        oper.setReturnClass(com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "startRefuelResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("endRefuel");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "endRefuelRequest"), org.apache.axis.description.ParameterDesc.IN,
                new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "endRefuelMessageRequest"),
                com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "endRefuelMessageResponse"));
        oper.setReturnClass(com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "endRefuelResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPaymentTransactionStatus");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("", "paymentTransactionStatusRequest"), org.apache.axis.description.ParameterDesc.IN,
                new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "paymentTransactionStatusRequest"),
                com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusRequest.class, false, false);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "paymentTransactionStatusResponse"));
        oper.setReturnClass(com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusResponse.class);
        oper.setReturnQName(new javax.xml.namespace.QName("", "paymentTransactionStatusResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        _operations[2] = oper;

    }

    public RefuelServiceSoapBindingStub() throws org.apache.axis.AxisFault {
        this(null);
    }

    public RefuelServiceSoapBindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        this(service);
        super.cachedEndpoint = endpointURL;
    }

    public RefuelServiceSoapBindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        }
        else {
            super.service = service;
        }
        ((org.apache.axis.client.Service) super.service).setTypeMappingVersion("1.2");
        java.lang.Class cls;
        javax.xml.namespace.QName qName;
        javax.xml.namespace.QName qName2;
        java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
        java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
        java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
        java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
        java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
        java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
        java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
        java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
        java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
        java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "endRefuelMessageRequest");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageRequest.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(beansf);
        cachedDeserFactories.add(beandf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "endRefuelMessageResponse");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageResponse.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(beansf);
        cachedDeserFactories.add(beandf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "EnumEVENTTYPE");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.EnumEVENTTYPE.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(enumsf);
        cachedDeserFactories.add(enumdf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "EnumSTATUSCODE");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.EnumSTATUSCODE.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(enumsf);
        cachedDeserFactories.add(enumdf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "EnumTRANSACTIONRESULT");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.EnumTRANSACTIONRESULT.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(enumsf);
        cachedDeserFactories.add(enumdf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "event");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.Event.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(beansf);
        cachedDeserFactories.add(beandf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "paymentTransactionStatus");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatus.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(beansf);
        cachedDeserFactories.add(beandf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "paymentTransactionStatusRequest");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusRequest.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(beansf);
        cachedDeserFactories.add(beandf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "paymentTransactionStatusResponse");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusResponse.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(beansf);
        cachedDeserFactories.add(beandf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "refuelDetail");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.RefuelDetail.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(beansf);
        cachedDeserFactories.add(beandf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "startRefuelMessageRequest");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageRequest.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(beansf);
        cachedDeserFactories.add(beandf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "startRefuelMessageResponse");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageResponse.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(beansf);
        cachedDeserFactories.add(beandf);

        qName = new javax.xml.namespace.QName("http://entities.refuel.integration.forecourt.mp.techedge.com/", "voucher");
        cachedSerQNames.add(qName);
        cls = com.techedge.mp.forecourt.integration.refuel.client.entities.Voucher.class;
        cachedSerClasses.add(cls);
        cachedSerFactories.add(beansf);
        cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName = (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class) cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class) cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory) cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory) cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageResponse startRefuel(
            com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageRequest startRefuelRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://refuel.integration.forecourt.mp.techedge.com/", "startRefuel"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {
            java.lang.Object _resp = _call.invoke(new java.lang.Object[] { startRefuelRequest });

            if (_resp instanceof java.rmi.RemoteException) {
                throw (java.rmi.RemoteException) _resp;
            }
            else {
                extractAttachments(_call);
                try {
                    return (com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageResponse) _resp;
                }
                catch (java.lang.Exception _exception) {
                    return (com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageResponse) org.apache.axis.utils.JavaUtils.convert(_resp,
                            com.techedge.mp.forecourt.integration.refuel.client.entities.StartRefuelMessageResponse.class);
                }
            }
        }
        catch (org.apache.axis.AxisFault axisFaultException) {
            throw axisFaultException;
        }
    }

    public com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageResponse endRefuel(
            com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageRequest endRefuelRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://refuel.integration.forecourt.mp.techedge.com/", "endRefuel"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {
            java.lang.Object _resp = _call.invoke(new java.lang.Object[] { endRefuelRequest });

            if (_resp instanceof java.rmi.RemoteException) {
                throw (java.rmi.RemoteException) _resp;
            }
            else {
                extractAttachments(_call);
                try {
                    return (com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageResponse) _resp;
                }
                catch (java.lang.Exception _exception) {
                    return (com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageResponse) org.apache.axis.utils.JavaUtils.convert(_resp,
                            com.techedge.mp.forecourt.integration.refuel.client.entities.EndRefuelMessageResponse.class);
                }
            }
        }
        catch (org.apache.axis.AxisFault axisFaultException) {
            throw axisFaultException;
        }
    }

    public com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusResponse getPaymentTransactionStatus(
            com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusRequest paymentTransactionStatusRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://refuel.integration.forecourt.mp.techedge.com/", "getPaymentTransactionStatus"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {
            java.lang.Object _resp = _call.invoke(new java.lang.Object[] { paymentTransactionStatusRequest });

            if (_resp instanceof java.rmi.RemoteException) {
                throw (java.rmi.RemoteException) _resp;
            }
            else {
                extractAttachments(_call);
                try {
                    return (com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusResponse) _resp;
                }
                catch (java.lang.Exception _exception) {
                    return (com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusResponse) org.apache.axis.utils.JavaUtils.convert(_resp,
                            com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatusResponse.class);
                }
            }
        }
        catch (org.apache.axis.AxisFault axisFaultException) {
            throw axisFaultException;
        }
    }


    public com.techedge.mp.forecourt.integration.refuel.client.entities.SendStationListMessageResponse sendStationList(
            com.techedge.mp.forecourt.integration.refuel.client.entities.SendStationListMessageRequest sendStationListMessageRequest) throws java.rmi.RemoteException {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://refuel.integration.forecourt.mp.techedge.com/", "sendStationList"));

        setRequestHeaders(_call);
        setAttachments(_call);
        try {
            java.lang.Object _resp = _call.invoke(new java.lang.Object[] { sendStationListMessageRequest });

            if (_resp instanceof java.rmi.RemoteException) {
                throw (java.rmi.RemoteException) _resp;
            }
            else {
                extractAttachments(_call);
                try {
                    return (com.techedge.mp.forecourt.integration.refuel.client.entities.SendStationListMessageResponse) _resp;
                }
                catch (java.lang.Exception _exception) {
                    return (com.techedge.mp.forecourt.integration.refuel.client.entities.SendStationListMessageResponse) org.apache.axis.utils.JavaUtils.convert(_resp,
                            com.techedge.mp.forecourt.integration.refuel.client.entities.SendStationListMessageResponse.class);
                }
            }
        }
        catch (org.apache.axis.AxisFault axisFaultException) {
            throw axisFaultException;
        }
    }    
}
