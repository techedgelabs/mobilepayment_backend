package com.techedge.mp.forecourt.integration.refuel.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendStationListMessageRequest", propOrder = {
	"requestID",
	"stationList"
})
public class SendStationListMessageRequest {
	
	@XmlElement(required=true)
	private String requestID;
	
	@XmlElementWrapper(required=true)
	@XmlElements(@XmlElement(name="stationDetail"))
	private StationDetail[] stationList;

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public StationDetail[] getStationList() {
        return stationList;
    }
	
	public void setStationList(StationDetail[] stationList) {
        this.stationList = stationList;
    }
	
	
	
	
	

}
