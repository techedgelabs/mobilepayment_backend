/**
 * PaymentTransactionStatus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client.entities;

public class PaymentTransactionStatus  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 3349982843306804585L;

    private java.lang.String shopLogin;

    private java.lang.String acquirerID;

    private java.lang.String paymentMode;

    private java.lang.String shopTransactionID;

    private java.lang.String bankTransactionID;

    private java.lang.String authorizationCode;

    private java.lang.String currency;

    private com.techedge.mp.forecourt.integration.refuel.client.entities.Event[] eventList;

    public PaymentTransactionStatus() {
    }

    public PaymentTransactionStatus(
           java.lang.String shopLogin,
           java.lang.String acquirerID,
           java.lang.String paymentMode,
           java.lang.String shopTransactionID,
           java.lang.String bankTransactionID,
           java.lang.String authorizationCode,
           java.lang.String currency,
           com.techedge.mp.forecourt.integration.refuel.client.entities.Event[] eventList) {
           this.shopLogin = shopLogin;
           this.acquirerID = acquirerID;
           this.paymentMode = paymentMode;
           this.shopTransactionID = shopTransactionID;
           this.bankTransactionID = bankTransactionID;
           this.authorizationCode = authorizationCode;
           this.currency = currency;
           this.eventList = eventList;
    }


    /**
     * Gets the shopLogin value for this PaymentTransactionStatus.
     * 
     * @return shopLogin
     */
    public java.lang.String getShopLogin() {
        return shopLogin;
    }


    /**
     * Sets the shopLogin value for this PaymentTransactionStatus.
     * 
     * @param shopLogin
     */
    public void setShopLogin(java.lang.String shopLogin) {
        this.shopLogin = shopLogin;
    }


    /**
     * Gets the acquirerID value for this PaymentTransactionStatus.
     * 
     * @return acquirerID
     */
    public java.lang.String getAcquirerID() {
        return acquirerID;
    }


    /**
     * Sets the acquirerID value for this PaymentTransactionStatus.
     * 
     * @param acquirerID
     */
    public void setAcquirerID(java.lang.String acquirerID) {
        this.acquirerID = acquirerID;
    }


    /**
     * Gets the paymentMode value for this PaymentTransactionStatus.
     * 
     * @return paymentMode
     */
    public java.lang.String getPaymentMode() {
        return paymentMode;
    }


    /**
     * Sets the paymentMode value for this PaymentTransactionStatus.
     * 
     * @param paymentMode
     */
    public void setPaymentMode(java.lang.String paymentMode) {
        this.paymentMode = paymentMode;
    }


    /**
     * Gets the shopTransactionID value for this PaymentTransactionStatus.
     * 
     * @return shopTransactionID
     */
    public java.lang.String getShopTransactionID() {
        return shopTransactionID;
    }


    /**
     * Sets the shopTransactionID value for this PaymentTransactionStatus.
     * 
     * @param shopTransactionID
     */
    public void setShopTransactionID(java.lang.String shopTransactionID) {
        this.shopTransactionID = shopTransactionID;
    }


    /**
     * Gets the bankTransactionID value for this PaymentTransactionStatus.
     * 
     * @return bankTransactionID
     */
    public java.lang.String getBankTransactionID() {
        return bankTransactionID;
    }


    /**
     * Sets the bankTransactionID value for this PaymentTransactionStatus.
     * 
     * @param bankTransactionID
     */
    public void setBankTransactionID(java.lang.String bankTransactionID) {
        this.bankTransactionID = bankTransactionID;
    }


    /**
     * Gets the authorizationCode value for this PaymentTransactionStatus.
     * 
     * @return authorizationCode
     */
    public java.lang.String getAuthorizationCode() {
        return authorizationCode;
    }


    /**
     * Sets the authorizationCode value for this PaymentTransactionStatus.
     * 
     * @param authorizationCode
     */
    public void setAuthorizationCode(java.lang.String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }


    /**
     * Gets the currency value for this PaymentTransactionStatus.
     * 
     * @return currency
     */
    public java.lang.String getCurrency() {
        return currency;
    }


    /**
     * Sets the currency value for this PaymentTransactionStatus.
     * 
     * @param currency
     */
    public void setCurrency(java.lang.String currency) {
        this.currency = currency;
    }


    /**
     * Gets the eventList value for this PaymentTransactionStatus.
     * 
     * @return eventList
     */
    public com.techedge.mp.forecourt.integration.refuel.client.entities.Event[] getEventList() {
        return eventList;
    }


    /**
     * Sets the eventList value for this PaymentTransactionStatus.
     * 
     * @param eventList
     */
    public void setEventList(com.techedge.mp.forecourt.integration.refuel.client.entities.Event[] eventList) {
        this.eventList = eventList;
    }

    public com.techedge.mp.forecourt.integration.refuel.client.entities.Event getEventList(int i) {
        return this.eventList[i];
    }

    public void setEventList(int i, com.techedge.mp.forecourt.integration.refuel.client.entities.Event _value) {
        this.eventList[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentTransactionStatus)) return false;
        PaymentTransactionStatus other = (PaymentTransactionStatus) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.shopLogin==null && other.getShopLogin()==null) || 
             (this.shopLogin!=null &&
              this.shopLogin.equals(other.getShopLogin()))) &&
            ((this.acquirerID==null && other.getAcquirerID()==null) || 
             (this.acquirerID!=null &&
              this.acquirerID.equals(other.getAcquirerID()))) &&
            ((this.paymentMode==null && other.getPaymentMode()==null) || 
             (this.paymentMode!=null &&
              this.paymentMode.equals(other.getPaymentMode()))) &&
            ((this.shopTransactionID==null && other.getShopTransactionID()==null) || 
             (this.shopTransactionID!=null &&
              this.shopTransactionID.equals(other.getShopTransactionID()))) &&
            ((this.bankTransactionID==null && other.getBankTransactionID()==null) || 
             (this.bankTransactionID!=null &&
              this.bankTransactionID.equals(other.getBankTransactionID()))) &&
            ((this.authorizationCode==null && other.getAuthorizationCode()==null) || 
             (this.authorizationCode!=null &&
              this.authorizationCode.equals(other.getAuthorizationCode()))) &&
            ((this.currency==null && other.getCurrency()==null) || 
             (this.currency!=null &&
              this.currency.equals(other.getCurrency()))) &&
            ((this.eventList==null && other.getEventList()==null) || 
             (this.eventList!=null &&
              java.util.Arrays.equals(this.eventList, other.getEventList())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getShopLogin() != null) {
            _hashCode += getShopLogin().hashCode();
        }
        if (getAcquirerID() != null) {
            _hashCode += getAcquirerID().hashCode();
        }
        if (getPaymentMode() != null) {
            _hashCode += getPaymentMode().hashCode();
        }
        if (getShopTransactionID() != null) {
            _hashCode += getShopTransactionID().hashCode();
        }
        if (getBankTransactionID() != null) {
            _hashCode += getBankTransactionID().hashCode();
        }
        if (getAuthorizationCode() != null) {
            _hashCode += getAuthorizationCode().hashCode();
        }
        if (getCurrency() != null) {
            _hashCode += getCurrency().hashCode();
        }
        if (getEventList() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEventList());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEventList(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentTransactionStatus.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://entities.client.refuel.integration.forecourt.mp.techedge.com/", "paymentTransactionStatus"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shopLogin");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shopLogin"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("acquirerID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "acquirerID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentMode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentMode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("shopTransactionID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "shopTransactionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("bankTransactionID");
        elemField.setXmlName(new javax.xml.namespace.QName("", "bankTransactionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("authorizationCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "authorizationCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("currency");
        elemField.setXmlName(new javax.xml.namespace.QName("", "currency"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://entities.client.refuel.integration.forecourt.mp.techedge.com/", "eventList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities.client.refuel.integration.forecourt.mp.techedge.com/", "eventList"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
