package com.techedge.mp.forecourt.integration.refuel.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "endRefuelMessageRequest", propOrder = {
	"requestID",
	"transactionID",
	"refuelDetail"
})
public class EndRefuelMessageRequest {

	
	@XmlElement(required=true)
	private String requestID;
	@XmlElement(required=true)
	private String transactionID;
	@XmlElement(required=true)
	private RefuelDetails refuelDetail;
	
	
	public final String getRequestID() {
		return requestID;
	}
	public final void setRequestID(String requestID) {
		this.requestID = requestID;
	}
	public final String getTransactionID() {
		return transactionID;
	}
	public final void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}
	public final RefuelDetails getRefuelDetail() {
		return refuelDetail;
	}
	public final void setRefuelDetail(RefuelDetails refuelDetail) {
		this.refuelDetail = refuelDetail;
	}
	
	
}
