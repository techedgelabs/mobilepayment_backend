/**
 * RefuelService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client;

public interface RefuelService extends javax.xml.rpc.Service {
    public java.lang.String getRefuelPortAddress();

    public com.techedge.mp.forecourt.integration.refuel.client.Refuel getRefuelPort() throws javax.xml.rpc.ServiceException;

    public com.techedge.mp.forecourt.integration.refuel.client.Refuel getRefuelPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
