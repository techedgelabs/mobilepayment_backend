/**
 * PaymentTransactionStatusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.forecourt.integration.refuel.client.entities;

public class PaymentTransactionStatusResponse  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -449014478213872188L;

    private com.techedge.mp.forecourt.integration.refuel.client.entities.EnumSTATUSCODE statusCode;

    private java.lang.String messageCode;

    private com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatus paymentTransactionStatus;

    private com.techedge.mp.forecourt.integration.refuel.client.entities.Voucher[] voucher;

    public PaymentTransactionStatusResponse() {
    }

    public PaymentTransactionStatusResponse(
           com.techedge.mp.forecourt.integration.refuel.client.entities.EnumSTATUSCODE statusCode,
           java.lang.String messageCode,
           com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatus paymentTransactionStatus,
           com.techedge.mp.forecourt.integration.refuel.client.entities.Voucher[] voucher) {
           this.statusCode = statusCode;
           this.messageCode = messageCode;
           this.paymentTransactionStatus = paymentTransactionStatus;
           this.voucher = voucher;
    }


    /**
     * Gets the statusCode value for this PaymentTransactionStatusResponse.
     * 
     * @return statusCode
     */
    public com.techedge.mp.forecourt.integration.refuel.client.entities.EnumSTATUSCODE getStatusCode() {
        return statusCode;
    }


    /**
     * Sets the statusCode value for this PaymentTransactionStatusResponse.
     * 
     * @param statusCode
     */
    public void setStatusCode(com.techedge.mp.forecourt.integration.refuel.client.entities.EnumSTATUSCODE statusCode) {
        this.statusCode = statusCode;
    }


    /**
     * Gets the messageCode value for this PaymentTransactionStatusResponse.
     * 
     * @return messageCode
     */
    public java.lang.String getMessageCode() {
        return messageCode;
    }


    /**
     * Sets the messageCode value for this PaymentTransactionStatusResponse.
     * 
     * @param messageCode
     */
    public void setMessageCode(java.lang.String messageCode) {
        this.messageCode = messageCode;
    }


    /**
     * Gets the paymentTransactionStatus value for this PaymentTransactionStatusResponse.
     * 
     * @return paymentTransactionStatus
     */
    public com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatus getPaymentTransactionStatus() {
        return paymentTransactionStatus;
    }


    /**
     * Sets the paymentTransactionStatus value for this PaymentTransactionStatusResponse.
     * 
     * @param paymentTransactionStatus
     */
    public void setPaymentTransactionStatus(com.techedge.mp.forecourt.integration.refuel.client.entities.PaymentTransactionStatus paymentTransactionStatus) {
        this.paymentTransactionStatus = paymentTransactionStatus;
    }


    /**
     * Gets the voucher value for this PaymentTransactionStatusResponse.
     * 
     * @return voucher
     */
    public com.techedge.mp.forecourt.integration.refuel.client.entities.Voucher[] getVoucher() {
        return voucher;
    }


    /**
     * Sets the voucher value for this PaymentTransactionStatusResponse.
     * 
     * @param voucher
     */
    public void setVoucher(com.techedge.mp.forecourt.integration.refuel.client.entities.Voucher[] voucher) {
        this.voucher = voucher;
    }

    public com.techedge.mp.forecourt.integration.refuel.client.entities.Voucher getVoucher(int i) {
        return this.voucher[i];
    }

    public void setVoucher(int i, com.techedge.mp.forecourt.integration.refuel.client.entities.Voucher _value) {
        this.voucher[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PaymentTransactionStatusResponse)) return false;
        PaymentTransactionStatusResponse other = (PaymentTransactionStatusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.statusCode==null && other.getStatusCode()==null) || 
             (this.statusCode!=null &&
              this.statusCode.equals(other.getStatusCode()))) &&
            ((this.messageCode==null && other.getMessageCode()==null) || 
             (this.messageCode!=null &&
              this.messageCode.equals(other.getMessageCode()))) &&
            ((this.paymentTransactionStatus==null && other.getPaymentTransactionStatus()==null) || 
             (this.paymentTransactionStatus!=null &&
              this.paymentTransactionStatus.equals(other.getPaymentTransactionStatus()))) &&
            ((this.voucher==null && other.getVoucher()==null) || 
             (this.voucher!=null &&
              java.util.Arrays.equals(this.voucher, other.getVoucher())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatusCode() != null) {
            _hashCode += getStatusCode().hashCode();
        }
        if (getMessageCode() != null) {
            _hashCode += getMessageCode().hashCode();
        }
        if (getPaymentTransactionStatus() != null) {
            _hashCode += getPaymentTransactionStatus().hashCode();
        }
        if (getVoucher() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVoucher());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVoucher(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PaymentTransactionStatusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://entities.client.refuel.integration.forecourt.mp.techedge.com/", "paymentTransactionStatusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "statusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities.client.refuel.integration.forecourt.mp.techedge.com/", "enumSTATUSCODE"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageCode");
        elemField.setXmlName(new javax.xml.namespace.QName("", "messageCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("paymentTransactionStatus");
        elemField.setXmlName(new javax.xml.namespace.QName("", "paymentTransactionStatus"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities.client.refuel.integration.forecourt.mp.techedge.com/", "paymentTransactionStatus"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("voucher");
        elemField.setXmlName(new javax.xml.namespace.QName("", "Voucher"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://entities.client.refuel.integration.forecourt.mp.techedge.com/", "voucher"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
