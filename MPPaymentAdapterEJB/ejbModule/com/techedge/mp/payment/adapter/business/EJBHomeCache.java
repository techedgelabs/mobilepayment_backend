package com.techedge.mp.payment.adapter.business;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.PostPaidV2TransactionServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;

public class EJBHomeCache {

    final String                       parametersServiceRemoteJndi  = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
    final String                       loggerServiceRemoteJndi      = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
    final String                       userServiceRemoteJndi        = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/UserService!com.techedge.mp.core.business.UserServiceRemote";
    final String                       transactionServiceRemoteJndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/TransactionService!com.techedge.mp.core.business.TransactionServiceRemote";
    final String                       postPaidV2TransactionServiceRemoteJndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/PostPaidV2TransactionService!com.techedge.mp.core.business.PostPaidV2TransactionServiceRemote";
    final String                       paymentServiceRemoteJndi     = "java:global/MPFidelityAdapterEAR/MPFidelityAdapterEJB/PaymentService!com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote";
    
    private static EJBHomeCache        instance;

    protected Context                  context                      = null;

    protected ParametersServiceRemote  parametersService            = null;
    protected LoggerServiceRemote      loggerService                = null;
    protected UserServiceRemote        userService                  = null;
    protected TransactionServiceRemote transactionService           = null;
    protected PostPaidV2TransactionServiceRemote postPaidV2TransactionService = null;
    protected PaymentServiceRemote     paymentService               = null;

    private EJBHomeCache() throws InterfaceNotFoundException {

        final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();

        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

        try {
            context = new InitialContext(jndiProperties);
        }
        catch (NamingException e) {

            throw new InterfaceNotFoundException("Naming exception: " + e.getMessage());
        }

        try {
            parametersService = (ParametersServiceRemote) context.lookup(parametersServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(parametersServiceRemoteJndi);
        }

        try {
            loggerService = (LoggerServiceRemote) context.lookup(loggerServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(loggerServiceRemoteJndi);
        }

        try {
            userService = (UserServiceRemote) context.lookup(userServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(userServiceRemoteJndi);
        }

        try {
            transactionService = (TransactionServiceRemote) context.lookup(transactionServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(transactionServiceRemoteJndi);
        }
        
        try {
            postPaidV2TransactionService = (PostPaidV2TransactionServiceRemote) context.lookup(postPaidV2TransactionServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(postPaidV2TransactionServiceRemoteJndi);
        }

        try {
            paymentService = (PaymentServiceRemote) context.lookup(paymentServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(paymentServiceRemoteJndi);
        }

    }

    public static synchronized EJBHomeCache getInstance() throws InterfaceNotFoundException {
        if (instance == null)
            instance = new EJBHomeCache();

        return instance;
    }

    public ParametersServiceRemote getParametersService() {
        return parametersService;
    }

    public LoggerServiceRemote getLoggerService() {
        return loggerService;
    }

    public UserServiceRemote getUserService() {
        return userService;
    }

    public TransactionServiceRemote getTransactionService() {
        return transactionService;
    }

    public PaymentServiceRemote getPaymentService() {
        return paymentService;
    }

    public PostPaidV2TransactionServiceRemote getPostPaidV2TransactionService() {
        return postPaidV2TransactionService;
    }
    
}
