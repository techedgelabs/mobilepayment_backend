package com.techedge.mp.payment.adapter.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Node;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.PostPaidV2TransactionServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.PostPaidTransaction;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.TransactionOperation;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionOperation;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CodeEnum;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnumStatusType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteAuthorizationResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteCaptureResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecutePaymentResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteReversalResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetPaymentStatusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.KeyValueInfo;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PaymentRefuelMode;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductCodeEnum;
import com.techedge.mp.fidelity.adapter.business.utilities.AmountConverter;
import com.techedge.mp.payment.adapter.business.interfaces.ExtendedGestPayData;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;
import com.techedge.mp.payment.adapter.business.interfaces.KeyValueData;
import com.techedge.mp.payment.adapter.cartasi.client.PaymentClientImplementation;
import com.techedge.mp.payment.adapter.cartasi.client.TipoUtilizzo;
import com.techedge.mp.payment.adapter.wss2s.ApplePayRequest;
import com.techedge.mp.payment.adapter.wss2s.CallDeleteS2SResponse.CallDeleteS2SResult;
import com.techedge.mp.payment.adapter.wss2s.CallDeleteTokenS2SResponse.CallDeleteTokenS2SResult;
import com.techedge.mp.payment.adapter.wss2s.CallPagamS2SResponse.CallPagamS2SResult;
import com.techedge.mp.payment.adapter.wss2s.CallReadTrxS2SResponse.CallReadTrxS2SResult;
import com.techedge.mp.payment.adapter.wss2s.CallRefundS2SResponse.CallRefundS2SResult;
import com.techedge.mp.payment.adapter.wss2s.CallSettleS2SResponse.CallSettleS2SResult;
import com.techedge.mp.payment.adapter.wss2s.WSs2S;
import com.techedge.mp.payment.adapter.wss2s.WSs2SSoap;
import com.techedge.mp.payment.adapter.wss2s.xsd.EventsType;
import com.techedge.mp.payment.adapter.wss2s.xsd.EventsType.Event;
import com.techedge.mp.payment.adapter.wss2s.xsd.BuyerType;
import com.techedge.mp.payment.adapter.wss2s.xsd.GestPayS2S;
import com.techedge.mp.payment.adapter.wss2s.xsd.TransResult;
import com.techedge.mp.payment.adapter.wss2s.xsd.TransType;
import com.techedge.mp.payment.adapter.wss2s.xsd.VbVType;

/**
 * Session Bean implementation class GPService
 */
@Stateless
@LocalBean
public class GPService implements GPServiceRemote, GPServiceLocal {
    private final static String     PARAM_SHOPLOGIN                    = "SHOPLOGIN";
    private final static String     PARAM_UIC                          = "UIC";
    private final static String     PARAM_INITIAL_AMOUNT               = "INITIAL_AMOUNT";
    private final static String     PARAM_PROXY_HOST                   = "PROXY_HOST";
    private final static String     PARAM_PROXY_PORT                   = "PROXY_PORT";
    private final static String     PARAM_PROXY_NO_HOSTS               = "PROXY_NO_HOSTS";

    private final static String     PARAM_PAYMENT_OUT_WSDL             = "PAYMENT_OUT_WSDL";
    private final static String     PARAM_PAYMENT_URL_PREFIX_WSDL      = "PAYMENT_URL_PREFIX_WSDL";

    private final static String     PARAM_SIMULATION_ACTIVE            = "SIMULATION_ACTIVE";
    private final static String     PARAM_SIMULATION_CALLPAGAM_ERROR   = "SIMULATION_GESTPAY_CALLPAGAM_ERROR";
    private final static String     PARAM_SIMULATION_CALLSETTLE_ERROR  = "SIMULATION_GESTPAY_CALLSETTLE_ERROR";
    private final static String     PARAM_SIMULATION_CALLREFUND_ERROR  = "SIMULATION_GESTPAY_CALLREFUND_ERROR";
    private final static String     PARAM_SIMULATION_DELETEPAGAM_ERROR = "SIMULATION_GESTPAY_DELETEPAGAM_ERROR";

    private final static String     PARAM_SIMULATION_CALLPAGAM_KO      = "SIMULATION_GESTPAY_CALLPAGAM_KO";
    private final static String     PARAM_SIMULATION_CALLSETTLE_KO     = "SIMULATION_GESTPAY_CALLSETTLE_KO";
    private final static String     PARAM_SIMULATION_CALLREFUND_KO     = "SIMULATION_GESTPAY_CALLREFUND_KO";
    private final static String     PARAM_SIMULATION_DELETEPAGAM_KO    = "SIMULATION_GESTPAY_DELETEPAGAM_KO";

    private ParametersServiceRemote  parametersService                  = null;
    private LoggerServiceRemote      loggerService                      = null;
    private PaymentServiceRemote     paymentService                     = null;
    //private UserServiceRemote        userService                        = null;
    private TransactionServiceRemote transactionService                 = null;
    private PostPaidV2TransactionServiceRemote postPaidV2TransactionService   = null;

    private URL                     url;

    // Valori di default
    private String                  shopLogin                          = "GESPAY61144";
    private String                  uic                                = "242";                                 //Euro
    private String                  amount                             = "0.01";
    private String                  proxyHost                          = "mpsquid.enimp.pri";
    private String                  proxyPort                          = "3128";
    private String                  proxyNoHosts                       = "localhost|127.0.0.1|*.enimp.pri";
    private boolean                 simulationActive                   = false;

    private GestPayData             gestPayData;
    private ExtendedGestPayData     extendedGestPayData;

    /**
     * Default constructor.
     */
    public GPService() {
        try {
            this.loggerService     = EJBHomeCache.getInstance().getLoggerService();
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
            this.paymentService    = EJBHomeCache.getInstance().getPaymentService();
            //this.userService = EJBHomeCache.getInstance().getUserService();
            this.transactionService = EJBHomeCache.getInstance().getTransactionService();
            this.postPaidV2TransactionService = EJBHomeCache.getInstance().getPostPaidV2TransactionService();
        }
        catch (InterfaceNotFoundException e) {
            e.printStackTrace();
        }

        String paymentUrlPrefixWsdl = "";
        try {
            paymentUrlPrefixWsdl = parametersService.getParamValue(GPService.PARAM_PAYMENT_URL_PREFIX_WSDL);
            this.proxyHost = parametersService.getParamValue(GPService.PARAM_PROXY_HOST);
            this.proxyPort = parametersService.getParamValue(GPService.PARAM_PROXY_PORT);
            this.proxyNoHosts = parametersService.getParamValue(GPService.PARAM_PROXY_NO_HOSTS);
            this.shopLogin = parametersService.getParamValue(GPService.PARAM_SHOPLOGIN);
            this.uic = parametersService.getParamValue(GPService.PARAM_UIC);
            this.simulationActive = Boolean.parseBoolean(parametersService.getParamValue(GPService.PARAM_SIMULATION_ACTIVE));
            
            if (simulationActive) {
                System.out.println("Simulazione Adapter GestPay attiva");
            }
        }
        catch (ParameterNotFoundException e) {
            e.printStackTrace();
        }

        try {
            InputStream fin = this.getClass().getResourceAsStream("/com/techedge/mp/payment/adapter/wsdl/WSs2s.wsdl");

            InputStreamReader is = new InputStreamReader(fin);
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(is);
            String read;

            while ((read = br.readLine()) != null) {
                sb.append(read);
            }

            br.close();
            read = sb.toString().replaceAll("__PAYMENT_URL_PREFIX_WSDL__", paymentUrlPrefixWsdl);

            /*
             * System.out.println("WSDL PAYMENT OUT: (Start)");
             * System.out.println(read);
             * System.out.println("WSDL PAYMENT OUT: (End)");
             */

            String prefix = System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "wsdl" + File.separator + "WSs2s";
            String suffix = ".wsdl";
            File tempFile = File.createTempFile(prefix, suffix);
            FileOutputStream fout = new FileOutputStream(tempFile);
            tempFile.deleteOnExit();
            fout.write(read.getBytes());
            fout.close();

            if (tempFile.exists()) {
                System.out.println("Create temp gpserverice wsdl: " + tempFile.getAbsolutePath());
            }
            else {
                System.err.println("Cannot to create temp gpserverice wsdl: " + tempFile.getAbsolutePath());
            }

            this.url = tempFile.toURI().toURL();
            //this.url = new URL(wsdlString);
        }
        catch (MalformedURLException e) {

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "Constructor", null, null, "connection error - url: " + this.url);

            e.printStackTrace();
        }
        catch (IOException e) {

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "Constructor", null, null, "connection error - url: " + this.url);

            e.printStackTrace();
        }
    }

    //@WebMethod(operationName = "callPagam")
    public GestPayData callPagam(Double i_amount, String i_shopTransactionID, String i_shopLogin, String i_valuta, String i_token, String i_applePayPKPaymentToken, String i_acquirerID, String i_groupAcquirer, String i_encodedSecretKey, String i_paymentMethodExpiration, Extension[] i_extension, String category) {

        String shopLogin = i_shopLogin;// dbobject.getShopLogin(); 
        String uicCode = i_valuta;
        String amount = Double.toString(i_amount);
        String shopTransactionId = i_shopTransactionID;
        String tokenValue = i_token;
        String acquirerId = i_acquirerID;
        String groupAcquirer = i_groupAcquirer;
        String encodedSecretKey = i_encodedSecretKey;
        String paymentMethodExpiration = i_paymentMethodExpiration;
        //String customInfo = "CST_DISTRID=";//this.setCustomInfo(i_extension);
        String customInfo = this.setCustomInfo(i_extension);

        System.out.println("callPagamByRM");

        System.out.println("SL: " + shopLogin + " _uicode: " + uicCode + " _amount: " + amount + " _stid: " + shopTransactionId + " _token: " + tokenValue);
        System.out.println("acquirerId: " + acquirerId + " groupAcquirer: " + groupAcquirer + " encodedSecretKey: " + encodedSecretKey + " paymentMethodExpiration: " + paymentMethodExpiration);

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("amount", String.valueOf(i_amount)));
        inputParameters.add(new Pair<String, String>("shopTransactionID", i_shopTransactionID));
        inputParameters.add(new Pair<String, String>("shopLogin", i_shopLogin));
        inputParameters.add(new Pair<String, String>("valuta", i_valuta));
        inputParameters.add(new Pair<String, String>("token", i_token));
        inputParameters.add(new Pair<String, String>("acquirerID", i_acquirerID));
        if ( i_applePayPKPaymentToken != null ) {
            inputParameters.add(new Pair<String, String>("applePayPKPaymentToken", "not null"));
        }
        else {
            inputParameters.add(new Pair<String, String>("applePayPKPaymentToken", "null"));
        }
        inputParameters.add(new Pair<String, String>("groupAcquirer", i_groupAcquirer));
        inputParameters.add(new Pair<String, String>("encodedSecretKey", i_encodedSecretKey));
        inputParameters.add(new Pair<String, String>("customInfo", customInfo));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "callPagamByRM", i_shopTransactionID, "opening", ActivityLog.createLogMessage(inputParameters));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        String simulationCallPagamError = null;
        boolean simulationCallPagamKO = false;
        
        try {

            if (simulationActive) {
                simulationCallPagamError = parametersService.getParamValueNoCache(GPService.PARAM_SIMULATION_CALLPAGAM_ERROR);
                simulationCallPagamKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(GPService.PARAM_SIMULATION_CALLPAGAM_KO));
                
                if (simulationCallPagamError != null && simulationCallPagamError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }

                if (simulationCallPagamKO) {
                    gestPayData = new GestPayData();
                    gestPayData.setErrorCode("9999");
                    gestPayData.setErrorDescription("Simulazione di KO");
                    gestPayData.setTransactionResult("KO");
                    gestPayData.setShopTransactionID(shopTransactionId);
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCurrency(uicCode);

                    GestPayS2S gps2s = new GestPayS2S();
                    gps2s.setAlertCode(null);
                    gps2s.setAlertDescription(null);
                    gps2s.setAuthorizationCode(null);
                    gps2s.setBankTransactionID(null);
                    gps2s.setBuyer(null);
                    gps2s.setCompany(null);
                    gps2s.setCountry(null);
                    gps2s.setCurrency(uicCode);
                    gps2s.setCustomInfo(null);
                    gps2s.setErrorCode("9999");
                    gps2s.setErrorDescription("Simulazione di KO");
                    gps2s.setShopTransactionID(i_shopTransactionID);
                    gps2s.setTransactionKey(null);
                    gps2s.setTransactionResult(TransResult.KO);
                    gps2s.setTransactionState(null);
                    gps2s.setTransactionType(TransType.PAGAM);
                    gps2s.setVbV(null);
                    
                    gestPayData.setGps2s(gps2s);
                    
                    return gestPayData;
                }
            }

            proxy.setHttp();
            
            ApplePayRequest applePay = new ApplePayRequest();
            if ( i_applePayPKPaymentToken != null ) {
                
                System.out.println("i_applePayPKPaymentToken: " + i_applePayPKPaymentToken);
                System.out.println("Valorizzazione oggetto applePay in richiesta callPagam");
                applePay.setApplePayPKPaymentToken(i_applePayPKPaymentToken);
            }
            
            CallPagamS2SResult eresult = null;
            
            if ( acquirerId.equals("BANCASELLA")) {
            
                WSs2S service1 = new WSs2S(this.url);
                System.out.println("Create Web Service...");
                WSs2SSoap port1 = service1.getWSs2SSoap();
                System.out.println("Call Web Service Operation...");
                eresult = port1.callPagamS2S(shopLogin, uicCode, amount, shopTransactionId, null, null, null, null,//cardNumber, expiryMonth, expiryYear, buyerName, 
                        null, null, null, null,//buyerEmail, languageId, cvv, min,  
                        null, null, customInfo, null,//transKey, paRes, customInfo, idea,  
                        null, tokenValue, null, null,//requestToken,tokenValue, clientIP, itemType,  
                        null, null, null, null,//shippingDetails, redFraudPrevention, redCustomerInfo, redShippingInfo,  
                        null, null, null, null, applePay, null //redBillingInfo, redCustomerData, redCustomInfo, redItems, applePay, orderDetails
                );
                
                proxy.unsetHttp();
                GestPayS2S gps2s;
                Object content = ((List) eresult.getContent()).get(0);
                JAXBContext context;
                try {
                    System.out.println("OK");
                    context = JAXBContext.newInstance(GestPayS2S.class);
                    Unmarshaller um = context.createUnmarshaller();
                    gps2s = (GestPayS2S) um.unmarshal((Node) content);

                    gestPayData = new GestPayData();
                    gestPayData.setGps2s(gps2s);
                    gestPayData.setTransactionType(gps2s.getTransactionType().value());
                    gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                    gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                    gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                    gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                    gestPayData.setCurrency(gps2s.getCurrency());
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCountry(gps2s.getCountry());

                    if (gps2s.getCustomInfo() != null) {
                        gestPayData.setCustomInfo(gps2s.getCustomInfo().toString());
                    }
                    if (gps2s.getBuyer() != null) {
                        gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                        gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                    }
                    gestPayData.setErrorCode(gps2s.getErrorCode());
                    gestPayData.setErrorDescription(gps2s.getErrorDescription());
                    gestPayData.setAlertCode(gps2s.getAlertCode());
                    gestPayData.setAlertDescription(gps2s.getAlertDescription());
                    if (gps2s.getVbV() != null) {
                        gestPayData.setVbVRisp(gps2s.getVbV().getVbVRisp());
                        gestPayData.setVbVBuyer(gps2s.getVbV().getVbVBuyer());
                        gestPayData.setVbVFlag(gps2s.getVbV().getVbVFlag());
                    }
                    System.out.println("gestPayData: " + gestPayData);
                    //this.setPositiveResponse("P", i_shopTransactionID ,gps2s, null);
                }
                catch (JAXBException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamByRM", i_shopTransactionID, null, "Error parsing response: " + e.getMessage());
                    return null;
                    //              this.setNegativeResponse("P","", i_shopTransactionID );
                    //              throw new BPELException();
                }
            }
            else {
                
                if ( acquirerId.equals("CARTASI")) {
                    
                    PaymentClientImplementation paymentClientImplementation = new PaymentClientImplementation();
                    
                    TipoUtilizzo tipoUtilizzo = TipoUtilizzo.UTENTI_PRIVATI;
                    if (category.equals(TransactionCategoryType.TRANSACTION_BUSINESS.getValue())) {
                        tipoUtilizzo = TipoUtilizzo.AZIENDE;
                    }
                    
                    GestPayS2S gps2s = paymentClientImplementation.callPagamS2S(shopLogin, uicCode, amount, shopTransactionId, i_extension, tokenValue, groupAcquirer, encodedSecretKey, paymentMethodExpiration, tipoUtilizzo);
                    
                    proxy.unsetHttp();
                    
                    System.out.println("OK");
                    
                    gestPayData = new GestPayData();
                    gestPayData.setGps2s(gps2s);
                    gestPayData.setTransactionType(gps2s.getTransactionType().value());
                    gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                    gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                    gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                    gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                    gestPayData.setCurrency(gps2s.getCurrency());
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCountry(gps2s.getCountry());

                    if (gps2s.getCustomInfo() != null) {
                        gestPayData.setCustomInfo(gps2s.getCustomInfo().toString());
                    }
                    if (gps2s.getBuyer() != null) {
                        gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                        gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                    }
                    gestPayData.setErrorCode(gps2s.getErrorCode());
                    gestPayData.setErrorDescription(gps2s.getErrorDescription());
                    gestPayData.setAlertCode(gps2s.getAlertCode());
                    gestPayData.setAlertDescription(gps2s.getAlertDescription());
                    if (gps2s.getVbV() != null) {
                        gestPayData.setVbVRisp(gps2s.getVbV().getVbVRisp());
                        gestPayData.setVbVBuyer(gps2s.getVbV().getVbVBuyer());
                        gestPayData.setVbVFlag(gps2s.getVbV().getVbVFlag());
                    }
                    System.out.println("gestPayData: " + gestPayData);
                    //this.setPositiveResponse("P", i_shopTransactionID ,gps2s, null);
                }
                else {
                    
                    if ( acquirerId.equals("MULTICARD")) {
                        
                        /*
                         * La preautorizzazione � stata gi� effettuata durante la creazione.
                         * In questo step vanno solo restituiti i dati gi� presenti a sistema.
                         */
                        
                        
                        Date now = new Date();
                        
                        //String operationID        = "00000a" + new IdGenerator().generateId(16).substring(0, 26); //32);
                        String operationID = new IdGenerator().generateId(16).substring(0, 32);
                        
                        Long requestTimestamp     = now.getTime();
                        
                        Transaction transaction = transactionService.getTransactionDetail(operationID, shopTransactionId);
                        /*
                        TransactionOperation transactionOperationAut = null;
                        for(TransactionOperation transactionOperation : transaction.getTransactionOperationList()) {
                            if (transactionOperation.getOperationType().equals("AUT")) {
                                transactionOperationAut = transactionOperation;
                                break;
                            }
                        }
                        
                        if (transactionOperationAut == null) {
                            // TODO: gestire errore null
                            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamByRM", i_shopTransactionID, null, "Error operazione di autorizzazioen non trovata");
                            return null;
                        }
                        
                        String paymentOperationId = transactionOperationAut.getOperationId();
                        
                        GetPaymentStatusResult getPaymentStatusResult = paymentService.getPaymentStatus(
                                paymentOperationId,
                                operationID,
                                PartnerType.EM,
                                requestTimestamp);
                        
                        if (getPaymentStatusResult == null) {
                            
                            // TODO: gestire errore null
                            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamByRM", i_shopTransactionID, null, "Error getPaymentStatusResult null");
                            return null;
                        }
                        if (!getPaymentStatusResult.getExecuteAuthorizationResult().getResult().getStatus().value().equals(EnumStatusType.SUCCESS.value())) {
                            
                            // Gestire stato != SUCCESS
                            CodeEnum code  = getPaymentStatusResult.getResult().getCode();
                            String message = getPaymentStatusResult.getResult().getMessage();
                            
                            // Errore applicativo nella chiamata al servizio di movimentazione credito con multicard
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", i_shopTransactionID, null, "getPaymentStatusResult - code: " + code.getValue() + ", message: " + message);
                            
                            // Gestione errori
                            // 00000 Nessun errore
                            // 00001 Parametri in input errati
                            // 00106 Partner Type non valido
                            // 00112 Operazioni di pagamento non trovate per l�operationId di input
                            // 00118 Operazione in timeout
                            // 00119 Operazione in corso
                            // 01*** Errori restituiti dall�autorizzativo
                            // 99001 Generic error 
                            
                            // Mappatura codici e descrizioni errori
                            String errorCode = code.getValue();
                            String errorDescription = message;
                            
                            System.out.println("La transazione " + shopTransactionId + " e' stata rifiutata; descrizione errore: " + errorDescription + " ("
                                    + errorCode + ")");

                            // Errori che devono essere riprocessati tramite riconciliazione
                            if (errorCode.equals(CodeEnum.TIMEOUT_OPERATION.getValue()) ||
                                errorCode.equals(CodeEnum.OPERATION_NOT_COMPLETED .getValue()) ||
                                errorCode.equals(CodeEnum.GENERIC_ERROR.getValue())) {
                                
                                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", i_shopTransactionID, null, "Error getPaymentStatusResult: " + errorCode + " -> transazione da riconciliare");
                                proxy.unsetHttp();
                                return null;
                            }
                            
                            if (errorDescription != null && errorDescription.length() > 255) {
                                errorDescription = errorDescription.substring(0, 255);
                            }

                            System.out.println("KO");
                            
                            gestPayData = new GestPayData();
                            gestPayData.setErrorCode(errorCode);
                            gestPayData.setErrorDescription(errorDescription);
                            gestPayData.setTransactionResult("KO");
                            gestPayData.setShopTransactionID(shopTransactionId);
                            gestPayData.setAmount(String.valueOf(i_amount));
                            gestPayData.setCurrency(uicCode);
                            gestPayData.setTransactionType("AUT");
                            
                            GestPayS2S gps2s = new GestPayS2S();

                            gps2s.setTransactionType(TransType.PAGAM);
                            gps2s.setTransactionResult(TransResult.KO);
                            gps2s.setShopTransactionID(shopTransactionId);
                            gps2s.setBankTransactionID("");
                            gps2s.setAuthorizationCode("");
                            gps2s.setCurrency(uicCode);
                            gps2s.setCountry("");
                            gps2s.setCustomInfo("");
                            gps2s.setBuyer(new BuyerType());
                            gps2s.setErrorCode(errorCode);
                            gps2s.setErrorDescription(errorDescription);
                            gps2s.setAlertCode("");
                            gps2s.setAlertDescription("");
                            gps2s.setVbV(new VbVType());

                            gestPayData.setGps2s(gps2s);
                        }
                        else {
                            
                            ExecuteAuthorizationResult executeAuthorizationResult = getPaymentStatusResult.getExecuteAuthorizationResult();
                            
                            System.out.println("OK");
                            
                            Integer amountAuthorized = executeAuthorizationResult.getAmountAuthorized();
                            
                            System.out.println("Autorizzazione con multicard. Amount authorized: " + amountAuthorized);
                            Double doubleAmountAuthorized = AmountConverter.toInternal(amountAuthorized);

                            gestPayData = new GestPayData();
                            gestPayData.setErrorCode("0");
                            gestPayData.setErrorDescription("Autorizzazione con Multicard");
                            gestPayData.setTransactionResult("OK");
                            gestPayData.setShopTransactionID(shopTransactionId);
                            gestPayData.setAmount(String.valueOf(doubleAmountAuthorized));
                            gestPayData.setCurrency(uicCode);
                            gestPayData.setTransactionType("AUT");
    
                            GestPayS2S gps2s = new GestPayS2S();
                            gps2s.setAlertCode(null);
                            gps2s.setAlertDescription(null);
                            gps2s.setAuthorizationCode(executeAuthorizationResult.getAuthCode());
                            gps2s.setBankTransactionID(executeAuthorizationResult.getRetrievalRefNumber());
                            gps2s.setBuyer(null);
                            gps2s.setCompany(null);
                            gps2s.setCountry(null);
                            gps2s.setCurrency(uicCode);
                            gps2s.setCustomInfo(null);
                            gps2s.setErrorCode("0");
                            gps2s.setErrorDescription("Autorizzazione con Multicard");
                            gps2s.setShopTransactionID(i_shopTransactionID);
                            gps2s.setTransactionKey(null);
                            gps2s.setTransactionResult(TransResult.OK);
                            gps2s.setTransactionState(null);
                            gps2s.setTransactionType(TransType.PAGAM);
                            gps2s.setVbV(null);
                            
                            gestPayData.setGps2s(gps2s);
                        }
                        */
                        
                        System.out.println("Skip step preautorizzazione per carta multicard.");
                        
                        System.out.println("OK");
                        
                        Double doubleAmountAuthorized = transaction.getInitialAmount();

                        gestPayData = new GestPayData();
                        gestPayData.setErrorCode("0");
                        gestPayData.setErrorDescription("Autorizzazione con Multicard");
                        gestPayData.setTransactionResult("OK");
                        gestPayData.setShopTransactionID(shopTransactionId);
                        gestPayData.setAmount(String.valueOf(doubleAmountAuthorized));
                        gestPayData.setCurrency(uicCode);
                        gestPayData.setTransactionType("AUT");

                        GestPayS2S gps2s = new GestPayS2S();
                        gps2s.setAlertCode(null);
                        gps2s.setAlertDescription(null);
                        gps2s.setAuthorizationCode(transaction.getAuthorizationCode());
                        gps2s.setBankTransactionID(transaction.getBankTansactionID());
                        gps2s.setBuyer(null);
                        gps2s.setCompany(null);
                        gps2s.setCountry(null);
                        gps2s.setCurrency(uicCode);
                        gps2s.setCustomInfo(null);
                        gps2s.setErrorCode("0");
                        gps2s.setErrorDescription("Autorizzazione con Multicard");
                        gps2s.setShopTransactionID(i_shopTransactionID);
                        gps2s.setTransactionKey(null);
                        gps2s.setTransactionResult(TransResult.OK);
                        gps2s.setTransactionState(null);
                        gps2s.setTransactionType(TransType.PAGAM);
                        gps2s.setVbV(null);
                        
                        gestPayData.setGps2s(gps2s);
                    }
                    else {
                        
                        // TODO gestire l'errore
                    }
                } 
            }
            
            if (simulationActive) {
                if (simulationCallPagamError != null && simulationCallPagamError.equalsIgnoreCase("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }
        }

        catch (javax.xml.ws.soap.SOAPFaultException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }
        catch (javax.xml.ws.ProtocolException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }
        catch (javax.xml.ws.WebServiceException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }
        catch (java.lang.SecurityException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }

        //	paymentService.updateStatus(gsresponse.getStatusCode(), gsresponse.getMessageCode(), i_shopTransactionID);
        //gsresponse.getGPS2S().setTransactionType(TransType.PAGAM);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("TransactionType", gestPayData.getTransactionType().toString()));
        outputParameters.add(new Pair<String, String>("TransactionResult", gestPayData.getTransactionResult().toString()));
        outputParameters.add(new Pair<String, String>("ShopTransactionID", gestPayData.getShopTransactionID()));
        outputParameters.add(new Pair<String, String>("BankTransactionID", gestPayData.getBankTransactionID()));
        outputParameters.add(new Pair<String, String>("AuthorizationCode", gestPayData.getAuthorizationCode()));
        outputParameters.add(new Pair<String, String>("Currency", gestPayData.getCurrency()));
        outputParameters.add(new Pair<String, String>("Amount", gestPayData.getAmount()));
        outputParameters.add(new Pair<String, String>("Country", gestPayData.getCountry()));
        outputParameters.add(new Pair<String, String>("CustomInfo", gestPayData.getCustomInfo()));
        outputParameters.add(new Pair<String, String>("BuyerName", gestPayData.getBuyerName()));
        outputParameters.add(new Pair<String, String>("BuyerEmail", gestPayData.getBuyerEmail()));
        outputParameters.add(new Pair<String, String>("ErrorCode", gestPayData.getErrorCode()));
        outputParameters.add(new Pair<String, String>("ErrorDescription", gestPayData.getErrorDescription()));
        outputParameters.add(new Pair<String, String>("AlertCode", gestPayData.getAlertCode()));
        outputParameters.add(new Pair<String, String>("AlertDescription", gestPayData.getAlertDescription()));
        outputParameters.add(new Pair<String, String>("VbVRisp", gestPayData.getVbVRisp()));
        outputParameters.add(new Pair<String, String>("VbVBuyer", gestPayData.getVbVBuyer()));
        outputParameters.add(new Pair<String, String>("VbVFlag", gestPayData.getVbVFlag()));
        //		outputParameters.add(new Pair<String,String>("Token", gestPayData.getToken()));
        //		outputParameters.add(new Pair<String,String>("TokenExpiryMonth", gestPayData.getTokenExpiryMonth()));
        //		outputParameters.add(new Pair<String,String>("TokenExpiryYear", gestPayData.getTokenExpiryYear()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "callPagamByRM", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return gestPayData;
    }

    //@WebMethod(operationName = "callSettle")
    public GestPayData callSettle(Double i_amount, String i_shopTransactionID, String i_shopLogin, String i_valuta, String i_acquirerId, String i_groupAcquirer, String i_encodedSecretKey,
            String i_token, String i_authorizationCode, String i_bankTransactionId, String i_refuelMode, String i_productCode, Double i_quantity, Double i_unitPrice) {

        //String shopLogin = this.shopLogin;

        String shopLogin = i_shopLogin;

        //String uicCode = this.uic;
        String uicCode = i_valuta;
        String amount = Double.toString(i_amount);
        String shopTransactionId = i_shopTransactionID;
        String acquirerId = i_acquirerId;
        String groupAcquirer = i_groupAcquirer;
        String encodedSecretKey = i_encodedSecretKey;
        String token = i_token;
        String authorizationCode = i_authorizationCode;
        String bankTransactionId = i_bankTransactionId;
        
        PaymentRefuelMode refuelMode = null;
        if (i_refuelMode != null) {
            if (i_refuelMode.equalsIgnoreCase("self")) {
                refuelMode = PaymentRefuelMode.I;
            }
            else if (i_refuelMode.equalsIgnoreCase("Servito")) {
                refuelMode = PaymentRefuelMode.F;
            }
            else if (i_refuelMode.equalsIgnoreCase("Fai_da_te")) {
                refuelMode = PaymentRefuelMode.S;
            }
        }
        
        ProductCodeEnum productCode  = null;
        if (i_productCode != null) {
            if (i_productCode.equals("SP")) {
                productCode  = ProductCodeEnum.SP;
            }
            else if (i_productCode.equals("GG")) {
                productCode  = ProductCodeEnum.GG;
            }
            else if (i_productCode.equals("BS")) {
                productCode  = ProductCodeEnum.BS;
            }
            else if (i_productCode.equals("BD")) {
                productCode  = ProductCodeEnum.BD;
            }
            else if (i_productCode.equals("MT")) {
                productCode  = ProductCodeEnum.MT;
            }
            else if (i_productCode.equals("GP")) {
                productCode  = ProductCodeEnum.GP;
            }
        }
        
        Integer quantity  = AmountConverter.toMobile3(i_quantity);
        Integer unitPrice = AmountConverter.toMobile3(i_unitPrice);

        System.out.println("callSettleByRM");
        System.out.println("shopTransactionId: " + shopTransactionId);
        System.out.println("shopLogin: " + shopLogin);
        System.out.println("uicCode: " + uicCode);
        System.out.println("amount: " + amount);
        System.out.println("acquirerId: " + acquirerId);
        System.out.println("groupAcquirer: " + groupAcquirer);
        System.out.println("encodedSecretKey: " + encodedSecretKey);
        System.out.println("token: " + token);
        System.out.println("authorizationCode: " + authorizationCode);
        System.out.println("bankTransactionId: " + bankTransactionId);
        System.out.println("refuelMode: " + refuelMode);
        System.out.println("productCode: " + productCode);
        System.out.println("quantity: " + i_quantity);
        System.out.println("unitPrice: " + i_unitPrice);
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("shopTransactionID", i_shopTransactionID));
        inputParameters.add(new Pair<String, String>("shopLogin", shopLogin));
        inputParameters.add(new Pair<String, String>("amount", String.valueOf(i_amount)));
        inputParameters.add(new Pair<String, String>("uicCode", uicCode));
        inputParameters.add(new Pair<String, String>("acquirerId", acquirerId));
        inputParameters.add(new Pair<String, String>("groupAcquirer", groupAcquirer));
        inputParameters.add(new Pair<String, String>("encodedSecretKey", encodedSecretKey));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        String simulationCallSettleError = null;
        boolean simulationCallSettleKO = false;
        
        try {
            
            if (simulationActive) {
            
                simulationCallSettleError = parametersService.getParamValueNoCache(GPService.PARAM_SIMULATION_CALLSETTLE_ERROR);
                simulationCallSettleKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(GPService.PARAM_SIMULATION_CALLSETTLE_KO));
                
                if (simulationCallSettleError != null && simulationCallSettleError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }
    
                if (simulationCallSettleKO) {
                    gestPayData = new GestPayData();
                    gestPayData.setErrorCode("9999");
                    gestPayData.setErrorDescription("Simulazione di KO");
                    gestPayData.setTransactionResult("KO");
                    gestPayData.setShopTransactionID(shopTransactionId);
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCurrency(uicCode);
                    
                    GestPayS2S gps2s = new GestPayS2S();
                    gps2s.setAlertCode(null);
                    gps2s.setAlertDescription(null);
                    gps2s.setAuthorizationCode(null);
                    gps2s.setBankTransactionID(null);
                    gps2s.setBuyer(null);
                    gps2s.setCompany(null);
                    gps2s.setCountry(null);
                    gps2s.setCurrency("242");
                    gps2s.setCustomInfo(null);
                    gps2s.setErrorCode("9999");
                    gps2s.setErrorDescription("Simulazione di KO");
                    gps2s.setShopTransactionID(i_shopTransactionID);
                    gps2s.setTransactionKey(null);
                    gps2s.setTransactionResult(TransResult.KO);
                    gps2s.setTransactionState(null);
                    gps2s.setTransactionType(TransType.SETTLE);
                    gps2s.setVbV(null);
                    
                    gestPayData.setGps2s(gps2s);
                    
    
                    return gestPayData;
                }
            }
            
            proxy.setHttp();

            if ( acquirerId.equals("BANCASELLA")) {
                
                WSs2S service1 = new WSs2S(this.url);
                WSs2SSoap port1 = service1.getWSs2SSoap();
    
                CallSettleS2SResult eresult = port1.callSettleS2S(shopLogin, uicCode, amount, shopTransactionId, null);
    
                GestPayS2S gps2s;
                Object content = ((List) eresult.getContent()).get(0);
                JAXBContext context;
                try {
                    context = JAXBContext.newInstance(GestPayS2S.class);
                    Unmarshaller um = context.createUnmarshaller();
                    gps2s = (GestPayS2S) um.unmarshal((Node) content);
                    gestPayData = new GestPayData();
                    gestPayData.setGps2s(gps2s);
    
                    System.out.println("TransactionType " + gps2s.getTransactionType());
                    if (gps2s.getTransactionType() != null) {
                        gestPayData.setTransactionType(gps2s.getTransactionType().value());
                    }
                    System.out.println("TransactionResult " + gps2s.getTransactionResult());
                    if (gps2s.getTransactionResult() != null) {
                        gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                    }
                    gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                    gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                    gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                    gestPayData.setCurrency(gps2s.getCurrency());
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCountry(gps2s.getCountry());
    
                    if (gps2s.getCustomInfo() != null) {
                        gestPayData.setCustomInfo(gps2s.getCustomInfo().toString());
                    }
                    if (gps2s.getBuyer() != null) {
                        gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                        gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                    }
                    gestPayData.setErrorCode(gps2s.getErrorCode());
                    gestPayData.setErrorDescription(gps2s.getErrorDescription());
                    gestPayData.setAlertCode(gps2s.getAlertCode());
                    gestPayData.setAlertDescription(gps2s.getAlertDescription());
                    if (gps2s.getVbV() != null) {
                        gestPayData.setVbVRisp(gps2s.getVbV().getVbVRisp());
                        gestPayData.setVbVBuyer(gps2s.getVbV().getVbVBuyer());
                        gestPayData.setVbVFlag(gps2s.getVbV().getVbVFlag());
                    }
                }
                catch (JAXBException e) {
                    e.printStackTrace();
                    this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callSettleByRM", i_shopTransactionID, null, "Error parsing response: " + e.getMessage());
                    proxy.unsetHttp();
                    return null;
                }
            }
            else {
                
                if ( acquirerId.equals("CARTASI")) {
                    
                    PaymentClientImplementation paymentClientImplementation = new PaymentClientImplementation();
                    
                    GestPayS2S gps2s = paymentClientImplementation.callSettleS2S(shopLogin, uicCode, amount, shopTransactionId, null, groupAcquirer, encodedSecretKey);
                    
                    proxy.unsetHttp();
                    
                    System.out.println("OK");
                    
                    gestPayData = new GestPayData();
                    gestPayData.setGps2s(gps2s);
                    gestPayData.setTransactionType(gps2s.getTransactionType().value());
                    gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                    gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                    gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                    gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                    gestPayData.setCurrency(gps2s.getCurrency());
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCountry(gps2s.getCountry());

                    if (gps2s.getCustomInfo() != null) {
                        gestPayData.setCustomInfo(gps2s.getCustomInfo().toString());
                    }
                    if (gps2s.getBuyer() != null) {
                        gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                        gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                    }
                    gestPayData.setErrorCode(gps2s.getErrorCode());
                    gestPayData.setErrorDescription(gps2s.getErrorDescription());
                    gestPayData.setAlertCode(gps2s.getAlertCode());
                    gestPayData.setAlertDescription(gps2s.getAlertDescription());
                    if (gps2s.getVbV() != null) {
                        gestPayData.setVbVRisp(gps2s.getVbV().getVbVRisp());
                        gestPayData.setVbVBuyer(gps2s.getVbV().getVbVBuyer());
                        gestPayData.setVbVFlag(gps2s.getVbV().getVbVFlag());
                    }
                    System.out.println("gestPayData: " + gestPayData);
                }
                else {
                    
                    if ( acquirerId.equals("MULTICARD")) {
                        
                        Date now = new Date();
                        
                        System.out.println("Movimentazione con Multicard");
                        
                        //String operationID           = "000000" + new IdGenerator().generateId(16).substring(0, 26); //32);
                        String operationID           = new IdGenerator().generateId(16).substring(0, 32);
                        
                        String mcCardDpan            = token;
                        String messageReasonCode     = null;
                        String retrievalRefNumber    = bankTransactionId;
                        String authCode              = authorizationCode;
                        String currencyCode          = uicCode;
                        Integer integerAmount        = AmountConverter.toMobile(i_amount);
                        PartnerType partnerType      = PartnerType.EM;
                        Long requestTimestamp        = now.getTime();
                        
                        if (integerAmount == 0) {
                            messageReasonCode = "4000";
                        }
                        
                        ExecuteCaptureResult executeCaptureResult = paymentService.executeCapture(
                                mcCardDpan,
                                messageReasonCode,
                                retrievalRefNumber,
                                authCode,
                                currencyCode,
                                refuelMode,
                                operationID,
                                integerAmount,
                                productCode,
                                quantity,
                                unitPrice,
                                partnerType,
                                requestTimestamp);
                        
                        proxy.unsetHttp();
                        
                        if (executeCaptureResult == null) {
                            
                            String response = transactionService.persistTransactionOperation(operationID, shopTransactionId, "null", "", "ERROR", "", "MOV",
                                    operationID, requestTimestamp, null);
                            
                            // Gestire esito NULL
                            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callSettleByRM", i_shopTransactionID, null, "Error executeCaptureResult null");
                            proxy.unsetHttp();
                            return null;
                        }
                        
                        String response = transactionService.persistTransactionOperation(operationID, shopTransactionId, executeCaptureResult.getResult().getCode().getValue(),
                                executeCaptureResult.getResult().getMessage(), executeCaptureResult.getResult().getStatus().value(), executeCaptureResult.getResult().getTransactionId(),
                                "MOV", operationID, requestTimestamp, integerAmount);
                        
                        if (!executeCaptureResult.getResult().getStatus().value().equals(EnumStatusType.SUCCESS.value())) {
                            
                            // Gestire stato != SUCCESS
                            CodeEnum code  = executeCaptureResult.getResult().getCode();
                            String message = executeCaptureResult.getResult().getMessage();
                            
                            // Errore applicativo nella chiamata al servizio di movimentazione credito con multicard
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", i_shopTransactionID, null, "executeCaptureResult - code: " + code.getValue() + ", message: " + message);
                            
                            // Gestione errori in movimentazione
                            // 00000   Nessun errore
                            // 00001   Parametri in input errati
                            // 00106   Partner Type non valido
                            // 00109   PAN non trovato
                            // 00116   Autorizzazione non trovata
                            // 00118   Operazione in timeout
                            // 00119   Operazione in corso
                            // 01***   Errori restituiti dall�autorizzativo
                            // 01014   Operazione di pagamento fallita 
                            // 99001   Generic error

                            
                            // Mappatura codici e descrizioni errori
                            String errorCode = code.getValue();
                            String errorDescription = message;
                            
                            System.out.println("La transazione " + shopTransactionId + " e' stata rifiutata; descrizione errore: " + errorDescription + " ("
                                    + errorCode + ")");

                            // Errori che devono essere riprocessati tramite riconciliazione
                            if (errorCode.equals(CodeEnum.TIMEOUT_OPERATION.getValue()) ||
                                errorCode.equals(CodeEnum.OPERATION_NOT_COMPLETED .getValue()) ||
                                errorCode.equals(CodeEnum.GENERIC_ERROR.getValue())) {
                                
                                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callSettleByRM", i_shopTransactionID, null, "Error executeCaptureResult: " + errorCode + " -> transazione da riconciliare");
                                proxy.unsetHttp();
                                return null;
                            }
                            
                            if (errorDescription != null && errorDescription.length() > 255) {
                                errorDescription = errorDescription.substring(0, 255);
                            }

                            System.out.println("KO");
                            
                            gestPayData = new GestPayData();
                            gestPayData.setErrorCode(errorCode);
                            gestPayData.setErrorDescription(errorDescription);
                            gestPayData.setTransactionResult("KO");
                            gestPayData.setShopTransactionID(shopTransactionId);
                            gestPayData.setAmount(String.valueOf(i_amount));
                            gestPayData.setCurrency(uicCode);
                            gestPayData.setTransactionType("MOV");
                            
                            GestPayS2S gps2s = new GestPayS2S();

                            gps2s.setTransactionType(TransType.SETTLE);
                            gps2s.setTransactionResult(TransResult.KO);
                            gps2s.setShopTransactionID(shopTransactionId);
                            gps2s.setBankTransactionID("");
                            gps2s.setAuthorizationCode("");
                            gps2s.setCurrency(uicCode);
                            gps2s.setCountry("");
                            gps2s.setCustomInfo("");
                            gps2s.setBuyer(new BuyerType());
                            gps2s.setErrorCode(errorCode);
                            gps2s.setErrorDescription(errorDescription);
                            gps2s.setAlertCode("");
                            gps2s.setAlertDescription("");
                            gps2s.setVbV(new VbVType());

                            gestPayData.setGps2s(gps2s);
                        }
                        else {
                        
                            System.out.println("OK");
                            
                            gestPayData = new GestPayData();
                            gestPayData.setErrorCode("0");
                            gestPayData.setErrorDescription("Movimentazione con Multicard");
                            gestPayData.setTransactionResult("OK");
                            gestPayData.setShopTransactionID(shopTransactionId);
                            gestPayData.setAmount(String.valueOf(i_amount));
                            gestPayData.setCurrency(uicCode);
                            gestPayData.setTransactionType("MOV");
    
                            GestPayS2S gps2s = new GestPayS2S();
                            gps2s.setAlertCode(null);
                            gps2s.setAlertDescription(null);
                            gps2s.setAuthorizationCode(null);
                            gps2s.setBankTransactionID(null);
                            gps2s.setBuyer(null);
                            gps2s.setCompany(null);
                            gps2s.setCountry(null);
                            gps2s.setCurrency(uicCode);
                            gps2s.setCustomInfo(null);
                            gps2s.setErrorCode("0");
                            gps2s.setErrorDescription("Movimentazione con Multicard");
                            gps2s.setShopTransactionID(i_shopTransactionID);
                            gps2s.setTransactionKey(null);
                            gps2s.setTransactionResult(TransResult.OK);
                            gps2s.setTransactionState(null);
                            gps2s.setTransactionType(TransType.SETTLE);
                            gps2s.setVbV(null);
                            
                            gestPayData.setGps2s(gps2s);
                        }
                        
                    }
                    else {
                        
                        // TODO - Gestire l'errore
                    }
                }
            }
            
            if (simulationActive) {
                if (simulationCallSettleError != null && simulationCallSettleError.equalsIgnoreCase("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }
        }

        catch (javax.xml.ws.soap.SOAPFaultException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callSettleByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (javax.xml.ws.ProtocolException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callSettleByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (javax.xml.ws.WebServiceException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callSettleByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (java.lang.SecurityException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callSettleByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callSettleByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }

        //		paymentService.updateStatus(gsresponse.getStatusCode(), gsresponse.getMessageCode(), i_shopTransactionID);

        //gsresponse.getGPS2S().setTransactionType(TransType.SETTLE);

        proxy.unsetHttp();

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("TransactionType", "SETTLE"));
        outputParameters.add(new Pair<String, String>("TransactionResult", gestPayData.getTransactionResult().toString()));
        outputParameters.add(new Pair<String, String>("ShopTransactionID", gestPayData.getShopTransactionID()));
        outputParameters.add(new Pair<String, String>("BankTransactionID", gestPayData.getBankTransactionID()));
        outputParameters.add(new Pair<String, String>("AuthorizationCode", gestPayData.getAuthorizationCode()));
        outputParameters.add(new Pair<String, String>("Currency", gestPayData.getCurrency()));
        outputParameters.add(new Pair<String, String>("Amount", gestPayData.getAmount()));
        outputParameters.add(new Pair<String, String>("Country", gestPayData.getCountry()));
        outputParameters.add(new Pair<String, String>("CustomInfo", gestPayData.getCustomInfo()));
        outputParameters.add(new Pair<String, String>("BuyerName", gestPayData.getBuyerName()));
        outputParameters.add(new Pair<String, String>("BuyerEmail", gestPayData.getBuyerEmail()));
        outputParameters.add(new Pair<String, String>("ErrorCode", gestPayData.getErrorCode()));
        outputParameters.add(new Pair<String, String>("ErrorDescription", gestPayData.getErrorDescription()));
        outputParameters.add(new Pair<String, String>("AlertCode", gestPayData.getAlertCode()));
        outputParameters.add(new Pair<String, String>("AlertDescription", gestPayData.getAlertDescription()));
        outputParameters.add(new Pair<String, String>("VbVRisp", gestPayData.getVbVRisp()));
        outputParameters.add(new Pair<String, String>("VbVBuyer", gestPayData.getVbVBuyer()));
        outputParameters.add(new Pair<String, String>("VbVFlag", gestPayData.getVbVFlag()));
        //	outputParameters.add(new Pair<String,String>("Token", gestPayData.getToken()));
        //	outputParameters.add(new Pair<String,String>("TokenExpiryMonth", gestPayData.getTokenExpiryMonth()));
        //	outputParameters.add(new Pair<String,String>("TokenExpiryYear", gestPayData.getTokenExpiryYear()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "callSettleByRM", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return gestPayData;

    }


    public ExtendedGestPayData callPagamAndSettle(Double i_amount, String i_shopTransactionID, String i_shopLogin, String i_valuta, String i_acquirerId,
            String i_token, String i_refuelMode, String i_productCode, Double i_quantity, Double i_unitPrice, String i_shopCode, String i_paymentCryptogram) {

        //String shopLogin = this.shopLogin;

        String shopLogin = i_shopLogin;

        //String uicCode = this.uic;
        String uicCode = i_valuta;
        String amount = Double.toString(i_amount);
        String shopTransactionId = i_shopTransactionID;
        String acquirerId = i_acquirerId;
        String token = i_token;
        
        PaymentRefuelMode refuelMode = null;
        if (i_refuelMode.equalsIgnoreCase("self")) {
            refuelMode = PaymentRefuelMode.I;
        }
        else if (i_refuelMode.equalsIgnoreCase("Servito")) {
            refuelMode = PaymentRefuelMode.F;
        }
        else if (i_refuelMode.equalsIgnoreCase("Fai_da_te")) {
            refuelMode = PaymentRefuelMode.S;
        }
        
        ProductCodeEnum productCode  = ProductCodeEnum.getCommand(i_productCode);
        
        Integer quantity  = AmountConverter.toMobile3(i_quantity);
        Integer unitPrice = AmountConverter.toMobile3(i_unitPrice);
        String shopCode = i_shopCode;
        String paymentCryptogram = i_paymentCryptogram;

        System.out.println("callPagamAndSettleByRM");
        System.out.println("shopTransactionId: " + shopTransactionId);
        System.out.println("shopLogin: " + shopLogin);
        System.out.println("uicCode: " + uicCode);
        System.out.println("amount: " + amount);
        System.out.println("acquirerId: " + acquirerId);
        System.out.println("token: " + token);
        System.out.println("refuelMode: " + refuelMode);
        System.out.println("productCode: " + productCode);
        System.out.println("quantity: " + i_quantity);
        System.out.println("unitPrice: " + i_unitPrice);
        System.out.println("paymentCryptogram: " + paymentCryptogram);
        
        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("shopTransactionID", i_shopTransactionID));
        inputParameters.add(new Pair<String, String>("shopLogin", shopLogin));
        inputParameters.add(new Pair<String, String>("amount", String.valueOf(i_amount)));
        inputParameters.add(new Pair<String, String>("uicCode", uicCode));
        inputParameters.add(new Pair<String, String>("acquirerId", acquirerId));
        inputParameters.add(new Pair<String, String>("token", token));
        inputParameters.add(new Pair<String, String>("refuelMode", i_refuelMode));
        inputParameters.add(new Pair<String, String>("productCode", i_productCode));
        inputParameters.add(new Pair<String, String>("quantity", i_quantity.toString()));
        inputParameters.add(new Pair<String, String>("unitPrice", i_unitPrice.toString()));
        inputParameters.add(new Pair<String, String>("paymentCryptogram", paymentCryptogram));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        String simulationCallSettleError = null;
        boolean simulationCallSettleKO = false;
        
        try {
            
            if (simulationActive) {
            
                simulationCallSettleError = parametersService.getParamValueNoCache(GPService.PARAM_SIMULATION_CALLSETTLE_ERROR);
                simulationCallSettleKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(GPService.PARAM_SIMULATION_CALLSETTLE_KO));
                
                if (simulationCallSettleError != null && simulationCallSettleError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }
    
                if (simulationCallSettleKO) {
                    extendedGestPayData = new ExtendedGestPayData();
                    extendedGestPayData.setErrorCode("9999");
                    extendedGestPayData.setErrorDescription("Simulazione di KO");
                    extendedGestPayData.setTransactionResult("KO");
                    extendedGestPayData.setShopTransactionID(shopTransactionId);
                    extendedGestPayData.setAmount(String.valueOf(i_amount));
                    extendedGestPayData.setCurrency(uicCode);
                    
                    GestPayS2S gps2s = new GestPayS2S();
                    gps2s.setAlertCode(null);
                    gps2s.setAlertDescription(null);
                    gps2s.setAuthorizationCode(null);
                    gps2s.setBankTransactionID(null);
                    gps2s.setBuyer(null);
                    gps2s.setCompany(null);
                    gps2s.setCountry(null);
                    gps2s.setCurrency("242");
                    gps2s.setCustomInfo(null);
                    gps2s.setErrorCode("9999");
                    gps2s.setErrorDescription("Simulazione di KO");
                    gps2s.setShopTransactionID(i_shopTransactionID);
                    gps2s.setTransactionKey(null);
                    gps2s.setTransactionResult(TransResult.KO);
                    gps2s.setTransactionState(null);
                    gps2s.setTransactionType(TransType.SETTLE);
                    gps2s.setVbV(null);
                    
                    extendedGestPayData.setGps2s(gps2s);
                    
    
                    return extendedGestPayData;
                }
            }
            
            proxy.setHttp();

            if ( acquirerId.equals("MULTICARD")) {
                
                Date now = new Date();
                
                System.out.println("Movimentazione con Multicard");
                
                //String operationID           = "000000" + new IdGenerator().generateId(16).substring(0, 26); //32);
                String operationID           = new IdGenerator().generateId(16).substring(0, 32);
                
                String mcCardDpan             = token;
                String currencyCode          = uicCode;
                Integer integerAmount        = AmountConverter.toMobile(i_amount);
                PartnerType partnerType      = PartnerType.EM;
                Long requestTimestamp        = now.getTime();
                
                ExecutePaymentResult executePaymentResult = paymentService.executePayment(
                        operationID,
                        integerAmount,
                        productCode,
                        quantity,
                        unitPrice,
                        paymentCryptogram,
                        currencyCode,
                        mcCardDpan,
                        refuelMode,
                        shopCode,
                        partnerType,
                        requestTimestamp);
                
                proxy.unsetHttp();
                
                if (executePaymentResult == null) {
                    
                    String response = transactionService.persistTransactionOperation(operationID, shopTransactionId, "null", "", "ERROR", "", "MOV",
                            operationID, requestTimestamp, null);
                    
                    // Gestire esito NULL
                    this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callSettleByRM", i_shopTransactionID, null, "Error executePaymentResult null");
                    proxy.unsetHttp();
                    return null;
                }
                
                String response = transactionService.persistTransactionOperation(operationID, shopTransactionId, executePaymentResult.getResult().getCode().getValue(),
                        executePaymentResult.getResult().getMessage(), executePaymentResult.getResult().getStatus().value(), executePaymentResult.getResult().getTransactionId(),
                        "MOV", operationID, requestTimestamp, integerAmount);
                
                if (!executePaymentResult.getResult().getStatus().value().equals(EnumStatusType.SUCCESS.value())) {
                    
                    // Gestire stato != SUCCESS
                    CodeEnum code  = executePaymentResult.getResult().getCode();
                    String message = executePaymentResult.getResult().getMessage();
                    
                    // Errore applicativo nella chiamata al servizio di movimentazione credito con multicard
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", i_shopTransactionID, null, "executePaymentResult - code: " + code.getValue() + ", message: " + message);
                    
                    // Gestione errori in movimentazione
                    // 00000   Nessun errore
                    // 00001   Parametri in input errati
                    // 00002   Operazione non valida 
                    // 00106   Partner Type non valido
                    // 00109   PAN non trovato
                    // 00114   Operazione di pagamento eseguita con altro dispositivo 
                    // 00115   Parametri dell�authCryptogram non validi. Mancata corrispondenza con i parametri della request 
                    // 00118   Operazione in timeout
                    // 00119   Operazione in corso
                    // 01001   PIN non verificato
                    // 01002   Carta scaduta 
                    // 01003   Operazione di pagamento fallita per problemi relativi al merchant
                    // 01004   Operazione di pagamento fallita per problemi di sicurezza
                    // 01005   Operazione di pagamento fallita per problemi sul terminale
                    // 01006   Operazione di pagamento fallita per problemi relativi al customer
                    // 01007   Disponibilit� insufficiente
                    // 01008   Operazione fallita per AuthCode non trovato
                    // 01009   Importo da movimentare maggiore dell�importo autorizzato
                    // 01010   Importo da stornare maggiore dell�importo movimentato
                    // 01011   Superato il limite di prelievo 
                    // 01012   Storno fallito
                    // 01013   Operazione fallito per importo non valido
                    // 01014   Operazione di pagamento fallita 
                    // 99001   Generic error

                    
                    // Mappatura codici e descrizioni errori
                    String errorCode = code.getValue();
                    String errorDescription = message;
                    
                    System.out.println("La transazione " + shopTransactionId + " e' stata rifiutata; descrizione errore: " + errorDescription + " ("
                            + errorCode + ")");

                    // Errori che devono essere riprocessati tramite riconciliazione
                    if (errorCode.equals(CodeEnum.TIMEOUT_OPERATION.getValue()) ||
                        errorCode.equals(CodeEnum.OPERATION_NOT_COMPLETED .getValue()) ||
                        errorCode.equals(CodeEnum.GENERIC_ERROR.getValue())) {
                        
                        this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamAndSettleByRM", i_shopTransactionID, null, "Error executePaymentResult: " + errorCode + " -> transazione da riconciliare");
                        proxy.unsetHttp();
                        return null;
                    }
                    
                    if (errorDescription != null && errorDescription.length() > 255) {
                        errorDescription = errorDescription.substring(0, 255);
                    }

                    System.out.println("KO");
                    
                    extendedGestPayData = new ExtendedGestPayData();
                    extendedGestPayData.setErrorCode(errorCode);
                    extendedGestPayData.setErrorDescription(errorDescription);
                    extendedGestPayData.setTransactionResult("KO");
                    extendedGestPayData.setShopTransactionID(shopTransactionId);
                    extendedGestPayData.setAmount(String.valueOf(i_amount));
                    extendedGestPayData.setCurrency(uicCode);
                    extendedGestPayData.setTransactionType("MOV");
                    
                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setTransactionType(TransType.SETTLE);
                    gps2s.setTransactionResult(TransResult.KO);
                    gps2s.setShopTransactionID(shopTransactionId);
                    gps2s.setBankTransactionID("");
                    gps2s.setAuthorizationCode("");
                    gps2s.setCurrency(uicCode);
                    gps2s.setCountry("");
                    gps2s.setCustomInfo("");
                    gps2s.setBuyer(new BuyerType());
                    gps2s.setErrorCode(errorCode);
                    gps2s.setErrorDescription(errorDescription);
                    gps2s.setAlertCode("");
                    gps2s.setAlertDescription("");
                    gps2s.setVbV(new VbVType());

                    extendedGestPayData.setGps2s(gps2s);
                }
                else {
                
                    System.out.println("OK");
                    
                    String retrievalRefNumber = executePaymentResult.getRetrievalRefNumber();
                    String authCode           = executePaymentResult.getAuthCode();
                    
                    extendedGestPayData = new ExtendedGestPayData();
                    extendedGestPayData.setErrorCode("0");
                    extendedGestPayData.setErrorDescription("Movimentazione con Multicard");
                    extendedGestPayData.setTransactionResult("OK");
                    extendedGestPayData.setShopTransactionID(shopTransactionId);
                    extendedGestPayData.setAmount(String.valueOf(i_amount));
                    extendedGestPayData.setCurrency(uicCode);
                    extendedGestPayData.setTransactionType("MOV");
                    extendedGestPayData.setAuthorizationCode(retrievalRefNumber);
                    extendedGestPayData.setBankTransactionID(authCode);

                    GestPayS2S gps2s = new GestPayS2S();
                    gps2s.setAlertCode(null);
                    gps2s.setAlertDescription(null);
                    gps2s.setAuthorizationCode(retrievalRefNumber);
                    gps2s.setBankTransactionID(authCode);
                    gps2s.setBuyer(null);
                    gps2s.setCompany(null);
                    gps2s.setCountry(null);
                    gps2s.setCurrency(uicCode);
                    gps2s.setCustomInfo(null);
                    gps2s.setErrorCode("0");
                    gps2s.setErrorDescription("Movimentazione con Multicard");
                    gps2s.setShopTransactionID(i_shopTransactionID);
                    gps2s.setTransactionKey(null);
                    gps2s.setTransactionResult(TransResult.OK);
                    gps2s.setTransactionState(null);
                    gps2s.setTransactionType(TransType.SETTLE);
                    gps2s.setVbV(null);
                    
                    extendedGestPayData.setGps2s(gps2s);
                    
                    for (KeyValueInfo keyValueInfo : executePaymentResult.getReceiptElementList()) {
                        KeyValueData keyValueData = new KeyValueData();
                        keyValueData.setKey(keyValueInfo.getKey());
                        keyValueData.setValue(keyValueInfo.getValue());
                        extendedGestPayData.getReceiptElementList().add(keyValueData);
                    }
                }
                
            }
            else {
                
                // TODO - Gestire l'errore
                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamAndSettleByRM", i_shopTransactionID, null, "Error: acquirer " + acquirerId + " non valido");
                proxy.unsetHttp();
                return null;
            }
            
            if (simulationActive) {
                if (simulationCallSettleError != null && simulationCallSettleError.equalsIgnoreCase("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }
        }

        catch (javax.xml.ws.soap.SOAPFaultException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamAndSettleByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (javax.xml.ws.ProtocolException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamAndSettleByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (javax.xml.ws.WebServiceException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamAndSettleByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (java.lang.SecurityException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamAndSettleByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callPagamAndSettleByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }

        //      paymentService.updateStatus(gsresponse.getStatusCode(), gsresponse.getMessageCode(), i_shopTransactionID);

        //gsresponse.getGPS2S().setTransactionType(TransType.SETTLE);

        proxy.unsetHttp();

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("TransactionType", "SETTLE"));
        outputParameters.add(new Pair<String, String>("TransactionResult", extendedGestPayData.getTransactionResult().toString()));
        outputParameters.add(new Pair<String, String>("ShopTransactionID", extendedGestPayData.getShopTransactionID()));
        outputParameters.add(new Pair<String, String>("BankTransactionID", extendedGestPayData.getBankTransactionID()));
        outputParameters.add(new Pair<String, String>("AuthorizationCode", extendedGestPayData.getAuthorizationCode()));
        outputParameters.add(new Pair<String, String>("Currency", extendedGestPayData.getCurrency()));
        outputParameters.add(new Pair<String, String>("Amount", extendedGestPayData.getAmount()));
        outputParameters.add(new Pair<String, String>("Country", extendedGestPayData.getCountry()));
        outputParameters.add(new Pair<String, String>("CustomInfo", extendedGestPayData.getCustomInfo()));
        outputParameters.add(new Pair<String, String>("BuyerName", extendedGestPayData.getBuyerName()));
        outputParameters.add(new Pair<String, String>("BuyerEmail", extendedGestPayData.getBuyerEmail()));
        outputParameters.add(new Pair<String, String>("ErrorCode", extendedGestPayData.getErrorCode()));
        outputParameters.add(new Pair<String, String>("ErrorDescription", extendedGestPayData.getErrorDescription()));
        outputParameters.add(new Pair<String, String>("AlertCode", extendedGestPayData.getAlertCode()));
        outputParameters.add(new Pair<String, String>("AlertDescription", extendedGestPayData.getAlertDescription()));
        outputParameters.add(new Pair<String, String>("VbVRisp", extendedGestPayData.getVbVRisp()));
        outputParameters.add(new Pair<String, String>("VbVBuyer", extendedGestPayData.getVbVBuyer()));
        outputParameters.add(new Pair<String, String>("VbVFlag", extendedGestPayData.getVbVFlag()));
        //  outputParameters.add(new Pair<String,String>("Token", extendedGestPayData.getToken()));
        //  outputParameters.add(new Pair<String,String>("TokenExpiryMonth", extendedGestPayData.getTokenExpiryMonth()));
        //  outputParameters.add(new Pair<String,String>("TokenExpiryYear", extendedGestPayData.getTokenExpiryYear()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "callPagamAndSettleByRM", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return extendedGestPayData;

    }
    

    //@WebMethod(operationName = "callRefund")
    public GestPayData callRefund(Double i_amount, String i_shopTransactionID, String i_shopLogin, String i_valuta, String i_token, String i_authorizationCode, String i_bankTransactionID, String i_shopCode, String i_acquirerId, String i_groupAcquirer, String i_encodedSecretKey) {

        String shopLogin = i_shopLogin;
        String uicCode = i_valuta;
        String amount = Double.toString(i_amount);
        String shopTransactionId = i_shopTransactionID;
        String token = i_token;
        String authorizationCode = i_authorizationCode;
        String bankTransactionId = i_bankTransactionID;
        String acquirerId = i_acquirerId;
        String groupAcquirer = i_groupAcquirer;
        String encodedSecretKey = i_encodedSecretKey;
        String shopCode = i_shopCode;

        System.out.println("callRefundByRM");
        System.out.println("shopTransactionId: " + shopTransactionId);
        System.out.println("shopLogin: " + shopLogin);
        System.out.println("uicCode: " + uicCode);
        System.out.println("amount: " + amount);
        System.out.println("acquirerId: " + acquirerId);
        System.out.println("groupAcquirer: " + groupAcquirer);
        System.out.println("encodedSecretKey: " + encodedSecretKey);
        System.out.println("token: " + token);
        System.out.println("authorizationCode: " + authorizationCode);
        System.out.println("bankTransactionId: " + bankTransactionId);
        System.out.println("shopCode: " + shopCode);
        
        System.out.println("SL: " + shopLogin + " _uicode: " + uicCode + " _amount: " + amount + " _stid: " + shopTransactionId + " _acquirerId: " + acquirerId + " _groupAcquirer: " + groupAcquirer + " _encodedSecretKey: " + encodedSecretKey);
        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        String simulationCallRefundError = null;
        boolean simulationCallRefundKO = false;
        
        try {

            if (simulationActive) {
            
                simulationCallRefundError = parametersService.getParamValueNoCache(GPService.PARAM_SIMULATION_CALLREFUND_ERROR);
                simulationCallRefundKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(GPService.PARAM_SIMULATION_CALLREFUND_KO));
                
                if (simulationCallRefundError != null && simulationCallRefundError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }
    
                if (simulationCallRefundKO) {
                    gestPayData = new GestPayData();
                    gestPayData.setErrorCode("9999");
                    gestPayData.setErrorDescription("Simulazione di KO");
                    gestPayData.setTransactionResult("KO");
                    gestPayData.setShopTransactionID(shopTransactionId);
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCurrency(uicCode);
    
                    GestPayS2S gps2s = new GestPayS2S();
                    gps2s.setAlertCode(null);
                    gps2s.setAlertDescription(null);
                    gps2s.setAuthorizationCode(null);
                    gps2s.setBankTransactionID(i_bankTransactionID);
                    gps2s.setBuyer(null);
                    gps2s.setCompany(null);
                    gps2s.setCountry(null);
                    gps2s.setCurrency(uicCode);
                    gps2s.setCustomInfo(null);
                    gps2s.setErrorCode("9999");
                    gps2s.setErrorDescription("Simulazione di KO");
                    gps2s.setShopTransactionID(i_shopTransactionID);
                    gps2s.setTransactionKey(null);
                    gps2s.setTransactionResult(TransResult.KO);
                    gps2s.setTransactionState(null);
                    gps2s.setTransactionType(TransType.REFUND);
                    gps2s.setVbV(null);
                    
                    gestPayData.setGps2s(gps2s);
                    
                    return gestPayData;
                }
            }
            
            proxy.setHttp();

            if ( acquirerId.equals("BANCASELLA")) {
                
                WSs2S service1 = new WSs2S(this.url);
                System.out.println("Create Web Service...");
                WSs2SSoap port1 = service1.getWSs2SSoap();
                System.out.println("Call Web Service Operation...");
                CallRefundS2SResult eresult = port1.callRefundS2S(shopLogin, uicCode, amount, shopTransactionId, bankTransactionId);
    
                GestPayS2S gps2s;
                Object content = ((List) eresult.getContent()).get(0);
                JAXBContext context;
                proxy.unsetHttp();
    
                try {
                    System.out.println("OK");
                    context = JAXBContext.newInstance(GestPayS2S.class);
                    Unmarshaller um = context.createUnmarshaller();
                    gps2s = (GestPayS2S) um.unmarshal((Node) content);
    
                    gestPayData = new GestPayData();
                    gestPayData.setGps2s(gps2s);
    
                    gestPayData.setTransactionType("REFUND");
                    gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                    gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                    gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                    gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                    gestPayData.setCurrency(gps2s.getCurrency());
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCountry(gps2s.getCountry());
    
                    if (gps2s.getCustomInfo() != null) {
                        gestPayData.setCustomInfo(gps2s.getCustomInfo().toString());
                    }
                    if (gps2s.getBuyer() != null) {
                        gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                        gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                    }
                    gestPayData.setErrorCode(gps2s.getErrorCode());
                    gestPayData.setErrorDescription(gps2s.getErrorDescription());
                    gestPayData.setAlertCode(gps2s.getAlertCode());
                    gestPayData.setAlertDescription(gps2s.getAlertDescription());
                    if (gps2s.getVbV() != null) {
                        gestPayData.setVbVRisp(gps2s.getVbV().getVbVRisp());
                        gestPayData.setVbVBuyer(gps2s.getVbV().getVbVBuyer());
                        gestPayData.setVbVFlag(gps2s.getVbV().getVbVFlag());
                    }
    
                }
                catch (JAXBException e) {
                    e.printStackTrace();
                    this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callRefundByRM", i_shopTransactionID, null, "Error parsing response: " + e.getMessage());
                    return null;
                }
            }
            else {
                
                if ( acquirerId.equals("CARTASI")) {
                    
                    PaymentClientImplementation paymentClientImplementation = new PaymentClientImplementation();
                    
                    GestPayS2S gps2s = paymentClientImplementation.callDeleteS2S(shopLogin, shopTransactionId, amount, uicCode, groupAcquirer, encodedSecretKey);
                    
                    proxy.unsetHttp();
                    
                    System.out.println("OK");
                    
                    gestPayData = new GestPayData();
                    gestPayData.setGps2s(gps2s);
                    gestPayData.setTransactionType(gps2s.getTransactionType().value());
                    gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                    gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                    gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                    gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                    gestPayData.setCurrency(gps2s.getCurrency());
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCountry(gps2s.getCountry());

                    if (gps2s.getCustomInfo() != null) {
                        gestPayData.setCustomInfo(gps2s.getCustomInfo().toString());
                    }
                    if (gps2s.getBuyer() != null) {
                        gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                        gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                    }
                    gestPayData.setErrorCode(gps2s.getErrorCode());
                    gestPayData.setErrorDescription(gps2s.getErrorDescription());
                    gestPayData.setAlertCode(gps2s.getAlertCode());
                    gestPayData.setAlertDescription(gps2s.getAlertDescription());
                    if (gps2s.getVbV() != null) {
                        gestPayData.setVbVRisp(gps2s.getVbV().getVbVRisp());
                        gestPayData.setVbVBuyer(gps2s.getVbV().getVbVBuyer());
                        gestPayData.setVbVFlag(gps2s.getVbV().getVbVFlag());
                    }
                    System.out.println("gestPayData: " + gestPayData);
                }
                else {
                    
                    if ( acquirerId.equals("MULTICARD")) {
                        
                        Date now = new Date();
                        
                        System.out.println("Storno con Multicard");
                        
                        //String operationID           = "000000" + new IdGenerator().generateId(16).substring(0, 26); //32);
                        String operationID           = new IdGenerator().generateId(16).substring(0, 32);
                        
                        String mcCardDpan            = token;
                        String messageReasonCode     = "4000";
                        String retrievalRefNumber    = authorizationCode;
                        String authCode              = bankTransactionId;
                        String currencyCode          = uicCode;
                        Integer integerAmount        = AmountConverter.toMobile(i_amount);
                        PartnerType partnerType      = PartnerType.EM;
                        Long requestTimestamp        = now.getTime();
                        
                        ExecuteReversalResult executeReversalResult = paymentService.executeReversal(
                                mcCardDpan,
                                messageReasonCode,
                                retrievalRefNumber,
                                authCode,
                                shopCode,
                                currencyCode,
                                operationID,
                                integerAmount,
                                partnerType,
                                requestTimestamp);
                        
                        proxy.unsetHttp();
                        
                        if (executeReversalResult == null) {
                            
                            String response = transactionService.persistTransactionOperation(operationID, shopTransactionId, "null", "", "ERROR", "", "STO",
                                    operationID, requestTimestamp, null);
                            
                            // Gestire esito NULL
                            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callRefundByRM", i_shopTransactionID, null, "Error executeReversalResult null");
                            proxy.unsetHttp();
                            return null;
                        }
                        
                        String response = transactionService.persistTransactionOperation(operationID, shopTransactionId, executeReversalResult.getResult().getCode().getValue(),
                                executeReversalResult.getResult().getMessage(), executeReversalResult.getResult().getStatus().value(), executeReversalResult.getResult().getTransactionId(),
                                "STO", operationID, requestTimestamp, integerAmount);
                        
                        if (!executeReversalResult.getResult().getStatus().value().equals(EnumStatusType.SUCCESS.value())) {
                            
                            // Gestire stato != SUCCESS
                            CodeEnum code  = executeReversalResult.getResult().getCode();
                            String message = executeReversalResult.getResult().getMessage();
                            
                            // Errore applicativo nella chiamata al servizio di movimentazione credito con multicard
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", i_shopTransactionID, null, "executeReversalResult - code: " + code.getValue() + ", message: " + message);
                            
                            // Gestione errori in movimentazione
                            // 00000   Nessun errore
                            // 00001   Parametri in input errati
                            // 00106   Partner Type non valido
                            // 00109   PAN non trovato
                            // 00117   Operazione di pagamento non travata per lo storno
                            // 00118   Operazione in timeout
                            // 00119   Operazione in corso
                            // 01***   Errori restituiti dall�autorizzativo
                            // 99001   Generic error
                            
                            // Mappatura codici e descrizioni errori
                            String errorCode = code.getValue();
                            String errorDescription = message;
                            
                            System.out.println("La transazione " + shopTransactionId + " e' stata rifiutata; descrizione errore: " + errorDescription + " ("
                                    + errorCode + ")");

                            // Errori che devono essere riprocessati tramite riconciliazione
                            if (errorCode.equals(CodeEnum.TIMEOUT_OPERATION.getValue()) ||
                                errorCode.equals(CodeEnum.OPERATION_NOT_COMPLETED .getValue()) ||
                                errorCode.equals(CodeEnum.GENERIC_ERROR.getValue())) {
                                
                                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callRefundByRM", i_shopTransactionID, null, "Error executeReversalResult: " + errorCode + " -> transazione da riconciliare");
                                proxy.unsetHttp();
                                return null;
                            }
                            
                            if (errorDescription != null && errorDescription.length() > 255) {
                                errorDescription = errorDescription.substring(0, 255);
                            }

                            System.out.println("KO");
                            
                            gestPayData = new GestPayData();
                            gestPayData.setErrorCode(errorCode);
                            gestPayData.setErrorDescription(errorDescription);
                            gestPayData.setTransactionResult("KO");
                            gestPayData.setShopTransactionID(shopTransactionId);
                            gestPayData.setAmount(String.valueOf(i_amount));
                            gestPayData.setCurrency(uicCode);
                            gestPayData.setTransactionType("STO");
                            
                            GestPayS2S gps2s = new GestPayS2S();

                            gps2s.setTransactionType(TransType.REFUND);
                            gps2s.setTransactionResult(TransResult.KO);
                            gps2s.setShopTransactionID(shopTransactionId);
                            gps2s.setBankTransactionID("");
                            gps2s.setAuthorizationCode("");
                            gps2s.setCurrency(uicCode);
                            gps2s.setCountry("");
                            gps2s.setCustomInfo("");
                            gps2s.setBuyer(new BuyerType());
                            gps2s.setErrorCode(errorCode);
                            gps2s.setErrorDescription(errorDescription);
                            gps2s.setAlertCode("");
                            gps2s.setAlertDescription("");
                            gps2s.setVbV(new VbVType());

                            gestPayData.setGps2s(gps2s);
                        }
                        else {
                        
                            System.out.println("OK");
                            
                            gestPayData = new GestPayData();
                            gestPayData.setErrorCode("0");
                            gestPayData.setErrorDescription("Storno con Multicard");
                            gestPayData.setTransactionResult("OK");
                            gestPayData.setShopTransactionID(shopTransactionId);
                            gestPayData.setAmount(String.valueOf(i_amount));
                            gestPayData.setCurrency(uicCode);
                            gestPayData.setTransactionType("STO");
    
                            GestPayS2S gps2s = new GestPayS2S();
                            gps2s.setAlertCode(null);
                            gps2s.setAlertDescription(null);
                            gps2s.setAuthorizationCode(null);
                            gps2s.setBankTransactionID(null);
                            gps2s.setBuyer(null);
                            gps2s.setCompany(null);
                            gps2s.setCountry(null);
                            gps2s.setCurrency(uicCode);
                            gps2s.setCustomInfo(null);
                            gps2s.setErrorCode("0");
                            gps2s.setErrorDescription("Storno con Multicard");
                            gps2s.setShopTransactionID(i_shopTransactionID);
                            gps2s.setTransactionKey(null);
                            gps2s.setTransactionResult(TransResult.OK);
                            gps2s.setTransactionState(null);
                            gps2s.setTransactionType(TransType.REFUND);
                            gps2s.setVbV(null);
                            
                            gestPayData.setGps2s(gps2s);
                        }
                    }
                    else {
                        
                        // TODO gestire l'errore
                    }
                }
            }
            
            if (simulationActive) {
                if (simulationCallRefundError != null && simulationCallRefundError.equalsIgnoreCase("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }
        }

        catch (javax.xml.ws.soap.SOAPFaultException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callRefundByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }
        catch (javax.xml.ws.ProtocolException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callRefundByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }
        catch (javax.xml.ws.WebServiceException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callRefundByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }
        catch (java.lang.SecurityException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callRefundByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callRefundByRM", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;

        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("TransactionType", "REFUND"));
        outputParameters.add(new Pair<String, String>("TransactionResult", gestPayData.getTransactionResult().toString()));
        outputParameters.add(new Pair<String, String>("ShopTransactionID", gestPayData.getShopTransactionID()));
        outputParameters.add(new Pair<String, String>("BankTransactionID", gestPayData.getBankTransactionID()));
        outputParameters.add(new Pair<String, String>("AuthorizationCode", gestPayData.getAuthorizationCode()));
        outputParameters.add(new Pair<String, String>("Currency", gestPayData.getCurrency()));
        outputParameters.add(new Pair<String, String>("Amount", gestPayData.getAmount()));
        outputParameters.add(new Pair<String, String>("Country", gestPayData.getCountry()));
        outputParameters.add(new Pair<String, String>("CustomInfo", gestPayData.getCustomInfo()));
        outputParameters.add(new Pair<String, String>("BuyerName", gestPayData.getBuyerName()));
        outputParameters.add(new Pair<String, String>("BuyerEmail", gestPayData.getBuyerEmail()));
        outputParameters.add(new Pair<String, String>("ErrorCode", gestPayData.getErrorCode()));
        outputParameters.add(new Pair<String, String>("ErrorDescription", gestPayData.getErrorDescription()));
        outputParameters.add(new Pair<String, String>("AlertCode", gestPayData.getAlertCode()));
        outputParameters.add(new Pair<String, String>("AlertDescription", gestPayData.getAlertDescription()));
        //		outputParameters.add(new Pair<String,String>("VbVRisp", gestPayData.getVbVRisp()));
        //		outputParameters.add(new Pair<String,String>("VbVBuyer", gestPayData.getVbVBuyer()));
        //		outputParameters.add(new Pair<String,String>("VbVFlag", gestPayData.getVbVFlag()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "callRefundByRM", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return gestPayData;
    }

    //@WebMethod(operationName = "deletePagam")
    public GestPayData deletePagam(Double i_amount, String i_shopTransactionID, String i_shopLogin, String i_valuta, String i_bankTransactionID, String i_operation, String i_acquirerId, String i_groupAcquirer, String i_encodedSecretKey) {

        String shopLogin = i_shopLogin;
        String uicCode = i_valuta;
        String amount = Double.toString(i_amount);
        String shopTransactionId = i_shopTransactionID;
        String bankTransactionID = i_bankTransactionID;
        String operation = i_operation;
        String acquirerId = i_acquirerId;
        String groupAcquirer = i_groupAcquirer;
        String encodedSecretKey = i_encodedSecretKey;

        System.out.println("callDeleteS2S");
        System.out.println("shopTransactionId: " + shopTransactionId);
        System.out.println("bankTransactionID: " + bankTransactionID);
        System.out.println("shopLogin: " + shopLogin);
        System.out.println("uicCode: " + uicCode);
        System.out.println("amount: " + amount);
        System.out.println("operation: " + operation);
        System.out.println("acquirerId: " + acquirerId);
        System.out.println("groupAcquirer: " + groupAcquirer);
        System.out.println("encodedSecretKey: " + encodedSecretKey);
        
        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        String simulationDeletePagamError = null;
        boolean simulationDeletePagamKO = false;
        
        try {
            
            if (simulationActive) {
                
                simulationDeletePagamError = parametersService.getParamValueNoCache(GPService.PARAM_SIMULATION_DELETEPAGAM_ERROR);
                simulationDeletePagamKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(GPService.PARAM_SIMULATION_DELETEPAGAM_KO));
                
                if (simulationDeletePagamError != null && simulationDeletePagamError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore");
                }
    
                if (simulationDeletePagamKO) {
                    gestPayData = new GestPayData();
                    gestPayData.setErrorCode("9999");
                    gestPayData.setErrorDescription("Simulazione di KO");
                    gestPayData.setTransactionResult("KO");
                    gestPayData.setShopTransactionID(i_shopTransactionID);
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCurrency(i_valuta);
    
                    GestPayS2S gps2s = new GestPayS2S();
                    gps2s.setAlertCode(null);
                    gps2s.setAlertDescription(null);
                    gps2s.setAuthorizationCode(null);
                    gps2s.setBankTransactionID(i_bankTransactionID);
                    gps2s.setBuyer(null);
                    gps2s.setCompany(null);
                    gps2s.setCountry(null);
                    gps2s.setCurrency(i_valuta);
                    gps2s.setCustomInfo(null);
                    gps2s.setErrorCode("9999");
                    gps2s.setErrorDescription("Simulazione di KO");
                    gps2s.setShopTransactionID(i_shopTransactionID);
                    gps2s.setTransactionKey(null);
                    gps2s.setTransactionResult(TransResult.KO);
                    gps2s.setTransactionState(null);
                    gps2s.setTransactionType(TransType.DELETE);
                    gps2s.setVbV(null);
                    
                    gestPayData.setGps2s(gps2s);
                    
                    return gestPayData;
                }
            }
            
            proxy.setHttp();
            
            if ( acquirerId.equals("BANCASELLA")) {

                WSs2S service1 = new WSs2S(this.url);
                System.out.println("Create Web Service...");
                WSs2SSoap port1 = service1.getWSs2SSoap();
                System.out.println("Call Web Service Operation...");
    
                CallDeleteS2SResult eresult = port1.callDeleteS2S(shopLogin, shopTransactionId, null);
                GestPayS2S gps2s;
                Object content = ((List) eresult.getContent()).get(0);
                JAXBContext context;
    
                proxy.unsetHttp();
    
                try {
                    context = JAXBContext.newInstance(GestPayS2S.class);
                    Unmarshaller um = context.createUnmarshaller();
                    gps2s = (GestPayS2S) um.unmarshal((Node) content);
    
                    gestPayData = new GestPayData();
                    gestPayData.setGps2s(gps2s);
    
                    gestPayData.setTransactionType("DELETE");
                    gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                    gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                    gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                    gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                    gestPayData.setCurrency(gps2s.getCurrency());
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCountry(gps2s.getCountry());
    
                    if (gps2s.getCustomInfo() != null) {
                        gestPayData.setCustomInfo(gps2s.getCustomInfo().toString());
                    }
                    if (gps2s.getBuyer() != null) {
                        gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                        gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                    }
                    gestPayData.setErrorCode(gps2s.getErrorCode());
                    gestPayData.setErrorDescription(gps2s.getErrorDescription());
                    gestPayData.setAlertCode(gps2s.getAlertCode());
                    gestPayData.setAlertDescription(gps2s.getAlertDescription());
                    if (gps2s.getVbV() != null) {
                        gestPayData.setVbVRisp(gps2s.getVbV().getVbVRisp());
                        gestPayData.setVbVBuyer(gps2s.getVbV().getVbVBuyer());
                        gestPayData.setVbVFlag(gps2s.getVbV().getVbVFlag());
                    }
    
                    // Si ripristina l'importo del cap disponibile precedentemente autorizzato
                    //response = userService.refundAvailableCap(i_shopTransactionID);
    
                }
                catch (JAXBException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            else {
                
                if ( acquirerId.equals("CARTASI")) {

                    PaymentClientImplementation paymentClientImplementation = new PaymentClientImplementation();
                    
                    GestPayS2S gps2s = paymentClientImplementation.callDeleteS2S(shopLogin, shopTransactionId, amount, uicCode, groupAcquirer, encodedSecretKey);
                    
                    proxy.unsetHttp();
                    
                    System.out.println("OK");
                    
                    gestPayData = new GestPayData();
                    gestPayData.setGps2s(gps2s);
                    gestPayData.setTransactionType(gps2s.getTransactionType().value());
                    gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                    gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                    gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                    gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                    gestPayData.setCurrency(gps2s.getCurrency());
                    gestPayData.setAmount(String.valueOf(i_amount));
                    gestPayData.setCountry(gps2s.getCountry());

                    if (gps2s.getCustomInfo() != null) {
                        gestPayData.setCustomInfo(gps2s.getCustomInfo().toString());
                    }
                    if (gps2s.getBuyer() != null) {
                        gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                        gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                    }
                    gestPayData.setErrorCode(gps2s.getErrorCode());
                    gestPayData.setErrorDescription(gps2s.getErrorDescription());
                    gestPayData.setAlertCode(gps2s.getAlertCode());
                    gestPayData.setAlertDescription(gps2s.getAlertDescription());
                    if (gps2s.getVbV() != null) {
                        gestPayData.setVbVRisp(gps2s.getVbV().getVbVRisp());
                        gestPayData.setVbVBuyer(gps2s.getVbV().getVbVBuyer());
                        gestPayData.setVbVFlag(gps2s.getVbV().getVbVFlag());
                    }
                    System.out.println("gestPayData: " + gestPayData);
                    
                }
                else {
                    
                    if ( acquirerId.equals("MULTICARD")) {
                        
                        Date now = new Date();
                        
                        System.out.println("Cancellazione autorizzazione con Multicard");
                        
                        Transaction transaction = transactionService.getTransactionDetail(i_shopTransactionID, shopTransactionId);
                        
                        if (transaction == null) {
                            // TODO: gestire errore null
                            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "deletePagam", i_shopTransactionID, null, "Error transazione non trovata");
                            return null;
                        }
                        
                        //String operationID           = "000000" + new IdGenerator().generateId(16).substring(0, 26); //32);
                        String operationID           = new IdGenerator().generateId(16).substring(0, 32);
                        
                        String mcCardDpan             = transaction.getToken();
                        String messageReasonCode     = "4000";
                        String retrievalRefNumber    = transaction.getBankTansactionID();
                        String authCode              = transaction.getAuthorizationCode();
                        String currencyCode          = transaction.getCurrency();
                        PaymentRefuelMode refuelMode = PaymentRefuelMode.I;
                        Integer integerAmount        = 0;
                        ProductCodeEnum productCode  = ProductCodeEnum.GG;
                        Integer quantity             = AmountConverter.toMobile3(transaction.getFuelQuantity());
                        Integer unitPrice            = AmountConverter.toMobile3(transaction.getUnitPrice());
                        PartnerType partnerType      = PartnerType.EM;
                        Long requestTimestamp        = now.getTime();
                        
                        ExecuteCaptureResult executeCaptureResult = paymentService.executeCapture(
                                mcCardDpan,
                                messageReasonCode,
                                retrievalRefNumber,
                                authCode,
                                currencyCode,
                                refuelMode,
                                operationID,
                                integerAmount,
                                productCode,
                                quantity,
                                unitPrice,
                                partnerType,
                                requestTimestamp);
                        
                        proxy.unsetHttp();
                        
                        if (executeCaptureResult == null) {
                            
                            String response = transactionService.persistTransactionOperation(operationID, shopTransactionId, "null", "", "ERROR", "", "CAN",
                                    operationID, requestTimestamp, null);
                            
                            // Gestire esito NULL
                            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callDeleteS2S", i_shopTransactionID, null, "Error executeCaptureResult null");
                            proxy.unsetHttp();
                            return null;
                        }
                        
                        if (!executeCaptureResult.getResult().getStatus().value().equals(EnumStatusType.SUCCESS.value())) {
                            
                            // Gestire stato != SUCCESS
                            CodeEnum code  = executeCaptureResult.getResult().getCode();
                            String message = executeCaptureResult.getResult().getMessage();
                            
                            // Errore applicativo nella chiamata al servizio di movimentazione credito con multicard
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", i_shopTransactionID, null, "executeCaptureResult - code: " + code.getValue() + ", message: " + message);
                            
                            // Gestione errori in movimentazione
                            // 00000   Nessun errore
                            // 00001   Parametri in input errati
                            // 00106   Partner Type non valido
                            // 00109   PAN non trovato
                            // 00117   Operazione di pagamento non travata per lo storno
                            // 00118   Operazione in timeout
                            // 00119   Operazione in corso
                            // 01***   Errori restituiti dall�autorizzativo
                            // 99001   Generic error
                            
                            // Mappatura codici e descrizioni errori
                            String errorCode = code.getValue();
                            String errorDescription = message;
                            
                            System.out.println("La transazione " + shopTransactionId + " e' stata rifiutata; descrizione errore: " + errorDescription + " ("
                                    + errorCode + ")");

                            // Errori che devono essere riprocessati tramite riconciliazione
                            if (errorCode.equals(CodeEnum.TIMEOUT_OPERATION.getValue()) ||
                                errorCode.equals(CodeEnum.OPERATION_NOT_COMPLETED .getValue()) ||
                                errorCode.equals(CodeEnum.GENERIC_ERROR.getValue())) {
                                
                                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callDeleteS2S", i_shopTransactionID, null, "Error executeCaptureResult: " + errorCode + " -> transazione da riconciliare");
                                proxy.unsetHttp();
                                return null;
                            }
                            
                            if (errorDescription != null && errorDescription.length() > 255) {
                                errorDescription = errorDescription.substring(0, 255);
                            }

                            System.out.println("KO");
                            
                            gestPayData = new GestPayData();
                            gestPayData.setErrorCode(errorCode);
                            gestPayData.setErrorDescription(errorDescription);
                            gestPayData.setTransactionResult("KO");
                            gestPayData.setShopTransactionID(shopTransactionId);
                            gestPayData.setAmount(String.valueOf(i_amount));
                            gestPayData.setCurrency(uicCode);
                            gestPayData.setTransactionType("CAN");
                            
                            GestPayS2S gps2s = new GestPayS2S();

                            gps2s.setTransactionType(TransType.DELETE);
                            gps2s.setTransactionResult(TransResult.KO);
                            gps2s.setShopTransactionID(shopTransactionId);
                            gps2s.setBankTransactionID("");
                            gps2s.setAuthorizationCode("");
                            gps2s.setCurrency(uicCode);
                            gps2s.setCountry("");
                            gps2s.setCustomInfo("");
                            gps2s.setBuyer(new BuyerType());
                            gps2s.setErrorCode(errorCode);
                            gps2s.setErrorDescription(errorDescription);
                            gps2s.setAlertCode("");
                            gps2s.setAlertDescription("");
                            gps2s.setVbV(new VbVType());

                            gestPayData.setGps2s(gps2s);
                        }
                        
                        String response = transactionService.persistTransactionOperation(operationID, shopTransactionId, executeCaptureResult.getResult().getCode().getValue(),
                                executeCaptureResult.getResult().getMessage(), executeCaptureResult.getResult().getStatus().value(), executeCaptureResult.getResult().getTransactionId(),
                                "CAN", operationID, requestTimestamp, integerAmount);
                        
                        System.out.println("OK");
                        
                        gestPayData = new GestPayData();
                        gestPayData.setErrorCode("0");
                        gestPayData.setErrorDescription("Cancellazione autorizzazione con Multicard");
                        gestPayData.setTransactionResult("OK");
                        gestPayData.setShopTransactionID(shopTransactionId);
                        gestPayData.setAmount(String.valueOf(i_amount));
                        gestPayData.setCurrency(uicCode);
                        gestPayData.setTransactionType("CAN");

                        GestPayS2S gps2s = new GestPayS2S();
                        gps2s.setAlertCode(null);
                        gps2s.setAlertDescription(null);
                        gps2s.setAuthorizationCode(null);
                        gps2s.setBankTransactionID(null);
                        gps2s.setBuyer(null);
                        gps2s.setCompany(null);
                        gps2s.setCountry(null);
                        gps2s.setCurrency(uicCode);
                        gps2s.setCustomInfo(null);
                        gps2s.setErrorCode("0");
                        gps2s.setErrorDescription("Cancellazione autorizzazione con Multicard");
                        gps2s.setShopTransactionID(i_shopTransactionID);
                        gps2s.setTransactionKey(null);
                        gps2s.setTransactionResult(TransResult.OK);
                        gps2s.setTransactionState(null);
                        gps2s.setTransactionType(TransType.DELETE);
                        gps2s.setVbV(null);
                        
                        gestPayData.setGps2s(gps2s);
                        
                    }
                    else {
                        
                        // TODO - Gestire l'errore
                    }
                }
            }
            
            if (simulationActive) {
                if (simulationDeletePagamError != null && simulationDeletePagamError.equalsIgnoreCase("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }
            
        }
        catch (Exception ex) {
            ex.printStackTrace();
            proxy.unsetHttp();
            return null;
        }

        return gestPayData;
    }

    public GestPayData callReadTrx(String i_shopLogin, String i_shopTransactionID, String i_bankTransactionID, String i_acquirerId, String i_groupAcquirer, String i_encodedSecretKey) {

        String shopLogin = i_shopLogin;
        String bankTransactionId = (i_bankTransactionID != null) ? i_bankTransactionID : "";
        String shopTransactionId = (i_shopTransactionID != null) ? i_shopTransactionID : "";
        String acquirerId = i_acquirerId;
        String groupAcquirer = i_groupAcquirer;
        String encodedSecretKey = i_encodedSecretKey;

        System.out.println("callReadTrxS2S");
        System.out.println("shopLogin: " + shopLogin);
        System.out.println("shopTransactionID: " + shopTransactionId);
        System.out.println("bankTransactionID: " + bankTransactionId);
        System.out.println("acquirerId: " + acquirerId);
        System.out.println("groupAcquirer: " + groupAcquirer);
        System.out.println("encodedSecretKey: " + encodedSecretKey);

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("shopLogin", shopLogin));
        inputParameters.add(new Pair<String, String>("shopTransactionId", shopTransactionId));
        inputParameters.add(new Pair<String, String>("shopTransactionId", bankTransactionId));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);
        try {

            proxy.setHttp();

            if ( acquirerId.equals("BANCASELLA") ) {
            
                WSs2S service1 = new WSs2S(this.url);
                WSs2SSoap port1 = service1.getWSs2SSoap();
    
                CallReadTrxS2SResult eresult = port1.callReadTrxS2S(i_shopLogin, i_shopTransactionID, i_bankTransactionID);
    
                GestPayS2S gps2s;
                Object content = ((List) eresult.getContent()).get(0);
                System.out.println(content);
                JAXBContext context;
    
                proxy.unsetHttp();
    
                try {
                    context = JAXBContext.newInstance(GestPayS2S.class);
                    Unmarshaller um = context.createUnmarshaller();
                    gps2s = (GestPayS2S) um.unmarshal((Node) content);
                    gestPayData = new GestPayData();
                    gestPayData.setGps2s(gps2s);
    
                    if (gps2s.getTransactionType() != null) {
                        gestPayData.setTransactionType(gps2s.getTransactionType().value());
                    }
    
                    if (gps2s.getTransactionResult() != null) {
                        gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                    }
    
                    gestPayData.setTransactionState(gps2s.getTransactionState());
                    gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                    gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                    gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                    gestPayData.setCurrency(gps2s.getCurrency());
                    gestPayData.setCountry(gps2s.getCountry());
    
                    if (gps2s.getCustomInfo() != null) {
                        gestPayData.setCustomInfo(gps2s.getCustomInfo().toString());
                    }
    
                    if (gps2s.getBuyer() != null) {
                        gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                        gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                    }
    
                    gestPayData.setErrorCode(gps2s.getErrorCode());
                    gestPayData.setErrorDescription(gps2s.getErrorDescription());
                    gestPayData.setAlertCode(gps2s.getAlertCode());
                    gestPayData.setAlertDescription(gps2s.getAlertDescription());
    
                    if (gps2s.getVbV() != null) {
                        gestPayData.setVbVRisp(gps2s.getVbV().getVbVRisp());
                        gestPayData.setVbVBuyer(gps2s.getVbV().getVbVBuyer());
                        gestPayData.setVbVFlag(gps2s.getVbV().getVbVFlag());
                    }
    
                    if (gps2s.getEvents() != null && gps2s.getEvents().size() > 0) {
                        gestPayData.setEvents(gps2s.getEvents());
                    }
    
                }
                catch (JAXBException e) {
                    e.printStackTrace();
                    this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callReadTrxS2S", i_shopTransactionID, null, "Error parsing response: " + e.getMessage());
                    return null;
                }
            }
            else {
                
                if ( acquirerId.equals("CARTASI")) {

                    PaymentClientImplementation paymentClientImplementation = new PaymentClientImplementation();
                    
                    GestPayS2S gps2s = paymentClientImplementation.callReadTrx(shopLogin, shopTransactionId, bankTransactionId, acquirerId, groupAcquirer, encodedSecretKey);
                    
                    proxy.unsetHttp();
                    
                    System.out.println("OK");
                    
                    gestPayData = new GestPayData();
                    gestPayData.setGps2s(gps2s);
                    
                    if (gps2s.getTransactionType() != null) {
                        gestPayData.setTransactionType(gps2s.getTransactionType().value());
                    }
    
                    if (gps2s.getTransactionResult() != null) {
                        gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                    }
    
                    gestPayData.setTransactionState(gps2s.getTransactionState());
                    gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                    gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                    gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                    gestPayData.setCurrency(gps2s.getCurrency());
                    gestPayData.setCountry(gps2s.getCountry());
    
                    if (gps2s.getCustomInfo() != null) {
                        gestPayData.setCustomInfo(gps2s.getCustomInfo().toString());
                    }
    
                    if (gps2s.getBuyer() != null) {
                        gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                        gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                    }
    
                    gestPayData.setErrorCode(gps2s.getErrorCode());
                    gestPayData.setErrorDescription(gps2s.getErrorDescription());
                    gestPayData.setAlertCode(gps2s.getAlertCode());
                    gestPayData.setAlertDescription(gps2s.getAlertDescription());
    
                    if (gps2s.getVbV() != null) {
                        gestPayData.setVbVRisp(gps2s.getVbV().getVbVRisp());
                        gestPayData.setVbVBuyer(gps2s.getVbV().getVbVBuyer());
                        gestPayData.setVbVFlag(gps2s.getVbV().getVbVFlag());
                    }
    
                    String amount = "";
                    
                    if (gps2s.getEvents() != null && gps2s.getEvents().size() > 0) {
                        gestPayData.setEvents(gps2s.getEvents());
                        
                        for (EventsType events : gps2s.getEvents()) {
                            for (Event event : events.getEvent()) {
                                amount = event.getEventamount().toString();
                                
                                System.out.println("EventType:   " + event.getEventtype() + ", EventAmount: " + amount);
                            }
                        }
                    }
                    
                    System.out.println("Amount: " + amount);
                    gestPayData.setAmount(amount);

                    System.out.println("gestPayData: " + gestPayData);
                    
                }
                else {
                    
                    if ( acquirerId.equals("MULTICARD")) {
                        
                        TransactionOperation transactionOperationAut = null;
                        TransactionOperation transactionOperationMov = null;
                        TransactionOperation transactionOperationCan = null;
                        TransactionOperation transactionOperationSto = null;
                        
                        String requestID = String.valueOf(new Date().getTime());
                        Double finalAmount = null;
                        
                        PostPaidTransaction postPaidTransaction = postPaidV2TransactionService.getPopTransactionDetail(requestID, shopTransactionId);
                        
                        if (postPaidTransaction != null) {
                            
                            finalAmount = postPaidTransaction.getAmount();
                            
                            for(PostPaidTransactionOperation postPaidTransactionOperation : postPaidTransaction.getPostPaidTransactionOperationBeanList()) {
                                if (postPaidTransactionOperation.getOperationType().equals("AUT")) {
                                    transactionOperationAut = postPaidTransactionOperation.toTransactionOperation();
                                    continue;
                                }
                                if (postPaidTransactionOperation.getOperationType().equals("MOV")) {
                                    transactionOperationMov = postPaidTransactionOperation.toTransactionOperation();
                                    continue;
                                }
                                if (postPaidTransactionOperation.getOperationType().equals("CAN")) {
                                    transactionOperationCan = postPaidTransactionOperation.toTransactionOperation();
                                    continue;
                                }
                                if (postPaidTransactionOperation.getOperationType().equals("STO")) {
                                    transactionOperationSto = postPaidTransactionOperation.toTransactionOperation();
                                    continue;
                                }
                            }
                        }
                        else {
                            
                            Transaction transaction = transactionService.getTransactionDetail(requestID, shopTransactionId);
                            
                            finalAmount = transaction.getFinalAmount();
                            
                            if (transaction != null) {
                                
                                for(TransactionOperation transactionOperation : transaction.getTransactionOperationList()) {
                                    if (transactionOperation.getOperationType().equals("AUT")) {
                                        transactionOperationAut = transactionOperation;
                                        continue;
                                    }
                                    if (transactionOperation.getOperationType().equals("MOV")) {
                                        transactionOperationMov = transactionOperation;
                                        continue;
                                    }
                                    if (transactionOperation.getOperationType().equals("CAN")) {
                                        transactionOperationCan = transactionOperation;
                                        continue;
                                    }
                                    if (transactionOperation.getOperationType().equals("STO")) {
                                        transactionOperationSto = transactionOperation;
                                        continue;
                                    }
                                }
                            }
                        }
                        
                        String paymentOperationId = null;
                        if (transactionOperationSto != null) {
                            paymentOperationId = transactionOperationSto.getOperationId();
                        }
                        else if (transactionOperationCan != null) {
                            paymentOperationId = transactionOperationCan.getOperationId();
                        }
                        else if (transactionOperationMov != null) {
                            paymentOperationId = transactionOperationMov.getOperationId();
                        }
                        else if (transactionOperationAut != null) {
                            paymentOperationId = transactionOperationAut.getOperationId();
                        }
                        
                        if (paymentOperationId == null) {
                            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callReadTrx", i_shopTransactionID, null, "Error transazione di pagamento non trovata");
                            return null;
                        }
                        
                        Date now = new Date();
                        
                        String operationID           = new IdGenerator().generateId(16).substring(0, 32);
                        PartnerType partnerType      = PartnerType.EM;
                        Long requestTimestamp        = now.getTime();
                        
                        GetPaymentStatusResult getPaymentStatus = paymentService.getPaymentStatus(
                                paymentOperationId,
                                operationID,
                                partnerType,
                                requestTimestamp);
                        
                        proxy.unsetHttp();
                        
                        if (getPaymentStatus == null) {
                            
                            // Gestire esito NULL
                            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callReadTrxS2S", i_shopTransactionID, null, "Error getPaymentStatus null");
                            proxy.unsetHttp();
                            return null;
                        }
                        
                        if (!getPaymentStatus.getResult().getStatus().value().equals(EnumStatusType.SUCCESS.value())) {
                            
                            // Gestire stato != SUCCESS
                            CodeEnum code  = getPaymentStatus.getResult().getCode();
                            String message = getPaymentStatus.getResult().getMessage();
                            
                            // Errore applicativo nella chiamata al servizio di movimentazione credito con multicard
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", i_shopTransactionID, null, "getPaymentStatus - code: " + code.getValue() + ", message: " + message);
                            
                            // Gestione errori
                            // 00000 Nessun errore
                            // 00001 Parametri in input errati
                            // 00106 Partner Type non valido
                            // 00112 Operazioni di pagamento non trovate per l�operationId di input
                            // 00118 Operazione in timeout
                            // 00119 Operazione in corso
                            // 01*** Errori restituiti dall�autorizzativo
                            // 99001 Generic error
                            
                            // Mappatura codici e descrizioni errori
                            String errorCode = code.getValue();
                            String errorDescription = message;
                            
                            System.out.println("La lettura della transazione " + shopTransactionId + " e' stata rifiutata; descrizione errore: " + errorDescription + " ("
                                    + errorCode + ")");

                            // Errori che devono essere riprocessati tramite riconciliazione
                            if (errorCode.equals(CodeEnum.TIMEOUT_OPERATION.getValue()) ||
                                errorCode.equals(CodeEnum.OPERATION_NOT_COMPLETED .getValue()) ||
                                errorCode.equals(CodeEnum.GENERIC_ERROR.getValue())) {
                                
                                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callRefundByRM", i_shopTransactionID, null, "Error getPaymentStatus: " + errorCode + " -> transazione da riconciliare");
                                proxy.unsetHttp();
                                return null;
                            }
                            
                            if (errorDescription != null && errorDescription.length() > 255) {
                                errorDescription = errorDescription.substring(0, 255);
                            }

                            GestPayS2S gps2s = new GestPayS2S();

                            gps2s.setTransactionType(TransType.QUERYTX);
                            gps2s.setTransactionResult(TransResult.KO);
                            gps2s.setShopTransactionID(shopTransactionId);
                            gps2s.setBankTransactionID("");
                            gps2s.setAuthorizationCode("");
                            gps2s.setCurrency("");
                            gps2s.setCountry("");
                            gps2s.setCustomInfo("");
                            gps2s.setBuyer(new BuyerType());
                            gps2s.setErrorCode(errorCode);
                            gps2s.setErrorDescription(errorDescription);
                            gps2s.setAlertCode("");
                            gps2s.setAlertDescription("");

                            gps2s.setVbV(new VbVType());

                            gestPayData = new GestPayData();
                            gestPayData.setGps2s(gps2s);
                            
                            if (gps2s.getTransactionType() != null) {
                                gestPayData.setTransactionType(gps2s.getTransactionType().value());
                            }
            
                            if (gps2s.getTransactionResult() != null) {
                                gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                            }
            
                            gestPayData.setTransactionState(gps2s.getTransactionState());
                            gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                            gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                            gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                            gestPayData.setCurrency(gps2s.getCurrency());
                            gestPayData.setCountry(gps2s.getCountry());
            
                            if (gps2s.getBuyer() != null) {
                                gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                                gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                            }
            
                            gestPayData.setErrorCode(gps2s.getErrorCode());
                            gestPayData.setErrorDescription(gps2s.getErrorDescription());
                            gestPayData.setAlertCode(gps2s.getAlertCode());
                            gestPayData.setAlertDescription(gps2s.getAlertDescription());
                            
                            if (gps2s.getEvents() != null && gps2s.getEvents().size() > 0) {
                                gestPayData.setEvents(gps2s.getEvents());
                                
                                for (EventsType events : gps2s.getEvents()) {
                                    for (Event event : events.getEvent()) {
                                        amount = event.getEventamount().toString();
                                        
                                        System.out.println("EventType:   " + event.getEventtype() + ", EventAmount: " + amount);
                                    }
                                }
                            }
        
                            System.out.println("gestPayData: " + gestPayData);
                            
                            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
                            outputParameters.add(new Pair<String, String>("TransactionType", "CALLREADTRX"));
                            outputParameters.add(new Pair<String, String>("TransactionResult", gestPayData.getTransactionResult().toString()));
                            outputParameters.add(new Pair<String, String>("TransactionState", gestPayData.getTransactionState()));
                            outputParameters.add(new Pair<String, String>("ShopTransactionID", gestPayData.getShopTransactionID()));
                            outputParameters.add(new Pair<String, String>("BankTransactionID", gestPayData.getBankTransactionID()));
                            outputParameters.add(new Pair<String, String>("AuthorizationCode", gestPayData.getAuthorizationCode()));
                            outputParameters.add(new Pair<String, String>("Currency", gestPayData.getCurrency()));
                            outputParameters.add(new Pair<String, String>("Country", gestPayData.getCountry()));
                            outputParameters.add(new Pair<String, String>("CustomInfo", gestPayData.getCustomInfo()));
                            outputParameters.add(new Pair<String, String>("BuyerName", gestPayData.getBuyerName()));
                            outputParameters.add(new Pair<String, String>("BuyerEmail", gestPayData.getBuyerEmail()));
                            outputParameters.add(new Pair<String, String>("ErrorCode", gestPayData.getErrorCode()));
                            outputParameters.add(new Pair<String, String>("ErrorDescription", gestPayData.getErrorDescription()));
                            outputParameters.add(new Pair<String, String>("AlertCode", gestPayData.getAlertCode()));
                            outputParameters.add(new Pair<String, String>("AlertDescription", gestPayData.getAlertDescription()));
                            outputParameters.add(new Pair<String, String>("VbVRisp", gestPayData.getVbVRisp()));
                            outputParameters.add(new Pair<String, String>("VbVBuyer", gestPayData.getVbVBuyer()));
                            outputParameters.add(new Pair<String, String>("VbVFlag", gestPayData.getVbVFlag()));

                            for (EventsType eventType : gestPayData.getEvents()) {
                                for (Event event : eventType.getEvent()) {
                                    outputParameters.add(new Pair<String, String>("Event.EventType", event.getEventtype()));
                                    outputParameters.add(new Pair<String, String>("Event.EventDate", event.getEventdate()));
                                    outputParameters.add(new Pair<String, String>("Event.EventAmount", event.getEventamount().toString()));
                                }
                            }

                            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "callReadTrxS2S", null, "closing", ActivityLog.createLogMessage(outputParameters));

                            return gestPayData;
                            
                        }
                        
                        /************************************/
                    
                        System.out.println("OK");
                        
                        System.out.println("Informazioni sulla transazione " + shopTransactionId + " recuperate con successo.");
                        
                        TransResult outTransactionResult = null;
                        String outShopTransactionId = null;
                        String outBankTransactionId = null;
                        String outAuthorizationCode = null;
                        String outCurrency = null;
                        String outCountry = null;
                        String outCompany = null;
                        String outTransactionState = null;
                        String eventtype = null;
                        BigDecimal eventamount = null;
                        String eventdate = null;
                        String outAmount = null;
                        
                        
                        GestPayS2S gps2s = new GestPayS2S();
                        
                        if (getPaymentStatus.getExecuteAuthorizationResult() != null) {
                            
                            System.out.println("Rilevato evento di richiesta autorizzazione pagamento.");
                            
                            ExecuteAuthorizationResult executeAuthorizationResult = getPaymentStatus.getExecuteAuthorizationResult();
                            
                            String executeAuthorizationResultStatus      = executeAuthorizationResult.getResult().getStatus().value();
                            String executeAuthorizationResultCode        = executeAuthorizationResult.getResult().getCode().getValue();
                            String executeAuthorizationResultDescription = executeAuthorizationResult.getResult().getMessage();
                            
                            if (executeAuthorizationResultStatus.equals(EnumStatusType.SUCCESS.value())) {
                                outTransactionResult = TransResult.OK;
                            }
                            else {
                                outTransactionResult = TransResult.KO;
                            }
                            
                            System.out.println("L'esito dell'operazione di autorizzazione �: " + executeAuthorizationResultDescription + " (" + executeAuthorizationResultCode + ")");
                            
                            Double amountAuthorized = AmountConverter.toInternal(executeAuthorizationResult.getAmountAuthorized());
                            
                            outShopTransactionId = executeAuthorizationResult.getResult().getTransactionId();
                            outBankTransactionId = executeAuthorizationResult.getAuthCode();
                            outAuthorizationCode = executeAuthorizationResult.getRetrievalRefNumber();
                            outCurrency = null;
                            outCountry = null;
                            outCompany = null;
                            outTransactionState = "AUT";
                            outAmount = amountAuthorized.toString();
                            
                            eventtype = "AUT";
                            eventamount = new BigDecimal(amountAuthorized);
                            eventdate = null;
                            
                            Event event = new Event();
                            event.setEventtype(eventtype);
                            event.setEventamount(eventamount);
                            event.setEventdate(eventdate);
                            EventsType eventType = new EventsType();
                            eventType.getEvent().add(event);
                            gps2s.getEvents().add(eventType);
                        }
                        
                        if (getPaymentStatus.getExecuteCaptureResult() != null) {
                            
                            System.out.println("La transazione " + shopTransactionId + " si trova in stato MOVIMENTATA (flusso prepaid).");
                            
                            ExecuteCaptureResult executeCaptureResult = getPaymentStatus.getExecuteCaptureResult();
                            
                            String executeCaptureResultStatus      = executeCaptureResult.getResult().getStatus().value();
                            String executeCaptureResultCode        = executeCaptureResult.getResult().getCode().getValue();
                            String executeCaptureResultDescription = executeCaptureResult.getResult().getMessage();
                            
                            if (executeCaptureResultStatus.equals(EnumStatusType.SUCCESS.value())) {
                                outTransactionResult = TransResult.OK;
                            }
                            else {
                                outTransactionResult = TransResult.KO;
                            }
                            
                            System.out.println("L'esito dell'operazione movimentazione �: " + executeCaptureResultDescription + " (" + executeCaptureResultCode + ")");
                            
                            outTransactionState = "MOV";
                            eventtype = "MOV";
                            
                            if (transactionOperationCan != null) {
                                finalAmount = new Double(0);
                                outAmount = "0";
                            }
                            
                            eventamount = new BigDecimal(finalAmount);
                            eventdate = null;
                            
                            Event event = new Event();
                            event.setEventtype(eventtype);
                            event.setEventamount(eventamount);
                            event.setEventdate(eventdate);
                            EventsType eventType = new EventsType();
                            eventType.getEvent().add(event);
                            gps2s.getEvents().add(eventType);
                        }
                        
                        if (getPaymentStatus.getExecutePaymentResult() != null) {
                            
                            System.out.println("La transazione " + shopTransactionId + " si trova in stato AUTORIZZATA E MOVIMENTATA (flusso postpaid).");
                            
                            ExecutePaymentResult executePaymentResult = getPaymentStatus.getExecutePaymentResult();
                            
                            String executePaymentResultStatus      = executePaymentResult.getResult().getStatus().value();
                            String executePaymentResultCode        = executePaymentResult.getResult().getCode().getValue();
                            String executePaymentResultDescription = executePaymentResult.getResult().getMessage();
                            
                            if (executePaymentResultStatus.equals(EnumStatusType.SUCCESS.value())) {
                                outTransactionResult = TransResult.OK;
                            }
                            else {
                                outTransactionResult = TransResult.KO;
                            }
                            
                            System.out.println("L'esito dell'operazione di autorizzazione+movimentazione �: " + executePaymentResultDescription + " (" + executePaymentResultCode + ")");
                            
                            outShopTransactionId = executePaymentResult.getResult().getTransactionId();
                            outBankTransactionId = executePaymentResult.getAuthCode();
                            outAuthorizationCode = executePaymentResult.getRetrievalRefNumber();
                            outCurrency = null;
                            outCountry = null;
                            outCompany = null;
                            outTransactionState = "MOV";
                            eventtype = "MOV";
                            
                            if (transactionOperationMov != null) {
                                finalAmount = AmountConverter.toInternal(transactionOperationMov.getAmount());
                                outAmount = finalAmount.toString();
                                System.out.println("finalAmount: " + finalAmount);
                            }
                            
                            eventamount = new BigDecimal(finalAmount);
                            eventdate = null;
                            
                            Event event = new Event();
                            event.setEventtype(eventtype);
                            event.setEventamount(eventamount);
                            event.setEventdate(eventdate);
                            EventsType eventType = new EventsType();
                            eventType.getEvent().add(event);
                            gps2s.getEvents().add(eventType);
                        }
                        
                        if (getPaymentStatus.getExecuteReversalResult() != null) {
                            
                            System.out.println("La transazione " + shopTransactionId + " si trova in stato STORNATA.");
                            
                            ExecuteReversalResult executeReversalResult = getPaymentStatus.getExecuteReversalResult();
                            
                            String executeReversalResultStatus      = executeReversalResult.getResult().getStatus().value();
                            String executeReversalResultCode        = executeReversalResult.getResult().getCode().getValue();
                            String executeReversalResultDescription = executeReversalResult.getResult().getMessage();
                            
                            if (executeReversalResultStatus.equals(EnumStatusType.SUCCESS.value())) {
                                outTransactionResult = TransResult.OK;
                            }
                            else {
                                outTransactionResult = TransResult.KO;
                            }
                            
                            System.out.println("L'esito dell'operazione di storno �: " + executeReversalResultDescription + " (" + executeReversalResultCode + ")");
                            
                            outTransactionState = "STO";
                            eventtype = "STO";
                            
                            if (transactionOperationMov != null) {
                                finalAmount = AmountConverter.toInternal(transactionOperationSto.getAmount());
                                outAmount = finalAmount.toString();
                                System.out.println("finalAmount: " + finalAmount);
                            }
                            
                            eventamount = new BigDecimal(outAmount);
                            eventdate = null;
                            
                            Event event = new Event();
                            event.setEventtype(eventtype);
                            event.setEventamount(eventamount);
                            event.setEventdate(eventdate);
                            EventsType eventType = new EventsType();
                            eventType.getEvent().add(event);
                            gps2s.getEvents().add(eventType);
                        }
                        
                        
                        gps2s.setTransactionType(TransType.QUERYTX);
                        gps2s.setTransactionResult(outTransactionResult);
                        gps2s.setShopTransactionID(outShopTransactionId);
                        gps2s.setBankTransactionID(outBankTransactionId);
                        gps2s.setAuthorizationCode(outAuthorizationCode);
                        gps2s.setCurrency(outCurrency);
                        gps2s.setCountry(outCountry);
                        gps2s.setCompany(outCompany);
                        gps2s.setCustomInfo(null);
                        gps2s.setBuyer(new BuyerType());
                        gps2s.setErrorCode("0");
                        gps2s.setErrorDescription("");
                        gps2s.setAlertCode("");
                        gps2s.setAlertDescription("");
                        gps2s.setTransactionState(outTransactionState);

                        gps2s.setVbV(new VbVType());
                        
                        gestPayData = new GestPayData();
                        gestPayData.setGps2s(gps2s);
                        
                        if (gps2s.getTransactionType() != null) {
                            gestPayData.setTransactionType(gps2s.getTransactionType().value());
                        }
        
                        if (gps2s.getTransactionResult() != null) {
                            gestPayData.setTransactionResult(gps2s.getTransactionResult().value());
                        }
        
                        gestPayData.setTransactionState(gps2s.getTransactionState());
                        gestPayData.setShopTransactionID(gps2s.getShopTransactionID());
                        gestPayData.setBankTransactionID(gps2s.getBankTransactionID());
                        gestPayData.setAuthorizationCode(gps2s.getAuthorizationCode());
                        gestPayData.setCurrency(gps2s.getCurrency());
                        gestPayData.setCountry(gps2s.getCountry());
        
                        if (gps2s.getBuyer() != null) {
                            gestPayData.setBuyerName(gps2s.getBuyer().getBuyerName());
                            gestPayData.setBuyerEmail(gps2s.getBuyer().getBuyerEmail());
                        }
        
                        gestPayData.setErrorCode(gps2s.getErrorCode());
                        gestPayData.setErrorDescription(gps2s.getErrorDescription());
                        gestPayData.setAlertCode(gps2s.getAlertCode());
                        gestPayData.setAlertDescription(gps2s.getAlertDescription());
                        
                        if (gps2s.getEvents() != null && gps2s.getEvents().size() > 0) {
                            gestPayData.setEvents(gps2s.getEvents());
                            
                            for (EventsType events : gps2s.getEvents()) {
                                for (Event event : events.getEvent()) {
                                    amount = event.getEventamount().toString();
                                    
                                    System.out.println("EventType:   " + event.getEventtype() + ", EventAmount: " + amount);
                                }
                            }
                        }
                        
                        gestPayData.setAmount(outAmount);
    
                        System.out.println("gestPayData: " + gestPayData);
                        
                        /************************************/
                        
                        /*
                        System.out.println("OK");
                        
                        gestPayData = new GestPayData();
                        gestPayData.setErrorCode("0");
                        gestPayData.setErrorDescription("Cancellazione autorizzazione con Multicard");
                        gestPayData.setTransactionResult("OK");
                        gestPayData.setShopTransactionID(shopTransactionId);
                        gestPayData.setAmount(String.valueOf(i_amount));
                        gestPayData.setCurrency(uicCode);
                        gestPayData.setTransactionType("CAN");

                        GestPayS2S gps2s = new GestPayS2S();
                        gps2s.setAlertCode(null);
                        gps2s.setAlertDescription(null);
                        gps2s.setAuthorizationCode(null);
                        gps2s.setBankTransactionID(null);
                        gps2s.setBuyer(null);
                        gps2s.setCompany(null);
                        gps2s.setCountry(null);
                        gps2s.setCurrency(uicCode);
                        gps2s.setCustomInfo(null);
                        gps2s.setErrorCode("0");
                        gps2s.setErrorDescription("Cancellazione autorizzazione con Multicard");
                        gps2s.setShopTransactionID(i_shopTransactionID);
                        gps2s.setTransactionKey(null);
                        gps2s.setTransactionResult(TransResult.OK);
                        gps2s.setTransactionState(null);
                        gps2s.setTransactionType(TransType.DELETE);
                        gps2s.setVbV(null);
                        
                        gestPayData.setGps2s(gps2s);
                        */
                    }
                    else {
                        
                        // TODO - Gestire l'errore
                    }
                }
            }
        }

        catch (javax.xml.ws.soap.SOAPFaultException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callReadTrxS2S", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (javax.xml.ws.ProtocolException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callReadTrxS2S", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (javax.xml.ws.WebServiceException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callReadTrxS2S", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (java.lang.SecurityException ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callReadTrxS2S", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "callReadTrxS2S", i_shopTransactionID, null, "Error parsing response: " + ex.getMessage());
            proxy.unsetHttp();
            return null;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("TransactionType", "CALLREADTRX"));
        outputParameters.add(new Pair<String, String>("TransactionResult", gestPayData.getTransactionResult().toString()));
        outputParameters.add(new Pair<String, String>("TransactionState", gestPayData.getTransactionState()));
        outputParameters.add(new Pair<String, String>("ShopTransactionID", gestPayData.getShopTransactionID()));
        outputParameters.add(new Pair<String, String>("BankTransactionID", gestPayData.getBankTransactionID()));
        outputParameters.add(new Pair<String, String>("AuthorizationCode", gestPayData.getAuthorizationCode()));
        outputParameters.add(new Pair<String, String>("Currency", gestPayData.getCurrency()));
        outputParameters.add(new Pair<String, String>("Country", gestPayData.getCountry()));
        outputParameters.add(new Pair<String, String>("CustomInfo", gestPayData.getCustomInfo()));
        outputParameters.add(new Pair<String, String>("BuyerName", gestPayData.getBuyerName()));
        outputParameters.add(new Pair<String, String>("BuyerEmail", gestPayData.getBuyerEmail()));
        outputParameters.add(new Pair<String, String>("ErrorCode", gestPayData.getErrorCode()));
        outputParameters.add(new Pair<String, String>("ErrorDescription", gestPayData.getErrorDescription()));
        outputParameters.add(new Pair<String, String>("AlertCode", gestPayData.getAlertCode()));
        outputParameters.add(new Pair<String, String>("AlertDescription", gestPayData.getAlertDescription()));
        outputParameters.add(new Pair<String, String>("VbVRisp", gestPayData.getVbVRisp()));
        outputParameters.add(new Pair<String, String>("VbVBuyer", gestPayData.getVbVBuyer()));
        outputParameters.add(new Pair<String, String>("VbVFlag", gestPayData.getVbVFlag()));

        for (EventsType eventType : gestPayData.getEvents()) {
            for (Event event : eventType.getEvent()) {
                outputParameters.add(new Pair<String, String>("Event.EventType", event.getEventtype()));
                outputParameters.add(new Pair<String, String>("Event.EventDate", event.getEventdate()));
                outputParameters.add(new Pair<String, String>("Event.EventAmount", event.getEventamount().toString()));
            }
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "callReadTrxS2S", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return gestPayData;

    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {

            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    private String setCustomInfo(Extension[] i_extension) {
        String customInfo = "";
        String key;
        String value;

        for (int i = 0; i < i_extension.length; i++) {
            key = i_extension[i].getKey();
            System.out.println("Key:" + key);
            value = i_extension[i].getValue();
            System.out.println("Value:" + value);
            if (key.startsWith("CST_") && value != "") {
                if (customInfo.equals("")) {
                    customInfo = key + "=" + value;
                }
                else {
                    customInfo = customInfo + "*P1*" + key + "=" + value;
                }
            }
        }

        if (customInfo == "") {
            customInfo = "CST_DISTRID=";
        }

        System.out.println("CustomInfo: " + customInfo);
        return customInfo;
    }

    public String deleteToken(String tokenValue, String shopLogin, String acquirerId, String encodedSecretKey) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("tokenValue", tokenValue));
        inputParameters.add(new Pair<String, String>("shopLogin", shopLogin));
        inputParameters.add(new Pair<String, String>("acquirerId", acquirerId));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "deleteToken", tokenValue, "opening", ActivityLog.createLogMessage(inputParameters));

        GestPayS2S gps2s = null;

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        try {
            proxy.setHttp();
            
            if ( acquirerId.equals("BANCASELLA") ) {
                
                WSs2S service1 = new WSs2S(this.url);
                WSs2SSoap port1 = service1.getWSs2SSoap();
    
                CallDeleteTokenS2SResult eresult = port1.callDeleteTokenS2S(tokenValue, shopLogin);
    
                List<Object> list = new ArrayList<Object>();
                list = eresult.getContent();
    
                Object content = list.get(0);
                JAXBContext context;
    
                context = JAXBContext.newInstance(GestPayS2S.class);
                Unmarshaller um = context.createUnmarshaller();
                gps2s = (GestPayS2S) um.unmarshal((Node) content);
            }
            else {
                
                if ( acquirerId.equals("CARTASI") ) {
                    
                    PaymentClientImplementation paymentClientImplementation = new PaymentClientImplementation();
                    
                    gps2s = paymentClientImplementation.callDeleteToken(shopLogin, tokenValue, encodedSecretKey);
                    
                }
                else {
                    
                    // TODO - Gestire l'errore
                }
            }

        }
        catch (JAXBException e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "deleteToken", tokenValue, null, "Error parsing response: " + e.getMessage());
            proxy.unsetHttp();
            return "KO";
        }
        catch (Exception e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "deleteToken", tokenValue, null, "Unable to connect to Payment Server: " + e.getMessage());
            proxy.unsetHttp();
            return "KO";
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("result", gps2s.getTransactionResult().toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "deleteToken", tokenValue, "closing", ActivityLog.createLogMessage(outputParameters));

        return gps2s.getTransactionResult().value();
    }

}
