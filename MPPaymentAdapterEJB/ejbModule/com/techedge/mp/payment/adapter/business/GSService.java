package com.techedge.mp.payment.adapter.business;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.w3c.dom.Node;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.payment.adapter.business.interfaces.GenerateRedirectUrlResponse;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;
import com.techedge.mp.payment.adapter.business.interfaces.GetTokenResponse;
import com.techedge.mp.payment.adapter.business.xsd.GestPayCryptDecrypt;
import com.techedge.mp.payment.adapter.cartasi.client.PaymentClientImplementation;
import com.techedge.mp.payment.adapter.servlet.DecryptResponse.DecryptResult;
import com.techedge.mp.payment.adapter.servlet.EncryptResponse.EncryptResult;
import com.techedge.mp.payment.adapter.servlet.WSCryptDecrypt;
import com.techedge.mp.payment.adapter.servlet.WSCryptDecryptSoap;

/**
 * Session Bean implementation class GSService
 */
@Stateless
@LocalBean
public class GSService implements GSServiceRemote, GSServiceLocal {

    private final static String     PARAM_PAYMENT_IN_WSDL         = "PAYMENT_IN_WSDL";
    private final static String     PARAM_SHOPLOGIN               = "SHOPLOGIN";
    private final static String     PARAM_UIC                     = "UIC";
    private final static String     PARAM_INITIAL_AMOUNT          = "INITIAL_AMOUNT";
    private final static String     PARAM_PROXY_HOST              = "PROXY_HOST";
    private final static String     PARAM_PROXY_PORT              = "PROXY_PORT";
    private final static String     PARAM_PROXY_NO_HOSTS          = "PROXY_NO_HOSTS";
    private final static String     PARAM_PAYMENT_URL_PREFIX_WSDL = "PAYMENT_URL_PREFIX_WSDL";

    private ParametersServiceRemote parametersService             = null;
    private LoggerServiceRemote     loggerService                 = null;

    private URL                     url;

    // Valori di default
    private String                  shopLogin                     = "GESPAY61144";
    private String                  uic                           = "242";                            //Euro
    private String                  amount                        = "0.01";
    private String                  proxyHost                     = "mpsquid.enimp.pri";
    private String                  proxyPort                     = "3128";
    private String                  proxyNoHosts                  = "localhost|127.0.0.1|*.enimp.pri";

    private final String            maskedPan                     = "MASKEDPAN";

    /**
     * Default constructor.
     */
    public GSService() {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {

            e.printStackTrace();

            //throw new BPELException("EJB interface not found: " + e.getMessage());
        }

        String wsdlString = "";
        String paymentUrlPrefixWsdl = "";
        try {
            wsdlString = parametersService.getParamValue(GSService.PARAM_PAYMENT_IN_WSDL);
            paymentUrlPrefixWsdl = parametersService.getParamValue(GSService.PARAM_PAYMENT_URL_PREFIX_WSDL);
            this.shopLogin = parametersService.getParamValue(GSService.PARAM_SHOPLOGIN);
            this.uic = parametersService.getParamValue(GSService.PARAM_UIC);
            this.amount = parametersService.getParamValue(GSService.PARAM_INITIAL_AMOUNT);
            this.proxyHost = parametersService.getParamValue(GSService.PARAM_PROXY_HOST);
            this.proxyPort = parametersService.getParamValue(GSService.PARAM_PROXY_PORT);
            this.proxyNoHosts = parametersService.getParamValue(GSService.PARAM_PROXY_NO_HOSTS);
        }
        catch (ParameterNotFoundException e) {

            e.printStackTrace();

            //throw new BPELException("Parameter " + Info.PARAM_FORECOURT_WSDL + " not found: " + e.getMessage());
        }

        try {
            InputStream fin = this.getClass().getResourceAsStream("/com/techedge/mp/payment/adapter/wsdl/WSCryptDecrypt.wsdl");

            InputStreamReader is = new InputStreamReader(fin);
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(is);
            String read;

            while ((read = br.readLine()) != null) {
                sb.append(read);
            }

            br.close();
            read = sb.toString().replaceAll("__PAYMENT_URL_PREFIX_WSDL__", paymentUrlPrefixWsdl);

            /*System.out.println("WSDL PAYMENT IN: (Start)");
            System.out.println(read);
            System.out.println("WSDL PAYMENT IN: (End)");*/

            String prefix = System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "wsdl" + File.separator + "WSCryptDecrypt";
            String suffix = ".wsdl";
            File tempFile = File.createTempFile(prefix, suffix);
            FileOutputStream fout = new FileOutputStream(tempFile);
            tempFile.deleteOnExit();
            fout.write(read.getBytes());
            fout.close();

            if (tempFile.exists()) {
                System.out.println("Create temp gsserverice wsdl: " + tempFile.getAbsolutePath());
            }
            else {
                System.err.println("Cannot to create temp gsserverice wsdl: " + tempFile.getAbsolutePath());
            }

            this.url = tempFile.toURI().toURL();
        }
        catch (MalformedURLException e) {

            e.printStackTrace();

            //throw new BPELException("Malformed URL: " + e.getMessage());
        }
        catch (IOException e) {

            e.printStackTrace();

            //throw new BPELException("Malformed URL: " + e.getMessage());
        }
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {

            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    @Override
    public GetTokenResponse getToken(String ticketId, String requestId, String operationId, String shopTransactionId, String shopLogin, String checkAmount) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("ticketId", ticketId));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("OperationId", operationId));
        inputParameters.add(new Pair<String, String>("shopTransactionId", shopTransactionId));
        inputParameters.add(new Pair<String, String>("shopLogin", shopLogin));
        inputParameters.add(new Pair<String, String>("checkAmount", checkAmount));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getToken", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        GestPayCryptDecrypt gpcd = null;

        try {

            Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

            proxy.setHttp();

            WSCryptDecrypt wsCryptDecrypt = new WSCryptDecrypt(this.url);
            WSCryptDecryptSoap wsCryptDecryptSoap = wsCryptDecrypt.getWSCryptDecryptSoap12();

            EncryptResult eresult = wsCryptDecryptSoap.encrypt(shopLogin, this.uic, checkAmount, shopTransactionId, null, null, null, null, null, null, null, null, this.maskedPan,
                    null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

            proxy.unsetHttp();

            gpcd = new GestPayCryptDecrypt();

            Object content = ((List<Object>) eresult.getContent()).get(0);

            JAXBContext context;

            context = JAXBContext.newInstance(GestPayCryptDecrypt.class);
            Unmarshaller um = context.createUnmarshaller();
            gpcd = (GestPayCryptDecrypt) um.unmarshal((Node) content);

        }
        catch (JAXBException e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getToken", requestId, null, "Error parsing response: " + e.getMessage());

            return null;
        }
        catch (Exception e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getToken", requestId, null, "Unable to connect to Payment Server: " + e.getMessage());

            return null;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("Amount", gpcd.getAmount()));
        outputParameters.add(new Pair<String, String>("AuthorizationCode", gpcd.getAuthorizationCode()));
        outputParameters.add(new Pair<String, String>("BankTransactionID", gpcd.getBankTransactionID()));

        if (gpcd.getBuyer() != null) {
            outputParameters.add(new Pair<String, String>("BuyerName", gpcd.getBuyer().getBuyerName()));
            outputParameters.add(new Pair<String, String>("BuyerEmail", gpcd.getBuyer().getBuyerEmail()));
        }

        outputParameters.add(new Pair<String, String>("CryptDecryptString", gpcd.getCryptDecryptString()));
        outputParameters.add(new Pair<String, String>("ErrorCode", gpcd.getErrorCode()));
        outputParameters.add(new Pair<String, String>("ErrorDescription", gpcd.getErrorDescription()));
        outputParameters.add(new Pair<String, String>("ShopTransactionID", gpcd.getShopTransactionID()));
        outputParameters.add(new Pair<String, String>("TOKEN", gpcd.getTOKEN()));
        outputParameters.add(new Pair<String, String>("TokenExpiryMonth", gpcd.getTokenExpiryMonth()));
        outputParameters.add(new Pair<String, String>("TokenExpiryYear", gpcd.getTokenExpiryYear()));
        outputParameters.add(new Pair<String, String>("TransactionKey", gpcd.getTransactionKey()));
        outputParameters.add(new Pair<String, String>("TransactionResult", gpcd.getTransactionResult().toString()));
        outputParameters.add(new Pair<String, String>("TransactionType", gpcd.getTransactionType().toString()));

        GetTokenResponse getTokenResponse = new GetTokenResponse();
        getTokenResponse.setBankTransactionId(gpcd.getBankTransactionID());
        getTokenResponse.setCurrency(gpcd.getAmount());
        getTokenResponse.setSecureString(gpcd.getCryptDecryptString());

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getToken", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return getTokenResponse;

    }

    @Override
    public GestPayData decryptString(String shopLogin, String encodedString) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("shopLogin", shopLogin));
        inputParameters.add(new Pair<String, String>("encodedString", encodedString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "decryptString", null, "opening", ActivityLog.createLogMessage(inputParameters));

        GestPayData gestPayData = null;

        try {

            Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

            proxy.setHttp();

            WSCryptDecrypt wsCryptDecrypt = new WSCryptDecrypt(this.url);
            WSCryptDecryptSoap wsCryptDecryptSoap = wsCryptDecrypt.getWSCryptDecryptSoap12();

            DecryptResult eresult = wsCryptDecryptSoap.decrypt(shopLogin, encodedString);

            proxy.unsetHttp();

            GestPayCryptDecrypt gpcd = new GestPayCryptDecrypt();

            List<Object> list = new ArrayList<Object>();
            list = eresult.getContent();

            Object content = list.get(0);
            JAXBContext context;

            context = JAXBContext.newInstance(GestPayCryptDecrypt.class);
            Unmarshaller um = context.createUnmarshaller();
            gpcd = (GestPayCryptDecrypt) um.unmarshal((Node) content);

            gestPayData = new GestPayData();

            gestPayData.setTransactionType(gpcd.getTransactionType().value());
            gestPayData.setTransactionResult(gpcd.getTransactionResult().value());
            gestPayData.setShopTransactionID(gpcd.getShopTransactionID());
            gestPayData.setBankTransactionID(gpcd.getBankTransactionID());
            gestPayData.setAuthorizationCode(gpcd.getAuthorizationCode());
            gestPayData.setCurrency(gpcd.getCurrency());
            gestPayData.setAmount(gpcd.getAmount());
            gestPayData.setCountry(gpcd.getCountry());

            if (gpcd.getCustomInfo() != null) {
                gestPayData.setCustomInfo(gpcd.getCustomInfo().toString());
            }
            if (gpcd.getBuyer() != null) {
                gestPayData.setBuyerName(gpcd.getBuyer().getBuyerName());
                gestPayData.setBuyerEmail(gpcd.getBuyer().getBuyerEmail());
            }
            gestPayData.setErrorCode(gpcd.getErrorCode());
            gestPayData.setErrorDescription(gpcd.getErrorDescription());
            gestPayData.setAlertCode(gpcd.getAlertCode());
            gestPayData.setAlertDescription(gpcd.getAlertDescription());
            gestPayData.setVbVRisp(gpcd.getVbVRisp());
            gestPayData.setVbVBuyer(gpcd.getVbVBuyer());
            gestPayData.setVbVFlag(gpcd.getVbVFlag());
            gestPayData.setToken(gpcd.getTOKEN());
            gestPayData.setTokenExpiryMonth(gpcd.getTokenExpiryMonth());
            gestPayData.setTokenExpiryYear(gpcd.getTokenExpiryYear());
            gestPayData.setCardBIN(gpcd.getCardBIN());
            gestPayData.setTdLevel(gpcd.getTdLevel());

        }
        catch (JAXBException e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "decryptString", null, null, "Error parsing response: " + e.getMessage());

            return null;
        }
        catch (Exception e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "decryptString", null, null, "Unable to connect to Payment Server: " + e.getMessage());

            return null;
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("TransactionType", gestPayData.getTransactionType().toString()));
        outputParameters.add(new Pair<String, String>("TransactionResult", gestPayData.getTransactionResult().toString()));
        outputParameters.add(new Pair<String, String>("ShopTransactionID", gestPayData.getShopTransactionID()));
        outputParameters.add(new Pair<String, String>("BankTransactionID", gestPayData.getBankTransactionID()));
        outputParameters.add(new Pair<String, String>("AuthorizationCode", gestPayData.getAuthorizationCode()));
        outputParameters.add(new Pair<String, String>("Currency", gestPayData.getCurrency()));
        outputParameters.add(new Pair<String, String>("Amount", gestPayData.getAmount()));
        outputParameters.add(new Pair<String, String>("Country", gestPayData.getCountry()));
        outputParameters.add(new Pair<String, String>("CustomInfo", gestPayData.getCustomInfo()));
        outputParameters.add(new Pair<String, String>("BuyerName", gestPayData.getBuyerName()));
        outputParameters.add(new Pair<String, String>("BuyerEmail", gestPayData.getBuyerEmail()));
        outputParameters.add(new Pair<String, String>("ErrorCode", gestPayData.getErrorCode()));
        outputParameters.add(new Pair<String, String>("ErrorDescription", gestPayData.getErrorDescription()));
        outputParameters.add(new Pair<String, String>("AlertCode", gestPayData.getAlertCode()));
        outputParameters.add(new Pair<String, String>("AlertDescription", gestPayData.getAlertDescription()));
        outputParameters.add(new Pair<String, String>("VbVRisp", gestPayData.getVbVRisp()));
        outputParameters.add(new Pair<String, String>("VbVBuyer", gestPayData.getVbVBuyer()));
        outputParameters.add(new Pair<String, String>("VbVFlag", gestPayData.getVbVFlag()));
        outputParameters.add(new Pair<String, String>("Token", gestPayData.getToken()));
        outputParameters.add(new Pair<String, String>("TokenExpiryMonth", gestPayData.getTokenExpiryMonth()));
        outputParameters.add(new Pair<String, String>("TokenExpiryYear", gestPayData.getTokenExpiryYear()));
        outputParameters.add(new Pair<String, String>("CardBIN", gestPayData.getCardBIN()));
        outputParameters.add(new Pair<String, String>("TDLevel", gestPayData.getTdLevel()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "decryptString", null, "closing", ActivityLog.createLogMessage(outputParameters));

        return gestPayData;

    }
    
    
    @Override
    public GenerateRedirectUrlResponse generateRedirectUrl(String requestId, String shopTransactionId, String apiKey, String checkAmount, String uicCode, String groupAcquirer, String encodedSecretKey, Boolean boUrl) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("shopTransactionId", shopTransactionId));
        inputParameters.add(new Pair<String, String>("apiKey", apiKey));
        inputParameters.add(new Pair<String, String>("checkAmount", checkAmount));
        inputParameters.add(new Pair<String, String>("uicCode", uicCode));
        inputParameters.add(new Pair<String, String>("groupAcquirer", groupAcquirer));
        inputParameters.add(new Pair<String, String>("encodedSecretKey", encodedSecretKey));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "generateRedirectUrl", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        PaymentClientImplementation paymentClientImplementation = new PaymentClientImplementation();
        
        GenerateRedirectUrlResponse generateRedirectUrlResponse = paymentClientImplementation.generateRedirectUrl(shopTransactionId, apiKey, checkAmount, uicCode, groupAcquirer, encodedSecretKey, boUrl);

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        
        if (generateRedirectUrlResponse.getCurrency() != null ) {
            outputParameters.add(new Pair<String, String>("currency", generateRedirectUrlResponse.getCurrency()));
        }
        else {
            outputParameters.add(new Pair<String, String>("currency", "null"));
        }
        
        if (generateRedirectUrlResponse.getRedirectUrl() != null ) {
            outputParameters.add(new Pair<String, String>("redirectUrl", generateRedirectUrlResponse.getRedirectUrl()));
        }
        else {
            outputParameters.add(new Pair<String, String>("redirectUrl", "null"));
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "generateRedirectUrl", requestId, "closing", ActivityLog.createLogMessage(outputParameters));

        return generateRedirectUrlResponse;

    }

}
