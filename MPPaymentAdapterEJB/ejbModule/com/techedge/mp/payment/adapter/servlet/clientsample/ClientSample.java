package com.techedge.mp.payment.adapter.servlet.clientsample;

import javax.xml.ws.WebServiceException;

import com.techedge.mp.payment.adapter.business.GSService;
import com.techedge.mp.payment.adapter.servlet.WSCryptDecrypt;
import com.techedge.mp.payment.adapter.servlet.WSCryptDecryptSoap;

public class ClientSample {

	public static void main(String[] args) {
		System.out.println("***********************");
        System.out.println("Create Web Service Client...");
        
        //System.setProperty("http.proxyHost", "sprite.techedge.corp");
    	//System.setProperty("http.proxyPort", "3128");
        
        try {
        	
        WSCryptDecrypt service1 = new WSCryptDecrypt();
        System.out.println("Create Web Service...");
        WSCryptDecryptSoap port1 = service1.getWSCryptDecryptSoap12();
        System.out.println("Call Web Service Operation...");
 
        GSService gsservice = new GSService();
        
//         gsservice.getToken("GESPAY61144", "242","0.01");
//        System.out.println("Server said: " + gsservice.getToken("GESPAY61144", "242","0.01","TRANSACTIONID"));
//        //Please input the parameters instead of 'null' for the upper method!
//
//        System.out.println("Server said: " + port1.encrypt(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null));
//        //Please input the parameters instead of 'null' for the upper method!
//
        System.out.println("Create Web Service...");
        WSCryptDecryptSoap port2 = service1.getWSCryptDecryptSoap12();
        System.out.println("Call Web Service Operation...");
        System.out.println("Server said: " + port2.decrypt(null,null));
        //Please input the parameters instead of 'null' for the upper method!

        System.out.println("Server said: " + port2.encrypt(null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null,null));
        
        } catch (WebServiceException e) {
        	
        	System.out.println("Impossibile raggiungere Banca Sella");
        	e.printStackTrace();
        }
	}
}
