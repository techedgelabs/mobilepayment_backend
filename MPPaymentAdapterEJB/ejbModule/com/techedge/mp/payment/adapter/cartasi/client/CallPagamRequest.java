package com.techedge.mp.payment.adapter.cartasi.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.techedge.mp.core.business.interfaces.Pair;

public class CallPagamRequest {

    protected String apiKey;
    protected String numeroContratto;
    protected String codiceTransazione;
    protected String importo;
    protected String divisa;
    protected String scadenza;
    protected String codiceGruppo;
    protected String timeStamp;
    protected String mac;
    protected HashMap<String, String> parametriAggiuntivi = new HashMap<String, String>();

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getNumeroContratto() {
        return numeroContratto;
    }

    public void setNumeroContratto(String numeroContratto) {
        this.numeroContratto = numeroContratto;
    }

    public String getCodiceTransazione() {
        return codiceTransazione;
    }

    public void setCodiceTransazione(String codiceTransazione) {
        this.codiceTransazione = codiceTransazione;
    }

    public String getImporto() {
        return importo;
    }

    public void setImporto(String importo) {
        this.importo = importo;
    }

    public String getDivisa() {
        return divisa;
    }

    public void setDivisa(String divisa) {
        this.divisa = divisa;
    }

    public String getScadenza() {
        return scadenza;
    }

    public void setScadenza(String scadenza) {
        this.scadenza = scadenza;
    }

    public String getCodiceGruppo() {
        return codiceGruppo;
    }

    public void setCodiceGruppo(String codiceGruppo) {
        this.codiceGruppo = codiceGruppo;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

    public HashMap<String, String> getParametriAggiuntivi() {
        return parametriAggiuntivi;
    }

    public void setParametriAggiuntivi(HashMap<String, String> parametriAggiuntivi) {
        this.parametriAggiuntivi = parametriAggiuntivi;
    }

}
