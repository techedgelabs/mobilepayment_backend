package com.techedge.mp.payment.adapter.cartasi.client;

public class CallPagamResponse extends BaseResponse {

    protected String codiceAutorizzazione;

    public String getCodiceAutorizzazione() {
        return codiceAutorizzazione;
    }

    public void setCodiceAutorizzazione(String codiceAutorizzazione) {
        this.codiceAutorizzazione = codiceAutorizzazione;
    }

}
