package com.techedge.mp.payment.adapter.cartasi.client;

import java.util.ArrayList;
import java.util.List;

public class CallReadTrxResponse extends BaseResponse {

    protected List<Report> report = new ArrayList<Report>(0);

    public List<Report> getReport() {
        return report;
    }

    public void setReport(List<Report> report) {
        this.report = report;
    }

}
