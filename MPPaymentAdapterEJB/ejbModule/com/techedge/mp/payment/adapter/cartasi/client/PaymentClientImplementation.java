package com.techedge.mp.payment.adapter.cartasi.client;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import com.google.gson.Gson;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.payment.adapter.business.EJBHomeCache;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GenerateRedirectUrlResponse;
import com.techedge.mp.payment.adapter.wss2s.xsd.BuyerType;
import com.techedge.mp.payment.adapter.wss2s.xsd.EventsType;
import com.techedge.mp.payment.adapter.wss2s.xsd.EventsType.Event;
import com.techedge.mp.payment.adapter.wss2s.xsd.GestPayS2S;
import com.techedge.mp.payment.adapter.wss2s.xsd.TransResult;
import com.techedge.mp.payment.adapter.wss2s.xsd.TransType;
import com.techedge.mp.payment.adapter.wss2s.xsd.VbVType;

public class PaymentClientImplementation implements PaymentClientInterface {

    private final static String     PARAM_PAYMENT_CARTASI_URL               = "PAYMENT_CARTASI_URL";
    private final static String     PARAM_PROXY_HOST                        = "PROXY_HOST";
    private final static String     PARAM_PROXY_PORT                        = "PROXY_PORT";
    private final static String     PARAM_PAYMENT_CARTASI_OFFSET            = "PAYMENT_CARTASI_OFFSET";
    private final static String     PARAM_DEPOSIT_CARD_CARTASI_URL          = "DEPOSIT_CARD_CARTASI_URL";
    private final static String     PARAM_DEPOSIT_CARD_CARTASI_URL_BACK     = "DEPOSIT_CARD_CARTASI_URL_BACK";
    private final static String     PARAM_DEPOSIT_CARD_CARTASI_URL_POST     = "DEPOSIT_CARD_CARTASI_URL_POST";
    private final static String     PARAM_DEPOSIT_CARD_CARTASI_BO_URL       = "DEPOSIT_CARD_CARTASI_BO_URL";
    private final static String     PARAM_DEPOSIT_CARD_CARTASI_BO_URL_BACK  = "DEPOSIT_CARD_CARTASI_BO_URL_BACK";
    private final static String     PARAM_DEPOSIT_CARD_CARTASI_BO_URL_POST  = "DEPOSIT_CARD_CARTASI_BO_URL_POST";
    private final static String     PARAM_PAYMENT_CLIENT_CONNECTION_TIMEOUT = "PAYMENT_CLIENT_CONNECTION_TIMEOUT";

    private final static String     AUTH_PAYMENT_PATH                   = "ecomm/api/recurring/pagamentoRicorrente";
    private final static String     SETTLE_PAYMENT_PATH                 = "ecomm/api/bo/contabilizza";
    private final static String     REFUND_PAYMENT_PATH                 = "ecomm/api/bo/storna";
    private final static String     READ_TRX_PATH                       = "ecomm/api/bo/situazioneOrdine";
    private final static String     REDIRECT_PATH                       = "ecomm/ecomm/DispatcherServlet";
    private final static String     DELETE_TOKEN_PATH                   = "ecomm/api/contratti/cancellaContratto";

    private ParametersServiceRemote parametersService                   = null;
    private LoggerServiceRemote     loggerService                       = null;

    private Gson                    gson                                = new Gson();

    private String                  basePath;
    private String                  depostCardCartaSiUrl;
    private String                  depostCardCartaSiUrlBack;
    private String                  depostCardCartaSiUrlPost;
    private String                  depostCardCartaSiBoUrl;
    private String                  depostCardCartaSiBoUrlBack;
    private String                  depostCardCartaSiBoUrlPost;

    private String                  proxyHost                           = null;
    private String                  proxyPort                           = null;

    private Long                    paymentOffset                       = 0l;
    private Integer                 paymentClientConnectionTimeout      = 0;

    public PaymentClientImplementation() {

        this.init();
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    private void init() {

        try {
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
            this.basePath = parametersService.getParamValue(PaymentClientImplementation.PARAM_PAYMENT_CARTASI_URL);
            this.proxyHost = parametersService.getParamValue(PaymentClientImplementation.PARAM_PROXY_HOST);
            this.proxyPort = parametersService.getParamValue(PaymentClientImplementation.PARAM_PROXY_PORT);
            this.paymentOffset = Long.valueOf(parametersService.getParamValue(PaymentClientImplementation.PARAM_PAYMENT_CARTASI_OFFSET));
            this.depostCardCartaSiUrl = parametersService.getParamValue(PaymentClientImplementation.PARAM_DEPOSIT_CARD_CARTASI_URL);
            this.depostCardCartaSiUrlBack = parametersService.getParamValue(PaymentClientImplementation.PARAM_DEPOSIT_CARD_CARTASI_URL_BACK);
            this.depostCardCartaSiUrlPost = parametersService.getParamValue(PaymentClientImplementation.PARAM_DEPOSIT_CARD_CARTASI_URL_POST);
            this.depostCardCartaSiBoUrl = parametersService.getParamValue(PaymentClientImplementation.PARAM_DEPOSIT_CARD_CARTASI_BO_URL);
            this.depostCardCartaSiBoUrlBack = parametersService.getParamValue(PaymentClientImplementation.PARAM_DEPOSIT_CARD_CARTASI_BO_URL_BACK);
            this.depostCardCartaSiBoUrlPost = parametersService.getParamValue(PaymentClientImplementation.PARAM_DEPOSIT_CARD_CARTASI_BO_URL_POST);
            this.paymentClientConnectionTimeout = Integer.parseInt(parametersService.getParamValue(PaymentClientImplementation.PARAM_PAYMENT_CLIENT_CONNECTION_TIMEOUT));
        }
        catch (ParameterNotFoundException e) {

            e.printStackTrace();
        }
        catch (InterfaceNotFoundException e) {

            e.printStackTrace();
        }
    }

    @Override
    public GestPayS2S callPagamS2S(String shopLogin, String uicCode, String amount, String shopTransactionId, Extension[] i_extension, String tokenValue, String groupAcquirer,
            String encodedSecretKey, String paymentMethodExpiration, TipoUtilizzo tipoUtilizzo) {

        //HttpClientBuilder builder = HttpClientBuilder.create();
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(this.paymentClientConnectionTimeout).build();
        HttpClientBuilder builder = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig);
        builder.setProxy(new HttpHost(this.proxyHost, Integer.valueOf(this.proxyPort)));
        HttpClient httpClient = builder.build();

        try {

            String url = this.basePath + PaymentClientImplementation.AUTH_PAYMENT_PATH;

            System.out.println("Invio chiamata autorizzazione pagamento alla url " + url);

            HttpPost post = new HttpPost(url);

            post.addHeader("Content-Type", "application/json");

            String scadenza = paymentMethodExpiration;
            String timestamp = this.getStringTimestamp();
            String convertedAmountString = PaymentClientImplementation.getConvertedAmount(amount);
            String codiceTransazione = PaymentClientImplementation.getCodiceTransazione(shopTransactionId);

            CallPagamRequest callPagamRequest = new CallPagamRequest();
            callPagamRequest.setApiKey(shopLogin);
            callPagamRequest.setCodiceGruppo(groupAcquirer);
            callPagamRequest.setCodiceTransazione(codiceTransazione);
            callPagamRequest.setDivisa(uicCode);
            callPagamRequest.setImporto(convertedAmountString);
            callPagamRequest.setNumeroContratto(tokenValue);
            callPagamRequest.setScadenza(scadenza);
            callPagamRequest.setTimeStamp(timestamp);

            callPagamRequest.getParametriAggiuntivi().put("infoc", codiceTransazione);
            callPagamRequest.getParametriAggiuntivi().put("tipo_utilizzo", tipoUtilizzo.value());
            
            for (Extension extension : i_extension) {
                callPagamRequest.getParametriAggiuntivi().put(extension.getKey(), extension.getValue());
            }

            String stringaMac = "apiKey=" + shopLogin + "numeroContratto=" + tokenValue + "codiceTransazione=" + codiceTransazione + "importo=" + convertedAmountString + "divisa="
                    + uicCode + "scadenza=" + scadenza + "timeStamp=" + timestamp + encodedSecretKey;

            String macCalculated = PaymentClientImplementation.hashMac(stringaMac);

            callPagamRequest.setMac(macCalculated);

            String requestString = gson.toJson(callPagamRequest);

            System.out.println("Request: " + requestString);

            StringEntity postString = new StringEntity(requestString);

            post.setEntity(postString);

            HttpResponse response = httpClient.execute(post);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString());

            CallPagamResponse callPagamResponse = gson.fromJson(result.toString(), CallPagamResponse.class);

            if (callPagamResponse != null) {

                if (callPagamResponse.getEsito().equals("OK")) {

                    String stringaMacResponse = "esito=" + callPagamResponse.getEsito() + "idOperazione=" + callPagamResponse.getIdOperazione() + "timeStamp="
                            + callPagamResponse.getTimeStamp() + encodedSecretKey;

                    String macResponseCalculated = PaymentClientImplementation.hashMac(stringaMacResponse);

                    if (!macResponseCalculated.equals(callPagamResponse.getMac())) {

                        System.out.println("S2S errore MAC: " + macResponseCalculated + " non corrisponde a " + callPagamResponse.getMac());

                        return null;
                    }

                    System.out.println("La transazione " + codiceTransazione + " e' avvenuta con successo; codice autorizzazione: " + callPagamResponse.getCodiceAutorizzazione());

                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setTransactionType(TransType.PAGAM);
                    gps2s.setTransactionResult(TransResult.OK);
                    gps2s.setShopTransactionID(codiceTransazione);
                    gps2s.setBankTransactionID(callPagamResponse.getIdOperazione());
                    gps2s.setAuthorizationCode(callPagamResponse.getCodiceAutorizzazione());
                    gps2s.setCurrency(uicCode);
                    gps2s.setCountry("");
                    gps2s.setCustomInfo("");
                    gps2s.setBuyer(new BuyerType());
                    gps2s.setErrorCode("0");
                    gps2s.setErrorDescription("");
                    gps2s.setAlertCode("");
                    gps2s.setAlertDescription("");

                    gps2s.setVbV(new VbVType());

                    return gps2s;
                }
                else {

                    System.out.println("La transazione " + codiceTransazione + " e' stata rifiutata; descrizione errore: " + callPagamResponse.getErrore().getMessaggio() + " ("
                            + callPagamResponse.getErrore().getCodice() + ")");

                    // Mappatura codici e descrizioni errori
                    String errorCode = callPagamResponse.getErrore().getCodice();
                    String errorDescription = callPagamResponse.getErrore().getMessaggio();

                    System.out.println("error:" + errorDescription);

                    if (errorDescription != null && errorDescription.length() > 255) {
                        errorDescription = errorDescription.substring(0, 255);
                    }

                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setTransactionType(TransType.PAGAM);
                    gps2s.setTransactionResult(TransResult.KO);
                    gps2s.setShopTransactionID(codiceTransazione);
                    gps2s.setBankTransactionID("");
                    gps2s.setAuthorizationCode("");
                    gps2s.setCurrency(uicCode);
                    gps2s.setCountry("");
                    gps2s.setCustomInfo("");
                    gps2s.setBuyer(new BuyerType());
                    gps2s.setErrorCode(errorCode);
                    gps2s.setErrorDescription(errorDescription);
                    gps2s.setAlertCode("");
                    gps2s.setAlertDescription("");

                    gps2s.setVbV(new VbVType());

                    return gps2s;
                }
            }

            /*
            try {
                request = gson.fromJson(responseString, UserRequest.class);
            }
            catch (JsonSyntaxException jsonEx) {
            */
            //handle response here...

        }
        catch (Exception ex) {

            ex.printStackTrace();

        }

        return null;
    }

    @Override
    public GestPayS2S callSettleS2S(String shopLogin, String uicCode, String amount, String shopTransactionId, Extension[] i_extension, String groupAcquirer,
            String encodedSecretKey) {

        //HttpClientBuilder builder = HttpClientBuilder.create();
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(this.paymentClientConnectionTimeout).build();
        HttpClientBuilder builder = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig);
        builder.setProxy(new HttpHost(this.proxyHost, Integer.valueOf(this.proxyPort)));
        HttpClient httpClient = builder.build();

        try {

            String url = this.basePath + PaymentClientImplementation.SETTLE_PAYMENT_PATH;

            System.out.println("Invio chiamata movimentazione pagamento alla url " + url);

            HttpPost post = new HttpPost(url);

            post.addHeader("Content-Type", "application/json");

            String timestamp = this.getStringTimestamp();
            String convertedAmountString = PaymentClientImplementation.getConvertedAmount(amount);
            String codiceTransazione = PaymentClientImplementation.getCodiceTransazione(shopTransactionId);

            CallSettleRequest callSettleRequest = new CallSettleRequest();
            callSettleRequest.setApiKey(shopLogin);
            callSettleRequest.setCodiceTransazione(codiceTransazione);
            callSettleRequest.setDivisa(uicCode);
            callSettleRequest.setImporto(convertedAmountString);
            callSettleRequest.setTimeStamp(timestamp);

            String stringaMac = "apiKey=" + shopLogin + "codiceTransazione=" + codiceTransazione + "divisa=" + uicCode + "importo=" + convertedAmountString + "timeStamp="
                    + timestamp + encodedSecretKey;

            String macCalculated = PaymentClientImplementation.hashMac(stringaMac);

            callSettleRequest.setMac(macCalculated);

            String requestString = gson.toJson(callSettleRequest);

            System.out.println("Request: " + requestString);

            StringEntity postString = new StringEntity(requestString);

            post.setEntity(postString);

            HttpResponse response = httpClient.execute(post);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString());

            CallSettleResponse callSettleResponse = gson.fromJson(result.toString(), CallSettleResponse.class);

            if (callSettleResponse != null) {

                if (callSettleResponse.getEsito().equals("OK")) {

                    String stringaMacResponse = "esito=" + callSettleResponse.getEsito() + "idOperazione=" + callSettleResponse.getIdOperazione() + "timeStamp="
                            + callSettleResponse.getTimeStamp() + encodedSecretKey;

                    String macResponseCalculated = PaymentClientImplementation.hashMac(stringaMacResponse);

                    if (!macResponseCalculated.equals(callSettleResponse.getMac())) {

                        System.out.println("S2S errore MAC: " + macResponseCalculated + " non corrisponde a " + callSettleResponse.getMac());

                        return null;
                    }

                    System.out.println("La transazione " + codiceTransazione + " e' stata contabilizzata con successo.");

                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setTransactionType(TransType.SETTLE);
                    gps2s.setTransactionResult(TransResult.OK);
                    gps2s.setShopTransactionID(codiceTransazione);
                    gps2s.setBankTransactionID("");
                    gps2s.setAuthorizationCode("");
                    gps2s.setCurrency(uicCode);
                    gps2s.setCountry("");
                    gps2s.setCustomInfo("");
                    gps2s.setBuyer(new BuyerType());
                    gps2s.setErrorCode("0");
                    gps2s.setErrorDescription("");
                    gps2s.setAlertCode("");
                    gps2s.setAlertDescription("");

                    gps2s.setVbV(new VbVType());

                    return gps2s;
                }
                else {

                    System.out.println("La transazione " + codiceTransazione + " e' stata rifiutata; descrizione errore: " + callSettleResponse.getErrore().getMessaggio() + " ("
                            + callSettleResponse.getErrore().getCodice() + ")");

                    // Mappatura codici e descrizioni errori
                    String errorCode = callSettleResponse.getErrore().getCodice();
                    String errorDescription = callSettleResponse.getErrore().getMessaggio();

                    System.out.println("error:" + errorDescription);

                    if (errorDescription != null && errorDescription.length() > 255) {
                        errorDescription = errorDescription.substring(0, 255);
                    }

                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setTransactionType(TransType.SETTLE);
                    gps2s.setTransactionResult(TransResult.KO);
                    gps2s.setShopTransactionID(codiceTransazione);
                    gps2s.setBankTransactionID("");
                    gps2s.setAuthorizationCode("");
                    gps2s.setCurrency(uicCode);
                    gps2s.setCountry("");
                    gps2s.setCustomInfo("");
                    gps2s.setBuyer(new BuyerType());
                    gps2s.setErrorCode(errorCode);
                    gps2s.setErrorDescription(errorDescription);
                    gps2s.setAlertCode("");
                    gps2s.setAlertDescription("");

                    gps2s.setVbV(new VbVType());

                    return gps2s;
                }
            }

            /*
            try {
                request = gson.fromJson(responseString, UserRequest.class);
            }
            catch (JsonSyntaxException jsonEx) {
            */
            //handle response here...

        }
        catch (Exception ex) {

            ex.printStackTrace();

        }

        return null;
    }

    @Override
    public GestPayS2S callDeleteS2S(String shopLogin, String shopTransactionId, String amount, String uicCode, String groupAcquirer, String encodedSecretKey) {

        //HttpClientBuilder builder = HttpClientBuilder.create();
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(this.paymentClientConnectionTimeout).build();
        HttpClientBuilder builder = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig);
        builder.setProxy(new HttpHost(this.proxyHost, Integer.valueOf(this.proxyPort)));
        HttpClient httpClient = builder.build();

        try {

            String url = this.basePath + PaymentClientImplementation.REFUND_PAYMENT_PATH;

            System.out.println("Invio chiamata cancellazione/storno pagamento alla url " + url);

            HttpPost post = new HttpPost(url);

            post.addHeader("Content-Type", "application/json");

            String timestamp = this.getStringTimestamp();
            String convertedAmountString = PaymentClientImplementation.getConvertedAmount(amount);
            String codiceTransazione = PaymentClientImplementation.getCodiceTransazione(shopTransactionId);

            CallRefundRequest callRefundRequest = new CallRefundRequest();
            callRefundRequest.setApiKey(shopLogin);
            callRefundRequest.setCodiceTransazione(codiceTransazione);
            callRefundRequest.setDivisa(uicCode);
            callRefundRequest.setImporto(convertedAmountString);
            callRefundRequest.setTimeStamp(timestamp);

            String stringaMac = "apiKey=" + shopLogin + "codiceTransazione=" + codiceTransazione + "divisa=" + uicCode + "importo=" + convertedAmountString + "timeStamp="
                    + timestamp + encodedSecretKey;

            String macCalculated = PaymentClientImplementation.hashMac(stringaMac);

            callRefundRequest.setMac(macCalculated);

            String requestString = gson.toJson(callRefundRequest);

            System.out.println("Request: " + requestString);

            StringEntity postString = new StringEntity(requestString);

            post.setEntity(postString);

            HttpResponse response = httpClient.execute(post);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString());

            CallRefundResponse callRefundResponse = gson.fromJson(result.toString(), CallRefundResponse.class);

            if (callRefundResponse != null) {

                if (callRefundResponse.getEsito().equals("OK")) {

                    String stringaMacResponse = "esito=" + callRefundResponse.getEsito() + "idOperazione=" + callRefundResponse.getIdOperazione() + "timeStamp="
                            + callRefundResponse.getTimeStamp() + encodedSecretKey;

                    String macResponseCalculated = PaymentClientImplementation.hashMac(stringaMacResponse);

                    if (!macResponseCalculated.equals(callRefundResponse.getMac())) {

                        System.out.println("S2S errore MAC: " + macResponseCalculated + " non corrisponde a " + callRefundResponse.getMac());

                        return null;
                    }

                    System.out.println("La transazione " + codiceTransazione + " e' stata annullata/stornata con successo.");

                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setTransactionType(TransType.DELETE);
                    gps2s.setTransactionResult(TransResult.OK);
                    gps2s.setShopTransactionID(codiceTransazione);
                    gps2s.setBankTransactionID("");
                    gps2s.setAuthorizationCode("");
                    gps2s.setCurrency(uicCode);
                    gps2s.setCountry("");
                    gps2s.setCustomInfo("");
                    gps2s.setBuyer(new BuyerType());
                    gps2s.setErrorCode("0");
                    gps2s.setErrorDescription("");
                    gps2s.setAlertCode("");
                    gps2s.setAlertDescription("");

                    gps2s.setVbV(new VbVType());

                    return gps2s;
                }
                else {

                    System.out.println("La transazione " + codiceTransazione + " e' stata rifiutata; descrizione errore: " + callRefundResponse.getErrore().getMessaggio() + " ("
                            + callRefundResponse.getErrore().getCodice() + ")");

                    // Mappatura codici e descrizioni errori
                    String errorCode = callRefundResponse.getErrore().getCodice();
                    String errorDescription = callRefundResponse.getErrore().getMessaggio();

                    System.out.println("error:" + errorDescription);

                    if (errorDescription != null && errorDescription.length() > 255) {
                        errorDescription = errorDescription.substring(0, 255);
                    }

                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setTransactionType(TransType.DELETE);
                    gps2s.setTransactionResult(TransResult.KO);
                    gps2s.setShopTransactionID(codiceTransazione);
                    gps2s.setBankTransactionID("");
                    gps2s.setAuthorizationCode("");
                    gps2s.setCurrency(uicCode);
                    gps2s.setCountry("");
                    gps2s.setCustomInfo("");
                    gps2s.setBuyer(new BuyerType());
                    gps2s.setErrorCode(errorCode);
                    gps2s.setErrorDescription(errorDescription);
                    gps2s.setAlertCode("");
                    gps2s.setAlertDescription("");

                    gps2s.setVbV(new VbVType());

                    return gps2s;
                }
            }

            /*
            try {
                request = gson.fromJson(responseString, UserRequest.class);
            }
            catch (JsonSyntaxException jsonEx) {
            */
            //handle response here...

        }
        catch (Exception ex) {

            ex.printStackTrace();

        }

        return null;
    }

    @Override
    public GestPayS2S callReadTrx(String shopLogin, String shopTransactionId, String bankTransactionID, String acquirerId, String groupAcquirer, String encodedSecretKey) {

        //HttpClientBuilder builder = HttpClientBuilder.create();
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(this.paymentClientConnectionTimeout).build();
        HttpClientBuilder builder = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig);
        builder.setProxy(new HttpHost(this.proxyHost, Integer.valueOf(this.proxyPort)));
        HttpClient httpClient = builder.build();

        try {

            String url = this.basePath + PaymentClientImplementation.READ_TRX_PATH;

            System.out.println("Invio chiamata per lettura informazioni transazione alla url " + url);

            HttpPost post = new HttpPost(url);

            post.addHeader("Content-Type", "application/json");

            String timestamp = this.getStringTimestamp();
            String codiceTransazione = PaymentClientImplementation.getCodiceTransazione(shopTransactionId);

            CallReadTrxRequest callReadTrxRequest = new CallReadTrxRequest();
            callReadTrxRequest.setApiKey(shopLogin);
            callReadTrxRequest.setCodiceTransazione(codiceTransazione);
            callReadTrxRequest.setTimeStamp(timestamp);

            String stringaMac = "apiKey=" + shopLogin + "codiceTransazione=" + codiceTransazione + "timeStamp=" + timestamp + encodedSecretKey;

            String macCalculated = PaymentClientImplementation.hashMac(stringaMac);

            callReadTrxRequest.setMac(macCalculated);

            String requestString = gson.toJson(callReadTrxRequest);

            System.out.println("Request: " + requestString);

            StringEntity postString = new StringEntity(requestString);

            post.setEntity(postString);

            HttpResponse response = httpClient.execute(post);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString());

            CallReadTrxResponse callReadTrxResponse = gson.fromJson(result.toString(), CallReadTrxResponse.class);

            if (callReadTrxResponse != null) {

                if (callReadTrxResponse.getEsito().equals("OK")) {

                    String stringaMacResponse = "esito=" + callReadTrxResponse.getEsito() + "idOperazione=" + callReadTrxResponse.getIdOperazione() + "timeStamp="
                            + callReadTrxResponse.getTimeStamp() + encodedSecretKey;

                    String macResponseCalculated = PaymentClientImplementation.hashMac(stringaMacResponse);

                    if (!macResponseCalculated.equals(callReadTrxResponse.getMac())) {

                        System.out.println("S2S errore MAC: " + macResponseCalculated + " non corrisponde a " + callReadTrxResponse.getMac());

                        return null;
                    }

                    System.out.println("Informazioni sulla transazione " + codiceTransazione + " recuperate con successo.");

                    GestPayS2S gps2s = new GestPayS2S();

                    Report report = callReadTrxResponse.getReport().get(0);

                    Dettaglio dettaglio = null;
                    if (report != null) {

                        dettaglio = report.getDettaglio().get(0);
                    }

                    gps2s.setTransactionType(TransType.QUERYTX);
                    gps2s.setTransactionResult(TransResult.OK);
                    gps2s.setShopTransactionID(codiceTransazione);
                    gps2s.setBankTransactionID(callReadTrxResponse.getIdOperazione());
                    gps2s.setAuthorizationCode(report.getCodiceAutorizzazione());
                    gps2s.setCurrency(report.getDivisa());
                    gps2s.setCountry(report.getNazione());
                    gps2s.setCompany(report.getBrand());
                    gps2s.setCustomInfo(report.getParametri());
                    gps2s.setBuyer(new BuyerType());
                    gps2s.setErrorCode("0");
                    gps2s.setErrorDescription("");
                    gps2s.setAlertCode("");
                    gps2s.setAlertDescription("");

                    gps2s.setVbV(new VbVType());

                    String transactionState = PaymentClientImplementation.getEventtype(report.getStato());

                    if (dettaglio != null) {

                        if (dettaglio.operazioni != null) {
                            
                            if (dettaglio.operazioni.isEmpty()) {
                                if (transactionState.equals("AUT")) {
                                    String eventtype = "AUT";
                                    BigDecimal eventamount = PaymentClientImplementation.convertImportoToBigDecimal(report.getImporto());
                                    String eventdate = PaymentClientImplementation.convertDataOperazione(report.getDataTransazione());
                                    
                                    Event event = new Event();
                                    event.setEventtype(eventtype);
                                    event.setEventamount(eventamount);
                                    event.setEventdate(eventdate);

                                    EventsType eventType = new EventsType();
                                    eventType.getEvent().add(event);

                                    gps2s.getEvents().add(eventType);
                                    
                                    System.out.println("DEBUG callReadTrx evento {"
                                            + "eventtype: " + eventtype
                                            + ", eventamount: " + eventamount
                                            + ", eventdate: " + eventdate
                                            + "}");
                                }
                                else if (transactionState.equals("CAN")) {
                                    String eventtype = "CAN";
                                    BigDecimal eventamount = PaymentClientImplementation.convertImportoToBigDecimal(report.getImporto());
                                    String eventdate = PaymentClientImplementation.convertDataOperazione(report.getDataTransazione());
                                    
                                    Event event = new Event();
                                    event.setEventtype(eventtype);
                                    event.setEventamount(eventamount);
                                    event.setEventdate(eventdate);

                                    EventsType eventType = new EventsType();
                                    eventType.getEvent().add(event);

                                    gps2s.getEvents().add(eventType);
                                    
                                    System.out.println("DEBUG callReadTrx evento {"
                                            + "eventtype: " + eventtype
                                            + ", eventamount: " + eventamount
                                            + ", eventdate: " + eventdate
                                            + "}");
                                }
                            }
                            else {
                                for (Operazione operazione : dettaglio.operazioni) {
    
                                    String tipoOperazione = operazione.tipoOperazione;
                                    String importo = operazione.importo;
                                    String dataOperazione = operazione.dataOperazione;
                                    
                                    System.out.println("DEBUG callReadTrx operazione {"
                                            + "tipoOperazione: " + tipoOperazione
                                            + ", importo: " + importo
                                            + ", dataOperazione: " + dataOperazione
                                            + "}");
    
                                    String eventtype = PaymentClientImplementation.getEventtype(tipoOperazione);
                                    BigDecimal eventamount = PaymentClientImplementation.convertImportoToBigDecimal(importo);
                                    String eventdate = PaymentClientImplementation.convertDataOperazione(dataOperazione);
    
                                    System.out.println("DEBUG callReadTrx evento {"
                                            + "eventtype: " + eventtype
                                            + ", eventamount: " + eventamount
                                            + ", eventdate: " + eventdate
                                            + "}");
                                    
                                    transactionState = eventtype;
    
                                    Event event = new Event();
                                    event.setEventtype(eventtype);
                                    event.setEventamount(eventamount);
                                    event.setEventdate(eventdate);
    
                                    EventsType eventType = new EventsType();
                                    eventType.getEvent().add(event);
    
                                    gps2s.getEvents().add(eventType);
                                }
                            }
                        }
                    }

                    gps2s.setTransactionState(transactionState);

                    return gps2s;
                }
                else {

                    System.out.println("Impossibile ottenere informazioni sulla transazione " + codiceTransazione + "; descrizione errore: "
                            + callReadTrxResponse.getErrore().getMessaggio() + " (" + callReadTrxResponse.getErrore().getCodice() + ")");

                    // Mappatura codici e descrizioni errori
                    String errorCode = callReadTrxResponse.getErrore().getCodice();
                    String errorDescription = callReadTrxResponse.getErrore().getMessaggio();

                    System.out.println("error:" + errorDescription);

                    if (errorDescription != null && errorDescription.length() > 255) {
                        errorDescription = errorDescription.substring(0, 255);
                    }

                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setTransactionType(TransType.QUERYTX);
                    gps2s.setTransactionResult(TransResult.KO);
                    gps2s.setShopTransactionID(codiceTransazione);
                    gps2s.setBankTransactionID("");
                    gps2s.setAuthorizationCode("");
                    gps2s.setCurrency("");
                    gps2s.setCountry("");
                    gps2s.setCustomInfo("");
                    gps2s.setBuyer(new BuyerType());
                    gps2s.setErrorCode(errorCode);
                    gps2s.setErrorDescription(errorDescription);
                    gps2s.setAlertCode("");
                    gps2s.setAlertDescription("");

                    gps2s.setVbV(new VbVType());

                    return gps2s;
                }
            }

            /*
            try {
                request = gson.fromJson(responseString, UserRequest.class);
            }
            catch (JsonSyntaxException jsonEx) {
            */
            //handle response here...

        }
        catch (Exception ex) {

            ex.printStackTrace();

        }

        return null;
    }

    @Override
    public GenerateRedirectUrlResponse generateRedirectUrl(String shopTransactionId, String shopLogin, String stringCheckAmount, String uicCode, String groupAcquirer,
            String encodedSecretKey, Boolean boUrl) {

        try {

            //PARAMETRI PER CALCOLO MAC
            String codTrans = PaymentClientImplementation.getCodiceTransazione(shopTransactionId);
            String divisa = uicCode;
            String importo = PaymentClientImplementation.getConvertedAmount(stringCheckAmount);
            String chiaveSegreta = encodedSecretKey;

            // CALCOLO MAC
            String stringaMac = "codTrans=" + codTrans + "divisa=" + divisa + "importo=" + importo + chiaveSegreta;

            String macCalculated = PaymentClientImplementation.hashMac(stringaMac);

            String numContratto = PaymentClientImplementation.generateContractNumber();
            String tipoRichiesta = "PP";

            String redirectUrl = "";

            String requestParams = "";
            
            String url     = this.depostCardCartaSiUrl;
            String urlBack = this.depostCardCartaSiUrlBack;
            String urlPost = this.depostCardCartaSiUrlPost;
            
            if (boUrl != null && boUrl == Boolean.TRUE) {
                url     = this.depostCardCartaSiBoUrl;
                urlBack = this.depostCardCartaSiBoUrlBack;
                urlPost = this.depostCardCartaSiBoUrlPost;
            }

            requestParams += "alias=" + URLEncoder.encode(shopLogin, "UTF-8") + "&";
            requestParams += "importo=" + URLEncoder.encode(importo, "UTF-8") + "&";
            requestParams += "divisa=" + URLEncoder.encode(divisa, "UTF-8") + "&";
            requestParams += "codTrans=" + URLEncoder.encode(codTrans, "UTF-8") + "&";
            requestParams += "url=" + URLEncoder.encode(url, "UTF-8") + "&";
            requestParams += "url_back=" + URLEncoder.encode(urlBack, "UTF-8") + "&";
            requestParams += "mac=" + URLEncoder.encode(macCalculated, "UTF-8") + "&";
            requestParams += "urlpost=" + URLEncoder.encode(urlPost, "UTF-8") + "&";
            requestParams += "num_contratto=" + URLEncoder.encode(numContratto, "UTF-8") + "&";
            requestParams += "tipo_servizio=" + URLEncoder.encode("paga_multi", "UTF-8") + "&";
            requestParams += "tipo_richiesta=" + URLEncoder.encode(tipoRichiesta, "UTF-8") + "&";
            requestParams += "gruppo=" + URLEncoder.encode(groupAcquirer, "UTF-8") + "&";

            redirectUrl = this.basePath + PaymentClientImplementation.REDIRECT_PATH + "?" + requestParams;

            GenerateRedirectUrlResponse generateRedirectUrlResponse = new GenerateRedirectUrlResponse();

            generateRedirectUrlResponse.setCurrency(divisa);
            generateRedirectUrlResponse.setRedirectUrl(redirectUrl);
            generateRedirectUrlResponse.setShopTransactionId(codTrans);

            return generateRedirectUrlResponse;
        }
        catch (Exception e) {

            e.printStackTrace();

            GenerateRedirectUrlResponse generateRedirectUrlResponse = new GenerateRedirectUrlResponse();
            generateRedirectUrlResponse.setCurrency(null);
            generateRedirectUrlResponse.setRedirectUrl(null);
            generateRedirectUrlResponse.setShopTransactionId(null);

            return generateRedirectUrlResponse;
        }
    }
    
    @Override
    public GestPayS2S callDeleteToken(String shopLogin, String tokenValue, String encodedSecretKey) {
        
        //HttpClientBuilder builder = HttpClientBuilder.create();
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(this.paymentClientConnectionTimeout).build();
        HttpClientBuilder builder = HttpClientBuilder.create().setDefaultRequestConfig(requestConfig);
        builder.setProxy(new HttpHost(this.proxyHost, Integer.valueOf(this.proxyPort)));
        HttpClient httpClient = builder.build();

        try {

            String url = this.basePath + PaymentClientImplementation.DELETE_TOKEN_PATH;

            System.out.println("Invio chiamata cancellazione contratto alla url " + url);

            HttpPost post = new HttpPost(url);

            post.addHeader("Content-Type", "application/json");

            String timestamp = this.getStringTimestamp();

            CallDeleteTokenRequest callDeleteTokenRequest = new CallDeleteTokenRequest();
            callDeleteTokenRequest.setApiKey(shopLogin);
            callDeleteTokenRequest.setNumeroContratto(tokenValue);
            callDeleteTokenRequest.setTimeStamp(timestamp);

            String stringaMac = "apiKey=" + shopLogin + "numeroContratto=" + tokenValue + "timeStamp=" + timestamp + encodedSecretKey;

            System.out.println("stringaMac: " + stringaMac);
            
            String macCalculated = PaymentClientImplementation.hashMac(stringaMac);

            System.out.println("macCalculated: " + macCalculated);
            
            callDeleteTokenRequest.setMac(macCalculated);

            String requestString = gson.toJson(callDeleteTokenRequest);

            System.out.println("Request: " + requestString);

            StringEntity postString = new StringEntity(requestString);

            post.setEntity(postString);

            HttpResponse response = httpClient.execute(post);

            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }

            System.out.println(result.toString());

            CallDeleteTokenResponse callDeleteTokenResponse = gson.fromJson(result.toString(), CallDeleteTokenResponse.class);

            if (callDeleteTokenResponse != null) {

                if (callDeleteTokenResponse.getEsito().equals("OK")) {

                    String stringaMacResponse = "esito=" + callDeleteTokenResponse.getEsito() + "idOperazione=" + callDeleteTokenResponse.getIdOperazione() + "timeStamp="
                            + callDeleteTokenResponse.getTimeStamp() + encodedSecretKey;

                    String macResponseCalculated = PaymentClientImplementation.hashMac(stringaMacResponse);

                    if (!macResponseCalculated.equals(callDeleteTokenResponse.getMac())) {

                        System.out.println("S2S errore MAC: " + macResponseCalculated + " non corrisponde a " + callDeleteTokenResponse.getMac());

                        return null;
                    }

                    System.out.println("Il numero contratto " + tokenValue + " e' stato cancellato con successo.");

                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setTransactionType(TransType.DELETE);
                    gps2s.setTransactionResult(TransResult.OK);
                    gps2s.setShopTransactionID("");
                    gps2s.setBankTransactionID("");
                    gps2s.setAuthorizationCode("");
                    gps2s.setCurrency("");
                    gps2s.setCountry("");
                    gps2s.setCustomInfo("");
                    gps2s.setBuyer(new BuyerType());
                    gps2s.setErrorCode("0");
                    gps2s.setErrorDescription("");
                    gps2s.setAlertCode("");
                    gps2s.setAlertDescription("");

                    gps2s.setVbV(new VbVType());

                    return gps2s;
                }
                else {

                    System.out.println("La cancellazione del numero contratto " + tokenValue + " e' stata rifiutata; descrizione errore: " + callDeleteTokenResponse.getErrore().getMessaggio() + " ("
                            + callDeleteTokenResponse.getErrore().getCodice() + ")");

                    // Mappatura codici e descrizioni errori
                    String errorCode = callDeleteTokenResponse.getErrore().getCodice();
                    String errorDescription = callDeleteTokenResponse.getErrore().getMessaggio();

                    System.out.println("error:" + errorDescription);

                    if (errorDescription != null && errorDescription.length() > 255) {
                        errorDescription = errorDescription.substring(0, 255);
                    }

                    GestPayS2S gps2s = new GestPayS2S();

                    gps2s.setTransactionType(TransType.DELETE);
                    gps2s.setTransactionResult(TransResult.KO);
                    gps2s.setShopTransactionID("");
                    gps2s.setBankTransactionID("");
                    gps2s.setAuthorizationCode("");
                    gps2s.setCurrency("");
                    gps2s.setCountry("");
                    gps2s.setCustomInfo("");
                    gps2s.setBuyer(new BuyerType());
                    gps2s.setErrorCode(errorCode);
                    gps2s.setErrorDescription(errorDescription);
                    gps2s.setAlertCode("");
                    gps2s.setAlertDescription("");

                    gps2s.setVbV(new VbVType());

                    return gps2s;
                }
            }

            /*
            try {
                request = gson.fromJson(responseString, UserRequest.class);
            }
            catch (JsonSyntaxException jsonEx) {
            */
            //handle response here...

        }
        catch (Exception ex) {

            ex.printStackTrace();
            
            GestPayS2S gps2s = new GestPayS2S();

            gps2s.setTransactionType(TransType.DELETE);
            gps2s.setTransactionResult(TransResult.KO);
            gps2s.setShopTransactionID("");
            gps2s.setBankTransactionID("");
            gps2s.setAuthorizationCode("");
            gps2s.setCurrency("");
            gps2s.setCountry("");
            gps2s.setCustomInfo("");
            gps2s.setBuyer(new BuyerType());
            gps2s.setErrorCode("0");
            gps2s.setErrorDescription("");
            gps2s.setAlertCode("");
            gps2s.setAlertDescription("");

            gps2s.setVbV(new VbVType());

            return gps2s;

        }

        return null;
    }

    private static String hashMac(String stringaMac) throws Exception {
        MessageDigest digest = MessageDigest.getInstance("SHA-1");
        byte[] in = digest.digest(stringaMac.getBytes("UTF-8"));

        final StringBuilder builder = new StringBuilder();
        for (byte b : in) {
            builder.append(String.format("%02x", b));
        }
        return builder.toString();
    }

    private String getStringTimestamp() {

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        Long timestampLong = timestamp.getTime();
        timestampLong += paymentOffset;

        String stringTimestamp = String.valueOf(timestampLong);

        return stringTimestamp;
    }

    private static String getConvertedAmount(String amount) {

        Double doubleAmount = (Double.valueOf(amount) * 100.0);
        Long l = Math.round(doubleAmount);
        Integer convertedAmount = Integer.valueOf(l.intValue());
        String convertedAmountString = convertedAmount.toString();

        return convertedAmountString;
    }

    private static String getCodiceTransazione(String shopTransactionId) {

        String codiceTransazione = shopTransactionId;

        if (shopTransactionId != null && shopTransactionId.length() >= 30) {

            codiceTransazione = shopTransactionId.substring(0, 30);
        }

        return codiceTransazione;
    }

    private static String getEventtype(String tipoOperazione) {

        String eventtype = "";

        if (tipoOperazione.equals("Autorizzato")) {

            eventtype = "AUT";
        }
        
        if (tipoOperazione.equals("CONTAB.")) {

            eventtype = "MOV";
        }
        
        if (tipoOperazione.equals("Annullato da Back-Office") || tipoOperazione.equals("Annullato")) {

            eventtype = "CAN";
        }

        return eventtype;
    }

    private static BigDecimal convertImportoToBigDecimal(String importo) {

        Double doubleAmount = Double.valueOf(importo);
        doubleAmount /= 100.0;
        BigDecimal convertedAmount = BigDecimal.valueOf(doubleAmount);

        return convertedAmount;
    }

    private static String convertDataOperazione(String dataOperazione) {

        // dataOperazione = '2017-07-19 16:29:10.0'
        // eventDate      = '19/07/17 20:37:45'

        String convertedEventDate = "";

        SimpleDateFormat parser = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.s");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yy HH:mm:ss");

        Date date;

        try {
            date = parser.parse(dataOperazione);
            convertedEventDate = formatter.format(date);
        }
        catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return convertedEventDate;
    }

    private static String generateContractNumber() {

        String contractNumber = new IdGenerator().generateId(16).substring(0, 16);

        return contractNumber;
    }
}
