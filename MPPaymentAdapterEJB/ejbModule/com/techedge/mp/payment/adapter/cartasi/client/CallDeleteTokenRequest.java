package com.techedge.mp.payment.adapter.cartasi.client;

public class CallDeleteTokenRequest {

    protected String apiKey;
    protected String numeroContratto;
    protected String timeStamp;
    protected String mac;

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getNumeroContratto() {
        return numeroContratto;
    }

    public void setNumeroContratto(String numeroContratto) {
        this.numeroContratto = numeroContratto;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getMac() {
        return mac;
    }

    public void setMac(String mac) {
        this.mac = mac;
    }

}
