package com.techedge.mp.payment.adapter.cartasi.client;

import java.util.ArrayList;
import java.util.List;

public class Dettaglio {

    protected String           nome;
    protected String           cognome;
    protected String           mail;
    protected String           importoRifiutato;
    protected String           importo;
    protected String           divisa;
    protected String           stato;
    protected String           codiceTransazione;
    protected String           parametriAggiuntivi;
    protected List<Operazione> operazioni = new ArrayList<Operazione>(0);

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getImportoRifiutato() {
        return importoRifiutato;
    }

    public void setImportoRifiutato(String importoRifiutato) {
        this.importoRifiutato = importoRifiutato;
    }

    public String getImporto() {
        return importo;
    }

    public void setImporto(String importo) {
        this.importo = importo;
    }

    public String getDivisa() {
        return divisa;
    }

    public void setDivisa(String divisa) {
        this.divisa = divisa;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public String getCodiceTransazione() {
        return codiceTransazione;
    }

    public void setCodiceTransazione(String codiceTransazione) {
        this.codiceTransazione = codiceTransazione;
    }

    public String getParametriAggiuntivi() {
        return parametriAggiuntivi;
    }

    public void setParametriAggiuntivi(String parametriAggiuntivi) {
        this.parametriAggiuntivi = parametriAggiuntivi;
    }

    public List<Operazione> getOperazioni() {
        return operazioni;
    }

    public void setOperazioni(List<Operazione> operazioni) {
        this.operazioni = operazioni;
    }

}
