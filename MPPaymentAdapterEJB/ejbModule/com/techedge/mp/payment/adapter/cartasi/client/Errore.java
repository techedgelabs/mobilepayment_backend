package com.techedge.mp.payment.adapter.cartasi.client;

public class Errore {

    protected String codice;
    protected String messaggio;

    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public String getMessaggio() {
        return messaggio;
    }

    public void setMessaggio(String messaggio) {
        this.messaggio = messaggio;
    }

}
