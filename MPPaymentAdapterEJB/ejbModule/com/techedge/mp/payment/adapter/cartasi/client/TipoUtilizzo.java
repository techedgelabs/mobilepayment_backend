package com.techedge.mp.payment.adapter.cartasi.client;

public enum TipoUtilizzo {
    UTENTI_PRIVATI("0"), AZIENDE("1");

    private final String value;

    TipoUtilizzo(String value) {
        this.value = value;
    }

    public String value() {
        return value;
    }
}
