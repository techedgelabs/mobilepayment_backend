package com.techedge.mp.payment.adapter.cartasi.client;

import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GenerateRedirectUrlResponse;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;
import com.techedge.mp.payment.adapter.wss2s.xsd.GestPayS2S;

public interface PaymentClientInterface {

    GestPayS2S callPagamS2S(String shopLogin, String uicCode, String amount, String shopTransactionId, Extension[] i_extension, String tokenValue, String groupAcquirer, String encodedSecretKey, String paymentMethodExpiration, TipoUtilizzo tipoUtilizzo);
    
    GestPayS2S callSettleS2S(String shopLogin, String uicCode, String amount, String shopTransactionId, Extension[] i_extension, String groupAcquirer, String encodedSecretKey);
    
    GestPayS2S callDeleteS2S(String shopLogin, String shopTransactionId, String amount, String uicCode, String groupAcquirer, String encodedSecretKey);
    
    GestPayS2S callReadTrx(String shopLogin, String shopTransactionID, String bankTransactionID, String acquirerId, String groupAcquirer, String encodedSecretKey);
    
    GenerateRedirectUrlResponse generateRedirectUrl(String shopTransactionId, String shopLogin, String stringCheckAmount, String uicCode, String groupAcquirer, String encodedSecretKey, Boolean boUrl);
    
    GestPayS2S callDeleteToken(String shopLogin, String tokenValue, String encodedSecretKey);
}
