package com.techedge.mp.payment.adapter.cartasi.client;

import java.util.ArrayList;
import java.util.List;

public class Report {

    protected String          codiceTransazione;
    protected String          numeroMerchant;
    protected String          importo;
    protected String          divisa;
    protected String          codiceAutorizzazione;
    protected String          brand;
    protected String          tipoPagamento;
    protected String          tipoTransazione;
    protected String          nazione;
    protected String          tipoProdotto;
    protected String          pan;
    protected String          parametri;
    protected String          stato;
    protected String          dataTransazione;
    protected String          mail;
    protected List<Dettaglio> dettaglio = new ArrayList<Dettaglio>(0);

    public String getCodiceTransazione() {
        return codiceTransazione;
    }

    public void setCodiceTransazione(String codiceTransazione) {
        this.codiceTransazione = codiceTransazione;
    }

    public String getNumeroMerchant() {
        return numeroMerchant;
    }

    public void setNumeroMerchant(String numeroMerchant) {
        this.numeroMerchant = numeroMerchant;
    }

    public String getImporto() {
        return importo;
    }

    public void setImporto(String importo) {
        this.importo = importo;
    }

    public String getDivisa() {
        return divisa;
    }

    public void setDivisa(String divisa) {
        this.divisa = divisa;
    }

    public String getCodiceAutorizzazione() {
        return codiceAutorizzazione;
    }

    public void setCodiceAutorizzazione(String codiceAutorizzazione) {
        this.codiceAutorizzazione = codiceAutorizzazione;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(String tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public String getTipoTransazione() {
        return tipoTransazione;
    }

    public void setTipoTransazione(String tipoTransazione) {
        this.tipoTransazione = tipoTransazione;
    }

    public String getNazione() {
        return nazione;
    }

    public void setNazione(String nazione) {
        this.nazione = nazione;
    }

    public String getTipoProdotto() {
        return tipoProdotto;
    }

    public void setTipoProdotto(String tipoProdotto) {
        this.tipoProdotto = tipoProdotto;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public String getParametri() {
        return parametri;
    }

    public void setParametri(String parametri) {
        this.parametri = parametri;
    }

    public String getStato() {
        return stato;
    }

    public void setStato(String stato) {
        this.stato = stato;
    }

    public String getDataTransazione() {
        return dataTransazione;
    }

    public void setDataTransazione(String dataTransazione) {
        this.dataTransazione = dataTransazione;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public List<Dettaglio> getDettaglio() {
        return dettaglio;
    }

    public void setDettaglio(List<Dettaglio> dettaglio) {
        this.dettaglio = dettaglio;
    }

}
