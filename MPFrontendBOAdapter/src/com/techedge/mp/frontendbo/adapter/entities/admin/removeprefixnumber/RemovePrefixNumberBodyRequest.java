package com.techedge.mp.frontendbo.adapter.entities.admin.removeprefixnumber;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RemovePrefixNumberBodyRequest implements Validable {

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public RemovePrefixNumberBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.code == null || this.code.isEmpty()) {

            status.setStatusCode(ResponseHelper.PREFIX_REMOVE_FAILURE);
            return status;

        }

        status.setStatusCode(ResponseHelper.PREFIX_REMOVE_SUCCESS);

        return status;
    }

}
