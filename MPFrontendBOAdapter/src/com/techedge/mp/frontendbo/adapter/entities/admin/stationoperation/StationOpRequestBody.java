package com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation;

import com.techedge.mp.frontendbo.adapter.entities.common.StationData;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class StationOpRequestBody implements Validable {

	private StationData station;


	public StationOpRequestBody() {}

	public StationData getStation() {
        return station;
    }

    public void setStation(StationData station) {
        this.station = station;
    }

    @Override
	public Status check() {
		Status status = new Status();
		if (this.station != null){
			status.setStatusCode(StatusCode.ADMIN_STATION_SUCCESS);
		}else{
			status.setStatusCode(StatusCode.ADMIN_STATION_PARAMETERS);
		}
		return status;
	}
}
