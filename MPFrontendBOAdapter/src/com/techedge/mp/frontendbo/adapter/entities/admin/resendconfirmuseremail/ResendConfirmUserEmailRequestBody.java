package com.techedge.mp.frontendbo.adapter.entities.admin.resendconfirmuseremail;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class ResendConfirmUserEmailRequestBody implements Validable {
	
	private String userEmail;
	private String category;
	
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getCategory() {
        return category;
    }
    public void setCategory(String category) {
        this.category = category;
    }
    
    @Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.userEmail == null) {
			
			status.setStatusCode(StatusCode.ADMIN_RESEND_CONFIRM_USER_EMAIL_WRONG_USER_EMAIL);
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_RESEND_CONFIRM_USER_EMAIL_SUCCESS);

		return status;
	}
	
}
