package com.techedge.mp.frontendbo.adapter.entities.admin.testscheduler;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestSchedulerRequestBody implements Validable {
	
	String action;
	List<String> params = new ArrayList<String>(0);
	
	public String getAction() {
		return action;
	}
	
	public void setAction(String action) {
		this.action = action;
	}

	public List<String> getParams() {
        return params;
    }

    public void setParams(List<String> params) {
        this.params = params;
    }

    @Override
	public Status check() {
		
		Status status = new Status();
		status.setStatusCode(StatusCode.ADMIN_TEST_SCHEDULER_SUCCESS);

		return status;
	}
}
