package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcategoryearnlist;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.dwh.adapter.interfaces.CategoryEarnDetail;

public class AdminDwhGetCategoryEarnListBodyResponse {

    private String                   errorCode;
    private String                   statusCode;
    private List<CategoryEarnDetail> categoryEarnDetailList = new ArrayList<CategoryEarnDetail>(0);

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<CategoryEarnDetail> getCategoryEarnDetailList() {
        return categoryEarnDetailList;
    }

    public void setCategoryEarnDetailList(List<CategoryEarnDetail> categoryEarnDetailList) {
        this.categoryEarnDetailList = categoryEarnDetailList;
    }

}
