package com.techedge.mp.frontendbo.adapter.entities.admin.authentication;

public class AuthenticationAdminResponseBody {

	private String ticketID;
	private AuthenticationAdminDataResponse adminData;
	
	public String getTicketID() {
		return ticketID;
	}
	public void setTicketID(String ticketID) {
		this.ticketID = ticketID;
	}

	public AuthenticationAdminDataResponse getAdminData() {
		return adminData;
	}
	public void setAdminData(AuthenticationAdminDataResponse adminData) {
		this.adminData = adminData;
	}
}
