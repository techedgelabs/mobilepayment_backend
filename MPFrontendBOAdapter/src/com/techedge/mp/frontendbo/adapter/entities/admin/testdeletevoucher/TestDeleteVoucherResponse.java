package com.techedge.mp.frontendbo.adapter.entities.admin.testdeletevoucher;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestDeleteVoucherResponse extends BaseResponse {

    private String csTransactionID;

    public TestDeleteVoucherResponse() {}

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }
}
