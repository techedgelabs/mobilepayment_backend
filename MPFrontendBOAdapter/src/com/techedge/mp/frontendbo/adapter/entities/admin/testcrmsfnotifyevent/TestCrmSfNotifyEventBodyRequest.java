package com.techedge.mp.frontendbo.adapter.entities.admin.testcrmsfnotifyevent;

import java.util.Date;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestCrmSfNotifyEventBodyRequest implements Validable {

	private String operationID;
	private String requestId;
	private String fiscalCode;
	private String date;
	private String stationId;
	private String productId;
	private Boolean paymentFlag;
	private Integer credits;
	private Double quantity;
	private String refuelMode;
	private String firstName;
	private String lastName;
	private String email;
	private String birthDate;
	private Boolean notificationFlag;
	private Boolean paymentCardFlag;
	private String brand;
	private String cluster;
	private Double amount;
	private Boolean privacyFlag1;
	private Boolean privacyFlag2;
	private String mobilePhone;
	private String loyaltyCard;
	private String parameter1;
	private String parameter2;
	private String paymentMode;
	private String eventType;

	public String getOperationID() {
		return operationID;
	}

	public void setOperationID(String operationID) {
		this.operationID = operationID;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getStationId() {
		return stationId;
	}

	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public Boolean getPaymentFlag() {
		return paymentFlag;
	}

	public void setPaymentFlag(Boolean paymentFlag) {
		this.paymentFlag = paymentFlag;
	}

	public Integer getCredits() {
		return credits;
	}

	public void setCredits(Integer credits) {
		this.credits = credits;
	}

	public Double getQuantity() {
		return quantity;
	}

	public void setQuantity(Double quantity) {
		this.quantity = quantity;
	}

	public String getRefuelMode() {
		return refuelMode;
	}

	public void setRefuelMode(String refuelMode) {
		this.refuelMode = refuelMode;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getNotificationFlag() {
		return notificationFlag;
	}

	public void setNotificationFlag(Boolean notificationFlag) {
		this.notificationFlag = notificationFlag;
	}

	public Boolean getPaymentCardFlag() {
		return paymentCardFlag;
	}

	public void setPaymentCardFlag(Boolean paymentCardFlag) {
		this.paymentCardFlag = paymentCardFlag;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCluster() {
		return cluster;
	}

	public void setCluster(String cluster) {
		this.cluster = cluster;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Boolean getPrivacyFlag1() {
		return privacyFlag1;
	}

	public void setPrivacyFlag1(Boolean privacyFlag1) {
		this.privacyFlag1 = privacyFlag1;
	}

	public Boolean getPrivacyFlag2() {
		return privacyFlag2;
	}

	public void setPrivacyFlag2(Boolean privacyFlag2) {
		this.privacyFlag2 = privacyFlag2;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}

	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getLoyaltyCard() {
		return loyaltyCard;
	}

	public void setLoyaltyCard(String loyaltyCard) {
		this.loyaltyCard = loyaltyCard;
	}

	public String getParameter1() {
		return parameter1;
	}

	public void setParameter1(String parameter1) {
		this.parameter1 = parameter1;
	}

	public String getParameter2() {
		return parameter2;
	}

	public void setParameter2(String parameter2) {
		this.parameter2 = parameter2;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public String getEventType() {
		return eventType;
	}

	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public Status check() {

		Status status = new Status();

		/*if (this.operationID == null || this.operationID.trim().isEmpty()) {

			status.setStatusCode(StatusCode.ADMIN_TEST_CRMSF_NOTIFY_EVENT_INVALID_REQUEST);

			return status;
		}*/

		status.setStatusCode(StatusCode.ADMIN_TEST_CRMSF_NOTIFY_EVENT_SUCCESS);

		return status;
	}

}
