package com.techedge.mp.frontendbo.adapter.entities.admin.updatepoptransaction;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdatePopTransactionRequestBody implements Validable {

    private String  mpTransactionId;
    private String  mpTransactionStatus;
    private Boolean toReconcile;

    public String getMpTransactionId() {
        return mpTransactionId;
    }

    public void setMpTransactionId(String mpTransactionId) {
        this.mpTransactionId = mpTransactionId;
    }

    public String getMpTransactionStatus() {
        return mpTransactionStatus;
    }

    public void setMpTransactionStatus(String mpTransactionStatus) {
        this.mpTransactionStatus = mpTransactionStatus;
    }

    public Boolean getToReconcile() {
        return toReconcile;
    }

    public void setToReconcile(Boolean toReconcile) {
        this.toReconcile = toReconcile;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.mpTransactionId == null) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_ERROR_PARAMETERS);
            return status;
        }
        else {

            if (this.mpTransactionId.length() > 40) {

                status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_ERROR_PARAMETERS);
                return status;
            }
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_SUCCESS);

        return status;
    }

}
