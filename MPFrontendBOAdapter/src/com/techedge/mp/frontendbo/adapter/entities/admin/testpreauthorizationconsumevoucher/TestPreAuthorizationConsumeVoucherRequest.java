package com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher;

import java.util.List;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PreAuthorizationConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestPreAuthorizationConsumeVoucherRequest extends AbstractBORequest implements Validable {

    private Credential                                    credential;
    private TestPreAuthorizationConsumeVoucherRequestBody body;

    public TestPreAuthorizationConsumeVoucherRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestPreAuthorizationConsumeVoucherRequestBody getBody() {
        return body;
    }

    public void setBody(TestPreAuthorizationConsumeVoucherRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PRE-AUTHORIZATION-CONSUME-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestPreAuthorizationConsumeVoucherResponse testPreAuthorizationConsumeVoucherResponse = new TestPreAuthorizationConsumeVoucherResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testPreAuthorizationConsumeVoucher");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testPreAuthorizationConsumeVoucherResponse.getStatus().setStatusCode("-1");
        }
        else {

            PreAuthorizationConsumeVoucherResult preAuthorizationConsumeVoucherResult = new PreAuthorizationConsumeVoucherResult();

            String operationID = this.getBody().getOperationID();
            VoucherConsumerType voucherConsumerType = getEnumerationValue(VoucherConsumerType.class, this.getBody().getVoucherConsumerType());
            PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());
            ProductType productType = getEnumerationValue(ProductType.class, this.getBody().getProductType());
            Long requestTimestamp = this.getBody().getRequestTimestamp();
            String mpTransactionID = this.getBody().getMpTransactionID();
            String language = this.getBody().getLanguage();
            Double amount = this.getBody().getAmount();
            String stationID = this.getBody().getStationID();
            List<VoucherCodeDetail> voucherCodeList = this.getBody().getVoucherList();

            try {
                preAuthorizationConsumeVoucherResult = getFidelityServiceRemote().preAuthorizationConsumeVoucher(operationID, mpTransactionID, voucherConsumerType, stationID,
                        amount, voucherCodeList, partnerType, requestTimestamp, language, productType);
            }
            catch (FidelityServiceException ex) {
                testPreAuthorizationConsumeVoucherResponse.getStatus().setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!preAuthorizationConsumeVoucherResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                testPreAuthorizationConsumeVoucherResponse.setCsTransactionID(preAuthorizationConsumeVoucherResult.getCsTransactionID());
                testPreAuthorizationConsumeVoucherResponse.setMarketingMsg(preAuthorizationConsumeVoucherResult.getMarketingMsg());
                testPreAuthorizationConsumeVoucherResponse.getStatus().setStatusMessage(preAuthorizationConsumeVoucherResult.getMessageCode());
                testPreAuthorizationConsumeVoucherResponse.getStatus().setStatusCode(preAuthorizationConsumeVoucherResult.getStatusCode());
                testPreAuthorizationConsumeVoucherResponse.setAmount(preAuthorizationConsumeVoucherResult.getAmount());
                testPreAuthorizationConsumeVoucherResponse.setPreAuthOperationID(preAuthorizationConsumeVoucherResult.getPreAuthOperationID());
            }
        }

        return testPreAuthorizationConsumeVoucherResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
