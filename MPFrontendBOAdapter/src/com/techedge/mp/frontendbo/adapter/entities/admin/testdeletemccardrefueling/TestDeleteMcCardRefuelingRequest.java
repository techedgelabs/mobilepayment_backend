package com.techedge.mp.frontendbo.adapter.entities.admin.testdeletemccardrefueling;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.interfaces.DeleteMcCardRefuelingResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestDeleteMcCardRefuelingRequest extends AbstractBORequest implements Validable {

    private Credential                           credential;
    private TestDeleteMcCardRefuelingBodyRequest body;

    public TestDeleteMcCardRefuelingRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestDeleteMcCardRefuelingBodyRequest getBody() {
        return body;
    }

    public void setBody(TestDeleteMcCardRefuelingBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DELETE-MC-CARD-REFUELING", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_DELETE_MC_CARD_REFUELING_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestDeleteMcCardRefuelingResponse testDeleteMcCardRefuelingResponse = new TestDeleteMcCardRefuelingResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testDeleteMcCardRefueling");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testDeleteMcCardRefuelingResponse.getStatus().setStatusCode("-1");
        }
        else {
            DeleteMcCardRefuelingResult deleteMcCardRefuelingResult = new DeleteMcCardRefuelingResult();

            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                deleteMcCardRefuelingResult = getCardInfoServiceRemote().deleteMcCardRefueling(this.getBody().getOperationId(), this.getBody().getUserId(), partnerType,
                        this.getBody().getRequestTimestamp(), this.getBody().getMcCardDpan(), this.getBody().getServerSerialNumber());
            }
            catch (Exception ex) {
                deleteMcCardRefuelingResult.setStatus("ERROR");
            }

            if (deleteMcCardRefuelingResult.getStatus() != null) {
                testDeleteMcCardRefuelingResponse.set_status(deleteMcCardRefuelingResult.getStatus());
                testDeleteMcCardRefuelingResponse.setCode(deleteMcCardRefuelingResult.getCode());
                testDeleteMcCardRefuelingResponse.setMessage(deleteMcCardRefuelingResult.getMessage());
                testDeleteMcCardRefuelingResponse.setTransactionId(deleteMcCardRefuelingResult.getTransactionId());
            }
        }

        return testDeleteMcCardRefuelingResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
