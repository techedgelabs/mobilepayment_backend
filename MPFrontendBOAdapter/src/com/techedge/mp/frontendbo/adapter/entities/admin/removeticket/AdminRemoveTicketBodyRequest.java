package com.techedge.mp.frontendbo.adapter.entities.admin.removeticket;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminRemoveTicketBodyRequest implements Validable {

    private String idFrom;
    private String idTo;
    private String ticketType;

    public String getTicketType() {
        return ticketType;
    }

    public void setTicketType(String ticketType) {
        this.ticketType = ticketType;
    }

    public AdminRemoveTicketBodyRequest() {}

    public String getIdFrom() {
        return idFrom;
    }

    public void setIdFrom(String idFrom) {
        this.idFrom = idFrom;
    }

    public String getIdTo() {
        return idTo;
    }

    public void setIdTo(String idTo) {
        this.idTo = idTo;
    }


    @Override
    public Status check() {

        Status status = new Status();

        if (this.idFrom == null || (this.idFrom != null && this.idFrom.isEmpty()) || this.idTo.isEmpty()
                || (this.idTo != null && this.idTo.isEmpty())) {
            status.setStatusCode(StatusCode.ADMIN_REMOVE_TICKET_INVALID_PARAMETERS);

            return status;
        }
        
        if(this.ticketType != null &&  ! this.ticketType.isEmpty()){
            if( !(this.ticketType.equalsIgnoreCase("customer") || this.ticketType.equalsIgnoreCase("service") || this.ticketType.equalsIgnoreCase("refueling"))){
//                if((! this.ticketType.equalsIgnoreCase("customer")) || (! this.ticketType.equalsIgnoreCase("system"))){
                status.setStatusCode(StatusCode.ADMIN_REMOVE_TICKET_INVALID_PARAMETERS);
                return status;
            }
        }

        status.setStatusCode(StatusCode.ADMIN_REMOVE_TICKET_SUCCESS);

        return status;
    }
}