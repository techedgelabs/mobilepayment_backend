package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusertype;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.user.UserCategory;
import com.techedge.mp.core.business.interfaces.user.UserType;
import com.techedge.mp.core.business.interfaces.user.UserTypeData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrieveUserTypeRequest extends AbstractBORequest implements Validable {

    private Status                      status = new Status();

    private Credential                  credential;
    private RetrieveUserTypeRequestBody body;

    public RetrieveUserTypeRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveUserTypeRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveUserTypeRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-USER-TYPE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_USER_TYPE_RETRIEVE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_USER_TYPE_RETRIEVE);

        return status;

    }

    @Override
    public BaseResponse execute() {

        RetrieveUserTypeResponse retrieveUserTypeResponse = new RetrieveUserTypeResponse();

        UserTypeData response = getAdminServiceRemote().adminRetrieveUserType(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getCode());

        status.setStatusCode(response.getStatusCode());
        status.setStatusMessage(prop.getProperty(response.getStatusCode()));

        retrieveUserTypeResponse.setStatus(status);

        List<RetrieveUserTypeBodyResponse> listRetrieveUserTypeBodyResponse = new ArrayList<RetrieveUserTypeBodyResponse>(0);
        for (UserType user : response.getUserTypes()) {
            RetrieveUserTypeBodyResponse retrieveUserTypeBodyResponse = new RetrieveUserTypeBodyResponse();
            retrieveUserTypeBodyResponse.setCode(user.getCode());
            List<String> names = new ArrayList<String>(0);
            for (UserCategory category : user.getUserCategories()) {
                names.add(category.getName());
            }
            if (names != null) {
                retrieveUserTypeBodyResponse.setUserCategory(names);
            }
            listRetrieveUserTypeBodyResponse.add(retrieveUserTypeBodyResponse);

        }

        retrieveUserTypeResponse.setUserType(listRetrieveUserTypeBodyResponse);

        return retrieveUserTypeResponse;
    }

}
