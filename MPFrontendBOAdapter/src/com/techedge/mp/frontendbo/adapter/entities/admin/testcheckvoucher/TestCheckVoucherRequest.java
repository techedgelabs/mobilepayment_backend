package com.techedge.mp.frontendbo.adapter.entities.admin.testcheckvoucher;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherCodeInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherInfo;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestCheckVoucherRequest extends AbstractBORequest implements Validable {

    private Credential                  credential;
    private TestCheckVoucherBodyRequest body;

    public TestCheckVoucherRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestCheckVoucherBodyRequest getBody() {
        return body;
    }

    public void setBody(TestCheckVoucherBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CHECK-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_CHECK_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestCheckVoucherResponse testCheckVoucherResponse = new TestCheckVoucherResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testCheckVoucher");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testCheckVoucherResponse.getStatus().setStatusCode("-1");
        }
        else {

            List<VoucherCodeDetail> voucherList = new ArrayList<VoucherCodeDetail>();

            for (VoucherCodeInfo voucherCodeInfo : this.getBody().getVoucherList()) {

                VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();

                voucherCodeDetail.setVoucherCode(voucherCodeInfo.getVoucherCode());

                voucherList.add(voucherCodeDetail);
            }

            CheckVoucherResult checkVoucherResult = new CheckVoucherResult();

            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());
                VoucherConsumerType voucherConsumerType = getEnumerationValue(VoucherConsumerType.class, this.getBody().getVoucherType());

                checkVoucherResult = getFidelityServiceRemote().checkVoucher(this.getBody().getOperationID(), voucherConsumerType, partnerType,
                        this.getBody().getRequestTimestamp(), voucherList);
            }
            catch (FidelityServiceException ex) {
                checkVoucherResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!checkVoucherResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                testCheckVoucherResponse.setCsTransactionID(checkVoucherResult.getCsTransactionID());
                testCheckVoucherResponse.getStatus().setStatusMessage(checkVoucherResult.getMessageCode());
                testCheckVoucherResponse.getStatus().setStatusCode(checkVoucherResult.getStatusCode());

                for (VoucherDetail voucherDetail : checkVoucherResult.getVoucherList()) {

                    VoucherInfo voucherInfo = new VoucherInfo();

                    voucherInfo.setConsumedValue(voucherDetail.getConsumedValue());
                    voucherInfo.setExpirationDate(voucherDetail.getExpirationDate());
                    voucherInfo.setInitialValue(voucherDetail.getInitialValue());
                    voucherInfo.setPromoCode(voucherDetail.getPromoCode());
                    voucherInfo.setPromoDescription(voucherDetail.getPromoDescription());
                    voucherInfo.setPromoDoc(voucherDetail.getPromoDoc());
                    voucherInfo.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                    voucherInfo.setVoucherCode(voucherDetail.getVoucherCode());
                    voucherInfo.setVoucherStatus(voucherDetail.getVoucherStatus());
                    voucherInfo.setVoucherType(voucherDetail.getVoucherType());
                    voucherInfo.setVoucherValue(voucherDetail.getVoucherValue());

                    testCheckVoucherResponse.getVoucherList().add(voucherInfo);
                }
            }
        }

        return testCheckVoucherResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
