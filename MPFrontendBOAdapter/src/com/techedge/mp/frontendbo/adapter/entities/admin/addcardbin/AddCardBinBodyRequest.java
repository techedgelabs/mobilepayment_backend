package com.techedge.mp.frontendbo.adapter.entities.admin.addcardbin;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AddCardBinBodyRequest implements Validable {

    private String bin;

    public AddCardBinBodyRequest() {}

    public String getBin() {
        return bin;
    }

    public void setBin(String bin) {
        this.bin = bin;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.bin == null) {
            status.setStatusCode(StatusCode.ADMIN_ADD_CARD_BIN_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_ADD_CARD_BIN_SUCCESS);

        return status;
    }

}
