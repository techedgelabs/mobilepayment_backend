package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveadminrole;

import java.util.List;

public class AdminRetrieveAdminRoleBodyResponse {

    private List<String> roles;

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

}
