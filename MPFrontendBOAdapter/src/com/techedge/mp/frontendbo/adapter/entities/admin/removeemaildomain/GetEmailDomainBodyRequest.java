package com.techedge.mp.frontendbo.adapter.entities.admin.removeemaildomain;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class GetEmailDomainBodyRequest implements Validable {

    private String email;

    public GetEmailDomainBodyRequest() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.MAIL_CREATE_SUCCESS);

        return status;
    }
}
