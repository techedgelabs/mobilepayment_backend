package com.techedge.mp.frontendbo.adapter.entities.admin.removemobilephone;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminRemoveMobilePhoneRequest extends AbstractBORequest implements Validable {

    private Status                            status = new Status();

    private Credential                        credential;
    private AdminRemoveMobilePhoneBodyRequest body;

    public AdminRemoveMobilePhoneRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminRemoveMobilePhoneBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminRemoveMobilePhoneBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REMOVE-MOBILE-PHONE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_REMOVE_MOBILE_PHONE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        AdminRemoveMobilePhoneResponse adminRemoveMobilePhoneResponse = new AdminRemoveMobilePhoneResponse();

        String response = getAdminServiceRemote().adminRemoveMobilePhone(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getMobilePhoneId());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        adminRemoveMobilePhoneResponse.setStatus(status);

        return adminRemoveMobilePhoneResponse;
    }
}
