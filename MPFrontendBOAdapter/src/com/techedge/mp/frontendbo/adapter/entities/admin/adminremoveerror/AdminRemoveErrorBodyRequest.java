package com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminRemoveErrorBodyRequest implements Validable {


    private String errorCode;

    public String getGpErrorCode() {
        return errorCode;
    }

    public void setGpErrorCode(String gpErrorCode) {
        this.errorCode = gpErrorCode;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (this.errorCode == null) {
            status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_REMOVE__INVALID_ERROR_CODE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_REMOVE__SUCCESS);

        return status;
    }

}
