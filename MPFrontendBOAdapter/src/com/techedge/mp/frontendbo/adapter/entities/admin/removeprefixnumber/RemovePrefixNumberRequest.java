package com.techedge.mp.frontendbo.adapter.entities.admin.removeprefixnumber;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RemovePrefixNumberRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private RemovePrefixNumberBodyRequest body;

    public RemovePrefixNumberRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RemovePrefixNumberBodyRequest getBody() {
        return body;
    }

    public void setBody(RemovePrefixNumberBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REMOVE-PREFIX", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(ResponseHelper.PREFIX_REMOVE_FAILURE);

            return status;

        }

        status.setStatusCode(ResponseHelper.PREFIX_REMOVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        RemovePrefixNumberResponse removePrefixNumberResponse = new RemovePrefixNumberResponse();

        String response = getAdminServiceRemote().adminRemovePrefixNumber(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getCode());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        removePrefixNumberResponse.setStatus(status);

        return removePrefixNumberResponse;
    }

}
