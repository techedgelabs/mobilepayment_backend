package com.techedge.mp.frontendbo.adapter.entities.common;

public class VoucherCodeInfo {

    private String voucherCode;

    public VoucherCodeInfo() {}

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

}
