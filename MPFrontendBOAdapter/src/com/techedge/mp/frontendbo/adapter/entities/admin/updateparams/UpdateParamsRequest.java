package com.techedge.mp.frontendbo.adapter.entities.admin.updateparams;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.AdminUpdateParamsResponse;
import com.techedge.mp.core.business.interfaces.ParamInfo;
import com.techedge.mp.core.business.interfaces.ParamUpdateInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.ParamUpdateResult;
import com.techedge.mp.frontendbo.adapter.entities.common.ParameterInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class UpdateParamsRequest extends AbstractBORequest implements Validable {

    private Status                  status = new Status();

    private Credential              credential;
    private UpdateParamsRequestBody body;

    public UpdateParamsRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateParamsRequestBody getBody() {
        return body;
    }

    public void setBody(UpdateParamsRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_PARAM_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        UpdateParamsResponse updateParamsResponse = new UpdateParamsResponse();

        List<ParamInfo> paramInfoList = new ArrayList<ParamInfo>(0);

        for (ParameterInfo parameterInfo : this.getBody().getParameterList()) {

            ParamInfo paramInfo = new ParamInfo();
            paramInfo.setKey(parameterInfo.getParam());
            paramInfo.setValue(parameterInfo.getValue());
            paramInfoList.add(paramInfo);
        }

        AdminUpdateParamsResponse adminUpdateParamsResponse = getAdminServiceRemote().adminUpdateParams(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), paramInfoList);

        for (ParamUpdateInfo paramUpdateInfo : adminUpdateParamsResponse.getParamList()) {

            ParamUpdateResult paramUpdateResult = new ParamUpdateResult();
            paramUpdateResult.setParam(paramUpdateInfo.getKey());
            paramUpdateResult.setStatusCode(paramUpdateInfo.getStatusCode());
            updateParamsResponse.getResult().add(paramUpdateResult);
        }

        status.setStatusCode(adminUpdateParamsResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(adminUpdateParamsResponse.getStatusCode()));

        updateParamsResponse.setStatus(status);

        return updateParamsResponse;

    }

}
