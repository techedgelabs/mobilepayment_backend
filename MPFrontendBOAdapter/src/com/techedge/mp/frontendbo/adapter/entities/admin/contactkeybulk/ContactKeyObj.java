package com.techedge.mp.frontendbo.adapter.entities.admin.contactkeybulk;

public class ContactKeyObj {

	private String fiscalCode;
	private String contactKey;

	public ContactKeyObj() {
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getContactKey() {
		return contactKey;
	}

	public void setContactKey(String contactKey) {
		this.contactKey = contactKey;
	}

}
