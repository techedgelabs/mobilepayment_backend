package com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.RefuelDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestSendMPTransactionNotificationRequest extends AbstractBORequest implements Validable {

    private SendCredential                               credential;
    private TestSendMPTransactionNotificationBodyRequest body;

    public TestSendMPTransactionNotificationRequest() {}

    public SendCredential getSendCredential() {
        return credential;
    }

    public void setSendCredential(SendCredential credential) {
        this.credential = credential;
    }

    public TestSendMPTransactionNotificationBodyRequest getBody() {
        return body;
    }

    public void setBody(TestSendMPTransactionNotificationBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_FIDELITY_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestSendMPTransactionResponse testSendMPTransactionNotificationResponse = new TestSendMPTransactionResponse();

        RefuelDetail refuelDetailRequest = this.getBody().getRefuelDetail();
        String requestID = this.getSendCredential().getRequestID();
        String srcTransactionID = this.getBody().getSrcTransactionID();
        String mpTransactionID = this.getBody().getMpTransactionID();
        String mpTransactionStatus = this.getBody().getMpTransactionStatus();

        com.techedge.mp.refueling.integration.entities.RefuelDetail refuelDetail = convertRefueling(refuelDetailRequest);

        String statusCode = getRefuelingNotificationServiceRemote().sendMPTransactionNotification(requestID, srcTransactionID, mpTransactionID, mpTransactionStatus, refuelDetail);

        testSendMPTransactionNotificationResponse.getStatus().setStatusCode(statusCode);
        testSendMPTransactionNotificationResponse.getStatus().setStatusMessage("");

        return testSendMPTransactionNotificationResponse;
    }

    private com.techedge.mp.refueling.integration.entities.RefuelDetail convertRefueling(RefuelDetail refuelDetailRequest) {

        com.techedge.mp.refueling.integration.entities.RefuelDetail refuelDetail = new com.techedge.mp.refueling.integration.entities.RefuelDetail();
        refuelDetail.setAmount(refuelDetailRequest.getAmount());
        refuelDetail.setFuelQuantity(refuelDetailRequest.getFuelQuantity());
        refuelDetail.setFuelType(refuelDetailRequest.getFuelType());
        refuelDetail.setProductDescription(refuelDetailRequest.getProductDescription());
        refuelDetail.setProductID(refuelDetailRequest.getProductId());
        refuelDetail.setTimestampEndRefuel(refuelDetailRequest.getTimestampEndRefuel());
        refuelDetail.setTimestampStartRefuel(refuelDetailRequest.getTimestampStartRefuel());
        return refuelDetail;
    }
}
