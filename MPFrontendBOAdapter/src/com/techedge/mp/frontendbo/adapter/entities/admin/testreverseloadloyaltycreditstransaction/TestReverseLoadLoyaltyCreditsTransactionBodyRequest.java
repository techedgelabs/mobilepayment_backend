package com.techedge.mp.frontendbo.adapter.entities.admin.testreverseloadloyaltycreditstransaction;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestReverseLoadLoyaltyCreditsTransactionBodyRequest implements Validable {

    private String operationID;
    private String operationIDtoReverse;
    private String partnerType;
    private Long   requestTimestamp;

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getOperationIDtoReverse() {
        return operationIDtoReverse;
    }

    public void setOperationIDtoReverse(String operationIDtoReverse) {
        this.operationIDtoReverse = operationIDtoReverse;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationID == null || this.operationID.trim().isEmpty() || this.operationIDtoReverse == null || this.operationIDtoReverse.trim().isEmpty()
                || this.partnerType == null || this.partnerType.trim().isEmpty() || this.requestTimestamp == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_SUCCESS);

        return status;
    }

}
