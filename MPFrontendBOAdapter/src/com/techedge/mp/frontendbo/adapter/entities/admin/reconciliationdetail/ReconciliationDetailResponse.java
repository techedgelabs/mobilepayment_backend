package com.techedge.mp.frontendbo.adapter.entities.admin.reconciliationdetail;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;


public class ReconciliationDetailResponse extends BaseResponse {
	
	private Integer processed;
	//private List<ReconciliationResult> resultList = new ArrayList<ReconciliationResult>(0);
	protected String timestampEndRefuel;
	protected double amount;
	protected Double fuelQuantity;
    protected String productID;
    protected String productDescription;
    //protected String fuelType;
    public String getTimestampEndRefuel() {
		return timestampEndRefuel;
	}
	public void setTimestampEndRefuel(String timestampEndRefuel) {
		this.timestampEndRefuel = timestampEndRefuel;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public Double getFuelQuantity() {
		return fuelQuantity;
	}
	public void setFuelQuantity(Double fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}
	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}
	
	
	
	
	public Integer getProcessed() {
		return processed;
	}
	public void setProcessed(Integer processed) {
		this.processed = processed;
	}
	
//	public List<ReconciliationResult> getResultList() {
//		return resultList;
//	}
//	public void setResultList(List<ReconciliationResult> resultList) {
//		this.resultList = resultList;
//	}
	
}