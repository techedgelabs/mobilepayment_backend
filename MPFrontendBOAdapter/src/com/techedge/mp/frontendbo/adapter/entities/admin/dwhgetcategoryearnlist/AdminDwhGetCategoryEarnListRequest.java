package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcategoryearnlist;

import java.util.Date;

import com.techedge.mp.dwh.adapter.interfaces.CategoryEarnDetail;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCategoryEarnResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminDwhGetCategoryEarnListRequest extends AbstractBORequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DWH-GET-CATEGORY-EARN-LIST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DWH_GET_CATEGORY_EARN_LIST_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        DWHGetCategoryEarnResult result = getDwhAdapterServiceRemote().getCategoryEarnList();

        AdminDwhGetCategoryEarnListResponse adminDwhGetCategoryEarnListResponse = new AdminDwhGetCategoryEarnListResponse();

        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(result.getMessageCode());
        adminDwhGetCategoryEarnListResponse.setStatus(status);

        if (result.getCategoryEarnList() != null && !result.getCategoryEarnList().isEmpty()) {

            AdminDwhGetCategoryEarnListBodyResponse adminDwhGetCategoryEarnListBodyResponse = new AdminDwhGetCategoryEarnListBodyResponse();

            for (CategoryEarnDetail categoryEarnDetail : result.getCategoryEarnList()) {

                adminDwhGetCategoryEarnListBodyResponse.getCategoryEarnDetailList().add(categoryEarnDetail);
            }

            adminDwhGetCategoryEarnListResponse.setBody(adminDwhGetCategoryEarnListBodyResponse);
        }

        return adminDwhGetCategoryEarnListResponse;
    }

}