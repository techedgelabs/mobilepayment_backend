package com.techedge.mp.frontendbo.adapter.entities.admin.reconciliationuser;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationUserData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationUserSummary;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.ReconciliationResult;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class ReconciliationUserRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private ReconciliationUserRequestBody body;

    public ReconciliationUserRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ReconciliationUserRequestBody getBody() {
        return body;
    }

    public void setBody(ReconciliationUserRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RECONCILIATION-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RECONCILIATION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        ReconciliationUserResponse reconciliationResponse = new ReconciliationUserResponse();

        ReconciliationUserSummary reconciliationUserSummary = getReconciliationServiceRemote().reconciliationUser(this.getBody().getIdList());
        status.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);

        reconciliationResponse.setStatus(status);

        reconciliationResponse.setProcessed(reconciliationUserSummary.getProcessed());

        List<ReconciliationUserData> listRedemption = this.getBody().getIdList();
        
        List<ReconciliationInfo> reconciliationInfoList = new ArrayList<>(0);
        for (ReconciliationUserData item : listRedemption) {

            if (item.getReconciliationType().equals("CRM_OFFER_ERROR")) {

                reconciliationInfoList = reconciliationUserSummary.getSummary(reconciliationUserSummary.SUMMARY_CRM_OFFER_ERROR);

                for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
                    ReconciliationResult reconciliationResult = new ReconciliationResult();
                    reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
                    reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
                    reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
                    reconciliationResponse.getResultList().add(reconciliationResult);
                }
            }
            if (item.getReconciliationType().equals("CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR")) {

                reconciliationInfoList = reconciliationUserSummary.getSummary(reconciliationUserSummary.SUMMARY_CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR);

                for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
                    ReconciliationResult reconciliationResult = new ReconciliationResult();
                    reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
                    reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
                    reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
                    reconciliationResponse.getResultList().add(reconciliationResult);
                }
            }
            if (item.getReconciliationType().equals("DWH_EVENT_ERROR")) {

                reconciliationInfoList = reconciliationUserSummary.getSummary(reconciliationUserSummary.SUMMARY_DWH_EVENT_ERROR);

                for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
                    ReconciliationResult reconciliationResult = new ReconciliationResult();
                    reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
                    reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
                    reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
                    reconciliationResponse.getResultList().add(reconciliationResult);
                }
            }
            if (item.getReconciliationType().equals("PUSH_NOTIFICATION_ERROR")) {

                reconciliationInfoList = reconciliationUserSummary.getSummary(reconciliationUserSummary.SUMMARY_PUSH_NOTIFICATION_ERROR);

                for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
                    ReconciliationResult reconciliationResult = new ReconciliationResult();
                    reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
                    reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
                    reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
                    reconciliationResponse.getResultList().add(reconciliationResult);
                }
            }
            if (item.getReconciliationType().equals("REDEMPTION_ERROR")) {

                reconciliationInfoList = reconciliationUserSummary.getSummary(reconciliationUserSummary.SUMMARY_REDEMPTION_ERROR);

                for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
                    ReconciliationResult reconciliationResult = new ReconciliationResult();
                    reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
                    reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
                    reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
                    reconciliationResponse.getResultList().add(reconciliationResult);
                }
            }
            else {
                reconciliationInfoList = reconciliationUserSummary.getSummary(reconciliationUserSummary.SUMMARY_STATUS_NOT_IDENTIFIED);

                for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
                    ReconciliationResult reconciliationResult = new ReconciliationResult();
                    reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
                    reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
                    reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
                    reconciliationResponse.getResultList().add(reconciliationResult);
                }
            }
            /*
             * ReconciliationInfoData reconciliationInfoDataResponse = getAdminServiceRemote().adminReconcileTransactions(this.getCredential().getTicketID(),
             * this.getCredential().getRequestID(), this.getBody().getTransactionList());
             * 
             * status.setStatusCode(reconciliationInfoDataResponse.getStatusCode());
             * status.setStatusMessage(prop.getProperty(reconciliationInfoDataResponse.getStatusCode()));
             * 
             * reconciliationResponse.setStatus(status);
             * 
             * if (reconciliationInfoDataResponse.getReconciliationInfoList() != null) {
             * for (ReconciliationInfo reconciliationInfo : reconciliationInfoDataResponse.getReconciliationInfoList()) {
             * 
             * ReconciliationResult reconciliationResult = new ReconciliationResult();
             * reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
             * reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
             * reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
             * 
             * reconciliationResponse.getResultList().add(reconciliationResult);
             * }
             * }
             */
        }
        return reconciliationResponse;
    }
}
