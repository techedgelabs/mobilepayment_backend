package com.techedge.mp.frontendbo.adapter.entities.admin.testgetpaymentstatus;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetPaymentStatusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestGetPaymentStatusRequest extends AbstractBORequest implements Validable {

    private Credential                    credential;
    private TestGetPaymentStatusBodyRequest body;

    public TestGetPaymentStatusRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestGetPaymentStatusBodyRequest getBody() {
        return body;
    }

    public void setBody(TestGetPaymentStatusBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-PAYMENT-STATUS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_GET_PAYMENT_STATUS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestGetPaymentStatusResponse testGetPaymentStatusResponse = new TestGetPaymentStatusResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testGetPaymentStatusResponse");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testGetPaymentStatusResponse.getStatus().setStatusCode("-1");
        }
        else {
            GetPaymentStatusResult getPaymentStatusResult = new GetPaymentStatusResult();
            
            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                getPaymentStatusResult = getPaymentServiceRemote().getPaymentStatus(this.getBody().getPaymentOperationId(), this.getBody().getOperationId(), partnerType, this.getBody().getRequestTimestamp());
            }
            catch (Exception ex) {
                getPaymentStatusResult = null;
            }

            testGetPaymentStatusResponse.setStatusCode("OK");
            testGetPaymentStatusResponse.setResult(getPaymentStatusResult);
        }

        return testGetPaymentStatusResponse;
    }
    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
