package com.techedge.mp.frontendbo.adapter.entities.admin.pushnotificationsend;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminPushNotificationSendRequest extends AbstractBORequest implements Validable {

    private Status                               status = new Status();

    private Credential                           credential;
    private AdminPushNotificationSendBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminPushNotificationSendBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminPushNotificationSendBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PUSH-NOTIFICATION-TEST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_PUSH_NOTIFICATION_TEST_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_PUSH_NOTIFICATION_TEST_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String result = getAdminServiceRemote().adminPushNotificationSend(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getUserID(),
                this.getBody().getMessageID(), this.getBody().getTitleNotification(), this.getBody().getMessageNotification());

        AdminPushNotificationSendResponse adminPushNotificationSendResponse = new AdminPushNotificationSendResponse();
        status.setStatusCode(result);
        status.setStatusMessage(prop.getProperty(result));
        adminPushNotificationSendResponse.setStatus(status);

        return adminPushNotificationSendResponse;
    }
}