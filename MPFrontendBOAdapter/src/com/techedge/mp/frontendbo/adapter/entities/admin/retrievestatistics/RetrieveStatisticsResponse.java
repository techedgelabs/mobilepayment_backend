package com.techedge.mp.frontendbo.adapter.entities.admin.retrievestatistics;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.StatisticInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;



public class RetrieveStatisticsResponse extends BaseResponse {
	
	private List<StatisticInfo> statisticList = new ArrayList<StatisticInfo>(0);

	public List<StatisticInfo> getStatisticList() {
		return statisticList;
	}

	public void setStatisticList(List<StatisticInfo> statisticList) {
		this.statisticList = statisticList;
	}
	
	

}
