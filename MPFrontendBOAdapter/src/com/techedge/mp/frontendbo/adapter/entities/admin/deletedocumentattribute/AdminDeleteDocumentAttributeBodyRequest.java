package com.techedge.mp.frontendbo.adapter.entities.admin.deletedocumentattribute;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminDeleteDocumentAttributeBodyRequest implements Validable {

    private String  checkKey;

    public String getCheckKey() {
        return checkKey;
    }

    public void setCheckKey(String checkKey) {
        this.checkKey = checkKey;
    }


    @Override
    public Status check() {
        Status status = new Status();

        if (this.checkKey == null || this.checkKey.isEmpty()
                || this.checkKey.trim().isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_DELETE_CHECK_FAILURE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_DELETE_SUCCESS);

        return status;
    }
}
