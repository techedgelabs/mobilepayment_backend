package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveblockperiod;

import java.util.List;

import com.techedge.mp.core.business.interfaces.UnavailabilityPeriod;

public class AdminRetrieveBlockPeriodBodyResponse {

    private List<UnavailabilityPeriod> blockPeriods;

    public List<UnavailabilityPeriod> getBlockPeriods() {
        return blockPeriods;
    }

    public void setBlockPeriods(List<UnavailabilityPeriod> blockPeriods) {
        this.blockPeriods = blockPeriods;
    }

}
