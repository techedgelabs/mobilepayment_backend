package com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class CreatePrefixNumberRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private CreatePrefixNumberBodyRequest body;

    public CreatePrefixNumberRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreatePrefixNumberBodyRequest getBody() {
        return body;
    }

    public void setBody(CreatePrefixNumberBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-PREFIX", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(ResponseHelper.PREFIX_CREATE_FAILURE);

            return status;

        }

        status.setStatusCode(ResponseHelper.PREFIX_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        CreatePrefixNumberResponse createPrefixNumberResponse = new CreatePrefixNumberResponse();

        String response = getAdminServiceRemote().adminCreatePrefixNumber(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getCode());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        createPrefixNumberResponse.setStatus(status);

        return createPrefixNumberResponse;
    }

}
