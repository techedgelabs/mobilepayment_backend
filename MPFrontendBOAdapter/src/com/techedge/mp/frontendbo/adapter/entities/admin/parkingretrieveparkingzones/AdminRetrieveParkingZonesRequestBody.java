package com.techedge.mp.frontendbo.adapter.entities.admin.parkingretrieveparkingzones;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminRetrieveParkingZonesRequestBody implements Validable {

    private String lang;
    private String latitude;
    private String longitude;
    private String accuracy;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(String accuracy) {
        this.accuracy = accuracy;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.lang == null || this.latitude == null || this.longitude == null || this.accuracy == null ||
            this.lang.isEmpty() || this.latitude.isEmpty() || this.longitude.isEmpty() || this.accuracy.isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);
        return status;
    }

}
