package com.techedge.mp.frontendbo.adapter.entities.admin.updatetransaction;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdateTransactionRequestBody implements Validable {
	
	private String transactionId;
	private String finalStatusType;
	private Boolean GFGNotification;
	private Boolean confirmed;
	private Integer reconciliationAttemptsLeft;
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}
	
	public String getFinalStatusType() {
		return finalStatusType;
	}
	public void setFinalStatusType(String finalStatusType) {
		this.finalStatusType = finalStatusType;
	}

	public Boolean getGFGNotification() {
		return GFGNotification;
	}
	public void setGFGNotification(Boolean gFGNotification) {
		GFGNotification = gFGNotification;
	}

	public Boolean getConfirmed() {
		return confirmed;
	}
	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}

	public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }
    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }
    
    @Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.transactionId == null) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_ERROR_PARAMETERS);
			return status;
		}
		else {
			
			if(this.transactionId.length() > 40) {
				
				status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_ERROR_PARAMETERS);
				return status;
			}
		}
		
		if(this.finalStatusType != null && this.finalStatusType.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_ERROR_PARAMETERS);
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_SUCCESS);

		return status;
	}
	
}
