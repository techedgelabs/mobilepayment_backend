package com.techedge.mp.frontendbo.adapter.entities.admin.reconciliation;

import java.util.List;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationTransactionSummary;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.ReconciliationResult;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class ReconciliationRequest extends AbstractBORequest implements Validable {

    private Status                    status = new Status();

    private Credential                credential;
    private ReconciliationRequestBody body;

    public ReconciliationRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ReconciliationRequestBody getBody() {
        return body;
    }

    public void setBody(ReconciliationRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RECONCILIATION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RECONCILIATION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        ReconciliationResponse reconciliationResponse = new ReconciliationResponse();

        ReconciliationTransactionSummary ReconciliationTransactionSummary = getReconciliationServiceRemote().reconciliationTransaction(this.getBody().getTransactionList());
        status.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);

        reconciliationResponse.setStatus(status);

        reconciliationResponse.setProcessed(ReconciliationTransactionSummary.getProcessed());

        List<ReconciliationInfo> reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_PREPAID_MISSING_PAYAUTH_DELETE_AFTER_REFUEL);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_PREPAID_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_PREPAID_MISSING_PAYMENT);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_PREPAID_MISSING_NOTIFICATION);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_PREPAID_ERROR);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_PREPAID_ABEND);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }
        
        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_PREPAID_VOUCHER_ERROR);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_POSTPAID_PAYMENT_ERROR);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_POSTPAID_VOUCHER_ERROR);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_POSTPAID_LOYALTY_ERROR);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_VOUCHER_PAYMENT_AUTH_ERROR);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_VOUCHER_PAYMENT_SETTLE_ERROR);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_VOUCHER_VOUCHER_CREATE_ERROR);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        reconciliationInfoList = ReconciliationTransactionSummary.getSummary(ReconciliationTransactionSummary.SUMMARY_VOUCHER_VOUCHER_DELETE_ERROR);

        for (ReconciliationInfo reconciliationInfo : reconciliationInfoList) {
            ReconciliationResult reconciliationResult = new ReconciliationResult();
            reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
            reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
            reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
            reconciliationResponse.getResultList().add(reconciliationResult);
        }

        /*
         * ReconciliationInfoData reconciliationInfoDataResponse = getAdminServiceRemote().adminReconcileTransactions(this.getCredential().getTicketID(),
         * this.getCredential().getRequestID(), this.getBody().getTransactionList());
         * 
         * status.setStatusCode(reconciliationInfoDataResponse.getStatusCode());
         * status.setStatusMessage(prop.getProperty(reconciliationInfoDataResponse.getStatusCode()));
         * 
         * reconciliationResponse.setStatus(status);
         * 
         * if (reconciliationInfoDataResponse.getReconciliationInfoList() != null) {
         * for (ReconciliationInfo reconciliationInfo : reconciliationInfoDataResponse.getReconciliationInfoList()) {
         * 
         * ReconciliationResult reconciliationResult = new ReconciliationResult();
         * reconciliationResult.setFinalStatusType(reconciliationInfo.getFinalStatusType());
         * reconciliationResult.setStatusCode(reconciliationInfo.getStatusCode());
         * reconciliationResult.setTransactionID(reconciliationInfo.getTransactionID());
         * 
         * reconciliationResponse.getResultList().add(reconciliationResult);
         * }
         * }
         */

        return reconciliationResponse;
    }

}
