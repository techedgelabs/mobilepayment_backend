package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveadmin;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminRetrieveRequestBody implements Validable {

    private Long    id;
    private String  email;
    private Integer status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_SUCCESS);

        return status;
    }

}
