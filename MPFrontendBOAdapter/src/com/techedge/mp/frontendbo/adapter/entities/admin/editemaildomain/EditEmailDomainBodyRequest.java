package com.techedge.mp.frontendbo.adapter.entities.admin.editemaildomain;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class EditEmailDomainBodyRequest implements Validable {

    private String id;
    private String email;

    public EditEmailDomainBodyRequest() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.email == null || this.id == null || this.id.equals("")) {

            status.setStatusCode(StatusCode.MAIL_EDIT_FAILURE);
            status.setStatusMessage("ID non valido");

            return status;
        }

        status.setStatusCode(StatusCode.MAIL_EDIT_SUCCESS);
        status.setStatusMessage("Mail modificata con successo");

        return status;
    }
}
