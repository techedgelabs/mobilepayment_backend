package com.techedge.mp.frontendbo.adapter.entities.admin.insertpaymentmethodrefuelinguser;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminInsertPaymentMethodRefuelingUserBodyRequest implements Validable {

    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String role) {
        this.username = role;
    }

    public AdminInsertPaymentMethodRefuelingUserBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.username == null || this.username.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_CREATE_REFUELING_USER_ERROR_PARAMETERS);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_REFUELING_USER_SUCCESS);

        return status;
    }

}
