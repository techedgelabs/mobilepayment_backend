package com.techedge.mp.frontendbo.adapter.entities.admin.testauthorizationplus;

import com.techedge.mp.fidelity.adapter.business.interfaces.AuthorizationPlusResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestAuthorizationPlusResponse extends BaseResponse {

    private String               statusCode;
    private AuthorizationPlusResult result;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public AuthorizationPlusResult getResult() {
        return result;
    }

    public void setResult(AuthorizationPlusResult result) {
        this.result = result;
    }

}
