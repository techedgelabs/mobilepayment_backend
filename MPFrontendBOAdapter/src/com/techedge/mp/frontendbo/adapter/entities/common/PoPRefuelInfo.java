package com.techedge.mp.frontendbo.adapter.entities.common;

import java.util.Date;


public class PoPRefuelInfo {
	
private String fuelType;
	
	private String productId;
	private String productDescription;
	private Double fuelQuantity;
	private Double fuelAmount;
	private String pumpId;
	private Integer pumpNumber;
	private String refuelMode;
	private Date timestampEndRefuel;
	

	public PoPRefuelInfo() {}


	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Double getFuelQuantity() {
		return fuelQuantity;
	}
	public void setFuelQuantity(Double fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}

	public Double getFuelAmount() {
		return fuelAmount;
	}
	public void setFuelAmount(Double fuelAmount) {
		this.fuelAmount = fuelAmount;
	}

	public String getPumpId() {
		return pumpId;
	}
	public void setPumpId(String pumpId) {
		this.pumpId = pumpId;
	}

	public Integer getPumpNumber() {
		return pumpNumber;
	}
	public void setPumpNumber(Integer pumpNumber) {
		this.pumpNumber = pumpNumber;
	}

	public String getRefuelMode() {
		return refuelMode;
	}
	public void setRefuelMode(String refuelMode) {
		this.refuelMode = refuelMode;
	}

	public Date getTimestampEndRefuel() {
		return timestampEndRefuel;
	}
	public void setTimestampEndRefuel(Date timestampEndRefuel) {
		this.timestampEndRefuel = timestampEndRefuel;
	}
}
