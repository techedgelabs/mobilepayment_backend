package com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.PaymentInfo;


public class FindPaymentMethodResponse extends BaseResponse {

	private PaymentInfo paymentInfo;

	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}
}
