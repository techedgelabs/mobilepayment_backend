package com.techedge.mp.frontendbo.adapter.entities.admin.testscheduler;

import java.lang.reflect.Method;

import com.techedge.mp.core.business.SchedulerServiceRemote;
import com.techedge.mp.core.business.interfaces.CrmOutboundProcessResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;

public class TestSchedulerRequest extends AbstractBORequest implements Validable {

    private Status                   status = new Status();

    private Credential               credential;
    private TestSchedulerRequestBody body;

    public TestSchedulerRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestSchedulerRequestBody getBody() {
        return body;
    }

    public void setBody(TestSchedulerRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("TEST_SCHEDULER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_SCHEDULER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestSchedulerResponse testSchedulerResponse = new TestSchedulerResponse();

        String action = this.getBody().getAction();
        //List<String> params = this.getBody().getParams();
        Class[] parameterTypes = new Class[0];

        try {
            SchedulerServiceRemote schedulerService = EJBHomeCache.getInstance().getSchedulerService();
            Method methodAction = schedulerService.getClass().getDeclaredMethod(action, parameterTypes);

            Object result = methodAction.invoke(schedulerService);
            
            if (result != null && result.getClass().equals(CrmOutboundProcessResponse.class)) {
                CrmOutboundProcessResponse crmOutboundProcessResponse = (CrmOutboundProcessResponse) result;
                status.setStatusCode(crmOutboundProcessResponse.getStatusCode());
                status.setStatusMessage(crmOutboundProcessResponse.getStatusMessage());
            }
            else {
                status.setStatusCode(StatusCode.ADMIN_TEST_SCHEDULER_SUCCESS);
                status.setStatusMessage("Metodo '" + action + "' del servizio di schedulazione eseguito");
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            status.setStatusCode(StatusCode.ADMIN_TEST_SCHEDULER_FAILURE);
            status.setStatusMessage(ex.toString());
        }

        testSchedulerResponse.setStatus(status);

        return testSchedulerResponse;
    }

}
