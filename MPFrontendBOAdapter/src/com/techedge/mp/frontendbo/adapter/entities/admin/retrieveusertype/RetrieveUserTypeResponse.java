package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusertype;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class RetrieveUserTypeResponse extends BaseResponse {

    private List<RetrieveUserTypeBodyResponse> userType = new ArrayList<RetrieveUserTypeBodyResponse>(0);

    public List<RetrieveUserTypeBodyResponse> getUserType() {
        return userType;
    }

    public void setUserType(List<RetrieveUserTypeBodyResponse> userType) {
        this.userType = userType;
    }

}
