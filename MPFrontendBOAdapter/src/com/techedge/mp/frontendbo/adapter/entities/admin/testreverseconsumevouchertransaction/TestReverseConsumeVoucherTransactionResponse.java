package com.techedge.mp.frontendbo.adapter.entities.admin.testreverseconsumevouchertransaction;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestReverseConsumeVoucherTransactionResponse extends BaseResponse{

    private String csTransactionID;

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }
}
