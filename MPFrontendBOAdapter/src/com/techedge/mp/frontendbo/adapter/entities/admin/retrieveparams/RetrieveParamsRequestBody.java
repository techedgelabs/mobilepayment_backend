package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveparams;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RetrieveParamsRequestBody implements Validable {
	

	private String param;
	
	



	@Override
	public Status check() {
		
		Status status = new Status();
		
//		if(this.id == null) {
//			
//			status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
//			return status;
//		}
		
		status.setStatusCode(StatusCode.ADMIN_RETRIEVE_PARAMS_SUCCESS);

		return status;
	}





	public String getParam() {
		return param;
	}


	public void setParam(String param) {
		this.param = param;
	}
	
}
