package com.techedge.mp.frontendbo.adapter.entities.common;

import com.techedge.mp.core.business.interfaces.Station;

public class StationData extends Station {

    /**
     * 
     */
    private static final long serialVersionUID = 2510571432979566217L;

    @Override
    public Boolean getNewAcquirerActive() {
        return newAcquirerActive;
    }

    @Override
    public Boolean getRefuelingActive() {
        return refuelingActive;
    }

    @Override
    public Boolean getLoyaltyActive() {
        return loyaltyActive;
    }
    
    @Override
    public Boolean getVoucherActive() {
        return loyaltyActive;
    }
    
    @Override
    public Boolean getBusinessActive() {
        return businessActive;
    }

    public Station toStation() {
        
        Station station = new Station();
        station.setId(getId());
        station.setStationID(getStationID());
        station.setBeaconCode(getBeaconCode());
        station.setAddress(getAddress());
        station.setCity(getCity());
        station.setCountry(getCountry());
        station.setProvince(getProvince());
        station.setLatitude(getLatitude());
        station.setLongitude(getLongitude());
        station.setOilShopLogin(getOilShopLogin());
        station.setOilAcquirerID(getOilAcquirerID());
        station.setNoOilShopLogin(getNoOilShopLogin());
        station.setNoOilAcquirerID(getNoOilAcquirerID());
        station.setStationStatus(getStationStatus());
        station.setPrepaidActive(getPrepaidActive());
        station.setPostpaidActive(getPostpaidActive());
        station.setShopActive(getShopActive());
        station.setDataAcquirer(getDataAcquirer());
        station.setNewAcquirerActive(newAcquirerActive);
        station.setRefuelingActive(refuelingActive);
        station.setLoyaltyActive(loyaltyActive);
        if (businessActive == null) {
            station.setBusinessActive(Boolean.FALSE);
        }
        else {
            station.setBusinessActive(businessActive);
        }
        if (voucherActive == null) {
            station.setVoucherActive(Boolean.FALSE);
        }
        else {
            station.setVoucherActive(voucherActive);
        }
        
        return station;
    }
    
}
