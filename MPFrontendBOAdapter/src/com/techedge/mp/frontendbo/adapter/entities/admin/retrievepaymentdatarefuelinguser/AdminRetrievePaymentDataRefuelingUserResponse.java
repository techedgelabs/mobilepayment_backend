package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepaymentdatarefuelinguser;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminRetrievePaymentDataRefuelingUserResponse extends BaseResponse{
    private AdminRetrievePaymentDataRefuelingUserBodyResponse body;

    public AdminRetrievePaymentDataRefuelingUserBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminRetrievePaymentDataRefuelingUserBodyResponse body) {
        this.body = body;
    }

}
