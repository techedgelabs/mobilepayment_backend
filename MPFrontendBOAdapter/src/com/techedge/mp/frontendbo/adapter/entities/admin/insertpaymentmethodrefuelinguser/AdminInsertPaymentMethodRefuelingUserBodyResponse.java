package com.techedge.mp.frontendbo.adapter.entities.admin.insertpaymentmethodrefuelinguser;

import com.techedge.mp.core.business.interfaces.PaymentMethod;

public class AdminInsertPaymentMethodRefuelingUserBodyResponse {

    private String        redirectUrl;
    private PaymentMethod paymentMethod;

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String role) {
        this.redirectUrl = role;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public AdminInsertPaymentMethodRefuelingUserBodyResponse() {}

}
