package com.techedge.mp.frontendbo.adapter.entities.admin.createprovince;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ProvinceData;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminCreateProvinceBodyRequest implements Validable {

    private List<ProvinceData> provinces = new ArrayList<ProvinceData>(0);

    public List<ProvinceData> getProvinces() {
        return provinces;
    }

    public void setProvinces(List<ProvinceData> provinces) {
        this.provinces = provinces;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (this.provinces == null || this.provinces.isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_CREATE_PROVINCE_INVALID_PARAMETER);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_PROVINCE_SUCCESS);

        return status;
    }

}
