package com.techedge.mp.frontendbo.adapter.entities.admin;

import java.lang.reflect.Field;

import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RootRequest {

    public AbstractBORequest getAbstractRequest() throws IllegalArgumentException, IllegalAccessException {

        Field fieldFound = null;

        for (Field field : this.getClass().getDeclaredFields()) {
            if (field.get(this) != null) {
                fieldFound = field;
                break;
            }
        }

        if (fieldFound == null) {
            return null;
        }

        AbstractBORequest request = (AbstractBORequest) fieldFound.get(this);

        return request;

    }
}
