package com.techedge.mp.frontendbo.adapter.entities.admin.createdocument;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminCreateDocumentRequest extends AbstractBORequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;
    private AdminCreateDocumentBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminCreateDocumentBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminCreateDocumentBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-DOCUMENT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_DOCUMENT_CREATE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String createDocumentResult = getAdminServiceRemote().adminCreateDocument(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getDocumentKey(), this.getBody().getPosition(), this.getBody().getTemplateFile(), this.getBody().getTitle(), this.getBody().getSubtitle(), this.getBody().getAttributes(),
                this.getBody().getUserCategory(), this.getBody().getGroupCategory());

        AdminCreateDocumentResponse createDocumentResponse = new AdminCreateDocumentResponse();
        status.setStatusCode(createDocumentResult);
        status.setStatusMessage(prop.getProperty(createDocumentResult));
        createDocumentResponse.setStatus(status);

        return createDocumentResponse;
    }

}