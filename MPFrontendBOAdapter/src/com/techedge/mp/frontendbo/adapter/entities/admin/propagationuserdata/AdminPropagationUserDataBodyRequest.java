package com.techedge.mp.frontendbo.adapter.entities.admin.propagationuserdata;

import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminPropagationUserDataBodyRequest implements Validable {

    private Long userID;
    private List<String> fiscalCode;

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }
    
    public List<String> getFiscalCode() {
        return fiscalCode;
    }
    
    public void setFiscalCode(List<String> fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if ((this.userID == null || this.userID == 0) && (fiscalCode == null || fiscalCode.isEmpty())) {
            status.setStatusCode(StatusCode.ADMIN_PROPAGATION_USERDATA_INVALID_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_PROPAGATION_USERDATA_SUCCESS);

        return status;
    }

}
