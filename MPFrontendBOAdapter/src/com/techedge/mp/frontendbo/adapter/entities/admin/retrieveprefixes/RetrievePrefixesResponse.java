package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveprefixes;

import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class RetrievePrefixesResponse extends BaseResponse {

    private List<String> prefixes;

    public List<String> getPrefixes() {
        return prefixes;
    }

    public void setPrefixes(List<String> prefixes) {
        this.prefixes = prefixes;
    }

}
