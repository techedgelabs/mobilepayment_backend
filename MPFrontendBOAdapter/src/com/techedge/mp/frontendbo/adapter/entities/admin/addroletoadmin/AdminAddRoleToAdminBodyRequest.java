package com.techedge.mp.frontendbo.adapter.entities.admin.addroletoadmin;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminAddRoleToAdminBodyRequest implements Validable {

    private String role;
    private String admin;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public AdminAddRoleToAdminBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.admin == null || this.admin.isEmpty() || this.role == null || this.role.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_ADD_ROLE_TO_ADMIN_CHECK_FAILURE);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_ADD_ROLE_TO_ADMIN_SUCCESS);

        return status;
    }

}
