package com.techedge.mp.frontendbo.adapter.entities.admin.testauthorizationplus;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.interfaces.AuthorizationPlusResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestAuthorizationPlusRequest extends AbstractBORequest implements Validable {

    private Credential                    credential;
    private TestAuthorizationPlusBodyRequest body;

    public TestAuthorizationPlusRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestAuthorizationPlusBodyRequest getBody() {
        return body;
    }

    public void setBody(TestAuthorizationPlusBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("AUTHORIZATION-PLUS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_AUTHORIZATION_PLUS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestAuthorizationPlusResponse testAuthorizationPlusResponse = new TestAuthorizationPlusResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testAuthorizationPlusResponse");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testAuthorizationPlusResponse.getStatus().setStatusCode("-1");
        }
        else {
        	AuthorizationPlusResult authorizationPlusResult = new AuthorizationPlusResult();

            try {
            	System.out.println("OK FATTO: " +this.getBody().getRequest().getAccessToken());

                authorizationPlusResult = getAuthorizationPlusServiceRemote().authorizationPlus(this.getBody().getRequest().getAccessToken());
            }
            catch (Exception ex) {
            	System.out.println("ERROR: " + ex );
                authorizationPlusResult = null;
            }

            testAuthorizationPlusResponse.setStatusCode("OK");
            testAuthorizationPlusResponse.setResult(authorizationPlusResult);
        }

        return testAuthorizationPlusResponse;
    }

}
