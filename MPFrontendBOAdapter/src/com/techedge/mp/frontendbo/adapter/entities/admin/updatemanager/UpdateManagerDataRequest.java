package com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager;

import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.frontendbo.adapter.entities.common.ManagerData;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdateManagerDataRequest extends ManagerData implements Validable {

    private Long id;
    private Integer status;

    public UpdateManagerDataRequest() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public Manager getManager() {
        Manager manager = super.getManager();
        manager.setId(this.id);
        manager.setStatus(this.status);

        return manager;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (getId() == null || getId().longValue() <= 0) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_MANAGER_INVALID_PARAMETERS);

            return status;
        }

        if (getUsername() == null || getUsername().equals("")) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_MANAGER_INVALID_PARAMETERS);

            return status;
        }

        if (getEmail() == null || getEmail().equals("")) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_MANAGER_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_MANAGER_SUCCESS);

        return status;

    }

}
