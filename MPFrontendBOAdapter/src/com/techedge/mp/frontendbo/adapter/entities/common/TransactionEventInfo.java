package com.techedge.mp.frontendbo.adapter.entities.common;


public class TransactionEventInfo {
	
	private Integer sequenceId;
	private String eventType;
	private Double eventAmount;
	private String transactionResult;
	private String errorCode;
	private String errorDescription;


	public TransactionEventInfo() {}

	public Integer getSequenceId() {
		return sequenceId;
	}
	public void setSequenceId(Integer sequenceId) {
		this.sequenceId = sequenceId;
	}

	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}

	public Double getEventAmount() {
		return eventAmount;
	}
	public void setEventAmount(Double eventAmount) {
		this.eventAmount = eventAmount;
	}

	public String getTransactionResult() {
		return transactionResult;
	}
	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}

	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
}
