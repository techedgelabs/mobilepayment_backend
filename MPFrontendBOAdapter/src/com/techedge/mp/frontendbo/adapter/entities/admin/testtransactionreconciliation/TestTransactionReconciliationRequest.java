package com.techedge.mp.frontendbo.adapter.entities.admin.testtransactionreconciliation;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionReconciliationResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;

public class TestTransactionReconciliationRequest extends AbstractBORequest implements Validable {

    private Credential                     credential;

    private final static String            PARAM_PROXY_HOST     = "PROXY_HOST";
    private final static String            PARAM_PROXY_PORT     = "PROXY_PORT";
    private final static String            PARAM_PROXY_NO_HOSTS = "PROXY_NO_HOSTS";
    //DA MODIFICARE
    private TestTransactionReconciliationRequestBody body;

    public TestTransactionReconciliationRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestTransactionReconciliationRequestBody getBody() {
        return body;
    }

    public void setBody(TestTransactionReconciliationRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-USERS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        TestTransactionReconciliationResponse testTransactionReconciliationResponse = new TestTransactionReconciliationResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testRetrieveStation");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testTransactionReconciliationResponse.getStatus().setStatusCode("-1");
        }
        else {

            ForecourtInfoServiceRemote forecourt;
            try {
                forecourt = EJBHomeCache.getInstance().getForecourtInfoService();

                ParametersServiceRemote parametersService = EJBHomeCache.getInstance().getParametersService();

                String proxyHost = parametersService.getParamValue(PARAM_PROXY_HOST);
                String proxyPort = parametersService.getParamValue(PARAM_PROXY_PORT);
                String proxyNoHosts = parametersService.getParamValue(PARAM_PROXY_NO_HOSTS);

                Proxy proxy = new Proxy(proxyHost, proxyPort, proxyNoHosts);
                proxy.setHttp();

                List<String> transactionIdList = new ArrayList<String>();
                transactionIdList.add(this.getBody().getTransactionID());
                String requestID = String.valueOf(new Date().getTime());

                TransactionReconciliationResponse response = forecourt.transactionReconciliation(requestID, transactionIdList);

                TestTransactionReconciliationResponseBody testTransactionReconciliationResponseBody = new TestTransactionReconciliationResponseBody();
                testTransactionReconciliationResponseBody.setTransactionReconciliationResponse(response);

                testTransactionReconciliationResponse.setBody(testTransactionReconciliationResponseBody);;
                testTransactionReconciliationResponse.getStatus().setStatusCode("200");
                testTransactionReconciliationResponse.getStatus().setStatusMessage("OK");
            }
            catch (InterfaceNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (ParameterNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        return testTransactionReconciliationResponse;
    }
}
