package com.techedge.mp.frontendbo.adapter.entities.admin.updatecities;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminUpdateCitiesBodyRequest implements Validable {

    private String fileStreamBase64;
    private String columnSeparator;
    private String rowSeparator;
    private String skipFirstRow;
    private String deleteAllRows;

    public AdminUpdateCitiesBodyRequest() {}

    public String getFileStreamBase64() {
        return fileStreamBase64;
    }

    public void setFileStreamBase64(String fileStreamBase64) {
        this.fileStreamBase64 = fileStreamBase64;
    }

    public String getColumnSeparator() {
        return columnSeparator;
    }

    public void setColumnSeparator(String columnSeparator) {
        this.columnSeparator = columnSeparator;
    }

    public String getRowSeparator() {
        return rowSeparator;
    }

    public void setRowSeparator(String rowSeparator) {
        this.rowSeparator = rowSeparator;
    }

    public String getSkipFirstRow() {
        return skipFirstRow;
    }

    public void setSkipFirstRow(String skipFirstRow) {
        this.skipFirstRow = skipFirstRow;
    }

    public String getDeleteAllRows() {
        return deleteAllRows;
    }

    public void setDeleteAllRows(String deleteAllRows) {
        this.deleteAllRows = deleteAllRows;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.fileStreamBase64 == null || this.fileStreamBase64.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_CITIES_INVALID_FILE);
            return status;

        }

        if ((this.columnSeparator == null || this.columnSeparator.isEmpty())
                || (this.rowSeparator == null || this.rowSeparator.isEmpty())
                || (this.skipFirstRow == null || this.skipFirstRow.isEmpty())
                || (this.deleteAllRows == null || this.deleteAllRows.isEmpty())) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_CITIES_INVALID_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_CITIES_SUCCESS);

        return status;
    }

}
