package com.techedge.mp.frontendbo.adapter.entities.admin.deleteuser;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class DeleteUserRequestBody implements Validable {
	
	private Long id;
	private String creationDateStart;
	private String creationDateEnd;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}



	public String getCreationDateStart() {
		return creationDateStart;
	}
	public void setCreationDateStart(String creationDateStart) {
		this.creationDateStart = creationDateStart;
	}
	public String getCreationDateEnd() {
		return creationDateEnd;
	}
	public void setCreationDateEnd(String creationDateEnd) {
		this.creationDateEnd = creationDateEnd;
	}
	@Override
	public Status check() {
		
		Status status = new Status();
		
//		if(this.id == null) {
//			
//			status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
//			return status;
//		}
		
		status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_SUCCESS);

		return status;
	}
	
}
