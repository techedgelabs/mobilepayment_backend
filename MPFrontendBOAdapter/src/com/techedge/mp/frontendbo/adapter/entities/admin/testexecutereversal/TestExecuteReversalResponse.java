package com.techedge.mp.frontendbo.adapter.entities.admin.testexecutereversal;

import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteReversalResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestExecuteReversalResponse extends BaseResponse {

    private String               statusCode;
    private ExecuteReversalResult result;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ExecuteReversalResult getResult() {
        return result;
    }

    public void setResult(ExecuteReversalResult result) {
        this.result = result;
    }

}
