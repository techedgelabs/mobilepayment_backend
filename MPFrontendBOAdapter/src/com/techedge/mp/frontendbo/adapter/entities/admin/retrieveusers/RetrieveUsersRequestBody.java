package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers;

import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RetrieveUsersRequestBody implements Validable {
	
	private Long id;
	private String firstName;
	private String lastName;
	private String fiscalCode;
	private String email;
	private Integer status;
	private String externalUserId;
	private Double minAvailableCap;
	private Double maxAvailableCap;
	private Double minEffectiveCap;
	private Double maxEffectiveCap;
	private Long creationDateStart;
	private Long creationDateEnd;
	private Integer userType;
	

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}
	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getExternalUserId() {
		return externalUserId;
	}
	public void setExternalUserId(String externalUserId) {
		this.externalUserId = externalUserId;
	}

	public Double getMinAvailableCap() {
		return minAvailableCap;
	}
	public void setMinAvailableCap(Double minAvailableCap) {
		this.minAvailableCap = minAvailableCap;
	}

	public Double getMaxAvailableCap() {
		return maxAvailableCap;
	}
	public void setMaxAvailableCap(Double maxAvailableCap) {
		this.maxAvailableCap = maxAvailableCap;
	}

	public Double getMinEffectiveCap() {
		return minEffectiveCap;
	}
	public void setMinEffectiveCap(Double minEffectiveCap) {
		this.minEffectiveCap = minEffectiveCap;
	}

	public Double getMaxEffectiveCap() {
		return maxEffectiveCap;
	}
	public void setMaxEffectiveCap(Double maxEffectiveCap) {
		this.maxEffectiveCap = maxEffectiveCap;
	}

	public Long getCreationDateStart() {
		return creationDateStart;
	}
	public void setCreationDateStart(Long creationDateStart) {
		this.creationDateStart = creationDateStart;
	}
	
	public Long getCreationDateEnd() {
		return creationDateEnd;
	}
	public void setCreationDateEnd(Long creationDateEnd) {
		this.creationDateEnd = creationDateEnd;
	}
	
	public Integer getUserType() {
        return userType;
    }
    public void setUserType(Integer userType) {
        this.userType = userType;
    }
    
    @Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.firstName != null && this.firstName.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.lastName != null && this.lastName.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.fiscalCode != null && this.fiscalCode.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.email != null && this.email.length() > 100) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.status != null &&
				( this.status != Admin.ADMIN_STATUS_BLOCKED &&
				  this.status != Admin.ADMIN_STATUS_NEW &&
				  this.status != Admin.ADMIN_STATUS_VERIFIED ) ) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.externalUserId != null && this.externalUserId.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.userType != null &&
                ( this.userType != User.USER_TYPE_CUSTOMER &&
                  this.userType != User.USER_TYPE_SERVICE &&
                  this.userType != User.USER_TYPE_TESTER &&
                  this.userType != User.USER_TYPE_REFUELING &&
                  this.userType != User.USER_TYPE_VOUCHER_TESTER &&
                  this.userType != User.USER_TYPE_NEW_ACQUIRER_TESTER &&
                  this.userType != User.USER_TYPE_REFUELING_NEW_ACQUIRER &&
                  this.userType != User.USER_TYPE_BUSINESS &&
                  this.userType != User.USER_TYPE_REFUELING_OAUTH2) ) {
            
            status.setStatusCode(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS);
            return status;
        }
		
		
		status.setStatusCode(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_SUCCESS);

		return status;
	}
	
}
