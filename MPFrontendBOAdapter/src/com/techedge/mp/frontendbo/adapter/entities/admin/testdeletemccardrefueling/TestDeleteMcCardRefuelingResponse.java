package com.techedge.mp.frontendbo.adapter.entities.admin.testdeletemccardrefueling;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestDeleteMcCardRefuelingResponse extends BaseResponse {

    private String           transactionId;
    private String           code;
    private String           message;
    private String           _status;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String get_status() {
        return _status;
    }

    public void set_status(String _status) {
        this._status = _status;
    }

}
