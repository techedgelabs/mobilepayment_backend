package com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestPreAuthorizationConsumeVoucherRequestBody implements Validable {

    private String          operationID;
    private String          voucherType;
    private String          partnerType;
    private String          productType;
    private Long            requestTimestamp;
    private String          mpTransactionID;
    private String          language;
    private Double          amount;
    private String          stationID;
    List<VoucherCodeDetail> voucherList = new ArrayList<VoucherCodeDetail>(0);

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getVoucherConsumerType() {
        return voucherType;
    }

    public void setVoucherConsumerType(String voucherType) {
        this.voucherType = voucherType;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public List<VoucherCodeDetail> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<VoucherCodeDetail> voucherList) {
        this.voucherList = voucherList;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationID == null || this.operationID.trim().isEmpty() || this.voucherType == null || this.voucherType.isEmpty() || this.partnerType == null
                || this.partnerType.isEmpty() || this.requestTimestamp == null || this.productType == null || this.productType.isEmpty() || this.amount == null
                || this.stationID == null || this.stationID.trim().isEmpty() || this.mpTransactionID == null || this.mpTransactionID.trim().isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_TEST_PRE_AUTHORIZATION_CONSUME_VOUCHER_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS);

        return status;
    }

}
