package com.techedge.mp.frontendbo.adapter.entities.admin.testcrmsfgetmission;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestCrmSfGetMissionBodyRequest implements Validable {

	private String fiscalCode;
	private String date;
	private Boolean notificationFlag;
	private Boolean paymentCardFlag;
	private String brand;
	private String cluster;

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Boolean getNotificationFlag() {
		return notificationFlag;
	}

	public void setNotificationFlag(Boolean notificationFlag) {
		this.notificationFlag = notificationFlag;
	}

	public Boolean getPaymentCardFlag() {
		return paymentCardFlag;
	}

	public void setPaymentCardFlag(Boolean paymentCardFlag) {
		this.paymentCardFlag = paymentCardFlag;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCluster() {
		return cluster;
	}

	public void setCluster(String cluster) {
		this.cluster = cluster;
	}

	@Override
	public Status check() {

		Status status = new Status();

		/*
		 * if (this.operationID == null || this.operationID.trim().isEmpty()) {
		 * 
		 * status.setStatusCode(StatusCode.
		 * ADMIN_TEST_CRMSF_NOTIFY_EVENT_INVALID_REQUEST);
		 * 
		 * return status; }
		 */

		status.setStatusCode(StatusCode.ADMIN_TEST_CRMSF_NOTIFY_EVENT_SUCCESS);

		return status;
	}

}
