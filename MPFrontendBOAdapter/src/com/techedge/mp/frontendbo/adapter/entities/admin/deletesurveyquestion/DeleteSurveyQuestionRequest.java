package com.techedge.mp.frontendbo.adapter.entities.admin.deletesurveyquestion;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class DeleteSurveyQuestionRequest extends AbstractBORequest implements Validable {

    private Status                          status = new Status();

    private Credential                      credential;
    private DeleteSurveyQuestionBodyRequest body;

    public DeleteSurveyQuestionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public DeleteSurveyQuestionBodyRequest getBody() {
        return body;
    }

    public void setBody(DeleteSurveyQuestionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DELETE-SURVEY-QUESTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_SURVEY_QUESTION_ADD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        DeleteSurveyQuestionBodyRequest deleteSurveyQuestionBodyRequest = this.getBody();
        DeleteSurveyQuestionResponse deleteSurveyQuestionResponse = new DeleteSurveyQuestionResponse();

        String response = getAdminServiceRemote().adminSurveyQuestionDelete(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                deleteSurveyQuestionBodyRequest.getCode());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        deleteSurveyQuestionResponse.setStatus(status);

        return deleteSurveyQuestionResponse;
    }

}
