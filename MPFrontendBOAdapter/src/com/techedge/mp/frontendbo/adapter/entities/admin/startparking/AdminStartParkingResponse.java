package com.techedge.mp.frontendbo.adapter.entities.admin.startparking;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminStartParkingResponse extends BaseResponse{
    
    private AdminStartParkingResponseBody body;

    public AdminStartParkingResponseBody getBody() {
        return body;
    }

    public void setBody(AdminStartParkingResponseBody body) {
        this.body = body;
    }
}
