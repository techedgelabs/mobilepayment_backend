package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcatalog;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.dwh.adapter.interfaces.AwardDetail;

public class AdminDwhGetCatalogBodyResponse {

    private String            errorCode;
    private String            statusCode;
    private List<AwardDetail> awardDetailList = new ArrayList<AwardDetail>();

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<AwardDetail> getAwardDetailList() {
        return awardDetailList;
    }

    public void setAwardDetailList(List<AwardDetail> awardDetailList) {
        this.awardDetailList = awardDetailList;
    }

}
