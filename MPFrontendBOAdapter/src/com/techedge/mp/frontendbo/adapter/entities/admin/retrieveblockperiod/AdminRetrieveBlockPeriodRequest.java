package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveblockperiod;

import com.techedge.mp.core.business.interfaces.UnavailabilityPeriodResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminRetrieveBlockPeriodRequest extends AbstractBORequest implements Validable {

    private Status                              status = new Status();

    private Credential                          credential;
    private AdminRetrieveBlockPeriodBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminRetrieveBlockPeriodBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminRetrieveBlockPeriodBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-BLOCK-PERIOD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_BLOCK_PERIOD_RETRIEVE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_BLOCK_PERIOD_RETRIEVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        UnavailabilityPeriodResponse retrieveBlockPeriodResult = getAdminServiceRemote().adminRetrieveBlockPeriod(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), this.getBody().getCode(), this.getBody().getActive());

        AdminRetrieveBlockPeriodResponse retrieveBlockPeriodResponse = new AdminRetrieveBlockPeriodResponse();
        status.setStatusCode(retrieveBlockPeriodResult.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveBlockPeriodResult.getStatusCode()));
        AdminRetrieveBlockPeriodBodyResponse retrieveBlockPeriodBodyResponse = new AdminRetrieveBlockPeriodBodyResponse();
        retrieveBlockPeriodBodyResponse.setBlockPeriods(retrieveBlockPeriodResult.getUnavailabilityPeriodList());
        retrieveBlockPeriodResponse.setBody(retrieveBlockPeriodBodyResponse);
        retrieveBlockPeriodResponse.setStatus(status);

        return retrieveBlockPeriodResponse;
    }

}