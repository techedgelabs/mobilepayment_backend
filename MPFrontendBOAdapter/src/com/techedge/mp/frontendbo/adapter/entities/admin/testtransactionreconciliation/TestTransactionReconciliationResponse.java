package com.techedge.mp.frontendbo.adapter.entities.admin.testtransactionreconciliation;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestTransactionReconciliationResponse extends BaseResponse{
    
    private TestTransactionReconciliationResponseBody body;

    public TestTransactionReconciliationResponseBody getBody() {
        return body;
    }

    public void setBody(TestTransactionReconciliationResponseBody body) {
        this.body = body;
    }
}
