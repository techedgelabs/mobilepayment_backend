package com.techedge.mp.frontendbo.adapter.entities.admin.estimateendparkingprice;

import com.techedge.mp.parking.integration.business.interfaces.EstimateEndParkingPriceResult;

public class AdminEstimateEndParkingPriceResponseBody {

    private EstimateEndParkingPriceResult estimateEndParkingPriceResult;

    public EstimateEndParkingPriceResult getEstimateEndParkingPriceResult() {
        return estimateEndParkingPriceResult;
    }

    public void setEstimateEndParkingPriceResult(EstimateEndParkingPriceResult estimateEndParkingPriceResult) {
        this.estimateEndParkingPriceResult = estimateEndParkingPriceResult;
    }

}
