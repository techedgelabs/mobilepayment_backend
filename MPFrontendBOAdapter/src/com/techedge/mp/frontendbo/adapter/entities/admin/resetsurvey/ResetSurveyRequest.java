package com.techedge.mp.frontendbo.adapter.entities.admin.resetsurvey;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class ResetSurveyRequest extends AbstractBORequest implements Validable {

    private Status                 status = new Status();

    private Credential             credential;
    private ResetSurveyBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ResetSurveyBodyRequest getBody() {
        return body;
    }

    public void setBody(ResetSurveyBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RESET-SURVEY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.ADMIN_SURVEY_RESET_SUCCESS);

        return status;
    }

    @Override
    public BaseResponse execute() {
        ResetSurveyBodyRequest resetSurveyBodyRequest = this.getBody();
        ResetSurveyResponse resetSurveyResponse = new ResetSurveyResponse();

        String response = getAdminServiceRemote().adminSurveyReset(this.getCredential().getTicketID(), this.getCredential().getRequestID(), resetSurveyBodyRequest.getCode());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        resetSurveyResponse.setStatus(status);

        return resetSurveyResponse;
    }

}
