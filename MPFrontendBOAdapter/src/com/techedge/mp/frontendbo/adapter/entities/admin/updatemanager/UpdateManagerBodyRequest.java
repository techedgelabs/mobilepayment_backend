package com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdateManagerBodyRequest implements Validable {

    private UpdateManagerDataRequest managerData;
    
    
    public UpdateManagerBodyRequest() {}

    public UpdateManagerDataRequest getManagerData() {
        return managerData;
    }
    public void setManagerData(UpdateManagerDataRequest managerData) {
        this.managerData = managerData;
    }
    
    @Override
    public Status check() {
        
        Status status = new Status();
        
        if(this.managerData != null) {
            
            status = this.managerData.check();
            
            if(!Validator.isValid(status.getStatusCode())) {
                
                return status;
                
            }
            
        } else {
            
            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            
            return status;
        }
        
        status.setStatusCode(StatusCode.ADMIN_UPDATE_MANAGER_SUCCESS);

        return status;
    }

}
