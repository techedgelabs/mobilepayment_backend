package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog;

import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RetrieveActivityLogRequestBody implements Validable {
	
	private Long start;
	private Long end;
	private ErrorLevel minLevel;
	private ErrorLevel maxLevel;
	private String source;
	private String groupId;
	private String phaseId;
	private String messagePattern;

	public Long getStart() {
		return start;
	}
	public void setStart(Long start) {
		this.start = start;
	}
	
	public Long getEnd() {
		return end;
	}
	public void setEnd(Long end) {
		this.end = end;
	}
	
	public ErrorLevel getMinLevel() {
		return minLevel;
	}
	public void setMinLevel(ErrorLevel minLevel) {
		this.minLevel = minLevel;
	}

	public ErrorLevel getMaxLevel() {
		return maxLevel;
	}
	public void setMaxLevel(ErrorLevel maxLevel) {
		this.maxLevel = maxLevel;
	}

	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}

	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	
	public String getPhaseId() {
		return phaseId;
	}
	public void setPhaseId(String phaseId) {
		this.phaseId = phaseId;
	}

	public String getMessagePattern() {
		return messagePattern;
	}
	public void setMessagePattern(String messagePattern) {
		this.messagePattern = messagePattern;
	}
	

	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.start == null || this.end == null || this.minLevel == null || this.maxLevel == null) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.source != null && this.source.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.groupId != null && this.groupId.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.phaseId != null && this.phaseId.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.messagePattern != null && this.messagePattern.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS);
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);

		return status;
	}
	
}
