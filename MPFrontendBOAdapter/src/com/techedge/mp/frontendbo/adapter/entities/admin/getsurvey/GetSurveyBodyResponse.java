package com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey;

import java.util.ArrayList;

import com.techedge.mp.frontendbo.adapter.entities.common.Survey;

public class GetSurveyBodyResponse {
    private ArrayList<Survey> surveys = new ArrayList<Survey>(0);

    public ArrayList<Survey> getSurveys() {
        return surveys;
    }

    public void setSurveys(ArrayList<Survey> surveys) {
        this.surveys = surveys;
    }
    

}
