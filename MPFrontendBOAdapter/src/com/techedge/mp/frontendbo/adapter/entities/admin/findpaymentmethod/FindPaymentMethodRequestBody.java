package com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class FindPaymentMethodRequestBody implements Validable {
	
	private Long id;
	private String type;


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.id == null) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.type != null && this.type.length() > 20) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS);
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_SUCCESS);

		return status;
	}
	
}
