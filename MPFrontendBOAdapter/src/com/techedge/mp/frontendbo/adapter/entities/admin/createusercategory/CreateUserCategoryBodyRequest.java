package com.techedge.mp.frontendbo.adapter.entities.admin.createusercategory;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class CreateUserCategoryBodyRequest implements Validable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public CreateUserCategoryBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.name == null || this.name.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_USER_CATEGORY_CREATE_FAILURE);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_USER_CATEGORY_CREATE);

        return status;
    }

}
