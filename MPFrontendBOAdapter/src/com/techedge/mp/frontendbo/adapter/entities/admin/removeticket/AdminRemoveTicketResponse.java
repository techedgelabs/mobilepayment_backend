package com.techedge.mp.frontendbo.adapter.entities.admin.removeticket;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminRemoveTicketResponse extends BaseResponse {

    private AdminRemoveTicketBodyResponse body;

    public AdminRemoveTicketBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminRemoveTicketBodyResponse body) {
        this.body = body;
    }

}
