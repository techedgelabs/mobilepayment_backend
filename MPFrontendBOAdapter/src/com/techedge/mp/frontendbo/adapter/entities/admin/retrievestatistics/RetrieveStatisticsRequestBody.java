package com.techedge.mp.frontendbo.adapter.entities.admin.retrievestatistics;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RetrieveStatisticsRequestBody implements Validable {

	@Override
	public Status check() {
		
		Status status = new Status();
		
		status.setStatusCode(StatusCode.ADMIN_RETRIEVE_STATISTICS_SUCCESS);

		return status;
	}
	
}
