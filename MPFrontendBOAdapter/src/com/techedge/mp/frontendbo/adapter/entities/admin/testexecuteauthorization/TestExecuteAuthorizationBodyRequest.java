package com.techedge.mp.frontendbo.adapter.entities.admin.testexecuteauthorization;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestExecuteAuthorizationBodyRequest implements Validable {

    private String  operationId;
    private Integer amount;
    private String  authCryptogram;
    private String  currencyCode;
    private String  mcCardDpan;
    private String  refuelMode;
    private String  shopCode;
    private String  partnerType;
    private Long    requestTimestamp;

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getAuthCryptogram() {
        return authCryptogram;
    }

    public void setAuthCryptogram(String authCryptogram) {
        this.authCryptogram = authCryptogram;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getMcCardDpan() {
        return mcCardDpan;
    }

    public void setMcCardDpan(String mcCardDpan) {
        this.mcCardDpan = mcCardDpan;
    }

    public String getRefuelMode() {
        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationId == null || this.operationId.trim().isEmpty() || this.amount == null || this.authCryptogram == null || this.authCryptogram.trim().isEmpty()
                || this.currencyCode == null || this.currencyCode.trim().isEmpty() || this.mcCardDpan == null || this.mcCardDpan.trim().isEmpty() || this.refuelMode == null
                || this.refuelMode.trim().isEmpty() || this.shopCode == null || this.shopCode.trim().isEmpty() || this.partnerType == null || this.partnerType.trim().isEmpty()
                || this.requestTimestamp == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_EXECUTE_PAYMENT_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_EXECUTE_PAYMENT_SUCCESS);

        return status;
    }

}
