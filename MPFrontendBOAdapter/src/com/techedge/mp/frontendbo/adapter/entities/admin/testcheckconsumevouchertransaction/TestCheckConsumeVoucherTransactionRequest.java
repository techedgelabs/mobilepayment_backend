package com.techedge.mp.frontendbo.adapter.entities.admin.testcheckconsumevouchertransaction;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherInfo;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestCheckConsumeVoucherTransactionRequest extends AbstractBORequest implements Validable {

    private Credential                                    credential;
    private TestCheckConsumeVoucherTransactionBodyRequest body;

    public TestCheckConsumeVoucherTransactionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestCheckConsumeVoucherTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(TestCheckConsumeVoucherTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CHECK-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_CHECK_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestCheckConsumeVoucherTransactionResponse testCheckConsumeVoucherTransactionResponse = new TestCheckConsumeVoucherTransactionResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testCheckConsumeVoucherTransaction");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testCheckConsumeVoucherTransactionResponse.getStatus().setStatusCode("-1");
        }
        else {

            CheckConsumeVoucherTransactionResult checkConsumeVoucherTransactionResult = new CheckConsumeVoucherTransactionResult();

            try {

                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                checkConsumeVoucherTransactionResult = getFidelityServiceRemote().checkConsumeVoucherTransaction(this.getBody().getOperationID(),
                        this.getBody().getOperationIDtoCheck(), partnerType, this.getBody().getRequestTimestamp());
            }
            catch (FidelityServiceException ex) {
                checkConsumeVoucherTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!checkConsumeVoucherTransactionResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                testCheckConsumeVoucherTransactionResponse.setCsTransactionID(checkConsumeVoucherTransactionResult.getCsTransactionID());
                testCheckConsumeVoucherTransactionResponse.setMarketingMsg(checkConsumeVoucherTransactionResult.getMarketingMsg());
                testCheckConsumeVoucherTransactionResponse.getStatus().setStatusMessage(checkConsumeVoucherTransactionResult.getMessageCode());
                testCheckConsumeVoucherTransactionResponse.getStatus().setStatusCode(checkConsumeVoucherTransactionResult.getStatusCode());

                for (VoucherDetail voucherDetail : checkConsumeVoucherTransactionResult.getVoucherList()) {

                    VoucherInfo voucherInfo = new VoucherInfo();

                    voucherInfo.setConsumedValue(voucherDetail.getConsumedValue());
                    voucherInfo.setExpirationDate(voucherDetail.getExpirationDate());
                    voucherInfo.setInitialValue(voucherDetail.getInitialValue());
                    voucherInfo.setPromoCode(voucherDetail.getPromoCode());
                    voucherInfo.setPromoDescription(voucherDetail.getPromoDescription());
                    voucherInfo.setPromoDoc(voucherDetail.getPromoDoc());
                    voucherInfo.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                    voucherInfo.setVoucherCode(voucherDetail.getVoucherCode());
                    voucherInfo.setVoucherStatus(voucherDetail.getVoucherStatus());
                    voucherInfo.setVoucherType(voucherDetail.getVoucherType());
                    voucherInfo.setVoucherValue(voucherDetail.getVoucherValue());

                    testCheckConsumeVoucherTransactionResponse.getVoucherList().add(voucherInfo);
                }
            }
        }

        return testCheckConsumeVoucherTransactionResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
