package com.techedge.mp.frontendbo.adapter.entities.admin.updateadmin;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminUpdateRequest extends AbstractBORequest implements Validable {

    private Status                 status = new Status();

    private Credential             credential;
    private AdminUpdateBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminUpdateBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminUpdateBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADMIN-UPDATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String adminUpdateResponse = getAdminServiceRemote().adminUpdate(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getAdminData());
        AdminUpdateResponse updateBlockPeriodResponse = new AdminUpdateResponse();
        status.setStatusCode(adminUpdateResponse);
        status.setStatusMessage(prop.getProperty(adminUpdateResponse));
        updateBlockPeriodResponse.setStatus(status);

        return updateBlockPeriodResponse;
    }

}