package com.techedge.mp.frontendbo.adapter.entities.admin.testcrmsfgetmission;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.GetOffersResult;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestCrmSfGetMissionRequest extends AbstractBORequest implements Validable {

    private Credential                  credential;
    private TestCrmSfGetMissionBodyRequest body;

    public TestCrmSfGetMissionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestCrmSfGetMissionBodyRequest getBody() {
        return body;
    }

    public void setBody(TestCrmSfGetMissionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GETMISSION-EVENT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_CRMSF_GET_MISSION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestCrmSfGetMissionResponse testCrmSfGetMissionResponse = new TestCrmSfGetMissionResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testCrmSfGetMission");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

        	testCrmSfGetMissionResponse.getStatus().setStatusCode("-1");
        }
        else {

        	GetOffersResult getOffersResult = new GetOffersResult();
        	
        	String requestId = new IdGenerator().generateId(16).substring(0, 32); 
        	
        	DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ITALIAN);
        	Date date = null;
        	try {
	        	if(this.getBody().getDate()!=null && !this.getBody().getDate().equalsIgnoreCase(""))
						date = formatDate.parse(this.getBody().getDate());
        	} catch (ParseException e) {
				e.printStackTrace();
			}

            try {
            	getOffersResult = getCrmService().getMissionsSf(requestId, this.getBody().getFiscalCode(), date, 
                		this.getBody().getNotificationFlag(), this.getBody().getPaymentCardFlag(), this.getBody().getBrand(), 
                		this.getBody().getCluster());
                		
            }
            catch (Exception ex) {
            	getOffersResult.setErrorCode(ResponseHelper.SYSTEM_ERROR);
            	getOffersResult.setSuccess(false);
            }
            
            testCrmSfGetMissionResponse.setErrorCode(getOffersResult.getErrorCode());
            testCrmSfGetMissionResponse.setMessage(getOffersResult.getMessage());
            testCrmSfGetMissionResponse.setRequestId(getOffersResult.getRequestId());
            testCrmSfGetMissionResponse.setSuccess(getOffersResult.getSuccess());
            testCrmSfGetMissionResponse.setOffers(getOffersResult.getOffers());   
            
        }

        return testCrmSfGetMissionResponse;
    }


}
