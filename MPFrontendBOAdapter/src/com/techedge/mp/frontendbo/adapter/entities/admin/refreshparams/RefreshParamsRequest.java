package com.techedge.mp.frontendbo.adapter.entities.admin.refreshparams;

import java.util.List;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.ParamInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RefreshParamsRequest extends AbstractBORequest implements Validable {

    private Status                   status = new Status();

    private Credential               credential;
    private RefreshParamsRequestBody body;

    public RefreshParamsRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RefreshParamsRequestBody getBody() {
        return body;
    }

    public void setBody(RefreshParamsRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REFRESH-PARAMS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_REFRESH_PARAMS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        ParametersServiceRemote parametersService = getParametersServiceRemote();
        RefreshParamsResponse refreshParamsResponse = new RefreshParamsResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "refreshParams");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {
            status.setStatusCode("-1");
            refreshParamsResponse.setStatus(status);
        }
        else {

            if (this.getBody() == null || !this.getBody().getCommand().equals("REFRESH")) {
                status.setStatusCode(StatusCode.ADMIN_REFRESH_PARAMS_COMMAND_ERROR);
                status.setStatusMessage("Comando non valido");
                refreshParamsResponse.setStatus(status);
            }
            else {
                List<ParamInfo> params = parametersService.refreshParameters();
                refreshParamsResponse.setParmas(params);
                status.setStatusCode(StatusCode.ADMIN_REFRESH_PARAMS_SUCCESS);
                status.setStatusMessage("Parametri ricaricati");
                refreshParamsResponse.setStatus(status);
            }
        }

        return refreshParamsResponse;
    }

}
