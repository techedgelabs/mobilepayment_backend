package com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey;

import java.util.Date;

import com.techedge.mp.core.business.interfaces.SurveyList;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Survey;
import com.techedge.mp.frontendbo.adapter.entities.common.SurveyQuestion;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class GetSurveyRequest extends AbstractBORequest implements Validable {

    private Status               status = new Status();

    private Credential           credential;
    private GetSurveyBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GetSurveyBodyRequest getBody() {
        return body;
    }

    public void setBody(GetSurveyBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-SURVEY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.ADMIN_SURVEY_GET_SUCCESS);

        return status;
    }

    @Override
    public BaseResponse execute() {
        GetSurveyBodyRequest getSurveyBodyRequest = this.getBody();
        GetSurveyBodyResponse getSurveyBodyResponse = new GetSurveyBodyResponse();
        GetSurveyResponse getSurveyResponse = new GetSurveyResponse();

        String code = null;
        Date startDate = null;
        Date endDate = null;
        Integer statusSurvey = null;

        if (getSurveyBodyRequest.getCode() != null && !getSurveyBodyRequest.getCode().isEmpty()) {
            code = getSurveyBodyRequest.getCode();
        }

        if (getSurveyBodyRequest.getStartSearch() != null && !getSurveyBodyRequest.getStartSearch().equals(new Long(0))) {
            startDate = new Date(getSurveyBodyRequest.getStartSearch());
        }

        if (getSurveyBodyRequest.getEndSearch() != null && !getSurveyBodyRequest.getEndSearch().equals(new Long(0))) {
            endDate = new Date(getSurveyBodyRequest.getEndSearch());
        }

        if (getSurveyBodyRequest.getStatus() != null && !getSurveyBodyRequest.getStatus().equals(new Integer(0))) {
            statusSurvey = getSurveyBodyRequest.getStatus();
        }

        SurveyList surveyList = getAdminServiceRemote().adminSurveyGet(this.getCredential().getTicketID(), this.getCredential().getRequestID(), code, startDate, endDate,
                statusSurvey);

        for (com.techedge.mp.core.business.interfaces.Survey coreSurvey : surveyList.getSurveyList()) {
            Survey survey = new Survey();
            survey.setCode(coreSurvey.getCode());
            survey.setDescription(coreSurvey.getDescription());
            survey.setNote(coreSurvey.getNote());
            survey.setStartDate(coreSurvey.getStartDate());
            survey.setEndDate(coreSurvey.getEndDate());
            survey.setStatus(coreSurvey.getStatus());

            for (com.techedge.mp.core.business.interfaces.SurveyQuestion coreQuestion : coreSurvey.getQuestions()) {
                SurveyQuestion question = new SurveyQuestion();
                question.setCode(coreQuestion.getCode());
                question.setSequence(coreQuestion.getSequence());
                question.setText(coreQuestion.getText());
                question.setType(coreQuestion.getType());
                survey.getQuestions().add(question);
            }

            getSurveyBodyResponse.getSurveys().add(survey);
        }

        getSurveyResponse.setBody(getSurveyBodyResponse);

        String response = surveyList.getStatusCode();
        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        getSurveyResponse.setStatus(status);

        return getSurveyResponse;
    }

}
