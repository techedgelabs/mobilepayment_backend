package com.techedge.mp.frontendbo.adapter.entities.admin.testgetloyaltycardlist;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.interfaces.CardDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetLoyaltyCardListResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.CardInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestGetLoyaltyCardListRequest extends AbstractBORequest implements Validable {

    private Credential                        credential;
    private TestGetLoyaltyCardListBodyRequest body;

    public TestGetLoyaltyCardListRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestGetLoyaltyCardListBodyRequest getBody() {
        return body;
    }

    public void setBody(TestGetLoyaltyCardListBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-LOYALTY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_GET_LOYALTY_CARD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestGetLoyaltyCardListResponse testGetLoyaltyCardListResponse = new TestGetLoyaltyCardListResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testGetLoyaltyCardList");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testGetLoyaltyCardListResponse.getStatus().setStatusCode("-1");
        }
        else {
            GetLoyaltyCardListResult getLoyaltyCardListResult = new GetLoyaltyCardListResult();

            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                getLoyaltyCardListResult = getFidelityServiceRemote().getLoyaltyCardList(this.getBody().getOperationID(), this.getBody().getFiscalCode(), partnerType,
                        this.getBody().getRequestTimestamp());
            }
            catch (Exception ex) {
                testGetLoyaltyCardListResponse.getStatus().setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (getLoyaltyCardListResult.getStatusCode() != null && !getLoyaltyCardListResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                testGetLoyaltyCardListResponse.setBalance(getLoyaltyCardListResult.getBalance());
                testGetLoyaltyCardListResponse.setCsTransactionID(getLoyaltyCardListResult.getCsTransactionID());
                testGetLoyaltyCardListResponse.getStatus().setStatusMessage(getLoyaltyCardListResult.getMessageCode());
                testGetLoyaltyCardListResponse.getStatus().setStatusCode(getLoyaltyCardListResult.getStatusCode());

                for (CardDetail cardDetail : getLoyaltyCardListResult.getCardList()) {

                    CardInfo cardInfo = new CardInfo();

                    cardInfo.setEanCode(cardDetail.getEanCode());
                    cardInfo.setPanCode(cardDetail.getPanCode());
                    cardInfo.setType(cardDetail.getCardType());
                    cardInfo.setVirtual(cardDetail.getVirtual());

                    testGetLoyaltyCardListResponse.getCardList().add(cardInfo);
                }
            }
        }

        return testGetLoyaltyCardListResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
