package com.techedge.mp.frontendbo.adapter.entities.admin.reconciliationuser;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationUserData;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class ReconciliationUserRequestBody implements Validable {

    List<ReconciliationUserData> idList = new ArrayList<ReconciliationUserData>(0);


    public List<ReconciliationUserData> getIdList() {
        return idList;
    }

    public void setIdList(List<ReconciliationUserData> idList) {
        this.idList = idList;
    }

    @Override
    public Status check() {

        Status status = new Status();
        status.setStatusCode(StatusCode.ADMIN_RECONCILIATION_SUCCESS);

        return status;
    }
}
