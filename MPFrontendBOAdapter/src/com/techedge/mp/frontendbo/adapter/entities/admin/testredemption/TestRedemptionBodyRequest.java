package com.techedge.mp.frontendbo.adapter.entities.admin.testredemption;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestRedemptionBodyRequest implements Validable {

    private String operationID;
    private String partnerType;
    private Long   requestTimestamp;
    private String fiscalCode;
    private Integer redemptionCode;

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String value) {
        this.fiscalCode = value;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public Integer getRedemptionCode() {
        return redemptionCode;
    }

    public void setRedemptionCode(Integer redemptionCode) {
        this.redemptionCode = redemptionCode;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationID == null || this.operationID.trim().isEmpty() || this.partnerType == null || this.partnerType.trim().isEmpty() || this.fiscalCode == null
                || this.fiscalCode.trim().isEmpty() || this.requestTimestamp == null || this.redemptionCode == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_FIDELITY_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_REDEMPTION_SUCCESS);

        return status;
    }

}
