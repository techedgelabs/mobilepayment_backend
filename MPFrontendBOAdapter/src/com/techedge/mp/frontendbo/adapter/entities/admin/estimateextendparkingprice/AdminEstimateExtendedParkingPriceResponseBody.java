package com.techedge.mp.frontendbo.adapter.entities.admin.estimateextendparkingprice;

import com.techedge.mp.parking.integration.business.interfaces.EstimateExtendedParkingPriceResult;

public class AdminEstimateExtendedParkingPriceResponseBody {

    private EstimateExtendedParkingPriceResult estimateExtendedParkingPriceResult;

    public EstimateExtendedParkingPriceResult getEstimateExtendedParkingPriceResult() {
        return estimateExtendedParkingPriceResult;
    }

    public void setEstimateExtendedParkingPriceResult(EstimateExtendedParkingPriceResult estimateExtendedParkingPriceResult) {
        this.estimateExtendedParkingPriceResult = estimateExtendedParkingPriceResult;
    }

}
