package com.techedge.mp.frontendbo.adapter.entities.common;

public class ArchiveTransactionInfo {
    private String id;
    private String type;
    private String statusCode;

    public ArchiveTransactionInfo(String id, String type, String statusCode) {
        this.id = id;
        this.type = type;
        this.statusCode = statusCode;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
