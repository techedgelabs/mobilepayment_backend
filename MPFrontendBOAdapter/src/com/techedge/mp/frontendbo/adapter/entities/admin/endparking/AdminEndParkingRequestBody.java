package com.techedge.mp.frontendbo.adapter.entities.admin.endparking;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminEndParkingRequestBody implements Validable {

    private String lang;
    private String parkingId;
    private String clientOperationID;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getParkingId() {
        return parkingId;
    }

    public void setParkingId(String parkingId) {
        this.parkingId = parkingId;
    }

    public String getClientOperationID() {
        return clientOperationID;
    }

    public void setClientOperationID(String clientOperationID) {
        this.clientOperationID = clientOperationID;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);
        return status;
    }

}
