package com.techedge.mp.frontendbo.adapter.entities.admin.testgettransactionstatus;

import java.util.Date;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetSrcTransactionStatusMessageResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;

public class TestGetTransactionStatusRequest extends AbstractBORequest implements Validable {

    private Credential                     credential;

    private final static String            PARAM_PROXY_HOST     = "PROXY_HOST";
    private final static String            PARAM_PROXY_PORT     = "PROXY_PORT";
    private final static String            PARAM_PROXY_NO_HOSTS = "PROXY_NO_HOSTS";
    //DA MODIFICARE
    private TestGetTransactionStatusRequestBody body;

    public TestGetTransactionStatusRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestGetTransactionStatusRequestBody getBody() {
        return body;
    }

    public void setBody(TestGetTransactionStatusRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-USERS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        TestGetTransactionStatusResponse testGetTransactionStatusResponse = new TestGetTransactionStatusResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testRetrieveStation");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testGetTransactionStatusResponse.getStatus().setStatusCode("-1");
        }
        else {

            ForecourtPostPaidServiceRemote forecourtPostPaid;
            
            try {
                forecourtPostPaid = EJBHomeCache.getInstance().getForecourtPostPaidService();

                ParametersServiceRemote parametersService = EJBHomeCache.getInstance().getParametersService();

                String proxyHost = parametersService.getParamValue(PARAM_PROXY_HOST);
                String proxyPort = parametersService.getParamValue(PARAM_PROXY_PORT);
                String proxyNoHosts = parametersService.getParamValue(PARAM_PROXY_NO_HOSTS);

                Proxy proxy = new Proxy(proxyHost, proxyPort, proxyNoHosts);
                proxy.setHttp();

                String requestID = String.valueOf(new Date().getTime());
                String mpTransactionID  = this.getBody().getMpTransactionID();
                String srcTransactionID = this.getBody().getSrcTransactionID();

                GetSrcTransactionStatusMessageResponse response = forecourtPostPaid.getTransactionStatus(requestID, mpTransactionID, srcTransactionID);

                TestGetTransactionStatusResponseBody testGetTransactionStatusResponseBody = new TestGetTransactionStatusResponseBody();
                testGetTransactionStatusResponseBody.setGetSrcTransactionStatusMessageResponse(response);

                testGetTransactionStatusResponse.setBody(testGetTransactionStatusResponseBody);;
                testGetTransactionStatusResponse.getStatus().setStatusCode("200");
                testGetTransactionStatusResponse.getStatus().setStatusMessage("OK");
            }
            catch (InterfaceNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (ParameterNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        return testGetTransactionStatusResponse;
    }
}
