package com.techedge.mp.frontendbo.adapter.entities.admin.editemaildomain;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class EditEmailDomainRequest extends AbstractBORequest implements Validable {

    private Status                     status = new Status();

    private Credential                 credential;
    private EditEmailDomainBodyRequest body;

    public EditEmailDomainRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public EditEmailDomainBodyRequest getBody() {
        return body;
    }

    public void setBody(EditEmailDomainBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        return status;
    }

    @Override
    public BaseResponse execute() {

        EditEmailDomainResponse editEmailDomainResponse = new EditEmailDomainResponse();

        String response = getAdminServiceRemote().adminEditEmailDomainInBlackList(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getId(),
                this.getBody().getEmail());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        editEmailDomainResponse.setStatus(status);

        return editEmailDomainResponse;
    }

}
