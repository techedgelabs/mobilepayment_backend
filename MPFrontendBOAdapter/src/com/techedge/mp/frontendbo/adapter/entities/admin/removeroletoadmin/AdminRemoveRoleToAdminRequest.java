package com.techedge.mp.frontendbo.adapter.entities.admin.removeroletoadmin;

import com.techedge.mp.frontendbo.adapter.entities.admin.addroletoadmin.AdminAddRoleToAdminResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminRemoveRoleToAdminRequest extends AbstractBORequest implements Validable {
    private Status                            status = new Status();

    private Credential                        credential;
    private AdminRemoveRoleToAdminBodyRequest body;

    public AdminRemoveRoleToAdminRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminRemoveRoleToAdminBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminRemoveRoleToAdminBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REMOVE-ROLE-TO-ADMIN", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_ADMIN_ROLE_REMOVE_CHECK_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_ADMIN_ROLE_REMOVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String adminRemoveRoleToAdminResult = getAdminServiceRemote().adminRemoveRoleToAdmin(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getRole(), this.getBody().getAdmin());

        AdminAddRoleToAdminResponse adminAddRoleToAdminResponse = new AdminAddRoleToAdminResponse();
        status.setStatusCode(adminRemoveRoleToAdminResult);
        status.setStatusMessage(prop.getProperty(adminRemoveRoleToAdminResult));
        adminAddRoleToAdminResponse.setStatus(status);

        return adminAddRoleToAdminResponse;

    }

}
