package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.PoPTransactionInfo;


public class RetrievePoPTransactionsResponse extends BaseResponse {
	
	List<PoPTransactionInfo> popTransactions = new ArrayList<PoPTransactionInfo>(0);

	public List<PoPTransactionInfo> getPopTransactions() {
		return popTransactions;
	}
	public void setPopTransactions(List<PoPTransactionInfo> popTransactions) {
		this.popTransactions = popTransactions;
	}
}
