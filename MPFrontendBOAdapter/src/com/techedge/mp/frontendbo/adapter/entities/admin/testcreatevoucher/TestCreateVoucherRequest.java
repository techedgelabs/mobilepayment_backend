package com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CreateVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestCreateVoucherRequest extends AbstractBORequest implements Validable {

    private Credential                   credential;
    private TestCreateVoucherBodyRequest body;

    public TestCreateVoucherRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestCreateVoucherBodyRequest getBody() {
        return body;
    }

    public void setBody(TestCreateVoucherBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CHECK-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_CREATE_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestCreateVoucherResponse testCreateVoucherResponse = new TestCreateVoucherResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testCheckConsumeVoucherTransaction");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testCreateVoucherResponse.getStatus().setStatusCode("-1");
        }
        else {

            CreateVoucherResult createVoucherResult = new CreateVoucherResult();
            VoucherConsumerType voucherType = VoucherConsumerType.ENI;
            PartnerType partnerType = PartnerType.MP;

            try {

                createVoucherResult = getFidelityServiceRemote().createVoucher(voucherType, this.getBody().getTotalAmount(), this.getBody().getBankTransactionID(),
                        this.getBody().getShopTransactionID(), this.getBody().getAuthorizationCode(), this.getBody().getOperationID(), partnerType,
                        this.getBody().getRequestTimestamp());
            }
            catch (FidelityServiceException ex) {
                createVoucherResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (createVoucherResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {

            }

            else {
                testCreateVoucherResponse.setCsTransactionID(createVoucherResult.getCsTransactionID());
                testCreateVoucherResponse.getStatus().setStatusMessage(createVoucherResult.getMessageCode());
                testCreateVoucherResponse.getStatus().setStatusCode(createVoucherResult.getStatusCode());
                testCreateVoucherResponse.setVoucher(createVoucherResult.getVoucher());

            }
        }

        return testCreateVoucherResponse;
    }

}
