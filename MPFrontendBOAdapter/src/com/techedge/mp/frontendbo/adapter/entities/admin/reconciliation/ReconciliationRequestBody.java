package com.techedge.mp.frontendbo.adapter.entities.admin.reconciliation;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class ReconciliationRequestBody implements Validable {
	
	List<String> transactionList = new ArrayList<String>(0);
	
	public List<String> getTransactionList() {
		return transactionList;
	}
	public void setTransactionList(List<String> transactionList) {
		this.transactionList = transactionList;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		status.setStatusCode(StatusCode.ADMIN_RECONCILIATION_SUCCESS);

		return status;
	}
}
