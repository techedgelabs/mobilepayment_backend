package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.ActivityLogInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;


public class RetrieveActivityLogResponse extends BaseResponse {
	
	List<ActivityLogInfo> activityLogData = new ArrayList<ActivityLogInfo>(0);

	public List<ActivityLogInfo> getActivityLogData() {
		return activityLogData;
	}

	public void setActivityLogData(List<ActivityLogInfo> activityLogData) {
		this.activityLogData = activityLogData;
	}
}
