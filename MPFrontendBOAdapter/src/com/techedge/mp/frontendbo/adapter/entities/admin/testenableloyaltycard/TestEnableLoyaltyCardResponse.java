package com.techedge.mp.frontendbo.adapter.entities.admin.testenableloyaltycard;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestEnableLoyaltyCardResponse extends BaseResponse{

    private String csTransactionID;


    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }
}
