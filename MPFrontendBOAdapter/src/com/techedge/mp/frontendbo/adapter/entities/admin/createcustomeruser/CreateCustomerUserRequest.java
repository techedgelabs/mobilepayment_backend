package com.techedge.mp.frontendbo.adapter.entities.admin.createcustomeruser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class CreateCustomerUserRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private CreateCustomerUserRequestBody body;

    public CreateCustomerUserRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateCustomerUserRequestBody getBody() {
        return body;
    }

    public void setBody(CreateCustomerUserRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        CreateCustomerUserResponse createCustomerUserResponse = new CreateCustomerUserResponse();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Date birthDate;
        try {
            birthDate = sdf.parse(this.getBody().getCustomerUserData().getUserInfo().getBirthDate());
        }
        catch (ParseException e) {
            status = new Status();
            status.setStatusCode(StatusCode.ADMIN_CREATE_DATE_BIRTH_WRONG);
            status.setStatusMessage(prop.getProperty(status.getStatusCode()));
            createCustomerUserResponse.setStatus(status);
            return createCustomerUserResponse;
        }

        User user = new User();

        PersonalData personalData = new PersonalData();

        personalData.setFirstName(this.getBody().getCustomerUserData().getUserInfo().getFirstName());
        personalData.setLastName(this.getBody().getCustomerUserData().getUserInfo().getLastName());
        personalData.setFiscalCode(this.getBody().getCustomerUserData().getUserInfo().getFiscalCode());
        personalData.setBirthDate(birthDate);
        personalData.setBirthMunicipality(this.getBody().getCustomerUserData().getUserInfo().getBirthMunicipality());
        personalData.setBirthProvince(this.getBody().getCustomerUserData().getUserInfo().getBirthProvince());
        personalData.setLanguage(this.getBody().getCustomerUserData().getUserInfo().getLanguage());
        personalData.setSex(this.getBody().getCustomerUserData().getUserInfo().getSex());
        personalData.setSecurityDataEmail(this.getBody().getCustomerUserData().getUserInfo().getEmail());
        personalData.setSecurityDataPassword(this.getBody().getCustomerUserData().getPassword());

        personalData.setAddress(null);

        personalData.setBillingAddress(null);

        user.setPersonalData(personalData);
        PaymentInfo paymentInfo = new PaymentInfo();
        Set<PaymentInfo> paymentInfoList = new HashSet<PaymentInfo>(0);
        List<com.techedge.mp.frontendbo.adapter.entities.common.PaymentInfoByUserCreate> paymentDataList = this.getBody().getCustomerUserData().getUserInfo().getPaymentData();
        for (com.techedge.mp.frontendbo.adapter.entities.common.PaymentInfoByUserCreate pi : paymentDataList) {
            if (pi.getDefaultMethod().equals("true"))
                paymentInfo.setDefaultMethod(true);
            else
                paymentInfo.setDefaultMethod(false);

            paymentInfo.setType(pi.getType());
            paymentInfo.setPan(pi.getPan());
            paymentInfo.setToken(pi.getToken());
            paymentInfo.setPin(pi.getPin());
            try {
                if (pi.getInsertTimestamp() != null)
                    paymentInfo.setInsertTimestamp(sdf.parse(pi.getInsertTimestamp()));
                if (pi.getExpirationDate() != null)
                    paymentInfo.setExpirationDate(sdf.parse(pi.getExpirationDate()));

            }
            catch (ParseException e) {
                status = new Status();
                status.setStatusCode(StatusCode.ADMIN_CREATE_DATE_WRONG);
                status.setStatusMessage(prop.getProperty(status.getStatusCode()));
                createCustomerUserResponse.setStatus(status);
                return createCustomerUserResponse;
            }

            paymentInfo.setStatus(pi.getStatus());
            paymentInfoList.add(paymentInfo);
        }

        user.setPaymentData(paymentInfoList);

        String response = getAdminServiceRemote().adminCustomerUserCreate(this.getCredential().getTicketID(), this.getCredential().getRequestID(), user);

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        createCustomerUserResponse.setStatus(status);

        return createCustomerUserResponse;
    }

}
