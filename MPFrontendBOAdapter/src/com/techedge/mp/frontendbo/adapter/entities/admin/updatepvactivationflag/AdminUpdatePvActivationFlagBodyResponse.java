package com.techedge.mp.frontendbo.adapter.entities.admin.updatepvactivationflag;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.PvGenerationInfo;

public class AdminUpdatePvActivationFlagBodyResponse {

    private String                 errorCode;
    private String                 statusCode;
    private List<PvGenerationInfo> pvActivationResultList = new ArrayList<PvGenerationInfo>(0);

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<PvGenerationInfo> getPvActivationResultList() {
        return pvActivationResultList;
    }

    public void setPvActivationResultList(List<PvGenerationInfo> pvActivationResultList) {
        this.pvActivationResultList = pvActivationResultList;
    }

}
