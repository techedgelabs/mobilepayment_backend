package com.techedge.mp.frontendbo.adapter.entities.admin.updatemailinglist;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminUpdateMailingListBodyRequest implements Validable {

    private Long id;
    private String  email;
    private String  name;
    private Integer status;
    private String  template;

    public AdminUpdateMailingListBodyRequest() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if ((this.id == null) && (this.email == null || this.email.trim().equals("")) && (this.name == null || this.name.trim().equals("")) && (this.status == null)
                && (this.template == null || this.template.trim().equals(""))) {
            status.setStatusCode(StatusCode.ADMIN_UPDATE_MAILING_LIST_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_MAILING_LIST_SUCCESS);

        return status;
    }

}
