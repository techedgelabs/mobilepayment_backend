package com.techedge.mp.frontendbo.adapter.entities.admin.createrefuelinguser;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminCreateRefuelingUserBodyRequest implements Validable {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String role) {
        this.username = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String admin) {
        this.password = admin;
    }

    public AdminCreateRefuelingUserBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.password == null || this.password.isEmpty() || this.username == null || this.username.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_CREATE_REFUELING_USER_ERROR_PARAMETERS);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_REFUELING_USER_SUCCESS);

        return status;
    }

}
