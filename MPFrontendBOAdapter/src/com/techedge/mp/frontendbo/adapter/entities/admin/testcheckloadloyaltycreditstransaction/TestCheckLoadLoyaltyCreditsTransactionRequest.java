package com.techedge.mp.frontendbo.adapter.entities.admin.testcheckloadloyaltycreditstransaction;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestCheckLoadLoyaltyCreditsTransactionRequest extends AbstractBORequest implements Validable {

    private Credential                                        credential;
    private TestCheckLoadLoyaltyCreditsTransactionBodyRequest body;

    public TestCheckLoadLoyaltyCreditsTransactionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestCheckLoadLoyaltyCreditsTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(TestCheckLoadLoyaltyCreditsTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CHECK-LOYALTY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_LOAD_CHECK_LOYALTY_CREDITS_TRANSACTION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestCheckLoadLoyaltyCreditsTransactionResponse testCheckLoadLoyaltyCreditsTransactionResponse = new TestCheckLoadLoyaltyCreditsTransactionResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testCheckLoadLoyaltyCreditsTransaction");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testCheckLoadLoyaltyCreditsTransactionResponse.getStatus().setStatusCode("-1");
        }
        else {

            CheckLoadLoyaltyCreditsTransactionResult checkLoadLoyaltyCreditsTransactionResult = new CheckLoadLoyaltyCreditsTransactionResult();

            try {

                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                checkLoadLoyaltyCreditsTransactionResult = getFidelityServiceRemote().checkLoadLoyaltyCreditsTransaction(this.getBody().getOperationID(),
                        this.getBody().getOperationIDtoCheck(), partnerType, this.getBody().getRequestTimestamp());
            }
            catch (FidelityServiceException ex) {
                checkLoadLoyaltyCreditsTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!checkLoadLoyaltyCreditsTransactionResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                testCheckLoadLoyaltyCreditsTransactionResponse.setBalance(checkLoadLoyaltyCreditsTransactionResult.getBalance());
                testCheckLoadLoyaltyCreditsTransactionResponse.setBalanceAmount(checkLoadLoyaltyCreditsTransactionResult.getBalanceAmount());
                testCheckLoadLoyaltyCreditsTransactionResponse.setCardClassification(checkLoadLoyaltyCreditsTransactionResult.getCardClassification());
                testCheckLoadLoyaltyCreditsTransactionResponse.setCardCodeIssuer(checkLoadLoyaltyCreditsTransactionResult.getCardCodeIssuer());
                testCheckLoadLoyaltyCreditsTransactionResponse.setCardStatus(checkLoadLoyaltyCreditsTransactionResult.getCardStatus());
                testCheckLoadLoyaltyCreditsTransactionResponse.setCardType(checkLoadLoyaltyCreditsTransactionResult.getCardType());
                testCheckLoadLoyaltyCreditsTransactionResponse.setCredits(checkLoadLoyaltyCreditsTransactionResult.getCredits());
                testCheckLoadLoyaltyCreditsTransactionResponse.setCsTransactionID(checkLoadLoyaltyCreditsTransactionResult.getCsTransactionID());
                testCheckLoadLoyaltyCreditsTransactionResponse.setEanCode(checkLoadLoyaltyCreditsTransactionResult.getEanCode());
                testCheckLoadLoyaltyCreditsTransactionResponse.setMarketingMsg(checkLoadLoyaltyCreditsTransactionResult.getMarketingMsg());
                testCheckLoadLoyaltyCreditsTransactionResponse.getStatus().setStatusMessage(checkLoadLoyaltyCreditsTransactionResult.getMessageCode());
                testCheckLoadLoyaltyCreditsTransactionResponse.getStatus().setStatusCode(checkLoadLoyaltyCreditsTransactionResult.getStatusCode());
            }
        }

        return testCheckLoadLoyaltyCreditsTransactionResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
