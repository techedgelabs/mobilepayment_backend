package com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class CreatePrefixNumberBodyRequest implements Validable {

    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public CreatePrefixNumberBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.code == null || this.code.isEmpty()) {

            status.setStatusCode(ResponseHelper.PREFIX_RETRIEVE_FAILURE);
            return status;

        }

        status.setStatusCode(ResponseHelper.PREFIX_RETRIEVE_SUCCESS);

        return status;
    }

}
