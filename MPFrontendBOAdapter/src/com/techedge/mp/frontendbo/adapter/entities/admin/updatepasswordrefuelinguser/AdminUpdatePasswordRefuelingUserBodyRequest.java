package com.techedge.mp.frontendbo.adapter.entities.admin.updatepasswordrefuelinguser;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminUpdatePasswordRefuelingUserBodyRequest implements Validable {

    private String username;
    private String newPassword;

    public String getUsername() {
        return username;
    }

    public void setUsername(String role) {
        this.username = role;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String admin) {
        this.newPassword = admin;
    }

    public AdminUpdatePasswordRefuelingUserBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.newPassword == null || this.newPassword.isEmpty() || this.username == null || this.username.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_CREATE_REFUELING_USER_ERROR_PARAMETERS);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPD_PWD_REFUELING_USER_SUCCESS);

        return status;
    }

}
