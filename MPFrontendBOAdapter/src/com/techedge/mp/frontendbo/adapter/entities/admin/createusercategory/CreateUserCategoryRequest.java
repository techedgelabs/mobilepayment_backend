package com.techedge.mp.frontendbo.adapter.entities.admin.createusercategory;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class CreateUserCategoryRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private CreateUserCategoryBodyRequest body;

    public CreateUserCategoryRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateUserCategoryBodyRequest getBody() {
        return body;
    }

    public void setBody(CreateUserCategoryBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-USER-CATEGORY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_USER_CATEGORY_CREATE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_USER_CATEGORY_CREATE);

        return status;

    }

    @Override
    public BaseResponse execute() {

        CreateUserCategoryResponse createUserCategoryResponse = new CreateUserCategoryResponse();

        String response = getAdminServiceRemote().adminCreateUserCategory(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getName());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        createUserCategoryResponse.setStatus(status);

        return createUserCategoryResponse;
    }
}
