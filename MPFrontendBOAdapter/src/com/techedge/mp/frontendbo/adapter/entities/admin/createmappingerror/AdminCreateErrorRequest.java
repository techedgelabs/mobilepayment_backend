package com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminCreateErrorRequest extends AbstractBORequest implements Validable {

    private Status                      status = new Status();

    private Credential                  credential;
    private AdminCreateErrorBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminCreateErrorBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminCreateErrorBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("MAPPING-ERROR-CREATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_CREATE__INVALID_CODE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_CREATE__SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        AdminCreateErrorResponse adminCreateErrorResponse = new AdminCreateErrorResponse();

        String response = getAdminServiceRemote().adminCreateMappingError(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getErrorCode(),
                this.getBody().getStatusCode());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        adminCreateErrorResponse.setStatus(status);

        return adminCreateErrorResponse;
    }

}