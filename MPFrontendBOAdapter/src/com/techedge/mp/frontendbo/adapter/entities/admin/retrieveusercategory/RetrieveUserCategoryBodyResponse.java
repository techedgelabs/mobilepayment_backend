package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusercategory;

import java.util.List;

public class RetrieveUserCategoryBodyResponse {

    private String        name;

    private List<Integer> userType;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getUserType() {
        return userType;
    }

    public void setUserType(List<Integer> userType) {
        this.userType = userType;
    }

}
