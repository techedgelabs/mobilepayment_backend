package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.user.RetrieveUserListData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.CardInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.UserInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherInfo;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrieveUsersRequest extends AbstractBORequest implements Validable {

    private Status                   status = new Status();

    private Credential               credential;
    private RetrieveUsersRequestBody body;

    public RetrieveUsersRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveUsersRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveUsersRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-USERS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        Timestamp start = null;
        Timestamp end = null;

        if (this.getBody().getCreationDateStart() != null) {
            start = new Timestamp(this.getBody().getCreationDateStart());
        }
        if (this.getBody().getCreationDateEnd() != null) {
            end = new Timestamp(this.getBody().getCreationDateEnd());
        }

        RetrieveUserListData retrieveUsersData = getAdminServiceRemote().adminUserRetrieve(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getId(), this.getBody().getFirstName(), this.getBody().getLastName(), this.getBody().getFiscalCode(), this.getBody().getEmail(),
                this.getBody().getStatus(), this.getBody().getExternalUserId(), this.getBody().getMinAvailableCap(), this.getBody().getMaxAvailableCap(),
                this.getBody().getMinEffectiveCap(), this.getBody().getMaxEffectiveCap(), start, end,  this.getBody().getUserType());

        RetrieveUsersResponse retrieveUsersResponse = new RetrieveUsersResponse();

        List<User> userList = retrieveUsersData.getUserList();

        if (!userList.isEmpty()) {

            for (User user : userList) {

                UserInfo userInfo = new UserInfo();
                userInfo.setBirthDate(this.checkField(user.getPersonalData().getBirthDate()));
                userInfo.setBirthMunicipality(this.checkField(user.getPersonalData().getBirthMunicipality()));
                userInfo.setBirthProvince(this.checkField(user.getPersonalData().getBirthProvince()));
                userInfo.setCapAvailable(user.getCapAvailable());
                userInfo.setCapEffective(user.getCapEffective());
                userInfo.setEmail(user.getPersonalData().getSecurityDataEmail());
                userInfo.setExternalUserId(this.checkField(user.getExternalUserId()));
                userInfo.setFirstName(this.checkField(user.getPersonalData().getFirstName()));
                userInfo.setFiscalCode(this.checkField(user.getPersonalData().getFiscalCode()));
                userInfo.setId(user.getId());
                userInfo.setLanguage(this.checkField(user.getPersonalData().getLanguage()));
                userInfo.setUserType(user.getUserType());

                com.techedge.mp.frontendbo.adapter.entities.common.LastLoginData lastLoginData = new com.techedge.mp.frontendbo.adapter.entities.common.LastLoginData();

                if (user.getLastLoginData() != null) {
                    lastLoginData.setDeviceId(user.getLastLoginData().getDeviceId());
                    lastLoginData.setDeviceName(user.getLastLoginData().getDeviceName());
                    lastLoginData.setLatitude(user.getLastLoginData().getLatitude());
                    lastLoginData.setLongitude(user.getLastLoginData().getLongitude());
                    lastLoginData.setTime(user.getLastLoginData().getTime());
                }

                userInfo.setLastLoginData(lastLoginData);
                userInfo.setLastName(this.checkField(user.getPersonalData().getLastName()));
                userInfo.setMobilePhone(this.checkField(user.getContactDataMobilephone()));

                Set<com.techedge.mp.core.business.interfaces.PaymentInfo> paymentInfoList = user.getPaymentData();
                List<com.techedge.mp.frontendbo.adapter.entities.common.PaymentInfo> paymentInfoListOut = new ArrayList<com.techedge.mp.frontendbo.adapter.entities.common.PaymentInfo>(
                        0);
                com.techedge.mp.frontendbo.adapter.entities.common.PaymentInfo paymentInfoOut;
                for (PaymentInfo paymentInfoInput : paymentInfoList) {
                    paymentInfoOut = new com.techedge.mp.frontendbo.adapter.entities.common.PaymentInfo();
                    paymentInfoOut.setAttemptsLeft(paymentInfoInput.getAttemptsLeft());
                    paymentInfoOut.setBrand(paymentInfoInput.getBrand());
                    paymentInfoOut.setCheckAmount(paymentInfoInput.getCheckAmount());
                    paymentInfoOut.setDefaultMethod(this.checkField(paymentInfoInput.getDefaultMethod()));
                    Date dt;
                    Timestamp ts;
                    if (paymentInfoInput.getExpirationDate() != null) {
                        dt = paymentInfoInput.getExpirationDate();

                        ts = new Timestamp(dt.getTime());
                        paymentInfoOut.setExpirationDate(ts);
                    }
                    paymentInfoOut.setId(paymentInfoInput.getId());
                    if (paymentInfoInput.getInsertTimestamp() != null) {
                        dt = paymentInfoInput.getInsertTimestamp();
                        ts = new Timestamp(dt.getTime());
                        paymentInfoOut.setInsertTimestamp(ts);
                    }
                    paymentInfoOut.setMessage(paymentInfoInput.getMessage());
                    paymentInfoOut.setPan(paymentInfoInput.getPan());
                    paymentInfoOut.setStatus(paymentInfoInput.getStatus());
                    paymentInfoOut.setType(paymentInfoInput.getType());
                    if (paymentInfoInput.getVerifiedTimestamp() != null) {
                        dt = paymentInfoInput.getVerifiedTimestamp();
                        ts = new Timestamp(dt.getTime());
                        paymentInfoOut.setVerifiedTimestamp(ts);
                    }
                    paymentInfoListOut.add(paymentInfoOut);
                }

                userInfo.setPaymentData(paymentInfoListOut);
                userInfo.setRegistrationCompleted(this.checkField(user.getUserStatusRegistrationCompleted()));
                userInfo.setSex(this.checkField(user.getPersonalData().getSex()));
                userInfo.setStatus(user.getUserStatus());

                for (Voucher voucher : user.getVoucherList()) {

                    VoucherInfo voucherInfo = new VoucherInfo();

                    voucherInfo.setConsumedValue(voucher.getConsumedValue());
                    voucherInfo.setExpirationDate(voucher.getExpirationDate());
                    voucherInfo.setInitialValue(voucher.getInitialValue());
                    voucherInfo.setPromoCode(voucher.getPromoCode());
                    voucherInfo.setPromoDescription(voucher.getPromoDescription());
                    voucherInfo.setPromoDoc(voucher.getPromoDoc());
                    voucherInfo.setVoucherBalanceDue(voucher.getVoucherBalanceDue());
                    voucherInfo.setVoucherCode(voucher.getCode());
                    voucherInfo.setVoucherStatus(voucher.getStatus());
                    voucherInfo.setVoucherType(voucher.getType());
                    voucherInfo.setVoucherValue(voucher.getValue());

                    userInfo.getVoucherData().add(voucherInfo);
                }

                for (LoyaltyCard loyaltyCard : user.getLoyaltyCardList()) {

                    CardInfo cardInfo = new CardInfo();

                    cardInfo.setEanCode(loyaltyCard.getEanCode());
                    cardInfo.setPanCode(loyaltyCard.getPanCode());
                    cardInfo.setType(loyaltyCard.getType());

                    userInfo.getCardData().add(cardInfo);
                }
                
                userInfo.setVirtualizationCompleted(this.checkField(user.getVirtualizationCompleted()));
                userInfo.setVirtualizationAttemptsLeft(user.getVirtualizationAttemptsLeft());
                userInfo.setEniStationUserType(this.checkField(user.getEniStationUserType()));
                userInfo.setDepositCardStepCompleted(this.checkField(user.getDepositCardStepCompleted()));

                retrieveUsersResponse.getUsers().add(userInfo);
            }
        }

        status.setStatusCode(retrieveUsersData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveUsersData.getStatusCode()));

        retrieveUsersResponse.setStatus(status);

        return retrieveUsersResponse;
    }

    private String checkField(Object obj) {
        if (obj != null) {
            return obj.toString();
        }
        else {
            return "";
        }

    }
}
