package com.techedge.mp.frontendbo.adapter.entities.admin.generatepv;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.PvGenerationInfo;

public class AdminGeneratePvBodyResponse {

    private String                 errorCode;
    private String                 statusCode;
    private List<PvGenerationInfo> pvGenerationResultList = new ArrayList<PvGenerationInfo>(0);

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<PvGenerationInfo> getPvGenerationResultList() {
        return pvGenerationResultList;
    }

    public void setPvGenerationResultList(List<PvGenerationInfo> pvGenerationResultList) {
        this.pvGenerationResultList = pvGenerationResultList;
    }

}
