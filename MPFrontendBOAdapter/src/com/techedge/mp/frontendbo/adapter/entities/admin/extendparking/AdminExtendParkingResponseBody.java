package com.techedge.mp.frontendbo.adapter.entities.admin.extendparking;

import com.techedge.mp.parking.integration.business.interfaces.ExtendParkingResult;

public class AdminExtendParkingResponseBody {

    private ExtendParkingResult extendParkingResult;

    public ExtendParkingResult getExtendParkingResult() {
        return extendParkingResult;
    }

    public void setExtendParkingResult(ExtendParkingResult extendParkingResult) {
        this.extendParkingResult = extendParkingResult;
    }

}
