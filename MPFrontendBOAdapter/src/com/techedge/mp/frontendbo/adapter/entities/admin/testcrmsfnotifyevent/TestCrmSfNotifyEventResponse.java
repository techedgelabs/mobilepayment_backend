package com.techedge.mp.frontendbo.adapter.entities.admin.testcrmsfnotifyevent;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestCrmSfNotifyEventResponse extends BaseResponse{

    private String            csTransactionID;
    private Long              requestTimestamp;
    private Boolean           success;
    private String            errorCode;
    private String            message;
    private String            requestId;

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

    

}
