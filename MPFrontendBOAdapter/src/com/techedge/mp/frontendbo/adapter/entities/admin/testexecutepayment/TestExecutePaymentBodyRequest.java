package com.techedge.mp.frontendbo.adapter.entities.admin.testexecutepayment;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestExecutePaymentBodyRequest implements Validable {

    private String  operationId;
    private Integer amount;
    private String  productCode;
    private Integer quantity;
    private Integer unitPrice;
    private String  authCryptogram;
    private String  currencyCode;
    private String  mcCardDpan;
    private String  refuelMode;
    private String  shopCode;
    private String  partnerType;
    private Long    requestTimestamp;

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = unitPrice;
    }

    public String getAuthCryptogram() {
        return authCryptogram;
    }

    public void setAuthCryptogram(String authCryptogram) {
        this.authCryptogram = authCryptogram;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getMcCardDpan() {
        return mcCardDpan;
    }

    public void setMcCardDpan(String mcCardDpan) {
        this.mcCardDpan = mcCardDpan;
    }

    public String getRefuelMode() {
        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationId == null || this.operationId.trim().isEmpty() || this.amount == null || this.productCode == null || this.productCode.trim().isEmpty()
                || this.quantity == null || this.unitPrice == null || this.authCryptogram == null || this.authCryptogram.trim().isEmpty() || this.currencyCode == null
                || this.currencyCode.trim().isEmpty() || this.mcCardDpan == null || this.mcCardDpan.trim().isEmpty() || this.refuelMode == null || this.refuelMode.trim().isEmpty()
                || this.shopCode == null || this.shopCode.trim().isEmpty() || this.partnerType == null || this.partnerType.trim().isEmpty() || this.requestTimestamp == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_EXECUTE_PAYMENT_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_EXECUTE_PAYMENT_SUCCESS);

        return status;
    }

}
