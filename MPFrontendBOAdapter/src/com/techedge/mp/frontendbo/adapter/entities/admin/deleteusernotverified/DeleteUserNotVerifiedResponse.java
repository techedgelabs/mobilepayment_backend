package com.techedge.mp.frontendbo.adapter.entities.admin.deleteusernotverified;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.AdminUserNotVerifiedDeleteSingleResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class DeleteUserNotVerifiedResponse extends BaseResponse {

    private List<AdminUserNotVerifiedDeleteSingleResult> results = new ArrayList<AdminUserNotVerifiedDeleteSingleResult>(0);

    public List<AdminUserNotVerifiedDeleteSingleResult> getResults() {
        return results;
    }

    public void setResults(List<AdminUserNotVerifiedDeleteSingleResult> results) {
        this.results = results;
    }

}
