package com.techedge.mp.frontendbo.adapter.entities.admin.updateparam;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class UpdateParamRequest extends AbstractBORequest implements Validable {

    private Status                 status = new Status();

    private Credential             credential;
    private UpdateParamRequestBody body;

    public UpdateParamRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateParamRequestBody getBody() {
        return body;
    }

    public void setBody(UpdateParamRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_PARAM_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        // da utilizzare anche per
        // l'inserimento(se non esiste
        // crea)

        UpdateParamResponse updateParamResponse = new UpdateParamResponse();

        String paramValue = "";
        if (this.getBody().getValue() != null) {
            paramValue = this.getBody().getValue();
        }

        String response = getAdminServiceRemote().adminUpdateParam(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getParam(), paramValue,
                this.getBody().getOperation());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updateParamResponse.setStatus(status);

        return updateParamResponse;
    }

}
