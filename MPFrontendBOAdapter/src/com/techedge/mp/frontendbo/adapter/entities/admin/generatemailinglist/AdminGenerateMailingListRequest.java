package com.techedge.mp.frontendbo.adapter.entities.admin.generatemailinglist;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminGenerateMailingListRequest extends AbstractBORequest implements Validable {

    private Status                              status = new Status();

    private Credential                          credential;
    private AdminGenerateMailingListBodyRequest body;

    public AdminGenerateMailingListRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminGenerateMailingListBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminGenerateMailingListBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GENERATE-MAILING-LIST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_GENERATE_MAILING_LIST_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String adminGenerateMailingListResult = getAdminServiceRemote().adminGenerateMailingList(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getSelectionType());

        AdminGenerateMailingListResponse adminGenerateMailingListResponse = new AdminGenerateMailingListResponse();

        status.setStatusCode(adminGenerateMailingListResult);
        status.setStatusMessage(prop.getProperty(adminGenerateMailingListResult));
        adminGenerateMailingListResponse.setStatus(status);

        return adminGenerateMailingListResponse;

    }

}
