package com.techedge.mp.frontendbo.adapter.entities.admin.contactkeybulk;

import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;



public class AdminContactKeyBulkBodyRequest implements Validable {

	private List<ContactKeyObj> listContactKeys;
	private String requestID;
	private String operationID;

	public List<ContactKeyObj> getListContactKeys() {
		return listContactKeys;
	}

	public void setListContactKeys(List<ContactKeyObj> listContactKeys) {
		this.listContactKeys = listContactKeys;
	}

	public String getRequestID() {
		return requestID;
	}

	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	public String getOperationID() {
		return operationID;
	}

	public void setOperationID(String operationID) {
		this.operationID = operationID;
	}

	@Override
	public Status check() {

		Status status = new Status();

		// status.setStatusCode(StatusCode.USER_V2_AUTH_EMAIL_WRONG);

		status.setStatusCode(StatusCode.ADMIN_CONTACT_KEY_BULK_SUCCESS);

		return status;
	}

}
