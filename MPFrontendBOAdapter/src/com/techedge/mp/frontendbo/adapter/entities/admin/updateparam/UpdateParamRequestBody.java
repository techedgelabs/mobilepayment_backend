package com.techedge.mp.frontendbo.adapter.entities.admin.updateparam;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdateParamRequestBody implements Validable {
	

	private String param;
	private String value;
	private String operation ; //(N= NEW, U= Update, D = Delete)
		

	
	public String getParam() {
		return param;
	}

	public void setParam(String param) {
		this.param = param;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}
	
	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.param == null) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PARAM_ERROR_PARAMETERS);
			return status;
		}
		if(this.value == null) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PARAM_ERROR_PARAMETERS);
			return status;
		}
		if(this.operation == null) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PARAM_ERROR_PARAMETERS);
			return status;
		}
		
		
		status.setStatusCode(StatusCode.ADMIN_UPDATE_PARAM_SUCCESS);

		return status;
	}

	
}
