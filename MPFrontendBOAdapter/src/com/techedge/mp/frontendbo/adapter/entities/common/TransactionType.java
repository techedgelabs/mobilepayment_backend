package com.techedge.mp.frontendbo.adapter.entities.common;

public enum TransactionType {
    PRE_PAID("PRE-PAID"), POST_PAID("POST-PAID");

    private final String code;

    TransactionType(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}
