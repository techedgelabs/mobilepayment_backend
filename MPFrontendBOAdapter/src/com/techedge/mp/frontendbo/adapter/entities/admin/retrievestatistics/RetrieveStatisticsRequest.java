package com.techedge.mp.frontendbo.adapter.entities.admin.retrievestatistics;

import com.techedge.mp.core.business.interfaces.RetrieveStatisticsData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrieveStatisticsRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private RetrieveStatisticsRequestBody body;

    public RetrieveStatisticsRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveStatisticsRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveStatisticsRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_STATISTICS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        RetrieveStatisticsResponse retrieveStatisticsResponse = new RetrieveStatisticsResponse();

        RetrieveStatisticsData retrieveStatisticsData = getAdminServiceRemote().adminRetrieveStatistics(this.getCredential().getTicketID(), this.getCredential().getRequestID());

        status.setStatusCode(retrieveStatisticsData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveStatisticsData.getStatusCode()));

        retrieveStatisticsResponse.setStatus(status);
        retrieveStatisticsResponse.setStatisticList(retrieveStatisticsData.getStatisticsList());

        return retrieveStatisticsResponse;
    }

}
