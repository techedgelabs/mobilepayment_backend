package com.techedge.mp.frontendbo.adapter.entities.admin.estimateendparkingprice;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminEstimateEndParkingPriceResponse extends BaseResponse{
    
    private AdminEstimateEndParkingPriceResponseBody body;

    public AdminEstimateEndParkingPriceResponseBody getBody() {
        return body;
    }

    public void setBody(AdminEstimateEndParkingPriceResponseBody body) {
        this.body = body;
    }
}
