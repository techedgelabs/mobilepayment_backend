package com.techedge.mp.frontendbo.adapter.entities.admin.testcheckvoucher;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherInfo;

public class TestCheckVoucherResponse extends BaseResponse{

    private String            csTransactionID;
    private Long              requestTimestamp;
    private List<VoucherInfo> voucherList = new ArrayList<VoucherInfo>(0);

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public List<VoucherInfo> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<VoucherInfo> voucherList) {
        this.voucherList = voucherList;
    }

}
