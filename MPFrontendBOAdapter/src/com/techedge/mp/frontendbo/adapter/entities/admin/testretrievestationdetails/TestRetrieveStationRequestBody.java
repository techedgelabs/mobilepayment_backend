package com.techedge.mp.frontendbo.adapter.entities.admin.testretrievestationdetails;

import java.util.Calendar;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestRetrieveStationRequestBody implements Validable {

    private String pumpID;
    private String requestID;
    private String stationID;
    private Boolean pumpDetailsReq;
    
    
    public String getPumpID() {
        return pumpID;
    }

    public void setPumpID(String pumpID) {
        this.pumpID = pumpID;
    }

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public Boolean getPumpDetailsReq() {
        return pumpDetailsReq;
    }

    public void setPumpDetailsReq(Boolean pumpDetailsReq) {
        this.pumpDetailsReq = pumpDetailsReq;
    }

    @Override
    public Status check() {
        
        Status status = new Status();
        
        
        if(this.pumpID == null && this.stationID == null) {
            status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS);
            return status;
        }
        if (this.pumpDetailsReq == null) {
            this.pumpDetailsReq = false;
        }
        if (this.requestID == null) {
            Long timestamp = Calendar.getInstance().getTimeInMillis();
            requestID = String.valueOf(timestamp);

        }
        
        
        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);
        return status;
    }

}
