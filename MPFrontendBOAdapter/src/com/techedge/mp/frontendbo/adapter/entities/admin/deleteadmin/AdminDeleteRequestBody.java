package com.techedge.mp.frontendbo.adapter.entities.admin.deleteadmin;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminDeleteRequestBody implements Validable {
	
	private Long adminID;
	
	
	public Long getAdminID() {
		return adminID;
	}
	public void setAdminID(Long adminID) {
		this.adminID = adminID;
	}

	@Override
	public Status check() {
		
		Status status = new Status();
		
		if (adminID == null || (adminID != null && adminID ==0)) {
		      status.setStatusCode(StatusCode.ADMIN_DELETE_ERROR_PARAMETERS);
		      return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_DELETE_SUCCESS);

		return status;
	}
	
}
