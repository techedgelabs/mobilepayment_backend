package com.techedge.mp.frontendbo.adapter.entities.admin.updatecities;

import java.util.ArrayList;

import sun.misc.BASE64Decoder;

import com.techedge.mp.core.business.interfaces.CityInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminUpdateCitiesRequest extends AbstractBORequest implements Validable {

    private Credential              credential;
    private AdminUpdateCitiesBodyRequest body;

    public AdminUpdateCitiesRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminUpdateCitiesBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminUpdateCitiesBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-CITIES", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_CITIES_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        AdminUpdateCitiesBodyRequest adminUpdateCitiesBodyRequest = this.getBody();
        AdminUpdateCitiesResponse adminUpdateCitiesResponse = new AdminUpdateCitiesResponse();
        
        Status status = new Status();
        String adminTicketId = this.getCredential().getTicketID();
        String requestId = this.getCredential().getRequestID();
        String fileStreamBase64 = adminUpdateCitiesBodyRequest.getFileStreamBase64();
        String columnSeparator = adminUpdateCitiesBodyRequest.getColumnSeparator();
        String rowSeparator = adminUpdateCitiesBodyRequest.getRowSeparator();
        boolean skipFirstRow = Boolean.parseBoolean(adminUpdateCitiesBodyRequest.getSkipFirstRow());
        boolean deleteAllRows = Boolean.parseBoolean(adminUpdateCitiesBodyRequest.getDeleteAllRows());

        ArrayList<CityInfo> citiesList = new ArrayList<CityInfo>();
        String content = null;
        int interfaceFieldsLenght = CityInfo.class.getDeclaredFields().length - 1; //Elimino dal conteggio il campo serialVersionUID
        
        try {
            BASE64Decoder decoder = new BASE64Decoder();
            byte[] byteContent = decoder.decodeBuffer(fileStreamBase64);
            content = new String(byteContent);
        }
        catch (Exception ex) {
            System.err.println("Errore nella lettura dello stream: " + ex.getMessage());
            
            status.setStatusCode(StatusCode.ADMIN_UPDATE_CITIES_ERROR_DECODE);
            status.setStatusMessage(prop.getProperty(StatusCode.ADMIN_UPDATE_CITIES_ERROR_DECODE));
            adminUpdateCitiesResponse.setStatus(status);

            return adminUpdateCitiesResponse;
        }
        
        String[] rows = content.split(rowSeparator);
        int row = (skipFirstRow) ? 1 : 0;
        for (; row < rows.length; row++) {
            String line = rows[row];
            String[] columns = line.split(columnSeparator);
            
            if (columns.length != interfaceFieldsLenght) {
                System.err.println("Errore! Lo stream contiene un numero di colonne (" + columns.length + ") diverso dall'interfaccia CityInfo (" + interfaceFieldsLenght + ")");
                
                status.setStatusCode(StatusCode.ADMIN_UPDATE_CITIES_ERROR_COLUMNS_COUNT);
                status.setStatusMessage(prop.getProperty(StatusCode.ADMIN_UPDATE_CITIES_ERROR_COLUMNS_COUNT));
                adminUpdateCitiesResponse.setStatus(status);

                return adminUpdateCitiesResponse;
            }
            
            CityInfo cityInfo = new CityInfo();
            cityInfo.setName(columns[0]);
            cityInfo.setProvince(columns[1]);
            cityInfo.setCode(columns[2]);
            
            citiesList.add(cityInfo);
        }
        
        String response = getAdminServiceRemote().adminUpdateCities(adminTicketId, requestId, citiesList, deleteAllRows);
        
        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        adminUpdateCitiesResponse.setStatus(status);

        return adminUpdateCitiesResponse;
    }
    
}
