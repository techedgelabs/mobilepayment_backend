package com.techedge.mp.frontendbo.adapter.entities.admin.reconciliationdetail;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class ReconciliationDetailRequestBody implements Validable {
	
	String transactionId = new String();
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		status.setStatusCode(StatusCode.ADMIN_RECONCILIATION_SUCCESS);

		return status;
	}
}
