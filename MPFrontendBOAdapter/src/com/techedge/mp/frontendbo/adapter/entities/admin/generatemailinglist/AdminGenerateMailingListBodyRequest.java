package com.techedge.mp.frontendbo.adapter.entities.admin.generatemailinglist;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminGenerateMailingListBodyRequest implements Validable {

    private String selectionType;

    public AdminGenerateMailingListBodyRequest() {}

    public String getSelectionType() {
        return selectionType;
    }

    public void setSelectionType(String selectionType) {
        this.selectionType = selectionType;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.selectionType == null || this.selectionType.trim().equals("")) {
            status.setStatusCode(StatusCode.ADMIN_GENERATE_MAILING_LIST_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_GENERATE_MAILING_LIST_SUCCESS);

        return status;
    }

}
