package com.techedge.mp.frontendbo.adapter.entities.common;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.techedge.mp.core.business.interfaces.ResponseHelper;

public class Validator {

    //private final static Integer MAX_HISTORY_ENTRIES      = 20;

    private final static String regex_password_maiuscola = ".*[A-Z].*";
    private final static String regex_password_minuscola = ".*[a-z].*";
    private final static String regex_password_numerica  = ".*[0-9].*";

    private final static String regex_email              = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";

    public static Status checkPassword(String operation, String password) {

        Status status = new Status();

        if (password == null || password.length() > 40) {

            status.setStatusCode(StatusCode.ADMIN_CREATE_PASSWORD_WRONG);

            return status;

        }
        else {

            if (password.length() < 8) {

                status.setStatusCode(StatusCode.ADMIN_CREATE_PASSWORD_SHORT);

                return status;

            }

            Pattern patternPasswordMaiuscola = Pattern.compile(regex_password_maiuscola);
            Matcher matchePasswordMaiuscola = patternPasswordMaiuscola.matcher(password);
            Pattern patternPasswordMinuscola = Pattern.compile(regex_password_minuscola);
            Matcher matchePasswordMinuscola = patternPasswordMinuscola.matcher(password);
            Pattern patternPasswordNumerica = Pattern.compile(regex_password_numerica);
            Matcher matchePasswordNumerica = patternPasswordNumerica.matcher(password);

            if (!matchePasswordMaiuscola.matches()) {

                status.setStatusCode(StatusCode.ADMIN_CREATE_UPPER_LESS);

                return status;

            }

            if (!matchePasswordMinuscola.matches()) {

                status.setStatusCode(StatusCode.ADMIN_CREATE_LOWER_LESS);

                return status;

            }

            if (!matchePasswordNumerica.matches()) {

                status.setStatusCode(StatusCode.ADMIN_CREATE_NUMBER_LESS);

                return status;

            }

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

        return status;

    }

    public static Status checkEmail(String operation, String email) {

        Status status = new Status();

        if (email == null || email.length() > 50) {

            status.setStatusCode(StatusCode.ADMIN_CREATE_EMAIL_WRONG);

            return status;

        }
        else {

            Pattern patternEmail = Pattern.compile(regex_email);
            Matcher matcherEmail = patternEmail.matcher(email);

            if (!matcherEmail.matches()) {

                status.setStatusCode(StatusCode.ADMIN_CREATE_EMAIL_WRONG);

                return status;

            }

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

        return status;

    }

    public static Status checkCredential(String operation, Credential credential) {

        Status status = new Status();

        if (credential != null) {

            if (credential.getTicketID() == null || credential.getTicketID().length() != 32) {

                status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_TICKET);

                return status;

            }

            if (credential.getRequestID() == null || credential.getRequestID().length() > 50) {

                status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        if (operation.equals("DELETE-MANAGER")) {

            status.setStatusCode(StatusCode.ADMIN_DELETE_MANAGER_SUCCESS);

            return status;

        }

        if (operation.equals("CREATE-MANAGER")) {

            status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS);

            return status;

        }

        if (operation.equals("UPDATE-MANAGER")) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_MANAGER_SUCCESS);

            return status;

        }

        if (operation.equals("SEARCH-MANAGER")) {

            status.setStatusCode(StatusCode.ADMIN_SEARCH_MANAGER_SUCCESS);

            return status;

        }

        if (operation.equals("ADDSTATION-MANAGER")) {

            status.setStatusCode(StatusCode.ADMIN_ADDSTATION_MANAGER_SUCCESS);

            return status;

        }

        if (operation.equals("RETRIEVE-TRANSACTIONS-MANAGER")) {

            status.setStatusCode(StatusCode.ADMIN_RETRIEVETRANSACTIONS_MANAGER_SUCCESS);

            return status;

        }

        if (operation.equals("ADDVOUCHERTOPROMOTION")) {

            status.setStatusCode(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_SUCCESS);

            return status;

        }

        if (operation.equals("CREATE-SURVEY")) {

            status.setStatusCode(StatusCode.ADMIN_SURVEY_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("UPDATE-SURVEY")) {

            status.setStatusCode(StatusCode.ADMIN_SURVEY_UPDATE_SUCCESS);

            return status;

        }

        if (operation.equals("GET-SURVEY")) {

            status.setStatusCode(StatusCode.ADMIN_SURVEY_GET_SUCCESS);

            return status;

        }

        if (operation.equals("RESET-SURVEY")) {

            status.setStatusCode(StatusCode.ADMIN_SURVEY_RESET_SUCCESS);

            return status;

        }

        if (operation.equals("ADD-SURVEY-QUESTION")) {

            status.setStatusCode(StatusCode.ADMIN_SURVEY_QUESTION_ADD_SUCCESS);

            return status;

        }

        if (operation.equals("DELETE-SURVEY-QUESTION")) {

            status.setStatusCode(StatusCode.ADMIN_SURVEY_QUESTION_DELETE_SUCCESS);

            return status;

        }

        if (operation.equals("MAPPING-ERROR-CREATE")) {

            status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_CREATE__SUCCESS);

            return status;

        }

        if (operation.equals("MAPPING-ERROR-EDIT")) {

            status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_EDIT__SUCCESS);

            return status;

        }

        if (operation.equals("MAPPING-ERROR-REMOVE")) {

            status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_REMOVE__SUCCESS);

            return status;

        }

        if (operation.equals("MAPPING-ERROR-GET")) {

            status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_GET__SUCCESS);

            return status;

        }

        if (operation.equals("CONSUME-VOUCHER")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_CONSUME_VOUCHER_SUCCESS);

            return status;

        }

        if (operation.equals("REVERSE-VOUCHER")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_REVERSE_CONSUME_VOUCHER_SUCCESS);

            return status;

        }

        if (operation.equals("CHECK-VOUCHER")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_CHECK_VOUCHER_SUCCESS);

            return status;

        }

        if (operation.equals("CHECK-LOYALTY")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_LOAD_CHECK_LOYALTY_CREDITS_TRANSACTION_SUCCESS);

            return status;

        }

        if (operation.equals("ENABLE-LOYALTY")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_ENABLE_LOYALTY_CARD_SUCCESS);

            return status;

        }

        if (operation.equals("GET-LOYALTY")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_GET_LOYALTY_CARD_SUCCESS);

            return status;

        }

        if (operation.equals("LOAD-LOYALTY")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_SUCCESS);

            return status;

        }

        if (operation.equals("REVERSE-LOAD-LOYALTY")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_SUCCESS);

            return status;

        }

        if (operation.equals("CREATE-CONSUME-VOUCHER")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_CREATE_CONSUME_VOUCHER_SUCCESS);

            return status;

        }

        if (operation.equals("REVERSE-CREATE-CONSUME-VOUCHER")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_REVERSE_CREATE_CONSUME_VOUCHER_SUCCESS);

            return status;

        }

        if (operation.equals("PRE-AUTHORIZATION-CONSUME-VOUCHER")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS);

            return status;

        }

        if (operation.equals("CANCEL-PRE-AUTHORIZATION-CONSUME-VOUCHER")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS);

            return status;

        }

        if (operation.equals("CREATE-USER-CATEGORY")) {

            status.setStatusCode(StatusCode.ADMIN_USER_CATEGORY_CREATE);

            return status;

        }

        if (operation.equals("CREATE-USER-TYPE")) {

            status.setStatusCode(StatusCode.ADMIN_USER_TYPE_CREATE);

            return status;

        }
        if (operation.equals("RETRIEVE-USER-CATEGORY")) {

            status.setStatusCode(StatusCode.ADMIN_USER_CATEGORY_RETRIEVE);

            return status;

        }

        if (operation.equals("RETRIEVE-USER-TYPE")) {

            status.setStatusCode(StatusCode.ADMIN_USER_TYPE_RETRIEVE);

            return status;

        }

        if (operation.equals("ADD_TYPE_TO_CATEGORY")) {

            status.setStatusCode(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE);

            return status;

        }

        if (operation.equals("REMOVE_TYPE_CATEGORY")) {

            status.setStatusCode(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE);

            return status;

        }

        if (operation.equals("REMOVE-PREFIX")) {

            status.setStatusCode(ResponseHelper.PREFIX_REMOVE_SUCCESS);

            return status;

        }

        if (operation.equals("CREATE-PREFIX")) {

            status.setStatusCode(ResponseHelper.PREFIX_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("CREATE-VOUCHER")) {

            status.setStatusCode(ResponseHelper.VOUCHER_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("RETRIEVE-VOUCHER-TRANSACTIONS")) {

            status.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_VOUCHER_TRANSACTIONS_SUCCESS);

            return status;

        }

        if (operation.equals("FORCE-UPDATE-TERM-OF-SERVICE")) {

            status.setStatusCode(ResponseHelper.ADMIN_UPDATE_TERMS_OF_SERVICE_SUCCESS);

            return status;

        }

        if (operation.equals("CREATE-BLOCK-PERIOD")) {

            status.setStatusCode(ResponseHelper.ADMIN_BLOCK_PERIOD_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("UPDATE-BLOCK-PERIOD")) {

            status.setStatusCode(ResponseHelper.ADMIN_BLOCK_PERIOD_UPDATE_SUCCESS);

            return status;

        }

        if (operation.equals("DELETE-BLOCK-PERIOD")) {

            status.setStatusCode(ResponseHelper.ADMIN_BLOCK_PERIOD_DELETE_SUCCESS);

            return status;

        }

        if (operation.equals("RETRIEVE-BLOCK-PERIOD")) {

            status.setStatusCode(ResponseHelper.ADMIN_BLOCK_PERIOD_RETRIEVE_SUCCESS);

            return status;

        }

        if (operation.equals("CREATE-DOCUMENT")) {

            status.setStatusCode(ResponseHelper.ADMIN_DOCUMENT_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("DELETE-DOCUMENT")) {

            status.setStatusCode(ResponseHelper.ADMIN_DOCUMENT_DELETE_SUCCESS);

            return status;

        }

        if (operation.equals("UPDATE-DOCUMENT")) {

            status.setStatusCode(ResponseHelper.ADMIN_DOCUMENT_UPDATE_SUCCESS);

            return status;

        }

        if (operation.equals("RETRIEVE-DOCUMENT")) {

            status.setStatusCode(ResponseHelper.ADMIN_DOCUMENT_RETRIEVE_SUCCESS);

            return status;

        }

        if (operation.equals("CREATE-DOCUMENT-ATTRIBUTE")) {

            status.setStatusCode(ResponseHelper.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("DELETE-DOCUMENT-ATTRIBUTE")) {

            status.setStatusCode(ResponseHelper.ADMIN_DOCUMENT_ATTRIBUTE_DELETE_SUCCESS);

            return status;

        }

        if (operation.equals("GENERATE-PV")) {

            status.setStatusCode(StatusCode.ADMIN_GENERATE_PV_SUCCESS);

            return status;

        }

        if (operation.equals("FORCE-SMS-NOTIFICATION-STATUS")) {

            status.setStatusCode(StatusCode.ADMIN_GENERATE_PV_SUCCESS);

            return status;

        }

        if (operation.equals("GENERATE-MAILING-LIST")) {

            status.setStatusCode(StatusCode.ADMIN_GENERATE_MAILING_LIST_SUCCESS);

            return status;

        }

        if (operation.equals("UPDATE-MAILING-LIST")) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_MAILING_LIST_SUCCESS);

            return status;

        }

        if (operation.equals("ASSIGN-VOUCHER")) {

            status.setStatusCode(StatusCode.ADMIN_ASSIGN_VOUCHER_SUCCESS);

            return status;

        }

        if (operation.equals("REMOVE-TRANSACTION-EVENT")) {

            status.setStatusCode(StatusCode.ADMIN_REMOVE_TRANSACTION_EVENT_SUCCESS);

            return status;

        }

        if (operation.equals("CREATE-MAIL-BLACKLIST")) {

            status.setStatusCode(StatusCode.MAIL_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("INSERTUSERINPROMOONHOLD")) {

            status.setStatusCode(StatusCode.ADMIN_INSERT_USER_IN_PROMO_ONHOLD_SUCCESS);

            return status;

        }

        if (operation.equals("DELETE-VOUCHER")) {

            status.setStatusCode(StatusCode.ADMIN_DELETE_VOUCHER_SUCCESS);

            return status;

        }

        if (operation.equals("REMOVE-MOBILE-PHONE")) {

            status.setStatusCode(StatusCode.ADMIN_REMOVE_MOBILE_PHONE_SUCCESS);

            return status;

        }

        if (operation.equals("INFO-REDEMPTION")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_INFO_REDEMPTION_SUCCESS);

            return status;

        }

        if (operation.equals("REDEMPTION")) {

            status.setStatusCode(StatusCode.ADMIN_TEST_REDEMPTION_SUCCESS);

            return status;

        }

        if (operation.equals("GFG-TRANSACTION-RECONCILIATION")) {

            status.setStatusCode(StatusCode.ADMIN_GFG_TRANSACTION_RECONCILIATION_SUCCESS);

            return status;

        }

        if (operation.equals("REFRESH-PARAMS")) {

            status.setStatusCode(StatusCode.ADMIN_REFRESH_PARAMS_SUCCESS);

            return status;

        }

        if (operation.equals("PROPAGATION-USERDATA")) {

            status.setStatusCode(StatusCode.ADMIN_PROPAGATION_USERDATA_SUCCESS);

            return status;

        }

        if (operation.equals("CHECK-LEGACY-PASSWORD")) {

            status.setStatusCode(StatusCode.ADMIN_CHECK_LEGACY_PWD_SUCCESS);

            return status;

        }

        if (operation.equals("GENERAL-CREATE")) {

            status.setStatusCode(StatusCode.ADMIN_GENERAL_CREATE_SUCCESS);

            return status;

        }

        if (operation.equals("GENERAL-UPDATE")) {

            status.setStatusCode(StatusCode.ADMIN_GENERAL_UPDATE_SUCCESS);

            return status;

        }

        if (operation.equals("REMOVE")) {

            status.setStatusCode(StatusCode.ADMIN_REMOVE_SUCCESS);

            return status;

        }

        if (operation.equals("PUSH-NOTIFICATION-TEST")) {

            status.setStatusCode(StatusCode.ADMIN_PUSH_NOTIFICATION_TEST_SUCCESS);

            return status;

        }
        
        if (operation.equals("ADMIN-RETRIEVE")) {

            status.setStatusCode(StatusCode.ADMIN_RETRIEVE_SUCCESS);

            return status;

        }
        if (operation.equals("ADMIN-UPDATE")) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_SUCCESS);

            return status;

        }
        if (operation.equals("ADMIN-DELETE")) {

            status.setStatusCode(StatusCode.ADMIN_DELETE_SUCCESS);

            return status;

        }
        
        if (operation.equals("ADMIN-CREATE-REFUELING-USER")) {

            status.setStatusCode(StatusCode.ADMIN_CREATE_REFUELING_USER_SUCCESS);

            return status;

        }

        if (operation.equals("ADMIN-UPD-PWD-REFUELING-USER")) {

            status.setStatusCode(StatusCode.ADMIN_UPD_PWD_REFUELING_USER_SUCCESS);

            return status;

        }

        if (operation.equals("ADMIN-INSERT_PAY_METH-REFUELING-USER")) {

            status.setStatusCode(StatusCode.ADMIN_INSERT_PAY_METH_REFUELING_USER_SUCCESS);

            return status;

        }
        
        if (operation.equals("ADMIN-RETRIEVE-PAYMENT-DATA-REFUELING-USER")) {

            status.setStatusCode(StatusCode.ADMIN_RETR_PAY_DATA_REFUELING_USER_SUCCESS);

            return status;

        }
        
        if (operation.equals("ADMIN-RM-PAYMENT-METHOD-REFUELING-USER")) {

            status.setStatusCode(StatusCode.ADMIN_RM_PAY_METH_REFUELING_USER_SUCCESS);

            return status;

        }
        
        if (operation.equals("MASSIVE-REMOVE")) {

            status.setStatusCode(StatusCode.ADMIN_REMOVE_SUCCESS);

            return status;

        }
        
        if (operation.equals("RETRIEVE-LANDING")) {

            status.setStatusCode(StatusCode.ADMIN_RETRIEVE_LANDING_SUCCESS);

            return status;

        }
        if (operation.equals("RETRIEVE-POLLING-INTERVAL")) {

            status.setStatusCode(StatusCode.RETRIEVE_POLLING_INTERVAL_SUCCESS);

            return status;

        }
        
        if (operation.equals("UPDATE-CITIES")) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_CITIES_SUCCESS);

            return status;

        }

        if (operation.equals("RECONCILIATION-USER")) {

            status.setStatusCode(StatusCode.ADMIN_RECONCILIATION_SUCCESS);

            return status;

        }
        
        if (operation.equals("TICKET-REMOVE")) {

            status.setStatusCode(StatusCode.ADMIN_REMOVE_TICKET_SUCCESS);

            return status;

        }
        
        if (operation.equals("DWH-GET-PARTNER-LIST")) {

            status.setStatusCode(StatusCode.ADMIN_RECONCILIATION_SUCCESS);

            return status;

        }
        
        if (operation.equals("DWH-GET-CATEGORY-EARN-LIST")) {

            status.setStatusCode(StatusCode.ADMIN_RECONCILIATION_SUCCESS);

            return status;

        }
        
        if (operation.equals("DWH-GET-CATALOG-LIST")) {

            status.setStatusCode(StatusCode.ADMIN_RECONCILIATION_SUCCESS);

            return status;

        }
        
        if (operation.equals("DWH-GET-BRAND-LIST")) {

            status.setStatusCode(StatusCode.ADMIN_RECONCILIATION_SUCCESS);

            return status;

        }
        
        if (operation.equals("SYSTEM-CHECK")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }
        
        if (operation.equals("GET-MC-CARD-STATUS")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }
        
        if (operation.equals("EXECUTE-PAYMENT")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }
        
        if (operation.equals("EXECUTE-AUTHORIZATION")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }

        if (operation.equals("EXECUTE-CAPTURE")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }
        
        if (operation.equals("EXECUTE-REVERSAL")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }
        
        if (operation.equals("MC-ENJOY-CARD-AUTHORIZE")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }
        
        if (operation.equals("DELETE-MC-CARD-REFUELING")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }
        
        if (operation.equals("GET-PAYMENT-STATUS")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }
        
        if (operation.equals("AUTHORIZATION-PLUS")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }
        
        if (operation.equals("GET-PAYMENT-TRANSACTION-DETAILS")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }
        
        if (operation.equals("ADMIN-INSERT-MULTICARD-REFUELING-USER")) {

            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

            return status;

        }
        
        if (operation.equals("COUNT-PENDING-TRANSACTIONS")) {

            status.setStatusCode(StatusCode.ADMIN_COUNT_PENDING_TRANSACTIONS_SUCCESS);

            return status;

        }
        
        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_USERS_SUCCESS);

        return status;

    }

    public static boolean isValid(String statusCode) {

        if (statusCode.contains("200"))

            return true;

        return false;

    }

}
