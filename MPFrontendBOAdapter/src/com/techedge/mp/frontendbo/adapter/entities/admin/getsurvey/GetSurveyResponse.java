package com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class GetSurveyResponse extends BaseResponse {

    private GetSurveyBodyResponse body;

    public GetSurveyBodyResponse getBody() {
        return body;
    }

    public void setBody(GetSurveyBodyResponse body) {
        this.body = body;
    }

}
