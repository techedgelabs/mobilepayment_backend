package com.techedge.mp.frontendbo.adapter.entities.admin.testcheckvoucher;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherCodeInfo;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestCheckVoucherBodyRequest implements Validable {

    private String        operationID;
    private String        voucherType;
    private String        partnerType;
    private Long          requestTimestamp;
    List<VoucherCodeInfo> voucherList = new ArrayList<VoucherCodeInfo>(0);

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public List<VoucherCodeInfo> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<VoucherCodeInfo> voucherList) {
        this.voucherList = voucherList;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationID == null || this.operationID.trim().isEmpty() || this.voucherType == null || this.voucherType.trim().isEmpty() || this.partnerType == null
                || this.partnerType.trim().isEmpty() || this.requestTimestamp == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_CHECK_VOUCHER_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_CHECK_VOUCHER_SUCCESS);

        return status;
    }

}
