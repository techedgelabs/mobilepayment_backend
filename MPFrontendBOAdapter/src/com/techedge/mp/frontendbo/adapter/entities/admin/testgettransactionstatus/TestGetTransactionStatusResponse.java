package com.techedge.mp.frontendbo.adapter.entities.admin.testgettransactionstatus;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestGetTransactionStatusResponse extends BaseResponse{
    
    private TestGetTransactionStatusResponseBody body;

    public TestGetTransactionStatusResponseBody getBody() {
        return body;
    }

    public void setBody(TestGetTransactionStatusResponseBody body) {
        this.body = body;
    }
}
