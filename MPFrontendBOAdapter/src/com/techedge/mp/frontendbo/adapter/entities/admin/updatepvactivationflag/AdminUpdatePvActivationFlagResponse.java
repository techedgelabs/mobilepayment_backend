package com.techedge.mp.frontendbo.adapter.entities.admin.updatepvactivationflag;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminUpdatePvActivationFlagResponse extends BaseResponse {
    
    private AdminUpdatePvActivationFlagBodyResponse body;

    public AdminUpdatePvActivationFlagBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminUpdatePvActivationFlagBodyResponse body) {
        this.body = body;
    }

}