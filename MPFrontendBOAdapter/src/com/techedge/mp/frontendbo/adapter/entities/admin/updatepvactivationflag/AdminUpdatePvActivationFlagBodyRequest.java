package com.techedge.mp.frontendbo.adapter.entities.admin.updatepvactivationflag;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.StationActivationData;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminUpdatePvActivationFlagBodyRequest implements Validable {

    private List<StationActivationData> stationActivationList = new ArrayList<StationActivationData>(0);

    public List<StationActivationData> getStationActivationList() {
        return stationActivationList;
    }

    public void setStationActivationList(List<StationActivationData> stationActivationList) {
        this.stationActivationList = stationActivationList;
    }

    @Override
    public Status check() {
        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_GENERATE_PV_SUCCESS);

        return status;
    }
}
