package com.techedge.mp.frontendbo.adapter.entities.admin.createadminrole;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminCreateAdminRoleRequest extends AbstractBORequest implements Validable {

    private Status                          status = new Status();

    private Credential                      credential;
    private AdminCreateAdminRoleBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminCreateAdminRoleBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminCreateAdminRoleBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-ADMIN-ROLE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_ADMIN_ROLE_CREATE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_ADMIN_ROLE_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String createAdminRoleResult = getAdminServiceRemote().adminCreateAdminRole(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), this.getBody().getName());

        AdminCreateAdminRoleResponse createAdminRoleResponse = new AdminCreateAdminRoleResponse();
        status.setStatusCode(createAdminRoleResult);
        status.setStatusMessage(prop.getProperty(createAdminRoleResult));
        createAdminRoleResponse.setStatus(status);

        return createAdminRoleResponse;

    }

}