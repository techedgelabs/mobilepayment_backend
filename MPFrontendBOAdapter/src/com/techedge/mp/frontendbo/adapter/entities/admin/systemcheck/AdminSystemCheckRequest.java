package com.techedge.mp.frontendbo.adapter.entities.admin.systemcheck;

import java.util.List;

import com.techedge.mp.core.business.interfaces.AdminRemoteSystemCheckResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminSystemCheckRequest extends AbstractBORequest implements Validable {

    private Credential                  credential;
    private AdminSystemCheckBodyRequest body;

    public AdminSystemCheckRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminSystemCheckBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminSystemCheckBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("SYSTEM-CHECK", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        AdminSystemCheckBodyRequest bodyRequest = this.getBody();
        AdminSystemCheckResponse response = new AdminSystemCheckResponse();
        AdminSystemCheckBodyResponse bodyResponse = new AdminSystemCheckBodyResponse();

        String adminTicketId = this.getCredential().getTicketID();
        String requestId = this.getCredential().getRequestID();

        boolean error = false;
        Status status = new Status();
        status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_FAILURE);

        List<AdminRemoteSystemCheckResult> resultList = getAdminServiceRemote().systemCheck(adminTicketId, requestId, bodyRequest.getSystems());
        bodyResponse.setSystems(resultList);
        
        for (AdminRemoteSystemCheckResult result : resultList) {
            if (result.getStatusCode() != null && !Validator.isValid(result.getStatusCode())) {
                error = true;
                break;
            }
        }
        
        if (!error) {
            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);
        }
        
        response.setStatus(status);
        response.setBody(bodyResponse);

        return response;
    }

}
