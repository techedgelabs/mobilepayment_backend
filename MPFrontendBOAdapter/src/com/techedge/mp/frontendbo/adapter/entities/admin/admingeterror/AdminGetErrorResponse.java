package com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror;

import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminGetErrorResponse extends BaseResponse {
    private List<AdminGetErrorBodyResponse> body;

    public List<AdminGetErrorBodyResponse> getBody() {
        return body;
    }

    public void setBody(List<AdminGetErrorBodyResponse> adminGetErrorBodyResponse) {
        this.body = adminGetErrorBodyResponse;
    }
    
}