package com.techedge.mp.frontendbo.adapter.entities.common;


public class ParameterInfo {
	
	private String param;
	private String value;

    public ParameterInfo(){}

	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
}
