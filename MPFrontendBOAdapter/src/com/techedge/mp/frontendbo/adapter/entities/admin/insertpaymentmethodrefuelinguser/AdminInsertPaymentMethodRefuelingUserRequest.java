package com.techedge.mp.frontendbo.adapter.entities.admin.insertpaymentmethodrefuelinguser;

import com.techedge.mp.core.business.interfaces.PaymentMethodResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminInsertPaymentMethodRefuelingUserRequest extends AbstractBORequest implements Validable {
    private Status                                           status = new Status();

    private Credential                                       credential;
    private AdminInsertPaymentMethodRefuelingUserBodyRequest body;

    public AdminInsertPaymentMethodRefuelingUserRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminInsertPaymentMethodRefuelingUserBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminInsertPaymentMethodRefuelingUserBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADMIN-INSERT_PAY_METH-REFUELING-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_INSERT_PAY_METH_REFUELING_USER_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_INSERT_PAY_METH_REFUELING_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        PaymentMethodResponse result = getAdminServiceRemote().adminInsertPaymentMethodRefuelingUser(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getUsername());

        AdminInsertPaymentMethodRefuelingUserResponse response = new AdminInsertPaymentMethodRefuelingUserResponse();
        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(prop.getProperty(result.getStatusCode()));
        response.setBody(new AdminInsertPaymentMethodRefuelingUserBodyResponse());
        response.setStatus(status);
        response.getBody().setPaymentMethod(result.getPaymentMethod());
        response.getBody().setRedirectUrl(result.getRedirectURL());
        return response;

    }

}
