package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepaymentdatarefuelinguser;

import com.techedge.mp.core.business.interfaces.PaymentMethod;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminRetrievePaymentDataRefuelingUserBodyRequest implements Validable {

    private String        username;
    private PaymentMethod paymentMethod;

    public String getUsername() {
        return username;
    }

    public void setUsername(String role) {
        this.username = role;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public AdminRetrievePaymentDataRefuelingUserBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.username == null || this.username.isEmpty() || paymentMethod == null || (paymentMethod != null && paymentMethod.getId() == null)) {

            status.setStatusCode(StatusCode.ADMIN_RETR_PAY_DATA_REFUELING_USER_ERROR_PARAMETERS);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETR_PAY_DATA_REFUELING_USER_SUCCESS);

        return status;
    }


}
