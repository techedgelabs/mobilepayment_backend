package com.techedge.mp.frontendbo.adapter.entities.admin.testreverseconsumevouchertransaction;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseConsumeVoucherTransactionResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestReverseConsumeVoucherTransactionRequest extends AbstractBORequest implements Validable {

    private Credential                                      credential;
    private TestReverseConsumeVoucherTransactionBodyRequest body;

    public TestReverseConsumeVoucherTransactionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestReverseConsumeVoucherTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(TestReverseConsumeVoucherTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REVERSE-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_REVERSE_CONSUME_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestReverseConsumeVoucherTransactionResponse testReverseConsumeVoucherTransactionResponse = new TestReverseConsumeVoucherTransactionResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testReverseConsumeVoucherTransaction");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testReverseConsumeVoucherTransactionResponse.getStatus().setStatusCode("-1");
        }
        else {
            ReverseConsumeVoucherTransactionResult reverseConsumeVoucherTransactionResult = new ReverseConsumeVoucherTransactionResult();
            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                reverseConsumeVoucherTransactionResult = getFidelityServiceRemote().reverseConsumeVoucherTransaction(this.getBody().getOperationID(),
                        this.getBody().getOperationIDtoReverse(), partnerType, this.getBody().getRequestTimestamp());
            }
            catch (FidelityServiceException ex) {
                reverseConsumeVoucherTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!reverseConsumeVoucherTransactionResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                testReverseConsumeVoucherTransactionResponse.setCsTransactionID(reverseConsumeVoucherTransactionResult.getCsTransactionID());
                testReverseConsumeVoucherTransactionResponse.getStatus().setStatusMessage(reverseConsumeVoucherTransactionResult.getMessageCode());
                testReverseConsumeVoucherTransactionResponse.getStatus().setStatusCode(reverseConsumeVoucherTransactionResult.getStatusCode());
            }
        }

        return testReverseConsumeVoucherTransactionResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
