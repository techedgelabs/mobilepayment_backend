package com.techedge.mp.frontendbo.adapter.entities.admin.updatevoucherdata;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminUpdateVoucherDataBodyRequest implements Validable {

    private Long   id;
    private String status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (this.id == null || this.status == null) {
            status.setStatusCode(StatusCode.ADMIN_DOCUMENT_UPDATE_CHECK_FAILURE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_UPDATE_SUCCESS);

        return status;
    }
}
