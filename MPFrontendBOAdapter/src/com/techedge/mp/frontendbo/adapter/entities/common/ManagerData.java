package com.techedge.mp.frontendbo.adapter.entities.common;

import com.techedge.mp.core.business.interfaces.Manager;

public class ManagerData {

    private String username;
    private String email;
    private String firstName;
    private String lastName;

    public ManagerData() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Manager getManager() {
        Manager manager = new Manager();

        manager.setUsername(this.username);
        manager.setEmail(this.email);
        manager.setFirstName(this.firstName);
        manager.setLastName(this.lastName);
        manager.setStatus(Manager.STATUS_ACTIVE);
        manager.setType(Manager.TYPE_STANDARD);

        return manager;
    }


}
