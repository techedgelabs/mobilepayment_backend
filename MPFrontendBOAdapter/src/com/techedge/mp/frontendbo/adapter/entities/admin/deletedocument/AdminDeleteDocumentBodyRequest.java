package com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminDeleteDocumentBodyRequest implements Validable {

    private String documentKey;

    public String getDocumentKey() {
        return documentKey;
    }

    public void setDocumentKey(String documentKey) {
        this.documentKey = documentKey;
    }

    @Override
    public Status check() {
        Status status = new Status();

        /*
        if (this.documentKey == null || this.documentKey.isEmpty() || this.documentKey.trim().isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_DOCUMENT_DELETE_CHECK_FAILURE);

            return status;
        }
        */

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_DELETE_SUCCESS);

        return status;
    }
}
