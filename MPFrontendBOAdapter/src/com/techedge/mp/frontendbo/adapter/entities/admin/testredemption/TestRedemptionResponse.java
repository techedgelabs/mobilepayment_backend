package com.techedge.mp.frontendbo.adapter.entities.admin.testredemption;

import java.io.Serializable;

import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestRedemptionResponse extends BaseResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 3653913958347659131L;
    private Integer           redemptionCode;
    private VoucherDetail     voucher;
    private Integer           credits;
    private Double            balance;
    private Double            balanceAmount;
    private String            marketingMsg;
    private String            warningMsg;
    private String            csTransactionID;

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String value) {
        this.csTransactionID = value;
    }

    public Integer getRedemptionCode() {
        return redemptionCode;
    }

    public void setRedemptionCode(Integer value) {
        this.redemptionCode = value;
    }

    public VoucherDetail getVoucher() {
        return voucher;
    }

    public void setVoucher(VoucherDetail value) {
        this.voucher = value;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer value) {
        this.credits = value;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double value) {
        this.balance = value;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double value) {
        this.balanceAmount = value;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String value) {
        this.marketingMsg = value;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String value) {
        this.warningMsg = value;
    }

}
