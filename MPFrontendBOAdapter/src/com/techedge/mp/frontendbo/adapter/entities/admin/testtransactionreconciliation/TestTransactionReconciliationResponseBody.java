package com.techedge.mp.frontendbo.adapter.entities.admin.testtransactionreconciliation;

import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionReconciliationResponse;

public class TestTransactionReconciliationResponseBody {

    private TransactionReconciliationResponse transactionReconciliationResponse;

    public TransactionReconciliationResponse getTransactionReconciliationResponse() {
        return transactionReconciliationResponse;
    }

    public void setTransactionReconciliationResponse(TransactionReconciliationResponse transactionReconciliationResponse) {
        this.transactionReconciliationResponse = transactionReconciliationResponse;
    }

}
