package com.techedge.mp.frontendbo.adapter.entities.admin.addroletoadmin;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminAddRoleToAdminRequest extends AbstractBORequest implements Validable {
    private Status                         status = new Status();

    private Credential                     credential;
    private AdminAddRoleToAdminBodyRequest body;

    public AdminAddRoleToAdminRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminAddRoleToAdminBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminAddRoleToAdminBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADD-ROLE-TO-ADMIN", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_ADD_ROLE_TO_ADMIN_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_ADD_ROLE_TO_ADMIN_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String adminAddRoleToAdminResult = getAdminServiceRemote().adminAddRoleToAdmin(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getRole(), this.getBody().getAdmin());

        AdminAddRoleToAdminResponse adminAddRoleToAdminResponse = new AdminAddRoleToAdminResponse();
        status.setStatusCode(adminAddRoleToAdminResult);
        status.setStatusMessage(prop.getProperty(adminAddRoleToAdminResult));
        adminAddRoleToAdminResponse.setStatus(status);

        return adminAddRoleToAdminResponse;

    }

}
