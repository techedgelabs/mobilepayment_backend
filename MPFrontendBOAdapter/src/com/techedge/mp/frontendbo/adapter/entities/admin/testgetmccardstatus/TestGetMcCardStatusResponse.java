package com.techedge.mp.frontendbo.adapter.entities.admin.testgetmccardstatus;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.fidelity.adapter.business.interfaces.DpanDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestGetMcCardStatusResponse extends BaseResponse {

    private String           transactionId;
    private String           code;
    private String           message;
    private String           _status;
    private List<DpanDetail> dpanDetailList = new ArrayList<DpanDetail>(0);

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String get_status() {
        return _status;
    }

    public void set_status(String _status) {
        this._status = _status;
    }

    public List<DpanDetail> getDpanDetailList() {
        return dpanDetailList;
    }

    public void setDpanDetailList(List<DpanDetail> dpanDetailList) {
        this.dpanDetailList = dpanDetailList;
    }

}
