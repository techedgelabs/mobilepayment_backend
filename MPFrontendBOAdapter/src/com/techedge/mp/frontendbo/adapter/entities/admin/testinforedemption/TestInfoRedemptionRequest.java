package com.techedge.mp.frontendbo.adapter.entities.admin.testinforedemption;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.InfoRedemptionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestInfoRedemptionRequest extends AbstractBORequest implements Validable {

    private Credential                    credential;
    private TestInfoRedemptionBodyRequest body;

    public TestInfoRedemptionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestInfoRedemptionBodyRequest getBody() {
        return body;
    }

    public void setBody(TestInfoRedemptionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("INFO-REDEMPTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_INFO_REDEMPTION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestInfoRedemptionResponse testInfoRedemptionResponse = new TestInfoRedemptionResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testInfoRedemption");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testInfoRedemptionResponse.getStatus().setStatusCode("-1");
        }
        else {
            InfoRedemptionResult infoRedemptionResult = new InfoRedemptionResult();
            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                infoRedemptionResult = getFidelityServiceRemote().infoRedemption(this.getBody().getOperationID(), partnerType, this.getBody().getRequestTimestamp(),
                        this.getBody().getFiscalCode());
            }
            catch (FidelityServiceException ex) {
                infoRedemptionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!infoRedemptionResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                infoRedemptionResult.setCsTransactionID(infoRedemptionResult.getCsTransactionID());
                testInfoRedemptionResponse.getStatus().setStatusMessage(infoRedemptionResult.getMessageCode());
                testInfoRedemptionResponse.getStatus().setStatusCode(infoRedemptionResult.getStatusCode());

                for (RedemptionDetail redemptionDetail : infoRedemptionResult.getRedemptionList()) {
                    testInfoRedemptionResponse.getRedemptionList().add(redemptionDetail);
                }
            }
        }

        return testInfoRedemptionResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
