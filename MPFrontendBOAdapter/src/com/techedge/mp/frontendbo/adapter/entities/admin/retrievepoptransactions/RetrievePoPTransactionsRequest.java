package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions;

import java.sql.Timestamp;
import java.util.List;

import com.techedge.mp.core.business.interfaces.PostPaidCart;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.PostPaidRefuel;
import com.techedge.mp.core.business.interfaces.PostPaidTransactionEvent;
import com.techedge.mp.core.business.interfaces.PostPaidTransactionHistory;
import com.techedge.mp.core.business.interfaces.PostPaidTransactionPaymentEvent;
import com.techedge.mp.core.business.interfaces.RetrievePoPTransactionListData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.PoPCartInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.PoPRefuelInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.PoPTransactionConsumeVoucherDetailInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.PoPTransactionConsumeVoucherInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.PoPTransactionEventInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.PoPTransactionInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.PoPTransactionLoadLoyaltyCreditsInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.PoPTransactionPaymentEventInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrievePoPTransactionsRequest extends AbstractBORequest implements Validable {

    private Status                             status = new Status();

    private Credential                         credential;
    private RetrievePoPTransactionsRequestBody body;

    public RetrievePoPTransactionsRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrievePoPTransactionsRequestBody getBody() {
        return body;
    }

    public void setBody(RetrievePoPTransactionsRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-USERS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        RetrievePoPTransactionsResponse retrievePoPTransactionsResponse = new RetrievePoPTransactionsResponse();

        Timestamp start = null;
        Timestamp end = null;

        if (this.getBody().getCreationTimestampStart() != null) {
            start = new Timestamp(this.getBody().getCreationTimestampStart());
        }
        if (this.getBody().getCreationTimestampEnd() != null) {
            end = new Timestamp(this.getBody().getCreationTimestampEnd());
        }

        RetrievePoPTransactionListData retrievePoPTransactionData = getAdminServiceRemote().adminPoPTransactionRetrieve(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), this.getBody().getMpTransactionId(), this.getBody().getUserId(), this.getBody().getStationId(), this.getBody().getSourceId(),
                this.getBody().getMpTransactionStatus(), this.getBody().getNotificationCreated(), this.getBody().getNotificationPaid(), this.getBody().getNotificationUser(),
                start, end, this.getBody().getMpTransactionHistoryFlag());

        List<PostPaidTransactionHistory> postPaidTransactionList = retrievePoPTransactionData.getPostPaidTransactionHistoryList();

        if (!postPaidTransactionList.isEmpty()) {

            for (PostPaidTransactionHistory postPaidTransactionHistory : postPaidTransactionList) {

                PoPTransactionInfo popTransactionInfo = new PoPTransactionInfo();

                popTransactionInfo.setId(postPaidTransactionHistory.getId());
                popTransactionInfo.setAcquirerID(postPaidTransactionHistory.getAcquirerID());
                popTransactionInfo.setAmount(postPaidTransactionHistory.getAmount());
                popTransactionInfo.setArchivingDate(postPaidTransactionHistory.getArchivingDate());
                popTransactionInfo.setAuthorizationCode(postPaidTransactionHistory.getAuthorizationCode());
                popTransactionInfo.setBankTansactionID(postPaidTransactionHistory.getBankTansactionID());
                popTransactionInfo.setCreationTimestamp(postPaidTransactionHistory.getCreationTimestamp());
                popTransactionInfo.setCurrency(postPaidTransactionHistory.getCurrency());
                popTransactionInfo.setLastModifyTimestamp(postPaidTransactionHistory.getLastModifyTimestamp());
                popTransactionInfo.setMpTransactionID(postPaidTransactionHistory.getMpTransactionID());
                popTransactionInfo.setMpTransactionStatus(postPaidTransactionHistory.getMpTransactionStatus());
                popTransactionInfo.setNotificationCreated(postPaidTransactionHistory.getNotificationCreated().toString());
                popTransactionInfo.setNotificationPaid(postPaidTransactionHistory.getNotificationPaid().toString());
                popTransactionInfo.setNotificationUser(postPaidTransactionHistory.getNotificationUser().toString());
                popTransactionInfo.setPaymentMethodId(postPaidTransactionHistory.getPaymentMethodId());
                popTransactionInfo.setPaymentMethodType(postPaidTransactionHistory.getPaymentMethodType());
                popTransactionInfo.setPaymentMode(postPaidTransactionHistory.getPaymentMode());
                popTransactionInfo.setPaymentType(postPaidTransactionHistory.getPaymentType());
                popTransactionInfo.setProductType(postPaidTransactionHistory.getProductType());
                popTransactionInfo.setShopLogin(postPaidTransactionHistory.getShopLogin());
                popTransactionInfo.setSource(postPaidTransactionHistory.getSource());
                popTransactionInfo.setSourceID(postPaidTransactionHistory.getSourceID());
                popTransactionInfo.setSrcTransactionID(postPaidTransactionHistory.getSrcTransactionID());
                popTransactionInfo.setSrcTransactionStatus(postPaidTransactionHistory.getSrcTransactionStatus());
                popTransactionInfo.setToReconcile(postPaidTransactionHistory.getToReconcile().toString());

                if (postPaidTransactionHistory.getStation() != null) {
                    popTransactionInfo.setStationId(postPaidTransactionHistory.getStation().getStationID());
                }
                else {
                    popTransactionInfo.setStationId("");
                }

                popTransactionInfo.setStatusType(postPaidTransactionHistory.getStatusType());
                popTransactionInfo.setStatus(null);
                popTransactionInfo.setSubStatus(null);
                popTransactionInfo.setToken(postPaidTransactionHistory.getToken());

                if (postPaidTransactionHistory.getUser() != null) {
                    popTransactionInfo.setUserId(postPaidTransactionHistory.getUser().getId());
                }
                else {
                    popTransactionInfo.setUserId(null);
                }

                for (PostPaidCart postPaidCart : postPaidTransactionHistory.getCartBean()) {

                    PoPCartInfo popCartInfo = new PoPCartInfo();
                    popCartInfo.setAmount(postPaidCart.getAmount());
                    popCartInfo.setProductDescription(postPaidCart.getProductDescription());
                    popCartInfo.setProductId(postPaidCart.getProductId());
                    popCartInfo.setQuantity(postPaidCart.getQuantity());
                    popTransactionInfo.getCartInfo().add(popCartInfo);
                }

                for (PostPaidRefuel postPaidRefuel : postPaidTransactionHistory.getRefuelBean()) {

                    PoPRefuelInfo popRefuelInfo = new PoPRefuelInfo();
                    popRefuelInfo.setFuelAmount(postPaidRefuel.getFuelAmount());
                    popRefuelInfo.setFuelQuantity(postPaidRefuel.getFuelQuantity());
                    popRefuelInfo.setFuelType(postPaidRefuel.getFuelType());
                    popRefuelInfo.setProductDescription(postPaidRefuel.getProductDescription());
                    popRefuelInfo.setProductId(postPaidRefuel.getProductId());
                    popRefuelInfo.setPumpId(postPaidRefuel.getPumpId());
                    popRefuelInfo.setPumpNumber(postPaidRefuel.getPumpNumber());
                    popRefuelInfo.setRefuelMode(postPaidRefuel.getRefuelMode());
                    popRefuelInfo.setTimestampEndRefuel(postPaidRefuel.getTimestampEndRefuel());
                    popTransactionInfo.getRefuelInfo().add(popRefuelInfo);
                }

                for (PostPaidTransactionEvent postPaidTransactionEvent : postPaidTransactionHistory.getPostPaidTransactionEventBean()) {

                    PoPTransactionEventInfo poPTransactionEventInfo = new PoPTransactionEventInfo();
                    poPTransactionEventInfo.setErrorCode(postPaidTransactionEvent.getErrorCode());
                    poPTransactionEventInfo.setErrorDescription(postPaidTransactionEvent.getErrorDescription());
                    poPTransactionEventInfo.setEventTimestamp(postPaidTransactionEvent.getEventTimestamp());
                    poPTransactionEventInfo.setEvent(postPaidTransactionEvent.getEvent());
                    poPTransactionEventInfo.setNewState(postPaidTransactionEvent.getNewState());
                    poPTransactionEventInfo.setOldState(postPaidTransactionEvent.getOldState());
                    poPTransactionEventInfo.setResult(postPaidTransactionEvent.getResult());
                    poPTransactionEventInfo.setStateType(postPaidTransactionEvent.getStateType());
                    popTransactionInfo.getPostPaidTransactionEventInfo().add(poPTransactionEventInfo);
                }

                for (PostPaidTransactionPaymentEvent postPaidTransactionPaymentEvent : postPaidTransactionHistory.getPostPaidTransactionPaymentEventBean()) {

                    PoPTransactionPaymentEventInfo poPTransactionPaymentEventInfo = new PoPTransactionPaymentEventInfo();
                    poPTransactionPaymentEventInfo.setAuthorizationCode(postPaidTransactionPaymentEvent.getAuthorizationCode());
                    poPTransactionPaymentEventInfo.setErrorCode(postPaidTransactionPaymentEvent.getErrorCode());
                    poPTransactionPaymentEventInfo.setErrorDescription(postPaidTransactionPaymentEvent.getErrorDescription());
                    poPTransactionPaymentEventInfo.setEventType(postPaidTransactionPaymentEvent.getEventType());
                    poPTransactionPaymentEventInfo.setSequence(postPaidTransactionPaymentEvent.getSequence());
                    poPTransactionPaymentEventInfo.setTransactionResult(postPaidTransactionPaymentEvent.getTransactionResult());
                    popTransactionInfo.getPostPaidTransactionPaymentEventInfo().add(poPTransactionPaymentEventInfo);
                }

                for (PostPaidConsumeVoucher postPaidConsumeVoucher : postPaidTransactionHistory.getPostPaidConsumeVoucherBeanList()) {

                    PoPTransactionConsumeVoucherInfo poPTransactionConsumeVoucherInfo = new PoPTransactionConsumeVoucherInfo();
                    poPTransactionConsumeVoucherInfo.setCsTransactionID(postPaidConsumeVoucher.getCsTransactionID());
                    poPTransactionConsumeVoucherInfo.setMarketingMsg(postPaidConsumeVoucher.getMarketingMsg());
                    poPTransactionConsumeVoucherInfo.setMessageCode(postPaidConsumeVoucher.getMessageCode());
                    poPTransactionConsumeVoucherInfo.setOperationID(postPaidConsumeVoucher.getOperationID());
                    poPTransactionConsumeVoucherInfo.setOperationType(postPaidConsumeVoucher.getOperationType());
                    poPTransactionConsumeVoucherInfo.setRequestTimestamp(postPaidConsumeVoucher.getRequestTimestamp());
                    poPTransactionConsumeVoucherInfo.setStatusCode(postPaidConsumeVoucher.getStatusCode());
                    poPTransactionConsumeVoucherInfo.setTotalConsumed(postPaidConsumeVoucher.getTotalConsumed());

                    for (PostPaidConsumeVoucherDetail PostPaidConsumeVoucherDetail : postPaidConsumeVoucher.getPostPaidConsumeVoucherDetail()) {

                        PoPTransactionConsumeVoucherDetailInfo poPTransactionConsumeVoucherDetailInfo = new PoPTransactionConsumeVoucherDetailInfo();
                        poPTransactionConsumeVoucherDetailInfo.setConsumedValue(PostPaidConsumeVoucherDetail.getConsumedValue());
                        poPTransactionConsumeVoucherDetailInfo.setExpirationDate(PostPaidConsumeVoucherDetail.getExpirationDate());
                        poPTransactionConsumeVoucherDetailInfo.setInitialValue(PostPaidConsumeVoucherDetail.getInitialValue());
                        poPTransactionConsumeVoucherDetailInfo.setPromoCode(PostPaidConsumeVoucherDetail.getPromoCode());
                        poPTransactionConsumeVoucherDetailInfo.setPromoDescription(PostPaidConsumeVoucherDetail.getPromoDescription());
                        poPTransactionConsumeVoucherDetailInfo.setPromoDoc(PostPaidConsumeVoucherDetail.getPromoDoc());
                        poPTransactionConsumeVoucherDetailInfo.setVoucherBalanceDue(PostPaidConsumeVoucherDetail.getVoucherBalanceDue());
                        poPTransactionConsumeVoucherDetailInfo.setVoucherCode(PostPaidConsumeVoucherDetail.getVoucherCode());
                        poPTransactionConsumeVoucherDetailInfo.setVoucherStatus(PostPaidConsumeVoucherDetail.getVoucherStatus());
                        poPTransactionConsumeVoucherDetailInfo.setVoucherType(PostPaidConsumeVoucherDetail.getVoucherType());
                        poPTransactionConsumeVoucherDetailInfo.setVoucherValue(PostPaidConsumeVoucherDetail.getVoucherValue());

                        poPTransactionConsumeVoucherInfo.getPostPaidConsumeVoucherList().add(poPTransactionConsumeVoucherDetailInfo);
                    }

                    popTransactionInfo.getPostPaidTransactionConsumeVoucherInfo().add(poPTransactionConsumeVoucherInfo);
                }

                for (PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits : postPaidTransactionHistory.getPostPaidLoadLoyaltyCreditsBeanList()) {

                    PoPTransactionLoadLoyaltyCreditsInfo poPTransactionLoadLoyaltyCreditsInfo = new PoPTransactionLoadLoyaltyCreditsInfo();
                    poPTransactionLoadLoyaltyCreditsInfo.setBalance(postPaidLoadLoyaltyCredits.getBalance());
                    poPTransactionLoadLoyaltyCreditsInfo.setBalanceAmount(postPaidLoadLoyaltyCredits.getBalanceAmount());
                    poPTransactionLoadLoyaltyCreditsInfo.setCardClassification(postPaidLoadLoyaltyCredits.getCardClassification());
                    poPTransactionLoadLoyaltyCreditsInfo.setCardCodeIssuer(postPaidLoadLoyaltyCredits.getCardCodeIssuer());
                    poPTransactionLoadLoyaltyCreditsInfo.setCardStatus(postPaidLoadLoyaltyCredits.getCardStatus());
                    poPTransactionLoadLoyaltyCreditsInfo.setCardType(postPaidLoadLoyaltyCredits.getCardType());
                    poPTransactionLoadLoyaltyCreditsInfo.setCredits(postPaidLoadLoyaltyCredits.getCredits());
                    poPTransactionLoadLoyaltyCreditsInfo.setCsTransactionID(postPaidLoadLoyaltyCredits.getCsTransactionID());
                    poPTransactionLoadLoyaltyCreditsInfo.setEanCode(postPaidLoadLoyaltyCredits.getEanCode());
                    poPTransactionLoadLoyaltyCreditsInfo.setMarketingMsg(postPaidLoadLoyaltyCredits.getMarketingMsg());
                    poPTransactionLoadLoyaltyCreditsInfo.setMessageCode(postPaidLoadLoyaltyCredits.getMessageCode());
                    poPTransactionLoadLoyaltyCreditsInfo.setOperationID(postPaidLoadLoyaltyCredits.getOperationID());
                    poPTransactionLoadLoyaltyCreditsInfo.setOperationType(postPaidLoadLoyaltyCredits.getOperationType());
                    poPTransactionLoadLoyaltyCreditsInfo.setRequestTimestamp(postPaidLoadLoyaltyCredits.getRequestTimestamp());
                    poPTransactionLoadLoyaltyCreditsInfo.setStatusCode(postPaidLoadLoyaltyCredits.getStatusCode());

                    popTransactionInfo.getPostPaidTransactionLoadLoyaltyCreditsInfo().add(poPTransactionLoadLoyaltyCreditsInfo);
                }

                retrievePoPTransactionsResponse.getPopTransactions().add(popTransactionInfo);
            }
        }

        status.setStatusCode("ADMIN_RETRIEVE_POP_TRANSACTION_200");
        status.setStatusMessage("OK");

        retrievePoPTransactionsResponse.setStatus(status);

        return retrievePoPTransactionsResponse;
    }

}
