package com.techedge.mp.frontendbo.adapter.entities.admin.systemcheck;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.AdminRemoteSystemCheckResult;


public class AdminSystemCheckBodyResponse {
	
    List<AdminRemoteSystemCheckResult> systems = new ArrayList<AdminRemoteSystemCheckResult>();
    
    public List<AdminRemoteSystemCheckResult> getSystems() {
        return systems;
    }
    
    public void setSystems(List<AdminRemoteSystemCheckResult> systems) {
        this.systems = systems;
    }
}
