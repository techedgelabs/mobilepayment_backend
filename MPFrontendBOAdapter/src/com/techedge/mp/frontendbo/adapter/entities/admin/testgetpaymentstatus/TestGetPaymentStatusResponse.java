package com.techedge.mp.frontendbo.adapter.entities.admin.testgetpaymentstatus;

import com.techedge.mp.fidelity.adapter.business.interfaces.GetPaymentStatusResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestGetPaymentStatusResponse extends BaseResponse {

    private String               statusCode;
    private GetPaymentStatusResult result;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public GetPaymentStatusResult getResult() {
        return result;
    }

    public void setResult(GetPaymentStatusResult result) {
        this.result = result;
    }

}
