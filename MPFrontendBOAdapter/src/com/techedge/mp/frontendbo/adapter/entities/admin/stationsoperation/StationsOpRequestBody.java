package com.techedge.mp.frontendbo.adapter.entities.admin.stationsoperation;

import java.util.List;

import com.techedge.mp.core.business.interfaces.StationAdmin;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class StationsOpRequestBody implements Validable {
	
	
	private List<StationAdmin> stationAdminList;
	
	
	public StationsOpRequestBody() {}

	public List<StationAdmin> getStation() {
		return stationAdminList;
	}

	public void setStation(List<StationAdmin> station) {
		this.stationAdminList = station;
	}




	@Override
	public Status check() {
		
		
		Status status = new Status();
		if (this.stationAdminList != null){
			status.setStatusCode(StatusCode.ADMIN_STATION_SUCCESS);
		}else{
			status.setStatusCode(StatusCode.ADMIN_STATION_PARAMETERS);
		}
		return status;
		
		
	}
	
}
