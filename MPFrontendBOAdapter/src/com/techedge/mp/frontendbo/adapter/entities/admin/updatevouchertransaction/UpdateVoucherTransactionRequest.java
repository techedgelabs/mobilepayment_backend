package com.techedge.mp.frontendbo.adapter.entities.admin.updatevouchertransaction;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class UpdateVoucherTransactionRequest extends AbstractBORequest implements Validable {

    private Status                              status = new Status();

    private Credential                          credential;
    private UpdateVoucherTransactionRequestBody body;

    public UpdateVoucherTransactionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateVoucherTransactionRequestBody getBody() {
        return body;
    }

    public void setBody(UpdateVoucherTransactionRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-TRANSACTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String response = getAdminServiceRemote().adminUpdateVoucherTransaction(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getVoucherTransactionId(), this.getBody().getFinalStatusType());

        UpdateVoucherTransactionResponse updateVoucherTransactionResponse = new UpdateVoucherTransactionResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updateVoucherTransactionResponse.setStatus(status);

        return updateVoucherTransactionResponse;
    }

}
