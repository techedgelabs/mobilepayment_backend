package com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreauthorizationconsumevoucher;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CancelPreAuthorizationConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestCancelPreAuthorizationConsumeVoucherRequest extends AbstractBORequest implements Validable {

    private Credential                                          credential;
    private TestCancelPreAuthorizationConsumeVoucherRequestBody body;

    public TestCancelPreAuthorizationConsumeVoucherRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestCancelPreAuthorizationConsumeVoucherRequestBody getBody() {
        return body;
    }

    public void setBody(TestCancelPreAuthorizationConsumeVoucherRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CANCEL-PRE-AUTHORIZATION-CONSUME-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestCancelPreAuthorizationConsumeVoucherResponse testCancelPreAuthorizationConsumeVoucherResponse = new TestCancelPreAuthorizationConsumeVoucherResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testCancelPreAuthorizationConsumeVoucher");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testCancelPreAuthorizationConsumeVoucherResponse.getStatus().setStatusCode("-1");
        }
        else {

            CancelPreAuthorizationConsumeVoucherResult cancelPreAuthorizationConsumeVoucherResult = new CancelPreAuthorizationConsumeVoucherResult();

            String operationID = this.getBody().getOperationID();
            PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());
            Long requestTimestamp = this.getBody().getRequestTimestamp();
            String preAuthOperationIDToCancel = this.getBody().getPreAuthOperationIDToCancel();

            try {
                cancelPreAuthorizationConsumeVoucherResult = getFidelityServiceRemote().cancelPreAuthorizationConsumeVoucher(operationID, preAuthOperationIDToCancel, partnerType,
                        requestTimestamp);
            }
            catch (FidelityServiceException ex) {
                testCancelPreAuthorizationConsumeVoucherResponse.getStatus().setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!cancelPreAuthorizationConsumeVoucherResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                testCancelPreAuthorizationConsumeVoucherResponse.setCsTransactionID(cancelPreAuthorizationConsumeVoucherResult.getCsTransactionID());
                testCancelPreAuthorizationConsumeVoucherResponse.getStatus().setStatusMessage(cancelPreAuthorizationConsumeVoucherResult.getMessageCode());
                testCancelPreAuthorizationConsumeVoucherResponse.getStatus().setStatusCode(cancelPreAuthorizationConsumeVoucherResult.getStatusCode());

            }
        }

        return testCancelPreAuthorizationConsumeVoucherResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
