package com.techedge.mp.frontendbo.adapter.entities.admin.countpendingtransactions;

public class CountPendingTransactionsResponseBody {

    private Integer count;

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

}
