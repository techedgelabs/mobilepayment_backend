package com.techedge.mp.frontendbo.adapter.entities.admin.updateuser;

import java.util.Date;

import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdateUserRequestBody implements Validable {

    private Long    id;
    private String  email;
    private String  firstName;
    private String  lastName;
    private Date    birthDate;
    private String  birthMunicipality;
    private String  birthProvince;
    private String  sex;
    private String  fiscalCode;
    private String  language;
    private Integer status;
    private String  registrationCompleted;
    private Double  capAvailable;
    private Double  capEffective;
    private String  externalUserId;
    private Integer userType;
    private String  virtualizationCompleted;
    private Integer virtualizationAttemptsLeft;
    private String  eniStationUserType;
    private String  depositCardStepCompleted;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getRegistrationCompleted() {
        return registrationCompleted;
    }

    public void setRegistrationCompleted(String registrationCompleted) {
        this.registrationCompleted = registrationCompleted;
    }

    public Double getCapAvailable() {
        return capAvailable;
    }

    public void setCapAvailable(Double capAvailable) {
        this.capAvailable = capAvailable;
    }

    public Double getCapEffective() {
        return capEffective;
    }

    public void setCapEffective(Double capEffective) {
        this.capEffective = capEffective;
    }

    public String getExternalUserId() {
        return externalUserId;
    }

    public void setExternalUserId(String externalUserId) {
        this.externalUserId = externalUserId;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthMunicipality() {
        return birthMunicipality;
    }

    public void setBirthMunicipality(String birthMunicipality) {
        this.birthMunicipality = birthMunicipality;
    }

    public String getBirthProvince() {
        return birthProvince;
    }

    public void setBirthProvince(String birthProvince) {
        this.birthProvince = birthProvince;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.id == null) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        if (this.language != null && this.language.length() > 10) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        if (this.status != null
                && (this.status != User.USER_STATUS_BLOCKED && this.status != User.USER_STATUS_NEW && this.status != User.USER_STATUS_TEMPORARY_PASSWORD && this.status != User.USER_STATUS_VERIFIED && this.status != User.USER_STATUS_CANCELLED)) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        if (this.registrationCompleted != null && (!this.registrationCompleted.equals("false") && !this.registrationCompleted.equals("true"))) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        if (this.capAvailable != null && (this.capAvailable < 0.0 || this.capAvailable > 1000.0)) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        if (this.capEffective != null && (this.capEffective < 0.0 || this.capEffective > 1000.0)) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        if (this.externalUserId != null && this.externalUserId.length() > 50) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        if (this.userType != null
                && (this.userType != User.USER_TYPE_CUSTOMER && this.userType != User.USER_TYPE_SERVICE && this.userType != User.USER_TYPE_TESTER
                        && this.userType != User.USER_TYPE_REFUELING && this.userType != User.USER_TYPE_REFUELING_OAUTH2 && this.userType != User.USER_TYPE_VOUCHER_TESTER 
                        && this.userType != User.USER_TYPE_NEW_ACQUIRER_TESTER && this.userType != User.USER_TYPE_REFUELING_NEW_ACQUIRER && this.userType != User.USER_TYPE_BUSINESS)) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }
        
        if (this.virtualizationCompleted != null && (!this.virtualizationCompleted.equals("false") && !this.virtualizationCompleted.equals("true"))) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }
        
        if (this.virtualizationAttemptsLeft != null && this.virtualizationAttemptsLeft < 0) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }
        
        if (this.eniStationUserType != null && (!this.eniStationUserType.equals("false") && !this.eniStationUserType.equals("true"))) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }
        
        if (this.depositCardStepCompleted != null && (!this.depositCardStepCompleted.equals("false") && !this.depositCardStepCompleted.equals("true"))) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_SUCCESS);

        return status;
    }

    public String getVirtualizationCompleted() {
        return virtualizationCompleted;
    }

    public void setVirtualizationCompleted(String virtualizationCompleted) {
        this.virtualizationCompleted = virtualizationCompleted;
    }

    public Integer getVirtualizationAttemptsLeft() {
        return virtualizationAttemptsLeft;
    }

    public void setVirtualizationAttemptsLeft(Integer virtualizationAttemptsLeft) {
        this.virtualizationAttemptsLeft = virtualizationAttemptsLeft;
    }

    public String getEniStationUserType() {
        return eniStationUserType;
    }

    public void setEniStationUserType(String eniStationUserType) {
        this.eniStationUserType = eniStationUserType;
    }

    public String getDepositCardStepCompleted() {
        return depositCardStepCompleted;
    }

    public void setDepositCardStepCompleted(String depositCardStepCompleted) {
        this.depositCardStepCompleted = depositCardStepCompleted;
    }

}
