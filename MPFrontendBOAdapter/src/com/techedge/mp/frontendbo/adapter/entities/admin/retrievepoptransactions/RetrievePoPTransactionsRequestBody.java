package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions;

import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RetrievePoPTransactionsRequestBody implements Validable {
	
	private String mpTransactionId;		
	private String userId;
	private String stationId;
	private String sourceId;
	private String mpTransactionStatus;
	private Boolean notificationCreated;
	private Boolean notificationPaid;
	private Boolean notificationUser;
	private Long creationTimestampStart;
	private Long creationTimestampEnd;
	private String source;
	private Boolean mpTransactionHistoryFlag;
	
	
	public String getMpTransactionId() {
		return mpTransactionId;
	}
	public void setMpTransactionId(String mpTransactionId) {
		this.mpTransactionId = mpTransactionId;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStationId() {
		return stationId;
	}
	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getSourceId() {
		return sourceId;
	}
	public void setSourceId(String sourceId) {
		this.sourceId = sourceId;
	}

	public String getMpTransactionStatus() {
		return mpTransactionStatus;
	}
	public void setMpTransactionStatus(String mpTransactionStatus) {
		this.mpTransactionStatus = mpTransactionStatus;
	}

	public Boolean getNotificationCreated() {
		return notificationCreated;
	}
	public void setNotificationCreated(Boolean notificationCreated) {
		this.notificationCreated = notificationCreated;
	}

	public Boolean getNotificationPaid() {
		return notificationPaid;
	}
	public void setNotificationPaid(Boolean notificationPaid) {
		this.notificationPaid = notificationPaid;
	}

	public Boolean getNotificationUser() {
		return notificationUser;
	}
	public void setNotificationUser(Boolean notificationUser) {
		this.notificationUser = notificationUser;
	}

	public Long getCreationTimestampStart() {
		return creationTimestampStart;
	}
	public void setCreationTimestampStart(Long creationTimestampStart) {
		this.creationTimestampStart = creationTimestampStart;
	}

	public Long getCreationTimestampEnd() {
		return creationTimestampEnd;
	}
	public void setCreationTimestampEnd(Long creationTimestampEnd) {
		this.creationTimestampEnd = creationTimestampEnd;
	}

	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}

	public Boolean getMpTransactionHistoryFlag() {
		return mpTransactionHistoryFlag;
	}
	public void setMpTransactionHistoryFlag(Boolean mpTransactionHistoryFlag) {
		this.mpTransactionHistoryFlag = mpTransactionHistoryFlag;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.mpTransactionId != null && this.mpTransactionId.length() > 40) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.userId != null && this.userId.length() > 10) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.stationId != null && this.stationId.length() > 10) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.sourceId != null && this.sourceId.length() > 10) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS);
			return status;
		}
		
		if( this.mpTransactionStatus != null &&
			( !this.mpTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID) &&
			  !this.mpTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD) &&
			  !this.mpTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID) &&
			  !this.mpTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED) &&
			  !this.mpTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED) &&
              !this.mpTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_NOT_RECONCILIABLE) ) ) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.source != null && this.source.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS);
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_SUCCESS);

		return status;
	}
	
}
