package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveadminrole;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminRetrieveAdminRoleResponse extends BaseResponse {

    private AdminRetrieveAdminRoleBodyResponse body;

    public AdminRetrieveAdminRoleBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminRetrieveAdminRoleBodyResponse body) {
        this.body = body;
    }

}