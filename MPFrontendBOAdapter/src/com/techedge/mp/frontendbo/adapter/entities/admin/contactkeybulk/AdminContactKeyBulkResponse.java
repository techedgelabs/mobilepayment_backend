package com.techedge.mp.frontendbo.adapter.entities.admin.contactkeybulk;

import java.util.Map;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminContactKeyBulkResponse extends BaseResponse {

	private Map<String, String> errorList;

	public AdminContactKeyBulkResponse() {
	}

	public AdminContactKeyBulkResponse(Map<String, String> errorList) {
		this.errorList = errorList;
	}

	public Map<String, String> getErrorList() {
		return errorList;
	}

	public void setErrorList(Map<String, String> errorList) {
		this.errorList = errorList;
	}

}
