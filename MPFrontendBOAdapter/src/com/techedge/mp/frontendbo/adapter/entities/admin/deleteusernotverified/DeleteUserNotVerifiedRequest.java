package com.techedge.mp.frontendbo.adapter.entities.admin.deleteusernotverified;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.techedge.mp.core.business.interfaces.AdminUserNotVerifiedDeleteResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class DeleteUserNotVerifiedRequest extends AbstractBORequest implements Validable {

    private Status                status = new Status();

    private Credential            credential;
    private DeleteUserNotVerifiedRequestBody body;

    public DeleteUserNotVerifiedRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public DeleteUserNotVerifiedRequestBody getBody() {
        return body;
    }

    public void setBody(DeleteUserNotVerifiedRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        
        DeleteUserNotVerifiedResponse deleteUserNotVerifiedResponse = new DeleteUserNotVerifiedResponse();
        
        AdminUserNotVerifiedDeleteResult adminUserNotVerifiedDeleteResult = null;

        try {
            
            Date startDate = null,
                 endDate = null;
            
            if (this.getBody().getCreationDateStart() != null) {
                startDate = sdf.parse(this.getBody().getCreationDateStart());
            }
            
            if (this.getBody().getCreationDateStart() != null) {
                endDate = sdf.parse(this.getBody().getCreationDateEnd());
            }

            adminUserNotVerifiedDeleteResult = getAdminServiceRemote().adminUserNotVerifiedDelete(
                    this.getCredential().getTicketID(),
                    this.getCredential().getRequestID(),
                    this.getBody().getUserId(),
                    startDate,
                    endDate);
        }
        catch (ParseException e) {

            status = new Status();
            status.setStatusCode(StatusCode.ADMIN_DELETE_USER_DATE_WRONG);
            status.setStatusMessage(prop.getProperty(status.getStatusCode()));
            deleteUserNotVerifiedResponse.setStatus(status);
            return deleteUserNotVerifiedResponse;
        }

        deleteUserNotVerifiedResponse.setResults(adminUserNotVerifiedDeleteResult.getResults());
        
        status.setStatusCode(adminUserNotVerifiedDeleteResult.getStatusCode());
        status.setStatusMessage(prop.getProperty(adminUserNotVerifiedDeleteResult.getStatusCode()));

        deleteUserNotVerifiedResponse.setStatus(status);

        return deleteUserNotVerifiedResponse;
    }

}
