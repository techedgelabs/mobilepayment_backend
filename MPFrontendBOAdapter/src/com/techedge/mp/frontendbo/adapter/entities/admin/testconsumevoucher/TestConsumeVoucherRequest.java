package com.techedge.mp.frontendbo.adapter.entities.admin.testconsumevoucher;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.ConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.ProductInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherCodeInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherInfo;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestConsumeVoucherRequest extends AbstractBORequest implements Validable {

    private Credential                    credential;
    private TestConsumeVoucherBodyRequest body;

    public TestConsumeVoucherRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestConsumeVoucherBodyRequest getBody() {
        return body;
    }

    public void setBody(TestConsumeVoucherBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CONSUME-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_CONSUME_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestConsumeVoucherResponse testConsumeVoucherResponse = new TestConsumeVoucherResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testConsumeVoucher");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testConsumeVoucherResponse.getStatus().setStatusCode("-1");
        }
        else {

            List<ProductDetail> productList = new ArrayList<ProductDetail>();

            for (ProductInfo productInfo : this.getBody().getProductList()) {

                ProductDetail productDetail = new ProductDetail();

                productDetail.setAmount(productInfo.getAmount());
                productDetail.setProductCode(productInfo.getProductCode());
                productDetail.setQuantity(productInfo.getQuantity());

                productList.add(productDetail);
            }

            List<VoucherCodeDetail> voucherList = new ArrayList<VoucherCodeDetail>();

            for (VoucherCodeInfo voucherCodeInfo : this.getBody().getVoucherList()) {

                VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();

                voucherCodeDetail.setVoucherCode(voucherCodeInfo.getVoucherCode());

                voucherList.add(voucherCodeDetail);
            }

            ConsumeVoucherResult consumeVoucherResult = new ConsumeVoucherResult();
            String operationID = this.getBody().getOperationID();
            String mpTransactionID = this.getBody().getMpTransactionID();
            VoucherConsumerType voucherCosumerType = getEnumerationValue(VoucherConsumerType.class, this.getBody().getVoucherType());
            String stationID = this.getBody().getStationID();
            String refuelMode = this.getBody().getRefuelMode();
            String paymentMode = this.getBody().getPaymentMode();
            PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());
            Long requestTimestamp = this.getBody().getRequestTimestamp();
            String consumeType = this.getBody().getConsumeType();
            String preAuthOperationID = this.getBody().getPreAuthOperationID();

            try {
                consumeVoucherResult = getFidelityServiceRemote().consumeVoucher(operationID, mpTransactionID, voucherCosumerType, stationID, refuelMode, paymentMode, "it",
                        partnerType, requestTimestamp, productList, voucherList, consumeType, preAuthOperationID);
            }
            catch (FidelityServiceException ex) {
                consumeVoucherResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!consumeVoucherResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {

                testConsumeVoucherResponse.setCsTransactionID(consumeVoucherResult.getCsTransactionID());
                testConsumeVoucherResponse.getStatus().setStatusMessage(consumeVoucherResult.getMessageCode());
                testConsumeVoucherResponse.getStatus().setStatusCode(consumeVoucherResult.getStatusCode());
                testConsumeVoucherResponse.setMarketingMsg(consumeVoucherResult.getMarketingMsg());

                for (VoucherDetail voucherDetail : consumeVoucherResult.getVoucherList()) {

                    VoucherInfo voucherInfo = new VoucherInfo();

                    voucherInfo.setConsumedValue(voucherDetail.getConsumedValue());
                    voucherInfo.setExpirationDate(voucherDetail.getExpirationDate());
                    voucherInfo.setInitialValue(voucherDetail.getInitialValue());
                    voucherInfo.setPromoCode(voucherDetail.getPromoCode());
                    voucherInfo.setPromoDescription(voucherDetail.getPromoDescription());
                    voucherInfo.setPromoDoc(voucherDetail.getPromoDoc());
                    voucherInfo.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                    voucherInfo.setVoucherCode(voucherDetail.getVoucherCode());
                    voucherInfo.setVoucherStatus(voucherDetail.getVoucherStatus());
                    voucherInfo.setVoucherType(voucherDetail.getVoucherType());
                    voucherInfo.setVoucherValue(voucherDetail.getVoucherValue());

                    testConsumeVoucherResponse.getVoucherList().add(voucherInfo);
                }
            }
        }

        return testConsumeVoucherResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
