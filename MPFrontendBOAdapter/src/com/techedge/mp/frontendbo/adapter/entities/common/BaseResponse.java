package com.techedge.mp.frontendbo.adapter.entities.common;

public class BaseResponse {
	
	private Status status = new Status();
	
	
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	
	
}
