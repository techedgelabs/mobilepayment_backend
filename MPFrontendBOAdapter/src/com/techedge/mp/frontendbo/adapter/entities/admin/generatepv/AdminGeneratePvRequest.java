package com.techedge.mp.frontendbo.adapter.entities.admin.generatepv;

import com.techedge.mp.core.business.interfaces.GeneratePvResponse;
import com.techedge.mp.core.business.interfaces.PvGenerationData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.PvGenerationInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminGeneratePvRequest extends AbstractBORequest implements Validable {

    private Status                     status = new Status();

    private Credential                 credential;

    private AdminGeneratePvBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminGeneratePvBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminGeneratePvBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GENERATE-PV", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_GENERATE_PV_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_GENERATE_PV_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        GeneratePvResponse generatePvResult = getAdminServiceRemote().adminGeneratePv(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getFinalStatus(), this.getBody().getSkipCheck(), this.getBody().getNoOilAcquirerId(), this.getBody().getNoOilShopLogin(),
                this.getBody().getOilAcquirerId(), this.getBody().getOilShopLogin(), this.getBody().getStationList());

        AdminGeneratePvResponse generatePvResponse = new AdminGeneratePvResponse();

        status.setStatusCode(generatePvResult.getStatusCode());
        status.setStatusMessage(prop.getProperty(generatePvResult.getStatusCode()));
        generatePvResponse.setStatus(status);

        AdminGeneratePvBodyResponse generatePvBodyResponse = new AdminGeneratePvBodyResponse();
        generatePvResponse.setBody(generatePvBodyResponse);

        for (PvGenerationData pvGenerationData : generatePvResult.getPvGenerationDataList()) {

            PvGenerationInfo pvGenerationInfo = new PvGenerationInfo();
            pvGenerationInfo.setStationId(pvGenerationData.getStationId());
            pvGenerationInfo.setStatusCode(pvGenerationData.getStatusCode());
            generatePvResponse.getBody().getPvGenerationResultList().add(pvGenerationInfo);
        }

        return generatePvResponse;
    }
}
