package com.techedge.mp.frontendbo.adapter.entities.admin.admineditmappingerror;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminEditErrorBodyRequest implements Validable {

    private String errorCode;
    private String statusCode;


    public String getGpErrorCode() {
        return errorCode;
    }

    public void setGpErrorCode(String gpErrorCode) {
        this.errorCode = gpErrorCode;
    }

    public String getMpStatusCode() {
        return statusCode;
    }

    public void setMpStatusCode(String mpStatusCode) {
        this.statusCode = mpStatusCode;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (this.errorCode == null) {
            status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_EDIT__INVALID_ERROR_CODE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_EDIT__SUCCESS);

        return status;
    }

}
