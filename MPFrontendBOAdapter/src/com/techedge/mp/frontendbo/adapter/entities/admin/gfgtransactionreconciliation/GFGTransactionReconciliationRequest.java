package com.techedge.mp.frontendbo.adapter.entities.admin.gfgtransactionreconciliation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionReconciliationResponse;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetSrcTransactionStatusMessageResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.TransactionType;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class GFGTransactionReconciliationRequest extends AbstractBORequest implements Validable {

    private Credential                              credential;
    private GFGTransactionReconciliationBodyRequest body;

    public GFGTransactionReconciliationRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GFGTransactionReconciliationBodyRequest getBody() {
        return body;
    }

    public void setBody(GFGTransactionReconciliationBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GFG-TRANSACTION-RECONCILIATION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_GFG_TRANSACTION_RECONCILIATION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        GFGTransactionReconciliationResponse gfgTransactionReconciliationResponse = new GFGTransactionReconciliationResponse();
        Status gfgTransactionReconciliationStatus = new Status();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "gfgTransactionReconciliation");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {
            gfgTransactionReconciliationStatus.setStatusCode("-1");
            gfgTransactionReconciliationResponse.setStatus(gfgTransactionReconciliationStatus);
        }
        else {

            String transactionType = this.getBody().getTransactionType();
            List<String> transactionIdList = new ArrayList<String>();
            String mpTransactionID = this.getBody().getMpTransactionId();
            String srcTransactionID = this.getBody().getSrcTransactionId();
            transactionIdList.add(mpTransactionID);
            String requestID = String.valueOf(new Date().getTime());

            if (transactionType.equals(TransactionType.PRE_PAID.getCode())) {
                System.out.println("chiamata a transactionReconciliation del Forecourt");
                TransactionReconciliationResponse forecourtResponse = getForecourtInfoServiceRemote().transactionReconciliation(requestID, transactionIdList);

                if (forecourtResponse == null || forecourtResponse.getTransactionDetails().isEmpty()) {
                    System.out.println("Errore nella operazione di riconciliazione - risposta Forecourt nulla o vuota (transazione: " + mpTransactionID + ")");

                    gfgTransactionReconciliationStatus.setStatusCode(StatusCode.ADMIN_GFG_TRANSACTION_RECONCILIATION_SERVICE_FAILURE);
                    gfgTransactionReconciliationStatus.setStatusMessage("Errore nella operazione di riconciliazione - risposta Forecourt nulla o vuota " + "(transazione: "
                            + mpTransactionID + ")");
                    gfgTransactionReconciliationResponse.setStatus(gfgTransactionReconciliationStatus);
                }

                TransactionDetail transactionDetail = forecourtResponse.getTransactionDetails().get(0);
                gfgTransactionReconciliationStatus.setStatusCode(StatusCode.ADMIN_GFG_TRANSACTION_RECONCILIATION_SUCCESS);
                gfgTransactionReconciliationResponse.setStatus(gfgTransactionReconciliationStatus);
                gfgTransactionReconciliationResponse.setTransactionDetail(transactionDetail);
            }
            else if (transactionType.equals(TransactionType.POST_PAID.getCode())) {
                System.out.println("chiamata a getSrcTransactionStatus del ForecourtPostPaid");
                GetSrcTransactionStatusMessageResponse forecourtResponse = getForecourtPostPaidServiceRemote().getTransactionStatus(requestID, mpTransactionID, srcTransactionID);

                if (forecourtResponse == null || forecourtResponse.getSrcTransactionDetail() == null
                        || forecourtResponse.getStatusCode().equals(StatusHelper.POST_PAID_TRANSACTION_STATUS_NOT_RECOGNIZED)) {
                    System.out.println("Errore nella operazione di riconciliazione - " + "risposta Forecourt nulla o non disponibile (transazione: " + mpTransactionID + ")");

                    gfgTransactionReconciliationStatus.setStatusCode(StatusCode.ADMIN_GFG_TRANSACTION_RECONCILIATION_SERVICE_FAILURE);
                    gfgTransactionReconciliationStatus.setStatusMessage("Errore nella operazione di riconciliazione - "
                            + "risposta Forecourt nulla o non disponibile (transazione: " + mpTransactionID + ")");
                    gfgTransactionReconciliationResponse.setStatus(gfgTransactionReconciliationStatus);
                }

                gfgTransactionReconciliationStatus.setStatusCode(StatusCode.ADMIN_GFG_TRANSACTION_RECONCILIATION_SUCCESS);
                gfgTransactionReconciliationResponse.setStatus(gfgTransactionReconciliationStatus);
                gfgTransactionReconciliationResponse.setPopTransactionDetail(forecourtResponse.getSrcTransactionDetail());
            }
            else {
                gfgTransactionReconciliationStatus.setStatusCode(StatusCode.ADMIN_GFG_TRANSACTION_RECONCILIATION_INVALID_TRANSACTION_TYPE);
                gfgTransactionReconciliationStatus.setStatusMessage("Parametro transactionType errato. Valori accettati: PRE-PAID/POST-PAID");
                gfgTransactionReconciliationResponse.setStatus(gfgTransactionReconciliationStatus);
            }
        }

        return gfgTransactionReconciliationResponse;
    }

}
