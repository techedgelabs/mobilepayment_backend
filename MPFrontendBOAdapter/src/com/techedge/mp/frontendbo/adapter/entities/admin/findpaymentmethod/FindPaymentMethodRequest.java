package com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod;

import java.sql.Timestamp;

import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PaymentInfoResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class FindPaymentMethodRequest extends AbstractBORequest implements Validable {

    private Status                       status = new Status();

    private Credential                   credential;
    private FindPaymentMethodRequestBody body;

    public FindPaymentMethodRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public FindPaymentMethodRequestBody getBody() {
        return body;
    }

    public void setBody(FindPaymentMethodRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-PAYMENTMETHOD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        PaymentInfoResponse paymentMethodResponse = getAdminServiceRemote().adminPaymentMethodRetrieve(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getId(), this.getBody().getType());

        FindPaymentMethodResponse findPaymentMethodResponse = new FindPaymentMethodResponse();
        PaymentInfo paymentInfo = paymentMethodResponse.getPaymentInfo();
        if (paymentInfo != null) {
            com.techedge.mp.frontendbo.adapter.entities.common.PaymentInfo paymentInfoBO = new com.techedge.mp.frontendbo.adapter.entities.common.PaymentInfo();
            paymentInfoBO.setAttemptsLeft(paymentInfo.getAttemptsLeft());
            paymentInfoBO.setBrand(paymentInfo.getBrand());
            paymentInfoBO.setDefaultMethod(paymentInfo.getDefaultMethod().toString());
            paymentInfoBO.setCheckAmount(paymentInfo.getCheckAmount());
            if (paymentInfo.getExpirationDate() != null)
                paymentInfoBO.setExpirationDate(new Timestamp(paymentInfo.getExpirationDate().getTime()));
            paymentInfoBO.setId(paymentInfo.getId());
            if (paymentInfo.getInsertTimestamp() != null)
                paymentInfoBO.setInsertTimestamp(new Timestamp(paymentInfo.getInsertTimestamp().getTime()));
            paymentInfoBO.setMessage(paymentInfo.getMessage());
            paymentInfoBO.setPan(paymentInfo.getPan());
            paymentInfoBO.setPin(paymentInfo.getPin());
            paymentInfoBO.setStatus(paymentInfo.getStatus());
            paymentInfoBO.setToken(paymentInfo.getToken());
            paymentInfoBO.setType(paymentInfo.getType());
            if (paymentInfo.getVerifiedTimestamp() != null)
                paymentInfoBO.setVerifiedTimestamp(new Timestamp(paymentInfo.getVerifiedTimestamp().getTime()));

            findPaymentMethodResponse.setPaymentInfo(paymentInfoBO);

        }

        status.setStatusCode(paymentMethodResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(paymentMethodResponse.getStatusCode()));

        findPaymentMethodResponse.setStatus(status);

        return findPaymentMethodResponse;
    }

}
