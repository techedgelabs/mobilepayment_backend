package com.techedge.mp.frontendbo.adapter.entities.admin.testexecutepayment;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecutePaymentResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PaymentRefuelMode;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductCodeEnum;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestExecutePaymentRequest extends AbstractBORequest implements Validable {

    private Credential                    credential;
    private TestExecutePaymentBodyRequest body;

    public TestExecutePaymentRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestExecutePaymentBodyRequest getBody() {
        return body;
    }

    public void setBody(TestExecutePaymentBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("EXECUTE-PAYMENT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_EXECUTE_PAYMENT_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestExecutePaymentResponse testExecutePaymentResponse = new TestExecutePaymentResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testExecutePayment");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testExecutePaymentResponse.getStatus().setStatusCode("-1");
        }
        else {
            ExecutePaymentResult executePaymentResult = new ExecutePaymentResult();

            try {
                PaymentRefuelMode paymentRefuelMode = getEnumerationValue(PaymentRefuelMode.class, this.getBody().getRefuelMode());
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());
                ProductCodeEnum productCode = getEnumerationValue(ProductCodeEnum.class, this.getBody().getProductCode());

                executePaymentResult = getPaymentServiceRemote().executePayment(this.getBody().getOperationId(), this.getBody().getAmount(), productCode,
                        this.getBody().getQuantity(), this.getBody().getUnitPrice(), this.getBody().getAuthCryptogram(), this.getBody().getCurrencyCode(),
                        this.getBody().getMcCardDpan(), paymentRefuelMode, this.getBody().getShopCode(), partnerType, this.getBody().getRequestTimestamp());
            }
            catch (Exception ex) {
                executePaymentResult = null;
            }

            testExecutePaymentResponse.setStatusCode("OK");
            testExecutePaymentResponse.setResult(executePaymentResult);
        }

        return testExecutePaymentResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
