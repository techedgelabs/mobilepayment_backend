package com.techedge.mp.frontendbo.adapter.entities.admin.deletesurveyquestion;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class DeleteSurveyQuestionBodyRequest implements Validable {

    private String         code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public DeleteSurveyQuestionBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.code == null || this.code.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_SURVEY_QUESTION_DELETE_SUCCESS);

        return status;
    }

}
