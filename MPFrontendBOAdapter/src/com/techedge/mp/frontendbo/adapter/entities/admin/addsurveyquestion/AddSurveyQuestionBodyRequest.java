package com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.SurveyQuestion;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AddSurveyQuestionBodyRequest implements Validable {

    private String         code;
    private SurveyQuestion question;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public SurveyQuestion getQuestion() {
        return this.question;
    }

    public void setQuestion(SurveyQuestion question) {
        this.question = question;
    }

    public AddSurveyQuestionBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.code == null || this.code.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }

        if (this.question == null) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }
        status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS);

        return status;
    }

}
