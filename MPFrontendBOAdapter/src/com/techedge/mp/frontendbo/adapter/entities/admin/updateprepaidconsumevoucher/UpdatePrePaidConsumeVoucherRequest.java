package com.techedge.mp.frontendbo.adapter.entities.admin.updateprepaidconsumevoucher;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class UpdatePrePaidConsumeVoucherRequest extends AbstractBORequest implements Validable {

    private Status                                 status = new Status();

    private Credential                             credential;
    private UpdatePrePaidConsumeVoucherRequestBody body;

    public UpdatePrePaidConsumeVoucherRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdatePrePaidConsumeVoucherRequestBody getBody() {
        return body;
    }

    public void setBody(UpdatePrePaidConsumeVoucherRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-PREPAID-CONSUME-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        UpdatePrePaidConsumeVoucherResponse updatePrePaidConsumeVoucherResponse = new UpdatePrePaidConsumeVoucherResponse();

        String response = getAdminServiceRemote().adminUpdatePrepaidConsumeVoucher(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getId(),
                this.getBody().getCsTransactionID(), this.getBody().getMarketingMsg(), this.getBody().getMessageCode(), this.getBody().getOperationID(),
                this.getBody().getOperationIDReversed(), this.getBody().getOperationType(), this.getBody().getReconciled(), this.getBody().getStatusCode(),
                this.getBody().getTotalConsumed(), this.getBody().getWarningMsg(), this.getBody().getAmount(), this.getBody().getPreAuthOperationID());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        updatePrePaidConsumeVoucherResponse.setStatus(status);

        return updatePrePaidConsumeVoucherResponse;
    }

}
