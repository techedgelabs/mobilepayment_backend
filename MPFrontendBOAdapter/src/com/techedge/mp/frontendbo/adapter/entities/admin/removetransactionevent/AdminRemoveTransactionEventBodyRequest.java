package com.techedge.mp.frontendbo.adapter.entities.admin.removetransactionevent;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminRemoveTransactionEventBodyRequest implements Validable {

    private Long transactionEventId;

    public AdminRemoveTransactionEventBodyRequest() {}

    public Long getTransactionEventId() {
        return transactionEventId;
    }

    public void setTransactionEventId(Long transactionEventId) {
        this.transactionEventId = transactionEventId;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.transactionEventId == null) {
            status.setStatusCode(StatusCode.ADMIN_REMOVE_TRANSACTION_EVENT_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_REMOVE_TRANSACTION_EVENT_SUCCESS);

        return status;
    }

}
