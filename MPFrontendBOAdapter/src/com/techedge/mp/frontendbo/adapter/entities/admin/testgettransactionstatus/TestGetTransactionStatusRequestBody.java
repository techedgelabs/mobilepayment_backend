package com.techedge.mp.frontendbo.adapter.entities.admin.testgettransactionstatus;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestGetTransactionStatusRequestBody implements Validable {

    private String mpTransactionID;
    private String srcTransactionID;

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.mpTransactionID == null || this.mpTransactionID.isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS);
            return status;
        }

        if (this.srcTransactionID == null || this.srcTransactionID.isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);
        return status;
    }

}
