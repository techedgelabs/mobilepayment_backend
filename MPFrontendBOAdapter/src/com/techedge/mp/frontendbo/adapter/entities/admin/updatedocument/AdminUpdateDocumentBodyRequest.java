package com.techedge.mp.frontendbo.adapter.entities.admin.updatedocument;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminUpdateDocumentBodyRequest implements Validable {

    private String  documentKey;
    private Integer position;
    private String  templateFile;
    private String  title;
    private String  subtitle;
    private String  userCategory;
    private String  groupCategory;

    public String getDocumentKey() {
        return documentKey;
    }

    public void setDocumentKey(String documentKey) {
        this.documentKey = documentKey;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getTemplateFile() {
        return templateFile;
    }

    public void setTemplateFile(String templateFile) {
        this.templateFile = templateFile;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUserCategory() {
        return userCategory;
    }

    public void setUserCategory(String userCategory) {
        this.userCategory = userCategory;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getGroupCategory() {
        return groupCategory;
    }

    public void setGroupCategory(String groupCategory) {
        this.groupCategory = groupCategory;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (this.documentKey == null || this.documentKey.isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_DOCUMENT_UPDATE_CHECK_FAILURE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_UPDATE_SUCCESS);

        return status;
    }
}
