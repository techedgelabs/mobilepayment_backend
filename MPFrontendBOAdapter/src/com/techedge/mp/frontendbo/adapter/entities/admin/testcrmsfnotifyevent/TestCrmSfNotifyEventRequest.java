package com.techedge.mp.frontendbo.adapter.entities.admin.testcrmsfnotifyevent;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestCrmSfNotifyEventRequest extends AbstractBORequest implements Validable {

    private Credential                  credential;
    private TestCrmSfNotifyEventBodyRequest body;

    public TestCrmSfNotifyEventRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestCrmSfNotifyEventBodyRequest getBody() {
        return body;
    }

    public void setBody(TestCrmSfNotifyEventBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("NOTIFY-EVENT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_CRMSF_NOTIFY_EVENT_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestCrmSfNotifyEventResponse testCrmSfNotifyEventResponse = new TestCrmSfNotifyEventResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testCrmSfNotifyEvent");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

        	testCrmSfNotifyEventResponse.getStatus().setStatusCode("-1");
        }
        else {

        	NotifyEventResponse notifyEventResult = new NotifyEventResponse();
        	
        	String requestId = new IdGenerator().generateId(16).substring(0, 32); 
        	
        	DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss", Locale.ITALIAN);
        	DateFormat formatBirthDate = new SimpleDateFormat("yyyy-MM-dd", Locale.ITALIAN);
        	Date date = null;
        	Date birthDate = null;
        	try {
	        	if(this.getBody().getDate()!=null && !this.getBody().getDate().equalsIgnoreCase(""))
						date = formatDate.parse(this.getBody().getDate());
	        	if(this.getBody().getBirthDate()!=null && !this.getBody().getBirthDate().equalsIgnoreCase(""))
	        		birthDate = formatBirthDate.parse(this.getBody().getBirthDate());
        	} catch (ParseException e) {
				e.printStackTrace();
			}

            try {
                notifyEventResult = getCrmService().sendNotifySfEventOffer(requestId, this.getBody().getFiscalCode(), 
                		date, this.getBody().getStationId(), this.getBody().getProductId(), this.getBody().getPaymentFlag(), 
                		this.getBody().getCredits(), this.getBody().getQuantity(), this.getBody().getRefuelMode(), this.getBody().getFirstName(), 
                		this.getBody().getLastName(), this.getBody().getEmail(), birthDate, this.getBody().getNotificationFlag(), 
                		this.getBody().getPaymentCardFlag(), this.getBody().getBrand(), this.getBody().getCluster(), this.getBody().getAmount(), 
                		this.getBody().getPrivacyFlag1(), this.getBody().getPrivacyFlag2(), this.getBody().getMobilePhone(), 
                		this.getBody().getLoyaltyCard(), this.getBody().getEventType(), this.getBody().getParameter1(), 
                		this.getBody().getParameter2(), this.getBody().getPaymentMode());
            }
            catch (Exception ex) {
            	notifyEventResult.setErrorCode(ResponseHelper.SYSTEM_ERROR);
            	notifyEventResult.setSuccess(false);
            }
            
            //testCrmSfNotifyEventResponse.setCsTransactionID(csTransactionID);
            testCrmSfNotifyEventResponse.setErrorCode(notifyEventResult.getErrorCode());
            testCrmSfNotifyEventResponse.setMessage(notifyEventResult.getMessage());
            testCrmSfNotifyEventResponse.setRequestId(notifyEventResult.getRequestId());
            //testCrmSfNotifyEventResponse.setRequestTimestamp(notifyEventResult.get);
            //testCrmSfNotifyEventResponse.setStatus(notifyEventResult.get);
            testCrmSfNotifyEventResponse.setSuccess(notifyEventResult.getSuccess());            
            
            

        }

        return testCrmSfNotifyEventResponse;
    }


}
