package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveadminrole;

import com.techedge.mp.core.business.interfaces.AdminRolesResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminRetrieveAdminRoleRequest extends AbstractBORequest implements Validable {
    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-ROLES", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_ADMIN_ROLE_RETRIEVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        AdminRolesResponse adminRetrieveAdminRolesResult = getAdminServiceRemote().retrieveAdminRole(this.getCredential().getTicketID(), this.getCredential().getRequestID());

        AdminRetrieveAdminRoleResponse adminRetrieveAdminRoleResponse = new AdminRetrieveAdminRoleResponse();
        status.setStatusCode(adminRetrieveAdminRolesResult.getStatusCode());
        status.setStatusMessage(prop.getProperty(adminRetrieveAdminRolesResult.getStatusCode()));

        adminRetrieveAdminRoleResponse.setStatus(status);
        AdminRetrieveAdminRoleBodyResponse bodyResponse = new AdminRetrieveAdminRoleBodyResponse();
        adminRetrieveAdminRoleResponse.setBody(bodyResponse);
        bodyResponse.setRoles(adminRetrieveAdminRolesResult.getAdminRoles());

        return adminRetrieveAdminRoleResponse;

    }

}