package com.techedge.mp.frontendbo.adapter.entities.admin.estimateextendparkingprice;

import java.util.Date;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminEstimateExtendedParkingPriceRequestBody implements Validable {

    private String lang;
    private String parkingId;
    private Date   requestedEndTime;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getParkingId() {
        return parkingId;
    }

    public void setParkingId(String parkingId) {
        this.parkingId = parkingId;
    }

    public Date getRequestedEndTime() {
        return requestedEndTime;
    }

    public void setRequestedEndTime(Date requestedEndTime) {
        this.requestedEndTime = requestedEndTime;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);
        return status;
    }

}
