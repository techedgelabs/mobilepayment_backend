package com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation;

import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.StationsAdminData;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationsoperation.StationsOpResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class StationSearchRequest extends AbstractBORequest implements Validable {

    private Status               status = new Status();

    private Credential           credential;
    private StationOpRequestBody body;

    public StationSearchRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public StationOpRequestBody getBody() {
        return body;
    }

    public void setBody(StationOpRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_STATION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        StationsOpResponse stationSOpResponse = new StationsOpResponse();
        // StationsAdminData stationsAdminData = new StationsAdminData();
        // stationsAdminData.setStationAdminList(this.getBody().getStation());
        
        Station station = new Station();
        station.setStationID(this.getBody().getStation().getStationID());
        station.setStationStatus(this.getBody().getStation().getStationStatus());
        station.setOilShopLogin(this.getBody().getStation().getOilShopLogin());
        station.setNoOilShopLogin(this.getBody().getStation().getNoOilShopLogin());
        
        StationsAdminData stationsAdminDataResponse = getAdminServiceRemote().adminSearchStations(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), station);

        status.setStatusCode(stationsAdminDataResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(stationsAdminDataResponse.getStatusCode()));

        stationSOpResponse.setStatus(status);
        stationSOpResponse.setStationsList(stationsAdminDataResponse.getStationAdminList());

        return stationSOpResponse;
    }
}
