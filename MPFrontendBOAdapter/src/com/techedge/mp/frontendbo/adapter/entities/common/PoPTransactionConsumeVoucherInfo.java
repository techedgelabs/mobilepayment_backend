package com.techedge.mp.frontendbo.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionData;

public class PoPTransactionConsumeVoucherInfo {

    private String                                       csTransactionID;
    private String                                       operationID;
    private String                                       operationType;
    private Long                                         requestTimestamp;
    private String                                       marketingMsg;
    private String                                       messageCode;
    private String                                       statusCode;
    private Double                                       totalConsumed;
    private PostPaidTransactionData                      transactionData;
    private List<PoPTransactionConsumeVoucherDetailInfo> PostPaidConsumeVoucherList = new ArrayList<PoPTransactionConsumeVoucherDetailInfo>(0);

    public PoPTransactionConsumeVoucherInfo() {}

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getTotalConsumed() {
        return totalConsumed;
    }

    public void setTotalConsumed(Double totalConsumed) {
        this.totalConsumed = totalConsumed;
    }

    public PostPaidTransactionData getTransactionData() {
        return transactionData;
    }

    public void setTransactionData(PostPaidTransactionData transactionData) {
        this.transactionData = transactionData;
    }

    public List<PoPTransactionConsumeVoucherDetailInfo> getPostPaidConsumeVoucherList() {
        return PostPaidConsumeVoucherList;
    }

    public void setPostPaidConsumeVoucherList(List<PoPTransactionConsumeVoucherDetailInfo> postPaidConsumeVoucherList) {
        PostPaidConsumeVoucherList = postPaidConsumeVoucherList;
    }

}
