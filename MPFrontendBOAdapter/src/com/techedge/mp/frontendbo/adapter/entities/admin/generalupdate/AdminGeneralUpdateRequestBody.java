package com.techedge.mp.frontendbo.adapter.entities.admin.generalupdate;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.TypeData;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminGeneralUpdateRequestBody implements Validable {

    private String         type;
    private Long         id;
    private List<TypeData> fields = new ArrayList<>(0);

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<TypeData> getFields() {
        return fields;
    }

    public void setFields(List<TypeData> fields) {
        this.fields = fields;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AdminGeneralUpdateRequestBody() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.type == null) {

            status.setStatusCode(StatusCode.ADMIN_GENERAL_CREATE_FAILURE);
            status.setStatusMessage("Type null");

            return status;
        }

        if (this.id == null) {

            status.setStatusCode(StatusCode.ADMIN_GENERAL_CREATE_FAILURE);
            status.setStatusMessage("ID null");

            return status;
        }
        status.setStatusCode(StatusCode.ADMIN_GENERAL_CREATE_SUCCESS);
        status.setStatusMessage("Oggetto creato con successo");

        return status;

    }
}
