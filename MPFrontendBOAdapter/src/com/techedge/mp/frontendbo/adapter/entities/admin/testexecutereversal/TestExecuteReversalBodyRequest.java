package com.techedge.mp.frontendbo.adapter.entities.admin.testexecutereversal;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestExecuteReversalBodyRequest implements Validable {

    private String  mcCardDpan;
    private String  retrievalRefNumber;
    private String  authCode;
    private String  shopCode;
    private Integer amount;
    private String  currencyCode;
    private String  messageReasonCode;
    private String  operationId;
    private String  partnerType;
    private Long    requestTimestamp;

    public String getMcCardDpan() {
        return mcCardDpan;
    }

    public void setMcCardDpan(String mcCardDpan) {
        this.mcCardDpan = mcCardDpan;
    }

    public String getRetrievalRefNumber() {
        return retrievalRefNumber;
    }

    public void setRetrievalRefNumber(String retrievalRefNumber) {
        this.retrievalRefNumber = retrievalRefNumber;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getMessageReasonCode() {
        return messageReasonCode;
    }

    public void setMessageReasonCode(String messageReasonCode) {
        this.messageReasonCode = messageReasonCode;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationId == null || this.operationId.trim().isEmpty() || this.amount == null || this.shopCode == null || this.shopCode.trim().isEmpty()
                || this.currencyCode == null || this.currencyCode.trim().isEmpty() || this.mcCardDpan == null || this.mcCardDpan.trim().isEmpty() || this.partnerType == null
                || this.partnerType.trim().isEmpty() || this.requestTimestamp == null || this.retrievalRefNumber == null || this.retrievalRefNumber.trim().isEmpty()
                || this.authCode == null || this.authCode.trim().isEmpty() || this.messageReasonCode == null || this.messageReasonCode.trim().isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_TEST_EXECUTE_REVERSAL_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_EXECUTE_REVERSAL_SUCCESS);

        return status;
    }

}
