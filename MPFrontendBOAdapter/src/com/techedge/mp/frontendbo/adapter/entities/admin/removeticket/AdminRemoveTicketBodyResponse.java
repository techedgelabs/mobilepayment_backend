package com.techedge.mp.frontendbo.adapter.entities.admin.removeticket;

public class AdminRemoveTicketBodyResponse {

    private Integer deletedTickets;

    public Integer getDeletedTickets() {
        return deletedTickets;
    }

    public void setDeletedTickets(Integer deletedTickets) {
        this.deletedTickets = deletedTickets;
    }

}
