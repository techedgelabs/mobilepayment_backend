package com.techedge.mp.frontendbo.adapter.entities.admin.extendparking;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminExtendParkingResponse extends BaseResponse{
    
    private AdminExtendParkingResponseBody body;

    public AdminExtendParkingResponseBody getBody() {
        return body;
    }

    public void setBody(AdminExtendParkingResponseBody body) {
        this.body = body;
    }
}
