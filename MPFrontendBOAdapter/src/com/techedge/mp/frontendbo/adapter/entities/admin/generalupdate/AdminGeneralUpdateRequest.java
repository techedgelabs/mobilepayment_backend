package com.techedge.mp.frontendbo.adapter.entities.admin.generalupdate;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.TypeData;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminGeneralUpdateRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private AdminGeneralUpdateRequestBody body;

    public AdminGeneralUpdateRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminGeneralUpdateRequestBody getBody() {
        return body;
    }

    public void setBody(AdminGeneralUpdateRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GENERAL-UPDATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_GENERAL_UPDATE_INVALID_BODY);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_GENERAL_UPDATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        List<com.techedge.mp.core.business.interfaces.TypeData> listTypeData = new ArrayList<>(0);

        for (TypeData item : this.getBody().getFields()) {
            if (item != null) {
                com.techedge.mp.core.business.interfaces.TypeData typeData = new com.techedge.mp.core.business.interfaces.TypeData();
                typeData.setField(item.getField());
                typeData.setValue(item.getValue());
                listTypeData.add(typeData);
            }
        }

        String response = getAdminServiceRemote().adminGeneralUpdate(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getType(),
                this.getBody().getId(), listTypeData);

        AdminGeneralUpdateResponse adminGeneralUpdateResponse = new AdminGeneralUpdateResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        adminGeneralUpdateResponse.setStatus(status);

        return adminGeneralUpdateResponse;
    }

}
