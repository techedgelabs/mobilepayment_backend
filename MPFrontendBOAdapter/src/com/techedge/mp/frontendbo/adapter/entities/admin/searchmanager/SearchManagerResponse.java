package com.techedge.mp.frontendbo.adapter.entities.admin.searchmanager;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class SearchManagerResponse extends BaseResponse {

    private List<Manager> managerData = new ArrayList<Manager>(0);

    public List<Manager> getManagerData() {
        return managerData;
    }

    public void setManagerData(List<Manager> managerData) {
        this.managerData = managerData;
    }
    /*
    private class SearchManagerData extends ManagerData {
        private String password;
        private Long   id;

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }
    */
}
