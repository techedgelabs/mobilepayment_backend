package com.techedge.mp.frontendbo.adapter.entities.admin.testenableloyaltycard;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnableLoyaltyCardResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestEnableLoyaltyCardRequest extends AbstractBORequest implements Validable {

    private Credential                       credential;
    private TestEnableLoyaltyCardBodyRequest body;

    public TestEnableLoyaltyCardRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestEnableLoyaltyCardBodyRequest getBody() {
        return body;
    }

    public void setBody(TestEnableLoyaltyCardBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ENABLE-LOYALTY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_ENABLE_LOYALTY_CARD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestEnableLoyaltyCardResponse testEnableLoyaltyCardResponse = new TestEnableLoyaltyCardResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testEnableLoyaltyCard");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testEnableLoyaltyCardResponse.getStatus().setStatusCode("-1");
        }
        else {

            EnableLoyaltyCardResult enableLoyaltyCardResult = new EnableLoyaltyCardResult();

            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                enableLoyaltyCardResult = getFidelityServiceRemote().enableLoyaltyCard(this.getBody().getOperationID(), this.getBody().getFiscalCode(),
                        this.getBody().getPanCode(), this.getBody().getEnable(), partnerType, this.getBody().getRequestTimestamp());
            }
            catch (FidelityServiceException ex) {
                enableLoyaltyCardResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!enableLoyaltyCardResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {

                testEnableLoyaltyCardResponse.setCsTransactionID(enableLoyaltyCardResult.getCsTransactionID());
                testEnableLoyaltyCardResponse.getStatus().setStatusMessage(enableLoyaltyCardResult.getMessageCode());
                testEnableLoyaltyCardResponse.getStatus().setStatusCode(enableLoyaltyCardResult.getStatusCode());
            }
        }

        return testEnableLoyaltyCardResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
