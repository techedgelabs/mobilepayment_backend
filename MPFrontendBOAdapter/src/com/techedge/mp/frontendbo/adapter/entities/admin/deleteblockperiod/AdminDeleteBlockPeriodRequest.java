package com.techedge.mp.frontendbo.adapter.entities.admin.deleteblockperiod;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminDeleteBlockPeriodRequest extends AbstractBORequest implements Validable {

    private Status                            status = new Status();

    private Credential                        credential;
    private AdminDeleteBlockPeriodBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminDeleteBlockPeriodBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminDeleteBlockPeriodBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DELETE-BLOCK-PERIOD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_BLOCK_PERIOD_DELETE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_BLOCK_PERIOD_DELETE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String deleteBlockPeriodResult = getAdminServiceRemote().adminDeleteBlockPeriod(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getCode());

        AdminDeleteBlockPeriodResponse deleteBlockPeriodResponse = new AdminDeleteBlockPeriodResponse();
        status.setStatusCode(deleteBlockPeriodResult);
        status.setStatusMessage(prop.getProperty(deleteBlockPeriodResult));
        deleteBlockPeriodResponse.setStatus(status);

        return deleteBlockPeriodResponse;
    }

}