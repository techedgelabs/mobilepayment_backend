package com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreauthorizationconsumevoucher;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestCancelPreAuthorizationConsumeVoucherResponse extends BaseResponse{

    private String csTransactionID;


    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

}
