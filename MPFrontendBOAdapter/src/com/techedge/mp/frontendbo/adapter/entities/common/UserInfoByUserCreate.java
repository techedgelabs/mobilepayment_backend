package com.techedge.mp.frontendbo.adapter.entities.common;

import java.util.ArrayList;
import java.util.List;




public class UserInfoByUserCreate {
	
	private Long id;
	private String firstName;
	private String lastName;
	private String fiscalCode;
	private String birthDate;
	private String birthMunicipality;
	private String birthProvince;
	private String language;
	private String sex;
	private String email;
	private LastLoginData lastLoginData;
	private Integer status;
	private String registrationCompleted;
	private Double capAvailable;
	private Double capEffective;
	private String mobilePhone;
	private String externalUserId;
	private List<PaymentInfoByUserCreate> paymentData = new ArrayList<PaymentInfoByUserCreate>(0);


	public UserInfoByUserCreate() {}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}
	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

	public String getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthMunicipality() {
		return birthMunicipality;
	}
	public void setBirthMunicipality(String birthMunicipality) {
		this.birthMunicipality = birthMunicipality;
	}

	public String getBirthProvince() {
		return birthProvince;
	}
	public void setBirthProvince(String birthProvince) {
		this.birthProvince = birthProvince;
	}

	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}

	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public LastLoginData getLastLoginData() {
		return lastLoginData;
	}
	public void setLastLoginData(LastLoginData lastLoginData) {
		this.lastLoginData = lastLoginData;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getRegistrationCompleted() {
		return registrationCompleted;
	}
	public void setRegistrationCompleted(String registrationCompleted) {
		this.registrationCompleted = registrationCompleted;
	}

	public Double getCapAvailable() {
		return capAvailable;
	}
	public void setCapAvailable(Double capAvailable) {
		this.capAvailable = capAvailable;
	}

	public Double getCapEffective() {
		return capEffective;
	}
	public void setCapEffective(Double capEffective) {
		this.capEffective = capEffective;
	}

	public String getMobilePhone() {
		return mobilePhone;
	}
	public void setMobilePhone(String mobilePhone) {
		this.mobilePhone = mobilePhone;
	}

	public String getExternalUserId() {
		return externalUserId;
	}
	public void setExternalUserId(String externalUserId) {
		this.externalUserId = externalUserId;
	}

	public List<PaymentInfoByUserCreate> getPaymentData() {
		return paymentData;
	}
	public void setPaymentData(List<PaymentInfoByUserCreate> paymentData) {
		this.paymentData = paymentData;
	}
}
