package com.techedge.mp.frontendbo.adapter.entities.admin.startparking;

import java.util.Date;

import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.EstimateParkingPriceResult;
import com.techedge.mp.parking.integration.business.interfaces.GetCitiesResult;
import com.techedge.mp.parking.integration.business.interfaces.StartParkingResult;

public class AdminStartParkingRequest extends AbstractBORequest implements Validable {

    private Credential                credential;

    private AdminStartParkingRequestBody body;

    public AdminStartParkingRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminStartParkingRequestBody getBody() {
        return body;
    }

    public void setBody(AdminStartParkingRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-USERS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        AdminStartParkingResponse adminStartParkingResponse = new AdminStartParkingResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testRetrieveStation");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            adminStartParkingResponse.getStatus().setStatusCode("-1");
        }
        else {

            ParkingServiceRemote parkingService;

            try {
                parkingService = EJBHomeCache.getInstance().getParkingService();

                String lang = this.getBody().getLang();
                String plateNumber = this.getBody().getPlateNumber();
                Date requestedEndTime = this.getBody().getRequestedEndTime();
                String parkingZoneId = this.getBody().getParkingZoneId();
                String stallCode = this.getBody().getStallCode();
                String clientOperationID = this.getBody().getClientOperationID();

                StartParkingResult startParkingResult = parkingService.startParking(lang, plateNumber, requestedEndTime, parkingZoneId, stallCode, clientOperationID);

                AdminStartParkingResponseBody adminStartParkingResponseBody = new AdminStartParkingResponseBody();
                adminStartParkingResponseBody.setStartParkingResult(startParkingResult);

                adminStartParkingResponse.setBody(adminStartParkingResponseBody);
                adminStartParkingResponse.getStatus().setStatusCode(adminStartParkingResponseBody.getStartParkingResult().getStatusCode());
            }
            catch (InterfaceNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return adminStartParkingResponse;
    }
}
