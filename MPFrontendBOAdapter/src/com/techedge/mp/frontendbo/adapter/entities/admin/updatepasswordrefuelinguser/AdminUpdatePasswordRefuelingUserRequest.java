package com.techedge.mp.frontendbo.adapter.entities.admin.updatepasswordrefuelinguser;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminUpdatePasswordRefuelingUserRequest extends AbstractBORequest implements Validable {
    private Status                                      status = new Status();

    private Credential                                  credential;
    private AdminUpdatePasswordRefuelingUserBodyRequest body;

    public AdminUpdatePasswordRefuelingUserRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminUpdatePasswordRefuelingUserBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminUpdatePasswordRefuelingUserBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADMIN-UPD-PWD-REFUELING-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_UPD_PWD_REFUELING_USER_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPD_PWD_REFUELING_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String adminUpdatePasswordRefuelingUserResult = getAdminServiceRemote().adminUpdatePasswordRefuelingUser(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), this.getBody().getUsername(), this.getBody().getNewPassword());
        AdminUpdatePasswordRefuelingUserResponse response = new AdminUpdatePasswordRefuelingUserResponse();
        status.setStatusCode(adminUpdatePasswordRefuelingUserResult);
        status.setStatusMessage(prop.getProperty(adminUpdatePasswordRefuelingUserResult));
        response.setStatus(status);

        return response;

    }

}
