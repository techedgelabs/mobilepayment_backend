package com.techedge.mp.frontendbo.adapter.entities.admin.deletevoucher;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminDeleteVoucherRequestBody implements Validable {

    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.id == null) {

            status.setStatusCode(StatusCode.ADMIN_DELETE_VOUCHER_ERROR_PARAMETER);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_DELETE_VOUCHER_SUCCESS);

        return status;
    }

}
