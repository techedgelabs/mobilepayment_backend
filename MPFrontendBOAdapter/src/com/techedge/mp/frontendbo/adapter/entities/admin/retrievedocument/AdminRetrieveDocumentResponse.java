package com.techedge.mp.frontendbo.adapter.entities.admin.retrievedocument;

import java.util.List;

import com.techedge.mp.core.business.interfaces.DocumentData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminRetrieveDocumentResponse extends BaseResponse {

    private List<DocumentData> document;

    public List<DocumentData> getDocument() {
        return document;
    }

    public void setDocument(List<DocumentData> document) {
        this.document = document;
    }
}