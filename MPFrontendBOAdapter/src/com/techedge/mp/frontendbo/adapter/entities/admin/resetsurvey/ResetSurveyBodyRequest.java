package com.techedge.mp.frontendbo.adapter.entities.admin.resetsurvey;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class ResetSurveyBodyRequest implements Validable {

	private String code;


	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
	@Override
	public Status check() {

		Status status = new Status();
		
        if (this.code == null || this.code.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }
		
		status.setStatusCode(StatusCode.ADMIN_SURVEY_RESET_SUCCESS);
		
		return status;
		
	}
	
}
