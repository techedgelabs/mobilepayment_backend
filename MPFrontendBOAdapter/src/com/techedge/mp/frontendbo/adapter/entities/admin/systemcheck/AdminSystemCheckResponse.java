package com.techedge.mp.frontendbo.adapter.entities.admin.systemcheck;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;


public class AdminSystemCheckResponse extends BaseResponse {
	
    AdminSystemCheckBodyResponse body;
    
    public AdminSystemCheckBodyResponse getBody() {
        return body;
    }
    
    public void setBody(AdminSystemCheckBodyResponse body) {
        this.body = body;
    }
}
