package com.techedge.mp.frontendbo.adapter.entities.admin.forceupdatetermsfservice;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class ForceUpdateTermsOfServiceRequest extends AbstractBORequest implements Validable {

    private Status                               status = new Status();

    private Credential                           credential;
    private ForceUpdateTermsOfServiceBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ForceUpdateTermsOfServiceBodyRequest getBody() {
        return body;
    }

    public void setBody(ForceUpdateTermsOfServiceBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("FORCE-UPDATE-TERM-OF-SERVICE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_TERMS_OF_SERVICE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        String forceUpdateTermsOfServiceResult = getAdminServiceRemote().adminForceUpdateTermsOfService(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getTermsOfServiceId(), this.getBody().getUserId(), this.getBody().getValid());
        ForceUpdateTermsOfServiceResponse forceUpdateTermsOfServiceResponse = new ForceUpdateTermsOfServiceResponse();
        status.setStatusCode(forceUpdateTermsOfServiceResult);
        status.setStatusMessage(prop.getProperty(forceUpdateTermsOfServiceResult));
        forceUpdateTermsOfServiceResponse.setStatus(status);

        return forceUpdateTermsOfServiceResponse;
    }

}
