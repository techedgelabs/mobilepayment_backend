package com.techedge.mp.frontendbo.adapter.entities.admin.refreshparams;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ParamInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;


public class RefreshParamsResponse extends BaseResponse {
	
	List<ParamInfo> params = new ArrayList<ParamInfo>(0);

	public List<ParamInfo> getParams() {
		return params;
	}
	public void setParmas(List<ParamInfo> params) {
		this.params = params;
	}
}