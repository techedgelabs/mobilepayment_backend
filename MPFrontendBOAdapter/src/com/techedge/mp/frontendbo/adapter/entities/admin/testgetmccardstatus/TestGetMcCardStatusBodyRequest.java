package com.techedge.mp.frontendbo.adapter.entities.admin.testgetmccardstatus;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestGetMcCardStatusBodyRequest implements Validable {

    private String operationId;
    private String userId;
    private String partnerType;
    private Long   requestTimestamp;

    public TestGetMcCardStatusBodyRequest() {}

    public String getOperationId() {
        return operationId;
    }

    public void setOperationID(String operationId) {
        this.operationId = operationId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationId == null || this.operationId.trim().isEmpty() || this.userId == null || this.userId.trim().isEmpty()
                || this.partnerType == null || this.partnerType.trim().isEmpty() || this.requestTimestamp == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_GET_MC_CARD_STATUS_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_GET_MC_CARD_STATUS_SUCCESS);

        return status;
    }

}
