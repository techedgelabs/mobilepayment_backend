package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveblockperiod;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminRetrieveBlockPeriodResponse extends BaseResponse {

    private AdminRetrieveBlockPeriodBodyResponse body;

    public AdminRetrieveBlockPeriodBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminRetrieveBlockPeriodBodyResponse body) {
        this.body = body;
    }

}