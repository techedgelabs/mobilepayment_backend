package com.techedge.mp.frontendbo.adapter.entities.admin.testgetloyaltycardlist;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.CardInfo;

public class TestGetLoyaltyCardListResponse extends BaseResponse{

    private String         csTransactionID;
    private Integer        balance;
    private List<CardInfo> cardList = new ArrayList<CardInfo>(0);

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public List<CardInfo> getCardList() {
        return cardList;
    }

    public void setCardList(List<CardInfo> cardList) {
        this.cardList = cardList;
    }

}
