package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepaymentdatarefuelinguser;

import com.techedge.mp.core.business.interfaces.PaymentDataResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminRetrievePaymentDataRefuelingUserRequest extends AbstractBORequest implements Validable {
    private Status                                           status = new Status();

    private Credential                                       credential;
    private AdminRetrievePaymentDataRefuelingUserBodyRequest body;

    public AdminRetrievePaymentDataRefuelingUserRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminRetrievePaymentDataRefuelingUserBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminRetrievePaymentDataRefuelingUserBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADMIN-RETRIEVE-PAYMENT-DATA-REFUELING-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_RETR_PAY_DATA_REFUELING_USER_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETR_PAY_DATA_REFUELING_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String statusResult = StatusCode.ADMIN_RETR_PAY_DATA_REFUELING_USER_SUCCESS;

        PaymentDataResponse paymentMethod = getAdminServiceRemote().adminRetrievePaymentDataMethodRefuelingUser(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), this.getBody().getUsername(), this.getBody().getPaymentMethod().getId());

        AdminRetrievePaymentDataRefuelingUserResponse response = new AdminRetrievePaymentDataRefuelingUserResponse();
        response.setBody(new AdminRetrievePaymentDataRefuelingUserBodyResponse());
        response.getBody().setPaymentMethodList(paymentMethod.getPaymentMethodList());
        status.setStatusCode(paymentMethod.getStatusCode());
        status.setStatusMessage(prop.getProperty(statusResult));
        response.setStatus(status);
        return response;

    }

}
