package com.techedge.mp.frontendbo.adapter.entities.admin.retrievedocument;

import com.techedge.mp.core.business.interfaces.RetrieveDocumentResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminRetrieveDocumentRequest extends AbstractBORequest implements Validable {

    private Status                           status = new Status();

    private Credential                       credential;
    private AdminRetrieveDocumentBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminRetrieveDocumentBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminRetrieveDocumentBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-DOCUMENT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_DOCUMENT_RETRIEVE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_RETRIEVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        RetrieveDocumentResponse retrieveDocumentResult = getAdminServiceRemote().adminRetrieveDocument(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getKey());

        AdminRetrieveDocumentResponse retrieveDocumentResponse = new AdminRetrieveDocumentResponse();
        status.setStatusCode(retrieveDocumentResult.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveDocumentResult.getStatusCode()));
        retrieveDocumentResponse.setStatus(status);
        retrieveDocumentResponse.setDocument(retrieveDocumentResult.getListDocument());

        return retrieveDocumentResponse;
    }

}