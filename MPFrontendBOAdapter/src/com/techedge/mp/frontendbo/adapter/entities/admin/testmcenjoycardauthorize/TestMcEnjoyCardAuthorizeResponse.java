package com.techedge.mp.frontendbo.adapter.entities.admin.testmcenjoycardauthorize;

import com.techedge.mp.fidelity.adapter.business.interfaces.McCardEnjoyAuthorizeResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestMcEnjoyCardAuthorizeResponse extends BaseResponse {

    private String                     statusCode;
    private McCardEnjoyAuthorizeResult result;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public McCardEnjoyAuthorizeResult getResult() {
        return result;
    }

    public void setResult(McCardEnjoyAuthorizeResult result) {
        this.result = result;
    }

}
