package com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.TransactionInfo;


public class RetrieveTransactionsResponse extends BaseResponse {
	
	List<TransactionInfo> transactions = new ArrayList<TransactionInfo>(0);

	public List<TransactionInfo> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<TransactionInfo> transactions) {
		this.transactions = transactions;
	}
}
