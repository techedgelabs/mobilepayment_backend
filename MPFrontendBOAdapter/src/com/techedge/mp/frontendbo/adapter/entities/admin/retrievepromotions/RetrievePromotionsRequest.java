package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepromotions;

import com.techedge.mp.core.business.interfaces.PromotionInfo;
import com.techedge.mp.core.business.interfaces.RetrievePromotionsData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Promotion;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrievePromotionsRequest extends AbstractBORequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public RetrievePromotionsRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-TRANSACTIONS-MANAGER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        RetrievePromotionsResponse retrievePromotionsResponse = new RetrievePromotionsResponse();

        RetrievePromotionsData retrievePromotionsData = getAdminServiceRemote().adminRetrievePromotions(this.getCredential().getTicketID(), this.getCredential().getRequestID());

        status.setStatusCode(retrievePromotionsData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrievePromotionsData.getStatusCode()));

        retrievePromotionsResponse.setStatus(status);

        for (PromotionInfo promotionInfo : retrievePromotionsData.getPromotionList()) {

            Promotion promotion = new Promotion();
            promotion.setCode(promotionInfo.getCode());
            promotion.setDescription(promotionInfo.getDescription());
            promotion.setEndData(promotionInfo.getEndData());
            promotion.setStartData(promotionInfo.getStartData());
            promotion.setStatus(promotionInfo.getStatus());
            retrievePromotionsResponse.getPromotions().add(promotion);
        }

        return retrievePromotionsResponse;
    }

}
