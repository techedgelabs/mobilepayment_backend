package com.techedge.mp.frontendbo.adapter.entities.admin.removetransactionevent;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminRemoveTransactionEventRequest extends AbstractBORequest implements Validable {

    private Status                                 status = new Status();

    private Credential                             credential;
    private AdminRemoveTransactionEventBodyRequest body;

    public AdminRemoveTransactionEventRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminRemoveTransactionEventBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminRemoveTransactionEventBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REMOVE-TRANSACTION-EVENT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_REMOVE_TRANSACTION_EVENT_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String adminRemoveTransactionEventResult = getAdminServiceRemote().adminRemoveTransactionEvent(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getTransactionEventId());

        AdminRemoveTransactionEventResponse AdminRemoveTransactionEventResponse = new AdminRemoveTransactionEventResponse();

        status.setStatusCode(adminRemoveTransactionEventResult);
        status.setStatusMessage(prop.getProperty(adminRemoveTransactionEventResult));
        AdminRemoveTransactionEventResponse.setStatus(status);

        return AdminRemoveTransactionEventResponse;
    }

}
