package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveparams;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ParamInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;



public class RetrieveParamsResponse extends BaseResponse {
	
	private List<ParamInfo> paramList = new ArrayList<ParamInfo>(0);

	public List<ParamInfo> getParamList() {
		return paramList;
	}

	public void setParamList(List<ParamInfo> paramList) {
		this.paramList = paramList;
	}
	
	

}
