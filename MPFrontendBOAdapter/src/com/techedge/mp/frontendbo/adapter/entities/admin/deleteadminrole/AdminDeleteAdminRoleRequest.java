package com.techedge.mp.frontendbo.adapter.entities.admin.deleteadminrole;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminDeleteAdminRoleRequest extends AbstractBORequest implements Validable {
    private Status                          status = new Status();

    private Credential                      credential;
    private AdminDeleteAdminRoleBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminDeleteAdminRoleBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminDeleteAdminRoleBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DELETE-ADMIN-ROLE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_ADMIN_ROLE_DELETE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_ADMIN_ROLE_DELETE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String deleteAdminRoleResult = getAdminServiceRemote().adminDeleteAdminRole(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getName());

        AdminDeleteAdminRoleResponse deleteAdminRoleResponse = new AdminDeleteAdminRoleResponse();
        status.setStatusCode(deleteAdminRoleResult);
        status.setStatusMessage(prop.getProperty(deleteAdminRoleResult));
        deleteAdminRoleResponse.setStatus(status);

        return deleteAdminRoleResponse;

    }

}