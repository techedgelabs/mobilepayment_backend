package com.techedge.mp.frontendbo.adapter.entities.admin.testmcenjoycardauthorize;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestMcEnjoyCardAuthorizeBodyRequest implements Validable {

    private String  operationId;
    private Integer amount;
    private String  currencyCode;
    private String  mcCardDpan;
    private String  partnerType;
    private String  paymentMode;
    private Long    requestTimestamp;
    private String  serverSerialNumber;
    private String  userId;

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public String getMcCardDpan() {
        return mcCardDpan;
    }

    public void setMcCardDpan(String mcCardDpan) {
        this.mcCardDpan = mcCardDpan;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getServerSerialNumber() {
        return serverSerialNumber;
    }

    public void setServerSerialNumber(String serverSerialNumber) {
        this.serverSerialNumber = serverSerialNumber;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationId == null || this.operationId.trim().isEmpty() || this.amount == null || this.currencyCode == null || this.currencyCode.trim().isEmpty()
                || this.mcCardDpan == null || this.mcCardDpan.trim().isEmpty() || this.partnerType == null || this.partnerType.trim().isEmpty() || this.paymentMode == null
                || this.paymentMode.trim().isEmpty() || this.requestTimestamp == null || this.serverSerialNumber == null || this.serverSerialNumber.trim().isEmpty()
                || this.userId == null || this.userId.trim().isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_TEST_MC_ENJOY_CARD_AUTHORIZE_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_MC_ENJOY_CARD_AUTHORIZE_SUCCESS);

        return status;
    }

}
