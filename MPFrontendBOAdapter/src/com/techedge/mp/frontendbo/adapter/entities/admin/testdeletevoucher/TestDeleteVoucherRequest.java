package com.techedge.mp.frontendbo.adapter.entities.admin.testdeletevoucher;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.DeleteVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestDeleteVoucherRequest extends AbstractBORequest implements Validable {

    private Credential                   credential;
    private TestDeleteVoucherBodyRequest body;

    public TestDeleteVoucherRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestDeleteVoucherBodyRequest getBody() {
        return body;
    }

    public void setBody(TestDeleteVoucherBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DELETE-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_DELETE_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestDeleteVoucherResponse testDeleteVoucherResponse = new TestDeleteVoucherResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testCheckConsumeVoucherTransaction");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testDeleteVoucherResponse.getStatus().setStatusCode("-1");
        }
        else {

            DeleteVoucherResult deleteVoucherResult = new DeleteVoucherResult();
            PartnerType partnerType = PartnerType.MP;

            try {

                deleteVoucherResult = getFidelityServiceRemote().deleteVoucher(this.getBody().getOperationIDtoReverse(), this.getBody().getOperationID(), partnerType,
                        this.getBody().getRequestTimestamp());
            }
            catch (FidelityServiceException ex) {
                deleteVoucherResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!deleteVoucherResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                testDeleteVoucherResponse.setCsTransactionID(deleteVoucherResult.getCsTransactionID());
                testDeleteVoucherResponse.getStatus().setStatusMessage(deleteVoucherResult.getMessageCode());
                testDeleteVoucherResponse.getStatus().setStatusCode(deleteVoucherResult.getStatusCode());
            }
        }

        return testDeleteVoucherResponse;
    }

}
