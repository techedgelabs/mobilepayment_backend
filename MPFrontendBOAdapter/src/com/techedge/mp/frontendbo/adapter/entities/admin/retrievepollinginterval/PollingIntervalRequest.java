package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepollinginterval;

import com.techedge.mp.core.business.interfaces.PollingIntervalData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class PollingIntervalRequest extends AbstractBORequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-POLLING-INTERVAL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.RETRIEVE_POLLING_INTERVAL_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        PollingIntervalData pollingIntervalData = getAdminServiceRemote().adminRetrievePollingInterval(this.getCredential().getTicketID(), this.getCredential().getRequestID());

        PollingIntervalResponse pollingIntervalResponse = new PollingIntervalResponse();

        status.setStatusCode(pollingIntervalData.getStatusCode());
        status.setStatusMessage(prop.getProperty(pollingIntervalData.getStatusCode()));

        pollingIntervalResponse.setStatus(status);
        pollingIntervalResponse.setBody(new PollingIntervalBodyResponse());
        pollingIntervalResponse.getBody().setPollingIntervalList(pollingIntervalData.getPollingInterval());

        return pollingIntervalResponse;
    }

}
