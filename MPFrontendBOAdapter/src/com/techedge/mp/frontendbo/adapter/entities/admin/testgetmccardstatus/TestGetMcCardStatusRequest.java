package com.techedge.mp.frontendbo.adapter.entities.admin.testgetmccardstatus;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.interfaces.DpanDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetMcCardStatusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestGetMcCardStatusRequest extends AbstractBORequest implements Validable {

    private Credential                     credential;
    private TestGetMcCardStatusBodyRequest body;

    public TestGetMcCardStatusRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestGetMcCardStatusBodyRequest getBody() {
        return body;
    }

    public void setBody(TestGetMcCardStatusBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-MC-CARD-STATUS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_GET_MC_CARD_STATUS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestGetMcCardStatusResponse testGetMcCardStatusResponse = new TestGetMcCardStatusResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testGetMcCardStatusList");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testGetMcCardStatusResponse.getStatus().setStatusCode("-1");
        }
        else {
            GetMcCardStatusResult getMcCardStatusResult = new GetMcCardStatusResult();

            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                getMcCardStatusResult = getCardInfoServiceRemote().getMcCardStatus(this.getBody().getOperationId(), this.getBody().getUserId(), partnerType,
                        this.getBody().getRequestTimestamp());
            }
            catch (Exception ex) {
                getMcCardStatusResult.setStatus("ERROR");
            }

            if (getMcCardStatusResult.getStatus() != null) {
                testGetMcCardStatusResponse.set_status(getMcCardStatusResult.getStatus());
                testGetMcCardStatusResponse.setCode(getMcCardStatusResult.getCode());
                testGetMcCardStatusResponse.setMessage(getMcCardStatusResult.getMessage());
                testGetMcCardStatusResponse.setTransactionId(getMcCardStatusResult.getTransactionId());
                for (DpanDetail dpanDetail : getMcCardStatusResult.getDpanDetailList()) {
                    testGetMcCardStatusResponse.getDpanDetailList().add(dpanDetail);
                }
            }
        }

        return testGetMcCardStatusResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
