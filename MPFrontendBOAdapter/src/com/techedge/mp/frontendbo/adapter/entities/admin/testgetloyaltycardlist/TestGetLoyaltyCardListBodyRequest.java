package com.techedge.mp.frontendbo.adapter.entities.admin.testgetloyaltycardlist;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestGetLoyaltyCardListBodyRequest implements Validable {

    private String operationID;
    private String fiscalCode;
    private String eanCode;
    private String partnerType;
    private Long   requestTimestamp;

    public TestGetLoyaltyCardListBodyRequest() {}

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationID == null || this.operationID.trim().isEmpty() || this.fiscalCode == null || this.fiscalCode.trim().isEmpty() || this.eanCode == null
                || this.eanCode.trim().isEmpty() || this.partnerType == null || this.partnerType.trim().isEmpty() || this.requestTimestamp == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_GET_LOYALTY_CARD_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_GET_LOYALTY_CARD_SUCCESS);

        return status;
    }

}
