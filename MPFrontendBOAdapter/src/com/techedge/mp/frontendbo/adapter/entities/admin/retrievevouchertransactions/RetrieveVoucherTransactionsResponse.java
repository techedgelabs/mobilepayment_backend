package com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherTransactionInfo;

public class RetrieveVoucherTransactionsResponse extends BaseResponse {

    List<VoucherTransactionInfo> voucherTransactions = new ArrayList<VoucherTransactionInfo>(0);

    public List<VoucherTransactionInfo> getVoucherTransactions() {
        return voucherTransactions;
    }

    public void setVoucherTransactions(List<VoucherTransactionInfo> voucherTransactions) {
        this.voucherTransactions = voucherTransactions;
    }

}
