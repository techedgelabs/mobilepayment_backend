package com.techedge.mp.frontendbo.adapter.entities.admin.testcrmsfgetmission;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.crm.OfferSF;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestCrmSfGetMissionResponse extends BaseResponse {


	private Boolean success;
	private String errorCode;
	private String message;
	private String requestId;
	private List<OfferSF> offers = new ArrayList<OfferSF>(0);

	public Boolean getSuccess() {
		return success;
	}

	public void setSuccess(Boolean success) {
		this.success = success;
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getRequestId() {
		return requestId;
	}

	public void setRequestId(String requestId) {
		this.requestId = requestId;
	}

	public List<OfferSF> getOffers() {
		return offers;
	}

	public void setOffers(List<OfferSF> offers) {
		this.offers = offers;
	}

}
