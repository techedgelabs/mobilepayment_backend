package com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher;

import java.math.BigDecimal;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestCreateVoucherBodyRequest implements Validable {

    private String     operationID;
    private String     partnerType;
    private Long       requestTimestamp;
    private String     voucherType;
    private BigDecimal totalAmount;
    private String     bankTransactionID;
    private String     shopTransactionID;
    private String     authorizationCode;

    public TestCreateVoucherBodyRequest() {}
    
    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }
    
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    public String getBankTransactionID() {
        return bankTransactionID;
    }

    public void setBankTransactionID(String bankTransactionID) {
        this.bankTransactionID = bankTransactionID;
    }

    public String getShopTransactionID() {
        return shopTransactionID;
    }

    public void setShopTransactionID(String shopTransactionID) {
        this.shopTransactionID = shopTransactionID;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }
    
    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }
    
    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.voucherType == null || this.voucherType.trim().isEmpty() || this.totalAmount == null || this.bankTransactionID == null || this.bankTransactionID.trim().isEmpty()
                || this.shopTransactionID == null || this.authorizationCode == null || this.authorizationCode.trim().isEmpty() || this.operationID == null
                || this.operationID.trim().isEmpty() || this.partnerType == null || this.partnerType.trim().isEmpty() || this.requestTimestamp == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_FIDELITY_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_FIDELITY_SUCCESS);

        return status;
    }

}
