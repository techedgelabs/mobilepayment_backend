package com.techedge.mp.frontendbo.adapter.entities.admin.getcities;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminGetCitiesResponse extends BaseResponse{
    
    private AdminGetCitiesResponseBody body;

    public AdminGetCitiesResponseBody getBody() {
        return body;
    }

    public void setBody(AdminGetCitiesResponseBody body) {
        this.body = body;
    }
}
