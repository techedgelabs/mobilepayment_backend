package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcatalog;

import java.util.Date;

import com.techedge.mp.dwh.adapter.interfaces.AwardDetail;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCatalogResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminDwhGetCatalogRequest extends AbstractBORequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DWH-GET-CATALOG-LIST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DWH_GET_CATALOG_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        Date now = new Date();

        String card = null;
        String requestId = String.valueOf(now.getTime());
        Long requestTimestamp = now.getTime();

        DWHGetCatalogResult result = getDwhAdapterServiceRemote().getCatalog(card, requestId, requestTimestamp);

        AdminDwhGetCatalogResponse adminDwhGetCatalogResponse = new AdminDwhGetCatalogResponse();

        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(result.getMessageCode());
        adminDwhGetCatalogResponse.setStatus(status);

        if (result.getAwardDetailList() != null && !result.getAwardDetailList().isEmpty()) {

            AdminDwhGetCatalogBodyResponse adminDwhGetCatalogBodyResponse = new AdminDwhGetCatalogBodyResponse();

            for (AwardDetail awardDetail : result.getAwardDetailList()) {

                adminDwhGetCatalogBodyResponse.getAwardDetailList().add(awardDetail);
            }

            adminDwhGetCatalogResponse.setBody(adminDwhGetCatalogBodyResponse);
        }

        return adminDwhGetCatalogResponse;
    }

}