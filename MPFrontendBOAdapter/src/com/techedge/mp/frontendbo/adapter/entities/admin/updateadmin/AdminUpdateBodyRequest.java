package com.techedge.mp.frontendbo.adapter.entities.admin.updateadmin;

import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminUpdateBodyRequest implements Validable {

    private Admin adminData;

    public Admin getAdminData() {
        return adminData;
    }

    public void setAdminData(Admin admin) {
        this.adminData = admin;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (adminData == null || (adminData != null && adminData.getId() == null)) {
            status.setStatusCode(StatusCode.ADMIN_UPDATE_ERROR_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_SUCCESS);

        return status;
    }
}
