package com.techedge.mp.frontendbo.adapter.entities.admin.createcustomeruser;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.UserInfoByUserCreate;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;


public class CreateCustomerUserDataRequest implements Validable {
	
	private UserInfoByUserCreate userInfo;
	private String password;
	private String token;
	private String newPin;
	//private AddressData addressData;
	//private AddressData billingAddressData;
	
	public CreateCustomerUserDataRequest() {
	}
	

	public UserInfoByUserCreate getUserInfo() {
		return userInfo;
	}



	public void setUserInfo(UserInfoByUserCreate userInfo) {
		this.userInfo = userInfo;
	}



	public String getToken() {
		return token;
	}





	public void setToken(String token) {
		this.token = token;
	}





	public String getNewPin() {
		return newPin;
	}



	public void setNewPin(String newPin) {
		this.newPin = newPin;
	}




	@Override
	public Status check() {
		
		Status status = new Status();
		
		status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);
		
		return status;
		
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
