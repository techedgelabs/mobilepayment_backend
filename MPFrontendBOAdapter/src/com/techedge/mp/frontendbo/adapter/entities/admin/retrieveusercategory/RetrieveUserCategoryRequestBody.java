package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusercategory;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RetrieveUserCategoryRequestBody implements Validable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_USER_CATEGORY_CREATE);

        return status;
    }

}
