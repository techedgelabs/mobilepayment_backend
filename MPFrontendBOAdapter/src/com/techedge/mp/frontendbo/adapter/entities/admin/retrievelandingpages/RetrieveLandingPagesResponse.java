package com.techedge.mp.frontendbo.adapter.entities.admin.retrievelandingpages;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.LandingData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class RetrieveLandingPagesResponse extends BaseResponse {

    private List<LandingData> landingPages = new ArrayList<LandingData>(0);

    public List<LandingData> getLandingPages() {
        return landingPages;
    }

    public void setLandingPages(List<LandingData> landingPages) {
        this.landingPages = landingPages;
    }

}
