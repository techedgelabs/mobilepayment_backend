package com.techedge.mp.frontendbo.adapter.entities.admin.getsurveysubmissions;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class GetSurveySubmissionsResponse extends BaseResponse {

    private GetSurveySubmissionsBodyResponse body;

    public GetSurveySubmissionsBodyResponse getBody() {
        return body;
    }

    public void setBody(GetSurveySubmissionsBodyResponse body) {
        this.body = body;
    }
}
