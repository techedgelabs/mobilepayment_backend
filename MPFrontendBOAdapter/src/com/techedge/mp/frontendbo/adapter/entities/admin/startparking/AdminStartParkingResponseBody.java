package com.techedge.mp.frontendbo.adapter.entities.admin.startparking;

import com.techedge.mp.parking.integration.business.interfaces.StartParkingResult;

public class AdminStartParkingResponseBody {

    private StartParkingResult startParkingResult;

    public StartParkingResult getStartParkingResult() {
        return startParkingResult;
    }

    public void setStartParkingResult(StartParkingResult startParkingResult) {
        this.startParkingResult = startParkingResult;
    }

}
