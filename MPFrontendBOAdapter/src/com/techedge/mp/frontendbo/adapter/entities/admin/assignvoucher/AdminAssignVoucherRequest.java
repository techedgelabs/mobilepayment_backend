package com.techedge.mp.frontendbo.adapter.entities.admin.assignvoucher;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminAssignVoucherRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private AdminAssignVoucherBodyRequest body;

    public AdminAssignVoucherRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminAssignVoucherBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminAssignVoucherBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ASSIGN-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_ASSIGN_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String adminAssignVoucherResult = getAdminServiceRemote().adminAssignVoucher(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getUserId(), this.getBody().getVoucherCode());

        AdminAssignVoucherResponse adminAssignVoucherResponse = new AdminAssignVoucherResponse();

        status.setStatusCode(adminAssignVoucherResult);
        status.setStatusMessage(prop.getProperty(adminAssignVoucherResult));
        adminAssignVoucherResponse.setStatus(status);

        return adminAssignVoucherResponse;

    }

}
