package com.techedge.mp.frontendbo.adapter.entities.admin.estimateparkingprice;

import com.techedge.mp.parking.integration.business.interfaces.EstimateParkingPriceResult;

public class AdminEstimateParkingPriceResponseBody {

    private EstimateParkingPriceResult estimateParkingPriceResult;

    public EstimateParkingPriceResult getEstimateParkingPriceResult() {
        return estimateParkingPriceResult;
    }

    public void setEstimateParkingPriceResult(EstimateParkingPriceResult estimateParkingPriceResult) {
        this.estimateParkingPriceResult = estimateParkingPriceResult;
    }

}
