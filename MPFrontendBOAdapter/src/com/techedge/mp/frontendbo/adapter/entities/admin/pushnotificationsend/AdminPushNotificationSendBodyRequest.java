package com.techedge.mp.frontendbo.adapter.entities.admin.pushnotificationsend;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminPushNotificationSendBodyRequest implements Validable {

    private Long   userID;
    private Long   messageID;
    private String titleNotification;
    private String messageNotification;

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public Long getMessageID() {
        return messageID;
    }

    public void setMessageID(Long messageID) {
        this.messageID = messageID;
    }

    public String getTitleNotification() {
        return titleNotification;
    }

    public void setTitleNotification(String titleNotification) {
        this.titleNotification = titleNotification;
    }

    public String getMessageNotification() {
        return messageNotification;
    }

    public void setMessageNotification(String messageNotification) {
        this.messageNotification = messageNotification;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (this.userID == null || this.userID == 0 || this.messageID == null || this.messageID == 0 || this.titleNotification == null || this.titleNotification.isEmpty()
                || this.titleNotification.trim().isEmpty() || this.messageNotification == null || this.messageNotification.isEmpty() || this.messageNotification.trim().isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_PUSH_NOTIFICATION_TEST_CHECK_FAILURE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_PUSH_NOTIFICATION_TEST_SUCCESS);

        return status;
    }
}
