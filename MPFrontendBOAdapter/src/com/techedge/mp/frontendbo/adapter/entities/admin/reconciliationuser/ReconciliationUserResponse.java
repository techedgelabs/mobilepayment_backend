package com.techedge.mp.frontendbo.adapter.entities.admin.reconciliationuser;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.ReconciliationResult;


public class ReconciliationUserResponse extends BaseResponse {
	
	private Integer processed;
	private List<ReconciliationResult> resultList = new ArrayList<ReconciliationResult>(0);
	
	public Integer getProcessed() {
		return processed;
	}
	public void setProcessed(Integer processed) {
		this.processed = processed;
	}
	
	public List<ReconciliationResult> getResultList() {
		return resultList;
	}
	public void setResultList(List<ReconciliationResult> resultList) {
		this.resultList = resultList;
	}
	
}