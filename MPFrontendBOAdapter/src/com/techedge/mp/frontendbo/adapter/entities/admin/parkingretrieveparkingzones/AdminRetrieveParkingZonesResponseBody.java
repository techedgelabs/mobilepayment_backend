package com.techedge.mp.frontendbo.adapter.entities.admin.parkingretrieveparkingzones;

import com.techedge.mp.parking.integration.business.interfaces.RetrieveParkingZonesResult;

public class AdminRetrieveParkingZonesResponseBody {

    private RetrieveParkingZonesResult retrieveParkingZonesResult;

    public RetrieveParkingZonesResult getRetrieveParkingZonesResult() {
        return retrieveParkingZonesResult;
    }

    public void setRetrieveParkingZonesResult(RetrieveParkingZonesResult retrieveParkingZonesResult) {
        this.retrieveParkingZonesResult = retrieveParkingZonesResult;
    }
    
}
