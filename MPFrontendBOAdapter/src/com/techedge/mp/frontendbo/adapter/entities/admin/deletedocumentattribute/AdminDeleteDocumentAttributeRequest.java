package com.techedge.mp.frontendbo.adapter.entities.admin.deletedocumentattribute;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminDeleteDocumentAttributeRequest extends AbstractBORequest implements Validable {

    private Status                                  status = new Status();

    private Credential                              credential;
    private AdminDeleteDocumentAttributeBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminDeleteDocumentAttributeBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminDeleteDocumentAttributeBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DELETE-DOCUMENT-ATTRIBUTE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_DELETE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_DELETE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String deleteDocumentAttributeResult = getAdminServiceRemote().adminDeleteDocumentAttribute(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getCheckKey());

        AdminDeleteDocumentAttributeResponse deleteDocumentAttributeResponse = new AdminDeleteDocumentAttributeResponse();
        status.setStatusCode(deleteDocumentAttributeResult);
        status.setStatusMessage(prop.getProperty(deleteDocumentAttributeResult));
        deleteDocumentAttributeResponse.setStatus(status);

        return deleteDocumentAttributeResponse;
    }

}