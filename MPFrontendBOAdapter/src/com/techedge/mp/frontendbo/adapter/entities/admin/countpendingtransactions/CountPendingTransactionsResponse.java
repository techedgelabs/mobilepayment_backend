package com.techedge.mp.frontendbo.adapter.entities.admin.countpendingtransactions;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class CountPendingTransactionsResponse extends BaseResponse{
    
    private CountPendingTransactionsResponseBody body;

    public CountPendingTransactionsResponseBody getBody() {
        return body;
    }

    public void setBody(CountPendingTransactionsResponseBody body) {
        this.body = body;
    }
}
