package com.techedge.mp.frontendbo.adapter.entities.admin.systemcheck;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.RemoteSystem;
import com.techedge.mp.core.business.interfaces.RemoteSystemType;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminSystemCheckBodyRequest implements Validable {

    private List<RemoteSystem> systems = new ArrayList<>();
    
    public AdminSystemCheckBodyRequest() {}

    public void setSystems(List<RemoteSystem> systems) {
        this.systems = systems;
    }
    
    public List<RemoteSystem> getSystems() {
        return systems;
    }

    @Override
    public Status check() {
        
        Status status = new Status();

        if (systems == null || systems.isEmpty()) {
            
            for(RemoteSystemType remoteSystemType : RemoteSystemType.values()) {
                
                RemoteSystem rs = new RemoteSystem();
                rs.setSystemId(remoteSystemType.name());
                systems.add(rs);
            }
            
            status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_SYSTEM_CHECK_SUCCESS);

        return status;
    }

}
