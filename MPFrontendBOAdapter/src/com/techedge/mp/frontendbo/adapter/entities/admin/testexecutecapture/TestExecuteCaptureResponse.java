package com.techedge.mp.frontendbo.adapter.entities.admin.testexecutecapture;

import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteCaptureResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestExecuteCaptureResponse extends BaseResponse {

    private String               statusCode;
    private ExecuteCaptureResult result;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ExecuteCaptureResult getResult() {
        return result;
    }

    public void setResult(ExecuteCaptureResult result) {
        this.result = result;
    }

}
