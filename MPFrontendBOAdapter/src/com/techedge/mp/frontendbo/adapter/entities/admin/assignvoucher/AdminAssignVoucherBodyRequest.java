package com.techedge.mp.frontendbo.adapter.entities.admin.assignvoucher;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminAssignVoucherBodyRequest implements Validable {

    private Long   userId;
    private String voucherCode;

    public AdminAssignVoucherBodyRequest() {}

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.userId == null) {
            status.setStatusCode(StatusCode.ADMIN_ASSIGN_VOUCHER_INVALID_PARAMETERS);

            return status;
        }

        if (this.voucherCode == null) {
            status.setStatusCode(StatusCode.ADMIN_ASSIGN_VOUCHER_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_ASSIGN_VOUCHER_SUCCESS);

        return status;
    }

}
