package com.techedge.mp.frontendbo.adapter.entities.admin.generatetesters;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;


public class GenerateTestersDataRequest implements Validable {
	
	private String emailPrefix;
	private String emailSuffix;
	private Integer startNumber;
	private Integer userCount;
	private String token;
	private Integer userType;
	
	
	public GenerateTestersDataRequest() {
	}


	public String getEmailPrefix() {
		return emailPrefix;
	}
	public void setEmailPrefix(String emailPrefix) {
		this.emailPrefix = emailPrefix;
	}

	public String getEmailSuffix() {
		return emailSuffix;
	}
	public void setEmailSuffix(String emailSuffix) {
		this.emailSuffix = emailSuffix;
	}

	public Integer getStartNumber() {
		return startNumber;
	}
	public void setStartNumber(Integer startNumber) {
		this.startNumber = startNumber;
	}

	public Integer getUserCount() {
		return userCount;
	}
	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}


	public Integer getUserType() {
        return userType;
    }


    public void setUserType(Integer userType) {
        this.userType = userType;
    }


    @Override
	public Status check() {
		
		Status status = new Status();
		
		status.setStatusCode(StatusCode.ADMIN_GENERATE_TESTER_USER_SUCCESS);
		
		return status;
		
	}
}
