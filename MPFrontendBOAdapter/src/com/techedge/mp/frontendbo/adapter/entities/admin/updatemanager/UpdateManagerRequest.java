package com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class UpdateManagerRequest extends AbstractBORequest implements Validable {

    private Status                 status = new Status();


	
	private Credential credential;
	private UpdateManagerBodyRequest body;

	
	public UpdateManagerRequest() {}
	
	public Credential getCredential() {
		return credential;
	}
	public void setCredential(Credential credential) {
		this.credential = credential;
	}

	public UpdateManagerBodyRequest getBody() {
		return body;
	}
	public void setBody(UpdateManagerBodyRequest body) {
		this.body = body;
	}
	

	@Override
	public Status check() {
		
		Status status = new Status();
		
		status = Validator.checkCredential("UPDATE-MANAGER", credential);
		
		if(!Validator.isValid(status.getStatusCode())) {
			
			return status;
			
		}
		
		if(this.body != null) {
			
			status = this.body.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_SUCCESS);
		
		return status;
		
	}

    @Override
    public BaseResponse execute() {
        UpdateManagerResponse updateManagerResponse = new UpdateManagerResponse();

        String response = getAdminServiceRemote().adminUpdateManager(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getManagerData().getManager());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updateManagerResponse.setStatus(status);

        return updateManagerResponse;
    }
	
}
