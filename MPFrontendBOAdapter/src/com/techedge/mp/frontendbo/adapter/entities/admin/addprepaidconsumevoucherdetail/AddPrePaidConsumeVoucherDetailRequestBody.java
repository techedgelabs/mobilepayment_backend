package com.techedge.mp.frontendbo.adapter.entities.admin.addprepaidconsumevoucherdetail;

import java.util.Date;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AddPrePaidConsumeVoucherDetailRequestBody implements Validable {

    private Double consumedValue;
    private Date   expirationDate;
    private Double initialValue;
    private String promoCode;
    private String promoDescription;
    private String promoDoc;
    private Double voucherBalanceDue;
    private String voucherCode;
    private String voucherStatus;
    private String voucherType;
    private Double voucherValue;
    private Long   prePaidConsumeVoucherId;

    public Double getConsumedValue() {
        return consumedValue;
    }

    public void setConsumedValue(Double consumedValue) {
        this.consumedValue = consumedValue;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Double initialValue) {
        this.initialValue = initialValue;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoDoc() {
        return promoDoc;
    }

    public void setPromoDoc(String promoDoc) {
        this.promoDoc = promoDoc;
    }

    public Double getVoucherBalanceDue() {
        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(Double voucherBalanceDue) {
        this.voucherBalanceDue = voucherBalanceDue;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public Double getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(Double voucherValue) {
        this.voucherValue = voucherValue;
    }

    public Long getPrePaidConsumeVoucherId() {
        return prePaidConsumeVoucherId;
    }

    public void setPrePaidConsumeVoucherId(Long prePaidConsumeVoucherId) {
        this.prePaidConsumeVoucherId = prePaidConsumeVoucherId;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_SUCCESS);

        return status;
    }
}
