package com.techedge.mp.frontendbo.adapter.entities.admin.createsurvey;

import java.util.ArrayList;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.SurveyQuestion;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class CreateSurveyBodyRequest implements Validable {

    private String                    code;
    private String                    description;
    private String                    note;
    private Long                      startDate;
    private Long                      endDate;
    private Integer                   status;
    private ArrayList<SurveyQuestion> questions;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public ArrayList<SurveyQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<SurveyQuestion> questions) {
        this.questions = questions;
    }

    public CreateSurveyBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.code == null || this.code.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }

        if (this.startDate == null) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }

        if (this.endDate == null) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }
        
        if (this.startDate > this.endDate) {

            status.setStatusCode(StatusCode.ADMIN_SURVEY_CREATE_START_DATE_ERROR);
            return status;

        }
        
        
        if (this.status == null) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }
        

        status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS);

        return status;
    }

}
