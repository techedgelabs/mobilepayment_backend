package com.techedge.mp.frontendbo.adapter.entities.admin.generatetesters;

import java.util.List;

import com.techedge.mp.core.business.interfaces.GenerateTestersData;
import com.techedge.mp.core.business.interfaces.TesterUser;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.UserConfig;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class GenerateTestersRequest extends AbstractBORequest implements Validable {

    private Status                     status = new Status();

    private Credential                 credential;
    private GenerateTestersRequestBody body;

    public GenerateTestersRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GenerateTestersRequestBody getBody() {
        return body;
    }

    public void setBody(GenerateTestersRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        GenerateTestersData generateTestersData = getAdminServiceRemote().adminTestersGenerate(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getCustomerUserData().getEmailPrefix(), this.getBody().getCustomerUserData().getEmailSuffix(),
                this.getBody().getCustomerUserData().getStartNumber(), this.getBody().getCustomerUserData().getUserCount(), 
                this.getBody().getCustomerUserData().getToken(), this.getBody().getCustomerUserData().getUserType());

        GenerateTestersResponse generateTestersResponse = new GenerateTestersResponse();

        List<TesterUser> testerList = generateTestersData.getTesterList();

        if (!testerList.isEmpty()) {

            for (TesterUser testerUser : testerList) {

                UserConfig userConfig = new UserConfig();

                userConfig.setEmail(testerUser.getEmail());
                userConfig.setPassword(testerUser.getPassword());
                userConfig.setEncryptedPassword(testerUser.getEncryptedPassword());
                userConfig.setPin(testerUser.getPin());
                userConfig.setEncryptedPin(testerUser.getEncryptedPin());
                userConfig.setGenerated(testerUser.isGenerated());
                userConfig.setResponse(testerUser.getResponse());

                generateTestersResponse.getTesters().add(userConfig);
            }
        }

        status.setStatusCode(generateTestersData.getStatusCode());
        status.setStatusMessage(prop.getProperty(generateTestersData.getStatusCode()));

        generateTestersResponse.setStatus(status);

        return generateTestersResponse;
    }

}
