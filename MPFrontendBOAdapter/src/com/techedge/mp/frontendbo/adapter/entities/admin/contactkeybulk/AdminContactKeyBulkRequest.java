package com.techedge.mp.frontendbo.adapter.entities.admin.contactkeybulk;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.techedge.mp.core.business.interfaces.crm.UpdateContactKeyBulkResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;


public class AdminContactKeyBulkRequest extends AbstractBORequest implements Validable {

	private Status status = new Status();

	private AdminContactKeyBulkBodyRequest body;

	public AdminContactKeyBulkBodyRequest getBody() {
		return body;
	}

	public void setBody(AdminContactKeyBulkBodyRequest body) {
		this.body = body;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	@Override
	public Status check() {

		Status status = new Status();

		if (this.body != null) {

			status = this.body.check();

			if (!Validator.isValid(status.getStatusCode())) {

				return status;

			}
		} else {

			status.setStatusCode(StatusCode.ADMIN_CONTACT_KEY_BULK_INVALID_REQUEST);

			return status;
		}

		status.setStatusCode(StatusCode.ADMIN_CONTACT_KEY_BULK_SUCCESS);

		return status;

	}

	@Override
	public BaseResponse execute() {

		StatusCode statusCode = null;

		AdminContactKeyBulkRequest contacBulkRequest = this;

		List<ContactKeyObj> listContactKeys = contacBulkRequest.getBody()
				.getListContactKeys();

		Map<String, String> mapContactKeys = new HashMap<>();

		for (ContactKeyObj ele : listContactKeys) {
			mapContactKeys.put(ele.getFiscalCode(), ele.getContactKey());
		}

		UpdateContactKeyBulkResult updateContactKeyResult = getCrmService()
				.insertBulkContactKey(
						contacBulkRequest.getBody().getOperationID(),
						contacBulkRequest.getBody().getRequestID(),
						mapContactKeys);

		AdminContactKeyBulkResponse resp = new AdminContactKeyBulkResponse(updateContactKeyResult.getErrorList());
		resp.setStatus(new Status(updateContactKeyResult.getStatusCode().toString(), ""));

		return resp;

	}

}
