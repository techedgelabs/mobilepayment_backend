package com.techedge.mp.frontendbo.adapter.entities.admin.testinforedemption;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestInfoRedemptionResponse extends BaseResponse implements Serializable {

    /**
     * 
     */
    private static final long      serialVersionUID = 3653913958347659131L;
    private String                 csTransactionID;

    private List<RedemptionDetail> redemptionList   = new ArrayList<RedemptionDetail>();

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String value) {
        this.csTransactionID = value;
    }

    public List<RedemptionDetail> getRedemptionList() {
        return redemptionList;
    }

    public void setRedemptionList(List<RedemptionDetail> redemptionList) {
        this.redemptionList = redemptionList;
    }

}
