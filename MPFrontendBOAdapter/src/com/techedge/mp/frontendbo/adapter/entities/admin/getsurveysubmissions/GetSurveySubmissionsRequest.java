package com.techedge.mp.frontendbo.adapter.entities.admin.getsurveysubmissions;

import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class GetSurveySubmissionsRequest implements Validable {

    private Credential                      credential;
    private GetSurveySubmissionsBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GetSurveySubmissionsBodyRequest getBody() {
        return body;
    }

    public void setBody(GetSurveySubmissionsBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-SURVEY-SUBMISSION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.ADMIN_SURVEY_GET_SUCCESS);

        return status;
    }

}
