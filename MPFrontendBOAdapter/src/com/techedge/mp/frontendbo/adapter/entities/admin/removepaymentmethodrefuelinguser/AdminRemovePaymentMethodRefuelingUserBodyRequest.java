package com.techedge.mp.frontendbo.adapter.entities.admin.removepaymentmethodrefuelinguser;

import com.techedge.mp.core.business.interfaces.PaymentMethod;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminRemovePaymentMethodRefuelingUserBodyRequest implements Validable {

    private String        username;
    private PaymentMethod paymentMethod;

    public String getUsername() {
        return username;
    }

    public void setUsername(String role) {
        this.username = role;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public AdminRemovePaymentMethodRefuelingUserBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.paymentMethod == null || (this.paymentMethod != null && (this.paymentMethod.getId() == null || this.paymentMethod.getId() == 0)) || this.username == null || this.username.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_RM_PAY_METH_REFUELING_USER_ERROR_PARAMETERS);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RM_PAY_METH_REFUELING_USER_SUCCESS);

        return status;
    }

}
