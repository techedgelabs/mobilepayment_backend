package com.techedge.mp.frontendbo.adapter.entities.common;

public class StatusCode {

    public final static String ADMIN_REQU_INVALID_REQUEST                                          = "ADMIN_REQU_400";
    public final static String ADMIN_REQU_INVALID_TICKET                                           = "ADMIN_REQU_401";
    public final static String ADMIN_REQU_JSON_SYNTAX_ERROR                                        = "ADMIN_REQU_402";
    public final static String ADMIN_REQU_UNAUTHORIZED                                             = "ADMIN_REQU_403";
    public final static String ADMIN_SYSTEM_ERROR                                                  = "ADMIN_REQU_500";

    public final static String MAIL_CREATE_SUCCESS                                                 = "MAIL_CREATE_200";
    public final static String MAIL_CREATE_FAILURE                                                 = "MAIL_CREATE_300";
    public final static String MAIL_CREATE_INVALID_TICKET                                          = "MAIL_REQU_401";
    public final static String MAIL_CREATE_MAIL_EXISTS                                             = "MAIL_CREATE_501";

    public final static String MAIL_EDIT_SUCCESS                                                   = "MAIL_EDIT_200";
    public final static String MAIL_EDIT_FAILURE                                                   = "MAIL_EDIT_300";
    public final static String MAIL_EDIT_INVALID_TICKET                                            = "MAIL_REQU_401";

    public final static String MAIL_REMOVE_SUCCESS                                                 = "MAIL_REMOVE_200";
    public final static String MAIL_REMOVE_INVALID_TICKET                                          = "MAIL_REQU_401";
    public final static String MAIL_REMOVE_MAIL_NOT_EXISTS                                         = "MAIL_REMOVE_501";

    public final static String MAIL_GET_SUCCESS                                                    = "MAIL_GET_200";
    public final static String MAIL_GET_INVALID_TICKET                                             = "MAIL_REQU_401";
    public final static String MAIL_GET_MAIL_NOT_EXISTS                                            = "MAIL_GET_501";

    public final static String ADMIN_CREATE_SUCCESS                                                = "ADMIN_CREATE_200";
    public final static String ADMIN_CREATE_FAILURE                                                = "ADMIN_CREATE_300";
    public final static String ADMIN_CREATE_EMAIL_WRONG                                            = "ADMIN_CREATE_401";
    public final static String ADMIN_CREATE_PASSWORD_WRONG                                         = "ADMIN_CREATE_402";
    public final static String ADMIN_CREATE_MASTER_DATA_WRONG                                      = "ADMIN_CREATE_403";
    public final static String ADMIN_CREATE_ADDRESS_RESIDENCE_WRONG                                = "ADMIN_CREATE_404";
    public final static String ADMIN_CREATE_ADDRESS_BILLING_WRONG                                  = "ADMIN_CREATE_405";
    public final static String ADMIN_CREATE_FIDELITY_DATA_WRONG                                    = "ADMIN_CREATE_406";
    public final static String ADMIN_CREATE_PRIVACY_DATA_WRONG                                     = "ADMIN_CREATE_407";
    public final static String ADMIN_CREATE_USERNAME_PASSWORD_EQUALS                               = "ADMIN_CREATE_408";
    public final static String ADMIN_CREATE_PASSWORD_SHORT                                         = "ADMIN_CREATE_409";
    public final static String ADMIN_CREATE_NUMBER_LESS                                            = "ADMIN_CREATE_410";
    public final static String ADMIN_CREATE_LOWER_LESS                                             = "ADMIN_CREATE_411";
    public final static String ADMIN_CREATE_UPPER_LESS                                             = "ADMIN_CREATE_412";
    public final static String ADMIN_CREATE_DATE_BIRTH_WRONG                                       = "ADMIN_CREATE_413";
    public final static String ADMIN_CREATE_DATE_WRONG                                             = "ADMIN_CREATE_414";
    public final static String ADMIN_CREATE_SYSTEM_ERROR                                           = "ADMIN_CREATE_500";
    public final static String ADMIN_CREATE_MAIL_EXIST                                             = "ADMIN_CREATE_501";

    public final static String ADMIN_AUTH_SUCCESS                                                  = "ADMIN_AUTH_200";
    public final static String ADMIN_AUTH_FAILURE                                                  = "ADMIN_AUTH_300";
    public final static String ADMIN_AUTH_EMAIL_WRONG                                              = "ADMIN_AUTH_400";
    public final static String ADMIN_AUTH_PASSWORD_WRONG                                           = "ADMIN_AUTH_401";

    public final static String ADMIN_LOGOUT_SUCCESS                                                = "ADMIN_LOGOUT_200";
    public final static String ADMIN_LOGOUT_FAILURE                                                = "ADMIN_LOGOUT_300";
    public final static String ADMIN_LOGOUT_ERROR                                                  = "ADMIN_LOGOUT_301";

    public final static String ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS                                 = "ADMIN_RETR_LOG_200";
    public final static String ADMIN_RETRIEVE_ACTIVITY_LOG_FAILURE                                 = "ADMIN_RETR_LOG_300";
    public final static String ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS                        = "ADMIN_RETR_LOG_400";

    public final static String ADMIN_RETRIEVE_USERS_SUCCESS                                        = "ADMIN_RETR_USERS_200";
    public final static String ADMIN_RETRIEVE_USERS_FAILURE                                        = "ADMIN_RETR_USERS_300";
    public final static String ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS                               = "ADMIN_RETR_USERS_400";

    public final static String ADMIN_RETRIEVE_TRANSACTIONS_SUCCESS                                 = "ADMIN_RETR_TRANSACTIONS_200";
    public final static String ADMIN_RETRIEVE_TRANSACTIONS_FAILURE                                 = "ADMIN_RETR_TRANSACTIONS_300";
    public final static String ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS                        = "ADMIN_RETR_TRANSACTIONS_400";

    public final static String ADMIN_UPDATE_USER_SUCCESS                                           = "ADMIN_UPDATE_USER_200";
    public final static String ADMIN_UPDATE_USER_FAILURE                                           = "ADMIN_UPDATE_USER_300";
    public final static String ADMIN_UPDATE_USER_ERROR_PARAMETERS                                  = "ADMIN_UPDATE_USER_400";

    public final static String ADMIN_UPDATE_PAYMENT_METHOD_SUCCESS                                 = "ADMIN_UPDATE_PAYMENT_METHOD_200";
    public final static String ADMIN_UPDATE_PAYMENT_METHOD_FAILURE                                 = "ADMIN_UPDATE_PAYMENT_METHOD_300";
    public final static String ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS                        = "ADMIN_UPDATE_PAYMENT_METHOD_400";

    public final static String ADMIN_UPDATE_TRANSACTION_SUCCESS                                    = "ADMIN_UPDATE_TRANSACTION_200";
    public final static String ADMIN_UPDATE_TRANSACTION_FAILURE                                    = "ADMIN_UPDATE_TRANSACTION_300";
    public final static String ADMIN_UPDATE_TRANSACTION_ERROR_PARAMETERS                           = "ADMIN_UPDATE_TRANSACTION_400";

    public final static String ADMIN_DELETE_USER_SUCCESS                                           = "ADMIN_DELETE_USER_200";
    public final static String ADMIN_DELETE_USER_FAILURE                                           = "ADMIN_DELETE_USER_300";
    public final static String ADMIN_DELETE_USER_ERROR_PARAMETERS                                  = "ADMIN_DELETE_USER_400";
    public final static String ADMIN_DELETE_USER_DATE_WRONG                                        = "ADMIN_CREATE_401";

    public final static String ADMIN_GENERATE_TESTER_USER_SUCCESS                                  = "ADMIN_GENERATE_TESTER_USER_200";
    public final static String ADMIN_GENERATE_TESTER_USER_FAILURE                                  = "ADMIN_GENERATE_TESTER_USER_300";
    public final static String ADMIN_GENERATE_TESTER_USER_ERROR_PARAMETERS                         = "ADMIN_GENERATE_TESTER_USER_400";

    public final static String ADMIN_DELETE_TESTER_USER_SUCCESS                                    = "ADMIN_DELETE_TESTER_USER_200";
    public final static String ADMIN_DELETE_TESTER_USER_FAILURE                                    = "ADMIN_DELETE_TESTER_USER_300";
    public final static String ADMIN_DELETE_TESTER_USER_ERROR_PARAMETERS                           = "ADMIN_DELETE_TESTER_USER_400";

    public final static String ADMIN_RETRIEVE_STATISTICS_SUCCESS                                   = "ADMIN_RETRIEVE_STATISTICS_200";
    public final static String ADMIN_RETRIEVE_STATISTICS_FAILURE                                   = "ADMIN_RETRIEVE_STATISTICS_300";
    public final static String ADMIN_RETRIEVE_STATISTICS_ERROR_PARAMETERS                          = "ADMIN_RETRIEVE_STATISTICS_400";

    public final static String ADMIN_STATION_SUCCESS                                               = "ADMIN_STATION_200";
    public final static String ADMIN_STATION_FAILURE                                               = "ADMIN_STATION_300";
    public final static String ADMIN_STATION_PARAMETERS                                            = "ADMIN_STATION_400";

    public final static String ADMIN_RETRIEVE_PARAMS_SUCCESS                                       = "ADMIN_RETRIEVE_PARAMS_200";
    public final static String ADMIN_RETRIEVE_PARAMS_FAILURE                                       = "ADMIN_RETRIEVE_PARAMS_300";
    public final static String ADMIN_RETRIEVE_PARAMS_ERROR_PARAMETERS                              = "ADMIN_RETRIEVE_PARAMS_400";

    public final static String ADMIN_UPDATE_PARAM_SUCCESS                                          = "ADMIN_UPDATE_PARAM_200";
    public final static String ADMIN_UPDATE_PARAM_FAILURE                                          = "ADMIN_UPDATE_PARAM_300";
    public final static String ADMIN_UPDATE_PARAM_ERROR_PARAMETERS                                 = "ADMIN_UPDATE_PARAM_400";

    public final static String ADMIN_UPDATE_PARAMS_SUCCESS                                         = "ADMIN_UPDATE_PARAMS_200";
    public final static String ADMIN_UPDATE_PARAMS_FAILURE                                         = "ADMIN_UPDATE_PARAMS_300";

    public final static String ADMIN_REFRESH_PARAMS_SUCCESS                                        = "ADMIN_UPDATE_PARAMS_200";
    public final static String ADMIN_REFRESH_PARAMS_FAILURE                                        = "ADMIN_UPDATE_PARAMS_300";
    public final static String ADMIN_REFRESH_PARAMS_COMMAND_ERROR                                  = "ADMIN_UPDATE_PARAMS_301";

    public final static String ADMIN_ARCHIVE_SUCCESS                                               = "ADMIN_ARCHIVE_200";
    public final static String ADMIN_ARCHIVE_FAILURE                                               = "ADMIN_ARCHIVE_300";

    public final static String ADMIN_POPARCHIVE_SUCCESS                                            = "ADMIN_POPARCHIVE_200";
    public final static String ADMIN_POPARCHIVE_FAILURE                                            = "ADMIN_POPARCHIVE_300";

    public final static String ADMIN_RECONCILIATION_SUCCESS                                        = "ADMIN_RECONCILIATION_200";
    public final static String ADMIN_RECONCILIATION_FAILURE                                        = "ADMIN_RECONCILIATION_300";

    public final static String ADMIN_TEST_SCHEDULER_SUCCESS                                        = "ADMIN_TEST_SCHEDULER_200";
    public final static String ADMIN_TEST_SCHEDULER_FAILURE                                        = "ADMIN_TEST_SCHEDULER_300";

    public final static String ADMIN_RESEND_CONFIRM_USER_EMAIL_SUCCESS                             = "ADMIN_RESEND_CONFIRM_EMAIL_200";
    public final static String ADMIN_RESEND_CONFIRM_USER_EMAIL_FAILURE                             = "ADMIN_RESEND_CONFIRM_EMAIL_300";
    public final static String ADMIN_RESEND_CONFIRM_USER_EMAIL_WRONG_USER_EMAIL                    = "ADMIN_RESEND_CONFIRM_EMAIL_400";

    public final static String ADMIN_RETRIEVE_POP_TRANSACTION_SUCCESS                              = "ADMIN_RETRIEVE_POP_TRANSACTION_200";
    public final static String ADMIN_RETRIEVE_POP_TRANSACTION_FAILURE                              = "ADMIN_RETRIEVE_POP_TRANSACTION_300";
    public final static String ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS                     = "ADMIN_RETRIEVE_POP_TRANSACTION_400";

    public final static String ADMIN_CREATE_MANAGER_SUCCESS                                        = "ADMIN_CREATE_MANAGER_200";
    public final static String ADMIN_CREATE_MANAGER_FAILURE                                        = "ADMIN_CREATE_MANAGER_300";
    public final static String ADMIN_CREATE_MANAGER_INVALID_PARAMETERS                             = "ADMIN_CREATE_MANAGER_301";
    public final static String ADMIN_CREATE_MANAGER_USERNAME_PASSWORD_EQUALS                       = "ADMIN_CREATE_MANAGER_302";
    public final static String ADMIN_CREATE_MANAGER_EMAIL_EXIST                                    = "ADMIN_CREATE_MANAGER_501";

    public final static String ADMIN_DELETE_MANAGER_SUCCESS                                        = "ADMIN_DELETE_MANAGER_200";
    public final static String ADMIN_DELETE_MANAGER_FAILURE                                        = "ADMIN_DELETE_MANAGER_300";
    public final static String ADMIN_DELETE_MANAGER_INVALID_PARAMETERS                             = "ADMIN_DELETE_MANAGER_301";

    public final static String ADMIN_UPDATE_MANAGER_SUCCESS                                        = "ADMIN_UPDATE_MANAGER_200";
    public final static String ADMIN_UPDATE_MANAGER_FAILURE                                        = "ADMIN_UPDATE_MANAGER_300";
    public final static String ADMIN_UPDATE_MANAGER_INVALID_PARAMETERS                             = "ADMIN_UPDATE_MANAGER_301";
    public final static String ADMIN_UPDATE_MANAGER_USERNAME_PASSWORD_EQUALS                       = "ADMIN_UPDATE_MANAGER_302";

    public final static String ADMIN_SEARCH_MANAGER_SUCCESS                                        = "ADMIN_SEARCH_MANAGER_200";
    public final static String ADMIN_SEARCH_MANAGER_FAILURE                                        = "ADMIN_SEARCH_MANAGER_300";
    public final static String ADMIN_SEARCH_MANAGER_INVALID_TICKET                                 = "ADMIN_REQU_401";
    public final static String ADMIN_SEARCH_MANAGER_INVALID_PARAMETERS                             = "ADMIN_SEARCH_MANAGER_301";

    public final static String ADMIN_ADDSTATION_MANAGER_SUCCESS                                    = "ADMIN_ADDSTATION_MANAGER_200";
    public final static String ADMIN_ADDSTATION_MANAGER_FAILURE                                    = "ADMIN_ADDSTATION_MANAGER_300";
    public final static String ADMIN_ADDSTATION_MANAGER_INVALID_PARAMETERS                         = "ADMIN_ADDSTATION_MANAGER_301";

    public final static String ADMIN_RETRIEVETRANSACTIONS_MANAGER_SUCCESS                          = "ADMIN_RETRIEVETRANSACTIONS_MANAGER_200";
    public final static String ADMIN_RETRIEVETRANSACTIONS_MANAGER_FAILURE                          = "ADMIN_RETRIEVETRANSACTIONS_MANAGER_300";
    public final static String ADMIN_RETRIEVETRANSACTIONS_INVALID_PARAMETERS                       = "ADMIN_RETRIEVETRANSACTIONS_MANAGER_301";
    public final static String ADMIN_RETRIEVETRANSACTIONS_INVALID_TICKET                           = "ADMIN_REQU_401";

    public final static String ADMIN_TEST_FIDELITY_SUCCESS                                         = "ADMIN_TEST_FIDELITY_200";
    public final static String ADMIN_TEST_FIDELITY_INVALID_PARAMETERS                              = "ADMIN_TEST_FIDELITY_301";

    public final static String ADMIN_ADD_VOUCHER_TO_PROMOTION_SUCCESS                              = "ADMIN_ADD_VOUCHER_TO_PROMOTION_200";
    public final static String ADMIN_ADD_VOUCHER_TO_PROMOTION_INVALID_REQUEST                      = "ADMIN_ADD_VOUCHER_TO_PROMOTION_301";
    public final static String ADMIN_ADD_VOUCHER_TO_PROMOTION_VOUCERINFO                           = "ADMIN_ADD_VOUCHER_TO_PROMOTION_302";
    public final static String ADMIN_ADD_VOUCHER_TO_PROMOTION_CODE                                 = "ADMIN_ADD_VOUCHER_TO_PROMOTION_303";

    public final static String ADMIN_SURVEY_CREATE_SUCCESS                                         = "ADMIN_SURVEY_CREATE_200";
    public final static String ADMIN_SURVEY_CREATE_FAILURE                                         = "ADMIN_SURVEY_CREATE_300";
    public final static String ADMIN_SURVEY_CREATE_START_DATE_ERROR                                = "ADMIN_SURVEY_CREATE_401";

    public final static String ADMIN_SURVEY_UPDATE_SUCCESS                                         = "ADMIN_SURVEY_UPDATE_200";
    public final static String ADMIN_SURVEY_UPDATE_FAILURE                                         = "ADMIN_SURVEY_UPDATE_300";
    public final static String ADMIN_SURVEY_UPDATE_START_DATE_ERROR                                = "ADMIN_SURVEY_UPDATE_401";

    public final static String ADMIN_SURVEY_GET_SUCCESS                                            = "ADMIN_SURVEY_GET_200";
    public final static String ADMIN_SURVEY_GET_FAILURE                                            = "ADMIN_SURVEY_GET_300";
    public final static String ADMIN_SURVEY_GET_START_DATE_ERROR                                   = "ADMIN_SURVEY_GET_301";

    public final static String ADMIN_SURVEY_RESET_SUCCESS                                          = "ADMIN_SURVEY_RESET_200";
    public final static String ADMIN_SURVEY_RESET_FAILURE                                          = "ADMIN_SURVEY_RESET_300";

    public final static String ADMIN_SURVEY_QUESTION_ADD_SUCCESS                                   = "ADMIN_SURVEY_QUESTION_ADD_200";
    public final static String ADMIN_SURVEY_QUESTION_ADD_FAILURE                                   = "ADMIN_SURVEY_QUESTION_ADD_300";

    public final static String ADMIN_SURVEY_QUESTION_DELETE_SUCCESS                                = "ADMIN_SURVEY_QUESTION_DELETE_200";
    public final static String ADMIN_SURVEY_QUESTION_DELETE_FAILURE                                = "ADMIN_SURVEY_QUESTION_DELETE_300";

    public final static String ADMIN_SURVEY_QUESTION_UPDATE_SUCCESS                                = "ADMIN_SURVEY_QUESTION_UPDATE_200";
    public final static String ADMIN_SURVEY_QUESTION_UPDATE_FAILURE                                = "ADMIN_SURVEY_QUESTION_UPDATE_300";

    public final static String ADMIN_SURVEY_SUBMISSIONS_GET_SUCCESS                                = "ADMIN_SURVEY_SUBMISSIONS_GET_200";
    public final static String ADMIN_SURVEY_SUBMISSIONS_GET_FAILURE                                = "ADMIN_SURVEY_SUBMISSIONS_GET_300";

    public final static String ADMIN_ADD_CARD_BIN_SUCCESS                                          = "ADMIN_CARD_BIN_ADD_200";
    public final static String ADMIN_ADD_CARD_BIN_FAILURE                                          = "ADMIN_CARD_BIN_ADD_300";
    public final static String ADMIN_ADD_CARD_BIN_INVALID_REQUEST                                  = "ADMIN_CARD_BIN_ADD_301";

    public final static String ADMIN_UPDATE_PROMOTION_ERROR_PARAMETERS                             = "ADMIN_UPDATE_PROMOTION_301";

    public final static String ADMIN_MAPPING_ERROR_CREATE__INVALID_TICKET                          = "ADMIN_REQU_401";
    public final static String ADMIN_MAPPING_ERROR_CREATE__FAILURE                                 = "ADMIN_MAPPING_ERROR_CREATE_300";
    public final static String ADMIN_MAPPING_ERROR_CREATE__SUCCESS                                 = "ADMIN_MAPPING_ERROR_CREATE_200";
    public final static String ADMIN_MAPPING_ERROR_CREATE__NOT_EXISTS                              = "ADMIN_MAPPING_ERROR_CREATE_302";
    public final static String ADMIN_MAPPING_ERROR_CREATE__INVALID_CODE                            = "ADMIN_MAPPING_ERROR_CREATE_301";

    public final static String ADMIN_MAPPING_ERROR_EDIT__INVALID_TICKET                            = "ADMIN_REQU_401";
    public final static String ADMIN_MAPPING_ERROR_EDIT__FAILURE                                   = "ADMIN_MAPPING_ERROR_EDIT_300";
    public final static String ADMIN_MAPPING_ERROR_EDIT__SUCCESS                                   = "ADMIN_MAPPING_ERROR_EDIT_200";
    public final static String ADMIN_MAPPING_ERROR_EDIT__NOT_EXISTS                                = "ADMIN_MAPPING_ERROR_EDIT_302";
    public final static String ADMIN_MAPPING_ERROR_EDIT__INVALID_ERROR_CODE                        = "ADMIN_MAPPING_ERROR_EDIT_301";
    public final static String ADMIN_MAPPING_ERROR_EDIT__INVALID_STATUS_CODE                       = "ADMIN_MAPPING_ERROR_EDIT_301";
    public final static String ADMIN_MAPPING_ERROR_EDIT__ERROR_CODE_NOT_FOUND                      = "ADMIN_MAPPING_ERROR_EDIT_400";

    public final static String ADMIN_MAPPING_ERROR_GET__INVALID_TICKET                             = "ADMIN_REQU_401";
    public final static String ADMIN_MAPPING_ERROR_GET__FAILURE                                    = "ADMIN_MAPPING_ERROR_GET_300";
    public final static String ADMIN_MAPPING_ERROR_GET__SUCCESS                                    = "ADMIN_MAPPING_ERROR_GET_200";
    public final static String ADMIN_MAPPING_ERROR_GET__NOT_EXISTS                                 = "ADMIN_MAPPING_ERROR_GET_302";
    public final static String ADMIN_MAPPING_ERROR_GET__INVALID_CODE                               = "ADMIN_MAPPING_ERROR_GET_301";

    public final static String ADMIN_MAPPING_ERROR_REMOVE__INVALID_TICKET                          = "ADMIN_REQU_401";
    public final static String ADMIN_MAPPING_ERROR_REMOVE__FAILURE                                 = "ADMIN_MAPPING_ERROR_REMOVE_300";
    public final static String ADMIN_MAPPING_ERROR_REMOVE__SUCCESS                                 = "ADMIN_MAPPING_ERROR_REMOVE_200";
    public final static String ADMIN_MAPPING_ERROR_REMOVE__NOT_EXISTS                              = "ADMIN_MAPPING_ERROR_REMOVE_302";
    public final static String ADMIN_MAPPING_ERROR_REMOVE__INVALID_ERROR_CODE                      = "ADMIN_MAPPING_ERROR_REMOVE_301";

    public final static String ADMIN_TEST_CONSUME_VOUCHER_SUCCESS                                  = "ADMIN_TEST_CONSUME_VOUCHER_200";
    public final static String ADMIN_TEST_CONSUME_VOUCHER_FAILURE                                  = "ADMIN_TEST_CONSUME_VOUCHER_300";
    public final static String ADMIN_TEST_CONSUME_VOUCHER_INVALID_REQUEST                          = "ADMIN_TEST_CONSUME_VOUCHER_301";

    public final static String ADMIN_TEST_REVERSE_CONSUME_VOUCHER_SUCCESS                          = "ADMIN_TEST_REVERSE_CONSUME_VOUCHER_200";
    public final static String ADMIN_TEST_REVERSE_CONSUME_VOUCHER_FAILURE                          = "ADMIN_TEST_REVERSE_CONSUME_VOUCHER_300";
    public final static String ADMIN_TEST_REVERSE_CONSUME_VOUCHER_INVALID_REQUEST                  = "ADMIN_TEST_REVERSE_CONSUME_VOUCHER_301";

    public final static String ADMIN_TEST_CHECK_VOUCHER_SUCCESS                                    = "ADMIN_TEST_CHECK_VOUCHER_200";
    public final static String ADMIN_TEST_CHECK_VOUCHER_FAILURE                                    = "ADMIN_TEST_CHECK_VOUCHER_300";
    public final static String ADMIN_TEST_CHECK_VOUCHER_INVALID_REQUEST                            = "ADMIN_TEST_CHECK_VOUCHER_301";

    public final static String ADMIN_TEST_CREATE_CONSUME_VOUCHER_SUCCESS                           = "ADMIN_TEST_CREATE_CONSUME_VOUCHER_200";
    public final static String ADMIN_TEST_CREATE_CONSUME_VOUCHER_FAILURE                           = "ADMIN_TEST_CREATE_CONSUME_VOUCHER_300";
    public final static String ADMIN_TEST_CREATE_CONSUME_VOUCHER_INVALID_REQUEST                   = "ADMIN_TEST_CREATE_CONSUME_VOUCHER_301";

    public final static String ADMIN_TEST_REVERSE_CREATE_CONSUME_VOUCHER_SUCCESS                   = "ADMIN_TEST_REVERSE_CREATE_CONSUME_VOUCHER_200";
    public final static String ADMIN_TEST_REVERSE_CREATE_CONSUME_VOUCHER_FAILURE                   = "ADMIN_TEST_REVERSE_CREATE_CONSUME_VOUCHER_300";
    public final static String ADMIN_TEST_REVERSE_CREATE_CONSUME_VOUCHER_INVALID_REQUEST           = "ADMIN_TEST_REVERSE_CREATE_CONSUME_VOUCHER_301";

    public final static String ADMIN_TEST_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS                = "ADMIN_TEST_PRE_AUTHORIZATION_CONSUME_VOUCHER_200";
    public final static String ADMIN_TEST_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE                = "ADMIN_TEST_PRE_AUTHORIZATION_CONSUME_VOUCHER_300";
    public final static String ADMIN_TEST_PRE_AUTHORIZATION_CONSUME_VOUCHER_INVALID_REQUEST        = "ADMIN_TEST_PRE_AUTHORIZATION_CONSUME_VOUCHER_301";

    public final static String ADMIN_TEST_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS         = "ADMIN_TEST_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_200";
    public final static String ADMIN_TEST_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_FAILURE         = "ADMIN_TEST_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_300";
    public final static String ADMIN_TEST_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_INVALID_REQUEST = "ADMIN_TEST_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_301";

    public final static String ADMIN_TEST_LOAD_CHECK_LOYALTY_CREDITS_TRANSACTION_SUCCESS           = "ADMIN_TEST_CHECK_LOAD_LOYALTY_CREDITS_TRANSACTION_200";
    public final static String ADMIN_TEST_LOAD_CHECK_LOYALTY_CREDITS_TRANSACTION_FAILURE           = "ADMIN_TEST_CHECK_LOAD_LOYALTY_CREDITS_TRANSACTION_300";
    public final static String ADMIN_TEST_LOAD_CHECK_LOYALTY_CREDITS_TRANSACTION_INVALID_REQUEST   = "ADMIN_TEST_CHECK_LOAD_LOYALTY_CREDITS_TRANSACTION_301";

    public final static String ADMIN_TEST_LOAD_LOYALTY_CREDITS_SUCCESS                             = "ADMIN_TEST_LOAD_LOYALTY_CREDITS_200";
    public final static String ADMIN_TEST_LOAD_LOYALTY_CREDITS_FAILURE                             = "ADMIN_TEST_LOAD_LOYALTY_CREDITS_300";
    public final static String ADMIN_TEST_LOAD_LOYALTY_CREDITS_INVALID_REQUEST                     = "ADMIN_TEST_LOAD_LOYALTY_CREDITS_301";

    public final static String ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_SUCCESS                     = "ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_200";
    public final static String ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_FAILURE                     = "ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_300";
    public final static String ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_INVALID_REQUEST             = "ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_301";

    public final static String ADMIN_TEST_ENABLE_LOYALTY_CARD_SUCCESS                              = "ADMIN_TEST_ENABLE_LOYALTY_CARD_200";
    public final static String ADMIN_TEST_ENABLE_LOYALTY_CARD_FAILURE                              = "ADMIN_TEST_ENABLE_LOYALTY_CARD_300";
    public final static String ADMIN_TEST_ENABLE_LOYALTY_CARD_INVALID_REQUEST                      = "ADMIN_TEST_ENABLE_LOYALTY_CARD_301";

    public final static String ADMIN_TEST_GET_LOYALTY_CARD_SUCCESS                                 = "ADMIN_TEST_GET_LOYALTY_CARD_200";
    public final static String ADMIN_TEST_GET_LOYALTY_CARD_FAILURE                                 = "ADMIN_TEST_GET_LOYALTY_CARD_300";
    public final static String ADMIN_TEST_GET_LOYALTY_CARD_INVALID_REQUEST                         = "ADMIN_TEST_GET_LOYALTY_CARD_301";

    public final static String ADMIN_TEST_INFO_REDEMPTION_SUCCESS                                  = "ADMIN_TEST_INFO_REDEMPTION_200";
    public final static String ADMIN_TEST_INFO_REDEMPTION_FAILURE                                  = "ADMIN_TEST_INFO_REDEMPTION_300";
    public final static String ADMIN_TEST_INFO_REDEMPTION_INVALID_REQUEST                          = "ADMIN_TEST_INFO_REDEMPTION_301";

    public final static String ADMIN_TEST_REDEMPTION_SUCCESS                                       = "ADMIN_TEST_REDEMPTION_200";
    public final static String ADMIN_TEST_REDEMPTION_FAILURE                                       = "ADMIN_TEST_REDEMPTION_300";
    public final static String ADMIN_TEST_REDEMPTION_INVALID_REQUEST                               = "ADMIN_TEST_REDEMPTION_301";

    public final static String ADMIN_USER_CATEGORY_CREATE                                          = "ADMIN_USERCATEGORY_CREATE_200";
    public final static String ADMIN_USER_CATEGORY_CREATE_FAILURE                                  = "ADMIN_USERCATEGORY_CREATE_300";
    public final static String ADMIN_USER_CATEGORY_CREATE_EXISTS                                   = "ADMIN_USERCATEGORY_CREATE_400";
    public final static String ADMIN_USER_CATEGORY_INVALID_TICKET                                  = "ADMIN_REQU_401";

    public final static String ADMIN_USER_CATEGORY_RETRIEVE                                        = "ADMIN_USERCATEGORY_RETRIEVE_200";
    public final static String ADMIN_USER_CATEGORY_RETRIEVE_FAILURE                                = "ADMIN_USERCATEGORY_RETRIEVE_300";
    public final static String ADMIN_USER_CATEGORY_RETRIEVE_EXISTS                                 = "ADMIN_USERCATEGORY_RETRIEVE_400";

    public final static String ADMIN_USER_TYPE_CREATE                                              = "ADMIN_USERTYPE_CREATE_200";
    public final static String ADMIN_USER_TYPE_CREATE_FAILURE                                      = "ADMIN_USERTYPE_CREATE_300";
    public final static String ADMIN_USER_TYPE_CREATE_EXISTS                                       = "ADMIN_USERTYPE_CREATE_400";

    public final static String ADMIN_USER_TYPE_RETRIEVE                                            = "ADMIN_USERTYPE_RETRIEVE_200";
    public final static String ADMIN_USER_TYPE_RETRIEVE_FAILURE                                    = "ADMIN_USERTYPE_RETRIEVE_300";
    public final static String ADMIN_USER_TYPE_RETRIEVE_EXISTS                                     = "ADMIN_USERTYPE_RETRIEVE_400";
    public final static String ADMIN_USER_TYPE_INVALID_TICKET                                      = "ADMIN_REQU_401";

    public final static String ADMIN_USER_TYPE_CATEGORY_UPDATE                                     = "ADMIN_USERTYPE_CATEGORY_UPDATE_200";
    public final static String ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE                             = "ADMIN_USERTYPE_CATEGORY_CREATE_300";
    public final static String ADMIN_USER_TYPE_CATEGORY_UPDATE_EXISTS                              = "ADMIN_USERTYPE_CATEGORY_CREATE_400";
    public final static String ADMIN_USER_TYPE_CATEGORY_INVALID_TICKET                             = "ADMIN_REQU_401";

    public final static String PREFIX_RETRIEVE_SUCCESS                                             = "PREFIX_RETRIEVE_200";
    public final static String PREFIX_RETRIEVE_FAILURE                                             = "PREFIX_RETRIEVE_300";
    public final static String PREFIX_RETRIEVE_INVALID_TICKET                                      = "PREFIX_RETRIEVE_400";

    public final static String PREFIX_CREATE_SUCCESS                                               = "PREFIX_CREATE_200";
    public final static String PREFIX_CREATE_FAILURE                                               = "PREFIX_CREATE_300";
    public final static String PREFIX_CREATE_INVALID_TICKET                                        = "PREFIX_CREATE_400";

    public final static String PREFIX_REMOVE_SUCCESS                                               = "PREFIX_REMOVE_200";
    public final static String PREFIX_REMOVE_FAILURE                                               = "PREFIX_REMOVE_300";
    public final static String PREFIX_REMOVE_INVALID_TICKET                                        = "PREFIX_REMOVE_400";

    public final static String ADMIN_TEST_CREATE_VOUCHER_SUCCESS                                   = "CREATE_VOUCHER_AUTH_200";
    public final static String ADMIN_TEST_CREATE_VOUCHER_FAILURE                                   = "CREATE_VOUCHER_AUTH_300";

    public final static String ADMIN_TEST_DELETE_VOUCHER_SUCCESS                                   = "DELETE_VOUCHER_AUTH_200";
    public final static String ADMIN_TEST_DELETE_VOUCHER_FAILURE                                   = "DELETE_VOUCHER_AUTH_300";

    public final static String RETRIEVE_VOUCHER_TRANSACTIONS_SUCCESS                               = "RETRIEVE_VOUCHER_TRANSACTIONS_200";
    public final static String RETRIEVE_VOUCHER_TRANSACTIONS_FAILURE                               = "RETRIEVE_VOUCHER_TRANSACTIONS_300";
    public final static String RETRIEVE_VOUCHER_INVALID_PARAMETES                                  = "RETRIEVE_VOUCHER_TRANSACTIONS_301";

    public final static String ADMIN_UPDATE_TERMS_OF_SERVICE_SUCCESS                               = "ADMIN_UPDATE_TERMS_OF_SERVICE_200";
    public final static String ADMIN_UPDATE_TERMS_OF_SERVICE_FAILURE                               = "ADMIN_UPDATE_TERMS_OF_SERVICE_300";
    public final static String ADMIN_UPDATE_TERMS_OF_SERVICE_CHECK_FAILURE                         = "ADMIN_UPDATE_TERMS_OF_SERVICE_301";
    public final static String ADMIN_UPDATE_TERMS_OF_SERVICE_INVALID_TICKET                        = "ADMIN_REQU_401";

    public final static String ADMIN_BLOCK_PERIOD_CREATE_SUCCESS                                   = "ADMIN_BLOCK_PERIOD_CREATE_200";
    public final static String ADMIN_BLOCK_PERIOD_CREATE_FAILURE                                   = "ADMIN_BLOCK_PERIOD_CREATE_300";
    public final static String ADMIN_BLOCK_PERIOD_CREATE_CHECK_FAILURE                             = "ADMIN_BLOCK_PERIOD_CREATE_301";
    public final static String ADMIN_BLOCK_PERIOD_CREATE_INVALID_TICKET                            = "ADMIN_REQU_401";
    public final static String ADMIN_BLOCK_PERIOD_CREATE_EXIST_CODE                                = "ADMIN_BLOCK_PERIOD_CREATE_302";

    public final static String ADMIN_BLOCK_PERIOD_UPDATE_SUCCESS                                   = "ADMIN_BLOCK_PERIOD_UPDATE_200";
    public final static String ADMIN_BLOCK_PERIOD_UPDATE_FAILURE                                   = "ADMIN_BLOCK_PERIOD_UPDATE_300";
    public final static String ADMIN_BLOCK_PERIOD_UPDATE_CHECK_FAILURE                             = "ADMIN_BLOCK_PERIOD_UPDATE_301";
    public final static String ADMIN_BLOCK_PERIOD_UPDATE_INVALID_TICKET                            = "ADMIN_REQU_401";
    public final static String ADMIN_BLOCK_PERIOD_UPDATE_NOT_EXIST_CODE                            = "ADMIN_BLOCK_PERIOD_UPDATE_302";

    public final static String ADMIN_BLOCK_PERIOD_DELETE_SUCCESS                                   = "ADMIN_BLOCK_PERIOD_DELETE_200";
    public final static String ADMIN_BLOCK_PERIOD_DELETE_FAILURE                                   = "ADMIN_BLOCK_PERIOD_DELETE_300";
    public final static String ADMIN_BLOCK_PERIOD_DELETE_CHECK_FAILURE                             = "ADMIN_BLOCK_PERIOD_DELETE_301";
    public final static String ADMIN_BLOCK_PERIOD_DELETE_INVALID_TICKET                            = "ADMIN_REQU_401";
    public final static String ADMIN_BLOCK_PERIOD_DELETE_NOT_EXIST_CODE                            = "ADMIN_BLOCK_PERIOD_DELETE_302";

    public final static String ADMIN_BLOCK_PERIOD_RETRIEVE_SUCCESS                                 = "ADMIN_BLOCK_PERIOD_RETRIEVE_200";
    public final static String ADMIN_BLOCK_PERIOD_RETRIEVE_FAILURE                                 = "ADMIN_BLOCK_PERIOD_RETRIEVE_300";
    public final static String ADMIN_BLOCK_PERIOD_RETRIEVE_CHECK_FAILURE                           = "ADMIN_BLOCK_PERIOD_RETRIEVE_301";
    public final static String ADMIN_BLOCK_PERIOD_RETRIEVE_INVALID_TICKET                          = "ADMIN_REQU_401";
    public final static String ADMIN_BLOCK_PERIOD_RETRIEVE_NOT_EXIST_CODE                          = "ADMIN_BLOCK_PERIOD_RETRIEVE_302";

    public final static String ADMIN_DOCUMENT_CREATE_SUCCESS                                       = "ADMIN_DOCUMENT_CREATE_200";
    public final static String ADMIN_DOCUMENT_CREATE_FAILURE                                       = "ADMIN_DOCUMENT_CREATE_300";
    public final static String ADMIN_DOCUMENT_CREATE_CHECK_FAILURE                                 = "ADMIN_DOCUMENT_CREATE_301";
    public final static String ADMIN_DOCUMENT_CREATE_INVALID_TICKET                                = "ADMIN_REQU_401";
    public final static String ADMIN_DOCUMENT_CREATE_NOT_EXIST_CODE                                = "ADMIN_DOCUMENT_CREATE_302";

    public final static String ADMIN_DOCUMENT_UPDATE_SUCCESS                                       = "ADMIN_DOCUMENT_UPDATE_200";
    public final static String ADMIN_DOCUMENT_UPDATE_FAILURE                                       = "ADMIN_DOCUMENT_UPDATE_300";
    public final static String ADMIN_DOCUMENT_UPDATE_CHECK_FAILURE                                 = "ADMIN_DOCUMENT_UPDATE_301";
    public final static String ADMIN_DOCUMENT_UPDATE_INVALID_TICKET                                = "ADMIN_REQU_401";
    public final static String ADMIN_DOCUMENT_UPDATE_NOT_EXIST_CODE                                = "ADMIN_DOCUMENT_UPDATE_302";

    public final static String ADMIN_DOCUMENT_DELETE_SUCCESS                                       = "ADMIN_DOCUMENT_DELETE_200";
    public final static String ADMIN_DOCUMENT_DELETE_FAILURE                                       = "ADMIN_DOCUMENT_DELETE_300";
    public final static String ADMIN_DOCUMENT_DELETE_CHECK_FAILURE                                 = "ADMIN_DOCUMENT_DELETE_301";
    public final static String ADMIN_DOCUMENT_DELETE_INVALID_TICKET                                = "ADMIN_REQU_401";
    public final static String ADMIN_DOCUMENT_DELETE_NOT_EXIST_CODE                                = "ADMIN_DOCUMENT_DELETE_302";

    public final static String ADMIN_DOCUMENT_RETRIEVE_SUCCESS                                     = "ADMIN_DOCUMENT_RETRIEVE_200";
    public final static String ADMIN_DOCUMENT_RETRIEVE_FAILURE                                     = "ADMIN_DOCUMENT_RETRIEVE_300";
    public final static String ADMIN_DOCUMENT_RETRIEVE_CHECK_FAILURE                               = "ADMIN_DOCUMENT_RETRIEVE_301";
    public final static String ADMIN_DOCUMENT_RETRIEVE_INVALID_TICKET                              = "ADMIN_REQU_401";
    public final static String ADMIN_DOCUMENT_RETRIEVE_NOT_EXIST_CODE                              = "ADMIN_DOCUMENT_RETRIEVE_302";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_CREATE_SUCCESS                             = "ADMIN_DOCUMENT_ATTRIBUTE_CREATE_200";
    public final static String ADMIN_DOCUMENT_ATTRIBUTE_CREATE_FAILURE                             = "ADMIN_DOCUMENT_ATTRIBUTE_CREATE_300";
    public final static String ADMIN_DOCUMENT_ATTRIBUTE_CREATE_CHECK_FAILURE                       = "ADMIN_DOCUMENT_ATTRIBUTE_CREATE_301";
    public final static String ADMIN_DOCUMENT_ATTRIBUTE_CREATE_INVALID_TICKET                      = "ADMIN_REQU_401";
    public final static String ADMIN_DOCUMENT_ATTRIBUTE_CREATE_NOT_EXIST_CODE                      = "ADMIN_DOCUMENT_ATTRIBUTE_CREATE_302";

    public final static String ADMIN_DOCUMENT_ATTRIBUTE_DELETE_SUCCESS                             = "ADMIN_DOCUMENT_ATTRIBUTE_DELETE_200";
    public final static String ADMIN_DOCUMENT_ATTRIBUTE_DELETE_FAILURE                             = "ADMIN_DOCUMENT_ATTRIBUTE_DELETE_300";
    public final static String ADMIN_DOCUMENT_ATTRIBUTE_DELETE_CHECK_FAILURE                       = "ADMIN_DOCUMENT_ATTRIBUTE_DELETE_301";
    public final static String ADMIN_DOCUMENT_ATTRIBUTE_DELETE_INVALID_TICKET                      = "ADMIN_REQU_401";
    public final static String ADMIN_DOCUMENT_ATTRIBUTE_DELETE_NOT_EXIST_CODE                      = "ADMIN_DOCUMENT_ATTRIBUTE_DELETE_302";

    public final static String ADMIN_GENERATE_PV_SUCCESS                                           = "ADMIN_GENERATE_PV_200";
    public final static String ADMIN_GENERATE_PV_FAILURE                                           = "ADMIN_GENERATE_PV_300";
    public final static String ADMIN_GENERATE_PV_INVALID_PARAMETERS                                = "ADMIN_GENERATE_PV_400";

    public final static String ADMIN_UPDATE_PASSWORD_SUCCESS                                       = "ADMIN_UPDATE_PASSWORD_200";
    public final static String ADMIN_UPDATE_PASSWORD_FAILURE                                       = "ADMIN_UPDATE_PASSWORD_300";
    public final static String ADMIN_UPDATE_PASSWORD_CHECK_FAILURE                                 = "ADMIN_UPDATE_PASSWORD_301";
    public final static String ADMIN_UPDATE_PASSWORD_INVALID_TICKET                                = "ADMIN_REQU_401";
    public final static String ADMIN_UPDATE_PASSWORD_USERNAME_PASSWORD_EQUALS                      = "ADMIN_UPDATE_PASSWORD_403";
    public final static String ADMIN_UPDATE_PASSWORD_OLD_PASSWORD_WRONG                            = "ADMIN_UPDATE_PASSWORD_402";

    public final static String ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_SUCCESS                        = "ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_200";
    public final static String ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_FAILURE                        = "ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_300";
    public final static String ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_CHECK_FAILURE                  = "ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_301";
    public final static String ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_INVALID_TICKET                 = "ADMIN_REQU_401";

    public final static String ADMIN_GENERATE_MAILING_LIST_SUCCESS                                 = "ADMIN_GENERATE_MAILING_LIST_200";
    public final static String ADMIN_GENERATE_MAILING_LIST_FAILURE                                 = "ADMIN_GENERATE_MAILING_LIST_300";
    public final static String ADMIN_GENERATE_MAILING_LIST_INVALID_PARAMETERS                      = "ADMIN_GENERATE_MAILING_LIST_400";

    public final static String ADMIN_UPDATE_MAILING_LIST_SUCCESS                                   = "ADMIN_UPDATE_MAILING_LIST_200";
    public final static String ADMIN_UPDATE_MAILING_LIST_FAILURE                                   = "ADMIN_UPDATE_MAILING_LIST_300";
    public final static String ADMIN_UPDATE_MAILING_LIST_INVALID_PARAMETERS                        = "ADMIN_UPDATE_MAILING_LIST_400";

    public final static String ADMIN_ASSIGN_VOUCHER_SUCCESS                                        = "ADMIN_ASSIGN_VOUCHER_200";
    public final static String ADMIN_ASSIGN_VOUCHER_FAILURE                                        = "ADMIN_ASSIGN_VOUCHER_300";
    public final static String ADMIN_ASSIGN_VOUCHER_INVALID_PARAMETERS                             = "ADMIN_ASSIGN_VOUCHER_400";

    public final static String ADMIN_REMOVE_TRANSACTION_EVENT_SUCCESS                              = "ADMIN_REMOVE_TRANSACTION_EVENT_200";
    public final static String ADMIN_REMOVE_TRANSACTION_EVENT_FAILURE                              = "ADMIN_REMOVE_TRANSACTION_EVENT_300";
    public final static String ADMIN_REMOVE_TRANSACTION_EVENT_INVALID_PARAMETERS                   = "ADMIN_REMOVE_TRANSACTION_EVENT_400";

    public final static String ADMIN_INSERT_USER_IN_PROMO_ONHOLD_SUCCESS                           = "ADMIN_INSERT_USER_IN_PROMO_ONHOLD_200";
    public final static String ADMIN_INSERT_USER_IN_PROMO_ONHOLD_FAILURE                           = "ADMIN_INSERT_USER_IN_PROMO_ONHOLD_300";
    public final static String ADMIN_INSERT_USER_IN_PROMO_ONHOLD_INVALID_PARAMETERS                = "ADMIN_INSERT_USER_IN_PROMO_ONHOLD_400";

    public final static String ADMIN_DELETE_VOUCHER_SUCCESS                                        = "ADMIN_DELETE_VOUCHER_200";
    public final static String ADMIN_DELETE_VOUCHER_FAILURE                                        = "ADMIN_DELETE_VOUCHER_300";
    public final static String ADMIN_DELETE_VOUCHER_INVALID_TICKET                                 = "ADMIN_REQU_401";
    public final static String ADMIN_DELETE_VOUCHER_ERROR_PARAMETER                                = "ADMIN_DELETE_VOUCHER_301";

    public final static String ADMIN_CREATE_PROVINCE_SUCCESS                                       = "ADMIN_CREATE_PROVINCE_200";
    public final static String ADMIN_CREATE_PROVINCE_FAILURE                                       = "ADMIN_CREATE_PROVINCE_300";
    public final static String ADMIN_CREATE_PROVINCE_INVALID_TICKET                                = "ADMIN_REQU_401";
    public final static String ADMIN_CREATE_PROVINCE_EXISTS                                        = "ADMIN_CREATE_PROVINCE_301";
    public final static String ADMIN_CREATE_PROVINCE_INVALID_PARAMETER                             = "ADMIN_CREATE_PROVINCE_302";

    public final static String ADMIN_REMOVE_MOBILE_PHONE_SUCCESS                                   = "ADMIN_REMOVE_MOBILE_PHONE_200";
    public final static String ADMIN_REMOVE_MOBILE_PHONE_FAILURE                                   = "ADMIN_REMOVE_MOBILE_PHONE_300";
    public final static String ADMIN_REMOVE_MOBILE_PHONE_INVALID_PARAMETERS                        = "ADMIN_REMOVE_MOBILE_PHONE_400";

    public final static String ADMIN_GFG_TRANSACTION_RECONCILIATION_SUCCESS                        = "ADMIN_GFG_TRANSACTION_RECONCILIATION_200";
    public final static String ADMIN_GFG_TRANSACTION_RECONCILIATION_FAILURE                        = "ADMIN_GFG_TRANSACTION_RECONCILIATION_300";
    public final static String ADMIN_GFG_TRANSACTION_RECONCILIATION_ERROR_PARAMETERS               = "ADMIN_GFG_TRANSACTION_RECONCILIATION_301";
    public final static String ADMIN_GFG_TRANSACTION_RECONCILIATION_INVALID_TRANSACTION_TYPE       = "ADMIN_GFG_TRANSACTION_RECONCILIATION_302";
    public final static String ADMIN_GFG_TRANSACTION_RECONCILIATION_SERVICE_FAILURE                = "ADMIN_GFG_TRANSACTION_RECONCILIATION_303";

    public final static String ADMIN_PROPAGATION_USERDATA_SUCCESS                                  = "ADMIN_PROPAGATION_USERDATA_200";
    public final static String ADMIN_PROPAGATION_USERDATA_FAILURE                                  = "ADMIN_PROPAGATION_USERDATA_300";
    public final static String ADMIN_PROPAGATION_USERDATA_INVALID_PARAMETERS                       = "ADMIN_PROPAGATION_USERDATA_400";
    public final static String ADMIN_PROPAGATION_USERDATA_EXISTS                                   = "ADMIN_PROPAGATION_USERDATA_301";

    public final static String ADMIN_CHECK_LEGACY_PWD_SUCCESS                                      = "ADMIN_CHECK_LEGACY_PWD_200";
    public final static String ADMIN_CHECK_LEGACY_PWD_FAILURE                                      = "ADMIN_CHECK_LEGACY_PWD_300";
    public final static String ADMIN_CHECK_LEGACY_PWD_INVALID_PARAMETERS                           = "ADMIN_CHECK_LEGACY_PWD_400";

    public final static String ADMIN_GENERAL_CREATE_SUCCESS                                        = "ADMIN_GENERAL_CREATE_200";
    public final static String ADMIN_GENERAL_CREATE_INVALID_BODY                                   = "ADMIN_GENERAL_CREATE_REQU_401";
    public final static String ADMIN_GENERAL_CREATE_NOT_EXISTS                                     = "ADMIN_GENERAL_CREATE_301";
    public final static String ADMIN_GENERAL_CREATE_FAILURE                                        = "ADMIN_GENERAL_CREATE_300";

    public final static String ADMIN_GENERAL_UPDATE_SUCCESS                                        = "ADMIN_GENERAL_UPDATE_200";
    public final static String ADMIN_GENERAL_UPDATE_INVALID_BODY                                   = "ADMIN_GENERAL_UPDATE_REQU_401";
    public final static String ADMIN_GENERAL_UPDATE_FAILURE                                        = "ADMIN_GENERAL_UPDATE_300";
    public final static String ADMIN_GENERAL_UPDATE_NOT_EXISTS                                     = "ADMIN_GENERAL_UPDATE_303";
    public final static String ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_SUCCESS                        = "ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_200";
    public final static String ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_FAILURE                        = "ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_300";
    public final static String ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_ERROR_PARAMETERS               = "ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_400";

    public final static String ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_SUCCESS                    = "ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_200";
    public final static String ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_FAILURE                    = "ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_300";
    public final static String ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_ERROR_PARAMETERS           = "ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_400";

    public final static String ADMIN_REMOVE_SUCCESS                                                = "ADMIN_REMOVE_200";
    public final static String ADMIN_REMOVE_INVALID_BODY                                           = "ADMIN_REMOVE_REQU_401";
    public final static String ADMIN_REMOVE_NOT_EXISTS                                             = "ADMIN_REMOVE_501";
    public final static String ADMIN_REMOVE_FAILURE                                                = "ADMIN_REMOVE_400";

    public final static String ABSTRACT_REQUEST_ERROR                                              = "ABSTRACT_REQUEST_500";

    public final static String ADMIN_ADMIN_ROLE_CREATE_SUCCESS                                     = "ADMIN_ADMIN_ROLE_CREATE_200";
    public final static String ADMIN_ADMIN_ROLE_CREATE_FAILURE                                     = "ADMIN_ADMIN_ROLE_CREATE_300";
    public final static String ADMIN_ADMIN_ROLE_CREATE_CHECK_FAILURE                               = "ADMIN_ADMIN_ROLE_CREATE_301";
    public final static String ADMIN_ADMIN_ROLE_CREATE_INVALID_TICKET                              = "ADMIN_REQU_401";
    public final static String ADMIN_ADMIN_ROLE_CREATE_NOT_EXIST_NAME                              = "ADMIN_ADMIN_ROLE_CREATE_302";

    public final static String ADMIN_ADMIN_ROLE_DELETE_SUCCESS                                     = "ADMIN_ADMIN_ROLE_DELETE_200";
    public final static String ADMIN_ADMIN_ROLE_DELETE_FAILURE                                     = "ADMIN_ADMIN_ROLE_DELETE_300";
    public final static String ADMIN_ADMIN_ROLE_DELETE_CHECK_FAILURE                               = "ADMIN_ADMIN_ROLE_DELETE_301";
    public final static String ADMIN_ADMIN_ROLE_DELETE_INVALID_TICKET                              = "ADMIN_REQU_401";
    public final static String ADMIN_ADMIN_ROLE_DELETE_NOT_EXIST_NAME                              = "ADMIN_ADMIN_ROLE_DELETE_302";

    public final static String ADMIN_ADD_ROLE_TO_ADMIN_SUCCESS                                     = "ADMIN_ADD_ROLE_TO_ADMIN_200";
    public final static String ADMIN_ADD_ROLE_TO_ADMIN_FAILURE                                     = "ADMIN_ADD_ROLE_TO_ADMIN_300";
    public final static String ADMIN_ADD_ROLE_TO_ADMIN_CHECK_FAILURE                               = "ADMIN_ADD_ROLE_TO_ADMIN_301";
    public final static String ADMIN_ADD_ROLE_TO_ADMIN_INVALID_TICKET                              = "ADMIN_REQU_401";
    public final static String ADMIN_ADD_ROLE_TO_ADMIN_NOT_EXIST_NAME                              = "ADMIN_ADD_ROLE_TO_ADMIN_302";

    public final static String ADMIN_ADMIN_ROLE_RETRIEVE_SUCCESS                                   = "ADMIN_ADMIN_ROLE_RETRIEVE_200";
    public final static String ADMIN_ADMIN_ROLE_RETRIEVE_FAILURE                                   = "ADMIN_ADMIN_ROLE_RETRIEVE_300";
    public final static String ADMIN_ADMIN_ROLE_RETRIEVE_CHECK_FAILURE                             = "ADMIN_ADMIN_ROLE_RETRIEVE_301";
    public final static String ADMIN_ADMIN_ROLE_RETRIEVE_INVALID_TICKET                            = "ADMIN_REQU_401";
    public final static String ADMIN_ADMIN_ROLE_RETRIEVE_NOT_EXIST_NAME                            = "ADMIN_ADMIN_ROLE_RETRIEVE_302";

    public final static String ADMIN_ADMIN_ROLE_REMOVE_SUCCESS                                     = "ADMIN_ADMIN_ROLE_REMOVE_200";
    public final static String ADMIN_ADMIN_ROLE_REMOVE_FAILURE                                     = "ADMIN_ADMIN_ROLE_REMOVE_300";
    public final static String ADMIN_ADMIN_ROLE_REMOVE_CHECK_FAILURE                               = "ADMIN_ADMIN_ROLE_REMOVE_301";
    public final static String ADMIN_ADMIN_ROLE_REMOVE_INVALID_TICKET                              = "ADMIN_REQU_401";
    public final static String ADMIN_ADMIN_ROLE_REMOVE_NOT_EXIST_NAME                              = "ADMIN_ADMIN_ROLE_REMOVE_302";

    public final static String ADMIN_PUSH_NOTIFICATION_TEST_SUCCESS                                = "ADMIN_PUSH_NOTIFICATION_TEST_200";
    public final static String ADMIN_PUSH_NOTIFICATION_TEST_FAILURE                                = "ADMIN_PUSH_NOTIFICATION_TEST_300";
    public final static String ADMIN_PUSH_NOTIFICATION_TEST_CHECK_FAILURE                          = "ADMIN_PUSH_NOTIFICATION_TEST_301";
    public final static String ADMIN_PUSH_NOTIFICATION_TEST_INVALID_TICKET                         = "ADMIN_REQU_401";
    public final static String ADMIN_PUSH_NOTIFICATION_TEST_NOT_EXIST_NAME                         = "ADMIN_PUSH_NOTIFICATION_TEST_302";

    public final static String ADMIN_RETRIEVE_SUCCESS                                              = "ADMIN_RETR_200";
    public final static String ADMIN_RETRIEVE_FAILURE                                              = "ADMIN_RETR_300";
    public final static String ADMIN_RETRIEVE_ERROR_PARAMETERS                                     = "ADMIN_RETR_400";

    public final static String ADMIN_UPDATE_SUCCESS                                                = "ADMIN_UPD_200";
    public final static String ADMIN_UPDATE_FAILURE                                                = "ADMIN_UPD_300";
    public final static String ADMIN_UPDATE_ERROR_PARAMETERS                                       = "ADMIN_UPD_400";

    public final static String ADMIN_DELETE_SUCCESS                                                = "ADMIN_DELETE_200";
    public final static String ADMIN_DELETE_FAILURE                                                = "ADMIN_DELETE_300";
    public final static String ADMIN_DELETE_ERROR_PARAMETERS                                       = "ADMIN_DELETE_400";

    public final static String ADMIN_CREATE_REFUELING_USER_SUCCESS                                 = "ADMIN_CREATE_REFUELING_USER_200";
    public final static String ADMIN_CREATE_REFUELING_USER_FAILURE                                 = "ADMIN_CREATE_REFUELING_USER_300";
    public final static String ADMIN_CREATE_REFUELING_USER_ERROR_PARAMETERS                        = "ADMIN_CREATE_REFUELING_USER_400";

    public final static String ADMIN_UPD_PWD_REFUELING_USER_SUCCESS                                = "ADMIN_UPD_PWD_REFUELING_USER_200";
    public final static String ADMIN_UPD_PWD_REFUELING_USER_FAILURE                                = "ADMIN_UPD_PWD_REFUELING_USER_300";
    public final static String ADMIN_UPD_PWD_REFUELING_USER_ERROR_PARAMETERS                       = "ADMIN_UPD_PWD_REFUELING_USER_400";

    public final static String ADMIN_INSERT_PAY_METH_REFUELING_USER_SUCCESS                        = "ADMIN_INSERT_PAY_METH_REFUELING_USER_200";
    public final static String ADMIN_INSERT_PAY_METH_REFUELING_USER_FAILURE                        = "ADMIN_INSERT_PAY_METH_REFUELING_USER_300";
    public final static String ADMIN_INSERT_PAY_METH_REFUELING_USER_ERROR_PARAMETERS               = "ADMIN_INSERT_PAY_METH_REFUELING_USER_400";

    public final static String ADMIN_RETR_PAY_DATA_REFUELING_USER_SUCCESS                          = "ADMIN_RETR_PAY_DATA_REFUELING_USER_200";
    public final static String ADMIN_RETR_PAY_DATA_REFUELING_USER_FAILURE                          = "ADMIN_RETR_PAY_DATA_REFUELING_USER_300";
    public final static String ADMIN_RETR_PAY_DATA_REFUELING_USER_ERROR_PARAMETERS                 = "ADMIN_RETR_PAY_DATA_REFUELING_USER_400";

    public final static String ADMIN_RM_PAY_METH_REFUELING_USER_SUCCESS                            = "ADMIN_RM_PAY_METH_REFUELING_USER_200";
    public final static String ADMIN_RM_PAY_METH_REFUELING_USER_FAILURE                            = "ADMIN_RM_PAY_METH_REFUELING_USER_300";
    public final static String ADMIN_RM_PAY_METH_REFUELING_USER_ERROR_PARAMETERS                   = "ADMIN_RM_PAY_METH_REFUELING_USER_400";
    
    public final static String ADMIN_INSERT_MULTICARD_REFUELING_USER_SUCCESS                       = "ADMIN_INSERT_MULTICARD_REFUELING_USER_200";
    public final static String ADMIN_INSERT_MULTICARD_REFUELING_USER_FAILURE                       = "ADMIN_INSERT_MULTICARD_REFUELING_USER_300";
    public final static String ADMIN_INSERT_MULTICARD_REFUELING_USER_ERROR_PARAMETERS              = "ADMIN_INSERT_MULTICARD_REFUELING_USER_400";

    public final static String ADMIN_MASSIVE_REMOVE_SUCCESS                                        = "ADMIN_MASSIVE_REMOVE_200";
    public final static String ADMIN_MASSIVE_REMOVE_INVALID_BODY                                   = "ADMIN_MASSIVE_REMOVE_REQU_401";
    public final static String ADMIN_MASSIVE_REMOVE_NOT_EXISTS                                     = "ADMIN_MASSIVE_REMOVE_501";
    public final static String ADMIN_MASSIVE_REMOVE_FAILURE                                        = "ADMIN_MASSIVE_REMOVE_400";

    public final static String ADMIN_RETRIEVE_LANDING_SUCCESS                                      = "ADMIN_RETRIEVE_LANDING_200";
    public final static String ADMIN_RETRIEVE_LANDING_FAILURE                                      = "ADMIN_RETRIEVE_LANDING_300";
    public final static String ADMIN_RETRIEVE_LANDING_ERROR_PARAMETERS                             = "ADMIN_RETRIEVE_LANDING_400";
    
    public final static String RETRIEVE_POLLING_INTERVAL_SUCCESS                                   = "RETRIEVE_POLLING_INTERVAL_200";
    public final static String RETRIEVE_POLLING_INTERVAL_FAILURE                                   = "RETRIEVE_POLLING_INTERVAL_300";
    public final static String RETRIEVE_POLLING_INTERVAL_NOT_RECOGNIZED                            = "RETRIEVE_POLLING_INTERVAL_400";

    public final static String ADMIN_UPDATE_CITIES_SUCCESS                                         = "ADMIN_UPDATE_CITIES_200";
    public final static String ADMIN_UPDATE_CITIES_FAILURE                                         = "ADMIN_UPDATE_CITIES_300";
    public final static String ADMIN_UPDATE_CITIES_INVALID_FILE                                    = "ADMIN_UPDATE_CITIES_301";
    public final static String ADMIN_UPDATE_CITIES_INVALID_PARAMETERS                              = "ADMIN_UPDATE_CITIES_302";
    public final static String ADMIN_UPDATE_CITIES_ERROR_COLUMNS_COUNT                             = "ADMIN_UPDATE_CITIES_303";
    public final static String ADMIN_UPDATE_CITIES_ERROR_DECODE                                    = "ADMIN_UPDATE_CITIES_304";
    
    public final static String ADMIN_REMOVE_TICKET_SUCCESS                                         = "ADMIN_REMOVE_TICKET_200";
    public final static String ADMIN_REMOVE_TICKET_INVALID_PARAMETERS                              = "ADMIN_REMOVE_TICKET_REQU_301";
    public final static String ADMIN_REMOVE_TICKET_FAILURE                                         = "ADMIN_REMOVE_TICKET_300";
    
    public final static String ADMIN_DWH_GET_PARTNER_LIST_SUCCESS                                  = "ADMIN_DWH_GET_PARTNER_LIST_200";
    public final static String ADMIN_DWH_GET_CATEGORY_EARN_LIST_SUCCESS                            = "ADMIN_DWH_GET_CATEGORY_EARN_LIST_200";
    public final static String ADMIN_DWH_GET_CATALOG_SUCCESS                                       = "ADMIN_DWH_GET_CATALOG_LIST_200";
    public final static String ADMIN_DWH_GET_BRAND_LIST_SUCCESS                                    = "ADMIN_DWH_GET_BRAND_LIST_200";
    
    public final static String ADMIN_SYSTEM_CHECK_SUCCESS                                          = "ADMIN_SYSTEM_CHECK_200";
    public final static String ADMIN_SYSTEM_CHECK_FAILURE                                          = "ADMIN_SYSTEM_CHECK_300";
    public final static String ADMIN_SYSTEM_CHECK_INVALID_PARAMETERS                               = "ADMIN_SYSTEM_CHECK_301";
    
    public final static String ADMIN_TEST_GET_MC_CARD_STATUS_SUCCESS                               = "ADMIN_TEST_GET_MC_CARD_STATUS_200";
    public final static String ADMIN_TEST_GET_MC_CARD_STATUS_FAILURE                               = "ADMIN_TEST_GET_MC_CARD_STATUS_300";
    public final static String ADMIN_TEST_GET_MC_CARD_STATUS_INVALID_REQUEST                       = "ADMIN_TEST_GET_MC_CARD_STATUS_301";
    
    public final static String ADMIN_TEST_DELETE_MC_CARD_REFUELING_SUCCESS                         = "ADMIN_TEST_DELETE_MC_CARD_REFUELING_200";
    public final static String ADMIN_TEST_DELETE_MC_CARD_REFUELING_FAILURE                         = "ADMIN_TEST_DELETE_MC_CARD_REFUELING_300";
    public final static String ADMIN_TEST_DELETE_MC_CARD_REFUELING_INVALID_REQUEST                 = "ADMIN_TEST_DELETE_MC_CARD_REFUELING_301";
    
    public final static String ADMIN_TEST_EXECUTE_PAYMENT_SUCCESS                                  = "ADMIN_TEST_EXECUTE_PAYMENT_200";
    public final static String ADMIN_TEST_EXECUTE_PAYMENT_FAILURE                                  = "ADMIN_TEST_EXECUTE_PAYMENT_300";
    public final static String ADMIN_TEST_EXECUTE_PAYMENT_INVALID_REQUEST                          = "ADMIN_TEST_EXECUTE_PAYMENT_301";
    
    public final static String ADMIN_TEST_EXECUTE_AUTHORIZATION_SUCCESS                            = "ADMIN_TEST_EXECUTE_AUTHORIZATION_200";
    public final static String ADMIN_TEST_EXECUTE_AUTHORIZATION_FAILURE                            = "ADMIN_TEST_EXECUTE_AUTHORIZATION_300";
    public final static String ADMIN_TEST_EXECUTE_AUTHORIZATION_INVALID_REQUEST                    = "ADMIN_TEST_EXECUTE_AUTHORIZATION_301";
    
    public final static String ADMIN_TEST_EXECUTE_CAPTURE_SUCCESS                                  = "ADMIN_TEST_EXECUTE_CAPTURE_200";
    public final static String ADMIN_TEST_EXECUTE_CAPTURE_FAILURE                                  = "ADMIN_TEST_EXECUTE_CAPTURE_300";
    public final static String ADMIN_TEST_EXECUTE_CAPTURE_INVALID_REQUEST                          = "ADMIN_TEST_EXECUTE_CAPTURE_301";
    
    public final static String ADMIN_TEST_EXECUTE_REVERSAL_SUCCESS                                 = "ADMIN_TEST_EXECUTE_REVERSAL_200";
    public final static String ADMIN_TEST_EXECUTE_REVERSAL_FAILURE                                 = "ADMIN_TEST_EXECUTE_REVERSAL_300";
    public final static String ADMIN_TEST_EXECUTE_REVERSAL_INVALID_REQUEST                         = "ADMIN_TEST_EXECUTE_REVERSAL_301";
    
    public final static String ADMIN_TEST_MC_ENJOY_CARD_AUTHORIZE_SUCCESS                          = "ADMIN_TEST_MC_ENJOY_CARD_AUTHORIZE_200";
    public final static String ADMIN_TEST_MC_ENJOY_CARD_AUTHORIZE_FAILURE                          = "ADMIN_TEST_MC_ENJOY_CARD_AUTHORIZE_300";
    public final static String ADMIN_TEST_MC_ENJOY_CARD_AUTHORIZE_INVALID_REQUEST                  = "ADMIN_TEST_MC_ENJOY_CARD_AUTHORIZE_301";
    
    public final static String ADMIN_TEST_GET_PAYMENT_STATUS_SUCCESS                               = "ADMIN_TEST_GET_PAYMENT_STATUS_200";
    public final static String ADMIN_TEST_GET_PAYMENT_STATUS_FAILURE                               = "ADMIN_TEST_GET_PAYMENT_STATUS_300";
    public final static String ADMIN_TEST_GET_PAYMENT_STATUS_INVALID_REQUEST                       = "ADMIN_TEST_GET_PAYMENT_STATUS_301";

    public final static String ADMIN_TEST_AUTHORIZATION_PLUS_SUCCESS                               = "ADMIN_TEST_AUTHORIZATION_PLUS_200";
    public final static String ADMIN_TEST_AUTHORIZATION_PLUS_FAILURE                               = "ADMIN_TEST_AUTHORIZATION_PLUS_300";
    public final static String ADMIN_TEST_AUTHORIZATION_PLUS_INVALID_REQUEST                       = "ADMIN_TEST_AUTHORIZATION_PLUS_301";
    
    public final static String ADMIN_TEST_GET_PAYMENT_TRANSACTION_DETAILS_SUCCESS                  = "ADMIN_TEST_GET_PAYMENT_TRANSACTION_DETAILS_200";
    public final static String ADMIN_TEST_GET_PAYMENT_TRANSACTION_DETAILS_FAILURE                  = "ADMIN_TEST_GET_PAYMENT_TRANSACTION_DETAILS_300";
    public final static String ADMIN_TEST_GET_PAYMENT_TRANSACTION_DETAILS_INVALID_REQUEST          = "ADMIN_TEST_GET_PAYMENT_TRANSACTION_DETAILS_301";
    
    public final static String ADMIN_CONTACT_KEY_BULK_SUCCESS                  					   = "ADMIN_CONTACT_KEY_BULK_200";
    public final static String ADMIN_CONTACT_KEY_BULK_FAILURE                  					   = "ADMIN_CONTACT_KEY_BULK_300";
    public final static String ADMIN_CONTACT_KEY_BULK_INVALID_REQUEST          					   = "ADMIN_CONTACT_KEY_BULK_301";
    
    public final static String ADMIN_TEST_CRMSF_NOTIFY_EVENT_SUCCESS                               = "ADMIN_TEST_CRMSF_NOTIFY_EVENT_200";
    public final static String ADMIN_TEST_CRMSF_NOTIFY_EVENT_FAILURE                               = "ADMIN_TEST_CRMSF_NOTIFY_EVENT_300";
    public final static String ADMIN_TEST_CRMSF_NOTIFY_EVENT_INVALID_REQUEST                       = "ADMIN_TEST_CRMSF_NOTIFY_EVENT_301";
    
    public final static String ADMIN_TEST_CRMSF_GET_MISSION_SUCCESS                                = "ADMIN_TEST_CRMSF_GET_MISSION_200";
    public final static String ADMIN_TEST_CRMSF_GET_MISSION_FAILURE                                = "ADMIN_TEST_CRMSF_GET_MISSION_300";
    public final static String ADMIN_TEST_CRMSF_GET_MISSION_INVALID_REQUEST                        = "ADMIN_TEST_CRMSF_GET_MISSION_301";
    
    public final static String ADMIN_COUNT_PENDING_TRANSACTIONS_SUCCESS                            = "ADMIN_COUNT_PENDING_TRANSACTIONS_200";
    public final static String ADMIN_COUNT_PENDING_TRANSACTIONS_FAILURE                            = "ADMIN_COUNT_PENDING_TRANSACTIONS_300";
    public final static String ADMIN_COUNT_PENDING_TRANSACTIONS_ERROR_PARAMETERS                   = "ADMIN_COUNT_PENDING_TRANSACTIONS_400";

  
    
}