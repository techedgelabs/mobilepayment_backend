package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetbrandlist;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminDwhGetBrandListResponse extends BaseResponse {

    private AdminDwhGetBrandListBodyResponse body;

    public AdminDwhGetBrandListBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminDwhGetBrandListBodyResponse body) {
        this.body = body;
    }

}