package com.techedge.mp.frontendbo.adapter.entities.admin.createprovince;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ProvinceData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminCreateProvinceRequest extends AbstractBORequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;
    private AdminCreateProvinceBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminCreateProvinceBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminCreateProvinceBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-PROVINCE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_CREATE_PROVINCE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_PROVINCE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        List<ProvinceData> provincesData = new ArrayList<ProvinceData>(0);
        for (ProvinceData item : this.getBody().getProvinces()) {
            provincesData.add(item);
        }

        String createDocumentAttributeResult = getAdminServiceRemote().adminCreateProvince(this.getCredential().getTicketID(), this.getCredential().getRequestID(), provincesData);

        AdminCreateProvinceResponse adminCreateProvinceResponse = new AdminCreateProvinceResponse();
        status.setStatusCode(createDocumentAttributeResult);
        status.setStatusMessage(prop.getProperty(createDocumentAttributeResult));
        adminCreateProvinceResponse.setStatus(status);

        return adminCreateProvinceResponse;
    }

}