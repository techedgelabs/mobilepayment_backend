package com.techedge.mp.frontendbo.adapter.entities.admin.deletevoucher;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminDeleteVoucherRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private AdminDeleteVoucherRequestBody body;

    public AdminDeleteVoucherRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminDeleteVoucherRequestBody getBody() {
        return body;
    }

    public void setBody(AdminDeleteVoucherRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DELETE-VOUCHER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DELETE_VOUCHER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String response = getAdminServiceRemote().adminDeleteVoucher(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getId());

        AdminDeleteVoucherResponse adminDeleteVoucherResponse = new AdminDeleteVoucherResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        adminDeleteVoucherResponse.setStatus(status);

        return adminDeleteVoucherResponse;
    }

}
