package com.techedge.mp.frontendbo.adapter.entities.admin.testloadloyaltycredits;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.ProductInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestLoadLoyaltyCreditsBodyRequest implements Validable {

    private String    operationID;
    private String    mpTransactionID;
    private String    stationID;
    private String    panCode;
    private String    BIN;
    private String    refuelMode;
    private String    paymentMode;
    private String    partnerType;
    private Long      requestTimestamp;
    private String    fiscalCode;
    List<ProductInfo> productList = new ArrayList<ProductInfo>(0);

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getPanCode() {
        return panCode;
    }

    public void setPanCode(String panCode) {
        this.panCode = panCode;
    }

    public String getBIN() {
        return BIN;
    }

    public void setBIN(String bIN) {
        BIN = bIN;
    }

    public String getRefuelMode() {
        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public List<ProductInfo> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductInfo> productList) {
        this.productList = productList;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }
    
    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationID == null || this.operationID.trim().equals("") || this.mpTransactionID == null || this.mpTransactionID.trim().equals("") || this.stationID == null
                || this.stationID.trim().equals("") || this.panCode == null || this.panCode.trim().equals("") || this.BIN == null || this.BIN.trim().equals("")
                || this.refuelMode == null || this.refuelMode.trim().equals("") || this.paymentMode == null || this.paymentMode.trim().equals("") || this.partnerType == null
                || this.partnerType.trim().trim().equals("") || this.requestTimestamp == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_SUCCESS);

        return status;
    }

}
