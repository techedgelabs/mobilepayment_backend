package com.techedge.mp.frontendbo.adapter.entities.admin.forcesmsnotificationstatus;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminForceSmsNotificationStatusBodyRequest implements Validable {

    private String  correlationId;
    private Integer status;

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public Status check() {

        Status status_ = new Status();

        if (status == null) {
            status_.setStatusCode(StatusCode.ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_CHECK_FAILURE);
            return status_;
        }

        status_.setStatusCode(StatusCode.ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_SUCCESS);

        return status_;
    }

}
