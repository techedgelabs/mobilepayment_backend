package com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AddStationManagerBodyRequest implements Validable {

    private Long managerID;
    private Long stationID;

    public AddStationManagerBodyRequest() {}

    public Long getManagerID() {
        return managerID;
    }

    public void setManagerID(Long managerID) {
        this.managerID = managerID;
    }

    public Long getStationID() {
        return stationID;
    }

    public void setStationID(Long stationID) {
        this.stationID = stationID;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if ((this.managerID == null || this.managerID.longValue() <= 0 ) || (this.stationID == null || this.stationID.longValue() <= 0 )) {
            status.setStatusCode(StatusCode.ADMIN_ADDSTATION_MANAGER_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_ADDSTATION_MANAGER_SUCCESS);

        return status;
    }

}
