package com.techedge.mp.frontendbo.adapter.entities.admin.testmcenjoycardauthorize;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.interfaces.McCardEnjoyAuthorizeResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PaymentMode;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestMcEnjoyCardAuthorizeRequest extends AbstractBORequest implements Validable {

    private Credential                          credential;
    private TestMcEnjoyCardAuthorizeBodyRequest body;

    public TestMcEnjoyCardAuthorizeRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestMcEnjoyCardAuthorizeBodyRequest getBody() {
        return body;
    }

    public void setBody(TestMcEnjoyCardAuthorizeBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("MC-ENJOY-CARD-AUTHORIZE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_MC_ENJOY_CARD_AUTHORIZE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestMcEnjoyCardAuthorizeResponse testMcEnjoyCardAuthorizeResponse = new TestMcEnjoyCardAuthorizeResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testMcEnjoyCardAuthorizeResponse");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testMcEnjoyCardAuthorizeResponse.getStatus().setStatusCode("-1");
        }
        else {
            McCardEnjoyAuthorizeResult mcCardEnjoyAuthorizeResult = new McCardEnjoyAuthorizeResult();

            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());
                PaymentMode paymentMode = getEnumerationValue(PaymentMode.class, this.getBody().getPaymentMode());

                mcCardEnjoyAuthorizeResult = getPaymentServiceRemote().mcCardEnjoyAuthorize(this.getBody().getOperationId(), this.getBody().getAmount(),
                        this.getBody().getCurrencyCode(), this.getBody().getMcCardDpan(), partnerType, paymentMode, this.getBody().getRequestTimestamp(),
                        this.getBody().getServerSerialNumber(), this.getBody().getUserId());
            }
            catch (Exception ex) {
                mcCardEnjoyAuthorizeResult = null;
            }

            testMcEnjoyCardAuthorizeResponse.setStatusCode("OK");
            testMcEnjoyCardAuthorizeResponse.setResult(mcCardEnjoyAuthorizeResult);
        }

        return testMcEnjoyCardAuthorizeResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
