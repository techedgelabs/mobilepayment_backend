package com.techedge.mp.frontendbo.adapter.entities.admin.gfgtransactionreconciliation;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.TransactionType;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class GFGTransactionReconciliationBodyRequest implements Validable {
	
	private String mpTransactionId;
    private String srcTransactionId;
	private String transactionType;	
	
	public String getMpTransactionId() {
		return mpTransactionId;
	}
	public void setMpTransactionId(String mpTransactionId) {
		this.mpTransactionId = mpTransactionId;
	}

    public String getSrcTransactionId() {
        return srcTransactionId;
    }
    public void setSrcTransactionId(String srcTransactionId) {
        this.srcTransactionId = srcTransactionId;
    }

	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.mpTransactionId != null && this.mpTransactionId.length() > 40) {
			
			status.setStatusCode(StatusCode.ADMIN_GFG_TRANSACTION_RECONCILIATION_ERROR_PARAMETERS);
			return status;
		}
		
		if (this.transactionType == null || this.transactionType.isEmpty()) {
            
		    status.setStatusCode(StatusCode.ADMIN_GFG_TRANSACTION_RECONCILIATION_ERROR_PARAMETERS);
            return status;
		}
		
		if(this.transactionType != null &&
			( !this.transactionType.equals(TransactionType.PRE_PAID.getCode()) &&
			  !this.transactionType.equals(TransactionType.POST_PAID.getCode()))) {
			
			status.setStatusCode(StatusCode.ADMIN_GFG_TRANSACTION_RECONCILIATION_ERROR_PARAMETERS);
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_GFG_TRANSACTION_RECONCILIATION_SUCCESS);

		return status;
	}
	
}
