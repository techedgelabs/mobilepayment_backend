package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusercategory;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.user.UserCategory;
import com.techedge.mp.core.business.interfaces.user.UserCategoryData;
import com.techedge.mp.core.business.interfaces.user.UserType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrieveUserCategoryRequest extends AbstractBORequest implements Validable {

    private Status                          status = new Status();

    private Credential                      credential;
    private RetrieveUserCategoryRequestBody body;

    public RetrieveUserCategoryRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveUserCategoryRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveUserCategoryRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-USER-CATEGORY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_USER_CATEGORY_CREATE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_USER_CATEGORY_CREATE);

        return status;

    }

    @Override
    public BaseResponse execute() {
        RetrieveUserCategoryResponse retrieveUserCategoryResponse = new RetrieveUserCategoryResponse();

        UserCategoryData response = getAdminServiceRemote().adminRetrieveUserCategory(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getName());

        status.setStatusCode(response.getStatusCode());
        status.setStatusMessage(prop.getProperty(response.getStatusCode()));

        retrieveUserCategoryResponse.setStatus(status);
        List<RetrieveUserCategoryBodyResponse> listRetrieveUserCategoryBodyResponse = new ArrayList<RetrieveUserCategoryBodyResponse>(0);
        for (UserCategory user : response.getUserCategories()) {
            RetrieveUserCategoryBodyResponse retrieveUserCategoryBodyResponse = new RetrieveUserCategoryBodyResponse();
            retrieveUserCategoryBodyResponse.setName(user.getName());
            List<Integer> codes = new ArrayList<Integer>(0);
            for (UserType type : user.getUserTypes()) {
                codes.add(type.getCode());
            }
            if (codes != null) {
                retrieveUserCategoryBodyResponse.setUserType(codes);
            }
            listRetrieveUserCategoryBodyResponse.add(retrieveUserCategoryBodyResponse);

        }
        retrieveUserCategoryResponse.setUserCategory(listRetrieveUserCategoryBodyResponse);

        return retrieveUserCategoryResponse;
    }

}
