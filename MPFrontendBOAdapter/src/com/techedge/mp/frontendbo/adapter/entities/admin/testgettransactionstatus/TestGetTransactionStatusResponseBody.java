package com.techedge.mp.frontendbo.adapter.entities.admin.testgettransactionstatus;

import com.techedge.mp.forecourt.integration.shop.interfaces.GetSrcTransactionStatusMessageResponse;

public class TestGetTransactionStatusResponseBody {

    private GetSrcTransactionStatusMessageResponse getSrcTransactionStatusMessageResponse;

    public GetSrcTransactionStatusMessageResponse getGetSrcTransactionStatusMessageResponse() {
        return getSrcTransactionStatusMessageResponse;
    }

    public void setGetSrcTransactionStatusMessageResponse(GetSrcTransactionStatusMessageResponse getSrcTransactionStatusMessageResponse) {
        this.getSrcTransactionStatusMessageResponse = getSrcTransactionStatusMessageResponse;
    }

}
