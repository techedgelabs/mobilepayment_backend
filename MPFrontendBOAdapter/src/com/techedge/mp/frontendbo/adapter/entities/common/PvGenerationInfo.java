package com.techedge.mp.frontendbo.adapter.entities.common;

public class PvGenerationInfo {

    private String stationId;
    private String statusCode;

    public PvGenerationInfo() {}

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

}
