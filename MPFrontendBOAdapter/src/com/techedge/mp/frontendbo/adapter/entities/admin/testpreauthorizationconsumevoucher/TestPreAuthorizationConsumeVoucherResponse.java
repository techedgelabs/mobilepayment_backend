package com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;


public class TestPreAuthorizationConsumeVoucherResponse extends BaseResponse{

    private String csTransactionID;

    private String marketingMsg;
    private String preAuthOperationID;
    private Double amount;

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    /**
     * @return the marketingMsg
     */
    public String getMarketingMsg() {
        return marketingMsg;
    }

    /**
     * @param marketingMsg
     *            the marketingMsg to set
     */
    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    /**
     * @return the preAuthOperationID
     */
    public String getPreAuthOperationID() {
        return preAuthOperationID;
    }

    /**
     * @param preAuthOperationID
     *            the preAuthOperationID to set
     */
    public void setPreAuthOperationID(String preAuthOperationID) {
        this.preAuthOperationID = preAuthOperationID;
    }

    /**
     * @return the amount
     */
    public Double getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(Double amount) {
        this.amount = amount;
    }

}
