package com.techedge.mp.frontendbo.adapter.entities.admin.generalremovefromid;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminGeneralRemoveRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private AdminGeneralRemoveBodyRequest body;

    public AdminGeneralRemoveRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminGeneralRemoveBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminGeneralRemoveBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REMOVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_REMOVE_MOBILE_PHONE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        AdminGeneralRemoveResponse adminGeneralRemoveResponse = new AdminGeneralRemoveResponse();

        String response = getAdminServiceRemote().adminGeneralDelete(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getType(),
                this.getBody().getId());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        adminGeneralRemoveResponse.setStatus(status);

        return adminGeneralRemoveResponse;
    }

}
