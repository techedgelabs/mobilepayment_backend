package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcategoryburnlist;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminDwhGetCategoryBurnListResponse extends BaseResponse {

    private AdminDwhGetCategoryBurnListBodyResponse body;

    public AdminDwhGetCategoryBurnListBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminDwhGetCategoryBurnListBodyResponse body) {
        this.body = body;
    }

}