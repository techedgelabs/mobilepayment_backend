package com.techedge.mp.frontendbo.adapter.entities.admin.insertuserinpromoonhold;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class InsertUserInPromoOnHoldBodyRequest implements Validable {

    private String promotionCode;
    private Long   userId;

    public InsertUserInPromoOnHoldBodyRequest() {}

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.promotionCode == null) {
            status.setStatusCode(StatusCode.ADMIN_INSERT_USER_IN_PROMO_ONHOLD_INVALID_PARAMETERS);

            return status;
        }

        if (this.userId == null) {
            status.setStatusCode(StatusCode.ADMIN_INSERT_USER_IN_PROMO_ONHOLD_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_INSERT_USER_IN_PROMO_ONHOLD_SUCCESS);

        return status;
    }

}
