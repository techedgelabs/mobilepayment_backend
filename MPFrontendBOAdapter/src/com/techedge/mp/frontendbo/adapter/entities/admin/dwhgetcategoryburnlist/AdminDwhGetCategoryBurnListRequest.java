package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcategoryburnlist;

import java.util.Date;

import com.techedge.mp.dwh.adapter.interfaces.CategoryBurnDetail;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCategoryBurnResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminDwhGetCategoryBurnListRequest extends AbstractBORequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DWH-GET-CATEGORY-EARN-LIST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DWH_GET_CATEGORY_EARN_LIST_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        Date now = new Date();

        String requestId = String.valueOf(now.getTime());
        Long requestTimestamp = now.getTime();

        DWHGetCategoryBurnResult result = getDwhAdapterServiceRemote().getCategoryBurnList(requestId, requestTimestamp);

        AdminDwhGetCategoryBurnListResponse adminDwhGetCategoryBurnListResponse = new AdminDwhGetCategoryBurnListResponse();

        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(result.getMessageCode());
        adminDwhGetCategoryBurnListResponse.setStatus(status);

        if (result.getCategoryBurnList() != null && !result.getCategoryBurnList().isEmpty()) {

            AdminDwhGetCategoryBurnListBodyResponse adminDwhGetCategoryBurnListBodyResponse = new AdminDwhGetCategoryBurnListBodyResponse();

            for (CategoryBurnDetail categoryBurnDetail : result.getCategoryBurnList()) {

                adminDwhGetCategoryBurnListBodyResponse.getCategoryBurnDetailList().add(categoryBurnDetail);
            }

            adminDwhGetCategoryBurnListResponse.setBody(adminDwhGetCategoryBurnListBodyResponse);
        }

        return adminDwhGetCategoryBurnListResponse;
    }

}