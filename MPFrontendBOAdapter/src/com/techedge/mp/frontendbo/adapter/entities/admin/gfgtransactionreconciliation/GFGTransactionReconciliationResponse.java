package com.techedge.mp.frontendbo.adapter.entities.admin.gfgtransactionreconciliation;

import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionDetail;
import com.techedge.mp.forecourt.integration.shop.interfaces.SrcTransactionDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class GFGTransactionReconciliationResponse extends BaseResponse {

    private SrcTransactionDetail popTransactionDetail;
    private TransactionDetail transactionDetail;

    public SrcTransactionDetail getPopTransactionDetail() {
        return popTransactionDetail;
    }

    public void setPopTransactionDetail(SrcTransactionDetail popTransactionDetail) {
        this.popTransactionDetail = popTransactionDetail;
    }

    public TransactionDetail getTransactionDetail() {
        return transactionDetail;
    }

    public void setTransactionDetail(TransactionDetail transactionDetail) {
        this.transactionDetail = transactionDetail;
    }

}
