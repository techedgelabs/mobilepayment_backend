package com.techedge.mp.frontendbo.adapter.entities.admin.deletemanager;

import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.frontendbo.adapter.entities.common.ManagerData;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class DeleteManagerDataRequest extends ManagerData implements Validable {

    private Long id; 
    
    public DeleteManagerDataRequest() {}

    public Long getId() {
        return id;
    }
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Manager getManager()
    {
        Manager manager = super.getManager();
        manager.setId(this.id);
        
        return manager;
    }

    @Override
    public Status check() {
        
        Status status = new Status();
        
        if(this.id == null) {
            
            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;
        }
        
        status.setStatusCode(StatusCode.ADMIN_DELETE_MANAGER_SUCCESS);

        return status;
    }


}
