package com.techedge.mp.frontendbo.adapter.entities.admin.endparking;

import com.techedge.mp.parking.integration.business.interfaces.EndParkingResult;

public class AdminEndParkingResponseBody {

    private EndParkingResult endParkingResult;

    public EndParkingResult getEndParkingResult() {
        return endParkingResult;
    }

    public void setEndParkingResult(EndParkingResult endParkingResult) {
        this.endParkingResult = endParkingResult;
    }

}
