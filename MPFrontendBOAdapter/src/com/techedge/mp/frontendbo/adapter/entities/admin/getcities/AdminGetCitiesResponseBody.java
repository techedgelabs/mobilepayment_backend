package com.techedge.mp.frontendbo.adapter.entities.admin.getcities;

import com.techedge.mp.parking.integration.business.interfaces.GetCitiesResult;

public class AdminGetCitiesResponseBody {

    private GetCitiesResult getCitiesResult;

    public GetCitiesResult getGetCitiesResult() {
        return getCitiesResult;
    }

    public void setGetCitiesResult(GetCitiesResult getCitiesResult) {
        this.getCitiesResult = getCitiesResult;
    }

}
