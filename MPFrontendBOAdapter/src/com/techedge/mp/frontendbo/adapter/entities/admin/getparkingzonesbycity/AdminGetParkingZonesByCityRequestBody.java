package com.techedge.mp.frontendbo.adapter.entities.admin.getparkingzonesbycity;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminGetParkingZonesByCityRequestBody implements Validable {

    private String lang;
    private String cityId;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.lang == null || this.cityId == null || this.lang.isEmpty() || this.cityId.isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);
        return status;
    }

}
