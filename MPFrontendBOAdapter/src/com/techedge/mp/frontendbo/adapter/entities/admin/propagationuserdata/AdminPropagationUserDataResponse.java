package com.techedge.mp.frontendbo.adapter.entities.admin.propagationuserdata;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.AdminPropagationUserDataResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminPropagationUserDataResponse extends BaseResponse {

    private List<AdminPropagationUserDataResult> results = new ArrayList<AdminPropagationUserDataResult>();
    
    public List<AdminPropagationUserDataResult> getResults() {
        return results;
    }
    
    public void setResults(List<AdminPropagationUserDataResult> results) {
        this.results = results;
    }
}