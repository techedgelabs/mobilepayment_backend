package com.techedge.mp.frontendbo.adapter.entities.admin.authentication;

import com.techedge.mp.core.business.interfaces.AdminAuthenticationResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AuthenticationAdminRequest extends AbstractBORequest implements Validable {

    private Status                         status = new Status();

    private AuthenticationAdminRequestBody body;

    public AuthenticationAdminRequest() {}

    public AuthenticationAdminRequestBody getBody() {
        return body;
    }

    public void setBody(AuthenticationAdminRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        AdminAuthenticationResponse adminAuthenticationResponse = getAdminServiceRemote().adminAuthentication(this.getBody().getUsername(), this.getBody().getPassword(),
                this.getBody().getRequestID());

        if (adminAuthenticationResponse.getStatusCode().equals(StatusCode.ADMIN_AUTH_SUCCESS)) {

            AuthenticationAdminResponseSuccess authenticationAdminResponseSuccess = new AuthenticationAdminResponseSuccess();

            status.setStatusCode(adminAuthenticationResponse.getStatusCode());
            status.setStatusMessage(prop.getProperty(adminAuthenticationResponse.getStatusCode()));
            authenticationAdminResponseSuccess.setStatus(status);

            AuthenticationAdminDataResponse adminData = new AuthenticationAdminDataResponse();
            adminData.setEmail(adminAuthenticationResponse.getAdmin().getEmail());
            adminData.setFirstname(adminAuthenticationResponse.getAdmin().getFirstName());
            adminData.setLastname(adminAuthenticationResponse.getAdmin().getLastName());
            adminData.setStatus(adminAuthenticationResponse.getAdmin().getStatus());
            adminData.setRoles(adminAuthenticationResponse.getAdmin().getRoles());

            AuthenticationAdminResponseBody body = new AuthenticationAdminResponseBody();
            body.setAdminData(adminData);
            body.setTicketID(adminAuthenticationResponse.getTicketId());

            authenticationAdminResponseSuccess.setBody(body);

            return authenticationAdminResponseSuccess;
        }
        else {

            AuthenticationAdminResponseFailure authenticationAdminResponseFailure = new AuthenticationAdminResponseFailure();

            status.setStatusCode(adminAuthenticationResponse.getStatusCode());
            status.setStatusMessage(prop.getProperty(adminAuthenticationResponse.getStatusCode()));

            authenticationAdminResponseFailure.setStatus(status);

            return authenticationAdminResponseFailure;
        }
    }

}
