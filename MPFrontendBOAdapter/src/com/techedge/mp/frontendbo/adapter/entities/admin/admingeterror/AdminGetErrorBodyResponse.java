package com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror;

public class AdminGetErrorBodyResponse {
    private String errorCode;
    private String statusCode;
    public String getErrorCode() {
        return errorCode;
    }
    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
    public String getStatusCode() {
        return statusCode;
    }
    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }
    
    
}
