package com.techedge.mp.frontendbo.adapter.entities.admin.createusertype;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class CreateUserTypeBodyRequest implements Validable {

    private Integer code;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public CreateUserTypeBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.code == null) {

            status.setStatusCode(StatusCode.ADMIN_USER_TYPE_CREATE_FAILURE);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_USER_TYPE_CREATE);

        return status;
    }

}
