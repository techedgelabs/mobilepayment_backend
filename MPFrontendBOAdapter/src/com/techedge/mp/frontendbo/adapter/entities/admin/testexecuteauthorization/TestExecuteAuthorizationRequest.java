package com.techedge.mp.frontendbo.adapter.entities.admin.testexecuteauthorization;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteAuthorizationResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestExecuteAuthorizationRequest extends AbstractBORequest implements Validable {

    private Credential                          credential;
    private TestExecuteAuthorizationBodyRequest body;

    public TestExecuteAuthorizationRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestExecuteAuthorizationBodyRequest getBody() {
        return body;
    }

    public void setBody(TestExecuteAuthorizationBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("EXECUTE-AUTHORIZATION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_EXECUTE_AUTHORIZATION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestExecuteAuthorizationResponse testExecuteAuthorizationResponse = new TestExecuteAuthorizationResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testExecuteAuthorization");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testExecuteAuthorizationResponse.getStatus().setStatusCode("-1");
        }
        else {
            ExecuteAuthorizationResult executeAuthorizationResult = new ExecuteAuthorizationResult();

            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                executeAuthorizationResult = getPaymentServiceRemote().executeAuthorization(this.getBody().getOperationId(), this.getBody().getAmount(),
                        this.getBody().getAuthCryptogram(), this.getBody().getCurrencyCode(), this.getBody().getMcCardDpan(), this.getBody().getShopCode(), partnerType,
                        this.getBody().getRequestTimestamp());
            }
            catch (Exception ex) {
                executeAuthorizationResult = null;
            }

            testExecuteAuthorizationResponse.setStatusCode("OK");
            testExecuteAuthorizationResponse.setResult(executeAuthorizationResult);
        }

        return testExecuteAuthorizationResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
