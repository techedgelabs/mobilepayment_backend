package com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.RetrieveVoucherTransactionListData;
import com.techedge.mp.core.business.interfaces.voucher.VoucherTransaction;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherTransactionInfo;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrieveVoucherTransactionsRequest extends AbstractBORequest implements Validable {

    private Status                                 status = new Status();

    private Credential                             credential;
    private RetrieveVoucherTransactionsRequestBody body;

    public RetrieveVoucherTransactionsRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveVoucherTransactionsRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveVoucherTransactionsRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-VOUCHER-TRANSACTIONS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.RETRIEVE_VOUCHER_TRANSACTIONS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        RetrieveVoucherTransactionsResponse retrieveVoucherTransactionsResponse = new RetrieveVoucherTransactionsResponse();

        Timestamp start = null;
        Timestamp end = null;

        if (this.getBody().getCreationTimestampStart() != null) {
            start = new Timestamp(this.getBody().getCreationTimestampStart());
        }
        if (this.getBody().getCreationTimestampEnd() != null) {
            end = new Timestamp(this.getBody().getCreationTimestampEnd());
        }

        RetrieveVoucherTransactionListData response = getAdminServiceRemote().adminRetrieveVoucherTransactions(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), this.getBody().getVoucherTransactionId(), this.getBody().getUserId(), this.getBody().getVoucherCode(), start, end,
                this.getBody().getFinalStatusType());

        status.setStatusCode(response.getStatusCode());
        status.setStatusMessage(prop.getProperty(response.getStatusCode()));

        List<VoucherTransactionInfo> voucherTransactionInfoList = new ArrayList<VoucherTransactionInfo>(0);

        if (response.getVoucherTransactionList() != null) {
            for (VoucherTransaction item : response.getVoucherTransactionList()) {
                VoucherTransactionInfo info = new VoucherTransactionInfo();
                info = info.toVoucherTransactionInfo(item);
                voucherTransactionInfoList.add(info);
            }
        }

        retrieveVoucherTransactionsResponse.setVoucherTransactions(voucherTransactionInfoList);
        retrieveVoucherTransactionsResponse.setStatus(status);

        return retrieveVoucherTransactionsResponse;
    }

}
