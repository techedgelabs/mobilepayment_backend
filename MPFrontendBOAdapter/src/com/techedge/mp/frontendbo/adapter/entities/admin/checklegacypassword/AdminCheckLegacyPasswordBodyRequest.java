package com.techedge.mp.frontendbo.adapter.entities.admin.checklegacypassword;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminCheckLegacyPasswordBodyRequest implements Validable {

    private String email;
    private String password;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Status check() {
        Status status = new Status();
        if (this.email == null || this.password == null || (this.email != null && this.email.isEmpty()) || (this.password != null && this.password.isEmpty())) {
            status.setStatusCode(StatusCode.ADMIN_CHECK_LEGACY_PWD_INVALID_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_CHECK_LEGACY_PWD_SUCCESS);

        return status;
    }

}
