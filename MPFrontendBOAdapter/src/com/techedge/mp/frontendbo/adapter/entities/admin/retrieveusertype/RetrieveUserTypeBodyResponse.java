package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusertype;

import java.util.List;

public class RetrieveUserTypeBodyResponse {

    private Integer      code;

    private List<String> userCategory;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public List<String> getUserCategory() {
        return userCategory;
    }

    public void setUserCategory(List<String> userCategory) {
        this.userCategory = userCategory;
    }

}
