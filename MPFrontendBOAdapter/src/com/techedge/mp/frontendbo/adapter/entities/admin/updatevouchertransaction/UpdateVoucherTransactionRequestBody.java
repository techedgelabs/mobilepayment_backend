package com.techedge.mp.frontendbo.adapter.entities.admin.updatevouchertransaction;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdateVoucherTransactionRequestBody implements Validable {

    private String voucherTransactionId;
    private String finalStatusType;

    public String getVoucherTransactionId() {
        return voucherTransactionId;
    }

    public void setVoucherTransactionId(String voucherTransactionId) {
        this.voucherTransactionId = voucherTransactionId;
    }

    public String getFinalStatusType() {
        return finalStatusType;
    }

    public void setFinalStatusType(String finalStatusType) {
        this.finalStatusType = finalStatusType;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.voucherTransactionId == null) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_ERROR_PARAMETERS);
            return status;
        }
        else {

            if (this.voucherTransactionId.length() > 40) {

                status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_ERROR_PARAMETERS);
                return status;
            }
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_SUCCESS);

        return status;
    }

}
