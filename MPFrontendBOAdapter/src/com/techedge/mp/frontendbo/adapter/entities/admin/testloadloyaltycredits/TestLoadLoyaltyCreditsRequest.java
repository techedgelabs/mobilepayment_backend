package com.techedge.mp.frontendbo.adapter.entities.admin.testloadloyaltycredits;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.LoadLoyaltyCreditsResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.ProductInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestLoadLoyaltyCreditsRequest extends AbstractBORequest implements Validable {

    private Credential                        credential;
    private TestLoadLoyaltyCreditsBodyRequest body;

    public TestLoadLoyaltyCreditsRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestLoadLoyaltyCreditsBodyRequest getBody() {
        return body;
    }

    public void setBody(TestLoadLoyaltyCreditsBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("LOAD-LOYALTY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestLoadLoyaltyCreditsResponse testLoadLoyaltyCreditsResponse = new TestLoadLoyaltyCreditsResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testLoadLoyaltyCredits");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testLoadLoyaltyCreditsResponse.getStatus().setStatusCode("-1");
        }
        else {

            List<ProductDetail> productList = new ArrayList<ProductDetail>();

            for (ProductInfo productInfo : this.getBody().getProductList()) {

                ProductDetail productDetail = new ProductDetail();

                productDetail.setAmount(productInfo.getAmount());
                productDetail.setProductCode(productInfo.getProductCode());
                productDetail.setQuantity(productInfo.getQuantity());

                productList.add(productDetail);
            }

            LoadLoyaltyCreditsResult loadLoyaltyCreditsResult = new LoadLoyaltyCreditsResult();
            String operationID = this.getBody().getOperationID();
            String mpTransactionID = this.getBody().getMpTransactionID();
            String panCode = this.getBody().getPanCode();
            String BIN = this.getBody().getBIN();
            String stationID = this.getBody().getStationID();
            String refuelMode = this.getBody().getRefuelMode();
            String paymentMode = this.getBody().getPaymentMode();
            PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());
            Long requestTimestamp = this.getBody().getRequestTimestamp();
            String fiscalCode = this.getBody().getFiscalCode();

            try {
                loadLoyaltyCreditsResult = getFidelityServiceRemote().loadLoyaltyCredits(operationID, mpTransactionID, stationID, panCode, BIN, refuelMode, paymentMode, "it",
                        partnerType, requestTimestamp, fiscalCode, productList);
            }
            catch (FidelityServiceException ex) {
                loadLoyaltyCreditsResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!loadLoyaltyCreditsResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                testLoadLoyaltyCreditsResponse.setCsTransactionID(loadLoyaltyCreditsResult.getCsTransactionID());
                testLoadLoyaltyCreditsResponse.getStatus().setStatusMessage(loadLoyaltyCreditsResult.getMessageCode());
                testLoadLoyaltyCreditsResponse.getStatus().setStatusCode(loadLoyaltyCreditsResult.getStatusCode());
                testLoadLoyaltyCreditsResponse.setCredits(loadLoyaltyCreditsResult.getCredits());
                testLoadLoyaltyCreditsResponse.setBalance(loadLoyaltyCreditsResult.getBalance());
                testLoadLoyaltyCreditsResponse.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                testLoadLoyaltyCreditsResponse.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                testLoadLoyaltyCreditsResponse.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                testLoadLoyaltyCreditsResponse.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                testLoadLoyaltyCreditsResponse.setCardType(loadLoyaltyCreditsResult.getCardType());
                testLoadLoyaltyCreditsResponse.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                testLoadLoyaltyCreditsResponse.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
            }
        }

        return testLoadLoyaltyCreditsResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
