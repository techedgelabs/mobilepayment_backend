package com.techedge.mp.frontendbo.adapter.entities.admin.testauthorizationplus;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestAuthorizationPlusDataRequest implements Validable {

	private String accessToken;
	
	

	public String getAccessToken() {
		return accessToken;
	}



	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}



	@Override
    public Status check() {

        Status status = new Status();

        if (this.accessToken == null || (this.accessToken != null && this.accessToken.isEmpty())) {

            status.setStatusCode(StatusCode.ADMIN_TEST_AUTHORIZATION_PLUS_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_AUTHORIZATION_PLUS_SUCCESS);

        return status;
    }

}
