package com.techedge.mp.frontendbo.adapter.entities.admin.testconsumevoucher;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.ProductInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherCodeInfo;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestConsumeVoucherBodyRequest implements Validable {

    private String        operationID;
    private String        mpTransactionID;
    private String        voucherType;
    private String        stationID;
    private String        refuelMode;
    private String        paymentMode;
    private String        partnerType;
    private Long          requestTimestamp;
    private String        consumeType;
    private String        preAuthOperationID;
    List<ProductInfo>     productList = new ArrayList<ProductInfo>(0);
    List<VoucherCodeInfo> voucherList = new ArrayList<VoucherCodeInfo>(0);

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public String getStationID() {
        return stationID;
    }

    public void setStationID(String stationID) {
        this.stationID = stationID;
    }

    public String getRefuelMode() {
        return refuelMode;
    }

    public void setRefuelMode(String refuelMode) {
        this.refuelMode = refuelMode;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public List<ProductInfo> getProductList() {
        return productList;
    }

    public void setProductList(List<ProductInfo> productList) {
        this.productList = productList;
    }

    public String getConsumeType() {
        return consumeType;
    }

    public void setConsumeType(String consumeType) {
        this.consumeType = consumeType;
    }

    public String getPreAuthOperationID() {
        return preAuthOperationID;
    }

    public void setPreAuthOperationID(String preAuthOperationID) {
        this.preAuthOperationID = preAuthOperationID;
    }

    public List<VoucherCodeInfo> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<VoucherCodeInfo> voucherList) {
        this.voucherList = voucherList;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationID == null || this.operationID.trim().isEmpty() || this.mpTransactionID == null || this.mpTransactionID.trim().isEmpty() || this.voucherType == null
                || this.voucherType.trim().isEmpty() || this.partnerType == null || this.partnerType.trim().isEmpty() || this.consumeType == null
                || this.consumeType.trim().isEmpty() || this.stationID == null || this.stationID.trim().isEmpty() || this.refuelMode == null || this.refuelMode.trim().isEmpty() || this.paymentMode == null || this.paymentMode.trim().isEmpty()
                || this.requestTimestamp == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_FIDELITY_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_CONSUME_VOUCHER_SUCCESS);

        return status;
    }

}
