package com.techedge.mp.frontendbo.adapter.entities.admin.removeemaildomain;

import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class GetEmailDomainResponse extends BaseResponse {
    
    private List<String> listaEmail;
    
    public List<String> getListaEmail() {
        return listaEmail;
    }

    public void setListaEmail(List<String> listaEmail) {
        this.listaEmail = listaEmail;
    }

    public GetEmailDomainResponse() {}
    
}
