package com.techedge.mp.frontendbo.adapter.entities.admin.checklegacypassword;

import com.techedge.mp.dwh.adapter.interfaces.DWHAdapterResult;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatetransaction.UpdateTransactionResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminCheckLegacyPasswordRequest extends AbstractBORequest implements Validable {

    private Status                              status = new Status();

    private Credential                          credential;
    private AdminCheckLegacyPasswordBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminCheckLegacyPasswordBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminCheckLegacyPasswordBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CHECK-LEGACY-PASSWORD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_CHECK_LEGACY_PWD_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CHECK_LEGACY_PWD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        DWHAdapterResult response = getDwhAdapterServiceRemote().checkLegacyPasswordPlus(this.getBody().getEmail(), this.getBody().getPassword(),
                this.getCredential().getRequestID());

        UpdateTransactionResponse updateTransactionResponse = new UpdateTransactionResponse();

        status.setStatusCode(response.getStatusCode());
        status.setStatusMessage(response.getMessageCode());

        updateTransactionResponse.setStatus(status);

        return updateTransactionResponse;
    }

}