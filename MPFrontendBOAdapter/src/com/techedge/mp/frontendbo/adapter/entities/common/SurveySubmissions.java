package com.techedge.mp.frontendbo.adapter.entities.common;

import java.util.ArrayList;

public class SurveySubmissions {
    
    private ArrayList<SurveyQuestionAnswers> questions = new ArrayList<SurveyQuestionAnswers>(0);

    public ArrayList<SurveyQuestionAnswers> getQuestions() {
        return questions;
    }

    public void setQuestions(ArrayList<SurveyQuestionAnswers> questions) {
        this.questions = questions;
    }
    
    
}
