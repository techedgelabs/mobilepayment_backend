package com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethod;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class UpdatePaymentMethodRequest extends AbstractBORequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;
    private UpdatePaymentMethodRequestBody body;

    public UpdatePaymentMethodRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdatePaymentMethodRequestBody getBody() {
        return body;
    }

    public void setBody(UpdatePaymentMethodRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-PAYMENTMETHOD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String response = getAdminServiceRemote().adminPaymentMethodUpdate(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getId(),
                this.getBody().getType(), this.getBody().getStatus(), this.getBody().getDefaultMethod(), this.getBody().getAttemptsLeft(), this.getBody().getCheckAmount());

        UpdatePaymentMethodResponse updatePaymentMethodResponse = new UpdatePaymentMethodResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updatePaymentMethodResponse.setStatus(status);

        return updatePaymentMethodResponse;
    }

}
