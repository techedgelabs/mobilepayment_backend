package com.techedge.mp.frontendbo.adapter.entities.common;


public class ParamUpdateResult {
	
	private String param;
	private String statusCode;

    public ParamUpdateResult(){}

	public String getParam() {
		return param;
	}
	public void setParam(String param) {
		this.param = param;
	}

	public String getStatusCode() {
		return statusCode;
	}
	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}
}
