package com.techedge.mp.frontendbo.adapter.entities.admin.updatepromotion;

import java.util.Date;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class UpdatePromotionRequest extends AbstractBORequest implements Validable {

    private Status                     status = new Status();

    private Credential                 credential;
    private UpdatePromotionRequestBody body;

    public UpdatePromotionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdatePromotionRequestBody getBody() {
        return body;
    }

    public void setBody(UpdatePromotionRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        UpdatePromotionResponse updatePromotionResponse = new UpdatePromotionResponse();

        Long startDate = this.getBody().getStartData();
        Long endDate = this.getBody().getEndData();

        Date parsedStartDate = new Date(startDate);
        Date parsedEndDate = new Date(endDate);

        String response = getAdminServiceRemote().adminUpdatePromotion(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getCode(),
                this.getBody().getDescription(), this.getBody().getStatus(), parsedStartDate, parsedEndDate);

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updatePromotionResponse.setStatus(status);

        return updatePromotionResponse;
    }

}
