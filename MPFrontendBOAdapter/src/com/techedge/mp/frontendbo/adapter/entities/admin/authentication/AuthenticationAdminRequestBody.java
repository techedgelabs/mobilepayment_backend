package com.techedge.mp.frontendbo.adapter.entities.admin.authentication;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AuthenticationAdminRequestBody implements Validable {
	

	private String username;
	private String password;
	private String requestID;
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRequestID() {
		return requestID;
	}
	public void setRequestID(String requestID) {
		this.requestID = requestID;
	}

	
	@Override
	public Status check() {
		
		Status status = new Status();
		
		
		if(this.username == null || this.username.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_AUTH_EMAIL_WRONG);
			
			return status;
			
		}

		
		if(this.password == null || this.password.length() > 40) {
			
			status.setStatusCode(StatusCode.ADMIN_AUTH_PASSWORD_WRONG);
			
			return status;
			
		}

		
		if(this.requestID == null || this.requestID.length() > 20 || this.requestID.trim() == "") {
			
			status.setStatusCode(StatusCode.ADMIN_AUTH_FAILURE);
			
			return status;
			
		}

		status.setStatusCode(StatusCode.ADMIN_AUTH_SUCCESS);

		return status;
	}
	
	
}
