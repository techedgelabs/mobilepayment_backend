package com.techedge.mp.frontendbo.adapter.entities.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class VoucherInfo {

    private String            voucherCode;
    private String            voucherStatus;
    private String            voucherType;
    private Double            voucherValue;
    private Double            initialValue;
    private Double            consumedValue;
    private Double            voucherBalanceDue;
    private Date              expirationDate;
    private String            promoCode;
    private String            promoDescription;
    private String            promoDoc;
    private Double            minQuantity;
    private Double            minAmount;
    private List<ProductInfo> validProduct    = new ArrayList<ProductInfo>(0);
    private String            validPV;
    private List<String>      validRefuelMode = new ArrayList<String>(0);
    private String            isCombinable;
    private String            promoPartner;

    public VoucherInfo() {}

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public String getVoucherStatus() {
        return voucherStatus;
    }

    public void setVoucherStatus(String voucherStatus) {
        this.voucherStatus = voucherStatus;
    }

    public String getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(String voucherType) {
        this.voucherType = voucherType;
    }

    public Double getVoucherValue() {
        return voucherValue;
    }

    public void setVoucherValue(Double voucherValue) {
        this.voucherValue = voucherValue;
    }

    public Double getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(Double initialValue) {
        this.initialValue = initialValue;
    }

    public Double getConsumedValue() {
        return consumedValue;
    }

    public void setConsumedValue(Double consumedValue) {
        this.consumedValue = consumedValue;
    }

    public Double getVoucherBalanceDue() {
        return voucherBalanceDue;
    }

    public void setVoucherBalanceDue(Double voucherBalanceDue) {
        this.voucherBalanceDue = voucherBalanceDue;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    public String getPromoDoc() {
        return promoDoc;
    }

    public void setPromoDoc(String promoDoc) {
        this.promoDoc = promoDoc;
    }

    /**
     * @return the minQuantity
     */
    public Double getMinQuantity() {
        return minQuantity;
    }

    /**
     * @param minQuantity
     *            the minQuantity to set
     */
    public void setMinQuantity(Double minQuantity) {
        this.minQuantity = minQuantity;
    }

    /**
     * @return the minAmount
     */
    public Double getMinAmount() {
        return minAmount;
    }

    /**
     * @param minAmount
     *            the minAmount to set
     */
    public void setMinAmount(Double minAmount) {
        this.minAmount = minAmount;
    }

    /**
     * @return the validProduct
     */
    public List<ProductInfo> getValidProduct() {
        return validProduct;
    }

    /**
     * @param validProduct
     *            the validProduct to set
     */
    public void setValidProduct(List<ProductInfo> validProduct) {
        this.validProduct = validProduct;
    }

    /**
     * @return the validPV
     */
    public String getValidPV() {
        return validPV;
    }

    /**
     * @param validPV
     *            the validPV to set
     */
    public void setValidPV(String validPV) {
        this.validPV = validPV;
    }

    /**
     * @return the validRefuelMode
     */
    public List<String> getValidRefuelMode() {
        return validRefuelMode;
    }

    /**
     * @param validRefuelMode
     *            the validRefuelMode to set
     */
    public void setValidRefuelMode(List<String> validRefuelMode) {
        this.validRefuelMode = validRefuelMode;
    }

    /**
     * @return the isCombinable
     */
    public String getIsCombinable() {
        return isCombinable;
    }

    /**
     * @param isCombinable
     *            the isCombinable to set
     */
    public void setIsCombinable(String isCombinable) {
        this.isCombinable = isCombinable;
    }

    /**
     * @return the promoPartner
     */
    public String getPromoPartner() {
        return promoPartner;
    }

    /**
     * @param promoPartner
     *            the promoPartner to set
     */
    public void setPromoPartner(String promoPartner) {
        this.promoPartner = promoPartner;
    }

}
