package com.techedge.mp.frontendbo.adapter.entities.admin.retrievelandingpages;

import com.techedge.mp.core.business.interfaces.LandingData;
import com.techedge.mp.core.business.interfaces.RetrieveLandingResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrieveLandingPagesRequest extends AbstractBORequest implements Validable {

    private Status                      status = new Status();

    private Credential                  credential;

    public RetrieveLandingPagesRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-LANDING", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_LANDING_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        RetrieveLandingPagesResponse retrieveLandingPagesResponse = new RetrieveLandingPagesResponse();
        
        RetrieveLandingResponse retrieveLandingResponse = getAdminServiceRemote().adminRetrieveLanding(this.getCredential().getTicketID(), this.getCredential().getRequestID());

        status.setStatusCode(retrieveLandingResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveLandingResponse.getStatusCode()));

        retrieveLandingPagesResponse.setStatus(status);

        if (retrieveLandingResponse != null && retrieveLandingResponse.getLandingPages() != null && !retrieveLandingResponse.getLandingPages().isEmpty()) {
            for(LandingData landingData : retrieveLandingResponse.getLandingPages()) {
                retrieveLandingPagesResponse.getLandingPages().add(landingData);
            }
        }

        return retrieveLandingPagesResponse;
    }

}
