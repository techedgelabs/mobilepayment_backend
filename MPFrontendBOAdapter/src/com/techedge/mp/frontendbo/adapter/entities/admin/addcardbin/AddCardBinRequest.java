package com.techedge.mp.frontendbo.adapter.entities.admin.addcardbin;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AddCardBinRequest extends AbstractBORequest implements Validable {

    private Status                status = new Status();

    private Credential            credential;
    private AddCardBinBodyRequest body;

    public AddCardBinRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AddCardBinBodyRequest getBody() {
        return body;
    }

    public void setBody(AddCardBinBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADDCARDBIN", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_ADD_CARD_BIN_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        AddCardBinResponse addCardBinResponse = new AddCardBinResponse();

        String response = getAdminServiceRemote().adminCardBinAdd(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getBin());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        addCardBinResponse.setStatus(status);

        return addCardBinResponse;
    }

}
