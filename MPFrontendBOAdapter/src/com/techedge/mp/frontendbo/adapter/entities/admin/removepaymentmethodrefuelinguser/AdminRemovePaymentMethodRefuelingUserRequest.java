package com.techedge.mp.frontendbo.adapter.entities.admin.removepaymentmethodrefuelinguser;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminRemovePaymentMethodRefuelingUserRequest extends AbstractBORequest implements Validable {
    private Status                                           status = new Status();

    private Credential                                       credential;
    private AdminRemovePaymentMethodRefuelingUserBodyRequest body;

    public AdminRemovePaymentMethodRefuelingUserRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminRemovePaymentMethodRefuelingUserBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminRemovePaymentMethodRefuelingUserBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADMIN-RM-PAYMENT-METHOD-REFUELING-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_RM_PAY_METH_REFUELING_USER_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RM_PAY_METH_REFUELING_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        
        String adminRemovePaymentMethodRefuelingUserResult = getAdminServiceRemote().adminRemovePaymentMethodRefuelingUser(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getUsername(), this.getBody().getPaymentMethod().getId());

        AdminRemovePaymentMethodRefuelingUserResponse response = new AdminRemovePaymentMethodRefuelingUserResponse();
        status.setStatusCode(adminRemovePaymentMethodRefuelingUserResult);
        status.setStatusMessage(prop.getProperty(adminRemovePaymentMethodRefuelingUserResult));
        response.setStatus(status);

        return response;

    }
}
