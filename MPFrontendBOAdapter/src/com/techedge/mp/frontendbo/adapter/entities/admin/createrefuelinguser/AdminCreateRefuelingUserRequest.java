package com.techedge.mp.frontendbo.adapter.entities.admin.createrefuelinguser;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminCreateRefuelingUserRequest extends AbstractBORequest implements Validable {
    private Status                              status = new Status();

    private Credential                          credential;
    private AdminCreateRefuelingUserBodyRequest body;

    public AdminCreateRefuelingUserRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminCreateRefuelingUserBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminCreateRefuelingUserBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADMIN-CREATE-REFUELING-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_CREATE_REFUELING_USER_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_REFUELING_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String adminCreateRefuelingUserResult = getAdminServiceRemote().adminCreateRefuelingUser(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getUsername(), this.getBody().getPassword());

        AdminCreateRefuelingUserResponse response = new AdminCreateRefuelingUserResponse();
        status.setStatusCode(adminCreateRefuelingUserResult);
        status.setStatusMessage(prop.getProperty(adminCreateRefuelingUserResult));
        response.setStatus(status);

        return response;

    }

}
