package com.techedge.mp.frontendbo.adapter.entities.admin.searchmanager;

import com.techedge.mp.core.business.interfaces.RetrieveManagerListData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class SearchManagerRequest extends AbstractBORequest implements Validable {

    private Status                   status = new Status();

    private Credential               credential;
    private SearchManagerBodyRequest body;

    public SearchManagerRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public SearchManagerBodyRequest getBody() {
        return body;
    }

    public void setBody(SearchManagerBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("SEARCH-MANAGER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_SEARCH_MANAGER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        SearchManagerResponse searchManagerResponse = new SearchManagerResponse();
        SearchManagerBodyRequest bodyRequest = this.getBody();

        RetrieveManagerListData retrieveManagerListData = getAdminServiceRemote().adminSearchManager(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                bodyRequest.getId(), bodyRequest.getUsername(), bodyRequest.getEmail(), bodyRequest.getMaxResults());

        status.setStatusCode(retrieveManagerListData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveManagerListData.getStatusCode()));

        searchManagerResponse.setStatus(status);
        searchManagerResponse.setManagerData(retrieveManagerListData.getManagerList());

        return searchManagerResponse;
    }

}
