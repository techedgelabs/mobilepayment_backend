package com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RetrieveVoucherTransactionsRequestBody implements Validable {

    private String voucherTransactionId;
    private Long   userId;
    private String voucherCode;
    private Long   creationTimestampStart;
    private Long   creationTimestampEnd;
    private String finalStatusType;

    public String getVoucherTransactionId() {
        return voucherTransactionId;
    }

    public void setVoucherTransactionId(String voucherTransactionId) {
        this.voucherTransactionId = voucherTransactionId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getVoucherCode() {
        return voucherCode;
    }

    public void setVoucherCode(String voucherCode) {
        this.voucherCode = voucherCode;
    }

    public Long getCreationTimestampStart() {
        return creationTimestampStart;
    }

    public void setCreationTimestampStart(Long creationTimestampStart) {
        this.creationTimestampStart = creationTimestampStart;
    }

    public Long getCreationTimestampEnd() {
        return creationTimestampEnd;
    }

    public void setCreationTimestampEnd(Long creationTimestampEnd) {
        this.creationTimestampEnd = creationTimestampEnd;
    }

    public String getFinalStatusType() {
        return finalStatusType;
    }

    public void setFinalStatusType(String finalStatusType) {
        this.finalStatusType = finalStatusType;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.voucherTransactionId != null && this.voucherTransactionId.length() != 32) {

            status.setStatusCode(StatusCode.RETRIEVE_VOUCHER_INVALID_PARAMETES);
            return status;
        }

        if (this.voucherCode != null && this.voucherCode.length() > 16) {

            status.setStatusCode(StatusCode.RETRIEVE_VOUCHER_INVALID_PARAMETES);
            return status;
        }

        if (this.finalStatusType != null && this.finalStatusType.length() > 20) {

            status.setStatusCode(StatusCode.RETRIEVE_VOUCHER_INVALID_PARAMETES);
            return status;
        }

        status.setStatusCode(StatusCode.RETRIEVE_VOUCHER_TRANSACTIONS_SUCCESS);

        return status;
    }

}
