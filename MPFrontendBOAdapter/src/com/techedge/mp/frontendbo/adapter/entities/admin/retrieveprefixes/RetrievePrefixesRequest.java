package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveprefixes;

import com.techedge.mp.core.business.interfaces.PrefixNumberResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrievePrefixesRequest extends AbstractBORequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-PREFIXES", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.PREFIX_RETRIEVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        PrefixNumberResult retrievePrefixData = getAdminServiceRemote().adminRetrieveAllPrefixNumber(this.getCredential().getTicketID(), this.getCredential().getRequestID());

        RetrievePrefixesResponse retrievePrefixNumberResponse = new RetrievePrefixesResponse();

        status.setStatusCode(retrievePrefixData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrievePrefixData.getStatusCode()));

        retrievePrefixNumberResponse.setStatus(status);
        retrievePrefixNumberResponse.setPrefixes(retrievePrefixData.getListPrefix());

        return retrievePrefixNumberResponse;
    }

}
