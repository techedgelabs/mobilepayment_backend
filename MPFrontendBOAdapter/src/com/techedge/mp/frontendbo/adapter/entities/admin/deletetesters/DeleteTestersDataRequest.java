package com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;


public class DeleteTestersDataRequest implements Validable {
	
	private String emailPrefix;
	private String emailSuffix;
	private Integer startNumber;
	private Integer userCount;
	
	
	
	public DeleteTestersDataRequest() {
	}


	public String getEmailPrefix() {
		return emailPrefix;
	}
	public void setEmailPrefix(String emailPrefix) {
		this.emailPrefix = emailPrefix;
	}

	public String getEmailSuffix() {
		return emailSuffix;
	}
	public void setEmailSuffix(String emailSuffix) {
		this.emailSuffix = emailSuffix;
	}

	public Integer getStartNumber() {
		return startNumber;
	}
	public void setStartNumber(Integer startNumber) {
		this.startNumber = startNumber;
	}

	public Integer getUserCount() {
		return userCount;
	}
	public void setUserCount(Integer userCount) {
		this.userCount = userCount;
	}

	


	@Override
	public Status check() {
		
		Status status = new Status();
		
		status.setStatusCode(StatusCode.ADMIN_DELETE_TESTER_USER_SUCCESS);
		
		return status;
		
	}
}
