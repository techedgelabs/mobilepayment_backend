package com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminGetErrorBodyRequest implements Validable {

    private String errorCode;

    public String getGpErrorCode() {
        return errorCode;
    }

    public void setGpErrorCode(String gpErrorCode) {
        this.errorCode = gpErrorCode;
    }

    @Override
    public Status check() {
        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_GET__SUCCESS);

        return status;
    }

}
