package com.techedge.mp.frontendbo.adapter.entities.admin.logout;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class LogoutAdminRequest extends AbstractBORequest implements Validable {

    private Status     status = new Status();

    private Credential credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("LOGOUT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_LOGOUT_SUCCESS);

        return status;
    }

    @Override
    public BaseResponse execute() {

        String response = getAdminServiceRemote().adminLogout(this.getCredential().getTicketID(), this.getCredential().getRequestID());

        LogoutAdminResponse logoutAdminResponse = new LogoutAdminResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        logoutAdminResponse.setStatus(status);

        return logoutAdminResponse;
    }

}
