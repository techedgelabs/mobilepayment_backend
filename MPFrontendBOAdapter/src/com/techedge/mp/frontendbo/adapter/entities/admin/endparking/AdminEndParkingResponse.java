package com.techedge.mp.frontendbo.adapter.entities.admin.endparking;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminEndParkingResponse extends BaseResponse{
    
    private AdminEndParkingResponseBody body;

    public AdminEndParkingResponseBody getBody() {
        return body;
    }

    public void setBody(AdminEndParkingResponseBody body) {
        this.body = body;
    }
}
