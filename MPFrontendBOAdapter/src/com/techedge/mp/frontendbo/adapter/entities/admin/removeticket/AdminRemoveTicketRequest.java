package com.techedge.mp.frontendbo.adapter.entities.admin.removeticket;

import com.techedge.mp.core.business.interfaces.AdminDeleteTicketResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminRemoveTicketRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private AdminRemoveTicketBodyRequest body;

    public AdminRemoveTicketRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminRemoveTicketBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminRemoveTicketBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("TICKET-REMOVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REMOVE_TICKET_INVALID_PARAMETERS);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_REMOVE_TICKET_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        AdminRemoveTicketResponse adminRemoveTicketResponse = new AdminRemoveTicketResponse();

        AdminDeleteTicketResponse adminDeleteTicketResponse = getAdminServiceRemote().adminTicketDelete(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getTicketType(),
                this.getBody().getIdFrom(), this.getBody().getIdTo());

        status.setStatusCode(adminDeleteTicketResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(adminDeleteTicketResponse.getStatusCode()));
        
        AdminRemoveTicketBodyResponse adminRemoveTicketBodyResponse = new AdminRemoveTicketBodyResponse();
        adminRemoveTicketBodyResponse.setDeletedTickets(adminDeleteTicketResponse.getDeletedTickets());
        
        adminRemoveTicketResponse.setStatus(status);
        adminRemoveTicketResponse.setBody(adminRemoveTicketBodyResponse);

        return adminRemoveTicketResponse;
    }

}
