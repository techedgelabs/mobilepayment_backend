package com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.AdminErrorResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminGetErrorRequest extends AbstractBORequest implements Validable {

    private Status                   status = new Status();

    private Credential               credential;

    private AdminGetErrorBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminGetErrorBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminGetErrorBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("MAPPING-ERROR-GET", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_GET__INVALID_CODE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_GET__SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        List<AdminGetErrorBodyResponse> adminGetErrorBodyResponseList = new ArrayList<AdminGetErrorBodyResponse>();

        AdminGetErrorResponse adminGetErrorResponse = new AdminGetErrorResponse();
        List<AdminErrorResponse> response = new ArrayList<AdminErrorResponse>();
        response = getAdminServiceRemote().adminGetMappingError(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getGpErrorCode());

        for (AdminErrorResponse item : response) {
            AdminGetErrorBodyResponse adminGetErrorBodyResponse = new AdminGetErrorBodyResponse();
            adminGetErrorBodyResponse.setErrorCode(item.getGpErrorCode());
            adminGetErrorBodyResponse.setStatusCode(item.getMpStatusCode());
            adminGetErrorBodyResponseList.add(adminGetErrorBodyResponse);
        }
        adminGetErrorResponse.setStatus(status);
        adminGetErrorResponse.setBody(adminGetErrorBodyResponseList);

        return adminGetErrorResponse;
    }

}
