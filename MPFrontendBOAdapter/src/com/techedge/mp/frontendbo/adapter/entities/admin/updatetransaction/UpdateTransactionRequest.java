package com.techedge.mp.frontendbo.adapter.entities.admin.updatetransaction;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class UpdateTransactionRequest extends AbstractBORequest implements Validable {

    private Status                       status = new Status();

    private Credential                   credential;
    private UpdateTransactionRequestBody body;

    public UpdateTransactionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateTransactionRequestBody getBody() {
        return body;
    }

    public void setBody(UpdateTransactionRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-TRANSACTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_TRANSACTION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String response = getAdminServiceRemote().adminTransactionUpdate(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getTransactionId(), this.getBody().getFinalStatusType(), this.getBody().getGFGNotification(), this.getBody().getConfirmed(),
                this.getBody().getReconciliationAttemptsLeft());

        UpdateTransactionResponse updateTransactionResponse = new UpdateTransactionResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updateTransactionResponse.setStatus(status);

        return updateTransactionResponse;
    }

}
