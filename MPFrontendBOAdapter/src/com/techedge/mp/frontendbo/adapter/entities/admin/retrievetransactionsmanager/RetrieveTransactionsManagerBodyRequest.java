package com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactionsmanager;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RetrieveTransactionsManagerBodyRequest implements Validable {
	
	
	private RetrieveTransactionsManagerDataRequest managerData;
	
	
	public RetrieveTransactionsManagerBodyRequest() {}

	public RetrieveTransactionsManagerDataRequest getManagerData() {
		return managerData;
	}
	public void setManagerData(RetrieveTransactionsManagerDataRequest managerData) {
		this.managerData = managerData;
	}
	
	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.managerData != null) {
			
			status = this.managerData.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
			
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS);

		return status;
	}
	
}
