package com.techedge.mp.frontendbo.adapter.entities.admin.createblockperiod;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminCreateBlockPeriodBodyRequest implements Validable {

    private String  code;
    private String  startDate;
    private String  endDate;
    private String  startTime;
    private String  endTime;
    private String  operation;
    private String  statusCode;
    private String  statusMessage;
    private Boolean active;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String mpStatusCode) {
        this.statusCode = mpStatusCode;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (this.code == null || this.code.isEmpty() || this.code.trim().isEmpty() || this.statusCode == null || this.statusCode.isEmpty() || this.statusCode.trim().isEmpty()
                || this.operation == null || this.operation.isEmpty() || this.operation.trim().isEmpty() || this.statusMessage == null || this.statusMessage.isEmpty()
                || this.statusMessage.trim().isEmpty() || this.active == null) {
            status.setStatusCode(StatusCode.ADMIN_BLOCK_PERIOD_CREATE_CHECK_FAILURE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_BLOCK_PERIOD_CREATE_SUCCESS);

        return status;
    }
}
