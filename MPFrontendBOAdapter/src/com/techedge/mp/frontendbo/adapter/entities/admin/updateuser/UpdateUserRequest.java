package com.techedge.mp.frontendbo.adapter.entities.admin.updateuser;

import java.util.Date;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class UpdateUserRequest extends AbstractBORequest implements Validable {

    private Status                status = new Status();

    private Credential            credential;
    private UpdateUserRequestBody body;

    public UpdateUserRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateUserRequestBody getBody() {
        return body;
    }

    public void setBody(UpdateUserRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        Boolean registrationCompleted    = null;
        Boolean virtualizationCompleted  = null;
        Boolean eniStationUserType       = null;
        Boolean depositCardStepCompleted = null;
        
        if (this.getBody().getRegistrationCompleted() != null) {
            if (this.getBody().getRegistrationCompleted().equals("true")) {
                registrationCompleted = Boolean.TRUE;
            }
            else {
                registrationCompleted = Boolean.FALSE;
            }
        }
        
        if (this.getBody().getVirtualizationCompleted() != null) {
            if (this.getBody().getVirtualizationCompleted().equals("true")) {
                virtualizationCompleted = Boolean.TRUE;
            }
            else {
                virtualizationCompleted = Boolean.FALSE;
            }
        }
        
        if (this.getBody().getEniStationUserType() != null) {
            if (this.getBody().getEniStationUserType().equals("true")) {
                eniStationUserType = Boolean.TRUE;
            }
            else {
                eniStationUserType = Boolean.FALSE;
            }
        }
        
        if (this.getBody().getDepositCardStepCompleted() != null) {
            if (this.getBody().getDepositCardStepCompleted().equals("true")) {
                depositCardStepCompleted = Boolean.TRUE;
            }
            else {
                depositCardStepCompleted = Boolean.FALSE;
            }
        }

        String response = getAdminServiceRemote().adminUserUpdate(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getId(),
                this.getBody().getFirstName(), this.getBody().getLastName(), this.getBody().getEmail(), this.getBody().getBirthDate(), this.getBody().getBirthMunicipality(),
                this.getBody().getBirthProvince(), this.getBody().getSex(), this.getBody().getFiscalCode(), this.getBody().getLanguage(), this.getBody().getStatus(),
                registrationCompleted, this.getBody().getCapAvailable(), this.getBody().getCapEffective(), this.getBody().getExternalUserId(), this.getBody().getUserType(),
                virtualizationCompleted, this.getBody().getVirtualizationAttemptsLeft(), eniStationUserType, depositCardStepCompleted);

        UpdateUserResponse updateUserResponse = new UpdateUserResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updateUserResponse.setStatus(status);

        return updateUserResponse;
    }

}
