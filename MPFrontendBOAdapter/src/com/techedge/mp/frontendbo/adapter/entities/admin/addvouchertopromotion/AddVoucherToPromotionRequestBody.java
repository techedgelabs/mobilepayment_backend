package com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion;

import java.util.Set;

import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AddVoucherToPromotionRequestBody implements Validable {
	
	PromoVoucherInput voucherInfo = new PromoVoucherInput(); //non vuoto ListaVoucherNonValida 
	String promotionCode = new String(); //no null e max length 40  prmotionCodeNon valido

	
	public PromoVoucherInput getVoucherInfo() {
        return voucherInfo;
    }

    public void setVoucherInfo(PromoVoucherInput voucherInfo) {
        this.voucherInfo = voucherInfo;
    }

    public String getPromotionCode() {
        return promotionCode;
    }

    public void setPromotionCode(String promotionCode) {
        this.promotionCode = promotionCode;
    }

    @Override
	public Status check() {
		
		Status status = new Status();
		status.setStatusCode(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_INVALID_REQUEST);
		
		if( this.voucherInfo != null){
		    
		    if( voucherInfo.getPromoAssociation() != null && !voucherInfo.getPromoAssociation().isEmpty()){
		        
		        Set<String> keySet = voucherInfo.getPromoAssociation().keySet();
		        if( !keySet.isEmpty() ){
		            
		            status.setStatusCode(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_SUCCESS);
		            
		        }
		    }
		}		
		
		if(!Validator.isValid(status.getStatusCode())) {
            
		    status.setStatusCode(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_VOUCERINFO);
            return status;
            
        }
		
		if( promotionCode != null && promotionCode.length() <= 20 ){
		    
		    status.setStatusCode(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_SUCCESS);
		} 
		else {
		    status.setStatusCode(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_CODE);
		}
		return status;
	}
}
