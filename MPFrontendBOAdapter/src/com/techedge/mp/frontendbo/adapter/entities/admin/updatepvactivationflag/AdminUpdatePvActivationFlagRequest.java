package com.techedge.mp.frontendbo.adapter.entities.admin.updatepvactivationflag;

import com.techedge.mp.core.business.interfaces.GeneratePvResponse;
import com.techedge.mp.core.business.interfaces.PvGenerationData;
import com.techedge.mp.core.business.interfaces.StationActivationData;
import com.techedge.mp.core.business.interfaces.UpdatePvActivationFlagResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.PvGenerationInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminUpdatePvActivationFlagRequest extends AbstractBORequest implements Validable {

    private Status                     status = new Status();

    private Credential                 credential;

    private AdminUpdatePvActivationFlagBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminUpdatePvActivationFlagBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminUpdatePvActivationFlagBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GENERATE-PV", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_GENERATE_PV_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_GENERATE_PV_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        
        UpdatePvActivationFlagResult updatePvActivationFlagResult = getAdminServiceRemote().adminUpdatePvActivationFlag(
                this.getCredential().getTicketID(),
                this.getCredential().getRequestID(),
                this.getBody().getStationActivationList());

        AdminUpdatePvActivationFlagResponse adminUpdatePvActivationFlagResponse = new AdminUpdatePvActivationFlagResponse();

        status.setStatusCode(updatePvActivationFlagResult.getStatusCode());
        status.setStatusMessage(prop.getProperty(updatePvActivationFlagResult.getStatusCode()));
        adminUpdatePvActivationFlagResponse.setStatus(status);

        AdminUpdatePvActivationFlagBodyResponse adminUpdatePvActivationFlagBodyResponse = new AdminUpdatePvActivationFlagBodyResponse();
        adminUpdatePvActivationFlagResponse.setBody(adminUpdatePvActivationFlagBodyResponse);

        for (PvGenerationData pvGenerationData : updatePvActivationFlagResult.getPvGenerationDataList()) {

            PvGenerationInfo pvGenerationInfo = new PvGenerationInfo();
            pvGenerationInfo.setStationId(pvGenerationData.getStationId());
            pvGenerationInfo.setStatusCode(pvGenerationData.getStatusCode());
            adminUpdatePvActivationFlagResponse.getBody().getPvActivationResultList().add(pvGenerationInfo);
        }
    
        return adminUpdatePvActivationFlagResponse;
    }
}
