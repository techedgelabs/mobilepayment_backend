package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.ActivityLogInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;


public class RetrieveActivityLogDataRequest implements Validable {
	
	private List<ActivityLogInfo> activityLogData = new ArrayList<ActivityLogInfo>(0);
	
	public RetrieveActivityLogDataRequest() {
	}

	public List<ActivityLogInfo> getActivityLogData() {
		return activityLogData;
	}
	public void setActivityLogData(List<ActivityLogInfo> activityLogData) {
		this.activityLogData = activityLogData;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);
		
		return status;
		
	}
	
	
}
