package com.techedge.mp.frontendbo.adapter.entities.admin.testgetpaymenttransactiondetails;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

public class TestGetPaymentTransactionDetailsRequest extends AbstractBORequest implements Validable {

    private Credential                                  credential;
    private TestGetPaymentTransactionDetailsBodyRequest body;

    public TestGetPaymentTransactionDetailsRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestGetPaymentTransactionDetailsBodyRequest getBody() {
        return body;
    }

    public void setBody(TestGetPaymentTransactionDetailsBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GET-PAYMENT-TRANSACTION-DETAILS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_GET_PAYMENT_TRANSACTION_DETAILS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestGetPaymentTransactionDetailsResponse testGetPaymentTransactionDetailsResponse = new TestGetPaymentTransactionDetailsResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testGetPaymentTransactionDetails");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testGetPaymentTransactionDetailsResponse.getStatus().setStatusCode("-1");
        }
        else {
            GestPayData gestPayData = new GestPayData();

            try {
                String shopLogin = this.getBody().getShopLogin();
                String shopTransactionID = this.getBody().getShopTransactionID();
                String bankTransactionID = this.getBody().getBankTransactionID();
                String acquirerId = this.getBody().getAcquirerId();
                String groupAcquirer = this.getBody().getGroupAcquirer();
                String encodedSecretKey = this.getBody().getEncodedSecretKey();

                gestPayData = getGpService().callReadTrx(shopLogin, shopTransactionID, bankTransactionID, acquirerId, groupAcquirer, encodedSecretKey);
            }
            catch (Exception ex) {
                gestPayData = null;
            }

            testGetPaymentTransactionDetailsResponse.setStatusCode("OK");
            testGetPaymentTransactionDetailsResponse.setResult(gestPayData);
        }

        return testGetPaymentTransactionDetailsResponse;
    }

}
