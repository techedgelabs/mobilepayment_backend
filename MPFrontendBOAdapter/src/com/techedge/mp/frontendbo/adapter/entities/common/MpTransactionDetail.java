package com.techedge.mp.frontendbo.adapter.entities.common;


public class MpTransactionDetail {

    
    protected String       srcTransactionID;
    protected String       mpTransactionID;
    protected String       mpTransactionStatus;
    protected RefuelDetail refuelDetail;

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public String getMpTransactionStatus() {
        return mpTransactionStatus;
    }

    public void setMpTransactionStatus(String mpTransactionStatus) {
        this.mpTransactionStatus = mpTransactionStatus;
    }

    public RefuelDetail getRefuelDetail() {
        return refuelDetail;
    }

    public void setRefuelDetail(RefuelDetail refuelDetail) {
        this.refuelDetail = refuelDetail;
    }

}