package com.techedge.mp.frontendbo.adapter.entities.admin.insertmulticardrefuelinguser;

import com.techedge.mp.core.business.interfaces.PaymentMethodResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminInsertMulticardRefuelingUserRequest extends AbstractBORequest implements Validable {
    private Status                                           status = new Status();

    private Credential                                       credential;
    private AdminInsertMulticardRefuelingUserBodyRequest body;

    public AdminInsertMulticardRefuelingUserRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminInsertMulticardRefuelingUserBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminInsertMulticardRefuelingUserBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADMIN-INSERT-MULTICARD-REFUELING-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_INSERT_MULTICARD_REFUELING_USER_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_INSERT_MULTICARD_REFUELING_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        PaymentMethodResponse result = getAdminServiceRemote().adminInsertMulticardRefuelingUser(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getUsername());

        AdminInsertMulticardRefuelingUserResponse response = new AdminInsertMulticardRefuelingUserResponse();
        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(prop.getProperty(result.getStatusCode()));
        response.setBody(new AdminInsertMulticardRefuelingUserBodyResponse());
        response.setStatus(status);
        response.getBody().setPaymentMethod(result.getPaymentMethod());
        response.getBody().setRedirectUrl(result.getRedirectURL());
        return response;

    }

}
