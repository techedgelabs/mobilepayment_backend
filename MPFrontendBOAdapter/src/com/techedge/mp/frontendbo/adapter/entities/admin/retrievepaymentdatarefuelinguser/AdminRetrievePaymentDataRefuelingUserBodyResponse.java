package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepaymentdatarefuelinguser;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.PaymentDataList;

public class AdminRetrievePaymentDataRefuelingUserBodyResponse {

    private List<PaymentDataList> paymentMethodList = new ArrayList<>(0);

    public List<PaymentDataList> getPaymentMethodList() {
        return paymentMethodList;
    }

    public void setPaymentMethodList(List<PaymentDataList> paymentMethod) {
        this.paymentMethodList = paymentMethod;
    }

    public AdminRetrievePaymentDataRefuelingUserBodyResponse() {}

}
