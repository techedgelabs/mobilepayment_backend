package com.techedge.mp.frontendbo.adapter.entities.admin.authentication;

import java.util.Set;

import com.techedge.mp.core.business.interfaces.AdminRole;

public class AuthenticationAdminDataResponse {

    private String         email;
    private String         firstname;
    private String         lastname;
    private Integer        status;
    private Set<AdminRole> roles;

    public AuthenticationAdminDataResponse() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Set<AdminRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<AdminRole> roles) {
        this.roles = roles;
    }

}
