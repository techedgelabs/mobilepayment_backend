package com.techedge.mp.frontendbo.adapter.entities.admin.generalcreate;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.TypeData;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminGeneralCreateRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private AdminGeneralCreateRequestBody body;

    public AdminGeneralCreateRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminGeneralCreateRequestBody getBody() {
        return body;
    }

    public void setBody(AdminGeneralCreateRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("GENERAL-CREATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_GENERAL_CREATE_INVALID_BODY);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_GENERAL_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        List<com.techedge.mp.core.business.interfaces.TypeData> listTypeData = new ArrayList<>(0);

        for (TypeData item : this.getBody().getFields()) {
            if (item != null) {
                com.techedge.mp.core.business.interfaces.TypeData typeData = new com.techedge.mp.core.business.interfaces.TypeData();
                typeData.setField(item.getField());
                typeData.setValue(item.getValue());
                listTypeData.add(typeData);
            }
        }

        String response = getAdminServiceRemote().adminGeneralCreate(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getType(),
                listTypeData);

        AdminGeneralCreateResponse adminGeneralCreateResponse = new AdminGeneralCreateResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        adminGeneralCreateResponse.setStatus(status);

        return adminGeneralCreateResponse;
    }

}
