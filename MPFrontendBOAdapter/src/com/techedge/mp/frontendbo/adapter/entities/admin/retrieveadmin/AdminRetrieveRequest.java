package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveadmin;

import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.AdminRetrieveDataResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminRetrieveRequest extends AbstractBORequest implements Validable {

    private Status                   status = new Status();

    private Credential               credential;
    private AdminRetrieveRequestBody body   = new AdminRetrieveRequestBody();

    public AdminRetrieveRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminRetrieveRequestBody getBody() {
        return body;
    }

    public void setBody(AdminRetrieveRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        status = Validator.checkCredential("ADMIN-RETRIEVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        AdminRetrieveDataResponse adminRetrieveDataResponse = getAdminServiceRemote().adminRetrieve(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getId(), this.getBody().getEmail(), this.getBody().getStatus());

        if (adminRetrieveDataResponse != null) {
            for (Admin item : adminRetrieveDataResponse.getAdmins()) {
                item.setPassword("");
            }
        }

        AdminRetrieveResponse adminRetrieveResponse = new AdminRetrieveResponse();

        status.setStatusCode(adminRetrieveDataResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(adminRetrieveDataResponse.getStatusCode()));

        adminRetrieveResponse.setAdminUsers(adminRetrieveDataResponse.getAdmins());
        adminRetrieveResponse.setStatus(status);

        return adminRetrieveResponse;
    }

}
