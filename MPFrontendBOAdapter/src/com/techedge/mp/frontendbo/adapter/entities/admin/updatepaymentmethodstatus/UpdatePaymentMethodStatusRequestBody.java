package com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethodstatus;

import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdatePaymentMethodStatusRequestBody implements Validable {

    private Long    id;
    private String  type;
    private Integer status;
    private String  message;

    @Override
    public Status check() {

        Status status = new Status();

        if (this.id == null) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS);
            return status;
        }

        if (this.type != null && this.type.length() > 20) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS);
            return status;
        }

        if (this.status != null
                && (this.status != PaymentInfo.PAYMENTINFO_STATUS_BLOCKED && this.status != PaymentInfo.PAYMENTINFO_STATUS_CANCELED
                        && this.status != PaymentInfo.PAYMENTINFO_STATUS_ERROR && this.status != PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED
                        && this.status != PaymentInfo.PAYMENTINFO_STATUS_PENDING && this.status != PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_SUCCESS);

        return status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
