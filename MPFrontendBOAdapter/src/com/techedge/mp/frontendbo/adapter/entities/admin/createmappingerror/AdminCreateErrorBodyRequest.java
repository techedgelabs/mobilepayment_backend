package com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminCreateErrorBodyRequest implements Validable {

    private String errorCode;
    private String statusCode;

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String gpErrorCode) {
        this.errorCode = gpErrorCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String mpStatusCode) {
        this.statusCode = mpStatusCode;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (this.errorCode == null || this.statusCode == null) {
            status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_CREATE__INVALID_CODE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_CREATE__SUCCESS);

        return status;
    }

}
