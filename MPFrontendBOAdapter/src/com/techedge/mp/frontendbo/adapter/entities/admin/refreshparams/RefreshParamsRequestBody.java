package com.techedge.mp.frontendbo.adapter.entities.admin.refreshparams;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RefreshParamsRequestBody implements Validable {
	
	private String command;

	
	public String getCommand() {
        return command;
    }


    public void setCommand(String command) {
        this.command = command;
    }


    @Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.command == null || this.command.isEmpty()) {
			
			status.setStatusCode(StatusCode.ADMIN_REFRESH_PARAMS_FAILURE);
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_REFRESH_PARAMS_SUCCESS);

		return status;
	}
}
