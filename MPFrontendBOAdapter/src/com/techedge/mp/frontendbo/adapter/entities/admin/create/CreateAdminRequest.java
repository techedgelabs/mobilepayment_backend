package com.techedge.mp.frontendbo.adapter.entities.admin.create;

import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class CreateAdminRequest extends AbstractBORequest implements Validable {

    private Status                 status = new Status();

    private Credential             credential;
    private CreateAdminRequestBody body;

    public CreateAdminRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateAdminRequestBody getBody() {
        return body;
    }

    public void setBody(CreateAdminRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        Admin admin = new Admin();

        admin.setEmail(this.getBody().getAdminData().getEmail());
        admin.setPassword(this.getBody().getAdminData().getPassword());
        admin.setFirstName(this.getBody().getAdminData().getFirstName());
        admin.setLastName(this.getBody().getAdminData().getLastName());
        admin.setRoles(this.getBody().getAdminData().getRoles());

        String response = getAdminServiceRemote().adminCreate(this.getCredential().getTicketID(), this.getCredential().getRequestID(), admin);

        CreateAdminResponse createAdminResponse = new CreateAdminResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        createAdminResponse.setStatus(status);

        return createAdminResponse;
    }
}
