package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveparams;

import com.techedge.mp.core.business.interfaces.RetrieveParamsData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrieveParamsRequest extends AbstractBORequest implements Validable {

    private Status                    status = new Status();

    private Credential                credential;
    private RetrieveParamsRequestBody body;

    public RetrieveParamsRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveParamsRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveParamsRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_STATISTICS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        RetrieveParamsResponse retrieveParamsResponse = new RetrieveParamsResponse();

        RetrieveParamsData retrieveParamsData = getAdminServiceRemote().adminRetrieveParams(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), this.getBody().getParam());

        status.setStatusCode(retrieveParamsData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveParamsData.getStatusCode()));

        retrieveParamsResponse.setStatus(status);
        retrieveParamsResponse.setParamList(retrieveParamsData.getParamsList());

        return retrieveParamsResponse;
    }

}
