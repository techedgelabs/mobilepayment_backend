package com.techedge.mp.frontendbo.adapter.entities.admin.stationsoperation;

import com.techedge.mp.core.business.interfaces.StationsAdminData;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class StationsAddRequest extends AbstractBORequest implements Validable {

    private Status                status = new Status();

    private Credential            credential;
    private StationsOpRequestBody body;

    public StationsAddRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public StationsOpRequestBody getBody() {
        return body;
    }

    public void setBody(StationsOpRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        StationsOpResponse stationSOpResponse = new StationsOpResponse();
        StationsAdminData stationsAdminData = new StationsAdminData();
        stationsAdminData.setStationAdminList(this.getBody().getStation());

        StationsAdminData stationsAdminDataResponse = getAdminServiceRemote().adminAddStations(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                stationsAdminData);

        status.setStatusCode(stationsAdminDataResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(stationsAdminDataResponse.getStatusCode()));

        stationSOpResponse.setStatus(status);
        stationSOpResponse.setStationsList(stationsAdminDataResponse.getStationAdminList());

        return stationSOpResponse;
    }

}
