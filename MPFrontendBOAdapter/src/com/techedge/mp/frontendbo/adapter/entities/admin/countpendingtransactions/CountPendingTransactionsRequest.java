package com.techedge.mp.frontendbo.adapter.entities.admin.countpendingtransactions;

import com.techedge.mp.core.business.AdminServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.CountPendingTransactionsResult;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;

public class CountPendingTransactionsRequest extends AbstractBORequest implements Validable {

    private Credential                credential;

    private CountPendingTransactionsRequestBody body;

    public CountPendingTransactionsRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CountPendingTransactionsRequestBody getBody() {
        return body;
    }

    public void setBody(CountPendingTransactionsRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("COUNT-PENDING-TRANSACTIONS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_COUNT_PENDING_TRANSACTIONS_ERROR_PARAMETERS);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_COUNT_PENDING_TRANSACTIONS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        CountPendingTransactionsResponse countPendingTransactionsResponse = new CountPendingTransactionsResponse();

        AdminServiceRemote   adminService;

        try {
            adminService = EJBHomeCache.getInstance().getAdminService();

            String serverName = this.getBody().getServerName();
            String adminTicketId = this.getCredential().getTicketID();
            String requestId = this.getCredential().getRequestID();

            CountPendingTransactionsResult countPendingTransactionsResult = adminService.adminCountPendingTransactions(adminTicketId, requestId, serverName);

            CountPendingTransactionsResponseBody countPendingTransactionsResponseBody = new CountPendingTransactionsResponseBody();
            countPendingTransactionsResponseBody.setCount(countPendingTransactionsResult.getCount());

            countPendingTransactionsResponse.setBody(countPendingTransactionsResponseBody);
            countPendingTransactionsResponse.getStatus().setStatusCode(countPendingTransactionsResult.getStatusCode());
        }
        catch (InterfaceNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return countPendingTransactionsResponse;
    }
}
