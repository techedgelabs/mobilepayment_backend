package com.techedge.mp.frontendbo.adapter.entities.admin.updateparams;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.ParamUpdateResult;


public class UpdateParamsResponse extends BaseResponse {
	
	List<ParamUpdateResult> result = new ArrayList<ParamUpdateResult>(0);

	public List<ParamUpdateResult> getResult() {
		return result;
	}
	public void setResult(List<ParamUpdateResult> result) {
		this.result = result;
	}
}