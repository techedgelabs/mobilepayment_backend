package com.techedge.mp.frontendbo.adapter.entities.admin.forceupdatetermsfservice;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class ForceUpdateTermsOfServiceBodyRequest implements Validable {

    private String  termsOfServiceId;
    private Long    userId;
    private Boolean valid;

    public String getTermsOfServiceId() {
        return termsOfServiceId;
    }

    public void setTermsOfServiceId(String termsOfServiceId) {
        this.termsOfServiceId = termsOfServiceId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_UPDATE_TERMS_OF_SERVICE_SUCCESS);

        return status;
    }

}
