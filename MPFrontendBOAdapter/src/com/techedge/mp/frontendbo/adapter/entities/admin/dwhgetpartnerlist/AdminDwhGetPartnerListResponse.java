package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetpartnerlist;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminDwhGetPartnerListResponse extends BaseResponse {

    private AdminDwhGetPartnerListBodyResponse body;

    public AdminDwhGetPartnerListBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminDwhGetPartnerListBodyResponse body) {
        this.body = body;
    }

}