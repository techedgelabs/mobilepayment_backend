package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetpartnerlist;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.dwh.adapter.interfaces.PartnerDetail;

public class AdminDwhGetPartnerListBodyResponse {

    private String              errorCode;
    private String              statusCode;
    private List<PartnerDetail> partnerDetailList = new ArrayList<PartnerDetail>(0);

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<PartnerDetail> getPartnerDetailList() {
        return partnerDetailList;
    }

    public void setPartnerDetailList(List<PartnerDetail> partnerDetailList) {
        this.partnerDetailList = partnerDetailList;
    }

}
