package com.techedge.mp.frontendbo.adapter.entities.admin.updatepassword;

import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocumentattribute.AdminDeleteDocumentAttributeResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminUpdatePasswordRequest extends AbstractBORequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;
    private AdminUpdatePasswordRequestBody body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminUpdatePasswordRequestBody getBody() {
        return body;
    }

    public void setBody(AdminUpdatePasswordRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PWD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_PASSWORD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String adminUpdatePasswordResult = getAdminServiceRemote().adminUpdatePassword(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getOldPassword(), this.getBody().getNewPassword());

        AdminDeleteDocumentAttributeResponse deleteDocumentAttributeResponse = new AdminDeleteDocumentAttributeResponse();
        status.setStatusCode(adminUpdatePasswordResult);
        status.setStatusMessage(prop.getProperty(adminUpdatePasswordResult));
        deleteDocumentAttributeResponse.setStatus(status);

        return deleteDocumentAttributeResponse;

    }

}
