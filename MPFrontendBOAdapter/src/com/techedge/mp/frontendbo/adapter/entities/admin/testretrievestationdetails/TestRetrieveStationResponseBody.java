package com.techedge.mp.frontendbo.adapter.entities.admin.testretrievestationdetails;

import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;

public class TestRetrieveStationResponseBody {
//    private String stationId;  //Codice della stazione di servizio
//    private String validityDateDetails; //Data di validit� delle informazioni di dettaglio fornite per lo stationId
//    private String address; //Indirizzo
//    private String city; //Citt�
//    private String province; //Provincia
//    private String country; //Nazione
//    private String latitude; //Latitudine
//    private String longitude; //Longitudine
//    /**
//     * @return the stationId
//     */
//    public String getStationId() {
//        return stationId;
//    }
//    /**
//     * @param stationId the stationId to set
//     */
//    public void setStationId(String stationId) {
//        this.stationId = stationId;
//    }
//    /**
//     * @return the validityDateDetails
//     */
//    public String getValidityDateDetails() {
//        return validityDateDetails;
//    }
//    /**
//     * @param validityDateDetails the validityDateDetails to set
//     */
//    public void setValidityDateDetails(String validityDateDetails) {
//        this.validityDateDetails = validityDateDetails;
//    }
//    /**
//     * @return the address
//     */
//    public String getAddress() {
//        return address;
//    }
//    /**
//     * @param address the address to set
//     */
//    public void setAddress(String address) {
//        this.address = address;
//    }
//    /**
//     * @return the city
//     */
//    public String getCity() {
//        return city;
//    }
//    /**
//     * @param city the city to set
//     */
//    public void setCity(String city) {
//        this.city = city;
//    }
//    /**
//     * @return the province
//     */
//    public String getProvince() {
//        return province;
//    }
//    /**
//     * @param province the province to set
//     */
//    public void setProvince(String province) {
//        this.province = province;
//    }
//    /**
//     * @return the country
//     */
//    public String getCountry() {
//        return country;
//    }
//    /**
//     * @param country the country to set
//     */
//    public void setCountry(String country) {
//        this.country = country;
//    }
//    /**
//     * @return the latitude
//     */
//    public String getLatitude() {
//        return latitude;
//    }
//    /**
//     * @param latitude the latitude to set
//     */
//    public void setLatitude(String latitude) {
//        this.latitude = latitude;
//    }
//    /**
//     * @return the longitude
//     */
//    public String getLongitude() {
//        return longitude;
//    }
//    /**
//     * @param longitude the longitude to set
//     */
//    public void setLongitude(String longitude) {
//        this.longitude = longitude;
//    }
//    
    private GetStationDetailsResponse getStationDetailsResponse;

    /**
     * @return the getStationDetailsResponse
     */
    public GetStationDetailsResponse getGetStationDetailsResponse() {
        return getStationDetailsResponse;
    }

    /**
     * @param getStationDetailsResponse the getStationDetailsResponse to set
     */
    public void setGetStationDetailsResponse(GetStationDetailsResponse getStationDetailsResponse) {
        this.getStationDetailsResponse = getStationDetailsResponse;
    }
    
    
}
