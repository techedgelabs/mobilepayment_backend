package com.techedge.mp.frontendbo.adapter.entities.common;

import java.sql.Timestamp;

public class PaymentInfo {
	
	private Long id;
	private String type;
	private String brand;
	private Timestamp expirationDate;
	private String pan;
    private Integer status;
    private String message;
    private String defaultMethod;
    private Integer attemptsLeft;
    private Double checkAmount;
    private Timestamp insertTimestamp;
    private Timestamp verifiedTimestamp;
    private String token;
    private String pin;

    
    public PaymentInfo(){}

	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getBrand() {
		return brand;
	}
	public void setBrand(String brand) {
		this.brand = brand;
	}

	public Timestamp getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(Timestamp expirationDate) {
		this.expirationDate = expirationDate;
	}

	public String getPan() {
		return pan;
	}
	public void setPan(String pan) {
		this.pan = pan;
	}

	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

	public String getDefaultMethod() {
		return defaultMethod;
	}
	public void setDefaultMethod(String defaultMethod) {
		this.defaultMethod = defaultMethod;
	}

	public Integer getAttemptsLeft() {
		return attemptsLeft;
	}
	public void setAttemptsLeft(Integer attemptsLeft) {
		this.attemptsLeft = attemptsLeft;
	}

	public Double getCheckAmount() {
		return checkAmount;
	}
	public void setCheckAmount(Double checkAmount) {
		this.checkAmount = checkAmount;
	}

	public Timestamp getInsertTimestamp() {
		return insertTimestamp;
	}
	public void setInsertTimestamp(Timestamp insertTimestamp) {
		this.insertTimestamp = insertTimestamp;
	}

	public Timestamp getVerifiedTimestamp() {
		return verifiedTimestamp;
	}
	public void setVerifiedTimestamp(Timestamp verifiedTimestamp) {
		this.verifiedTimestamp = verifiedTimestamp;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getPin() {
		return pin;
	}

	public void setPin(String pin) {
		this.pin = pin;
	}

}
