package com.techedge.mp.frontendbo.adapter.entities.admin.createblockperiod;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminCreateBlockPeriodRequest extends AbstractBORequest implements Validable {

    private Status                            status = new Status();

    private Credential                        credential;
    private AdminCreateBlockPeriodBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminCreateBlockPeriodBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminCreateBlockPeriodBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-BLOCK-PERIOD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_BLOCK_PERIOD_CREATE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_BLOCK_PERIOD_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String createBlockPeriodResult = getAdminServiceRemote().adminCreateBlockPeriod(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getCode(), this.getBody().getStartDate(), this.getBody().getEndDate(), this.getBody().getStartTime(), this.getBody().getEndTime(),
                this.getBody().getOperation(), this.getBody().getStatusCode(), this.getBody().getStatusMessage(), this.getBody().getActive());

        AdminCreateBlockPeriodResponse createBlockPeriodResponse = new AdminCreateBlockPeriodResponse();
        status.setStatusCode(createBlockPeriodResult);
        status.setStatusMessage(prop.getProperty(createBlockPeriodResult));
        createBlockPeriodResponse.setStatus(status);

        return createBlockPeriodResponse;
    }
}