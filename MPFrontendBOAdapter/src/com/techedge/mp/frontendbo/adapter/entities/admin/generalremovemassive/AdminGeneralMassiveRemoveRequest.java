package com.techedge.mp.frontendbo.adapter.entities.admin.generalremovemassive;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminGeneralMassiveRemoveRequest extends AbstractBORequest implements Validable {

    private Status                        status = new Status();

    private Credential                    credential;
    private AdminGeneralMassiveRemoveBodyRequest body;

    public AdminGeneralMassiveRemoveRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminGeneralMassiveRemoveBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminGeneralMassiveRemoveBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("MASSIVE-REMOVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_MASSIVE_REMOVE_INVALID_BODY);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_MASSIVE_REMOVE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        AdminGeneralMassiveRemoveResponse adminGeneralMassiveRemoveResponse = new AdminGeneralMassiveRemoveResponse();

        String response = getAdminServiceRemote().adminGeneralMassiveDelete(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getType(),
                this.getBody().getIdFrom(), this.getBody().getIdTo());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        adminGeneralMassiveRemoveResponse.setStatus(status);

        return adminGeneralMassiveRemoveResponse;
    }

}
