package com.techedge.mp.frontendbo.adapter.entities.admin.testloadloyaltycredits;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestLoadLoyaltyCreditsResponse extends BaseResponse{

    private String  csTransactionID;
    private Integer credits;
    private Integer balance;
    private Double  balanceAmount;
    private String  cardCodeIssuer;
    private String  eanCode;
    private String  cardStatus;
    private String  cardType;
    private String  cardClassification;
    private String  marketingMsg;

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public Integer getCredits() {
        return credits;
    }

    public void setCredits(Integer credits) {
        this.credits = credits;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Double getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(Double balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    public String getCardCodeIssuer() {
        return cardCodeIssuer;
    }

    public void setCardCodeIssuer(String cardCodeIssuer) {
        this.cardCodeIssuer = cardCodeIssuer;
    }

    public String getEanCode() {
        return eanCode;
    }

    public void setEanCode(String eanCode) {
        this.eanCode = eanCode;
    }

    public String getCardStatus() {
        return cardStatus;
    }

    public void setCardStatus(String cardStatus) {
        this.cardStatus = cardStatus;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    public String getCardClassification() {
        return cardClassification;
    }

    public void setCardClassification(String cardClassification) {
        this.cardClassification = cardClassification;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

}
