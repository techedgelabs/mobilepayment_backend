package com.techedge.mp.frontendbo.adapter.entities.admin.parkingretrieveparkingzones;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminRetrieveParkingZonesResponse extends BaseResponse{
    
    private AdminRetrieveParkingZonesResponseBody body;

    public AdminRetrieveParkingZonesResponseBody getBody() {
        return body;
    }

    public void setBody(AdminRetrieveParkingZonesResponseBody body) {
        this.body = body;
    }
}
