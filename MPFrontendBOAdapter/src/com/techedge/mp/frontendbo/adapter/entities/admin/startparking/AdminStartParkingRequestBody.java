package com.techedge.mp.frontendbo.adapter.entities.admin.startparking;

import java.util.Date;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminStartParkingRequestBody implements Validable {

    private String lang;
    private String plateNumber;
    private Date   requestedEndTime;
    private String parkingZoneId;
    private String stallCode;
    private String clientOperationID;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public Date getRequestedEndTime() {
        return requestedEndTime;
    }

    public void setRequestedEndTime(Date requestedEndTime) {
        this.requestedEndTime = requestedEndTime;
    }

    public String getParkingZoneId() {
        return parkingZoneId;
    }

    public void setParkingZoneId(String parkingZoneId) {
        this.parkingZoneId = parkingZoneId;
    }

    public String getStallCode() {
        return stallCode;
    }

    public void setStallCode(String stallCode) {
        this.stallCode = stallCode;
    }
    
    public String getClientOperationID() {
        return clientOperationID;
    }

    public void setClientOperationID(String clientOperationID) {
        this.clientOperationID = clientOperationID;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);
        return status;
    }

}
