package com.techedge.mp.frontendbo.adapter.entities.admin.testexecutecapture;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteCaptureResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecutePaymentResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PaymentRefuelMode;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductCodeEnum;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestExecuteCaptureRequest extends AbstractBORequest implements Validable {

    private Credential                    credential;
    private TestExecuteCaptureBodyRequest body;

    public TestExecuteCaptureRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestExecuteCaptureBodyRequest getBody() {
        return body;
    }

    public void setBody(TestExecuteCaptureBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("EXECUTE-CAPTURE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_EXECUTE_CAPTURE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestExecuteCaptureResponse testExecuteCaptureResponse = new TestExecuteCaptureResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testExecuteCapture");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testExecuteCaptureResponse.getStatus().setStatusCode("-1");
        }
        else {
            ExecuteCaptureResult executeCaptureResult = new ExecuteCaptureResult();
            
            try {
                PaymentRefuelMode paymentRefuelMode = getEnumerationValue(PaymentRefuelMode.class, this.getBody().getRefuelMode());
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());
                ProductCodeEnum productCode = getEnumerationValue(ProductCodeEnum.class, this.getBody().getProductCode());

                executeCaptureResult = getPaymentServiceRemote().executeCapture(this.getBody().getMcCardDpan(), this.getBody().getMessageReasonCode(), this.getBody().getRetrievalRefNumber(), this.getBody().getAuthCode(),
                		this.getBody().getCurrencyCode(), paymentRefuelMode, this.getBody().getOperationId(), this.getBody().getAmount(), productCode, this.getBody().getQuantity(), this.getBody().getUnitPrice(), partnerType, this.getBody().getRequestTimestamp());
            }
            catch (Exception ex) {
                executeCaptureResult = null;
            }

            testExecuteCaptureResponse.setStatusCode("OK");
            testExecuteCaptureResponse.setResult(executeCaptureResult);
        }

        return testExecuteCaptureResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
