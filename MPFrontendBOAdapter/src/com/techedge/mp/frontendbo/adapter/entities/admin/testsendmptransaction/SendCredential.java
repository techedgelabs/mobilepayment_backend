package com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction;

public class SendCredential {
    
    private String requestID;
    
    public SendCredential(){}

    public String getRequestID() {
        return requestID;
    }

    public void setRequestID(String requestID) {
        this.requestID = requestID;
    }
    
}
