package com.techedge.mp.frontendbo.adapter.entities.admin.createsurvey;

import java.util.ArrayList;
import java.util.Date;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.SurveyQuestion;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class CreateSurveyRequest extends AbstractBORequest implements Validable {

    private Status                  status = new Status();

    private Credential              credential;
    private CreateSurveyBodyRequest body;

    public CreateSurveyRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateSurveyBodyRequest getBody() {
        return body;
    }

    public void setBody(CreateSurveyBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-SURVEY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_SURVEY_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        CreateSurveyBodyRequest createSurveyBodyRequest = this.getBody();
        CreateSurveyResponse createSurveyResponse = new CreateSurveyResponse();

        ArrayList<com.techedge.mp.core.business.interfaces.SurveyQuestion> questions = new ArrayList<com.techedge.mp.core.business.interfaces.SurveyQuestion>(0);

        for (SurveyQuestion question : createSurveyBodyRequest.getQuestions()) {
            com.techedge.mp.core.business.interfaces.SurveyQuestion surveyQuestion = new com.techedge.mp.core.business.interfaces.SurveyQuestion();
            surveyQuestion.setCode(question.getCode());
            surveyQuestion.setText(question.getText());
            surveyQuestion.setType(question.getType());
            surveyQuestion.setSequence(question.getSequence());
            questions.add(surveyQuestion);
        }

        Date startDate = new Date(createSurveyBodyRequest.getStartDate());
        Date endDate = new Date(createSurveyBodyRequest.getEndDate());

        String response = getAdminServiceRemote().adminSurveyCreate(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                createSurveyBodyRequest.getCode(), createSurveyBodyRequest.getDescription(), createSurveyBodyRequest.getNote(), createSurveyBodyRequest.getStatus(), startDate,
                endDate, questions);

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        createSurveyResponse.setStatus(status);

        return createSurveyResponse;
    }
}
