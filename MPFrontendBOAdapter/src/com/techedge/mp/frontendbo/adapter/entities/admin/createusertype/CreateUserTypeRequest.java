package com.techedge.mp.frontendbo.adapter.entities.admin.createusertype;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class CreateUserTypeRequest extends AbstractBORequest implements Validable {

    private Status                    status = new Status();

    private Credential                credential;
    private CreateUserTypeBodyRequest body;

    public CreateUserTypeRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateUserTypeBodyRequest getBody() {
        return body;
    }

    public void setBody(CreateUserTypeBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-USER-TYPE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_USER_TYPE_CREATE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_USER_TYPE_CREATE);

        return status;

    }

    @Override
    public BaseResponse execute() {

        CreateUserTypeResponse createUserTypeResponse = new CreateUserTypeResponse();

        String response = getAdminServiceRemote().adminCreateUserType(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getCode());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        createUserTypeResponse.setStatus(status);

        return createUserTypeResponse;

    }

}
