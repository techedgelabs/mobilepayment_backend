package com.techedge.mp.frontendbo.adapter.entities.admin.propagationuserdata;

import java.util.List;

import com.techedge.mp.core.business.interfaces.AdminPropagationUserDataResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminPropagationUserDataRequest extends AbstractBORequest implements Validable {

    private Credential                          credential;
    private AdminPropagationUserDataBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminPropagationUserDataBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminPropagationUserDataBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("PROPAGATION-USERDATA", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_PROPAGATION_USERDATA_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_PROPAGATION_USERDATA_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        AdminPropagationUserDataResponse response = new AdminPropagationUserDataResponse();

        List<AdminPropagationUserDataResult> resultList = getAdminServiceRemote().adminPropagationUserData(this.getCredential().getTicketID(), this.getCredential().getRequestID(), 
                this.getBody().getUserID(), this.getBody().getFiscalCode());

        
        String statusCode = StatusCode.ADMIN_PROPAGATION_USERDATA_SUCCESS;
        
        for (AdminPropagationUserDataResult result: resultList) {
            if (result != null && !result.getStatusCode().equals(StatusCode.ADMIN_PROPAGATION_USERDATA_SUCCESS)) {
                statusCode = StatusCode.ADMIN_PROPAGATION_USERDATA_FAILURE;
                break;
            }
        }
        
        response.setResults(resultList);
        
        Status status = new Status();
        status.setStatusCode(statusCode);
        status.setStatusMessage(prop.getProperty(statusCode));

        response.setStatus(status);

        return response;
    }

}