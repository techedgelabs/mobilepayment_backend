package com.techedge.mp.frontendbo.adapter.entities.admin.removeusertypefromusercategory;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RemoveUserTypeFromUserCategoryBodyRequest implements Validable {

    private String  name;
    private Integer code;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public RemoveUserTypeFromUserCategoryBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.name == null || this.name.isEmpty() || this.code == null) {

            status.setStatusCode(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE);

        return status;
    }

}
