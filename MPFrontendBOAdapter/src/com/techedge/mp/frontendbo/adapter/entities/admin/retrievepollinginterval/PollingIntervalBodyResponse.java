package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepollinginterval;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.PollingInterval;

public class PollingIntervalBodyResponse {

    private List<PollingInterval> pollingIntervalList = new ArrayList<PollingInterval>(0);

    public List<PollingInterval> getPollingIntervalList() {
        return pollingIntervalList;
    }

    public void setPollingIntervalList(List<PollingInterval> pollingIntervalList) {
        this.pollingIntervalList = pollingIntervalList;
    }

}
