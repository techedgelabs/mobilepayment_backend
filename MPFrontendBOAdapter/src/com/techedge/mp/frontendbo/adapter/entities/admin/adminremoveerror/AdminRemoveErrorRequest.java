package com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminRemoveErrorRequest extends AbstractBORequest implements Validable {

    private Status                      status = new Status();

    private Credential                  credential;
    private AdminRemoveErrorBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminRemoveErrorBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminRemoveErrorBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("MAPPING-ERROR-REMOVE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_REMOVE__INVALID_ERROR_CODE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_REMOVE__SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        AdminRemoveErrorResponse adminRemoveErrorResponse = new AdminRemoveErrorResponse();

        String response = getAdminServiceRemote().adminRemoveMappingError(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), this.getBody().getGpErrorCode());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        adminRemoveErrorResponse.setStatus(status);

        return adminRemoveErrorResponse;
    }

}
