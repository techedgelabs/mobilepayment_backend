package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.UserInfo;


public class RetrieveUsersResponse extends BaseResponse {
	
	List<UserInfo> users = new ArrayList<UserInfo>(0);

	public List<UserInfo> getUsers() {
		return users;
	}
	public void setUsers(List<UserInfo> users) {
		this.users = users;
	}
}
