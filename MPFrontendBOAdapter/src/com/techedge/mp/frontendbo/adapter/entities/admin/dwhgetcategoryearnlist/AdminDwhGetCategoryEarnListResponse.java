package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcategoryearnlist;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminDwhGetCategoryEarnListResponse extends BaseResponse {

    private AdminDwhGetCategoryEarnListBodyResponse body;

    public AdminDwhGetCategoryEarnListBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminDwhGetCategoryEarnListBodyResponse body) {
        this.body = body;
    }

}