package com.techedge.mp.frontendbo.adapter.entities.admin.admineditmappingerror;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminEditErrorRequest extends AbstractBORequest implements Validable {

    private Status                    status = new Status();

    private AdminEditErrorBodyRequest body;
    private Credential                credential;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminEditErrorBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminEditErrorBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("MAPPING-ERROR-EDIT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_EDIT__INVALID_ERROR_CODE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_MAPPING_ERROR_EDIT__SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        AdminEditErrorResponse adminEditErrorResponse = new AdminEditErrorResponse();

        String response = getAdminServiceRemote().adminEditMappingError(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getGpErrorCode(),
                this.getBody().getMpStatusCode());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        adminEditErrorResponse.setStatus(status);

        return adminEditErrorResponse;
    }

}
