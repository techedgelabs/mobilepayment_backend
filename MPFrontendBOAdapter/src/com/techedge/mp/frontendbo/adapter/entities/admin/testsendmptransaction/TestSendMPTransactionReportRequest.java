package com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.MpTransactionDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.RefuelDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestSendMPTransactionReportRequest extends AbstractBORequest implements Validable {

    private SendCredential                         credential;
    private TestSendMPTransactionReportBodyRequest body;

    public TestSendMPTransactionReportRequest() {}

    public SendCredential getSendCredential() {
        return credential;
    }

    public void setSendCredential(SendCredential credential) {
        this.credential = credential;
    }

    public TestSendMPTransactionReportBodyRequest getBody() {

        return body;
    }

    public void setBody(TestSendMPTransactionReportBodyRequest body) {

        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_FIDELITY_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestSendMPTransactionResponse testSendMPTransactionResponse = new TestSendMPTransactionResponse();

        String requestID = this.getSendCredential().getRequestID();
        String startDate = this.getBody().getStartDate();
        String endDate = this.getBody().getEndDate();
        MpTransactionDetail[] mpTransactionArray = this.getBody().getMptransactionList();
        List<com.techedge.mp.refueling.integration.entities.MpTransactionDetail> mpTransactionList = new ArrayList<com.techedge.mp.refueling.integration.entities.MpTransactionDetail>();

        for (int i = 0; i < mpTransactionArray.length; i++) {
            com.techedge.mp.refueling.integration.entities.MpTransactionDetail mpTransactionDeatail = new com.techedge.mp.refueling.integration.entities.MpTransactionDetail();
            mpTransactionDeatail.setMpTransactionID(mpTransactionArray[i].getMpTransactionID());
            mpTransactionDeatail.setMpTransactionStatus(mpTransactionArray[i].getMpTransactionStatus());
            mpTransactionDeatail.setSrcTransactionID(mpTransactionArray[i].getSrcTransactionID());
            com.techedge.mp.refueling.integration.entities.RefuelDetail refuelDetail = convertRefueling(mpTransactionArray[i].getRefuelDetail());
            mpTransactionDeatail.setRefuelDetail(refuelDetail);
            mpTransactionList.add(mpTransactionDeatail);
        }

        String statusCode = getRefuelingNotificationServiceRemote().sendMPTransactionReport(requestID, startDate, endDate, mpTransactionList);

        testSendMPTransactionResponse.getStatus().setStatusCode(statusCode);
        testSendMPTransactionResponse.getStatus().setStatusMessage("");

        return testSendMPTransactionResponse;
    }

    private com.techedge.mp.refueling.integration.entities.RefuelDetail convertRefueling(RefuelDetail refuelDetailRequest) {

        com.techedge.mp.refueling.integration.entities.RefuelDetail refuelDetail = new com.techedge.mp.refueling.integration.entities.RefuelDetail();
        refuelDetail.setAmount(refuelDetailRequest.getAmount());
        refuelDetail.setFuelQuantity(refuelDetailRequest.getFuelQuantity());
        refuelDetail.setFuelType(refuelDetailRequest.getFuelType());
        refuelDetail.setProductDescription(refuelDetailRequest.getProductDescription());
        refuelDetail.setProductID(refuelDetailRequest.getProductId());
        refuelDetail.setTimestampEndRefuel(refuelDetailRequest.getTimestampEndRefuel());
        refuelDetail.setTimestampStartRefuel(refuelDetailRequest.getTimestampStartRefuel());
        return refuelDetail;
    }
}
