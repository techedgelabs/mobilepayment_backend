package com.techedge.mp.frontendbo.adapter.entities.common;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TransactionInfo {
	
	private Long id;
	private String transactionId;
	private Long userId;
	private String stationId;
	private Timestamp creationTimestamp;
	private Timestamp endRefuelTimestamp;
	private Double initialAmount;
	private Double finalAmount;
	private Double fuelQuantity;
	private Double fuelAmount;
	private String pumpId;
	private Integer pumpNumber;
	private String currency;
	private String shopLogin;
	private String acquirerId;
	private String bankTransactionId;
	private String authorizationCode;
	private String token;
	private String paymentType;
	private String finalStatusType;
	private String GFGnotification;
	private String confirmed;
	private Long paymentMethodId;
	private String paymentMethodType;
	private String serverName;
	private Integer statusAttemptsLeft;
	private String productID;
	private String productDescription;
	private String transactionCategory;
	List<TransactionStatusInfo> transactionStatusList = new ArrayList<TransactionStatusInfo>(0);
	List<TransactionEventInfo> transactionEventList = new ArrayList<TransactionEventInfo>(0);
	private Date archivingDate;
	private Integer reconciliationAttemptsLeft;
	

	public TransactionInfo() {}


	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}

	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public Long getUserId() {
		return userId;
	}
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getStationId() {
		return stationId;
	}
	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public Timestamp getCreationTimestamp() {
		return creationTimestamp;
	}
	public void setCreationTimestamp(Timestamp creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public Timestamp getEndRefuelTimestamp() {
		return endRefuelTimestamp;
	}
	public void setEndRefuelTimestamp(Timestamp endRefuelTimestamp) {
		this.endRefuelTimestamp = endRefuelTimestamp;
	}

	public Double getInitialAmount() {
		return initialAmount;
	}
	public void setInitialAmount(Double initialAmount) {
		this.initialAmount = initialAmount;
	}

	public Double getFinalAmount() {
		return finalAmount;
	}
	public void setFinalAmount(Double finalAmount) {
		this.finalAmount = finalAmount;
	}

	public Double getFuelQuantity() {
		return fuelQuantity;
	}
	public void setFuelQuantity(Double fuelQuantity) {
		this.fuelQuantity = fuelQuantity;
	}

	public Double getFuelAmount() {
		return fuelAmount;
	}
	public void setFuelAmount(Double fuelAmount) {
		this.fuelAmount = fuelAmount;
	}

	public String getPumpId() {
		return pumpId;
	}
	public void setPumpId(String pumpId) {
		this.pumpId = pumpId;
	}

	public Integer getPumpNumber() {
		return pumpNumber;
	}
	public void setPumpNumber(Integer pumpNumber) {
		this.pumpNumber = pumpNumber;
	}

	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getShopLogin() {
		return shopLogin;
	}
	public void setShopLogin(String shopLogin) {
		this.shopLogin = shopLogin;
	}

	public String getAcquirerId() {
		return acquirerId;
	}
	public void setAcquirerId(String acquirerId) {
		this.acquirerId = acquirerId;
	}

	public String getBankTransactionId() {
		return bankTransactionId;
	}
	public void setBankTransactionId(String bankTransactionId) {
		this.bankTransactionId = bankTransactionId;
	}

	public String getAuthorizationCode() {
		return authorizationCode;
	}
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}

	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}

	public String getPaymentType() {
		return paymentType;
	}
	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}

	public String getFinalStatusType() {
		return finalStatusType;
	}
	public void setFinalStatusType(String finalStatusType) {
		this.finalStatusType = finalStatusType;
	}

	public String getGFGnotification() {
		return GFGnotification;
	}
	public void setGFGnotification(String gFGnotification) {
		GFGnotification = gFGnotification;
	}

	public String getConfirmed() {
		return confirmed;
	}
	public void setConfirmed(String confirmed) {
		this.confirmed = confirmed;
	}

	public Long getPaymentMethodId() {
		return paymentMethodId;
	}
	public void setPaymentMethodId(Long paymentMethodId) {
		this.paymentMethodId = paymentMethodId;
	}

	public String getPaymentMethodType() {
		return paymentMethodType;
	}
	public void setPaymentMethodType(String paymentMethodType) {
		this.paymentMethodType = paymentMethodType;
	}

	public String getServerName() {
		return serverName;
	}
	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public Integer getStatusAttemptsLeft() {
		return statusAttemptsLeft;
	}
	public void setStatusAttemptsLeft(Integer statusAttemptsLeft) {
		this.statusAttemptsLeft = statusAttemptsLeft;
	}

	public List<TransactionStatusInfo> getTransactionStatusList() {
		return transactionStatusList;
	}
	public void setTransactionStatusList(
			List<TransactionStatusInfo> transactionStatusList) {
		this.transactionStatusList = transactionStatusList;
	}

	public List<TransactionEventInfo> getTransactionEventList() {
		return transactionEventList;
	}
	public void setTransactionEventList(
			List<TransactionEventInfo> transactionEventList) {
		this.transactionEventList = transactionEventList;
	}

	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}

	public String getProductDescription() {
		return productDescription;
	}
	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Date getArchivingDate() {
		return archivingDate;
	}
	public void setArchivingDate(Date archivingDate) {
		this.archivingDate = archivingDate;
	}

    public String getTransactionCategory() {
        return transactionCategory;
    }
    public void setTransactionCategory(String transactionCategory) {
        this.transactionCategory = transactionCategory;
    }

    public Integer getReconciliationAttemptsLeft() {
        return reconciliationAttemptsLeft;
    }
    public void setReconciliationAttemptsLeft(Integer reconciliationAttemptsLeft) {
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
    }
    
}
