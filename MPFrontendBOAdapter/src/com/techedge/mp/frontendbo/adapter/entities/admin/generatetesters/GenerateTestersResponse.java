package com.techedge.mp.frontendbo.adapter.entities.admin.generatetesters;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.UserConfig;


public class GenerateTestersResponse extends BaseResponse {
	
	List<UserConfig> testers = new ArrayList<UserConfig>(0);

	public List<UserConfig> getTesters() {
		return testers;
	}
	public void setTesters(List<UserConfig> testers) {
		this.testers = testers;
	}
}
