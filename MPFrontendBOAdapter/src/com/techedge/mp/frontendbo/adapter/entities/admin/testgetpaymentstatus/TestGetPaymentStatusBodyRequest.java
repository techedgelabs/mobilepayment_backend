package com.techedge.mp.frontendbo.adapter.entities.admin.testgetpaymentstatus;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestGetPaymentStatusBodyRequest implements Validable {


    private String  paymentOperationId;
    private String  partnerType;
    private Long    requestTimestamp;
    private String  operationId;


	public String getPaymentOperationId() {
		return paymentOperationId;
	}



	public void setPaymentOperationId(String paymentOperationId) {
		this.paymentOperationId = paymentOperationId;
	}



	public String getPartnerType() {
		return partnerType;
	}



	public void setPartnerType(String partnerType) {
		this.partnerType = partnerType;
	}



	public Long getRequestTimestamp() {
		return requestTimestamp;
	}



	public void setRequestTimestamp(Long requestTimestamp) {
		this.requestTimestamp = requestTimestamp;
	}



	public String getOperationId() {
		return operationId;
	}



	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}



	@Override
    public Status check() {

        Status status = new Status();

        if (this.paymentOperationId == null || this.paymentOperationId.trim().isEmpty() || this.partnerType == null || this.partnerType.trim().isEmpty() || this.requestTimestamp == null || this.operationId == null || this.operationId.trim().isEmpty()) {
        	
            status.setStatusCode(StatusCode.ADMIN_TEST_GET_PAYMENT_STATUS_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_GET_PAYMENT_STATUS_SUCCESS);

        return status;
    }

}
