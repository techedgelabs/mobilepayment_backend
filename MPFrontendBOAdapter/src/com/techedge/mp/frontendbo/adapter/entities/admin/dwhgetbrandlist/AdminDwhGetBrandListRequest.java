package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetbrandlist;

import java.util.Date;

import com.techedge.mp.dwh.adapter.interfaces.BrandDetail;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetBrandResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminDwhGetBrandListRequest extends AbstractBORequest implements Validable {

    private Status                              status = new Status();

    private Credential                          credential;

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DWH-GET-BRAND-LIST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DWH_GET_BRAND_LIST_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        Date now = new Date();

        String requestId = String.valueOf(now.getTime());
        Long requestTimestamp = now.getTime();
        
        DWHGetBrandResult result = getDwhAdapterServiceRemote().getBrandList(requestId, requestTimestamp);

        AdminDwhGetBrandListResponse adminDwhGetBrandListResponse = new AdminDwhGetBrandListResponse();
        
        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(result.getMessageCode());
        adminDwhGetBrandListResponse.setStatus(status);

        if (result.getBrandDetailList() != null && !result.getBrandDetailList().isEmpty()) {
            
            AdminDwhGetBrandListBodyResponse adminDwhGetBrandListBodyResponse = new AdminDwhGetBrandListBodyResponse();
            
            for(BrandDetail brandDetail : result.getBrandDetailList()) {
                
                adminDwhGetBrandListBodyResponse.getBrandDetailList().add(brandDetail);
            }
            
            adminDwhGetBrandListResponse.setBody(adminDwhGetBrandListBodyResponse);
        }
        
        return adminDwhGetBrandListResponse;
    }

}