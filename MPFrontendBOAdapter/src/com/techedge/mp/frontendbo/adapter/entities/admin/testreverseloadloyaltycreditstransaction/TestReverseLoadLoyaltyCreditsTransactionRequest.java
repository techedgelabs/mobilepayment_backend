package com.techedge.mp.frontendbo.adapter.entities.admin.testreverseloadloyaltycreditstransaction;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestReverseLoadLoyaltyCreditsTransactionRequest extends AbstractBORequest implements Validable {

    private Credential                                          credential;
    private TestReverseLoadLoyaltyCreditsTransactionBodyRequest body;

    public TestReverseLoadLoyaltyCreditsTransactionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestReverseLoadLoyaltyCreditsTransactionBodyRequest getBody() {
        return body;
    }

    public void setBody(TestReverseLoadLoyaltyCreditsTransactionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REVERSE-LOAD-LOYALTY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestReverseLoadLoyaltyCreditsTransactionResponse testReverseLoadLoyaltyCreditsTransactionResponse = new TestReverseLoadLoyaltyCreditsTransactionResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(),
                "testReverseLoadLoyaltyCreditsTransaction");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testReverseLoadLoyaltyCreditsTransactionResponse.getStatus().setStatusCode("-1");
        }
        else {

            ReverseLoadLoyaltyCreditsTransactionResult reverseLoadLoyaltyCreditsTransactionResult = new ReverseLoadLoyaltyCreditsTransactionResult();

            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                reverseLoadLoyaltyCreditsTransactionResult = getFidelityServiceRemote().reverseLoadLoyaltyCreditsTransaction(
                        this.getBody().getOperationID(),
                        this.getBody().getOperationIDtoReverse(), partnerType,
                        this.getBody().getRequestTimestamp());
            }
            catch (FidelityServiceException ex) {
                reverseLoadLoyaltyCreditsTransactionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!reverseLoadLoyaltyCreditsTransactionResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                testReverseLoadLoyaltyCreditsTransactionResponse.setCsTransactionID(reverseLoadLoyaltyCreditsTransactionResult.getCsTransactionID());
                testReverseLoadLoyaltyCreditsTransactionResponse.getStatus().setStatusMessage(reverseLoadLoyaltyCreditsTransactionResult.getMessageCode());
                testReverseLoadLoyaltyCreditsTransactionResponse.getStatus().setStatusCode(reverseLoadLoyaltyCreditsTransactionResult.getStatusCode());
            }
        }

        return testReverseLoadLoyaltyCreditsTransactionResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
