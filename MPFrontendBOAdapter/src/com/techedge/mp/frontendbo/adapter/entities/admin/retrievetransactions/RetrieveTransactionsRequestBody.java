package com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions;

import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RetrieveTransactionsRequestBody implements Validable {
	
	private String transactionId;		
	private String userId;
	private String stationId;
	private String pumpId;
	private String finalStatusType;
	private Boolean GFGNotification;
	private Boolean confirmed;
	private Long creationTimestampStart;
	private Long creationTimestampEnd;
	private String productID;
	private Boolean transactionHistoryFlag;
	
	
	public String getTransactionId() {
		return transactionId;
	}
	public void setTransactionId(String transactionId) {
		this.transactionId = transactionId;
	}

	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getStationId() {
		return stationId;
	}
	public void setStationId(String stationId) {
		this.stationId = stationId;
	}

	public String getPumpId() {
		return pumpId;
	}
	public void setPumpId(String pumpId) {
		this.pumpId = pumpId;
	}

	public String getFinalStatusType() {
		return finalStatusType;
	}
	public void setFinalStatusType(String finalStatusType) {
		this.finalStatusType = finalStatusType;
	}

	public Boolean getGFGNotification() {
		return GFGNotification;
	}
	public void setGFGNotification(Boolean gFGNotification) {
		GFGNotification = gFGNotification;
	}

	public Boolean getConfirmed() {
		return confirmed;
	}
	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}

	public Long getCreationTimestampStart() {
		return creationTimestampStart;
	}
	public void setCreationTimestampStart(Long creationTimestampStart) {
		this.creationTimestampStart = creationTimestampStart;
	}
	
	public Long getCreationTimestampEnd() {
		return creationTimestampEnd;
	}
	public void setCreationTimestampEnd(Long creationTimestampEnd) {
		this.creationTimestampEnd = creationTimestampEnd;
	}
	
	public String getProductID() {
		return productID;
	}
	public void setProductID(String productID) {
		this.productID = productID;
	}


	public Boolean getTransactionHistoryFlag() {
		return transactionHistoryFlag;
	}
	public void setTransactionHistoryFlag(Boolean transactionHistoryFlag) {
		this.transactionHistoryFlag = transactionHistoryFlag;
	}
	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.transactionId != null && this.transactionId.length() > 40) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.userId != null && this.userId.length() > 10) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.stationId != null && this.stationId.length() > 10) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.pumpId != null && this.pumpId.length() > 10) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.finalStatusType != null &&
				( !this.finalStatusType.equals(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL) &&
				  !this.finalStatusType.equals(StatusHelper.FINAL_STATUS_TYPE_FAILED) &&
				  !this.finalStatusType.equals(StatusHelper.FINAL_STATUS_TYPE_ERROR) &&
				  !this.finalStatusType.equals(StatusHelper.FINAL_STATUS_TYPE_RUNNING) &&
				  !this.finalStatusType.equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_NOTIFICATION) &&
				  !this.finalStatusType.equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT) &&
				  !this.finalStatusType.equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL) &&
				  !this.finalStatusType.equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_AFTER_REFUEL) &&
                  !this.finalStatusType.equals(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE) &&
                  !this.finalStatusType.equals(StatusHelper.FINAL_STATUS_TYPE_ABEND) ) ) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.productID != null && this.productID.length() > 50) {
			
			status.setStatusCode(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS);
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_RETRIEVE_USERS_SUCCESS);

		return status;
	}
	
}
