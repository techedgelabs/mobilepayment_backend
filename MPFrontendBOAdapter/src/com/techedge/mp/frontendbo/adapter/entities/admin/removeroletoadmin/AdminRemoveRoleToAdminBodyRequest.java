package com.techedge.mp.frontendbo.adapter.entities.admin.removeroletoadmin;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminRemoveRoleToAdminBodyRequest implements Validable {

    private String role;
    private String admin;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getAdmin() {
        return admin;
    }

    public void setAdmin(String admin) {
        this.admin = admin;
    }

    public AdminRemoveRoleToAdminBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.admin == null || this.admin.isEmpty() || this.role == null || this.role.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_ADMIN_ROLE_REMOVE_CHECK_FAILURE);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_ADMIN_ROLE_REMOVE_SUCCESS);

        return status;
    }

}
