package com.techedge.mp.frontendbo.adapter.entities.admin.parkingretrieveparkingzones;

import java.math.BigDecimal;

import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.RetrieveParkingZonesResult;

public class AdminRetrieveParkingZonesRequest extends AbstractBORequest implements Validable {

    private Credential                     credential;
    
    private AdminRetrieveParkingZonesRequestBody body;

    public AdminRetrieveParkingZonesRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminRetrieveParkingZonesRequestBody getBody() {
        return body;
    }

    public void setBody(AdminRetrieveParkingZonesRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-USERS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        AdminRetrieveParkingZonesResponse adminRetrieveParkingZonesResponse = new AdminRetrieveParkingZonesResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testRetrieveStation");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            adminRetrieveParkingZonesResponse.getStatus().setStatusCode("-1");
        }
        else {

            ParkingServiceRemote parkingService;
            
            try {
                parkingService = EJBHomeCache.getInstance().getParkingService();

                String lang = this.getBody().getLang();
                BigDecimal latitude  = new BigDecimal(this.getBody().getLatitude());
                BigDecimal longitude = new BigDecimal(this.getBody().getLongitude());
                BigDecimal accuracy  = new BigDecimal(this.getBody().getAccuracy());
                
                RetrieveParkingZonesResult retrieveParkingZonesResult = parkingService.retrieveParkingZones(lang, latitude, longitude, accuracy);
                
                
                AdminRetrieveParkingZonesResponseBody adminRetrieveParkingZonesResponseBody = new AdminRetrieveParkingZonesResponseBody();
                adminRetrieveParkingZonesResponseBody.setRetrieveParkingZonesResult(retrieveParkingZonesResult);

                adminRetrieveParkingZonesResponse.setBody(adminRetrieveParkingZonesResponseBody);;
                adminRetrieveParkingZonesResponse.getStatus().setStatusCode(retrieveParkingZonesResult.getStatusCode());
            }
            catch (InterfaceNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return adminRetrieveParkingZonesResponse;
    }
}
