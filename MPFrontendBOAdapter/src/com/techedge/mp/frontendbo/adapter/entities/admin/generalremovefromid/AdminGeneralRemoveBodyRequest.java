package com.techedge.mp.frontendbo.adapter.entities.admin.generalremovefromid;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminGeneralRemoveBodyRequest implements Validable {

    private String id;
    private String type;

    public AdminGeneralRemoveBodyRequest() {}

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.id == null || this.id.isEmpty() || this.type == null || this.id.isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_REMOVE_FAILURE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_REMOVE_SUCCESS);

        return status;
    }

}