package com.techedge.mp.frontendbo.adapter.entities.admin.removeemaildomain;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class GetEmailDomainRequest extends AbstractBORequest implements Validable {

    private Status                    status = new Status();

    private Credential                credential;
    private GetEmailDomainBodyRequest body;

    public GetEmailDomainRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public GetEmailDomainBodyRequest getBody() {
        return body;
    }

    public void setBody(GetEmailDomainBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.MAIL_GET_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        GetEmailDomainResponse getEmailDomainResponse = new GetEmailDomainResponse();

        List<String> response = new ArrayList<String>();
        response = getAdminServiceRemote().adminGetEmailDomainInBlackList(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getEmail());

        getEmailDomainResponse.setListaEmail(response);
        status.setStatusCode(ResponseHelper.MAIL_GET_SUCCESS);
        status.setStatusMessage(prop.getProperty(ResponseHelper.MAIL_GET_SUCCESS));
        getEmailDomainResponse.setStatus(status);

        return getEmailDomainResponse;
    }
}
