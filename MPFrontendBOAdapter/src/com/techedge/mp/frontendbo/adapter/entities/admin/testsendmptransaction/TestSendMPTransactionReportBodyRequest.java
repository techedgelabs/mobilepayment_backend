package com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction;

import com.techedge.mp.frontendbo.adapter.entities.common.MpTransactionDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestSendMPTransactionReportBodyRequest implements Validable {

    private String                startDate;
    private String                endDate;
    private MpTransactionDetail[] mptransactionList = new MpTransactionDetail[0];

    public String getStartDate() {

        return startDate;
    }

    public String getEndDate() {

        return endDate;
    }

    public MpTransactionDetail[] getMptransactionList() {

        return mptransactionList;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public void setMptransactionList(MpTransactionDetail[] mptransactionList) {
        this.mptransactionList = mptransactionList;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.mptransactionList == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_FIDELITY_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_FIDELITY_SUCCESS);

        return status;
    }

}
