package com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminDeleteDocumentRequest extends AbstractBORequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;
    private AdminDeleteDocumentBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminDeleteDocumentBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminDeleteDocumentBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DELETE-DOCUMENT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_DOCUMENT_DELETE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_DELETE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String deleteDocumentResult = getAdminServiceRemote().adminDeleteDocument(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getDocumentKey());

        AdminDeleteDocumentResponse deleteDocumentResponse = new AdminDeleteDocumentResponse();
        status.setStatusCode(deleteDocumentResult);
        status.setStatusMessage(prop.getProperty(deleteDocumentResult));
        deleteDocumentResponse.setStatus(status);

        return deleteDocumentResponse;
    }

}