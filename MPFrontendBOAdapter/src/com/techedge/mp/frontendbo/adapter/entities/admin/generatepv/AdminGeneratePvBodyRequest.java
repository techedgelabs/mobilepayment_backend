package com.techedge.mp.frontendbo.adapter.entities.admin.generatepv;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.StationData;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminGeneratePvBodyRequest implements Validable {

    private Integer           finalStatus;

    private Boolean           skipCheck;

    private String            noOilAcquirerId;
    private String            noOilShopLogin;
    private String            oilAcquirerId;
    private String            oilShopLogin;

    private List<StationData> stationList = new ArrayList<StationData>(0);

    public Integer getFinalStatus() {
        return finalStatus;
    }

    public void setFinalStatus(Integer finalStatus) {
        this.finalStatus = finalStatus;
    }

    public Boolean getSkipCheck() {
        return skipCheck;
    }

    public void setSkipCheck(Boolean skipCheck) {
        this.skipCheck = skipCheck;
    }

    public List<StationData> getStationList() {
        return stationList;
    }

    public void setStationList(List<StationData> stationList) {
        this.stationList = stationList;
    }

    public String getNoOilAcquirerId() {
        return noOilAcquirerId;
    }

    public void setNoOilAcquirerId(String noOilAcquirerId) {
        this.noOilAcquirerId = noOilAcquirerId;
    }

    public String getNoOilShopLogin() {
        return noOilShopLogin;
    }

    public void setNoOilShopLogin(String noOilShopLogin) {
        this.noOilShopLogin = noOilShopLogin;
    }

    public String getOilAcquirerId() {
        return oilAcquirerId;
    }

    public void setOilAcquirerId(String oilAcquirerId) {
        this.oilAcquirerId = oilAcquirerId;
    }

    public String getOilShopLogin() {
        return oilShopLogin;
    }

    public void setOilShopLogin(String oilShopLogin) {
        this.oilShopLogin = oilShopLogin;
    }

    @Override
    public Status check() {
        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_GENERATE_PV_SUCCESS);

        return status;
    }
}
