package com.techedge.mp.frontendbo.adapter.entities.common;

public class TypeData {

    private String field;
    private String value;

    public TypeData() {}

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }



}
