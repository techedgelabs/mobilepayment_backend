package com.techedge.mp.frontendbo.adapter.entities.admin.addprepaidconsumevoucherdetail;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AddPrePaidConsumeVoucherDetailRequest extends AbstractBORequest implements Validable {
    private Status                                    status = new Status();

    private Credential                                credential;
    private AddPrePaidConsumeVoucherDetailRequestBody body;

    public AddPrePaidConsumeVoucherDetailRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AddPrePaidConsumeVoucherDetailRequestBody getBody() {
        return body;
    }

    public void setBody(AddPrePaidConsumeVoucherDetailRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_SUCCESS);

        status = Validator.checkCredential("ADD-PREPAID-CONSUME-VOUCHER-DETAIL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        return status;
    }

    @Override
    public BaseResponse execute() {

        AddPrePaidConsumeVoucherDetailResponse addPrePaidConsumeVoucherDetailResponse = new AddPrePaidConsumeVoucherDetailResponse();

        String response = getAdminServiceRemote().adminAddPrepaidConsumeVoucherDetail(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getConsumedValue(), this.getBody().getExpirationDate(), this.getBody().getInitialValue(), this.getBody().getPromoCode(),
                this.getBody().getPromoDescription(), this.getBody().getPromoDoc(), this.getBody().getVoucherBalanceDue(), this.getBody().getVoucherCode(),
                this.getBody().getVoucherStatus(), this.getBody().getVoucherType(), this.getBody().getVoucherValue(), this.getBody().getPrePaidConsumeVoucherId());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        addPrePaidConsumeVoucherDetailResponse.setStatus(status);

        return addPrePaidConsumeVoucherDetailResponse;
    }

}
