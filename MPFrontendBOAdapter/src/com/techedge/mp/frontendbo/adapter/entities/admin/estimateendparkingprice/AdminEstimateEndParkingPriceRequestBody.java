package com.techedge.mp.frontendbo.adapter.entities.admin.estimateendparkingprice;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminEstimateEndParkingPriceRequestBody implements Validable {

    private String lang;
    private String parkingId;

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getParkingId() {
        return parkingId;
    }

    public void setParkingId(String parkingId) {
        this.parkingId = parkingId;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);
        return status;
    }

}
