package com.techedge.mp.frontendbo.adapter.entities.admin.testexecuteauthorization;

import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteAuthorizationResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestExecuteAuthorizationResponse extends BaseResponse {

    private String                     statusCode;
    private ExecuteAuthorizationResult result;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ExecuteAuthorizationResult getResult() {
        return result;
    }

    public void setResult(ExecuteAuthorizationResult result) {
        this.result = result;
    }

}
