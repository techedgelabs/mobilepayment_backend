package com.techedge.mp.frontendbo.adapter.entities.admin.getemaildomain;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RemoveEmailDomainBodyRequest implements Validable {

    private String email;

    public RemoveEmailDomainBodyRequest() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.email == null) {

            status.setStatusCode(StatusCode.MAIL_CREATE_FAILURE);

            return status;
        }

        status.setStatusCode(StatusCode.MAIL_CREATE_SUCCESS);

        return status;
    }
}
