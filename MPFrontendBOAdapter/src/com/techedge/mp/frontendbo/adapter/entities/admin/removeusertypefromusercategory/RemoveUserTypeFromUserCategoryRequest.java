package com.techedge.mp.frontendbo.adapter.entities.admin.removeusertypefromusercategory;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RemoveUserTypeFromUserCategoryRequest extends AbstractBORequest implements Validable {

    private Status                                    status = new Status();

    private Credential                                credential;
    private RemoveUserTypeFromUserCategoryBodyRequest body;

    public RemoveUserTypeFromUserCategoryRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RemoveUserTypeFromUserCategoryBodyRequest getBody() {
        return body;
    }

    public void setBody(RemoveUserTypeFromUserCategoryBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REMOVE_TYPE_CATEGORY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE);

        return status;

    }

    @Override
    public BaseResponse execute() {

        RemoveUserTypeFromUserCategoryResponse removeUserTypeFromUserCategoryResponse = new RemoveUserTypeFromUserCategoryResponse();

        String response = getAdminServiceRemote().adminRemoveTypeFromUserCategory(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getName(), this.getBody().getCode());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        removeUserTypeFromUserCategoryResponse.setStatus(status);

        return removeUserTypeFromUserCategoryResponse;
    }

}
