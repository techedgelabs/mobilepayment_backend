package com.techedge.mp.frontendbo.adapter.entities.admin.testgetpaymenttransactiondetails;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

public class TestGetPaymentTransactionDetailsResponse extends BaseResponse {

    private String      statusCode;
    private GestPayData result;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public GestPayData getResult() {
        return result;
    }

    public void setResult(GestPayData result) {
        this.result = result;
    }

}
