package com.techedge.mp.frontendbo.adapter.entities.admin.deleteadmin;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminDeleteRequest extends AbstractBORequest implements Validable {

    private Status                 status = new Status();

    private Credential             credential;
    private AdminDeleteRequestBody body;

    public AdminDeleteRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminDeleteRequestBody getBody() {
        return body;
    }

    public void setBody(AdminDeleteRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADMIN-DELETE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DELETE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        String response;
        AdminDeleteResponse deleteUserResponse = new AdminDeleteResponse();

        response = getAdminServiceRemote().adminDelete(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getAdminID());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        deleteUserResponse.setStatus(status);

        return deleteUserResponse;
    }

}
