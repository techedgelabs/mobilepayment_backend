package com.techedge.mp.frontendbo.adapter.entities.admin.insertuserinpromoonhold;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class InsertUserInPromoOnHoldRequest extends AbstractBORequest implements Validable {

    private Status                             status = new Status();

    private Credential                         credential;
    private InsertUserInPromoOnHoldBodyRequest body;

    public InsertUserInPromoOnHoldRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public InsertUserInPromoOnHoldBodyRequest getBody() {
        return body;
    }

    public void setBody(InsertUserInPromoOnHoldBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("INSERTUSERINPROMOONHOLD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_INSERT_USER_IN_PROMO_ONHOLD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String insertUserInPromoOnHoldResult = getAdminServiceRemote().adminInsertUserInPromoOnHold(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getPromotionCode(), this.getBody().getUserId());

        InsertUserInPromoOnHoldResponse insertUserInPromoOnHoldResponse = new InsertUserInPromoOnHoldResponse();

        status.setStatusCode(insertUserInPromoOnHoldResult);
        status.setStatusMessage(prop.getProperty(insertUserInPromoOnHoldResult));
        insertUserInPromoOnHoldResponse.setStatus(status);

        return insertUserInPromoOnHoldResponse;
    }

}
