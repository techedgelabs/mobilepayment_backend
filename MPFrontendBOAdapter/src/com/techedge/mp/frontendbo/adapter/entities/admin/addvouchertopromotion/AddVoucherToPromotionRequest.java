package com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion;

import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AddVoucherToPromotionRequest extends AbstractBORequest implements Validable {

    private Status                           status = new Status();

    private Credential                       credential;
    private AddVoucherToPromotionRequestBody body;

    public AddVoucherToPromotionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AddVoucherToPromotionRequestBody getBody() {
        return body;
    }

    public void setBody(AddVoucherToPromotionRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_INVALID_REQUEST);

        status = Validator.checkCredential("ADDVOUCHERTOPROMOTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        return status;
    }

    @Override
    public BaseResponse execute() {

        AddVoucherToPromotionResponse voucherToPromotionResponse = new AddVoucherToPromotionResponse();

        PromoVoucherInput promoVoucherInputList = this.getBody().getVoucherInfo();

        String response = getAdminServiceRemote().adminAddVoucherToPromotion(this.getCredential().getTicketID(), this.getCredential().getRequestID(), promoVoucherInputList,
                this.getBody().getPromotionCode());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        voucherToPromotionResponse.setStatus(status);

        return voucherToPromotionResponse;
    }

}
