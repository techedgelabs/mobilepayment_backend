package com.techedge.mp.frontendbo.adapter.entities.admin.updateparams;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.ParameterInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdateParamsRequestBody implements Validable {
	
	private List<ParameterInfo> parameterList = new ArrayList<ParameterInfo>(0);

	
	public List<ParameterInfo> getParameterList() {
		return parameterList;
	}
	public void setParameterList(List<ParameterInfo> parameterList) {
		this.parameterList = parameterList;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.parameterList == null || this.parameterList.isEmpty()) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PARAMS_FAILURE);
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_UPDATE_PARAMS_SUCCESS);

		return status;
	}
}
