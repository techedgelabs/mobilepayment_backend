package com.techedge.mp.frontendbo.adapter.entities.admin.insertpaymentmethodrefuelinguser;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminInsertPaymentMethodRefuelingUserResponse extends BaseResponse{
    private AdminInsertPaymentMethodRefuelingUserBodyResponse body;

    public AdminInsertPaymentMethodRefuelingUserBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminInsertPaymentMethodRefuelingUserBodyResponse body) {
        this.body = body;
    }

}
