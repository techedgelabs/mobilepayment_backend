package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetbrandlist;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.dwh.adapter.interfaces.BrandDetail;

public class AdminDwhGetBrandListBodyResponse {

    private String            errorCode;
    private String            statusCode;
    private List<BrandDetail> brandDetailList = new ArrayList<BrandDetail>(0);

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<BrandDetail> getBrandDetailList() {
        return brandDetailList;
    }

    public void setBrandDetailList(List<BrandDetail> brandDetailList) {
        this.brandDetailList = brandDetailList;
    }

}
