package com.techedge.mp.frontendbo.adapter.entities.admin.create;

import java.util.HashSet;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.AdminRole;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class CreateAdminDataRequest implements Validable {

    private String         email;
    private String         password;
    private String         firstName;
    private String         lastName;
    private Set<AdminRole> roles = new HashSet<>(0);

    public CreateAdminDataRequest() {}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Set<AdminRole> getRoles() {
        return roles;
    }

    public void setRoles(Set<AdminRole> roles) {
        this.roles = roles;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

        return status;

    }

}
