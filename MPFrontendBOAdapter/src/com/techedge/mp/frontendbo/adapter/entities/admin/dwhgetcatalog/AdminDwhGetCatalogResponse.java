package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcatalog;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminDwhGetCatalogResponse extends BaseResponse {

    private AdminDwhGetCatalogBodyResponse body;

    public AdminDwhGetCatalogBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminDwhGetCatalogBodyResponse body) {
        this.body = body;
    }

}