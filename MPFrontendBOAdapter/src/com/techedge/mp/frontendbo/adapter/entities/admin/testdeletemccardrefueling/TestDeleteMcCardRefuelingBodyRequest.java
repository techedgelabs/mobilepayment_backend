package com.techedge.mp.frontendbo.adapter.entities.admin.testdeletemccardrefueling;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestDeleteMcCardRefuelingBodyRequest implements Validable {

    private String operationId;
    private String userId;
    private String partnerType;
    private Long   requestTimestamp;
    private String mcCardDpan;
    private String serverSerialNumber;

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPartnerType() {
        return partnerType;
    }

    public void setPartnerType(String partnerType) {
        this.partnerType = partnerType;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getMcCardDpan() {
        return mcCardDpan;
    }

    public void setMcCardDpan(String mcCardDpan) {
        this.mcCardDpan = mcCardDpan;
    }

    public String getServerSerialNumber() {
        return serverSerialNumber;
    }

    public void setServerSerialNumber(String serverSerialNumber) {
        this.serverSerialNumber = serverSerialNumber;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.operationId == null || this.operationId.trim().isEmpty() || this.userId == null || this.userId.trim().isEmpty() || this.partnerType == null
                || this.partnerType.trim().isEmpty() || this.requestTimestamp == null || this.mcCardDpan == null || this.mcCardDpan.trim().isEmpty()
                || this.serverSerialNumber == null || this.serverSerialNumber.trim().isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_TEST_DELETE_MC_CARD_REFUELING_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_DELETE_MC_CARD_REFUELING_SUCCESS);

        return status;
    }

}
