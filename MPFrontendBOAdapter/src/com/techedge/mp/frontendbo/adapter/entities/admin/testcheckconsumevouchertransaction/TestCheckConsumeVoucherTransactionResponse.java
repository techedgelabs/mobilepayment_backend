package com.techedge.mp.frontendbo.adapter.entities.admin.testcheckconsumevouchertransaction;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.VoucherInfo;

public class TestCheckConsumeVoucherTransactionResponse extends BaseResponse {

    private String            csTransactionID;

    private String            marketingMsg;
    private List<VoucherInfo> voucherList = new ArrayList<VoucherInfo>(0);

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public List<VoucherInfo> getVoucherList() {
        return voucherList;
    }

    public void setVoucherList(List<VoucherInfo> voucherList) {
        this.voucherList = voucherList;
    }

}
