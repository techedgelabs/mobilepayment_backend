package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveadmin;

import java.util.List;

import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminRetrieveResponse extends BaseResponse {

    private List<Admin> adminUsers;

    public List<Admin> getAdminUsers() {
        return adminUsers;
    }

    public void setAdminUsers(List<Admin> adminUsers) {
        this.adminUsers = adminUsers;
    }

}
