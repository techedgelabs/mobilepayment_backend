package com.techedge.mp.frontendbo.adapter.entities.admin.updatedocument;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminUpdateDocumentRequest extends AbstractBORequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;
    private AdminUpdateDocumentBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminUpdateDocumentBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminUpdateDocumentBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-DOCUMENT", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_DOCUMENT_UPDATE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_UPDATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String templateFile = "";
        if (this.getBody().getTemplateFile() != null) {
            templateFile = this.getBody().getTemplateFile();
        }

        String updateDocumentResult = getAdminServiceRemote().adminUpdateDocument(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getDocumentKey(), this.getBody().getPosition(), templateFile, this.getBody().getTitle(), this.getBody().getSubtitle(), this.getBody().getUserCategory(), this.getBody().getGroupCategory());

        AdminUpdateDocumentResponse updateDocumentResponse = new AdminUpdateDocumentResponse();
        status.setStatusCode(updateDocumentResult);
        status.setStatusMessage(prop.getProperty(updateDocumentResult));
        updateDocumentResponse.setStatus(status);

        return updateDocumentResponse;
    }

}