package com.techedge.mp.frontendbo.adapter.entities.admin.insertmulticardrefuelinguser;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminInsertMulticardRefuelingUserResponse extends BaseResponse{
    private AdminInsertMulticardRefuelingUserBodyResponse body;

    public AdminInsertMulticardRefuelingUserBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminInsertMulticardRefuelingUserBodyResponse body) {
        this.body = body;
    }

}
