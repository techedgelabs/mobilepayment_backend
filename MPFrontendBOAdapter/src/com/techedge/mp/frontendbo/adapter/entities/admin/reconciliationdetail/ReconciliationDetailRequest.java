package com.techedge.mp.frontendbo.adapter.entities.admin.reconciliationdetail;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class ReconciliationDetailRequest extends AbstractBORequest implements Validable {

    private Status                          status = new Status();

    private Credential                      credential;
    private ReconciliationDetailRequestBody body;

    public ReconciliationDetailRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ReconciliationDetailRequestBody getBody() {
        return body;
    }

    public void setBody(ReconciliationDetailRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RECONCILIATION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RECONCILIATION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        ReconciliationDetailResponse reconciliationDetailResponse = new ReconciliationDetailResponse();

        ReconciliationDetail reconciliationDetailResponseService = getAdminServiceRemote().adminReconcileDetail(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), this.getBody().getTransactionId());

        status.setStatusCode(reconciliationDetailResponseService.getStatusCode());
        status.setStatusMessage(prop.getProperty(reconciliationDetailResponseService.getStatusCode()));

        reconciliationDetailResponse.setStatus(status);

        if (reconciliationDetailResponseService.getStatusCode() != ResponseHelper.RECONCILIATION_DETAIL_FAILURE) {
            reconciliationDetailResponse.setAmount(reconciliationDetailResponseService.getAmount());
            reconciliationDetailResponse.setFuelQuantity(reconciliationDetailResponseService.getFuelQuantity());
            reconciliationDetailResponse.setProductID(reconciliationDetailResponseService.getProductID());
            reconciliationDetailResponse.setProductDescription(reconciliationDetailResponseService.getProductDescription());
            reconciliationDetailResponse.setTimestampEndRefuel(reconciliationDetailResponseService.getTimestampEndRefuel());

        }

        return reconciliationDetailResponse;
    }

}
