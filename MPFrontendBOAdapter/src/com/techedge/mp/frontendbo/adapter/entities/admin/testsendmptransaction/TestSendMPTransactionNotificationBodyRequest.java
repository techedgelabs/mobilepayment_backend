package com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction;

import com.techedge.mp.frontendbo.adapter.entities.common.RefuelDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestSendMPTransactionNotificationBodyRequest implements Validable {

    private String       srcTransactionID;
    private String       mpTransactionID;
    private String       mpTransactionStatus;
    private RefuelDetail refuelDetail = new RefuelDetail();

    public String getSrcTransactionID() {

        return srcTransactionID;
    }

    public String getMpTransactionID() {

        return mpTransactionID;
    }

    public String getMpTransactionStatus() {

        return mpTransactionStatus;
    }

    public RefuelDetail getRefuelDetail() {

        return refuelDetail;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public void setMpTransactionStatus(String mpTransactionStatus) {
        this.mpTransactionStatus = mpTransactionStatus;
    }

    public void setRefuelDetail(RefuelDetail refuelDetail) {
        this.refuelDetail = refuelDetail;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.refuelDetail == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_FIDELITY_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_FIDELITY_SUCCESS);

        return status;
    }

}
