package com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher;

import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestCreateVoucherResponse extends BaseResponse{

    private VoucherDetail voucher;
    private String        csTransactionID;

    public VoucherDetail getVoucher() {
        return voucher;
    }

    public void setVoucher(VoucherDetail voucher) {
        this.voucher = voucher;
    }

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

}
