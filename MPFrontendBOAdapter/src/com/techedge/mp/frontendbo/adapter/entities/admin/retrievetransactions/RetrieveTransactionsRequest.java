package com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions;

import java.sql.Timestamp;
import java.util.List;
import java.util.Set;

import com.techedge.mp.core.business.interfaces.RetrieveTransactionListData;
import com.techedge.mp.core.business.interfaces.TransactionEventHistory;
import com.techedge.mp.core.business.interfaces.TransactionHistory;
import com.techedge.mp.core.business.interfaces.TransactionStatusHistory;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.TransactionEventInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.TransactionInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.TransactionStatusInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrieveTransactionsRequest extends AbstractBORequest implements Validable {

    private Status                          status = new Status();

    private Credential                      credential;
    private RetrieveTransactionsRequestBody body;

    public RetrieveTransactionsRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveTransactionsRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveTransactionsRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-USERS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        Timestamp start = null;
        Timestamp end = null;

        if (this.getBody().getCreationTimestampStart() != null) {
            start = new Timestamp(this.getBody().getCreationTimestampStart());
        }
        if (this.getBody().getCreationTimestampEnd() != null) {
            end = new Timestamp(this.getBody().getCreationTimestampEnd());
        }

        RetrieveTransactionListData retrieveTransactionData = getAdminServiceRemote().adminTransactionRetrieve(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), this.getBody().getTransactionId(), this.getBody().getUserId(), this.getBody().getStationId(), this.getBody().getPumpId(),
                this.getBody().getFinalStatusType(), this.getBody().getGFGNotification(), this.getBody().getConfirmed(), start, end, this.getBody().getProductID(),
                this.getBody().getTransactionHistoryFlag());

        RetrieveTransactionsResponse retrieveTransactionsResponse = new RetrieveTransactionsResponse();

        List<TransactionHistory> transactionList = retrieveTransactionData.getTransactionHistoryList();

        if (!transactionList.isEmpty()) {

            for (TransactionHistory transactionHistory : transactionList) {

                TransactionInfo transactionInfo = new TransactionInfo();
                transactionInfo.setTransactionId(String.valueOf(transactionHistory.getTransactionID()));
                transactionInfo.setUserId(transactionHistory.getUser().getId());
                transactionInfo.setStationId(transactionHistory.getStation().getStationID());

                if (transactionHistory.getCreationTimestamp() != null) {
                    transactionInfo.setCreationTimestamp(new Timestamp(transactionHistory.getCreationTimestamp().getTime()));
                }
                else {
                    transactionInfo.setCreationTimestamp(null);
                }

                if (transactionHistory.getEndRefuelTimestamp() != null) {
                    transactionInfo.setEndRefuelTimestamp(new Timestamp(transactionHistory.getEndRefuelTimestamp().getTime()));
                }
                else {
                    transactionInfo.setEndRefuelTimestamp(null);
                }

                transactionInfo.setInitialAmount(transactionHistory.getInitialAmount());
                transactionInfo.setFinalAmount(transactionHistory.getFinalAmount());
                transactionInfo.setFuelQuantity(transactionHistory.getFuelQuantity());
                transactionInfo.setFuelAmount(transactionHistory.getFuelAmount());
                transactionInfo.setPumpId(transactionHistory.getPumpID());
                transactionInfo.setPumpNumber(transactionHistory.getPumpNumber());
                transactionInfo.setCurrency(transactionHistory.getCurrency());
                transactionInfo.setShopLogin(transactionHistory.getShopLogin());
                transactionInfo.setAcquirerId(transactionHistory.getAcquirerID());
                transactionInfo.setBankTransactionId(transactionHistory.getBankTansactionID());
                transactionInfo.setAuthorizationCode(transactionHistory.getAuthorizationCode());
                transactionInfo.setToken(transactionHistory.getToken());
                transactionInfo.setPaymentType(transactionHistory.getPaymentType());
                transactionInfo.setFinalStatusType(transactionHistory.getFinalStatusType());
                transactionInfo.setGFGnotification(this.checkField(transactionHistory.getGFGNotification()));
                transactionInfo.setConfirmed(this.checkField(transactionHistory.getConfirmed()));
                transactionInfo.setPaymentMethodId(transactionHistory.getPaymentMethodId());
                transactionInfo.setPaymentMethodType(transactionHistory.getPaymentMethodType());
                transactionInfo.setServerName(transactionHistory.getServerName());
                transactionInfo.setStatusAttemptsLeft(transactionHistory.getStatusAttemptsLeft());
                transactionInfo.setProductID(transactionHistory.getProductID());
                transactionInfo.setProductDescription(transactionHistory.getProductDescription());
                transactionInfo.setArchivingDate(transactionHistory.getArchivingDate());
                transactionInfo.setTransactionCategory(transactionHistory.getTransactionCategory().getValue());
                transactionInfo.setReconciliationAttemptsLeft(transactionHistory.getReconciliationAttemptsLeft());

                Set<TransactionStatusHistory> transactionStatusHistoryList = transactionHistory.getTransactionStatusHistoryData();

                for (TransactionStatusHistory transactionStatusHistory : transactionStatusHistoryList) {

                    TransactionStatusInfo transactionStatusInfo = new TransactionStatusInfo();

                    transactionStatusInfo.setTimestamp(transactionStatusHistory.getTimestamp().getTime());
                    transactionStatusInfo.setSequenceId(transactionStatusHistory.getSequenceID());
                    transactionStatusInfo.setStatus(transactionStatusHistory.getStatus());
                    transactionStatusInfo.setSubStatus(transactionStatusHistory.getSubStatus());
                    transactionStatusInfo.setSubStatusDescription(transactionStatusHistory.getSubStatusDescription());

                    transactionInfo.getTransactionStatusList().add(transactionStatusInfo);
                }

                Set<TransactionEventHistory> transactionEventHistoryList = transactionHistory.getTransactionEventHistoryData();

                for (TransactionEventHistory transactionEventHistory : transactionEventHistoryList) {

                    TransactionEventInfo transactionEventInfo = new TransactionEventInfo();

                    transactionEventInfo.setSequenceId(transactionEventHistory.getSequenceID());
                    transactionEventInfo.setEventType(transactionEventHistory.getEventType());
                    transactionEventInfo.setEventAmount(transactionEventHistory.getEventAmount());
                    transactionEventInfo.setTransactionResult(transactionEventHistory.getTransactionResult());
                    transactionEventInfo.setErrorCode(transactionEventHistory.getErrorCode());
                    transactionEventInfo.setErrorDescription(transactionEventHistory.getErrorDescription());

                    transactionInfo.getTransactionEventList().add(transactionEventInfo);
                }

                retrieveTransactionsResponse.getTransactions().add(transactionInfo);
            }
        }

        status.setStatusCode(retrieveTransactionData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveTransactionData.getStatusCode()));

        retrieveTransactionsResponse.setStatus(status);

        return retrieveTransactionsResponse;
    }

    private String checkField(Object obj) {
        if (obj != null) {
            return obj.toString();
        }
        else {
            return "";
        }

    }

}
