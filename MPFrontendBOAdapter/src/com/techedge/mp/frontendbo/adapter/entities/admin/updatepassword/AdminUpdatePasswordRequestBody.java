package com.techedge.mp.frontendbo.adapter.entities.admin.updatepassword;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminUpdatePasswordRequestBody implements Validable {

    private String oldPassword;
    private String newPassword;

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkPassword("PWD", newPassword);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;
        }

        if (oldPassword == null || oldPassword.trim() == "") {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_PASSWORD_OLD_PASSWORD_WRONG);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_PASSWORD_SUCCESS);

        return status;
    }

}
