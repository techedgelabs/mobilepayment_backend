package com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactionsmanager;

import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RetrieveTransactionsManagerRequest implements Validable {

	
	private Credential credential;
	private RetrieveTransactionsManagerBodyRequest body;

	
	public RetrieveTransactionsManagerRequest() {}
	
    public Credential getCredential() {
		return credential;
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}
	
	public RetrieveTransactionsManagerBodyRequest getBody() {
		return body;
	}

	public void setBody(RetrieveTransactionsManagerBodyRequest body) {
		this.body = body;
	}

	@Override
	public Status check() {
		
		Status status = new Status();
		
		status = Validator.checkCredential("RETRIEVE-TRANSACTIONS-MANAGER", credential);
		
		if(!Validator.isValid(status.getStatusCode())) {
			
			return status;
			
		}
		
		if(this.body != null) {
			
			status = this.body.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS);
		
		return status;
		
	}
	
}
