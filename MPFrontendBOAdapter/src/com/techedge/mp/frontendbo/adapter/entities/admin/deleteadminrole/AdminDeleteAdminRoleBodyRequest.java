package com.techedge.mp.frontendbo.adapter.entities.admin.deleteadminrole;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminDeleteAdminRoleBodyRequest implements Validable {

    private String role;

    public String getName() {
        return role;
    }

    public void setName(String role) {
        this.role = role;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (this.role == null || this.role.isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_ADMIN_ROLE_DELETE_CHECK_FAILURE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_ADMIN_ROLE_DELETE_SUCCESS);

        return status;
    }
}
