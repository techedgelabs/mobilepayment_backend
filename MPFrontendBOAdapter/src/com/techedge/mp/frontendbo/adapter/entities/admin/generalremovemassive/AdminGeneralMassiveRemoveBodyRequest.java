package com.techedge.mp.frontendbo.adapter.entities.admin.generalremovemassive;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminGeneralMassiveRemoveBodyRequest implements Validable {

    private String idFrom;
    private String idTo;
    private String type;

    public AdminGeneralMassiveRemoveBodyRequest() {}

    public String getIdFrom() {
        return idFrom;
    }

    public void setIdFrom(String idFrom) {
        this.idFrom = idFrom;
    }

    public String getIdTo() {
        return idTo;
    }

    public void setIdTo(String idTo) {
        this.idTo = idTo;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.idFrom == null || (this.idFrom != null && this.idFrom.isEmpty()) || this.type == null || (this.type != null && this.type.isEmpty()) || this.idTo.isEmpty()
                || (this.idTo != null && this.idTo.isEmpty())) {
            status.setStatusCode(StatusCode.ADMIN_MASSIVE_REMOVE_FAILURE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_MASSIVE_REMOVE_SUCCESS);

        return status;
    }
}