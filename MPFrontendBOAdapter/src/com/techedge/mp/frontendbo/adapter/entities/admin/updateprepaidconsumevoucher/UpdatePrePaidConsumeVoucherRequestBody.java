package com.techedge.mp.frontendbo.adapter.entities.admin.updateprepaidconsumevoucher;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdatePrePaidConsumeVoucherRequestBody implements Validable {

    private Long    id;
    private String  csTransactionID;
    private String  marketingMsg;
    private String  messageCode;
    private String  operationID;
    private String  operationIDReversed;
    private String  operationType;
    private Boolean reconciled;
    private String  statusCode;
    private Double  totalConsumed;
    private String  warningMsg;
    private Double  amount;
    private String  preAuthOperationID;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCsTransactionID() {
        return csTransactionID;
    }

    public void setCsTransactionID(String csTransactionID) {
        this.csTransactionID = csTransactionID;
    }

    public String getMarketingMsg() {
        return marketingMsg;
    }

    public void setMarketingMsg(String marketingMsg) {
        this.marketingMsg = marketingMsg;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public String getOperationIDReversed() {
        return operationIDReversed;
    }

    public void setOperationIDReversed(String operationIDReversed) {
        this.operationIDReversed = operationIDReversed;
    }

    public String getOperationType() {
        return operationType;
    }

    public void setOperationType(String operationType) {
        this.operationType = operationType;
    }

    public Boolean getReconciled() {
        return reconciled;
    }

    public void setReconciled(Boolean reconciled) {
        this.reconciled = reconciled;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public Double getTotalConsumed() {
        return totalConsumed;
    }

    public void setTotalConsumed(Double totalConsumed) {
        this.totalConsumed = totalConsumed;
    }

    public String getWarningMsg() {
        return warningMsg;
    }

    public void setWarningMsg(String warningMsg) {
        this.warningMsg = warningMsg;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getPreAuthOperationID() {
        return preAuthOperationID;
    }

    public void setPreAuthOperationID(String preAuthOperationID) {
        this.preAuthOperationID = preAuthOperationID;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.id == null) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_ERROR_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_SUCCESS);

        return status;
    }

}
