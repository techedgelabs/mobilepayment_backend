package com.techedge.mp.frontendbo.adapter.entities.admin.testredemption;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestRedemptionRequest extends AbstractBORequest implements Validable {

    private Credential                credential;
    private TestRedemptionBodyRequest body;

    public TestRedemptionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestRedemptionBodyRequest getBody() {
        return body;
    }

    public void setBody(TestRedemptionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("REDEMPTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_REDEMPTION_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestRedemptionResponse testRedemptionResponse = new TestRedemptionResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "redemption");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testRedemptionResponse.getStatus().setStatusCode("-1");
        }
        else {
            RedemptionResult redemptionResult = new RedemptionResult();
            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                redemptionResult = getFidelityServiceRemote().redemption(this.getBody().getOperationID(), partnerType, this.getBody().getRequestTimestamp(),
                        this.getBody().getFiscalCode(), this.getBody().getRedemptionCode());
            }
            catch (FidelityServiceException ex) {
                redemptionResult.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            }

            if (!redemptionResult.getStatusCode().equals(ResponseHelper.SYSTEM_ERROR)) {
                redemptionResult.setCsTransactionID(redemptionResult.getCsTransactionID());
                testRedemptionResponse.getStatus().setStatusMessage(redemptionResult.getMessageCode());
                testRedemptionResponse.getStatus().setStatusCode(redemptionResult.getStatusCode());
                testRedemptionResponse.setRedemptionCode(redemptionResult.getRedemptionCode());
                testRedemptionResponse.setBalance(redemptionResult.getBalance());
                testRedemptionResponse.setBalanceAmount(redemptionResult.getBalanceAmount());
                testRedemptionResponse.setCredits(redemptionResult.getCredits());
                testRedemptionResponse.setWarningMsg(redemptionResult.getWarningMsg());
                testRedemptionResponse.setMarketingMsg(redemptionResult.getMarketingMsg());

                if (redemptionResult.getVoucher() != null) {
                    testRedemptionResponse.setVoucher(redemptionResult.getVoucher());
                }
            }
        }

        return testRedemptionResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
