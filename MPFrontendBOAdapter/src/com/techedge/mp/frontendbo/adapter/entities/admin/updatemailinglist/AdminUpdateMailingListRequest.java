package com.techedge.mp.frontendbo.adapter.entities.admin.updatemailinglist;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminUpdateMailingListRequest extends AbstractBORequest implements Validable {

    private Status                            status = new Status();

    private Credential                        credential;
    private AdminUpdateMailingListBodyRequest body;

    public AdminUpdateMailingListRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminUpdateMailingListBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminUpdateMailingListBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-MAILING-LIST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_MAILING_LIST_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String adminUpdateMailingListResult = getAdminServiceRemote().adminUpdateMailingList(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getId(), this.getBody().getEmail(), this.getBody().getName(), this.getBody().getStatus(), this.getBody().getTemplate());

        AdminUpdateMailingListResponse adminUpdateMailingListResponse = new AdminUpdateMailingListResponse();

        status.setStatusCode(adminUpdateMailingListResult);
        status.setStatusMessage(prop.getProperty(adminUpdateMailingListResult));
        adminUpdateMailingListResponse.setStatus(status);

        return adminUpdateMailingListResponse;
    }

}
