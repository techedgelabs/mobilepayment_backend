package com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AddSurveyQuestionRequest extends AbstractBORequest implements Validable {

    private Status                       status = new Status();

    private Credential                   credential;
    private AddSurveyQuestionBodyRequest body;

    public AddSurveyQuestionRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AddSurveyQuestionBodyRequest getBody() {
        return body;
    }

    public void setBody(AddSurveyQuestionBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("ADD-SURVEY-QUESTION", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_SURVEY_QUESTION_ADD_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        AddSurveyQuestionBodyRequest addSurveyQuestionBodyRequest = this.getBody();
        AddSurveyQuestionResponse addSurveyQuestionResponse = new AddSurveyQuestionResponse();

        String response = getAdminServiceRemote().adminSurveyQuestionAdd(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), addSurveyQuestionBodyRequest.getCode(), addSurveyQuestionBodyRequest.getQuestion().getCode(),
                addSurveyQuestionBodyRequest.getQuestion().getText(), addSurveyQuestionBodyRequest.getQuestion().getType(),
                addSurveyQuestionBodyRequest.getQuestion().getSequence());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        addSurveyQuestionResponse.setStatus(status);

        return addSurveyQuestionResponse;
    }

}
