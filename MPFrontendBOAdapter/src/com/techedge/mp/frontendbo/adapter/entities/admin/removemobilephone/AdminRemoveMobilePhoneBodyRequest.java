package com.techedge.mp.frontendbo.adapter.entities.admin.removemobilephone;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminRemoveMobilePhoneBodyRequest implements Validable {

    private Long mobilePhoneId;

    public AdminRemoveMobilePhoneBodyRequest() {}

    public Long getMobilePhoneId() {
        return mobilePhoneId;
    }

    public void setMobilePhoneId(Long mobilePhoneId) {
        this.mobilePhoneId = mobilePhoneId;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.mobilePhoneId == null) {
            status.setStatusCode(StatusCode.ADMIN_REMOVE_MOBILE_PHONE_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_REMOVE_MOBILE_PHONE_SUCCESS);

        return status;
    }

}
