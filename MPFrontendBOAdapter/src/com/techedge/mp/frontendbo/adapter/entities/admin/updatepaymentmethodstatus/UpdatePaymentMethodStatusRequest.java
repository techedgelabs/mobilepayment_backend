package com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethodstatus;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class UpdatePaymentMethodStatusRequest extends AbstractBORequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;
    private UpdatePaymentMethodStatusRequestBody body;

    public UpdatePaymentMethodStatusRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdatePaymentMethodStatusRequestBody getBody() {
        return body;
    }

    public void setBody(UpdatePaymentMethodStatusRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-PAYMENTMETHOD", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String response = getAdminServiceRemote().adminPaymentMethodStatusUpdate(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getId(),
                this.getBody().getType(), this.getBody().getStatus(), this.getBody().getMessage());

        UpdatePaymentMethodStatusResponse updatePaymentMethodStatusResponse = new UpdatePaymentMethodStatusResponse();

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updatePaymentMethodStatusResponse.setStatus(status);

        return updatePaymentMethodStatusResponse;
    }

}
