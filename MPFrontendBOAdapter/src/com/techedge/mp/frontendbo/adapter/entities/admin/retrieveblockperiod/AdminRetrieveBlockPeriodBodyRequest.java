package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveblockperiod;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminRetrieveBlockPeriodBodyRequest implements Validable {

    private String  code;
    private Boolean active;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public Status check() {
        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_BLOCK_PERIOD_RETRIEVE_SUCCESS);

        return status;
    }
}
