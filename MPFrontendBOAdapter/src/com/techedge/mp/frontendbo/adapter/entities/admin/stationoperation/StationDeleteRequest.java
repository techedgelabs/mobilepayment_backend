package com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class StationDeleteRequest extends AbstractBORequest implements Validable {

    private Status               status = new Status();

    private Credential           credential;
    private StationOpRequestBody body;

    public StationDeleteRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public StationOpRequestBody getBody() {
        return body;
    }

    public void setBody(StationOpRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        StationOpResponse stationOpResponse = new StationOpResponse();
        String response = getAdminServiceRemote().adminDeleteStation(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getStation());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        stationOpResponse.setStatus(status);

        return stationOpResponse;
    }

}
