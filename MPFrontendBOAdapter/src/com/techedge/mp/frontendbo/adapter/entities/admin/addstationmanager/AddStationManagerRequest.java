package com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AddStationManagerRequest extends AbstractBORequest implements Validable {

    private Status                 status = new Status();


	
	private Credential credential;
	private AddStationManagerBodyRequest body;

	
	public AddStationManagerRequest() {}
	
    public Credential getCredential() {
		return credential;
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}
	
	public AddStationManagerBodyRequest getBody() {
		return body;
	}

	public void setBody(AddStationManagerBodyRequest body) {
		this.body = body;
	}

	@Override
	public Status check() {
		
		Status status = new Status();
		
		status = Validator.checkCredential("ADDSTATION-MANAGER", credential);
		
		if(!Validator.isValid(status.getStatusCode())) {
			
			return status;
			
		}
		
		if(this.body != null) {
			
			status = this.body.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
			
			return status;
			
		}
		
		status.setStatusCode(StatusCode.ADMIN_ADDSTATION_MANAGER_SUCCESS);
		
		return status;
		
	}

    @Override
    public BaseResponse execute() {
        AddStationManagerBodyRequest bodyRequest = this.getBody();
        AddStationManagerResponse addStationManagerResponse = new AddStationManagerResponse();

        String response = getAdminServiceRemote().adminAddStationManager(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), bodyRequest.getManagerID(), bodyRequest.getStationID());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        addStationManagerResponse.setStatus(status);

        return addStationManagerResponse;
    }
	
}
