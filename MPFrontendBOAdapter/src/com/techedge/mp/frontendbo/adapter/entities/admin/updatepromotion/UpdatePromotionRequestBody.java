package com.techedge.mp.frontendbo.adapter.entities.admin.updatepromotion;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdatePromotionRequestBody implements Validable {

    private String  code;
    private String  description;
    private Integer status;
    private Long    startData;
    private Long    endData;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Long getStartData() {
        return startData;
    }

    public void setStartData(Long startData) {
        this.startData = startData;
    }

    public Long getEndData() {
        return endData;
    }

    public void setEndData(Long endData) {
        this.endData = endData;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.code == null) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_PROMOTION_ERROR_PARAMETERS);
            return status;
        }

        if (this.description == null) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        if (this.status == null) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        if (this.startData == null) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        if (this.endData == null) {

            status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_SUCCESS);

        return status;
    }

}
