package com.techedge.mp.frontendbo.adapter.entities.admin.getparkingzonesbycity;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminGetParkingZonesByCityResponse extends BaseResponse{
    
    private AdminGetParkingZonesByCityResponseBody body;

    public AdminGetParkingZonesByCityResponseBody getBody() {
        return body;
    }

    public void setBody(AdminGetParkingZonesByCityResponseBody body) {
        this.body = body;
    }
}
