package com.techedge.mp.frontendbo.adapter.entities.admin.getsurveysubmissions;

import java.util.ArrayList;

import com.techedge.mp.frontendbo.adapter.entities.common.SurveySubmissions;

public class GetSurveySubmissionsBodyResponse {
    private ArrayList<SurveySubmissions> surveySubmissions = new ArrayList<SurveySubmissions>(0);

    public ArrayList<SurveySubmissions> getSurveySubmissions() {
        return surveySubmissions;
    }

    public void setSurveySubmissions(ArrayList<SurveySubmissions> surveySubmissions) {
        this.surveySubmissions = surveySubmissions;
    }

}
