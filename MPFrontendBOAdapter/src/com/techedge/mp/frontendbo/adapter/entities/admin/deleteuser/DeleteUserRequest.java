package com.techedge.mp.frontendbo.adapter.entities.admin.deleteuser;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class DeleteUserRequest extends AbstractBORequest implements Validable {

    private Status                status = new Status();

    private Credential            credential;
    private DeleteUserRequestBody body;

    public DeleteUserRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public DeleteUserRequestBody getBody() {
        return body;
    }

    public void setBody(DeleteUserRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_USER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String response;
        DeleteUserResponse updateUserResponse = new DeleteUserResponse();

        try {
            Date startDate = null, endDate = null;
            if (this.getBody().getCreationDateStart() != null)
                startDate = sdf.parse(this.getBody().getCreationDateStart());
            if (this.getBody().getCreationDateStart() != null)
                endDate = sdf.parse(this.getBody().getCreationDateEnd());

            response = getAdminServiceRemote().adminUserDelete(this.getCredential().getTicketID(), this.getCredential().getRequestID(), this.getBody().getId(), startDate, endDate);
        }
        catch (ParseException e) {

            status = new Status();
            status.setStatusCode(StatusCode.ADMIN_DELETE_USER_DATE_WRONG);
            status.setStatusMessage(prop.getProperty(status.getStatusCode()));
            updateUserResponse.setStatus(status);
            return updateUserResponse;
        }

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updateUserResponse.setStatus(status);

        return updateUserResponse;
    }

}
