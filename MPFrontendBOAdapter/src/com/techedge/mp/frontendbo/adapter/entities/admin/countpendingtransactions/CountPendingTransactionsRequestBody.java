package com.techedge.mp.frontendbo.adapter.entities.admin.countpendingtransactions;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class CountPendingTransactionsRequestBody implements Validable {

    private String serverName;

   

    public String getServerName() {
        return serverName;
    }



    public void setServerName(String serverName) {
        this.serverName = serverName;
    }



    @Override
    public Status check() {

        Status status = new Status();

        if (this.serverName == null || this.serverName.isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_COUNT_PENDING_TRANSACTIONS_ERROR_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_COUNT_PENDING_TRANSACTIONS_SUCCESS);
        return status;
    }

}
