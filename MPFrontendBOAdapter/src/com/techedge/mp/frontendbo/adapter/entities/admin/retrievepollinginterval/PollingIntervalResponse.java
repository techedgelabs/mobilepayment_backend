package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepollinginterval;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class PollingIntervalResponse extends BaseResponse {

    private PollingIntervalBodyResponse body;

    public PollingIntervalBodyResponse getBody() {
        return body;
    }

    public void setBody(PollingIntervalBodyResponse body) {
        this.body = body;
    }

}
