package com.techedge.mp.frontendbo.adapter.entities.admin.testexecutepayment;

import com.techedge.mp.fidelity.adapter.business.interfaces.ExecutePaymentResult;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestExecutePaymentResponse extends BaseResponse {

    private String               statusCode;
    private ExecutePaymentResult result;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public ExecutePaymentResult getResult() {
        return result;
    }

    public void setResult(ExecutePaymentResult result) {
        this.result = result;
    }

}
