package com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminCreateDocumentAttributeRequest extends AbstractBORequest implements Validable {

    private Status                                  status = new Status();

    private Credential                              credential;
    private AdminCreateDocumentAttributeBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminCreateDocumentAttributeBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminCreateDocumentAttributeBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-DOCUMENT-ATTRIBUTE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_FAILURE);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String createDocumentAttributeResult = getAdminServiceRemote().adminCreateDocumentAttribute(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getCheckKey(), this.getBody().getMandatory(), this.getBody().getPosition(), this.getBody().getConditionText(),
                this.getBody().getExtendedConditionText(), this.getBody().getDocumentKey(), this.getBody().getSubtitle());

        AdminCreateDocumentAttributeResponse createDocumentAttributeResponse = new AdminCreateDocumentAttributeResponse();
        status.setStatusCode(createDocumentAttributeResult);
        status.setStatusMessage(prop.getProperty(createDocumentAttributeResult));
        createDocumentAttributeResponse.setStatus(status);

        return createDocumentAttributeResponse;
    }

}