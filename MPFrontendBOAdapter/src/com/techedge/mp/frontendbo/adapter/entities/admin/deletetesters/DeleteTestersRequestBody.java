package com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class DeleteTestersRequestBody implements Validable {
	
	
	private DeleteTestersDataRequest customerUserData;
	
	
	public DeleteTestersRequestBody() {}

	public DeleteTestersDataRequest getCustomerUserData() {
		return customerUserData;
	}
	public void setCustomerUserData(DeleteTestersDataRequest customerUserData) {
		this.customerUserData = customerUserData;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.customerUserData != null) {
			
			status = this.customerUserData.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
			
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

		return status;
	}
	
}
