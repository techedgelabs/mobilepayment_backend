package com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethod;

import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdatePaymentMethodRequestBody implements Validable {
	
	private Long id;
	private String type;
	private Integer status;
	private String defaultMethod;
	private Integer attemptsLeft;
	private Double checkAmount;


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.id == null) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.type != null && this.type.length() > 20) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.status != null &&
				( this.status != PaymentInfo.PAYMENTINFO_STATUS_BLOCKED &&
				  this.status != PaymentInfo.PAYMENTINFO_STATUS_CANCELED &&
				  this.status != PaymentInfo.PAYMENTINFO_STATUS_ERROR &&
				  this.status != PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED &&
				  this.status != PaymentInfo.PAYMENTINFO_STATUS_PENDING &&
				  this.status != PaymentInfo.PAYMENTINFO_STATUS_VERIFIED ) ) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.defaultMethod != null &&
				( !this.defaultMethod.equals("false") &&
				  !this.defaultMethod.equals("true" ) ) ) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.attemptsLeft != null && this.attemptsLeft > 10) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS);
			return status;
		}
		
		if(this.checkAmount != null && this.checkAmount > 10.0) {
			
			status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS);
			return status;
		}
		
		
		status.setStatusCode(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_SUCCESS);

		return status;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getType() {
		return type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Integer getStatus() {
		return status;
	}


	public void setStatus(Integer status) {
		this.status = status;
	}


	public String getDefaultMethod() {
		return defaultMethod;
	}


	public void setDefaultMethod(String defaultMethod) {
		this.defaultMethod = defaultMethod;
	}


	public Integer getAttemptsLeft() {
		return attemptsLeft;
	}


	public void setAttemptsLeft(Integer attemptsLeft) {
		this.attemptsLeft = attemptsLeft;
	}


	public Double getCheckAmount() {
		return checkAmount;
	}


	public void setCheckAmount(Double checkAmount) {
		this.checkAmount = checkAmount;
	}
	
}
