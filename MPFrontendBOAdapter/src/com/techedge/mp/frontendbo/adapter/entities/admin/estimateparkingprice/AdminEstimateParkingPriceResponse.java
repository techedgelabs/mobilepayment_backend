package com.techedge.mp.frontendbo.adapter.entities.admin.estimateparkingprice;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminEstimateParkingPriceResponse extends BaseResponse{
    
    private AdminEstimateParkingPriceResponseBody body;

    public AdminEstimateParkingPriceResponseBody getBody() {
        return body;
    }

    public void setBody(AdminEstimateParkingPriceResponseBody body) {
        this.body = body;
    }
}
