package com.techedge.mp.frontendbo.adapter.entities.admin.createmanager;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class CreateManagerRequest extends AbstractBORequest implements Validable {

    private Status                   status = new Status();

    private Credential               credential;
    private CreateManagerBodyRequest body;

    public CreateManagerRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateManagerBodyRequest getBody() {
        return body;
    }

    public void setBody(CreateManagerBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE-MANAGER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        CreateManagerResponse createManagerResponse = new CreateManagerResponse();

        String response = getAdminServiceRemote().adminCreateManager(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getManagerData().getManager());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        createManagerResponse.setStatus(status);

        return createManagerResponse;
    }

}
