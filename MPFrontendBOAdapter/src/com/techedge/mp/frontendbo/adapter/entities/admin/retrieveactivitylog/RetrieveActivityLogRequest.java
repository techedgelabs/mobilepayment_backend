package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog;

import java.sql.Timestamp;
import java.util.List;

import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.RetrieveActivityLogData;
import com.techedge.mp.frontendbo.adapter.entities.common.ActivityLogInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class RetrieveActivityLogRequest extends AbstractBORequest implements Validable {

    private Status                         status = new Status();

    private Credential                     credential;
    private RetrieveActivityLogRequestBody body;

    public RetrieveActivityLogRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public RetrieveActivityLogRequestBody getBody() {
        return body;
    }

    public void setBody(RetrieveActivityLogRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("RETRIEVE-ACTIVITY-LOG", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        Timestamp start = null;
        Timestamp end = null;

        if (this.getBody().getStart() != null) {
            start = new Timestamp(this.getBody().getStart());
        }
        if (this.getBody().getEnd() != null) {
            end = new Timestamp(this.getBody().getEnd());
        }

        RetrieveActivityLogData retrieveActivityLogData = getAdminServiceRemote().adminActivityLogRetrieve(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                start, end, this.getBody().getMinLevel(), this.getBody().getMaxLevel(), this.getBody().getSource(), this.getBody().getGroupId(), this.getBody().getPhaseId(),
                this.getBody().getMessagePattern());

        RetrieveActivityLogResponse retrieveActivityLogResponse = new RetrieveActivityLogResponse();

        List<ActivityLog> activityLogList = retrieveActivityLogData.getActivityLogList();

        if (!activityLogList.isEmpty()) {

            for (ActivityLog activityLog : activityLogList) {

                ActivityLogInfo activityLogInfo = new ActivityLogInfo();

                activityLogInfo.setGroupId(activityLog.getGroupId());
                activityLogInfo.setLevel(activityLog.getLevel().toString());
                activityLogInfo.setMessage(activityLog.getMessage());
                activityLogInfo.setPhaseId(activityLog.getPhaseId());
                activityLogInfo.setSource(activityLog.getSource());
                activityLogInfo.setTimestamp(activityLog.getTimestamp());

                retrieveActivityLogResponse.getActivityLogData().add(activityLogInfo);
            }
        }

        status.setStatusCode(retrieveActivityLogData.getStatusCode());
        status.setStatusMessage(prop.getProperty(retrieveActivityLogData.getStatusCode()));

        retrieveActivityLogResponse.setStatus(status);

        return retrieveActivityLogResponse;
    }

}
