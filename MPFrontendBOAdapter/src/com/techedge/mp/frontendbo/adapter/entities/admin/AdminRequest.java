package com.techedge.mp.frontendbo.adapter.entities.admin;

import com.techedge.mp.frontendbo.adapter.entities.admin.addcardbin.AddCardBinRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addprepaidconsumevoucherdetail.AddPrePaidConsumeVoucherDetailRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addroletoadmin.AdminAddRoleToAdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager.AddStationManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion.AddSurveyQuestionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addusertypetousercategory.AddUserTypeToUserCategoryRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion.AddVoucherToPromotionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.admineditmappingerror.AdminEditErrorRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror.AdminGetErrorRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror.AdminRemoveErrorRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.archive.ArchiveRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.assignvoucher.AdminAssignVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.authentication.AuthenticationAdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.checklegacypassword.AdminCheckLegacyPasswordRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.contactkeybulk.AdminContactKeyBulkRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.countpendingtransactions.CountPendingTransactionsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.create.CreateAdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createadminrole.AdminCreateAdminRoleRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createblockperiod.AdminCreateBlockPeriodRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createcustomeruser.CreateCustomerUserRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createdocument.AdminCreateDocumentRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute.AdminCreateDocumentAttributeRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createemaildomain.CreateEmailDomainRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createmanager.CreateManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror.AdminCreateErrorRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber.CreatePrefixNumberRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createprovince.AdminCreateProvinceRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createrefuelinguser.AdminCreateRefuelingUserRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createsurvey.CreateSurveyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createusercategory.CreateUserCategoryRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createusertype.CreateUserTypeRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deleteadmin.AdminDeleteRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deleteadminrole.AdminDeleteAdminRoleRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deleteblockperiod.AdminDeleteBlockPeriodRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument.AdminDeleteDocumentRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocumentattribute.AdminDeleteDocumentAttributeRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletemanager.DeleteManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletesurveyquestion.DeleteSurveyQuestionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters.DeleteTestersRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deleteuser.DeleteUserRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deleteusernotverified.DeleteUserNotVerifiedRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletevoucher.AdminDeleteVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetbrandlist.AdminDwhGetBrandListRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcatalog.AdminDwhGetCatalogRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcategoryburnlist.AdminDwhGetCategoryBurnListRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcategoryearnlist.AdminDwhGetCategoryEarnListRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetpartnerlist.AdminDwhGetPartnerListRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.editemaildomain.EditEmailDomainRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.endparking.AdminEndParkingRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.estimateendparkingprice.AdminEstimateEndParkingPriceRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.estimateextendparkingprice.AdminEstimateExtendedParkingPriceRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.estimateparkingprice.AdminEstimateParkingPriceRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.extendparking.AdminExtendParkingRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod.FindPaymentMethodRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.forcesmsnotificationstatus.AdminForceSmsNotificationStatusRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.forceupdatetermsfservice.ForceUpdateTermsOfServiceRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.generalcreate.AdminGeneralCreateRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.generalremovefromid.AdminGeneralRemoveRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.generalremovemassive.AdminGeneralMassiveRemoveRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.generalupdate.AdminGeneralUpdateRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.generatemailinglist.AdminGenerateMailingListRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.generatepv.AdminGeneratePvRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.generatetesters.GenerateTestersRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.getcities.AdminGetCitiesRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.getemaildomain.RemoveEmailDomainRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.getparkingzonesbycity.AdminGetParkingZonesByCityRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey.GetSurveyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.gfgtransactionreconciliation.GFGTransactionReconciliationRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.insertmulticardrefuelinguser.AdminInsertMulticardRefuelingUserRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.insertpaymentmethodrefuelinguser.AdminInsertPaymentMethodRefuelingUserRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.insertuserinpromoonhold.InsertUserInPromoOnHoldRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.logout.LogoutAdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.parkingretrieveparkingzones.AdminRetrieveParkingZonesRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.propagationuserdata.AdminPropagationUserDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.pushnotificationsend.AdminPushNotificationSendRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.reconciliation.ReconciliationRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.reconciliationdetail.ReconciliationDetailRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.reconciliationuser.ReconciliationUserRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.refreshparams.RefreshParamsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.removeemaildomain.GetEmailDomainRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.removemobilephone.AdminRemoveMobilePhoneRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.removepaymentmethodrefuelinguser.AdminRemovePaymentMethodRefuelingUserRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.removeprefixnumber.RemovePrefixNumberRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.removeroletoadmin.AdminRemoveRoleToAdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.removeticket.AdminRemoveTicketRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.removetransactionevent.AdminRemoveTransactionEventRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.removeusertypefromusercategory.RemoveUserTypeFromUserCategoryRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.resendconfirmuseremail.ResendConfirmUserEmailRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.resetsurvey.ResetSurveyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog.RetrieveActivityLogRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveadmin.AdminRetrieveRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveadminrole.AdminRetrieveAdminRoleRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveblockperiod.AdminRetrieveBlockPeriodRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievedocument.AdminRetrieveDocumentRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievelandingpages.RetrieveLandingPagesRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveparams.RetrieveParamsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepaymentdatarefuelinguser.AdminRetrievePaymentDataRefuelingUserRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepollinginterval.PollingIntervalRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions.RetrievePoPTransactionsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactionsreconciliation.RetrievePoPTransactionsReconciliationRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveprefixes.RetrievePrefixesRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepromotions.RetrievePromotionsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievestatistics.RetrieveStatisticsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions.RetrieveTransactionsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactionsmanager.RetrieveTransactionsManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusercategory.RetrieveUserCategoryRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers.RetrieveUsersRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusertype.RetrieveUserTypeRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions.RetrieveVoucherTransactionsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.searchmanager.SearchManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.startparking.AdminStartParkingRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation.StationAddRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation.StationDeleteRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation.StationSearchRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation.StationUpdateRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationsoperation.StationsAddRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationsoperation.StationsDeleteRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.systemcheck.AdminSystemCheckRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testauthorizationplus.TestAuthorizationPlusRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreauthorizationconsumevoucher.TestCancelPreAuthorizationConsumeVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcheckconsumevouchertransaction.TestCheckConsumeVoucherTransactionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcheckloadloyaltycreditstransaction.TestCheckLoadLoyaltyCreditsTransactionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcheckvoucher.TestCheckVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testconsumevoucher.TestConsumeVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher.TestCreateVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcrmsfgetmission.TestCrmSfGetMissionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcrmsfnotifyevent.TestCrmSfNotifyEventRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testdeletemccardrefueling.TestDeleteMcCardRefuelingRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testdeletevoucher.TestDeleteVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testenableloyaltycard.TestEnableLoyaltyCardRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testexecuteauthorization.TestExecuteAuthorizationRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testexecutecapture.TestExecuteCaptureRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testexecutepayment.TestExecutePaymentRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testexecutereversal.TestExecuteReversalRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testgetloyaltycardlist.TestGetLoyaltyCardListRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testgetmccardstatus.TestGetMcCardStatusRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testgetpaymentstatus.TestGetPaymentStatusRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testgetpaymenttransactiondetails.TestGetPaymentTransactionDetailsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testgettransactionstatus.TestGetTransactionStatusRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testinforedemption.TestInfoRedemptionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testloadloyaltycredits.TestLoadLoyaltyCreditsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testmcenjoycardauthorize.TestMcEnjoyCardAuthorizeRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher.TestPreAuthorizationConsumeVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testredemption.TestRedemptionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testretrievestationdetails.TestRetrieveStationRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testreverseconsumevouchertransaction.TestReverseConsumeVoucherTransactionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testreverseloadloyaltycreditstransaction.TestReverseLoadLoyaltyCreditsTransactionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testscheduler.TestSchedulerRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction.TestSendMPTransactionNotificationRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction.TestSendMPTransactionReportRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testtransactionreconciliation.TestTransactionReconciliationRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.unarchive.UnArchiveRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateadmin.AdminUpdateRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateblockperiod.AdminUpdateBlockPeriodRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatecities.AdminUpdateCitiesRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatedocument.AdminUpdateDocumentRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatemailinglist.AdminUpdateMailingListRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager.UpdateManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateparam.UpdateParamRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateparams.UpdateParamsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatepassword.AdminUpdatePasswordRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatepasswordrefuelinguser.AdminUpdatePasswordRefuelingUserRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethod.UpdatePaymentMethodRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethodstatus.UpdatePaymentMethodStatusRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatepoptransaction.UpdatePopTransactionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateprepaidconsumevoucher.UpdatePrePaidConsumeVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatepromotion.UpdatePromotionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatepvactivationflag.AdminUpdatePvActivationFlagRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatesurvey.UpdateSurveyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatetransaction.UpdateTransactionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateuser.UpdateUserRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatevoucherdata.AdminUpdateVoucherDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatevouchertransaction.UpdateVoucherTransactionRequest;

public class AdminRequest extends RootRequest {

    protected CreateAdminRequest                              createAdminUser;
    protected AuthenticationAdminRequest                      authenticationAdmin;
    protected LogoutAdminRequest                              logoutAdmin;
    protected RetrieveActivityLogRequest                      retrieveActivityLog;
    protected RetrieveUsersRequest                            retrieveUsers;
    protected RetrieveTransactionsRequest                     retrieveTransactions;
    protected RetrievePoPTransactionsRequest                  retrievePoPTransactions;
    protected UpdateUserRequest                               updateUser;
    protected UpdatePaymentMethodRequest                      updatePaymentMethod;
    protected UpdateTransactionRequest                        updateTransaction;
    protected UpdatePopTransactionRequest                     updatePopTransaction;
    protected UpdateVoucherTransactionRequest                 updateVoucherTransaction;
    protected FindPaymentMethodRequest                        findPaymentMethod;
    protected CreateCustomerUserRequest                       createCustomerUser;
    protected DeleteUserRequest                               deleteUser;
    protected GenerateTestersRequest                          generateTesters;
    protected DeleteTestersRequest                            deleteTesters;
    protected RetrieveStatisticsRequest                       retrieveStatistics;
    protected RetrievePoPTransactionsReconciliationRequest    retrievePoPTransactionsReconciliation;

    protected GFGTransactionReconciliationRequest             gfgTransactionReconciliation;

    protected CreateManagerRequest                            createManager;
    protected DeleteManagerRequest                            deleteManager;
    protected UpdateManagerRequest                            updateManager;
    protected SearchManagerRequest                            searchManager;
    protected AddStationManagerRequest                        addStationManager;
    protected RetrieveTransactionsManagerRequest              retrieveTransactionsManager;

    protected StationAddRequest                               stationAdd;
    protected StationUpdateRequest                            stationUpdate;
    protected StationDeleteRequest                            stationDelete;
    protected StationsAddRequest                              stationsAdd;
    protected StationsDeleteRequest                           stationsDelete;
    protected StationSearchRequest                           stationsSearch;

    protected RetrieveParamsRequest                           retrieveParams;
    protected UpdateParamRequest                              updateParam;
    protected UpdateParamsRequest                             updateParams;
    protected RefreshParamsRequest                            refreshParams;

    protected ArchiveRequest                                  archive;

    protected UnArchiveRequest                                unArchive;

    protected ReconciliationRequest                           reconciliation;
    protected ReconciliationDetailRequest                     reconciliationDetail;

    protected ResendConfirmUserEmailRequest                   resendConfirmUserEmail;

    protected TestCheckConsumeVoucherTransactionRequest       testCheckConsumeVoucherTransaction;
    protected TestCheckLoadLoyaltyCreditsTransactionRequest   testCheckLoadLoyaltyCreditsTransaction;
    protected TestCheckVoucherRequest                         testCheckVoucher;
    protected TestConsumeVoucherRequest                       testConsumeVoucher;
    protected TestEnableLoyaltyCardRequest                    testEnableLoyaltyCard;
    protected TestGetLoyaltyCardListRequest                   testGetLoyaltyCardList;
    protected TestLoadLoyaltyCreditsRequest                   testLoadLoyaltyCredits;
    protected TestReverseConsumeVoucherTransactionRequest     testReverseConsumeVoucherTransaction;
    protected TestReverseLoadLoyaltyCreditsTransactionRequest testReverseLoadLoyaltyCreditsTransaction;
    protected TestPreAuthorizationConsumeVoucherRequest       testPreAuthorizationConsumeVoucher;
    protected TestCancelPreAuthorizationConsumeVoucherRequest testCancelPreAuthorizationConsumeVoucher;
    protected TestCreateVoucherRequest                        testCreateVoucher;
    protected TestDeleteVoucherRequest                        testDeleteVoucher;

    protected TestInfoRedemptionRequest                       testInfoRedemption;
    protected TestRedemptionRequest                           testRedemption;

    protected TestRetrieveStationRequest                      testRetrieveStation;
    protected TestTransactionReconciliationRequest            testTransactionReconciliation;
    protected TestGetTransactionStatusRequest                 testGetTransactionStatus;

    protected AddVoucherToPromotionRequest                    addVoucherToPromotion;

    protected CreateSurveyRequest                             createSurvey;
    protected UpdateSurveyRequest                             updateSurvey;
    protected GetSurveyRequest                                getSurvey;
    protected ResetSurveyRequest                              resetSurvey;
    protected AddSurveyQuestionRequest                        addSurveyQuestion;
    protected DeleteSurveyQuestionRequest                     deleteSurveyQuestion;

    protected TestSendMPTransactionNotificationRequest        testCheckSendMPTransactionNotification;
    protected TestSendMPTransactionReportRequest              testCheckSendMPTransactionReport;

    protected TestSchedulerRequest                            testScheduler;

    protected AddCardBinRequest                               addCardBin;

    protected RetrievePromotionsRequest                       retrievePromotions;
    protected UpdatePromotionRequest                          updatePromotion;

    protected AdminCreateErrorRequest                         adminCreateError;
    protected AdminEditErrorRequest                           adminEditError;
    protected AdminRemoveErrorRequest                         adminRemoveError;
    protected AdminGetErrorRequest                            adminGetError;

    protected CreateUserCategoryRequest                       createUserCategory;
    protected CreateUserTypeRequest                           createUserType;
    protected RetrieveUserCategoryRequest                     retrieveUserCategory;
    protected RetrieveUserTypeRequest                         retrieveUserType;
    protected AddUserTypeToUserCategoryRequest                addUserTypeToUserCategory;
    protected RemoveUserTypeFromUserCategoryRequest           removeUserTypeFromUserCategory;

    protected CreatePrefixNumberRequest                       createPrefixNumber;
    protected RemovePrefixNumberRequest                       removePrefixNumber;
    protected RetrievePrefixesRequest                         retrievePrefixes;

    protected RetrieveVoucherTransactionsRequest              retrieveVoucherTransactions;
    protected ForceUpdateTermsOfServiceRequest                forceUpdateTermsOfService;
    protected AdminCreateBlockPeriodRequest                   createBlockPeriod;
    protected AdminUpdateBlockPeriodRequest                   updateBlockPeriod;
    protected AdminDeleteBlockPeriodRequest                   deleteBlockPeriod;
    protected AdminRetrieveBlockPeriodRequest                 retrieveBlockPeriod;
    protected AdminCreateDocumentRequest                      createDocument;
    protected AdminDeleteDocumentRequest                      deleteDocument;
    protected AdminRetrieveDocumentRequest                    retrieveDocument;
    protected AdminUpdateDocumentRequest                      updateDocument;
    protected AdminCreateDocumentAttributeRequest             createDocumentAttribute;
    protected AdminDeleteDocumentAttributeRequest             deleteDocumentAttribute;
    protected AdminGeneratePvRequest                          generatePv;
    protected AdminUpdatePasswordRequest                      updateAdminPassword;
    protected AdminForceSmsNotificationStatusRequest          forceSmsNotificationStatus;
    protected AdminGenerateMailingListRequest                 generateMailingList;
    protected AdminUpdateMailingListRequest                   updateMailingList;
    protected AdminAssignVoucherRequest                       assignVoucher;
    protected AdminRemoveTransactionEventRequest              removeTransactionEvent;
    protected CreateEmailDomainRequest                        createEmailDomain;
    protected GetEmailDomainRequest                           getEmailDomain;
    protected RemoveEmailDomainRequest                        removeEmailDomain;
    protected EditEmailDomainRequest                          editEmailDomain;
    protected InsertUserInPromoOnHoldRequest                  insertUserInPromoOnHold;

    protected AdminUpdateVoucherDataRequest                   updateVoucherData;
    protected AdminDeleteVoucherRequest                       deleteVoucher;
    protected AdminCreateProvinceRequest                      createProvince;
    protected AdminRemoveMobilePhoneRequest                   removeMobilePhone;
    protected AdminPropagationUserDataRequest                 propagationUserData;
    protected AdminCheckLegacyPasswordRequest                 checkLegacyPassword;
    protected AdminGeneralCreateRequest                       adminGeneralCreate;
    protected AdminGeneralUpdateRequest                       adminGeneralUpdate;
    protected UpdatePrePaidConsumeVoucherRequest              updatePrePaidConsumeVoucher;
    protected AddPrePaidConsumeVoucherDetailRequest           addPrePaidConsumeVoucherDetail;
    protected AdminGeneralRemoveRequest                       adminGeneralRemove;

    protected AdminCreateAdminRoleRequest                     createAdminRole;
    protected AdminDeleteAdminRoleRequest                     deleteAdminRole;
    protected AdminAddRoleToAdminRequest                      addRoleToAdmin;
    protected AdminRetrieveAdminRoleRequest                   retrieveAdminRole;
    protected AdminRemoveRoleToAdminRequest                   removeRoleToAdmin;
    protected AdminPushNotificationSendRequest                adminPushNotificationSend;
    protected AdminRetrieveRequest                            retrieveAdminUsers;
    protected AdminUpdateRequest                              updateAdminUser;
    protected AdminDeleteRequest                              deleteAdminUser;

    protected AdminCreateRefuelingUserRequest                 createRefuelingUser;
    protected AdminInsertPaymentMethodRefuelingUserRequest    insertPaymentMethodRefuelingUser;
    protected AdminUpdatePasswordRefuelingUserRequest         updatePasswordRefuelingUser;
    protected AdminRetrievePaymentDataRefuelingUserRequest    retrievePaymentDataRefuelingUser;
    protected AdminRemovePaymentMethodRefuelingUserRequest    removePaymentMethodRefuelingUser;

    protected RetrieveLandingPagesRequest                     retrieveLandingPages;

    protected AdminGeneralMassiveRemoveRequest                adminGeneralMassiveRemove;

    protected PollingIntervalRequest                          adminRetrievePollingInterval;

    protected AdminUpdateCitiesRequest                        adminUpdateCities;

    protected ReconciliationUserRequest                       reconciliationUser;

    protected DeleteUserNotVerifiedRequest                    deleteUserNotVerified;

    protected AdminUpdatePvActivationFlagRequest              adminUpdatePvActivationFlag;

    protected AdminRemoveTicketRequest                        adminRemoveTicket;

    protected AdminDwhGetPartnerListRequest                   adminDwhGetPartnerList;
    protected AdminDwhGetCategoryEarnListRequest              adminDwhGetCategoryEarnList;
    protected AdminDwhGetCatalogRequest                       adminDwhGetCatalog;
    protected AdminDwhGetBrandListRequest                     adminDwhGetBrandList;
    protected AdminDwhGetCategoryBurnListRequest              adminDwhGetCategoryBurnList;

    protected AdminGetCitiesRequest                           adminGetCities;
    protected AdminGetParkingZonesByCityRequest               adminGetParkingZonesByCity;
    protected AdminRetrieveParkingZonesRequest                adminRetrieveParkingZones;
    protected AdminEstimateParkingPriceRequest                adminEstimateParkingPrice;
    protected AdminStartParkingRequest                        adminStartParking;
    protected AdminEstimateExtendedParkingPriceRequest        adminEstimateExtendedParkingPrice;
    protected AdminExtendParkingRequest                       adminExtendParking;
    protected AdminEstimateEndParkingPriceRequest             adminEstimateEndParkingPrice;
    protected AdminEndParkingRequest                          adminEndParking;
    
    protected AdminSystemCheckRequest                         adminSystemCheck;
    
    protected TestGetMcCardStatusRequest                      testGetMcCardStatus;
    protected TestExecutePaymentRequest                       testExecutePayment;
    protected TestExecuteAuthorizationRequest                 testExecuteAuthorization;
    protected TestExecuteCaptureRequest                 	  testExecuteCapture;
    protected TestExecuteReversalRequest                 	  testExecuteReversal;
    protected TestGetPaymentStatusRequest                 	  testGetPaymentStatus;
    protected TestAuthorizationPlusRequest                 	  testAuthorizationPlus;
    protected TestMcEnjoyCardAuthorizeRequest                 testMcEnjoyCardAuthorize;
    protected TestDeleteMcCardRefuelingRequest                testDeleteMcCardRefueling;
    
    protected TestGetPaymentTransactionDetailsRequest         testGetPaymentTransactionDetails;
    
    protected AdminContactKeyBulkRequest               		  adminContactKeyBulk;
    
    protected TestCrmSfNotifyEventRequest 					  testCrmSfNotifyEvent;
    protected TestCrmSfGetMissionRequest 					  testCrmSfGetMission;
    
    protected AdminInsertMulticardRefuelingUserRequest        insertMulticardRefuelingUser;
    protected UpdatePaymentMethodStatusRequest                updatePaymentMethodStatus;
    
    protected CountPendingTransactionsRequest                 countPendingTransactions;
    

    public CreateAdminRequest getCreateAdminUser() {
        return createAdminUser;
    }

    public void setCreateAdminUser(CreateAdminRequest createAdminUser) {
        this.createAdminUser = createAdminUser;
    }

    public AuthenticationAdminRequest getAuthenticationAdmin() {
        return authenticationAdmin;
    }

    public void setAuthenticationAdmin(AuthenticationAdminRequest authenticationAdmin) {
        this.authenticationAdmin = authenticationAdmin;
    }

    public LogoutAdminRequest getLogoutAdmin() {
        return logoutAdmin;
    }

    public void setLogoutAdmin(LogoutAdminRequest logoutAdmin) {
        this.logoutAdmin = logoutAdmin;
    }

    public RetrieveActivityLogRequest getRetrieveActivityLog() {
        return retrieveActivityLog;
    }

    public void setRetrieveActivityLog(RetrieveActivityLogRequest retrieveActivityLog) {
        this.retrieveActivityLog = retrieveActivityLog;
    }

    public RetrieveUsersRequest getRetrieveUsers() {
        return retrieveUsers;
    }

    public void setRetrieveUsers(RetrieveUsersRequest retrieveUsers) {
        this.retrieveUsers = retrieveUsers;
    }

    public RetrieveTransactionsRequest getRetrieveTransactions() {
        return retrieveTransactions;
    }

    public void setRetrieveTransactions(RetrieveTransactionsRequest retrieveTransactions) {
        this.retrieveTransactions = retrieveTransactions;
    }

    public RetrievePoPTransactionsRequest getRetrievePoPTransactions() {
        return retrievePoPTransactions;
    }

    public void setRetrievePoPTransactions(RetrievePoPTransactionsRequest retrievePoPTransactions) {
        this.retrievePoPTransactions = retrievePoPTransactions;
    }

    public UpdateUserRequest getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(UpdateUserRequest updateUser) {
        this.updateUser = updateUser;
    }

    public UpdatePaymentMethodRequest getUpdatePaymentMethod() {
        return updatePaymentMethod;
    }

    public void setUpdatePaymentMethod(UpdatePaymentMethodRequest updatePaymentMethod) {
        this.updatePaymentMethod = updatePaymentMethod;
    }

    public UpdateTransactionRequest getUpdateTransaction() {
        return updateTransaction;
    }

    public void setUpdateTransaction(UpdateTransactionRequest updateTransaction) {
        this.updateTransaction = updateTransaction;
    }

    public UpdatePopTransactionRequest getUpdatePopTransaction() {
        return updatePopTransaction;
    }

    public void setUpdatePopTransaction(UpdatePopTransactionRequest updatePopTransaction) {
        this.updatePopTransaction = updatePopTransaction;
    }

    public UpdateVoucherTransactionRequest getUpdateVoucherTransaction() {
        return updateVoucherTransaction;
    }

    public void setUpdateVoucherTransaction(UpdateVoucherTransactionRequest updateVoucherTransaction) {
        this.updateVoucherTransaction = updateVoucherTransaction;
    }

    public FindPaymentMethodRequest getFindPaymentMethod() {
        return findPaymentMethod;
    }

    public void setFindPaymentMethod(FindPaymentMethodRequest findPaymentMethod) {
        this.findPaymentMethod = findPaymentMethod;
    }

    public CreateCustomerUserRequest getCreateCustomerUser() {
        return createCustomerUser;
    }

    public void setCreateCustomerUser(CreateCustomerUserRequest createCustomerUser) {
        this.createCustomerUser = createCustomerUser;
    }

    public DeleteUserRequest getDeleteUser() {
        return deleteUser;
    }

    public void setDeleteUser(DeleteUserRequest deleteUser) {
        this.deleteUser = deleteUser;
    }

    public GenerateTestersRequest getGenerateTesters() {
        return generateTesters;
    }

    public void setGenerateTesters(GenerateTestersRequest generateTesters) {
        this.generateTesters = generateTesters;
    }

    public DeleteTestersRequest getDeleteTesters() {
        return deleteTesters;
    }

    public void setDeleteTesters(DeleteTestersRequest deleteTesters) {
        this.deleteTesters = deleteTesters;
    }

    public RetrieveStatisticsRequest getRetrieveStatistics() {
        return retrieveStatistics;
    }

    public void setRetrieveStatistics(RetrieveStatisticsRequest retrieveStatistics) {
        this.retrieveStatistics = retrieveStatistics;
    }

    public RetrievePoPTransactionsReconciliationRequest getRetrievePoPTransactionsReconciliation() {
        return retrievePoPTransactionsReconciliation;
    }

    public void setRetrievePoPTransactionsReconciliation(RetrievePoPTransactionsReconciliationRequest retrievePoPTransactionsReconciliation) {
        this.retrievePoPTransactionsReconciliation = retrievePoPTransactionsReconciliation;
    }

    public GFGTransactionReconciliationRequest getGfgTransactionReconciliation() {
        return gfgTransactionReconciliation;
    }

    public void setGfgTransactionReconciliation(GFGTransactionReconciliationRequest gfgTransactionReconciliation) {
        this.gfgTransactionReconciliation = gfgTransactionReconciliation;
    }

    public CreateManagerRequest getCreateManager() {
        return createManager;
    }

    public void setCreateManager(CreateManagerRequest createManager) {
        this.createManager = createManager;
    }

    public DeleteManagerRequest getDeleteManager() {
        return deleteManager;
    }

    public void setDeleteManager(DeleteManagerRequest deleteManager) {
        this.deleteManager = deleteManager;
    }

    public UpdateManagerRequest getUpdateManager() {
        return updateManager;
    }

    public void setUpdateManager(UpdateManagerRequest updateManager) {
        this.updateManager = updateManager;
    }

    public SearchManagerRequest getSearchManager() {
        return searchManager;
    }

    public void setSearchManager(SearchManagerRequest searchManager) {
        this.searchManager = searchManager;
    }

    public AddStationManagerRequest getAddStationManager() {
        return addStationManager;
    }

    public void setAddStationManager(AddStationManagerRequest addStationManager) {
        this.addStationManager = addStationManager;
    }

    public RetrieveTransactionsManagerRequest getRetrieveTransactionsManager() {
        return retrieveTransactionsManager;
    }

    public void setRetrieveTransactionsManager(RetrieveTransactionsManagerRequest retrieveTransactionsManager) {
        this.retrieveTransactionsManager = retrieveTransactionsManager;
    }

    public StationAddRequest getStationAdd() {
        return stationAdd;
    }

    public void setStationAdd(StationAddRequest stationAdd) {
        this.stationAdd = stationAdd;
    }

    public StationUpdateRequest getStationUpdate() {
        return stationUpdate;
    }

    public void setStationUpdate(StationUpdateRequest stationUpdate) {
        this.stationUpdate = stationUpdate;
    }

    public StationDeleteRequest getStationDelete() {
        return stationDelete;
    }

    public void setStationDelete(StationDeleteRequest stationDelete) {
        this.stationDelete = stationDelete;
    }

    public StationsAddRequest getStationsAdd() {
        return stationsAdd;
    }

    public void setStationsAdd(StationsAddRequest stationsAdd) {
        this.stationsAdd = stationsAdd;
    }

    public StationsDeleteRequest getStationsDelete() {
        return stationsDelete;
    }

    public void setStationsDelete(StationsDeleteRequest stationsDelete) {
        this.stationsDelete = stationsDelete;
    }

    public StationSearchRequest getStationsSearch() {
        return stationsSearch;
    }

    public void setStationsSearch(StationSearchRequest stationsSearch) {
        this.stationsSearch = stationsSearch;
    }

    public RetrieveParamsRequest getRetrieveParams() {
        return retrieveParams;
    }

    public void setRetrieveParams(RetrieveParamsRequest retrieveParams) {
        this.retrieveParams = retrieveParams;
    }

    public UpdateParamRequest getUpdateParam() {
        return updateParam;
    }

    public void setUpdateParam(UpdateParamRequest updateParam) {
        this.updateParam = updateParam;
    }

    public UpdateParamsRequest getUpdateParams() {
        return updateParams;
    }

    public void setUpdateParams(UpdateParamsRequest updateParams) {
        this.updateParams = updateParams;
    }

    public RefreshParamsRequest getRefreshParams() {
        return refreshParams;
    }

    public void setRefreshParams(RefreshParamsRequest refreshParams) {
        this.refreshParams = refreshParams;
    }

    public ArchiveRequest getArchive() {
        return archive;
    }

    public void setArchive(ArchiveRequest archive) {
        this.archive = archive;
    }

    public UnArchiveRequest getUnArchive() {
        return unArchive;
    }

    public void setUnArchive(UnArchiveRequest unArchive) {
        this.unArchive = unArchive;
    }

    public ReconciliationRequest getReconciliation() {
        return reconciliation;
    }

    public void setReconciliation(ReconciliationRequest reconciliation) {
        this.reconciliation = reconciliation;
    }

    public ReconciliationDetailRequest getReconciliationDetail() {
        return reconciliationDetail;
    }

    public void setReconciliationDetail(ReconciliationDetailRequest reconciliationDetail) {
        this.reconciliationDetail = reconciliationDetail;
    }

    public ResendConfirmUserEmailRequest getResendConfirmUserEmail() {
        return resendConfirmUserEmail;
    }

    public void setResendConfirmUserEmail(ResendConfirmUserEmailRequest resendConfirmUserEmail) {
        this.resendConfirmUserEmail = resendConfirmUserEmail;
    }

    public TestCheckConsumeVoucherTransactionRequest getTestCheckConsumeVoucherTransaction() {
        return testCheckConsumeVoucherTransaction;
    }

    public void setTestCheckConsumeVoucherTransaction(TestCheckConsumeVoucherTransactionRequest testCheckConsumeVoucherTransaction) {
        this.testCheckConsumeVoucherTransaction = testCheckConsumeVoucherTransaction;
    }

    public TestCheckLoadLoyaltyCreditsTransactionRequest getTestCheckLoadLoyaltyCreditsTransaction() {
        return testCheckLoadLoyaltyCreditsTransaction;
    }

    public void setTestCheckLoadLoyaltyCreditsTransaction(TestCheckLoadLoyaltyCreditsTransactionRequest testCheckLoadLoyaltyCreditsTransaction) {
        this.testCheckLoadLoyaltyCreditsTransaction = testCheckLoadLoyaltyCreditsTransaction;
    }

    public TestCheckVoucherRequest getTestCheckVoucher() {
        return testCheckVoucher;
    }

    public void setTestCheckVoucher(TestCheckVoucherRequest testCheckVoucher) {
        this.testCheckVoucher = testCheckVoucher;
    }

    public TestConsumeVoucherRequest getTestConsumeVoucher() {
        return testConsumeVoucher;
    }

    public void setTestConsumeVoucher(TestConsumeVoucherRequest testConsumeVoucher) {
        this.testConsumeVoucher = testConsumeVoucher;
    }

    public TestEnableLoyaltyCardRequest getTestEnableLoyaltyCard() {
        return testEnableLoyaltyCard;
    }

    public void setTestEnableLoyaltyCard(TestEnableLoyaltyCardRequest testEnableLoyaltyCard) {
        this.testEnableLoyaltyCard = testEnableLoyaltyCard;
    }

    public TestGetLoyaltyCardListRequest getTestGetLoyaltyCardList() {
        return testGetLoyaltyCardList;
    }

    public void setTestGetLoyaltyCardList(TestGetLoyaltyCardListRequest testGetLoyaltyCardList) {
        this.testGetLoyaltyCardList = testGetLoyaltyCardList;
    }

    public TestLoadLoyaltyCreditsRequest getTestLoadLoyaltyCredits() {
        return testLoadLoyaltyCredits;
    }

    public void setTestLoadLoyaltyCredits(TestLoadLoyaltyCreditsRequest testLoadLoyaltyCredits) {
        this.testLoadLoyaltyCredits = testLoadLoyaltyCredits;
    }

    public TestReverseConsumeVoucherTransactionRequest getTestReverseConsumeVoucherTransaction() {
        return testReverseConsumeVoucherTransaction;
    }

    public void setTestReverseConsumeVoucherTransaction(TestReverseConsumeVoucherTransactionRequest testReverseConsumeVoucherTransaction) {
        this.testReverseConsumeVoucherTransaction = testReverseConsumeVoucherTransaction;
    }

    public TestReverseLoadLoyaltyCreditsTransactionRequest getTestReverseLoadLoyaltyCreditsTransaction() {
        return testReverseLoadLoyaltyCreditsTransaction;
    }

    public void setTestReverseLoadLoyaltyCreditsTransaction(TestReverseLoadLoyaltyCreditsTransactionRequest testReverseLoadLoyaltyCreditsTransaction) {
        this.testReverseLoadLoyaltyCreditsTransaction = testReverseLoadLoyaltyCreditsTransaction;
    }

    public TestPreAuthorizationConsumeVoucherRequest getTestPreAuthorizationConsumeVoucher() {
        return testPreAuthorizationConsumeVoucher;
    }

    public void setTestPreAuthorizationConsumeVoucher(TestPreAuthorizationConsumeVoucherRequest testPreAuthorizationConsumeVoucher) {
        this.testPreAuthorizationConsumeVoucher = testPreAuthorizationConsumeVoucher;
    }

    public TestCancelPreAuthorizationConsumeVoucherRequest getTestCancelPreAuthorizationConsumeVoucher() {
        return testCancelPreAuthorizationConsumeVoucher;
    }

    public void setTestCancelPreAuthorizationConsumeVoucher(TestCancelPreAuthorizationConsumeVoucherRequest testCancelPreAuthorizationConsumeVoucher) {
        this.testCancelPreAuthorizationConsumeVoucher = testCancelPreAuthorizationConsumeVoucher;
    }

    public TestCreateVoucherRequest getTestCreateVoucher() {
        return testCreateVoucher;
    }

    public void setTestCreateVoucher(TestCreateVoucherRequest testCreateVoucher) {
        this.testCreateVoucher = testCreateVoucher;
    }

    public TestDeleteVoucherRequest getTestDeleteVoucher() {
        return testDeleteVoucher;
    }

    public void setTestDeleteVoucher(TestDeleteVoucherRequest testDeleteVoucher) {
        this.testDeleteVoucher = testDeleteVoucher;
    }

    public TestInfoRedemptionRequest getTestInfoRedemption() {
        return testInfoRedemption;
    }

    public void setTestInfoRedemption(TestInfoRedemptionRequest testInfoRedemption) {
        this.testInfoRedemption = testInfoRedemption;
    }

    public TestRedemptionRequest getTestRedemption() {
        return testRedemption;
    }

    public void setTestRedemption(TestRedemptionRequest testRedemption) {
        this.testRedemption = testRedemption;
    }

    public TestRetrieveStationRequest getTestRetrieveStation() {
        return testRetrieveStation;
    }

    public void setTestRetrieveStation(TestRetrieveStationRequest testRetrieveStation) {
        this.testRetrieveStation = testRetrieveStation;
    }

    public AddVoucherToPromotionRequest getAddVoucherToPromotion() {
        return addVoucherToPromotion;
    }

    public void setAddVoucherToPromotion(AddVoucherToPromotionRequest addVoucherToPromotion) {
        this.addVoucherToPromotion = addVoucherToPromotion;
    }

    public CreateSurveyRequest getCreateSurvey() {
        return createSurvey;
    }

    public void setCreateSurvey(CreateSurveyRequest createSurvey) {
        this.createSurvey = createSurvey;
    }

    public UpdateSurveyRequest getUpdateSurvey() {
        return updateSurvey;
    }

    public void setUpdateSurvey(UpdateSurveyRequest updateSurvey) {
        this.updateSurvey = updateSurvey;
    }

    public GetSurveyRequest getGetSurvey() {
        return getSurvey;
    }

    public void setGetSurvey(GetSurveyRequest getSurvey) {
        this.getSurvey = getSurvey;
    }

    public ResetSurveyRequest getResetSurvey() {
        return resetSurvey;
    }

    public void setResetSurvey(ResetSurveyRequest resetSurvey) {
        this.resetSurvey = resetSurvey;
    }

    public AddSurveyQuestionRequest getAddSurveyQuestion() {
        return addSurveyQuestion;
    }

    public void setAddSurveyQuestion(AddSurveyQuestionRequest addSurveyQuestion) {
        this.addSurveyQuestion = addSurveyQuestion;
    }

    public DeleteSurveyQuestionRequest getDeleteSurveyQuestion() {
        return deleteSurveyQuestion;
    }

    public void setDeleteSurveyQuestion(DeleteSurveyQuestionRequest deleteSurveyQuestion) {
        this.deleteSurveyQuestion = deleteSurveyQuestion;
    }

    public TestSendMPTransactionNotificationRequest getTestCheckSendMPTransactionNotification() {
        return testCheckSendMPTransactionNotification;
    }

    public void setTestCheckSendMPTransactionNotification(TestSendMPTransactionNotificationRequest testCheckSendMPTransactionNotification) {
        this.testCheckSendMPTransactionNotification = testCheckSendMPTransactionNotification;
    }

    public TestSendMPTransactionReportRequest getTestCheckSendMPTransactionReport() {
        return testCheckSendMPTransactionReport;
    }

    public void setTestCheckSendMPTransactionReport(TestSendMPTransactionReportRequest testCheckSendMPTransactionReport) {
        this.testCheckSendMPTransactionReport = testCheckSendMPTransactionReport;
    }

    public TestSchedulerRequest getTestScheduler() {
        return testScheduler;
    }

    public void setTestScheduler(TestSchedulerRequest testScheduler) {
        this.testScheduler = testScheduler;
    }

    public AddCardBinRequest getAddCardBin() {
        return addCardBin;
    }

    public void setAddCardBin(AddCardBinRequest addCardBin) {
        this.addCardBin = addCardBin;
    }

    public RetrievePromotionsRequest getRetrievePromotions() {
        return retrievePromotions;
    }

    public void setRetrievePromotions(RetrievePromotionsRequest retrievePromotions) {
        this.retrievePromotions = retrievePromotions;
    }

    public UpdatePromotionRequest getUpdatePromotion() {
        return updatePromotion;
    }

    public void setUpdatePromotion(UpdatePromotionRequest updatePromotion) {
        this.updatePromotion = updatePromotion;
    }

    public AdminCreateErrorRequest getAdminCreateError() {
        return adminCreateError;
    }

    public void setAdminCreateError(AdminCreateErrorRequest adminCreateError) {
        this.adminCreateError = adminCreateError;
    }

    public AdminEditErrorRequest getAdminEditError() {
        return adminEditError;
    }

    public void setAdminEditError(AdminEditErrorRequest adminEditError) {
        this.adminEditError = adminEditError;
    }

    public AdminRemoveErrorRequest getAdminRemoveError() {
        return adminRemoveError;
    }

    public void setAdminRemoveError(AdminRemoveErrorRequest adminRemoveError) {
        this.adminRemoveError = adminRemoveError;
    }

    public AdminGetErrorRequest getAdminGetError() {
        return adminGetError;
    }

    public void setAdminGetError(AdminGetErrorRequest adminGetError) {
        this.adminGetError = adminGetError;
    }

    public CreateUserCategoryRequest getCreateUserCategory() {
        return createUserCategory;
    }

    public void setCreateUserCategory(CreateUserCategoryRequest createUserCategory) {
        this.createUserCategory = createUserCategory;
    }

    public CreateUserTypeRequest getCreateUserType() {
        return createUserType;
    }

    public void setCreateUserType(CreateUserTypeRequest createUserType) {
        this.createUserType = createUserType;
    }

    public RetrieveUserCategoryRequest getRetrieveUserCategory() {
        return retrieveUserCategory;
    }

    public void setRetrieveUserCategory(RetrieveUserCategoryRequest retrieveUserCategory) {
        this.retrieveUserCategory = retrieveUserCategory;
    }

    public RetrieveUserTypeRequest getRetrieveUserType() {
        return retrieveUserType;
    }

    public void setRetrieveUserType(RetrieveUserTypeRequest retrieveUserType) {
        this.retrieveUserType = retrieveUserType;
    }

    public AddUserTypeToUserCategoryRequest getAddUserTypeToUserCategory() {
        return addUserTypeToUserCategory;
    }

    public void setAddUserTypeToUserCategory(AddUserTypeToUserCategoryRequest addUserTypeToUserCategory) {
        this.addUserTypeToUserCategory = addUserTypeToUserCategory;
    }

    public RemoveUserTypeFromUserCategoryRequest getRemoveUserTypeFromUserCategory() {
        return removeUserTypeFromUserCategory;
    }

    public void setRemoveUserTypeFromUserCategory(RemoveUserTypeFromUserCategoryRequest removeUserTypeFromUserCategory) {
        this.removeUserTypeFromUserCategory = removeUserTypeFromUserCategory;
    }

    public CreatePrefixNumberRequest getCreatePrefixNumber() {
        return createPrefixNumber;
    }

    public void setCreatePrefixNumber(CreatePrefixNumberRequest createPrefixNumber) {
        this.createPrefixNumber = createPrefixNumber;
    }

    public RemovePrefixNumberRequest getRemovePrefixNumber() {
        return removePrefixNumber;
    }

    public void setRemovePrefixNumber(RemovePrefixNumberRequest removePrefixNumber) {
        this.removePrefixNumber = removePrefixNumber;
    }

    public RetrievePrefixesRequest getRetrievePrefixes() {
        return retrievePrefixes;
    }

    public void setRetrievePrefixes(RetrievePrefixesRequest retrievePrefixes) {
        this.retrievePrefixes = retrievePrefixes;
    }

    public RetrieveVoucherTransactionsRequest getRetrieveVoucherTransactions() {
        return retrieveVoucherTransactions;
    }

    public void setRetrieveVoucherTransactions(RetrieveVoucherTransactionsRequest retrieveVoucherTransactions) {
        this.retrieveVoucherTransactions = retrieveVoucherTransactions;
    }

    public ForceUpdateTermsOfServiceRequest getForceUpdateTermsOfService() {
        return forceUpdateTermsOfService;
    }

    public void setForceUpdateTermsOfService(ForceUpdateTermsOfServiceRequest forceUpdateTermsOfService) {
        this.forceUpdateTermsOfService = forceUpdateTermsOfService;
    }

    public AdminCreateBlockPeriodRequest getCreateBlockPeriod() {
        return createBlockPeriod;
    }

    public void setCreateBlockPeriod(AdminCreateBlockPeriodRequest createBlockPeriod) {
        this.createBlockPeriod = createBlockPeriod;
    }

    public AdminUpdateBlockPeriodRequest getUpdateBlockPeriod() {
        return updateBlockPeriod;
    }

    public void setUpdateBlockPeriod(AdminUpdateBlockPeriodRequest updateBlockPeriod) {
        this.updateBlockPeriod = updateBlockPeriod;
    }

    public AdminDeleteBlockPeriodRequest getDeleteBlockPeriod() {
        return deleteBlockPeriod;
    }

    public void setDeleteBlockPeriod(AdminDeleteBlockPeriodRequest deleteBlockPeriod) {
        this.deleteBlockPeriod = deleteBlockPeriod;
    }

    public AdminRetrieveBlockPeriodRequest getRetrieveBlockPeriod() {
        return retrieveBlockPeriod;
    }

    public void setRetrieveBlockPeriod(AdminRetrieveBlockPeriodRequest retrieveBlockPeriod) {
        this.retrieveBlockPeriod = retrieveBlockPeriod;
    }

    public AdminCreateDocumentRequest getCreateDocument() {
        return createDocument;
    }

    public void setCreateDocument(AdminCreateDocumentRequest createDocument) {
        this.createDocument = createDocument;
    }

    public AdminDeleteDocumentRequest getDeleteDocument() {
        return deleteDocument;
    }

    public void setDeleteDocument(AdminDeleteDocumentRequest deleteDocument) {
        this.deleteDocument = deleteDocument;
    }

    public AdminRetrieveDocumentRequest getRetrieveDocument() {
        return retrieveDocument;
    }

    public void setRetrieveDocument(AdminRetrieveDocumentRequest retrieveDocument) {
        this.retrieveDocument = retrieveDocument;
    }

    public AdminUpdateDocumentRequest getUpdateDocument() {
        return updateDocument;
    }

    public void setUpdateDocument(AdminUpdateDocumentRequest updateDocument) {
        this.updateDocument = updateDocument;
    }

    public AdminCreateDocumentAttributeRequest getCreateDocumentAttribute() {
        return createDocumentAttribute;
    }

    public void setCreateDocumentAttribute(AdminCreateDocumentAttributeRequest createDocumentAttribute) {
        this.createDocumentAttribute = createDocumentAttribute;
    }

    public AdminDeleteDocumentAttributeRequest getDeleteDocumentAttribute() {
        return deleteDocumentAttribute;
    }

    public void setDeleteDocumentAttribute(AdminDeleteDocumentAttributeRequest deleteDocumentAttribute) {
        this.deleteDocumentAttribute = deleteDocumentAttribute;
    }

    public AdminGeneratePvRequest getGeneratePv() {
        return generatePv;
    }

    public void setGeneratePv(AdminGeneratePvRequest generatePv) {
        this.generatePv = generatePv;
    }

    public AdminUpdatePasswordRequest getUpdateAdminPassword() {
        return updateAdminPassword;
    }

    public void setUpdateAdminPassword(AdminUpdatePasswordRequest updateAdminPassword) {
        this.updateAdminPassword = updateAdminPassword;
    }

    public AdminForceSmsNotificationStatusRequest getForceSmsNotificationStatus() {
        return forceSmsNotificationStatus;
    }

    public void setForceSmsNotificationStatus(AdminForceSmsNotificationStatusRequest forceSmsNotificationStatus) {
        this.forceSmsNotificationStatus = forceSmsNotificationStatus;
    }

    public AdminGenerateMailingListRequest getGenerateMailingList() {
        return generateMailingList;
    }

    public void setGenerateMailingList(AdminGenerateMailingListRequest generateMailingList) {
        this.generateMailingList = generateMailingList;
    }

    public AdminUpdateMailingListRequest getUpdateMailingList() {
        return updateMailingList;
    }

    public void setUpdateMailingList(AdminUpdateMailingListRequest updateMailingList) {
        this.updateMailingList = updateMailingList;
    }

    public AdminAssignVoucherRequest getAssignVoucher() {
        return assignVoucher;
    }

    public void setAssignVoucher(AdminAssignVoucherRequest assignVoucher) {
        this.assignVoucher = assignVoucher;
    }

    public AdminRemoveTransactionEventRequest getRemoveTransactionEvent() {
        return removeTransactionEvent;
    }

    public void setRemoveTransactionEvent(AdminRemoveTransactionEventRequest removeTransactionEvent) {
        this.removeTransactionEvent = removeTransactionEvent;
    }

    public CreateEmailDomainRequest getCreateEmailDomain() {
        return createEmailDomain;
    }

    public void setCreateEmailDomain(CreateEmailDomainRequest createEmailDomain) {
        this.createEmailDomain = createEmailDomain;
    }

    public GetEmailDomainRequest getGetEmailDomain() {
        return getEmailDomain;
    }

    public void setGetEmailDomain(GetEmailDomainRequest getEmailDomain) {
        this.getEmailDomain = getEmailDomain;
    }

    public RemoveEmailDomainRequest getRemoveEmailDomain() {
        return removeEmailDomain;
    }

    public void setRemoveEmailDomain(RemoveEmailDomainRequest removeEmailDomain) {
        this.removeEmailDomain = removeEmailDomain;
    }

    public EditEmailDomainRequest getEditEmailDomain() {
        return editEmailDomain;
    }

    public void setEditEmailDomain(EditEmailDomainRequest editEmailDomain) {
        this.editEmailDomain = editEmailDomain;
    }

    public InsertUserInPromoOnHoldRequest getInsertUserInPromoOnHold() {
        return insertUserInPromoOnHold;
    }

    public void setInsertUserInPromoOnHold(InsertUserInPromoOnHoldRequest insertUserInPromoOnHold) {
        this.insertUserInPromoOnHold = insertUserInPromoOnHold;
    }

    public AdminUpdateVoucherDataRequest getUpdateVoucherData() {
        return updateVoucherData;
    }

    public void setUpdateVoucherData(AdminUpdateVoucherDataRequest updateVoucherData) {
        this.updateVoucherData = updateVoucherData;
    }

    public AdminDeleteVoucherRequest getDeleteVoucher() {
        return deleteVoucher;
    }

    public void setDeleteVoucher(AdminDeleteVoucherRequest deleteVoucher) {
        this.deleteVoucher = deleteVoucher;
    }

    public AdminCreateProvinceRequest getCreateProvince() {
        return createProvince;
    }

    public void setCreateProvince(AdminCreateProvinceRequest createProvince) {
        this.createProvince = createProvince;
    }

    public AdminRemoveMobilePhoneRequest getRemoveMobilePhone() {
        return removeMobilePhone;
    }

    public void setRemoveMobilePhone(AdminRemoveMobilePhoneRequest removeMobilePhone) {
        this.removeMobilePhone = removeMobilePhone;
    }

    public AdminPropagationUserDataRequest getPropagationUserData() {
        return propagationUserData;
    }

    public void setPropagationUserData(AdminPropagationUserDataRequest propagationUserData) {
        this.propagationUserData = propagationUserData;
    }

    public AdminCheckLegacyPasswordRequest getCheckLegacyPassword() {
        return checkLegacyPassword;
    }

    public void setCheckLegacyPassword(AdminCheckLegacyPasswordRequest checkLegacyPassword) {
        this.checkLegacyPassword = checkLegacyPassword;
    }

    public AdminGeneralCreateRequest getAdminGeneralCreate() {
        return adminGeneralCreate;
    }

    public void setAdminGeneralCreate(AdminGeneralCreateRequest adminGeneralCreate) {
        this.adminGeneralCreate = adminGeneralCreate;
    }

    public AdminGeneralUpdateRequest getAdminGeneralUpdate() {
        return adminGeneralUpdate;
    }

    public void setAdminGeneralUpdate(AdminGeneralUpdateRequest adminGeneralUpdate) {
        this.adminGeneralUpdate = adminGeneralUpdate;
    }

    public UpdatePrePaidConsumeVoucherRequest getUpdatePrePaidConsumeVoucher() {
        return updatePrePaidConsumeVoucher;
    }

    public void setUpdatePrePaidConsumeVoucher(UpdatePrePaidConsumeVoucherRequest updatePrePaidConsumeVoucher) {
        this.updatePrePaidConsumeVoucher = updatePrePaidConsumeVoucher;
    }

    public AddPrePaidConsumeVoucherDetailRequest getAddPrePaidConsumeVoucherDetail() {
        return addPrePaidConsumeVoucherDetail;
    }

    public void setAddPrePaidConsumeVoucherDetail(AddPrePaidConsumeVoucherDetailRequest addPrePaidConsumeVoucherDetail) {
        this.addPrePaidConsumeVoucherDetail = addPrePaidConsumeVoucherDetail;
    }

    public AdminGeneralRemoveRequest getAdminGeneralRemove() {
        return adminGeneralRemove;
    }

    public void setAdminGeneralRemove(AdminGeneralRemoveRequest adminGeneralRemove) {
        this.adminGeneralRemove = adminGeneralRemove;
    }

    public AdminCreateAdminRoleRequest getCreateAdminRole() {
        return createAdminRole;
    }

    public void setCreateAdminRole(AdminCreateAdminRoleRequest createAdminRole) {
        this.createAdminRole = createAdminRole;
    }

    public AdminDeleteAdminRoleRequest getDeleteAdminRole() {
        return deleteAdminRole;
    }

    public void setDeleteAdminRole(AdminDeleteAdminRoleRequest deleteAdminRole) {
        this.deleteAdminRole = deleteAdminRole;
    }

    public AdminAddRoleToAdminRequest getAddRoleToAdmin() {
        return addRoleToAdmin;
    }

    public void setAddRoleToAdmin(AdminAddRoleToAdminRequest addRoleToAdmin) {
        this.addRoleToAdmin = addRoleToAdmin;
    }

    public AdminRetrieveAdminRoleRequest getRetrieveAdminRole() {
        return retrieveAdminRole;
    }

    public void setRetrieveAdminRole(AdminRetrieveAdminRoleRequest retrieveAdminRole) {
        this.retrieveAdminRole = retrieveAdminRole;
    }

    public AdminRemoveRoleToAdminRequest getRemoveRoleToAdmin() {
        return removeRoleToAdmin;
    }

    public void setRemoveRoleToAdmin(AdminRemoveRoleToAdminRequest removeRoleToAdmin) {
        this.removeRoleToAdmin = removeRoleToAdmin;
    }

    public AdminPushNotificationSendRequest getAdminPushNotificationSend() {
        return adminPushNotificationSend;
    }

    public void setAdminPushNotificationSend(AdminPushNotificationSendRequest adminPushNotificationSend) {
        this.adminPushNotificationSend = adminPushNotificationSend;
    }

    public AdminRetrieveRequest getRetrieveAdminUsers() {
        return retrieveAdminUsers;
    }

    public void setRetrieveAdminUsers(AdminRetrieveRequest retrieveAdminUsers) {
        this.retrieveAdminUsers = retrieveAdminUsers;
    }

    public AdminUpdateRequest getUpdateAdminUser() {
        return updateAdminUser;
    }

    public void setUpdateAdminUser(AdminUpdateRequest updateAdminUser) {
        this.updateAdminUser = updateAdminUser;
    }

    public AdminDeleteRequest getDeleteAdminUser() {
        return deleteAdminUser;
    }

    public void setDeleteAdminUser(AdminDeleteRequest deleteAdminUser) {
        this.deleteAdminUser = deleteAdminUser;
    }

    public AdminCreateRefuelingUserRequest getCreateRefuelingUser() {
        return createRefuelingUser;
    }

    public void setCreateRefuelingUser(AdminCreateRefuelingUserRequest createRefuelingUser) {
        this.createRefuelingUser = createRefuelingUser;
    }

    public AdminInsertPaymentMethodRefuelingUserRequest getInsertPaymentMethodRefuelingUser() {
        return insertPaymentMethodRefuelingUser;
    }

    public void setInsertPaymentMethodRefuelingUser(AdminInsertPaymentMethodRefuelingUserRequest insertPaymentMethodRefuelingUser) {
        this.insertPaymentMethodRefuelingUser = insertPaymentMethodRefuelingUser;
    }

    public AdminUpdatePasswordRefuelingUserRequest getUpdatePasswordRefuelingUser() {
        return updatePasswordRefuelingUser;
    }

    public void setUpdatePasswordRefuelingUser(AdminUpdatePasswordRefuelingUserRequest updatePasswordRefuelingUser) {
        this.updatePasswordRefuelingUser = updatePasswordRefuelingUser;
    }

    public AdminRetrievePaymentDataRefuelingUserRequest getRetrievePaymentDataRefuelingUser() {
        return retrievePaymentDataRefuelingUser;
    }

    public void setRetrievePaymentDataRefuelingUser(AdminRetrievePaymentDataRefuelingUserRequest retrievePaymentDataRefuelingUser) {
        this.retrievePaymentDataRefuelingUser = retrievePaymentDataRefuelingUser;
    }

    public AdminRemovePaymentMethodRefuelingUserRequest getRemovePaymentMethodRefuelingUser() {
        return removePaymentMethodRefuelingUser;
    }

    public void setRemovePaymentMethodRefuelingUser(AdminRemovePaymentMethodRefuelingUserRequest removePaymentMethodRefuelingUser) {
        this.removePaymentMethodRefuelingUser = removePaymentMethodRefuelingUser;
    }

    public RetrieveLandingPagesRequest getRetrieveLandingPages() {
        return retrieveLandingPages;
    }

    public void setRetrieveLandingPages(RetrieveLandingPagesRequest retrieveLandingPages) {
        this.retrieveLandingPages = retrieveLandingPages;
    }

    public AdminGeneralMassiveRemoveRequest getAdminGeneralMassiveRemove() {
        return adminGeneralMassiveRemove;
    }

    public void setAdminGeneralMassiveRemove(AdminGeneralMassiveRemoveRequest adminGeneralMassiveRemove) {
        this.adminGeneralMassiveRemove = adminGeneralMassiveRemove;
    }

    public PollingIntervalRequest getAdminRetrievePollingInterval() {
        return adminRetrievePollingInterval;
    }

    public void setAdminRetrievePollingInterval(PollingIntervalRequest adminRetrievePollingInterval) {
        this.adminRetrievePollingInterval = adminRetrievePollingInterval;
    }

    public AdminUpdateCitiesRequest getAdminUpdateCities() {
        return adminUpdateCities;
    }

    public void setAdminUpdateCities(AdminUpdateCitiesRequest adminUpdateCities) {
        this.adminUpdateCities = adminUpdateCities;
    }

    public ReconciliationUserRequest getReconciliationUser() {
        return reconciliationUser;
    }

    public void setReconciliationUser(ReconciliationUserRequest reconciliationUser) {
        this.reconciliationUser = reconciliationUser;
    }

    public DeleteUserNotVerifiedRequest getDeleteUserNotVerified() {
        return deleteUserNotVerified;
    }

    public void setDeleteUserNotVerified(DeleteUserNotVerifiedRequest deleteUserNotVerified) {
        this.deleteUserNotVerified = deleteUserNotVerified;
    }

    public AdminUpdatePvActivationFlagRequest getAdminUpdatePvActivationFlag() {
        return adminUpdatePvActivationFlag;
    }

    public void setAdminUpdatePvActivationFlag(AdminUpdatePvActivationFlagRequest adminUpdatePvActivationFlag) {
        this.adminUpdatePvActivationFlag = adminUpdatePvActivationFlag;
    }

    public AdminRemoveTicketRequest getAdminRemoveTicket() {
        return adminRemoveTicket;
    }

    public void setAdminRemoveTicket(AdminRemoveTicketRequest adminRemoveTicket) {
        this.adminRemoveTicket = adminRemoveTicket;
    }

    public AdminDwhGetPartnerListRequest getAdminDwhGetPartnerList() {
        return adminDwhGetPartnerList;
    }

    public void setAdminDwhGetPartnerList(AdminDwhGetPartnerListRequest adminDwhGetPartnerList) {
        this.adminDwhGetPartnerList = adminDwhGetPartnerList;
    }

    public AdminDwhGetCategoryEarnListRequest getAdminDwhGetCategoryEarnList() {
        return adminDwhGetCategoryEarnList;
    }

    public void setAdminDwhGetCategoryEarnList(AdminDwhGetCategoryEarnListRequest adminDwhGetCategoryEarnList) {
        this.adminDwhGetCategoryEarnList = adminDwhGetCategoryEarnList;
    }

    public AdminDwhGetCatalogRequest getAdminDwhGetCatalog() {
        return adminDwhGetCatalog;
    }

    public void setAdminDwhGetCatalog(AdminDwhGetCatalogRequest adminDwhGetCatalog) {
        this.adminDwhGetCatalog = adminDwhGetCatalog;
    }

    public AdminDwhGetBrandListRequest getAdminDwhGetBrandList() {
        return adminDwhGetBrandList;
    }

    public void setAdminDwhGetBrandList(AdminDwhGetBrandListRequest adminDwhGetBrandList) {
        this.adminDwhGetBrandList = adminDwhGetBrandList;
    }

    public AdminDwhGetCategoryBurnListRequest getAdminDwhGetCategoryBurnList() {
        return adminDwhGetCategoryBurnList;
    }

    public void setAdminDwhGetCategoryBurnList(AdminDwhGetCategoryBurnListRequest adminDwhGetCategoryBurnList) {
        this.adminDwhGetCategoryBurnList = adminDwhGetCategoryBurnList;
    }

    public AdminGetCitiesRequest getAdminGetCities() {
        return adminGetCities;
    }

    public void setAdminGetCities(AdminGetCitiesRequest adminGetCities) {
        this.adminGetCities = adminGetCities;
    }

    public AdminGetParkingZonesByCityRequest getAdminGetParkingZonesByCity() {
        return adminGetParkingZonesByCity;
    }

    public void setAdminGetParkingZonesByCity(AdminGetParkingZonesByCityRequest adminGetParkingZonesByCity) {
        this.adminGetParkingZonesByCity = adminGetParkingZonesByCity;
    }

    public AdminRetrieveParkingZonesRequest getAdminRetrieveParkingZones() {
        return adminRetrieveParkingZones;
    }

    public void setAdminRetrieveParkingZones(AdminRetrieveParkingZonesRequest adminRetrieveParkingZones) {
        this.adminRetrieveParkingZones = adminRetrieveParkingZones;
    }

    public AdminEstimateParkingPriceRequest getAdminEstimateParkingPrice() {
        return adminEstimateParkingPrice;
    }

    public void setAdminEstimateParkingPrice(AdminEstimateParkingPriceRequest adminEstimateParkingPrice) {
        this.adminEstimateParkingPrice = adminEstimateParkingPrice;
    }

    public AdminStartParkingRequest getAdminStartParking() {
        return adminStartParking;
    }

    public void setAdminStartParking(AdminStartParkingRequest adminStartParking) {
        this.adminStartParking = adminStartParking;
    }

    public AdminEstimateExtendedParkingPriceRequest getAdminEstimateExtendedParkingPrice() {
        return adminEstimateExtendedParkingPrice;
    }

    public void setAdminEstimateExtendedParkingPrice(AdminEstimateExtendedParkingPriceRequest adminEstimateExtendedParkingPrice) {
        this.adminEstimateExtendedParkingPrice = adminEstimateExtendedParkingPrice;
    }

    public AdminExtendParkingRequest getAdminExtendParking() {
        return adminExtendParking;
    }

    public void setAdminExtendParking(AdminExtendParkingRequest adminExtendParking) {
        this.adminExtendParking = adminExtendParking;
    }

    public AdminEstimateEndParkingPriceRequest getAdminEstimateEndParkingPrice() {
        return adminEstimateEndParkingPrice;
    }

    public void setAdminEstimateEndParkingPrice(AdminEstimateEndParkingPriceRequest adminEstimateEndParkingPrice) {
        this.adminEstimateEndParkingPrice = adminEstimateEndParkingPrice;
    }

    public AdminEndParkingRequest getAdminEndParking() {
        return adminEndParking;
    }

    public void setAdminEndParking(AdminEndParkingRequest adminEndParking) {
        this.adminEndParking = adminEndParking;
    }
    
    public AdminSystemCheckRequest getAdminSystemCheck() {
        return adminSystemCheck;
    }
    
    public void setAdminSystemCheck(AdminSystemCheckRequest adminSystemCheck) {
        this.adminSystemCheck = adminSystemCheck;
    }

    public TestGetMcCardStatusRequest getTestGetMcCardStatus() {
        return testGetMcCardStatus;
    }

    public void setTestGetMcCardStatus(TestGetMcCardStatusRequest testGetMcCardStatus) {
        this.testGetMcCardStatus = testGetMcCardStatus;
    }

    public TestExecutePaymentRequest getTestExecutePayment() {
        return testExecutePayment;
    }

    public void setTestExecutePayment(TestExecutePaymentRequest testExecutePayment) {
        this.testExecutePayment = testExecutePayment;
    }

    public TestExecuteAuthorizationRequest getTestExecuteAuthorization() {
        return testExecuteAuthorization;
    }

    public void setTestExecuteAuthorization(TestExecuteAuthorizationRequest testExecuteAuthorization) {
        this.testExecuteAuthorization = testExecuteAuthorization;
    }

	public TestExecuteCaptureRequest getTestExecuteCapture() {
		return testExecuteCapture;
	}

	public void setTestExecuteCapture(TestExecuteCaptureRequest testExecuteCapture) {
		this.testExecuteCapture = testExecuteCapture;
	}

	public TestExecuteReversalRequest getTestExecuteReversal() {
		return testExecuteReversal;
	}

	public void setTestExecuteReversal(
			TestExecuteReversalRequest testExecuteReversal) {
		this.testExecuteReversal = testExecuteReversal;
	}

	public TestGetPaymentStatusRequest getTestGetPaymentStatus() {
		return testGetPaymentStatus;
	}

	public void setTestGetPaymentStatus(
			TestGetPaymentStatusRequest testGetPaymentStatus) {
		this.testGetPaymentStatus = testGetPaymentStatus;
	}

	public TestAuthorizationPlusRequest getTestAuthorizationPlus() {
		return testAuthorizationPlus;
	}

	public void setTestAuthorizationPlus(
			TestAuthorizationPlusRequest testAuthorizationPlus) {
		this.testAuthorizationPlus = testAuthorizationPlus;
	}

    public TestGetPaymentTransactionDetailsRequest getTestGetPaymentTransactionDetails() {
        return testGetPaymentTransactionDetails;
    }

    public void setTestGetPaymentTransactionDetails(TestGetPaymentTransactionDetailsRequest testGetPaymentTransactionDetails) {
        this.testGetPaymentTransactionDetails = testGetPaymentTransactionDetails;
    }

	public AdminContactKeyBulkRequest getAdminContactKeyBulk() {
		return adminContactKeyBulk;
	}

	public void setAdminContactKeyBulk(
			AdminContactKeyBulkRequest adminContactKeyBulk) {
		this.adminContactKeyBulk = adminContactKeyBulk;
	}

	public TestCrmSfNotifyEventRequest getTestCrmSfNotifyEvent() {
		return testCrmSfNotifyEvent;
	}

	public void setTestCrmSfNotifyEvent(
			TestCrmSfNotifyEventRequest testCrmSfNotifyEvent) {
		this.testCrmSfNotifyEvent = testCrmSfNotifyEvent;
	}

	public TestCrmSfGetMissionRequest getTestCrmSfGetMission() {
		return testCrmSfGetMission;
	}

	public void setTestCrmSfGetMission(
			TestCrmSfGetMissionRequest testCrmSfGetMission) {
		this.testCrmSfGetMission = testCrmSfGetMission;
	}

    public TestMcEnjoyCardAuthorizeRequest getTestMcEnjoyCardAuthorize() {
        return testMcEnjoyCardAuthorize;
    }

    public void setTestMcEnjoyCardAuthorize(TestMcEnjoyCardAuthorizeRequest testMcEnjoyCardAuthorize) {
        this.testMcEnjoyCardAuthorize = testMcEnjoyCardAuthorize;
    }

    public TestDeleteMcCardRefuelingRequest getTestDeleteMcCardRefueling() {
        return testDeleteMcCardRefueling;
    }

    public void setTestDeleteMcCardRefueling(TestDeleteMcCardRefuelingRequest testDeleteMcCardRefueling) {
        this.testDeleteMcCardRefueling = testDeleteMcCardRefueling;
    }

    public AdminInsertMulticardRefuelingUserRequest getInsertMulticardRefuelingUser() {
        return insertMulticardRefuelingUser;
    }

    public void setInsertMulticardRefuelingUser(AdminInsertMulticardRefuelingUserRequest insertMulticardRefuelingUser) {
        this.insertMulticardRefuelingUser = insertMulticardRefuelingUser;
    }
	
	public CountPendingTransactionsRequest getCountPendingTransactions() {
        return countPendingTransactions;
    }

    public void setCountPendingTransactions(CountPendingTransactionsRequest countPendingTransactions) {
        this.countPendingTransactions = countPendingTransactions;
    }

    public UpdatePaymentMethodStatusRequest getUpdatePaymentMethodStatus() {
        return updatePaymentMethodStatus;
    }

    public void setUpdatePaymentMethodStatus(UpdatePaymentMethodStatusRequest updatePaymentMethodStatus) {
        this.updatePaymentMethodStatus = updatePaymentMethodStatus;
    }

    public TestTransactionReconciliationRequest getTestTransactionReconciliation() {
        return testTransactionReconciliation;
    }

    public void setTestTransactionReconciliation(TestTransactionReconciliationRequest testTransactionReconciliation) {
        this.testTransactionReconciliation = testTransactionReconciliation;
    }

    public TestGetTransactionStatusRequest getTestGetTransactionStatus() {
        return testGetTransactionStatus;
    }

    public void setTestGetTransactionStatus(TestGetTransactionStatusRequest testGetTransactionStatus) {
        this.testGetTransactionStatus = testGetTransactionStatus;
    }
    
}
