package com.techedge.mp.frontendbo.adapter.entities.admin.testtransactionreconciliation;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestTransactionReconciliationRequestBody implements Validable {

    private String transactionID;

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.transactionID == null || this.transactionID.isEmpty()) {
            status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS);
            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);
        return status;
    }

}
