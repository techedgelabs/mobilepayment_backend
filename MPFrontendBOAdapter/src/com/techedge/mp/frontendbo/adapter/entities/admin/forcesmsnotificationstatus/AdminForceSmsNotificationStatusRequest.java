package com.techedge.mp.frontendbo.adapter.entities.admin.forcesmsnotificationstatus;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminForceSmsNotificationStatusRequest extends AbstractBORequest implements Validable {

    private Status                                     status = new Status();

    private Credential                                 credential;
    private AdminForceSmsNotificationStatusBodyRequest body;

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public AdminForceSmsNotificationStatusBodyRequest getBody() {
        return body;
    }

    public void setBody(AdminForceSmsNotificationStatusBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("FORCE-SMS-NOTIFICATION-STATUS", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }
        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_SMS_NOTIFICATION_STATUS_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        String adminForceSmsNotificationStatusResult = getAdminServiceRemote().adminForceSmsNotificationStatus(this.getCredential().getTicketID(),
                this.getCredential().getRequestID(), this.getBody().getCorrelationId(), this.getBody().getStatus());

        AdminForceSmsNotificationStatusResponse deleteDocumentAttributeResponse = new AdminForceSmsNotificationStatusResponse();
        status.setStatusCode(adminForceSmsNotificationStatusResult);
        status.setStatusMessage(prop.getProperty(adminForceSmsNotificationStatusResult));
        deleteDocumentAttributeResponse.setStatus(status);

        return deleteDocumentAttributeResponse;
    }

}
