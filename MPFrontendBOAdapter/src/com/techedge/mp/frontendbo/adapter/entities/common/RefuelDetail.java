package com.techedge.mp.frontendbo.adapter.entities.common;


public class RefuelDetail {

        private Double amount;
        private Double fuelQuantity;
        private String fuelType;
        private String productDescription;
        private String productId;
        private String timestampEndRefuel;
        private String timestampStartRefuel;

        public RefuelDetail() {}

        public java.lang.Double getAmount() {
            return amount;
        }

        public void setAmount(java.lang.Double amount) {
            this.amount = amount;
        }

        public java.lang.Double getFuelQuantity() {
            return fuelQuantity;
        }

        public void setFuelQuantity(java.lang.Double fuelQuantity) {
            this.fuelQuantity = fuelQuantity;
        }

        public java.lang.String getFuelType() {
            return fuelType;
        }

        public void setFuelType(java.lang.String fuelType) {
            this.fuelType = fuelType;
        }

        public java.lang.String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(java.lang.String productDescription) {
            this.productDescription = productDescription;
        }

        public String getProductId() {
            return productId;
        }

        public void setProductId(String productId) {
            this.productId = productId;
        }

        public java.lang.String getTimestampEndRefuel() {
            return timestampEndRefuel;
        }

        public void setTimestampEndRefuel(java.lang.String timestampEndRefuel) {
            this.timestampEndRefuel = timestampEndRefuel;
        }

        public java.lang.String getTimestampStartRefuel() {
            return timestampStartRefuel;
        }

        public void setTimestampStartRefuel(java.lang.String timestampStartRefuel) {
            this.timestampStartRefuel = timestampStartRefuel;
        }

    }

