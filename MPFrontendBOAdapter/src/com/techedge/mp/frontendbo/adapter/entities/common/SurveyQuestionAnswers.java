package com.techedge.mp.frontendbo.adapter.entities.common;

import java.util.ArrayList;

public class SurveyQuestionAnswers {

    private String   code;
    private ArrayList<Answer> answers = new ArrayList<Answer>(0);

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public void addAnswer(String value) {
        this.answers.add(new Answer(value));
    }
    
    public ArrayList<Answer> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<Answer> answers) {
        this.answers = answers;
    }

    private class Answer {
        
        private String value;        

        public Answer(String value) {
            this.value = value;
        }
        
        public String getValue() {
            return value;
        }
    }

}
