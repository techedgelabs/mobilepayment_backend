package com.techedge.mp.frontendbo.adapter.entities.admin.retrievedocument;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminRetrieveDocumentBodyRequest implements Validable {

    private String key;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @Override
    public Status check() {
        Status status = new Status();

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_RETRIEVE_SUCCESS);

        return status;
    }
}
