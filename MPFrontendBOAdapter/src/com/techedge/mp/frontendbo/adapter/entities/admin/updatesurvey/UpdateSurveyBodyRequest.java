package com.techedge.mp.frontendbo.adapter.entities.admin.updatesurvey;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UpdateSurveyBodyRequest implements Validable {

    private String  code;
    private String  description;
    private String  note;
    private Long    startDate;
    private Long    endDate;
    private Integer status;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public UpdateSurveyBodyRequest() {}

    @Override
    public Status check() {

        Status status = new Status();

        if (this.code == null || this.code.isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }

        if (this.status == null) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }

        if (this.startDate == null) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }

        if (this.endDate == null) {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
            return status;

        }

        if (this.startDate > this.endDate) {

            status.setStatusCode(StatusCode.ADMIN_SURVEY_UPDATE_START_DATE_ERROR);
            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_SURVEY_UPDATE_SUCCESS);

        return status;
    }

}
