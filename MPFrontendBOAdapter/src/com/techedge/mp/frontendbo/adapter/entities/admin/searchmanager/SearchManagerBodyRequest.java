package com.techedge.mp.frontendbo.adapter.entities.admin.searchmanager;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class SearchManagerBodyRequest implements Validable {

    Long   id;
    String username;
    String email;
    Integer maxResults;

    public SearchManagerBodyRequest() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getMaxResults() {
        return maxResults;
    }

    public void setMaxResults(Integer maxResults) {
        this.maxResults = maxResults;
    }

    @Override
    public Status check() {

        Status status = new Status();

        /*if (this.id == null && this.username == null && this.email == null) {

            status.setStatusCode(StatusCode.ADMIN_SEARCH_MANAGER_INVALID_PARAMETERS);

            return status;
        }*/
        
        status.setStatusCode(StatusCode.ADMIN_SEARCH_MANAGER_SUCCESS);
        return status;
    }

}
