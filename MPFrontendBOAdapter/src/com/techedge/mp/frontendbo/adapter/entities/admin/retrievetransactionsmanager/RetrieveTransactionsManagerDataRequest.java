package com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactionsmanager;

import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.frontendbo.adapter.entities.common.ManagerData;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class RetrieveTransactionsManagerDataRequest extends ManagerData implements Validable {

    private String password;

    public RetrieveTransactionsManagerDataRequest() {}

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Manager getManager()
    {
        Manager manager = super.getManager();
        manager.setPassword(this.password);
        
        return manager;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (getUsername() == null || getUsername().equals("")) {

            status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_INVALID_PARAMETERS);

            return status;
        }

        if (this.password == null || this.password.equals("")) {

            status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_INVALID_PARAMETERS);

            return status;
        }

        if (getUsername().equals(this.password)) {

            status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_USERNAME_PASSWORD_EQUALS);

            return status;
        }

        if (getEmail() == null || getEmail().equals("")) {

            status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_INVALID_PARAMETERS);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS);

        return status;

    }

}
