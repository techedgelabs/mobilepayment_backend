package com.techedge.mp.frontendbo.adapter.entities.admin.unarchive;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.ArchiveTransactionInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class UnArchiveResponse extends BaseResponse {

    private List<ArchiveTransactionInfo> listArchiveTransactionInfo = new ArrayList<ArchiveTransactionInfo>(0);

    public List<ArchiveTransactionInfo> getListArchiveTransactionInfo() {
        return listArchiveTransactionInfo;
    }

    public void setListArchiveTransactionInfo(List<ArchiveTransactionInfo> listArchiveTransactionInfo) {
        this.listArchiveTransactionInfo = listArchiveTransactionInfo;
    }

}