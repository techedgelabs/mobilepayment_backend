package com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusercategory;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class RetrieveUserCategoryResponse extends BaseResponse {

    private List<RetrieveUserCategoryBodyResponse> userCategory = new ArrayList<RetrieveUserCategoryBodyResponse>(0);

    public List<RetrieveUserCategoryBodyResponse> getUserCategory() {
        return userCategory;
    }

    public void setUserCategory(List<RetrieveUserCategoryBodyResponse> userCategory) {
        this.userCategory = userCategory;
    }

}
