package com.techedge.mp.frontendbo.adapter.entities.admin.authentication;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AuthenticationAdminResponseSuccess extends BaseResponse {

	private AuthenticationAdminResponseBody body;

	public AuthenticationAdminResponseBody getBody() {
		return body;
	}

	public void setBody(AuthenticationAdminResponseBody body) {
		this.body = body;
	}
}
