package com.techedge.mp.frontendbo.adapter.entities.admin.testexecutereversal;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteReversalResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class TestExecuteReversalRequest extends AbstractBORequest implements Validable {

    private Credential                    credential;
    private TestExecuteReversalBodyRequest body;

    public TestExecuteReversalRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public TestExecuteReversalBodyRequest getBody() {
        return body;
    }

    public void setBody(TestExecuteReversalBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("EXECUTE-REVERSAL", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_TEST_EXECUTE_REVERSAL_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        TestExecuteReversalResponse testExecuteReversalResponse = new TestExecuteReversalResponse();

        String authCheckResponse = getAdminServiceRemote().adminCheckAdminAuthorization(this.getCredential().getTicketID(), "testExecuteReversalResponse");

        if (!authCheckResponse.equals(ResponseHelper.ADMIN_CHECK_ADMIN_AUTHORIZATION_SUCCESS)) {

            testExecuteReversalResponse.getStatus().setStatusCode("-1");
        }
        else {
            ExecuteReversalResult executeReversalResult = new ExecuteReversalResult();
            
            try {
                PartnerType partnerType = getEnumerationValue(PartnerType.class, this.getBody().getPartnerType());

                executeReversalResult = getPaymentServiceRemote().executeReversal(this.getBody().getMcCardDpan(), this.getBody().getMessageReasonCode(), this.getBody().getRetrievalRefNumber(), this.getBody().getAuthCode(),this.getBody().getShopCode(), 
                		this.getBody().getCurrencyCode(), this.getBody().getOperationId(), this.getBody().getAmount(), partnerType, this.getBody().getRequestTimestamp());
            }
            catch (Exception ex) {
                executeReversalResult = null;
            }
            
            
            testExecuteReversalResponse.setStatusCode("OK");
            testExecuteReversalResponse.setResult(executeReversalResult);
        }

        return testExecuteReversalResponse;
    }

    private <T extends Enum<T>> T getEnumerationValue(Class<T> enumeration, String name) {

        for (T enumValue : enumeration.getEnumConstants()) {
            if (enumValue.name().equalsIgnoreCase(name)) {
                return enumValue;
            }
        }

        throw new IllegalArgumentException(String.format("There is no value with name '%s' in Enum %s", name, enumeration.getName()));
    }
}
