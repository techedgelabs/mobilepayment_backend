package com.techedge.mp.frontendbo.adapter.entities.admin.unarchive;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class UnArchiveRequestBody implements Validable {

    private String transactionID;

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.transactionID != null) {
            if (this.transactionID.length() > 50) {
                status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
                return status;
            }
        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS);

        return status;
    }
}
