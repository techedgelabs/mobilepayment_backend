package com.techedge.mp.frontendbo.adapter.entities.admin.updatesurvey;

import java.util.Date;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class UpdateSurveyRequest extends AbstractBORequest implements Validable {

    private Status                  status = new Status();

    private Credential              credential;
    private UpdateSurveyBodyRequest body;

    public UpdateSurveyRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public UpdateSurveyBodyRequest getBody() {
        return body;
    }

    public void setBody(UpdateSurveyBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-SURVEY", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_SURVEY_UPDATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        UpdateSurveyBodyRequest updateSurveyBodyRequest = this.getBody();
        UpdateSurveyResponse updateSurveyResponse = new UpdateSurveyResponse();

        Date startDate = new Date(updateSurveyBodyRequest.getStartDate());
        Date endDate = new Date(updateSurveyBodyRequest.getEndDate());

        String response = getAdminServiceRemote().adminSurveyUpdate(this.getCredential().getTicketID(), this.getCredential().getRequestID(), updateSurveyBodyRequest.getCode(),
                updateSurveyBodyRequest.getDescription(), updateSurveyBodyRequest.getNote(), updateSurveyBodyRequest.getStatus(), startDate, endDate);

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));

        updateSurveyResponse.setStatus(status);

        return updateSurveyResponse;
    }

}
