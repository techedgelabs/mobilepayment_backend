package com.techedge.mp.frontendbo.adapter.entities.admin.archive;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.AdminArchiveResponse;
import com.techedge.mp.core.business.interfaces.ArchiveTransactionResult;
import com.techedge.mp.frontendbo.adapter.entities.common.ArchiveTransactionInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class ArchiveRequest extends AbstractBORequest implements Validable {

    private Status             status = new Status();

    private Credential         credential;
    private ArchiveRequestBody body;

    public ArchiveRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public ArchiveRequestBody getBody() {
        return body;
    }

    public void setBody(ArchiveRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("UPDATE-USER", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_UPDATE_PARAM_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {
        ArchiveResponse archiveResponse = new ArchiveResponse();

        if (this.getBody().getTransactionID() != null && (this.getBody().getTransactionID().equals("") || this.getBody().getTransactionID().isEmpty())) {
            this.getBody().setTransactionID(null);
        }

        AdminArchiveResponse adminArchiveResponse = getAdminServiceRemote().adminArchive(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getTransactionID());
        status.setStatusCode(adminArchiveResponse.getStatusCode());
        status.setStatusMessage(prop.getProperty(adminArchiveResponse.getStatusCode()));
        List<ArchiveTransactionInfo> listInfo = new ArrayList<ArchiveTransactionInfo>(0);

        for (ArchiveTransactionResult singleResult : adminArchiveResponse.getArchiveTransactionResultList()) {
            ArchiveTransactionInfo infoResult = new ArchiveTransactionInfo(singleResult.getId(), singleResult.getType(), singleResult.getStatusCode());
            listInfo.add(infoResult);
        }
        archiveResponse.setStatus(status);
        archiveResponse.setListArchiveTransactionInfo(listInfo);

        return archiveResponse;
    }

}
