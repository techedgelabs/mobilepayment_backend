package com.techedge.mp.frontendbo.adapter.entities.admin.create;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class CreateAdminRequestBody implements Validable {
	
	
	private CreateAdminDataRequest adminData;
	
	
	public CreateAdminRequestBody() {}

	public CreateAdminDataRequest getAdminData() {
		return adminData;
	}
	public void setAdminData(CreateAdminDataRequest adminData) {
		this.adminData = adminData;
	}


	@Override
	public Status check() {
		
		Status status = new Status();
		
		if(this.adminData != null) {
			
			status = this.adminData.check();
			
			if(!Validator.isValid(status.getStatusCode())) {
				
				return status;
				
			}
			
		} else {
			
			status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);
			
			return status;
		}
		
		status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

		return status;
	}
	
}
