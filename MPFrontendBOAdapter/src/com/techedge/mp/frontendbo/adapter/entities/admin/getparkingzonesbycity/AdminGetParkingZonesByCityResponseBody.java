package com.techedge.mp.frontendbo.adapter.entities.admin.getparkingzonesbycity;

import com.techedge.mp.parking.integration.business.interfaces.GetParkingZonesByCityResult;

public class AdminGetParkingZonesByCityResponseBody {

    private GetParkingZonesByCityResult getParkingZonesByCityResult;

    public GetParkingZonesByCityResult getGetParkingZonesByCityResult() {
        return getParkingZonesByCityResult;
    }

    public void setGetParkingZonesByCityResult(GetParkingZonesByCityResult getParkingZonesByCityResult) {
        this.getParkingZonesByCityResult = getParkingZonesByCityResult;
    }

}
