package com.techedge.mp.frontendbo.adapter.entities.admin.estimateextendparkingprice;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminEstimateExtendedParkingPriceResponse extends BaseResponse{
    
    private AdminEstimateExtendedParkingPriceResponseBody body;

    public AdminEstimateExtendedParkingPriceResponseBody getBody() {
        return body;
    }

    public void setBody(AdminEstimateExtendedParkingPriceResponseBody body) {
        this.body = body;
    }
}
