package com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters;

import java.util.List;

import com.techedge.mp.core.business.interfaces.DeleteTestersData;
import com.techedge.mp.core.business.interfaces.TesterUser;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.UserConfig;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class DeleteTestersRequest extends AbstractBORequest implements Validable {

    private Status                   status = new Status();

    private Credential               credential;
    private DeleteTestersRequestBody body;

    public DeleteTestersRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public DeleteTestersRequestBody getBody() {
        return body;
    }

    public void setBody(DeleteTestersRequestBody body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("CREATE", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        if (this.body != null) {

            status = this.body.check();

            if (!Validator.isValid(status.getStatusCode())) {

                return status;

            }

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_CREATE_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        DeleteTestersData deleteTestersData = getAdminServiceRemote().adminTestersDelete(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getCustomerUserData().getEmailPrefix(), this.getBody().getCustomerUserData().getEmailSuffix(),
                this.getBody().getCustomerUserData().getStartNumber(), this.getBody().getCustomerUserData().getUserCount()

        );

        DeleteTestersResponse deleteTestersResponse = new DeleteTestersResponse();

        List<TesterUser> testerList = deleteTestersData.getTesterList();

        if (!testerList.isEmpty()) {

            for (TesterUser testerUser : testerList) {

                UserConfig userConfig = new UserConfig();

                userConfig.setEmail(testerUser.getEmail());
                userConfig.setResponse(testerUser.getResponse());
                userConfig.setDeleted(testerUser.isDeleted());
                deleteTestersResponse.getTesters().add(userConfig);
            }
        }

        status.setStatusCode(deleteTestersData.getStatusCode());
        status.setStatusMessage(prop.getProperty(deleteTestersData.getStatusCode()));

        deleteTestersResponse.setStatus(status);

        return deleteTestersResponse;
    }

}
