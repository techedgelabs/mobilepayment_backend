package com.techedge.mp.frontendbo.adapter.entities.admin.generatepv;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class AdminGeneratePvResponse extends BaseResponse {
    
    private AdminGeneratePvBodyResponse body;

    public AdminGeneratePvBodyResponse getBody() {
        return body;
    }

    public void setBody(AdminGeneratePvBodyResponse body) {
        this.body = body;
    }

}