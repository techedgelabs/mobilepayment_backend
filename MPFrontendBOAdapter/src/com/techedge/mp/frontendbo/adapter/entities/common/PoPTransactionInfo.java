package com.techedge.mp.frontendbo.adapter.entities.common;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class PoPTransactionInfo {

    private Long                                      id;
    private String                                    mpTransactionID;
    private Long                                      userId;
    private String                                    stationId;
    private String                                    status;
    private String                                    subStatus;
    private String                                    source;
    private String                                    sourceID;
    private String                                    srcTransactionID;
    private String                                    bankTansactionID;
    private String                                    authorizationCode;
    private String                                    token;
    private String                                    paymentType;
    private Double                                    amount;
    private String                                    currency;
    private String                                    productType;
    private Set<PoPCartInfo>                          cartInfo                                  = new HashSet<PoPCartInfo>(0);
    private Set<PoPRefuelInfo>                        refuelInfo                                = new HashSet<PoPRefuelInfo>(0);
    private String                                    statusType;
    private String                                    mpTransactionStatus;
    private String                                    srcTransactionStatus;
    private Date                                      creationTimestamp;
    private Date                                      lastModifyTimestamp;
    private Set<PoPTransactionEventInfo>              postPaidTransactionEventInfo              = new HashSet<PoPTransactionEventInfo>(0);
    private Set<PoPTransactionPaymentEventInfo>       postPaidTransactionPaymentEventInfo       = new HashSet<PoPTransactionPaymentEventInfo>(0);
    private String                                    shopLogin;
    private String                                    acquirerID;
    private String                                    paymentMode;
    private Long                                      paymentMethodId;
    private String                                    paymentMethodType;
    private String                                    notificationCreated;
    private String                                    notificationPaid;
    private String                                    notificationUser;
    private Date                                      archivingDate;
    private Set<PoPTransactionConsumeVoucherInfo>     postPaidTransactionConsumeVoucherInfo     = new HashSet<PoPTransactionConsumeVoucherInfo>(0);
    private Set<PoPTransactionLoadLoyaltyCreditsInfo> postPaidTransactionLoadLoyaltyCreditsInfo = new HashSet<PoPTransactionLoadLoyaltyCreditsInfo>(0);
    private String                                    toReconcile;

    public PoPTransactionInfo() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMpTransactionID() {
        return mpTransactionID;
    }

    public void setMpTransactionID(String mpTransactionID) {
        this.mpTransactionID = mpTransactionID;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getStationId() {
        return stationId;
    }

    public void setStationId(String stationId) {
        this.stationId = stationId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubStatus() {
        return subStatus;
    }

    public void setSubStatus(String subStatus) {
        this.subStatus = subStatus;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceID() {
        return sourceID;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    public String getSrcTransactionID() {
        return srcTransactionID;
    }

    public void setSrcTransactionID(String srcTransactionID) {
        this.srcTransactionID = srcTransactionID;
    }

    public String getBankTansactionID() {
        return bankTansactionID;
    }

    public void setBankTansactionID(String bankTansactionID) {
        this.bankTansactionID = bankTansactionID;
    }

    public String getAuthorizationCode() {
        return authorizationCode;
    }

    public void setAuthorizationCode(String authorizationCode) {
        this.authorizationCode = authorizationCode;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public Set<PoPCartInfo> getCartInfo() {
        return cartInfo;
    }

    public void setCartInfo(Set<PoPCartInfo> cartInfo) {
        this.cartInfo = cartInfo;
    }

    public Set<PoPRefuelInfo> getRefuelInfo() {
        return refuelInfo;
    }

    public void setRefuelInfo(Set<PoPRefuelInfo> refuelInfo) {
        this.refuelInfo = refuelInfo;
    }

    public String getStatusType() {
        return statusType;
    }

    public void setStatusType(String statusType) {
        this.statusType = statusType;
    }

    public String getMpTransactionStatus() {
        return mpTransactionStatus;
    }

    public void setMpTransactionStatus(String mpTransactionStatus) {
        this.mpTransactionStatus = mpTransactionStatus;
    }

    public String getSrcTransactionStatus() {
        return srcTransactionStatus;
    }

    public void setSrcTransactionStatus(String srcTransactionStatus) {
        this.srcTransactionStatus = srcTransactionStatus;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public void setCreationTimestamp(Date creationTimestamp) {
        this.creationTimestamp = creationTimestamp;
    }

    public Date getLastModifyTimestamp() {
        return lastModifyTimestamp;
    }

    public void setLastModifyTimestamp(Date lastModifyTimestamp) {
        this.lastModifyTimestamp = lastModifyTimestamp;
    }

    public Set<PoPTransactionEventInfo> getPostPaidTransactionEventInfo() {
        return postPaidTransactionEventInfo;
    }

    public void setPostPaidTransactionEventInfo(Set<PoPTransactionEventInfo> postPaidTransactionEventInfo) {
        this.postPaidTransactionEventInfo = postPaidTransactionEventInfo;
    }

    public Set<PoPTransactionPaymentEventInfo> getPostPaidTransactionPaymentEventInfo() {
        return postPaidTransactionPaymentEventInfo;
    }

    public void setPostPaidTransactionPaymentEventInfo(Set<PoPTransactionPaymentEventInfo> postPaidTransactionPaymentEventInfo) {
        this.postPaidTransactionPaymentEventInfo = postPaidTransactionPaymentEventInfo;
    }

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getAcquirerID() {
        return acquirerID;
    }

    public void setAcquirerID(String acquirerID) {
        this.acquirerID = acquirerID;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Long getPaymentMethodId() {
        return paymentMethodId;
    }

    public void setPaymentMethodId(Long paymentMethodId) {
        this.paymentMethodId = paymentMethodId;
    }

    public String getPaymentMethodType() {
        return paymentMethodType;
    }

    public void setPaymentMethodType(String paymentMethodType) {
        this.paymentMethodType = paymentMethodType;
    }

    public String getNotificationCreated() {
        return notificationCreated;
    }

    public void setNotificationCreated(String notificationCreated) {
        this.notificationCreated = notificationCreated;
    }

    public String getNotificationPaid() {
        return notificationPaid;
    }

    public void setNotificationPaid(String notificationPaid) {
        this.notificationPaid = notificationPaid;
    }

    public String getNotificationUser() {
        return notificationUser;
    }

    public void setNotificationUser(String notificationUser) {
        this.notificationUser = notificationUser;
    }

    public Date getArchivingDate() {
        return archivingDate;
    }

    public void setArchivingDate(Date archivingDate) {
        this.archivingDate = archivingDate;
    }

    public Set<PoPTransactionConsumeVoucherInfo> getPostPaidTransactionConsumeVoucherInfo() {
        return postPaidTransactionConsumeVoucherInfo;
    }

    public void setPostPaidTransactionConsumeVoucherInfo(Set<PoPTransactionConsumeVoucherInfo> postPaidTransactionConsumeVoucherInfo) {
        this.postPaidTransactionConsumeVoucherInfo = postPaidTransactionConsumeVoucherInfo;
    }

    public Set<PoPTransactionLoadLoyaltyCreditsInfo> getPostPaidTransactionLoadLoyaltyCreditsInfo() {
        return postPaidTransactionLoadLoyaltyCreditsInfo;
    }

    public void setPostPaidTransactionLoadLoyaltyCreditsInfo(Set<PoPTransactionLoadLoyaltyCreditsInfo> postPaidTransactionLoadLoyaltyCreditsInfo) {
        this.postPaidTransactionLoadLoyaltyCreditsInfo = postPaidTransactionLoadLoyaltyCreditsInfo;
    }

    public String getToReconcile() {
        return toReconcile;
    }

    public void setToReconcile(String toReconcile) {
        this.toReconcile = toReconcile;
    }

}
