package com.techedge.mp.frontendbo.adapter.entities.admin.testretrievestationdetails;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;

public class TestRetrieveStationResponse extends BaseResponse{
    
    private TestRetrieveStationResponseBody body;

    public TestRetrieveStationResponseBody getBody() {
        return body;
    }

    public void setBody(TestRetrieveStationResponseBody body) {
        this.body = body;
    }
}
