package com.techedge.mp.frontendbo.adapter.entities.admin.createemaildomain;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class CreateEmailDomainBodyRequest implements Validable {

    private List<String> emails = new ArrayList<String>(0);

    public CreateEmailDomainBodyRequest() {}

    public List<String> getEmails() {
        return emails;
    }

    public void setEmails(List<String> emails) {
        this.emails = emails;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.emails == null || this.emails.isEmpty() ) {

            status.setStatusCode(StatusCode.MAIL_CREATE_FAILURE);

            return status;
        }

        status.setStatusCode(StatusCode.MAIL_CREATE_SUCCESS);

        return status;
    }
}
