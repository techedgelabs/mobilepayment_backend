package com.techedge.mp.frontendbo.adapter.entities.admin.stationsoperation;

import java.util.ArrayList;
import java.util.List;



import com.techedge.mp.core.business.interfaces.StationAdmin;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;


public class StationsOpResponse extends BaseResponse {
	
	List<StationAdmin> stationList = new ArrayList<StationAdmin>(0);

	public List<StationAdmin> getStationsList() {
		return stationList;
	}

	public void setStationsList(List<StationAdmin> stationsList) {
		this.stationList = stationsList;
	}
	
}
