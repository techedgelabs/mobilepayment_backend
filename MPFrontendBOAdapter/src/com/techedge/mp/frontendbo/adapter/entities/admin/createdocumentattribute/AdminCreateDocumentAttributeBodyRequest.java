package com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class AdminCreateDocumentAttributeBodyRequest implements Validable {

    private String  checkKey;
    private Boolean mandatory;
    private Integer position;
    private String  conditionText;
    private String  extendedConditionText;
    private String  documentKey;
    private String  subtitle;

    public String getCheckKey() {
        return checkKey;
    }

    public void setCheckKey(String checkKey) {
        this.checkKey = checkKey;
    }

    public Boolean getMandatory() {
        return mandatory;
    }

    public void setMandatory(Boolean mandatory) {
        this.mandatory = mandatory;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public String getConditionText() {
        return conditionText;
    }

    public void setConditionText(String conditionText) {
        this.conditionText = conditionText;
    }

    public String getExtendedConditionText() {
        return extendedConditionText;
    }

    public void setExtendedConditionText(String extendedConditionText) {
        this.extendedConditionText = extendedConditionText;
    }

    public String getDocumentKey() {
        return documentKey;
    }

    public void setDocumentKey(String documentKey) {
        this.documentKey = documentKey;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    @Override
    public Status check() {
        Status status = new Status();

        if (this.documentKey == null || this.documentKey.isEmpty() || this.documentKey.trim().isEmpty() || position == null || this.checkKey == null || this.checkKey.isEmpty()
                || this.checkKey.trim().isEmpty() || this.mandatory == null) {
            status.setStatusCode(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_CHECK_FAILURE);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_SUCCESS);

        return status;
    }
}
