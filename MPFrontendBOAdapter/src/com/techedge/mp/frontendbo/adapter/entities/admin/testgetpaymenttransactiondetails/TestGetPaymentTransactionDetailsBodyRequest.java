package com.techedge.mp.frontendbo.adapter.entities.admin.testgetpaymenttransactiondetails;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestGetPaymentTransactionDetailsBodyRequest implements Validable {

    private String shopLogin;
    private String shopTransactionID;
    private String bankTransactionID;
    private String acquirerId;
    private String groupAcquirer;
    private String encodedSecretKey;

    public String getShopLogin() {
        return shopLogin;
    }

    public void setShopLogin(String shopLogin) {
        this.shopLogin = shopLogin;
    }

    public String getShopTransactionID() {
        return shopTransactionID;
    }

    public void setShopTransactionID(String shopTransactionID) {
        this.shopTransactionID = shopTransactionID;
    }

    public String getBankTransactionID() {
        return bankTransactionID;
    }

    public void setBankTransactionID(String bankTransactionID) {
        this.bankTransactionID = bankTransactionID;
    }

    public String getAcquirerId() {
        return acquirerId;
    }

    public void setAcquirerId(String acquirerId) {
        this.acquirerId = acquirerId;
    }

    public String getGroupAcquirer() {
        return groupAcquirer;
    }

    public void setGroupAcquirer(String groupAcquirer) {
        this.groupAcquirer = groupAcquirer;
    }

    public String getEncodedSecretKey() {
        return encodedSecretKey;
    }

    public void setEncodedSecretKey(String encodedSecretKey) {
        this.encodedSecretKey = encodedSecretKey;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.shopTransactionID == null || this.shopTransactionID.trim().isEmpty() || this.acquirerId == null || this.acquirerId.trim().isEmpty()) {

            status.setStatusCode(StatusCode.ADMIN_TEST_GET_PAYMENT_TRANSACTION_DETAILS_INVALID_REQUEST);

            return status;
        }

        status.setStatusCode(StatusCode.ADMIN_TEST_GET_PAYMENT_TRANSACTION_DETAILS_SUCCESS);

        return status;
    }

}
