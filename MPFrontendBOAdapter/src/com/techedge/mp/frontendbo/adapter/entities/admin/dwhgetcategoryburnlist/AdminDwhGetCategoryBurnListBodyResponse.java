package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetcategoryburnlist;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.dwh.adapter.interfaces.CategoryBurnDetail;

public class AdminDwhGetCategoryBurnListBodyResponse {

    private String                   errorCode;
    private String                   statusCode;
    private List<CategoryBurnDetail> categoryBurnDetailList = new ArrayList<CategoryBurnDetail>(0);

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public List<CategoryBurnDetail> getCategoryBurnDetailList() {
        return categoryBurnDetailList;
    }

    public void setCategoryBurnDetailList(List<CategoryBurnDetail> categoryBurnDetailList) {
        this.categoryBurnDetailList = categoryBurnDetailList;
    }

}
