package com.techedge.mp.frontendbo.adapter.entities.admin.dwhgetpartnerlist;

import com.techedge.mp.dwh.adapter.interfaces.DWHGetPartnerResult;
import com.techedge.mp.dwh.adapter.interfaces.PartnerDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class AdminDwhGetPartnerListRequest extends AbstractBORequest implements Validable {

    private Status                              status = new Status();

    private Credential                          credential;

    @Override
    public Status check() {

        Status status = new Status();

        status = Validator.checkCredential("DWH-GET-PARTNER-LIST", credential);

        if (!Validator.isValid(status.getStatusCode())) {

            return status;

        }

        status.setStatusCode(StatusCode.ADMIN_DWH_GET_PARTNER_LIST_SUCCESS);

        return status;

    }

    @Override
    public BaseResponse execute() {

        DWHGetPartnerResult result = getDwhAdapterServiceRemote().getPartnerList();

        AdminDwhGetPartnerListResponse adminDwhGetPartnerListResponse = new AdminDwhGetPartnerListResponse();
        
        status.setStatusCode(result.getStatusCode());
        status.setStatusMessage(result.getMessageCode());
        adminDwhGetPartnerListResponse.setStatus(status);

        if (result.getPartnerDetailList() != null && !result.getPartnerDetailList().isEmpty()) {
            
            AdminDwhGetPartnerListBodyResponse adminDwhGetPartnerListBodyResponse = new AdminDwhGetPartnerListBodyResponse();
            
            for(PartnerDetail partnerDetail : result.getPartnerDetailList()) {
                
                adminDwhGetPartnerListBodyResponse.getPartnerDetailList().add(partnerDetail);
            }
            
            adminDwhGetPartnerListResponse.setBody(adminDwhGetPartnerListBodyResponse);
        }
        
        return adminDwhGetPartnerListResponse;
    }

}