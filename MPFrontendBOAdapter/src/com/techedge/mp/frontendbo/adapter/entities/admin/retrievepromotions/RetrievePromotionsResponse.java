package com.techedge.mp.frontendbo.adapter.entities.admin.retrievepromotions;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Promotion;

public class RetrievePromotionsResponse extends BaseResponse {

    List<Promotion> promotions = new ArrayList<Promotion>(0);

    public List<Promotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(List<Promotion> promotions) {
        this.promotions = promotions;
    }

}
