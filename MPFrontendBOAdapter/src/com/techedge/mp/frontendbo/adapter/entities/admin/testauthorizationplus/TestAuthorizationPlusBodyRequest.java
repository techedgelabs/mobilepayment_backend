package com.techedge.mp.frontendbo.adapter.entities.admin.testauthorizationplus;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class TestAuthorizationPlusBodyRequest implements Validable {

	private TestAuthorizationPlusDataRequest request;


	public TestAuthorizationPlusDataRequest getRequest() {
		return request;
	}

	public void setRequest(TestAuthorizationPlusDataRequest request) {
		this.request = request;
	}


	@Override
    public Status check() {

        Status status = new Status();

        if (this.request == null) {

            status.setStatusCode(StatusCode.ADMIN_TEST_AUTHORIZATION_PLUS_INVALID_REQUEST);

            return status;
        }
        

        return this.request.check();
    }

}
