package com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;

public class GetSurveyBodyRequest implements Validable {

	private String code;
    private Long startSearch;
    private Long endSearch;
    private Integer status;

	
	public String getCode() {
		return code;
	}
	
	public void setCode(String code) {
		this.code = code;
	}
	
    public Long getStartSearch() {
        return startSearch;
    }
    
    public void setStartSearch(Long startSearch) {
        this.startSearch = startSearch;
    }
	
    public Long getEndSearch() {
        return endSearch;
    }
    
    public void setEndSearch(Long endSearch) {
        this.endSearch = endSearch;
    }

	public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
	public Status check() {

		Status status = new Status();
		
        if (this.startSearch != null && this.endSearch != null && this.startSearch > this.endSearch) {

            status.setStatusCode(StatusCode.ADMIN_SURVEY_GET_START_DATE_ERROR);
            return status;

        }
		
		status.setStatusCode(StatusCode.ADMIN_SURVEY_GET_SUCCESS);
		
		return status;
		
	}
	
}
