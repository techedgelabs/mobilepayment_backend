package com.techedge.mp.frontendbo.adapter.entities.admin.createemaildomain;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.interfaces.Validable;
import com.techedge.mp.frontendbo.adapter.webservices.AbstractBORequest;

public class CreateEmailDomainRequest extends AbstractBORequest implements Validable {

    private Status                       status = new Status();

    private Credential                   credential;
    private CreateEmailDomainBodyRequest body;

    public CreateEmailDomainRequest() {}

    public Credential getCredential() {
        return credential;
    }

    public void setCredential(Credential credential) {
        this.credential = credential;
    }

    public CreateEmailDomainBodyRequest getBody() {
        return body;
    }

    public void setBody(CreateEmailDomainBodyRequest body) {
        this.body = body;
    }

    @Override
    public Status check() {

        Status status = new Status();

        if (this.body != null) {

            status = this.body.check();

        }
        else {

            status.setStatusCode(StatusCode.ADMIN_REQU_INVALID_REQUEST);

            return status;

        }

        return status;

    }

    @Override
    public BaseResponse execute() {

        CreateEmailDomainResponse createEmailDomainResponse = new CreateEmailDomainResponse();

        String response = getAdminServiceRemote().adminCreateEmailDomainInBlackList(this.getCredential().getTicketID(), this.getCredential().getRequestID(),
                this.getBody().getEmails());

        status.setStatusCode(response);
        status.setStatusMessage(prop.getProperty(response));
        createEmailDomainResponse.setStatus(status);

        return createEmailDomainResponse;
    }
}
