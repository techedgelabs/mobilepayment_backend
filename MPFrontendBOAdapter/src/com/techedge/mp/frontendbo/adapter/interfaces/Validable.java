package com.techedge.mp.frontendbo.adapter.interfaces;

import com.techedge.mp.frontendbo.adapter.entities.common.Status;

public interface Validable {

	public Status check();
	
}
