package com.techedge.mp.frontendbo.adapter.webservices;

import java.util.Properties;

import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.Validator;

public class AbstractBORequestUtility {

    public static Gson       gson = new Gson();
    public static String     json;


    @SuppressWarnings("rawtypes")
    public static Response abstractRequestInitialize(Properties prop, AbstractBORequest abstractRequest, LoggerServiceRemote loggerService, Class frontendEndpoint, String stringRequest,
            String methodLogger) {
        Status status = null;
        try {

            if (abstractRequest == null) {
                loggerService.log(ErrorLevel.INFO, frontendEndpoint.getClass().getSimpleName(), methodLogger, null, "opening", stringRequest);

                BaseResponse baseResponse = new BaseResponse();

                Status statusResponse = new Status();
                statusResponse.setStatusCode(StatusCode.ABSTRACT_REQUEST_ERROR);
                statusResponse.setStatusMessage(prop.getProperty(StatusCode.ABSTRACT_REQUEST_ERROR));
                baseResponse.setStatus(statusResponse);
                json = gson.toJson(baseResponse);

                loggerService.log(ErrorLevel.INFO, frontendEndpoint.getClass().getSimpleName(), methodLogger, null, "closing", json.toString());

                return Response.status(200).entity(baseResponse).build();

            }
            abstractRequest.setProp(prop);
            status = abstractRequest.check();

            loggerService.log(ErrorLevel.INFO, frontendEndpoint.getClass().getSimpleName(), methodLogger, null, "opening", stringRequest);

            if (Validator.isValid(status.getStatusCode())) {

                BaseResponse response = abstractRequest.execute();

                json = gson.toJson(response);

                loggerService.log(ErrorLevel.INFO, frontendEndpoint.getClass().getSimpleName(), methodLogger, null, "closing", json.toString());

                return Response.status(200).entity(response).build();
            }

        }
        catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        BaseResponse baseResponse = new BaseResponse();

        Status statusResponse = new Status();
        statusResponse.setStatusCode(status.getStatusCode());
        statusResponse.setStatusMessage(prop.getProperty(status.getStatusCode()));
        baseResponse.setStatus(statusResponse);

        json = gson.toJson(baseResponse);
        loggerService.log(ErrorLevel.INFO, frontendEndpoint.getClass().getSimpleName(), methodLogger, null, "closing", json.toString());

        return Response.status(200).entity(baseResponse).build();
    }
}
