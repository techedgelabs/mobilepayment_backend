package com.techedge.mp.frontendbo.adapter.webservices;

import java.util.Properties;

import com.techedge.mp.core.business.AdminServiceRemote;
import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.ReconciliationServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.fidelity.adapter.business.AuthorizationPlusServiceRemote;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public abstract class AbstractBORequest {

    protected Properties                       prop = new Properties();

    private AdminServiceRemote                 adminServiceRemote;
    private TransactionServiceRemote           transactionServiceRemote;
    private ReconciliationServiceRemote        reconciliationServiceRemote;
    private FidelityServiceRemote              fidelityServiceRemote;
    private RefuelingNotificationServiceRemote refuelingNotificationServiceRemote;
    private ForecourtInfoServiceRemote         forecourtInfoServiceRemote;
    private ForecourtPostPaidServiceRemote     forecourtPostPaidServiceRemote;
    private ParametersServiceRemote            parametersServiceRemote;
    private DWHAdapterServiceRemote            dwhAdapterServiceRemote;
    private ParkingServiceRemote               parkingServiceRemote;
    private CardInfoServiceRemote              cardInfoServiceRemote;
    private PaymentServiceRemote               paymentServiceRemote;
    private AuthorizationPlusServiceRemote     authorizationPlusServiceRemote;
    private GPServiceRemote                    gpService;
    private CRMServiceRemote                   crmService;

    public abstract Status check();

    public abstract BaseResponse execute();

    public Properties getProp() {
        return prop;
    }

    public void setProp(Properties prop) {
        this.prop = prop;
    }

    public AdminServiceRemote getAdminServiceRemote() {
        return adminServiceRemote;
    }

    public void setAdminServiceRemote(AdminServiceRemote adminServiceRemote) {
        this.adminServiceRemote = adminServiceRemote;
    }

    public TransactionServiceRemote getTransactionServiceRemote() {
        return transactionServiceRemote;
    }

    public void setTransactionServiceRemote(TransactionServiceRemote transactionServiceRemote) {
        this.transactionServiceRemote = transactionServiceRemote;
    }

    public ReconciliationServiceRemote getReconciliationServiceRemote() {
        return reconciliationServiceRemote;
    }

    public void setReconciliationServiceRemote(ReconciliationServiceRemote reconciliationServiceRemote) {
        this.reconciliationServiceRemote = reconciliationServiceRemote;
    }

    public FidelityServiceRemote getFidelityServiceRemote() {
        return fidelityServiceRemote;
    }

    public void setFidelityServiceRemote(FidelityServiceRemote fidelityServiceRemote) {
        this.fidelityServiceRemote = fidelityServiceRemote;
    }

    public RefuelingNotificationServiceRemote getRefuelingNotificationServiceRemote() {
        return refuelingNotificationServiceRemote;
    }

    public void setRefuelingNotificationServiceRemote(RefuelingNotificationServiceRemote refuelingNotificationServiceRemote) {
        this.refuelingNotificationServiceRemote = refuelingNotificationServiceRemote;
    }

    public ForecourtInfoServiceRemote getForecourtInfoServiceRemote() {
        return forecourtInfoServiceRemote;
    }

    public void setForecourtInfoServiceRemote(ForecourtInfoServiceRemote forecourtInfoServiceRemote) {
        this.forecourtInfoServiceRemote = forecourtInfoServiceRemote;
    }

    public ForecourtPostPaidServiceRemote getForecourtPostPaidServiceRemote() {
        return forecourtPostPaidServiceRemote;
    }

    public void setForecourtPostPaidServiceRemote(ForecourtPostPaidServiceRemote forecourtPostPaidServiceRemote) {
        this.forecourtPostPaidServiceRemote = forecourtPostPaidServiceRemote;
    }

    public ParametersServiceRemote getParametersServiceRemote() {
        return parametersServiceRemote;
    }

    public void setParametersServiceRemote(ParametersServiceRemote parametersServiceRemote) {
        this.parametersServiceRemote = parametersServiceRemote;
    }

    public DWHAdapterServiceRemote getDwhAdapterServiceRemote() {
        return dwhAdapterServiceRemote;
    }

    public void setDwhAdapterServiceRemote(DWHAdapterServiceRemote dwhAdapterServiceRemote) {
        this.dwhAdapterServiceRemote = dwhAdapterServiceRemote;
    }

    public ParkingServiceRemote getParkingServiceRemote() {
        return parkingServiceRemote;
    }

    public void setParkingServiceRemote(ParkingServiceRemote parkingServiceRemote) {
        this.parkingServiceRemote = parkingServiceRemote;
    }

    public CardInfoServiceRemote getCardInfoServiceRemote() {
        return cardInfoServiceRemote;
    }

    public void setCardInfoServiceRemote(CardInfoServiceRemote cardInfoServiceRemote) {
        this.cardInfoServiceRemote = cardInfoServiceRemote;
    }

    public PaymentServiceRemote getPaymentServiceRemote() {
        return paymentServiceRemote;
    }

    public void setPaymentServiceRemote(PaymentServiceRemote paymentServiceRemote) {
        this.paymentServiceRemote = paymentServiceRemote;
    }

	public AuthorizationPlusServiceRemote getAuthorizationPlusServiceRemote() {
		return authorizationPlusServiceRemote;
	}

	public void setAuthorizationPlusServiceRemote(
			AuthorizationPlusServiceRemote authorizationPlusServiceRemote) {
		this.authorizationPlusServiceRemote = authorizationPlusServiceRemote;
	}

    public GPServiceRemote getGpService() {
        return gpService;
    }

    public void setGpService(GPServiceRemote gpService) {
        this.gpService = gpService;
    }

	public CRMServiceRemote getCrmService() {
		return crmService;
	}

	public void setCrmService(CRMServiceRemote crmService) {
		this.crmService = crmService;
	}
    
    
    
}
