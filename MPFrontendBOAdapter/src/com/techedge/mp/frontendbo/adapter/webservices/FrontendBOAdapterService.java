package com.techedge.mp.frontendbo.adapter.webservices;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.techedge.mp.core.business.AdminServiceRemote;
import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.ReconciliationServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.fidelity.adapter.business.AuthorizationPlusServiceRemote;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

@Path("")
public class FrontendBOAdapterService {

    private Properties prop = new Properties();

    private Gson       gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private String     json;

    public FrontendBOAdapterService() {
        loadFileProperty();
    }

    private void loadFileProperty() {

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("response_service.properties");
        try {
            prop.load(inputStream);
            if (inputStream == null) {
                throw new FileNotFoundException("property file response_service.properties not found in the classpath");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

    @POST()
    @Path("/AdminJson")
    @Produces(MediaType.APPLICATION_JSON)
    public Response adminJsonHandler(String stringRequest) throws InterfaceNotFoundException, ParameterNotFoundException {

        AdminRequest request = null;

        LoggerServiceRemote loggerService = null;

        try {
            loggerService = EJBHomeCache.getInstance().getLoggerService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.ADMIN_CREATE_SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.ADMIN_CREATE_SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            System.out.println("Error in log initialization: " + e.getMessage());

            return Response.status(200).entity(baseResponse).build();
        }

        try {
            request = gson.fromJson(stringRequest, AdminRequest.class);
        }
        catch (JsonSyntaxException jsonEx) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.ADMIN_REQU_JSON_SYNTAX_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.ADMIN_REQU_JSON_SYNTAX_ERROR));
            baseResponse.setStatus(statusResponse);

            loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "adminJsonHandler", null, "opening", stringRequest);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "adminJsonHandler", null, "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();

        }

        AdminServiceRemote adminServiceRemote = null;
        TransactionServiceRemote transactionService = null;
        ReconciliationServiceRemote reconciliationService = null;
        FidelityServiceRemote fidelityService = null;
        RefuelingNotificationServiceRemote refuelingNotificationService = null;
        DWHAdapterServiceRemote dwhAdapterServiceRemote = null;
        ForecourtInfoServiceRemote forecourtInfoServiceRemote = null;
        ForecourtPostPaidServiceRemote forecourtPostPaidServiceRemote = null;
        ParametersServiceRemote parametersServiceRemote = null;
        ParkingServiceRemote parkingServiceRemote = null;
        CardInfoServiceRemote cardInfoService = null;
        PaymentServiceRemote paymentService = null;
        AuthorizationPlusServiceRemote authorizationPlusService = null;
        GPServiceRemote gpService = null;
        CRMServiceRemote crmService =null;

        try {
            adminServiceRemote = EJBHomeCache.getInstance().getAdminService();
            transactionService = EJBHomeCache.getInstance().getTransactionService();
            reconciliationService = EJBHomeCache.getInstance().getReconciliationService();
            fidelityService = EJBHomeCache.getInstance().getFidelityService();
            refuelingNotificationService = EJBHomeCache.getInstance().getRefuelingNotificationService();
            dwhAdapterServiceRemote = EJBHomeCache.getInstance().getDwhAdapterService();
            forecourtInfoServiceRemote = EJBHomeCache.getInstance().getForecourtInfoService();
            forecourtPostPaidServiceRemote = EJBHomeCache.getInstance().getForecourtPostPaidService();
            parametersServiceRemote = EJBHomeCache.getInstance().getParametersService();
            parkingServiceRemote = EJBHomeCache.getInstance().getParkingService();
            cardInfoService = EJBHomeCache.getInstance().getCardInfoService();
            paymentService = EJBHomeCache.getInstance().getPaymentService();
            authorizationPlusService = EJBHomeCache.getInstance().getAuthorizationPlusService();
            gpService = EJBHomeCache.getInstance().getGpService();
            crmService = EJBHomeCache.getInstance().getCrmService();
        }
        catch (InterfaceNotFoundException e) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.ADMIN_SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.ADMIN_SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "adminJsonHandler", null, "opening", stringRequest);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "adminJsonHandler", null, "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        if (adminServiceRemote == null || transactionService == null || reconciliationService == null || fidelityService == null || refuelingNotificationService == null) {

            BaseResponse baseResponse = new BaseResponse();

            Status statusResponse = new Status();
            statusResponse.setStatusCode(StatusCode.ADMIN_SYSTEM_ERROR);
            statusResponse.setStatusMessage(prop.getProperty(StatusCode.ADMIN_SYSTEM_ERROR));
            baseResponse.setStatus(statusResponse);

            loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "adminJsonHandler", null, "opening", stringRequest);

            json = gson.toJson(baseResponse);
            // logger.log(Level.INFO, "response : " + json.toString());
            loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "adminJsonHandler", null, "closing", json.toString());

            return Response.status(200).entity(baseResponse).build();
        }

        if (request.getRetrieveActivityLog() == null) {
            loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "adminJsonHandler", null, "opening", stringRequest);
        }

        AbstractBORequest abstractRequest = null;
        try {
            abstractRequest = request.getAbstractRequest();
        }
        catch (IllegalArgumentException | IllegalAccessException e) {
            e.printStackTrace();
        }
        if (abstractRequest != null) {
            abstractRequest.setAdminServiceRemote(adminServiceRemote);
            abstractRequest.setDwhAdapterServiceRemote(dwhAdapterServiceRemote);
            abstractRequest.setFidelityServiceRemote(fidelityService);
            abstractRequest.setForecourtInfoServiceRemote(forecourtInfoServiceRemote);
            abstractRequest.setForecourtPostPaidServiceRemote(forecourtPostPaidServiceRemote);
            abstractRequest.setParametersServiceRemote(parametersServiceRemote);
            abstractRequest.setParkingServiceRemote(parkingServiceRemote);
            abstractRequest.setReconciliationServiceRemote(reconciliationService);
            abstractRequest.setTransactionServiceRemote(transactionService);
            abstractRequest.setCardInfoServiceRemote(cardInfoService);
            abstractRequest.setPaymentServiceRemote(paymentService);
            abstractRequest.setAuthorizationPlusServiceRemote(authorizationPlusService);
            abstractRequest.setGpService(gpService);
            abstractRequest.setCrmService(crmService);
        }

        return AbstractBORequestUtility.abstractRequestInitialize(prop, abstractRequest, loggerService, FrontendBOAdapterService.class, stringRequest, "adminJsonHandler");

    }
}
