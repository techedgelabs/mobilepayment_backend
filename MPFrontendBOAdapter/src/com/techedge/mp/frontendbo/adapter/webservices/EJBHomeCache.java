package com.techedge.mp.frontendbo.adapter.webservices;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;

import com.techedge.mp.core.business.AdminServiceRemote;
import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.ReconciliationServiceRemote;
import com.techedge.mp.core.business.SchedulerServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.fidelity.adapter.business.AuthorizationPlusServiceRemote;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

public class EJBHomeCache {

    final String                                 adminServiceRemoteJndi                 = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/AdminService!com.techedge.mp.core.business.AdminServiceRemote";
    final String                                 parametersServiceRemoteJndi            = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
    final String                                 loggerServiceRemoteJndi                = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
    final String                                 transactionServiceRemoteJndi           = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/TransactionService!com.techedge.mp.core.business.TransactionServiceRemote";
    final String                                 reconciliationServiceRemoteJndi        = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ReconciliationService!com.techedge.mp.core.business.ReconciliationServiceRemote";
    final String                                 fidelityServiceRemoteJndi              = "java:global/MPFidelityAdapterEAR/MPFidelityAdapterEJB/FidelityService!com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote";
    final String                                 schedulerServiceRemoteJndi             = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/SchedulerService!com.techedge.mp.core.business.SchedulerServiceRemote";
    final String                                 forecourtInfoServiceJndi               = "java:global/MPForecourtAdapterEAR/MPForecourtAdapterEJB/ForecourtInfoService!com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote";
    final String                                 forecourtPostPaidServiceRemoteJndi     = "java:global/MPForecourtShopAdapterEAR/MPForecourtShopAdapterEJB/ForecourtPostPaidService!com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote";
    final String                                 refuelingNotificationServiceRemoteJndi = "java:global/MPRefuelingAdapterEAR/MPRefuelingAdapterEJB/RefuelingNotificationService!com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote";
    final String                                 smsServiceRemoteJndi                   = "java:global/MPSmsAdapterEAR/MPSmsAdapterEJB/SmsService!com.techedge.mp.sms.adapter.business.SmsServiceRemote";
    final String                                 dwhAdapterServiceRemoteJndi            = "java:global/MPDWHAdapterEAR/MPDWHAdapterEJB/DWHAdapterService!com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote";
    final String                                 parkingServiceRemoteJndi               = "java:global/MPParkingServiceEAR/MPParkingServiceEJB/ParkingService!com.techedge.mp.parking.integration.business.ParkingServiceRemote";
    final String                                 cardInfoServiceRemoteJndi              = "java:global/MPFidelityAdapterEAR/MPFidelityAdapterEJB/CardInfoService!com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote";
    final String                                 paymentServiceRemoteJndi               = "java:global/MPFidelityAdapterEAR/MPFidelityAdapterEJB/PaymentService!com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote";
    final String                                 authorizationPlusServiceRemoteJndi     = "java:global/MPFidelityAdapterEAR/MPFidelityAdapterEJB/AuthorizationPlusService!com.techedge.mp.fidelity.adapter.business.AuthorizationPlusServiceRemote";
    final String                                 gpServiceRemoteJndi                    = "java:global/MPPaymentAdapterEAR/MPPaymentAdapterEJB/GPService!com.techedge.mp.payment.adapter.business.GPServiceRemote";
    final String                                 crmServiceRemoteJndi                   = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/CRMService!com.techedge.mp.core.business.CRMServiceRemote";
    
    
    private static EJBHomeCache                  instance;

    protected Context                            context                                = null;

    protected AdminServiceRemote                 adminService                           = null;
    protected ParametersServiceRemote            parametersService                      = null;
    protected LoggerServiceRemote                loggerService                          = null;
    protected TransactionServiceRemote           transactionService                     = null;
    protected ReconciliationServiceRemote        reconciliationService                  = null;
    protected FidelityServiceRemote              fidelityService                        = null;
    protected SchedulerServiceRemote             schedulerService                       = null;
    protected ForecourtInfoServiceRemote         forecourtService                       = null;
    protected ForecourtPostPaidServiceRemote     forecourtPostPaidService               = null;
    protected RefuelingNotificationServiceRemote refuelingNotificationService           = null;
    protected SmsServiceRemote                   smsService                             = null;
    protected DWHAdapterServiceRemote            dwhAdapterService                      = null;
    protected ParkingServiceRemote               parkingService                         = null;
    protected CardInfoServiceRemote              cardInfoService                        = null;
    protected PaymentServiceRemote               paymentService                         = null;
    protected AuthorizationPlusServiceRemote     authorizationPlusService               = null;
    protected GPServiceRemote                    gpService                              = null;
    protected CRMServiceRemote                   crmService                             = null;

    private EJBHomeCache() throws InterfaceNotFoundException {

        final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();

        jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");

        try {

            context = new InitialContext(jndiProperties);

            adminService = (AdminServiceRemote) context.lookup(adminServiceRemoteJndi);
            parametersService = (ParametersServiceRemote) context.lookup(parametersServiceRemoteJndi);
            loggerService = (LoggerServiceRemote) context.lookup(loggerServiceRemoteJndi);
            transactionService = (TransactionServiceRemote) context.lookup(transactionServiceRemoteJndi);
            reconciliationService = (ReconciliationServiceRemote) context.lookup(reconciliationServiceRemoteJndi);
            fidelityService = (FidelityServiceRemote) context.lookup(fidelityServiceRemoteJndi);
            schedulerService = (SchedulerServiceRemote) context.lookup(schedulerServiceRemoteJndi);
            forecourtService = (ForecourtInfoServiceRemote) context.lookup(forecourtInfoServiceJndi);
            forecourtPostPaidService = (ForecourtPostPaidServiceRemote) context.lookup(forecourtPostPaidServiceRemoteJndi);
            refuelingNotificationService = (RefuelingNotificationServiceRemote) context.lookup(refuelingNotificationServiceRemoteJndi);
            smsService = (SmsServiceRemote) context.lookup(smsServiceRemoteJndi);
            dwhAdapterService = (DWHAdapterServiceRemote) context.lookup(dwhAdapterServiceRemoteJndi);
            parkingService = (ParkingServiceRemote) context.lookup(parkingServiceRemoteJndi);
            cardInfoService = (CardInfoServiceRemote) context.lookup(cardInfoServiceRemoteJndi);
            paymentService = (PaymentServiceRemote) context.lookup(paymentServiceRemoteJndi);
            authorizationPlusService = (AuthorizationPlusServiceRemote) context.lookup(authorizationPlusServiceRemoteJndi);
            gpService = (GPServiceRemote) context.lookup(gpServiceRemoteJndi);
            crmService = (CRMServiceRemote) context.lookup(crmServiceRemoteJndi);

        }
        catch (Exception ex) {

            //            throw new InterfaceNotFoundException(adminServiceRemoteJndi);
            System.err.println("Errore nella creazione dei servizi remoti: " + ex.getMessage());
        }

    }

    public static synchronized EJBHomeCache getInstance() throws InterfaceNotFoundException {
        if (instance == null)
            instance = new EJBHomeCache();

        return instance;
    }

    public AdminServiceRemote getAdminService() {
        return adminService;
    }

    public ParametersServiceRemote getParametersService() {
        return parametersService;
    }

    public LoggerServiceRemote getLoggerService() {
        return loggerService;
    }

    public TransactionServiceRemote getTransactionService() {
        return transactionService;
    }

    public ReconciliationServiceRemote getReconciliationService() {
        return reconciliationService;
    }

    public FidelityServiceRemote getFidelityService() {
        return fidelityService;
    }

    public SchedulerServiceRemote getSchedulerService() {
        return schedulerService;
    }

    public ForecourtInfoServiceRemote getForecourtInfoService() {
        return forecourtService;
    }

    public ForecourtPostPaidServiceRemote getForecourtPostPaidService() {
        return forecourtPostPaidService;
    }

    public RefuelingNotificationServiceRemote getRefuelingNotificationService() {

        return refuelingNotificationService;
    }

    public SmsServiceRemote getSMSService() {
        if (smsService == null) {
            try {
                smsService = (SmsServiceRemote) context.lookup(smsServiceRemoteJndi);
            }
            catch (Exception ex) {
                System.err.println("Errore nella creazione del servizio di sms: " + ex.getMessage());
            }
        }

        return smsService;
    }

    public void setLoggerService(LoggerServiceRemote loggerService) {
        this.loggerService = loggerService;
    }

    public void setAdminService(AdminServiceRemote adminService) {
        this.adminService = adminService;
    }

    public void setTransactionService(TransactionServiceRemote transactionService) {
        this.transactionService = transactionService;
    }

    public void setReconciliationService(ReconciliationServiceRemote reconciliationService) {
        this.reconciliationService = reconciliationService;
    }

    public void setFidelityService(FidelityServiceRemote fidelityService) {
        this.fidelityService = fidelityService;
    }

    public void setSchedulerService(SchedulerServiceRemote schedulerService) {
        this.schedulerService = schedulerService;
    }

    public void setRefuelingNotificationService(RefuelingNotificationServiceRemote refuelingNotificationService) {
        this.refuelingNotificationService = refuelingNotificationService;
    }

    public DWHAdapterServiceRemote getDwhAdapterService() {
        return dwhAdapterService;
    }

    public void setDwhAdapterService(DWHAdapterServiceRemote dwhAdapterService) {
        this.dwhAdapterService = dwhAdapterService;
    }

    public ParkingServiceRemote getParkingService() {
        return parkingService;
    }

    public void setParkingService(ParkingServiceRemote parkingService) {
        this.parkingService = parkingService;
    }

    public CardInfoServiceRemote getCardInfoService() {
        return cardInfoService;
    }

    public void setCardInfoService(CardInfoServiceRemote cardInfoService) {
        this.cardInfoService = cardInfoService;
    }

    public PaymentServiceRemote getPaymentService() {
        return paymentService;
    }

    public void setPaymentService(PaymentServiceRemote paymentService) {
        this.paymentService = paymentService;
    }

	public AuthorizationPlusServiceRemote getAuthorizationPlusService() {
		return authorizationPlusService;
	}

	public void setAuthorizationPlusService(
			AuthorizationPlusServiceRemote authorizationPlusService) {
		this.authorizationPlusService = authorizationPlusService;
	}

    public GPServiceRemote getGpService() {
        return gpService;
    }

    public void setGpService(GPServiceRemote gpService) {
        this.gpService = gpService;
    }

	public CRMServiceRemote getCrmService() {
		return crmService;
	}

	public void setCrmService(CRMServiceRemote crmService) {
		this.crmService = crmService;
	}
    
    
}
