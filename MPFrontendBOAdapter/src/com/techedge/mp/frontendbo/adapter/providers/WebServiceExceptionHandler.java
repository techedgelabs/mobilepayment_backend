package com.techedge.mp.frontendbo.adapter.providers;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

import com.techedge.mp.frontendbo.adapter.entities.common.BaseResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Status;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class WebServiceExceptionHandler implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception arg0) {

        BaseResponse baseResponse = new BaseResponse();

        Status statusResponse = new Status();
        statusResponse.setStatusCode(StatusCode.ADMIN_SYSTEM_ERROR);
        statusResponse.setStatusMessage("Errore");
        baseResponse.setStatus(statusResponse);

        return Response.status(200).entity(baseResponse).type(MediaType.APPLICATION_JSON).build();
    }

}
