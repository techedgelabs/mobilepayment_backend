package test.com.techedge.mp.frontendbo.adapter.entities.admin.resetsurvey;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.resetsurvey.ResetSurveyBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.resetsurvey.ResetSurveyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class ResetSurveyRequestTest extends BaseTestCase {
    private ResetSurveyRequest     request;
    private ResetSurveyBodyRequest body;
    private String                 code;

    // assigning the values
    protected void setUp() {
        request = new ResetSurveyRequest();
        body = new ResetSurveyBodyRequest();
        code = "test";
        body.setCode(code);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_SURVEY_RESET_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
