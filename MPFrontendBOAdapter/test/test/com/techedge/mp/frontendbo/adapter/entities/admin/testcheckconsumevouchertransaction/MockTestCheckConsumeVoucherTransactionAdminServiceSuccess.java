package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcheckconsumevouchertransaction;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockTestCheckConsumeVoucherTransactionAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminCheckAdminAuthorization(String adminTicketId, String operation) {
        return "ADMIN_CHECK_AUTH_200";
    }
}
