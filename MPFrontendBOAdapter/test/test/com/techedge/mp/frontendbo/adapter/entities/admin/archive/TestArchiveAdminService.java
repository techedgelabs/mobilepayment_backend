package test.com.techedge.mp.frontendbo.adapter.entities.admin.archive;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.archive.ArchiveRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.archive.ArchiveRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.admin.archive.ArchiveResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestArchiveAdminService extends BaseTestCase {
    private FrontendBOAdapterService frontend;
    private ArchiveRequest           request;
    private ArchiveRequestBody       body;
    private ArchiveResponse          response;
    private Response                 baseResponse;
    private String                   json;
    private Gson                     gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest             adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new ArchiveRequest();
        body = new ArchiveRequestBody();
        body.setTransactionID("transaction");
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
        request.setBody(body);
        adminRequest = new AdminRequest();
        adminRequest.setArchive(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
    }

    @Test
    public void testArchiveSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockArchiveAdminServiceSuccess());
        baseResponse = frontend.adminJsonHandler(json);
        response = (ArchiveResponse) baseResponse.getEntity();
        assertEquals("ADMIN_ARCHIVE_200", response.getStatus().getStatusCode());
    }

    @Test
    public void testArchiveFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockArchiveAdminServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (ArchiveResponse) baseResponse.getEntity();
        assertEquals("ADMIN_ARCHIVE_300", response.getStatus().getStatusCode());
    }
}
