package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.Manager;

public class MockUpdateManagerAdminServiceFailure extends MockAdminService {

    @Override
    public String adminUpdateManager(String adminTicketId, String requestId, Manager manager) {
        // TODO Auto-generated method stub
        return "ADMIN_UPDATE_USER_300";
    }
}
