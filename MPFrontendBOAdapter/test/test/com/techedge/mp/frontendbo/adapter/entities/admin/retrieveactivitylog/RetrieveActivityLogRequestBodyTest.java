package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog.RetrieveActivityLogRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveActivityLogRequestBodyTest extends BaseTestCase {

    private RetrieveActivityLogRequestBody body;
    private Long                           start;
    private Long                           end;
    private ErrorLevel                     minLevel;
    private ErrorLevel                     maxLevel;
    private String                         source;
    private String                         groupId;
    private String                         phaseId;
    private String                         messagePattern;

    // assigning the values
    protected void setUp() {
        body = new RetrieveActivityLogRequestBody();
        maxLevel = ErrorLevel.buildErrorLevel(1);
        minLevel = ErrorLevel.buildErrorLevel(1);
        start = 10L;
        end = 11L;
        source = "test";
        groupId = "id";
        phaseId = "test";
        messagePattern = "patter";
        body.setGroupId(groupId);
        body.setEnd(end);
        body.setMinLevel(minLevel);
        body.setMaxLevel(maxLevel);
        body.setMessagePattern(messagePattern);
        body.setPhaseId(phaseId);
        body.setSource(source);
        body.setStart(start);
    }

    @Test
    public void testStartNull() {
        body.setStart(null);
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testEndNull() {
        body.setStart(null);
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testMaxLevelNull() {
        body.setMaxLevel(null);
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testMinLevelNull() {
        body.setMinLevel(null);
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testGroupIDMajor() {
        body.setGroupId("test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testSourceIDMajor() {
        body.setSource("test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testPhaseIDMajor() {
        body.setPhaseId("test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testPatternMajor() {
        body.setMessagePattern("test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_ERROR_PARAMETERS, body.check().getStatusCode());
    }
}