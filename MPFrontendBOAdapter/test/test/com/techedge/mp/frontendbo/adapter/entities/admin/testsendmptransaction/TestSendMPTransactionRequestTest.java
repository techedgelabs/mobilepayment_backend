package test.com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction;

import java.math.BigDecimal;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher.TestCreateVoucherBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher.TestCreateVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestSendMPTransactionRequestTest extends BaseTestCase {
    private TestCreateVoucherRequest     request;
    private TestCreateVoucherBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new TestCreateVoucherRequest();
        body = new TestCreateVoucherBodyRequest();
        body.setAuthorizationCode("auth");
        body.setBankTransactionID("bank");
        body.setVoucherType(VoucherConsumerType.ENI.getValue());
        body.setOperationID("operation");
        body.setPartnerType(PartnerType.MP.getValue());
        body.setRequestTimestamp(12L);
        body.setShopTransactionID("shop");
        body.setTotalAmount(new BigDecimal(1.2));
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_TEST_CREATE_VOUCHER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}