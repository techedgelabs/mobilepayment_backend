package test.com.techedge.mp.frontendbo.adapter.entities.admin.resendconfirmuseremail;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.resendconfirmuseremail.ResendConfirmUserEmailRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class ResendConfirmUserEmailBodyRequestTest extends BaseTestCase {

    private ResendConfirmUserEmailRequestBody body;
    private String                            userEmail;

    // assigning the values
    protected void setUp() {
        body = new ResendConfirmUserEmailRequestBody();
        userEmail = "test";
        body.setUserEmail(userEmail);
    }

    @Test
    public void testEmailNull() {
        body.setUserEmail(null);
        assertEquals(StatusCode.ADMIN_RESEND_CONFIRM_USER_EMAIL_WRONG_USER_EMAIL, body.check().getStatusCode());
    }
}