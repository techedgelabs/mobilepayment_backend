package test.com.techedge.mp.frontendbo.adapter.entities.admin.searchmanager;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.RetrieveManagerListData;

public class MockSearchManagerAdminServiceFailure extends MockAdminService {

    @Override
    public RetrieveManagerListData adminSearchManager(String adminTicketId, String requestId, Long id, String username, String email, Integer maxResults) {
        RetrieveManagerListData data = new RetrieveManagerListData();
        data.setStatusCode("ADMIN_SEARCH_MANAGER_300");
        return data;
    }
}
