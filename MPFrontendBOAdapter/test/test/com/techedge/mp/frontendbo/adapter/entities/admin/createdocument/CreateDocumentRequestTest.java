package test.com.techedge.mp.frontendbo.adapter.entities.admin.createdocument;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createdocument.AdminCreateDocumentBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createdocument.AdminCreateDocumentRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class CreateDocumentRequestTest extends BaseTestCase {

    private AdminCreateDocumentRequest     request;
    private AdminCreateDocumentBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminCreateDocumentRequest();
        body = new AdminCreateDocumentBodyRequest();
        body.setPosition(1);
        body.setDocumentKey("documentKey");
        body.setTemplateFile("template");
        body.setTitle("title");
        body.setUserCategory("NUOVO_FLUSSO");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_DOCUMENT_CREATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}