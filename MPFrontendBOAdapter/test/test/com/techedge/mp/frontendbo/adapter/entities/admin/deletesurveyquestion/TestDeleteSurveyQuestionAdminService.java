package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletesurveyquestion;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletesurveyquestion.DeleteSurveyQuestionResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestDeleteSurveyQuestionAdminService extends TestCase {
    private FrontendBOAdapterService     frontend;
    private Response                     response;
    private String                       json;
    private DeleteSurveyQuestionResponse deleteSurveyQuestionResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"deleteSurveyQuestion\":{\"credential\":{\"ticketID\":\"ODU2MTQ3QzAyNEY2NTFCNkFEMTFEOUE5\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"code\":\"s1q1\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
    }

    @Test
    public void testCreateCustomerUserSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockDeleteSurveyQuestionAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        deleteSurveyQuestionResponse = (DeleteSurveyQuestionResponse) response.getEntity();
        assertEquals("ADMIN_SURVEY_QUESTION_DELETE_200", deleteSurveyQuestionResponse.getStatus().getStatusCode());
    }

    @Test
    public void testCreateCustomerUserFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockDeleteSurveyQuestionAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        deleteSurveyQuestionResponse = (DeleteSurveyQuestionResponse) response.getEntity();
        assertEquals("ADMIN_SURVEY_QUESTION_DELETE_300", deleteSurveyQuestionResponse.getStatus().getStatusCode());
    }
}
