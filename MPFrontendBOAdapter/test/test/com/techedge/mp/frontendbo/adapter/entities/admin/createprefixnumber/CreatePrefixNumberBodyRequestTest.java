package test.com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber.CreatePrefixNumberBodyRequest;

public class CreatePrefixNumberBodyRequestTest extends BaseTestCase {

    private CreatePrefixNumberBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new CreatePrefixNumberBodyRequest();
        body.setCode("code");

    }

    @Test
    public void testCodeNull() {
        body.setCode(null);
        assertEquals(ResponseHelper.PREFIX_RETRIEVE_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testCodeIdEmpty() {
        body.setCode("");
        assertEquals(ResponseHelper.PREFIX_RETRIEVE_FAILURE, body.check().getStatusCode());
    }

}