package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatepromotion;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updatepromotion.UpdatePromotionRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdatePromotionMethodBodyRequestTest extends BaseTestCase {
    private UpdatePromotionRequestBody body;
    private String                     code;
    private String                     description;
    private Integer                    status;
    private Long                       startData;
    private Long                       endData;

    protected void setUp() {

        body = new UpdatePromotionRequestBody();

        code = "code";
        description = "descr";
        status = 4;
        startData = 10101010L;
        endData = 1111111L;

        body.setCode(code);
        body.setDescription(description);
        body.setEndData(endData);
        body.setStartData(startData);
        body.setStatus(status);

    }

    @Test
    public void testCodeNull() {
        body.setCode(null);
        assertEquals(StatusCode.ADMIN_UPDATE_PROMOTION_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testDescriptionNull() {
        body.setDescription(null);
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testStatusNull() {
        body.setStatus(null);
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testStarDateNull() {
        body.setStartData(null);
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testEndDateNull() {
        body.setEndData(null);
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }
}
