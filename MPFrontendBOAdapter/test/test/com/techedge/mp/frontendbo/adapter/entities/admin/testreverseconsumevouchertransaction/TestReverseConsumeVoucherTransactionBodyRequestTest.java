package test.com.techedge.mp.frontendbo.adapter.entities.admin.testreverseconsumevouchertransaction;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testreverseconsumevouchertransaction.TestReverseConsumeVoucherTransactionBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestReverseConsumeVoucherTransactionBodyRequestTest extends BaseTestCase {
    private TestReverseConsumeVoucherTransactionBodyRequest body;
    private String                                          operationID;
    private String                                          operationIDtoReverse;
    private PartnerType                                     partnerType;
    private Long                                            requestTimestamp;

    protected void setUp() {

        body = new TestReverseConsumeVoucherTransactionBodyRequest();

        operationID = "id";
        operationIDtoReverse = "request";
        partnerType = PartnerType.MP;
        requestTimestamp = 10101010L;

        body.setOperationID(operationID);
        body.setOperationIDtoReverse(operationIDtoReverse);
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);
    }

    @Test
    public void testOperationIDNull() {

        body.setOperationID(null);
        assertEquals(StatusCode.ADMIN_TEST_REVERSE_CONSUME_VOUCHER_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testOperationReverseNull() {

        body.setOperationIDtoReverse(null);
        assertEquals(StatusCode.ADMIN_TEST_REVERSE_CONSUME_VOUCHER_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testPartnerNull() {

        body.setPartnerType(null);
        assertEquals(StatusCode.ADMIN_TEST_REVERSE_CONSUME_VOUCHER_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testRequestTimestampNull() {

        body.setRequestTimestamp(null);
        assertEquals(StatusCode.ADMIN_TEST_REVERSE_CONSUME_VOUCHER_INVALID_REQUEST, body.check().getStatusCode());

    }

}
