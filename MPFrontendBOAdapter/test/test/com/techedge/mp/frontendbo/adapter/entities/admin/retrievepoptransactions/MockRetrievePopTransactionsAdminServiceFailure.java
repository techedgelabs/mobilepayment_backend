package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.PostPaidTransactionHistory;
import com.techedge.mp.core.business.interfaces.RetrievePoPTransactionListData;

public class MockRetrievePopTransactionsAdminServiceFailure extends MockAdminService {

    @Override
    public RetrievePoPTransactionListData adminPoPTransactionRetrieve(String adminTicketId, String requestId, String transactionId, String userId, String stationId,
            String sourceId, String mpTransactionStatus, Boolean notificationCreated, Boolean notificationPaid, Boolean notificationUser, Date creationTimestampStart,
            Date creationTimestampEnd, Boolean transactionHistoryFlag) {
        RetrievePoPTransactionListData data = new RetrievePoPTransactionListData();
        List<PostPaidTransactionHistory> postPaidTransactionHistoryList = new ArrayList<PostPaidTransactionHistory>(0);
        data.setPostPaidTransactionHistoryList(postPaidTransactionHistoryList);
        data.setStatusCode("ADMIN_RETRIEVE_POP_TRANSACTION_300");
        return data;
    }

}
