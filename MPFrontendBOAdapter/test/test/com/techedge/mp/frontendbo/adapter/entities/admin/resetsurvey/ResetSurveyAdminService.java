package test.com.techedge.mp.frontendbo.adapter.entities.admin.resetsurvey;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.resetsurvey.ResetSurveyResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class ResetSurveyAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private ResetSurveyResponse      resetSurveyAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"resetSurvey\":{\"credential\":{\"ticketID\":\"ODU2MTQ3QzAyNEY2NTFCNkFEMTFEOUE5\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"code\":\"SURVEY_TRANSACTION\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testResendConfirmUserEmailSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockResetSurveyAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        resetSurveyAdminResponse = (ResetSurveyResponse) response.getEntity();
        assertEquals("ADMIN_SURVEY_RESET_200", resetSurveyAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testResendConfirmUserEmailFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockResetSurveyAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        resetSurveyAdminResponse = (ResetSurveyResponse) response.getEntity();
        assertEquals("ADMIN_SURVEY_RESET_300", resetSurveyAdminResponse.getStatus().getStatusCode());
    }
}
