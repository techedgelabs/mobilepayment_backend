package test.com.techedge.mp.frontendbo.adapter.entities.admin.createdocument;

import java.util.List;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.DocumentAttribute;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockCreateDocumentAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminCreateDocument(String adminTicketId, String requestID, String documentKey, Integer position, String templateFile, String title, String subtitle,
            List<DocumentAttribute> attributes, String userCategory, String groupCategory) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_DOCUMENT_CREATE_SUCCESS;
    }
}
