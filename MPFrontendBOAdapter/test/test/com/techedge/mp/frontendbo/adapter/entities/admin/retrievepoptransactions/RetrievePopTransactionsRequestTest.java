package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions.RetrievePoPTransactionsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions.RetrievePoPTransactionsRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrievePopTransactionsRequestTest extends BaseTestCase {
    private RetrievePoPTransactionsRequest     request;
    private RetrievePoPTransactionsRequestBody body;
    private String                             mpTransactionId;
    private String                             userId;
    private String                             stationId;
    private String                             sourceId;
    private String                             mpTransactionStatus;
    private Boolean                            notificationCreated;
    private Boolean                            notificationPaid;
    private Boolean                            notificationUser;
    private Long                               creationTimestampStart;
    private Long                               creationTimestampEnd;
    private String                             source;
    private Boolean                            mpTransactionHistoryFlag;

    // assigning the values
    protected void setUp() {
        request = new RetrievePoPTransactionsRequest();
        body = new RetrievePoPTransactionsRequestBody();
        userId = "user";
        stationId = "stationID";
        source = "source";
        mpTransactionId = "transactionID";
        sourceId = "sourceID";
        mpTransactionStatus = "PAID";
        notificationCreated = true;
        notificationPaid = true;
        notificationUser = true;
        creationTimestampEnd = 100L;
        creationTimestampStart = 99L;
        mpTransactionHistoryFlag = false;

        body.setCreationTimestampEnd(creationTimestampEnd);
        body.setCreationTimestampStart(creationTimestampStart);
        body.setMpTransactionHistoryFlag(mpTransactionHistoryFlag);
        body.setMpTransactionId(mpTransactionId);
        body.setMpTransactionStatus(mpTransactionStatus);
        body.setNotificationCreated(notificationCreated);
        body.setNotificationPaid(notificationPaid);
        body.setNotificationUser(notificationUser);
        body.setSource(source);
        body.setSourceId(sourceId);
        body.setStationId(stationId);
        body.setUserId(userId);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {
        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialRequestFalse());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
