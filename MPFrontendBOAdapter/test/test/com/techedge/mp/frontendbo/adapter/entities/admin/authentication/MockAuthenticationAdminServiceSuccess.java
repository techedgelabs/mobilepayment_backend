package test.com.techedge.mp.frontendbo.adapter.entities.admin.authentication;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.AdminAuthenticationResponse;

public class MockAuthenticationAdminServiceSuccess extends MockAdminService {
    @Override
    public AdminAuthenticationResponse adminAuthentication(String email, String password, String requestId) {
        AdminAuthenticationResponse response = new AdminAuthenticationResponse();
        Admin data = new Admin();
        data.setEmail("a@a.it");
        data.setFirstName("Andrea");
        data.setLastName("last");
        data.setStatus(1);
        response.setAdmin(data);
        response.setStatusCode("ADMIN_AUTH_200");
        return response;
    }
}
