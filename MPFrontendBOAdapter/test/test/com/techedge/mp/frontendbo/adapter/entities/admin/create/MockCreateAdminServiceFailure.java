package test.com.techedge.mp.frontendbo.adapter.entities.admin.create;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.Admin;

public class MockCreateAdminServiceFailure extends MockAdminService {
    @Override
    public String adminCreate(String adminTicketId, String requestId, Admin admin) {
        // TODO Auto-generated method stub
        return "ADMIN_CREATE_300";
    }
}
