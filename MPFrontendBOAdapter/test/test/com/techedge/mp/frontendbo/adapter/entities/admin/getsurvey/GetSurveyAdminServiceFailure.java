package test.com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey;

import java.util.Date;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.SurveyList;

public class GetSurveyAdminServiceFailure extends MockAdminService {
    @Override
    public SurveyList adminSurveyGet(String adminTicketId, String requestId, String surveyCode, Date startSearch, Date endSearch, Integer status) {
        SurveyList list = new SurveyList();
        list.setStatusCode("ADMIN_SURVEY_GET_300");
        return list;
    }
}
