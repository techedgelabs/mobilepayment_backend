package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethod;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethod.UpdatePaymentMethodRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethod.UpdatePaymentMethodRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdatePaymentMethodRequestTest extends BaseTestCase {
    private UpdatePaymentMethodRequest     request;
    private UpdatePaymentMethodRequestBody body;
    private Long                           id;
    private String                         type;
    private Integer                        status;
    private String                         defaultMethod;
    private Integer                        attemptsLeft;
    private Double                         checkAmount;

    protected void setUp() {

        request = new UpdatePaymentMethodRequest();
        body = new UpdatePaymentMethodRequestBody();

        id = 1L;
        type = "1";
        status = 3;
        defaultMethod = "true";
        attemptsLeft = 1;
        checkAmount = 1.1;

        body.setAttemptsLeft(attemptsLeft);
        body.setCheckAmount(checkAmount);
        body.setDefaultMethod(defaultMethod);
        body.setId(id);
        body.setStatus(status);
        body.setType(type);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_UPDATE_USER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialRequestFalse());
        request.setBody(null);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
