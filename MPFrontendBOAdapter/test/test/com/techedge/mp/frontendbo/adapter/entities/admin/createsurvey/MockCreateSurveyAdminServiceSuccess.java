package test.com.techedge.mp.frontendbo.adapter.entities.admin.createsurvey;

import java.util.ArrayList;
import java.util.Date;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.SurveyQuestion;

public class MockCreateSurveyAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminSurveyCreate(String adminTicketId, String requestId, String code, String description, String note, Integer status, Date startDate, Date endDate,
            ArrayList<SurveyQuestion> questions) {
        // TODO Auto-generated method stub
        return "ADMIN_SURVEY_CREATE_200";
    }
}
