package test.com.techedge.mp.frontendbo.adapter.entities.admin.stationSoperation;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationsoperation.StationsOpResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class StationSAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private StationsOpResponse       stationsOpResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testAddStationsSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationSAdminServiceSuccess());
        json = "{\"stationsAdd\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"stationAdminList\":[{\"response\":\"risposta\",\"isGenerated\":\"1\",\"isDeleted\":\"0\"}]}}}";
        response = frontend.adminJsonHandler(json);
        stationsOpResponse = (StationsOpResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_200", stationsOpResponse.getStatus().getStatusCode());
    }

    @Test
    public void testAddStationsFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationSAdminServiceFailure());
        json = "{\"stationsAdd\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"stationAdminList\":[{\"response\":\"risposta\",\"isGenerated\":\"1\",\"isDeleted\":\"0\"}]}}}";
        response = frontend.adminJsonHandler(json);
        stationsOpResponse = (StationsOpResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_300", stationsOpResponse.getStatus().getStatusCode());
    }

    @Test
    public void testDeletesStationSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationSAdminServiceSuccess());
        json = "{\"stationsDelete\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"stationAdminList\":[{\"response\":\"risposta\",\"isGenerated\":\"1\",\"isDeleted\":\"0\"}]}}}";
        response = frontend.adminJsonHandler(json);
        stationsOpResponse = (StationsOpResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_200", stationsOpResponse.getStatus().getStatusCode());
    }

    @Test
    public void testDeleteStationsFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationSAdminServiceFailure());
        json = "{\"stationsDelete\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"stationAdminList\":[{\"response\":\"risposta\",\"isGenerated\":\"1\",\"isDeleted\":\"0\"}]}}}";
        response = frontend.adminJsonHandler(json);
        stationsOpResponse = (StationsOpResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_300", stationsOpResponse.getStatus().getStatusCode());
    }

}
