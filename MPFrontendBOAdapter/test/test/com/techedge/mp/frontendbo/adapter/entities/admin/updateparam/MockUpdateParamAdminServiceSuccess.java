package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateparam;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockUpdateParamAdminServiceSuccess extends MockAdminService {

    @Override
    public String adminUpdateParam(String adminTicketId, String requestId, String param, String value, String operation) {
        // TODO Auto-generated method stub
        return "ADMIN_UPDATE_PARAM_200";
    }
}
