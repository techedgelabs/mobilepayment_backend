package test.com.techedge.mp.frontendbo.adapter.entities.admin.createblockperiod;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockCreateBlockPeriodAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminCreateBlockPeriod(String ticketId, String requestId, String code, String startDate, String endDate, String startTime, String endTime, String operation,
            String statusCode, String statusMessage, Boolean active) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_BLOCK_PERIOD_CREATE_SUCCESS;
    }
}
