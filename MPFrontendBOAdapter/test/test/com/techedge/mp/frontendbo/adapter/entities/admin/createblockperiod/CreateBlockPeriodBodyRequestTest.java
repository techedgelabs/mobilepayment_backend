package test.com.techedge.mp.frontendbo.adapter.entities.admin.createblockperiod;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createblockperiod.AdminCreateBlockPeriodBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class CreateBlockPeriodBodyRequestTest extends BaseTestCase {

    private AdminCreateBlockPeriodBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new AdminCreateBlockPeriodBodyRequest();
        body.setActive(true);
        body.setCode("code");
        body.setEndDate("2010-05-02");
        body.setStartDate("2010-05-02");
        body.setEndTime("10:10");
        body.setStartTime("11:11");
        body.setOperation("operation");
        body.setEndTime("12:12");
        body.setStatusCode("Error_200");
        body.setStatusMessage("message");

    }

    @Test
    public void testActiveNull() {
        body.setActive(null);
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testCodeNull() {
        body.setCode(null);
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testCodeIsEmpty() {
        body.setCode("");
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testStatusCodeNull() {
        body.setStatusCode(null);
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testStatusCodeIsEmpty() {
        body.setStatusCode("");
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testStatusMessageIsEmpty() {
        body.setStatusMessage("");
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testStatusMessageNull() {
        body.setStatusMessage(null);
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

}