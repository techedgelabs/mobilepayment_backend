package test.com.techedge.mp.frontendbo.adapter.entities.admin.searchmanager;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.searchmanager.SearchManagerBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.searchmanager.SearchManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class SearchManagerRequestTest extends BaseTestCase {
    private SearchManagerRequest     request;
    private SearchManagerBodyRequest body;
    Long                             id;
    String                           username;
    String                           email;
    Integer                          maxResults;

    protected void setUp() {

        request = new SearchManagerRequest();
        body = new SearchManagerBodyRequest();

        body.setEmail(email);
        body.setId(id);
        body.setMaxResults(maxResults);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_SEARCH_MANAGER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
