package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.DeleteTestersData;

public class MockDeleteTestersAdminServiceSuccess extends MockAdminService {
    @Override
    public DeleteTestersData adminTestersDelete(String adminTicketId, String requestId, String emailPrefix, String emailSuffix, Integer startNumber, Integer userCount) {
        DeleteTestersData data = new DeleteTestersData();
        data.setStatusCode("ADMIN_DELETE_TESTER_USER_200");
        return data;
    }
}
