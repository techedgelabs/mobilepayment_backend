package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatepromotion;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updatepromotion.UpdatePromotionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatepromotion.UpdatePromotionRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdatePromotionMethodRequestTest extends BaseTestCase {
    private UpdatePromotionRequest     request;
    private UpdatePromotionRequestBody body;
    private String                     code;
    private String                     description;
    private Integer                    status;
    private Long                       startData;
    private Long                       endData;

    protected void setUp() {

        request = new UpdatePromotionRequest();
        body = new UpdatePromotionRequestBody();

        code = "code";
        description = "descr";
        status = 4;
        startData = 10101010L;
        endData = 1111111L;

        body.setCode(code);
        body.setDescription(description);
        body.setEndData(endData);
        body.setStartData(startData);
        body.setStatus(status);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_UPDATE_USER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialRequestFalse());
        request.setBody(null);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
