package test.com.techedge.mp.frontendbo.adapter.entities.admin.createusercategory;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createusercategory.CreateUserCategoryBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddUserCategoryBodyRequestTest extends BaseTestCase {

    private CreateUserCategoryBodyRequest body;
    private String                        name;

    // assigning the values
    protected void setUp() {
        body = new CreateUserCategoryBodyRequest();
        name = "name";
        body.setName(name);

    }

    @Test
    public void testNameNull() {
        body.setName(null);
        assertEquals(StatusCode.ADMIN_USER_CATEGORY_CREATE_FAILURE, body.check().getStatusCode());
    }

    public void testNameIsEmpty() {
        body.setName("");
        assertEquals(StatusCode.ADMIN_USER_CATEGORY_CREATE_FAILURE, body.check().getStatusCode());
    }

}