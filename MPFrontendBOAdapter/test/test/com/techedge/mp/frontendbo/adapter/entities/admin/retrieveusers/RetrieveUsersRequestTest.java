package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers.RetrieveUsersRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers.RetrieveUsersRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveUsersRequestTest extends BaseTestCase {
    private RetrieveUsersRequest     request;
    private RetrieveUsersRequestBody body;
    private Long                     id;
    private String                   firstName;
    private String                   lastName;
    private String                   fiscalCode;
    private String                   email;
    private Integer                  status;
    private String                   externalUserId;
    private Double                   minAvailableCap;
    private Double                   maxAvailableCap;
    private Double                   minEffectiveCap;
    private Double                   maxEffectiveCap;
    private Long                     creationDateStart;
    private Long                     creationDateEnd;

    // assigning the values
    protected void setUp() {
        request = new RetrieveUsersRequest();
        body = new RetrieveUsersRequestBody();

        email = "email@email.it";
        id = 01L;
        firstName = "name";
        lastName = "last";
        fiscalCode = "QWERTYUIOPLKJHGF";
        status = 5;
        externalUserId = "0012";
        minEffectiveCap = 2.5;
        maxEffectiveCap = 5.5;
        creationDateEnd = 001100L;
        creationDateStart = 110011L;

        body.setCreationDateEnd(creationDateEnd);
        body.setCreationDateStart(creationDateStart);
        body.setEmail(email);
        body.setExternalUserId(externalUserId);
        body.setFirstName(firstName);
        body.setFiscalCode(fiscalCode);
        body.setLastName(lastName);
        body.setId(id);
        body.setMaxAvailableCap(maxAvailableCap);
        body.setMaxEffectiveCap(maxEffectiveCap);
        body.setMinAvailableCap(minAvailableCap);
        body.setMinEffectiveCap(minEffectiveCap);
        body.setStatus(status);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {
        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialRequestFalse());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
