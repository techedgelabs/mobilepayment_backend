package test.com.techedge.mp.frontendbo.adapter.entities.admin.create;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.create.CreateAdminDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.create.CreateAdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.create.CreateAdminRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.admin.create.CreateAdminResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestCreateAdminService extends BaseTestCase {
    private FrontendBOAdapterService frontend;

    private CreateAdminRequest       request;
    private CreateAdminRequestBody   body;
    private CreateAdminDataRequest   data;
    private CreateAdminResponse      response;
    private Response                 baseResponse;
    private String                   json;
    private Gson                     gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest             adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new CreateAdminRequest();
        body = new CreateAdminRequestBody();
        data = new CreateAdminDataRequest();
        body.setAdminData(data);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        adminRequest = new AdminRequest();
        adminRequest.setCreateAdminUser(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
    }

    @Test
    public void testCreateAdminSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateAdminServiceSuccess());
        baseResponse = frontend.adminJsonHandler(json);
        response = (CreateAdminResponse) baseResponse.getEntity();
        assertEquals("ADMIN_CREATE_200", response.getStatus().getStatusCode());
    }

    @Test
    public void testCreateAdminFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateAdminServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (CreateAdminResponse) baseResponse.getEntity();
        assertEquals("ADMIN_CREATE_300", response.getStatus().getStatusCode());
    }
}
