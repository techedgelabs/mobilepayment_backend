package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters.DeleteTestersDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters.DeleteTestersRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class DeleteTestersBodyRequestTest extends BaseTestCase {
    private DeleteTestersRequestBody body;
    private DeleteTestersDataRequest data;

    // assigning the values
    protected void setUp() {
        body = new DeleteTestersRequestBody();
        data = new DeleteTestersDataRequest();
        body.setCustomerUserData(data);
    }

    @Test
    public void testCodeNull() {
        body.setCustomerUserData(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }
}