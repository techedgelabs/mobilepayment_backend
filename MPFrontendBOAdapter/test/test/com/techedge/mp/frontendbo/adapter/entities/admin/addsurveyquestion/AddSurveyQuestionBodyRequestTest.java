package test.com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion.AddSurveyQuestionBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.SurveyQuestion;

public class AddSurveyQuestionBodyRequestTest extends BaseTestCase {

    private AddSurveyQuestionBodyRequest body;
    private String                       code;
    private SurveyQuestion               question;

    // assigning the values
    protected void setUp() {
        body = new AddSurveyQuestionBodyRequest();
        question = new SurveyQuestion();
    }

    @Test
    public void testCodeNull() {

        code = null;
        body.setCode(code);
        body.setQuestion(question);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testCodeIsEmpty() {

        code = "";
        body.setCode(code);
        body.setQuestion(question);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testQuestionNull() {

        code = "3";
        question = null;
        body.setCode(code);
        body.setQuestion(question);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());

    }
}
