package test.com.techedge.mp.frontendbo.adapter.entities.admin.testretrievestation;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.testretrievestationdetails.TestRetrieveStationRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestRetrieveStationBodyRequestTest extends BaseTestCase {
    private TestRetrieveStationRequestBody body;
    private String                         pumpID;
    private String                         requestID;
    private String                         stationID;
    private Boolean                        pumpDetailsReq;

    protected void setUp() {

        body = new TestRetrieveStationRequestBody();

        pumpID = "id";
        requestID = "request";
        stationID = "station";
        pumpDetailsReq = true;

        body.setPumpDetailsReq(pumpDetailsReq);
        body.setPumpID(pumpID);
        body.setRequestID(requestID);
        body.setStationID(stationID);
    }

    @Test
    public void testPumpDetailsReqNull() {

        body.setPumpDetailsReq(null);
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS, body.check().getStatusCode());

    }

    @Test
    public void testPumpIDNull() {

        body.setPumpID(null);
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS, body.check().getStatusCode());

    }

    @Test
    public void testRequestIDNull() {

        body.setRequestID(null);
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS, body.check().getStatusCode());

    }

    @Test
    public void testStationNull() {

        body.setStationID(null);
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS, body.check().getStatusCode());

    }

}
