package test.com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager.AddStationManagerBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager.AddStationManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager.AddStationManagerResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestAddStationManagerAdminService extends BaseTestCase {
    private FrontendBOAdapterService     frontend;
    private AddStationManagerRequest     request;
    private AddStationManagerBodyRequest body;
    private AddStationManagerResponse    response;
    private Response                     baseResponse;
    private String                       json;
    private Gson                         gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest                 adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException, ParameterNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AddStationManagerRequest();
        body = new AddStationManagerBodyRequest();
        body.setManagerID(1L);
        body.setStationID(2L);
        request.setBody(body);
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());

        adminRequest = new AdminRequest();
        adminRequest.setAddStationManager(request);
        json = gson.toJson(adminRequest, AdminRequest.class);

    }

    @Test
    public void testAddStationManagerSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAddStationManagerAdminServiceSuccess());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AddStationManagerResponse) baseResponse.getEntity();
        assertEquals("ADMIN_ADDSTATION_MANAGER_200", response.getStatus().getStatusCode());
    }

    @Test
    public void testAddStationManagerFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAddStationManagerAdminServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AddStationManagerResponse) baseResponse.getEntity();
        assertEquals("ADMIN_ADDSTATION_MANAGER_300", response.getStatus().getStatusCode());
    }
}
