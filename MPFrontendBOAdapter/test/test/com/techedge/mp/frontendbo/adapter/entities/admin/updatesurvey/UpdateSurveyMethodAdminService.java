package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatesurvey;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatesurvey.UpdateSurveyResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class UpdateSurveyMethodAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private UpdateSurveyResponse     updateSurveyMethodAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"updateSurvey\":{\"credential\":{\"ticketID\":\"ODU2MTQ3QzAyNEY2NTFCNkFEMTFEOUE5\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"code\":\"SURVEY_TRANSACTION\",\"description\":\"Aiutaci a migliorare: dai la tua valutazione di Eni Pay. Valuta da 1 a 5 i seguenti quesiti.\",\"note\":\"1 punteggio minimo e 5 punteggio massimo.\",\"startDate\":\"1449807012\",\"endDate\":\"1469807012\",\"status\":\"1\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testUpdateSurveySuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockUpdateSurveyMethodAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        updateSurveyMethodAdminResponse = (UpdateSurveyResponse) response.getEntity();
        assertEquals("ADMIN_SURVEY_UPDATE_200", updateSurveyMethodAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testUpdateSurveyFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockUpdateSurveyMethodAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        updateSurveyMethodAdminResponse = (UpdateSurveyResponse) response.getEntity();
        assertEquals("ADMIN_SURVEY_UPDATE_300", updateSurveyMethodAdminResponse.getStatus().getStatusCode());
    }
}
