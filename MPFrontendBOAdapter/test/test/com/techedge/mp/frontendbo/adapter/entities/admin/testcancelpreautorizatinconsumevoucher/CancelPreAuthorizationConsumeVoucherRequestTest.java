package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreautorizatinconsumevoucher;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreauthorizationconsumevoucher.TestCancelPreAuthorizationConsumeVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreauthorizationconsumevoucher.TestCancelPreAuthorizationConsumeVoucherRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class CancelPreAuthorizationConsumeVoucherRequestTest extends BaseTestCase {

    private TestCancelPreAuthorizationConsumeVoucherRequest     request;
    private TestCancelPreAuthorizationConsumeVoucherRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new TestCancelPreAuthorizationConsumeVoucherRequest();
        body = new TestCancelPreAuthorizationConsumeVoucherRequestBody();
        body.setOperationID("operatioID");
        body.setPartnerType("partnerType");
        body.setPreAuthOperationIDToCancel("preAuth");
        body.setRequestTimestamp(1470142226L);
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_TEST_CANCEL_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
