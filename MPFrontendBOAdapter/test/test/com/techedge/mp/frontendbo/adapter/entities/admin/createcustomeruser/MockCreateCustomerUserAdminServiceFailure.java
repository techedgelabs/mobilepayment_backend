package test.com.techedge.mp.frontendbo.adapter.entities.admin.createcustomeruser;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.user.User;

public class MockCreateCustomerUserAdminServiceFailure extends MockAdminService {
    @Override
    public String adminCustomerUserCreate(String adminTicketId, String requestId, User user) {
        // TODO Auto-generated method stub
        return "ADMIN_CREATE_300";
    }
}
