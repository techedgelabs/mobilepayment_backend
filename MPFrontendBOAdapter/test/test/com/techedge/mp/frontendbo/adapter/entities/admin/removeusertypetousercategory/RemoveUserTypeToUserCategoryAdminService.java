package test.com.techedge.mp.frontendbo.adapter.entities.admin.removeusertypetousercategory;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.removeusertypefromusercategory.RemoveUserTypeFromUserCategoryResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class RemoveUserTypeToUserCategoryAdminService extends TestCase {
    private FrontendBOAdapterService               frontend;
    private Response                               response;
    private String                                 json;
    private RemoveUserTypeFromUserCategoryResponse removeUserTypeToCategoryAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"removeUserTypeFromUserCategory\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"name\":\"ENABLE_USERS\",\"code\":1}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testReconciliationDetailSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRemoveUserTypeToUserCategoryAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        removeUserTypeToCategoryAdminResponse = (RemoveUserTypeFromUserCategoryResponse) response.getEntity();
        assertEquals("ADMIN_USERTYPE_CATEGORY_CREATE_200", removeUserTypeToCategoryAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testReconciliationDetailFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRemoveUserTypeToUserCategoryAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        removeUserTypeToCategoryAdminResponse = (RemoveUserTypeFromUserCategoryResponse) response.getEntity();
        assertEquals("ADMIN_USERTYPE_CATEGORY_CREATE_300", removeUserTypeToCategoryAdminResponse.getStatus().getStatusCode());
    }
}
