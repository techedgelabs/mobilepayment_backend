package test.com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod.FindPaymentMethodRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod.FindPaymentMethodRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class FindPaymentMethodRequestTest extends BaseTestCase {
    private FindPaymentMethodRequest     request;
    private FindPaymentMethodRequestBody body;
    private Long                         id;
    private String                       type;

    // assigning the values
    protected void setUp() {
        request = new FindPaymentMethodRequest();
        body = new FindPaymentMethodRequestBody();
        id = 1L;
        type = "tester";
        body.setId(id);
        body.setType(type);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_UPDATE_USER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
