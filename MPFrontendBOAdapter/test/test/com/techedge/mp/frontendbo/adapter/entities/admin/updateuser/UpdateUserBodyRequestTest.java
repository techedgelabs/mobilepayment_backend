package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateuser;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updateuser.UpdateUserRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateUserBodyRequestTest extends BaseTestCase {
    private UpdateUserRequestBody body;

    private Long                  id;
    private String                language;
    private Integer               status;
    private String                registrationCompleted;
    private Double                capAvailable;
    private Double                capEffective;
    private String                externalUserId;
    private Integer               userType;

    protected void setUp() {

        body = new UpdateUserRequestBody();

        id = 1L;
        language = "ITA";
        status = 0;
        registrationCompleted = "test";
        capAvailable = 4.2;
        capEffective = 3.2;
        externalUserId = "useri";
        userType = 4;

        body.setCapAvailable(capAvailable);
        body.setCapEffective(capEffective);
        body.setExternalUserId(externalUserId);
        body.setId(id);
        body.setLanguage(language);
        body.setRegistrationCompleted(registrationCompleted);
        body.setStatus(status);
        body.setUserType(userType);
    }

    @Test
    public void testIDNull() {
        body.setId(null);
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testLanguage() {
        body.setLanguage("qwertyuiopasfg");
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testRegistration() {
        body.setRegistrationCompleted("qwertyuiopasfg");
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testStatus() {
        body.setStatus(123);
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testCAPMajor() {
        body.setCapEffective(1000000000000.1);
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testCAPMinor() {
        body.setCapEffective(-1000000000000.1);
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testCAPAvailableMajor() {
        body.setCapAvailable(1000000000000.1);
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testCAPAvailableMinor() {
        body.setCapAvailable(-1000000000000.1);
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testExternal() {
        body.setExternalUserId("qwertyuiopasfgqwertyuiopasfgqwertyuiopasfgqwertyuiopasfgqwertyuiopasfgqwertyuiopasfg");
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }
    
    @Test
    public void testUserID() {
        body.setUserType(1);
        assertEquals(StatusCode.ADMIN_UPDATE_USER_ERROR_PARAMETERS, body.check().getStatusCode());
    }
}