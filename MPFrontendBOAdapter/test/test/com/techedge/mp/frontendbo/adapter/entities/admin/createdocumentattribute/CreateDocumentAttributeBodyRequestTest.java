package test.com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute.AdminCreateDocumentAttributeBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class CreateDocumentAttributeBodyRequestTest extends BaseTestCase {

    private AdminCreateDocumentAttributeBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new AdminCreateDocumentAttributeBodyRequest();
        body.setPosition(1);
        body.setDocumentKey("documentKey");
        body.setCheckKey("checkKey");
        body.setConditionText("condition");
        body.setExtendedConditionText("extend");
        body.setMandatory(true);
        body.setPosition(1);
    }

    @Test
    public void testDocumentKeyNull() {
        body.setDocumentKey(null);
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testDocumentKeyIsEmpty() {
        body.setDocumentKey("");
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testPositionNull() {
        body.setPosition(null);
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testCheckKeyNull() {
        body.setCheckKey(null);
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testCheckKeyIsEmpty() {
        body.setCheckKey("");
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testMandatoryNull() {
        body.setMandatory(null);
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

}