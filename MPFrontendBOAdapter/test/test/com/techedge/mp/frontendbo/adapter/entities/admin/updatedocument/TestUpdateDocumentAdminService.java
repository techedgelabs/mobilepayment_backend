package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatedocument;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateblockperiod.AdminUpdateBlockPeriodBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateblockperiod.AdminUpdateBlockPeriodRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateblockperiod.AdminUpdateBlockPeriodResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestUpdateDocumentAdminService extends BaseTestCase {
    private FrontendBOAdapterService          frontend;
    private AdminUpdateBlockPeriodRequest     request;
    private AdminUpdateBlockPeriodBodyRequest body;
    private AdminUpdateBlockPeriodResponse    response;
    private Response                          baseResponse;
    private String                            json;
    private Gson                              gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AdminUpdateBlockPeriodRequest();
        body = new AdminUpdateBlockPeriodBodyRequest();
        body.setActive(true);
        body.setCode("code");
        body.setEndDate("2010-05-02");
        body.setStartDate("2010-05-02");
        body.setEndTime("10:10");
        body.setStartTime("11:11");
        body.setOperation("operation");
        body.setEndTime("12:12");
        body.setStatusCode("Error_200");
        body.setStatusMessage("message");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testCreateBlockPeriodSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockUpdateDocumentAdminServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setUpdateBlockPeriod(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminUpdateBlockPeriodResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_UPDATE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testCreateBlockPeriodFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockUpdateDocumentAdminServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setUpdateBlockPeriod(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminUpdateBlockPeriodResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_UPDATE_FAILURE, response.getStatus().getStatusCode());
    }
}
