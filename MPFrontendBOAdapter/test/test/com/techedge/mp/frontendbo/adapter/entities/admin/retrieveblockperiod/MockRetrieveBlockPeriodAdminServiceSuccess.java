package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveblockperiod;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.UnavailabilityPeriodResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockRetrieveBlockPeriodAdminServiceSuccess extends MockAdminService {

    @Override
    public UnavailabilityPeriodResponse adminRetrieveBlockPeriod(String ticketId, String requestId, String code, Boolean active) {
        UnavailabilityPeriodResponse response = new UnavailabilityPeriodResponse();
        response.setStatusCode(StatusCode.ADMIN_BLOCK_PERIOD_RETRIEVE_SUCCESS);
        return response;
    }
}
