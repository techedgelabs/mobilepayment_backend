package test.com.techedge.mp.frontendbo.adapter.entities.admin.addcardbin;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockAddCardBinAdminServiceSuccess extends MockAdminService{

    @Override
    public String adminCardBinAdd(String adminTicketId, String requestId, String bin) {
        return "ADMIN_CARD_BIN_ADD_200";
    }
    
}
