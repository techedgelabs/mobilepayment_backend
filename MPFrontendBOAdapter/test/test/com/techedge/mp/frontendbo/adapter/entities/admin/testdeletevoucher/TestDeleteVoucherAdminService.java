package test.com.techedge.mp.frontendbo.adapter.entities.admin.testdeletevoucher;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.entities.admin.testcheckvoucher.MockCheckVoucherAdminServiceSuccess;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testdeletevoucher.TestDeleteVoucherBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testdeletevoucher.TestDeleteVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testdeletevoucher.TestDeleteVoucherResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestDeleteVoucherAdminService extends BaseTestCase {
    private FrontendBOAdapterService     frontend;
    private TestDeleteVoucherRequest     request;
    private TestDeleteVoucherBodyRequest body;
    private TestDeleteVoucherResponse    response;
    private Response                     baseResponse;
    private String                       json;
    private Gson                         gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest                 adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new TestDeleteVoucherRequest();
        body = new TestDeleteVoucherBodyRequest();
        body.setOperationID("operation");
        body.setPartnerType(PartnerType.MP.getValue());
        body.setRequestTimestamp(12L);
        body.setOperationIDtoReverse("operation");
        request.setBody(body);
        request.setCredential(createCredentialTrue());

        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
        adminRequest = new AdminRequest();
        EJBHomeCache.getInstance().setAdminService(new MockCheckVoucherAdminServiceSuccess());

        adminRequest.setTestDeleteVoucher(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setFidelityService(new MockTestDeleteVoucherFidelityServiceSuccess());
        EJBHomeCache.getInstance().setAdminService(new MockCheckVoucherAdminServiceSuccess());

        baseResponse = frontend.adminJsonHandler(json);
        response = (TestDeleteVoucherResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_TEST_DELETE_VOUCHER_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setFidelityService(new MockTestDeleteVoucherFidelityServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (TestDeleteVoucherResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_TEST_DELETE_VOUCHER_FAILURE, response.getStatus().getStatusCode());
    }
}
