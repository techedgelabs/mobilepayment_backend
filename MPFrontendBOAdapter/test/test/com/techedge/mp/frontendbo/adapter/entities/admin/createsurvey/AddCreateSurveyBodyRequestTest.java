package test.com.techedge.mp.frontendbo.adapter.entities.admin.createsurvey;

import java.util.ArrayList;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createsurvey.CreateSurveyBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.SurveyQuestion;

public class AddCreateSurveyBodyRequestTest extends BaseTestCase {

    private CreateSurveyBodyRequest   body;
    private String                    code;
    private String                    description;
    private String                    note;
    private Long                      startDate;
    private Long                      endDate;
    private Integer                   status;
    private ArrayList<SurveyQuestion> questions;

    // assigning the values
    protected void setUp() {
        body = new CreateSurveyBodyRequest();
        code = "0123";
        description = "descrizione";
        note = "note";
        startDate = 10091990L;
        endDate = 11091990L;
        status = 2;
        questions = new ArrayList<SurveyQuestion>(0);
        body.setCode(code);
        body.setDescription(description);
        body.setEndDate(endDate);
        body.setNote(note);
        body.setStartDate(startDate);
        body.setStatus(status);
        body.setQuestions(questions);

    }

    @Test
    public void testCodeNull() {
        body.setCode(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    public void testCodeIsEmpty() {
        body.setCode("");
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testStartDateNull() {
        body.setCode(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testEndDateNull() {
        body.setCode(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testStarDateEndDate() {
        body.setEndDate(1000000L);
        body.setStartDate(1000001L);
        assertEquals(StatusCode.ADMIN_SURVEY_CREATE_START_DATE_ERROR, body.check().getStatusCode());
    }

    @Test
    public void testStatusNull() {
        body.setStatus(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

}