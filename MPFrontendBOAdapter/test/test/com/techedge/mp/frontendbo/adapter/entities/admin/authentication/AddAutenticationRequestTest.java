package test.com.techedge.mp.frontendbo.adapter.entities.admin.authentication;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.authentication.AuthenticationAdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.authentication.AuthenticationAdminRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddAutenticationRequestTest extends BaseTestCase {

    private AuthenticationAdminRequest     request;
    private AuthenticationAdminRequestBody body;
    private String                         username;
    private String                         password;
    private String                         requestID;

    // assigning the values
    protected void setUp() {
        request = new AuthenticationAdminRequest();
        body = new AuthenticationAdminRequestBody();
        username = "test";
        password = "123";
        requestID = "abcde";
        body.setPassword(password);
        body.setRequestID(requestID);
        body.setUsername(username);
        request.setBody(body);

    }

    @Test
    public void testBodyValid() {

        assertEquals(StatusCode.ADMIN_CREATE_SUCCESS, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
