package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactionsreconciliation;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.RetrievePoPTransactionListData;

public class MockRetrievePopTransactionsReconciliationAdminServiceFailure extends MockAdminService {

    @Override
    public RetrievePoPTransactionListData adminRetrievePoPTransactionReconciliation(String adminTicketId, String requestId) {
        RetrievePoPTransactionListData data = new RetrievePoPTransactionListData();
        data.setStatusCode("ADMIN_RETR_LOG_300");
        return data;
    }

}
