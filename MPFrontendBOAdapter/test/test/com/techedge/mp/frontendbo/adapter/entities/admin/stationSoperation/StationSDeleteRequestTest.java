package test.com.techedge.mp.frontendbo.adapter.entities.admin.stationSoperation;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.StationAdmin;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationsoperation.StationsDeleteRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationsoperation.StationsOpRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class StationSDeleteRequestTest extends BaseTestCase {
    private StationsDeleteRequest request;
    private StationsOpRequestBody body;
    private List<StationAdmin>    stations;

    protected void setUp() {

        request = new StationsDeleteRequest();
        body = new StationsOpRequestBody();
        stations = new ArrayList<StationAdmin>(0);
        body.setStation(stations);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_CREATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testStationNull() {
        request.setCredential(createCredentialTrue());
        body.setStation(null);
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_STATION_PARAMETERS, body.check().getStatusCode());
    }

}
