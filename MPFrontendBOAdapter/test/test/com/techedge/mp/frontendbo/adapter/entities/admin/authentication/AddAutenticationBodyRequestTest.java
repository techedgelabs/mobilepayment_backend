package test.com.techedge.mp.frontendbo.adapter.entities.admin.authentication;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.authentication.AuthenticationAdminRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddAutenticationBodyRequestTest extends BaseTestCase {

    private AuthenticationAdminRequestBody body;
    private String                         username;
    private String                         password;
    private String                         requestID;

    // assigning the values
    protected void setUp() {
        body = new AuthenticationAdminRequestBody();
        username = "test";
        password = "123";
        requestID = "abcde";
        body.setPassword(password);
        body.setRequestID(requestID);
        body.setUsername(username);

    }

    @Test
    public void testUsernameNull() {
        body.setUsername(null);
        assertEquals(StatusCode.ADMIN_AUTH_EMAIL_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testUsernameMajor() {
        username = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest";
        body.setUsername(username);
        assertEquals(StatusCode.ADMIN_AUTH_EMAIL_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testPasswordNull() {
        body.setPassword(null);
        assertEquals(StatusCode.ADMIN_AUTH_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testPasswordMajor() {
        password = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest";
        body.setPassword(password);
        assertEquals(StatusCode.ADMIN_AUTH_PASSWORD_WRONG, body.check().getStatusCode());
    }

    @Test
    public void testRequestMajor() {
        requestID = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttest";
        body.setRequestID(requestID);
        assertEquals(StatusCode.ADMIN_AUTH_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testRequestTrim() {
        requestID = "testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttest";
        body.setRequestID(requestID);
        assertEquals(StatusCode.ADMIN_AUTH_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testRequestNull() {
        body.setRequestID(null);
        assertEquals(StatusCode.ADMIN_AUTH_FAILURE, body.check().getStatusCode());
    }
}