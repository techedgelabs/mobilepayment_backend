package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactionsreconciliation;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactionsreconciliation.RetrievePoPTransactionsReconciliationResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class RetrievePopTransactionsReconciliationAdminService extends TestCase {
    private FrontendBOAdapterService                      frontend;
    private Response                                      response;
    private String                                        json;
    private RetrievePoPTransactionsReconciliationResponse retrievePopTransactionsReconciliationAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"retrievePoPTransactionsReconciliation\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrievePopTransactionsReconciliationAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        retrievePopTransactionsReconciliationAdminResponse = (RetrievePoPTransactionsReconciliationResponse) response.getEntity();
        assertEquals("ADMIN_RETRIEVE_POP_TRANSACTION_RECONCILIATION_200", retrievePopTransactionsReconciliationAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrievePopTransactionsReconciliationAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        retrievePopTransactionsReconciliationAdminResponse = (RetrievePoPTransactionsReconciliationResponse) response.getEntity();
        assertEquals("ADMIN_RETRIEVE_POP_TRANSACTION_RECONCILIATION_200", retrievePopTransactionsReconciliationAdminResponse.getStatus().getStatusCode());
    }
}
