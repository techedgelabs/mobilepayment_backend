package test.com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion;

import com.techedge.mp.core.business.interfaces.PromoVoucherInput;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockAddVoucherToPromotionAdminServiceFailure extends MockAdminService {
    @Override
    public String adminAddVoucherToPromotion(String adminTicketId, String requestId, PromoVoucherInput listPromoVoucher, String promotionCode) {
        // TODO Auto-generated method stub
        return "ADMIN_ADD_VOUCHER_TO_PROMOTION_301";
    }
}
