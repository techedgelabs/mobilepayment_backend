package test.com.techedge.mp.frontendbo.adapter.entities.admin.createmanager;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createmanager.CreateManagerBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createmanager.CreateManagerDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createmanager.CreateManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddCreateManagerRequestTest extends BaseTestCase {

    private CreateManagerRequest     request;
    private CreateManagerBodyRequest body;
    private CreateManagerDataRequest data;
    private String username;
    private String password;
    private String email;

    // assigning the values
    protected void setUp() {
        request = new CreateManagerRequest();
        body = new CreateManagerBodyRequest();
        data = new CreateManagerDataRequest();
        username = "user";
        password = "password123";
        email = "user@email.com";
        data.setEmail(email);
        data.setPassword(password);
        data.setUsername(username);
        body.setManagerData(data);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS, body.check().getStatusCode());
    }

    @Test
    public void testDataValid() {
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS, body.check().getStatusCode());
    }

    @Test
    public void testDataNull() {
        body.setManagerData(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testData() {
        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS, data.check().getStatusCode());
    }
}
