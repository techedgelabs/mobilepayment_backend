package test.com.techedge.mp.frontendbo.adapter.entities.admin.resendconfirmuseremail;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockResendConfirmUserEmailAdminServiceFailure extends MockAdminService {

    @Override
    public String adminResendConfirmUserEmail(String adminTicketId, String requestId, String userEmail, String category) {
        // TODO Auto-generated method stub
        return "ADMIN_UPDATE_USER_300";
    }
}
