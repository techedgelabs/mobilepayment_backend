package test.com.techedge.mp.frontendbo.adapter.entities.admin.testdeletevoucher;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.DeleteVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockTestDeleteVoucherFidelityServiceSuccess extends MockFidelityService {

    @Override
    public DeleteVoucherResult deleteVoucher(String operationIDtoReverse, String operationID, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {
        DeleteVoucherResult result = new DeleteVoucherResult();
        result.setCsTransactionID("src");
        result.setMessageCode("message");
        result.setStatusCode(StatusCode.ADMIN_TEST_DELETE_VOUCHER_SUCCESS);
        return result;
    }
}
