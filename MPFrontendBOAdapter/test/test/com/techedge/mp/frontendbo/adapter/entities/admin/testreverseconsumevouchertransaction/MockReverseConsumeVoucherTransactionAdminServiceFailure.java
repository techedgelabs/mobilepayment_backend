package test.com.techedge.mp.frontendbo.adapter.entities.admin.testreverseconsumevouchertransaction;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockReverseConsumeVoucherTransactionAdminServiceFailure extends MockAdminService {
    @Override
    public String adminCheckAdminAuthorization(String adminTicketId, String operation) {
        return "ADMIN_CHECK_AUTH_300";
    }
}
