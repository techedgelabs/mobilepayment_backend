package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatetransactions;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updatetransaction.UpdateTransactionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatetransaction.UpdateTransactionRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateTransactionRequestTest extends BaseTestCase {
    private UpdateTransactionRequest     request;
    private UpdateTransactionRequestBody body;

    private String                       transactionId;
    private String                       finalStatusType;
    private Boolean                      GFGNotification;
    private Boolean                      confirmed;

    protected void setUp() {

        request = new UpdateTransactionRequest();
        body = new UpdateTransactionRequestBody();

        transactionId = "id";
        finalStatusType = "finale";
        GFGNotification = true;
        confirmed = false;

        body.setConfirmed(confirmed);
        body.setFinalStatusType(finalStatusType);
        body.setGFGNotification(GFGNotification);
        body.setTransactionId(transactionId);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_UPDATE_TRANSACTION_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialRequestFalse());
        request.setBody(null);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
