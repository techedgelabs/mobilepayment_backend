package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcheckloadloyaltycreditstransaction;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcheckloadloyaltycreditstransaction.TestCheckLoadLoyaltyCreditsTransactionBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestCheckLoadLoyaltyCreditsTransactionBodyRequestTest extends BaseTestCase {
    private TestCheckLoadLoyaltyCreditsTransactionBodyRequest body;
    private String                                            operationID;
    private String                                            operationIDtoCheck;
    private PartnerType                                       partnerType;
    private Long                                              requestTimestamp;

    protected void setUp() {

        body = new TestCheckLoadLoyaltyCreditsTransactionBodyRequest();
        operationID = "operation";
        operationIDtoCheck = "check";
        partnerType = PartnerType.MP;
        requestTimestamp = 1010101L;

        body.setOperationID(operationID);
        body.setOperationIDtoCheck(operationIDtoCheck);
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);

    }

    @Test
    public void testOperationNull() {

        body.setOperationID(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_CHECK_LOYALTY_CREDITS_TRANSACTION_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testIOperationIDToCheckNull() {

        body.setOperationIDtoCheck(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_CHECK_LOYALTY_CREDITS_TRANSACTION_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testPartnerTypeNull() {

        body.setPartnerType(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_CHECK_LOYALTY_CREDITS_TRANSACTION_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testRequestTimestampNull() {

        body.setRequestTimestamp(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_CHECK_LOYALTY_CREDITS_TRANSACTION_INVALID_REQUEST, body.check().getStatusCode());

    }

}
