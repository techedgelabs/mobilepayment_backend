package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievedocument;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrievedocument.AdminRetrieveDocumentBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievedocument.AdminRetrieveDocumentRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveDocumentRequestTest extends BaseTestCase {
    private AdminRetrieveDocumentRequest     request;
    private AdminRetrieveDocumentBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminRetrieveDocumentRequest();
        body = new AdminRetrieveDocumentBodyRequest();
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_DOCUMENT_RETRIEVE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);

        assertEquals(StatusCode.ADMIN_DOCUMENT_RETRIEVE_FAILURE, request.check().getStatusCode());
    }

}