package test.com.techedge.mp.frontendbo.adapter.entities.admin.removeprefixnumber;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.removeprefixnumber.RemovePrefixNumberBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.removeprefixnumber.RemovePrefixNumberRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RemovePrefixNumberRequestTest extends BaseTestCase {

    private RemovePrefixNumberRequest     request;
    private RemovePrefixNumberBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new RemovePrefixNumberRequest();
        body = new RemovePrefixNumberBodyRequest();
        body.setCode("code");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.PREFIX_REMOVE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}