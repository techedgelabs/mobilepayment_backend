package test.com.techedge.mp.frontendbo.adapter.entities.admin.archive;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.archive.ArchiveRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddArchiveBodyRequestTest extends BaseTestCase {

    private ArchiveRequestBody body;
    private String             transaction;

    // assigning the values
    protected void setUp() {
        body = new ArchiveRequestBody();
        transaction = "test";
        body.setTransactionID(transaction);
    }

    @Test
    public void testTransactionMajor() {
        body.setTransactionID("test1test1test1test1test1test1test1" + "test1test1test1test1test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());

    }

}
