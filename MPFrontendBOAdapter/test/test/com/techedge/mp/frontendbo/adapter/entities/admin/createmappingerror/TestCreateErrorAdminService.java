package test.com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror.AdminCreateErrorBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror.AdminCreateErrorRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror.AdminCreateErrorResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestCreateErrorAdminService extends BaseTestCase {
    private FrontendBOAdapterService    frontend;
    private AdminCreateErrorRequest     request;
    private AdminCreateErrorBodyRequest body;
    private AdminCreateErrorResponse    response;
    private Response                    baseResponse;
    private String                      json;
    private Gson                        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AdminCreateErrorRequest();
        body = new AdminCreateErrorBodyRequest();
        body.setErrorCode("error");
        body.setStatusCode("error");
        request.setBody(body);
        request.setCredential(createCredentialTrue());

        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testCreateCustomerUserSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateErrorAdminServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setAdminCreateError(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminCreateErrorResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_CREATE__SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testCreateCustomerUserFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateErrorAdminServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setAdminCreateError(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminCreateErrorResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_CREATE__FAILURE, response.getStatus().getStatusCode());
    }
}
