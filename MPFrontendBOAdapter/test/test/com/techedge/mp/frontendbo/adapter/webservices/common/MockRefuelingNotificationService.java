package test.com.techedge.mp.frontendbo.adapter.webservices.common;

import java.util.List;

import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.MpTransactionDetail;
import com.techedge.mp.refueling.integration.entities.NotifySubscriptionResult;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;
import com.techedge.mp.refueling.integration.entities.RefuelingAuthentiationResult;

public class MockRefuelingNotificationService implements RefuelingNotificationServiceRemote {

    @Override
    public String sendMPTransactionNotification(String requestID, String srcTransactionID, String mpTransactionID, String mpTransactionStatus, RefuelDetail refuelDetail) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String sendMPTransactionReport(String requestID, String startDate, String endDate, List<MpTransactionDetail> mpTransactionList) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RefuelingAuthentiationResult refuelingAuthentication(String requestID, String username, String password) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public NotifySubscriptionResult notifySubscription(String requestID, String fiscalCode) {
        // TODO Auto-generated method stub
        return null;
    }

}
