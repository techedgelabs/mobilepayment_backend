package test.com.techedge.mp.frontendbo.adapter.webservices.common;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.techedge.mp.core.business.AdminServiceRemote;
import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.AdminArchiveInvertResponse;
import com.techedge.mp.core.business.interfaces.AdminArchiveResponse;
import com.techedge.mp.core.business.interfaces.AdminAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.AdminDeleteTicketResponse;
import com.techedge.mp.core.business.interfaces.AdminErrorResponse;
import com.techedge.mp.core.business.interfaces.AdminPoPArchiveResponse;
import com.techedge.mp.core.business.interfaces.AdminPropagationUserDataResult;
import com.techedge.mp.core.business.interfaces.AdminRemoteSystemCheckResult;
import com.techedge.mp.core.business.interfaces.AdminRetrieveDataResponse;
import com.techedge.mp.core.business.interfaces.AdminRolesResponse;
import com.techedge.mp.core.business.interfaces.AdminUpdateParamsResponse;
import com.techedge.mp.core.business.interfaces.AdminUserNotVerifiedDeleteResult;
import com.techedge.mp.core.business.interfaces.CityInfo;
import com.techedge.mp.core.business.interfaces.CountPendingTransactionsResult;
import com.techedge.mp.core.business.interfaces.DeleteTestersData;
import com.techedge.mp.core.business.interfaces.DocumentAttribute;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GeneratePvResponse;
import com.techedge.mp.core.business.interfaces.GenerateTestersData;
import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.ParamInfo;
import com.techedge.mp.core.business.interfaces.PaymentDataResponse;
import com.techedge.mp.core.business.interfaces.PaymentInfoResponse;
import com.techedge.mp.core.business.interfaces.PaymentMethodResponse;
import com.techedge.mp.core.business.interfaces.PollingIntervalData;
import com.techedge.mp.core.business.interfaces.PrefixNumberResult;
import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.core.business.interfaces.ProvinceData;
import com.techedge.mp.core.business.interfaces.RemoteSystem;
import com.techedge.mp.core.business.interfaces.RetrieveActivityLogData;
import com.techedge.mp.core.business.interfaces.RetrieveDocumentResponse;
import com.techedge.mp.core.business.interfaces.RetrieveLandingResponse;
import com.techedge.mp.core.business.interfaces.RetrieveManagerListData;
import com.techedge.mp.core.business.interfaces.RetrieveParamsData;
import com.techedge.mp.core.business.interfaces.RetrievePoPTransactionListData;
import com.techedge.mp.core.business.interfaces.RetrievePromotionsData;
import com.techedge.mp.core.business.interfaces.RetrieveStatisticsData;
import com.techedge.mp.core.business.interfaces.RetrieveTransactionListData;
import com.techedge.mp.core.business.interfaces.RetrieveVoucherTransactionListData;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.StationActivationData;
import com.techedge.mp.core.business.interfaces.StationData;
import com.techedge.mp.core.business.interfaces.StationsAdminData;
import com.techedge.mp.core.business.interfaces.SurveyList;
import com.techedge.mp.core.business.interfaces.SurveyQuestion;
import com.techedge.mp.core.business.interfaces.TypeData;
import com.techedge.mp.core.business.interfaces.UnavailabilityPeriodResponse;
import com.techedge.mp.core.business.interfaces.UpdatePvActivationFlagResult;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationDetail;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfoData;
import com.techedge.mp.core.business.interfaces.user.RetrieveUserListData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryData;
import com.techedge.mp.core.business.interfaces.user.UserTypeData;

public class MockAdminService implements AdminServiceRemote {

    @Override
    public AdminAuthenticationResponse adminAuthentication(String email, String password, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminLogout(String ticketId, String requestId) {
        return null;
    }

    @Override
    public String adminCreate(String adminTicketId, String requestId, Admin admin) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveActivityLogData adminActivityLogRetrieve(String adminTicketId, String requestId, Timestamp start, Timestamp end, ErrorLevel minLevel, ErrorLevel maxLevel,
            String source, String groupId, String phaseId, String messagePattern) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveUserListData adminUserRetrieve(String adminTicketId, String requestId, Long id, String firstName, String lastName, String fiscalCode, String securityDataEmail,
            Integer userStatus, String externalUserId, Double capAvailableMin, Double capAvailableMax, Double capEffectiveMin, Double capEffectiveMax, Timestamp creationDateMin,
            Timestamp creationDateMax, Integer userType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveTransactionListData adminTransactionRetrieve(String adminTicketId, String requestId, String transactionId, String userId, String stationId, String pumpId,
            String finalStatusType, Boolean GFGNotification, Boolean confirmed, Date creationTimestampStart, Date creationTimestampEnd, String productID,
            Boolean transactionHistoryFlag) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrievePoPTransactionListData adminPoPTransactionRetrieve(String adminTicketId, String requestId, String transactionId, String userId, String stationId,
            String sourceId, String mpTransactionStatus, Boolean notificationCreated, Boolean notificationPaid, Boolean notificationUser, Date creationTimestampStart,
            Date creationTimestampEnd, Boolean transactionHistoryFlag) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentInfoResponse adminPaymentMethodRetrieve(String adminTicketId, String requestId, Long id, String type) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUserUpdate(String adminTicketId, String requestId, Long id, String firstName, String lastName, String email, Date birthDate, String birthMunicipality,
            String birthProvince, String sex, String fiscalCode, String language, Integer status, Boolean registrationCompleted, Double capAvailable, Double capEffective,
            String externalUserId, Integer userType, Boolean virtualizationCompleted, Integer virtualizationAttemptsLeft, Boolean eniStationUserType, Boolean depositCardStepCompleted) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminTransactionUpdate(String adminTicketId, String requestId, String transactionId, String finalStatusType, Boolean GFGNotification, Boolean confirmed,
            Integer ReconciliationAttemptsLeft) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminPaymentMethodUpdate(String adminTicketId, String requestId, Long id, String type, Integer status, String defaultMethod, Integer attemptsLeft,
            Double checkAmount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCustomerUserCreate(String adminTicketId, String requestId, User user) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUserDelete(String adminTicketId, String requestId, Long userId, Date creationStart, Date creationEnd) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GenerateTestersData adminTestersGenerate(String adminTicketId, String requestId, String emailPrefix, String emailsuffix, Integer startNumber, Integer userCount,
            String token, Integer userType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public DeleteTestersData adminTestersDelete(String adminTicketId, String requestId, String emailPrefix, String emailSuffix, Integer startNumber, Integer userCount) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveStatisticsData adminRetrieveStatistics(String adminTicketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminAddStation(String adminTicketId, String requestId, Station station) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdateStation(String adminTicketId, String requestId, Station station) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminDeleteStation(String adminTicketId, String requestId, Station station) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public StationsAdminData adminAddStations(String adminTicketId, String requestId, StationsAdminData stationsAdminData) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public StationsAdminData adminDeleteStations(String adminTicketId, String requestId, StationsAdminData stationsAdminData) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public StationsAdminData adminSearchStations(String adminTicketId, String requestId, Station station) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveParamsData adminRetrieveParams(String adminTicketId, String requestId, String param) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdateParam(String adminTicketId, String requestId, String param, String value, String operation) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AdminUpdateParamsResponse adminUpdateParams(String adminTicketId, String requestId, List<ParamInfo> params) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminResendConfirmUserEmail(String adminTicketId, String requestId, String userEmail, String category) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ReconciliationInfoData adminReconcileTransactions(String adminTicketId, String requestId, List<String> transactionsID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ReconciliationDetail adminReconcileDetail(String adminTicketId, String requestId, String transactionID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreateManager(String adminTicketId, String requestId, Manager manager) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminDeleteManager(String adminTicketId, String requestId, Manager manager) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdateManager(String adminTicketId, String requestId, Manager manager) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveManagerListData adminSearchManager(String adminTicketId, String requestId, Long id, String username, String email, Integer maxResults) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminAddStationManager(String adminTicketId, String requestId, Long managerId, Long stationId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCheckAdminAuthorization(String adminTicketId, String operation) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrievePoPTransactionListData adminRetrievePoPTransactionReconciliation(String adminTicketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminAddVoucherToPromotion(String adminTicketId, String requestId, PromoVoucherInput listPromoVoucher, String promotionCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminSurveyCreate(String adminTicketId, String requestId, String code, String description, String note, Integer status, Date startDate, Date endDate,
            ArrayList<SurveyQuestion> questions) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminSurveyUpdate(String adminTicketId, String requestId, String code, String description, String note, Integer status, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminSurveyQuestionAdd(String adminTicketId, String requestId, String surveyCode, String questionCode, String text, String type, Integer sequence) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public SurveyList adminSurveyGet(String adminTicketId, String requestId, String surveyCode, Date startSearch, Date endSearch, Integer status) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminSurveyReset(String adminTicketId, String requestId, String surveyCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminSurveyQuestionUpdate(String adminTicketId, String requestId, String questionCode, String text, String type, Integer sequence) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminSurveyQuestionDelete(String adminTicketId, String requestId, String questionCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCardBinAdd(String adminTicketId, String requestId, String bin) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrievePromotionsData adminRetrievePromotions(String adminTicketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdatePromotion(String adminTicketId, String requestId, String code, String description, Integer status, Date startData, Date endData) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreateUserCategory(String adminTicketId, String requestId, String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreateUserType(String adminTicketId, String requestId, Integer code) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminAddTypeToUserCategory(String adminTicketId, String requestId, String name, Integer code) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminRemoveTypeFromUserCategory(String adminTicketId, String requestId, String name, Integer code) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public UserCategoryData adminRetrieveUserCategory(String adminTicketId, String requestId, String name) {
        return null;
    }

    @Override
    public UserTypeData adminRetrieveUserType(String adminTicketId, String requestId, Integer code) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AdminArchiveResponse adminArchive(String adminTicketId, String requestId, String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AdminArchiveInvertResponse adminArchiveInvert(String adminTicketId, String requestId, String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AdminPoPArchiveResponse adminPoPArchive(String adminTicketId, String requestId, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminEditMappingError(String adminTicketId, String requestID, String gpErrorCode, String mpStatusCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreateMappingError(String adminTicketId, String requestID, String gpErrorCode, String mpStatusCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<AdminErrorResponse> adminGetMappingError(String adminTicketId, String requestID, String mpStatusCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminRemoveMappingError(String adminTicketId, String requestID, String mpStatusCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreatePrefixNumber(String adminTicketId, String requestId, String code) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminRemovePrefixNumber(String adminTicketId, String requestId, String code) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PrefixNumberResult adminRetrieveAllPrefixNumber(String adminTicketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveVoucherTransactionListData adminRetrieveVoucherTransactions(String adminTicketId, String requestId, String voucherTransactionId, Long userId,
            String voucherCode, Date creationTimestampStart, Date creationTimestampEnd, String finalStatusType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminForceUpdateTermsOfService(String ticketId, String requestId, String keyval, Long personalDataId, Boolean valid) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreateBlockPeriod(String ticketId, String requestId, String code, String startDate, String endDate, String startTime, String endTime, String operation,
            String statusCode, String statusMessage, Boolean active) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdateBlockPeriod(String ticketId, String requestId, String code, String startDate, String endDate, String startTime, String endTime, String operation,
            String statusCode, String statusMessage, Boolean active) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminDeleteBlockPeriod(String ticketId, String requestId, String code) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public UnavailabilityPeriodResponse adminRetrieveBlockPeriod(String ticketId, String requestId, String code, Boolean active) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminDeleteDocument(String adminTicketId, String requestID, String documentKey) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveDocumentResponse adminRetrieveDocument(String adminTicketId, String requestID, String documentKey) {
        // TODO Auto-generated method stub
        return null;
    }


    @Override
    public String adminDeleteDocumentAttribute(String adminTicketId, String requestID, String checkKey) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdatePassword(String adminTicketId, String requestID, String oldPassword, String newPassword) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminForceSmsNotificationStatus(String ticketId, String requestId, String correlationId, Integer status) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminGenerateMailingList(String ticketId, String requestId, String selectionType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdateMailingList(String ticketId, String requestId, Long id, String email, String name, Integer status, String template) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminAssignVoucher(String ticketId, String requestId, Long userId, String voucherCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminRemoveTransactionEvent(String ticketId, String requestId, Long transactionEventId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreateEmailDomainInBlackList(String adminTicketId, String requestId, List<String> emails) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminEditEmailDomainInBlackList(String adminTicketId, String requestId, String id, String email) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminRemoveEmailDomainInBlackList(String adminTicketId, String requestId, String email) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> adminGetEmailDomainInBlackList(String adminTicketId, String requestId, String email) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminInsertUserInPromoOnHold(String ticketId, String requestId, String promotionCode, Long userId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdatePopTransaction(String ticketId, String requestId, String transactionId, String mpTransactionStatus, Boolean toReconcile) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdateVoucherTransaction(String ticketId, String requestId, String voucherTransactionId, String finalStatusType) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdateVoucherData(String ticketId, String requestId, Long id, String status) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminDeleteVoucher(String ticketId, String requestId, Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreateProvince(String ticketId, String requestId, List<ProvinceData> province) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminRemoveMobilePhone(String ticketId, String requestId, Long mobilePhoneId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<AdminPropagationUserDataResult> adminPropagationUserData(String adminTicketId, String requestId, Long userID, List<String> fiscalCode) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminGeneralCreate(String adminTicketId, String requestId, String type, List<TypeData> fields) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminGeneralUpdate(String adminTicketId, String requestId, String type, Long id, List<TypeData> fields) {
        // TODO Auto-generated method stub
        return null;
    }

  @Override
    public String adminUpdatePrepaidConsumeVoucher(String ticketId, String requestId, Long id, String csTransactionID, String marketingMsg, String messageCode, String operationID,
            String operationIDReversed, String operationType, Boolean reconciled, String statusCode, Double totalConsumed, String warningMsg, Double amount,
            String preAuthOperationID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminAddPrepaidConsumeVoucherDetail(String ticketId, String requestId, Double consumedValue, Date expirationDate, Double initialValue, String promoCode,
            String promoDescription, String promoDoc, Double voucherBalanceDue, String voucherCode, String voucherStatus, String voucherType, Double voucherValue,
            Long prePaidConsumeVoucherId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminGeneralDelete(String ticketId, String requestId, String type, String id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreateAdminRole(String adminTicketId, String requestID, String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminDeleteAdminRole(String adminTicketId, String requestID, String name) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminAddRoleToAdmin(String adminTicketId, String requestId, String role, String admin) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AdminRolesResponse retrieveAdminRole(String adminTicketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminRemoveRoleToAdmin(String adminTicketId, String requestId, String role, String admin) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminPushNotificationSend(String adminTicketId, String requestID, Long userID, Long idMessage, String titleNotification, String messageNotification) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GeneratePvResponse adminGeneratePv(String ticketId, String requestID, Integer finalStatus, Boolean skipCheck, String noOilAcquirerId, String noOilShopLogin,
            String oilAcquirerId, String oilShopLogin, List<StationData> stationList) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AdminRetrieveDataResponse adminRetrieve(String adminTicketId, String requestID, Long id, String email, Integer status) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdate(String adminTicketId, String requestID, Admin admin) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminDelete(String adminTicketId, String requestID, Long adminID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreateRefuelingUser(String ticketId, String requestId, String username, String password) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdatePasswordRefuelingUser(String ticketId, String requestId, String username, String newPassword) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentMethodResponse adminInsertPaymentMethodRefuelingUser(String ticketId, String requestId, String username) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminRemovePaymentMethodRefuelingUser(String ticketId, String requestId, String username, Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentDataResponse adminRetrievePaymentDataMethodRefuelingUser(String ticketId, String requestId, String username, Long id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminGeneralMassiveDelete(String ticketId, String requestId, String type, String idFrom, String idTo) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public RetrieveLandingResponse adminRetrieveLanding(String ticketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreateDocument(String adminTicketId, String requestID, String documentKey, Integer position, String templateFile, String title, String subtitle,
            List<DocumentAttribute> attributes, String userCategory, String groupCategory) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdateDocument(String adminTicketId, String requestID, String documentKey, Integer position, String templateFile, String title, String subtitle,
            String userCategory, String groupCategory) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminCreateDocumentAttribute(String adminTicketId, String requestID, String checkKey, Boolean mandatory, Integer position, String conditionText,
            String extendedConditionText, String documentKey, String subtitle) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PollingIntervalData adminRetrievePollingInterval(String adminTicketId, String requestId) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminUpdateCities(String adminTicketId, String requestId, List<CityInfo> citiesList, boolean deleteAllRows) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AdminUserNotVerifiedDeleteResult adminUserNotVerifiedDelete(String adminTicketId, String requestId, Long userId, Date creationStart, Date creationEnd) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public UpdatePvActivationFlagResult adminUpdatePvActivationFlag(String ticketId, String requestID, List<StationActivationData> stationActivationList) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AdminDeleteTicketResponse adminTicketDelete(String ticketId, String requestId, String ticketType, String idFrom, String idTo) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<AdminRemoteSystemCheckResult> systemCheck(String adminTicketId, String requestId, List<RemoteSystem> remoteSystemList) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PaymentMethodResponse adminInsertMulticardRefuelingUser(String adminTicketId, String requestId, String username) {
        // TODO Auto-generated method stub
        return null;
    }

	@Override
    public CountPendingTransactionsResult adminCountPendingTransactions(String adminTicketId, String requestId, String serverName) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String adminPaymentMethodStatusUpdate(String adminTicketId, String requestId, Long id, String type, Integer status, String message) {
        // TODO Auto-generated method stub
        return null;
    }
}
