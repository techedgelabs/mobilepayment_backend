package test.com.techedge.mp.frontendbo.adapter.entities.admin.testenableloyaltycardlist;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testgetloyaltycardlist.TestGetLoyaltyCardListBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestEnableLoyaltyCardListBodyRequestTest extends BaseTestCase {
    private TestGetLoyaltyCardListBodyRequest body;
    private String                            operationID;
    private String                            fiscalCode;
    private String                            eanCode;
    private PartnerType                       partnerType;
    private Long                              requestTimestamp;

    protected void setUp() {

        body = new TestGetLoyaltyCardListBodyRequest();
        operationID = "operation";
        fiscalCode = "check";
        eanCode = "panCode";
        partnerType = PartnerType.MP;
        requestTimestamp = 1010101L;

        body.setOperationID(operationID);
        body.setFiscalCode(fiscalCode);
        body.setEanCode(eanCode);
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);

    }

    @Test
    public void testOperationNull() {

        body.setOperationID(null);
        assertEquals(StatusCode.ADMIN_TEST_GET_LOYALTY_CARD_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testIFiscalCodeNull() {

        body.setFiscalCode(null);
        assertEquals(StatusCode.ADMIN_TEST_GET_LOYALTY_CARD_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testPanCodeNull() {

        body.setEanCode(null);
        assertEquals(StatusCode.ADMIN_TEST_GET_LOYALTY_CARD_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testPartnerTypeNull() {

        body.setPartnerType(null);
        assertEquals(StatusCode.ADMIN_TEST_GET_LOYALTY_CARD_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testRequestTimestampNull() {

        body.setRequestTimestamp(null);
        assertEquals(StatusCode.ADMIN_TEST_GET_LOYALTY_CARD_INVALID_REQUEST, body.check().getStatusCode());

    }

}
