package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievepromotions;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepromotions.RetrievePromotionsRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrievePromotionsRequestTest extends BaseTestCase {
    private RetrievePromotionsRequest request;

    // assigning the values
    protected void setUp() {
        request = new RetrievePromotionsRequest();

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}