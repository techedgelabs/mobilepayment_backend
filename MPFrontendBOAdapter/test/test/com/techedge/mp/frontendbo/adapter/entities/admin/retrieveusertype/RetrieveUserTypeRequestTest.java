package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusertype;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusertype.RetrieveUserTypeRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusertype.RetrieveUserTypeRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveUserTypeRequestTest extends BaseTestCase {
    private RetrieveUserTypeRequest     request;
    private RetrieveUserTypeRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveUserTypeRequest();
        body = new RetrieveUserTypeRequestBody();
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_USER_TYPE_RETRIEVE, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_USER_TYPE_RETRIEVE_FAILURE, request.check().getStatusCode());
    }

}