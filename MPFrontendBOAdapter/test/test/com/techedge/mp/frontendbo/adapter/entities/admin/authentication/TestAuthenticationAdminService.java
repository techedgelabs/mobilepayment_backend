package test.com.techedge.mp.frontendbo.adapter.entities.admin.authentication;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.authentication.AuthenticationAdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.authentication.AuthenticationAdminRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.admin.authentication.AuthenticationAdminResponseFailure;
import com.techedge.mp.frontendbo.adapter.entities.admin.authentication.AuthenticationAdminResponseSuccess;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestAuthenticationAdminService extends BaseTestCase {
    private FrontendBOAdapterService       frontend;
    private AuthenticationAdminRequest     request;
    private AuthenticationAdminRequestBody body;
    private Response                       baseResponse;
    private String                         json;
    private Gson                           gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest                   adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AuthenticationAdminRequest();
        body = new AuthenticationAdminRequestBody();
        String username = "test";
        String password = "Password123";
        String requestID = "abcde";
        body.setPassword(password);
        body.setRequestID(requestID);
        body.setUsername(username);
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());

        adminRequest = new AdminRequest();
        adminRequest.setAuthenticationAdmin(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
    }

    @Test
    public void testAuthenticationSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        AuthenticationAdminResponseSuccess response;
        EJBHomeCache.getInstance().setAdminService(new MockAuthenticationAdminServiceSuccess());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AuthenticationAdminResponseSuccess) baseResponse.getEntity();
        assertEquals("ADMIN_AUTH_200", response.getStatus().getStatusCode());
    }

    @Test
    public void testAuthenticationFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        AuthenticationAdminResponseFailure response;
        EJBHomeCache.getInstance().setAdminService(new MockAuthenticationAdminServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AuthenticationAdminResponseFailure) baseResponse.getEntity();
        assertEquals("ADMIN_AUTH_300", response.getStatus().getStatusCode());
    }
}
