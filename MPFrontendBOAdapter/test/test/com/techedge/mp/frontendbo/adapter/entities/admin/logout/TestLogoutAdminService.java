package test.com.techedge.mp.frontendbo.adapter.entities.admin.logout;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.logout.LogoutAdminResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestLogoutAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private LogoutAdminResponse      logoutAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"logoutAdmin\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
    }

    @Test
    public void testLogoutSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockLogoutAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        logoutAdminResponse = (LogoutAdminResponse) response.getEntity();
        assertEquals("ADMIN_LOGOUT_200", logoutAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testLogoutFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockLogoutAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        logoutAdminResponse = (LogoutAdminResponse) response.getEntity();
        assertEquals("ADMIN_LOGOUT_300", logoutAdminResponse.getStatus().getStatusCode());
    }
}
