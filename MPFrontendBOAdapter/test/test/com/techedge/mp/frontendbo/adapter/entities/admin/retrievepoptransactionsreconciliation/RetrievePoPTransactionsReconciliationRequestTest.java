package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactionsreconciliation;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactionsreconciliation.RetrievePoPTransactionsReconciliationRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrievePoPTransactionsReconciliationRequestTest extends BaseTestCase {
    private RetrievePoPTransactionsReconciliationRequest request;

    // assigning the values
    protected void setUp() {
        request = new RetrievePoPTransactionsReconciliationRequest();

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}