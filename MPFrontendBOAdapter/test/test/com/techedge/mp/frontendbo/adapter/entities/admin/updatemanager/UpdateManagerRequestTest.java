package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager.UpdateManagerBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager.UpdateManagerDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager.UpdateManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateManagerRequestTest extends BaseTestCase {
    private UpdateManagerRequest     request;
    private UpdateManagerBodyRequest body;
    private UpdateManagerDataRequest managerData;
    private Long                     id;
    private String                   email;
    private String                   username;

    //    private List<String>             params = new ArrayList<String>(0);

    protected void setUp() {

        request = new UpdateManagerRequest();
        body = new UpdateManagerBodyRequest();
        managerData = new UpdateManagerDataRequest();

        id = 1L;
        username = "user";
        email = "a@a.it";

        managerData.setEmail(email);
        managerData.setId(id);
        managerData.setUsername(username);

        body.setManagerData(managerData);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_UPDATE_USER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
