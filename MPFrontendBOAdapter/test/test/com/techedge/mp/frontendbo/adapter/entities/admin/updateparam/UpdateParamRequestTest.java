package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateparam;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updateparam.UpdateParamRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateparam.UpdateParamRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateParamRequestTest extends BaseTestCase {
    private UpdateParamRequest     request;
    private UpdateParamRequestBody body;
    private String                 param;
    private String                 value;
    private String                 operation;

    protected void setUp() {

        request = new UpdateParamRequest();
        body = new UpdateParamRequestBody();

        operation = "ope";
        value = "value";
        param = "param";

        body.setOperation(operation);
        body.setParam(param);
        body.setValue(value);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_UPDATE_PARAM_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
