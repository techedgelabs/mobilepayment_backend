package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcheckloadloyaltycreditstransaction;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockTestCheckLoadLoyaltyCreditsTransactionFidelityServiceSuccess extends MockFidelityService {
    @Override
    public CheckLoadLoyaltyCreditsTransactionResult checkLoadLoyaltyCreditsTransaction(String operationID, String operationIDtoCheck, PartnerType partnerType, Long requestTimestamp)
            throws FidelityServiceException {
        CheckLoadLoyaltyCreditsTransactionResult data = new CheckLoadLoyaltyCreditsTransactionResult();
        data.setStatusCode(StatusCode.ADMIN_TEST_LOAD_CHECK_LOYALTY_CREDITS_TRANSACTION_SUCCESS);
        return data;
    }
}
