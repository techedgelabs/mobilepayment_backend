package test.com.techedge.mp.frontendbo.adapter.entities.admin.createcustomeruser;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.createcustomeruser.CreateCustomerUserResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestCreateCustomerUserAdminService extends TestCase {
    private FrontendBOAdapterService   frontend;
    private Response                   response;
    private String                     json;
    private CreateCustomerUserResponse createCustomerUserResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"createCustomerUser\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"customerUserData\":{\"password\":\"test\",\"token\":\"token\",\"newPin\":\"13\",\"userInfo\":{\"id\":\"1\",\"firstName\":\"test\",\"lastName\":\"lastname\",\"fiscalCode\":\"fiscal\",\"birthDate\":\"00-00-0000\",\"birthMunicipality\":\"00-00-0000\",\"birthProvince\":\"11-11-1111\",\"language\":\"ita\",\"sex\":\"m\",\"email\":\"test@test.it\",\"lastLoginData\":{\"time\":\"2008-12-01 12:00:00\",\"deviceId\":\"EJB\",\"deviceName\":\"iPhone\",\"latitude\":\"45.65\",\"longitude\":\"35.99\"},\"status\":\"4\",\"registrationCompleted\":\"completed\",\"capAvailable\":\"175\",\"capEffective\":\"175\",\"mobilePhone\":\"0680909090\",\"externalUserId\":\"12\",\"paymentData\":[{\"id\":\"123\",\"type\":\"male\",\"brand\":\"mastercard\",\"expirationDate\":\"00-00-0000\",\"pan\":\"oip\",\"status\":\"4\",\"message\":\"succ\",\"defaultMethod\":\"contati\",\"attemptsLeft\":\"0\",\"checkAmount\":\"3.0\",\"insertTimestamp\":\"1956-12-12\",\"verifiedTimestamp\":\"1956-12-12\",\"token\":\"1234\",\"pin\":\"1234\"}]}}}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
    }

    @Test
    public void testCreateCustomerUserSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateCustomerUserAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        createCustomerUserResponse = (CreateCustomerUserResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_200", createCustomerUserResponse.getStatus().getStatusCode());
    }

    @Test
    public void testCreateCustomerUserFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateCustomerUserAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        createCustomerUserResponse = (CreateCustomerUserResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_300", createCustomerUserResponse.getStatus().getStatusCode());
    }
}
