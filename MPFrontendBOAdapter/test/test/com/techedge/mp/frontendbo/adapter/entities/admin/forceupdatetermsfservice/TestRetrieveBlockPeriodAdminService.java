package test.com.techedge.mp.frontendbo.adapter.entities.admin.forceupdatetermsfservice;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.forceupdatetermsfservice.ForceUpdateTermsOfServiceBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.forceupdatetermsfservice.ForceUpdateTermsOfServiceRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.forceupdatetermsfservice.ForceUpdateTermsOfServiceResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestRetrieveBlockPeriodAdminService extends BaseTestCase {
    private FrontendBOAdapterService             frontend;
    private ForceUpdateTermsOfServiceRequest     request;
    private ForceUpdateTermsOfServiceBodyRequest body;
    private ForceUpdateTermsOfServiceResponse    response;
    private Response                             baseResponse;
    private String                               json;
    private Gson                                 gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new ForceUpdateTermsOfServiceRequest();
        body = new ForceUpdateTermsOfServiceBodyRequest();
        body.setTermsOfServiceId("term_of_service");
        body.setUserId(001L);
        body.setValid(true);
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testCreateBlockPeriodSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveBlockPeriodAdminServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setForceUpdateTermsOfService(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (ForceUpdateTermsOfServiceResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_UPDATE_TERMS_OF_SERVICE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testCreateBlockPeriodFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveBlockPeriodAdminServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setForceUpdateTermsOfService(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (ForceUpdateTermsOfServiceResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_UPDATE_TERMS_OF_SERVICE_FAILURE, response.getStatus().getStatusCode());
    }
}
