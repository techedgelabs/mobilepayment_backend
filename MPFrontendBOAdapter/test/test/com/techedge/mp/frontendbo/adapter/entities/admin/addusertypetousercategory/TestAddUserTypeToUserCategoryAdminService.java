package test.com.techedge.mp.frontendbo.adapter.entities.admin.addusertypetousercategory;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addusertypetousercategory.AddUserTypeToUserCategoryBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addusertypetousercategory.AddUserTypeToUserCategoryRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addusertypetousercategory.AddUserTypeToUserCategoryResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestAddUserTypeToUserCategoryAdminService extends BaseTestCase {
    private FrontendBOAdapterService             frontend;
    private AddUserTypeToUserCategoryRequest     request;
    private AddUserTypeToUserCategoryBodyRequest body;
    private AddUserTypeToUserCategoryResponse    response;
    private Response                             baseResponse;
    private String                               json;
    private Gson                                 gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest                         adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AddUserTypeToUserCategoryRequest();
        body = new AddUserTypeToUserCategoryBodyRequest();
        body.setCode(1);
        body.setName("name");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());

        adminRequest = new AdminRequest();
        adminRequest.setAddUserTypeToUserCategory(request);
        json = gson.toJson(adminRequest, AdminRequest.class);

    }

    @Test
    public void testAddUserTypeToUserCategorySuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAddUserTypeToUserCategoryAdminServiceSuccess());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AddUserTypeToUserCategoryResponse) baseResponse.getEntity();
        assertEquals("ADMIN_USERTYPE_CATEGORY_UPDATE_200", response.getStatus().getStatusCode());
    }

    @Test
    public void testAddUserTypeToUserCategoryFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAddUserTypeToUserCategoryAdminServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AddUserTypeToUserCategoryResponse) baseResponse.getEntity();
        assertEquals("ADMIN_USERTYPE_CATEGORY_UPDATE_300", response.getStatus().getStatusCode());
    }
}
