package test.com.techedge.mp.frontendbo.adapter.entities.admin.admineditmappingerror;

import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockAdminEditErrorAdminServiceFailure extends MockAdminService {
    @Override
    public String adminEditMappingError(String adminTicketId, String requestID, String gpErrorCode, String mpStatusCode) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_MAPPING_ERROR_EDIT__FAILURE;
    }
}
