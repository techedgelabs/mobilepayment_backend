package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveparams;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveparams.RetrieveParamsResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class RetrieveActivityParamsAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private RetrieveParamsResponse   retrieveParamsAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"retrieveParams\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"param\":\"param\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveParamsAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        retrieveParamsAdminResponse = (RetrieveParamsResponse) response.getEntity();
        assertEquals("ADMIN_RETRIEVE_PARAMS_200", retrieveParamsAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveParamsAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        retrieveParamsAdminResponse = (RetrieveParamsResponse) response.getEntity();
        assertEquals("ADMIN_RETRIEVE_PARAMS_300", retrieveParamsAdminResponse.getStatus().getStatusCode());
    }
}
