package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatedocument;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updatedocument.AdminUpdateDocumentBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatedocument.AdminUpdateDocumentRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateDocumentRequestTest extends BaseTestCase {

    private AdminUpdateDocumentRequest     request;
    private AdminUpdateDocumentBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminUpdateDocumentRequest();
        body = new AdminUpdateDocumentBodyRequest();
        body.setDocumentKey("key");
        request.setBody(body);
        request.setCredential(createCredentialTrue());

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_DOCUMENT_UPDATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_DOCUMENT_UPDATE_FAILURE, request.check().getStatusCode());
    }

}