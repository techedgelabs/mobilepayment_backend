package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions;

import java.util.Date;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.RetrieveVoucherTransactionListData;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockRetrieveVoucherTransactionsAdminServiceFailure extends MockAdminService {

    @Override
    public RetrieveVoucherTransactionListData adminRetrieveVoucherTransactions(String adminTicketId, String requestId, String voucherTransactionId, Long userId,
            String voucherCode, Date creationTimestampStart, Date creationTimestampEnd, String finalStatusType) {
        RetrieveVoucherTransactionListData result = new RetrieveVoucherTransactionListData();
        result.setStatusCode(StatusCode.RETRIEVE_VOUCHER_TRANSACTIONS_FAILURE);
        return result;
    }

}
