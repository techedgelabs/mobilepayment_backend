package test.com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute.AdminCreateDocumentAttributeBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute.AdminCreateDocumentAttributeRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute.AdminCreateDocumentAttributeResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestCreateDocumentAttributeAdminService extends BaseTestCase {
    private FrontendBOAdapterService                frontend;
    private AdminCreateDocumentAttributeRequest     request;
    private AdminCreateDocumentAttributeBodyRequest body;
    private AdminCreateDocumentAttributeResponse    response;
    private Response                                baseResponse;
    private String                                  json;
    private Gson                                    gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AdminCreateDocumentAttributeRequest();
        body = new AdminCreateDocumentAttributeBodyRequest();
        body.setPosition(1);
        body.setDocumentKey("documentKey");
        body.setCheckKey("checkKey");
        body.setConditionText("condition");
        body.setExtendedConditionText("extend");
        body.setMandatory(true);
        body.setPosition(1);
        request.setBody(body);
        request.setCredential(createCredentialTrue());

        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testCreateCustomerUserSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateDocumentAdminAttributeServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setCreateDocumentAttribute(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminCreateDocumentAttributeResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testCreateCustomerUserFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateDocumentAttributeAdminServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setCreateDocumentAttribute(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminCreateDocumentAttributeResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_FAILURE, response.getStatus().getStatusCode());
    }
}
