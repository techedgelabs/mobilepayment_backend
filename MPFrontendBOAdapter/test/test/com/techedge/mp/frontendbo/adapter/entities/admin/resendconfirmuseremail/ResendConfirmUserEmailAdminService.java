package test.com.techedge.mp.frontendbo.adapter.entities.admin.resendconfirmuseremail;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.resendconfirmuseremail.ResendConfirmUserEmailResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class ResendConfirmUserEmailAdminService extends TestCase {
    private FrontendBOAdapterService       frontend;
    private Response                       response;
    private String                         json;
    private ResendConfirmUserEmailResponse resendConfirmUserEmailAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"resendConfirmUserEmail\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"userEmail\":\"ENABLE_USERS\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testResendConfirmUserEmailSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockResendConfirmUserEmailAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        resendConfirmUserEmailAdminResponse = (ResendConfirmUserEmailResponse) response.getEntity();
        assertEquals("ADMIN_UPDATE_USER_200", resendConfirmUserEmailAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testResendConfirmUserEmailFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockResendConfirmUserEmailAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        resendConfirmUserEmailAdminResponse = (ResendConfirmUserEmailResponse) response.getEntity();
        assertEquals("ADMIN_UPDATE_USER_300", resendConfirmUserEmailAdminResponse.getStatus().getStatusCode());
    }
}
