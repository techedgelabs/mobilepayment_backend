package test.com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction.SendCredential;
import com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction.TestSendMPTransactionNotificationBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction.TestSendMPTransactionNotificationRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction.TestSendMPTransactionReportBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction.TestSendMPTransactionReportRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction.TestSendMPTransactionResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.RefuelDetail;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestSendMPTransactionAdminService extends BaseTestCase {
    private FrontendBOAdapterService                     frontend;
    private TestSendMPTransactionReportRequest           request_report;
    private TestSendMPTransactionReportBodyRequest       body_report;
    private TestSendMPTransactionNotificationRequest     request_notification;
    private TestSendMPTransactionNotificationBodyRequest body_notification;
    private TestSendMPTransactionResponse                response;
    private Response                                     baseResponse;
    private String                                       json;
    private Gson                                         gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest                                 adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request_report = new TestSendMPTransactionReportRequest();
        body_report = new TestSendMPTransactionReportBodyRequest();
        body_report.setEndDate("19");
        body_report.setStartDate("20");
        request_report.setBody(body_report);

        request_notification = new TestSendMPTransactionNotificationRequest();
        body_notification = new TestSendMPTransactionNotificationBodyRequest();
        body_notification.setMpTransactionID("id");
        body_notification.setMpTransactionStatus("status");
        body_notification.setRefuelDetail(new RefuelDetail());
        body_notification.setSrcTransactionID("src");
        request_notification.setBody(body_notification);

        SendCredential sendCredential = new SendCredential();
        sendCredential.setRequestID("IPH-jjj");
        request_report.setSendCredential(sendCredential);
        request_notification.setSendCredential(sendCredential);

        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
        adminRequest = new AdminRequest();

        json = gson.toJson(adminRequest, AdminRequest.class);
    }

    @Test
    public void testTestSendMPTransactionReportRequestSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockTestSendMPTransactionRefuelingNotificationServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestCheckSendMPTransactionReport(request_report);
        json = gson.toJson(adminRequest, AdminRequest.class);

        baseResponse = frontend.adminJsonHandler(json);
        response = (TestSendMPTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_TEST_FIDELITY_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void TestSendMPTransactionReportRequestFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockTestSendMPTransactionRefuelingNotificationServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestCheckSendMPTransactionReport(request_report);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (TestSendMPTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, response.getStatus().getStatusCode());
    }

    @Test
    public void testTestSendMPTransactionNotificationRequestSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockTestSendMPTransactionRefuelingNotificationServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestCheckSendMPTransactionReport(request_report);
        json = gson.toJson(adminRequest, AdminRequest.class);

        baseResponse = frontend.adminJsonHandler(json);
        response = (TestSendMPTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_TEST_FIDELITY_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void TestSendMPTransactionNotificationRequestFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockTestSendMPTransactionRefuelingNotificationServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestCheckSendMPTransactionReport(request_report);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (TestSendMPTransactionResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, response.getStatus().getStatusCode());
    }
}
