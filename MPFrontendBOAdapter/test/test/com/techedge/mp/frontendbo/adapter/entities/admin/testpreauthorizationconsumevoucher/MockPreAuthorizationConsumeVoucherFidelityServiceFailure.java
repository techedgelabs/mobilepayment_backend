package test.com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher;

import java.util.List;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PreAuthorizationConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;

public class MockPreAuthorizationConsumeVoucherFidelityServiceFailure extends MockFidelityService {
    @Override
    public PreAuthorizationConsumeVoucherResult preAuthorizationConsumeVoucher(String operationID, String mpTransactionID, VoucherConsumerType voucherType, String stationID,
            Double amount, List<VoucherCodeDetail> voucherCodeList, PartnerType partnerType, Long requestTimestamp, String language, ProductType productType)
            throws FidelityServiceException {

        PreAuthorizationConsumeVoucherResult response = new PreAuthorizationConsumeVoucherResult();
        response.setAmount(10.05);
        response.setCsTransactionID("000987654321000");
        response.setPreAuthOperationID("111111");
        response.setMessageCode("Nessun voucher valido");
        response.setStatusCode(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_INVALID_VOUCHER);
        return response;
    }
}
