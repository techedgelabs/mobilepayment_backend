package test.com.techedge.mp.frontendbo.adapter.entities.admin.testconsumevoucher;

import java.util.List;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.ConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockConsumeVoucherFidelityServiceSuccess extends MockFidelityService {

    @Override
    public ConsumeVoucherResult consumeVoucher(String operationID, String mpTransactionID, VoucherConsumerType voucherType, String stationID, String refuelMode,
            String paymentMode, String language, PartnerType partnerType, Long requestTimestamp, List<ProductDetail> productList, List<VoucherCodeDetail> voucherCodeList, String consumeType, String preAuthOperationID)
            throws FidelityServiceException {
        ConsumeVoucherResult data = new ConsumeVoucherResult();
        data.setStatusCode(StatusCode.ADMIN_TEST_CONSUME_VOUCHER_SUCCESS);
        return data;
    }
}
