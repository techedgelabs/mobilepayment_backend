package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletemanager;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.deletemanager.DeleteManagerBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletemanager.DeleteManagerDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class DeleteManagerBodyRequestTest extends BaseTestCase {
    private DeleteManagerBodyRequest body;
    private DeleteManagerDataRequest data;
    private Long                     id;

    // assigning the values
    protected void setUp() {
        body = new DeleteManagerBodyRequest();
        data = new DeleteManagerDataRequest();
        id = 1L;
        data.setId(id);
        body.setManagerData(data);

    }

    @Test
    public void testBodyNull() {
        body.setManagerData(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testIdNull() {
        data.setId(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }
}
