package test.com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey.GetSurveyBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey.GetSurveyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class GetSurveyRequestTest extends BaseTestCase {
    private GetSurveyRequest     request;
    private GetSurveyBodyRequest body;
    private String               code;
    private Long                 startSearch;
    private Long                 endSearch;
    private Integer              status;

    // assigning the values
    protected void setUp() {
        request = new GetSurveyRequest();
        body = new GetSurveyBodyRequest();
        code = "001";
        startSearch = 0L;
        endSearch = 1L;
        status = 4;
        body.setCode(code);
        body.setEndSearch(endSearch);
        body.setStartSearch(startSearch);
        body.setStatus(status);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_SURVEY_GET_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
