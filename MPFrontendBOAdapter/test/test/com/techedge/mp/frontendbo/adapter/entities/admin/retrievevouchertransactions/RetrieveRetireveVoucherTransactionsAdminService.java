package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions.RetrieveVoucherTransactionsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions.RetrieveVoucherTransactionsRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions.RetrieveVoucherTransactionsResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class RetrieveRetireveVoucherTransactionsAdminService extends BaseTestCase {
    private FrontendBOAdapterService               frontend;
    private RetrieveVoucherTransactionsRequest     request;
    private RetrieveVoucherTransactionsRequestBody body;

    private RetrieveVoucherTransactionsResponse    response;
    private Response                               baseResponse;
    private String                                 json;
    private Gson                                   gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest                           adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new RetrieveVoucherTransactionsRequest();
        body = new RetrieveVoucherTransactionsRequestBody();
        body.setCreationTimestampEnd(1L);
        body.setCreationTimestampStart(2L);
        body.setFinalStatusType("OK");
        body.setUserId(1L);
        body.setVoucherCode("code");
        body.setVoucherTransactionId("transacttransacttransacttransact");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
        adminRequest = new AdminRequest();
        adminRequest.setRetrieveVoucherTransactions(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveVoucherTransactionsAdminServiceSuccess());
        baseResponse = frontend.adminJsonHandler(json);
        response = (RetrieveVoucherTransactionsResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIEVE_VOUCHER_TRANSACTIONS_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveVoucherTransactionsAdminServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (RetrieveVoucherTransactionsResponse) baseResponse.getEntity();
        assertEquals(StatusCode.RETRIEVE_VOUCHER_TRANSACTIONS_FAILURE, response.getStatus().getStatusCode());
    }
}
