package test.com.techedge.mp.frontendbo.adapter.entities.admin.createusertype;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createusertype.CreateUserTypeBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddUserTypeBodyRequestTest extends BaseTestCase {

    private CreateUserTypeBodyRequest body;
    private Integer                   code;

    // assigning the values
    protected void setUp() {
        body = new CreateUserTypeBodyRequest();
        code = 1;
        body.setCode(code);

    }

    @Test
    public void testNameNull() {
        body.setCode(null);
        assertEquals(StatusCode.ADMIN_USER_TYPE_CREATE_FAILURE, body.check().getStatusCode());
    }

}