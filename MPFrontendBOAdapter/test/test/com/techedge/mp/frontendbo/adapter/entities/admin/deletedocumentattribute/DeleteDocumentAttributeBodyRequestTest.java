package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletedocumentattribute;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocumentattribute.AdminDeleteDocumentAttributeBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class DeleteDocumentAttributeBodyRequestTest extends BaseTestCase {

    private AdminDeleteDocumentAttributeBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new AdminDeleteDocumentAttributeBodyRequest();
        body.setCheckKey("document");

    }

    @Test
    public void testCheckKeyNull() {
        body.setCheckKey(null);
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_DELETE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testCheckKeyIsEmpty() {
        body.setCheckKey("");
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_DELETE_CHECK_FAILURE, body.check().getStatusCode());
    }

}