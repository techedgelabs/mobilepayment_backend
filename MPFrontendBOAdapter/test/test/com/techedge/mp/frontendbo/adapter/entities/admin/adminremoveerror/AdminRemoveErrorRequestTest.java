package test.com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror.AdminRemoveErrorBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror.AdminRemoveErrorRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AdminRemoveErrorRequestTest extends BaseTestCase {

    private AdminRemoveErrorRequest     request;
    private AdminRemoveErrorBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminRemoveErrorRequest();
        body = new AdminRemoveErrorBodyRequest();
        body.setGpErrorCode("001");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_REMOVE__SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}