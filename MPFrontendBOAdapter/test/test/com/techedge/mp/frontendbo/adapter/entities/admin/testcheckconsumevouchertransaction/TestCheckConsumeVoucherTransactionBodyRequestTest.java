package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcheckconsumevouchertransaction;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcheckconsumevouchertransaction.TestCheckConsumeVoucherTransactionBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestCheckConsumeVoucherTransactionBodyRequestTest extends BaseTestCase {
    private TestCheckConsumeVoucherTransactionBodyRequest body;
    private String                                        operationID;
    private String                                        operationIDtoCheck;
    private PartnerType                                   partnerType;
    private Long                                          requestTimestamp;

    protected void setUp() {

        body = new TestCheckConsumeVoucherTransactionBodyRequest();
        operationID = "operation";
        operationIDtoCheck = "check";
        partnerType = PartnerType.MP;
        requestTimestamp = 1010101L;

        body.setOperationID(operationID);
        body.setOperationIDtoCheck(operationIDtoCheck);
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);

    }

    @Test
    public void testOperationNull() {

        body.setOperationID(null);
        assertEquals(StatusCode.ADMIN_TEST_FIDELITY_INVALID_PARAMETERS, body.check().getStatusCode());

    }

    @Test
    public void testIOperationIDToCheckNull() {

        body.setOperationIDtoCheck(null);
        assertEquals(StatusCode.ADMIN_TEST_FIDELITY_INVALID_PARAMETERS, body.check().getStatusCode());

    }

    @Test
    public void testPartnerTypeNull() {

        body.setPartnerType(null);
        assertEquals(StatusCode.ADMIN_TEST_FIDELITY_INVALID_PARAMETERS, body.check().getStatusCode());

    }

    @Test
    public void testRequestTimestampNull() {

        body.setRequestTimestamp(null);
        assertEquals(StatusCode.ADMIN_TEST_FIDELITY_INVALID_PARAMETERS, body.check().getStatusCode());

    }

}
