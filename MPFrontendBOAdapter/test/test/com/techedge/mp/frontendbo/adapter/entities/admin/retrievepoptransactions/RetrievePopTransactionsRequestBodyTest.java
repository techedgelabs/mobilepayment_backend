package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions.RetrievePoPTransactionsRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrievePopTransactionsRequestBodyTest extends BaseTestCase {

    private RetrievePoPTransactionsRequestBody body;
    private String                             mpTransactionId;
    private String                             userId;
    private String                             stationId;
    private String                             sourceId;
    private String                             mpTransactionStatus;
    private Boolean                            notificationCreated;
    private Boolean                            notificationPaid;
    private Boolean                            notificationUser;
    private Long                               creationTimestampStart;
    private Long                               creationTimestampEnd;
    private String                             source;
    private Boolean                            mpTransactionHistoryFlag;

    // assigning the values
    protected void setUp() {
        body = new RetrievePoPTransactionsRequestBody();
        userId = "user";
        stationId = "stationID";
        source = "source";
        mpTransactionId = "transactionID";
        sourceId = "sourceID";
        mpTransactionStatus = "ONHOLD";
        notificationCreated = true;
        notificationPaid = true;
        notificationUser = true;
        creationTimestampEnd = 100L;
        creationTimestampStart = 99L;
        mpTransactionHistoryFlag = false;

        body.setCreationTimestampEnd(creationTimestampEnd);
        body.setCreationTimestampStart(creationTimestampStart);
        body.setMpTransactionHistoryFlag(mpTransactionHistoryFlag);
        body.setMpTransactionId(mpTransactionId);
        body.setMpTransactionStatus(mpTransactionStatus);
        body.setNotificationCreated(notificationCreated);
        body.setNotificationPaid(notificationPaid);
        body.setNotificationUser(notificationUser);
        body.setSource(source);
        body.setSourceId(sourceId);
        body.setStationId(stationId);
        body.setUserId(userId);
    }

    @Test
    public void testMPTransactionIdMajor() {
        body.setMpTransactionId("transactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionID");
        assertEquals(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testUserIdMajor() {
        body.setUserId("transactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionID");
        assertEquals(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testSourceIdMajor() {
        body.setSourceId("transactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionID");
        assertEquals(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testStationIdMajor() {
        body.setStationId("transactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionID");
        assertEquals(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS, body.check().getStatusCode());
    }
    
    @Test
    public void testSourceMajor() {
        body.setSource("transactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionID");
        assertEquals(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS, body.check().getStatusCode());
    }
    
    @Test
    public void testMPTransactionStatuseMajor() {
        body.setMpTransactionStatus("test");
        assertEquals(StatusCode.ADMIN_RETRIEVE_POP_TRANSACTION_ERROR_PARAMETERS, body.check().getStatusCode());
    }
}