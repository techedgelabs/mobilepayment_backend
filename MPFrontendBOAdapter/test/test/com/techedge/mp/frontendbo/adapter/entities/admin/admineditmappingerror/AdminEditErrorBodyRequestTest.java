package test.com.techedge.mp.frontendbo.adapter.entities.admin.admineditmappingerror;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.admineditmappingerror.AdminEditErrorBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AdminEditErrorBodyRequestTest extends BaseTestCase {

    private AdminEditErrorBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new AdminEditErrorBodyRequest();
        body.setGpErrorCode("001");
        body.setMpStatusCode("status");

    }

    @Test
    public void testNameNull() {
        body.setGpErrorCode(null);
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_EDIT__INVALID_ERROR_CODE, body.check().getStatusCode());
    }

}