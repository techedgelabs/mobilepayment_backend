package test.com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror;

import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.core.business.interfaces.AdminErrorResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockAdminGetErrorAdminServiceFailure extends MockAdminService {
    @Override
    public List<AdminErrorResponse> adminGetMappingError(String adminTicketId, String requestID, String mpStatusCode) {
        List<AdminErrorResponse> list = new ArrayList<AdminErrorResponse>(0);
        AdminErrorResponse item = new AdminErrorResponse();
        item.setGpErrorCode("ciao");
        item.setMpStatusCode(StatusCode.ADMIN_MAPPING_ERROR_GET__FAILURE);
        list.add(item);
        return list;
    }
}
