package test.com.techedge.mp.frontendbo.adapter.entities.admin.reconciliationdetail;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.reconciliationdetail.ReconciliationDetailResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class ReconciliationDetailRequestAdminService extends TestCase {
    private FrontendBOAdapterService     frontend;
    private Response                     response;
    private String                       json;
    private ReconciliationDetailResponse reconciliationDetailAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"reconciliationDetail\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"transactionId\":\"1\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testReconciliationDetailSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockReconciliationDetailRequestAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        reconciliationDetailAdminResponse = (ReconciliationDetailResponse) response.getEntity();
        assertEquals("RECONCILIATION_DETAIL_200", reconciliationDetailAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testReconciliationDetailFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockReconciliationDetailRequestAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        reconciliationDetailAdminResponse = (ReconciliationDetailResponse) response.getEntity();
        assertEquals("RECONCILIATION_DETAIL_300", reconciliationDetailAdminResponse.getStatus().getStatusCode());
    }
}
