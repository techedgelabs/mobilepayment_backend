package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateparams;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updateparams.UpdateParamsRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.ParameterInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateParamsBodyRequestTest extends BaseTestCase {
    private UpdateParamsRequestBody body;
    private List<ParameterInfo>     parameterList = new ArrayList<ParameterInfo>(0);

    //    private List<String>             params = new ArrayList<String>(0);

    protected void setUp() {

        body = new UpdateParamsRequestBody();
        body.setParameterList(parameterList);

    }

    @Test
    public void testListNull() {
        body.setParameterList(null);
        assertEquals(StatusCode.ADMIN_UPDATE_PARAMS_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testListIsEmpty() {
        body.setParameterList(new ArrayList<ParameterInfo>());
        assertEquals(StatusCode.ADMIN_UPDATE_PARAMS_FAILURE, body.check().getStatusCode());
    }

}
