package test.com.techedge.mp.frontendbo.adapter.entities.admin.testconsumevoucher;

import java.util.Date;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testconsumevoucher.TestConsumeVoucherBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testconsumevoucher.TestConsumeVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testconsumevoucher.TestConsumeVoucherResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestConsumeVoucherAdminService extends BaseTestCase {
    private FrontendBOAdapterService      frontend;
    private Response                      baseResponse;
    private String                        json;
    private Gson                          gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private TestConsumeVoucherRequest     request;
    private TestConsumeVoucherBodyRequest body;
    private TestConsumeVoucherResponse    response;
    private Credential                    credential;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        request = new TestConsumeVoucherRequest();
        body = new TestConsumeVoucherBodyRequest();
        credential = createCredentialTrue();
        request.setCredential(credential);
        body.setOperationID("56789");
        body.setPartnerType("MP");
        body.setRequestTimestamp(new Date().getTime());
        body.setVoucherType("ENI");
        body.setMpTransactionID("transaction");
        body.setConsumeType("consume");
        body.setStationID("station");
        body.setRefuelMode("refuel");
        body.setPaymentMode("payment");
        body.setPreAuthOperationID("id");
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());

    }

    @Test
    public void testTestCheckLoadLoyaltyCreditsTransactionRequestSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setFidelityService(new MockConsumeVoucherFidelityServiceSuccess());
        EJBHomeCache.getInstance().setAdminService(new MockConsumeVoucherAdminServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestConsumeVoucher(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (TestConsumeVoucherResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_TEST_CONSUME_VOUCHER_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testTestCheckLoadLoyaltyCreditsTransactionFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setFidelityService(new MockConsumeVoucherFidelityServiceFailure());
        EJBHomeCache.getInstance().setAdminService(new MockConsumeVoucherAdminServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestConsumeVoucher(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (TestConsumeVoucherResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_TEST_CONSUME_VOUCHER_INVALID_REQUEST, response.getStatus().getStatusCode());
    }
}
