package test.com.techedge.mp.frontendbo.adapter.webservices.common;

import java.math.BigDecimal;
import java.util.List;

import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CancelPreAuthorizationConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CreateVoucherPromotionalResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CreateVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.DeleteVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnableLoyaltyCardResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetLoyaltyCardListResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.InfoRedemptionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.LoadLoyaltyCreditsResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PreAuthorizationConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductType;
import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;

public class MockFidelityService implements FidelityServiceRemote {

    @Override
    public CheckConsumeVoucherTransactionResult checkConsumeVoucherTransaction(String operationID, String operationIDtoCheck, PartnerType partnerType, Long requestTimestamp)
            throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CheckLoadLoyaltyCreditsTransactionResult checkLoadLoyaltyCreditsTransaction(String operationID, String operationIDtoCheck, PartnerType partnerType, Long requestTimestamp)
            throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CheckVoucherResult checkVoucher(String operationID, VoucherConsumerType voucherType, PartnerType partnerType, Long requestTimestamp,
            List<VoucherCodeDetail> voucherCodeList) throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ConsumeVoucherResult consumeVoucher(String operationID, String mpTransactionID, VoucherConsumerType voucherType, String stationID, String refuelMode,
            String paymentMode, String language, PartnerType partnerType, Long requestTimestamp, List<ProductDetail> productList, List<VoucherCodeDetail> voucherCodeList,
            String consumeType, String preAuthOperationID) throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public EnableLoyaltyCardResult enableLoyaltyCard(String operationID, String fiscalCode, String panCode, Boolean enable, PartnerType partnerType, Long requestTimestamp)
            throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public GetLoyaltyCardListResult getLoyaltyCardList(String operationID, String fiscalCode, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public LoadLoyaltyCreditsResult loadLoyaltyCredits(String operationID, String mpTransactionID, String stationID, String panCode, String BIN, String refuelMode,
            String paymentMode, String language, PartnerType partnerType, Long requestTimestamp, String fiscalCode, List<ProductDetail> productList) throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ReverseConsumeVoucherTransactionResult reverseConsumeVoucherTransaction(String operationID, String operationIDtoReverse, PartnerType partnerType, Long requestTimestamp)
            throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ReverseLoadLoyaltyCreditsTransactionResult reverseLoadLoyaltyCreditsTransaction(String operationID, String operationIDtoReverse, PartnerType partnerType,
            Long requestTimestamp) throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public PreAuthorizationConsumeVoucherResult preAuthorizationConsumeVoucher(String operationID, String mpTransactionID, VoucherConsumerType voucherType, String stationID,
            Double amount, List<VoucherCodeDetail> voucherCodeList, PartnerType partnerType, Long requestTimestamp, String language, ProductType productType)
            throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CancelPreAuthorizationConsumeVoucherResult cancelPreAuthorizationConsumeVoucher(String operationID, String preAuthOperationIDToCancel, PartnerType partnerType,
            Long requestTimestamp) throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public DeleteVoucherResult deleteVoucher(String operationIDtoReverse, String operationID, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CreateVoucherResult createVoucher(VoucherConsumerType voucherType, BigDecimal totalAmount, String bankTransactionID, String shopTransactionID, String authorizationCode,
            String operationID, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public InfoRedemptionResult infoRedemption(String operationID, PartnerType partnerType, Long requestTimestamp, String fiscalCode) throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }
    
    @Override
    public RedemptionResult redemption(String operationID, PartnerType partnerType, Long requestTimestamp, String fiscalCode, Integer redemptionCode)
            throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public CreateVoucherPromotionalResult createVoucherPromotional(String operationID, VoucherConsumerType voucherType, PartnerType partnerType, Long requestTimestamp,
            String fiscalCode, String promoCode, BigDecimal totalAmount) throws FidelityServiceException {
        // TODO Auto-generated method stub
        return null;
    }
}
