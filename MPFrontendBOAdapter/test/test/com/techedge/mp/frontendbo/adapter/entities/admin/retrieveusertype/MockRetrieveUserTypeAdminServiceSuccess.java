package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusertype;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.user.UserTypeData;

public class MockRetrieveUserTypeAdminServiceSuccess extends MockAdminService {
    @Override
    public UserTypeData adminRetrieveUserType(String adminTicketId, String requestId, Integer code) {
        UserTypeData data = new UserTypeData();
        data.setStatusCode("ADMIN_USERTYPE_RETRIEVE_200");
        return data;
    }
}
