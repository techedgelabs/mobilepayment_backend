package test.com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror.AdminRemoveErrorBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AdminRemoveErrorBodyRequestTest extends BaseTestCase {

    private AdminRemoveErrorBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new AdminRemoveErrorBodyRequest();
        body.setGpErrorCode("001");

    }

    @Test
    public void testeRRORNull() {
        body.setGpErrorCode(null);
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_REMOVE__INVALID_ERROR_CODE, body.check().getStatusCode());
    }

}