package test.com.techedge.mp.frontendbo.adapter.entities.admin.testloadloyaltycredits;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testloadloyaltycredits.TestLoadLoyaltyCreditsBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.ProductInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestLoadLoyaltyCreditsBodyRequestTest extends BaseTestCase {
    private TestLoadLoyaltyCreditsBodyRequest body;
    private String                            operationID;
    private String                            mpTransactionID;
    private String                            stationID;
    private String                            panCode;
    private String                            BIN;
    private String                            refuelMode;
    private String                            paymentMode;
    private PartnerType                       partnerType;
    private Long                              requestTimestamp;
    List<ProductInfo>                         productList = new ArrayList<ProductInfo>(0);

    protected void setUp() {

        body = new TestLoadLoyaltyCreditsBodyRequest();
        operationID = "operation";
        mpTransactionID = "check";
        stationID = "panCode";
        panCode = "pan";
        BIN = "bin";
        refuelMode = "refuel";
        paymentMode = "mode";
        partnerType = PartnerType.MP;
        requestTimestamp = 1010101L;

        body.setOperationID(operationID);
        body.setMpTransactionID(mpTransactionID);
        body.setStationID(stationID);
        body.setPanCode(panCode);
        body.setBIN(BIN);
        body.setRefuelMode(refuelMode);
        body.setPaymentMode(paymentMode);
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);

    }

    @Test
    public void testOperationNull() {

        body.setOperationID(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testMPTransacionNull() {

        body.setMpTransactionID(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testStationIDNull() {

        body.setStationID(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testPanCodeNull() {

        body.setPanCode(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testBINNull() {

        body.setBIN(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testPartnerTypeNull() {

        body.setPartnerType(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testPaymentNull() {

        body.setPaymentMode(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testRequestTimestampNull() {

        body.setRequestTimestamp(null);
        assertEquals(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

}
