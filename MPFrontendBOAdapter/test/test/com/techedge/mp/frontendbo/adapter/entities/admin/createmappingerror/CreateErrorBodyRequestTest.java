package test.com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror.AdminCreateErrorBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class CreateErrorBodyRequestTest extends BaseTestCase {

    private AdminCreateErrorBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new AdminCreateErrorBodyRequest();
        body.setErrorCode("error");
        body.setStatusCode("error");

    }

    @Test
    public void testErrorNull() {
        body.setErrorCode(null);
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_CREATE__INVALID_CODE, body.check().getStatusCode());
    }

}