package test.com.techedge.mp.frontendbo.adapter.entities.admin.testreverseloadloyaltycreditstransaction;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testreverseloadloyaltycreditstransaction.TestReverseLoadLoyaltyCreditsTransactionBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestReverseLoadLoyaltyCreditsTransactionBodyRequestTest extends BaseTestCase {
    private TestReverseLoadLoyaltyCreditsTransactionBodyRequest body;
    private String                                              operationID;
    private String                                              operationIDtoReverse;
    private Long                                                requestTimestamp;

    protected void setUp() {

        body = new TestReverseLoadLoyaltyCreditsTransactionBodyRequest();

        operationID = "id";
        operationIDtoReverse = "request";
        requestTimestamp = 10101010L;

        body.setOperationID(operationID);
        body.setOperationIDtoReverse(operationIDtoReverse);
        body.setPartnerType(PartnerType.MP.getValue());
        body.setRequestTimestamp(requestTimestamp);
    }

    @Test
    public void testOperationIDNull() {

        body.setOperationID(null);
        assertEquals(StatusCode.ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testOperationReverseNull() {

        body.setOperationIDtoReverse(null);
        assertEquals(StatusCode.ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testPartnerNull() {

        body.setPartnerType(null);
        assertEquals(StatusCode.ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testRequestTimestampNull() {

        body.setRequestTimestamp(null);
        assertEquals(StatusCode.ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_INVALID_REQUEST, body.check().getStatusCode());

    }

}
