package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletesurveyquestion;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.deletesurveyquestion.DeleteSurveyQuestionBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletesurveyquestion.DeleteSurveyQuestionRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class DeleteSurveyQuestionRequestTest extends BaseTestCase {
    private DeleteSurveyQuestionRequest     request;
    private DeleteSurveyQuestionBodyRequest body;
    private String                          code;

    // assigning the values
    protected void setUp() {
        request = new DeleteSurveyQuestionRequest();
        body = new DeleteSurveyQuestionBodyRequest();
        code = "test";
        body.setCode(code);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_SURVEY_QUESTION_ADD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
