package test.com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion.AddVoucherToPromotionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion.AddVoucherToPromotionRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddVoucherPromotionRequestTest extends BaseTestCase {

    private AddVoucherToPromotionRequest     request;
    private AddVoucherToPromotionRequestBody body;
    private String                           promotionCode;
    private PromoVoucherInput                voucherInfo;

    // assigning the values
    protected void setUp() {
        request = new AddVoucherToPromotionRequest();
        body = new AddVoucherToPromotionRequestBody();
        promotionCode = "promo";
        voucherInfo = new PromoVoucherInput();
        voucherInfo.getPromoAssociation().put("test", 1.1);
        body.setPromotionCode(promotionCode);
        body.setVoucherInfo(voucherInfo);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    //    @Test
    //    public void testBodyNull() {
    //
    //        request.setCredential(createCredentialTrue());
    //        request.setBody(null);
    //
    //        assertEquals(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE, request.check().getStatusCode());
    //    }
}
