package test.com.techedge.mp.frontendbo.adapter.entities.admin.removeprefixnumber;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.ResponseHelper;

public class MockRemovePrefixNumberAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminRemovePrefixNumber(String adminTicketId, String requestId, String code) {
        // TODO Auto-generated method stub
        return ResponseHelper.PREFIX_REMOVE_SUCCESS;
    }
}
