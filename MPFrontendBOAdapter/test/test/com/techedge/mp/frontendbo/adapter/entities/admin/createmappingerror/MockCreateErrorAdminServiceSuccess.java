package test.com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockCreateErrorAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminCreateMappingError(String adminTicketId, String requestID, String gpErrorCode, String mpStatusCode) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_MAPPING_ERROR_CREATE__SUCCESS;
    }
}
