package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethod;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethod.UpdatePaymentMethodResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class UpdatePaymentAdminService extends TestCase {
    private FrontendBOAdapterService    frontend;
    private Response                    response;
    private String                      json;
    private UpdatePaymentMethodResponse updatePaymentMethodAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"updatePaymentMethod\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"id\":\"1\",\"type\":\"06300200100\",\"status\":\"2\",\"defaultMethod\":\"true\",\"attemptsLeft\":\"1\",\"checkAmount\":\"0.1\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockUpdatePaymentAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        updatePaymentMethodAdminResponse = (UpdatePaymentMethodResponse) response.getEntity();
        assertEquals("ADMIN_UPDATE_PAYMENT_METHOD_200", updatePaymentMethodAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockUpdatePaymentAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        updatePaymentMethodAdminResponse = (UpdatePaymentMethodResponse) response.getEntity();
        assertEquals("ADMIN_UPDATE_PAYMENT_METHOD_300", updatePaymentMethodAdminResponse.getStatus().getStatusCode());
    }
}
