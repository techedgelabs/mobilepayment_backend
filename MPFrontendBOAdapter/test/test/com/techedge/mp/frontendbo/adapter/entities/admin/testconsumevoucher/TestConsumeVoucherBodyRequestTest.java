package test.com.techedge.mp.frontendbo.adapter.entities.admin.testconsumevoucher;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcheckvoucher.TestCheckVoucherBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestConsumeVoucherBodyRequestTest extends BaseTestCase {
    private TestCheckVoucherBodyRequest body;
    private String                      operationID;
    private VoucherConsumerType         voucherType;
    private PartnerType                 partnerType;
    private Long                        requestTimestamp;

    protected void setUp() {

        body = new TestCheckVoucherBodyRequest();
        operationID = "operation";
        voucherType = VoucherConsumerType.ENI;
        partnerType = PartnerType.MP;
        requestTimestamp = 1010101L;

        body.setOperationID(operationID);
        body.setVoucherType(voucherType.getValue());
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);

    }

    @Test
    public void testOperationNull() {

        body.setOperationID(null);
        assertEquals(StatusCode.ADMIN_TEST_CHECK_VOUCHER_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testIOperationIDToCheckNull() {

        body.setVoucherType(null);
        assertEquals(StatusCode.ADMIN_TEST_CHECK_VOUCHER_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testPartnerTypeNull() {

        body.setPartnerType(null);
        assertEquals(StatusCode.ADMIN_TEST_CHECK_VOUCHER_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testRequestTimestampNull() {

        body.setRequestTimestamp(null);
        assertEquals(StatusCode.ADMIN_TEST_CHECK_VOUCHER_INVALID_REQUEST, body.check().getStatusCode());

    }

}
