package test.com.techedge.mp.frontendbo.adapter.entities.admin.generatetesters;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.GenerateTestersData;

public class GenerateTestersAdminServiceSuccess extends MockAdminService {
    @Override
    public GenerateTestersData adminTestersGenerate(String adminTicketId, String requestId, String emailPrefix, String emailsuffix, Integer startNumber, Integer userCount,
            String token, Integer userType) {
        GenerateTestersData data = new GenerateTestersData();
        data.setStatusCode("ADMIN_GENERATE_TESTER_USER_200");
        return data;
    }
}
