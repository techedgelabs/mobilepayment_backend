package test.com.techedge.mp.frontendbo.adapter.entities.admin.removeusertypetousercategory;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.removeusertypefromusercategory.RemoveUserTypeFromUserCategoryBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.removeusertypefromusercategory.RemoveUserTypeFromUserCategoryRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RemoveUserTypeToUserCategoryRequestTest extends BaseTestCase {
    private RemoveUserTypeFromUserCategoryRequest     request;
    private RemoveUserTypeFromUserCategoryBodyRequest body;
    private Integer                                   code;
    private String                                    name;

    // assigning the values
    protected void setUp() {
        request = new RemoveUserTypeFromUserCategoryRequest();
        body = new RemoveUserTypeFromUserCategoryBodyRequest();
        code = 100;
        name = "test";
        body.setCode(code);
        body.setName(name);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE, request.check().getStatusCode());
    }
}
