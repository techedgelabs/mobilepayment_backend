package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateparams;

import java.util.List;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.AdminUpdateParamsResponse;
import com.techedge.mp.core.business.interfaces.ParamInfo;

public class MockUpdateParamsAdminServiceSuccess extends MockAdminService {

    @Override
    public AdminUpdateParamsResponse adminUpdateParams(String adminTicketId, String requestId, List<ParamInfo> params) {
        AdminUpdateParamsResponse data = new AdminUpdateParamsResponse();
        data.setStatusCode("ADMIN_UPDATE_PARAMS_200");
        return data;
    }
}
