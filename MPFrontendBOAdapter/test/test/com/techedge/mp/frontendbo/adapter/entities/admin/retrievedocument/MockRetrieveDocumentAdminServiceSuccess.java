package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievedocument;

import java.util.ArrayList;
import java.util.List;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.PrefixNumberResult;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockRetrieveDocumentAdminServiceSuccess extends MockAdminService {

    @Override
    public PrefixNumberResult adminRetrieveAllPrefixNumber(String adminTicketId, String requestId) {
        PrefixNumberResult result = new PrefixNumberResult();
        result.setStatusCode(StatusCode.PREFIX_RETRIEVE_SUCCESS);
        List<String> listPrefix = new ArrayList<String>(0);
        listPrefix.add("06");
        result.setListPrefix(listPrefix);
        return result;
    }
}
