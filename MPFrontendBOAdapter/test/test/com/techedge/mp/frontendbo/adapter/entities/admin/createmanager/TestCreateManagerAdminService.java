package test.com.techedge.mp.frontendbo.adapter.entities.admin.createmanager;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.createmanager.CreateManagerResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestCreateManagerAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private CreateManagerResponse    createManagerResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"createManager\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-flkjkfskjcvsdasas\"},\"body\":{\"managerData\":{\"firstName\":\"Augusto\",\"lastName\":\"Montoni\",\"username\":\"amontoni\",\"email\":\"montoni.augusto@gmail.com\",\"password\":\"1283ssjsyehccnff\"}}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
    }

    @Test
    public void testCreateCustomerUserSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateManagerAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        createManagerResponse = (CreateManagerResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_MANAGER_200", createManagerResponse.getStatus().getStatusCode());
    }

    @Test
    public void testCreateCustomerUserFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateManagerAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        createManagerResponse = (CreateManagerResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_MANAGER_300", createManagerResponse.getStatus().getStatusCode());
    }
}
