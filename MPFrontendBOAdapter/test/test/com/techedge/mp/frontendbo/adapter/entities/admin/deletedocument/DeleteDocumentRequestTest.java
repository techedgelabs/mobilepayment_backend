package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument.AdminDeleteDocumentBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument.AdminDeleteDocumentRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class DeleteDocumentRequestTest extends BaseTestCase {

    private AdminDeleteDocumentRequest     request;
    private AdminDeleteDocumentBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminDeleteDocumentRequest();
        body = new AdminDeleteDocumentBodyRequest();
        body.setDocumentKey("document");
        request.setBody(body);
        request.setCredential(createCredentialTrue());

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_DOCUMENT_DELETE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_DOCUMENT_DELETE_FAILURE, request.check().getStatusCode());
    }

}