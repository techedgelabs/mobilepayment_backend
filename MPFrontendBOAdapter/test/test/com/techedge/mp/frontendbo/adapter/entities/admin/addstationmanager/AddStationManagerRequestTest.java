package test.com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager.AddStationManagerBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager.AddStationManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddStationManagerRequestTest extends BaseTestCase {

    private AddStationManagerRequest     request;
    private AddStationManagerBodyRequest body;
    private Long                         managerID;
    private Long                         stationID;

    // assigning the values
    protected void setUp() {
        request = new AddStationManagerRequest();
        body = new AddStationManagerBodyRequest();
    }

    @Test
    public void testCredentialTrue() {

        managerID = 1L;
        stationID = 1L;
        body.setManagerID(managerID);
        body.setStationID(stationID);
        request.setCredential(createCredentialTrue());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_ADDSTATION_MANAGER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        managerID = 1L;
        stationID = 1L;
        body.setManagerID(managerID);
        body.setStationID(stationID);
        request.setCredential(createCredentialFalse());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        managerID = 1L;
        stationID = 1L;
        body.setManagerID(managerID);
        body.setStationID(stationID);
        request.setCredential(createCredentialTicketFalse());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        managerID = 1L;
        stationID = 1L;
        body.setManagerID(managerID);
        body.setStationID(stationID);
        request.setCredential(createCredentialRequestFalse());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testCredentialBodyNull() {

        request.setCredential(createCredentialRequestFalse());
        request.setBody(null);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}