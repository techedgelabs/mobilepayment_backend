package test.com.techedge.mp.frontendbo.adapter.entities.admin.createmanager;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.Manager;

public class MockCreateManagerAdminServiceFailure extends MockAdminService {
    @Override
    public String adminCreateManager(String adminTicketId, String requestId, Manager manager) {
        // TODO Auto-generated method stub
        return "ADMIN_CREATE_MANAGER_300";
    }
}
