package test.com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion.AddSurveyQuestionBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion.AddSurveyQuestionRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.SurveyQuestion;

public class AddSurveyQuestionRequestTest extends BaseTestCase {

    private AddSurveyQuestionRequest     request;
    private AddSurveyQuestionBodyRequest body;
    private String                       code;
    private SurveyQuestion               question;

    // assigning the values
    protected void setUp() {
        request = new AddSurveyQuestionRequest();
        body = new AddSurveyQuestionBodyRequest();
        question = new SurveyQuestion();
    }

    @Test
    public void testCredentialTrue() {

        code = "001";
        body.setCode(code);
        body.setQuestion(question);
        request.setCredential(createCredentialTrue());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_SURVEY_QUESTION_ADD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        code = "001";
        body.setCode(code);
        body.setQuestion(question);
        request.setCredential(createCredentialFalse());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        code = "001";
        body.setCode(code);
        body.setQuestion(question);
        request.setCredential(createCredentialTicketFalse());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        code = "001";
        body.setCode(code);
        body.setQuestion(question);
        request.setCredential(createCredentialRequestFalse());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
