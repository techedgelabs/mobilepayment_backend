package test.com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher;

import java.util.ArrayList;
import java.util.Date;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher.TestPreAuthorizationConsumeVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher.TestPreAuthorizationConsumeVoucherRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class PreAuthorizationConsumeVoucherRequestTest extends BaseTestCase {

    private TestPreAuthorizationConsumeVoucherRequest     request;
    private TestPreAuthorizationConsumeVoucherRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new TestPreAuthorizationConsumeVoucherRequest();
        body = new TestPreAuthorizationConsumeVoucherRequestBody();

        body.setAmount(10.05);
        body.setMpTransactionID("1234");
        body.setOperationID("56789");
        body.setPartnerType(PartnerType.MP.getValue());
        body.setProductType(ProductType.OIL.getValue());
        body.setRequestTimestamp(new Date().getTime());
        body.setStationID("111111");
        body.setVoucherConsumerType(VoucherConsumerType.ENI.getValue());
        ArrayList<VoucherCodeDetail> voucherList = new ArrayList<VoucherCodeDetail>();
        VoucherCodeDetail voucherCode = new VoucherCodeDetail();
        voucherCode.setVoucherCode("00000000000");
        voucherList.add(voucherCode);
        body.setVoucherList(voucherList);
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        request.setBody(body);

    }

    @Test
    public void testBodyValid() {

        assertEquals(StatusCode.ADMIN_TEST_PRE_AUTHORIZATION_CONSUME_VOUCHER_SUCCESS, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
