package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions.RetrieveVoucherTransactionsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions.RetrieveVoucherTransactionsRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveVoucherTransactionsRequestTest extends BaseTestCase {
    private RetrieveVoucherTransactionsRequest     request;
    private RetrieveVoucherTransactionsRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveVoucherTransactionsRequest();
        body = new RetrieveVoucherTransactionsRequestBody();
        body.setCreationTimestampEnd(1L);
        body.setCreationTimestampStart(2L);
        body.setFinalStatusType("OK");
        body.setUserId(1L);
        body.setVoucherCode("code");
        body.setVoucherTransactionId("transacttransacttransacttransact");
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.RETRIEVE_VOUCHER_TRANSACTIONS_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}