package test.com.techedge.mp.frontendbo.adapter.entities.admin.createusercategory;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockCreateUserCategoryAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminCreateUserCategory(String adminTicketId, String requestId, String name) {
        // TODO Auto-generated method stub
        return "ADMIN_USERCATEGORY_CREATE_200";
    }
}
