package test.com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation.StationOpResponse;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationsoperation.StationsOpResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class StationsAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private StationOpResponse        stationOpResponse;
    private StationsOpResponse       stationsOpResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testAddStationSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationsAdminServiceSuccess());
        json = "{\"stationAdd\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"station\":{\"id\":\"1\",\"stationID\":\"cdernfjriwoego1o\",\"beaconCode\":\"test\",\"address\":\"Via Salvatore Quasimodo, 2\",\"city\":\"Roma\",\"country\":\"ITA\",\"province\":\"RM\",\"latitude\":\"1.2\",\"longitude\":\"1.4\",\"oilShopLogin\":\"test\",\"oilAcquirerID\":\"testA\",\"noOilShopLogin\":\"testN\",\"noOilAcquirerID\":\"testB\",\"stationStatus\":\"1\",\"prepaidActive\":\"1\",\"postpaidActive\":\"1\",\"shopActive\":\"1\"}}}}";
        response = frontend.adminJsonHandler(json);
        stationOpResponse = (StationOpResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_200", stationOpResponse.getStatus().getStatusCode());
    }

    @Test
    public void testAddStationFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationsAdminServiceFailure());
        json = "{\"stationAdd\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"station\":{\"id\":\"1\",\"stationID\":\"cdernfjriwoego1o\",\"beaconCode\":\"test\",\"address\":\"Via Salvatore Quasimodo, 2\",\"city\":\"Roma\",\"country\":\"ITA\",\"province\":\"RM\",\"latitude\":\"1.2\",\"longitude\":\"1.4\",\"oilShopLogin\":\"test\",\"oilAcquirerID\":\"testA\",\"noOilShopLogin\":\"testN\",\"noOilAcquirerID\":\"testB\",\"stationStatus\":\"1\",\"prepaidActive\":\"1\",\"postpaidActive\":\"1\",\"shopActive\":\"1\"}}}}";
        response = frontend.adminJsonHandler(json);
        stationOpResponse = (StationOpResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_300", stationOpResponse.getStatus().getStatusCode());
    }

    @Test
    public void testDeleteStationSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationsAdminServiceSuccess());
        json = "{\"stationDelete\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"station\":{\"id\":\"1\",\"stationID\":\"cdernfjriwoego1o\",\"beaconCode\":\"test\",\"address\":\"Via Salvatore Quasimodo, 2\",\"city\":\"Roma\",\"country\":\"ITA\",\"province\":\"RM\",\"latitude\":\"1.2\",\"longitude\":\"1.4\",\"oilShopLogin\":\"test\",\"oilAcquirerID\":\"testA\",\"noOilShopLogin\":\"testN\",\"noOilAcquirerID\":\"testB\",\"stationStatus\":\"1\",\"prepaidActive\":\"1\",\"postpaidActive\":\"1\",\"shopActive\":\"1\"}}}}";
        response = frontend.adminJsonHandler(json);
        stationOpResponse = (StationOpResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_200", stationOpResponse.getStatus().getStatusCode());
    }

    @Test
    public void testDeleteStationFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationsAdminServiceFailure());
        json = "{\"stationDelete\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"station\":{\"id\":\"1\",\"stationID\":\"cdernfjriwoego1o\",\"beaconCode\":\"test\",\"address\":\"Via Salvatore Quasimodo, 2\",\"city\":\"Roma\",\"country\":\"ITA\",\"province\":\"RM\",\"latitude\":\"1.2\",\"longitude\":\"1.4\",\"oilShopLogin\":\"test\",\"oilAcquirerID\":\"testA\",\"noOilShopLogin\":\"testN\",\"noOilAcquirerID\":\"testB\",\"stationStatus\":\"1\",\"prepaidActive\":\"1\",\"postpaidActive\":\"1\",\"shopActive\":\"1\"}}}}";
        response = frontend.adminJsonHandler(json);
        stationOpResponse = (StationOpResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_300", stationOpResponse.getStatus().getStatusCode());
    }

    @Test
    public void testUpdateStationSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationsAdminServiceSuccess());
        json = "{\"stationUpdate\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"station\":{\"id\":\"1\",\"stationID\":\"cdernfjriwoego1o\",\"beaconCode\":\"test\",\"address\":\"Via Salvatore Quasimodo, 2\",\"city\":\"Roma\",\"country\":\"ITA\",\"province\":\"RM\",\"latitude\":\"1.2\",\"longitude\":\"1.4\",\"oilShopLogin\":\"test\",\"oilAcquirerID\":\"testA\",\"noOilShopLogin\":\"testN\",\"noOilAcquirerID\":\"testB\",\"stationStatus\":\"1\",\"prepaidActive\":\"1\",\"postpaidActive\":\"1\",\"shopActive\":\"1\"}}}}";
        response = frontend.adminJsonHandler(json);
        stationOpResponse = (StationOpResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_200", stationOpResponse.getStatus().getStatusCode());
    }

    @Test
    public void testUpdateStationFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationsAdminServiceFailure());
        json = "{\"stationUpdate\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"station\":{\"id\":\"1\",\"stationID\":\"cdernfjriwoego1o\",\"beaconCode\":\"test\",\"address\":\"Via Salvatore Quasimodo, 2\",\"city\":\"Roma\",\"country\":\"ITA\",\"province\":\"RM\",\"latitude\":\"1.2\",\"longitude\":\"1.4\",\"oilShopLogin\":\"test\",\"oilAcquirerID\":\"testA\",\"noOilShopLogin\":\"testN\",\"noOilAcquirerID\":\"testB\",\"stationStatus\":\"1\",\"prepaidActive\":\"1\",\"postpaidActive\":\"1\",\"shopActive\":\"1\"}}}}";
        response = frontend.adminJsonHandler(json);
        stationOpResponse = (StationOpResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_300", stationOpResponse.getStatus().getStatusCode());
    }

    @Test
    public void testStationsSearchSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationsAdminServiceSuccess());
        json = "{\"stationsSearch\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"station\":{\"id\":\"1\",\"stationID\":\"cdernfjriwoego1o\",\"beaconCode\":\"test\",\"address\":\"Via Salvatore Quasimodo, 2\",\"city\":\"Roma\",\"country\":\"ITA\",\"province\":\"RM\",\"latitude\":\"1.2\",\"longitude\":\"1.4\",\"oilShopLogin\":\"test\",\"oilAcquirerID\":\"testA\",\"noOilShopLogin\":\"testN\",\"noOilAcquirerID\":\"testB\",\"stationStatus\":\"1\",\"prepaidActive\":\"1\",\"postpaidActive\":\"1\",\"shopActive\":\"1\"}}}}";
        response = frontend.adminJsonHandler(json);
        stationsOpResponse = (StationsOpResponse) response.getEntity();
        assertEquals("ADMIN_STATION_200", stationsOpResponse.getStatus().getStatusCode());
    }

    @Test
    public void testStationsSearchFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockStationsAdminServiceFailure());
        json = "{\"stationsSearch\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"station\":{\"id\":\"1\",\"stationID\":\"cdernfjriwoego1o\",\"beaconCode\":\"test\",\"address\":\"Via Salvatore Quasimodo, 2\",\"city\":\"Roma\",\"country\":\"ITA\",\"province\":\"RM\",\"latitude\":\"1.2\",\"longitude\":\"1.4\",\"oilShopLogin\":\"test\",\"oilAcquirerID\":\"testA\",\"noOilShopLogin\":\"testN\",\"noOilAcquirerID\":\"testB\",\"stationStatus\":\"1\",\"prepaidActive\":\"1\",\"postpaidActive\":\"1\",\"shopActive\":\"1\"}}}}";
        response = frontend.adminJsonHandler(json);
        stationsOpResponse = (StationsOpResponse) response.getEntity();
        assertEquals("ADMIN_STATION_300", stationsOpResponse.getStatus().getStatusCode());
    }
}
