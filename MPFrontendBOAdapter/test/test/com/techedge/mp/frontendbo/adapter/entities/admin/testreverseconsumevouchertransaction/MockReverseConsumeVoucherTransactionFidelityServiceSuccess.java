package test.com.techedge.mp.frontendbo.adapter.entities.admin.testreverseconsumevouchertransaction;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseConsumeVoucherTransactionResult;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockReverseConsumeVoucherTransactionFidelityServiceSuccess extends MockFidelityService {

    @Override
    public ReverseConsumeVoucherTransactionResult reverseConsumeVoucherTransaction(String operationID, String operationIDtoReverse, PartnerType partnerType, Long requestTimestamp)
            throws FidelityServiceException {
        ReverseConsumeVoucherTransactionResult data = new ReverseConsumeVoucherTransactionResult();
        data.setStatusCode(StatusCode.ADMIN_TEST_REVERSE_CONSUME_VOUCHER_SUCCESS);
        return data;
    }
}
