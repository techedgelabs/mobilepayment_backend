package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletedocumentattribute;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockDeleteDocumentAttributeAdminServiceSuccess extends MockAdminService {

    @Override
    public String adminDeleteDocumentAttribute(String adminTicketId, String requestID, String documentKey) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_DELETE_SUCCESS;
    }
}
