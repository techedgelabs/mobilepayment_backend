package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument.AdminDeleteDocumentBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class DeleteDocumentBodyRequestTest extends BaseTestCase {

    private AdminDeleteDocumentBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new AdminDeleteDocumentBodyRequest();
        body.setDocumentKey("document");

    }

    @Test
    public void testDocumentNull() {
        body.setDocumentKey(null);
        assertEquals(StatusCode.ADMIN_DOCUMENT_DELETE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testDocumentIsEmpty() {
        body.setDocumentKey("");
        assertEquals(StatusCode.ADMIN_DOCUMENT_DELETE_CHECK_FAILURE, body.check().getStatusCode());
    }

}