package test.com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockCreateDocumentAdminAttributeServiceSuccess extends MockAdminService {

    @Override
    public String adminCreateDocumentAttribute(String adminTicketId, String requestID, String checkKey, Boolean mandatory, Integer position, String conditionText,
            String extendedConditionText, String documentKey, String subtitle) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_SUCCESS;
    }
}
