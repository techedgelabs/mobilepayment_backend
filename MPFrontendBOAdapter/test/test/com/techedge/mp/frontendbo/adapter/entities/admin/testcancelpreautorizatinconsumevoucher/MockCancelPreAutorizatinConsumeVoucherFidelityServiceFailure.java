package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreautorizatinconsumevoucher;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CancelPreAuthorizationConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

public class MockCancelPreAutorizatinConsumeVoucherFidelityServiceFailure extends MockFidelityService {
    @Override
    public CancelPreAuthorizationConsumeVoucherResult cancelPreAuthorizationConsumeVoucher(String operationID, String preAuthOperationIDToCancel, PartnerType partnerType,
            Long requestTimestamp) throws FidelityServiceException {
        CancelPreAuthorizationConsumeVoucherResult response = new CancelPreAuthorizationConsumeVoucherResult();
        response.setCsTransactionID("src");
        response.setMessageCode("message");
        response.setStatusCode(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_GENERIC_ERROR);
        return response;
    }
}
