package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletedocumentattribute;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocumentattribute.AdminDeleteDocumentAttributeBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocumentattribute.AdminDeleteDocumentAttributeRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class DeleteDocumentAttributeRequestTest extends BaseTestCase {

    private AdminDeleteDocumentAttributeRequest     request;
    private AdminDeleteDocumentAttributeBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminDeleteDocumentAttributeRequest();
        body = new AdminDeleteDocumentAttributeBodyRequest();
        body.setCheckKey("document");
        request.setBody(body);
        request.setCredential(createCredentialTrue());

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_DELETE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_DELETE_FAILURE, request.check().getStatusCode());
    }

}