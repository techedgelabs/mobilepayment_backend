package test.com.techedge.mp.frontendbo.adapter.entities.admin.generatetesters;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.generatetesters.GenerateTestersDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.generatetesters.GenerateTestersRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.generatetesters.GenerateTestersRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class GenerateTestersRequestTest extends BaseTestCase {
    private GenerateTestersRequest     request;
    private GenerateTestersRequestBody body;
    private GenerateTestersDataRequest data;

    // assigning the values
    protected void setUp() {
        request = new GenerateTestersRequest();
        body = new GenerateTestersRequestBody();
        data = new GenerateTestersDataRequest();
        body.setCustomerUserData(data);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_CREATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testDataNull() {
        body.setCustomerUserData(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }
}