package test.com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod.FindPaymentMethodResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class FindPaymentMethodAdminService extends TestCase {
    private FrontendBOAdapterService  frontend;
    private Response                  response;
    private String                    json;
    private FindPaymentMethodResponse findPaymentMethodResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"findPaymentMethod\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"id\":\"001\",\"type\":\"text\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
    }

    @Test
    public void testCreateCustomerUserSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new FindPaymentMethodAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        findPaymentMethodResponse = (FindPaymentMethodResponse) response.getEntity();
        assertEquals("ADMIN_UPDATE_USER_200", findPaymentMethodResponse.getStatus().getStatusCode());
    }

    @Test
    public void testCreateCustomerUserFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new FindPaymentMethodAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        findPaymentMethodResponse = (FindPaymentMethodResponse) response.getEntity();
        assertEquals("ADMIN_UPDATE_USER_300", findPaymentMethodResponse.getStatus().getStatusCode());
    }
}
