package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions.RetrieveTransactionsRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveTransactionsRequestBodyTest extends BaseTestCase {

    private RetrieveTransactionsRequestBody body;
    private String                          transactionId;
    private String                          userId;
    private String                          stationId;
    private String                          pumpId;
    private String                          finalStatusType;
    private Boolean                         GFGNotification;
    private Boolean                         confirmed;
    private Long                            creationTimestampStart;
    private Long                            creationTimestampEnd;
    private String                          productID;
    private Boolean                         transactionHistoryFlag;

    // assigning the values
    protected void setUp() {
        body = new RetrieveTransactionsRequestBody();
        transactionId = "transactionID";
        userId = "user";
        stationId = "stationID";
        pumpId = "source";
        finalStatusType = "sourceID";
        GFGNotification = true;
        confirmed = true;
        creationTimestampEnd = 100L;
        creationTimestampStart = 99L;
        productID = "prodID";
        transactionHistoryFlag = false;

        body.setTransactionId(transactionId);
        body.setUserId(userId);
        body.setStationId(stationId);
        body.setPumpId(pumpId);
        body.setFinalStatusType(finalStatusType);
        body.setGFGNotification(GFGNotification);
        body.setConfirmed(confirmed);
        body.setCreationTimestampEnd(creationTimestampEnd);
        body.setCreationTimestampStart(creationTimestampStart);
        body.setProductID(productID);
        body.setTransactionHistoryFlag(transactionHistoryFlag);
    }

    @Test
    public void testTransactionIdMajor() {
        body.setTransactionId("transactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionID");
        assertEquals(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testUserIdMajor() {
        body.setUserId("transactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionID");
        assertEquals(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testSourceIdMajor() {
        body.setPumpId("transactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionID");
        assertEquals(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testStationIdMajor() {
        body.setStationId("transactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionID");
        assertEquals(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testSourceMajor() {
        body.setProductID("transactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionIDtransactionID");
        assertEquals(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testMPTransactionStatuseMajor() {
        body.setFinalStatusType("test");
        assertEquals(StatusCode.ADMIN_RETRIEVE_USERS_ERROR_PARAMETERS, body.check().getStatusCode());
    }
}