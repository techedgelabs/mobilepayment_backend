package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockCheckVoucherAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminCheckAdminAuthorization(String adminTicketId, String operation) {
        return "ADMIN_CHECK_AUTH_200";
    }
}
