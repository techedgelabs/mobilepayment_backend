package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcheckvoucher;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcheckvoucher.TestCheckVoucherBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcheckvoucher.TestCheckVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestCheckVoucherRequestTest extends BaseTestCase {
    private TestCheckVoucherRequest     request;
    private TestCheckVoucherBodyRequest body;
    private String                      operationID;
    private VoucherConsumerType         voucherType;
    private PartnerType                 partnerType;
    private Long                        requestTimestamp;

    protected void setUp() {

        request = new TestCheckVoucherRequest();
        body = new TestCheckVoucherBodyRequest();
        operationID = "operation";
        voucherType = VoucherConsumerType.ENI;
        partnerType = PartnerType.MP;
        requestTimestamp = 1010101L;

        body.setOperationID(operationID);
        body.setVoucherType(voucherType.getValue());
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_TEST_CHECK_VOUCHER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
