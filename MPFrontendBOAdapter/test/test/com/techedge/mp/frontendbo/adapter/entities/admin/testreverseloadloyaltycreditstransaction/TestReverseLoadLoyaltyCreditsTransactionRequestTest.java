package test.com.techedge.mp.frontendbo.adapter.entities.admin.testreverseloadloyaltycreditstransaction;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testreverseloadloyaltycreditstransaction.TestReverseLoadLoyaltyCreditsTransactionBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testreverseloadloyaltycreditstransaction.TestReverseLoadLoyaltyCreditsTransactionRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestReverseLoadLoyaltyCreditsTransactionRequestTest extends BaseTestCase {
    private TestReverseLoadLoyaltyCreditsTransactionRequest     request;
    private TestReverseLoadLoyaltyCreditsTransactionBodyRequest body;
    private String                                              operationID;
    private String                                              operationIDtoReverse;
    private Long                                                requestTimestamp;

    protected void setUp() {

        request = new TestReverseLoadLoyaltyCreditsTransactionRequest();
        body = new TestReverseLoadLoyaltyCreditsTransactionBodyRequest();

        operationID = "id";
        operationIDtoReverse = "request";
        requestTimestamp = 10101010L;

        body.setOperationID(operationID);
        body.setOperationIDtoReverse(operationIDtoReverse);
        body.setPartnerType(PartnerType.MP.getValue());
        body.setRequestTimestamp(requestTimestamp);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_TEST_REVERSE_LOAD_LOYALTY_CREDITS_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
