package test.com.techedge.mp.frontendbo.adapter.entities.admin.admineditmappingerror;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.admineditmappingerror.AdminEditErrorBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.admineditmappingerror.AdminEditErrorRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AdminEditErrorRequestTest extends BaseTestCase {

    private AdminEditErrorRequest     request;
    private AdminEditErrorBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminEditErrorRequest();
        body = new AdminEditErrorBodyRequest();
        body.setGpErrorCode("001");
        body.setMpStatusCode("status");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_EDIT__SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}