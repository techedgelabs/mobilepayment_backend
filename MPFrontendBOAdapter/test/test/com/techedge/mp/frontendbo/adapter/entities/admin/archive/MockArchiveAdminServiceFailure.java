package test.com.techedge.mp.frontendbo.adapter.entities.admin.archive;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.AdminArchiveResponse;

public class MockArchiveAdminServiceFailure extends MockAdminService {
    @Override
    public AdminArchiveResponse adminArchive(String adminTicketId, String requestId, String id) {
        AdminArchiveResponse response = new AdminArchiveResponse();
        response.setStatusCode("ADMIN_ARCHIVE_300");
        return response;
    }
}
