package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusertype;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusertype.RetrieveUserTypeResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class RetrieveUserTypeAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private RetrieveUserTypeResponse retrieveUserTypeResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"retrieveUserType\":{\"credential\":{\"ticketID\":\"NkRBNkIzRjcxNUI0OTk0MTlCNUY5Q0Ey\",\"requestID\":\"WEB-1378034510683\"},\"body\":{\"code\":1}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testRetrieveUserTypeSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveUserTypeAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        retrieveUserTypeResponse = (RetrieveUserTypeResponse) response.getEntity();
        assertEquals("ADMIN_USERTYPE_RETRIEVE_200", retrieveUserTypeResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveUserTypeFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveUserTypeAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        retrieveUserTypeResponse = (RetrieveUserTypeResponse) response.getEntity();
        assertEquals("ADMIN_USERTYPE_RETRIEVE_300", retrieveUserTypeResponse.getStatus().getStatusCode());
    }
}
