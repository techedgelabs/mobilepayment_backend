package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletemanager;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.deletemanager.DeleteManagerBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletemanager.DeleteManagerDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletemanager.DeleteManagerRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class DeleteManagerRequestTest extends BaseTestCase {
    private DeleteManagerRequest     request;
    private DeleteManagerBodyRequest body;
    private DeleteManagerDataRequest data;
    private Long                     id;

    // assigning the values
    protected void setUp() {
        request = new DeleteManagerRequest();
        body = new DeleteManagerBodyRequest();
        data = new DeleteManagerDataRequest();
        id = 1L;
        data.setId(id);
        body.setManagerData(data);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_DELETE_MANAGER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
