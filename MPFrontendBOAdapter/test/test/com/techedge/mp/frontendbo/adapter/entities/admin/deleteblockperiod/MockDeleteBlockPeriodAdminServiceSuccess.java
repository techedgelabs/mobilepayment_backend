package test.com.techedge.mp.frontendbo.adapter.entities.admin.deleteblockperiod;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockDeleteBlockPeriodAdminServiceSuccess extends MockAdminService {

    @Override
    public String adminDeleteBlockPeriod(String ticketId, String requestId, String code) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_BLOCK_PERIOD_DELETE_SUCCESS;
    }
}
