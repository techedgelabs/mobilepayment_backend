package test.com.techedge.mp.frontendbo.adapter.entities.admin.forceupdatetermsfservice;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockRetrieveBlockPeriodAdminServiceFailure extends MockAdminService {
    @Override
    public String adminForceUpdateTermsOfService(String ticketId, String requestId, String keyval, Long personalDataId, Boolean valid) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_UPDATE_TERMS_OF_SERVICE_FAILURE;
    }
}
