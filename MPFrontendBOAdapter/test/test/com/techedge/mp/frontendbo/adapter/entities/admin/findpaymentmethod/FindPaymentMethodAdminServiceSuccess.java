package test.com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.PaymentInfoResponse;

public class FindPaymentMethodAdminServiceSuccess extends MockAdminService {
    @Override
    public PaymentInfoResponse adminPaymentMethodRetrieve(String adminTicketId, String requestId, Long id, String type) {
        PaymentInfoResponse data = new PaymentInfoResponse();
        data.setStatusCode("ADMIN_UPDATE_USER_200");
        return data;
    }
}
