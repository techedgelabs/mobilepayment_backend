package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcheckvoucher;

import java.util.List;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockCheckVoucherFidelityServiceSuccess extends MockFidelityService {

    @Override
    public CheckVoucherResult checkVoucher(String operationID, VoucherConsumerType voucherType, PartnerType partnerType, Long requestTimestamp,
            List<VoucherCodeDetail> voucherCodeList) throws FidelityServiceException {
        CheckVoucherResult data = new CheckVoucherResult();
        data.setStatusCode(StatusCode.ADMIN_TEST_CHECK_VOUCHER_SUCCESS);
        return data;
    }
}
