package test.com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber.CreatePrefixNumberBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber.CreatePrefixNumberRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber.CreatePrefixNumberResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestCreatePrefixNumberAdminService extends BaseTestCase {
    private FrontendBOAdapterService      frontend;
    private CreatePrefixNumberRequest     request;
    private CreatePrefixNumberBodyRequest body;
    private CreatePrefixNumberResponse    response;
    private Response                      baseResponse;
    private String                        json;
    private Gson                          gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new CreatePrefixNumberRequest();
        body = new CreatePrefixNumberBodyRequest();
        body.setCode("code");
        request.setBody(body);
        request.setCredential(createCredentialTrue());

        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testCreateCustomerUserSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreatePrefixNumberAdminServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setCreatePrefixNumber(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (CreatePrefixNumberResponse) baseResponse.getEntity();
        assertEquals(StatusCode.PREFIX_CREATE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testCreateCustomerUserFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreatePrefixNumberAdminServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setCreatePrefixNumber(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (CreatePrefixNumberResponse) baseResponse.getEntity();
        assertEquals(StatusCode.PREFIX_CREATE_FAILURE, response.getStatus().getStatusCode());
    }
}
