package test.com.techedge.mp.frontendbo.adapter.entities.admin.resendconfirmuseremail;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.resendconfirmuseremail.ResendConfirmUserEmailRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.resendconfirmuseremail.ResendConfirmUserEmailRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class ResendConfirmUserEmailRequestTest extends BaseTestCase {
    private ResendConfirmUserEmailRequest     request;
    private ResendConfirmUserEmailRequestBody body;
    private String                            userEmail;

    // assigning the values
    protected void setUp() {
        request = new ResendConfirmUserEmailRequest();
        body = new ResendConfirmUserEmailRequestBody();
        userEmail = "test";
        body.setUserEmail(userEmail);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_UPDATE_USER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
