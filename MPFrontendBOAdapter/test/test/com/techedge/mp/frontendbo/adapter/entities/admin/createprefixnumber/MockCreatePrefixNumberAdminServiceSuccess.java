package test.com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.ResponseHelper;

public class MockCreatePrefixNumberAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminCreatePrefixNumber(String adminTicketId, String requestId, String code) {
        // TODO Auto-generated method stub
        return ResponseHelper.PREFIX_CREATE_SUCCESS;
    }
}
