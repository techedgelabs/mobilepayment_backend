package test.com.techedge.mp.frontendbo.adapter.entities.admin.testreverseconsumevouchertransaction;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testreverseconsumevouchertransaction.TestReverseConsumeVoucherTransactionBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testreverseconsumevouchertransaction.TestReverseConsumeVoucherTransactionRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestReverseConsumeVoucherTransactionRequestTest extends BaseTestCase {
    private TestReverseConsumeVoucherTransactionRequest     request;
    private TestReverseConsumeVoucherTransactionBodyRequest body;
    private String                                          operationID;
    private String                                          operationIDtoReverse;
    private PartnerType                                     partnerType;
    private Long                                            requestTimestamp;

    protected void setUp() {

        request = new TestReverseConsumeVoucherTransactionRequest();
        body = new TestReverseConsumeVoucherTransactionBodyRequest();

        operationID = "id";
        operationIDtoReverse = "request";
        partnerType = PartnerType.MP;
        requestTimestamp = 10101010L;

        body.setOperationID(operationID);
        body.setOperationIDtoReverse(operationIDtoReverse);
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_TEST_REVERSE_CONSUME_VOUCHER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
