package test.com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute.AdminCreateDocumentAttributeBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createdocumentattribute.AdminCreateDocumentAttributeRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class CreateDocumentAttributeRequestTest extends BaseTestCase {

    private AdminCreateDocumentAttributeRequest     request;
    private AdminCreateDocumentAttributeBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminCreateDocumentAttributeRequest();
        body = new AdminCreateDocumentAttributeBodyRequest();
        body.setPosition(1);
        body.setDocumentKey("documentKey");
        body.setCheckKey("checkKey");
        body.setConditionText("condition");
        body.setExtendedConditionText("extend");
        body.setMandatory(true);
        body.setPosition(1);
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        
        request.setBody(null);
        
        assertEquals(StatusCode.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_FAILURE, request.check().getStatusCode());
    }
}