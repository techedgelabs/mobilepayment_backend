package test.com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockAdminRemoveErrorAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminRemoveMappingError(String adminTicketId, String requestID, String mpStatusCode) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_MAPPING_ERROR_REMOVE__SUCCESS;
    }
}
