package test.com.techedge.mp.frontendbo.adapter.entities.admin.addusertypetousercategory;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.addusertypetousercategory.AddUserTypeToUserCategoryBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addusertypetousercategory.AddUserTypeToUserCategoryRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddUserTypeToUserCategoryBodyRequestTest extends BaseTestCase {

    private AddUserTypeToUserCategoryRequest     request;
    private AddUserTypeToUserCategoryBodyRequest body;
    private String                           name;
    private Integer                          code;

    // assigning the values
    protected void setUp() {
        request = new AddUserTypeToUserCategoryRequest();
        body = new AddUserTypeToUserCategoryBodyRequest();
        name = "category";
        code = 1;
        body.setCode(code);
        body.setName(name);
        request.setBody(body);
    }

    @Test
    public void testNameNull() {

        name = null;
        body.setName(name);

        assertEquals(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE, body.check().getStatusCode());

    }

    @Test
    public void testCodeNull() {

        code = null;
        body.setCode(code);

        assertEquals(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE, body.check().getStatusCode());

    }

    @Test
    public void testNameIsEmpty() {

        name = "";
        body.setName(name);

        assertEquals(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE, body.check().getStatusCode());

    }
}
