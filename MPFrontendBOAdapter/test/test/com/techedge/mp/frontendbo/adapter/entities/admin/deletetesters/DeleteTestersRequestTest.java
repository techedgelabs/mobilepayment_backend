package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters.DeleteTestersDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters.DeleteTestersRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletetesters.DeleteTestersRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class DeleteTestersRequestTest extends BaseTestCase {
    private DeleteTestersRequest     request;
    private DeleteTestersRequestBody body;
    private DeleteTestersDataRequest data;

    // assigning the values
    protected void setUp() {
        request = new DeleteTestersRequest();
        body = new DeleteTestersRequestBody();
        data = new DeleteTestersDataRequest();
        body.setCustomerUserData(data);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_CREATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
