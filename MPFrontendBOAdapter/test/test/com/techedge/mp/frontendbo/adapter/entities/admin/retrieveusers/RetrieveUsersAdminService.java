package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers.RetrieveUsersResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class RetrieveUsersAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private RetrieveUsersResponse    retrieveUsersAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"retrieveUsers\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"id\":\"1\",\"fiscalCode\":\"cdernfjriwoego1o\",\"email\":\"a@a.it\",\"firstName\":\"Giovanni\",\"lastName\":\"D'Orazio\",\"status\":\"2\",\"externalUserId\":\"test\",\"minAvailableCap\":\"00100\",\"maxAvailableCap\":\"00900\",\"minEffectiveCap\":\"00100\",\"maxEffectiveCap\":\"00999\",\"creationDateStart\":\"0000000\",\"creationDateEnd\":\"999999999\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveUsersAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        retrieveUsersAdminResponse = (RetrieveUsersResponse) response.getEntity();
        assertEquals("ADMIN_RETR_USERS_200", retrieveUsersAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveUsersAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        retrieveUsersAdminResponse = (RetrieveUsersResponse) response.getEntity();
        assertEquals("ADMIN_RETR_USERS_300", retrieveUsersAdminResponse.getStatus().getStatusCode());
    }
}
