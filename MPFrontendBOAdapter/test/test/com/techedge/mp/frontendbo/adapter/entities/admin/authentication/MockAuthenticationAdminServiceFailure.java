package test.com.techedge.mp.frontendbo.adapter.entities.admin.authentication;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.AdminAuthenticationResponse;

public class MockAuthenticationAdminServiceFailure extends MockAdminService {
    @Override
    public AdminAuthenticationResponse adminAuthentication(String email, String password, String requestId) {
        AdminAuthenticationResponse response = new AdminAuthenticationResponse();
        response.setStatusCode("ADMIN_AUTH_300");
        return response;
    }
}
