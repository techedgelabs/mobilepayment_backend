package test.com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror.AdminGetErrorBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror.AdminGetErrorRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AdminGetErrorRequestTest extends BaseTestCase {

    private AdminGetErrorRequest     request;
    private AdminGetErrorBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminGetErrorRequest();
        body = new AdminGetErrorBodyRequest();
        body.setGpErrorCode("error");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_GET__SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}