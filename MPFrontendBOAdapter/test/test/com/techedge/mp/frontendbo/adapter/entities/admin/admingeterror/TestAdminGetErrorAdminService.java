package test.com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror.AdminGetErrorBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror.AdminGetErrorRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.admingeterror.AdminGetErrorResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestAdminGetErrorAdminService extends BaseTestCase {
    private FrontendBOAdapterService frontend;
    private AdminGetErrorRequest     request;
    private AdminGetErrorBodyRequest body;
    private AdminGetErrorResponse    response;
    private Response                 baseResponse;
    private String                   json;
    private Gson                     gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AdminGetErrorRequest();
        body = new AdminGetErrorBodyRequest();
        body.setGpErrorCode("error");
        request.setBody(body);
        request.setCredential(createCredentialTrue());

        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testAdminGetErrorSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAdminGetErrorAdminServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setAdminGetError(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminGetErrorResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_GET__SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testAdminGetErrorFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAdminGetErrorAdminServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setAdminGetError(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminGetErrorResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_GET__SUCCESS, response.getStatus().getStatusCode());
    }
}
