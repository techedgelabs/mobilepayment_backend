package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletesurveyquestion;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockDeleteSurveyQuestionAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminSurveyQuestionDelete(String adminTicketId, String requestId, String questionCode) {
        // TODO Auto-generated method stub
        return "ADMIN_SURVEY_QUESTION_DELETE_200";
    }
}
