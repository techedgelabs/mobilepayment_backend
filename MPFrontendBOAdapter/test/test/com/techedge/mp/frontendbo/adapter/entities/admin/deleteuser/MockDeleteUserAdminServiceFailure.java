package test.com.techedge.mp.frontendbo.adapter.entities.admin.deleteuser;

import java.util.Date;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockDeleteUserAdminServiceFailure extends MockAdminService {
    @Override
    public String adminUserDelete(String adminTicketId, String requestId, Long userId, Date creationStart, Date creationEnd) {
        // TODO Auto-generated method stub
        return "ADMIN_DELETE_USER_300";
    }
}
