package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreautorizatinconsumevoucher;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockCancelPreAutorizatinConsumeVoucherAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminCheckAdminAuthorization(String adminTicketId, String operation) {
        return "ADMIN_CHECK_AUTH_200";
    }
}
