package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers;

import java.sql.Timestamp;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.user.RetrieveUserListData;

public class MockRetrieveUsersAdminServiceSuccess extends MockAdminService {
    public RetrieveUserListData adminUserRetrieve(String adminTicketId, String requestId, Long id, String firstName, String lastName, String fiscalCode, String securityDataEmail,
            Integer userStatus, String externalUserId, Double capAvailableMin, Double capAvailableMax, Double capEffectiveMin, Double capEffectiveMax, Timestamp creationDateMin,
            Timestamp creationDateMax) {
        RetrieveUserListData data = new RetrieveUserListData();
        data.setStatusCode("ADMIN_RETR_USERS_200");
        return data;
    }
}
