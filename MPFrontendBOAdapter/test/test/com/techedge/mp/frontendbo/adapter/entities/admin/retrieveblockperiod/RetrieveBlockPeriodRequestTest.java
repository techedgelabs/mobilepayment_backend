package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveblockperiod;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveblockperiod.AdminRetrieveBlockPeriodBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveblockperiod.AdminRetrieveBlockPeriodRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveBlockPeriodRequestTest extends BaseTestCase {

    private AdminRetrieveBlockPeriodRequest     request;
    private AdminRetrieveBlockPeriodBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminRetrieveBlockPeriodRequest();
        body = new AdminRetrieveBlockPeriodBodyRequest();
        body.setActive(true);
        body.setCode("code");
        request.setBody(body);
        request.setCredential(createCredentialTrue());

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_RETRIEVE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}