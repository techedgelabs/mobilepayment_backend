package test.com.techedge.mp.frontendbo.adapter.entities.admin.resetsurvey;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockResetSurveyAdminServiceSuccess extends MockAdminService {

    @Override
    public String adminSurveyReset(String adminTicketId, String requestId, String surveyCode) {
        // TODO Auto-generated method stub
        return "ADMIN_SURVEY_RESET_200";
    }
}
