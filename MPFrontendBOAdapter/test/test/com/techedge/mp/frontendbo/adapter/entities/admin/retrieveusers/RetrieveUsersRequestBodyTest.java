package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusers.RetrieveUsersRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveUsersRequestBodyTest extends BaseTestCase {

    private RetrieveUsersRequestBody body;
    private Long                     id;
    private String                   firstName;
    private String                   lastName;
    private String                   fiscalCode;
    private String                   email;
    private Integer                  status;
    private String                   externalUserId;
    private Double                   minAvailableCap;
    private Double                   maxAvailableCap;
    private Double                   minEffectiveCap;
    private Double                   maxEffectiveCap;
    private Long                     creationDateStart;
    private Long                     creationDateEnd;

    // assigning the values
    protected void setUp() {
        body = new RetrieveUsersRequestBody();

        email = "email@email.it";
        id = 01L;
        firstName = "name";
        lastName = "last";
        fiscalCode = "QWERTYUIOPLKJHGF";
        status = 5;
        externalUserId = "0012";
        minEffectiveCap = 2.5;
        maxEffectiveCap = 5.5;
        creationDateEnd = 001100L;
        creationDateStart = 110011L;

        body.setCreationDateEnd(creationDateEnd);
        body.setCreationDateStart(creationDateStart);
        body.setEmail(email);
        body.setExternalUserId(externalUserId);
        body.setFirstName(firstName);
        body.setFiscalCode(fiscalCode);
        body.setLastName(lastName);
        body.setId(id);
        body.setMaxAvailableCap(maxAvailableCap);
        body.setMaxEffectiveCap(maxEffectiveCap);
        body.setMinAvailableCap(minAvailableCap);
        body.setMinEffectiveCap(minEffectiveCap);
        body.setStatus(status);
    }

    @Test
    public void testFirstNameMajor() {

        body.setFirstName("FirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstName");
        assertEquals(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testLastNameMajor() {
        body.setLastName("FirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstName");
        assertEquals(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testFiscalCodeMajor() {
        body.setFiscalCode("FirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstName");
        assertEquals(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testEmailMajor() {
        body.setEmail("FirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstName"
                + "FirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstName"
                + "FirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstName");
        assertEquals(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testExternalMajor() {
        body.setExternalUserId("FirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstNameFirstName");
        assertEquals(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testStatus() {
        body.setStatus(50);
        assertEquals(StatusCode.ADMIN_RETRIEVE_TRANSACTIONS_ERROR_PARAMETERS, body.check().getStatusCode());
    }




}