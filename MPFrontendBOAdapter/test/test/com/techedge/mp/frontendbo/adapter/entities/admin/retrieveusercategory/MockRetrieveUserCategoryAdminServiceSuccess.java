package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusercategory;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.user.UserCategoryData;

public class MockRetrieveUserCategoryAdminServiceSuccess extends MockAdminService {

    @Override
    public UserCategoryData adminRetrieveUserCategory(String adminTicketId, String requestId, String name) {
        UserCategoryData data = new UserCategoryData();
        data.setStatusCode("ADMIN_USERCATEGORY_RETRIEVE_200");
        return data;
    }

}
