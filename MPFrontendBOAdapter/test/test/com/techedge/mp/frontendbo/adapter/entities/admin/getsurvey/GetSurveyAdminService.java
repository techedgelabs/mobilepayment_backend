package test.com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey.GetSurveyResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class GetSurveyAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private GetSurveyResponse        getSurveyResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"getSurvey\":{\"credential\":{\"ticketID\":\"ODU2MTQ3QzAyNEY2NTFCNkFEMTFEOUE5\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"code\":\"SURVEY_TRANSACTION\",\"startSearch\":1445171200,\"endSearch\":1465171200,\"status\":\"1\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testGetSurveySuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new GetSurveyAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        getSurveyResponse = (GetSurveyResponse) response.getEntity();
        assertEquals("ADMIN_SURVEY_GET_200", getSurveyResponse.getStatus().getStatusCode());
    }

    @Test
    public void testGetSurveyFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new GetSurveyAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        getSurveyResponse = (GetSurveyResponse) response.getEntity();
        assertEquals("ADMIN_SURVEY_GET_300", getSurveyResponse.getStatus().getStatusCode());
    }
}
