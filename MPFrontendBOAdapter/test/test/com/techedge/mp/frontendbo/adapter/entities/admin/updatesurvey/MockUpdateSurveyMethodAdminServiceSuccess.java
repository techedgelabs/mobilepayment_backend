package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatesurvey;

import java.util.Date;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockUpdateSurveyMethodAdminServiceSuccess extends MockAdminService {

    @Override
    public String adminSurveyUpdate(String adminTicketId, String requestId, String code, String description, String note, Integer status, Date startDate, Date endDate) {
        // TODO Auto-generated method stub
        return "ADMIN_SURVEY_UPDATE_200";
    }
}
