package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatesurvey;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updatesurvey.UpdateSurveyBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateSurveyBodyRequestTest extends BaseTestCase {
    private UpdateSurveyBodyRequest body;
    private String                  code;
    private String                  description;
    private Integer                 status;
    private Long                    startData;
    private Long                    endData;

    protected void setUp() {

        body = new UpdateSurveyBodyRequest();

        code = "code";
        description = "descr";
        status = 4;
        startData = 1111110L;
        endData = 1111111L;

        body.setCode(code);
        body.setDescription(description);
        body.setEndDate(endData);
        body.setStartDate(startData);
        body.setStatus(status);

    }

    @Test
    public void testCodeNull() {
        body.setCode(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testStatusNull() {
        body.setStatus(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testStarDateNull() {
        body.setStartDate(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testEndDateNull() {
        body.setEndDate(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testStartEndDate() {
        body.setStartDate(2L);
        body.setEndDate(1L);
        assertEquals(StatusCode.ADMIN_SURVEY_UPDATE_START_DATE_ERROR, body.check().getStatusCode());
    }
}