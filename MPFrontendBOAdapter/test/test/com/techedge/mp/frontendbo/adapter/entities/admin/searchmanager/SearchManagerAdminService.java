package test.com.techedge.mp.frontendbo.adapter.entities.admin.searchmanager;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.searchmanager.SearchManagerResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class SearchManagerAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private SearchManagerResponse    searchManagerResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"searchManager\":{\"credential\":{\"ticketID\":\"RTY4MEI5MDNCNzdENTlFMUE5MEZEMTg4\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"id\":34,\"username\":\"string-pattern-match\",\"email\":\"string-pattern-match\",\"maxResults\":100}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testSearchManagerSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockSearchManagerAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        searchManagerResponse = (SearchManagerResponse) response.getEntity();
        assertEquals("ADMIN_SEARCH_MANAGER_200", searchManagerResponse.getStatus().getStatusCode());
    }

    @Test
    public void testSearchManagerFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockSearchManagerAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        searchManagerResponse = (SearchManagerResponse) response.getEntity();
        assertEquals("ADMIN_SEARCH_MANAGER_300", searchManagerResponse.getStatus().getStatusCode());
    }
}
