package test.com.techedge.mp.frontendbo.adapter.entities.admin.removeusertypetousercategory;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.removeusertypefromusercategory.RemoveUserTypeFromUserCategoryBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RemoveUserTypeToUserCategoryBodyRequestTest extends BaseTestCase {

    private RemoveUserTypeFromUserCategoryBodyRequest body;
    private Integer                             code;
    private String                              name;

    // assigning the values
    protected void setUp() {
        body = new RemoveUserTypeFromUserCategoryBodyRequest();
        code = 100;
        name = "test";
        body.setCode(code);
        body.setName(name);
    }

    @Test
    public void testNameNull() {
        body.setName(null);
        assertEquals(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testNameIsEmpty() {
        body.setName("");
        assertEquals(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testCodeNull() {
        body.setCode(null);
        assertEquals(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE, body.check().getStatusCode());
    }
}