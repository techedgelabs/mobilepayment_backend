package test.com.techedge.mp.frontendbo.adapter.entities.admin.createsurvey;

import java.util.ArrayList;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createsurvey.CreateSurveyBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createsurvey.CreateSurveyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.frontendbo.adapter.entities.common.SurveyQuestion;

public class AddCreateSurveyRequestTest extends BaseTestCase {

    private CreateSurveyRequest       request;
    private CreateSurveyBodyRequest   body;
    private String                    code;
    private String                    description;
    private String                    note;
    private Long                      startDate;
    private Long                      endDate;
    private Integer                   status;
    private ArrayList<SurveyQuestion> questions;

    // assigning the values
    protected void setUp() {
        request = new CreateSurveyRequest();
        body = new CreateSurveyBodyRequest();
        code = "0123";
        description = "descrizione";
        note = "note";
        startDate = 10091990L;
        endDate = 11091990L;
        status = 2;
        questions = new ArrayList<SurveyQuestion>(0);
        body.setCode(code);
        body.setDescription(description);
        body.setEndDate(endDate);
        body.setNote(note);
        body.setStartDate(startDate);
        body.setStatus(status);
        body.setQuestions(questions);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_SURVEY_CREATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
