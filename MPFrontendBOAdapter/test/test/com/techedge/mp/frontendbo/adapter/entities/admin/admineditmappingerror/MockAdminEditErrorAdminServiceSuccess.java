package test.com.techedge.mp.frontendbo.adapter.entities.admin.admineditmappingerror;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockAdminEditErrorAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminEditMappingError(String adminTicketId, String requestID, String gpErrorCode, String mpStatusCode) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_MAPPING_ERROR_EDIT__SUCCESS;
    }
}
