package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateuser;

import java.util.Date;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockUpdateUserAdminServiceFailure extends MockAdminService {
    @Override
    public String adminUserUpdate(String adminTicketId, String requestId, Long id, String firstName, String lastName, String email, Date birthDate, String birthMunicipality,
            String birthProvince, String sex, String fiscalCode, String language, Integer status, Boolean registrationCompleted, Double capAvailable, Double capEffective,
            String externalUserId, Integer userType, Boolean virtualizationCompleted, Integer virtualizationAttemptsLeft, Boolean eniStationUserType, Boolean depositCardStepCompleted) {
        // TODO Auto-generated method stub
        return "ADMIN_UPDATE_USER_300";
    }
}
