package test.com.techedge.mp.frontendbo.adapter.entities.admin.testenableloyaltycardlist;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testgetloyaltycardlist.TestGetLoyaltyCardListBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testgetloyaltycardlist.TestGetLoyaltyCardListRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestEnableLoyaltyCardListRequestTest extends BaseTestCase {
    private TestGetLoyaltyCardListRequest     request;
    private TestGetLoyaltyCardListBodyRequest body;
    private String                            operationID;
    private String                            fiscalCode;
    private String                            eanCode;
    private PartnerType                       partnerType;
    private Long                              requestTimestamp;

    protected void setUp() {

        request = new TestGetLoyaltyCardListRequest();
        body = new TestGetLoyaltyCardListBodyRequest();
        operationID = "operation";
        fiscalCode = "check";
        eanCode = "panCode";
        partnerType = PartnerType.MP;
        requestTimestamp = 1010101L;

        body.setOperationID(operationID);
        body.setFiscalCode(fiscalCode);
        body.setEanCode(eanCode);
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_TEST_GET_LOYALTY_CARD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
