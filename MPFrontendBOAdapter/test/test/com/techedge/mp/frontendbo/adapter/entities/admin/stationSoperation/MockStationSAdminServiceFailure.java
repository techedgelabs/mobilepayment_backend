package test.com.techedge.mp.frontendbo.adapter.entities.admin.stationSoperation;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.StationsAdminData;

public class MockStationSAdminServiceFailure extends MockAdminService {
    @Override
    public StationsAdminData adminAddStations(String adminTicketId, String requestId, StationsAdminData stationsAdminData) {
        StationsAdminData data = new StationsAdminData();
        data.setStatusCode("ADMIN_CREATE_300");
        return data;
    }

    @Override
    public StationsAdminData adminDeleteStations(String adminTicketId, String requestId, StationsAdminData stationsAdminData) {
        StationsAdminData data = new StationsAdminData();
        data.setStatusCode("ADMIN_CREATE_300");
        return data;
    }
}
