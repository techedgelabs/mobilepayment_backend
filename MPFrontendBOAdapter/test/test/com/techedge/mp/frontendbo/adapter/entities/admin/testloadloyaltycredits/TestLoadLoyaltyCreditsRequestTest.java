package test.com.techedge.mp.frontendbo.adapter.entities.admin.testloadloyaltycredits;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testloadloyaltycredits.TestLoadLoyaltyCreditsBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testloadloyaltycredits.TestLoadLoyaltyCreditsRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.ProductInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestLoadLoyaltyCreditsRequestTest extends BaseTestCase {
    private TestLoadLoyaltyCreditsRequest     request;
    private TestLoadLoyaltyCreditsBodyRequest body;
    private String                            operationID;
    private String                            mpTransactionID;
    private String                            stationID;
    private String                            panCode;
    private String                            BIN;
    private String                            refuelMode;
    private String                            paymentMode;
    private PartnerType                       partnerType;
    private Long                              requestTimestamp;
    List<ProductInfo>                         productList = new ArrayList<ProductInfo>(0);

    protected void setUp() {

        request = new TestLoadLoyaltyCreditsRequest();
        body = new TestLoadLoyaltyCreditsBodyRequest();
        operationID = "operation";
        mpTransactionID = "check";
        stationID = "panCode";
        panCode = "pan";
        BIN = "bin";
        refuelMode = "refuel";
        paymentMode = "mode";
        partnerType = PartnerType.MP;
        requestTimestamp = 1010101L;

        body.setOperationID(operationID);
        body.setMpTransactionID(mpTransactionID);
        body.setStationID(stationID);
        body.setPanCode(panCode);
        body.setBIN(BIN);
        body.setRefuelMode(refuelMode);
        body.setPaymentMode(paymentMode);
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_TEST_LOAD_LOYALTY_CREDITS_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
