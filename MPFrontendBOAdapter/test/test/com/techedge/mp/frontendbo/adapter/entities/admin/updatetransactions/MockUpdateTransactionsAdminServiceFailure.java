package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatetransactions;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockUpdateTransactionsAdminServiceFailure extends MockAdminService {
    @Override
    public String adminTransactionUpdate(String adminTicketId, String requestId, String transactionId, String finalStatusType, Boolean GFGNotification, Boolean confirmed, Integer ReconciliationAttemptsLeft) {
        // TODO Auto-generated method stub
        return "ADMIN_UPDATE_TRANSACTION_300";
    }
}
