package test.com.techedge.mp.frontendbo.adapter.entities.admin.testenableloyaltycard;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testenableloyaltycard.TestEnableLoyaltyCardBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestEnableLoyaltyCardBodyRequestTest extends BaseTestCase {
    private TestEnableLoyaltyCardBodyRequest body;
    private String                           operationID;
    private String                           fiscalCode;
    private String                           panCode;
    private Boolean                          enable;
    private PartnerType                      partnerType;
    private Long                             requestTimestamp;

    protected void setUp() {

        body = new TestEnableLoyaltyCardBodyRequest();
        operationID = "operation";
        fiscalCode = "check";
        panCode = "panCode";
        enable = true;
        partnerType = PartnerType.MP;
        requestTimestamp = 1010101L;

        body.setOperationID(operationID);
        body.setFiscalCode(fiscalCode);
        body.setPanCode(panCode);
        body.setEnable(enable);
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);

    }

    @Test
    public void testOperationNull() {

        body.setOperationID(null);
        assertEquals(StatusCode.ADMIN_TEST_ENABLE_LOYALTY_CARD_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testIFiscalCodeNull() {

        body.setFiscalCode(null);
        assertEquals(StatusCode.ADMIN_TEST_ENABLE_LOYALTY_CARD_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testPanCodeNull() {

        body.setPanCode(null);
        assertEquals(StatusCode.ADMIN_TEST_ENABLE_LOYALTY_CARD_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testEnalbleNull() {

        body.setEnable(null);
        assertEquals(StatusCode.ADMIN_TEST_ENABLE_LOYALTY_CARD_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testPartnerTypeNull() {

        body.setPartnerType(null);
        assertEquals(StatusCode.ADMIN_TEST_ENABLE_LOYALTY_CARD_INVALID_REQUEST, body.check().getStatusCode());

    }

    @Test
    public void testRequestTimestampNull() {

        body.setRequestTimestamp(null);
        assertEquals(StatusCode.ADMIN_TEST_ENABLE_LOYALTY_CARD_INVALID_REQUEST, body.check().getStatusCode());

    }

}
