package test.com.techedge.mp.frontendbo.adapter.entities.admin.create;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.create.CreateAdminDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.create.CreateAdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.create.CreateAdminRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class CreateRequestTest extends BaseTestCase {

    private CreateAdminRequest     request;
    private CreateAdminRequestBody body;
    private CreateAdminDataRequest data;

    // assigning the values
    protected void setUp() {
        request = new CreateAdminRequest();
        body = new CreateAdminRequestBody();
        data = new CreateAdminDataRequest();
        body.setAdminData(data);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_CREATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_CREATE_SUCCESS, body.check().getStatusCode());
    }

    @Test
    public void testDataValid() {
        assertEquals(StatusCode.ADMIN_CREATE_SUCCESS, body.check().getStatusCode());
    }

    @Test
    public void testDataNull() {
        body.setAdminData(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testData() {
        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_CREATE_SUCCESS, data.check().getStatusCode());
    }
}
