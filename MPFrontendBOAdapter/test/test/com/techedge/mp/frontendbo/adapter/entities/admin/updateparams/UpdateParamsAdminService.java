package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateparams;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.AdminUpdateParamsResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class UpdateParamsAdminService extends TestCase {
    private FrontendBOAdapterService  frontend;
    private Response                  response;
    private String                    json;
    private AdminUpdateParamsResponse updateParamsAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"updateParams\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"parameterList\":[{\"param\":\"CALL_CENTER_REAL\",\"value\":\"06300200100\"},{\"param\":\"CALL_CENTER_REAL\",\"value\":\"06300200100\"}]}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockUpdateParamsAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        updateParamsAdminResponse = (AdminUpdateParamsResponse) response.getEntity();
        assertEquals("ADMIN_UPDATE_PARAMS_200", updateParamsAdminResponse.getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockUpdateParamsAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        updateParamsAdminResponse = (AdminUpdateParamsResponse) response.getEntity();
        assertEquals("ADMIN_UPDATE_PARAMS_300", updateParamsAdminResponse.getStatusCode());
    }
}
