package test.com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey.GetSurveyBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class GetSurveyBodyRequestTest extends BaseTestCase {
    private GetSurveyBodyRequest body;
    private String               code;
    private Long                 startSearch;
    private Long                 endSearch;
    private Integer              status;

    // assigning the values
    protected void setUp() {
        body = new GetSurveyBodyRequest();
        code = "001";
        startSearch = 0L;
        endSearch = 1L;
        status = 4;
        body.setCode(code);
        body.setEndSearch(endSearch);
        body.setStartSearch(startSearch);
        body.setStatus(status);
    }

    @Test
    public void testStartSearchMajorEndSearch() {
        body.setStartSearch(00101010L);
        assertEquals(StatusCode.ADMIN_SURVEY_GET_START_DATE_ERROR, body.check().getStatusCode());
    }
}