package test.com.techedge.mp.frontendbo.adapter.entities.admin.createsurvey;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.createsurvey.CreateSurveyResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestCreateSurveyAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private CreateSurveyResponse     createSurveyResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"createSurvey\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"code\":\"SURVEY_TRANSACTION\",\"description\":\"Aiutaci a migliorare: dai la tua valutazione di Eni Pay. Valuta da 1 a 5 i seguenti quesiti.\",\"note\":\"1 punteggio minimo e 5 punteggio massimo.\",\"startDate\":1469633083,\"endDate\":1469633083,\"status\":\"1\",\"questions\":[{\"code\":\"s1q1\",\"text\":\"Quanto valuti utile utilizzare il telefono per pagare i tuoi rifornimenti presso le Eni Station?\",\"type\":\"RANGE_1_5\",\"sequence\":\"1\"},{\"code\":\"s1q2\",\"text\":\"Quanto valuti rapida e semplice l'esperienza di rifornimento e pagamento con Eni Pay?\",\"type\":\"RANGE_1_5\",\"sequence\":\"2\"},{\"code\":\"s1q3\",\"text\":\"Nel complesso, quanto ti ritieni soddisfatto dell�esperienza fatta?\",\"type\":\"RANGE_1_5\",\"sequence\":\"3\"}]}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
    }

    @Test
    public void testCreateCustomerUserSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateSurveyAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        createSurveyResponse = (CreateSurveyResponse) response.getEntity();
        assertEquals("ADMIN_SURVEY_CREATE_200", createSurveyResponse.getStatus().getStatusCode());
    }

    @Test
    public void testCreateCustomerUserFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateSurveyAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        createSurveyResponse = (CreateSurveyResponse) response.getEntity();
        assertEquals("ADMIN_SURVEY_CREATE_300", createSurveyResponse.getStatus().getStatusCode());
    }
}
