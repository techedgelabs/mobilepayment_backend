package test.com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation.StationDeleteRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation.StationOpRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StationData;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class StationDeleteRequestTest extends BaseTestCase {
    private StationDeleteRequest request;
    private StationOpRequestBody body;
    private StationData          station;

    protected void setUp() {

        request = new StationDeleteRequest();
        body = new StationOpRequestBody();
        station = new StationData();
        body.setStation(station);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_CREATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testStationNull() {
        request.setCredential(createCredentialTrue());
        body.setStation(null);
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_STATION_PARAMETERS, body.check().getStatusCode());
    }

}
