package test.com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror.AdminRemoveErrorBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror.AdminRemoveErrorRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.adminremoveerror.AdminRemoveErrorResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestAdminRemoveErrorAdminService extends BaseTestCase {
    private FrontendBOAdapterService    frontend;
    private AdminRemoveErrorRequest     request;
    private AdminRemoveErrorBodyRequest body;
    private AdminRemoveErrorResponse    response;
    private Response                    baseResponse;
    private String                      json;
    private Gson                        gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AdminRemoveErrorRequest();
        body = new AdminRemoveErrorBodyRequest();
        body.setGpErrorCode("001");
        request.setBody(body);
        request.setCredential(createCredentialTrue());

        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testCreateCustomerUserSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAdminRemoveErrorAdminServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setAdminRemoveError(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminRemoveErrorResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_REMOVE__SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testCreateCustomerUserFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAdminRemoveErrorAdminServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setAdminRemoveError(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminRemoveErrorResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_REMOVE__FAILURE, response.getStatus().getStatusCode());
    }
}
