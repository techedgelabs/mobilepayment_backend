package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog;

import java.sql.Timestamp;

import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.RetrieveActivityLogData;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockRetrieveActivityLogAdminServiceSuccess extends MockAdminService {

    @Override
    public RetrieveActivityLogData adminActivityLogRetrieve(String adminTicketId, String requestId, Timestamp start, Timestamp end, ErrorLevel minLevel, ErrorLevel maxLevel,
            String source, String groupId, String phaseId, String messagePattern) {
        RetrieveActivityLogData data = new RetrieveActivityLogData();
        data.setStatusCode("ADMIN_RETR_LOG_200");
        return data;
    }
}
