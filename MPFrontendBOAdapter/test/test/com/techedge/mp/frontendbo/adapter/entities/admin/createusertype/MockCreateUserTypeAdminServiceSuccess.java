package test.com.techedge.mp.frontendbo.adapter.entities.admin.createusertype;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockCreateUserTypeAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminCreateUserType(String adminTicketId, String requestId, Integer code) {
        // TODO Auto-generated method stub
        return "ADMIN_USERTYPE_CREATE_200";
    }
}
