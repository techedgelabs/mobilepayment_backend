package test.com.techedge.mp.frontendbo.adapter.entities.admin.removeprefixnumber;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.frontendbo.adapter.entities.admin.removeprefixnumber.RemovePrefixNumberBodyRequest;

public class RemovePrefixNumberBodyRequestTest extends BaseTestCase {

    private RemovePrefixNumberBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new RemovePrefixNumberBodyRequest();
        body.setCode("code");

    }

    @Test
    public void testCodeNull() {
        body.setCode(null);
        assertEquals(ResponseHelper.PREFIX_REMOVE_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testCodeIdEmpty() {
        body.setCode("");
        assertEquals(ResponseHelper.PREFIX_REMOVE_FAILURE, body.check().getStatusCode());
    }

}