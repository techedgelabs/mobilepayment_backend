package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveparams;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.RetrieveParamsData;

public class MockRetrieveParamsAdminServiceSuccess extends MockAdminService {

    @Override
    public RetrieveParamsData adminRetrieveParams(String adminTicketId, String requestId, String param) {
        RetrieveParamsData data = new RetrieveParamsData();
        data.setStatusCode("ADMIN_RETRIEVE_PARAMS_200");
        return data;
    }
}
