package test.com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher;

import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher.TestPreAuthorizationConsumeVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher.TestPreAuthorizationConsumeVoucherRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher.TestPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestPreAuthorizationConsumeVoucherFidelityService extends BaseTestCase {
    private FrontendBOAdapterService                      frontend;
    private Response                                      baseResponse;
    private String                                        json;
    private Gson                                          gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private TestPreAuthorizationConsumeVoucherRequest     request;
    private TestPreAuthorizationConsumeVoucherRequestBody body;
    private TestPreAuthorizationConsumeVoucherResponse    response;
    private Credential                                    credential;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        request = new TestPreAuthorizationConsumeVoucherRequest();
        body = new TestPreAuthorizationConsumeVoucherRequestBody();
        credential = createCredentialTrue();
        request.setCredential(credential);
        body.setAmount(10.05);
        body.setMpTransactionID("1234");
        body.setOperationID("56789");
        body.setPartnerType(PartnerType.MP.getValue());
        body.setProductType("OIL");
        body.setRequestTimestamp(new Date().getTime());
        body.setStationID("111111");
        body.setVoucherConsumerType(VoucherConsumerType.ENI.getValue());
        ArrayList<VoucherCodeDetail> voucherList = new ArrayList<VoucherCodeDetail>();
        VoucherCodeDetail voucherCode = new VoucherCodeDetail();
        voucherCode.setVoucherCode("00000000000");
        voucherList.add(voucherCode);
        body.setVoucherList(voucherList);
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testPreAauthorizationConsumeVoucherSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setFidelityService(new MockPreAuthorizationConsumeVoucherFidelityServiceSuccess());
        EJBHomeCache.getInstance().setAdminService(new MockPreAuthorizationConsumeVoucherFidelityAdminServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestPreAuthorizationConsumeVoucher(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (TestPreAuthorizationConsumeVoucherResponse) baseResponse.getEntity();
        assertEquals(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_OK, response.getStatus().getStatusCode());
    }

    @Test
    public void testPreAuthorizationConsumeVoucherFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockPreAuthorizationConsumeVoucherFidelityAdminServiceSuccess());
        EJBHomeCache.getInstance().setFidelityService(new MockPreAuthorizationConsumeVoucherFidelityServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestPreAuthorizationConsumeVoucher(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        Response baseResponse = frontend.adminJsonHandler(json);
        response = (TestPreAuthorizationConsumeVoucherResponse) baseResponse.getEntity();
        assertEquals(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_INVALID_VOUCHER, response.getStatus().getStatusCode());
    }

    @Test
    public void testPreAauthorizationConsumeVoucherFidelitySuccessAdminFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setFidelityService(new MockPreAuthorizationConsumeVoucherFidelityServiceSuccess());
        EJBHomeCache.getInstance().setAdminService(new MockPreAuthorizationConsumeVoucherFidelityAdminServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestPreAuthorizationConsumeVoucher(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (TestPreAuthorizationConsumeVoucherResponse) baseResponse.getEntity();
        assertEquals("-1", response.getStatus().getStatusCode());
    }

}
