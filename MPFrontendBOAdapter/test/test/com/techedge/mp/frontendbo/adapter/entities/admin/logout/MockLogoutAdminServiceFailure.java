package test.com.techedge.mp.frontendbo.adapter.entities.admin.logout;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockLogoutAdminServiceFailure extends MockAdminService{

    @Override
    public String adminLogout(String ticketId, String requestId) {
        return "ADMIN_LOGOUT_300";
    }
    
}
