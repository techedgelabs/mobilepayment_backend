package test.com.techedge.mp.frontendbo.adapter.entities.admin.createusertype;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.createusertype.CreateUserTypeResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestCreateUserTypeAdminService extends TestCase {
    private FrontendBOAdapterService frontend;
    private Response                 response;
    private String                   json;
    private CreateUserTypeResponse   createUserTypeResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"createUserType\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"code\":1}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
    }

    @Test
    public void testCreateCustomerUserSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateUserTypeAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        createUserTypeResponse = (CreateUserTypeResponse) response.getEntity();
        assertEquals("ADMIN_USERTYPE_CREATE_200", createUserTypeResponse.getStatus().getStatusCode());
    }

    @Test
    public void testCreateCustomerUserFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateUserTypeAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        createUserTypeResponse = (CreateUserTypeResponse) response.getEntity();
        assertEquals("ADMIN_USERTYPE_CREATE_300", createUserTypeResponse.getStatus().getStatusCode());
    }
}
