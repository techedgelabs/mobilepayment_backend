package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockDeleteDocumentAdminServiceFailure extends MockAdminService {
    @Override
    public String adminDeleteDocument(String adminTicketId, String requestID, String documentKey) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_DOCUMENT_DELETE_FAILURE;
    }
}
