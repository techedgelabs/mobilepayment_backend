package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatesurvey;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updatesurvey.UpdateSurveyBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatesurvey.UpdateSurveyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateSurveyRequestTest extends BaseTestCase {
    private UpdateSurveyRequest     request;
    private UpdateSurveyBodyRequest body;
    private String                  code;
    private String                  description;
    private String                  note;
    private Long                    startData;
    private Long                    endData;
    private Integer                 status;

    protected void setUp() {

        request = new UpdateSurveyRequest();
        body = new UpdateSurveyBodyRequest();

        code = "code";
        description = "descr";
        status = 4;
        startData = 1111110L;
        endData = 1111111L;
        note = "test";

        body.setNote(note);
        body.setCode(code);
        body.setDescription(description);
        body.setEndDate(endData);
        body.setStartDate(startData);
        body.setStatus(status);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_SURVEY_UPDATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialRequestFalse());
        request.setBody(null);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
