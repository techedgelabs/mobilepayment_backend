package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletemanager;

import com.techedge.mp.core.business.interfaces.Manager;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockDeleteManagerAdminServiceFailure extends MockAdminService {
    @Override
    public String adminDeleteManager(String adminTicketId, String requestId, Manager manager) {
        // TODO Auto-generated method stub
        return "ADMIN_DELETE_MANAGER_300";
    }
}
