package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethod;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethod.UpdatePaymentMethodRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdatePaymentMethodBodyRequestTest extends BaseTestCase {
    private UpdatePaymentMethodRequestBody body;
    private Long                           id;
    private String                         type;
    private Integer                        status;
    private String                         defaultMethod;
    private Integer                        attemptsLeft;
    private Double                         checkAmount;

    protected void setUp() {

        body = new UpdatePaymentMethodRequestBody();

        id = 1L;
        type = "1";
        status = 4;
        defaultMethod = "test";
        attemptsLeft = 1;
        checkAmount = 1.1;

        body.setAttemptsLeft(attemptsLeft);
        body.setCheckAmount(checkAmount);
        body.setDefaultMethod(defaultMethod);
        body.setId(id);
        body.setStatus(status);
        body.setType(type);

    }

    @Test
    public void testIdNull() {
        body.setId(null);
        assertEquals(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testTypeNull() {
        body.setType("typestypestypestypestypestypestypes");
        assertEquals(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testStatusNull() {
        body.setStatus(1234);
        assertEquals(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testDefaultMethodNull() {
        body.setDefaultMethod("prd");
        assertEquals(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testAttemptsNull() {
        body.setAttemptsLeft(30);
        assertEquals(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testCheckAmountsNull() {
        body.setCheckAmount(1000.3);
        assertEquals(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS, body.check().getStatusCode());
    }

}
