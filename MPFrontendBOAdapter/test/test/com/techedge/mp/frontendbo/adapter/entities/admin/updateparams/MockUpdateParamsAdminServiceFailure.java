package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateparams;

import java.util.List;

import com.techedge.mp.core.business.interfaces.AdminUpdateParamsResponse;
import com.techedge.mp.core.business.interfaces.ParamInfo;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockUpdateParamsAdminServiceFailure extends MockAdminService {

    @Override
    public AdminUpdateParamsResponse adminUpdateParams(String adminTicketId, String requestId, List<ParamInfo> params) {
        AdminUpdateParamsResponse data = new AdminUpdateParamsResponse();
        data.setStatusCode("ADMIN_UPDATE_PARAMS_300");
        return data;
    }
}
