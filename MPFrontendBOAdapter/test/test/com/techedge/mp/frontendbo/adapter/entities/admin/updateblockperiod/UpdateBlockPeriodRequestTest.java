package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateblockperiod;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updateblockperiod.AdminUpdateBlockPeriodBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateblockperiod.AdminUpdateBlockPeriodRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateBlockPeriodRequestTest extends BaseTestCase {

    private AdminUpdateBlockPeriodRequest     request;
    private AdminUpdateBlockPeriodBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminUpdateBlockPeriodRequest();
        body = new AdminUpdateBlockPeriodBodyRequest();
        body.setActive(true);
        body.setCode("code");
        body.setEndDate("2010-05-02");
        body.setStartDate("2010-05-02");
        body.setEndTime("10:10");
        body.setStartTime("11:11");
        body.setOperation("operation");
        body.setEndTime("12:12");
        body.setStatusCode("Error_200");
        body.setStatusMessage("message");
        request.setBody(body);
        request.setCredential(createCredentialTrue());

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_UPDATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_BLOCK_PERIOD_UPDATE_FAILURE, request.check().getStatusCode());
    }

}