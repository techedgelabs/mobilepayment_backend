package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions;

import java.util.Date;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.RetrieveTransactionListData;

public class MockRetrieveTransactionsAdminServiceSuccess extends MockAdminService {

    public RetrieveTransactionListData adminTransactionRetrieve(String adminTicketId, String requestId, String transactionId, String userId, String stationId, String pumpId,
            String finalStatusType, Boolean GFGNotification, Boolean confirmed, Date creationTimestampStart, Date creationTimestampEnd, String productID,
            Boolean transactionHistoryFlag) {
        RetrieveTransactionListData data = new RetrieveTransactionListData();
        data.setStatusCode("ADMIN_RETR_TRANSACTIONS_200");
        return data;
    }

}
