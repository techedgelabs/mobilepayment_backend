package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateparam;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updateparam.UpdateParamRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateParamBodyRequestTest extends BaseTestCase {
    private UpdateParamRequestBody body;
    private String                 param;
    private String                 value;
    private String                 operation;

    //    private List<String>             params = new ArrayList<String>(0);

    protected void setUp() {

        body = new UpdateParamRequestBody();

        operation = "ope";
        value = "value";
        param = "param";

        body.setOperation(operation);
        body.setParam(param);
        body.setValue(value);

    }

    @Test
    public void testOperationNull() {
        body.setOperation(null);
        assertEquals(StatusCode.ADMIN_UPDATE_PARAM_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testValueNull() {
        body.setValue(null);
        assertEquals(StatusCode.ADMIN_UPDATE_PARAM_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testParamNull() {
        body.setParam(null);
        assertEquals(StatusCode.ADMIN_UPDATE_PARAM_ERROR_PARAMETERS, body.check().getStatusCode());
    }
}
