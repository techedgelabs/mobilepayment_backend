package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrievevouchertransactions.RetrieveVoucherTransactionsRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveVoucherTransactionsBodyRequestTest extends BaseTestCase {
    private RetrieveVoucherTransactionsRequestBody body;

    // assigning the values
    protected void setUp() {
        body = new RetrieveVoucherTransactionsRequestBody();
        body.setCreationTimestampEnd(1L);
        body.setCreationTimestampStart(2L);
        body.setFinalStatusType("OK");
        body.setUserId(1L);
        body.setVoucherCode("code");
        body.setVoucherTransactionId("transacttransacttransacttransact");
    }

    @Test
    public void testVoucherTransactionNot32() {
        body.setVoucherTransactionId("ss");
        assertEquals(StatusCode.RETRIEVE_VOUCHER_INVALID_PARAMETES, body.check().getStatusCode());
    }

    @Test
    public void testFinalStatusMajor() {
        body.setFinalStatusType("qwertqwertqwertqwertqwertqwertqwert");
        assertEquals(StatusCode.RETRIEVE_VOUCHER_INVALID_PARAMETES, body.check().getStatusCode());
    }

    @Test
    public void testVoucherCodeMajor() {
        body.setVoucherCode("qwertqwertqwertqwertqwertqwertqwert");
        assertEquals(StatusCode.RETRIEVE_VOUCHER_INVALID_PARAMETES, body.check().getStatusCode());
    }
}