package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager.UpdateManagerBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatemanager.UpdateManagerDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateManagerBodyRequestTest extends BaseTestCase {
    private UpdateManagerBodyRequest body;
    private UpdateManagerDataRequest managerData;
    private Long                     id;
    private String                   email;
    private String                   username;

    //    private List<String>             params = new ArrayList<String>(0);

    protected void setUp() {

        body = new UpdateManagerBodyRequest();
        managerData = new UpdateManagerDataRequest();

        id = 1L;
        username = "user";
        email = "a@a.it";

        managerData.setEmail(email);
        managerData.setId(id);
        managerData.setUsername(username);

        body.setManagerData(managerData);

    }

    @Test
    public void testEmailNull() {
        managerData.setEmail(null);
        assertEquals(StatusCode.ADMIN_UPDATE_MANAGER_INVALID_PARAMETERS, managerData.check().getStatusCode());
    }

    @Test
    public void testEmailIsEmpty() {
        managerData.setEmail("");
        assertEquals(StatusCode.ADMIN_UPDATE_MANAGER_INVALID_PARAMETERS, managerData.check().getStatusCode());
    }

    @Test
    public void testUsernameNull() {
        managerData.setUsername(null);
        assertEquals(StatusCode.ADMIN_UPDATE_MANAGER_INVALID_PARAMETERS, managerData.check().getStatusCode());
    }

    @Test
    public void testUsernameIsEmpty() {
        managerData.setUsername("");
        assertEquals(StatusCode.ADMIN_UPDATE_MANAGER_INVALID_PARAMETERS, managerData.check().getStatusCode());
    }

    @Test
    public void testIDNegative() {
        managerData.setId(-3L);
        assertEquals(StatusCode.ADMIN_UPDATE_MANAGER_INVALID_PARAMETERS, managerData.check().getStatusCode());
    }

    @Test
    public void testIDNull() {
        managerData.setId(null);
        assertEquals(StatusCode.ADMIN_UPDATE_MANAGER_INVALID_PARAMETERS, managerData.check().getStatusCode());
    }
}
