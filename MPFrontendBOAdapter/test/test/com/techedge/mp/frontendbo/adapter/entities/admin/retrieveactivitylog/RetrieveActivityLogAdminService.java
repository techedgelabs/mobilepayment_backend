package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog.RetrieveActivityLogResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class RetrieveActivityLogAdminService extends TestCase {
    private FrontendBOAdapterService    frontend;
    private Response                    response;
    private String                      json;
    private RetrieveActivityLogResponse retrieveActivityLogAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"retrieveActivityLog\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"start\":1469720032,\"end\":1449720032,\"source\":\"source\",\"groupId\":\"groupId\",\"phaseId\":\"phaseId\",\"messagePattern\":\"messagePattern\",\"minLevel\":\"TRACE\",\"maxLevel\":\"TRACE\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveActivityLogAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        retrieveActivityLogAdminResponse = (RetrieveActivityLogResponse) response.getEntity();
        assertEquals("ADMIN_RETR_LOG_200", retrieveActivityLogAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveActivityLogAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        retrieveActivityLogAdminResponse = (RetrieveActivityLogResponse) response.getEntity();
        assertEquals("ADMIN_RETR_LOG_300", retrieveActivityLogAdminResponse.getStatus().getStatusCode());
    }
}
