package test.com.techedge.mp.frontendbo.adapter.entities.admin.createusercategory;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createusercategory.CreateUserCategoryBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createusercategory.CreateUserCategoryRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddUserCategoryRequestTest extends BaseTestCase {

    private CreateUserCategoryRequest     request;
    private CreateUserCategoryBodyRequest body;
    private String                        name;

    // assigning the values
    protected void setUp() {
        request = new CreateUserCategoryRequest();
        body = new CreateUserCategoryBodyRequest();
        name = "name";
        body.setName(name);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_USER_CATEGORY_CREATE, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_USER_CATEGORY_CREATE_FAILURE, request.check().getStatusCode());
    }

}