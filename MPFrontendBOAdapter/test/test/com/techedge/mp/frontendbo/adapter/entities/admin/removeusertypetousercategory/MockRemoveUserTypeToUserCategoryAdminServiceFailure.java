package test.com.techedge.mp.frontendbo.adapter.entities.admin.removeusertypetousercategory;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockRemoveUserTypeToUserCategoryAdminServiceFailure extends MockAdminService {

    @Override
    public String adminRemoveTypeFromUserCategory(String adminTicketId, String requestId, String name, Integer code) {
        // TODO Auto-generated method stub
        return "ADMIN_USERTYPE_CATEGORY_CREATE_300";
    }
}
