package test.com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion.AddVoucherToPromotionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion.AddVoucherToPromotionRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion.AddVoucherToPromotionResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestAddVoucherToPromotionAdminService extends BaseTestCase {
    private FrontendBOAdapterService         frontend;
    private AddVoucherToPromotionRequest     request;
    private AddVoucherToPromotionRequestBody body;
    private AddVoucherToPromotionResponse    response;
    private Response                         baseResponse;
    private String                           json;
    private Gson                             gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest                     adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AddVoucherToPromotionRequest();
        body = new AddVoucherToPromotionRequestBody();
        String promotionCode = "promo";
        PromoVoucherInput voucherInfo = new PromoVoucherInput();
        voucherInfo.getPromoAssociation().put("test", 1.1);
        body.setPromotionCode(promotionCode);
        body.setVoucherInfo(voucherInfo);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
        request.setBody(body);
        adminRequest = new AdminRequest();
        adminRequest.setAddVoucherToPromotion(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
    }

    @Test
    public void testAddVoucherToPromotionSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAddVoucherToPromotionAdminServiceSuccess());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AddVoucherToPromotionResponse) baseResponse.getEntity();
        assertEquals("ADMIN_ADD_VOUCHER_TO_PROMOTION_200", response.getStatus().getStatusCode());
    }

    @Test
    public void testAddVoucherToPromotionFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAddVoucherToPromotionAdminServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AddVoucherToPromotionResponse) baseResponse.getEntity();
        assertEquals("ADMIN_ADD_VOUCHER_TO_PROMOTION_301", response.getStatus().getStatusCode());
    }
}
