package test.com.techedge.mp.frontendbo.adapter.entities.admin.reconciliation;

import java.util.List;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;

import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationTransactionSummary;

public class MockReconciliationRequestAdminServiceFailure extends MockReconciliationService {

    @Override
    public ReconciliationTransactionSummary reconciliationTransaction(List<String> transactionsIDList) {
        ReconciliationTransactionSummary sum = new ReconciliationTransactionSummary();
        sum.setStatusCode("RECONCILIATION_RECONCILE_300");
        return sum;
    }

}
