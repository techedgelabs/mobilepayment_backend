package test.com.techedge.mp.frontendbo.adapter.entities.admin.addusertypetousercategory;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockAddUserTypeToUserCategoryAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminAddTypeToUserCategory(String adminTicketId, String requestId, String name, Integer code) {
        // TODO Auto-generated method stub
        return "ADMIN_USERTYPE_CATEGORY_UPDATE_200";
    }
}
