package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletesurveyquestion;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.deletesurveyquestion.DeleteSurveyQuestionBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class DeleteSurveyQuestionBodyRequestTest extends BaseTestCase {
    private DeleteSurveyQuestionBodyRequest body;
    private String                          code;

    // assigning the values
    protected void setUp() {
        body = new DeleteSurveyQuestionBodyRequest();
        code = "test";
        body.setCode(code);

    }

    @Test
    public void testCodeNull() {
        body.setCode(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testCodeIsEmpty() {
        body.setCode("");
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }
}
