package test.com.techedge.mp.frontendbo.adapter.entities.admin.archive;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.archive.ArchiveRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.archive.ArchiveRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddArchiveRequestTest extends BaseTestCase {

    private ArchiveRequest     request;
    private ArchiveRequestBody body;
    private String             transactionID;

    // assigning the values
    protected void setUp() {
        request = new ArchiveRequest();
        body = new ArchiveRequestBody();
        transactionID = "01ds";
        body.setTransactionID(transactionID);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_UPDATE_PARAM_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    //        @Test
    //        public void testBodyNull() {
    //    
    //            request.setCredential(createCredentialTrue());
    //            request.setBody(null);
    //    
    //            assertEquals(StatusCode.ADMIN_USER_TYPE_CATEGORY_UPDATE_FAILURE, request.check().getStatusCode());
    //        }
}
