package test.com.techedge.mp.frontendbo.adapter.entities.admin.createdocument;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createdocument.AdminCreateDocumentBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class CreateDocumentBodyRequestTest extends BaseTestCase {

    private AdminCreateDocumentBodyRequest body;

    // assigning the values
    protected void setUp() {
        body = new AdminCreateDocumentBodyRequest();
        body.setPosition(1);
        body.setDocumentKey("documentKey");
        body.setTemplateFile("template");
        body.setTitle("title");
        body.setUserCategory("NUOVO_FLUSSO");

    }

    @Test
    public void testDocumentKeyNull() {
        body.setDocumentKey(null);
        assertEquals(StatusCode.ADMIN_DOCUMENT_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testDocumentKeyIsEmpty() {
        body.setDocumentKey("");
        assertEquals(StatusCode.ADMIN_DOCUMENT_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

    @Test
    public void testPositionNull() {
        body.setPosition(null);
        assertEquals(StatusCode.ADMIN_DOCUMENT_CREATE_CHECK_FAILURE, body.check().getStatusCode());
    }

}