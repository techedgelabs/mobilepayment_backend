package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievestatistics;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.RetrieveStatisticsData;

public class MockRetrieveStatisticsAdminServiceSuccess extends MockAdminService {

    @Override
    public RetrieveStatisticsData adminRetrieveStatistics(String adminTicketId, String requestId) {
        RetrieveStatisticsData data = new RetrieveStatisticsData();
        data.setStatusCode("ADMIN_RETRIEVE_STATISTICS_200");
        return data;
    }

}
