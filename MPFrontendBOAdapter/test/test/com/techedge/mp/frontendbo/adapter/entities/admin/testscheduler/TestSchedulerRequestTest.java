package test.com.techedge.mp.frontendbo.adapter.entities.admin.testscheduler;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.testscheduler.TestSchedulerRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testscheduler.TestSchedulerRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestSchedulerRequestTest extends BaseTestCase {
    private TestSchedulerRequest     request;
    private TestSchedulerRequestBody body;
    private String                   action;

    //    private List<String>             params = new ArrayList<String>(0);

    protected void setUp() {

        request = new TestSchedulerRequest();
        body = new TestSchedulerRequestBody();
        body.setAction(action);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_TEST_SCHEDULER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
