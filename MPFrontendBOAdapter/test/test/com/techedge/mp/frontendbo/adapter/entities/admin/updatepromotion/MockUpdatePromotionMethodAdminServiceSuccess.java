package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatepromotion;

import java.util.Date;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockUpdatePromotionMethodAdminServiceSuccess extends MockAdminService {

    @Override
    public String adminUpdatePromotion(String adminTicketId, String requestId, String code, String description, Integer status, Date startData, Date endData) {
        // TODO Auto-generated method stub
        return "ADMIN_UPDATE_USER_200";
    }
}
