package test.com.techedge.mp.frontendbo.adapter.entities.admin.stationoperation;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.StationsAdminData;

public class MockStationsAdminServiceFailure extends MockAdminService {
    @Override
    public String adminAddStation(String adminTicketId, String requestId, Station station) {
        return "ADMIN_CREATE_300";
    }
    
    @Override
    public String adminDeleteStation(String adminTicketId, String requestId, Station station) {
        return "ADMIN_CREATE_300";
    }

    @Override
    public StationsAdminData adminSearchStations(String adminTicketId, String requestId, Station station) {
        StationsAdminData data = new StationsAdminData();
        data.setStatusCode("ADMIN_STATION_300");
        return data;
    }

    @Override
    public String adminUpdateStation(String adminTicketId, String requestId, Station station) {
        // TODO Auto-generated method stub
        return "ADMIN_CREATE_300";
    }
}
