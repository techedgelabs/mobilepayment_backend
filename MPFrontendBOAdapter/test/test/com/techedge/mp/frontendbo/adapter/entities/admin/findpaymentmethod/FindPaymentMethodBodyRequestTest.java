package test.com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.findpaymentmethod.FindPaymentMethodRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class FindPaymentMethodBodyRequestTest extends BaseTestCase {
    private FindPaymentMethodRequestBody body;
    private Long                         id;
    private String                       type;

    // assigning the values
    protected void setUp() {
        body = new FindPaymentMethodRequestBody();
        id = 1L;
        type = "tester";
        body.setId(id);
        body.setType(type);
    }

    @Test
    public void testIdNull() {
        body.setId(null);
        assertEquals(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS, body.check().getStatusCode());
    }

    @Test
    public void testTypeMajor() {
        body.setType("test1test1test1test1test1test1test1test1");
        assertEquals(StatusCode.ADMIN_UPDATE_PAYMENT_METHOD_ERROR_PARAMETERS, body.check().getStatusCode());
    }
}