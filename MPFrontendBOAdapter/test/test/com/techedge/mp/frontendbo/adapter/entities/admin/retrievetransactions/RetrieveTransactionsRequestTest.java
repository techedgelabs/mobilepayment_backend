package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions.RetrieveTransactionsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions.RetrieveTransactionsRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveTransactionsRequestTest extends BaseTestCase {
    private RetrieveTransactionsRequest     request;
    private RetrieveTransactionsRequestBody body;
    private String                          transactionId;
    private String                          userId;
    private String                          stationId;
    private String                          pumpId;
    private String                          finalStatusType;
    private Boolean                         GFGNotification;
    private Boolean                         confirmed;
    private Long                            creationTimestampStart;
    private Long                            creationTimestampEnd;
    private String                          productID;
    private Boolean                         transactionHistoryFlag;

    // assigning the values
    protected void setUp() {
        request = new RetrieveTransactionsRequest();
        body = new RetrieveTransactionsRequestBody();
        transactionId = "transactionID";
        userId = "user";
        stationId = "stationID";
        pumpId = "source";
        finalStatusType = "MISSING_PAYMENT";
        GFGNotification = true;
        confirmed = true;
        creationTimestampEnd = 100L;
        creationTimestampStart = 99L;
        productID = "prodID";
        transactionHistoryFlag = false;

        body.setTransactionId(transactionId);
        body.setUserId(userId);
        body.setStationId(stationId);
        body.setPumpId(pumpId);
        body.setFinalStatusType(finalStatusType);
        body.setGFGNotification(GFGNotification);
        body.setConfirmed(confirmed);
        body.setCreationTimestampEnd(creationTimestampEnd);
        body.setCreationTimestampStart(creationTimestampStart);
        body.setProductID(productID);
        body.setTransactionHistoryFlag(transactionHistoryFlag);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {
        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialRequestFalse());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
