package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateuser;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updateuser.UpdateUserRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateuser.UpdateUserRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateUserRequestTest extends BaseTestCase {
    private UpdateUserRequest     request;
    private UpdateUserRequestBody body;

    private Long                  id;
    private String                language;
    private Integer               status;
    private String                registrationCompleted;
    private Double                capAvailable;
    private Double                capEffective;
    private String                externalUserId;
    private Integer               userType;

    protected void setUp() {

        request = new UpdateUserRequest();
        body = new UpdateUserRequestBody();

        id = 1L;
        language = "ITA";
        status = 0;
        registrationCompleted = "true";
        capAvailable = 4.2;
        capEffective = 3.2;
        externalUserId = "useri";
        userType = 4;

        body.setCapAvailable(capAvailable);
        body.setCapEffective(capEffective);
        body.setExternalUserId(externalUserId);
        body.setId(id);
        body.setLanguage(language);
        body.setRegistrationCompleted(registrationCompleted);
        body.setStatus(status);
        body.setUserType(userType);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_UPDATE_USER_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialRequestFalse());
        request.setBody(null);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
