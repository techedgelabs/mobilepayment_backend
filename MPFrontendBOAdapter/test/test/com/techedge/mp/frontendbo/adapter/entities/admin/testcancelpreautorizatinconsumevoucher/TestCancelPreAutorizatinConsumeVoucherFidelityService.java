package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreautorizatinconsumevoucher;

import java.util.Date;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreauthorizationconsumevoucher.TestCancelPreAuthorizationConsumeVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreauthorizationconsumevoucher.TestCancelPreAuthorizationConsumeVoucherRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcancelpreauthorizationconsumevoucher.TestCancelPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.Credential;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestCancelPreAutorizatinConsumeVoucherFidelityService extends BaseTestCase {
    private FrontendBOAdapterService                            frontend;
    private Response                                            baseResponse;
    private String                                              json;
    private Gson                                                gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private TestCancelPreAuthorizationConsumeVoucherRequest     request;
    private TestCancelPreAuthorizationConsumeVoucherRequestBody body;
    private TestCancelPreAuthorizationConsumeVoucherResponse    response;
    private Credential                                          credential;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        request = new TestCancelPreAuthorizationConsumeVoucherRequest();
        body = new TestCancelPreAuthorizationConsumeVoucherRequestBody();
        credential = createCredentialTrue();
        request.setCredential(credential);
        body.setOperationID("56789");
        body.setPartnerType(PartnerType.MP.getValue());
        body.setRequestTimestamp(new Date().getTime());
        body.setPreAuthOperationIDToCancel("preAuth");
        request.setBody(body);
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testPreAauthorizationConsumeVoucherSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setFidelityService(new MockCancelPreAutorizatinConsumeVoucherFidelityServiceSuccess());
        EJBHomeCache.getInstance().setAdminService(new MockCancelPreAutorizatinConsumeVoucherAdminServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestCancelPreAuthorizationConsumeVoucher(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (TestCancelPreAuthorizationConsumeVoucherResponse) baseResponse.getEntity();
        assertEquals(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_OK, response.getStatus().getStatusCode());
    }

    @Test
    public void testPreAuthorizationConsumeVoucherFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCancelPreAutorizatinConsumeVoucherAdminServiceSuccess());
        EJBHomeCache.getInstance().setFidelityService(new MockCancelPreAutorizatinConsumeVoucherFidelityServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestCancelPreAuthorizationConsumeVoucher(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        Response baseResponse = frontend.adminJsonHandler(json);
        response = (TestCancelPreAuthorizationConsumeVoucherResponse) baseResponse.getEntity();
        assertEquals(FidelityResponse.PRE_AUTHORIZATION_CONSUME_VOUCHER_GENERIC_ERROR, response.getStatus().getStatusCode());
    }

    @Test
    public void testPreAauthorizationConsumeVoucherFidelitySuccessAdminFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setFidelityService(new MockCancelPreAutorizatinConsumeVoucherFidelityServiceSuccess());
        EJBHomeCache.getInstance().setAdminService(new MockCancelPreAutorizatinConsumeVoucherAdminServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setTestCancelPreAuthorizationConsumeVoucher(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (TestCancelPreAuthorizationConsumeVoucherResponse) baseResponse.getEntity();
        assertEquals("-1", response.getStatus().getStatusCode());
    }

}
