package test.com.techedge.mp.frontendbo.adapter.webservices.common;

import java.util.List;

import com.techedge.mp.core.business.ReconciliationServiceRemote;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationDetail;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfoData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationParkingData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationParkingSummary;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationTransactionSummary;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationUserData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationUserSummary;

public class MockReconciliationService implements ReconciliationServiceRemote {

    @Override
    public ReconciliationInfoData reconciliationReconcileTransactions(String adminTicketId, String requestId, List<String> transactionsID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ReconciliationDetail getReconciliationDetail(String adminTicketId, String requestId, String transactionsID) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ReconciliationTransactionSummary reconciliationTransaction(List<String> transactionsIDList) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ReconciliationUserSummary reconciliationUser(List<ReconciliationUserData> userReconciliationInterfaceIDList) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ReconciliationParkingSummary reconciliationParking(List<ReconciliationParkingData> parkingReconciliationInterfaceIDList) {
        // TODO Auto-generated method stub
        return null;
    }

}
