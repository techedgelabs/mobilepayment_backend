package test.com.techedge.mp.frontendbo.adapter.entities.admin.addcardbin;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.addcardbin.AddCardBinBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addcardbin.AddCardBinRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddCardBinRequestTest extends BaseTestCase {
    private AddCardBinBodyRequest body;
    private AddCardBinRequest     request;
    private String                bin;

    // assigning the values
    protected void setUp() {
        body = new AddCardBinBodyRequest();
        request = new AddCardBinRequest();
    }

    @Test
    public void testCredentialTrue() {

        bin = "13";
        body.setBin(bin);
        request.setCredential(createCredentialTrue());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_ADD_CARD_BIN_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        bin = "13";
        body.setBin(bin);
        request.setCredential(createCredentialFalse());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        bin = "13";
        body.setBin(bin);
        request.setCredential(createCredentialTicketFalse());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        bin = "13";
        body.setBin(bin);
        request.setCredential(createCredentialRequestFalse());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testCredentialBodyNull() {

        request.setCredential(createCredentialRequestFalse());
        request.setBody(null);

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
    
    @Test
    public void testBinFalse() {

        bin = null;
        body.setBin(bin);
        request.setCredential(createCredentialTrue());
        request.setBody(body);

        assertEquals(StatusCode.ADMIN_ADD_CARD_BIN_INVALID_REQUEST, request.check().getStatusCode());
    }

}