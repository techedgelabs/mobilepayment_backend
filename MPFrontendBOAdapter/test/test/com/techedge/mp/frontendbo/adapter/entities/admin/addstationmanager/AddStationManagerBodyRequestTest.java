package test.com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager.AddStationManagerBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddStationManagerBodyRequestTest extends BaseTestCase {

    private AddStationManagerBodyRequest body;
    private Long                         managerID;
    private Long                         stationID;

    // assigning the values
    protected void setUp() {
        body = new AddStationManagerBodyRequest();
    }

    @Test
    public void testManagerNull() {

        managerID = null;
        stationID = 1L;
        body.setManagerID(managerID);
        body.setStationID(stationID);

        assertEquals(StatusCode.ADMIN_ADDSTATION_MANAGER_INVALID_PARAMETERS, body.check().getStatusCode());

    }

    @Test
    public void testManagerMinor() {

        managerID = -3L;
        stationID = 1L;
        body.setManagerID(managerID);
        body.setStationID(stationID);

        assertEquals(StatusCode.ADMIN_ADDSTATION_MANAGER_INVALID_PARAMETERS, body.check().getStatusCode());

    }

    @Test
    public void testStationIDNull() {

        managerID = 1L;
        stationID = null;
        body.setManagerID(managerID);
        body.setStationID(stationID);

        assertEquals(StatusCode.ADMIN_ADDSTATION_MANAGER_INVALID_PARAMETERS, body.check().getStatusCode());

    }

    @Test
    public void testStationIDMinor() {

        managerID = 1L;
        stationID = -1L;
        body.setManagerID(managerID);
        body.setStationID(stationID);

        assertEquals(StatusCode.ADMIN_ADDSTATION_MANAGER_INVALID_PARAMETERS, body.check().getStatusCode());

    }

}