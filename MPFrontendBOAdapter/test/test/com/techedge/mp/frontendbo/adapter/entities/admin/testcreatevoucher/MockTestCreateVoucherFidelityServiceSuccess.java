package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher;

import java.math.BigDecimal;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CreateVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockTestCreateVoucherFidelityServiceSuccess extends MockFidelityService {

    @Override
    public CreateVoucherResult createVoucher(VoucherConsumerType voucherType, BigDecimal totalAmount, String bankTransactionID, String shopTransactionID, String authorizationCode,
            String operationID, PartnerType partnerType, Long requestTimestamp) throws FidelityServiceException {
        CreateVoucherResult result = new CreateVoucherResult();
        result.setCsTransactionID("src");
        result.setMessageCode("message");
        result.setStatusCode(StatusCode.ADMIN_TEST_CREATE_VOUCHER_SUCCESS);
        return result;
    }
}
