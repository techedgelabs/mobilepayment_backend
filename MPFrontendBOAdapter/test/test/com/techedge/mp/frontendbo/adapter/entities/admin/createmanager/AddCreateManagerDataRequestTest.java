package test.com.techedge.mp.frontendbo.adapter.entities.admin.createmanager;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createmanager.CreateManagerDataRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddCreateManagerDataRequestTest extends BaseTestCase {

    private CreateManagerDataRequest data;
    private String                   username;
    private String                   password;
    private String                   email;

    // assigning the values
    protected void setUp() {

        data = new CreateManagerDataRequest();
        username = "user";
        password = "password123";
        email = "user@email.com";
        data.setEmail(email);
        data.setPassword(password);
        data.setUsername(username);

    }

    @Test
    public void testUsernameNull() {
        data.setUsername(null);
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_INVALID_PARAMETERS, data.check().getStatusCode());
    }

    @Test
    public void testUsernameIsEmpty() {
        data.setUsername("");
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_INVALID_PARAMETERS, data.check().getStatusCode());
    }

    @Test
    public void testPasswordNull() {
        data.setPassword(null);
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_INVALID_PARAMETERS, data.check().getStatusCode());
    }

    @Test
    public void testPasswordIsEmpty() {
        data.setPassword("");
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_INVALID_PARAMETERS, data.check().getStatusCode());
    }

    @Test
    public void testEmailNull() {
        data.setEmail(null);
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_INVALID_PARAMETERS, data.check().getStatusCode());
    }

    @Test
    public void testEmailIsEmpty() {
        data.setEmail("");
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_INVALID_PARAMETERS, data.check().getStatusCode());
    }

    @Test
    public void testUsernamePassword() {
        data.setPassword(username);
        assertEquals(StatusCode.ADMIN_CREATE_MANAGER_USERNAME_PASSWORD_EQUALS, data.check().getStatusCode());
    }
}