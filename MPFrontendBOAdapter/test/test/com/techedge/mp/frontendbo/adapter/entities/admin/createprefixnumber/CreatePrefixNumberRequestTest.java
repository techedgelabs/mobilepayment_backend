package test.com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber.CreatePrefixNumberBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createprefixnumber.CreatePrefixNumberRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class CreatePrefixNumberRequestTest extends BaseTestCase {

    private CreatePrefixNumberRequest     request;
    private CreatePrefixNumberBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new CreatePrefixNumberRequest();
        body = new CreatePrefixNumberBodyRequest();
        body.setCode("code");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.PREFIX_CREATE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}