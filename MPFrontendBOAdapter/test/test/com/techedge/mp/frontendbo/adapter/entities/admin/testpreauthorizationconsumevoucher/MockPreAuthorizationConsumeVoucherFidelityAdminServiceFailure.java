package test.com.techedge.mp.frontendbo.adapter.entities.admin.testpreauthorizationconsumevoucher;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockPreAuthorizationConsumeVoucherFidelityAdminServiceFailure extends MockAdminService {
    @Override
    public String adminCheckAdminAuthorization(String adminTicketId, String operation) {
        return "ADMIN_CHECK_AUTH_300";
    }
}
