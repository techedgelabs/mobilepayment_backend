package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievetransactions.RetrieveTransactionsResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class RetrieveTransactionsAdminService extends TestCase {
    private FrontendBOAdapterService     frontend;
    private Response                     response;
    private String                       json;
    private RetrieveTransactionsResponse retrieveTransactionsAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"retrieveTransactions\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"transactionId\":\"abc\",\"userId\":\"test\",\"stationId\":\"pre\",\"pumpId\":\"123\",\"finalStatusType\":\"FAILED\",\"GFGNotification\":\"1\",\"confirmed\":\"0\",\"creationTimestampStart\":\"000000\",\"creationTimestampEnd\":\"00000000\",\"productID\":\"chi\",\"transactionHistoryFlag\":\"1\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveTransactionsAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        retrieveTransactionsAdminResponse = (RetrieveTransactionsResponse) response.getEntity();
        assertEquals("ADMIN_RETR_TRANSACTIONS_200", retrieveTransactionsAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveTransactionsAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        retrieveTransactionsAdminResponse = (RetrieveTransactionsResponse) response.getEntity();
        assertEquals("ADMIN_RETR_TRANSACTIONS_300", retrieveTransactionsAdminResponse.getStatus().getStatusCode());
    }
}
