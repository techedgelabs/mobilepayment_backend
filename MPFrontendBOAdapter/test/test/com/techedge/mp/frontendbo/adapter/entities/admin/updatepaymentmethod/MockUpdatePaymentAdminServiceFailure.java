package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatepaymentmethod;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockUpdatePaymentAdminServiceFailure extends MockAdminService {
    @Override
    public String adminPaymentMethodUpdate(String adminTicketId, String requestId, Long id, String type, Integer status, String defaultMethod, Integer attemptsLeft,
            Double checkAmount) {
        // TODO Auto-generated method stub
        return "ADMIN_UPDATE_PAYMENT_METHOD_300";
    }
}
