package test.com.techedge.mp.frontendbo.adapter.entities.admin.createusertype;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createusertype.CreateUserTypeBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createusertype.CreateUserTypeRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddUserTypeRequestTest extends BaseTestCase {

    private CreateUserTypeRequest     request;
    private CreateUserTypeBodyRequest body;
    private Integer                   code;

    // assigning the values
    protected void setUp() {
        request = new CreateUserTypeRequest();
        body = new CreateUserTypeBodyRequest();
        code = 1;
        body.setCode(code);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_USER_TYPE_CREATE, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {
        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_USER_TYPE_CREATE_FAILURE, request.check().getStatusCode());
    }

}