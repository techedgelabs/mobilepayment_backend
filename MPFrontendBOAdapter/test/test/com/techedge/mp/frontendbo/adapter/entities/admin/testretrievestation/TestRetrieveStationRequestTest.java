package test.com.techedge.mp.frontendbo.adapter.entities.admin.testretrievestation;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.testretrievestationdetails.TestRetrieveStationRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testretrievestationdetails.TestRetrieveStationRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestRetrieveStationRequestTest extends BaseTestCase {
    private TestRetrieveStationRequest     request;
    private TestRetrieveStationRequestBody body;
    private String                         pumpID;
    private String                         requestID;
    private String                         stationID;
    private Boolean                        pumpDetailsReq;

    protected void setUp() {

        request = new TestRetrieveStationRequest();
        body = new TestRetrieveStationRequestBody();

        pumpID = "id";
        requestID = "request";
        stationID = "station";
        pumpDetailsReq = true;

        body.setPumpDetailsReq(pumpDetailsReq);
        body.setPumpID(pumpID);
        body.setRequestID(requestID);
        body.setStationID(stationID);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
