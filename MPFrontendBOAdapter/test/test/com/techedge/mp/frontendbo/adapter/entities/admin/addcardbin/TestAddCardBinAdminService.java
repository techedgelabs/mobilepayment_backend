package test.com.techedge.mp.frontendbo.adapter.entities.admin.addcardbin;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addcardbin.AddCardBinBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addcardbin.AddCardBinRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addcardbin.AddCardBinResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestAddCardBinAdminService extends BaseTestCase {
    private FrontendBOAdapterService frontend;
    private AddCardBinRequest        request;
    private AddCardBinBodyRequest    body;
    private AddCardBinResponse       response;
    private Response                 baseResponse;
    private String                   json;
    private Gson                     gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest             adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException, ParameterNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AddCardBinRequest();
        body = new AddCardBinBodyRequest();
        body.setBin("13");
        request.setBody(body);
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());

        adminRequest = new AdminRequest();
        adminRequest.setAddCardBin(request);
        json = gson.toJson(adminRequest, AdminRequest.class);

    }

    @Test
    public void testAddCardBinSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAddCardBinAdminServiceSuccess());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AddCardBinResponse) baseResponse.getEntity();
        assertEquals("ADMIN_CARD_BIN_ADD_200", response.getStatus().getStatusCode());
    }

    @Test
    public void testAddCardBinFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAddCardBinAdminServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AddCardBinResponse) baseResponse.getEntity();
        assertEquals("ADMIN_CARD_BIN_ADD_300", response.getStatus().getStatusCode());
    }
}
