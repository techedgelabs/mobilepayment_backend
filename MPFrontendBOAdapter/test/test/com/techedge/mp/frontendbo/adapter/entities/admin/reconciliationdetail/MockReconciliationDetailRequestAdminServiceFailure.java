package test.com.techedge.mp.frontendbo.adapter.entities.admin.reconciliationdetail;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationDetail;

public class MockReconciliationDetailRequestAdminServiceFailure extends MockAdminService {

    @Override
    public ReconciliationDetail adminReconcileDetail(String adminTicketId, String requestId, String transactionID) {
        ReconciliationDetail detail = new ReconciliationDetail();
        detail.setStatusCode("RECONCILIATION_DETAIL_300");
        return detail;
    }
}
