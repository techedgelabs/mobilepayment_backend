package test.com.techedge.mp.frontendbo.adapter.entities.admin.testenableloyaltycard;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.admin.testenableloyaltycard.TestEnableLoyaltyCardBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testenableloyaltycard.TestEnableLoyaltyCardRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class TestEnableLoyaltyCardRequestTest extends BaseTestCase {
    private TestEnableLoyaltyCardRequest     request;
    private TestEnableLoyaltyCardBodyRequest body;
    private String                           operationID;
    private String                           fiscalCode;
    private String                           panCode;
    private Boolean                          enable;
    private PartnerType                      partnerType;
    private Long                             requestTimestamp;

    protected void setUp() {

        request = new TestEnableLoyaltyCardRequest();
        body = new TestEnableLoyaltyCardBodyRequest();
        operationID = "operation";
        fiscalCode = "check";
        panCode = "panCode";
        enable = true;
        partnerType = PartnerType.MP;
        requestTimestamp = 1010101L;

        body.setOperationID(operationID);
        body.setFiscalCode(fiscalCode);
        body.setPanCode(panCode);
        body.setEnable(enable);
        body.setPartnerType(partnerType.getValue());
        body.setRequestTimestamp(requestTimestamp);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_TEST_ENABLE_LOYALTY_CARD_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
