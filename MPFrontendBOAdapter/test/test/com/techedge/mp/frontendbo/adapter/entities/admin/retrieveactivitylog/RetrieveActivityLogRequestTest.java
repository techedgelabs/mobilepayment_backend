package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog.RetrieveActivityLogRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveactivitylog.RetrieveActivityLogRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveActivityLogRequestTest extends BaseTestCase {
    private RetrieveActivityLogRequest     request;
    private RetrieveActivityLogRequestBody body;
    private ErrorLevel                     minLevel;
    private ErrorLevel                     maxLevel;
    private Long                           start;
    private Long                           end;
    private String                         source;
    private String                         groupId;
    private String                         phaseId;
    private String                         messagePattern;

    // assigning the values
    protected void setUp() {
        request = new RetrieveActivityLogRequest();
        body = new RetrieveActivityLogRequestBody();
        maxLevel = ErrorLevel.buildErrorLevel(1);
        minLevel = ErrorLevel.buildErrorLevel(1);
        start = 10L;
        end = 11L;
        source = "test";
        groupId = "id";
        phaseId = "test";
        messagePattern = "patter";
        body.setGroupId(groupId);
        body.setMinLevel(minLevel);
        body.setMaxLevel(maxLevel);
        body.setEnd(end);
        body.setMessagePattern(messagePattern);
        body.setPhaseId(phaseId);
        body.setSource(source);
        body.setStart(start);
        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {
        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialRequestFalse());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }
}
