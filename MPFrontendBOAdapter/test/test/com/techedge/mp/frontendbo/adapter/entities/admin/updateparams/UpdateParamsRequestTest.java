package test.com.techedge.mp.frontendbo.adapter.entities.admin.updateparams;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updateparams.UpdateParamsRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.updateparams.UpdateParamsRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.ParameterInfo;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateParamsRequestTest extends BaseTestCase {
    private UpdateParamsRequest request;
    private UpdateParamsRequestBody body;
    private List<ParameterInfo> parameterList = new ArrayList<ParameterInfo>(0);


    protected void setUp() {

        request = new UpdateParamsRequest();
        body = new UpdateParamsRequestBody();
        
        body.setParameterList(parameterList);

        request.setBody(body);

    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_UPDATE_PARAM_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}
