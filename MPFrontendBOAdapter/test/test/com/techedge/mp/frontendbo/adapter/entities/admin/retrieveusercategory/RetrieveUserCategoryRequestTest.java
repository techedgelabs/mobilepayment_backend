package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusercategory;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusercategory.RetrieveUserCategoryRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveusercategory.RetrieveUserCategoryRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveUserCategoryRequestTest extends BaseTestCase {
    private RetrieveUserCategoryRequest     request;
    private RetrieveUserCategoryRequestBody body;

    // assigning the values
    protected void setUp() {
        request = new RetrieveUserCategoryRequest();
        body = new RetrieveUserCategoryRequestBody();
        request.setBody(body);
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_USER_CATEGORY_CREATE, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

    @Test
    public void testBodyNull() {

        request.setCredential(createCredentialTrue());
        request.setBody(null);
        assertEquals(StatusCode.ADMIN_USER_CATEGORY_CREATE_FAILURE, request.check().getStatusCode());
    }

}