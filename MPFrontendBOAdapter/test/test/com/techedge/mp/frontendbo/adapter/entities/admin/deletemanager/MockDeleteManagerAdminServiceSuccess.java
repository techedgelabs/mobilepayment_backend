package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletemanager;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.Manager;

public class MockDeleteManagerAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminDeleteManager(String adminTicketId, String requestId, Manager manager) {
        // TODO Auto-generated method stub
        return "ADMIN_DELETE_MANAGER_200";
    }
}
