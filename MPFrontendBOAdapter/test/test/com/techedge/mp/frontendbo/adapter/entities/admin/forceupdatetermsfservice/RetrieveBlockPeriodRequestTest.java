package test.com.techedge.mp.frontendbo.adapter.entities.admin.forceupdatetermsfservice;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.forceupdatetermsfservice.ForceUpdateTermsOfServiceBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.forceupdatetermsfservice.ForceUpdateTermsOfServiceRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class RetrieveBlockPeriodRequestTest extends BaseTestCase {

    private ForceUpdateTermsOfServiceRequest     request;
    private ForceUpdateTermsOfServiceBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new ForceUpdateTermsOfServiceRequest();
        body = new ForceUpdateTermsOfServiceBodyRequest();
        body.setTermsOfServiceId("term_of_service");
        body.setUserId(001L);
        body.setValid(true);
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_UPDATE_TERMS_OF_SERVICE_SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}