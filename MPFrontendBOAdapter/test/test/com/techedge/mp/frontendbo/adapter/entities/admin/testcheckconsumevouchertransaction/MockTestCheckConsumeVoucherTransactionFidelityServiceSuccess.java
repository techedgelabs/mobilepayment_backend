package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcheckconsumevouchertransaction;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;

import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class MockTestCheckConsumeVoucherTransactionFidelityServiceSuccess extends MockFidelityService {
    @Override
    public CheckConsumeVoucherTransactionResult checkConsumeVoucherTransaction(String operationID, String operationIDtoCheck, PartnerType partnerType, Long requestTimestamp)
            throws FidelityServiceException {
        CheckConsumeVoucherTransactionResult data = new CheckConsumeVoucherTransactionResult();
        data.setStatusCode(StatusCode.ADMIN_TEST_CHECK_VOUCHER_SUCCESS);
        return data;
    }
}
