package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievepromotions;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepromotions.RetrievePromotionsResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class RetrievePromotionsAdminService extends TestCase {
    private FrontendBOAdapterService   frontend;
    private Response                   response;
    private String                     json;
    private RetrievePromotionsResponse retrievePromotionsAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"retrievePromotions\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrievePromotionsAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        retrievePromotionsAdminResponse = (RetrievePromotionsResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_MANAGER_200", retrievePromotionsAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrievePromotionsAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        retrievePromotionsAdminResponse = (RetrievePromotionsResponse) response.getEntity();
        assertEquals("ADMIN_CREATE_MANAGER_300", retrievePromotionsAdminResponse.getStatus().getStatusCode());
    }
}
