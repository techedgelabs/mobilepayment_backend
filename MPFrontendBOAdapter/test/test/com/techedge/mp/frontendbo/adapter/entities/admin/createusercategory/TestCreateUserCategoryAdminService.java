package test.com.techedge.mp.frontendbo.adapter.entities.admin.createusercategory;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.createusercategory.CreateUserCategoryResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestCreateUserCategoryAdminService extends TestCase {
    private FrontendBOAdapterService   frontend;
    private Response                   response;
    private String                     json;
    private CreateUserCategoryResponse createUserCategoryResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"createUserCategory\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"name\":\"ENABLE_USERS\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
    }

    @Test
    public void testCreateCustomerUserSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateUserCategoryAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        createUserCategoryResponse = (CreateUserCategoryResponse) response.getEntity();
        assertEquals("ADMIN_USERCATEGORY_CREATE_200", createUserCategoryResponse.getStatus().getStatusCode());
    }

    @Test
    public void testCreateCustomerUserFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockCreateUserCategoryAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        createUserCategoryResponse = (CreateUserCategoryResponse) response.getEntity();
        assertEquals("ADMIN_USERCATEGORY_CREATE_300", createUserCategoryResponse.getStatus().getStatusCode());
    }
}
