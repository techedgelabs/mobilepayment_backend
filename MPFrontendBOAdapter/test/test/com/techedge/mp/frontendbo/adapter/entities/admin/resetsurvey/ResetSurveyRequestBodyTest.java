package test.com.techedge.mp.frontendbo.adapter.entities.admin.resetsurvey;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.resetsurvey.ResetSurveyBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class ResetSurveyRequestBodyTest extends BaseTestCase {

    private ResetSurveyBodyRequest body;
    private String                 code;

    // assigning the values
    protected void setUp() {
        body = new ResetSurveyBodyRequest();
        code = "test";
        body.setCode(code);
    }

    @Test
    public void testEmailNull() {
        body.setCode(null);
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }

    @Test
    public void testEmailIsEmpty() {
        body.setCode("");
        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, body.check().getStatusCode());
    }
}