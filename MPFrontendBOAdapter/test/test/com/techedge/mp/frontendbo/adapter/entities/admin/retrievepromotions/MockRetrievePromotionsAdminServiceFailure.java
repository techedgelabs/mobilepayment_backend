package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievepromotions;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.RetrievePromotionsData;

public class MockRetrievePromotionsAdminServiceFailure extends MockAdminService {

    @Override
    public RetrievePromotionsData adminRetrievePromotions(String adminTicketId, String requestId) {
        RetrievePromotionsData data = new RetrievePromotionsData();
        data.setStatusCode("ADMIN_CREATE_MANAGER_300");
        return data;
    }

}
