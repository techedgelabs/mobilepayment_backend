package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrievepoptransactions.RetrievePoPTransactionsResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class RetrievePopTransactionsAdminService extends TestCase {
    private FrontendBOAdapterService        frontend;
    private Response                        response;
    private String                          json;
    private RetrievePoPTransactionsResponse retrievePopTransactionAdminResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"retrievePoPTransactions\":{\"credential\":{\"ticketID\":\"ODFCQjQ4OUY0QkY0NDdBMjNBRTkxNkYw\",\"requestID\":\"AND-1426159337093\"},\"body\":{\"mpTransactionId\":\"123456\",\"userId\":\"1\",\"stationId\":\"00001\",\"sourceId\":\"source\",\"mpTransactionStatus\":\"PAID\",\"notificationCreated\":\"true\",\"notificationPaid\":\"true\",\"notificationUser\":\"true\",\"creationTimestampStart\":\"1234567890\",\"creationTimestampEnd\":\"1234567890\",\"source\":\"source\",\"mpTransactionHistoryFlag\":\"true\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrievePopTransactionsAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        retrievePopTransactionAdminResponse = (RetrievePoPTransactionsResponse) response.getEntity();
        assertEquals("ADMIN_RETRIEVE_POP_TRANSACTION_200", retrievePopTransactionAdminResponse.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrievePopTransactionsAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        retrievePopTransactionAdminResponse = (RetrievePoPTransactionsResponse) response.getEntity();
        assertEquals("ADMIN_RETRIEVE_POP_TRANSACTION_200", retrievePopTransactionAdminResponse.getStatus().getStatusCode());
    }
}
