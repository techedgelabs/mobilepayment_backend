package test.com.techedge.mp.frontendbo.adapter.webservices.common;

import junit.framework.TestCase;

import org.junit.Test;

import com.techedge.mp.frontendbo.adapter.entities.common.Credential;

public class BaseTestCase extends TestCase {
    private String     ticketID_true   = "RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF";
    private String     requestID_true  = "1378034510683";
    private String     ticketID_false  = "RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF1233422";
    private String     requestID_false = "137803451068313780345106831378034510683137803451068313780345106831378034510683";

    private Credential credential      = new Credential();

    public BaseTestCase() {}

    public Credential createCredentialTrue() {
        credential.setTicketID(ticketID_true);
        credential.setRequestID(requestID_true);
        return credential;
    }

    public Credential createCredentialTicketFalse() {
        credential.setTicketID(ticketID_false);
        credential.setRequestID(requestID_true);
        return credential;
    }

    public Credential createCredentialRequestFalse() {
        credential.setTicketID(ticketID_true);
        credential.setRequestID(requestID_false);
        return credential;
    }

    public Credential createCredentialFalse() {
        credential.setTicketID(ticketID_false);
        credential.setRequestID(requestID_false);
        return credential;
    }

    public String getTicketID_true() {
        return ticketID_true;
    }

    public void setTicketID_true(String ticketID_true) {
        this.ticketID_true = ticketID_true;
    }

    public String getRequestID_true() {
        return requestID_true;
    }

    public void setRequestID_true(String requestID_true) {
        this.requestID_true = requestID_true;
    }

    public String getTicketID_false() {
        return ticketID_false;
    }

    public void setTicketID_false(String ticketID_false) {
        this.ticketID_false = ticketID_false;
    }

    public String getRequestID_false() {
        return requestID_false;
    }

    public void setRequestID_false(String requestID_false) {
        this.requestID_false = requestID_false;
    }

    @Test
    public void test() {
        assertEquals("1", "1");
    }

}
