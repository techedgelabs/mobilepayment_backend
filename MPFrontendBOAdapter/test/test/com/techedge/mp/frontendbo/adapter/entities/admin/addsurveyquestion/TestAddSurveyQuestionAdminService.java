package test.com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion.AddSurveyQuestionBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion.AddSurveyQuestionRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion.AddSurveyQuestionResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.SurveyQuestion;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestAddSurveyQuestionAdminService extends BaseTestCase {
    private FrontendBOAdapterService     frontend;
    private AddSurveyQuestionRequest     request;
    private AddSurveyQuestionBodyRequest body;
    private AddSurveyQuestionResponse    response;
    private Response                     baseResponse;
    private String                       json;
    private Gson                         gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest                 adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AddSurveyQuestionRequest();
        body = new AddSurveyQuestionBodyRequest();
        body.setCode("001");
        SurveyQuestion question = new SurveyQuestion();
        body.setQuestion(question);
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());

        adminRequest = new AdminRequest();
        adminRequest.setAddSurveyQuestion(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
    }

    @Test
    public void testSurveyQuestionSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAddSurveyQuestionAdminServiceSuccess());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AddSurveyQuestionResponse) baseResponse.getEntity();
        assertEquals("ADMIN_SURVEY_QUESTION_ADD_200", response.getStatus().getStatusCode());
    }

    @Test
    public void testSurveyQuestionFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockAddSurveyQuestionAdminServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (AddSurveyQuestionResponse) baseResponse.getEntity();
        assertEquals("ADMIN_SURVEY_QUESTION_ADD_300", response.getStatus().getStatusCode());
    }
}
