package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatetransactions;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.updatetransaction.UpdateTransactionRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class UpdateTransactionBodyRequestTest extends BaseTestCase {
    private UpdateTransactionRequestBody body;

    private String                       transactionId;
    private String                       finalStatusType;
    private Boolean                      GFGNotification;
    private Boolean                      confirmed;

    protected void setUp() {

        body = new UpdateTransactionRequestBody();

        transactionId = "id";
        finalStatusType = "finale";
        GFGNotification = true;
        confirmed = false;

        body.setConfirmed(confirmed);
        body.setFinalStatusType(finalStatusType);
        body.setGFGNotification(GFGNotification);
        body.setTransactionId(transactionId);

    }

    @Test
    public void testTransactionIDNull() {
        body.setTransactionId(null);
        assertEquals(StatusCode.ADMIN_UPDATE_TRANSACTION_ERROR_PARAMETERS, body.check().getStatusCode());
    }
    
    @Test
    public void testTransactionIDMajor40() {
        body.setTransactionId("testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest");
        assertEquals(StatusCode.ADMIN_UPDATE_TRANSACTION_ERROR_PARAMETERS, body.check().getStatusCode());
    }
    
    @Test
    public void testFinalStatus() {
        body.setFinalStatusType("testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttesttest");
        assertEquals(StatusCode.ADMIN_UPDATE_TRANSACTION_ERROR_PARAMETERS, body.check().getStatusCode());
    }
}