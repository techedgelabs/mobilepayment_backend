package test.com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror.AdminCreateErrorBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.createmappingerror.AdminCreateErrorRequest;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class CreateErrorRequestTest extends BaseTestCase {

    private AdminCreateErrorRequest     request;
    private AdminCreateErrorBodyRequest body;

    // assigning the values
    protected void setUp() {
        request = new AdminCreateErrorRequest();
        body = new AdminCreateErrorBodyRequest();
        body.setErrorCode("error");
        body.setStatusCode("error");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
    }

    @Test
    public void testCredentialTrue() {

        request.setCredential(createCredentialTrue());
        assertEquals(StatusCode.ADMIN_MAPPING_ERROR_CREATE__SUCCESS, request.check().getStatusCode());

    }

    @Test
    public void testCredentialFalse() {

        request.setCredential(createCredentialFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialTicketIDFalse() {

        request.setCredential(createCredentialTicketFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_TICKET, request.check().getStatusCode());
    }

    @Test
    public void testCredentialRequestFalse() {

        request.setCredential(createCredentialRequestFalse());

        assertEquals(StatusCode.ADMIN_REQU_INVALID_REQUEST, request.check().getStatusCode());
    }

}