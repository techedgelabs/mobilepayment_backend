package test.com.techedge.mp.frontendbo.adapter.entities.admin.addsurveyquestion;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockAddSurveyQuestionAdminServiceSuccess extends MockAdminService {
    @Override
    public String adminSurveyQuestionAdd(String adminTicketId, String requestId, String surveyCode, String questionCode, String text, String type, Integer sequence) {
        // TODO Auto-generated method stub
        return "ADMIN_SURVEY_QUESTION_ADD_200";
    }
}
