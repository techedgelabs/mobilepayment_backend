package test.com.techedge.mp.frontendbo.adapter.entities.admin.updatetransactions;

import javax.ws.rs.core.Response;

import junit.framework.TestCase;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.updatetransaction.UpdateTransactionResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class UpdateTransactionsAdminService extends TestCase {
    private FrontendBOAdapterService  frontend;
    private Response                  response;
    private String                    json;
    private UpdateTransactionResponse updateTransactionsResponse;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        json = "{\"updateTransaction\":{\"credential\":{\"ticketID\":\"RDkzRUQ4OTQyMTVBMjc0NTIxQjdEM0JF\",\"requestID\":\"IPH-1378034510683\"},\"body\":{\"transactionId\":\"test\",\"finalStatusType\":\"test\",\"GFGNotification\":\"1\",\"confirmed\":\"1\"}}}";
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testUpdateSurveySuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockUpdateTransactionsAdminServiceSuccess());
        response = frontend.adminJsonHandler(json);
        updateTransactionsResponse = (UpdateTransactionResponse) response.getEntity();
        assertEquals("ADMIN_UPDATE_TRANSACTION_200", updateTransactionsResponse.getStatus().getStatusCode());
    }

    @Test
    public void testUpdateSurveyFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockUpdateTransactionsAdminServiceFailure());
        response = frontend.adminJsonHandler(json);
        updateTransactionsResponse = (UpdateTransactionResponse) response.getEntity();
        assertEquals("ADMIN_UPDATE_TRANSACTION_300", updateTransactionsResponse.getStatus().getStatusCode());
    }
}
