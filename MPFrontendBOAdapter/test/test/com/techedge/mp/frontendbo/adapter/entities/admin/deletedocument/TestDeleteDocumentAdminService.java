package test.com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument.AdminDeleteDocumentBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument.AdminDeleteDocumentRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.deletedocument.AdminDeleteDocumentResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestDeleteDocumentAdminService extends BaseTestCase {
    private FrontendBOAdapterService       frontend;
    private AdminDeleteDocumentRequest     request;
    private AdminDeleteDocumentBodyRequest body;
    private AdminDeleteDocumentResponse    response;
    private Response                       baseResponse;
    private String                         json;
    private Gson                           gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new AdminDeleteDocumentRequest();
        body = new AdminDeleteDocumentBodyRequest();
        body.setDocumentKey("document");
        request.setBody(body);
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
    }

    @Test
    public void testCreateBlockPeriodSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockDeleteDocumentAdminServiceSuccess());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setDeleteDocument(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminDeleteDocumentResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_DOCUMENT_DELETE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testCreateBlockPeriodFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockDeleteDocumentAdminServiceFailure());
        AdminRequest adminRequest = new AdminRequest();
        adminRequest.setDeleteDocument(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
        baseResponse = frontend.adminJsonHandler(json);
        response = (AdminDeleteDocumentResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_DOCUMENT_DELETE_FAILURE, response.getStatus().getStatusCode());
    }
}
