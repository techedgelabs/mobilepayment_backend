package test.com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher;

import java.math.BigDecimal;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.entities.admin.testcheckvoucher.MockCheckVoucherAdminServiceSuccess;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher.TestCreateVoucherBodyRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher.TestCreateVoucherRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.testcreatevoucher.TestCreateVoucherResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class TestCreateVoucherAdminService extends BaseTestCase {
    private FrontendBOAdapterService     frontend;
    private TestCreateVoucherRequest     request;
    private TestCreateVoucherBodyRequest body;
    private TestCreateVoucherResponse    response;
    private Response                     baseResponse;
    private String                       json;
    private Gson                         gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest                 adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new TestCreateVoucherRequest();
        body = new TestCreateVoucherBodyRequest();
        body.setAuthorizationCode("auth");
        body.setBankTransactionID("bank");
        body.setOperationID("operation");
        body.setPartnerType("partner");
        body.setRequestTimestamp(12L);
        body.setShopTransactionID("shop");
        body.setVoucherType("voucher");
        body.setTotalAmount(new BigDecimal(1.2));
        request.setBody(body);
        request.setCredential(createCredentialTrue());

        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
        adminRequest = new AdminRequest();
        EJBHomeCache.getInstance().setAdminService(new MockCheckVoucherAdminServiceSuccess());

        adminRequest.setTestCreateVoucher(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setFidelityService(new MockTestCreateVoucherFidelityServiceSuccess());
        EJBHomeCache.getInstance().setAdminService(new MockCheckVoucherAdminServiceSuccess());

        baseResponse = frontend.adminJsonHandler(json);
        response = (TestCreateVoucherResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_TEST_CREATE_VOUCHER_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setFidelityService(new MockTestCreateVoucherFidelityServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (TestCreateVoucherResponse) baseResponse.getEntity();
        assertEquals(StatusCode.ADMIN_TEST_CREATE_VOUCHER_FAILURE, response.getStatus().getStatusCode());
    }
}
