package test.com.techedge.mp.frontendbo.adapter.entities.admin.retrievedocument;

import javax.ws.rs.core.Response;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockFidelityService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockLoggerService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockReconciliationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;
import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockTransactionService;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.frontendbo.adapter.entities.admin.AdminRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveprefixes.RetrievePrefixesRequest;
import com.techedge.mp.frontendbo.adapter.entities.admin.retrieveprefixes.RetrievePrefixesResponse;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.frontendbo.adapter.webservices.EJBHomeCache;
import com.techedge.mp.frontendbo.adapter.webservices.FrontendBOAdapterService;

public class RetrieveDocumentPrefixesAdminService extends BaseTestCase {
    private FrontendBOAdapterService frontend;
    private RetrievePrefixesRequest  request;
    private RetrievePrefixesResponse response;
    private Response                 baseResponse;
    private String                   json;
    private Gson                     gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();
    private AdminRequest             adminRequest;

    // assigning the values
    protected void setUp() throws InterfaceNotFoundException {
        frontend = new FrontendBOAdapterService();
        request = new RetrievePrefixesRequest();
        request.setCredential(createCredentialTrue());
        EJBHomeCache.getInstance().setLoggerService(new MockLoggerService());
        EJBHomeCache.getInstance().setFidelityService(new MockFidelityService());
        EJBHomeCache.getInstance().setTransactionService(new MockTransactionService());
        EJBHomeCache.getInstance().setReconciliationService(new MockReconciliationService());
        EJBHomeCache.getInstance().setAdminService(new MockAdminService());
        EJBHomeCache.getInstance().setRefuelingNotificationService(new MockRefuelingNotificationService());
        adminRequest = new AdminRequest();
        adminRequest.setRetrievePrefixes(request);
        json = gson.toJson(adminRequest, AdminRequest.class);
    }

    @Test
    public void testRetrieveActivityLogSuccess() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveDocumentAdminServiceSuccess());
        baseResponse = frontend.adminJsonHandler(json);
        response = (RetrievePrefixesResponse) baseResponse.getEntity();
        assertEquals(StatusCode.PREFIX_RETRIEVE_SUCCESS, response.getStatus().getStatusCode());
    }

    @Test
    public void testRetrieveActivityLogFailure() throws InterfaceNotFoundException, ParameterNotFoundException {
        EJBHomeCache.getInstance().setAdminService(new MockRetrieveDocumentAdminServiceFailure());
        baseResponse = frontend.adminJsonHandler(json);
        response = (RetrievePrefixesResponse) baseResponse.getEntity();
        assertEquals(StatusCode.PREFIX_RETRIEVE_FAILURE, response.getStatus().getStatusCode());
    }
}
