package test.com.techedge.mp.frontendbo.adapter.entities.admin.getsurvey;

import java.util.ArrayList;
import java.util.Date;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

import com.techedge.mp.core.business.interfaces.Survey;
import com.techedge.mp.core.business.interfaces.SurveyList;

public class GetSurveyAdminServiceSuccess extends MockAdminService {
    @Override
    public SurveyList adminSurveyGet(String adminTicketId, String requestId, String surveyCode, Date startSearch, Date endSearch, Integer status) {
        SurveyList list = new SurveyList();
        ArrayList<Survey> surveyList = new ArrayList<Survey>(0);
        Survey item = new Survey();
        item.setCode("01");
        item.setDescription("desc");
        item.setEndDate(new Date(1445171200));
        item.setStartDate(new Date(1465171200));
        item.setNote("note");
        item.setStatus(1);
        surveyList.add(item);
        list.setSurveyList(surveyList);
        list.setStatusCode("ADMIN_SURVEY_GET_200");
        return list;
    }
}
