package test.com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion;

import java.util.concurrent.ConcurrentHashMap;

import org.junit.Test;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.BaseTestCase;

import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.frontendbo.adapter.entities.admin.addvouchertopromotion.AddVoucherToPromotionRequestBody;
import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;

public class AddVoucherToPromotionBodyRequestTest extends BaseTestCase {

    private AddVoucherToPromotionRequestBody body;
    private String                           promotionCode;
    private PromoVoucherInput                voucherInfo;

    // assigning the values
    protected void setUp() {
        body = new AddVoucherToPromotionRequestBody();
        promotionCode = "promo";
        voucherInfo = new PromoVoucherInput();
        voucherInfo.getPromoAssociation().put("test", 1.1);
        body.setPromotionCode(promotionCode);
        body.setVoucherInfo(voucherInfo);
    }

    @Test
    public void testPromotionCodeNull() {

        promotionCode = null;
        body.setPromotionCode(promotionCode);

        assertEquals(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_CODE, body.check().getStatusCode());

    }

    @Test
    public void testPromotionCodeMajor() {

        promotionCode = "test1test1test1test1test1test1";
        body.setPromotionCode(promotionCode);

        assertEquals(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_CODE, body.check().getStatusCode());

    }

    @Test
    public void testVoucherInfoNull() {

        body.setVoucherInfo(null);
        System.out.print(body.check().getStatusCode());
        assertEquals(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_VOUCERINFO, body.check().getStatusCode());

    }

    @Test
    public void testPromotionAssociationNull() {
        voucherInfo.setPromoAssociation(null);
        body.setVoucherInfo(voucherInfo);
        System.out.print(body.check().getStatusCode());
        assertEquals(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_VOUCERINFO, body.check().getStatusCode());

    }
    
    @Test
    public void testPromotionAssociationIsEmpty() {
        ConcurrentHashMap<String, Double> promoAssociation = new ConcurrentHashMap<String, Double>(0);
        voucherInfo.setPromoAssociation(promoAssociation);
        body.setVoucherInfo(voucherInfo);
        System.out.print(body.check().getStatusCode());
        assertEquals(StatusCode.ADMIN_ADD_VOUCHER_TO_PROMOTION_VOUCERINFO, body.check().getStatusCode());

    }
    
}
