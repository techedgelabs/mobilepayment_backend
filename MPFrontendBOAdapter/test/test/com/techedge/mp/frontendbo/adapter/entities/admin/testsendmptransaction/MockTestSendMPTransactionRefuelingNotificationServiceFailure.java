package test.com.techedge.mp.frontendbo.adapter.entities.admin.testsendmptransaction;

import java.util.List;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockRefuelingNotificationService;

import com.techedge.mp.frontendbo.adapter.entities.common.StatusCode;
import com.techedge.mp.refueling.integration.entities.MpTransactionDetail;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;

public class MockTestSendMPTransactionRefuelingNotificationServiceFailure extends MockRefuelingNotificationService {

    @Override
    public String sendMPTransactionNotification(String requestID, String srcTransactionID, String mpTransactionID, String mpTransactionStatus, RefuelDetail refuelDetail) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_REQU_INVALID_REQUEST;
    }

    @Override
    public String sendMPTransactionReport(String requestID, String startDate, String endDate, List<MpTransactionDetail> mpTransactionList) {
        // TODO Auto-generated method stub
        return StatusCode.ADMIN_REQU_INVALID_REQUEST;
    }
}
