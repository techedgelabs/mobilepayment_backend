package test.com.techedge.mp.frontendbo.adapter.entities.admin.addstationmanager;

import test.com.techedge.mp.frontendbo.adapter.webservices.common.MockAdminService;

public class MockAddStationManagerAdminServiceFailure extends MockAdminService {

    @Override
    public String adminAddStationManager(String adminTicketId, String requestId, Long managerId, Long stationId) {
        return "ADMIN_ADDSTATION_MANAGER_300";
    }

}
