package com.techedge.mp.crm.adapter.interfaces;

import java.io.Serializable;
import java.util.Date;

public class OfferSF implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -6406232112570319529L;
    private String            offerCode;
    private String            firstName;
    private String            bannerId;
    private Date              startDate;
    private Date              endDate;
    private String            description;
    private String            offerType;
    private String            channel;
    private String            missionType;
    private Integer           numExecutedSteps;
    private Integer           numTotalSteps;
    private String            url;
    private String            parameter1;
    private String            parameter2;
    private String            parameter3;
    private Double            parameter4;

    public String getOfferCode() {
        return offerCode;
    }

    public void setOfferCode(String offerCode) {
        this.offerCode = offerCode;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getBannerId() {
        return bannerId;
    }

    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getMissionType() {
        return missionType;
    }

    public void setMissionType(String missionType) {
        this.missionType = missionType;
    }

    public Integer getNumExecutedSteps() {
        return numExecutedSteps;
    }

    public void setNumExecutedSteps(Integer numExecutedSteps) {
        this.numExecutedSteps = numExecutedSteps;
    }

    public Integer getNumTotalSteps() {
        return numTotalSteps;
    }

    public void setNumTotalSteps(Integer numTotalSteps) {
        this.numTotalSteps = numTotalSteps;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getParameter1() {
        return parameter1;
    }

    public void setParameter1(String parameter1) {
        this.parameter1 = parameter1;
    }

    public String getParameter2() {
        return parameter2;
    }

    public void setParameter2(String parameter2) {
        this.parameter2 = parameter2;
    }

    public String getParameter3() {
        return parameter3;
    }

    public void setParameter3(String parameter3) {
        this.parameter3 = parameter3;
    }

    public Double getParameter4() {
        return parameter4;
    }

    public void setParameter4(Double parameter4) {
        this.parameter4 = parameter4;
    }

}