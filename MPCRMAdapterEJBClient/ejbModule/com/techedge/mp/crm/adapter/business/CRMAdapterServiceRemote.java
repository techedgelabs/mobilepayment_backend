package com.techedge.mp.crm.adapter.business;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Remote;

import com.techedge.mp.core.business.interfaces.crm.GetOfferRequest;
import com.techedge.mp.core.business.interfaces.crm.GetOffersResult;
import com.techedge.mp.core.business.interfaces.crm.PromotionSF;
import com.techedge.mp.core.business.interfaces.crm.Response;
import com.techedge.mp.crm.adapter.interfaces.NotifyEventResult;

@Remote
public interface CRMAdapterServiceRemote {
    
    public Response startSession(String sessionID, String fiscalCode, String audienceID, HashMap<String, Object> parameters);
    
    public Response endSession(String sessionID);
    
    public Response getOffers(String sessionID, String interactionPoint, Integer numberOfOffers);
    
    public Response getOffersForMultipleInteractionPoints(String sessionID, List<GetOfferRequest> requests);
    
    public List<Response> executeBatchGetOffers(String sessionID, String fiscalCode, String audienceID, String interactionPoint, HashMap<String, Object> parameters);
    
    public List<Response> executeBatchGetOffersForMultipleInteractionPoints(String sessionID, String fiscalCode, String audienceID, List<GetOfferRequest> requests, HashMap<String, Object> parameters);

    public NotifyEventResult notifyEvent(String requestId, String fiscalCode, Date date, String stationId, String productId, Boolean paymentFlag, Integer credits, Double quantity, String refuelMode, String firstName, String lastName,
            String email, Date birthDate, Boolean notificationFlag, Boolean paymentCardFlag, String brand, String cluster, Double amount, Boolean privacyFlag1, Boolean privacyFlag2, String mobilePhone,
            String loyaltyCard, String eventType, String parameter1, String parameter2, String paymentMode);
    
    public GetOffersResult getOffersSF(String requestId, String fiscalCode, Date date, Boolean notificationFlag, Boolean paymentCardFlag, String brand, String cluster);
    
    public List<PromotionSF> getPromotionsSF(String requestId, Date date, String parameter1, String parameter2, String parameter3,  String parameter4);
}
