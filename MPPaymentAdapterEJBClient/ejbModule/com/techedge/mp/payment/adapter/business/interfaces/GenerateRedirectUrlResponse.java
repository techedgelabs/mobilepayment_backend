package com.techedge.mp.payment.adapter.business.interfaces;

import java.io.Serializable;

public class GenerateRedirectUrlResponse implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -2720116409970770740L;
    
    protected String redirectUrl;
    protected String currency;
    protected String shopTransactionId;

    public GenerateRedirectUrlResponse() {}

    public String getRedirectUrl() {
        return redirectUrl;
    }

    public void setRedirectUrl(String redirectUrl) {
        this.redirectUrl = redirectUrl;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getShopTransactionId() {
        return shopTransactionId;
    }

    public void setShopTransactionId(String shopTransactionId) {
        this.shopTransactionId = shopTransactionId;
    }

}
