package com.techedge.mp.payment.adapter.business.interfaces;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "extension", propOrder = {
	"key",
	"value",
})

public class Extension implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8519979252552873641L;
	@XmlElement(required = true)
	private String key;
	@XmlElement(required = true)
	private String value;
	
	public String getKey(){
		return key;
	}

	public void setKey( String str1){
		 key = str1;
	}
	public String getValue(){
		return value;
	}
	public void setValue(String val1){
		value = val1;
	}
}
