package com.techedge.mp.payment.adapter.business;

import javax.ejb.Remote;

import com.techedge.mp.payment.adapter.business.interfaces.ExtendedGestPayData;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

@Remote
public interface GPServiceRemote {

    public GestPayData callPagam(Double i_amount, String i_shopTransactionID, String i_shopLogin, String i_valuta, String i_token, String i_applePayPKPaymentToken,
            String i_acquirerID, String i_groupAcquirer, String i_encodedSecretKey, String i_paymentMethodExpiration, Extension[] i_extension, String category);

    public GestPayData callSettle(Double i_amount, String i_shopTransactionID, String i_shopLogin, String i_valuta, String i_acquirerId, String i_groupAcquirer,
            String i_encodedSecretKey, String i_token, String i_authorizationCode, String i_bankTransactionId, String i_refuelMode, String i_productCode,
            Double i_quantity, Double i_unitPrice);

    public ExtendedGestPayData callPagamAndSettle(Double i_amount, String i_shopTransactionID, String i_shopLogin, String i_valuta, String i_acquirerId,
            String i_token, String i_refuelMode, String i_productCode, Double i_quantity, Double i_unitPrice, String i_shopCode, String i_paymentCryptogram);
    
    public GestPayData callRefund(Double i_amount, String i_shopTransactionID, String i_shopLogin, String i_valuta, String i_token, String i_authorizationCode,
            String i_bankTransactionID, String i_shopCode, String i_acquirerId, String i_groupAcquirer, String i_encodedSecretKey);

    public GestPayData deletePagam(Double i_amount, String i_shopTransactionID, String i_shopLogin, String i_valuta, String i_bankTransactionID, String i_operation,
            String i_acquirerId, String i_groupAcquirer, String i_encodedSecretKey);

    public String deleteToken(String tokenValue, String shopLogin, String acquirerId, String encodedSecretKey);

    public GestPayData callReadTrx(String i_shopLogin, String i_shopTransactionID, String i_bankTransactionID, String i_acquirerId, String i_groupAcquirer,
            String i_encodedSecretKey);
}
