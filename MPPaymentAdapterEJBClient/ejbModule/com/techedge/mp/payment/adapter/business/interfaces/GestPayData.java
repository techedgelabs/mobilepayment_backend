package com.techedge.mp.payment.adapter.business.interfaces;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.techedge.mp.payment.adapter.wss2s.xsd.EventsType;
import com.techedge.mp.payment.adapter.wss2s.xsd.EventsType.Event;
import com.techedge.mp.payment.adapter.wss2s.xsd.GestPayS2S;

public class GestPayData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1569550961556245269L;
	
	private String transactionType;
    private String transactionResult;
    private String transactionState;
    private String shopTransactionID;
    private String bankTransactionID;
    private String authorizationCode; 
    private String currency;
    private String amount;
    private String country;
    private String customInfo;
    private String buyerName;
    private String buyerEmail;
    private String tdLevel;
    private String errorCode;
    private String errorDescription;
    private String alertCode;
    private String alertDescription;
    private String vbVRisp; 
    private String vbVBuyer; 
    private String vbVFlag; 
    private String TransactionKey;
    private String token;
	private String tokenExpiryMonth;
	private String tokenExpiryYear;
	private String cardBIN;
	private String hash;
	private List<EventsType> events = new ArrayList<EventsType>(0);
	//private HashMap<String, Event> events = new HashMap<String, Event>();
	
	private GestPayS2S gps2s;
	
	public String getTransactionType() {
		return transactionType;
	}
	public void setTransactionType(String transactionType) {
		this.transactionType = transactionType;
	}
	public String getTransactionResult() {
		return transactionResult;
	}
	public void setTransactionResult(String transactionResult) {
		this.transactionResult = transactionResult;
	}
	public String getTransactionState() {
        return transactionState;
    }
    public void setTransactionState(String transactionState) {
        this.transactionState = transactionState;
    }
    public String getShopTransactionID() {
		return shopTransactionID;
	}
	public void setShopTransactionID(String shopTransactionID) {
		this.shopTransactionID = shopTransactionID;
	}
	public String getBankTransactionID() {
		return bankTransactionID;
	}
	public void setBankTransactionID(String bankTransactionID) {
		this.bankTransactionID = bankTransactionID;
	}
	public String getAuthorizationCode() {
		return authorizationCode;
	}
	public void setAuthorizationCode(String authorizationCode) {
		this.authorizationCode = authorizationCode;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getCustomInfo() {
		return customInfo;
	}
	public void setCustomInfo(String customInfo) {
		this.customInfo = customInfo;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public String getBuyerEmail() {
		return buyerEmail;
	}
	public void setBuyerEmail(String buyerEmail) {
		this.buyerEmail = buyerEmail;
	}
	public String getTdLevel() {
		return tdLevel;
	}
	public void setTdLevel(String tdLevel) {
		this.tdLevel = tdLevel;
	}
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getErrorDescription() {
		return errorDescription;
	}
	public void setErrorDescription(String errorDescription) {
		this.errorDescription = errorDescription;
	}
	public String getAlertCode() {
		return alertCode;
	}
	public void setAlertCode(String alertCode) {
		this.alertCode = alertCode;
	}
	public String getAlertDescription() {
		return alertDescription;
	}
	public void setAlertDescription(String alertDescription) {
		this.alertDescription = alertDescription;
	}
	public String getVbVRisp() {
		return vbVRisp;
	}
	public void setVbVRisp(String vbVRisp) {
		this.vbVRisp = vbVRisp;
	}
	public String getVbVBuyer() {
		return vbVBuyer;
	}
	public void setVbVBuyer(String vbVBuyer) {
		this.vbVBuyer = vbVBuyer;
	}
	public String getVbVFlag() {
		return vbVFlag;
	}
	public void setVbVFlag(String vbVFlag) {
		this.vbVFlag = vbVFlag;
	}
	public String getTransactionKey() {
		return TransactionKey;
	}
	public void setTransactionKey(String transactionKey) {
		TransactionKey = transactionKey;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getTokenExpiryMonth() {
		return tokenExpiryMonth;
	}
	public void setTokenExpiryMonth(String tokenExpiryMonth) {
		this.tokenExpiryMonth = tokenExpiryMonth;
	}
	public String getTokenExpiryYear() {
		return tokenExpiryYear;
	}
	public void setTokenExpiryYear(String tokenExpiryYear) {
		this.tokenExpiryYear = tokenExpiryYear;
	}
	public GestPayS2S getGps2s() {
		return gps2s;
	}
	public void setGps2s(GestPayS2S gps2s) {
		this.gps2s = gps2s;
	}
    public String getCardBIN() {
        return cardBIN;
    }
    public void setCardBIN(String cardBIN) {
        this.cardBIN = cardBIN;
    }
    public String getHash() {
        return hash;
    }
    public void setHash(String hash) {
        this.hash = hash;
    }
    public List<EventsType> getEvents() {
        return events;
    }
    public void setEvents(List<EventsType> events) {
        this.events = events;
    }
    public BigDecimal getEventAmount(String eventType) {
        BigDecimal amount = new BigDecimal(0);

        for (EventsType eventsType : events) {
            for (Event event : eventsType.getEvent()) {
                if (event.getEventtype().equals(eventType)) {
                    if (event.getEventamount() != null) {
                        amount = event.getEventamount();
                        break;
                    }
                }
            }
        }        
        return amount;
    }
    
    /*
    public List<EventsType> getEvents() {
        ArrayList<EventsType> events = new ArrayList<EventsType>(0);
        
        for (Event event : this.events.values()) {
            EventsType eventsType = new EventsType();
            eventsType.getEvent().add(event);
            events.add(eventsType);
        }
        
        return events;
    }
    public void setEvents(List<EventsType> events) {
        this.events.clear();
        for (EventsType eventsType : events) {
            for (Event event : eventsType.getEvent()) {
                this.events.put(event.getEventtype(), event);
            }
        }
    }
    
    public BigDecimal getEventAmount(String eventType) {
        if (this.events.get(eventType) == null ||  this.events.get(eventType).getEventamount() == null) {
            return new BigDecimal(0);
        }
        
        return this.events.get(eventType).getEventamount();
    }
    */
}
