//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.4-2 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2014.10.08 at 03:53:12 PM CEST 
//


package com.techedge.mp.payment.adapter.business.xsd;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.techedge.mp.bancasella.adapter.ejb.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.techedge.mp.bancasella.adapter.ejb.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EventsType }
     * 
     */
    public EventsType createEventsType() {
        return new EventsType();
    }

    /**
     * Create an instance of {@link GestPayCryptDecrypt }
     * 
     */
    public GestPayCryptDecrypt createGestPayCryptDecrypt() {
        return new GestPayCryptDecrypt();
    }

    /**
     * Create an instance of {@link BuyerType }
     * 
     */
    public BuyerType createBuyerType() {
        return new BuyerType();
    }

    /**
     * Create an instance of {@link EventsType.Event }
     * 
     */
    public EventsType.Event createEventsTypeEvent() {
        return new EventsType.Event();
    }

}
