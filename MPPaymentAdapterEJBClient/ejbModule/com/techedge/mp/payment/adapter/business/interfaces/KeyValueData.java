package com.techedge.mp.payment.adapter.business.interfaces;

import java.io.Serializable;

public class KeyValueData implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = -3144767913137787015L;

    protected String          key;
    protected String          value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
