package com.techedge.mp.payment.adapter.business;

import javax.ejb.Remote;

import com.techedge.mp.payment.adapter.business.interfaces.GenerateRedirectUrlResponse;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;
import com.techedge.mp.payment.adapter.business.interfaces.GetTokenResponse;

@Remote
public interface GSServiceRemote {

	public GetTokenResponse getToken(String ticketId, String requestId, String OperationId, String shopTransactionId, String shopLogin, String checkAmount );
	public GestPayData decryptString(String shopLogin, String encodedString);
	public GenerateRedirectUrlResponse generateRedirectUrl(String requestId, String shopTransactionId, String apiKey, String checkAmount, String uicCode, String groupAcquirer, String encodedSecretKey, Boolean boUrl);
}
