package com.techedge.mp.payment.adapter.business.interfaces;

import java.io.Serializable;

public class GetTokenResponse implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7209383811776325423L;
	
	private String secureString;
	private String bankTransactionId;
	private String currency;
	
	public String getSecureString() {
		return secureString;
	}
	public void setSecureString(String secureString) {
		this.secureString = secureString;
	}
	
	public String getBankTransactionId() {
		return bankTransactionId;
	}
	public void setBankTransactionId(String bankTransactionId) {
		this.bankTransactionId = bankTransactionId;
	}
	
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
}
