package com.techedge.mp.payment.adapter.business.interfaces;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class ExtendedGestPayData extends GestPayData implements Serializable {

    /**
     * 
     */
    private static final long  serialVersionUID   = 1038503629322287358L;
    
    private List<KeyValueData> receiptElementList = new ArrayList<KeyValueData>(0);

    public List<KeyValueData> getReceiptElementList() {
        return receiptElementList;
    }

    public void setReceiptElementList(List<KeyValueData> receiptElementList) {
        this.receiptElementList = receiptElementList;
    }

}
