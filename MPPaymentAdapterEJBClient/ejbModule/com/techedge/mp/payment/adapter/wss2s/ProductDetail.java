
package com.techedge.mp.payment.adapter.wss2s;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ProductDetail complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ProductDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element minOccurs="0" maxOccurs="1" name="ProductCode" type="s:string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="SKU" type="s:string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Name" type="s:string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Description" type="s:string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Quantity" type="s:string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Price" type="s:string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="UnitPrice" type="s:string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Type" type="s:string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Vat" type="s:string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Discount" type="s:string" />
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductDetail", propOrder = {
    "productCode",
    "sku",
    "name",
    "quantity",
    "price",
    "unitPrice",
    "type",
    "vat",
    "discount"
})
public class ProductDetail {

    protected String productCode;
    protected String sku;
    protected String name;
    protected String quantity;
    protected String price;
    protected String unitPrice;
    protected String type;
    protected String vat;
    protected String discount;
    /**
     * @return the productCode
     */
    public String getProductCode() {
        return productCode;
    }
    /**
     * @param productCode the productCode to set
     */
    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }
    /**
     * @return the sku
     */
    public String getSku() {
        return sku;
    }
    /**
     * @param sku the sku to set
     */
    public void setSku(String sku) {
        this.sku = sku;
    }
    /**
     * @return the name
     */
    public String getName() {
        return name;
    }
    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }
    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    /**
     * @return the price
     */
    public String getPrice() {
        return price;
    }
    /**
     * @param price the price to set
     */
    public void setPrice(String price) {
        this.price = price;
    }
    /**
     * @return the unitPrice
     */
    public String getUnitPrice() {
        return unitPrice;
    }
    /**
     * @param unitPrice the unitPrice to set
     */
    public void setUnitPrice(String unitPrice) {
        this.unitPrice = unitPrice;
    }
    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }
    /**
     * @return the vat
     */
    public String getVat() {
        return vat;
    }
    /**
     * @param vat the vat to set
     */
    public void setVat(String vat) {
        this.vat = vat;
    }
    /**
     * @return the discount
     */
    public String getDiscount() {
        return discount;
    }
    /**
     * @param discount the discount to set
     */
    public void setDiscount(String discount) {
        this.discount = discount;
    }
    




}
