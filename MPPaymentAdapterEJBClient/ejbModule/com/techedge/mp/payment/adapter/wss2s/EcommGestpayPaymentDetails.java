
package com.techedge.mp.payment.adapter.wss2s;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per EcommGestpayPaymentDetails complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="EcommGestpayPaymentDetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element minOccurs="0" maxOccurs="1" name="CustomerDetail" type="{https://ecomms2s.sella.it/}CustomerDetail"/>
 *         &lt;element minOccurs="0" maxOccurs="1" name="ShippingAddress" type="{https://ecomms2s.sella.it/}ShippingAddress"/>
 *         &lt;element minOccurs="0" maxOccurs="1" name="BillingAddress" type="{https://ecomms2s.sella.it/}BillingAddress"/>
 *         &lt;element minOccurs="0" maxOccurs="1" name="ProductDetails" type="{https://ecomms2s.sella.it/}ArrayOfProductDetail"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EcommGestpayPaymentDetails", propOrder = {
    "customerDetail",
    "shippingAddress",
    "billingAddress",
    "productDetails"
})
public class EcommGestpayPaymentDetails {

    protected CustomerDetail customerDetail;
    protected ShippingAddress shippingAddress;
    protected BillingAddress billingAddress;
    protected ArrayOfProductDetail productDetails;

    /**
     * Recupera il valore della proprietÓ customerDetail.
     * 
     * @return
     *     possible object is
     *     {@link CustomerDetail }
     *     
     */
    public CustomerDetail getCustomerDetail() {
        return customerDetail;
    }

    /**
     * Imposta il valore della proprietÓ customerDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerDetail }
     *     
     */
    public void setCustomerDetail(CustomerDetail value) {
        this.customerDetail = value;
    }

    /**
     * Recupera il valore della proprietÓ shippingAddress.
     * 
     * @return
     *     possible object is
     *     {@link ShippingAddress }
     *     
     */
    public ShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Imposta il valore della proprietÓ shippingAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link ShippingAddress }
     *     
     */
    public void setShippingAddress(ShippingAddress value) {
        this.shippingAddress = value;
    }

    /**
     * Recupera il valore della proprietÓ billingAddress.
     * 
     * @return
     *     possible object is
     *     {@link BillingAddress }
     *     
     */
    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    /**
     * Imposta il valore della proprietÓ billingAddress.
     * 
     * @param value
     *     allowed object is
     *     {@link BillingAddress }
     *     
     */
    public void setBillingAddress(BillingAddress value) {
        this.billingAddress = value;
    }

    /**
     * Recupera il valore della proprietÓ productDetails.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfProductDetail }
     *     
     */
    public ArrayOfProductDetail getProductDetails() {
        return productDetails;
    }

    /**
     * Imposta il valore della proprietÓ productDetails.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfProductDetail }
     *     
     */
    public void setProductDetails(ArrayOfProductDetail value) {
        this.productDetails = value;
    }

}
