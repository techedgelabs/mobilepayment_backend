
package com.techedge.mp.payment.adapter.wss2s;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ShippingAddress complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ShippingAddress">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element minOccurs="0" maxOccurs="1" name="ProfileID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Lastname" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="StreetName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Streetname2" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="HouseNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="HouseExtention" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="City" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="ZipCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="State" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="CountryCode" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Email" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="PrimaryPhone" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="SecondaryPhone" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ShippingAddress", propOrder = {
    "profileID",
    "firstName",
    "middleName",
    "lastName",
    "streetName",
    "streetName2",
    "houseNumber",
    "houseExtention",
    "city",
    "zipCode",
    "state",
    "countryCode",
    "email",
    "primaryPhone",
    "secondaryPhone"
})
public class ShippingAddress {

    protected String profileID;
    protected String firstName;
    protected String middleName;
    protected String lastName;
    protected String streetName;
    protected String streetName2;
    protected String houseNumber;
    protected String houseExtention;
    protected String city;
    protected String zipCode;
    protected String state;
    protected String countryCode;
    protected String email;
    protected String primaryPhone;
    protected String secondaryPhone;
    /**
     * @return the profileID
     */
    public String getProfileID() {
        return profileID;
    }
    /**
     * @param profileID the profileID to set
     */
    public void setProfileID(String profileID) {
        this.profileID = profileID;
    }
    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }
    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }
    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * @return the streetName
     */
    public String getStreetName() {
        return streetName;
    }
    /**
     * @param streetName the streetName to set
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }
    /**
     * @return the streetName2
     */
    public String getStreetName2() {
        return streetName2;
    }
    /**
     * @param streetName2 the streetName2 to set
     */
    public void setStreetName2(String streetName2) {
        this.streetName2 = streetName2;
    }
    /**
     * @return the houseNumber
     */
    public String getHouseNumber() {
        return houseNumber;
    }
    /**
     * @param houseNumber the houseNumber to set
     */
    public void setHouseNumber(String houseNumber) {
        this.houseNumber = houseNumber;
    }
    /**
     * @return the houseExtention
     */
    public String getHouseExtention() {
        return houseExtention;
    }
    /**
     * @param houseExtention the houseExtention to set
     */
    public void setHouseExtention(String houseExtention) {
        this.houseExtention = houseExtention;
    }
    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }
    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }
    /**
     * @return the zipCode
     */
    public String getZipCode() {
        return zipCode;
    }
    /**
     * @param zipCode the zipCode to set
     */
    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
    /**
     * @return the state
     */
    public String getState() {
        return state;
    }
    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }
    /**
     * @return the countryCode
     */
    public String getCountryCode() {
        return countryCode;
    }
    /**
     * @param countryCode the countryCode to set
     */
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }
    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }
    /**
     * @return the primaryPhone
     */
    public String getPrimaryPhone() {
        return primaryPhone;
    }
    /**
     * @param primaryPhone the primaryPhone to set
     */
    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }
    /**
     * @return the secondaryPhone
     */
    public String getSecondaryPhone() {
        return secondaryPhone;
    }
    /**
     * @param secondaryPhone the secondaryPhone to set
     */
    public void setSecondaryPhone(String secondaryPhone) {
        this.secondaryPhone = secondaryPhone;
    }




}
