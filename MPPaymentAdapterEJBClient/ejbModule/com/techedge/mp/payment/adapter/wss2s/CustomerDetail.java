
package com.techedge.mp.payment.adapter.wss2s;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per CustomerDetail complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="CustomerDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element minOccurs="0" maxOccurs="1" name="ProfileID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="MerchantCustomerID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="FirstName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="MiddleName" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Lastname" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="PrimaryEmail" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="SecondaryEmail" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="PrimaryPhone" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="SecondaryPhone" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="DateOfBirth" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Gender" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="SocialSecurityNumber" type="{http://www.w3.org/2001/XMLSchema}string" />
 *         &lt;element minOccurs="0" maxOccurs="1" name="Company" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerDetail", propOrder = {
    "profileID",
    "merchantCustomerID",
    "firstName",
    "middleName",
    "lastName",
    "primaryEmail",
    "secondaryEmail",
    "primaryPhone",
    "secondaryPhone",
    "dateOfBirth",
    "gender",
    "socialSecurityNumber",
    "company"
})
public class CustomerDetail {

    protected String profileID;
    protected String merchantCustomerID;
    protected String firstName;
    protected String middleName;
    protected String lastName;
    protected String primaryEmail;
    protected String secondaryEmail;
    protected String primaryPhone;
    protected String secondaryPhone;
    protected String dateOfBirth;
    protected String gender;
    protected String socialSecurityNumber;
    protected String company;
    /**
     * @return the profileID
     */
    public String getProfileID() {
        return profileID;
    }
    /**
     * @param profileID the profileID to set
     */
    public void setProfileID(String profileID) {
        this.profileID = profileID;
    }
    /**
     * @return the merchantCustomerID
     */
    public String getMerchantCustomerID() {
        return merchantCustomerID;
    }
    /**
     * @param merchantCustomerID the merchantCustomerID to set
     */
    public void setMerchantCustomerID(String merchantCustomerID) {
        this.merchantCustomerID = merchantCustomerID;
    }
    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }
    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    /**
     * @return the middleName
     */
    public String getMiddleName() {
        return middleName;
    }
    /**
     * @param middleName the middleName to set
     */
    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }
    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }
    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    /**
     * @return the primaryEmail
     */
    public String getPrimaryEmail() {
        return primaryEmail;
    }
    /**
     * @param primaryEmail the primaryEmail to set
     */
    public void setPrimaryEmail(String primaryEmail) {
        this.primaryEmail = primaryEmail;
    }
    /**
     * @return the secondaryEmail
     */
    public String getSecondaryEmail() {
        return secondaryEmail;
    }
    /**
     * @param secondaryEmail the secondaryEmail to set
     */
    public void setSecondaryEmail(String secondaryEmail) {
        this.secondaryEmail = secondaryEmail;
    }
    /**
     * @return the primaryPhone
     */
    public String getPrimaryPhone() {
        return primaryPhone;
    }
    /**
     * @param primaryPhone the primaryPhone to set
     */
    public void setPrimaryPhone(String primaryPhone) {
        this.primaryPhone = primaryPhone;
    }
    /**
     * @return the secondaryPhone
     */
    public String getSecondaryPhone() {
        return secondaryPhone;
    }
    /**
     * @param secondaryPhone the secondaryPhone to set
     */
    public void setSecondaryPhone(String secondaryPhone) {
        this.secondaryPhone = secondaryPhone;
    }
    /**
     * @return the dateOfBirth
     */
    public String getDateOfBirth() {
        return dateOfBirth;
    }
    /**
     * @param dateOfBirth the dateOfBirth to set
     */
    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }
    /**
     * @return the gender
     */
    public String getGender() {
        return gender;
    }
    /**
     * @param gender the gender to set
     */
    public void setGender(String gender) {
        this.gender = gender;
    }
    /**
     * @return the socialSecurityNumber
     */
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }
    /**
     * @param socialSecurityNumber the socialSecurityNumber to set
     */
    public void setSocialSecurityNumber(String socialSecurityNumber) {
        this.socialSecurityNumber = socialSecurityNumber;
    }
    /**
     * @return the company
     */
    public String getCompany() {
        return company;
    }
    /**
     * @param company the company to set
     */
    public void setCompany(String company) {
        this.company = company;
    }




}
