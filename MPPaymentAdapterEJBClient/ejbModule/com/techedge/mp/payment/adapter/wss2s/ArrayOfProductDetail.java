
package com.techedge.mp.payment.adapter.wss2s;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ArrayOfProductDetail complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ArrayOfProductDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element minOccurs="0" maxOccurs="unbounded" name="ProductDetail" nillable="true" type="{https://ecomms2s.sella.it/}ProductDetail"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOfProductDetail", propOrder = {
    "productDetail"
})
public class ArrayOfProductDetail {

    protected ProductDetail productDetail;

    /**
     * Recupera il valore della proprietÓ shipToName.
     * 
     * @return
     *     possible object is
     *     {@link ProductDetail }
     *     
     */
    public ProductDetail getProductDetail() {
        return productDetail;
    }

    /**
     * Imposta il valore della proprietÓ productDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductDetail }
     *     
     */
    public void setProductDetail(ProductDetail value) {
        this.productDetail = value;
    }



}
