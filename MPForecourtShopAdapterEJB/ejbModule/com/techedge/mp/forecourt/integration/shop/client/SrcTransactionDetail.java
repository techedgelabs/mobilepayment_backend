
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per SrcTransactionDetail complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="SrcTransactionDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="SrcTransactionStatus" type="{http://gatewaymobilepayment.4ts.it/}SrcTransactionStatusEnum"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SrcTransactionDetail", propOrder = {
    "amount",
    "srcTransactionStatus"
})
public class SrcTransactionDetail {

    @XmlElement(name = "Amount")
    protected double amount;
    @XmlElement(name = "SrcTransactionStatus", required = true)
    protected SrcTransactionStatusEnum srcTransactionStatus;

    /**
     * Recupera il valore della proprietÓ amount.
     * 
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Imposta il valore della proprietÓ amount.
     * 
     */
    public void setAmount(double value) {
        this.amount = value;
    }

    /**
     * Recupera il valore della proprietÓ srcTransactionStatus.
     * 
     * @return
     *     possible object is
     *     {@link SrcTransactionStatusEnum }
     *     
     */
    public SrcTransactionStatusEnum getSrcTransactionStatus() {
        return srcTransactionStatus;
    }

    /**
     * Imposta il valore della proprietÓ srcTransactionStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link SrcTransactionStatusEnum }
     *     
     */
    public void setSrcTransactionStatus(SrcTransactionStatusEnum value) {
        this.srcTransactionStatus = value;
    }

}
