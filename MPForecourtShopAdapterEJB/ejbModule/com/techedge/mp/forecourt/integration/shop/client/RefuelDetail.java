
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per RefuelDetail complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="RefuelDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TimestampEndRefuel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="FuelType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="FuelQuantity" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="ProductId" type="{http://gatewaymobilepayment.4ts.it/}ProductIdEnum" minOccurs="0"/>
 *         &lt;element name="ProductDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="UnitPrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RefuelDetail", propOrder = {
    "timestampEndRefuel",
    "amount",
    "fuelType",
    "fuelQuantity",
    "productId",
    "productDescription",
    "unitPrice"
})
public class RefuelDetail {

    @XmlElement(name = "TimestampEndRefuel", required = true, nillable = true)
    protected String timestampEndRefuel;
    @XmlElement(name = "Amount")
    protected double amount;
    @XmlElement(name = "FuelType")
    protected String fuelType;
    @XmlElement(name = "FuelQuantity")
    protected Double fuelQuantity;
    @XmlElement(name = "ProductId")
    @XmlSchemaType(name = "string")
    protected ProductIdEnum productId;
    @XmlElement(name = "ProductDescription")
    protected String productDescription;
    @XmlElement(name = "UnitPrice")
    protected Double unitPrice;

    /**
     * Recupera il valore della proprietÓ timestampEndRefuel.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTimestampEndRefuel() {
        return timestampEndRefuel;
    }

    /**
     * Imposta il valore della proprietÓ timestampEndRefuel.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTimestampEndRefuel(String value) {
        this.timestampEndRefuel = value;
    }

    /**
     * Recupera il valore della proprietÓ amount.
     * 
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Imposta il valore della proprietÓ amount.
     * 
     */
    public void setAmount(double value) {
        this.amount = value;
    }

    /**
     * Recupera il valore della proprietÓ fuelType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelType() {
        return fuelType;
    }

    /**
     * Imposta il valore della proprietÓ fuelType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelType(String value) {
        this.fuelType = value;
    }

    /**
     * Recupera il valore della proprietÓ fuelQuantity.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getFuelQuantity() {
        return fuelQuantity;
    }

    /**
     * Imposta il valore della proprietÓ fuelQuantity.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setFuelQuantity(Double value) {
        this.fuelQuantity = value;
    }

    /**
     * Recupera il valore della proprietÓ productId.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdEnum }
     *     
     */
    public ProductIdEnum getProductId() {
        return productId;
    }

    /**
     * Imposta il valore della proprietÓ productId.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdEnum }
     *     
     */
    public void setProductId(ProductIdEnum value) {
        this.productId = value;
    }

    /**
     * Recupera il valore della proprietÓ productDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductDescription() {
        return productDescription;
    }

    /**
     * Imposta il valore della proprietÓ productDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductDescription(String value) {
        this.productDescription = value;
    }

    /**
     * Recupera il valore della proprietÓ unitPrice.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getUnitPrice() {
        return unitPrice;
    }

    /**
     * Imposta il valore della proprietÓ unitPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setUnitPrice(Double value) {
        this.unitPrice = value;
    }

}
