
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GetSrcTransactionStatusResponse" type="{http://gatewaymobilepayment.4ts.it/}GetSrcTransactionStatusMessageResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getSrcTransactionStatusResponse"
})
@XmlRootElement(name = "GetSrcTransactionStatusResponse")
public class GetSrcTransactionStatusResponse {

    @XmlElement(name = "GetSrcTransactionStatusResponse")
    protected GetSrcTransactionStatusMessageResponse getSrcTransactionStatusResponse;

    /**
     * Recupera il valore della proprietÓ getSrcTransactionStatusResponse.
     * 
     * @return
     *     possible object is
     *     {@link GetSrcTransactionStatusMessageResponse }
     *     
     */
    public GetSrcTransactionStatusMessageResponse getGetSrcTransactionStatusResponse() {
        return getSrcTransactionStatusResponse;
    }

    /**
     * Imposta il valore della proprietÓ getSrcTransactionStatusResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link GetSrcTransactionStatusMessageResponse }
     *     
     */
    public void setGetSrcTransactionStatusResponse(GetSrcTransactionStatusMessageResponse value) {
        this.getSrcTransactionStatusResponse = value;
    }

}
