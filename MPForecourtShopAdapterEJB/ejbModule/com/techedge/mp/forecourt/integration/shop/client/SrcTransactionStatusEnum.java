
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per SrcTransactionStatusEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="SrcTransactionStatusEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PAID"/>
 *     &lt;enumeration value="ONHOLD"/>
 *     &lt;enumeration value="CANCELLED"/>
 *     &lt;enumeration value="REVERSED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "SrcTransactionStatusEnum")
@XmlEnum
public enum SrcTransactionStatusEnum {

    PAID,
    ONHOLD,
    CANCELLED,
    REVERSED;

    public String value() {
        return name();
    }

    public static SrcTransactionStatusEnum fromValue(String v) {
        return valueOf(v);
    }

}
