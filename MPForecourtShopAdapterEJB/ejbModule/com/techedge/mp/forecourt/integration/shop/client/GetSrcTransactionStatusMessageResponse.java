
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per GetSrcTransactionStatusMessageResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="GetSrcTransactionStatusMessageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusCode" type="{http://gatewaymobilepayment.4ts.it/}GetSrcTransactionStatusStatusCode"/>
 *         &lt;element name="MessageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SrcTransactionDetail" type="{http://gatewaymobilepayment.4ts.it/}SrcTransactionDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSrcTransactionStatusMessageResponse", propOrder = {
    "statusCode",
    "messageCode",
    "srcTransactionDetail"
})
public class GetSrcTransactionStatusMessageResponse {

    @XmlElement(name = "StatusCode", required = true)
    protected GetSrcTransactionStatusStatusCode statusCode;
    @XmlElement(name = "MessageCode", required = true, nillable = true)
    protected String messageCode;
    @XmlElement(name = "SrcTransactionDetail")
    protected SrcTransactionDetail srcTransactionDetail;

    /**
     * Recupera il valore della proprietÓ statusCode.
     * 
     * @return
     *     possible object is
     *     {@link GetSrcTransactionStatusStatusCode }
     *     
     */
    public GetSrcTransactionStatusStatusCode getStatusCode() {
        return statusCode;
    }

    /**
     * Imposta il valore della proprietÓ statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link GetSrcTransactionStatusStatusCode }
     *     
     */
    public void setStatusCode(GetSrcTransactionStatusStatusCode value) {
        this.statusCode = value;
    }

    /**
     * Recupera il valore della proprietÓ messageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Imposta il valore della proprietÓ messageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    /**
     * Recupera il valore della proprietÓ srcTransactionDetail.
     * 
     * @return
     *     possible object is
     *     {@link SrcTransactionDetail }
     *     
     */
    public SrcTransactionDetail getSrcTransactionDetail() {
        return srcTransactionDetail;
    }

    /**
     * Imposta il valore della proprietÓ srcTransactionDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link SrcTransactionDetail }
     *     
     */
    public void setSrcTransactionDetail(SrcTransactionDetail value) {
        this.srcTransactionDetail = value;
    }

}
