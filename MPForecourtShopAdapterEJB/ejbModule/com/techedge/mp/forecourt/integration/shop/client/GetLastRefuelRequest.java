
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per GetLastRefuelRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="GetLastRefuelRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PumpId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetLastRefuelRequest", propOrder = {
    "requestId",
    "pumpId"
})
public class GetLastRefuelRequest {

    @XmlElement(name = "RequestId", required = true, nillable = true)
    protected String requestId;
    @XmlElement(name = "PumpId", required = true, nillable = true)
    protected String pumpId;

    /**
     * Recupera il valore della proprietÓ requestId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Imposta il valore della proprietÓ requestId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Recupera il valore della proprietÓ pumpId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpId() {
        return pumpId;
    }

    /**
     * Imposta il valore della proprietÓ pumpId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpId(String value) {
        this.pumpId = value;
    }

}
