
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per PaymentTransactionResult complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="PaymentTransactionResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Amount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="EventType" type="{http://gatewaymobilepayment.4ts.it/}EventTypeEnum"/>
 *         &lt;element name="TransactionResult" type="{http://gatewaymobilepayment.4ts.it/}TransactionResultEnum"/>
 *         &lt;element name="ShopLogin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AcquirerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PaymentMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ShopTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="BankTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="AuthorizationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="Currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ErrorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ErrorDescritpion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaymentTransactionResult", propOrder = {
    "amount",
    "eventType",
    "transactionResult",
    "shopLogin",
    "acquirerId",
    "paymentMode",
    "shopTransactionId",
    "bankTransactionId",
    "authorizationCode",
    "currency",
    "errorCode",
    "errorDescritpion"
})
public class PaymentTransactionResult {

    @XmlElement(name = "Amount")
    protected double amount;
    @XmlElement(name = "EventType", required = true)
    protected EventTypeEnum eventType;
    @XmlElement(name = "TransactionResult", required = true)
    protected TransactionResultEnum transactionResult;
    @XmlElement(name = "ShopLogin")
    protected String shopLogin;
    @XmlElement(name = "AcquirerId")
    protected String acquirerId;
    @XmlElement(name = "PaymentMode")
    protected String paymentMode;
    @XmlElement(name = "ShopTransactionId")
    protected String shopTransactionId;
    @XmlElement(name = "BankTransactionId")
    protected String bankTransactionId;
    @XmlElement(name = "AuthorizationCode")
    protected String authorizationCode;
    @XmlElement(name = "Currency")
    protected String currency;
    @XmlElement(name = "ErrorCode")
    protected String errorCode;
    @XmlElement(name = "ErrorDescritpion")
    protected String errorDescritpion;

    /**
     * Recupera il valore della proprietÓ amount.
     * 
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Imposta il valore della proprietÓ amount.
     * 
     */
    public void setAmount(double value) {
        this.amount = value;
    }

    /**
     * Recupera il valore della proprietÓ eventType.
     * 
     * @return
     *     possible object is
     *     {@link EventTypeEnum }
     *     
     */
    public EventTypeEnum getEventType() {
        return eventType;
    }

    /**
     * Imposta il valore della proprietÓ eventType.
     * 
     * @param value
     *     allowed object is
     *     {@link EventTypeEnum }
     *     
     */
    public void setEventType(EventTypeEnum value) {
        this.eventType = value;
    }

    /**
     * Recupera il valore della proprietÓ transactionResult.
     * 
     * @return
     *     possible object is
     *     {@link TransactionResultEnum }
     *     
     */
    public TransactionResultEnum getTransactionResult() {
        return transactionResult;
    }

    /**
     * Imposta il valore della proprietÓ transactionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionResultEnum }
     *     
     */
    public void setTransactionResult(TransactionResultEnum value) {
        this.transactionResult = value;
    }

    /**
     * Recupera il valore della proprietÓ shopLogin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopLogin() {
        return shopLogin;
    }

    /**
     * Imposta il valore della proprietÓ shopLogin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopLogin(String value) {
        this.shopLogin = value;
    }

    /**
     * Recupera il valore della proprietÓ acquirerId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcquirerId() {
        return acquirerId;
    }

    /**
     * Imposta il valore della proprietÓ acquirerId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcquirerId(String value) {
        this.acquirerId = value;
    }

    /**
     * Recupera il valore della proprietÓ paymentMode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * Imposta il valore della proprietÓ paymentMode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMode(String value) {
        this.paymentMode = value;
    }

    /**
     * Recupera il valore della proprietÓ shopTransactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopTransactionId() {
        return shopTransactionId;
    }

    /**
     * Imposta il valore della proprietÓ shopTransactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopTransactionId(String value) {
        this.shopTransactionId = value;
    }

    /**
     * Recupera il valore della proprietÓ bankTransactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankTransactionId() {
        return bankTransactionId;
    }

    /**
     * Imposta il valore della proprietÓ bankTransactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankTransactionId(String value) {
        this.bankTransactionId = value;
    }

    /**
     * Recupera il valore della proprietÓ authorizationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationCode() {
        return authorizationCode;
    }

    /**
     * Imposta il valore della proprietÓ authorizationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationCode(String value) {
        this.authorizationCode = value;
    }

    /**
     * Recupera il valore della proprietÓ currency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Imposta il valore della proprietÓ currency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Recupera il valore della proprietÓ errorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Imposta il valore della proprietÓ errorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Recupera il valore della proprietÓ errorDescritpion.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorDescritpion() {
        return errorDescritpion;
    }

    /**
     * Imposta il valore della proprietÓ errorDescritpion.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorDescritpion(String value) {
        this.errorDescritpion = value;
    }

}
