
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per ProductDetail complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="ProductDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ProductID" type="{http://gatewaymobilepayment.4ts.it/}ProductIdEnum"/>
 *         &lt;element name="ProductDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="FuelType" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="ProductPrice" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProductDetail", propOrder = {
    "productID",
    "productDescription",
    "fuelType",
    "productPrice"
})
public class ProductDetail {

    @XmlElement(name = "ProductID", required = true)
    protected ProductIdEnum productID;
    @XmlElement(name = "ProductDescription", required = true, nillable = true)
    protected String productDescription;
    @XmlElement(name = "FuelType", required = true, nillable = true)
    protected String fuelType;
    @XmlElement(name = "ProductPrice")
    protected Double productPrice;

    /**
     * Recupera il valore della proprietÓ productID.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdEnum }
     *     
     */
    public ProductIdEnum getProductID() {
        return productID;
    }

    /**
     * Imposta il valore della proprietÓ productID.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdEnum }
     *     
     */
    public void setProductID(ProductIdEnum value) {
        this.productID = value;
    }

    /**
     * Recupera il valore della proprietÓ productDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductDescription() {
        return productDescription;
    }

    /**
     * Imposta il valore della proprietÓ productDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductDescription(String value) {
        this.productDescription = value;
    }

    /**
     * Recupera il valore della proprietÓ fuelType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFuelType() {
        return fuelType;
    }

    /**
     * Imposta il valore della proprietÓ fuelType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFuelType(String value) {
        this.fuelType = value;
    }

    /**
     * Recupera il valore della proprietÓ productPrice.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getProductPrice() {
        return productPrice;
    }

    /**
     * Imposta il valore della proprietÓ productPrice.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setProductPrice(Double value) {
        this.productPrice = value;
    }

}
