
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per GetLastRefuelMessageResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="GetLastRefuelMessageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusCode" type="{http://gatewaymobilepayment.4ts.it/}GetLastRefuelStatusCode"/>
 *         &lt;element name="MessageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="SrcTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PumpId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PumpNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="RefuelMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="StationDetail" type="{http://gatewaymobilepayment.4ts.it/}StationDetail"/>
 *         &lt;element name="RefuelDetail" type="{http://gatewaymobilepayment.4ts.it/}RefuelDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetLastRefuelMessageResponse", propOrder = {
    "statusCode",
    "messageCode",
    "srcTransactionId",
    "pumpId",
    "pumpNumber",
    "refuelMode",
    "stationDetail",
    "refuelDetail"
})
public class GetLastRefuelMessageResponse {

    @XmlElement(name = "StatusCode", required = true)
    protected GetLastRefuelStatusCode statusCode;
    @XmlElement(name = "MessageCode", required = true, nillable = true)
    protected String messageCode;
    @XmlElement(name = "SrcTransactionId")
    protected String srcTransactionId;
    @XmlElement(name = "PumpId", required = true, nillable = true)
    protected String pumpId;
    @XmlElement(name = "PumpNumber", required = true, nillable = true)
    protected String pumpNumber;
    @XmlElement(name = "RefuelMode")
    protected String refuelMode;
    @XmlElement(name = "StationDetail", required = true, nillable = true)
    protected StationDetail stationDetail;
    @XmlElement(name = "RefuelDetail")
    protected RefuelDetail refuelDetail;

    /**
     * Recupera il valore della proprietÓ statusCode.
     * 
     * @return
     *     possible object is
     *     {@link GetLastRefuelStatusCode }
     *     
     */
    public GetLastRefuelStatusCode getStatusCode() {
        return statusCode;
    }

    /**
     * Imposta il valore della proprietÓ statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link GetLastRefuelStatusCode }
     *     
     */
    public void setStatusCode(GetLastRefuelStatusCode value) {
        this.statusCode = value;
    }

    /**
     * Recupera il valore della proprietÓ messageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Imposta il valore della proprietÓ messageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    /**
     * Recupera il valore della proprietÓ srcTransactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrcTransactionId() {
        return srcTransactionId;
    }

    /**
     * Imposta il valore della proprietÓ srcTransactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrcTransactionId(String value) {
        this.srcTransactionId = value;
    }

    /**
     * Recupera il valore della proprietÓ pumpId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpId() {
        return pumpId;
    }

    /**
     * Imposta il valore della proprietÓ pumpId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpId(String value) {
        this.pumpId = value;
    }

    /**
     * Recupera il valore della proprietÓ pumpNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpNumber() {
        return pumpNumber;
    }

    /**
     * Imposta il valore della proprietÓ pumpNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpNumber(String value) {
        this.pumpNumber = value;
    }

    /**
     * Recupera il valore della proprietÓ refuelMode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefuelMode() {
        return refuelMode;
    }

    /**
     * Imposta il valore della proprietÓ refuelMode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefuelMode(String value) {
        this.refuelMode = value;
    }

    /**
     * Recupera il valore della proprietÓ stationDetail.
     * 
     * @return
     *     possible object is
     *     {@link StationDetail }
     *     
     */
    public StationDetail getStationDetail() {
        return stationDetail;
    }

    /**
     * Imposta il valore della proprietÓ stationDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link StationDetail }
     *     
     */
    public void setStationDetail(StationDetail value) {
        this.stationDetail = value;
    }

    /**
     * Recupera il valore della proprietÓ refuelDetail.
     * 
     * @return
     *     possible object is
     *     {@link RefuelDetail }
     *     
     */
    public RefuelDetail getRefuelDetail() {
        return refuelDetail;
    }

    /**
     * Imposta il valore della proprietÓ refuelDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link RefuelDetail }
     *     
     */
    public void setRefuelDetail(RefuelDetail value) {
        this.refuelDetail = value;
    }

}
