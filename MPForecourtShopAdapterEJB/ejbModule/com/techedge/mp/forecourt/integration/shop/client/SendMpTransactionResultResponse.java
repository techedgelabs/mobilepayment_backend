
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="SendMpTransactionResultResponse" type="{http://gatewaymobilepayment.4ts.it/}SendMpTransactionResultMessageResponse" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendMpTransactionResultResponse"
})
@XmlRootElement(name = "SendMpTransactionResultResponse")
public class SendMpTransactionResultResponse {

    @XmlElement(name = "SendMpTransactionResultResponse")
    protected SendMpTransactionResultMessageResponse sendMpTransactionResultResponse;

    /**
     * Recupera il valore della proprietÓ sendMpTransactionResultResponse.
     * 
     * @return
     *     possible object is
     *     {@link SendMpTransactionResultMessageResponse }
     *     
     */
    public SendMpTransactionResultMessageResponse getSendMpTransactionResultResponse() {
        return sendMpTransactionResultResponse;
    }

    /**
     * Imposta il valore della proprietÓ sendMpTransactionResultResponse.
     * 
     * @param value
     *     allowed object is
     *     {@link SendMpTransactionResultMessageResponse }
     *     
     */
    public void setSendMpTransactionResultResponse(SendMpTransactionResultMessageResponse value) {
        this.sendMpTransactionResultResponse = value;
    }

}
