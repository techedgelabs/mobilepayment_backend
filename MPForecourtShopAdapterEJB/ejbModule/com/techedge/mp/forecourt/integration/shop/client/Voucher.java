
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per Voucher complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="Voucher">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="VoucherAmount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="VoucherCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PromoCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="PromoDescription" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Voucher", propOrder = {
    "voucherAmount",
    "voucherCode",
    "promoCode",
    "promoDescription"
})
public class Voucher {

    @XmlElement(name = "VoucherAmount")
    protected double voucherAmount;
    @XmlElement(name = "VoucherCode", required = true, nillable = true)
    protected String voucherCode;
    @XmlElement(name = "PromoCode", required = true, nillable = true)
    protected String promoCode;
    @XmlElement(name = "PromoDescription", required = true, nillable = true)
    protected String promoDescription;

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getPromoDescription() {
        return promoDescription;
    }

    public void setPromoDescription(String promoDescription) {
        this.promoDescription = promoDescription;
    }

    /**
     * Recupera il valore della proprietÓ voucherAmount.
     * 
     */
    public double getVoucherAmount() {
        return voucherAmount;
    }

    /**
     * Imposta il valore della proprietÓ voucherAmount.
     * 
     */
    public void setVoucherAmount(double value) {
        this.voucherAmount = value;
    }

    /**
     * Recupera il valore della proprietÓ voucherCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVoucherCode() {
        return voucherCode;
    }

    /**
     * Imposta il valore della proprietÓ voucherCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVoucherCode(String value) {
        this.voucherCode = value;
    }

}
