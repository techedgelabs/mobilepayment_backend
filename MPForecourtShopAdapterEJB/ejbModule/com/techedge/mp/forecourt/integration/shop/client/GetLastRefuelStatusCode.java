
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per GetLastRefuelStatusCode.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="GetLastRefuelStatusCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="LAST_REFUEL_FOUND_200"/>
 *     &lt;enumeration value="LAST_REFUEL_NOT_FOUND_404"/>
 *     &lt;enumeration value="PUMP_STATUS_NOT_AVAILABLE_500"/>
 *     &lt;enumeration value="PUMP_IN_FUELLING_500"/>
 *     &lt;enumeration value="PUMP_NOT_FOUND_404"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GetLastRefuelStatusCode")
@XmlEnum
public enum GetLastRefuelStatusCode {

    LAST_REFUEL_FOUND_200,
    LAST_REFUEL_NOT_FOUND_404,
    PUMP_STATUS_NOT_AVAILABLE_500,
    PUMP_IN_FUELLING_500,
    PUMP_NOT_FOUND_404;

    public String value() {
        return name();
    }

    public static GetLastRefuelStatusCode fromValue(String v) {
        return valueOf(v);
    }

}
