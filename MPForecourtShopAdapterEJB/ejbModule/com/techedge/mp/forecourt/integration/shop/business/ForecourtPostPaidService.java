package com.techedge.mp.forecourt.integration.shop.business;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.forecourt.integration.shop.business.exception.BPELException;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.forecourt.integration.shop.client.EventTypeEnum;
import com.techedge.mp.forecourt.integration.shop.client.GetLastRefuelRequest;
import com.techedge.mp.forecourt.integration.shop.client.GetSrcTransactionStatusMessageResponse;
import com.techedge.mp.forecourt.integration.shop.client.GetSrcTransactionStatusRequest;
import com.techedge.mp.forecourt.integration.shop.client.GfgShopServiceImpl;
import com.techedge.mp.forecourt.integration.shop.client.IGfgShopService;
import com.techedge.mp.forecourt.integration.shop.client.SendMpTransactionResultMessageResponse;
import com.techedge.mp.forecourt.integration.shop.client.SendMpTransactionResultRequest;
import com.techedge.mp.forecourt.integration.shop.client.TransactionResultEnum;
import com.techedge.mp.forecourt.integration.shop.client.TransactionStatusEnum;
import com.techedge.mp.forecourt.integration.shop.client.Voucher;
import com.techedge.mp.forecourt.integration.shop.interfaces.ElectronicInvoice;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetLastRefuelMessageResponse;
import com.techedge.mp.forecourt.integration.shop.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.integration.shop.interfaces.PumpDetail;
import com.techedge.mp.forecourt.integration.shop.interfaces.RefuelDetails;
import com.techedge.mp.forecourt.integration.shop.interfaces.SendMPTransactionResultMessageResponse;
import com.techedge.mp.forecourt.integration.shop.interfaces.SrcTransactionDetail;
import com.techedge.mp.forecourt.integration.shop.interfaces.StationDetail;
import com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail;

@Stateless
@LocalBean
public class ForecourtPostPaidService implements ForecourtPostPaidServiceLocal, ForecourtPostPaidServiceRemote {

    private final static String     PARAM_FORECOURT_PP_WSDL                        = "FORECOURT_PP_WSDL";
    private final static String     PARAM_SIMULATION_ACTIVE                        = "SIMULATION_ACTIVE";
    private final static String     PARAM_SIMULATION_SENDMPTRANSACTIONRESULT_ERROR = "SIMULATION_GFG_SENDMPTRANSACTIONRESULT_ERROR";
    private final static String     PARAM_SIMULATION_SENDMPTRANSACTIONRESULT_KO    = "SIMULATION_GFG_SENDMPTRANSACTIONRESULT_KO";

    private ParametersServiceRemote parametersService                              = null;
    private LoggerServiceRemote     loggerService                                  = null;
    private boolean                 simulationActive                               = false;

    private URL                     url;

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    private URL getUrl() throws BPELException {

        if (this.url == null) {

            String wsdlString = "";
            try {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
                wsdlString = parametersService.getParamValue(ForecourtPostPaidService.PARAM_FORECOURT_PP_WSDL);
                this.simulationActive = Boolean.parseBoolean(parametersService.getParamValue(ForecourtPostPaidService.PARAM_SIMULATION_ACTIVE));
            }
            catch (ParameterNotFoundException e) {

                e.printStackTrace();

                throw new BPELException("Parameter " + ForecourtPostPaidService.PARAM_FORECOURT_PP_WSDL + " not found: " + e.getMessage());
            }
            catch (InterfaceNotFoundException e) {

                e.printStackTrace();

                throw new BPELException("InterfaceNotFoundException for jndi " + e.getJndiString());
            }

            try {
                this.url = new URL(wsdlString);
            }
            catch (MalformedURLException e) {

                e.printStackTrace();

                throw new BPELException("Malformed URL: " + e.getMessage());
            }
        }

        if (simulationActive) {
            System.out.println("Simulazione GFG attiva");
        }

        return this.url;
    }

    @Override
    public SendMPTransactionResultMessageResponse sendMPTransactionResult(String requestID, String stationID, String mpTransactionID, String srcTransactionID,
            String transactionResult, PaymentTransactionResult paymentTransactionResult, Boolean loyaltyCredits, List<VoucherDetail> vouchers, ElectronicInvoice electronicInvoice) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));
        inputParameters.add(new Pair<String, String>("srcTransactionID", srcTransactionID));
        inputParameters.add(new Pair<String, String>("transactionResult", transactionResult));

        if (paymentTransactionResult != null) {
            String message = "{ amount: " + paymentTransactionResult.getAmount().toString() + ", shopTransactionID: " + paymentTransactionResult.getShopTransactionID()
                    + ", bankTransactionID: " + paymentTransactionResult.getBankTransactionID() + ", shopLogin: " + paymentTransactionResult.getShopLogin() + ", acquirerID: "
                    + paymentTransactionResult.getAcquirerID() + ", authorizationCode: " + paymentTransactionResult.getAuthorizationCode() + ", currency: "
                    + paymentTransactionResult.getCurrency() + ", eventType: " + paymentTransactionResult.getEventType() + ", paymentMode: "
                    + paymentTransactionResult.getPaymentMode() + ", transactionResult: " + paymentTransactionResult.getTransactionResult() + " }";

            inputParameters.add(new Pair<String, String>("paymentTransactionResult", message));
        }
        else {
            inputParameters.add(new Pair<String, String>("paymentTransactionResult", "null"));
        }

        if (electronicInvoice != null) {
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.address", electronicInvoice.getAddress()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.businnesName", electronicInvoice.getBusinessName()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.city", electronicInvoice.getCity()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.country", electronicInvoice.getCountry()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.emailAdddress", electronicInvoice.getEmailAdddress()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.fiscalCode", electronicInvoice.getFiscalCode()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.licensePlate", electronicInvoice.getLicensePlate()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.name", electronicInvoice.getName()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.pecEmailAdddress", electronicInvoice.getPecEmailAdddress()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.province", electronicInvoice.getProvince()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.sdiCode", electronicInvoice.getSdiCode()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.streetNumber", electronicInvoice.getStreetNumber()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.surname", electronicInvoice.getSurname()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.vatNumber", electronicInvoice.getVatNumber()));
            inputParameters.add(new Pair<String, String>("ElectronicInvoice.zipCode", electronicInvoice.getZipCode()));
        }
        else {
            inputParameters.add(new Pair<String, String>("ElectronicInvoice", "null"));
        }
        
        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendMPTransactionResult", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        SendMPTransactionResultMessageResponse sendMPTransactionResultMessageResponseOut = new SendMPTransactionResultMessageResponse();

        try {

            String simulationSendMPTransactionResultError = null;
            boolean simulationSendMPTransactionResultKO = false;

            new Proxy().unsetHttp();

            GfgShopServiceImpl service1 = new GfgShopServiceImpl(this.getUrl());

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendMPTransactionResult", requestID, null, "service creted from url: " + this.getUrl());

            IGfgShopService port1 = service1.getWsEndpointGatewayMobilePaymentShop();

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendMPTransactionResult", requestID, null, "port created");

            SendMpTransactionResultRequest sendMpTransactionResultRequest = new SendMpTransactionResultRequest();
            sendMpTransactionResultRequest.setRequestId(requestID);
            sendMpTransactionResultRequest.setStationId(stationID);
            sendMpTransactionResultRequest.setSrcTransactionId(srcTransactionID);
            sendMpTransactionResultRequest.setMpTransactionId(mpTransactionID);
            if (transactionResult.equals("ONHOLD")) {
                sendMpTransactionResultRequest.setMpTransactionStatus(TransactionStatusEnum.UNPAID);
            }
            else {
                sendMpTransactionResultRequest.setMpTransactionStatus(TransactionStatusEnum.valueOf(transactionResult));
            }
            com.techedge.mp.forecourt.integration.shop.client.PaymentTransactionResult paymentTransactionResultRequest = null;
            if (paymentTransactionResult != null) {
                paymentTransactionResultRequest = new com.techedge.mp.forecourt.integration.shop.client.PaymentTransactionResult();
                paymentTransactionResultRequest.setAmount(paymentTransactionResult.getAmount());
                paymentTransactionResultRequest.setEventType(EventTypeEnum.valueOf(paymentTransactionResult.getEventType()));
                paymentTransactionResultRequest.setTransactionResult(TransactionResultEnum.valueOf(paymentTransactionResult.getTransactionResult()));
                paymentTransactionResultRequest.setShopLogin(paymentTransactionResult.getShopLogin());
                paymentTransactionResultRequest.setAcquirerId(paymentTransactionResult.getAcquirerID());
                paymentTransactionResultRequest.setPaymentMode(paymentTransactionResult.getPaymentMode());
                paymentTransactionResultRequest.setShopTransactionId(paymentTransactionResult.getShopTransactionID());
                paymentTransactionResultRequest.setBankTransactionId(paymentTransactionResult.getBankTransactionID());
                paymentTransactionResultRequest.setAuthorizationCode(paymentTransactionResult.getAuthorizationCode());
                paymentTransactionResultRequest.setCurrency(paymentTransactionResult.getCurrency());
                paymentTransactionResultRequest.setErrorCode(paymentTransactionResult.getErrorCode());
                paymentTransactionResultRequest.setErrorDescritpion(paymentTransactionResult.getErrorDescription());
            }

            sendMpTransactionResultRequest.setPaymentTransactionResult(paymentTransactionResultRequest);

            sendMpTransactionResultRequest.setLoyaltyCredits(loyaltyCredits);

            for (VoucherDetail voucherDetail : vouchers) {

                Voucher voucher = new Voucher();
                voucher.setVoucherAmount(voucherDetail.getVoucherAmount());
                voucher.setVoucherCode(voucherDetail.getVoucherCode());
                voucher.setPromoCode(voucherDetail.getPromoCode());
                voucher.setPromoDescription(voucherDetail.getPromoDescription());
                sendMpTransactionResultRequest.getVoucher().add(voucher);
            }

            if (electronicInvoice != null) {
                com.techedge.mp.forecourt.integration.shop.client.ElectronicInvoice forecourtElectronicInvoice = new com.techedge.mp.forecourt.integration.shop.client.ElectronicInvoice();
                forecourtElectronicInvoice.setAddress(electronicInvoice.getAddress());
                forecourtElectronicInvoice.setBusinessName(electronicInvoice.getBusinessName());
                forecourtElectronicInvoice.setCity(electronicInvoice.getCity());
                forecourtElectronicInvoice.setCountry(electronicInvoice.getCountry());
                forecourtElectronicInvoice.setEmailAddress(electronicInvoice.getEmailAdddress());
                forecourtElectronicInvoice.setFiscalCode(electronicInvoice.getFiscalCode());
                forecourtElectronicInvoice.setLicensePlate(electronicInvoice.getLicensePlate());
                forecourtElectronicInvoice.setName(electronicInvoice.getName());
                forecourtElectronicInvoice.setPecEmailAddress(electronicInvoice.getPecEmailAdddress());
                forecourtElectronicInvoice.setProvince(electronicInvoice.getProvince());
                forecourtElectronicInvoice.setSdiCode(electronicInvoice.getSdiCode());
                forecourtElectronicInvoice.setStreetNumber(electronicInvoice.getStreetNumber());
                forecourtElectronicInvoice.setSurname(electronicInvoice.getSurname());
                forecourtElectronicInvoice.setVatNumber(electronicInvoice.getVatNumber());
                forecourtElectronicInvoice.setZipCode(electronicInvoice.getZipCode());
                
                sendMpTransactionResultRequest.setElectronicInvoice(forecourtElectronicInvoice);
                
                System.out.println("Aggiunta alla richiesta SOAP i dati della fattura elettronica");
                System.out.println("      address: " + forecourtElectronicInvoice.getAddress());
                System.out.println("      businnesName: " + forecourtElectronicInvoice.getBusinessName());
                System.out.println("      city: " + forecourtElectronicInvoice.getCity());
                System.out.println("      country: " + forecourtElectronicInvoice.getCountry());
                System.out.println("      emailAdddress: " + forecourtElectronicInvoice.getEmailAddress());
                System.out.println("      fiscalCode: " + forecourtElectronicInvoice.getFiscalCode());
                System.out.println("      licensePlate: " + forecourtElectronicInvoice.getLicensePlate());
                System.out.println("      name: " + forecourtElectronicInvoice.getName());
                System.out.println("      pecEmailAddress: " + forecourtElectronicInvoice.getPecEmailAddress());
                System.out.println("      province: " + forecourtElectronicInvoice.getProvince());
                System.out.println("      sdiCode: " + forecourtElectronicInvoice.getSdiCode());
                System.out.println("      streetNumber: " + forecourtElectronicInvoice.getStreetNumber());
                System.out.println("      surname: " + forecourtElectronicInvoice.getSurname());
                System.out.println("      vatNumber: " + forecourtElectronicInvoice.getVatNumber());
                System.out.println("      zipCode: " + forecourtElectronicInvoice.getZipCode());
            }
            
            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendMPTransactionResult", requestID, null, "call to remote service");

            if (simulationActive) {

                simulationSendMPTransactionResultError = parametersService.getParamValueNoCache(ForecourtPostPaidService.PARAM_SIMULATION_SENDMPTRANSACTIONRESULT_ERROR);
                simulationSendMPTransactionResultKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(ForecourtPostPaidService.PARAM_SIMULATION_SENDMPTRANSACTIONRESULT_KO));

                if (simulationSendMPTransactionResultError != null && simulationSendMPTransactionResultError.equalsIgnoreCase("before")) {
                    throw new Exception("Eccezione simulata di errore before");
                }

                if (simulationSendMPTransactionResultKO) {
                    sendMPTransactionResultMessageResponseOut.setStatusCode("SOURCE_STATUS_NOT_AVAILABLE_500");
                    sendMPTransactionResultMessageResponseOut.setMessageCode("Simulazione di KO");

                    return sendMPTransactionResultMessageResponseOut;
                }
            }

            SendMpTransactionResultMessageResponse sendMpTransactionResultResponse = port1.sendMpTransactionResult(sendMpTransactionResultRequest);

            sendMPTransactionResultMessageResponseOut.setStatusCode(sendMpTransactionResultResponse.getStatusCode().value());
            sendMPTransactionResultMessageResponseOut.setMessageCode(sendMpTransactionResultResponse.getMessageCode());
            sendMPTransactionResultMessageResponseOut.setElectronicInvoiceID(sendMpTransactionResultResponse.getElectronicInvoiceID());

            if (simulationActive) {
                if (simulationSendMPTransactionResultError != null && simulationSendMPTransactionResultError.equalsIgnoreCase("after")) {
                    throw new Exception("Eccezione simulata di errore after");
                }
            }

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", sendMPTransactionResultMessageResponseOut.getStatusCode()));
            outputParameters.add(new Pair<String, String>("messageCode", sendMPTransactionResultMessageResponseOut.getMessageCode()));
            outputParameters.add(new Pair<String, String>("ElectronicInvoiceID", sendMPTransactionResultMessageResponseOut.getElectronicInvoiceID()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendMPTransactionResult", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return sendMPTransactionResultMessageResponseOut;

        }
        catch (BPELException e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendMPTransactionResult", requestID, null, "BPELException message: " + e.getMessage());

            sendMPTransactionResultMessageResponseOut.setStatusCode(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", sendMPTransactionResultMessageResponseOut.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendMPTransactionResult", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return sendMPTransactionResultMessageResponseOut;
        }
        catch (Exception e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendMPTransactionResult", requestID, null, "Exception message: " + e.getMessage());

            sendMPTransactionResultMessageResponseOut.setStatusCode(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", sendMPTransactionResultMessageResponseOut.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendMPTransactionResult", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return sendMPTransactionResultMessageResponseOut;
        }
    }

    @Override
    public com.techedge.mp.forecourt.integration.shop.interfaces.GetSrcTransactionStatusMessageResponse getTransactionStatus(String requestID, String mpTransactionID,
            String srcTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));
        inputParameters.add(new Pair<String, String>("srcTransactionID", srcTransactionID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionStatus", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        com.techedge.mp.forecourt.integration.shop.interfaces.GetSrcTransactionStatusMessageResponse getSRCTransactionStatusMessageResponseOUT = new com.techedge.mp.forecourt.integration.shop.interfaces.GetSrcTransactionStatusMessageResponse();

        try {

            new Proxy().unsetHttp();

            GfgShopServiceImpl service1 = new GfgShopServiceImpl(this.getUrl());

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "service creted from url: " + this.getUrl());

            IGfgShopService port1 = service1.getWsEndpointGatewayMobilePaymentShop();

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "port created");

            GetSrcTransactionStatusRequest getSrcTransactionStatusRequest = new GetSrcTransactionStatusRequest();
            getSrcTransactionStatusRequest.setRequestId(requestID);
            getSrcTransactionStatusRequest.setMpTransactionId(mpTransactionID);
            getSrcTransactionStatusRequest.setSrcTransactionId(srcTransactionID);

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "call to remote service");

            GetSrcTransactionStatusMessageResponse getSrcTransactionStatusMessageResponse = port1.getSrcTransactionStatus(getSrcTransactionStatusRequest);

            getSRCTransactionStatusMessageResponseOUT.setStatusCode(getSrcTransactionStatusMessageResponse.getStatusCode().value());
            getSRCTransactionStatusMessageResponseOUT.setMessageCode(getSrcTransactionStatusMessageResponse.getMessageCode());

            if (getSrcTransactionStatusMessageResponse.getSrcTransactionDetail() != null) {

                SrcTransactionDetail srcTransactionDetailOut = new SrcTransactionDetail();

                srcTransactionDetailOut.setAmount(getSrcTransactionStatusMessageResponse.getSrcTransactionDetail().getAmount());
                srcTransactionDetailOut.setSrcTransactionStatus(getSrcTransactionStatusMessageResponse.getSrcTransactionDetail().getSrcTransactionStatus().value());

                getSRCTransactionStatusMessageResponseOUT.setSrcTransactionDetail(srcTransactionDetailOut);
            }

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", getSRCTransactionStatusMessageResponseOUT.getStatusCode()));
            outputParameters.add(new Pair<String, String>("messageCode", getSRCTransactionStatusMessageResponseOUT.getMessageCode()));

            if (getSrcTransactionStatusMessageResponse.getSrcTransactionDetail() != null) {

                com.techedge.mp.forecourt.integration.shop.client.SrcTransactionDetail transactionDetail = getSrcTransactionStatusMessageResponse.getSrcTransactionDetail();
                String message = "{ amount: " + transactionDetail.getAmount() + ", transactionStatus: " + transactionDetail.getSrcTransactionStatus().value() + " }";

                outputParameters.add(new Pair<String, String>("srcTransactionDetail", message));
            }
            else {

                outputParameters.add(new Pair<String, String>("srcTransactionDetail", null));
            }

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return getSRCTransactionStatusMessageResponseOUT;

        }
        catch (BPELException e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "BPELException message: " + e.getMessage());

            getSRCTransactionStatusMessageResponseOUT.setStatusCode(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", getSRCTransactionStatusMessageResponseOUT.getStatusCode()));
            outputParameters.add(new Pair<String, String>("messageCode", getSRCTransactionStatusMessageResponseOUT.getMessageCode()));

            if (getSRCTransactionStatusMessageResponseOUT.getSrcTransactionDetail() != null) {

                SrcTransactionDetail transactionDetail = getSRCTransactionStatusMessageResponseOUT.getSrcTransactionDetail();
                String message = "{ amount: " + transactionDetail.getAmount() + ", transactionStatus: " + transactionDetail.getSrcTransactionStatus() + " }";

                outputParameters.add(new Pair<String, String>("srcTransactionDetail", message));
            }
            else {

                outputParameters.add(new Pair<String, String>("srcTransactionDetail", null));
            }

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return getSRCTransactionStatusMessageResponseOUT;

        }
        catch (Exception e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "Exception message: " + e.getMessage());

            getSRCTransactionStatusMessageResponseOUT.setStatusCode(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", getSRCTransactionStatusMessageResponseOUT.getStatusCode()));
            outputParameters.add(new Pair<String, String>("messageCode", getSRCTransactionStatusMessageResponseOUT.getMessageCode()));

            if (getSRCTransactionStatusMessageResponseOUT.getSrcTransactionDetail() != null) {

                SrcTransactionDetail transactionDetail = getSRCTransactionStatusMessageResponseOUT.getSrcTransactionDetail();
                String message = "{ amount: " + transactionDetail.getAmount() + ", transactionStatus: " + transactionDetail.getSrcTransactionStatus() + " }";

                outputParameters.add(new Pair<String, String>("srcTransactionDetail", message));
            }
            else {

                outputParameters.add(new Pair<String, String>("srcTransactionDetail", null));
            }

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return getSRCTransactionStatusMessageResponseOUT;
        }

    }

    @Override
    public GetLastRefuelMessageResponse getLastRefuel(String requestID, String pumpID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("pumpID", pumpID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getLastRefuel", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetLastRefuelMessageResponse getLastRefuelMessageResponseOut = new GetLastRefuelMessageResponse();

        try {

            new Proxy().unsetHttp();

            GfgShopServiceImpl service1 = new GfgShopServiceImpl(this.getUrl());

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getLastRefuel", requestID, null, "service creted from url: " + this.getUrl());

            IGfgShopService port1 = service1.getWsEndpointGatewayMobilePaymentShop();

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getLastRefuel", requestID, null, "port created");

            GetLastRefuelRequest getLastRefuelRequest = new GetLastRefuelRequest();

            getLastRefuelRequest.setPumpId(pumpID);
            getLastRefuelRequest.setRequestId(requestID);

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getLastRefuel", requestID, null, "call to remote service");

            com.techedge.mp.forecourt.integration.shop.client.GetLastRefuelMessageResponse getLastRefuelMessageResponse = port1.getLastRefuel(getLastRefuelRequest);

            getLastRefuelMessageResponseOut.setStatusCode(getLastRefuelMessageResponse.getStatusCode().value());
            getLastRefuelMessageResponseOut.setMessageCode(getLastRefuelMessageResponse.getMessageCode());
            getLastRefuelMessageResponseOut.setSrcTransactionID(getLastRefuelMessageResponse.getSrcTransactionId());
            getLastRefuelMessageResponseOut.setPumpID(getLastRefuelMessageResponse.getPumpId());
            getLastRefuelMessageResponseOut.setPumpNumber(getLastRefuelMessageResponse.getPumpNumber());
            getLastRefuelMessageResponseOut.setRefuelMode(getLastRefuelMessageResponse.getRefuelMode());

            StationDetail stationDetailOut = new StationDetail();
            stationDetailOut.setStationID(getLastRefuelMessageResponse.getStationDetail().getStationId());
            stationDetailOut.setValidityDateDetails(getLastRefuelMessageResponse.getStationDetail().getValidityDateDetails());
            stationDetailOut.setCity(getLastRefuelMessageResponse.getStationDetail().getCity());
            stationDetailOut.setAddress(getLastRefuelMessageResponse.getStationDetail().getAddress());
            stationDetailOut.setCountry(getLastRefuelMessageResponse.getStationDetail().getCountry());
            stationDetailOut.setProvince(getLastRefuelMessageResponse.getStationDetail().getProvince());
            stationDetailOut.setLatitude(getLastRefuelMessageResponse.getStationDetail().getLatitude());
            stationDetailOut.setLongitude(getLastRefuelMessageResponse.getStationDetail().getLongitude());
            getLastRefuelMessageResponseOut.setStationDetail(stationDetailOut);

            RefuelDetails refuelDetailOut = null;
            if (getLastRefuelMessageResponse.getRefuelDetail() != null) {
                refuelDetailOut = new RefuelDetails();
                refuelDetailOut.setTimestampEndRefuel(getLastRefuelMessageResponse.getRefuelDetail().getTimestampEndRefuel());
                refuelDetailOut.setAmount(getLastRefuelMessageResponse.getRefuelDetail().getAmount());
                refuelDetailOut.setFuelType(getLastRefuelMessageResponse.getRefuelDetail().getFuelType());
                refuelDetailOut.setFuelQuantity(getLastRefuelMessageResponse.getRefuelDetail().getFuelQuantity());
                refuelDetailOut.setProductID(getLastRefuelMessageResponse.getRefuelDetail().getProductId().value());
                refuelDetailOut.setProductDescription(getLastRefuelMessageResponse.getRefuelDetail().getProductDescription());
                refuelDetailOut.setUnitPrice(getLastRefuelMessageResponse.getRefuelDetail().getUnitPrice());
            }

            getLastRefuelMessageResponseOut.setRefuelDetail(refuelDetailOut);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", getLastRefuelMessageResponseOut.getStatusCode()));
            outputParameters.add(new Pair<String, String>("statusMessage", getLastRefuelMessageResponseOut.getMessageCode()));
            outputParameters.add(new Pair<String, String>("srcTransactionID", getLastRefuelMessageResponseOut.getSrcTransactionID()));
            outputParameters.add(new Pair<String, String>("pumpID", getLastRefuelMessageResponseOut.getPumpID()));
            outputParameters.add(new Pair<String, String>("refuelMode", getLastRefuelMessageResponseOut.getRefuelMode()));

            if (getLastRefuelMessageResponseOut.getStationDetail() != null) {
                StationDetail stationDetail = getLastRefuelMessageResponseOut.getStationDetail();

                String message = "{ stationID: " + stationDetail.getStationID() + ", address: " + stationDetail.getAddress() + ", city: " + stationDetail.getCity()
                        + ", province: " + stationDetail.getProvince() + " }";

                outputParameters.add(new Pair<String, String>("stationDetail", message));

                for (PumpDetail pumpDetail : stationDetail.getPumpDetails()) {
                    message = "{ pumpID: " + pumpDetail.getPumpID() + ", pumpNumber: " + pumpDetail.getPumpNumber() + ", pumpStatus: " + pumpDetail.getPumpStatus()
                            + ", refuelMode: " + pumpDetail.getRefuelMode() + " }";

                    outputParameters.add(new Pair<String, String>("stationDetail.pumpDetail", message));
                }
            }

            if (getLastRefuelMessageResponseOut.getRefuelDetail() != null) {
                RefuelDetails refuelDetail = getLastRefuelMessageResponseOut.getRefuelDetail();
                String stringUnitPrice = "";
                if (refuelDetail.getUnitPrice() != null) {
                    stringUnitPrice = refuelDetail.getUnitPrice().toString();
                }
                String message = "{ amount: " + refuelDetail.getAmount().toString() + ", fuelQuantity: " + refuelDetail.getFuelQuantity().toString() + ", fuelType: "
                        + refuelDetail.getFuelType() + ", productID: " + refuelDetail.getProductID() + ", productDescription: " + refuelDetail.getProductDescription()
                        + ", timestampEndRefuel: " + refuelDetail.getTimestampEndRefuel() + ", unitPrice: " + stringUnitPrice + " }";

                outputParameters.add(new Pair<String, String>("refuelDetail", message));
            }

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getLastRefuel", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return getLastRefuelMessageResponseOut;

        }
        catch (BPELException e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getLastRefuel", requestID, null, "BPELException message: " + e.getMessage());

            getLastRefuelMessageResponseOut.setStatusCode(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", getLastRefuelMessageResponseOut.getStatusCode()));
            outputParameters.add(new Pair<String, String>("statusMessage", getLastRefuelMessageResponseOut.getMessageCode()));
            outputParameters.add(new Pair<String, String>("srcTransactionID", getLastRefuelMessageResponseOut.getSrcTransactionID()));
            outputParameters.add(new Pair<String, String>("pumpID", getLastRefuelMessageResponseOut.getPumpID()));
            outputParameters.add(new Pair<String, String>("refuelMode", getLastRefuelMessageResponseOut.getRefuelMode()));

            if (getLastRefuelMessageResponseOut.getStationDetail() != null) {
                StationDetail stationDetail = getLastRefuelMessageResponseOut.getStationDetail();

                String message = "{ stationID: " + stationDetail.getStationID() + ", address: " + stationDetail.getAddress() + ", city: " + stationDetail.getCity()
                        + ", province: " + stationDetail.getProvince() + " }";

                outputParameters.add(new Pair<String, String>("stationDetail", message));

                for (PumpDetail pumpDetail : stationDetail.getPumpDetails()) {
                    message = "{ pumpID: " + pumpDetail.getPumpID() + ", pumpNumber: " + pumpDetail.getPumpNumber() + ", pumpStatus: " + pumpDetail.getPumpStatus()
                            + ", refuelMode: " + pumpDetail.getRefuelMode() + " }";

                    outputParameters.add(new Pair<String, String>("stationDetail.pumpDetail", message));
                }
            }

            if (getLastRefuelMessageResponseOut.getRefuelDetail() != null) {
                RefuelDetails refuelDetail = getLastRefuelMessageResponseOut.getRefuelDetail();
                String message = "{ amount: " + refuelDetail.getAmount().toString() + ", fuelQuantity: " + refuelDetail.getFuelQuantity().toString() + ", fuelType: "
                        + refuelDetail.getFuelType() + ", productID: " + refuelDetail.getProductID() + ", productDescription: " + refuelDetail.getProductDescription()
                        + ", timestampEndRefuel: " + refuelDetail.getTimestampEndRefuel() + " }";

                outputParameters.add(new Pair<String, String>("refuelDetail", message));
            }

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getLastRefuel", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return getLastRefuelMessageResponseOut;
        }
        catch (Exception e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getLastRefuel", requestID, null, "Exception message: " + e.getMessage());

            getLastRefuelMessageResponseOut.setStatusCode(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", getLastRefuelMessageResponseOut.getStatusCode()));
            outputParameters.add(new Pair<String, String>("statusMessage", getLastRefuelMessageResponseOut.getMessageCode()));
            outputParameters.add(new Pair<String, String>("srcTransactionID", getLastRefuelMessageResponseOut.getSrcTransactionID()));
            outputParameters.add(new Pair<String, String>("pumpID", getLastRefuelMessageResponseOut.getPumpID()));
            outputParameters.add(new Pair<String, String>("refuelMode", getLastRefuelMessageResponseOut.getRefuelMode()));

            if (getLastRefuelMessageResponseOut.getStationDetail() != null) {
                StationDetail stationDetail = getLastRefuelMessageResponseOut.getStationDetail();

                String message = "{ stationID: " + stationDetail.getStationID() + ", address: " + stationDetail.getAddress() + ", city: " + stationDetail.getCity()
                        + ", province: " + stationDetail.getProvince() + " }";

                outputParameters.add(new Pair<String, String>("stationDetail", message));

                for (PumpDetail pumpDetail : stationDetail.getPumpDetails()) {
                    message = "{ pumpID: " + pumpDetail.getPumpID() + ", pumpNumber: " + pumpDetail.getPumpNumber() + ", pumpStatus: " + pumpDetail.getPumpStatus()
                            + ", refuelMode: " + pumpDetail.getRefuelMode() + " }";

                    outputParameters.add(new Pair<String, String>("stationDetail.pumpDetail", message));
                }
            }

            if (getLastRefuelMessageResponseOut.getRefuelDetail() != null) {
                RefuelDetails refuelDetail = getLastRefuelMessageResponseOut.getRefuelDetail();
                String message = "{ amount: " + refuelDetail.getAmount().toString() + ", fuelQuantity: " + refuelDetail.getFuelQuantity().toString() + ", fuelType: "
                        + refuelDetail.getFuelType() + ", productID: " + refuelDetail.getProductID() + ", productDescription: " + refuelDetail.getProductDescription()
                        + ", timestampEndRefuel: " + refuelDetail.getTimestampEndRefuel() + " }";

                outputParameters.add(new Pair<String, String>("refuelDetail", message));
            }

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getLastRefuel", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return getLastRefuelMessageResponseOut;
        }
    }
}
