
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per TransactionStatusEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="TransactionStatusEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="PAID"/>
 *     &lt;enumeration value="UNPAID"/>
 *     &lt;enumeration value="CANCELLED"/>
 *     &lt;enumeration value="REVERSED"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "TransactionStatusEnum")
@XmlEnum
public enum TransactionStatusEnum {

    PAID,
    UNPAID,
    CANCELLED,
    REVERSED;

    public String value() {
        return name();
    }

    public static TransactionStatusEnum fromValue(String v) {
        return valueOf(v);
    }

}
