
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per GetSrcTransactionStatusStatusCode.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="GetSrcTransactionStatusStatusCode">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MESSAGE_RECEIVED_200"/>
 *     &lt;enumeration value="MESSAGE_REJECTED_500"/>
 *     &lt;enumeration value="TRANSACTION_NOT_RECOGNIZED_400"/>
 *     &lt;enumeration value="SOURCE_STATUS_NOT_AVAILABLE_500"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "GetSrcTransactionStatusStatusCode")
@XmlEnum
public enum GetSrcTransactionStatusStatusCode {

    MESSAGE_RECEIVED_200,
    MESSAGE_REJECTED_500,
    TRANSACTION_NOT_RECOGNIZED_400,
    SOURCE_STATUS_NOT_AVAILABLE_500;

    public String value() {
        return name();
    }

    public static GetSrcTransactionStatusStatusCode fromValue(String v) {
        return valueOf(v);
    }

}
