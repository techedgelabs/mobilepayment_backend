
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.techedge.mp.forecourt.integration.shop.client package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.techedge.mp.forecourt.integration.shop.client
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendMpTransactionResult }
     * 
     */
    public SendMpTransactionResult createSendMpTransactionResult() {
        return new SendMpTransactionResult();
    }

    /**
     * Create an instance of {@link SendMpTransactionResultRequest }
     * 
     */
    public SendMpTransactionResultRequest createSendMpTransactionResultRequest() {
        return new SendMpTransactionResultRequest();
    }

    /**
     * Create an instance of {@link SendMpTransactionResultResponse }
     * 
     */
    public SendMpTransactionResultResponse createSendMpTransactionResultResponse() {
        return new SendMpTransactionResultResponse();
    }

    /**
     * Create an instance of {@link SendMpTransactionResultMessageResponse }
     * 
     */
    public SendMpTransactionResultMessageResponse createSendMpTransactionResultMessageResponse() {
        return new SendMpTransactionResultMessageResponse();
    }

    /**
     * Create an instance of {@link GetLastRefuelResponse }
     * 
     */
    public GetLastRefuelResponse createGetLastRefuelResponse() {
        return new GetLastRefuelResponse();
    }

    /**
     * Create an instance of {@link GetLastRefuelMessageResponse }
     * 
     */
    public GetLastRefuelMessageResponse createGetLastRefuelMessageResponse() {
        return new GetLastRefuelMessageResponse();
    }

    /**
     * Create an instance of {@link GetLastRefuel }
     * 
     */
    public GetLastRefuel createGetLastRefuel() {
        return new GetLastRefuel();
    }

    /**
     * Create an instance of {@link GetLastRefuelRequest }
     * 
     */
    public GetLastRefuelRequest createGetLastRefuelRequest() {
        return new GetLastRefuelRequest();
    }

    /**
     * Create an instance of {@link GetSrcTransactionStatus }
     * 
     */
    public GetSrcTransactionStatus createGetSrcTransactionStatus() {
        return new GetSrcTransactionStatus();
    }

    /**
     * Create an instance of {@link GetSrcTransactionStatusRequest }
     * 
     */
    public GetSrcTransactionStatusRequest createGetSrcTransactionStatusRequest() {
        return new GetSrcTransactionStatusRequest();
    }

    /**
     * Create an instance of {@link GetSrcTransactionStatusResponse }
     * 
     */
    public GetSrcTransactionStatusResponse createGetSrcTransactionStatusResponse() {
        return new GetSrcTransactionStatusResponse();
    }

    /**
     * Create an instance of {@link GetSrcTransactionStatusMessageResponse }
     * 
     */
    public GetSrcTransactionStatusMessageResponse createGetSrcTransactionStatusMessageResponse() {
        return new GetSrcTransactionStatusMessageResponse();
    }

    /**
     * Create an instance of {@link PumpDetail }
     * 
     */
    public PumpDetail createPumpDetail() {
        return new PumpDetail();
    }

    /**
     * Create an instance of {@link ProductDetail }
     * 
     */
    public ProductDetail createProductDetail() {
        return new ProductDetail();
    }

    /**
     * Create an instance of {@link StationDetail }
     * 
     */
    public StationDetail createStationDetail() {
        return new StationDetail();
    }

    /**
     * Create an instance of {@link Voucher }
     * 
     */
    public Voucher createVoucher() {
        return new Voucher();
    }

    /**
     * Create an instance of {@link PaymentTransactionResult }
     * 
     */
    public PaymentTransactionResult createPaymentTransactionResult() {
        return new PaymentTransactionResult();
    }

    /**
     * Create an instance of {@link RefuelDetail }
     * 
     */
    public RefuelDetail createRefuelDetail() {
        return new RefuelDetail();
    }

    /**
     * Create an instance of {@link SrcTransactionDetail }
     * 
     */
    public SrcTransactionDetail createSrcTransactionDetail() {
        return new SrcTransactionDetail();
    }

}
