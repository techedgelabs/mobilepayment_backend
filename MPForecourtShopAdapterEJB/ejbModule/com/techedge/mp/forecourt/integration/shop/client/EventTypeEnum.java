
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per EventTypeEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="EventTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AUT"/>
 *     &lt;enumeration value="MOV"/>
 *     &lt;enumeration value="STO"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "EventTypeEnum")
@XmlEnum
public enum EventTypeEnum {

    AUT,
    MOV,
    STO;

    public String value() {
        return name();
    }

    public static EventTypeEnum fromValue(String v) {
        return valueOf(v);
    }

}
