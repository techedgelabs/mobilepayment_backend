
package com.techedge.mp.forecourt.integration.shop.client;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

/**
 * <p>Classe Java per SendMpTransactionResultRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="SendMpTransactionResultRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="StationId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SrcTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="MpTransactionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MpTransactionStatus" type="{http://gatewaymobilepayment.4ts.it/}TransactionStatusEnum"/>
 *         &lt;element name="PaymentTransactionResult" type="{http://gatewaymobilepayment.4ts.it/}PaymentTransactionResult" minOccurs="0"/>
 *         &lt;element name="LoyaltyCredits" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="Voucher" type="{http://gatewaymobilepayment.4ts.it/}Voucher" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendMpTransactionResultRequest", propOrder = {
    "requestId",
    "stationId",
    "srcTransactionId",
    "mpTransactionId",
    "mpTransactionStatus",
    "paymentTransactionResult",
    "loyaltyCredits",
    "voucher",
    "electronicInvoice"
})
public class SendMpTransactionResultRequest {

    @XmlElement(name = "RequestId", required = true, nillable = true)
    protected String requestId;
    @XmlElement(name = "StationId")
    protected String stationId;
    @XmlElement(name = "SrcTransactionId")
    protected String srcTransactionId;
    @XmlElement(name = "MpTransactionId", required = true, nillable = true)
    protected String mpTransactionId;
    @XmlElement(name = "MpTransactionStatus", required = true)
    protected TransactionStatusEnum mpTransactionStatus;
    @XmlElement(name = "PaymentTransactionResult")
    protected PaymentTransactionResult paymentTransactionResult;
    @XmlElement(name = "LoyaltyCredits")
    protected Boolean loyaltyCredits;
    @XmlElement(name = "Voucher")
    protected List<Voucher> voucher;
    @XmlElement(name = "ElectronicInvoice")
    protected ElectronicInvoice electronicInvoice;

    /**
     * Recupera il valore della proprietÓ requestId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Imposta il valore della proprietÓ requestId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }
    
    /**
     * Recupera il valore della proprietÓ stationId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStationId() {
        return stationId;
    }

    /**
     * Imposta il valore della proprietÓ stationId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStationId(String value) {
        this.stationId = value;
    }

    /**
     * Recupera il valore della proprietÓ srcTransactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrcTransactionId() {
        return srcTransactionId;
    }

    /**
     * Imposta il valore della proprietÓ srcTransactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrcTransactionId(String value) {
        this.srcTransactionId = value;
    }

    /**
     * Recupera il valore della proprietÓ mpTransactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMpTransactionId() {
        return mpTransactionId;
    }

    /**
     * Imposta il valore della proprietÓ mpTransactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMpTransactionId(String value) {
        this.mpTransactionId = value;
    }

    /**
     * Recupera il valore della proprietÓ mpTransactionStatus.
     * 
     * @return
     *     possible object is
     *     {@link TransactionStatusEnum }
     *     
     */
    public TransactionStatusEnum getMpTransactionStatus() {
        return mpTransactionStatus;
    }

    /**
     * Imposta il valore della proprietÓ mpTransactionStatus.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionStatusEnum }
     *     
     */
    public void setMpTransactionStatus(TransactionStatusEnum value) {
        this.mpTransactionStatus = value;
    }

    /**
     * Recupera il valore della proprietÓ paymentTransactionResult.
     * 
     * @return
     *     possible object is
     *     {@link PaymentTransactionResult }
     *     
     */
    public PaymentTransactionResult getPaymentTransactionResult() {
        return paymentTransactionResult;
    }

    /**
     * Imposta il valore della proprietÓ paymentTransactionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentTransactionResult }
     *     
     */
    public void setPaymentTransactionResult(PaymentTransactionResult value) {
        this.paymentTransactionResult = value;
    }

    /**
     * Recupera il valore della proprietÓ loyaltyCredits.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLoyaltyCredits() {
        return loyaltyCredits;
    }

    /**
     * Imposta il valore della proprietÓ loyaltyCredits.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLoyaltyCredits(Boolean value) {
        this.loyaltyCredits = value;
    }

    /**
     * Gets the value of the voucher property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucher property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucher().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Voucher }
     * 
     * 
     */
    public List<Voucher> getVoucher() {
        if (voucher == null) {
            voucher = new ArrayList<Voucher>();
        }
        return this.voucher;
    }

    public void setElectronicInvoice(ElectronicInvoice electronicInvoice) {
        this.electronicInvoice = electronicInvoice;
    }
    
    public ElectronicInvoice getElectronicInvoice() {
        return electronicInvoice;
    }
    
}
