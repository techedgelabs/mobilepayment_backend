
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="request" type="{http://gatewaymobilepayment.4ts.it/}GetSrcTransactionStatusRequest" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "request"
})
@XmlRootElement(name = "GetSrcTransactionStatus")
public class GetSrcTransactionStatus {

    protected GetSrcTransactionStatusRequest request;

    /**
     * Recupera il valore della proprietÓ request.
     * 
     * @return
     *     possible object is
     *     {@link GetSrcTransactionStatusRequest }
     *     
     */
    public GetSrcTransactionStatusRequest getRequest() {
        return request;
    }

    /**
     * Imposta il valore della proprietÓ request.
     * 
     * @param value
     *     allowed object is
     *     {@link GetSrcTransactionStatusRequest }
     *     
     */
    public void setRequest(GetSrcTransactionStatusRequest value) {
        this.request = value;
    }

}
