package com.techedge.mp.forecourt.integration.shop.client.clientsample;

import com.techedge.mp.forecourt.integration.shop.client.*;

public class ClientSample {

	public static void main(String[] args) {
	        System.out.println("***********************");
	        System.out.println("Create Web Service Client...");
	        GfgShopServiceImpl service1 = new GfgShopServiceImpl();
	        System.out.println("Create Web Service...");
	        IGfgShopService port1 = service1.getWsEndpointGatewayMobilePaymentShop();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port1.getSrcTransactionStatus(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.sendMpTransactionResult(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port1.getLastRefuel(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Create Web Service...");
	        IGfgShopService port2 = service1.getWsEndpointGatewayMobilePaymentShop();
	        System.out.println("Call Web Service Operation...");
	        System.out.println("Server said: " + port2.getSrcTransactionStatus(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.sendMpTransactionResult(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("Server said: " + port2.getLastRefuel(null));
	        //Please input the parameters instead of 'null' for the upper method!
	
	        System.out.println("***********************");
	        System.out.println("Call Over!");
	}
}
