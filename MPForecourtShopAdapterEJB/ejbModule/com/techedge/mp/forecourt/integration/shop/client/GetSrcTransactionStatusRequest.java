
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per GetSrcTransactionStatusRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="GetSrcTransactionStatusRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RequestId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MpTransactionId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SrcTransactionId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GetSrcTransactionStatusRequest", propOrder = {
    "requestId",
    "mpTransactionId",
    "srcTransactionId"
})
public class GetSrcTransactionStatusRequest {

    @XmlElement(name = "RequestId", required = true, nillable = true)
    protected String requestId;
    @XmlElement(name = "MpTransactionId")
    protected String mpTransactionId;
    @XmlElement(name = "SrcTransactionId", required = true, nillable = true)
    protected String srcTransactionId;

    /**
     * Recupera il valore della proprietÓ requestId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestId() {
        return requestId;
    }

    /**
     * Imposta il valore della proprietÓ requestId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestId(String value) {
        this.requestId = value;
    }

    /**
     * Recupera il valore della proprietÓ mpTransactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMpTransactionId() {
        return mpTransactionId;
    }

    /**
     * Imposta il valore della proprietÓ mpTransactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMpTransactionId(String value) {
        this.mpTransactionId = value;
    }

    /**
     * Recupera il valore della proprietÓ srcTransactionId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSrcTransactionId() {
        return srcTransactionId;
    }

    /**
     * Imposta il valore della proprietÓ srcTransactionId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSrcTransactionId(String value) {
        this.srcTransactionId = value;
    }

}
