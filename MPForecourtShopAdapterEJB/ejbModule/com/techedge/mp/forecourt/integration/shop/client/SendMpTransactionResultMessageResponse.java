
package com.techedge.mp.forecourt.integration.shop.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per SendMpTransactionResultMessageResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="SendMpTransactionResultMessageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="StatusCode" type="{http://gatewaymobilepayment.4ts.it/}SendMpTransactionResultStatusCode"/>
 *         &lt;element name="MessageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SendMpTransactionResultMessageResponse", propOrder = {
    "statusCode",
    "messageCode",
    "electronicInvoiceID"
})
public class SendMpTransactionResultMessageResponse {

    @XmlElement(name = "StatusCode", required = true)
    protected SendMpTransactionResultStatusCode statusCode;
    @XmlElement(name = "MessageCode", required = true, nillable = true)
    protected String messageCode;
    @XmlElement(name = "ElectronicInvoiceID")
    protected String electronicInvoiceID;

    /**
     * Recupera il valore della proprietÓ statusCode.
     * 
     * @return
     *     possible object is
     *     {@link SendMpTransactionResultStatusCode }
     *     
     */
    public SendMpTransactionResultStatusCode getStatusCode() {
        return statusCode;
    }

    /**
     * Imposta il valore della proprietÓ statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SendMpTransactionResultStatusCode }
     *     
     */
    public void setStatusCode(SendMpTransactionResultStatusCode value) {
        this.statusCode = value;
    }

    /**
     * Recupera il valore della proprietÓ messageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Imposta il valore della proprietÓ messageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    public String getElectronicInvoiceID() {
        return electronicInvoiceID;
    }
    
    public void setElectronicInvoiceID(String electronicInvoiceID) {
        this.electronicInvoiceID = electronicInvoiceID;
    }
}
