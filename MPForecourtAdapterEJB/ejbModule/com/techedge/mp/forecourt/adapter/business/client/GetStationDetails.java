
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getStationDetailsMessageRequest" type="{http://gatewaymobilepayment.4ts.it/}getStationDetailsMessageRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getStationDetailsMessageRequest"
})
@XmlRootElement(name = "getStationDetails")
public class GetStationDetails {

    @XmlElement(required = true, nillable = true)
    protected GetStationDetailsMessageRequest getStationDetailsMessageRequest;

    /**
     * Recupera il valore della proprietÓ getStationDetailsMessageRequest.
     * 
     * @return
     *     possible object is
     *     {@link GetStationDetailsMessageRequest }
     *     
     */
    public GetStationDetailsMessageRequest getGetStationDetailsMessageRequest() {
        return getStationDetailsMessageRequest;
    }

    /**
     * Imposta il valore della proprietÓ getStationDetailsMessageRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link GetStationDetailsMessageRequest }
     *     
     */
    public void setGetStationDetailsMessageRequest(GetStationDetailsMessageRequest value) {
        this.getStationDetailsMessageRequest = value;
    }

}
