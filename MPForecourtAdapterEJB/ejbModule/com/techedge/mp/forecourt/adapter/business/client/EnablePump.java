
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="enablePumpRequest" type="{http://gatewaymobilepayment.4ts.it/}enablePumpMessageRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "enablePumpRequest"
})
@XmlRootElement(name = "enablePump")
public class EnablePump {

    @XmlElement(required = true, nillable = true)
    protected EnablePumpMessageRequest enablePumpRequest;

    /**
     * Recupera il valore della proprietÓ enablePumpRequest.
     * 
     * @return
     *     possible object is
     *     {@link EnablePumpMessageRequest }
     *     
     */
    public EnablePumpMessageRequest getEnablePumpRequest() {
        return enablePumpRequest;
    }

    /**
     * Imposta il valore della proprietÓ enablePumpRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link EnablePumpMessageRequest }
     *     
     */
    public void setEnablePumpRequest(EnablePumpMessageRequest value) {
        this.enablePumpRequest = value;
    }

}
