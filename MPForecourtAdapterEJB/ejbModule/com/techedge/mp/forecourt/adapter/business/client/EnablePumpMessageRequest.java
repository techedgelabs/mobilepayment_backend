
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per enablePumpMessageRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="enablePumpMessageRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pumpID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="paymentMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="productID" type="{http://gatewaymobilepayment.4ts.it/}productIdEnum" minOccurs="0"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="paymentAuthorizationResult" type="{http://gatewaymobilepayment.4ts.it/}paymentAuthorizationResult"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enablePumpMessageRequest", propOrder = {
    "requestID",
    "transactionID",
    "stationID",
    "pumpID",
    "paymentMode",
    "productID",
    "amount",
    "paymentAuthorizationResult"
})
public class EnablePumpMessageRequest {

    @XmlElement(required = true, nillable = true)
    protected String requestID;
    @XmlElement(required = true, nillable = true)
    protected String transactionID;
    @XmlElement(required = true, nillable = true)
    protected String stationID;
    @XmlElement(required = true, nillable = true)
    protected String pumpID;
    @XmlElement(required = true, nillable = true)
    protected String paymentMode;
    protected ProductIdEnum productID;
    protected double amount;
    @XmlElement(required = true, nillable = true)
    protected PaymentAuthorizationResult paymentAuthorizationResult;

    /**
     * Recupera il valore della proprietÓ requestID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Imposta il valore della proprietÓ requestID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Recupera il valore della proprietÓ transactionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Imposta il valore della proprietÓ transactionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Recupera il valore della proprietÓ stationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStationID() {
        return stationID;
    }

    /**
     * Imposta il valore della proprietÓ stationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStationID(String value) {
        this.stationID = value;
    }

    /**
     * Recupera il valore della proprietÓ pumpID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpID() {
        return pumpID;
    }

    /**
     * Imposta il valore della proprietÓ pumpID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpID(String value) {
        this.pumpID = value;
    }

    /**
     * Recupera il valore della proprietÓ paymentMode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMode() {
        return paymentMode;
    }

    /**
     * Imposta il valore della proprietÓ paymentMode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMode(String value) {
        this.paymentMode = value;
    }

    /**
     * Recupera il valore della proprietÓ productID.
     * 
     * @return
     *     possible object is
     *     {@link ProductIdEnum }
     *     
     */
    public ProductIdEnum getProductID() {
        return productID;
    }

    /**
     * Imposta il valore della proprietÓ productID.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductIdEnum }
     *     
     */
    public void setProductID(ProductIdEnum value) {
        this.productID = value;
    }

    /**
     * Recupera il valore della proprietÓ amount.
     * 
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Imposta il valore della proprietÓ amount.
     * 
     */
    public void setAmount(double value) {
        this.amount = value;
    }

    /**
     * Recupera il valore della proprietÓ paymentAuthorizationResult.
     * 
     * @return
     *     possible object is
     *     {@link PaymentAuthorizationResult }
     *     
     */
    public PaymentAuthorizationResult getPaymentAuthorizationResult() {
        return paymentAuthorizationResult;
    }

    /**
     * Imposta il valore della proprietÓ paymentAuthorizationResult.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentAuthorizationResult }
     *     
     */
    public void setPaymentAuthorizationResult(PaymentAuthorizationResult value) {
        this.paymentAuthorizationResult = value;
    }

}
