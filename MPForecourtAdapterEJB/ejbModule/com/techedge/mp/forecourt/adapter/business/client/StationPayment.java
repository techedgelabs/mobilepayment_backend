
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "stationPayment", propOrder = {
    "stationId",
    "payment"
})
public class StationPayment {

    @XmlElement(required = true)
    protected String stationId;
    @XmlElement(required = true)
    protected boolean payment;

    /**
     * Recupera il valore della proprietà stationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStationId() {
        return stationId;
    }

    /**
     * Imposta il valore della proprietà stationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStationId(String value) {
        this.stationId = value;
    }

    /**
     * Recupera il valore della proprietà payment.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public boolean getPayment() {
        return payment;
    }

    /**
     * Imposta il valore della proprietà payment.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setPayment(boolean value) {
        this.payment = value;
    }
}
