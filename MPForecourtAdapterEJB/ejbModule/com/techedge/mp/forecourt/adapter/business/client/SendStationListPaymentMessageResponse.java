
package com.techedge.mp.forecourt.adapter.business.client;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per sendStationListPaymentMessageResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="sendStationListPaymentMessageResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="statusCode" type="{http://gatewaymobilepayment.4ts.it/}sendStationListPaymentMessageResponseEnum"/&gt;
 *         &lt;element name="messageCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="stationList" type="{http://gatewaymobilepayment.4ts.it/}stationNotFound" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendStationListPaymentMessageResponse", propOrder = {
    "statusCode",
    "messageCode",
    "stationList"
})
public class SendStationListPaymentMessageResponse {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected SendStationListPaymentMessageResponseEnum statusCode;
    @XmlElement(required = true, nillable = true)
    protected String messageCode;
    protected List<StationNotFound> stationList;

    /**
     * Recupera il valore della proprietà statusCode.
     * 
     * @return
     *     possible object is
     *     {@link SendStationListPaymentMessageResponseEnum }
     *     
     */
    public SendStationListPaymentMessageResponseEnum getStatusCode() {
        return statusCode;
    }

    /**
     * Imposta il valore della proprietà statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SendStationListPaymentMessageResponseEnum }
     *     
     */
    public void setStatusCode(SendStationListPaymentMessageResponseEnum value) {
        this.statusCode = value;
    }

    /**
     * Recupera il valore della proprietà messageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Imposta il valore della proprietà messageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    /**
     * Gets the value of the stationList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stationList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStationList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StationNotFound }
     * 
     * 
     */
    public List<StationNotFound> getStationList() {
        if (stationList == null) {
            stationList = new ArrayList<StationNotFound>();
        }
        return this.stationList;
    }

}
