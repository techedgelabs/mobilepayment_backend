
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionReconciliationRequest" type="{http://gatewaymobilepayment.4ts.it/}transactionReconciliationMessageRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionReconciliationRequest"
})
@XmlRootElement(name = "transactionReconciliation")
public class TransactionReconciliation {

    @XmlElement(required = true, nillable = true)
    protected TransactionReconciliationMessageRequest transactionReconciliationRequest;

    /**
     * Recupera il valore della proprietÓ transactionReconciliationRequest.
     * 
     * @return
     *     possible object is
     *     {@link TransactionReconciliationMessageRequest }
     *     
     */
    public TransactionReconciliationMessageRequest getTransactionReconciliationRequest() {
        return transactionReconciliationRequest;
    }

    /**
     * Imposta il valore della proprietÓ transactionReconciliationRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionReconciliationMessageRequest }
     *     
     */
    public void setTransactionReconciliationRequest(TransactionReconciliationMessageRequest value) {
        this.transactionReconciliationRequest = value;
    }

}
