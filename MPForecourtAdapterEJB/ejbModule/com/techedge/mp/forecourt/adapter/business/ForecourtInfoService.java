package com.techedge.mp.forecourt.adapter.business;

import java.net.MalformedURLException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PlateNumber;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.PersonalDataBusiness;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.forecourt.adapter.business.client.ArrayOfString;
import com.techedge.mp.forecourt.adapter.business.client.EnablePumpMessageRequest;
import com.techedge.mp.forecourt.adapter.business.client.EnablePumpMessageResponse;
import com.techedge.mp.forecourt.adapter.business.client.EventTypeEnum;
import com.techedge.mp.forecourt.adapter.business.client.GatewayMobilePaymentImpl;
import com.techedge.mp.forecourt.adapter.business.client.GetStationDetailsMessageRequest;
import com.techedge.mp.forecourt.adapter.business.client.GetTransactionStatusResponseEnum;
import com.techedge.mp.forecourt.adapter.business.client.IGatewayMobilePayment;
import com.techedge.mp.forecourt.adapter.business.client.ProductDetail;
import com.techedge.mp.forecourt.adapter.business.client.ProductIdEnum;
import com.techedge.mp.forecourt.adapter.business.client.PumpDetail;
import com.techedge.mp.forecourt.adapter.business.client.SendStationListPaymentMessageRequest;
import com.techedge.mp.forecourt.adapter.business.client.SourceDetail;
import com.techedge.mp.forecourt.adapter.business.client.StationNotFound;
import com.techedge.mp.forecourt.adapter.business.client.TransactionDetails;
import com.techedge.mp.forecourt.adapter.business.client.TransactionResultEnum;
import com.techedge.mp.forecourt.adapter.business.client.TransactionStatusHistory;
import com.techedge.mp.forecourt.adapter.business.client.TransactionStatusMessageRequest;
import com.techedge.mp.forecourt.adapter.business.client.TransactionStatusMessageResponse;
import com.techedge.mp.forecourt.adapter.business.client.Voucher;
import com.techedge.mp.forecourt.adapter.business.exception.BPELException;
import com.techedge.mp.forecourt.adapter.business.interfaces.EnablePumpResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetPumpStatusResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetTransactionStatusResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.PaymentAuthorizationResult;
import com.techedge.mp.forecourt.adapter.business.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.adapter.business.interfaces.RefuelDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.ResponseHelper;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendPaymentTransactionResultResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendStationListPaymentResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.StationDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.StationPayment;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionReconciliationResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionStatus;

/**
 * Session Bean implementation class ForecourtInfoService
 */
@Stateless
@LocalBean
public class ForecourtInfoService implements ForecourtInfoServiceRemote, ForecourtInfoServiceLocal {

    private final static String      PARAM_FORECOURT_WSDL                                = "FORECOURT_WSDL";
    private final static String      REFUEL_MODE_SELF                                    = "Self";
    private final static String      PARAM_SIMULATION_ACTIVE                             = "SIMULATION_ACTIVE";
    private final static String      PARAM_SIMULATION_SENDPAYMENTTRANSACTIONRESULT_ERROR = "SIMULATION_GFG_SENDPAYMENTTRANSACTIONRESULT_ERROR";
    private final static String      PARAM_SIMULATION_SENDPAYMENTTRANSACTIONRESULT_KO    = "SIMULATION_GFG_SENDPAYMENTTRANSACTIONRESULT_KO";

    private TransactionServiceRemote transactionService                                  = null;
    private ParametersServiceRemote  parametersService                                   = null;
    private LoggerServiceRemote      loggerService                                       = null;

    private URL                      url;
    private boolean                  simulationActive                                    = false;

    public ForecourtInfoService() throws BPELException {

    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }
    
    private static class TrustAllCertificates implements X509TrustManager {
        public void checkClientTrusted(X509Certificate[] certs, String authType) {
        }
     
        public void checkServerTrusted(X509Certificate[] certs, String authType) {
        }
     
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }
    
    private HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, javax.net.ssl.SSLSession session) {
            return true;
        }
    };

    private URL getUrl() throws BPELException {

        if (this.url == null) {
            
            TrustManager[] tm = null;
            tm = new TrustManager[] {new TrustAllCertificates()};
            
            SSLContext sslContext;
            try {
                sslContext = SSLContext.getInstance("TLSv1.2");
                sslContext.init(null, tm, new SecureRandom());
                HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
                HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
            }
            catch (NoSuchAlgorithmException e1) {
                
                e1.printStackTrace();
                
                throw new BPELException("NoSuchAlgorithmException" + e1.getMessage());
            }
            catch (KeyManagementException e) {
                
                e.printStackTrace();
                
                throw new BPELException("KeyManagementException" + e.getMessage());
            }
            

            String wsdlString = "";
            try {
                this.parametersService = EJBHomeCache.getInstance().getParametersService();
                wsdlString = parametersService.getParamValue(ForecourtInfoService.PARAM_FORECOURT_WSDL);
                this.simulationActive = Boolean.parseBoolean(parametersService.getParamValue(ForecourtInfoService.PARAM_SIMULATION_ACTIVE));
            }
            catch (ParameterNotFoundException e) {

                e.printStackTrace();

                throw new BPELException("Parameter " + ForecourtInfoService.PARAM_FORECOURT_WSDL + " not found: " + e.getMessage());
            }
            catch (InterfaceNotFoundException e) {

                e.printStackTrace();

                throw new BPELException("InterfaceNotFoundException for jndi " + e.getJndiString());
            }

            try {
                this.url = new URL(wsdlString);
            }
            catch (MalformedURLException e) {

                e.printStackTrace();

                throw new BPELException("Malformed URL: " + e.getMessage());
            }
        }

        if (simulationActive) {
            System.out.println("Simulazione GFG attiva");
        }

        return this.url;
    }

    @Override
    public GetPumpStatusResponse getPumpStatus(String requestID, String stationID, String pumpID, String transactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPumpStatus", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetPumpStatusResponse getPumpStatusResponse = new GetPumpStatusResponse();

        Boolean errorFound = false;

        if (stationID == null || stationID.trim().isEmpty()) {

            getPumpStatusResponse.setStatusCode(ResponseHelper.GET_PUMP_STATUS_PUMP_NOT_FOUND);
            getPumpStatusResponse.setMessageCode("");
            errorFound = true;
        }

        if (pumpID == null || pumpID.trim().isEmpty()) {

            getPumpStatusResponse.setStatusCode(ResponseHelper.GET_PUMP_STATUS_PUMP_NOT_FOUND);
            getPumpStatusResponse.setMessageCode("");
            errorFound = true;
        }

        if (!errorFound) {

            try {

                // Log timestamp chiamata
                Date currentTimestamp = new Date();
                String currentTimestampString = String.valueOf(currentTimestamp.getTime());

                Set<Pair<String, String>> remoteServiceInputParameters = new HashSet<Pair<String, String>>();
                remoteServiceInputParameters.add(new Pair<String, String>("timestamp", currentTimestampString));

                this.log(ErrorLevel.DEBUG, "WsEndpointGatewayMobilePayment", "getPumpStatus", requestID, "opening", ActivityLog.createLogMessage(remoteServiceInputParameters));

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                new Proxy().unsetHttp();

                GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());

                //Originale Forecourt
                IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
                //Chiamata Forecourt Emulator
                //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();

                com.techedge.mp.forecourt.adapter.business.client.GetPumpStatusMessageRequest requestForecourt = new com.techedge.mp.forecourt.adapter.business.client.GetPumpStatusMessageRequest();

                requestForecourt.setPumpID(pumpID);
                requestForecourt.setRequestID(requestID);
                requestForecourt.setStationID(stationID);

                com.techedge.mp.forecourt.adapter.business.client.GetPumpStatusMessageResponse forecourtResponse = port1.getPumpStatus(requestForecourt);

                // Log timestamp risposta
                currentTimestamp = new Date();
                currentTimestampString = String.valueOf(currentTimestamp.getTime());

                Set<Pair<String, String>> remoteServiceOutputParameters = new HashSet<Pair<String, String>>();
                remoteServiceOutputParameters.add(new Pair<String, String>("timestamp", currentTimestampString));

                this.log(ErrorLevel.DEBUG, "WsEndpointGatewayMobilePayment", "getPumpStatus", requestID, "closing", ActivityLog.createLogMessage(remoteServiceOutputParameters));

                String refuelMode = forecourtResponse.getRefuelMode();

                if (!refuelMode.equals(ForecourtInfoService.REFUEL_MODE_SELF)) {

                    // Erogatore non Self

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "refuelMode not Self: " + refuelMode);

                    getPumpStatusResponse.setStatusCode(ResponseHelper.GET_PUMP_STATUS_PUMP_NOT_AVAILABLE_501);

                    getPumpStatusResponse.setMessageCode("");

                    if (transactionID != null) {

                        String response = transactionService.persistPumpAvailabilityStatus(getPumpStatusResponse.getStatusCode(), getPumpStatusResponse.getMessageCode(),
                                transactionID, requestID);

                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "persistPumpAvailabilityStatus: " + response);
                    }
                    else {

                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "transactionID null");
                    }
                }
                else {

                    getPumpStatusResponse.setStatusCode(forecourtResponse.getStatusCode().toString());
                    getPumpStatusResponse.setMessageCode("");
                    getPumpStatusResponse.setRefuelMode(refuelMode);

                    if (transactionID != null) {

                        String response = transactionService.persistPumpAvailabilityStatus(getPumpStatusResponse.getStatusCode(), getPumpStatusResponse.getMessageCode(),
                                transactionID, requestID);

                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "persistPumpAvailabilityStatus: " + response);
                    }
                    else {

                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "transactionID null");
                    }
                }

            }
            catch (Exception ex) {

                ex.printStackTrace();

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "Exception message: " + ex.getMessage());

                String statusCode = "SYSTEM_ERROR_500";
                String statusMessage = "System error";

                String resp = transactionService.persistPumpAvailabilityStatus(statusCode, statusMessage, transactionID, requestID);

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getPumpStatus", requestID, null, "persistPumpAvailabilityStatus: " + resp);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getPumpStatusResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", getPumpStatusResponse.getMessageCode()));
        outputParameters.add(new Pair<String, String>("refuelMode", getPumpStatusResponse.getRefuelMode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPumpStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getPumpStatusResponse;
    }

    @Override
    public GetStationDetailsResponse getStationDetails(String requestID, String stationID, String pumpID, Boolean pumpDetailsReq) {

        String pumpDetailsReqString = null;
        if (pumpDetailsReq != null) {
            pumpDetailsReqString = pumpDetailsReq.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpDetailsReq", pumpDetailsReqString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getStationDetails", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetStationDetailsResponse getStationDetailsResponse = new GetStationDetailsResponse();

        Boolean errorFound = false;

        if ((pumpID == null || pumpID.trim().isEmpty()) && (stationID == null || stationID.trim().isEmpty())) {

            getStationDetailsResponse.setStatusCode(ResponseHelper.GET_STATION_DETAILS_PARAMETER_NOT_FOUND);
            getStationDetailsResponse.setMessageCode("");

            errorFound = true;
        }

        if (pumpDetailsReq == null) {

            getStationDetailsResponse.setStatusCode(ResponseHelper.GET_STATION_DETAILS_PARAMETER_NOT_FOUND);
            getStationDetailsResponse.setMessageCode("");

            errorFound = true;
        }

        if (!errorFound) {

            try {

                // Log timestamp chiamata
                Date currentTimestamp = new Date();
                String currentTimestampString = String.valueOf(currentTimestamp.getTime());

                Set<Pair<String, String>> remoteServiceInputParameters = new HashSet<Pair<String, String>>();
                remoteServiceInputParameters.add(new Pair<String, String>("timestamp", currentTimestampString));

                this.log(ErrorLevel.DEBUG, "WsEndpointGatewayMobilePayment", "getStationDetails", requestID, "opening", ActivityLog.createLogMessage(remoteServiceInputParameters));

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                new Proxy().unsetHttp();

                GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());

                //Originale Forecourt
                IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();

                //Chiamata Forecourt Emulator
                //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();

                //((BindingProvider) port1).getRequestContext().put("javax.xml.ws.client.connectionTimeout", "60000");
                //((BindingProvider) port1).getRequestContext().put("javax.xml.ws.client.receiveTimeout", "10000");

                GetStationDetailsMessageRequest forecourtRequest = new GetStationDetailsMessageRequest();
                com.techedge.mp.forecourt.adapter.business.client.StationStatusMessageResponse forecourtResponse;

                forecourtRequest.setPumpID(pumpID);
                forecourtRequest.setRequestID(requestID);
                forecourtRequest.setStationID(stationID);
                forecourtRequest.setPumpDetailsReq(pumpDetailsReq);

                forecourtResponse = port1.getStationDetails(forecourtRequest);

                getStationDetailsResponse.setMessageCode(forecourtResponse.getMessageCode());

                // Log timestamp risposta
                currentTimestamp = new Date();
                currentTimestampString = String.valueOf(currentTimestamp.getTime());

                Set<Pair<String, String>> remoteServiceOutputParameters = new HashSet<Pair<String, String>>();
                remoteServiceOutputParameters.add(new Pair<String, String>("timestamp", currentTimestampString));

                this.log(ErrorLevel.DEBUG, "WsEndpointGatewayMobilePayment", "getStationDetails", requestID, "closing", ActivityLog.createLogMessage(remoteServiceOutputParameters));

                if (forecourtResponse.getStationDetail() != null) {

                    StationDetail sd_local = new StationDetail();

                    sd_local.setAddress(forecourtResponse.getStationDetail().getAddress());
                    sd_local.setCity(forecourtResponse.getStationDetail().getCity());
                    sd_local.setCountry(forecourtResponse.getStationDetail().getCountry());
                    sd_local.setLatitude(forecourtResponse.getStationDetail().getLatitude());
                    sd_local.setLongitude(forecourtResponse.getStationDetail().getLongitude());
                    sd_local.setProvince(forecourtResponse.getStationDetail().getProvince());
                    sd_local.setStationID(forecourtResponse.getStationDetail().getStationID());

                    Date dob = null;
                    DateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:sss");

                    try {
                        dob = df.parse(forecourtResponse.getStationDetail().getValidityDateDetails());
                    }
                    catch (Exception ex) {

                        this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getStationDetails", requestID, null, "Error parsing validity date details: "
                                + forecourtResponse.getStationDetail().getValidityDateDetails());

                        dob = new Date();
                    }

                    GregorianCalendar cal = new GregorianCalendar();
                    cal.setTimeInMillis(dob.getTime());
                    XMLGregorianCalendar xmlDate2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(cal);

                    sd_local.setValidityDateDetails(xmlDate2.toString());

                    // Inserimento degli erogatori
                    List<PumpDetail> pumpList = forecourtResponse.getStationDetail().getPumpDetails();

                    for (PumpDetail pumpDetail : pumpList) {

                        com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail pumpDetailOut = new com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail();

                        pumpDetailOut.setPumpID(pumpDetail.getPumpID());
                        pumpDetailOut.setPumpNumber(pumpDetail.getPumpNumber());

                        if (pumpDetail.getRefuelMode() == null) {

                            pumpDetailOut.setPumpStatus("PUMP_AVAILABLE_200");
                            pumpDetailOut.setRefuelMode(null);
                        }
                        else {

                            pumpDetailOut.setPumpStatus(pumpDetail.getPumpStatus().value());

                            /*if (!pumpDetail.getRefuelMode().equals(ForecourtInfoService.REFUEL_MODE_SELF)) {

                                // Se l'erogatore ha refuelMode diverso da self si imposta lo stato a non disponibile
                                pumpDetailOut.setPumpStatus("PUMP_NOT_AVAILABLE_501");

                                this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getStationDetails", requestID, null,
                                        "refuelMode not valid: " + pumpDetail.getRefuelMode());
                            }
                            else {

                                pumpDetailOut.setPumpStatus(pumpDetail.getPumpStatus().value());
                            }*/

                            pumpDetailOut.setRefuelMode(pumpDetail.getRefuelMode());
                        }

                        for (ProductDetail productDetail : pumpDetail.getProductDetails()) {

                            com.techedge.mp.forecourt.adapter.business.interfaces.ProductDetail productDetailOut = new com.techedge.mp.forecourt.adapter.business.interfaces.ProductDetail();

                            productDetailOut.setFuelType(productDetail.getFuelType());
                            productDetailOut.setProductDescription(productDetail.getProductDescription());
                            productDetailOut.setProductID(productDetail.getProductID().value());
                            productDetailOut.setProductPrice(productDetail.getProductPrice());

                            pumpDetailOut.getProductDetails().add(productDetailOut);
                        }

                        sd_local.getPumpDetails().add(pumpDetailOut);
                    }

                    // Inserimento delle casse
                    List<SourceDetail> sourceList = forecourtResponse.getStationDetail().getSourceDetails();

                    for (SourceDetail sourceDetail : sourceList) {

                        com.techedge.mp.forecourt.adapter.business.interfaces.SourceDetail sourceDetailOut = new com.techedge.mp.forecourt.adapter.business.interfaces.SourceDetail();

                        sourceDetailOut.setSourceID(sourceDetail.getSourceID());
                        sourceDetailOut.setSourceNumber(sourceDetail.getSourceNumber());

                        sd_local.getSourceDetails().add(sourceDetailOut);
                    }

                    getStationDetailsResponse.setStationDetail(sd_local);

                }
                else {

                    this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getStationDetails", requestID, null, "forecourtResponse.getStationDetail null");

                }

                getStationDetailsResponse.setStatusCode(forecourtResponse.getStatusCode().value());
                getStationDetailsResponse.setMessageCode("");
            }
            catch (Exception ex) {

                ex.printStackTrace();

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getStationDetails", requestID, null, "Exception message: " + ex.getMessage());

                getStationDetailsResponse.setStatusCode("GENERIC_FAULT_500");
                getStationDetailsResponse.setMessageCode(ex.getMessage());

            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getStationDetailsResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", getStationDetailsResponse.getMessageCode()));

        if (getStationDetailsResponse.getStationDetail() != null) {
            StationDetail stationDetail = getStationDetailsResponse.getStationDetail();

            String message = "{ stationID: " + stationDetail.getStationID() + ", address: " + stationDetail.getAddress() + ", city: " + stationDetail.getCity() + ", province: "
                    + stationDetail.getProvince() + " }";

            outputParameters.add(new Pair<String, String>("stationDetail", message));

            for (com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail pumpDetail : stationDetail.getPumpDetails()) {
                message = "{ pumpID: " + pumpDetail.getPumpID() + ", pumpNumber: " + pumpDetail.getPumpNumber() + ", pumpStatus: " + pumpDetail.getPumpStatus() + ", refuelMode: "
                        + pumpDetail.getRefuelMode() + " }";

                outputParameters.add(new Pair<String, String>("stationDetail.pumpDetail", message));
            }

            for (com.techedge.mp.forecourt.adapter.business.interfaces.SourceDetail sourceDetail : stationDetail.getSourceDetails()) {
                message = "{ sourceID: " + sourceDetail.getSourceID() + ", sourceNumber: " + sourceDetail.getSourceNumber() + " }";

                outputParameters.add(new Pair<String, String>("stationDetail.sourceDetail", message));
            }
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getStationDetails", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getStationDetailsResponse;

    }

    @Override
    public EnablePumpResponse enablePump(String requestID, String transactionID, String stationID, String pumpID, String paymentMode, String productID, Double amount,
            PaymentAuthorizationResult paymentAuthorizationResult) {

        String amountString = null;
        if (amount != null) {
            amountString = amount.toString();
        }

        String paymentAuthorizationResultString = null;
        if (paymentAuthorizationResult != null) {
            paymentAuthorizationResultString = "AuthorizationCode: " + paymentAuthorizationResult.getAuthorizationCode() + "BankTransactionID: "
                    + paymentAuthorizationResult.getAuthorizationCode() + "TransactionResult" + paymentAuthorizationResult.getTransactionResult();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
        inputParameters.add(new Pair<String, String>("paymentMode", paymentMode));
        inputParameters.add(new Pair<String, String>("productID", productID));
        inputParameters.add(new Pair<String, String>("amount", amountString));
        inputParameters.add(new Pair<String, String>("paymentAuthorizationResult", paymentAuthorizationResultString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "enablePump", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        EnablePumpResponse enablePumpResponse = new EnablePumpResponse();

        Boolean errorFound = false;

        if (transactionID == null || transactionID.trim().isEmpty()) {

            enablePumpResponse.setStatusCode(ResponseHelper.ENABLE_PUMP_PARAMETER_NOT_FOUND);
            enablePumpResponse.setMessageCode("");

            errorFound = true;

        }

        if (pumpID == null || pumpID.trim().isEmpty()) {

            enablePumpResponse.setStatusCode(ResponseHelper.ENABLE_PUMP_PARAMETER_NOT_FOUND);
            enablePumpResponse.setMessageCode("");

            errorFound = true;

        }

        if (paymentMode == null || paymentMode.trim().isEmpty()) {

            enablePumpResponse.setStatusCode(ResponseHelper.ENABLE_PUMP_PARAMETER_NOT_FOUND);
            enablePumpResponse.setMessageCode("");

            errorFound = true;

        }

        if (amount == null) {

            enablePumpResponse.setStatusCode(ResponseHelper.ENABLE_PUMP_PARAMETER_NOT_FOUND);
            enablePumpResponse.setMessageCode("");

            errorFound = true;

        }

        if (!errorFound) {
            /*
             * String statusCode = "GENERIC_FAULT_500";
             * String statusMessage = "Enable Pump Generic Fault";
             * 
             * String resp5 = transactionService.persistPumpEnableStatus(statusCode, statusMessage, transactionID, requestID);
             * 
             * this.log( ErrorLevel.ERROR, this.getClass().getSimpleName(), "enablePump", requestID, null, "persistPumpEnableStatus: " + resp5);
             * 
             * String resp6 = transactionService.persistPumpGenericFaultStatus(transactionID, requestID);
             * 
             * this.log( ErrorLevel.ERROR, this.getClass().getSimpleName(), "enablePump", requestID, null, "persistPumpGenericFaultStatus: " + resp6);
             */

            try {

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                new Proxy().unsetHttp();

                GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());

                //Originale Forecourt
                IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
                //Chiamata Forecourt Emulator
                //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();

                EnablePumpMessageRequest requestForecourt = new EnablePumpMessageRequest();

                requestForecourt.setAmount(amount);
                requestForecourt.setPaymentMode(paymentMode);

                if (productID.equals("SP")) {
                    requestForecourt.setProductID(ProductIdEnum.SP);
                }
                if (productID.equals("GG")) {
                    requestForecourt.setProductID(ProductIdEnum.GG);
                }
                if (productID.equals("BS")) {
                    requestForecourt.setProductID(ProductIdEnum.BS);
                }
                if (productID.equals("BD")) {
                    requestForecourt.setProductID(ProductIdEnum.BD);
                }
                if (productID.equals("MT")) {
                    requestForecourt.setProductID(ProductIdEnum.MT);
                }
                if (productID.equals("GP")) {
                    requestForecourt.setProductID(ProductIdEnum.GP);
                }
                if (productID.equals("AD")) {
                    requestForecourt.setProductID(ProductIdEnum.AD);
                }

                requestForecourt.setPumpID(pumpID);
                requestForecourt.setRequestID(requestID);
                requestForecourt.setStationID(stationID);
                requestForecourt.setTransactionID(transactionID);

                com.techedge.mp.forecourt.adapter.business.client.PaymentAuthorizationResult paymentAuthorizationResultForecourt = new com.techedge.mp.forecourt.adapter.business.client.PaymentAuthorizationResult();
                paymentAuthorizationResultForecourt.setAcquirerId(paymentAuthorizationResult.getAcquirerId());
                paymentAuthorizationResultForecourt.setAuthorizationCode(paymentAuthorizationResult.getAuthorizationCode());
                paymentAuthorizationResultForecourt.setBankTransactionID(paymentAuthorizationResult.getBankTransactionID());
                paymentAuthorizationResultForecourt.setCurrency(paymentAuthorizationResult.getCurrency());
                paymentAuthorizationResultForecourt.setErrorCode(paymentAuthorizationResult.getErrorCode());
                paymentAuthorizationResultForecourt.setErrorDescription(paymentAuthorizationResult.getErrorDescription());
                paymentAuthorizationResultForecourt.setEventType(EventTypeEnum.AUT);
                paymentAuthorizationResultForecourt.setShopLogin(paymentAuthorizationResult.getShopLogin());
                paymentAuthorizationResultForecourt.setShopTransactionID(paymentAuthorizationResult.getShopTransactionID());

                if (paymentAuthorizationResult.getTransactionResult().equals("OK")) {
                    paymentAuthorizationResultForecourt.setTransactionResult(TransactionResultEnum.OK);
                }
                else {
                    paymentAuthorizationResultForecourt.setTransactionResult(TransactionResultEnum.KO);
                }

                requestForecourt.setPaymentAuthorizationResult(paymentAuthorizationResultForecourt);

                EnablePumpMessageResponse forecourtResponse = new EnablePumpMessageResponse();
                //throw new Exception("Eccezione simulata");

                forecourtResponse = port1.enablePump(requestForecourt);

                enablePumpResponse.setStatusCode(forecourtResponse.getStatusCode().toString());
                enablePumpResponse.setMessageCode(forecourtResponse.getMessageCode());

                String response = transactionService.persistPumpEnableStatus(enablePumpResponse.getStatusCode(), enablePumpResponse.getMessageCode(), transactionID, requestID);

                this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "enablePump", requestID, null, "persistPumpEnableStatus: " + response);

            }
            catch (Exception ex) {

                ex.printStackTrace();

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "enablePump", requestID, null, "Exception message: " + ex.getMessage());

                enablePumpResponse.setStatusCode("GENERIC_FAULT_500");
                enablePumpResponse.setMessageCode(ex.getMessage());

                String resp = transactionService.persistPumpEnableStatus(enablePumpResponse.getStatusCode(), enablePumpResponse.getMessageCode(), transactionID, requestID);

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "enablePump", requestID, null, "persistPumpEnableStatus: " + resp);

                String resp2 = transactionService.persistPumpGenericFaultStatus(transactionID, requestID);

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "enablePump", requestID, null, "persistPumpGenericFaultStatus: " + resp2);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", enablePumpResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", enablePumpResponse.getMessageCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "enablePump", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return enablePumpResponse;
    }

    @Override
    public GetTransactionStatusResponse getTransactionStatus(String requestID, String transactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionStatus", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetTransactionStatusResponse getTransactionStatusResponse = new GetTransactionStatusResponse();

        Boolean errorFound = false;

        if (transactionID == null || transactionID.trim().isEmpty()) {

            getTransactionStatusResponse.setStatusCode(ResponseHelper.GET_TRANSACTION_STATUS_PARAMETER_NOT_FOUND);
            getTransactionStatusResponse.setMessageCode("");

            errorFound = true;

        }

        if (!errorFound) {

            try {

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                new Proxy().unsetHttp();

                GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());

                //Originale Forecourt
                IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
                //Chiamata Forecourt Emulator
                //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();

                TransactionStatusMessageRequest forecourtRequest = new TransactionStatusMessageRequest();

                forecourtRequest.setRequestID(requestID);
                forecourtRequest.setTransactionID(transactionID);

                TransactionStatusMessageResponse forecourtResponse = new TransactionStatusMessageResponse();

                forecourtResponse = port1.getTransactionStatus(forecourtRequest);
                forecourtResponse.setMessageCode(forecourtResponse.getMessageCode());

                if (forecourtResponse.getStatusCode() == GetTransactionStatusResponseEnum.REFUEL_INPROGRESS_401) {

                    getTransactionStatusResponse.setStatusCode("REFUEL_INPROGRESS_401");

                    String response = transactionService.persistStartRefuelReceivedStatus(requestID, transactionID, "getTransactionStatus");

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "persistStartRefuelReceivedStatus: " + response);

                }

                if (forecourtResponse.getStatusCode() == GetTransactionStatusResponseEnum.REFUEL_TERMINATED_200) {

                    getTransactionStatusResponse.setStatusCode("REFUEL_TERMINATED_200");

                    Double amount = forecourtResponse.getRefuelDetail().getAmount();
                    Double fuelQuantity = forecourtResponse.getRefuelDetail().getFuelQuantity();
                    String fuelType = forecourtResponse.getRefuelDetail().getFuelType();
                    String productDescription = forecourtResponse.getRefuelDetail().getProductDescription();
                    Double unitPrice = forecourtResponse.getRefuelDetail().getUnitPrice();

                    String productID = "";
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.SP) {
                        productID = "SP";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.GG) {
                        productID = "GG";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.BS) {
                        productID = "BS";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.BD) {
                        productID = "BD";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.MT) {
                        productID = "MT";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.GP) {
                        productID = "GP";
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.AD) {
                        productID = "AD";
                    }

                    System.out.println("TimestampEndRefuel: " + forecourtResponse.getRefuelDetail().getTimestampEndRefuel());

                    String timestampEndRefuel = forecourtResponse.getRefuelDetail().getTimestampEndRefuel();

                    //String timestampEndRefuel = "";

                    String response = transactionService.persistEndRefuelReceivedStatus(requestID, transactionID, amount, fuelQuantity, fuelType, productDescription, productID,
                            timestampEndRefuel, unitPrice, "getTransactionStatus");

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "persistEndRefuelReceivedStatus: " + response);

                }

                if (forecourtResponse.getStatusCode() == GetTransactionStatusResponseEnum.REFUEL_NOT_STARTED_400) {

                    getTransactionStatusResponse.setStatusCode("REFUEL_NOT_STARTED_400");
                }

                if (forecourtResponse.getStatusCode() == GetTransactionStatusResponseEnum.TRANSACTION_NOT_RECOGNIZED_400) {

                    getTransactionStatusResponse.setStatusCode("TRANSACTION_NOT_RECOGNIZED_400");

                    String response = transactionService.persistTransactionStatusRequestKoStatus(requestID, transactionID, "TRANSACTION_NOT_RECOGNIZED_400",
                            "Transaction not recognized");

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "persistTransactionStatusRequestKoStatus: " + response);

                }

                if (forecourtResponse.getStatusCode() == GetTransactionStatusResponseEnum.REFUEL_STATUS_NOT_AVAILABLE_500) {

                    getTransactionStatusResponse.setStatusCode("REFUEL_STATUS_NOT_AVAILABLE_500");

                    String response = transactionService.persistTransactionStatusRequestKoStatus(requestID, transactionID, "REFUEL_STATUS_NOT_AVAILABLE_500",
                            "Refuel status not available");

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "persistTransactionStatusRequestKoStatus: " + response);

                }

                this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "statusCode: " + getTransactionStatusResponse.getStatusCode());

                if (forecourtResponse.getRefuelDetail() != null) {
                    /*
                    String message = "forecourt response: { amount: " + forecourtResponse.getRefuelDetail().getAmount() + ", fuel quantity: "
                            + forecourtResponse.getRefuelDetail().getFuelQuantity() + ", fuel type: " + forecourtResponse.getRefuelDetail().getFuelType() + ", fuel description: "
                            + forecourtResponse.getRefuelDetail().getProductDescription() + ", fuel timestampEndRefuel: "
                            + forecourtResponse.getRefuelDetail().getTimestampEndRefuel() + " }";

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, message);
                    */
                    RefuelDetail rd_local = new RefuelDetail();
                    rd_local.setAmount(forecourtResponse.getRefuelDetail().getAmount());
                    rd_local.setFuelQuantity(forecourtResponse.getRefuelDetail().getFuelQuantity());
                    rd_local.setFuelType(forecourtResponse.getRefuelDetail().getFuelType());
                    rd_local.setProductDescription(forecourtResponse.getRefuelDetail().getProductDescription());

                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.SP) {
                        rd_local.setProductID("SP");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.GG) {
                        rd_local.setProductID("GG");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.BS) {
                        rd_local.setProductID("BS");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.BD) {
                        rd_local.setProductID("BD");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.MT) {
                        rd_local.setProductID("MT");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.GP) {
                        rd_local.setProductID("GP");
                    }
                    if (forecourtResponse.getRefuelDetail().getProductID() == ProductIdEnum.AD) {
                        rd_local.setProductID("AD");
                    }

                    try {

                        //SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");

                        //Date parsedDate = dateFormat.parse(forecourtResponse.getRefuelDetail().getTimestampEndRefuel());

                        //rd_local.setTimestampEndRefuel(parsedDate.getTime());

                        rd_local.setTimestampEndRefuel(forecourtResponse.getRefuelDetail().getTimestampEndRefuel());

                    }
                    catch (Exception ex) {

                        this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "Error timestamp conversion: "
                                + forecourtResponse.getRefuelDetail().getTimestampEndRefuel());

                        rd_local.setTimestampEndRefuel((new Date()).toString());
                    }

                    getTransactionStatusResponse.setRefuelDetail(rd_local);
                }
                else {

                    RefuelDetail rd_local = new RefuelDetail();

                    rd_local.setAmount(0.0);
                    rd_local.setFuelQuantity(0.0);
                    rd_local.setFuelType("");
                    rd_local.setProductDescription("");
                    rd_local.setTimestampEndRefuel("");

                    getTransactionStatusResponse.setRefuelDetail(rd_local);
                }
            }
            catch (Exception ex) {

                ex.printStackTrace();

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "Exception message: " + ex.getMessage());

                getTransactionStatusResponse.setStatusCode("GENERIC_FAULT_500");
                getTransactionStatusResponse.setMessageCode(ex.getMessage());

                String response = transactionService.persistTransactionStatusRequestKoStatus(requestID, transactionID, "GENERIC_FAULT_500", ex.getMessage());

                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getTransactionStatus", requestID, null, "persistTransactionStatusRequestKoStatus: " + response);
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", getTransactionStatusResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", getTransactionStatusResponse.getMessageCode()));

        if (getTransactionStatusResponse.getRefuelDetail() != null) {
            RefuelDetail refuelDetail = getTransactionStatusResponse.getRefuelDetail();
            String message = "{ amount: " + refuelDetail.getAmount().toString() + ", fuelQuantity: " + refuelDetail.getFuelQuantity().toString() + ", fuelType: "
                    + refuelDetail.getFuelType() + ", productID: " + refuelDetail.getProductID() + ", productDescription: " + refuelDetail.getProductDescription()
                    + ", timestampEndRefuel: " + refuelDetail.getTimestampEndRefuel() + " }";

            outputParameters.add(new Pair<String, String>("refuelDetail", message));
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getTransactionStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return getTransactionStatusResponse;
    }

    @Override
    public TransactionReconciliationResponse transactionReconciliation(String requestID, List<String> transactionIDList) {

        int transactionCount = transactionIDList.toArray().length;

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("transactionCount", String.valueOf(transactionCount)));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "transactionReconciliation", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        TransactionReconciliationResponse transactionReconciliationResponse = new TransactionReconciliationResponse();

        if (requestID == null || requestID.trim().isEmpty()) {

            //			trmResponse.setStatusCode(EnumTRANSACTIONSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            //			trmResponse.setMessageCode(prop.getProperty(EnumPUMPSTAUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            return transactionReconciliationResponse;

        }

        /*if (transactionIDList == null) {

            //			trmResponse.setStatusCode(EnumTRANSACTIONSTATUSRESPONSE.PARAMETER_NOT_FOUND_400.name());
            //			trmResponse.setMessageCode(prop.getProperty(EnumPUMPSTAUSRESPONSE.PARAMETER_NOT_FOUND_400.name()));

            return transactionReconciliationResponse;

        }*/

        System.out.println("***********************");
        System.out.println("Create Web Service Client...");
        GatewayMobilePaymentImpl service1;
        try {
            service1 = new GatewayMobilePaymentImpl(this.getUrl());

            System.out.println("Create Web Service...");

            //Originale Forecourt
            IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
            //Chiamata Forecourt Emulator
            //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();
            System.out.println("Call Web Service Operation...");

            this.transactionService = EJBHomeCache.getInstance().getTransactionService();

            new Proxy().unsetHttp();

            //			com.techedge.mp.forecourt.integration.info.client.TransactionReconciliationMessageRequest forecourtRequest = new com.techedge.mp.forecourt.integration.info.client.TransactionReconciliationMessageRequest();
            //			
            //			forecourtRequest.setRequestID(trmRquest.getRequestID());
            //			forecourtRequest.setTransactionIDList(trmRquest.getTransactionIDList());
            //			
            com.techedge.mp.forecourt.adapter.business.client.TransactionReconciliationMessageResponse forecourtResponse = new com.techedge.mp.forecourt.adapter.business.client.TransactionReconciliationMessageResponse();

            com.techedge.mp.forecourt.adapter.business.client.TransactionReconciliationMessageRequest trmRequest = new com.techedge.mp.forecourt.adapter.business.client.TransactionReconciliationMessageRequest();

            ArrayOfString transactionList = new ArrayOfString();
            for (String transactionId : transactionIDList) {
                transactionList.getTransactionID().add(transactionId);
            }

            trmRequest.setRequestID(requestID);
            trmRequest.setTransactionIDList(transactionList);

            forecourtResponse = port1.transactionReconciliation(trmRequest);
            if (forecourtResponse.getTransactionDetails().size() > 0) {
                System.out.println("After call..." + forecourtResponse.getTransactionDetails().get(0).getTransactionID());
            }

            for (TransactionDetails transactionDetails : forecourtResponse.getTransactionDetails()) {

                TransactionDetail transactionDetail = new TransactionDetail();

                transactionDetail.setStatusCode(transactionDetails.getStatusCode().value());
                transactionDetail.setMessageCode(transactionDetails.getMessageCode());
                transactionDetail.setTransactionID(transactionDetails.getTransactionID());
                if (transactionDetails.getTimestamp() != null) {
                    transactionDetail.setTimestamp(transactionDetails.getTimestamp());
                }
                if (transactionDetails.getLastTransactionStatus() != null) {
                    transactionDetail.setLastTransactionStatus(transactionDetails.getLastTransactionStatus().value());
                }

                for (TransactionStatusHistory transactionStatusHistory : transactionDetails.getTransactionStatusHistory()) {

                    TransactionStatus transactionStatus = new TransactionStatus();

                    transactionStatus.setMessageCode(transactionStatusHistory.getMessageCode());
                    transactionStatus.setStatusCode(transactionStatusHistory.getStatusCode().value());
                    transactionStatus.setTimestamp(transactionStatusHistory.getTimestamp());

                    transactionDetail.getTransactionStatusHistory().add(transactionStatus);
                }

                if (transactionDetails.getRefuelDetail() != null) {

                    RefuelDetail refuelDetail = new RefuelDetail();

                    refuelDetail.setAmount(transactionDetails.getRefuelDetail().getAmount());
                    refuelDetail.setFuelQuantity(transactionDetails.getRefuelDetail().getFuelQuantity());
                    refuelDetail.setFuelType(transactionDetails.getRefuelDetail().getFuelType());
                    refuelDetail.setProductDescription(transactionDetails.getRefuelDetail().getProductDescription());
                    refuelDetail.setProductID(transactionDetails.getRefuelDetail().getProductID().value());
                    refuelDetail.setTimestampEndRefuel(transactionDetails.getRefuelDetail().getTimestampEndRefuel());
                    refuelDetail.setUnitPrice(transactionDetails.getRefuelDetail().getUnitPrice());

                    transactionDetail.setRefuelDetail(refuelDetail);
                }

                transactionReconciliationResponse.getTransactionDetails().add(transactionDetail);
            }

            //trmResponse = port1.transactionReconciliation(trmRquest);

            //trmResponse = InfoEmulator.transactionReconciliation(trmRquest);
            //trmResponse.setMessageCode(prop.getProperty(trmResponse.getStatusCode()));

            //		} catch(javax.xml.ws.soap.SOAPFaultException ex) {
            //			
            //			throw new BPELException("SOAP fault exception");
            //
            //		} catch(javax.xml.ws.ProtocolException ex){
            //		
            //			throw new BPELException("Protocol exception");
            //
            //		} catch(javax.xml.ws.WebServiceException ex){
            //
            //			throw new BPELException("Web service exception");
            //
            //		} catch(java.lang.SecurityException ex){
            //
            //			throw new BPELException("Security exception");
        }
        catch (BPELException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return transactionReconciliationResponse;

        }
        catch (Exception ex) {

            return transactionReconciliationResponse;

            //	throw new BPELException("General exception");

        }

        //		trmResponse.setStatusCode(EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE.MESSAGE_RECEIVED_200.name());
        //		trmResponse.setMessageCode(prop.getProperty(EnumSENDPAYMENTTRANSACTIONRESULTSTATUSRESPONSE.MESSAGE_RECEIVED_200.name()));

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();

        for (TransactionDetail transactionDetail : transactionReconciliationResponse.getTransactionDetails()) {
            RefuelDetail refuelDetail = transactionDetail.getRefuelDetail();

            outputParameters.add(new Pair<String, String>("statusCode", transactionDetail.getStatusCode()));
            outputParameters.add(new Pair<String, String>("messageCode", transactionDetail.getMessageCode()));
            outputParameters.add(new Pair<String, String>("transactionID", transactionDetail.getTransactionID()));
            outputParameters.add(new Pair<String, String>("lastTransactionStatus", transactionDetail.getLastTransactionStatus()));

            String amount = "";
            String fuelQuantity = "";
            String fuelType = "";
            String productID = "";
            String productDescription = "";
            String timestampEndRefuel = "";
            String unitPrice = "";

            if (refuelDetail != null) {
                amount = (refuelDetail.getAmount() != null) ? refuelDetail.getAmount().toString() : "";
                fuelQuantity = (refuelDetail.getFuelQuantity() != null) ? refuelDetail.getFuelQuantity().toString() : "";
                fuelType = (refuelDetail.getFuelType() != null) ? refuelDetail.getFuelType() : "";
                productID = (refuelDetail.getProductID() != null) ? refuelDetail.getProductID() : "";
                productDescription = (refuelDetail.getProductDescription() != null) ? refuelDetail.getProductDescription() : "";
                timestampEndRefuel = (refuelDetail.getTimestampEndRefuel() != null) ? refuelDetail.getTimestampEndRefuel() : "";
                unitPrice = (refuelDetail.getUnitPrice() != null) ? refuelDetail.getUnitPrice().toString() : "";
            }

            String message = "{ amount: " + amount + ", fuelQuantity: " + fuelQuantity + ", fuelType: " + fuelType + ", productID: " + productID + ", productDescription: "
                    + productDescription + ", unitPrice: " + unitPrice + ", timestampEndRefuel: " + timestampEndRefuel + " }";

            outputParameters.add(new Pair<String, String>("refuelDetail", message));

            for (TransactionStatus transactionStatus : transactionDetail.getTransactionStatusHistory()) {
                message = "{ statusCode: " + transactionStatus.getStatusCode() + ", messageCode: " + transactionStatus.getMessageCode() + ", timestamp: "
                        + transactionStatus.getTimestamp() + " }";

                outputParameters.add(new Pair<String, String>("transactionStatus", message));
            }
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "transactionReconciliation", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return transactionReconciliationResponse;

    }

    @Override
    public SendPaymentTransactionResultResponse sendPaymentTransactionResult(String requestID, String transactionID, Double amount,
            PaymentTransactionResult paymentTransactionResult) {

        String amountString = null;
        if (amount != null) {
            amountString = amount.toString();
        }

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("amount", amountString));
        inputParameters.add(new Pair<String, String>("PaymentTransactionResult.eventType", paymentTransactionResult.getEventType()));
        inputParameters.add(new Pair<String, String>("PaymentTransactionResult.transactionResult", paymentTransactionResult.getTransactionResult()));
        inputParameters.add(new Pair<String, String>("PaymentTransactionResult.shopTransactionID", paymentTransactionResult.getShopTransactionID()));
        inputParameters.add(new Pair<String, String>("PaymentTransactionResult.bankTransactionID", paymentTransactionResult.getBankTransactionID()));
        inputParameters.add(new Pair<String, String>("PaymentTransactionResult.authorizationCode", paymentTransactionResult.getAuthorizationCode()));
        inputParameters.add(new Pair<String, String>("PaymentTransactionResult.errorCode", paymentTransactionResult.getErrorCode()));
        inputParameters.add(new Pair<String, String>("PaymentTransactionResult.errorDescription", paymentTransactionResult.getErrorDescription()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        SendPaymentTransactionResultResponse sendPaymentTransactionResultResponse = new SendPaymentTransactionResultResponse();

        Boolean errorFound = false;

        if (transactionID == null || transactionID.isEmpty()) {

            sendPaymentTransactionResultResponse.setStatusCode(ResponseHelper.SEND_PAYMENT_TRANSACTION_RESULT_PARAMETER_NOT_FOUND);
            sendPaymentTransactionResultResponse.setMessageCode("");

            errorFound = true;

        }

        if (amount == null) {

            sendPaymentTransactionResultResponse.setStatusCode(ResponseHelper.SEND_PAYMENT_TRANSACTION_RESULT_PARAMETER_NOT_FOUND);
            sendPaymentTransactionResultResponse.setMessageCode("");

            errorFound = true;

        }

        if (!errorFound) {

            try {

                String simulationSendPaymentTransactionResultError = null;
                boolean simulationSendPaymentTransactionResultKO = false;

                this.transactionService = EJBHomeCache.getInstance().getTransactionService();

                Transaction transactionResponse = transactionService.getTransactionDetail(requestID, transactionID);

                new Proxy().unsetHttp();

                GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());

                //Originale Forecourt
                IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
                //Chiamata Forecourt Emulator
                //IGatewayMobilePayment port1 = service1.getIGatewayMobilePaymentImplPort();

                com.techedge.mp.forecourt.adapter.business.client.SendPaymentTransactionResultMessageResponse forecourtResponse = new com.techedge.mp.forecourt.adapter.business.client.SendPaymentTransactionResultMessageResponse();
                com.techedge.mp.forecourt.adapter.business.client.SendPaymentTransactionResultMessageRequest forecourtRequest = new com.techedge.mp.forecourt.adapter.business.client.SendPaymentTransactionResultMessageRequest();

                Double initialAmount = transactionResponse.getInitialAmount();

                System.out.println("Amount iniziale: " + initialAmount + ", Amound finale: " + amount);

                if (amount > initialAmount) {

                    System.out.println("Errore elettrovalvola: final amount maggiore dell'initial amount");

                    amount = initialAmount;

                    System.out.println("Initial amount: " + initialAmount + ", final amount: " + amount);
                }

                forecourtRequest.setAmount(amount);
                forecourtRequest.setRequestID(requestID);
                forecourtRequest.setTransactionID(transactionID);

                System.out.println("RequestID: " + forecourtRequest.getRequestID() + "\n");
                System.out.println("TransactionID: " + forecourtRequest.getTransactionID() + "\n");
                System.out.println("Amount: " + forecourtRequest.getAmount() + "\n");

                com.techedge.mp.forecourt.adapter.business.client.PaymentTransactionResult ptr_local = new com.techedge.mp.forecourt.adapter.business.client.PaymentTransactionResult();

                ptr_local.setShopTransactionID(transactionID);
                ptr_local.setAuthorizationCode(paymentTransactionResult.getAuthorizationCode());
                ptr_local.setBankTransactionID(paymentTransactionResult.getBankTransactionID());
                ptr_local.setErrorCode(paymentTransactionResult.getErrorCode());
                ptr_local.setErrorDescription(paymentTransactionResult.getErrorDescription());

                /* Codice sostituito con la nuova versione per la gestione del flusso di pagamento con voucher
                if (paymentTransactionResult.getTransactionResult().equals("OK")) {

                    ptr_local.setTransactionResult(TransactionResultEnum.OK);

                    if (amount > 0.0) {
                        ptr_local.setEventType(EventTypeEnum.MOV);
                    }
                    else {
                        ptr_local.setEventType(EventTypeEnum.CAN);
                    }
                }
                else {

                    ptr_local.setTransactionResult(TransactionResultEnum.KO);
                }
                */

                if (transactionResponse.getNewPaymentFlow()) {
                    ptr_local.setEventType(EventTypeEnum.CAN);
                }
                else {
                    System.out.println("PaymentMethodType: " + transactionResponse.getPaymentMethodType());
                    if (transactionResponse.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {
                        ptr_local.setEventType(EventTypeEnum.CAN);
                    }
                    else {
                        if (amount > 0.0) {
                            ptr_local.setEventType(EventTypeEnum.MOV);
                        }
                        else {
                            ptr_local.setEventType(EventTypeEnum.CAN);
                        }
                    }
                }

                if (paymentTransactionResult.getTransactionResult().equals("OK")) {
                    ptr_local.setTransactionResult(TransactionResultEnum.OK);
                }
                else {
                    ptr_local.setTransactionResult(TransactionResultEnum.KO);
                }

                forecourtRequest.setPaymentTransactionResult(ptr_local);

                Double totalVoucherAmount = 0.0;

                // Valorizzazione voucher
                if (transactionResponse != null) {

                    if (transactionResponse.getPrePaidConsumeVoucherList().size() > 0) {

                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null, "Consume voucher header found");

                        for (PrePaidConsumeVoucher prePaidVoucherConsume : transactionResponse.getPrePaidConsumeVoucherList()) {

                            if (prePaidVoucherConsume.getOperationType().equals("CONSUME") && prePaidVoucherConsume.getPrePaidConsumeVoucherDetail().size() > 0) {

                                this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null, "Consume voucher found");

                                for (PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetail : prePaidVoucherConsume.getPrePaidConsumeVoucherDetail()) {

                                    Voucher voucher = new Voucher();
                                    voucher.setPromoCode(prePaidConsumeVoucherDetail.getPromoCode());
                                    voucher.setPromoDescription(prePaidConsumeVoucherDetail.getPromoDescription());
                                    voucher.setVoucherAmount(prePaidConsumeVoucherDetail.getConsumedValue());
                                    voucher.setVoucherCode(prePaidConsumeVoucherDetail.getVoucherCode());

                                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null,
                                            "Voucher: code: " + voucher.getVoucherCode() + ", amount: " + voucher.getVoucherAmount());

                                    totalVoucherAmount += voucher.getVoucherAmount();

                                    forecourtRequest.getVoucher().add(voucher);
                                }
                            }
                        }
                    }
                }

                Double residualAmount = amount - totalVoucherAmount;
                System.out.println("   Residual amount:" + residualAmount + "\n");

                if (!transactionResponse.getNewPaymentFlow() && !transactionResponse.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {
                    if (residualAmount > 0.0) {
                        ptr_local.setEventType(EventTypeEnum.MOV);
                    }
                    else {
                        ptr_local.setEventType(EventTypeEnum.CAN);
                    }
                }

                System.out.println("PaymentTransactionResult: \n");
                System.out.println("   ShopTransactionID:" + ptr_local.getShopTransactionID() + "\n");
                System.out.println("   AuthorizationCode:" + ptr_local.getAuthorizationCode() + "\n");
                System.out.println("   BankTransactionID:" + ptr_local.getBankTransactionID() + "\n");
                System.out.println("   ErrorCode:" + ptr_local.getErrorCode() + "\n");
                System.out.println("   ErrorDescription:" + ptr_local.getErrorDescription() + "\n");
                System.out.println("   EventType:" + ptr_local.getEventType() + "\n");
                System.out.println("   TransactionResult:" + ptr_local.getTransactionResult() + "\n");
                
                if (transactionResponse.getBusiness()) {
                    
                    PersonalDataBusiness personalDataBusiness = transactionResponse.getUser().getPersonalDataBusinessList().get(0);
                    PersonalData personalData = transactionResponse.getUser().getPersonalData();
                    
                    if (personalDataBusiness == null) {
                        throw new Exception("No business data for user: " + personalData.getSecurityDataEmail());
                    }
                    
                    com.techedge.mp.forecourt.adapter.business.client.ElectronicInvoice forecourtElectronicInvoice = new com.techedge.mp.forecourt.adapter.business.client.ElectronicInvoice();
                    forecourtElectronicInvoice.setAddress(personalDataBusiness.getAddress());
                    forecourtElectronicInvoice.setBusinessName(personalDataBusiness.getBusinessName());
                    forecourtElectronicInvoice.setCity(personalDataBusiness.getCity());
                    forecourtElectronicInvoice.setCountry(personalDataBusiness.getCountry());
                    forecourtElectronicInvoice.setEmailAddress(personalData.getSecurityDataEmail());
                    
                    // Ricerca la targa preferita
                    String licensePlate = personalDataBusiness.getLicensePlate();
                    for(PlateNumber plateNumber : transactionResponse.getUser().getPlateNumberList()) {
                        
                        if (plateNumber.getDefaultPlateNumber()) {
                            
                            licensePlate = plateNumber.getPlateNumber();
                            break;
                        }
                    }
                    
                    forecourtElectronicInvoice.setLicensePlate(licensePlate);
                    forecourtElectronicInvoice.setName(personalDataBusiness.getFirstName());
                    forecourtElectronicInvoice.setPecEmailAddress(personalDataBusiness.getPecEmail());
                    forecourtElectronicInvoice.setProvince(personalDataBusiness.getProvince());
                    forecourtElectronicInvoice.setSdiCode(personalDataBusiness.getSdiCode());
                    forecourtElectronicInvoice.setStreetNumber(personalDataBusiness.getStreetNumber());
                    forecourtElectronicInvoice.setSurname(personalDataBusiness.getLastName());
                    forecourtElectronicInvoice.setVatNumber(personalDataBusiness.getVatNumber());
                    forecourtElectronicInvoice.setZipCode(personalDataBusiness.getZipCode());
                    forecourtElectronicInvoice.setFiscalCode(personalDataBusiness.getFiscalCode());
                    
                    forecourtRequest.setElectronicInvoice(forecourtElectronicInvoice);
                    
                    System.out.println("Aggiunta alla richiesta SOAP i dati della fattura elettronica");
                    System.out.println("      address: " + forecourtElectronicInvoice.getAddress());
                    System.out.println("      businnesName: " + forecourtElectronicInvoice.getBusinessName());
                    System.out.println("      city: " + forecourtElectronicInvoice.getCity());
                    System.out.println("      country: " + forecourtElectronicInvoice.getCountry());
                    System.out.println("      emailAddress: " + forecourtElectronicInvoice.getEmailAddress());
                    System.out.println("      fiscalCode: " + forecourtElectronicInvoice.getFiscalCode());
                    System.out.println("      licensePlate: " + forecourtElectronicInvoice.getLicensePlate());
                    System.out.println("      name: " + forecourtElectronicInvoice.getName());
                    System.out.println("      pecEmailAddress: " + forecourtElectronicInvoice.getPecEmailAddress());
                    System.out.println("      province: " + forecourtElectronicInvoice.getProvince());
                    System.out.println("      sdiCode: " + forecourtElectronicInvoice.getSdiCode());
                    System.out.println("      streetNumber: " + forecourtElectronicInvoice.getStreetNumber());
                    System.out.println("      surname: " + forecourtElectronicInvoice.getSurname());
                    System.out.println("      vatNumber: " + forecourtElectronicInvoice.getVatNumber());
                    System.out.println("      zipCode: " + forecourtElectronicInvoice.getZipCode());
                }

                if (simulationActive) {

                    simulationSendPaymentTransactionResultError = parametersService.getParamValueNoCache(ForecourtInfoService.PARAM_SIMULATION_SENDPAYMENTTRANSACTIONRESULT_ERROR);
                    simulationSendPaymentTransactionResultKO = Boolean.parseBoolean(parametersService.getParamValueNoCache(ForecourtInfoService.PARAM_SIMULATION_SENDPAYMENTTRANSACTIONRESULT_KO));

                    if (simulationSendPaymentTransactionResultError != null && simulationSendPaymentTransactionResultError.equalsIgnoreCase("before")) {
                        throw new Exception("Eccezione simulata di errore before");
                    }

                    if (simulationSendPaymentTransactionResultKO) {
                        sendPaymentTransactionResultResponse.setStatusCode("SOURCE_STATUS_NOT_AVAILABLE_500");
                        sendPaymentTransactionResultResponse.setMessageCode("Simulazione di KO");

                        return sendPaymentTransactionResultResponse;
                    }
                }

                forecourtResponse = port1.sendPaymentTransactionResult(forecourtRequest);
                String electronicInvoiceID = null;
                
                if (forecourtResponse.getElectronicInvoiceID() != null && !forecourtResponse.getElectronicInvoiceID().trim().isEmpty()) {
                    electronicInvoiceID = forecourtResponse.getElectronicInvoiceID();
                }

                sendPaymentTransactionResultResponse.setMessageCode(forecourtResponse.getMessageCode());
                sendPaymentTransactionResultResponse.setStatusCode(forecourtResponse.getStatusCode().toString());
                sendPaymentTransactionResultResponse.setElectronicInvoiceID(electronicInvoiceID);

                String response = transactionService.setGFGNotification(transactionID, true, electronicInvoiceID);
                
                if (forecourtResponse.getStatusCode().toString().equals(ResponseHelper.SEND_PAYMENT_TRANSACTION_RESULT_MESSAGE_RECEIVED)) {
                    if (electronicInvoiceID != null) {
                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null, "setGFGNotification and setGFGElectronicInvoiceID response: " + response);
                    }
                    else {
                        this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null, "setGFGNotification response: " + response);
                    }
                }
                else {

                    this.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, null, "setGFGNotification response: KO");
                }
            }
            catch (Exception ex) {

                ex.printStackTrace();

                sendPaymentTransactionResultResponse.setStatusCode(ResponseHelper.SYSTEM_ERROR);
                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, "closing", "Exception message: " + ex.getMessage());
            }
        }

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", sendPaymentTransactionResultResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", sendPaymentTransactionResultResponse.getMessageCode()));
        outputParameters.add(new Pair<String, String>("electronicInvoiceID", sendPaymentTransactionResultResponse.getElectronicInvoiceID()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendPaymentTransactionResult", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

        return sendPaymentTransactionResultResponse;

    }
    
    @Override
    public SendStationListPaymentResponse sendStationListPayment(String requestID, List<StationPayment> stationPaymentList) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendStationListPayment", requestID, "opening", ActivityLog.createLogMessage(inputParameters));
        
        SendStationListPaymentMessageRequest request = new SendStationListPaymentMessageRequest();
        SendStationListPaymentResponse response = new SendStationListPaymentResponse();
        request.setRequestID(requestID);
        
        try {        
            if (stationPaymentList != null) {
                for (StationPayment stationPayment : stationPaymentList) {
                    com.techedge.mp.forecourt.adapter.business.client.StationPayment stationPaymentRequest = new com.techedge.mp.forecourt.adapter.business.client.StationPayment();
                    stationPaymentRequest.setStationId(stationPayment.getStationID());
                    stationPaymentRequest.setPayment(stationPayment.getPayment());
                    request.getStationList().add(stationPaymentRequest);
                }
            }
        
            new Proxy().unsetHttp();
    
            GatewayMobilePaymentImpl service1 = new GatewayMobilePaymentImpl(this.getUrl());
    
            //Originale Forecourt
            IGatewayMobilePayment port1 = service1.getWsEndpointGatewayMobilePayment();
            
            com.techedge.mp.forecourt.adapter.business.client.SendStationListPaymentMessageResponse stationPaymentListResponse = port1.sendStationListPayment(request);
            
            if (stationPaymentListResponse.getStatusCode() != null) {
                response.setStatusCode(stationPaymentListResponse.getStatusCode().value());
                response.setMessageCode(stationPaymentListResponse.getMessageCode());
                
                if (stationPaymentListResponse.getStationList() != null) {
                    for (StationNotFound stationNotFound : stationPaymentListResponse.getStationList()) {
                        response.getStationNotFound().add(stationNotFound.getStationId());
                    }
                }
            }
            
            String stationListString = "{ ";
            
            if (stationPaymentListResponse.getStationList() != null) {
                int i = 0;
                for (StationNotFound stationNotFound : stationPaymentListResponse.getStationList()) {
                    stationListString += "stationID: " + stationNotFound.getStationId();
                    i++;

                    if (i < stationPaymentListResponse.getStationList().size()) {
                        stationListString += "; ";
                    }
                }
            }
            
            stationListString += "}";
            
            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", stationPaymentListResponse.getStatusCode().name()));
            outputParameters.add(new Pair<String, String>("statusMessage", stationPaymentListResponse.getMessageCode()));
            outputParameters.add(new Pair<String, String>("StationNotFound", stationListString));
            
            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "sendStationListPayment", requestID, "closing", ActivityLog.createLogMessage(outputParameters));
            
        }
        catch (Exception ex) {
    
            ex.printStackTrace();
    
            response.setStatusCode(ResponseHelper.SYSTEM_ERROR);
            response.setMessageCode(ex.getMessage());

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "sendStationListPayment", requestID, "closing", "Exception message: " + ex.getMessage());
        }
        
        return response;
    }

}
