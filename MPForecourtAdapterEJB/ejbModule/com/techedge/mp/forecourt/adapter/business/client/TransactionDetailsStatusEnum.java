
package com.techedge.mp.forecourt.adapter.business.client;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per transactionDetailsStatusEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="transactionDetailsStatusEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MESSAGE_RECEIVED_200"/>
 *     &lt;enumeration value="TRANSACTION_NOT_RECOGNIZED_400"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "transactionDetailsStatusEnum")
@XmlEnum
public enum TransactionDetailsStatusEnum implements Serializable {

    MESSAGE_RECEIVED_200,
    TRANSACTION_NOT_RECOGNIZED_400;

    public String value() {
        return name();
    }

    public static TransactionDetailsStatusEnum fromValue(String v) {
        return valueOf(v);
    }

}
