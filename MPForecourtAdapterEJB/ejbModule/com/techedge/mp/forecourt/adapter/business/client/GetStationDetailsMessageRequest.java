
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per getStationDetailsMessageRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="getStationDetailsMessageRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stationID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pumpID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pumpDetailsReq" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getStationDetailsMessageRequest", propOrder = {
    "requestID",
    "stationID",
    "pumpID",
    "pumpDetailsReq"
})
public class GetStationDetailsMessageRequest {

    @XmlElement(required = true, nillable = true)
    protected String requestID;
    protected String stationID;
    @XmlElement(required = true, nillable = true)
    protected String pumpID;
    protected boolean pumpDetailsReq;

    /**
     * Recupera il valore della proprietÓ requestID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Imposta il valore della proprietÓ requestID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Recupera il valore della proprietÓ stationID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStationID() {
        return stationID;
    }

    /**
     * Imposta il valore della proprietÓ stationID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStationID(String value) {
        this.stationID = value;
    }

    /**
     * Recupera il valore della proprietÓ pumpID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpID() {
        return pumpID;
    }

    /**
     * Imposta il valore della proprietÓ pumpID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpID(String value) {
        this.pumpID = value;
    }

    /**
     * Recupera il valore della proprietÓ pumpDetailsReq.
     * 
     */
    public boolean isPumpDetailsReq() {
        return pumpDetailsReq;
    }

    /**
     * Imposta il valore della proprietÓ pumpDetailsReq.
     * 
     */
    public void setPumpDetailsReq(boolean value) {
        this.pumpDetailsReq = value;
    }

}
