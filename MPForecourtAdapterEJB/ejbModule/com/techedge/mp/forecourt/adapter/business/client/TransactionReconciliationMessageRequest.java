
package com.techedge.mp.forecourt.adapter.business.client;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per transactionReconciliationMessageRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="transactionReconciliationMessageRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionIDList" type="{http://gatewaymobilepayment.4ts.it/}ArrayOfString"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "transactionReconciliationMessageRequest", propOrder = {
    "requestID",
    "transactionIDList"
})
public class TransactionReconciliationMessageRequest implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -1093021163062251675L;
	@XmlElement(required = true, nillable = true)
    protected String requestID;
    @XmlElement(required = true, nillable = true)
    protected ArrayOfString transactionIDList = new ArrayOfString();

    /**
     * Recupera il valore della proprietÓ requestID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Imposta il valore della proprietÓ requestID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Recupera il valore della proprietÓ transactionIDList.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfString }
     *     
     */
    public ArrayOfString getTransactionIDList() {
        return transactionIDList;
    }

    /**
     * Imposta il valore della proprietÓ transactionIDList.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfString }
     *     
     */
    public void setTransactionIDList(ArrayOfString value) {
        this.transactionIDList = value;
    }

}
