
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sendStationListPaymentRequest" type="{http://gatewaymobilepayment.4ts.it/}sendStationListPaymentMessageRequest"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendStationListPaymentRequest"
})
@XmlRootElement(name = "sendStationListPayment")
public class SendStationListPayment {

    @XmlElement(required = true, nillable = true)
    protected SendStationListPaymentMessageRequest sendStationListPaymentRequest;

    /**
     * Recupera il valore della proprietà sendStationListPaymentRequest.
     * 
     * @return
     *     possible object is
     *     {@link SendStationListPaymentMessageRequest }
     *     
     */
    public SendStationListPaymentMessageRequest getSendStationListPaymentRequest() {
        return sendStationListPaymentRequest;
    }

    /**
     * Imposta il valore della proprietà sendStationListPaymentRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link SendStationListPaymentMessageRequest }
     *     
     */
    public void setSendStationListPaymentRequest(SendStationListPaymentMessageRequest value) {
        this.sendStationListPaymentRequest = value;
    }

}
