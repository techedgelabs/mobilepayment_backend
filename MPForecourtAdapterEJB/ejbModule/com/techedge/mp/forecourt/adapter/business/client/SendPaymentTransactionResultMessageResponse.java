
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per sendPaymentTransactionResultMessageResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="sendPaymentTransactionResultMessageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="statusCode" type="{http://gatewaymobilepayment.4ts.it/}sendPaymentTransactionResultResponseEnum"/>
 *         &lt;element name="messageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendPaymentTransactionResultMessageResponse", propOrder = {
    "statusCode",
    "messageCode",
    "electronicInvoiceID"
})
public class SendPaymentTransactionResultMessageResponse {

    @XmlElement(required = true)
    protected SendPaymentTransactionResultResponseEnum statusCode;
    @XmlElement(required = true, nillable = true)
    protected String messageCode;
    @XmlElement(name = "electronicInvoiceID")
    protected String electronicInvoiceID;

    /**
     * Recupera il valore della proprietÓ statusCode.
     * 
     * @return
     *     possible object is
     *     {@link SendPaymentTransactionResultResponseEnum }
     *     
     */
    public SendPaymentTransactionResultResponseEnum getStatusCode() {
        return statusCode;
    }

    /**
     * Imposta il valore della proprietÓ statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link SendPaymentTransactionResultResponseEnum }
     *     
     */
    public void setStatusCode(SendPaymentTransactionResultResponseEnum value) {
        this.statusCode = value;
    }

    /**
     * Recupera il valore della proprietÓ messageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Imposta il valore della proprietÓ messageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    public String getElectronicInvoiceID() {
        return electronicInvoiceID;
    }
    
    public void setElectronicInvoiceID(String electronicInvoiceID) {
        this.electronicInvoiceID = electronicInvoiceID;
    }
}
