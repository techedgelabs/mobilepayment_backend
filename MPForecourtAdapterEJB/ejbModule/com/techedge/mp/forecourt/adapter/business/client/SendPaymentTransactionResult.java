
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per anonymous complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sendPaymentTransactionResultMessageRequest" type="{http://gatewaymobilepayment.4ts.it/}sendPaymentTransactionResultMessageRequest"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "sendPaymentTransactionResultMessageRequest"
})
@XmlRootElement(name = "sendPaymentTransactionResult")
public class SendPaymentTransactionResult {

    @XmlElement(required = true, nillable = true)
    protected SendPaymentTransactionResultMessageRequest sendPaymentTransactionResultMessageRequest;

    /**
     * Recupera il valore della proprietÓ sendPaymentTransactionResultMessageRequest.
     * 
     * @return
     *     possible object is
     *     {@link SendPaymentTransactionResultMessageRequest }
     *     
     */
    public SendPaymentTransactionResultMessageRequest getSendPaymentTransactionResultMessageRequest() {
        return sendPaymentTransactionResultMessageRequest;
    }

    /**
     * Imposta il valore della proprietÓ sendPaymentTransactionResultMessageRequest.
     * 
     * @param value
     *     allowed object is
     *     {@link SendPaymentTransactionResultMessageRequest }
     *     
     */
    public void setSendPaymentTransactionResultMessageRequest(SendPaymentTransactionResultMessageRequest value) {
        this.sendPaymentTransactionResultMessageRequest = value;
    }

}
