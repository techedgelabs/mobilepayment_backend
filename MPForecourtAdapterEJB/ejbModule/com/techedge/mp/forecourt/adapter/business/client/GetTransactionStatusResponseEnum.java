
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per getTransactionStatusResponseEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="getTransactionStatusResponseEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REFUEL_TERMINATED_200"/>
 *     &lt;enumeration value="REFUEL_NOT_STARTED_400"/>
 *     &lt;enumeration value="REFUEL_INPROGRESS_401"/>
 *     &lt;enumeration value="TRANSACTION_NOT_RECOGNIZED_400"/>
 *     &lt;enumeration value="REFUEL_STATUS_NOT_AVAILABLE_500"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "getTransactionStatusResponseEnum")
@XmlEnum
public enum GetTransactionStatusResponseEnum {

    REFUEL_TERMINATED_200,
    REFUEL_NOT_STARTED_400,
    REFUEL_INPROGRESS_401,
    TRANSACTION_NOT_RECOGNIZED_400,
    REFUEL_STATUS_NOT_AVAILABLE_500;

    public String value() {
        return name();
    }

    public static GetTransactionStatusResponseEnum fromValue(String v) {
        return valueOf(v);
    }

}
