
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per stationStatusMessageResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="stationStatusMessageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="statusCode" type="{http://gatewaymobilepayment.4ts.it/}stationStatusMessageResponseEnum"/>
 *         &lt;element name="messageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stationDetail" type="{http://gatewaymobilepayment.4ts.it/}stationDetail" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "stationStatusMessageResponse", propOrder = {
    "statusCode",
    "messageCode",
    "stationDetail"
})
public class StationStatusMessageResponse {

    @XmlElement(required = true)
    protected StationStatusMessageResponseEnum statusCode;
    @XmlElement(required = true, nillable = true)
    protected String messageCode;
    protected StationDetail stationDetail;

    /**
     * Recupera il valore della proprietÓ statusCode.
     * 
     * @return
     *     possible object is
     *     {@link StationStatusMessageResponseEnum }
     *     
     */
    public StationStatusMessageResponseEnum getStatusCode() {
        return statusCode;
    }

    /**
     * Imposta il valore della proprietÓ statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link StationStatusMessageResponseEnum }
     *     
     */
    public void setStatusCode(StationStatusMessageResponseEnum value) {
        this.statusCode = value;
    }

    /**
     * Recupera il valore della proprietÓ messageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Imposta il valore della proprietÓ messageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }

    /**
     * Recupera il valore della proprietÓ stationDetail.
     * 
     * @return
     *     possible object is
     *     {@link StationDetail }
     *     
     */
    public StationDetail getStationDetail() {
        return stationDetail;
    }

    /**
     * Imposta il valore della proprietÓ stationDetail.
     * 
     * @param value
     *     allowed object is
     *     {@link StationDetail }
     *     
     */
    public void setStationDetail(StationDetail value) {
        this.stationDetail = value;
    }

}
