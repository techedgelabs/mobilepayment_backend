
package com.techedge.mp.forecourt.adapter.business.client;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per sendPaymentTransactionResultMessageRequest complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="sendPaymentTransactionResultMessageRequest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="requestID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="transactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="paymentTransactionResult" type="{http://gatewaymobilepayment.4ts.it/}paymentTransactionResult"/>
 *         &lt;element name="Voucher" type="{http://gatewaymobilepayment.4ts.it/}voucher" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sendPaymentTransactionResultMessageRequest", propOrder = {
    "requestID",
    "transactionID",
    "amount",
    "paymentTransactionResult",
    "voucher",
    "electronicInvoice"
})
public class SendPaymentTransactionResultMessageRequest {

    @XmlElement(required = true, nillable = true)
    protected String requestID;
    @XmlElement(required = true, nillable = true)
    protected String transactionID;
    protected double amount;
    @XmlElement(required = true, nillable = true)
    protected PaymentTransactionResult paymentTransactionResult;
    @XmlElement(name = "Voucher")
    protected List<Voucher> voucher;
    @XmlElement(name = "ElectronicInvoice")
    protected ElectronicInvoice electronicInvoice;

    /**
     * Recupera il valore della proprietÓ requestID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestID() {
        return requestID;
    }

    /**
     * Imposta il valore della proprietÓ requestID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestID(String value) {
        this.requestID = value;
    }

    /**
     * Recupera il valore della proprietÓ transactionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Imposta il valore della proprietÓ transactionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Recupera il valore della proprietÓ amount.
     * 
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Imposta il valore della proprietÓ amount.
     * 
     */
    public void setAmount(double value) {
        this.amount = value;
    }

    /**
     * Recupera il valore della proprietÓ paymentTransactionResult.
     * 
     * @return
     *     possible object is
     *     {@link PaymentTransactionResult }
     *     
     */
    public PaymentTransactionResult getPaymentTransactionResult() {
        return paymentTransactionResult;
    }

    /**
     * Imposta il valore della proprietÓ paymentTransactionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link PaymentTransactionResult }
     *     
     */
    public void setPaymentTransactionResult(PaymentTransactionResult value) {
        this.paymentTransactionResult = value;
    }

    /**
     * Gets the value of the voucher property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the voucher property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getVoucher().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Voucher }
     * 
     * 
     */
    public List<Voucher> getVoucher() {
        if (voucher == null) {
            voucher = new ArrayList<Voucher>();
        }
        return this.voucher;
    }

    public void setElectronicInvoice(ElectronicInvoice electronicInvoice) {
        this.electronicInvoice = electronicInvoice;
    }
    
    public ElectronicInvoice getElectronicInvoice() {
        return electronicInvoice;
    }
    
}
