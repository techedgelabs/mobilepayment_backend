
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per pumpDetail complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="sourceDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sourceID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="sourceNumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "sourceDetail", propOrder = {
    "sourceID",
    "sourceNumber"
})
public class SourceDetail {

    @XmlElement(required = true, nillable = true)
    protected String sourceID;
    @XmlElement(required = true, nillable = true)
    protected String sourceNumber;

    /**
     * Recupera il valore della proprietÓ sourceID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceID() {
        return sourceID;
    }

    /**
     * Imposta il valore della proprietÓ sourceID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceID(String value) {
        this.sourceID = value;
    }

    /**
     * Recupera il valore della proprietÓ sourceNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceNumber() {
        return sourceNumber;
    }

    /**
     * Imposta il valore della proprietÓ sourceNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceNumber(String value) {
        this.sourceNumber = value;
    }
}
