
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per sendStationListPaymentMessageResponseEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="sendStationListPaymentMessageResponseEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="STATION_NOT_FOUND_404"/&gt;
 *     &lt;enumeration value="MESSAGE_RECEIVED_200"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "sendStationListPaymentMessageResponseEnum")
@XmlEnum
public enum SendStationListPaymentMessageResponseEnum {

    STATION_NOT_FOUND_404,
    MESSAGE_RECEIVED_200;

    public String value() {
        return name();
    }

    public static SendStationListPaymentMessageResponseEnum fromValue(String v) {
        return valueOf(v);
    }

}
