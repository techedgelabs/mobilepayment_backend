
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per paymentAuthorizationResult complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="paymentAuthorizationResult">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="shopLogin" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="acquirerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="eventType" type="{http://gatewaymobilepayment.4ts.it/}eventTypeEnum"/>
 *         &lt;element name="transactionResult" type="{http://gatewaymobilepayment.4ts.it/}transactionResultEnum"/>
 *         &lt;element name="shopTransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="bankTransactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="authorizationCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="errorDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "paymentAuthorizationResult", propOrder = {
    "shopLogin",
    "acquirerId",
    "eventType",
    "transactionResult",
    "shopTransactionID",
    "bankTransactionID",
    "authorizationCode",
    "currency",
    "errorCode",
    "errorDescription"
})
public class PaymentAuthorizationResult {

    @XmlElement(required = true, nillable = true)
    protected String shopLogin;
    @XmlElement(required = true, nillable = true)
    protected String acquirerId;
    @XmlElement(required = true)
    protected EventTypeEnum eventType;
    @XmlElement(required = true)
    protected TransactionResultEnum transactionResult;
    @XmlElement(required = true, nillable = true)
    protected String shopTransactionID;
    @XmlElement(required = true, nillable = true)
    protected String bankTransactionID;
    @XmlElement(required = true, nillable = true)
    protected String authorizationCode;
    protected String currency;
    protected String errorCode;
    protected String errorDescription;

    /**
     * Recupera il valore della proprietÓ shopLogin.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopLogin() {
        return shopLogin;
    }

    /**
     * Imposta il valore della proprietÓ shopLogin.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopLogin(String value) {
        this.shopLogin = value;
    }

    /**
     * Recupera il valore della proprietÓ acquirerId.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAcquirerId() {
        return acquirerId;
    }

    /**
     * Imposta il valore della proprietÓ acquirerId.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAcquirerId(String value) {
        this.acquirerId = value;
    }

    /**
     * Recupera il valore della proprietÓ eventType.
     * 
     * @return
     *     possible object is
     *     {@link EventTypeEnum }
     *     
     */
    public EventTypeEnum getEventType() {
        return eventType;
    }

    /**
     * Imposta il valore della proprietÓ eventType.
     * 
     * @param value
     *     allowed object is
     *     {@link EventTypeEnum }
     *     
     */
    public void setEventType(EventTypeEnum value) {
        this.eventType = value;
    }

    /**
     * Recupera il valore della proprietÓ transactionResult.
     * 
     * @return
     *     possible object is
     *     {@link TransactionResultEnum }
     *     
     */
    public TransactionResultEnum getTransactionResult() {
        return transactionResult;
    }

    /**
     * Imposta il valore della proprietÓ transactionResult.
     * 
     * @param value
     *     allowed object is
     *     {@link TransactionResultEnum }
     *     
     */
    public void setTransactionResult(TransactionResultEnum value) {
        this.transactionResult = value;
    }

    /**
     * Recupera il valore della proprietÓ shopTransactionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShopTransactionID() {
        return shopTransactionID;
    }

    /**
     * Imposta il valore della proprietÓ shopTransactionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShopTransactionID(String value) {
        this.shopTransactionID = value;
    }

    /**
     * Recupera il valore della proprietÓ bankTransactionID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankTransactionID() {
        return bankTransactionID;
    }

    /**
     * Imposta il valore della proprietÓ bankTransactionID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankTransactionID(String value) {
        this.bankTransactionID = value;
    }

    /**
     * Recupera il valore della proprietÓ authorizationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAuthorizationCode() {
        return authorizationCode;
    }

    /**
     * Imposta il valore della proprietÓ authorizationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAuthorizationCode(String value) {
        this.authorizationCode = value;
    }

    /**
     * Recupera il valore della proprietÓ currency.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Imposta il valore della proprietÓ currency.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Recupera il valore della proprietÓ errorCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorCode() {
        return errorCode;
    }

    /**
     * Imposta il valore della proprietÓ errorCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorCode(String value) {
        this.errorCode = value;
    }

    /**
     * Recupera il valore della proprietÓ errorDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getErrorDescription() {
        return errorDescription;
    }

    /**
     * Imposta il valore della proprietÓ errorDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setErrorDescription(String value) {
        this.errorDescription = value;
    }

}
