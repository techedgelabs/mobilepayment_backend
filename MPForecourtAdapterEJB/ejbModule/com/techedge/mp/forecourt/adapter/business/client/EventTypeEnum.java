
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per eventTypeEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="eventTypeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="AUT"/>
 *     &lt;enumeration value="MOV"/>
 *     &lt;enumeration value="CAN"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "eventTypeEnum")
@XmlEnum
public enum EventTypeEnum {

    AUT,
    MOV,
    CAN;

    public String value() {
        return name();
    }

    public static EventTypeEnum fromValue(String v) {
        return valueOf(v);
    }

}
