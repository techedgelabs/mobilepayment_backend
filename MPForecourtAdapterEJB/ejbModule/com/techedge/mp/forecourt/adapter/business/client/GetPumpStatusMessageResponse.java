
package com.techedge.mp.forecourt.adapter.business.client;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per getPumpStatusMessageResponse complex type.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * 
 * <pre>
 * &lt;complexType name="getPumpStatusMessageResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="statusCode" type="{http://gatewaymobilepayment.4ts.it/}pumpStatusEnum"/>
 *         &lt;element name="messageCode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="refuelMode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getPumpStatusMessageResponse", propOrder = {
    "statusCode",
    "messageCode",
    "refuelMode"
})
public class GetPumpStatusMessageResponse {

    @XmlElement(required = true)
    protected PumpStatusEnum statusCode;
    @XmlElement(required = true, nillable = true)
    protected String messageCode;
    @XmlElement(required = true, nillable = true)
    protected String refuelMode;

    /**
     * Recupera il valore della proprietÓ statusCode.
     * 
     * @return
     *     possible object is
     *     {@link PumpStatusEnum }
     *     
     */
    public PumpStatusEnum getStatusCode() {
        return statusCode;
    }

    /**
     * Imposta il valore della proprietÓ statusCode.
     * 
     * @param value
     *     allowed object is
     *     {@link PumpStatusEnum }
     *     
     */
    public void setStatusCode(PumpStatusEnum value) {
        this.statusCode = value;
    }

    /**
     * Recupera il valore della proprietÓ messageCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageCode() {
        return messageCode;
    }

    /**
     * Imposta il valore della proprietÓ messageCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageCode(String value) {
        this.messageCode = value;
    }
    
    /**
     * Recupera il valore della proprietÓ refuelMode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefuelMode() {
        return refuelMode;
    }

    /**
     * Imposta il valore della proprietÓ refuelMode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefuelMode(String value) {
        this.refuelMode = value;
    }

}
