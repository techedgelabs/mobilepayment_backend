
package com.techedge.mp.forecourt.adapter.business.client;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java per transactionStatusEnum.
 * 
 * <p>Il seguente frammento di schema specifica il contenuto previsto contenuto in questa classe.
 * <p>
 * <pre>
 * &lt;simpleType name="transactionStatusEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="REFUEL_ENABLED_200"/>
 *     &lt;enumeration value="REFUEL_STARTED_201"/>
 *     &lt;enumeration value="REFUEL_ENDED_202"/>
 *     &lt;enumeration value="TIMEOUT_AFTER_PUMP_ENABLE_400"/>
 *     &lt;enumeration value="TIMEOUT_AFTER_START_REFUEL_400"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "transactionStatusEnum")
@XmlEnum
public enum TransactionStatusEnum implements Serializable {

    REFUEL_ENABLED_200,
    REFUEL_STARTED_201,
    REFUEL_ENDED_202,
    TIMEOUT_AFTER_PUMP_ENABLE_400,
    TIMEOUT_AFTER_START_REFUEL_400;

    public String value() {
        return name();
    }

    public static TransactionStatusEnum fromValue(String v) {
        return valueOf(v);
    }

}
