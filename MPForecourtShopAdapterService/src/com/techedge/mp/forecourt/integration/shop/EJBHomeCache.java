package com.techedge.mp.forecourt.integration.shop;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.PostPaidTransactionServiceRemote; 
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;


public class EJBHomeCache {

	final String userServiceRemoteJndi        = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/UserService!com.techedge.mp.core.business.UserServiceRemote";
	final String parametersServiceRemoteJndi  = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
	final String loggerServiceRemoteJndi      = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
	final String transactionServiceRemoteJndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/TransactionService!com.techedge.mp.core.business.TransactionServiceRemote";
	final String postPaidTransactionServiceRemoteJndi      = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/PostPaidTransactionService!com.techedge.mp.core.business.PostPaidTransactionServiceRemote";
	
	private static EJBHomeCache instance;
	
	protected Context context = null;
	
	protected UserServiceRemote       userService       = null;
	protected ParametersServiceRemote parametersService = null;
	protected LoggerServiceRemote     loggerService     = null;
	protected TransactionServiceRemote transactionService = null;
	protected PostPaidTransactionServiceRemote postPaidTransactionService = null;
	
	
	private EJBHomeCache( ) throws InterfaceNotFoundException {
		
		final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();
		
		jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		
		try {
			context = new InitialContext(jndiProperties);
		} catch (NamingException e) {

			throw new InterfaceNotFoundException("Naming exception: " + e.getMessage());
		}
		
		try {
            userService = (UserServiceRemote)context.lookup(userServiceRemoteJndi);
		}
		catch (Exception ex) {

        	throw new InterfaceNotFoundException(userServiceRemoteJndi);
        }
		
		try {
            parametersService = (ParametersServiceRemote)context.lookup(parametersServiceRemoteJndi);
		}
		catch (Exception ex) {

        	throw new InterfaceNotFoundException(parametersServiceRemoteJndi);
        }
		
		try {
            loggerService = (LoggerServiceRemote)context.lookup(loggerServiceRemoteJndi);
        }
		catch (Exception ex) {

        	throw new InterfaceNotFoundException(loggerServiceRemoteJndi);
        }
		
		try {
            transactionService = (TransactionServiceRemote)context.lookup(transactionServiceRemoteJndi);
        }
		catch (Exception ex) {

        	throw new InterfaceNotFoundException(transactionServiceRemoteJndi);
        }
		
		try {
			postPaidTransactionService = (PostPaidTransactionServiceRemote)context.lookup(postPaidTransactionServiceRemoteJndi);
		}
		catch (Exception ex) {

        	throw new InterfaceNotFoundException(parametersServiceRemoteJndi);
        }
		
	}

	public PostPaidTransactionServiceRemote getPostPaidTransactionService() {
		return postPaidTransactionService;
	}

	public static synchronized EJBHomeCache getInstance( ) throws InterfaceNotFoundException
	{
		if (instance == null)
			instance = new EJBHomeCache( );
		
		return instance;
	}

	public UserServiceRemote getUserService( )
	{
		return userService;
	}
	
	public ParametersServiceRemote getParametersService( )
	{
		return parametersService;
	}
	
	public LoggerServiceRemote getLoggerService( )
	{
		return loggerService;
	}
	
	public TransactionServiceRemote getTransactionService( )
	{
		return transactionService;
	}
}
