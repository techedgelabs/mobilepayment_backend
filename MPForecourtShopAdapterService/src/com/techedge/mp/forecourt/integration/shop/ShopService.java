package com.techedge.mp.forecourt.integration.shop;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.PostPaidTransactionServiceRemote;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PaymentTransactionEvent;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCreateShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidEnableShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidReverseShopTransactionResponse;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.forecourt.integration.shop.interfaces.CreateMPTransactionMessageResponseOUT;
import com.techedge.mp.forecourt.integration.shop.interfaces.EnableMPTransactionMessageResponseOUT;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetMPTransactionStatusMessageResponseOUT;
import com.techedge.mp.forecourt.integration.shop.interfaces.PaymentTransactionStatus;
import com.techedge.mp.forecourt.integration.shop.interfaces.ProductTypeEnum;
import com.techedge.mp.forecourt.integration.shop.interfaces.ReverseMPTransactionMessageResponseOUT;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShopRefuelDetail;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShoppingCartDetail;

@WebService()
public class ShopService {

    private PostPaidTransactionServiceRemote postPaidTransactionService = null;
    private LoggerServiceRemote              loggerService              = null;

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    @WebMethod(operationName = "createMPTransaction")
    @WebResult(name = "createMPTransactionMessageResponse")
    public @XmlElement(required = true)
    CreateMPTransactionMessageResponseOUT createMPTransaction(@XmlElement(required = true) @WebParam(name = "requestID") String requestID, @XmlElement(required = true) @WebParam(name = "source") String source, @XmlElement(required = true) @WebParam(name = "sourceID") String sourceID, @XmlElement(required = true) @WebParam(name = "sourceNumber") String sourceNumber, @XmlElement(required = true) @WebParam(name = "stationID") String stationID, @XmlElement(required = true) @WebParam(name = "srcTransactionID") String srcTransactionID, @XmlElement(required = true) @WebParam(name = "amount") Double amount, @XmlElement(required = true) @WebParam(name = "productType") ProductTypeEnum productType, @WebParam(name = "shoppingCart") List<ShoppingCartDetail> shoppingCart, @WebParam(name = "refuelDetail") List<ShopRefuelDetail> refuelDetail) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("sourceID", sourceID));
        inputParameters.add(new Pair<String, String>("sourceNumber", sourceNumber));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("srcTransactionID", srcTransactionID));
        inputParameters.add(new Pair<String, String>("amount", amount.toString()));
        inputParameters.add(new Pair<String, String>("productType", productType.value()));

        if (shoppingCart == null || shoppingCart.isEmpty()) {
            inputParameters.add(new Pair<String, String>("shoppingCart", "empty"));
        }
        else {
            inputParameters.add(new Pair<String, String>("shoppingCart", "not empty"));
        }

        if (refuelDetail == null || refuelDetail.isEmpty()) {
            inputParameters.add(new Pair<String, String>("refuelDetail", "empty"));
        }
        else {
            inputParameters.add(new Pair<String, String>("refuelDetail", "not empty"));
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMPTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        CreateMPTransactionMessageResponseOUT createMPTransactionMessageResponse = new CreateMPTransactionMessageResponseOUT();

        PostPaidCreateShopTransactionResponse postPaidCreateShopTransactionResponse = null;

        try {
            postPaidTransactionService = EJBHomeCache.getInstance().getPostPaidTransactionService();
            postPaidCreateShopTransactionResponse = postPaidTransactionService.createShopTransaction(requestID, source, sourceID, sourceNumber, stationID, srcTransactionID,
                    amount, productType.value(), shoppingCart, refuelDetail);

            createMPTransactionMessageResponse.setMessageCode(postPaidCreateShopTransactionResponse.getMessageCode());
            createMPTransactionMessageResponse.setStatusCode(postPaidCreateShopTransactionResponse.getStatusCode());
            createMPTransactionMessageResponse.setMpTransactionID(postPaidCreateShopTransactionResponse.getMpTransactionID());

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", createMPTransactionMessageResponse.getStatusCode().toString()));
            outputParameters.add(new Pair<String, String>("statusMessage", createMPTransactionMessageResponse.getMessageCode()));
            outputParameters.add(new Pair<String, String>("mpTransactionID", createMPTransactionMessageResponse.getMpTransactionID()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMPTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return createMPTransactionMessageResponse;

        }
        catch (InterfaceNotFoundException e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createMPTransaction", requestID, null, "InterfaceNotFoundException message: " + e.getMessage());

            createMPTransactionMessageResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", createMPTransactionMessageResponse.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMPTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return createMPTransactionMessageResponse;
        }
    }

    @WebMethod(operationName = "enableMPTransaction")
    @WebResult(name = "enableMPTransactionMessageResponse")
    public @XmlElement(required = true)
    EnableMPTransactionMessageResponseOUT enableMPTransaction(@XmlElement(required = true) @WebParam(name = "requestID") String requestID, @XmlElement(required = true) @WebParam(name = "mpTransactionID") String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "enableMPTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        EnableMPTransactionMessageResponseOUT enableMPTransactionMessageResponse = new EnableMPTransactionMessageResponseOUT();

        PostPaidEnableShopTransactionResponse postPaidEnableShopTransactionResponse = null;

        try {
            postPaidTransactionService = EJBHomeCache.getInstance().getPostPaidTransactionService();
            postPaidEnableShopTransactionResponse = postPaidTransactionService.enableShopTransaction(requestID, mpTransactionID);

            enableMPTransactionMessageResponse.setMessageCode(postPaidEnableShopTransactionResponse.getMessageCode());
            enableMPTransactionMessageResponse.setStatusCode(postPaidEnableShopTransactionResponse.getStatusCode());

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", enableMPTransactionMessageResponse.getStatusCode().toString()));
            outputParameters.add(new Pair<String, String>("statusMessage", enableMPTransactionMessageResponse.getMessageCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "enableMPTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return enableMPTransactionMessageResponse;

        }
        catch (InterfaceNotFoundException e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "enableMPTransaction", requestID, null, "InterfaceNotFoundException message: " + e.getMessage());

            enableMPTransactionMessageResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", enableMPTransactionMessageResponse.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "enableMPTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return enableMPTransactionMessageResponse;
        }
    }

    @WebMethod(operationName = "reverseMPTransaction")
    @WebResult(name = "reverseMPTransactionMessageResponse")
    public @XmlElement(required = true)
    ReverseMPTransactionMessageResponseOUT reverseMPTransaction(@XmlElement(required = true) @WebParam(name = "requestID") String requestID,
    // @WebParam(name="srcTransactionID") String srcTransactionID,
    @XmlElement(required = true) @WebParam(name = "mpTransactionID") String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reverseMPTransaction", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        ReverseMPTransactionMessageResponseOUT refundMPTransactionMessageResponse = new ReverseMPTransactionMessageResponseOUT();

        PostPaidReverseShopTransactionResponse postPaidReverseShopTransactionResponse = null;

        /*
         * Per test timeout reverse for(int i=0; i<15; i++) {
         * System.out.println("pre-refund start sleep " + i); try {
         * Thread.sleep(5000); } catch(InterruptedException ex) {
         * Thread.currentThread().interrupt(); }
         * System.out.println("pre-refund end sleep " + i); }
         */

        try {
            postPaidTransactionService = EJBHomeCache.getInstance().getPostPaidTransactionService();
            postPaidReverseShopTransactionResponse = postPaidTransactionService.reverseShopTransaction(requestID, mpTransactionID);

            refundMPTransactionMessageResponse.setMessageCode(postPaidReverseShopTransactionResponse.getMessageCode());
            refundMPTransactionMessageResponse.setStatusCode(postPaidReverseShopTransactionResponse.getStatusCode());

            if (postPaidReverseShopTransactionResponse.getStatusCode() == ResponseHelper.PP_TRANSACTION_REVERSE_SUCCESS) {
                refundMPTransactionMessageResponse.setTransactionAuthorized("OK");
                // refundMPTransactionMessageResponse.setPaymentTransactionResult(postPaidReverseShopTransactionResponse.getPaymentTransactionResult());
            }
            else {
                refundMPTransactionMessageResponse.setTransactionAuthorized("KO");
                // refundMPTransactionMessageResponse.setPaymentTransactionResult(postPaidReverseShopTransactionResponse.getPaymentTransactionResult());
            }

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", refundMPTransactionMessageResponse.getStatusCode().toString()));
            outputParameters.add(new Pair<String, String>("statusMessage", refundMPTransactionMessageResponse.getMessageCode()));
            outputParameters.add(new Pair<String, String>("transactionAuthorized", refundMPTransactionMessageResponse.getTransactionAuthorized()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reverseMPTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return refundMPTransactionMessageResponse;

        }
        catch (InterfaceNotFoundException e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "reverseMPTransaction", requestID, null, "InterfaceNotFoundException message: " + e.getMessage());

            refundMPTransactionMessageResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REVERSE_FAILURE);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", refundMPTransactionMessageResponse.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "reverseMPTransaction", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return refundMPTransactionMessageResponse;
        }
    }

    @WebMethod(operationName = "getMPTransactionStatus")
    @WebResult(name = "getMPTransactionStatusMessageResponse")
    public @XmlElement(required = true)
    GetMPTransactionStatusMessageResponseOUT getMPTransactionStatus(@XmlElement(required = true) @WebParam(name = "requestID") String requestID,
    // @WebParam(name="srcTransactionID") String srcTransactionID,
    @XmlElement(required = true) @WebParam(name = "mpTransactionID") String mpTransactionID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestID", requestID));
        inputParameters.add(new Pair<String, String>("mpTransactionID", mpTransactionID));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMPTransactionStatus", requestID, "opening", ActivityLog.createLogMessage(inputParameters));

        GetMPTransactionStatusMessageResponseOUT getMPTransactionStatusMessageResponse = new GetMPTransactionStatusMessageResponseOUT();

        PostPaidGetShopTransactionResponse postPaidGetShopTransactionResponse = null;

        try {
            postPaidTransactionService = EJBHomeCache.getInstance().getPostPaidTransactionService();
            postPaidGetShopTransactionResponse = postPaidTransactionService.getShopTransaction(requestID, mpTransactionID);

            getMPTransactionStatusMessageResponse.setMessageCode(postPaidGetShopTransactionResponse.getMessageCode());
            getMPTransactionStatusMessageResponse.setStatusCode(postPaidGetShopTransactionResponse.getStatusCode());

            com.techedge.mp.core.business.interfaces.postpaid.PaymentTransactionStatus paymentTransactionStatusCore = postPaidGetShopTransactionResponse
                    .getPaymentTransactionStatus();
            getMPTransactionStatusMessageResponse.setTransactionResult(paymentTransactionStatusCore.getMpTransactionStatus());

            PaymentTransactionStatus paymentTransactionStatus = new PaymentTransactionStatus();
            paymentTransactionStatus.setAcquirerID(paymentTransactionStatusCore.getAcquirerID());
            paymentTransactionStatus.setAuthorizationCode(paymentTransactionStatusCore.getAuthorizationCode());
            paymentTransactionStatus.setBankTransactionID(paymentTransactionStatusCore.getBankTransactionID());
            paymentTransactionStatus.setCurrency(paymentTransactionStatusCore.getCurrency());
            paymentTransactionStatus.setPaymentMode(paymentTransactionStatusCore.getPaymentMode());
            // paymentTransactionStatus.setMpTransactionStatus(paymentTransactionStatusCore.getMpTransactionStatus());
            paymentTransactionStatus.setShopLogin(paymentTransactionStatusCore.getShopLogin());
            paymentTransactionStatus.setShopTransactionID(paymentTransactionStatusCore.getShopTransactionID());

            Set<PaymentTransactionEvent> paymentTransactionEventCoreList = paymentTransactionStatusCore.getEventList();
            com.techedge.mp.forecourt.integration.shop.interfaces.PaymentTransactionEvent paymentTransactionEvent;
            for (PaymentTransactionEvent paymentTransactionEventCore : paymentTransactionEventCoreList) {
                paymentTransactionEvent = new com.techedge.mp.forecourt.integration.shop.interfaces.PaymentTransactionEvent();
                paymentTransactionEvent.setSequence(paymentTransactionEventCore.getSequence());
                paymentTransactionEvent.setAmount(paymentTransactionEventCore.getAmount());
                paymentTransactionEvent.setErrorCode(paymentTransactionEventCore.getErrorCode());
                paymentTransactionEvent.setErrorDescription(paymentTransactionEventCore.getErrorDescription());
                paymentTransactionEvent.setTransactionResult(paymentTransactionEventCore.getTransactionResult());
                paymentTransactionEvent.setType(paymentTransactionEventCore.getType());

                paymentTransactionStatus.getEventList().add(paymentTransactionEvent);

            }

            getMPTransactionStatusMessageResponse.setPaymentTransactionStatus(paymentTransactionStatus);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", getMPTransactionStatusMessageResponse.getStatusCode().toString()));
            outputParameters.add(new Pair<String, String>("statusMessage", getMPTransactionStatusMessageResponse.getMessageCode()));
            outputParameters.add(new Pair<String, String>("transactionResult", getMPTransactionStatusMessageResponse.getTransactionResult()));

            if (getMPTransactionStatusMessageResponse.getPaymentTransactionStatus() == null) {

                outputParameters.add(new Pair<String, String>("paymentTransactionStatus", null));
            }
            else {

                outputParameters.add(new Pair<String, String>("paymentTransactionStatus", "not null"));
            }

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMPTransactionStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return getMPTransactionStatusMessageResponse;

        }
        catch (InterfaceNotFoundException e) {

            e.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getMPTransactionStatus", requestID, null, "InterfaceNotFoundException message: " + e.getMessage());

            getMPTransactionStatusMessageResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GET_NOT_RECOGNIZED);

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("statusCode", getMPTransactionStatusMessageResponse.getStatusCode()));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getMPTransactionStatus", requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            return getMPTransactionStatusMessageResponse;
        }
    }
}
