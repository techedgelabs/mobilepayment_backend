package com.techedgegroup.mprefuelingprocess;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceFeature;
import javax.xml.ws.Service;

/**
 * This class was generated by Apache CXF 2.7.7.redhat-1
 * 2014-10-30T17:49:46.305+01:00
 * Generated source version: 2.7.7.redhat-1
 * 
 */
@WebServiceClient(name = "MPRefuelingProcess", 
                  wsdlLocation = "http://10.231.241.11:8080/MPRefuelingProcess/MPRefuelingProcess?wsdl",
                  targetNamespace = "http://www.techedgegroup.com/MPRefuelingProcess/") 
public class MPRefuelingProcess_Service extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://www.techedgegroup.com/MPRefuelingProcess/", "MPRefuelingProcess");
    public final static QName MPRefuelingProcessSOAP = new QName("http://www.techedgegroup.com/MPRefuelingProcess/", "MPRefuelingProcessSOAP");
    static {
        URL url = null;
        try {
            url = new URL("http://10.231.241.11:8080/MPRefuelingProcess/MPRefuelingProcess?wsdl");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(MPRefuelingProcess_Service.class.getName())
                .log(java.util.logging.Level.INFO, 
                     "Can not initialize the default wsdl from {0}", "http://10.231.241.11:8080/MPRefuelingProcess/MPRefuelingProcess?wsdl");
        }
        WSDL_LOCATION = url;
    }

    public MPRefuelingProcess_Service(URL wsdlLocation) {
        super(wsdlLocation, SERVICE);
    }

    public MPRefuelingProcess_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public MPRefuelingProcess_Service() {
        super(WSDL_LOCATION, SERVICE);
    }
    
    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public MPRefuelingProcess_Service(WebServiceFeature ... features) {
        super(WSDL_LOCATION, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public MPRefuelingProcess_Service(URL wsdlLocation, WebServiceFeature ... features) {
        super(wsdlLocation, SERVICE, features);
    }

    //This constructor requires JAX-WS API 2.2. You will need to endorse the 2.2
    //API jar or re-run wsdl2java with "-frontend jaxws21" to generate JAX-WS 2.1
    //compliant code instead.
    public MPRefuelingProcess_Service(URL wsdlLocation, QName serviceName, WebServiceFeature ... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     *
     * @return
     *     returns MPRefuelingProcess
     */
    @WebEndpoint(name = "MPRefuelingProcessSOAP")
    public MPRefuelingProcess getMPRefuelingProcessSOAP() {
        return super.getPort(MPRefuelingProcessSOAP, MPRefuelingProcess.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns MPRefuelingProcess
     */
    @WebEndpoint(name = "MPRefuelingProcessSOAP")
    public MPRefuelingProcess getMPRefuelingProcessSOAP(WebServiceFeature... features) {
        return super.getPort(MPRefuelingProcessSOAP, MPRefuelingProcess.class, features);
    }

}
