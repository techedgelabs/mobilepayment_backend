
package com.techedgegroup.mprefuelingprocess;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="stationID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="pumpId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="acquirerId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="bankDetail" type="{http://www.techedgegroup.com/MPRefuelingProcess/}BankDetail"/>
 *         &lt;element name="refuelDetail" type="{http://www.techedgegroup.com/MPRefuelingProcess/}RefuelDetail" minOccurs="0"/>
 *         &lt;element name="extensionFields" type="{http://www.techedgegroup.com/MPRefuelingProcess/}ExtensionFieldList" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "transactionID",
    "stationID",
    "pumpId",
    "amount",
    "acquirerID",
    "bankDetail",
    "refuelDetail",
    "extensionFields"
})
@XmlRootElement(name = "StartRefuelingProcessRequest")
public class StartRefuelingProcessRequest {

    @XmlElement(required = true)
    protected String transactionID;
    @XmlElement(required = true)
    protected String stationID;
    @XmlElement(required = true)
    protected String pumpId;
    protected double amount;
    protected String acquirerID;
    @XmlElement(required = true)
    protected BankDetail bankDetail;
    protected RefuelDetail refuelDetail;
    protected List<ExtensionFieldList> extensionFields;

    /**
     * Gets the value of the transactionID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionID() {
        return transactionID;
    }

    /**
     * Sets the value of the transactionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionID(String value) {
        this.transactionID = value;
    }

    /**
     * Gets the value of the stationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStationID() {
        return stationID;
    }

    /**
     * Sets the value of the stationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStationID(String value) {
        this.stationID = value;
    }

    /**
     * Gets the value of the pumpId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPumpId() {
        return pumpId;
    }

    /**
     * Sets the value of the pumpId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPumpId(String value) {
        this.pumpId = value;
    }

    /**
     * Gets the value of the amount property.
     * 
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAmount(double value) {
        this.amount = value;
    }
    
    /**
     * Gets the value of the amount property.
     * 
     */
    public String getAcquirerID() {
        return acquirerID;
    }

    /**
     * Sets the value of the amount property.
     * 
     */
    public void setAcquirerID(String value) {
        this.acquirerID = value;
    }

    /**
     * Gets the value of the bankDetail property.
     * 
     * @return
     *     possible object is
     *     {@link BankDetail }
     *     
     */
    public BankDetail getBankDetail() {
        return bankDetail;
    }

    /**
     * Sets the value of the bankDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankDetail }
     *     
     */
    public void setBankDetail(BankDetail value) {
        this.bankDetail = value;
    }

    /**
     * Gets the value of the refuelDetail property.
     * 
     * @return
     *     possible object is
     *     {@link RefuelDetail }
     *     
     */
    public RefuelDetail getRefuelDetail() {
        return refuelDetail;
    }

    /**
     * Sets the value of the refuelDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link RefuelDetail }
     *     
     */
    public void setRefuelDetail(RefuelDetail value) {
        this.refuelDetail = value;
    }

    /**
     * Gets the value of the extensionFields property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the extensionFields property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getExtensionFields().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ExtensionFieldList }
     * 
     * 
     */
    public List<ExtensionFieldList> getExtensionFields() {
        if (extensionFields == null) {
            extensionFields = new ArrayList<ExtensionFieldList>();
        }
        return this.extensionFields;
    }

}
