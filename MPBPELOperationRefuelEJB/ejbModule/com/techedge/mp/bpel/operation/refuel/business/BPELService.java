package com.techedge.mp.bpel.operation.refuel.business;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedgegroup.mprefuelingprocess.BankDetail;
import com.techedgegroup.mprefuelingprocess.MPRefuelingProcess;
import com.techedgegroup.mprefuelingprocess.MPRefuelingProcess_Service;
import com.techedgegroup.mprefuelingprocess.RefuelDetail;
import com.techedgegroup.mprefuelingprocess.StartRefuelingProcessRequest;
import com.techedgegroup.mprefuelingprocess.StartRefuelingProcessResponse;

/**
 * Session Bean implementation class BPELService
 */
@Stateless
@LocalBean
public class BPELService implements BPELServiceRemote, BPELServiceLocal {

	private final static String PARAM_WSDL_PREFIX        = "WSDL_PREFIX";
	private final static String PARAM_REFUEL_WSDL_SUFFIX = "REFUEL_WSDL_SUFFIX";
	private final static String PARAM_SERVER_NAME        = "SERVER_NAME";
	
	private ParametersServiceRemote  parametersService  = null;
	private LoggerServiceRemote      loggerService      = null;
	
	private URL url = null;
    
    /**
     * Default constructor. 
     */
    public BPELService() {
    	
    	try {
			this.loggerService = EJBHomeCache.getInstance().getLoggerService();
			this.parametersService = EJBHomeCache.getInstance().getParametersService();
		}
    	catch (InterfaceNotFoundException e) {
			
			e.printStackTrace();
			
			//throw new BPELException("EJB interface not found: " + e.getMessage());
		}
		
    	String wsdlString = "";
		try {
			String wsdlStringPrefix = parametersService.getParamValue(BPELService.PARAM_WSDL_PREFIX);
			String wsdlStringSuffix = parametersService.getParamValue(BPELService.PARAM_REFUEL_WSDL_SUFFIX);
			String serverName       = parametersService.getParamValue(BPELService.PARAM_SERVER_NAME);
			wsdlString = wsdlStringPrefix + serverName + wsdlStringSuffix;
		}
		catch (ParameterNotFoundException e) {

			e.printStackTrace();
			
			//throw new BPELException("Parameter " + Info.PARAM_FORECOURT_WSDL + " not found: " + e.getMessage());
		}
		
    	try {
			this.url = new URL(wsdlString);
		}
    	catch (MalformedURLException e) {
			
			e.printStackTrace();
			
			//throw new BPELException("Malformed URL: " + e.getMessage());
		}
    }
    
    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }
    
    
    /*
    @Override
	public String startRefuel(String transactionID) {
		
    	StartRefuelRequest startRefuelRequest = new StartRefuelRequest();
    	
    	startRefuelRequest.setTransactionID(transactionID);
    	
    	try {
    		
    		System.clearProperty("http.proxyHost");
	    	System.clearProperty("http.proxyPort");
	    	
    		MPRefuelingProcess_Service service = new MPRefuelingProcess_Service(this.url);
            MPRefuelingProcess port = service.getMPRefuelingProcessSOAP();
        	
        	StartRefuelResponse startRefuelResponse = port.startRefuel(startRefuelRequest);
        	
        	return startRefuelResponse.getStatusCode();
    	}
    	catch (Exception ex) {
    		
    		ex.printStackTrace();
    		
    		return null;
    	}
	}
    
	
	@Override
	public String endRefuel(String transactionID, Double amount) {
		
		EndRefuelRequest endRefuelRequest = new EndRefuelRequest();
		
		endRefuelRequest.setTransactionID(transactionID);
		endRefuelRequest.setAmount(amount);
		
		try {
			
			System.clearProperty("http.proxyHost");
	    	System.clearProperty("http.proxyPort");
	    	
			MPRefuelingProcess_Service service = new MPRefuelingProcess_Service(this.url);
            MPRefuelingProcess port = service.getMPRefuelingProcessSOAP();
            
            EndRefuelResponse endRefuelResponse = port.endRefuel(endRefuelRequest);
    		
    		return endRefuelResponse.getStatusCode();
		}
		catch (Exception ex) {
    		
    		ex.printStackTrace();
    		
    		return null;
    	}
		
	}
	*/
	
	@Override
	public String startRefuelingProcess(
			String transactionID,
			String stationID,
			String pumpID,
			Double amount,
			String acquirerID,
			String shopLogin,
			String uicCode,
			String shopTransactionID,
			String tokenValue,
			String fuelType,
			String productDescription,
			String productID) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        
        String amountString = (amount != null) ? amount.toString() : "";

        inputParameters.add(new Pair<String, String>("transactionID", transactionID));
        inputParameters.add(new Pair<String, String>("stationID", stationID));
        inputParameters.add(new Pair<String, String>("pumpID", pumpID));
        inputParameters.add(new Pair<String, String>("amount", amountString));
        inputParameters.add(new Pair<String, String>("acquirerID", acquirerID));
        inputParameters.add(new Pair<String, String>("shopLogin", shopLogin));
        inputParameters.add(new Pair<String, String>("uicCode", uicCode));
        inputParameters.add(new Pair<String, String>("shopTransactionID", shopTransactionID));
        inputParameters.add(new Pair<String, String>("tokenValue", tokenValue));
        inputParameters.add(new Pair<String, String>("fuelType", fuelType));
        inputParameters.add(new Pair<String, String>("productDescription", productDescription));
        inputParameters.add(new Pair<String, String>("productID", productID));


        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "startRefuelingProcess", null, "opening", ActivityLog.createLogMessage(inputParameters));
	    
		StartRefuelingProcessRequest startRefuelingProcessRequest = new StartRefuelingProcessRequest();
		
		startRefuelingProcessRequest.setTransactionID(transactionID);
		startRefuelingProcessRequest.setStationID(stationID);
		startRefuelingProcessRequest.setPumpId(pumpID);
		startRefuelingProcessRequest.setAmount(amount);
		startRefuelingProcessRequest.setAcquirerID(acquirerID);
        
        BankDetail bankDetail = new BankDetail();
        bankDetail.setAmount(amount);
        bankDetail.setShopLogin(shopLogin);
        bankDetail.setUicCode(uicCode);
        bankDetail.setShopTransactionId(shopTransactionID);
        bankDetail.setTokenValue(tokenValue);
        
        RefuelDetail refuelDetail = new RefuelDetail();
        refuelDetail.setFuelType(fuelType);
		refuelDetail.setProductDescription(productDescription);
		refuelDetail.setProductID(productID);
        
        startRefuelingProcessRequest.setBankDetail(bankDetail);
        startRefuelingProcessRequest.setRefuelDetail(refuelDetail);
        
        try {
			
        	new Proxy().unsetHttp();
	    	
			MPRefuelingProcess_Service service = new MPRefuelingProcess_Service(this.url);
            MPRefuelingProcess port = service.getMPRefuelingProcessSOAP();
        
			StartRefuelingProcessResponse startRefuelingProcessResponse = port.startRefuelingProcess(startRefuelingProcessRequest);
			
	        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
	        outputParameters.add(new Pair<String, String>("statusCode", startRefuelingProcessResponse.getStatusCode()));
	        outputParameters.add(new Pair<String, String>("statusMessage", startRefuelingProcessResponse.getMessageCode()));

	        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "startRefuelingProcess", null, "closing", ActivityLog.createLogMessage(outputParameters));
			
			return startRefuelingProcessResponse.getStatusCode();
        }
        catch (Exception ex) {
    		
    		ex.printStackTrace();
    		
    		return null;
    	}
        
	}

}
