package com.techedge.mp.crm.adapter.service.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processEventMessageRequest", propOrder = { "operationId", "requestId", "fiscalCode", "promoCode", "voucherAmount" })
public class ProcessEventMessageRequest {

    @XmlElement(required = true, nillable = false, name = "OPERATION_ID")
    private String operationId;
    @XmlElement(required = true, nillable = false, name = "REQUEST_ID")
    private String requestId;
    @XmlElement(required = true, nillable = false, name = "FISCALCODE")
    private String fiscalCode;
    @XmlElement(required = true, nillable = false, name = "PROMO_CODE")
    private String promoCode;
    @XmlElement(required = true, nillable = false, name = "VOUCHER_AMOUNT")
    private Double voucherAmount;

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public Double getVoucherAmount() {
        return voucherAmount;
    }

    public void setVoucherAmount(Double voucherAmount) {
        this.voucherAmount = voucherAmount;
    }

}
