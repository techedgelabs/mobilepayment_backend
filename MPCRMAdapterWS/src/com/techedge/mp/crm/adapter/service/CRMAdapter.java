package com.techedge.mp.crm.adapter.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.InputMismatchException;
import java.util.Properties;
import java.util.Set;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.crm.ProcessEventResult;
import com.techedge.mp.core.business.interfaces.crm.StatusCodeEnum;
import com.techedge.mp.core.business.interfaces.crm.UpdateContactKeyResult;
import com.techedge.mp.crm.adapter.service.entities.ProcessEventMessageRequest;
import com.techedge.mp.crm.adapter.service.entities.ProcessEventMessageResponse;
import com.techedge.mp.crm.adapter.service.entities.UpdateContactKeyMessageRequest;
import com.techedge.mp.crm.adapter.service.entities.UpdateContactKeyMessageResponse;

@WebService()
public class CRMAdapter {

    private Properties          prop          = new Properties();

    private LoggerServiceRemote loggerService = null;

    private CRMServiceRemote    crmService    = null;

    public CRMAdapter() {
        initService();
    }

    private void initService() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("response_service.properties");

        try {
            prop.load(inputStream);
            if (inputStream == null) {
                throw new FileNotFoundException("property file response_service.properties not found in the classpath");
            }
        }
        catch (IOException e) {
            System.err.println("File properties is not available: " + e.getMessage());
        }
        
        try {
            this.crmService = EJBHomeCache.getInstance().getCrmService();
        }
        catch (Exception ex) {
            System.err.println("crmService is not available: " + ex.getMessage());
        }
    }

    private void log(ErrorLevel level, String methodName, String groupId, String phaseId, String message) {
        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, this.getClass().getSimpleName(), methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {
            System.err.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    @WebMethod(operationName = "processEvent")
    @WebResult(name = "processEventResponse")
    public @XmlElement(required = true)
    ProcessEventMessageResponse processEvent(@XmlElement(required = true) @WebParam(name = "processEventRequest") ProcessEventMessageRequest processEventRequest) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationId", processEventRequest.getOperationId()));
        inputParameters.add(new Pair<String, String>("requestId", processEventRequest.getRequestId()));
        inputParameters.add(new Pair<String, String>("fiscalCode", processEventRequest.getFiscalCode()));
        inputParameters.add(new Pair<String, String>("promoCode", processEventRequest.getPromoCode()));
        if (processEventRequest.getVoucherAmount() != null) {
            inputParameters.add(new Pair<String, String>("voucherAmount", processEventRequest.getVoucherAmount().toString()));
        }
        else {
            inputParameters.add(new Pair<String, String>("voucherAmount", "null"));
        }

        log(ErrorLevel.DEBUG, "processEvent", processEventRequest.getOperationId(), "opening", ActivityLog.createLogMessage(inputParameters));

        ProcessEventMessageResponse processEventResponse = new ProcessEventMessageResponse();
        
        try {
        	
        	// Validazione campi di input
        	validateProcessEventInput(processEventRequest);
            
            String operationId   = processEventRequest.getOperationId();
            String requestId     = processEventRequest.getRequestId();
            String fiscalCode    = processEventRequest.getFiscalCode();
            String promoCode     = processEventRequest.getPromoCode();
            Double voucherAmount = processEventRequest.getVoucherAmount();
            
            ProcessEventResult processEventResult = this.crmService.processEvent(operationId, requestId, fiscalCode, promoCode, voucherAmount);
            StatusCodeEnum statusCodeEnum = processEventResult.getStatusCode();
            String operationIdOut         = processEventResult.getOperationId();

            processEventResponse.setStatusCode(statusCodeEnum.name());
            processEventResponse.setOperationId(operationId);
        }
        catch (InputMismatchException ex) {
        	//ex.printStackTrace();
            log(ErrorLevel.DEBUG, "processEvent", processEventRequest.getOperationId(), "closing", ex.getMessage());
            String operationId = processEventRequest.getOperationId();
            processEventResponse.setStatusCode(StatusCodeEnum.INVALID_INPUT.name());
            processEventResponse.setOperationId(operationId);
        }
        catch (Exception ex) {
        	//ex.printStackTrace();
            log(ErrorLevel.DEBUG, "processEvent", processEventRequest.getOperationId(), "closing", ex.getMessage());
            String operationId = processEventRequest.getOperationId();
            processEventResponse.setStatusCode(StatusCodeEnum.SYSTEM_ERROR.name());
            processEventResponse.setOperationId(operationId);
        }

        processEventResponse.setMessageCode(prop.getProperty(processEventResponse.getStatusCode()));

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", processEventResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", processEventResponse.getMessageCode()));
        outputParameters.add(new Pair<String, String>("operationId", processEventResponse.getOperationId()));

        log(ErrorLevel.INFO, "processEvent", processEventRequest.getOperationId(), "closing", ActivityLog.createLogMessage(outputParameters));

        return processEventResponse;
    }
    
    
    @WebMethod(operationName = "updateContactKey")
    @WebResult(name = "updateContactKeyResponse")
    public @XmlElement(required = true)
    UpdateContactKeyMessageResponse updateContactKey(@XmlElement(required = true) @WebParam(name = "updateContactKeyRequest") UpdateContactKeyMessageRequest updateContactKeyRequest) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationId", updateContactKeyRequest.getOperationId()));
        inputParameters.add(new Pair<String, String>("requestId", updateContactKeyRequest.getRequestId()));
        inputParameters.add(new Pair<String, String>("fiscalCode", updateContactKeyRequest.getFiscalCode()));
        inputParameters.add(new Pair<String, String>("contactKey", updateContactKeyRequest.getContactKey()));

        log(ErrorLevel.DEBUG, "updateContactKey", updateContactKeyRequest.getOperationId(), "opening", ActivityLog.createLogMessage(inputParameters));

        UpdateContactKeyMessageResponse updateContactKeyResponse = new UpdateContactKeyMessageResponse();
        
        try {
            
            // Validazione campi di input
            validateUpdateContactKeyInput(updateContactKeyRequest);
            
            String operationId   = updateContactKeyRequest.getOperationId();
            String requestId     = updateContactKeyRequest.getRequestId();
            String fiscalCode    = updateContactKeyRequest.getFiscalCode();
            String contactKey    = updateContactKeyRequest.getContactKey();
            
            UpdateContactKeyResult updateContactKeyResult = this.crmService.updateContactKey(operationId, requestId, fiscalCode, contactKey);
            StatusCodeEnum statusCodeEnum = updateContactKeyResult.getStatusCode();

            updateContactKeyResponse.setStatusCode(statusCodeEnum.name());
            updateContactKeyResponse.setOperationId(operationId);
        }
        catch (InputMismatchException ex) {
            //ex.printStackTrace();
            log(ErrorLevel.DEBUG, "updateContactKey", updateContactKeyRequest.getOperationId(), "closing", ex.getMessage());
            String operationId = updateContactKeyRequest.getOperationId();
            updateContactKeyResponse.setStatusCode(StatusCodeEnum.INVALID_INPUT.name());
            updateContactKeyResponse.setOperationId(operationId);
        }
        catch (Exception ex) {
            //ex.printStackTrace();
            log(ErrorLevel.DEBUG, "updateContactKey", updateContactKeyRequest.getOperationId(), "closing", ex.getMessage());
            String operationId = updateContactKeyRequest.getOperationId();
            updateContactKeyResponse.setStatusCode(StatusCodeEnum.SYSTEM_ERROR.name());
            updateContactKeyResponse.setOperationId(operationId);
        }

        updateContactKeyResponse.setMessageCode(prop.getProperty(updateContactKeyResponse.getStatusCode()));

        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", updateContactKeyResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", updateContactKeyResponse.getMessageCode()));
        outputParameters.add(new Pair<String, String>("operationId", updateContactKeyResponse.getOperationId()));

        log(ErrorLevel.INFO, "updateContactKey", updateContactKeyRequest.getOperationId(), "closing", ActivityLog.createLogMessage(outputParameters));

        return updateContactKeyResponse;
    }
    
    private void validateProcessEventInput(ProcessEventMessageRequest processEventRequest) throws InputMismatchException {
        
    	//OPERATION_ID String length 13
    	if (processEventRequest.getOperationId().length()!=13)
    		throw new InputMismatchException("OperationId wrong length :"+processEventRequest.getOperationId().length());
    	
    	//REQUEST_ID String 32
    	if (processEventRequest.getRequestId().length()!=32)
    		throw new InputMismatchException("RequestId wrong length :"+processEventRequest.getRequestId().length());
    	
    	//FISCALCODE String length 16
    	if (processEventRequest.getFiscalCode().length()!=16)
    		throw new InputMismatchException("FiscalCode wrong length :"+processEventRequest.getFiscalCode().length());
    	
    	//PROMO_CODE String length 10
    	if (processEventRequest.getPromoCode().length()>10)
    		throw new InputMismatchException("PromoCode wrong length :"+processEventRequest.getPromoCode().length());
    	
    	//VOUCHER_AMOUNT Max. 7 interi e 2 decimali separati dal punto
    	BigDecimal bigDecimal = new BigDecimal(String.valueOf(processEventRequest.getVoucherAmount()));
    	int intValue = bigDecimal.intValue();
    	BigDecimal decimalValue=bigDecimal.subtract(new BigDecimal(intValue));
    	//System.out.println("Double Number: " + bigDecimal.toPlainString());
    	//System.out.println("Integer Part: " + intValue);
    	//System.out.println("Decimal Part: " + decimalValue.toPlainString());

    	if (intValue>9999999 || decimalValue.toPlainString().length()>4)
    		throw new InputMismatchException("VoucherAmount restrict violate:"+processEventRequest.getVoucherAmount());
    }
    
    private void validateUpdateContactKeyInput(UpdateContactKeyMessageRequest updateContactKeyRequest) throws InputMismatchException {
        
        //OPERATION_ID String length 13
        if (updateContactKeyRequest.getOperationId().length()!=13)
            throw new InputMismatchException("OperationId wrong length :" + updateContactKeyRequest.getOperationId().length());
        
        //REQUEST_ID String 32
        if (updateContactKeyRequest.getRequestId().length()!=32)
            throw new InputMismatchException("RequestId wrong length :" + updateContactKeyRequest.getRequestId().length());
        
        //FISCALCODE String length 16
        if (updateContactKeyRequest.getFiscalCode().length()!=16)
            throw new InputMismatchException("FiscalCode wrong length :" + updateContactKeyRequest.getFiscalCode().length());
        
        //CONTACT_KEY String length 10
        if (updateContactKeyRequest.getContactKey().length()>18)
            throw new InputMismatchException("PromoCode wrong length :" + updateContactKeyRequest.getContactKey().length());
        
    }
}
