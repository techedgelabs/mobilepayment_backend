package com.techedge.mp.crm.adapter.service.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateContactKeyMessageResponse", propOrder = { "statusCode", "messageCode", "operationId" })
public class UpdateContactKeyMessageResponse {

    @XmlElement(required = true, name = "STATUS_CODE")
    protected String statusCode;
    @XmlElement(required = true, name = "MESSAGE_CODE")
    protected String messageCode;
    @XmlElement(required = true, name = "OPERATION_ID")
    protected String operationId;

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

}
