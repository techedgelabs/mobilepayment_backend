package com.techedge.mp.crm.adapter.service.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "updateContactKeyMessageRequest", propOrder = { "operationId", "requestId", "fiscalCode", "contactKey" })
public class UpdateContactKeyMessageRequest {

    @XmlElement(required = true, nillable = false, name = "OPERATION_ID")
    private String operationId;
    @XmlElement(required = true, nillable = false, name = "REQUEST_ID")
    private String requestId;
    @XmlElement(required = true, nillable = false, name = "FISCALCODE")
    private String fiscalCode;
    @XmlElement(required = true, nillable = false, name = "CONTACT_KEY")
    private String contactKey;

    public String getOperationId() {
        return operationId;
    }

    public void setOperationId(String operationId) {
        this.operationId = operationId;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getContactKey() {
        return contactKey;
    }

    public void setContactKey(String contactKey) {
        this.contactKey = contactKey;
    }

}
