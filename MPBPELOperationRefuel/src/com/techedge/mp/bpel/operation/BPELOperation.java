package com.techedge.mp.bpel.operation;

import java.util.Hashtable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.xml.bind.annotation.XmlElement;

import com.techedge.mp.bpel.operation.exception.BPELException;
import com.techedge.mp.core.business.TransactionServiceRemote;

@WebService()
public class BPELOperation {
	
	private final String PREPAYMENT = "USER_CANCEL_REFUEL_PREPAYMENT";
	private final String POSTPAYMENT = "USER_CANCEL_REFUEL_POSTPAYMENT";
	
	private TransactionServiceRemote transactionService;
	
	public BPELOperation() {
		
		this.transactionService = getRemoteObject();
	}

	@WebMethod(operationName = "checkUndo")
	public boolean checkUndo(@WebParam(name="transactionID") @XmlElement(required=true) String transactionID) throws BPELException {
		
		return transactionService.checkUndoOperation(transactionID);
	}
	
	@WebMethod(operationName = "updateTransaction")
	public boolean updateTransaction(@WebParam(name="transactionID") @XmlElement(required=true) String transactionID, @WebParam(name="transactionStep") @XmlElement(required=true) String transactionStep) throws BPELException {
		
		// effettuare il controllo sul transactionStep in modo da fare i giusti controlli
		if(transactionStep.equals(PREPAYMENT)) {
			
			
			
			return true;
			
		}
		
		if(transactionStep.equals(POSTPAYMENT)) {
			
			
			
			return true;
			
		}
		
	    return false;
	    
	}
	
	
	private TransactionServiceRemote getRemoteObject() {
		
		TransactionServiceRemote transactionService = null;
		
		final Hashtable jndiProperties = new Hashtable();
		
		jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		
		try {
			
            final Context context = new InitialContext(jndiProperties);

            final String jndi = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/TransactionService!com.techedge.mp.core.business.TransactionServiceRemote";

            transactionService = (TransactionServiceRemote)context.lookup(jndi);
            
            return transactionService;
            
        } catch (NamingException ex) {
        	//ex.printStackTrace();
        	//logger.log(Level.INFO, "Error in getRemoteObject()");
			return null;
        }
	}
	
}
