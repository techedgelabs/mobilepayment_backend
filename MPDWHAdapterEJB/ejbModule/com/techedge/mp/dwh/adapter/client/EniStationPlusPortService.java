/**
 * EniStationPlusPortService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.client;

public interface EniStationPlusPortService extends javax.xml.rpc.Service {
    public java.lang.String getEniStationPlusPortSoap11Address();

    public EniStationPlusPort getEniStationPlusPortSoap11() throws javax.xml.rpc.ServiceException;

    public EniStationPlusPort getEniStationPlusPortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
