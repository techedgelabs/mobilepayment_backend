package com.techedge.mp.dwh.adapter.client.exceptions;


public class DWHWsException extends Exception {
    
    /**
     * 
     */
    private static final long serialVersionUID = -593042575365657066L;

    public DWHWsException(String message, Throwable cause) {
        super(message, cause);
    }

    public DWHWsException(String message) {
        this(message, null);
    }



}
