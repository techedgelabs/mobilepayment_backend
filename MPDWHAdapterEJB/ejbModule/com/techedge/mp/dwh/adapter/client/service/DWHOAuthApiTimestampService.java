package com.techedge.mp.dwh.adapter.client.service;

import org.scribe.services.TimestampServiceImpl;

public class DWHOAuthApiTimestampService extends TimestampServiceImpl {

    private String timestamp;
    private String nonce;

    public DWHOAuthApiTimestampService(String timestamp, String nonce) {
        super();
        this.timestamp = timestamp;
        this.nonce = nonce;
    }

    public DWHOAuthApiTimestampService() {
        this(null, null);
    }

    @Override
    public String getTimestampInSeconds() {
        if (timestamp != null && !timestamp.equals("")) {
            return timestamp;
        }
        else {
            return super.getTimestampInSeconds();
        }
    }

    @Override
    public String getNonce() {
        if (nonce != null && !nonce.equals("")) {
            return nonce;
        }
        else {
            return super.getNonce();
        }
    }
}
