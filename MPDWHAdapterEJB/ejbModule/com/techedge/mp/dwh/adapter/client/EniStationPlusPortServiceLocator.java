/**
 * EniStationPlusPortServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.client;

import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.HandlerRegistry;

import com.techedge.mp.dwh.adapter.client.validator.OAuthValidationHandler;

public class EniStationPlusPortServiceLocator extends org.apache.axis.client.Service implements EniStationPlusPortService {

    public EniStationPlusPortServiceLocator() {
    }


    public EniStationPlusPortServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public EniStationPlusPortServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    @Override
    public HandlerRegistry getHandlerRegistry() {
        HandlerRegistry registry = super.getHandlerRegistry();
        QName portName = new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "EniStationPlusPortSoap11");
        List handlerChain = registry.getHandlerChain(portName);

        HandlerInfo hi = new HandlerInfo();
        hi.setHandlerClass(OAuthValidationHandler.class);
        handlerChain.add(hi);        
 
       return registry;
    }
    
    // Use to get a proxy class for EniStationPlusPortSoap11
    private java.lang.String EniStationPlusPortSoap11_address = "http://localhost:8080/ws";

    public java.lang.String getEniStationPlusPortSoap11Address() {
        return EniStationPlusPortSoap11_address;
    }

    // The WSDD com.techedge.mp.dwh.adapter.client.service name defaults to the port name.
    private java.lang.String EniStationPlusPortSoap11WSDDServiceName = "EniStationPlusPortSoap11";

    public java.lang.String getEniStationPlusPortSoap11WSDDServiceName() {
        return EniStationPlusPortSoap11WSDDServiceName;
    }

    public void setEniStationPlusPortSoap11WSDDServiceName(java.lang.String name) {
        EniStationPlusPortSoap11WSDDServiceName = name;
    }

    public EniStationPlusPort getEniStationPlusPortSoap11() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(EniStationPlusPortSoap11_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getEniStationPlusPortSoap11(endpoint);
    }

    public EniStationPlusPort getEniStationPlusPortSoap11(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            EniStationPlusPortSoap11Stub _stub = new EniStationPlusPortSoap11Stub(portAddress, this);
            _stub.setPortName(getEniStationPlusPortSoap11WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setEniStationPlusPortSoap11EndpointAddress(java.lang.String address) {
        EniStationPlusPortSoap11_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this com.techedge.mp.dwh.adapter.client.service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (EniStationPlusPort.class.isAssignableFrom(serviceEndpointInterface)) {
                EniStationPlusPortSoap11Stub _stub = new EniStationPlusPortSoap11Stub(new java.net.URL(EniStationPlusPortSoap11_address), this);
                _stub.setPortName(getEniStationPlusPortSoap11WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this com.techedge.mp.dwh.adapter.client.service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("EniStationPlusPortSoap11".equals(inputPortName)) {
            return getEniStationPlusPortSoap11();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "EniStationPlusPortService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "EniStationPlusPortSoap11"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("EniStationPlusPortSoap11".equals(portName)) {
            setEniStationPlusPortSoap11EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
