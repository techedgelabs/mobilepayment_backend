package com.techedge.mp.dwh.adapter.client.model;

import org.scribe.builder.api.DefaultApi10a;
import org.scribe.model.Token;
import org.scribe.services.SignatureService;
import org.scribe.services.TimestampService;

import com.techedge.mp.dwh.adapter.client.service.DWHOAuthApiTimestampService;
import com.techedge.mp.dwh.adapter.client.service.HMACSha256SignatureService;

public class DWHOAuthApi extends DefaultApi10a {
    private static String NOT_USED_STRING = "Not used since ENI CS supports 2-legged authentication";

    @Override
    public String getRequestTokenEndpoint() {
        return NOT_USED_STRING;
    }

    @Override
    public String getAccessTokenEndpoint() {
        return NOT_USED_STRING;
    }

    @Override
    public String getAuthorizationUrl(Token requestToken) {
        return NOT_USED_STRING;
    }

    @Override
    public SignatureService getSignatureService() {
        return new HMACSha256SignatureService();
    }

    @Override
    public TimestampService getTimestampService() {
        return new DWHOAuthApiTimestampService();
    }

    public TimestampService getTimestampService(String timestamp, String nonce) {
        return new DWHOAuthApiTimestampService(timestamp, nonce);
    }
    
}
