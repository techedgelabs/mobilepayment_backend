/**
 * EniStationPlusPort.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.client;

public interface EniStationPlusPort extends java.rmi.Remote {
    public com.techedge.mp.dwh.adapter.elements.ChangeUserPasswordPlusResponse changeUserPasswordPlus(com.techedge.mp.dwh.adapter.elements.ChangeUserPasswordPlusRequest changeUserPasswordPlusRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.dwh.adapter.elements.SetUserDataPlusResponse setUserDataPlus(com.techedge.mp.dwh.adapter.elements.SetUserDataPlusRequest setUserDataPlusRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.dwh.adapter.elements.CheckLegacyPasswordPlusResponse checkLegacyPasswordPlus(com.techedge.mp.dwh.adapter.elements.CheckLegacyPasswordPlusRequest checkLegacyPasswordPlusRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.dwh.adapter.elements.GetListAwardResponse getListAward(com.techedge.mp.dwh.adapter.elements.GetListAwardRequest getListAwardRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.dwh.adapter.elements.GetAwardResponse getAward(com.techedge.mp.dwh.adapter.elements.GetAwardRequest getAwardRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.dwh.adapter.elements.GetPartnerListResponse getPartnerList(com.techedge.mp.dwh.adapter.elements.GetPartnerListRequest getPartnerListRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.dwh.adapter.elements.GetActionURLResponse getActionURL(com.techedge.mp.dwh.adapter.elements.GetActionURLRequest getActionURLRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.dwh.adapter.elements.GetCatalogResponse getCatalog(com.techedge.mp.dwh.adapter.elements.GetCatalogRequest getCatalogRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.dwh.adapter.elements.GetCategoryBurnResponse getCategoryBurn(com.techedge.mp.dwh.adapter.elements.GetCategoryBurnRequest getCategoryBurnRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.dwh.adapter.elements.GetCategoryEarnResponse getCategoryEarn(com.techedge.mp.dwh.adapter.elements.GetCategoryEarnRequest getCategoryEarnRequest) throws java.rmi.RemoteException;
    public com.techedge.mp.dwh.adapter.elements.GetBrandResponse getBrand(com.techedge.mp.dwh.adapter.elements.GetBrandRequest getBrandRequest) throws java.rmi.RemoteException;
}
