package com.techedge.mp.dwh.adapter.client.exceptions;

public class OAuthAutenticationException extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 3685231397172066332L;
    
    public OAuthAutenticationException(String message) {
        super(message);
    }

}
