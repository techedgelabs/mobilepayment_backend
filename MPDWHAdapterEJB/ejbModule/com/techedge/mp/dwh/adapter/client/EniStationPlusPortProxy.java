package com.techedge.mp.dwh.adapter.client;

public class EniStationPlusPortProxy implements EniStationPlusPort {
    private String             _endpoint          = null;
    private EniStationPlusPort eniStationPlusPort = null;

    public EniStationPlusPortProxy() {
        _initEniStationPlusPortProxy();
    }

    public EniStationPlusPortProxy(String endpoint) {
        _endpoint = endpoint;
        _initEniStationPlusPortProxy();
    }

    private void _initEniStationPlusPortProxy() {
        try {
            eniStationPlusPort = (new EniStationPlusPortServiceLocator()).getEniStationPlusPortSoap11();
            if (eniStationPlusPort != null) {
                if (_endpoint != null)
                    ((javax.xml.rpc.Stub) eniStationPlusPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
                else
                    _endpoint = (String) ((javax.xml.rpc.Stub) eniStationPlusPort)._getProperty("javax.xml.rpc.service.endpoint.address");
            }

        }
        catch (javax.xml.rpc.ServiceException serviceException) {}
    }

    public String getEndpoint() {
        return _endpoint;
    }

    public void setEndpoint(String endpoint) {
        _endpoint = endpoint;
        if (eniStationPlusPort != null)
            ((javax.xml.rpc.Stub) eniStationPlusPort)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);

    }

    public EniStationPlusPort getEniStationPlusPort() {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort;
    }

    public com.techedge.mp.dwh.adapter.elements.GetListAwardResponse getListAward(com.techedge.mp.dwh.adapter.elements.GetListAwardRequest getListAwardRequest)
            throws java.rmi.RemoteException {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort.getListAward(getListAwardRequest);
    }

    public com.techedge.mp.dwh.adapter.elements.GetAwardResponse getAward(com.techedge.mp.dwh.adapter.elements.GetAwardRequest getAwardRequest) throws java.rmi.RemoteException {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort.getAward(getAwardRequest);
    }

    public com.techedge.mp.dwh.adapter.elements.GetPartnerListResponse getPartnerList(com.techedge.mp.dwh.adapter.elements.GetPartnerListRequest getPartnerListRequest)
            throws java.rmi.RemoteException {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort.getPartnerList(getPartnerListRequest);
    }

    public com.techedge.mp.dwh.adapter.elements.SetUserDataPlusResponse setUserDataPlus(com.techedge.mp.dwh.adapter.elements.SetUserDataPlusRequest setUserDataPlusRequest)
            throws java.rmi.RemoteException {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort.setUserDataPlus(setUserDataPlusRequest);
    }

    public com.techedge.mp.dwh.adapter.elements.ChangeUserPasswordPlusResponse changeUserPasswordPlus(
            com.techedge.mp.dwh.adapter.elements.ChangeUserPasswordPlusRequest changeUserPasswordPlusRequest) throws java.rmi.RemoteException {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort.changeUserPasswordPlus(changeUserPasswordPlusRequest);
    }

    public com.techedge.mp.dwh.adapter.elements.CheckLegacyPasswordPlusResponse checkLegacyPasswordPlus(
            com.techedge.mp.dwh.adapter.elements.CheckLegacyPasswordPlusRequest checkLegacyPasswordPlusRequest) throws java.rmi.RemoteException {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort.checkLegacyPasswordPlus(checkLegacyPasswordPlusRequest);
    }

    public com.techedge.mp.dwh.adapter.elements.GetActionURLResponse getActionURL(com.techedge.mp.dwh.adapter.elements.GetActionURLRequest getActionURLRequest)
            throws java.rmi.RemoteException {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort.getActionURL(getActionURLRequest);
    }

    public com.techedge.mp.dwh.adapter.elements.GetCatalogResponse getCatalog(com.techedge.mp.dwh.adapter.elements.GetCatalogRequest getCatalogRequest)
            throws java.rmi.RemoteException {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort.getCatalog(getCatalogRequest);
    }

    public com.techedge.mp.dwh.adapter.elements.GetCategoryBurnResponse getCategoryBurn(com.techedge.mp.dwh.adapter.elements.GetCategoryBurnRequest getCategoryBurnRequest)
            throws java.rmi.RemoteException {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort.getCategoryBurn(getCategoryBurnRequest);
    }

    public com.techedge.mp.dwh.adapter.elements.GetCategoryEarnResponse getCategoryEarn(com.techedge.mp.dwh.adapter.elements.GetCategoryEarnRequest getCategoryEarnRequest)
            throws java.rmi.RemoteException {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort.getCategoryEarn(getCategoryEarnRequest);
    }

    public com.techedge.mp.dwh.adapter.elements.GetBrandResponse getBrand(com.techedge.mp.dwh.adapter.elements.GetBrandRequest getBrandRequest) throws java.rmi.RemoteException {
        if (eniStationPlusPort == null)
            _initEniStationPlusPortProxy();
        return eniStationPlusPort.getBrand(getBrandRequest);
    }

}