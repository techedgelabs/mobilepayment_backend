package com.techedge.mp.dwh.adapter.client.validator;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;

import javax.xml.namespace.QName;
import javax.xml.rpc.JAXRPCException;
import javax.xml.rpc.handler.GenericHandler;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.MessageContext;
import javax.xml.rpc.handler.soap.SOAPMessageContext;
import javax.xml.soap.Detail;
import javax.xml.soap.DetailEntry;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;

import org.scribe.model.OAuthConfig;
import org.scribe.model.OAuthConstants;
import org.scribe.model.OAuthRequest;
import org.scribe.model.SignatureType;
import org.scribe.model.Verb;
import org.scribe.oauth.OAuth10aServiceImpl;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.dwh.adapter.business.EJBHomeCache;
import com.techedge.mp.dwh.adapter.client.exceptions.OAuthAutenticationException;
import com.techedge.mp.dwh.adapter.client.model.DWHOAuthApi;

public class OAuthValidationHandler extends GenericHandler {

    /**
     * 
     */
    private static final long       serialVersionUID             = 2344096747378583010L;

    private final static String     PARAM_DWH_URL_WSDL           = "DWH_URL_WSDL";
    private final static String     PARAM_DWH_OAUTH_CONSUMER_KEY = "DWH_OAUTH_CONSUMER_KEY";

    private ParametersServiceRemote parametersService            = null;
    private OAuthRequest            oauthRequest                 = null;
    private OAuth10aServiceImpl     oauthService                 = null;

    @Override
    public void init(HandlerInfo config) {
        try {
            loadOAuthParams();
        }
        catch (Exception ex) {
            System.err.println("Error loading oauth parameters: " + ex.getMessage());
        }

        super.init(config);
    }

    @Override
    public QName[] getHeaders() {
        return null;
    }

    /*
    @Override
    public void init() {
        try {
            loadOAuthParams();
        }
        catch (Exception ex) {
            System.err.println("Error loading oauth parameters: " + ex.getMessage());
        }

        super.init();
    }
    
    @Override
    public void invoke(MessageContext messageContext) throws AxisFault {
        System.out.println("HandlerChain [OAuthValidator]: invoke()......");
        
       //SOAPMessageContext soapMessageContext = (SOAPMessageContext) messageContext;
        try {
            addOAuthHeaders(messageContext);
        }
        catch (OAuthAutenticationException ex) {
            System.err.println("OAuth autentication error: " + ex.getMessage());
            messageContext.setMessage(generateSOAPFaultMessage("OAuth autentication error", ex.getMessage()));
        }
    }
    */
    public boolean handleRequest(MessageContext messageContext) throws JAXRPCException {

        System.out.println("HandlerChain [OAuthValidation]: handleRequest()......");

        SOAPMessageContext soapMessageContext = (SOAPMessageContext) messageContext;

        try {
            addOAuthHeaders(soapMessageContext);
        }
        catch (OAuthAutenticationException ex) {
            System.err.println("OAuth autentication error: " + ex.getMessage());
            soapMessageContext.setMessage(generateSOAPFaultMessage("OAuth autentication error", ex.getMessage()));
            return false;
        }
        //continue other handler chain
        return true;
    }
    
    public boolean handleResponse(MessageContext messageContext) throws JAXRPCException {

        System.out.println("HandlerChain [ResponseHandler]: handleResponse()......");

        SOAPMessageContext soapMessageContext = (SOAPMessageContext) messageContext;

        try {
            
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            soapMessageContext.getMessage().writeTo(baos);
            String responseContent = baos.toString();
            
            if (responseContent.length() >= 1000) {
                responseContent = responseContent.substring(0, 1000);
            }
            System.out.println("Response: " + responseContent);
        }
        catch (SOAPException ex1) {
            ex1.printStackTrace();
            return false;
        }
        catch (IOException ex2) {
            ex2.printStackTrace();
            return false;
        }
        
        //continue other handler chain
        return true;
    }    

    private void loadOAuthParams() throws InterfaceNotFoundException, ParameterNotFoundException, IOException {

        System.out.println("HandlerChain [OAuthValidation]: init()......");

        final String callback = OAuthConstants.OUT_OF_BAND;
        final SignatureType signatureType = SignatureType.Header;
        //final OutputStream debugStream = System.out;
        final OutputStream debugStream = null;
        final String scope = null;
        String consumerSecret = null;
        String consumerKey = null;

        this.parametersService = EJBHomeCache.getInstance().getParametersService();

        String url = parametersService.getParamValue(PARAM_DWH_URL_WSDL);
        consumerKey = parametersService.getParamValue(PARAM_DWH_OAUTH_CONSUMER_KEY);

        File fileCert = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "oauth" + File.separator + "dwh_client.oauth");
        FileInputStream input = new FileInputStream(fileCert);

        InputStreamReader is = new InputStreamReader(input);
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(is);
        String read;

        while ((read = br.readLine()) != null) {
            //System.out.println(read);
            if (read.contains("BEGIN CERTIFICATE") || read.contains("END CERTIFICATE"))
                continue;

            sb.append(read);
        }

        br.close();
        consumerSecret = sb.toString();

        DWHOAuthApi api = new DWHOAuthApi();
        OAuthConfig config = new OAuthConfig(consumerKey, consumerSecret, callback, signatureType, scope, debugStream);
        oauthService = new OAuth10aServiceImpl(api, config);
        oauthRequest = new OAuthRequest(Verb.POST, url);
    }

    private void addOAuthHeaders(SOAPMessageContext messageContext) throws OAuthAutenticationException {
        String requestContent = null;
        SOAPMessage soapMessage = messageContext.getMessage();

        try {
            //soapMessage.getSOAPHeader().detachNode();

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            soapMessage.writeTo(baos);
            requestContent = baos.toString();
            /*
            NodeList soapBody = soapMessage.getSOAPHeader().rgetSOAPBody().getChildNodes();
            DOMSource source = new DOMSource();
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            for (int i = 0; i < soapBody.getLength(); ++i) {
                source.setNode(soapBody.item(i));
                transformer.transform(source, result);
            }

            requestContent = writer.toString();
            */

            System.out.println("=========== SOAP REQUEST ===========");
            System.out.println(requestContent.replaceAll("/>", "/>\n"));
            System.out.println("=========== END ===========");
        }
        catch (Exception ex) {
            throw new OAuthAutenticationException("Http SOAP message read error (" + ex.getMessage() + ")");
        }

        oauthRequest.addBodyParameter("request_body", requestContent);
        oauthService.signRequest(OAuthConstants.EMPTY_TOKEN, oauthRequest);
        /*
        Map<String, Object> httpHeaders = (Map<String, Object>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
        for (String key : oauthRequest.getHeaders().keySet())
            httpHeaders.put(key, oauthRequest.getHeaders().get(key));
        */
        MimeHeaders httpHeaders = soapMessage.getMimeHeaders();
        for (String key : oauthRequest.getHeaders().keySet())
            httpHeaders.addHeader(key, oauthRequest.getHeaders().get(key));
    }

    private SOAPMessage generateSOAPFaultMessage(String error, String errorDescription) {
        SOAPMessage soapMessage = null;
        try {
            soapMessage = MessageFactory.newInstance().createMessage();
            soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ns1", "http://service.adapter.dwh.mp.techedge.com");
            SOAPFault soapFault = soapMessage.getSOAPBody().addFault();
            QName faultName = new QName(SOAPConstants.URI_NS_SOAP_ENVELOPE, "Server");
            soapFault.setFaultCode(faultName);
            soapFault.setFaultString(error);
            Detail detail = soapFault.addDetail();
            QName entryName = new QName("http://service.adapter.dwh.mp.techedge.com", "OAuthAutenticationException", "ns1");
            DetailEntry entry = detail.addDetailEntry(entryName);
            entry.addTextNode((errorDescription != null) ? errorDescription : error);
        }
        catch (SOAPException ex) {
            System.err.println("Errore nella creazione del messaggio soap: " + ex.getMessage());
        }

        return soapMessage;
    }

}
