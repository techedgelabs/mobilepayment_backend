/**
 * CategoryEarnType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.types;

public class CategoryEarnType  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 9217318820097299219L;

    private int categoryId;

    private java.lang.String categoryDesc;

    private java.lang.String urlCategory;

    public CategoryEarnType() {
    }

    public CategoryEarnType(
           int categoryId,
           java.lang.String categoryDesc,
           java.lang.String urlCategory) {
           this.categoryId = categoryId;
           this.categoryDesc = categoryDesc;
           this.urlCategory = urlCategory;
    }


    /**
     * Gets the categoryId value for this CategoryEarnType.
     * 
     * @return categoryId
     */
    public int getCategoryId() {
        return categoryId;
    }


    /**
     * Sets the categoryId value for this CategoryEarnType.
     * 
     * @param categoryId
     */
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }


    /**
     * Gets the categoryDesc value for this CategoryEarnType.
     * 
     * @return categoryDesc
     */
    public java.lang.String getCategoryDesc() {
        return categoryDesc;
    }


    /**
     * Sets the categoryDesc value for this CategoryEarnType.
     * 
     * @param categoryDesc
     */
    public void setCategoryDesc(java.lang.String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }


    /**
     * Gets the urlCategory value for this CategoryEarnType.
     * 
     * @return urlCategory
     */
    public java.lang.String getUrlCategory() {
        return urlCategory;
    }


    /**
     * Sets the urlCategory value for this CategoryEarnType.
     * 
     * @param urlCategory
     */
    public void setUrlCategory(java.lang.String urlCategory) {
        this.urlCategory = urlCategory;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CategoryEarnType)) return false;
        CategoryEarnType other = (CategoryEarnType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.categoryId == other.getCategoryId() &&
            ((this.categoryDesc==null && other.getCategoryDesc()==null) || 
             (this.categoryDesc!=null &&
              this.categoryDesc.equals(other.getCategoryDesc()))) &&
            ((this.urlCategory==null && other.getUrlCategory()==null) || 
             (this.urlCategory!=null &&
              this.urlCategory.equals(other.getUrlCategory())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getCategoryId();
        if (getCategoryDesc() != null) {
            _hashCode += getCategoryDesc().hashCode();
        }
        if (getUrlCategory() != null) {
            _hashCode += getUrlCategory().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CategoryEarnType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "CategoryEarnType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "categoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "categoryDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("urlCategory");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "urlCategory"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
