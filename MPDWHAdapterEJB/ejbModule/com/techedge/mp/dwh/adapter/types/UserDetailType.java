/**
 * UserDetailType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.types;

public class UserDetailType  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -6685162884299547740L;

    private java.lang.String name;

    private java.lang.String surname;

    private long dateOfBirth;

    private java.lang.String gender;

    private java.lang.String fiscalCode;

    private java.lang.String cityOfBirth;

    private java.lang.String countryOfBirth;

    private java.lang.String mobilePhoneNumber;

    private FlgPrivacyType[] flgPrivacy;

    private boolean flgBlacklist;

    private java.lang.String codCarta;

    private java.lang.String campaignType;
    
    private boolean isSocial;

    private java.lang.String socialType;
    
    private java.lang.String idSocial;
    
    private boolean flgValidateMail;

    public UserDetailType() {
    }

    public UserDetailType(
           java.lang.String name,
           java.lang.String surname,
           long dateOfBirth,
           java.lang.String gender,
           java.lang.String fiscalCode,
           java.lang.String cityOfBirth,
           java.lang.String countryOfBirth,
           java.lang.String mobilePhoneNumber,
           FlgPrivacyType[] flgPrivacy,
           boolean flgBlacklist,
           java.lang.String codCarta,
           java.lang.String campaignType,
           boolean isSocial,
           java.lang.String socialType,
           java.lang.String idSocial,
           boolean flgValidateMail) {
           this.name = name;
           this.surname = surname;
           this.dateOfBirth = dateOfBirth;
           this.gender = gender;
           this.fiscalCode = fiscalCode;
           this.cityOfBirth = cityOfBirth;
           this.countryOfBirth = countryOfBirth;
           this.mobilePhoneNumber = mobilePhoneNumber;
           this.flgPrivacy = flgPrivacy;
           this.flgBlacklist = flgBlacklist;
           this.codCarta = codCarta;
           this.campaignType = campaignType;
           this.isSocial = isSocial;
           this.socialType = socialType;
           this.idSocial = idSocial;
           this.flgValidateMail = flgValidateMail;
    }


    /**
     * Gets the name value for this UserDetailType.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this UserDetailType.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the surname value for this UserDetailType.
     * 
     * @return surname
     */
    public java.lang.String getSurname() {
        return surname;
    }


    /**
     * Sets the surname value for this UserDetailType.
     * 
     * @param surname
     */
    public void setSurname(java.lang.String surname) {
        this.surname = surname;
    }


    /**
     * Gets the dateOfBirth value for this UserDetailType.
     * 
     * @return dateOfBirth
     */
    public long getDateOfBirth() {
        return dateOfBirth;
    }


    /**
     * Sets the dateOfBirth value for this UserDetailType.
     * 
     * @param dateOfBirth
     */
    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }


    /**
     * Gets the gender value for this UserDetailType.
     * 
     * @return gender
     */
    public java.lang.String getGender() {
        return gender;
    }


    /**
     * Sets the gender value for this UserDetailType.
     * 
     * @param gender
     */
    public void setGender(java.lang.String gender) {
        this.gender = gender;
    }


    /**
     * Gets the fiscalCode value for this UserDetailType.
     * 
     * @return fiscalCode
     */
    public java.lang.String getFiscalCode() {
        return fiscalCode;
    }


    /**
     * Sets the fiscalCode value for this UserDetailType.
     * 
     * @param fiscalCode
     */
    public void setFiscalCode(java.lang.String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }


    /**
     * Gets the cityOfBirth value for this UserDetailType.
     * 
     * @return cityOfBirth
     */
    public java.lang.String getCityOfBirth() {
        return cityOfBirth;
    }


    /**
     * Sets the cityOfBirth value for this UserDetailType.
     * 
     * @param cityOfBirth
     */
    public void setCityOfBirth(java.lang.String cityOfBirth) {
        this.cityOfBirth = cityOfBirth;
    }


    /**
     * Gets the countryOfBirth value for this UserDetailType.
     * 
     * @return countryOfBirth
     */
    public java.lang.String getCountryOfBirth() {
        return countryOfBirth;
    }


    /**
     * Sets the countryOfBirth value for this UserDetailType.
     * 
     * @param countryOfBirth
     */
    public void setCountryOfBirth(java.lang.String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }


    /**
     * Gets the mobilePhoneNumber value for this UserDetailType.
     * 
     * @return mobilePhoneNumber
     */
    public java.lang.String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }


    /**
     * Sets the mobilePhoneNumber value for this UserDetailType.
     * 
     * @param mobilePhoneNumber
     */
    public void setMobilePhoneNumber(java.lang.String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }


    /**
     * Gets the flgPrivacy value for this UserDetailType.
     * 
     * @return flgPrivacy
     */
    public FlgPrivacyType[] getFlgPrivacy() {
        return flgPrivacy;
    }


    /**
     * Sets the flgPrivacy value for this UserDetailType.
     * 
     * @param flgPrivacy
     */
    public void setFlgPrivacy(FlgPrivacyType[] flgPrivacy) {
        this.flgPrivacy = flgPrivacy;
    }

    public FlgPrivacyType getFlgPrivacy(int i) {
        return this.flgPrivacy[i];
    }

    public void setFlgPrivacy(int i, FlgPrivacyType _value) {
        this.flgPrivacy[i] = _value;
    }


    /**
     * Gets the flgBlacklist value for this UserDetailType.
     * 
     * @return flgBlacklist
     */
    public boolean isFlgBlacklist() {
        return flgBlacklist;
    }


    /**
     * Sets the flgBlacklist value for this UserDetailType.
     * 
     * @param flgBlacklist
     */
    public void setFlgBlacklist(boolean flgBlacklist) {
        this.flgBlacklist = flgBlacklist;
    }


    /**
     * Gets the codCarta value for this UserDetailType.
     * 
     * @return codCarta
     */
    public java.lang.String getCodCarta() {
        return codCarta;
    }


    /**
     * Sets the codCarta value for this UserDetailType.
     * 
     * @param codCarta
     */
    public void setCodCarta(java.lang.String codCarta) {
        this.codCarta = codCarta;
    }


    /**
     * Gets the campaignType value for this UserDetailType.
     * 
     * @return campaignType
     */
    public java.lang.String getCampaignType() {
        return campaignType;
    }


    /**
     * Sets the campaignType value for this UserDetailType.
     * 
     * @param campaignType
     */
    public void setCampaignType(java.lang.String campaignType) {
        this.campaignType = campaignType;
    }

    public boolean getIsSocial() {
        return isSocial;
    }

    public void setIsSocial(boolean isSocial) {
        this.isSocial = isSocial;
    }

    public java.lang.String getSocialType() {
        return socialType;
    }

    public void setSocialType(java.lang.String socialType) {
        this.socialType = socialType;
    }
    
    public java.lang.String getIdSocial() {
        return idSocial;
    }

    public void setIdSocial(java.lang.String idSocial) {
        this.idSocial = idSocial;
    }

    public boolean isFlgValidateMail() {
        return flgValidateMail;
    }

    public void setFlgValidateMail(boolean flgValidateMail) {
        this.flgValidateMail = flgValidateMail;
    }



    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof UserDetailType)) return false;
        UserDetailType other = (UserDetailType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.surname==null && other.getSurname()==null) || 
             (this.surname!=null &&
              this.surname.equals(other.getSurname()))) &&
            this.dateOfBirth == other.getDateOfBirth() &&
            ((this.gender==null && other.getGender()==null) || 
             (this.gender!=null &&
              this.gender.equals(other.getGender()))) &&
            ((this.fiscalCode==null && other.getFiscalCode()==null) || 
             (this.fiscalCode!=null &&
              this.fiscalCode.equals(other.getFiscalCode()))) &&
            ((this.cityOfBirth==null && other.getCityOfBirth()==null) || 
             (this.cityOfBirth!=null &&
              this.cityOfBirth.equals(other.getCityOfBirth()))) &&
            ((this.countryOfBirth==null && other.getCountryOfBirth()==null) || 
             (this.countryOfBirth!=null &&
              this.countryOfBirth.equals(other.getCountryOfBirth()))) &&
            ((this.mobilePhoneNumber==null && other.getMobilePhoneNumber()==null) || 
             (this.mobilePhoneNumber!=null &&
              this.mobilePhoneNumber.equals(other.getMobilePhoneNumber()))) &&
            ((this.flgPrivacy==null && other.getFlgPrivacy()==null) || 
             (this.flgPrivacy!=null &&
              java.util.Arrays.equals(this.flgPrivacy, other.getFlgPrivacy()))) &&
            this.flgBlacklist == other.isFlgBlacklist() &&
            ((this.codCarta==null && other.getCodCarta()==null) || 
             (this.codCarta!=null &&
              this.codCarta.equals(other.getCodCarta()))) &&
            ((this.campaignType==null && other.getCampaignType()==null) || 
             (this.campaignType!=null &&
              this.campaignType.equals(other.getCampaignType()))) &&
            this.isSocial == other.getIsSocial() &&
            ((this.socialType==null && other.getSocialType()==null) || 
             (this.socialType!=null &&
              this.socialType.equals(other.getSocialType()))) &&
            ((this.idSocial==null && other.getIdSocial()==null) || 
             (this.idSocial!=null &&
              this.idSocial.equals(other.getIdSocial()))) &&
            this.isFlgValidateMail() == other.getIsSocial();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getSurname() != null) {
            _hashCode += getSurname().hashCode();
        }
        _hashCode += new Long(getDateOfBirth()).hashCode();
        if (getGender() != null) {
            _hashCode += getGender().hashCode();
        }
        if (getFiscalCode() != null) {
            _hashCode += getFiscalCode().hashCode();
        }
        if (getCityOfBirth() != null) {
            _hashCode += getCityOfBirth().hashCode();
        }
        if (getCountryOfBirth() != null) {
            _hashCode += getCountryOfBirth().hashCode();
        }
        if (getMobilePhoneNumber() != null) {
            _hashCode += getMobilePhoneNumber().hashCode();
        }
        if (getFlgPrivacy() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getFlgPrivacy());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getFlgPrivacy(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        _hashCode += (isFlgBlacklist() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCodCarta() != null) {
            _hashCode += getCodCarta().hashCode();
        }
        if (getCampaignType() != null) {
            _hashCode += getCampaignType().hashCode();
        }
        _hashCode += (getIsSocial() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getSocialType() != null) {
            _hashCode += getSocialType().hashCode();
        }
        if (getIdSocial() != null) {
            _hashCode += getIdSocial().hashCode();
        }
        _hashCode += (isFlgValidateMail() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(UserDetailType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "UserDetailType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("surname");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "surname"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateOfBirth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "dateOfBirth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("gender");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "gender"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fiscalCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "fiscalCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cityOfBirth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "cityOfBirth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("countryOfBirth");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "countryOfBirth"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("mobilePhoneNumber");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "mobilePhoneNumber"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flgPrivacy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "flgPrivacy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "FlgPrivacyType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flgBlacklist");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "flgBlacklist"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCarta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "codCarta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("campaignType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "campaignType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("isSocial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "isSocial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("socialType");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "socialType"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idSocial");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "idSocial"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flgValidateMail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "flgValidateMail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
 new org.apache.axis.encoding.ser.BeanDeserializer(_javaType, _xmlType, typeDesc);
    }

}
