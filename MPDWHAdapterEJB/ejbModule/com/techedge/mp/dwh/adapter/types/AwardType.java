/**
 * AwardType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.types;

public class AwardType  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7458634859466922199L;

    private java.lang.Integer idAward;

    private int categoryId;
    
    private java.lang.String category;

    private int brandId;

    private java.lang.String brand;
    
    private java.lang.String description;

    private java.lang.String descriptionShort;

    private int point;

    private java.lang.Double contribution;
    
    private java.lang.Double value;

    private long dateStartRedemption;

    private long dateStopRedemption;

    private boolean cancelable;

    private boolean tangible;

    private java.lang.String type;

    private java.lang.Boolean redeemable;

    private java.lang.String urlImageAward;
    
    private int pointPartner;
    
    private java.lang.String channelRedemption;
    
    private java.lang.String typePointPartner;

    public AwardType() {
    }

    public AwardType(
            java.lang.Integer idAward,
           int categoryId,
           java.lang.String category,
           int brandId,
           java.lang.String brand,
           java.lang.String description,
           java.lang.String descriptionShort,
           int point,
           java.lang.Double contribution,
           java.lang.Double value,
           long dateStartRedemption,
           long dateStopRedemption,
           boolean cancelable,
           boolean tangible,
           java.lang.String type,
           java.lang.Boolean redeemable,
           java.lang.String urlImageAward,
           int pointPartner,
           java.lang.String channelRedemption,
           java.lang.String typePointPartner) {
           this.idAward = idAward;
           this.categoryId = categoryId;
           this.category = category;
           this.brandId = brandId;
           this.brand = brand;
           this.description = description;
           this.descriptionShort = descriptionShort;
           this.point = point;
           this.contribution = contribution;
           this.value = value;
           this.dateStartRedemption = dateStartRedemption;
           this.dateStopRedemption = dateStopRedemption;
           this.cancelable = cancelable;
           this.tangible = tangible;
           this.type = type;
           this.redeemable = redeemable;
           this.urlImageAward = urlImageAward;
           this.pointPartner = pointPartner;
           this.channelRedemption = channelRedemption;
           this.typePointPartner = typePointPartner;
    }


    /**
     * Gets the idAward value for this AwardType.
     * 
     * @return idAward
     */
    public java.lang.Integer getIdAward() {
        return idAward;
    }


    /**
     * Sets the idAward value for this AwardType.
     * 
     * @param idAward
     */
    public void setIdAward(java.lang.Integer idAward) {
        this.idAward = idAward;
    }


    /**
     * Gets the categoryId value for this AwardType.
     * 
     * @return categoryId
     */
    public int getCategoryId() {
        return categoryId;
    }


    /**
     * Sets the categoryId value for this AwardType.
     * 
     * @param categoryId
     */
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public java.lang.String getCategory() {
        return category;
    }

    public void setCategory(java.lang.String category) {
        this.category = category;
    }

    /**
     * Gets the brandId value for this AwardType.
     * 
     * @return brandId
     */
    public int getBrandId() {
        return brandId;
    }


    /**
     * Sets the brandId value for this AwardType.
     * 
     * @param brandId
     */
    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }
    

    public java.lang.String getBrand() {
        return brand;
    }

    public void setBrand(java.lang.String brand) {
        this.brand = brand;
    }

    /**
     * Gets the description value for this AwardType.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this AwardType.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the descriptionShort value for this AwardType.
     * 
     * @return descriptionShort
     */
    public java.lang.String getDescriptionShort() {
        return descriptionShort;
    }


    /**
     * Sets the descriptionShort value for this AwardType.
     * 
     * @param descriptionShort
     */
    public void setDescriptionShort(java.lang.String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }


    /**
     * Gets the point value for this AwardType.
     * 
     * @return point
     */
    public int getPoint() {
        return point;
    }


    /**
     * Sets the point value for this AwardType.
     * 
     * @param point
     */
    public void setPoint(int point) {
        this.point = point;
    }


    /**
     * Gets the contribution value for this AwardType.
     * 
     * @return contribution
     */
    public java.lang.Double getContribution() {
        return contribution;
    }


    /**
     * Sets the value for this AwardType.
     * 
     * @param value
     */
    public void setContribution(java.lang.Double contribution) {
        this.contribution = contribution;
    }
    
    
    /**
     * Gets the value for this AwardType.
     * 
     * @return value
     */
    public java.lang.Double getValue() {
        return value;
    }

    
    /**
     * Sets the contribution value for this AwardType.
     * 
     * @param contribution
     */
    public void setValue(java.lang.Double value) {
        this.value = value;
    }

    /**
     * Gets the dateStartRedemption value for this AwardType.
     * 
     * @return dateStartRedemption
     */
    public long getDateStartRedemption() {
        return dateStartRedemption;
    }


    /**
     * Sets the dateStartRedemption value for this AwardType.
     * 
     * @param dateStartRedemption
     */
    public void setDateStartRedemption(long dateStartRedemption) {
        this.dateStartRedemption = dateStartRedemption;
    }


    /**
     * Gets the dateStopRedemption value for this AwardType.
     * 
     * @return dateStopRedemption
     */
    public long getDateStopRedemption() {
        return dateStopRedemption;
    }


    /**
     * Sets the dateStopRedemption value for this AwardType.
     * 
     * @param dateStopRedemption
     */
    public void setDateStopRedemption(long dateStopRedemption) {
        this.dateStopRedemption = dateStopRedemption;
    }


    /**
     * Gets the cancelable value for this AwardType.
     * 
     * @return cancelable
     */
    public boolean isCancelable() {
        return cancelable;
    }


    /**
     * Sets the cancelable value for this AwardType.
     * 
     * @param cancelable
     */
    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }


    /**
     * Gets the tangible value for this AwardType.
     * 
     * @return tangible
     */
    public boolean isTangible() {
        return tangible;
    }


    /**
     * Sets the tangible value for this AwardType.
     * 
     * @param tangible
     */
    public void setTangible(boolean tangible) {
        this.tangible = tangible;
    }


    /**
     * Gets the type value for this AwardType.
     * 
     * @return type
     */
    public java.lang.String getType() {
        return type;
    }


    /**
     * Sets the type value for this AwardType.
     * 
     * @param type
     */
    public void setType(java.lang.String type) {
        this.type = type;
    }


    /**
     * Gets the redeemable value for this AwardType.
     * 
     * @return redeemable
     */
    public java.lang.Boolean getRedeemable() {
        return redeemable;
    }


    /**
     * Sets the redeemable value for this AwardType.
     * 
     * @param redeemable
     */
    public void setRedeemable(java.lang.Boolean redeemable) {
        this.redeemable = redeemable;
    }


    /**
     * Gets the urlImageAward value for this AwardType.
     * 
     * @return urlImageAward
     */
    public java.lang.String getUrlImageAward() {
        return urlImageAward;
    }


    /**
     * Sets the urlImageAward value for this AwardType.
     * 
     * @param urlImageAward
     */
    public void setUrlImageAward(java.lang.String urlImageAward) {
        this.urlImageAward = urlImageAward;
    }
    

    public int getPointPartner() {
        return pointPartner;
    }

    public void setPointPartner(int pointPartner) {
        this.pointPartner = pointPartner;
    }

    public java.lang.String getChannelRedemption() {
        return channelRedemption;
    }

    public void setChannelRedemption(java.lang.String channelRedemption) {
        this.channelRedemption = channelRedemption;
    }

    public java.lang.String getTypePointPartner() {
        return typePointPartner;
    }

    public void setTypePointPartner(java.lang.String typePointPartner) {
        this.typePointPartner = typePointPartner;
    }



    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AwardType)) return false;
        AwardType other = (AwardType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.idAward==null && other.getIdAward()==null) || 
             (this.idAward!=null &&
              this.idAward.equals(other.getIdAward()))) &&
            this.categoryId == other.getCategoryId() &&
            ((this.category==null && other.getCategory()==null) || 
             (this.category!=null &&
              this.category.equals(other.getCategory()))) &&
            this.brandId == other.getBrandId() &&
            ((this.brand==null && other.getBrand()==null) || 
             (this.brand!=null &&
              this.brand.equals(other.getBrand()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.descriptionShort==null && other.getDescriptionShort()==null) || 
             (this.descriptionShort!=null &&
              this.descriptionShort.equals(other.getDescriptionShort()))) &&
            this.point == other.getPoint() &&
            ((this.contribution==null && other.getContribution()==null) || 
             (this.contribution!=null &&
              this.contribution.equals(other.getContribution()))) &&
            ((this.value==null && other.getValue()==null) || 
             (this.value!=null &&
              this.value.equals(other.getValue()))) &&
            this.dateStartRedemption == other.getDateStartRedemption() &&
            this.dateStopRedemption == other.getDateStopRedemption() &&
            this.cancelable == other.isCancelable() &&
            this.tangible == other.isTangible() &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.redeemable==null && other.getRedeemable()==null) || 
             (this.redeemable!=null &&
              this.redeemable.equals(other.getRedeemable()))) &&
            ((this.urlImageAward==null && other.getUrlImageAward()==null) || 
             (this.urlImageAward!=null &&
              this.urlImageAward.equals(other.getUrlImageAward()))) &&
            this.pointPartner == other.getPointPartner() &&
            ((this.channelRedemption==null && other.getChannelRedemption()==null) || 
            (this.channelRedemption!=null &&
             this.channelRedemption.equals(other.getChannelRedemption()))) &&
            ((this.typePointPartner==null && other.getTypePointPartner()==null) || 
            (this.typePointPartner!=null &&
             this.typePointPartner.equals(other.getTypePointPartner())));
        
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getIdAward() != null) {
            _hashCode += getIdAward().hashCode();
        }
        _hashCode += getCategoryId();
        if (getCategory() != null) {
            _hashCode += getCategory().hashCode();
        }
        _hashCode += getBrandId();
        if (getBrand() != null) {
            _hashCode += getBrand().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getDescriptionShort() != null) {
            _hashCode += getDescriptionShort().hashCode();
        }
        _hashCode += getPoint();
        if (getContribution() != null) {
            _hashCode += getContribution().hashCode();
        }
        if (getValue() != null) {
            _hashCode += getValue().hashCode();
        }
        _hashCode += new Long(getDateStartRedemption()).hashCode();
        _hashCode += new Long(getDateStopRedemption()).hashCode();
        _hashCode += (isCancelable() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isTangible() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getRedeemable() != null) {
            _hashCode += getRedeemable().hashCode();
        }
        if (getUrlImageAward() != null) {
            _hashCode += getUrlImageAward().hashCode();
        }
        _hashCode += getPointPartner();
        if (getChannelRedemption() != null) {
            _hashCode += getChannelRedemption().hashCode();
        }
        if (getTypePointPartner() != null) {
            _hashCode += getTypePointPartner().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AwardType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "AwardType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("idAward");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "idAward"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "categoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("category");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "category"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("brandId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "brandId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("brand");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "brand"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descriptionShort");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "descriptionShort"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("point");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "point"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("contribution");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "contribution"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("value");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "value"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateStartRedemption");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "dateStartRedemption"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("dateStopRedemption");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "dateStopRedemption"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "cancelable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tangible");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "tangible"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("redeemable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "redeemable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("urlImageAward");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "urlImageAward"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("pointPartner");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "pointPartner"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("channelRedemption");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "channelRedemption"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typePointPartner");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "typePointPartner"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
