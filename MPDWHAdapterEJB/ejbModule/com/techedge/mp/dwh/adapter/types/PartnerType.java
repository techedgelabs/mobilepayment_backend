/**
 * PartnerType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.types;

public class PartnerType  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -2254110949984572856L;

    private int partnerId;

    private int categoryId;

    private java.lang.String categoryDesc;

    private java.lang.String name;

    private java.lang.String description;

    private java.lang.String title;

    private java.lang.String logic;

    private java.lang.String logoURL;

    private java.lang.String logoSmallURL;

    public PartnerType() {
    }

    public PartnerType(
           int partnerId,
           int categoryId,
           java.lang.String categoryDesc,
           java.lang.String name,
           java.lang.String description,
           java.lang.String title,
           java.lang.String logic,
           java.lang.String logoURL,
           java.lang.String logoSmallURL) {
           this.partnerId = partnerId;
           this.categoryId = categoryId;
           this.categoryDesc = categoryDesc;
           this.name = name;
           this.description = description;
           this.title = title;
           this.logic = logic;
           this.logoURL = logoURL;
           this.logoSmallURL = logoSmallURL;
    }


    /**
     * Gets the partnerId value for this PartnerType.
     * 
     * @return partnerId
     */
    public int getPartnerId() {
        return partnerId;
    }


    /**
     * Sets the partnerId value for this PartnerType.
     * 
     * @param partnerId
     */
    public void setPartnerId(int partnerId) {
        this.partnerId = partnerId;
    }


    /**
     * Gets the categoryId value for this PartnerType.
     * 
     * @return categoryId
     */
    public int getCategoryId() {
        return categoryId;
    }


    /**
     * Sets the categoryId value for this PartnerType.
     * 
     * @param categoryId
     */
    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }


    /**
     * Gets the categoryDesc value for this PartnerType.
     * 
     * @return categoryDesc
     */
    public java.lang.String getCategoryDesc() {
        return categoryDesc;
    }


    /**
     * Sets the categoryDesc value for this PartnerType.
     * 
     * @param categoryDesc
     */
    public void setCategoryDesc(java.lang.String categoryDesc) {
        this.categoryDesc = categoryDesc;
    }


    /**
     * Gets the name value for this PartnerType.
     * 
     * @return name
     */
    public java.lang.String getName() {
        return name;
    }


    /**
     * Sets the name value for this PartnerType.
     * 
     * @param name
     */
    public void setName(java.lang.String name) {
        this.name = name;
    }


    /**
     * Gets the description value for this PartnerType.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this PartnerType.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the title value for this PartnerType.
     * 
     * @return title
     */
    public java.lang.String getTitle() {
        return title;
    }


    /**
     * Sets the title value for this PartnerType.
     * 
     * @param title
     */
    public void setTitle(java.lang.String title) {
        this.title = title;
    }


    /**
     * Gets the logic value for this PartnerType.
     * 
     * @return logic
     */
    public java.lang.String getLogic() {
        return logic;
    }


    /**
     * Sets the logic value for this PartnerType.
     * 
     * @param logic
     */
    public void setLogic(java.lang.String logic) {
        this.logic = logic;
    }


    /**
     * Gets the logoURL value for this PartnerType.
     * 
     * @return logoURL
     */
    public java.lang.String getLogoURL() {
        return logoURL;
    }


    /**
     * Sets the logoURL value for this PartnerType.
     * 
     * @param logoURL
     */
    public void setLogoURL(java.lang.String logoURL) {
        this.logoURL = logoURL;
    }


    /**
     * Gets the logoSmallURL value for this PartnerType.
     * 
     * @return logoSmallURL
     */
    public java.lang.String getLogoSmallURL() {
        return logoSmallURL;
    }


    /**
     * Sets the logoSmallURL value for this PartnerType.
     * 
     * @param logoSmallURL
     */
    public void setLogoSmallURL(java.lang.String logoSmallURL) {
        this.logoSmallURL = logoSmallURL;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof PartnerType)) return false;
        PartnerType other = (PartnerType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.partnerId == other.getPartnerId() &&
            this.categoryId == other.getCategoryId() &&
            ((this.categoryDesc==null && other.getCategoryDesc()==null) || 
             (this.categoryDesc!=null &&
              this.categoryDesc.equals(other.getCategoryDesc()))) &&
            ((this.name==null && other.getName()==null) || 
             (this.name!=null &&
              this.name.equals(other.getName()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.title==null && other.getTitle()==null) || 
             (this.title!=null &&
              this.title.equals(other.getTitle()))) &&
            ((this.logic==null && other.getLogic()==null) || 
             (this.logic!=null &&
              this.logic.equals(other.getLogic()))) &&
            ((this.logoURL==null && other.getLogoURL()==null) || 
             (this.logoURL!=null &&
              this.logoURL.equals(other.getLogoURL()))) &&
            ((this.logoSmallURL==null && other.getLogoSmallURL()==null) || 
             (this.logoSmallURL!=null &&
              this.logoSmallURL.equals(other.getLogoSmallURL())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getPartnerId();
        _hashCode += getCategoryId();
        if (getCategoryDesc() != null) {
            _hashCode += getCategoryDesc().hashCode();
        }
        if (getName() != null) {
            _hashCode += getName().hashCode();
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getTitle() != null) {
            _hashCode += getTitle().hashCode();
        }
        if (getLogic() != null) {
            _hashCode += getLogic().hashCode();
        }
        if (getLogoURL() != null) {
            _hashCode += getLogoURL().hashCode();
        }
        if (getLogoSmallURL() != null) {
            _hashCode += getLogoSmallURL().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(PartnerType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "PartnerType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partnerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "partnerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "categoryId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryDesc");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "categoryDesc"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("name");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "name"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("title");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "title"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logic");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "logic"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logoURL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "logoURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("logoSmallURL");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "logoSmallURL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
