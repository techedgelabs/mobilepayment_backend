/**
 * RequestedAwardType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.types;

public class RequestedAwardType  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 8525137042176369323L;

    private int orderId;

    private AwardType awards;

    private long timestampReedem;

    private boolean cancelable;

    public RequestedAwardType() {
    }

    public RequestedAwardType(
           int orderId,
           AwardType awards,
           long timestampReedem,
           boolean cancelable) {
           this.orderId = orderId;
           this.awards = awards;
           this.timestampReedem = timestampReedem;
           this.cancelable = cancelable;
    }


    /**
     * Gets the orderId value for this RequestedAwardType.
     * 
     * @return orderId
     */
    public int getOrderId() {
        return orderId;
    }


    /**
     * Sets the orderId value for this RequestedAwardType.
     * 
     * @param orderId
     */
    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }


    /**
     * Gets the awards value for this RequestedAwardType.
     * 
     * @return awards
     */
    public AwardType getAwards() {
        return awards;
    }


    /**
     * Sets the awards value for this RequestedAwardType.
     * 
     * @param awards
     */
    public void setAwards(AwardType awards) {
        this.awards = awards;
    }


    /**
     * Gets the timestampReedem value for this RequestedAwardType.
     * 
     * @return timestampReedem
     */
    public long getTimestampReedem() {
        return timestampReedem;
    }


    /**
     * Sets the timestampReedem value for this RequestedAwardType.
     * 
     * @param timestampReedem
     */
    public void setTimestampReedem(long timestampReedem) {
        this.timestampReedem = timestampReedem;
    }


    /**
     * Gets the cancelable value for this RequestedAwardType.
     * 
     * @return cancelable
     */
    public boolean isCancelable() {
        return cancelable;
    }


    /**
     * Sets the cancelable value for this RequestedAwardType.
     * 
     * @param cancelable
     */
    public void setCancelable(boolean cancelable) {
        this.cancelable = cancelable;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RequestedAwardType)) return false;
        RequestedAwardType other = (RequestedAwardType) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.orderId == other.getOrderId() &&
            ((this.awards==null && other.getAwards()==null) || 
             (this.awards!=null &&
              this.awards.equals(other.getAwards()))) &&
            this.timestampReedem == other.getTimestampReedem() &&
            this.cancelable == other.isCancelable();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getOrderId();
        if (getAwards() != null) {
            _hashCode += getAwards().hashCode();
        }
        _hashCode += new Long(getTimestampReedem()).hashCode();
        _hashCode += (isCancelable() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RequestedAwardType.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "RequestedAwardType"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("orderId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "orderId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("awards");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "awards"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "AwardType"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("timestampReedem");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "timestampReedem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cancelable");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "cancelable"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
