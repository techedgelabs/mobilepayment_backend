/**
 * GetCatalogResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.elements;

import com.techedge.mp.dwh.adapter.types.AwardType;

public class GetCatalogResponse  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -7661176478874026968L;

    private AwardType[] awards;

    private java.lang.String statusCode;

    private java.lang.String messageCode;

    public GetCatalogResponse() {
    }

    public GetCatalogResponse(
           AwardType[] awards,
           java.lang.String statusCode,
           java.lang.String messageCode) {
           this.awards = awards;
           this.statusCode = statusCode;
           this.messageCode = messageCode;
    }


    /**
     * Gets the awards value for this GetCatalogResponse.
     * 
     * @return awards
     */
    public AwardType[] getAwards() {
        return awards;
    }


    /**
     * Sets the awards value for this GetCatalogResponse.
     * 
     * @param awards
     */
    public void setAwards(AwardType[] awards) {
        this.awards = awards;
    }

    public AwardType getAwards(int i) {
        return this.awards[i];
    }

    public void setAwards(int i, AwardType _value) {
        this.awards[i] = _value;
    }


    /**
     * Gets the statusCode value for this GetCatalogResponse.
     * 
     * @return statusCode
     */
    public java.lang.String getStatusCode() {
        return statusCode;
    }


    /**
     * Sets the statusCode value for this GetCatalogResponse.
     * 
     * @param statusCode
     */
    public void setStatusCode(java.lang.String statusCode) {
        this.statusCode = statusCode;
    }


    /**
     * Gets the messageCode value for this GetCatalogResponse.
     * 
     * @return messageCode
     */
    public java.lang.String getMessageCode() {
        return messageCode;
    }


    /**
     * Sets the messageCode value for this GetCatalogResponse.
     * 
     * @param messageCode
     */
    public void setMessageCode(java.lang.String messageCode) {
        this.messageCode = messageCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCatalogResponse)) return false;
        GetCatalogResponse other = (GetCatalogResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.awards==null && other.getAwards()==null) || 
             (this.awards!=null &&
              java.util.Arrays.equals(this.awards, other.getAwards()))) &&
            ((this.statusCode==null && other.getStatusCode()==null) || 
             (this.statusCode!=null &&
              this.statusCode.equals(other.getStatusCode()))) &&
            ((this.messageCode==null && other.getMessageCode()==null) || 
             (this.messageCode!=null &&
              this.messageCode.equals(other.getMessageCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAwards() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAwards());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAwards(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getStatusCode() != null) {
            _hashCode += getStatusCode().hashCode();
        }
        if (getMessageCode() != null) {
            _hashCode += getMessageCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetCatalogResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", ">getCatalogResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("awards");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "awards"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "AwardType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "statusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "messageCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
