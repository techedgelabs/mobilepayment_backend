/**
 * CheckLegacyPasswordPlusResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.elements;

import com.techedge.mp.dwh.adapter.types.UserDetailType;

public class CheckLegacyPasswordPlusResponse  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 7112642415388274588L;

    private java.lang.String statusCode;

    private java.lang.String messageCode;

    private UserDetailType userDetail;

    public CheckLegacyPasswordPlusResponse() {
    }

    public CheckLegacyPasswordPlusResponse(
           java.lang.String statusCode,
           java.lang.String messageCode,
           UserDetailType userDetail) {
           this.statusCode = statusCode;
           this.messageCode = messageCode;
           this.userDetail = userDetail;
    }


    /**
     * Gets the statusCode value for this CheckLegacyPasswordPlusResponse.
     * 
     * @return statusCode
     */
    public java.lang.String getStatusCode() {
        return statusCode;
    }


    /**
     * Sets the statusCode value for this CheckLegacyPasswordPlusResponse.
     * 
     * @param statusCode
     */
    public void setStatusCode(java.lang.String statusCode) {
        this.statusCode = statusCode;
    }


    /**
     * Gets the messageCode value for this CheckLegacyPasswordPlusResponse.
     * 
     * @return messageCode
     */
    public java.lang.String getMessageCode() {
        return messageCode;
    }


    /**
     * Sets the messageCode value for this CheckLegacyPasswordPlusResponse.
     * 
     * @param messageCode
     */
    public void setMessageCode(java.lang.String messageCode) {
        this.messageCode = messageCode;
    }


    /**
     * Gets the userDetail value for this CheckLegacyPasswordPlusResponse.
     * 
     * @return userDetail
     */
    public UserDetailType getUserDetail() {
        return userDetail;
    }


    /**
     * Sets the userDetail value for this CheckLegacyPasswordPlusResponse.
     * 
     * @param userDetail
     */
    public void setUserDetail(UserDetailType userDetail) {
        this.userDetail = userDetail;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CheckLegacyPasswordPlusResponse)) return false;
        CheckLegacyPasswordPlusResponse other = (CheckLegacyPasswordPlusResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.statusCode==null && other.getStatusCode()==null) || 
             (this.statusCode!=null &&
              this.statusCode.equals(other.getStatusCode()))) &&
            ((this.messageCode==null && other.getMessageCode()==null) || 
             (this.messageCode!=null &&
              this.messageCode.equals(other.getMessageCode()))) &&
            ((this.userDetail==null && other.getUserDetail()==null) || 
             (this.userDetail!=null &&
              this.userDetail.equals(other.getUserDetail())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getStatusCode() != null) {
            _hashCode += getStatusCode().hashCode();
        }
        if (getMessageCode() != null) {
            _hashCode += getMessageCode().hashCode();
        }
        if (getUserDetail() != null) {
            _hashCode += getUserDetail().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CheckLegacyPasswordPlusResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", ">checkLegacyPasswordPlusResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "statusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "messageCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("userDetail");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "userDetail"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "UserDetailType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
