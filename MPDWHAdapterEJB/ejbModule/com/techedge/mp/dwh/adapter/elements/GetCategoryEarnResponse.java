/**
 * GetCategoryEarnResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.elements;

import com.techedge.mp.dwh.adapter.types.CategoryEarnType;

public class GetCategoryEarnResponse  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 6381868711060886568L;

    private CategoryEarnType[] categoryEarn;

    private java.lang.String statusCode;

    private java.lang.String messageCode;

    public GetCategoryEarnResponse() {
    }

    public GetCategoryEarnResponse(
           CategoryEarnType[] categoryEarn,
           java.lang.String statusCode,
           java.lang.String messageCode) {
           this.categoryEarn = categoryEarn;
           this.statusCode = statusCode;
           this.messageCode = messageCode;
    }


    /**
     * Gets the categoryEarn value for this GetCategoryEarnResponse.
     * 
     * @return categoryEarn
     */
    public CategoryEarnType[] getCategoryEarn() {
        return categoryEarn;
    }


    /**
     * Sets the categoryEarn value for this GetCategoryEarnResponse.
     * 
     * @param categoryEarn
     */
    public void setCategoryEarn(CategoryEarnType[] categoryEarn) {
        this.categoryEarn = categoryEarn;
    }

    public CategoryEarnType getCategoryEarn(int i) {
        return this.categoryEarn[i];
    }

    public void setCategoryEarn(int i, CategoryEarnType _value) {
        this.categoryEarn[i] = _value;
    }


    /**
     * Gets the statusCode value for this GetCategoryEarnResponse.
     * 
     * @return statusCode
     */
    public java.lang.String getStatusCode() {
        return statusCode;
    }


    /**
     * Sets the statusCode value for this GetCategoryEarnResponse.
     * 
     * @param statusCode
     */
    public void setStatusCode(java.lang.String statusCode) {
        this.statusCode = statusCode;
    }


    /**
     * Gets the messageCode value for this GetCategoryEarnResponse.
     * 
     * @return messageCode
     */
    public java.lang.String getMessageCode() {
        return messageCode;
    }


    /**
     * Sets the messageCode value for this GetCategoryEarnResponse.
     * 
     * @param messageCode
     */
    public void setMessageCode(java.lang.String messageCode) {
        this.messageCode = messageCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetCategoryEarnResponse)) return false;
        GetCategoryEarnResponse other = (GetCategoryEarnResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.categoryEarn==null && other.getCategoryEarn()==null) || 
             (this.categoryEarn!=null &&
              java.util.Arrays.equals(this.categoryEarn, other.getCategoryEarn()))) &&
            ((this.statusCode==null && other.getStatusCode()==null) || 
             (this.statusCode!=null &&
              this.statusCode.equals(other.getStatusCode()))) &&
            ((this.messageCode==null && other.getMessageCode()==null) || 
             (this.messageCode!=null &&
              this.messageCode.equals(other.getMessageCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCategoryEarn() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getCategoryEarn());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getCategoryEarn(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getStatusCode() != null) {
            _hashCode += getStatusCode().hashCode();
        }
        if (getMessageCode() != null) {
            _hashCode += getMessageCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetCategoryEarnResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", ">getCategoryEarnResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoryEarn");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "categoryEarn"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "CategoryEarnType"));
        elemField.setNillable(false);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "statusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("messageCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "messageCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
