/**
 * GetActionURLRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.dwh.adapter.elements;

public class GetActionURLRequest  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 1886675720348684249L;

    private java.lang.String codCarta;

    private java.lang.String codFiscale;

    private java.lang.String site;

    private int partnerId;

    public GetActionURLRequest() {
    }

    public GetActionURLRequest(
           java.lang.String codCarta,
           java.lang.String codFiscale,
           java.lang.String site,
           int partnerId) {
           this.codCarta = codCarta;
           this.codFiscale = codFiscale;
           this.site = site;
           this.partnerId = partnerId;
    }


    /**
     * Gets the codCarta value for this GetActionURLRequest.
     * 
     * @return codCarta
     */
    public java.lang.String getCodCarta() {
        return codCarta;
    }


    /**
     * Sets the codCarta value for this GetActionURLRequest.
     * 
     * @param codCarta
     */
    public void setCodCarta(java.lang.String codCarta) {
        this.codCarta = codCarta;
    }


    /**
     * Gets the codFiscale value for this GetActionURLRequest.
     * 
     * @return codFiscale
     */
    public java.lang.String getCodFiscale() {
        return codFiscale;
    }


    /**
     * Sets the codFiscale value for this GetActionURLRequest.
     * 
     * @param codFiscale
     */
    public void setCodFiscale(java.lang.String codFiscale) {
        this.codFiscale = codFiscale;
    }


    /**
     * Gets the site value for this GetActionURLRequest.
     * 
     * @return site
     */
    public java.lang.String getSite() {
        return site;
    }


    /**
     * Sets the site value for this GetActionURLRequest.
     * 
     * @param site
     */
    public void setSite(java.lang.String site) {
        this.site = site;
    }


    /**
     * Gets the partnerId value for this GetActionURLRequest.
     * 
     * @return partnerId
     */
    public int getPartnerId() {
        return partnerId;
    }


    /**
     * Sets the partnerId value for this GetActionURLRequest.
     * 
     * @param partnerId
     */
    public void setPartnerId(int partnerId) {
        this.partnerId = partnerId;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetActionURLRequest)) return false;
        GetActionURLRequest other = (GetActionURLRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codCarta==null && other.getCodCarta()==null) || 
             (this.codCarta!=null &&
              this.codCarta.equals(other.getCodCarta()))) &&
            ((this.codFiscale==null && other.getCodFiscale()==null) || 
             (this.codFiscale!=null &&
              this.codFiscale.equals(other.getCodFiscale()))) &&
            ((this.site==null && other.getSite()==null) || 
             (this.site!=null &&
              this.site.equals(other.getSite()))) &&
            this.partnerId == other.getPartnerId();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodCarta() != null) {
            _hashCode += getCodCarta().hashCode();
        }
        if (getCodFiscale() != null) {
            _hashCode += getCodFiscale().hashCode();
        }
        if (getSite() != null) {
            _hashCode += getSite().hashCode();
        }
        _hashCode += getPartnerId();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetActionURLRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", ">getActionURLRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codCarta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "codCarta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codFiscale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "codFiscale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("site");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "site"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("partnerId");
        elemField.setXmlName(new javax.xml.namespace.QName("http://enistationplus.serisystem.com/EniStationPlus", "partnerId"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
