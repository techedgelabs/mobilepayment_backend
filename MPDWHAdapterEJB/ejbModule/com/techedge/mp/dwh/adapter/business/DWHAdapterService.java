package com.techedge.mp.dwh.adapter.business;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import org.jboss.security.vault.SecurityVaultException;
import org.jboss.security.vault.SecurityVaultFactory;
import org.picketbox.plugins.vault.PicketBoxSecurityVault;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.dwh.adapter.client.EniStationPlusPortProxy;
import com.techedge.mp.dwh.adapter.elements.ChangeUserPasswordPlusRequest;
import com.techedge.mp.dwh.adapter.elements.ChangeUserPasswordPlusResponse;
import com.techedge.mp.dwh.adapter.elements.CheckLegacyPasswordPlusRequest;
import com.techedge.mp.dwh.adapter.elements.CheckLegacyPasswordPlusResponse;
import com.techedge.mp.dwh.adapter.elements.GetActionURLRequest;
import com.techedge.mp.dwh.adapter.elements.GetActionURLResponse;
import com.techedge.mp.dwh.adapter.elements.GetAwardRequest;
import com.techedge.mp.dwh.adapter.elements.GetAwardResponse;
import com.techedge.mp.dwh.adapter.elements.GetBrandRequest;
import com.techedge.mp.dwh.adapter.elements.GetBrandResponse;
import com.techedge.mp.dwh.adapter.elements.GetCatalogRequest;
import com.techedge.mp.dwh.adapter.elements.GetCatalogResponse;
import com.techedge.mp.dwh.adapter.elements.GetCategoryBurnRequest;
import com.techedge.mp.dwh.adapter.elements.GetCategoryBurnResponse;
import com.techedge.mp.dwh.adapter.elements.GetCategoryEarnRequest;
import com.techedge.mp.dwh.adapter.elements.GetCategoryEarnResponse;
import com.techedge.mp.dwh.adapter.elements.GetPartnerListRequest;
import com.techedge.mp.dwh.adapter.elements.GetPartnerListResponse;
import com.techedge.mp.dwh.adapter.elements.SetUserDataPlusRequest;
import com.techedge.mp.dwh.adapter.elements.SetUserDataPlusResponse;
import com.techedge.mp.dwh.adapter.interfaces.AwardDetail;
import com.techedge.mp.dwh.adapter.interfaces.BrandDetail;
import com.techedge.mp.dwh.adapter.interfaces.CategoryBurnDetail;
import com.techedge.mp.dwh.adapter.interfaces.CategoryEarnDetail;
import com.techedge.mp.dwh.adapter.interfaces.DWHAdapterResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetActionUrlResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetBrandResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCatalogResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCategoryBurnResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCategoryEarnResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetPartnerResult;
import com.techedge.mp.dwh.adapter.interfaces.DwhGetAwardResult;
import com.techedge.mp.dwh.adapter.interfaces.PartnerDetail;
import com.techedge.mp.dwh.adapter.interfaces.UserDetail;
import com.techedge.mp.dwh.adapter.types.AwardType;
import com.techedge.mp.dwh.adapter.types.BrandType;
import com.techedge.mp.dwh.adapter.types.CategoryEarnType;
import com.techedge.mp.dwh.adapter.types.CategoryType;
import com.techedge.mp.dwh.adapter.types.CredentialsType;
import com.techedge.mp.dwh.adapter.types.FlgPrivacyType;
import com.techedge.mp.dwh.adapter.types.PartnerType;
import com.techedge.mp.dwh.adapter.types.UserDetailType;
import com.techedge.mp.dwh.adapter.utilities.StatusCode;

/**
 * Session Bean implementation class DWHAdapterService
 */
@Stateless
@LocalBean
public class DWHAdapterService implements DWHAdapterServiceRemote, DWHAdapterServiceLocal {

    private final static String     PARAM_PROXY_HOST                = "PROXY_HOST";
    private final static String     PARAM_PROXY_PORT                = "PROXY_PORT";
    private final static String     PARAM_PROXY_NO_HOSTS            = "PROXY_NO_HOSTS";
    private final static String     PARAM_DWH_URL_WSDL              = "DWH_URL_WSDL";
    private final static String     PARAM_DWH_VAULT_BLOCK           = "DWH_VAULT_BLOCK";
    private final static String     PARAM_DWH_VAULT_BLOCK_CRYPT_KEY = "DWH_VAULT_BLOCK_ATTRIBUTE_KEY";

    private String                  proxyHost                       = "mpsquid.enimp.pri";
    private String                  proxyPort                       = "3128";
    private String                  proxyNoHosts                    = "localhost|127.0.0.1|*.enimp.pri";
    private String                  secretKey                       = null;

    private ParametersServiceRemote parametersService               = null;
    private LoggerServiceRemote     loggerService                   = null;

    private String                  url                             = null;

    /**
     * Default constructor.
     */
    public DWHAdapterService() {
        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.parametersService = EJBHomeCache.getInstance().getParametersService();
        }
        catch (InterfaceNotFoundException e) {
            System.err.println("Service not found: " + e.getMessage());
        }

        String vaultBlock = "";
        String vaultBlockAttributeCryptKey = "";

        try {
            this.proxyHost = parametersService.getParamValue(PARAM_PROXY_HOST);
            this.proxyPort = parametersService.getParamValue(PARAM_PROXY_PORT);
            this.proxyNoHosts = parametersService.getParamValue(PARAM_PROXY_NO_HOSTS);
            this.url = parametersService.getParamValue(PARAM_DWH_URL_WSDL);

            vaultBlock = parametersService.getParamValue(PARAM_DWH_VAULT_BLOCK);
            vaultBlockAttributeCryptKey = parametersService.getParamValue(PARAM_DWH_VAULT_BLOCK_CRYPT_KEY);
        }
        catch (ParameterNotFoundException e) {
            System.err.println("Parameter not found: " + e.getMessage());
        }

        try {
            PicketBoxSecurityVault securityVault = (PicketBoxSecurityVault) SecurityVaultFactory.get();
            System.out.println("VAULT IS INITIALIZED: " + securityVault.isInitialized());

            if (securityVault.isInitialized()) {

                try {
                    this.secretKey = new String(securityVault.retrieve(vaultBlock, vaultBlockAttributeCryptKey, new byte[] { 1 })).trim();
                }
                catch (IllegalArgumentException ex) {
                    System.err.println("Error in security vault: " + ex.getMessage());
                }
            }
            else {
                System.err.println("VAULT NOT INITIALIZED!!!");
            }
        }
        catch (SecurityVaultException e) {
            System.err.println("Error in security vault: " + e.getMessage());
            //e.printStackTrace();
        }

        System.out.println("VAULT BLOCK: '" + vaultBlock + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 NAME: '" + vaultBlockAttributeCryptKey + "'");
        System.out.println("VAULT BLOCK ATTRIBUTE 1 VALUE: '" + this.secretKey + "'");
    }

    @Override
    public DWHAdapterResult checkLegacyPasswordPlus(String email, String password, String requestId) {

        System.out.println("checkLegacyPasswordPlus");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("email", email));
        inputParameters.add(new Pair<String, String>("password", password));
        inputParameters.add(new Pair<String, String>("requestId", requestId));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkLegacyPasswordPlus", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        proxy.setHttp();

        EncryptionAES encryption = new EncryptionAES();
        DWHAdapterResult result = new DWHAdapterResult();
        String passwordEncrypted = null;

        try {
            encryption.loadSecretKey(this.secretKey);
            passwordEncrypted = new String(encryption.encrypt(password));
        }
        catch (Exception e) {
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "checkLegacyPasswordPlus", requestId, "", "Encryption password error: " + e.getMessage());
            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
            result.setMessageCode(e.getMessage());
        }

        if (passwordEncrypted != null) {

            try {
                EniStationPlusPortProxy port = new EniStationPlusPortProxy(this.url);

                CredentialsType credentials = new CredentialsType();
                credentials.setEmail(email);
                credentials.setPassword(passwordEncrypted);

                CheckLegacyPasswordPlusRequest request = new CheckLegacyPasswordPlusRequest();
                request.setCredentials(credentials);
                request.setRequestId(requestId);
                CheckLegacyPasswordPlusResponse response = port.checkLegacyPasswordPlus(request);

                System.out.println("StatusCode: " + response.getStatusCode() + "   MessageCode: " + response.getMessageCode() + "   UserDetail: "
                        + (response.getUserDetail() != null ? response.getUserDetail().toString() : "null"));

                result.setStatusCode(statusCodeConverter(response.getStatusCode()));
                result.setMessageCode(response.getMessageCode());

                if (response.getUserDetail() != null) {
                    UserDetail userDetailResult = new UserDetail();
                    UserDetailType userDetailResponse = response.getUserDetail();
                    userDetailResult.setCityOfBirth(userDetailResponse.getCityOfBirth());
                    userDetailResult.setCodCarta(userDetailResponse.getCodCarta());
                    userDetailResult.setCountryOfBirth(userDetailResponse.getCountryOfBirth());
                    userDetailResult.setDateOfBirth(new Date(userDetailResponse.getDateOfBirth()));
                    userDetailResult.setFiscalCode(userDetailResponse.getFiscalCode());
                    userDetailResult.setGender(userDetailResponse.getGender());
                    userDetailResult.setMobilePhoneNumber(userDetailResponse.getMobilePhoneNumber());
                    userDetailResult.setName(userDetailResponse.getName());
                    userDetailResult.setSurname(userDetailResponse.getSurname());
                    if (userDetailResponse.isFlgBlacklist()) {
                        userDetailResult.setFlagBlackList(Boolean.TRUE);
                    }
                    else {
                        userDetailResult.setFlagBlackList(Boolean.FALSE);
                    }

                    for (FlgPrivacyType privacyType : userDetailResponse.getFlgPrivacy()) {
                        userDetailResult.getFlagPrivacy().put(privacyType.getName(), privacyType.isValue());
                    }

                    result.setUserDetail(userDetailResult);
                }
            }
            catch (Exception ex) {
                this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "checkLegacyPasswordPlus", requestId, "", ex.getMessage());
                result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
                result.setMessageCode(ex.getMessage());
            }
        }

        /*
        result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SUCCESS);
        result.setMessageCode("Operazione eseguita con successo");

        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        UserDetail userDetailResult = new UserDetail();
        userDetailResult.setCityOfBirth("Atri");
        userDetailResult.setCodCarta("0123456789");
        userDetailResult.setCountryOfBirth("Italia");
        try {
            userDetailResult.setDateOfBirth(sdf.parse("14-05-1975"));
        }
        catch (ParseException ex) {
            System.err.println(ex.getMessage());
        }

        userDetailResult.setFiscalCode("DRZGNN75E14A488H");
        userDetailResult.setGender("M");
        userDetailResult.setMobilePhoneNumber("344 2074843");
        userDetailResult.setName("Giovanni");
        userDetailResult.setSurname("D'Orazio");

        userDetailResult.getFlagPrivacy().put("INVIO_COMUNICAZIONI_ENI_1", false);
        userDetailResult.getFlagPrivacy().put("CLAUSOLE_VESSATORIE_1", true);
        userDetailResult.getFlagPrivacy().put("TERMS_CONDITIONS_1", true);
        userDetailResult.getFlagPrivacy().put("PRIVACY_1", true);

        result.setUserDetail(userDetailResult);
        */
        proxy.unsetHttp();

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();

        String resultString = "{ " + "statusCode: " + result.getStatusCode() + ", messageCode: " + result.getMessageCode() + ", userDetail: ";

        if (result.getUserDetail() != null) {
            UserDetail userDetail = result.getUserDetail();
            String userDetailString = "{ " + "name: " + userDetail.getName() + ", surname: " + userDetail.getSurname() + ", dateOfBirth: " + userDetail.getDateOfBirth()
                    + ", gender: " + userDetail.getGender() + ", fiscalCode: " + userDetail.getFiscalCode() + ", cityOfBirth: " + userDetail.getCityOfBirth()
                    + ", countryOfBirth: " + userDetail.getCountryOfBirth() + ", mobilePhoneNumber: " + userDetail.getMobilePhoneNumber() + ", codCarta: "
                    + userDetail.getCodCarta();

            boolean first = true;

            for (String key : userDetail.getFlagPrivacy().keySet()) {
                userDetailString += (!first ? ", " : "") + key + ": " + userDetail.getFlagPrivacy().get(key);

                if (first) {
                    first = false;
                }
            }

            userDetailString += " }";
            resultString += userDetailString;
        }
        else {
            resultString += "null";
        }

        resultString += " }";
        outParameters.add(new Pair<String, String>("result", resultString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkLegacyPasswordPlus", requestId, "closing", ActivityLog.createLogMessage(outParameters));

        return result;
    }

    @Override
    public DWHAdapterResult setUserDataPlus(String email, String password, String name, String surname, String gender, String fiscalCode, Date dateOfBirth, String cityOfBirth,
            String countryOfBirth, String mobilePhoneNumber, String codCarta, HashMap<String, Boolean> flagPrivacy, String requestId, Long requestTimestamp, Boolean flgBlacklist,
            String campaignType, Boolean isSocial, String socialType, String idSocial) {

        System.out.println("setUserDataPlus");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        String privacy = "{ ";
        boolean first = true;

        for (String key : flagPrivacy.keySet()) {
            privacy += (!first ? "," : "") + key + ": " + flagPrivacy.get(key);

            if (first) {
                first = false;
            }
        }

        privacy += " }";

        inputParameters.add(new Pair<String, String>("email", email));
        inputParameters.add(new Pair<String, String>("password", password));
        inputParameters.add(new Pair<String, String>("name", name));
        inputParameters.add(new Pair<String, String>("surname", surname));
        inputParameters.add(new Pair<String, String>("gender", gender));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));
        inputParameters.add(new Pair<String, String>("dateOfBirth", dateOfBirth.toString()));
        inputParameters.add(new Pair<String, String>("cityOfBirth", cityOfBirth));
        inputParameters.add(new Pair<String, String>("countryOfBirth", countryOfBirth));
        inputParameters.add(new Pair<String, String>("mobilePhoneNumber", mobilePhoneNumber));
        inputParameters.add(new Pair<String, String>("codCarta", codCarta));
        inputParameters.add(new Pair<String, String>("flagPrivacy", privacy));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));
        inputParameters.add(new Pair<String, String>("flgBlacklist", flgBlacklist.toString()));
        inputParameters.add(new Pair<String, String>("campaignType", campaignType.toString()));
        inputParameters.add(new Pair<String, String>("isSocial", isSocial.toString()));
        inputParameters.add(new Pair<String, String>("socialType", socialType));
        inputParameters.add(new Pair<String, String>("idSocial", idSocial));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setUserDataPlus", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        proxy.setHttp();

        DWHAdapterResult result = new DWHAdapterResult();

        try {
            EniStationPlusPortProxy port = new EniStationPlusPortProxy(this.url);

            CredentialsType credentials = new CredentialsType();
            credentials.setEmail(email);
            credentials.setPassword(password);

            if (cityOfBirth != null && !cityOfBirth.isEmpty()) {
                cityOfBirth = cityOfBirth.trim();
            }
            if (name != null && !name.isEmpty()) {
                name = name.trim();
            }
            if (surname != null && !surname.isEmpty()) {
                surname = surname.trim();
            }

            if (countryOfBirth != null && !countryOfBirth.isEmpty()) {
                countryOfBirth = countryOfBirth.trim();
            }

            UserDetailType userDetailRequest = new UserDetailType();
            userDetailRequest.setCityOfBirth(cityOfBirth);
            userDetailRequest.setCodCarta(codCarta);
            userDetailRequest.setCountryOfBirth(countryOfBirth);
            userDetailRequest.setDateOfBirth(dateOfBirth.getTime());
            userDetailRequest.setFiscalCode(fiscalCode);
            userDetailRequest.setGender(gender);
            userDetailRequest.setMobilePhoneNumber(mobilePhoneNumber);
            userDetailRequest.setName(name);
            userDetailRequest.setSurname(surname);
            userDetailRequest.setIdSocial(idSocial);

            FlgPrivacyType[] flgPrivacyTypeArray = new FlgPrivacyType[flagPrivacy.size()];
            int i = 0;
            for (String key : flagPrivacy.keySet()) {
                FlgPrivacyType privacyType = new FlgPrivacyType();
                privacyType.setName(key);
                privacyType.setValue(flagPrivacy.get(key));
                flgPrivacyTypeArray[i++] = privacyType;
            }
            userDetailRequest.setFlgPrivacy(flgPrivacyTypeArray);

            userDetailRequest.setFlgBlacklist(flgBlacklist);
            userDetailRequest.setCampaignType(campaignType);
            
            userDetailRequest.setIsSocial(isSocial);
            userDetailRequest.setSocialType(socialType);

            SetUserDataPlusRequest request = new SetUserDataPlusRequest();
            request.setCredentials(credentials);
            request.setRequestId(requestId);
            request.setRequestTimestamp(requestTimestamp);
            request.setUserDetail(userDetailRequest);
            SetUserDataPlusResponse response = port.setUserDataPlus(request);

            result.setStatusCode(statusCodeConverter(response.getStatusCode()));
            result.setMessageCode(response.getMessageCode());
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "setUserDataPlus", requestId, "", ex.getMessage());
            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
            result.setMessageCode(ex.getMessage());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();

        String resultString = "{ " + "statusCode: " + result.getStatusCode() + ", messageCode: " + result.getMessageCode() + ", userDetail: ";

        if (result.getUserDetail() != null) {
            UserDetail userDetail = result.getUserDetail();
            String userDetailString = "{ " + "name: " + userDetail.getName() + ", surname: " + userDetail.getSurname() + ", dateOfBirth: " + userDetail.getDateOfBirth()
                    + ", gender: " + userDetail.getGender() + ", fiscalCode: " + userDetail.getFiscalCode() + ", cityOfBirth: " + userDetail.getCityOfBirth()
                    + ", countryOfBirth: " + userDetail.getCountryOfBirth() + ", mobilePhoneNumber: " + userDetail.getMobilePhoneNumber() + ", codCarta: "
                    + userDetail.getCodCarta();

            first = true;
            for (String key : userDetail.getFlagPrivacy().keySet()) {
                userDetailString += (!first ? ", " : "") + key + ": " + userDetail.getFlagPrivacy().get(key);

                if (first) {
                    first = false;
                }
            }

            userDetailString += " }";
            resultString += userDetailString;
        }
        else {
            resultString += "null";
        }

        resultString += " }";
        outParameters.add(new Pair<String, String>("result", resultString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "setUserDataPlus", requestId, "closing", ActivityLog.createLogMessage(outParameters));

        return result;
    }

    @Override
    public DWHAdapterResult changeUserPasswordPlus(String email, String password, String requestId, Long requestTimestamp) {
        System.out.println("changeUserPasswordPlus");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        inputParameters.add(new Pair<String, String>("email", email));
        inputParameters.add(new Pair<String, String>("password", password));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "changeUserPasswordPlus", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        proxy.setHttp();

        DWHAdapterResult result = new DWHAdapterResult();

        try {
            EniStationPlusPortProxy port = new EniStationPlusPortProxy(this.url);

            CredentialsType credentials = new CredentialsType();
            credentials.setEmail(email);
            credentials.setPassword(password);

            ChangeUserPasswordPlusRequest request = new ChangeUserPasswordPlusRequest();
            request.setCredentials(credentials);
            request.setRequestId(requestId);
            request.setRequestTimestamp(requestTimestamp.toString());
            ChangeUserPasswordPlusResponse response = port.changeUserPasswordPlus(request);

            result.setStatusCode(statusCodeConverter(response.getStatusCode()));
            result.setMessageCode(response.getMessageCode());
        }
        catch (Exception ex) {
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "changeUserPasswordPlus", requestId, "", ex.getMessage());
            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
            result.setMessageCode(ex.getMessage());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();

        String resultString = "{ " + "statusCode: " + result.getStatusCode() + ", messageCode: " + result.getMessageCode() + ", userDetail: ";

        if (result.getUserDetail() != null) {
            UserDetail userDetail = result.getUserDetail();
            String userDetailString = "{ " + "name: " + userDetail.getName() + ", surname: " + userDetail.getSurname() + ", dateOfBirth: " + userDetail.getDateOfBirth()
                    + ", gender: " + userDetail.getGender() + ", fiscalCode: " + userDetail.getFiscalCode() + ", cityOfBirth: " + userDetail.getCityOfBirth()
                    + ", countryOfBirth: " + userDetail.getCountryOfBirth() + ", mobilePhoneNumber: " + userDetail.getMobilePhoneNumber() + ", codCarta: "
                    + userDetail.getCodCarta();

            boolean first = false;
            for (String key : userDetail.getFlagPrivacy().keySet()) {
                userDetailString += (!first ? ", " : "") + key + ": " + userDetail.getFlagPrivacy().get(key);

                if (first) {
                    first = false;
                }
            }

            userDetailString += " }";
            resultString += userDetailString;
        }
        else {
            resultString += "null";
        }

        resultString += " }";
        outParameters.add(new Pair<String, String>("result", resultString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "changeUserPasswordPlus", requestId, "closing", ActivityLog.createLogMessage(outParameters));

        return result;
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            //this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    private String statusCodeConverter(String statusCodeWS) {
        if (statusCodeWS == null || statusCodeWS.contains("500_ERROR")) {
            return StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR;
        }

        if (statusCodeWS.contains("200_OK")) {
            return StatusCode.DWH_ADAPTER_RESULT_SUCCESS;
        }

        if (statusCodeWS.contains("210_REG_LITE")) {
            return StatusCode.DWH_ADAPTER_RESULT_REG_LITE;
        }

        if (statusCodeWS.contains("400_BAD_REQUEST")) {
            return StatusCode.DWH_ADAPTER_RESULT_BAD_REQUEST;
        }

        if (statusCodeWS.contains("400_MISSING_PARAMETERS")) {
            return StatusCode.DWH_ADAPTER_RESULT_MISSING_PARAMETERS;
        }

        if (statusCodeWS.contains("400_USER_NOT_FOUND")) {
            return StatusCode.DWH_ADAPTER_RESULT_USER_NOT_FOUND;
        }

        if (statusCodeWS.contains("400_REQUESTID_ALREADY_EXISTS")) {
            return StatusCode.DWH_ADAPTER_RESULT_REQUESTID_ALREADY_EXISTS;
        }

        if (statusCodeWS.contains("401_BAD_CREDENTIALS")) {
            return StatusCode.DWH_ADAPTER_RESULT_BAD_CREDENTIALS;
        }

        if (statusCodeWS.contains("402_INSUFFICIENT_POINT")) {
            return StatusCode.DWH_ADAPTER_RESULT_INSUFFICIENT_POINT;
        }

        if (statusCodeWS.contains("404_AWARD_NOT_FOUND")) {
            return StatusCode.DWH_ADAPTER_RESULT_AWARD_NOT_FOUND;
        }

        if (statusCodeWS.contains("412_AWARD_OUT_OF_DATE")) {
            return StatusCode.DWH_ADAPTER_RESULT_AWARD_OUT_OF_DATE;
        }

        return StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR;
    }

    @Override
    public DWHGetPartnerResult getPartnerList() {

        Date now = new Date();
        String requestId = String.valueOf(now.getTime());

        System.out.println("getPartnerList");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPartnerList", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        proxy.setHttp();

        DWHGetPartnerResult result = new DWHGetPartnerResult();

        try {
            EniStationPlusPortProxy port = new EniStationPlusPortProxy(this.url);

            GetPartnerListRequest request = new GetPartnerListRequest();
            request.setSite("AP");
            GetPartnerListResponse response = port.getPartnerList(request);

            if (response.getPartner() != null && response.getPartner().length > 0) {

                //System.out.println("result.getPartnerDetailList not empty");

                for (PartnerType partnerType : response.getPartner()) {

                    //System.out.println("partner: " + partnerType.getPartnerId());

                    PartnerDetail partnerDetail = new PartnerDetail();

                    //System.out.println("CategoryId: " + partnerType.getCategoryId());
                    partnerDetail.setCategoryId(partnerType.getCategoryId());
                    //System.out.println("Description: " + partnerType.getDescription());
                    partnerDetail.setDescription(partnerType.getDescription());
                    //System.out.println("Logic: " + partnerType.getLogic());
                    partnerDetail.setLogic(partnerType.getLogic());
                    //System.out.println("LogoSmallURL: " + partnerType.getLogoSmallURL());
                    partnerDetail.setLogoSmallUrl(partnerType.getLogoSmallURL());
                    //System.out.println("LogoURL: " + partnerType.getLogoURL());
                    partnerDetail.setLogoUrl(partnerType.getLogoURL());
                    //System.out.println("Name: " + partnerType.getName());
                    partnerDetail.setName(partnerType.getName());
                    //System.out.println("PartnerId: " + partnerType.getPartnerId());
                    partnerDetail.setPartnerId(partnerType.getPartnerId());
                    //System.out.println("Title: " + partnerType.getTitle());
                    partnerDetail.setTitle(partnerType.getTitle());

                    //System.out.println("add");
                    result.getPartnerDetailList().add(partnerDetail);
                    //System.out.println("add ok");
                }
            }

            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SUCCESS);
            result.setMessageCode(response.getMessageCode());
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getPartnerList", requestId, "", ex.getMessage());
            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
            result.setMessageCode(ex.getMessage());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();

        String resultString = "{ " + "statusCode: " + result.getStatusCode() + ", messageCode: " + result.getMessageCode() + ", partnerDetailList: ";

        if (!result.getPartnerDetailList().isEmpty()) {

            for (PartnerDetail partnerDetail : result.getPartnerDetailList()) {

                String partnerDetailString = "{ " + "categoryId: " + partnerDetail.getCategoryId() + ", description:" + partnerDetail.getDescription() + ", logic: "
                        + partnerDetail.getLogic() + ", logoSmallUrl: " + partnerDetail.getLogoSmallUrl() + ", logoUrl: " + partnerDetail.getLogoUrl() + ", name: "
                        + partnerDetail.getName() + ", partnerId: " + partnerDetail.getPartnerId() + ", title: " + partnerDetail.getTitle();

                partnerDetailString += " }";
                resultString += partnerDetailString;
            }
        }
        else {
            resultString += "null";
        }

        resultString += " }";
        outParameters.add(new Pair<String, String>("result", resultString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getPartnerList", requestId, "closing", ActivityLog.createLogMessage(outParameters));

        return result;

    }

    @Override
    public DWHGetActionUrlResult getActionUrl(String partnerId, String fiscalCode, String eanCode) {

        Date now = new Date();
        String requestId = String.valueOf(now.getTime());

        System.out.println("getActionUrl");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("partnerId", partnerId));
        inputParameters.add(new Pair<String, String>("fiscalCode", fiscalCode));
        inputParameters.add(new Pair<String, String>("eanCode", eanCode));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getActionUrl", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        proxy.setHttp();

        DWHGetActionUrlResult result = new DWHGetActionUrlResult();

        try {
            EniStationPlusPortProxy port = new EniStationPlusPortProxy(this.url);

            GetActionURLRequest request = new GetActionURLRequest();

            Integer partnerIdInteger = Integer.parseInt(partnerId);

            request.setCodCarta(eanCode);
            request.setCodFiscale(fiscalCode);
            request.setSite("AP");
            request.setPartnerId(partnerIdInteger);

            GetActionURLResponse response = port.getActionURL(request);

            result.setActionUrl(response.getUrl());
            result.setKey(response.getKey());
            result.setActionForwardId(response.getActionForwardId());
            
            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SUCCESS);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getActionUrl", requestId, "", ex.getMessage());
            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();
        outParameters.add(new Pair<String, String>("actionUrl", result.getActionUrl()));
        outParameters.add(new Pair<String, String>("statusCode", result.getStatusCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getActionUrl", requestId, "closing", ActivityLog.createLogMessage(outParameters));

        return result;

    }

    @Override
    public DWHGetCatalogResult getCatalog(String card, String requestId, Long requestTimestamp) {

        System.out.println("getCatalogList");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("requestTimestamp", String.valueOf(requestTimestamp)));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCatalogList", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        proxy.setHttp();

        DWHGetCatalogResult result = new DWHGetCatalogResult();

        try {
            EniStationPlusPortProxy port = new EniStationPlusPortProxy(this.url);

            GetCatalogRequest request = new GetCatalogRequest();

            request.setSite("AP");
            request.setCard(card);
            request.setRequestId(requestId);
            request.setRequestTimestamp(requestTimestamp);

            GetCatalogResponse response = port.getCatalog(request);

            result.setStatusCode(statusCodeConverter(response.getStatusCode()));

            if (response.getAwards() != null && response.getAwards().length > 0) {

                for (AwardType awardType : response.getAwards()) {

                    BigDecimal value        = new BigDecimal(awardType.getValue()).setScale(2,BigDecimal.ROUND_HALF_UP);
                    BigDecimal contribution = new BigDecimal(awardType.getContribution()).setScale(2,BigDecimal.ROUND_HALF_UP);
                    
                    AwardDetail awardDetail = new AwardDetail();

                    awardDetail.setAwardId(awardType.getIdAward());
                    awardDetail.setCategoryId(awardType.getCategoryId());
                    awardDetail.setCategory(awardType.getCategory());
                    awardDetail.setBrandId(awardType.getBrandId());
                    awardDetail.setBrand(awardType.getBrand());
                    awardDetail.setDescription(awardType.getDescription());
                    awardDetail.setDescriptionShort(awardType.getDescriptionShort());
                    awardDetail.setPoint(awardType.getPoint());
                    awardDetail.setContribution(contribution);
                    awardDetail.setDateStartRedemption(awardType.getDateStartRedemption());
                    awardDetail.setDateStopRedemption(awardType.getDateStopRedemption());
                    awardDetail.setCancelable(awardType.isCancelable());
                    awardDetail.setTangible(awardType.isTangible());
                    awardDetail.setType(awardType.getType());
                    awardDetail.setRedeemable(awardType.getRedeemable());
                    awardDetail.setUrlImageAward(awardType.getUrlImageAward());
                    awardDetail.setValue(value);
                    awardDetail.setPointPartner(awardType.getPointPartner());
                    awardDetail.setChannelRedemption(awardType.getChannelRedemption());
                    awardDetail.setTypePointPartner(awardType.getTypePointPartner());

                    //System.out.println("PointPartner:      " + awardType.getPointPartner());
                    //System.out.println("ChannelRedemption: " + awardType.getChannelRedemption());
                    
                    result.getAwardDetailList().add(awardDetail);
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getCatalogList", requestId, "", ex.getMessage());
            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();

        /*
        String resultString = "{ " + "statusCode: " + result.getStatusCode() + ", messageCode: " + result.getMessageCode() + ", catalogDetailList: ";

        if (!result.getAwardDetailList().isEmpty()) {

            for (AwardDetail awardDetail : result.getAwardDetailList()) {

                String awardDetailString = "{ " + "awardId: " + awardDetail.getAwardId() + ", categoryId:" + awardDetail.getCategoryId() + ", category:" + awardDetail.getCategory() 
                        + ", brandId: " + awardDetail.getBrandId() + ", brand: " + awardDetail.getBrand()
                        + ", description: " + awardDetail.getDescription() + ", descriptionShort: " + awardDetail.getDescriptionShort() + ", point: " + awardDetail.getPoint()
                        + ", contribution: " + awardDetail.getContribution() + ", dateStartRedemption: " + awardDetail.getDateStartRedemption() + ", dateStopRedemption: "
                        + awardDetail.getDateStopRedemption() + ", cancelable: " + awardDetail.getCancelable() + ", tangible: " + awardDetail.getTangible() + ", type: "
                        + awardDetail.getType() + ", redeemable: " + awardDetail.getRedeemable() + ", urlImageAward: " + awardDetail.getUrlImageAward();;

                awardDetailString += " }";
                resultString += awardDetailString;
            }
        }
        else {
            resultString += "null";
        }
        resultString += " }";
        
        outParameters.add(new Pair<String, String>("result", resultString));
        */
        
        outParameters.add(new Pair<String, String>("statusCode", result.getStatusCode()));
        outParameters.add(new Pair<String, String>("messageCode", result.getMessageCode()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCatalogList", requestId, "closing", ActivityLog.createLogMessage(outParameters));

        return result;
    }

    @Override
    public DWHGetBrandResult getBrandList(String requestId, Long requestTimestamp) {

        System.out.println("getBrandList");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getBrandList", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        proxy.setHttp();

        DWHGetBrandResult result = new DWHGetBrandResult();
        try {

            EniStationPlusPortProxy port = new EniStationPlusPortProxy(this.url);

            GetBrandRequest request = new GetBrandRequest();

            request.setRequestId(requestId);
            request.setRequestTimestamp(requestTimestamp);
            request.setSite("AP");
            
            GetBrandResponse response = port.getBrand(request);

            result.setStatusCode(statusCodeConverter(response.getStatusCode()));

            if (response.getBrand() != null && response.getBrand().length > 0) {
                for (BrandType brandType : response.getBrand()) {

                    BrandDetail brandDetailElemenent = new BrandDetail();

                    brandDetailElemenent.setBrand(brandType.getBrand());
                    brandDetailElemenent.setBrandId(brandType.getBrandId());
                    brandDetailElemenent.setBrandUrl(brandType.getUrlBrand());

                    result.getBrandDetailList().add(brandDetailElemenent);
                }
            }

        }

        catch (Exception ex) {
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getBrandList", "requestId_parameter missing", "", ex.getMessage());
            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
            result.setMessageCode(ex.getMessage());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();

        String resultString = "{ " + "statusCode: " + result.getStatusCode() + ", messageCode: " + result.getMessageCode() + ", brandDetailList: ";

        if (!result.getBrandDetailList().isEmpty()) {

            for (BrandDetail brandDetail : result.getBrandDetailList()) {

                String brandDetailString = "{ " + "brandId: " + brandDetail.getBrandId() + ", brand:" + brandDetail.getBrand() + ", brandUrl: " + brandDetail.getBrandUrl();
                brandDetailString += " }";
                resultString += brandDetailString;
            }
        }
        else {
            resultString += "null";
        }

        resultString += " }";
        outParameters.add(new Pair<String, String>("result", resultString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getBrandList", requestId, "closing", ActivityLog.createLogMessage(outParameters));

        return result;
    }

    @Override
    public DWHGetCategoryEarnResult getCategoryEarnList() {

        System.out.println("getCategoryEarnList");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCategoryEarnList", "", "opening", ActivityLog.createLogMessage(inputParameters));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        proxy.setHttp();

        DWHGetCategoryEarnResult result = new DWHGetCategoryEarnResult();

        try {

            EniStationPlusPortProxy port = new EniStationPlusPortProxy(this.url);

            GetCategoryEarnRequest request = new GetCategoryEarnRequest();
            
            request.setSite("AP");

            GetCategoryEarnResponse response = port.getCategoryEarn(request);

            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SUCCESS);

            if (response.getCategoryEarn() != null && response.getCategoryEarn().length > 0) {
                for (CategoryEarnType categoryEarnType : response.getCategoryEarn()) {

                    CategoryEarnDetail categoryEarnDetail = new CategoryEarnDetail();

                    categoryEarnDetail.setCategory(categoryEarnType.getCategoryDesc());
                    categoryEarnDetail.setCategoryId(categoryEarnType.getCategoryId());
                    categoryEarnDetail.setCategoryImageUrl(categoryEarnType.getUrlCategory());

                    result.getCategoryEarnList().add(categoryEarnDetail);

                }
            }
        }

        catch (Exception ex) {
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getCategoryEarnList", "requestId_parameter missing", "", ex.getMessage());
            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
            result.setMessageCode(ex.getMessage());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();

        String resultString = "{ " + "statusCode: " + result.getStatusCode() + ", messageCode: " + result.getMessageCode() + ", categoryEarnDetailList: ";

        if (!result.getCategoryEarnList().isEmpty()) {

            for (CategoryEarnDetail categoryEarnDetail : result.getCategoryEarnList()) {

                String categoryEarnDetailString = "{ " + "categoryId: " + categoryEarnDetail.getCategoryId() + ", category:" + categoryEarnDetail.getCategory()
                        + ", categoryImageUrl:" + categoryEarnDetail.getCategoryImageUrl();

                categoryEarnDetailString += " }";
                resultString += categoryEarnDetailString;
            }
        }
        else {
            resultString += "null";
        }

        resultString += " }";
        outParameters.add(new Pair<String, String>("result", resultString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCategoryEarnList", "", "closing", ActivityLog.createLogMessage(outParameters));

        return result;
    }

    @Override
    public DWHGetCategoryBurnResult getCategoryBurnList(String requestId, Long requestTimestamp) {

        System.out.println("getCategoryBurnList");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCategoryBurnList", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        proxy.setHttp();

        DWHGetCategoryBurnResult result = new DWHGetCategoryBurnResult();

        try {

            EniStationPlusPortProxy port = new EniStationPlusPortProxy(this.url);

            GetCategoryBurnRequest request = new GetCategoryBurnRequest();

            request.setRequestId(requestId);
            request.setRequestTimestamp(requestTimestamp);
            request.setSite("AP");
            
            GetCategoryBurnResponse response = port.getCategoryBurn(request);

            result.setStatusCode(statusCodeConverter(response.getStatusCode()));

            if (response.getCategory() != null && response.getCategory().length > 0) {
                for (CategoryType categoryBurnType : response.getCategory()) {

                    CategoryBurnDetail categoryBurnDetail = new CategoryBurnDetail();

                    categoryBurnDetail.setCategory(categoryBurnType.getCategory());
                    categoryBurnDetail.setCategoryId(categoryBurnType.getCategoryId());
                    categoryBurnDetail.setCategoryImageUrl(categoryBurnType.getUrlCategory());

                    result.getCategoryBurnList().add(categoryBurnDetail);

                }
            }
        }

        catch (Exception ex) {
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getCategoryBurnList", "requestId_parameter missing", "", ex.getMessage());
            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
            result.setMessageCode(ex.getMessage());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();

        String resultString = "{ " + "statusCode: " + result.getStatusCode() + ", messageCode: " + result.getMessageCode() + ", categoryBurnDetailList: ";

        if (!result.getCategoryBurnList().isEmpty()) {

            for (CategoryBurnDetail categoryBurnDetail : result.getCategoryBurnList()) {

                String categoryBurncDetailString = "{ " + "categoryId: " + categoryBurnDetail.getCategoryId() + ", category:" + categoryBurnDetail.getCategory()
                        + ", categoryImageUrl:" + categoryBurnDetail.getCategoryImageUrl();

                categoryBurncDetailString += " }";
                resultString += categoryBurncDetailString;
            }
        }
        else {
            resultString += "null";
        }

        resultString += " }";
        outParameters.add(new Pair<String, String>("result", resultString));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getCategoryBurnList", requestId, "closing", ActivityLog.createLogMessage(outParameters));

        return result;
    }

    @Override
    public DwhGetAwardResult getAwardResult(Integer awardId, String card, String cardPartner, String requestId, Long requestTimestamp) {

        System.out.println("getAwardResult");

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("awardId", awardId.toString()));
        inputParameters.add(new Pair<String, String>("card", card));
        inputParameters.add(new Pair<String, String>("cardPartner", cardPartner));
        inputParameters.add(new Pair<String, String>("requestId", requestId));
        inputParameters.add(new Pair<String, String>("requestTimestamp", requestTimestamp.toString()));

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getAwardResult", requestId, "opening", ActivityLog.createLogMessage(inputParameters));

        Proxy proxy = new Proxy(this.proxyHost, this.proxyPort, this.proxyNoHosts);

        proxy.setHttp();

        DwhGetAwardResult result = new DwhGetAwardResult();

        try {
            EniStationPlusPortProxy port = new EniStationPlusPortProxy(this.url);

            GetAwardRequest request = new GetAwardRequest();

            request.setCard(card);
            request.setCardPartner(cardPartner);
            request.setRequestId(requestId);
            request.setIdAward(awardId);
            request.setRequestTimestamp(requestTimestamp);
            request.setSite("AP");

            GetAwardResponse resposnse = port.getAward(request);

            result.setStatusCode(statusCodeConverter(resposnse.getStatusCode()));
            result.setMessageCode(resposnse.getMessageCode());
            if(resposnse.getOrderId()!=null && !resposnse.getOrderId().equals("")
                    && resposnse.getVoucher()!=null && !resposnse.getVoucher().equals("")){
                
                result.setOrderId((resposnse.getOrderId()));
                result.setVoucher(resposnse.getVoucher());
            }

        }

        catch (Exception ex) {
            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "getAwardResult", "requestId_parameter missing", "", ex.getMessage());
            result.setStatusCode(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR);
            result.setMessageCode(ex.getMessage());
        }

        proxy.unsetHttp();

        Set<Pair<String, String>> outParameters = new HashSet<Pair<String, String>>();
        outParameters.add(new Pair<String, String>("MessageCode", result.getMessageCode()));
        outParameters.add(new Pair<String, String>("statusCode", result.getStatusCode()));
        if(result.getOrderId()!=null && result.getVoucher()!=null){
            outParameters.add(new Pair<String, String>("orderId", result.getOrderId().toString()));
            outParameters.add(new Pair<String, String>("Voucher", result.getVoucher()));
        }

        this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "getAwardResult", requestId, "closing", ActivityLog.createLogMessage(outParameters));

        return result;

    }

}
