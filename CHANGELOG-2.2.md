CHANGELOG for 2.2.x
===================

This changelog references the relevant changes (bug and security fixes) done
in 2.2 minor versions.

* 2.2.25 (2017-05-24)

 * Implementazione servizio di backoffice per cancellazione righe database

* 2.2.24 (2017-05-17)

 * Integrazione Apple Pay

* 2.2.23 (2017-05-13)

 * Implementazione servizi updatePrePaidConsumeVoucher e addPrePaicConsumeVoucherDetail

* 2.2.22 (2017-05-08)

 * Modifica scheduler per assegnazione voucher di benvenuto a richiesta

* 2.2.21 (2017-04-03)

 * Implementazione controllo corrispondenza codice fiscale con dati anagrafici
 * Gestione concorrenza su servizio di conferma transazione per flag di notifica a GFG
 * Fix in gestione casistica duplicazione chiamata cancellazione autorizzazione pagamento in vecchio flusso (refueling)

* 2.2.20 (2017-03-09)

 * Fix bug su ricerca utente via email
 * Implementazione servizio di backoffice per verifica stato transazioni sul gestionale
 * Implementazione servizio di backoffice per forzare l'aggiornamento dei parametri di sistema senza effettuare il riavvio delle macchine

* 2.2.19 (2017-02-24)

 * Fix bug riconciliazione transazioni in stato ERROR per stato finale diverso da REFUEL_ENDED_202
 * Modifica programma generazione allegati per leggere png header in path locale

* 2.2.18 (2017-02-14)

 * Creato nuovo stato per gestire gli utenti che richiedono la cancellazione della propria utenza
 * Implementazione servizio per eliminazione numero di telefono
 * Modificato flusso per associazione voucher di benvenuto; i voucher sono associati mediante un job schedulato
 * Modificato allegato excel del report di riconciliazione per dare maggiori informazioni sul tipo di riconciliazione effettuata e sull'esito
 * Eliminazione check su timestamp acquisto voucher nelle transazioni in modalità postpaid
 * Bugfix vari sul processo di riconciliazione

* 2.2.17 (2017-01-30)

 * Creazione tabella province e implementazione servizio di caricamento dati

* 2.2.16b (2017-01-27)

 * Implementazione servizi di check connessione verso sistemi Quenit e Fortech
 * Estensione servizio di backoffice di aggiornamento utente per campi anagrafica

* 2.2.16a (2017-01-24)

 * Fix job di recupero assegnazione welcome voucher per utenti senza carta di credito

* 2.2.16 (2017-01-18)

 * Creazione servizio di backoffice per cancellazione voucher
 * Gestione errore caricamento punti loyalty in servizio di approvazione transazione postpaid

* 2.2.15 (2017-01-16)

 * Fix vari sul processo di riconciliazione

* 2.2.14 (2017-01-13)

 * Creazione servizio di backoffice per modificare lo stato dei voucher associati agli utenti
 * Fix soggetto email di riconciliazione per data errata
 * Fix vari su formato allegato email di riconciliazione

* 2.2.13b (2017-01-13)

 * Estensione servizio di backoffice per modifica stato transazioni postpaid

* 2.2.13a (2017-01-12)

 * Fix testi email di fine rifornimento

* 2.2.13 (2017-01-11)

 * Aggiornamento report giornaliero per visualizzazione utenti con e senza carta di credito
 * Fix report riconciliazione per transazioni non notificate
 * Bugfix minori

* 2.2.12b (2017-01-10)

 * Fix getSourceDetail per transazioni con lo stesso srcTransactionID

* 2.2.12a (2017-01-05)

 * Servizi di backoffice per aggiornamento transazioni postpaid e voucher

* 2.2.12 (2017-01-03)

 * Fix per errore pagamento voucher duplicato

* 2.2.11b (2017-01-02)

 * Fix estrazione voucher consumati associati a promo non attive su report statistiche

* 2.2.11a (2016-12-31)

 * Fix riconciliazione transazioni con timeout in notifica pagamento

* 2.2.11 (2016-12-31)

 * Associazione welcome voucher anche in caso di skip inserimento carta di pagamento

* 2.2.10 (2016-12-23)

 * Blocco iscrizione con domini non sicuri

* 2.2.9 (2016-12-23)

 * Possibilità di inserire un numero di telefono di un utente che non ha ancora verificato l'email e check telefono già utilizzato sulla verifica dell'email
 * Blocco utilizzo carte non italiane

* 2.2.8a (2016-12-15)

 * Attivazione servizio history per visualizzazione statistiche erogazioni mensili
 * Implementazione servizio per recupero assegnazione voucher promozionali a utenti
 * Bugfix minori

* 2.2.8 (2016-12-12)

 * Predisposizione servizio history per visualizzazione statistiche erogazioni mensili
 * Filtro sullo storico per visualizzare solo le transazioni concluse con successo
 * Implementazione check PV non abilitati per consumo voucher
 * Bugfix minori

* 2.2.7 (2016-12-01)

 * Funzione di backoffice per cancellazione eventi errati
 * Servizio di backoffice per assegnare un voucher a un utente
 * Bugfix minori

* 2.2.6 (2016-11-29)

 * Gestione errore per impianto non abilitato all'utilizzo dei voucher per transazioni prepaid e postpaid
 * Estensione servizio di backoffice di update user per consentire l'aggiornamento dell'email
 * Arrotondamento a 100m delle distanze dalle stazioni nella pagina Eni Station abilitate
 * Bugfix minori

* 2.2.5a (2016-11-29)

 * Differenziazione messaggio per utente bloccato per troppi tentativi di login falliti

* 2.2.5 (2016-11-25)

 * Modifica report giornaliero con nuovo algoritmo estrazione utenti attivi e eliminazione dettaglio per Roma e Milano
 * Bugfix minori

* 2.2.4 (2016-11-24)

 * Creazione job per invio email massive tramite mailing list generata dinamicamente
 * Bugfix minori

* 2.2.3 (2016-11-22)

 * Il codice di verifica del numero di telefono conterrà solo numeri
 * Bugfix minori

* 2.2.2c (2016-11-17)

 * Modifica flusso iscrizione per utilizzo metodi di pagamento precedentemente configurati
 * Bugfix minori

* 2.2.2b (2016-11-14)

 * Job di verifica sms delivery
 * Bugfix minori

* 2.2.2a (2016-11-11)

 * Fix lettura nuovo codice esercente per deposito carta utente nuovo flusso

* 2.2.2 (2016-11-08)

 * Implementazione del servizio per l'invio di SMS
 * Modifica gestione dei testi dei messaggi di errore: i testi dei messaggi saranno sempre forniti dal backend
 * Bugfix minori

* 2.2.0 (2016-10-17)

 * Implementazione del flusso di rifornimento prepaid con l'utilizzo di soli voucher
 * Estensione del flusso prepaid per consentire l'acquisto automatico di voucher in caso di credito voucher non sufficiente
 * Adeguamento del flusso di rifornimento post-paid e shop per consentire solo l'utilizzo di voucher
 * Implementazione del flusso di acquisto voucher
 * Raggruppamento dei tipi di utenti in categorie per differenziare il comportamento degli utenti per il betatest
 * Il numero di telefono è un campo obbligatorio della nuova procedura di iscrizione
 * Il pin è associato all'utente e non più al metodo di pagamento; l'inserimento del metodo di pagamento richiede la digitazione del pin precedentemente inserito
 * Possibilità di forzare la richiesta di accettazione delle condizioni di utilizzo del servizio
