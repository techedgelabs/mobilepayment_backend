package com.techedge.mp.dwh.client.service;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.scribe.exceptions.OAuthSignatureException;
import org.scribe.services.Base64Encoder;
import org.scribe.services.SignatureService;
import org.scribe.utils.OAuthEncoder;
import org.scribe.utils.Preconditions;

public class HMACSha256SignatureService implements SignatureService{

	private static final String EMPTY_STRING = "";
	  private static final String CARRIAGE_RETURN = "\r\n";
	  private static final String UTF8 = "UTF-8";
	  private static final String HMAC_SHA256 = "HmacSHA256";
	  private static final String METHOD = "HMAC-SHA256";

	  /**
	   * {@inheritDoc}
	   */
	  public String getSignature(String baseString, String apiSecret, String tokenSecret)
	  {
	    try
	    {
	      Preconditions.checkEmptyString(baseString, "Base string cant be null or empty string");
	      Preconditions.checkEmptyString(apiSecret, "Api secret cant be null or empty string");
	      return doSign(baseString, OAuthEncoder.encode(apiSecret) + '&' + OAuthEncoder.encode(tokenSecret));
	    } 
	    catch (Exception e)
	    {
	      throw new OAuthSignatureException(baseString, e);
	    }
	  }

	  private String doSign(String toSign, String keyString) throws Exception
	  {
	    SecretKeySpec key = new SecretKeySpec((keyString).getBytes(UTF8), HMAC_SHA256);
	    Mac mac = Mac.getInstance(HMAC_SHA256);
	    mac.init(key);
	    byte[] bytes = mac.doFinal(toSign.getBytes(UTF8));
	    return bytesToBase64String(bytes).replace(CARRIAGE_RETURN, EMPTY_STRING);
	  }

	  private String bytesToBase64String(byte[] bytes)
	  {
	    return Base64Encoder.getInstance().encode(bytes);
	  }

	  /**
	   * {@inheritDoc}
	   */
	  public String getSignatureMethod()
	  {
	    return METHOD;
	  }

}
