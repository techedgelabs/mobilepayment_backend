package com.techedge.mp.dwh.client.service;

import org.scribe.services.TimestampServiceImpl;

public class EniOAuthApiTimestampService extends TimestampServiceImpl {

    private String timestamp;
    private String nonce;

    public EniOAuthApiTimestampService(String timestamp, String nonce) {
        this.timestamp = timestamp;
        this.nonce = nonce;
    }

    public EniOAuthApiTimestampService() {
        this(null, null);
    }

    @Override
    public String getTimestampInSeconds() {
        if (timestamp != null && !timestamp.equals("")) {
            return timestamp;
        }
        else {
            return super.getTimestampInSeconds();
        }
    }

    @Override
    public String getNonce() {
        if (nonce != null && !nonce.equals("")) {
            return nonce;
        }
        else {
            return super.getNonce();
        }
    }
}
