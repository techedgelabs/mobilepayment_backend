package com.techedge.mp.dwh.adapter.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.scribe.model.OAuthConfig;
import org.scribe.model.OAuthConstants;
import org.scribe.model.SignatureType;
import org.scribe.model.Token;
import org.scribe.oauth.OAuth10aServiceImpl;
import org.scribe.services.TimestampService;
import org.scribe.utils.OAuthEncoder;

import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.dwh.adapter.service.utilities.DWHServletRequestWrapper;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.dwh.adapter.service.exception.OAuthAutenticationException;
import com.techedge.mp.dwh.client.EniOAuthApi;
import com.techedge.mp.dwh.client.model.EniOAuthRequest;

@WebServlet("/DWHAdapterServlet")
public class DWHAdapterServlet extends HttpServlet {

    private static final long       serialVersionUID                          = -7184039902803359832L;
    private final static String     PARAM_DWH_ADAPTER_WSDL               = "DWH_ADAPTER_WSDL";
    private final static String     PARAM_DWH_ADAPTER_OAUTH_CONSUMER_KEY = "DWH_ADAPTER_OAUTH_CONSUMER_KEY";

    private ParametersServiceRemote parametersService                         = null;
    private LoggerServiceRemote     loggerService                             = null;
    private String                  consumerSecret                            = null;
    private String                  consumerKey                               = null;
    private String                  wsdlString                                = null;
    //private String                  requestContent                            = null;

    public DWHAdapterServlet() {
        loadOAuthParams();
    }

    private void loadOAuthParams() {

        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.parametersService = EJBHomeCache.getInstance().getParametersService();

            this.wsdlString = parametersService.getParamValue(DWHAdapterServlet.PARAM_DWH_ADAPTER_WSDL);
            this.consumerKey = parametersService.getParamValue(DWHAdapterServlet.PARAM_DWH_ADAPTER_OAUTH_CONSUMER_KEY);

            File fileCert = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "oauth" + File.separator + "quenit.oauth");
            FileInputStream input = new FileInputStream(fileCert);

            InputStreamReader is = new InputStreamReader(input);
            StringBuilder sb = new StringBuilder();
            BufferedReader br = new BufferedReader(is);
            String read;

            while ((read = br.readLine()) != null) {
                //System.out.println(read);
                if (read.contains("BEGIN CERTIFICATE") || read.contains("END CERTIFICATE"))
                    continue;

                sb.append(read);
            }

            br.close();
            consumerSecret = sb.toString();
        }
        catch (IOException ex) {
            ex.printStackTrace();
            System.err.println("Reading Oauth consumer secret Error: " + ex.getMessage());
        }
        catch (ParameterNotFoundException ex) {
            ex.printStackTrace();
            System.err.println("Parameter not found: " + ex.getMessage());
        }
        catch (InterfaceNotFoundException ex) {
            ex.printStackTrace();
            System.err.println("Service is not available: " + ex.getMessage());
        }

    }

    /**
     * @throws ServletException
     *             , IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getQueryString() != null && request.getQueryString().equals("wsdl")) {
            final RequestDispatcher dispatcher = request.getRequestDispatcher("/DWHAdapter");
            dispatcher.forward(request, response);
        }
    }

    /**
     * @throws ServletException
     *             , IOException
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            verifyOAuth(request);
            final ServletRequest requestWrapper = new DWHServletRequestWrapper(request/*, requestContent*/);
            final RequestDispatcher dispatcher = request.getRequestDispatcher("/DWHAdapter");
            dispatcher.forward(requestWrapper, response);
        }
        catch (OAuthAutenticationException ex) {

            response.setContentType("text/xml");
            PrintWriter out = response.getWriter();

            String message = "<soap:Fault>\r\n"
            + "<faultcode>soap:Server</faultcode>\r\n"
            + "<faultstring>" + ex.getMessage() + "</faultstring>\r\n"
            + "<detail>\r\n"
            + "<ns1:OAuthAutenticationException xmlns:ns1=\"http://com.techedge.mp.dwh.client.service.adapter.fidelity.mp.techedge.com/\"/>\r\n"
            + "</detail>\r\n"
            + "</soap:Fault>\r\n";
            
            String soapResponse =
            "<?xml version=\"1.0\"?>\r\n"
            + "<SOAP-ENV:Envelope "
            + "xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" "
            + "xmlns:ser=\"http://com.techedge.mp.dwh.client.service.adapter.fidelity.mp.techedge.com/\">\r\n"
            + "<SOAP-ENV:Header/>\r\n"
            + "<SOAP-ENV:Body>\r\n"
            + message
            + "</SOAP-ENV:Body>\r\n</SOAP-ENV:Envelope>\r\n";

            out.println(soapResponse);
        }
    }

    private void verifyOAuth(HttpServletRequest servletRequest) throws OAuthAutenticationException, IOException {
        HashMap<String, String> oauthClient = new HashMap<String, String>();
        StringBuilder sb = new StringBuilder();
        Reader reader = new InputStreamReader(servletRequest.getInputStream());
        BufferedReader br = new BufferedReader(reader);
        String buffer;

        while ((buffer = br.readLine()) != null) {
            sb.append(buffer);
        }

        br.close();
        String requestContent = sb.toString();
        System.out.println("=========== SOAP REQUEST ===========");
        System.out.println(requestContent.replaceAll("/>", "/>\n"));
        System.out.println("=========== END ===========");

        String authHeader = servletRequest.getHeader("Authorization");

        if (authHeader != null) {

            String[] oauths = authHeader.replaceFirst("OAuth ", "").split(",");

            for (int i = 0; i < oauths.length; i++) {
                String[] keyValue = oauths[i].split("=");
                String key = keyValue[0].trim();
                String value = keyValue[1].trim().replaceAll("\"", "");

                oauthClient.put(key, OAuthEncoder.decode(value));
            }
        }
        else {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "verifyOAuth", null, null, "Http Header Authorization is null or is empty");
            throw new OAuthAutenticationException("Http Header Authorization is null or is empty");
        }

        String callback = OAuthConstants.OUT_OF_BAND;
        SignatureType signatureType = SignatureType.Header;
        OutputStream debugStream = System.out;
        String scope = null;
        EniOAuthApi api = new EniOAuthApi();

        TimestampService timestampService = api.getTimestampService(oauthClient.get(OAuthConstants.TIMESTAMP), oauthClient.get(OAuthConstants.NONCE));
        OAuthConfig config = new OAuthConfig(consumerKey, consumerSecret, callback, signatureType, scope, debugStream);
        OAuth10aServiceImpl provider = new OAuth10aServiceImpl(api, config);

        EniOAuthRequest oauthRequest = new EniOAuthRequest(wsdlString);
        oauthRequest.addOAuthParameter(OAuthConstants.TIMESTAMP, timestampService.getTimestampInSeconds());
        oauthRequest.addOAuthParameter(OAuthConstants.NONCE, timestampService.getNonce());
        oauthRequest.addOAuthParameter(OAuthConstants.CONSUMER_KEY, config.getApiKey());
        oauthRequest.addOAuthParameter(OAuthConstants.SIGN_METHOD, api.getSignatureService().getSignatureMethod());
        oauthRequest.addOAuthParameter(OAuthConstants.VERSION, provider.getVersion());
        oauthRequest.addBodyParameter("request_body", requestContent);
        oauthRequest.addPayload(requestContent);

        String baseString = api.getBaseStringExtractor().extract(oauthRequest);
        String serverSignature = api.getSignatureService().getSignature(baseString, config.getApiSecret(), Token.empty().getSecret());
        String clientSignature = OAuthEncoder.decode(oauthClient.get(OAuthConstants.SIGNATURE));

        System.out.println("Client Authorization OAuth");
        for (String key : oauthClient.keySet()) {
            System.out.println("  |--> " + key + ": " + OAuthEncoder.decode(oauthClient.get(key)));
        }

        System.out.println("Server Authorization OAuth");
        System.out.println("  |--> oauth_consumer_key: " + consumerKey);
        System.out.println("  |--> oauth_signature_method: " + api.getSignatureService().getSignatureMethod());
        System.out.println("  |--> oauth_timestamp: " + timestampService.getTimestampInSeconds());
        System.out.println("  |--> oauth_nonce: " + timestampService.getNonce());
        System.out.println("  |--> oauth_version: " + oauthRequest.getOauthParameters().get(OAuthConstants.VERSION));
        System.out.println("  |--> oauth_signature: " + serverSignature);
        System.out.println("         |--> base_string: " + baseString);

        if (!serverSignature.equals(clientSignature)) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "verifyOAuth", null, null, "Invalid Oauth Signature");
            throw new OAuthAutenticationException("Invalid Oauth Signature");
        }
    }


}
/*
 * @WebService()
 * public class DWHAdapter {
 * 
 * @Resource
 * WebServiceContext context;
 * 
 * private final static String PARAM_DWH_ADAPTER_WSDL = "FIDELITY_ADAPTER_WSDL";
 * private final static String PARAM_DWH_ADAPTER_OAUTH_CONSUMER_KEY = "FIDELITY_ADAPTER_OAUTH_CONSUMER_KEY";
 * 
 * private Properties prop = new Properties();
 * 
 * private ParametersServiceRemote parametersService = null;
 * private LoggerServiceRemote loggerService = null;
 * private UserServiceRemote userService = null;
 * private String consumerSecret = null;
 * private String consumerKey = null;
 * private String wsdlString = null;
 * 
 * public DWHAdapter() {
 * loadOAuthParams();
 * loadFileProperty();
 * }
 * 
 * private void loadOAuthParams() {
 * 
 * try {
 * this.loggerService = EJBHomeCache.getInstance().getLoggerService();
 * this.parametersService = EJBHomeCache.getInstance().getParametersService();
 * this.userService = EJBHomeCache.getInstance().getUserService();
 * 
 * this.wsdlString = parametersService.getParamValue(DWHAdapter.PARAM_FIDELITY_ADAPTER_WSDL);
 * this.consumerKey = parametersService.getParamValue(DWHAdapter.PARAM_FIDELITY_ADAPTER_OAUTH_CONSUMER_KEY);
 * 
 * File fileCert = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "oauth" + File.separator + "quenit.oauth");
 * FileInputStream input = new FileInputStream(fileCert);
 * 
 * InputStreamReader is = new InputStreamReader(input);
 * StringBuilder sb = new StringBuilder();
 * BufferedReader br = new BufferedReader(is);
 * String read;
 * 
 * while ((read = br.readLine()) != null) {
 * //System.out.println(read);
 * if (read.contains("BEGIN CERTIFICATE") || read.contains("END CERTIFICATE"))
 * continue;
 * 
 * sb.append(read);
 * }
 * 
 * br.close();
 * consumerSecret = sb.toString();
 * }
 * catch (IOException ex) {
 * ex.printStackTrace();
 * System.err.println("Reading Oauth consumer secret Error: " + ex.getMessage());
 * }
 * catch (ParameterNotFoundException ex) {
 * ex.printStackTrace();
 * System.err.println("Parameter not found: " + ex.getMessage());
 * }
 * catch (InterfaceNotFoundException ex) {
 * ex.printStackTrace();
 * System.err.println("Service is not available: " + ex.getMessage());
 * }
 * 
 * }
 * 
 * private void loadFileProperty() {
 * 
 * InputStream inputStream = getClass().getClassLoader().getResourceAsStream("response_service.properties");
 * try {
 * prop.load(inputStream);
 * if (inputStream == null) {
 * throw new FileNotFoundException("property file response_service.properties not found in the classpath");
 * }
 * }
 * catch (IOException e) {
 * System.err.println("File properties is not available: " + e.getMessage());
 * }
 * 
 * }
 * 
 * @WebMethod(operationName = "checkSession")
 * 
 * @WebResult(name = "checkSessionResponse")
 * public CheckSessionMessageResponse checkSession(@XmlElement(required = true) @WebParam(name = "checkSessionRequest") ChangeUserPasswordMessageRequest checkSessionMessageRequest)
 * throws OAuthAutenticationException {
 * 
 * verifyOAuth();
 * 
 * Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
 * inputParameters.add(new Pair<String, String>("operationID", checkSessionMessageRequest.getOperationID()));
 * if (checkSessionMessageRequest.getRequestTimestamp() != null) {
 * inputParameters.add(new Pair<String, String>("requestTimestamp", checkSessionMessageRequest.getRequestTimestamp().toString()));
 * }
 * else {
 * inputParameters.add(new Pair<String, String>("requestTimestamp", "null"));
 * }
 * inputParameters.add(new Pair<String, String>("sessionID", checkSessionMessageRequest.getSessionID()));
 * inputParameters.add(new Pair<String, String>("fiscalCode", checkSessionMessageRequest.getFiscalCode()));
 * 
 * this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkSession", checkSessionMessageRequest.getOperationID(), "opening",
 * ActivityLog.createLogMessage(inputParameters));
 * 
 * CheckSessionMessageResponse checkSessionMessageResponse = new CheckSessionMessageResponse();
 * 
 * Boolean errorFound = false;
 * 
 * if (checkSessionMessageRequest.getOperationID() == null || checkSessionMessageRequest.getOperationID().trim().isEmpty()) {
 * 
 * checkSessionMessageResponse.setOperationID("null");
 * checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
 * checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));
 * 
 * errorFound = true;
 * 
 * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "checkSession", checkSessionMessageRequest.getOperationID(), null, "OperationID null or empty");
 * 
 * }
 * else {
 * 
 * if (checkSessionMessageRequest.getOperationID().length() != 33) {
 * 
 * if (checkSessionMessageRequest.getOperationID().length() > 33) {
 * checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID().substring(0, 33));
 * }
 * else {
 * checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
 * }
 * checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
 * checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));
 * 
 * errorFound = true;
 * 
 * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "checkSession", checkSessionMessageRequest.getOperationID(), null,
 * "OperationID invalid length");
 * }
 * }
 * 
 * if (!errorFound) {
 * 
 * if (checkSessionMessageRequest.getSessionID() == null || checkSessionMessageRequest.getSessionID().trim().isEmpty()) {
 * 
 * checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
 * checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
 * checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));
 * 
 * errorFound = true;
 * 
 * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "checkSession", checkSessionMessageRequest.getOperationID(), null,
 * "SessionID null or empty");
 * 
 * }
 * else {
 * 
 * if (checkSessionMessageRequest.getSessionID().length() != 32) {
 * 
 * checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
 * checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
 * checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));
 * 
 * errorFound = true;
 * 
 * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "checkSession", checkSessionMessageRequest.getOperationID(), null,
 * "SessionID invalid length");
 * }
 * }
 * }
 * 
 * if (!errorFound) {
 * 
 * if (checkSessionMessageRequest.getFiscalCode() == null || checkSessionMessageRequest.getFiscalCode().trim().isEmpty()) {
 * 
 * checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
 * checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
 * checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));
 * 
 * errorFound = true;
 * 
 * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "checkSession", checkSessionMessageRequest.getOperationID(), null,
 * "FiscalCode null or empty");
 * 
 * }
 * else {
 * 
 * if (checkSessionMessageRequest.getFiscalCode().length() != 16) {
 * 
 * checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
 * checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_INVALID_REQUEST);
 * checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_INVALID_REQUEST));
 * 
 * errorFound = true;
 * 
 * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "checkSession", checkSessionMessageRequest.getOperationID(), null,
 * "FiscalCode invalid length");
 * }
 * }
 * }
 * 
 * if (!errorFound) {
 * 
 * String response = userService.checkLoyaltySession(checkSessionMessageRequest.getSessionID(), checkSessionMessageRequest.getFiscalCode());
 * checkSessionMessageResponse.setOperationID(checkSessionMessageRequest.getOperationID());
 * 
 * if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.USER_CHECK_LOYALTY_SESSION_SUCCESS)) {
 * checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_SUCCESS);
 * checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_SUCCESS));
 * }
 * else {
 * checkSessionMessageResponse.setStatusCode(ResponseHelper.CHECK_SESSION_FAILURE);
 * checkSessionMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.CHECK_SESSION_FAILURE));
 * }
 * }
 * 
 * Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
 * outputParameters.add(new Pair<String, String>("operationID", checkSessionMessageResponse.getOperationID()));
 * outputParameters.add(new Pair<String, String>("statusCode", checkSessionMessageResponse.getStatusCode()));
 * outputParameters.add(new Pair<String, String>("statusMessage", checkSessionMessageResponse.getMessageCode()));
 * 
 * this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "checkSession", checkSessionMessageRequest.getOperationID(), "closing",
 * ActivityLog.createLogMessage(outputParameters));
 * 
 * return checkSessionMessageResponse;
 * }
 * 
 * @WebMethod(operationName = "eventNotification")
 * 
 * @WebResult(name = "eventNotificationMessageResponse")
 * public EventNotificationMessageResponse eventNotification(
 * 
 * @XmlElement(required = true) @WebParam(name = "eventNotificationMessageRequest") EventNotificationMessageRequest eventNotificationMessageRequest)
 * throws OAuthAutenticationException {
 * 
 * verifyOAuth();
 * 
 * Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
 * inputParameters.add(new Pair<String, String>("eventType", eventNotificationMessageRequest.getEventType().toString()));
 * inputParameters.add(new Pair<String, String>("fiscalCode", eventNotificationMessageRequest.getFiscalCode()));
 * 
 * inputParameters.add(new Pair<String, String>("requestTimestamp", eventNotificationMessageRequest.getRequestTimestamp().toString()));
 * inputParameters.add(new Pair<String, String>("srcTransactionID", eventNotificationMessageRequest.getSrcTransactionID()));
 * 
 * if (eventNotificationMessageRequest.getLoyaltyTransactionDetail() != null) {
 * LoyaltyTransactionDetail loyaltyTransactionDetail = eventNotificationMessageRequest.getLoyaltyTransactionDetail();
 * String message = "{ " + "sessionID: " + (loyaltyTransactionDetail.getSessionID() != null ? loyaltyTransactionDetail.getSessionID() : "") + ", panCode: "
 * + loyaltyTransactionDetail.getPanCode() + ", stationID: " + loyaltyTransactionDetail.getStationID() + ", terminalID: "
 * + ((loyaltyTransactionDetail.getTerminalID() != null) ? loyaltyTransactionDetail.getTerminalID() : "") + ", transactionResult: "
 * + loyaltyTransactionDetail.getTransactionResult() + ", transactionDate: "
 * + (loyaltyTransactionDetail.getTransactionDate() != null ? loyaltyTransactionDetail.getTransactionDate() : "") + ", credits: "
 * + (loyaltyTransactionDetail.getCredits() != null ? loyaltyTransactionDetail.getCredits() : 0) + ", balance: " + loyaltyTransactionDetail.getBalance()
 * + ", paymentMode: " + loyaltyTransactionDetail.getPaymentMode() + ", marketingMsg: "
 * + (loyaltyTransactionDetail.getMarketingMsg() != null ? loyaltyTransactionDetail.getMarketingMsg() : "") + ", warningMsg: "
 * + (loyaltyTransactionDetail.getWarningMsg() != null ? loyaltyTransactionDetail.getWarningMsg() : "") + ", refuelDetail: ";
 * 
 * if (loyaltyTransactionDetail.getRefuelDetail() != null) {
 * message += " { ";
 * 
 * for (RefuelDetail refuelDetail : loyaltyTransactionDetail.getRefuelDetail()) {
 * String messageTmp = "{ " + "refuelMode: " + refuelDetail.getRefuelMode() + ", pumpNumber: "
 * + (refuelDetail.getPumpNumber() != null ? refuelDetail.getPumpNumber() : "") + ", productID: " + refuelDetail.getProductID() + ", productDescription: "
 * + (refuelDetail.getProductDescription() != null ? refuelDetail.getProductDescription() : "") + ", fuelQuantity: " + refuelDetail.getFuelQuantity()
 * + ", amount: " + refuelDetail.getAmount() + ", credits: " + refuelDetail.getCredits() + " }";
 * 
 * message += messageTmp;
 * }
 * }
 * else {
 * message += "null";
 * }
 * 
 * message += ", nonOilDetail: ";
 * 
 * if (loyaltyTransactionDetail.getNonOilDetail() != null) {
 * message += " { ";
 * 
 * for (NonOilDetail nonOilDetail : loyaltyTransactionDetail.getNonOilDetail()) {
 * String messageTmp = "{ " + "productID: " + (nonOilDetail.getProductID() != null ? nonOilDetail.getProductID() : "") + ", productDescription: "
 * + (nonOilDetail.getProductDescription() != null ? nonOilDetail.getProductDescription() : "") + ", amount: " + nonOilDetail.getAmount() + ", credits: "
 * + nonOilDetail.getCredits() + " }";
 * 
 * message += messageTmp;
 * }
 * }
 * else {
 * message += "null";
 * }
 * 
 * inputParameters.add(new Pair<String, String>("loyaltyTransactionDetail", message));
 * }
 * else {
 * inputParameters.add(new Pair<String, String>("loyaltyTransactionDetail", "null"));
 * }
 * 
 * if (eventNotificationMessageRequest.getRewardDetail() != null) {
 * RewardDetail rewardDetail = eventNotificationMessageRequest.getRewardDetail();
 * String message = "{ " + "operationType: " + rewardDetail.getOperationType() + ", subtype: " + (rewardDetail.getSubtype() != null ? rewardDetail.getSubtype() : "")
 * + ", parameterDetails: ";
 * 
 * if (rewardDetail.getParameterDetails() != null) {
 * for (ParameterDetails parameterDetails : rewardDetail.getParameterDetails()) {
 * String messageTmp = "{ parameterID: " + parameterDetails.getParameterID() + ", parameterValue: " + parameterDetails.getParameterValue() + " }";
 * 
 * message += messageTmp;
 * }
 * }
 * else {
 * message += "null";
 * }
 * 
 * inputParameters.add(new Pair<String, String>("rewardDetail", message));
 * }
 * else {
 * inputParameters.add(new Pair<String, String>("rewardDetail", "null"));
 * }
 * 
 * this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), "opening",
 * ActivityLog.createLogMessage(inputParameters));
 * 
 * EventNotification eventNotification = new EventNotification();
 * 
 * eventNotification.setEventType(eventNotificationMessageRequest.getEventType().getCode());
 * eventNotification.setFiscalCode(eventNotificationMessageRequest.getFiscalCode());
 * eventNotification.setRequestTimestamp(new Date(eventNotificationMessageRequest.getRequestTimestamp()));
 * eventNotification.setSrcTransactionID(eventNotificationMessageRequest.getSrcTransactionID());
 * 
 * if (eventNotificationMessageRequest.getLoyaltyTransactionDetail() != null) {
 * LoyaltyTransactionDetail loyaltyTransactionDetail = eventNotificationMessageRequest.getLoyaltyTransactionDetail();
 * LoyaltyTransaction loyaltyTransaction = new LoyaltyTransaction();
 * loyaltyTransaction.setBalance(loyaltyTransactionDetail.getBalance().intValue());
 * loyaltyTransaction.setCredits(loyaltyTransactionDetail.getCredits().intValue());
 * loyaltyTransaction.setSessionID(loyaltyTransactionDetail.getSessionID());
 * loyaltyTransaction.setPanCode(loyaltyTransactionDetail.getPanCode());
 * loyaltyTransaction.setStationID(loyaltyTransactionDetail.getStationID());
 * loyaltyTransaction.setTerminalID(loyaltyTransactionDetail.getTerminalID());
 * loyaltyTransaction.setTransactionResult(loyaltyTransactionDetail.getTransactionResult());
 * loyaltyTransaction.setTransactionDate(loyaltyTransactionDetail.getTransactionDate());
 * loyaltyTransaction.setPaymentMode(loyaltyTransactionDetail.getPaymentMode());
 * loyaltyTransaction.setMarketingMsg(loyaltyTransactionDetail.getMarketingMsg());
 * loyaltyTransaction.setWarningMsg(loyaltyTransactionDetail.getWarningMsg());
 * 
 * if (loyaltyTransactionDetail.getRefuelDetail() != null) {
 * for (RefuelDetail refuelDetail : loyaltyTransactionDetail.getRefuelDetail()) {
 * LoyaltyTransactionRefuel refuel = new LoyaltyTransactionRefuel();
 * refuel.setRefuelMode(refuelDetail.getRefuelMode());
 * refuel.setPumpNumber(refuelDetail.getPumpNumber());
 * refuel.setProductID(refuelDetail.getProductID());
 * refuel.setProductDescription(refuelDetail.getProductDescription());
 * refuel.setFuelQuantity(refuelDetail.getFuelQuantity().doubleValue());
 * refuel.setAmount(refuelDetail.getAmount().doubleValue());
 * refuel.setCredits(refuelDetail.getCredits().intValue());
 * 
 * loyaltyTransaction.getRefuelDetails().add(refuel);
 * }
 * }
 * 
 * if (loyaltyTransactionDetail.getNonOilDetail() != null) {
 * for (NonOilDetail nonOilDetail : loyaltyTransactionDetail.getNonOilDetail()) {
 * LoyaltyTransactionNonOil nonOil = new LoyaltyTransactionNonOil();
 * nonOil.setProductID(nonOilDetail.getProductID());
 * nonOil.setProductDescription(nonOilDetail.getProductDescription());
 * nonOil.setAmount(nonOilDetail.getAmount().doubleValue());
 * nonOil.setCredits(nonOilDetail.getCredits().intValue());
 * 
 * loyaltyTransaction.getNonOilDetails().add(nonOil);
 * }
 * }
 * 
 * eventNotification.setLoyaltyTransactionDetail(loyaltyTransaction);
 * }
 * 
 * if (eventNotificationMessageRequest.getRewardDetail() != null) {
 * RewardDetail rewardDetail = eventNotificationMessageRequest.getRewardDetail();
 * RewardTransaction rewardTransaction = new RewardTransaction();
 * rewardTransaction.setOperationType(rewardDetail.getOperationType());
 * rewardTransaction.setSubtype(rewardDetail.getSubtype());
 * 
 * if (rewardDetail.getParameterDetails() != null) {
 * for (ParameterDetails parameterDetails : rewardDetail.getParameterDetails()) {
 * RewardTransactionParameter parameter = new RewardTransactionParameter();
 * parameter.setParameterID(parameterDetails.getParameterID());
 * parameter.setParameterValue(parameterDetails.getParameterValue());
 * 
 * rewardTransaction.getParameterDetails().add(parameter);
 * }
 * }
 * }
 * 
 * String response = userService.setEventNotification(eventNotification);
 * 
 * EventNotificationMessageResponse eventNotificationMessageResponse = new EventNotificationMessageResponse();
 * 
 * if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.USER_SET_EVENT_NOTIFICATION_USER_NOT_FOUND)) {
 * eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_USER_NOT_FOUND);
 * eventNotificationMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.EVENT_NOTIFICATION_USER_NOT_FOUND));
 * }
 * 
 * if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.USER_SET_EVENT_NOTIFICATION_SUCCESS)) {
 * eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_SUCCESS);
 * eventNotificationMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.EVENT_NOTIFICATION_SUCCESS));
 * }
 * 
 * if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.USER_SET_EVENT_NOTIFICATION_STATION_NOT_FOUND)) {
 * eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
 * eventNotificationMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.EVENT_NOTIFICATION_ERROR));
 * }
 * 
 * if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.SYSTEM_ERROR)) {
 * eventNotificationMessageResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_ERROR);
 * eventNotificationMessageResponse.setMessageCode(prop.getProperty(ResponseHelper.EVENT_NOTIFICATION_ERROR));
 * }
 * 
 * Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
 * outputParameters.add(new Pair<String, String>("statusCode", eventNotificationMessageResponse.getStatusCode()));
 * outputParameters.add(new Pair<String, String>("statusMessage", eventNotificationMessageResponse.getMessageCode()));
 * 
 * this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "eventNotification", eventNotificationMessageRequest.getRequestTimestamp().toString(), "closing",
 * ActivityLog.createLogMessage(outputParameters));
 * 
 * return eventNotificationMessageResponse;
 * }
 * 
 * @SuppressWarnings("unchecked")
 * private void verifyOAuth() throws OAuthAutenticationException {
 * HashMap<String, String> oauthClient = new HashMap<String, String>();
 * MessageContext messageContext = context.getMessageContext();
 * 
 * ServletContext servletContext = (ServletContext)messageContext.get(MessageContext.SERVLET_CONTEXT);
 * 
 * //WrappedMessageContext wmc = (WrappedMessageContext) context;
 * //Object content = wmc.getWrappedMessage().getContent(Object.class);
 * //System.out.println("SERVLET BODY PAYLOAD: " + content.toString());
 * 
 * //HttpServletRequest servletRequest = (HttpServletRequest) messageContext.get(MessageContext.SERVLET_REQUEST);
 * try {
 * 
 * InputStreamReader is = new InputStreamReader(servletContext.getResourceAsStream("/DWHAdapter"));
 * StringBuilder sb = new StringBuilder();
 * BufferedReader br = new BufferedReader(is);
 * String read;
 * 
 * while ((read = br.readLine()) != null) {
 * System.out.println("READ: " + read);
 * sb.append(read);
 * }
 * 
 * br.close();
 * System.out.println("SERVLET BODY PAYLOAD: " + sb.toString());
 * }
 * catch (IOException ex) {
 * System.err.println("Errore nella lettura del contenuto della servlet: " + ex.getMessage());
 * }
 * 
 * if (messageContext != null) {
 * Map<String, Object> httpHeaders = (Map<String, Object>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
 * ArrayList<String> authorizationList = (ArrayList<String>) httpHeaders.get("Authorization");
 * if (authorizationList != null && authorizationList.size() > 0) {
 * String authHeader = authorizationList.get(0);
 * String[] oauths = authHeader.replaceFirst("OAuth ", "").split(",");
 * for (int i = 0; i < oauths.length; i++) {
 * String[] keyValue = oauths[i].split("=");
 * String key = keyValue[0].trim();
 * String value = keyValue[1].trim().replaceAll("\"", "");
 * //String value = OAuthEncoder.decode(keyValue[1].trim().replaceAll("\"", ""));
 * oauthClient.put(key, OAuthEncoder.decode(value));
 * }
 * }
 * else {
 * this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "verifyOAuth", null, null, "Http Header Authorization is null or is empty");
 * 
 * throw new OAuthAutenticationException("Http Header Authorization is null or is empty");
 * }
 * }
 * else {
 * this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "verifyOAuth", null, null, "MessageContext is null");
 * 
 * throw new OAuthAutenticationException("Http Header Authorization is null or is empty");
 * }
 * 
 * String callback = OAuthConstants.OUT_OF_BAND;
 * SignatureType signatureType = SignatureType.Header;
 * OutputStream debugStream = System.out;
 * String scope = null;
 * EniOAuthApi api = new EniOAuthApi();
 * 
 * TimestampService timestampService = api.getTimestampService(oauthClient.get(OAuthConstants.TIMESTAMP), oauthClient.get(OAuthConstants.NONCE));
 * OAuthConfig config = new OAuthConfig(consumerKey, consumerSecret, callback, signatureType, scope, debugStream);
 * OAuth10aServiceImpl provider = new OAuth10aServiceImpl(api, config);
 * 
 * EniOAuthRequest oauthRequest = new EniOAuthRequest(wsdlString);
 * oauthRequest.addOAuthParameter(OAuthConstants.TIMESTAMP, timestampService.getTimestampInSeconds());
 * oauthRequest.addOAuthParameter(OAuthConstants.NONCE, timestampService.getNonce());
 * oauthRequest.addOAuthParameter(OAuthConstants.CONSUMER_KEY, config.getApiKey());
 * oauthRequest.addOAuthParameter(OAuthConstants.SIGN_METHOD, api.getSignatureService().getSignatureMethod());
 * oauthRequest.addOAuthParameter(OAuthConstants.VERSION, provider.getVersion());
 * //oauthRequest.addBodyParameter("request_body", body);
 * //oauthRequest.addPayload(body);
 * 
 * String baseString = api.getBaseStringExtractor().extract(oauthRequest);
 * String serverSignature = api.getSignatureService().getSignature(baseString, config.getApiSecret(), Token.empty().getSecret());
 * String clientSignature = OAuthEncoder.decode(oauthClient.get(OAuthConstants.SIGNATURE));
 * 
 * System.out.println("Client Authorization OAuth");
 * for (String key : oauthClient.keySet()) {
 * System.out.println("  |--> " + key + ": " + OAuthEncoder.decode(oauthClient.get(key)));
 * }
 * 
 * System.out.println("Server Authorization OAuth");
 * System.out.println("  |--> oauth_consumer_key: " + consumerKey);
 * System.out.println("  |--> oauth_signature_method: " + api.getSignatureService().getSignatureMethod());
 * System.out.println("  |--> oauth_timestamp: " + timestampService.getTimestampInSeconds());
 * System.out.println("  |--> oauth_nonce: " + timestampService.getNonce());
 * System.out.println("  |--> oauth_version: " + oauthRequest.getOauthParameters().get(OAuthConstants.VERSION));
 * System.out.println("  |--> oauth_signature: " + serverSignature);
 * System.out.println("         |--> base_string: " + baseString);
 * 
 * if (!serverSignature.equals(clientSignature)) {
 * this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "verifyOAuth", null, null, "Invalid Oauth Signature");
 * throw new OAuthAutenticationException("Invalid Oauth Signature");
 * }
 * }
 * }
 */