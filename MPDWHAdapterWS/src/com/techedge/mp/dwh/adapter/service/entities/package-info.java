/*@XmlSchema(
        namespace = "http://com.techedge.mp.dwh.client.service.adapter.fidelity.mp.techedge.com",
        elementFormDefault = XmlNsForm.QUALIFIED, 
        xmlns = { 
                @XmlNs(
                        prefix = "soap", 
                        namespaceURI = "http://schemas.xmlsoap.org/soap/envelope"
                 ),
                 @XmlNs(
                         prefix = "soap-env", 
                         namespaceURI = "http://schemas.xmlsoap.org/soap/envelope"
                  ),
                 @XmlNs(
                         prefix = "ns1", 
                         namespaceURI = "http://service.adapter.dwh.mp.techedge.com"
                  ),
                @XmlNs(
                        prefix = "tns", 
                        namespaceURI = "http://service.adapter.dwh.mp.techedge.com"
                 )
})*/


package com.techedge.mp.dwh.adapter.service.entities;
import javax.xml.bind.annotation.XmlNs;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
