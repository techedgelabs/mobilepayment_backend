package com.techedge.mp.dwh.adapter.service.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "changeUserPasswordMessageRequest", propOrder = { "operationID", "requestTimestamp", "fiscalCode", "email", "oldPassword", "newPassword" })
public class ChangeUserPasswordMessageRequest {

    @XmlElement(required = true, nillable = false)
    private String operationID;
    @XmlElement(required = true, nillable = false)
    private Long   requestTimestamp;
    @XmlElement(required = true, nillable = false)
    private String fiscalCode;
    @XmlElement(required = true, nillable = false)
    private String email;
    @XmlElement(required = true, nillable = false)
    private String oldPassword;
    @XmlElement(required = true, nillable = false)
    private String newPassword;


    public String getOperationID() {
        return operationID;
    }

    public void setOperationID(String operationID) {
        this.operationID = operationID;
    }

    public Long getRequestTimestamp() {
        return requestTimestamp;
    }

    public void setRequestTimestamp(Long requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFiscalCode() {
        return fiscalCode;
    }

    public void setFiscalCode(String fiscalCode) {
        this.fiscalCode = fiscalCode;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

}
