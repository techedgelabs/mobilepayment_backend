package com.techedge.mp.dwh.adapter.service.utilities;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class DWHServletRequestWrapper extends HttpServletRequestWrapper {

    private HttpServletRequest originalRequest;
    private ServletInputStream servletInStream;
    private String servletContent;

    public DWHServletRequestWrapper(HttpServletRequest req) {
        this(req, null);
    }

    public DWHServletRequestWrapper(HttpServletRequest req, String content) {
        super(req);
        originalRequest = req;
        servletContent = content;
        init();
    }
    
    /**
     * Extract the data from the HTTP request and create an XML/A request
     */
    private void init() {
        // Parameter not set. Look for the request in the body of the http
        // request.

        if (servletContent == null) {
            try {
                final ServletInputStream inputStream = originalRequest.getInputStream();

                StringBuilder sb = new StringBuilder();
                Reader reader = new InputStreamReader(inputStream);
                BufferedReader br = new BufferedReader(reader);
                String buffer;

                while ((buffer = br.readLine()) != null) {
                    sb.append(buffer);
                }

                //br.close();                
                servletContent = sb.toString();
            }
            catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        
        if (servletContent.length() == 0) {
            throw new RuntimeException("servlet content is empty");
        }
        
        servletInStream = new FidelityServletInputStream(servletContent);
    }

    public String getContentType() {
        return "text/xml";
    }

    public ServletInputStream getInputStream() {
        return servletInStream;
    }

    private static class FidelityServletInputStream extends ServletInputStream {

        private ByteArrayInputStream bais;

        FidelityServletInputStream(String source) {
            bais = new ByteArrayInputStream(source.getBytes());
        }

        public int readLine(byte[] arg0, int arg1, int arg2) throws IOException {
            return bais.read(arg0, arg1, arg2);
        }

        public int available() throws IOException {
            return bais.available();
        }

        public void close() throws IOException {
            bais.close();
        }

        public synchronized void mark(int readlimit) {
            bais.mark(readlimit);
        }

        public boolean markSupported() {
            return bais.markSupported();
        }

        public int read() throws IOException {
            return bais.read();
        }

        public int read(byte[] b, int off, int len) throws IOException {
            return bais.read(b, off, len);
        }

        public int read(byte[] b) throws IOException {
            return bais.read(b);
        }

        public synchronized void reset() throws IOException {
            bais.reset();
        }

        public long skip(long n) throws IOException {
            return bais.skip(n);
        }
    }
}
