package com.techedge.mp.dwh.adapter.service.validator;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.xml.namespace.QName;
import javax.xml.soap.Detail;
import javax.xml.soap.DetailEntry;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;

import org.scribe.model.OAuthConfig;
import org.scribe.model.OAuthConstants;
import org.scribe.model.SignatureType;
import org.scribe.model.Token;
import org.scribe.oauth.OAuth10aServiceImpl;
import org.scribe.services.TimestampService;
import org.scribe.utils.OAuthEncoder;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.dwh.adapter.service.EJBHomeCache;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.dwh.adapter.service.exception.OAuthAutenticationException;
import com.techedge.mp.dwh.client.EniOAuthApi;
import com.techedge.mp.dwh.client.model.EniOAuthRequest;

public class OAuthValidator implements SOAPHandler<SOAPMessageContext> {

    private final static String     PARAM_DWH_ADAPTER_WSDL               = "DWH_ADAPTER_WSDL";
    private final static String     PARAM_DWH_ADAPTER_OAUTH_CONSUMER_KEY = "DWH_ADAPTER_OAUTH_CONSUMER_KEY";

    private ParametersServiceRemote parametersService                         = null;
    private String                  consumerSecret                            = null;
    private String                  consumerKey                               = null;
    private String                  wsdlString                                = null;

    @Override
    public boolean handleFault(SOAPMessageContext messageContext) {
        System.out.println("HandlerChain [OAuthValidator]: handleFault()......");
        return true;
    }

    @Override
    public Set<QName> getHeaders() {
        System.out.println("HandlerChain [OAuthValidator]: getHeaders()......");
        return null;
    }
    
    @Override
    public void close(MessageContext messageContext) {
        System.out.println("HandlerChain [OAuthValidator]: close()......");
    }

    @Override
    public boolean handleMessage(SOAPMessageContext messageContext) {

        System.out.println("HandlerChain [OAuthValidator]: handleMessage()......");

        Boolean isRequest = (Boolean) messageContext.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

        //for response message only, true for outbound messages, false for inbound
        if (!isRequest) {
            try {
                loadOAuthParams();
                verifyOAuth(messageContext);
            }
            catch (IOException ex) {
                System.err.println("Reading Oauth consumer secret Error: " + ex.getMessage());
                messageContext.setMessage(generateSOAPFaultMessage("Reading Oauth consumer secret Error", ex.getMessage()));
                return false;

            }
            catch (ParameterNotFoundException ex) {
                System.err.println("Parameter not found: " + ex.getMessage());
                messageContext.setMessage(generateSOAPFaultMessage("Parameter not found", ex.getMessage()));
                return false;
            }
            catch (InterfaceNotFoundException ex) {
                System.err.println("Service is not available: " + ex.getMessage());
                messageContext.setMessage(generateSOAPFaultMessage("Service is not available", ex.getMessage()));
                return false;
            }
            catch (OAuthAutenticationException ex) {
                System.err.println("OAuth autentication error: " + ex.getMessage());
                messageContext.setMessage(generateSOAPFaultMessage("OAuth autentication error", ex.getMessage()));
                return false;
            }
        }
        //continue other handler chain
        return true;
    }

    private void loadOAuthParams() throws InterfaceNotFoundException, ParameterNotFoundException, IOException {
        this.parametersService = EJBHomeCache.getInstance().getParametersService();

        this.wsdlString = parametersService.getParamValue(PARAM_DWH_ADAPTER_WSDL);
        this.consumerKey = parametersService.getParamValue(PARAM_DWH_ADAPTER_OAUTH_CONSUMER_KEY);

        File fileCert = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "oauth" + File.separator + "dwh_server.oauth");
        FileInputStream input = new FileInputStream(fileCert);

        InputStreamReader is = new InputStreamReader(input);
        StringBuilder sb = new StringBuilder();
        BufferedReader br = new BufferedReader(is);
        String read;

        while ((read = br.readLine()) != null) {
            //System.out.println(read);
            if (read.contains("BEGIN CERTIFICATE") || read.contains("END CERTIFICATE"))
                continue;

            sb.append(read);
        }

        br.close();
        consumerSecret = sb.toString();
    }

    @SuppressWarnings("unchecked")
    private void verifyOAuth(SOAPMessageContext messageContext) throws OAuthAutenticationException {
        HashMap<String, String> oauthClient = new HashMap<String, String>();
        String requestContent = null;
        try {
            SOAPMessage soapMessage = messageContext.getMessage();
            soapMessage.getSOAPHeader().detachNode();
                        
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            soapMessage.writeTo(baos);
            requestContent = baos.toString();
            /*
            NodeList soapBody = soapMessage.getSOAPHeader().rgetSOAPBody().getChildNodes();
            DOMSource source = new DOMSource();
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");

            for (int i = 0; i < soapBody.getLength(); ++i) {
                source.setNode(soapBody.item(i));
                transformer.transform(source, result);
            }

            requestContent = writer.toString();
            */
            
            System.out.println("=========== SOAP REQUEST ===========");
            System.out.println(requestContent.replaceAll("/>", "/>\n"));
            System.out.println("=========== END ===========");
        }
        catch (Exception ex) {
            throw new OAuthAutenticationException("Http SOAP message read error (" + ex.getMessage() + ")");
        }

        Map<String, Object> httpHeaders = (Map<String, Object>) messageContext.get(MessageContext.HTTP_REQUEST_HEADERS);
        ArrayList<String> authorizationList = (ArrayList<String>) httpHeaders.get("Authorization");

        if (authorizationList != null && authorizationList.size() > 0) {
            String authHeader = authorizationList.get(0);
            String[] oauths = authHeader.replaceFirst("OAuth ", "").split(",");

            for (int i = 0; i < oauths.length; i++) {
                String[] keyValue = oauths[i].split("=");
                String key = keyValue[0].trim();
                String value = keyValue[1].trim().replaceAll("\"", "");
                //String value = OAuthEncoder.decode(keyValue[1].trim().replaceAll("\"", ""));
                oauthClient.put(key, OAuthEncoder.decode(value));
            }
        }
        else {
            System.err.println("Http Header Authorization is null or is empty");
            throw new OAuthAutenticationException("Http Header Authorization is null or is empty");
        }

        String callback = OAuthConstants.OUT_OF_BAND;
        SignatureType signatureType = SignatureType.Header;
        OutputStream debugStream = System.out;
        String scope = null;
        EniOAuthApi api = new EniOAuthApi();

        TimestampService timestampService = api.getTimestampService(oauthClient.get(OAuthConstants.TIMESTAMP), oauthClient.get(OAuthConstants.NONCE));
        OAuthConfig config = new OAuthConfig(consumerKey, consumerSecret, callback, signatureType, scope, debugStream);
        OAuth10aServiceImpl provider = new OAuth10aServiceImpl(api, config);

        EniOAuthRequest oauthRequest = new EniOAuthRequest(wsdlString);
        oauthRequest.addOAuthParameter(OAuthConstants.TIMESTAMP, timestampService.getTimestampInSeconds());
        oauthRequest.addOAuthParameter(OAuthConstants.NONCE, timestampService.getNonce());
        oauthRequest.addOAuthParameter(OAuthConstants.CONSUMER_KEY, config.getApiKey());
        oauthRequest.addOAuthParameter(OAuthConstants.SIGN_METHOD, api.getSignatureService().getSignatureMethod());
        oauthRequest.addOAuthParameter(OAuthConstants.VERSION, provider.getVersion());
        oauthRequest.addBodyParameter("request_body", requestContent);
        //oauthRequest.addPayload(requestContent);

        String baseString = api.getBaseStringExtractor().extract(oauthRequest);
        String serverSignature = api.getSignatureService().getSignature(baseString, config.getApiSecret(), Token.empty().getSecret());
        //String clientSignature = OAuthEncoder.decode(oauthClient.get(OAuthConstants.SIGNATURE));
        String clientSignature = oauthClient.get(OAuthConstants.SIGNATURE);

        System.out.println("Client Authorization OAuth");
        for (String key : oauthClient.keySet()) {
            System.out.println("  |--> " + key + ": " + OAuthEncoder.decode(oauthClient.get(key)));
        }

        System.out.println("Server Authorization OAuth");
        System.out.println("  |--> oauth_consumer_key: " + consumerKey);
        System.out.println("  |--> oauth_signature_method: " + api.getSignatureService().getSignatureMethod());
        System.out.println("  |--> oauth_timestamp: " + timestampService.getTimestampInSeconds());
        System.out.println("  |--> oauth_nonce: " + timestampService.getNonce());
        System.out.println("  |--> oauth_version: " + oauthRequest.getOauthParameters().get(OAuthConstants.VERSION));
        System.out.println("  |--> oauth_signature: " + serverSignature);
        System.out.println("         |--> base_string: " + baseString);

        if (!serverSignature.equals(clientSignature)) {
            System.err.println("Invalid Oauth Signature");
            throw new OAuthAutenticationException("Invalid Oauth Signature");
        }
    }

    private SOAPMessage generateSOAPFaultMessage(String error, String errorDescription) {
        SOAPMessage soapMessage = null;
        try {
            soapMessage = MessageFactory.newInstance().createMessage();
            soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ns1", "http://service.adapter.dwh.mp.techedge.com");
            SOAPFault soapFault = soapMessage.getSOAPBody().addFault();
            QName faultName = new QName(SOAPConstants.URI_NS_SOAP_ENVELOPE, "Server");
            soapFault.setFaultCode(faultName);
            soapFault.setFaultString(error);
            Detail detail = soapFault.addDetail();
            QName entryName = new QName("http://service.adapter.dwh.mp.techedge.com", "OAuthAutenticationException", "ns1");
            DetailEntry entry = detail.addDetailEntry(entryName);
            entry.addTextNode((errorDescription != null) ? errorDescription : error);
        }
        catch (SOAPException ex) {
            System.err.println("Errore nella creazione del messaggio soap: " + ex.getMessage());
        }
        
        return soapMessage;
    }
    

}
