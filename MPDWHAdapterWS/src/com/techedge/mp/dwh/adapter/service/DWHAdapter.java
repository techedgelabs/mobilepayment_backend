package com.techedge.mp.dwh.adapter.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

import com.techedge.mp.core.business.DWHServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.dwh.adapter.service.entities.ActivatePromotionMessageRequest;
import com.techedge.mp.dwh.adapter.service.entities.ActivatePromotionMessageResponse;
import com.techedge.mp.dwh.adapter.service.entities.ChangeUserPasswordMessageRequest;
import com.techedge.mp.dwh.adapter.service.entities.ChangeUserPasswordMessageResponse;
import com.techedge.mp.dwh.adapter.service.utilities.ResponseHelper;

@HandlerChain(file = "handler-chain.xml")
@WebService()
public class DWHAdapter {

    private static final String VHB_PROMOTION_ID = "vodafone_happy_black";
    
    private Properties          prop          = new Properties();

    private LoggerServiceRemote loggerService = null;
    private DWHServiceRemote    dwhService    = null;

    public DWHAdapter() {
        initService();
    }

    private void initService() {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("response_service.properties");

        try {
            prop.load(inputStream);
            if (inputStream == null) {
                throw new FileNotFoundException("property file response_service.properties not found in the classpath");
            }
        }
        catch (IOException e) {
            System.err.println("File properties is not available: " + e.getMessage());
        }
    }

    private void log(ErrorLevel level, String methodName, String groupId, String phaseId, String message) {
        try {
            this.loggerService = EJBHomeCache.getInstance().getLoggerService();
            this.loggerService.log(level, this.getClass().getSimpleName(), methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {
            System.err.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    @WebMethod(operationName = "changeUserPassword")
    @WebResult(name = "changeUserPasswordResponse")
    public @XmlElement(required = true)
    ChangeUserPasswordMessageResponse changeUserPassword(@XmlElement(required = true) @WebParam(name = "changeUserPasswordRequest") ChangeUserPasswordMessageRequest changeUserPasswordMessageRequest) {

        Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("operationID", changeUserPasswordMessageRequest.getOperationID()));
        inputParameters.add(new Pair<String, String>("requestTimestamp", changeUserPasswordMessageRequest.getRequestTimestamp().toString()));
        inputParameters.add(new Pair<String, String>("fiscalCode", changeUserPasswordMessageRequest.getFiscalCode()));
        inputParameters.add(new Pair<String, String>("email", changeUserPasswordMessageRequest.getEmail()));
        inputParameters.add(new Pair<String, String>("oldPassword", changeUserPasswordMessageRequest.getOldPassword()));
        inputParameters.add(new Pair<String, String>("newPassword", changeUserPasswordMessageRequest.getNewPassword()));

        log(ErrorLevel.DEBUG, "changeUserPassword", changeUserPasswordMessageRequest.getOperationID(), "opening", ActivityLog.createLogMessage(inputParameters));

        ChangeUserPasswordMessageResponse changeUserPasswordMessageResponse = new ChangeUserPasswordMessageResponse();
        
        try {
            dwhService = EJBHomeCache.getInstance().getDWHService();
            
            String response = dwhService.updatePassword(changeUserPasswordMessageRequest.getOperationID(), changeUserPasswordMessageRequest.getRequestTimestamp(), 
                    changeUserPasswordMessageRequest.getFiscalCode(), changeUserPasswordMessageRequest.getEmail(), 
                    changeUserPasswordMessageRequest.getOldPassword(), changeUserPasswordMessageRequest.getNewPassword());        

            if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.DWH_UPDATE_PASSWORD_SUCCESS)) {
                changeUserPasswordMessageResponse.setStatusCode(ResponseHelper.USER_CHANGE_PASSWORD_SUCCESS);
            }
            else if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.SYSTEM_ERROR)) {
                changeUserPasswordMessageResponse.setStatusCode(ResponseHelper.USER_CHANGE_PASSWORD_SYSTEM_ERROR);
            }
            else {
                changeUserPasswordMessageResponse.setStatusCode(ResponseHelper.USER_CHANGE_PASSWORD_INVALID_REQUEST);
            }
        }
        catch (InterfaceNotFoundException ex) {
            log(ErrorLevel.DEBUG, "changeUserPassword", changeUserPasswordMessageRequest.getOperationID(), "closing", ex.getMessage());
            changeUserPasswordMessageResponse.setStatusCode(ResponseHelper.USER_CHANGE_PASSWORD_SYSTEM_ERROR);
        }

        changeUserPasswordMessageResponse.setMessageCode(prop.getProperty(changeUserPasswordMessageResponse.getStatusCode()));
        
        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", changeUserPasswordMessageResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", changeUserPasswordMessageResponse.getMessageCode()));

        log(ErrorLevel.INFO, "changeUserPassword", changeUserPasswordMessageRequest.getOperationID(), "closing", ActivityLog.createLogMessage(outputParameters));

        return changeUserPasswordMessageResponse;
    }
    
    
   @WebMethod(operationName = "activatePromotion")
    @WebResult(name = "activatePromotionResponse")
    public @XmlElement(required = true)
    ActivatePromotionMessageResponse activatePromotion(@XmlElement(required = true) @WebParam(name = "activatePromotionRequest") ActivatePromotionMessageRequest activatePromotionRequest) {
    	Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
        inputParameters.add(new Pair<String, String>("promotionID", activatePromotionRequest.getPromotionID()));
        inputParameters.add(new Pair<String, String>("fiscalCode", activatePromotionRequest.getFiscalCode()));
        
        log(ErrorLevel.DEBUG, "activatePromotion", activatePromotionRequest.getFiscalCode(), "opening", ActivityLog.createLogMessage(inputParameters));
        
        ActivatePromotionMessageResponse activatePromotionResponse = new ActivatePromotionMessageResponse();
        
        try {
            
            if (activatePromotionRequest.getPromotionID() == null || activatePromotionRequest.getPromotionID().isEmpty() || !activatePromotionRequest.getPromotionID().equals(DWHAdapter.VHB_PROMOTION_ID)) {
                log(ErrorLevel.DEBUG, "activatePromotion", activatePromotionRequest.getFiscalCode(), "closing", "Invalid promotionID: " + activatePromotionRequest.getPromotionID());
                activatePromotionResponse.setStatusCode(ResponseHelper.ACTIVATE_PROMOTION_SYSTEM_ERROR);
                return activatePromotionResponse;
            }
            
            if (activatePromotionRequest.getFiscalCode() == null || activatePromotionRequest.getFiscalCode().isEmpty()) {
                log(ErrorLevel.DEBUG, "activatePromotion", activatePromotionRequest.getFiscalCode(), "closing", "Fiscalcode null or empty");
                activatePromotionResponse.setStatusCode(ResponseHelper.ACTIVATE_PROMOTION_SYSTEM_ERROR);
                return activatePromotionResponse;
            }
            
            dwhService = EJBHomeCache.getInstance().getDWHService();
            String response = dwhService.activatePromotion(activatePromotionRequest.getPromotionID(), activatePromotionRequest.getFiscalCode());
            
            if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.DWH_ACTIVATE_PROMOTION_SUCCESS)) {
            	activatePromotionResponse.setStatusCode(ResponseHelper.ACTIVATE_PROMOTION_SUCCESS);
            }
            if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.DWH_ACTIVATE_PROMOTION_USER_NOT_FOUND)) {
            	activatePromotionResponse.setStatusCode(ResponseHelper.ACTIVATE_PROMOTION_CF_NOT_REGISTER);
            }
            if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.DWH_ACTIVATE_PROMOTION_CARD_NOT_FOUND)) {
            	activatePromotionResponse.setStatusCode(ResponseHelper.ACTIVATE_PROMOTION_CF_NOT_CREDIT_CARD);
            }
            if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.DWH_ACTIVATE_PROMOTION_ALREADY_EXISTS)) {
            	activatePromotionResponse.setStatusCode(ResponseHelper.ACTIVATE_PROMOTION_ALREADY_ACTIVE);
            }
            if (response.equals(com.techedge.mp.core.business.interfaces.ResponseHelper.DWH_ACTIVATE_PROMOTION_SYSTEM_ERROR)) {
            	activatePromotionResponse.setStatusCode(ResponseHelper.ACTIVATE_PROMOTION_SYSTEM_ERROR);
            }
        }
        catch (InterfaceNotFoundException ex) {
            log(ErrorLevel.DEBUG, "activatePromotion", activatePromotionRequest.getFiscalCode(), "closing", ex.getMessage());
            activatePromotionResponse.setStatusCode(ResponseHelper.ACTIVATE_PROMOTION_SYSTEM_ERROR);
            return activatePromotionResponse;
        }
        
        Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
        outputParameters.add(new Pair<String, String>("statusCode", activatePromotionResponse.getStatusCode()));
        outputParameters.add(new Pair<String, String>("statusMessage", activatePromotionResponse.getMessageCode()));

        log(ErrorLevel.INFO, "activatePromotion", activatePromotionRequest.getFiscalCode(), "closing", ActivityLog.createLogMessage(outputParameters));
        
        return activatePromotionResponse;
    }

}
