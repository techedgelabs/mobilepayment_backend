package com.techedge.mp.dwh.adapter.service.entities;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "activatePromotionMessageRequest", propOrder = { "promotionID","fiscalCode" })
public class ActivatePromotionMessageRequest {

	@XmlElement(required = true, nillable = false)
	private String promotionID;

	@XmlElement(required = true, nillable = false)
	private String fiscalCode;

	public String getPromotionID() {
		return promotionID;
	}

	public void setPromotionID(String promotionID) {
		this.promotionID = promotionID;
	}

	public String getFiscalCode() {
		return fiscalCode;
	}

	public void setFiscalCode(String fiscalCode) {
		this.fiscalCode = fiscalCode;
	}

}
