package com.techedge.mp.dwh.adapter.service.utilities;

public class ResponseHelper {

    public final static String USER_CHANGE_PASSWORD_SUCCESS             = "USER_CHANGE_PASSWORD_200";
    public final static String USER_CHANGE_PASSWORD_INVALID_REQUEST     = "USER_CHANGE_PASSWORD_400";
    public final static String USER_CHANGE_PASSWORD_SYSTEM_ERROR        = "USER_CHANGE_PASSWORD_500";
    
    public final static String ACTIVATE_PROMOTION_SUCCESS				="ACTIVATE_PROMOTION_200";
    public final static String ACTIVATE_PROMOTION_CF_NOT_REGISTER		="ACTIVATE_PROMOTION_400";
    public final static String ACTIVATE_PROMOTION_CF_NOT_CREDIT_CARD	="ACTIVATE_PROMOTION_301";
    public final static String ACTIVATE_PROMOTION_ALREADY_ACTIVE	 	="ACTIVATE_PROMOTION_409";
    public final static String ACTIVATE_PROMOTION_SYSTEM_ERROR	 		="ACTIVATE_PROMOTION_500";

}
