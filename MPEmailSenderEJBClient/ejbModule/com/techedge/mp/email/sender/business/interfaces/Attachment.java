package com.techedge.mp.email.sender.business.interfaces;

import java.io.Serializable;

public class Attachment implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 2637190815197857505L;

    private byte[]            bytes;
    private String            fileName;

    public Attachment() {}

    public byte[] getBytes() {
        return bytes;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
