package com.techedge.mp.email.sender.business;

import java.util.List;

import javax.ejb.Remote;

import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Remote
public interface EmailSenderRemote {

    public void init(String host, String port, String proxyHost, String proxyPort, String proxyNoHosts, String from);

    public String sendEmail(EmailType type, String from, String to, String cc, String bcc, String subject, List<Parameter> parameters);
    
    public String sendEmail(EmailType type, String from, String to, List<Parameter> parameters);

    public String sendEmail(EmailType type, String to, List<Parameter> parameters);

    public String sendEmailWithAttachments(EmailType type, String from, String to, String cc, String bcc, String subject, List<Parameter> parameters, List<Attachment> attachments);
    
    public String sendEmailWithAttachments(EmailType type, String from, String to, String subject, List<Parameter> parameters, List<Attachment> attachments);

    public String sendEmailWithAttachments(EmailType type, String to, String subject, List<Parameter> parameters, List<Attachment> attachments);
    
    public String sendEmailWithAttachments(EmailType type, String to, List<Parameter> parameters, List<Attachment> attachments);
    
    public String getSender();
}
