package com.techedge.mp.crm.adapter.clientoauth;

import java.io.ByteArrayOutputStream;

import javax.xml.namespace.QName;
import javax.xml.rpc.JAXRPCException;
import javax.xml.rpc.handler.GenericHandler;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.MessageContext;
import javax.xml.rpc.handler.soap.SOAPMessageContext;
import javax.xml.soap.Detail;
import javax.xml.soap.DetailEntry;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPFault;
import javax.xml.soap.SOAPMessage;

import com.techedge.mp.crm.adapter.business.CRMAdapterService;

public class OAuth2AuthorizationHandler extends GenericHandler {

    @Override
    public void init(HandlerInfo config) {
        super.init(config);
    }

    @Override
    public QName[] getHeaders() {
        return null;
    }

    public boolean handleRequest(MessageContext messageContext) throws JAXRPCException {

        System.out.println("HandlerChain [OAuthValidation]: handleRequest()......");

        SOAPMessageContext soapMessageContext = (SOAPMessageContext) messageContext;
        
        System.out.println("=========== SOAP REQUEST ===========");

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            soapMessageContext.getMessage().writeTo(baos);
            String responseContent = baos.toString();
            System.out.println(responseContent.replaceAll("/>", "/>\n"));
        }
        catch (Exception ex) {
            System.err.println("handleRequest error: " + ex.getMessage());
        }
        
        System.out.println("=========== END ===========");

        try {
            addOAuthHeaders(soapMessageContext);
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("OAuth autentication error: " + ex.getMessage());
            soapMessageContext.setMessage(generateSOAPFaultMessage("OAuth autentication error", ex.getMessage()));
            return false;
        }
        //continue other handler chain
        return true;
    }

    private void addOAuthHeaders(SOAPMessageContext messageContext) throws Exception {
        SOAPMessage soapMessage = messageContext.getMessage();
        MimeHeaders httpHeaders = soapMessage.getMimeHeaders();
        httpHeaders.addHeader("Content-Type", "text/xml");
        httpHeaders.addHeader("Authorization", "Bearer " + CRMAdapterService.getAccessToken().getAccess_token());
    }
    
    private SOAPMessage generateSOAPFaultMessage(String error, String errorDescription) {
        SOAPMessage soapMessage = null;
        try {
            soapMessage = MessageFactory.newInstance().createMessage();
            soapMessage.getSOAPPart().getEnvelope().addNamespaceDeclaration("ns1", "http://service.adapter.dwh.mp.techedge.com");
            SOAPFault soapFault = soapMessage.getSOAPBody().addFault();
            QName faultName = new QName(SOAPConstants.URI_NS_SOAP_ENVELOPE, "Server");
            soapFault.setFaultCode((Name) faultName);
            soapFault.setFaultString(error);
            Detail detail = soapFault.addDetail();
            QName entryName = new QName("http://service.adapter.dwh.mp.techedge.com", "OAuthAutenticationException", "ns1");
            DetailEntry entry = detail.addDetailEntry((Name) entryName);
            entry.addTextNode((errorDescription != null) ? errorDescription : error);
        }
        catch (SOAPException ex) {
            System.err.println("Errore nella creazione del messaggio soap: " + ex.getMessage());
        }

        return soapMessage;
    }
    
    
    public boolean handleResponse(MessageContext messageContext) throws JAXRPCException {

        System.out.println("HandlerChain [ResponseHandler]: handleResponse()......");

        SOAPMessageContext soapMessageContext = (SOAPMessageContext) messageContext;

        System.out.println("=========== SOAP RESPONSE ===========");

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            soapMessageContext.getMessage().writeTo(baos);
            String responseContent = baos.toString();
            System.out.println(responseContent.replaceAll("/>", "/>\n"));
        }
        catch (Exception ex) {
            System.err.println("handleRequest error: " + ex.getMessage());
        }
        
        System.out.println("=========== END ===========");
        
        //continue other handler chain
        return true;
    } 
    
}
