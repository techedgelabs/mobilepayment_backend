package com.techedge.mp.crm.adapter.clientoauth;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class OAuth2Client {
	
	 public static Gson gson = new GsonBuilder().disableHtmlEscaping().create();

    private int        responseHttpStatus;

    private Proxy      proxy;

    private String     proxyHost;

    private Integer    proxyPort;
    
    private String     keyManagerAlgorithm = "SunX509";
    private String     keyStoreType        = "JKS";
    
    private String     keyStore;
    private String     keyStorePassword;
    private String     keyPassword;

    private final String USER_AGENT = "Mozilla/5.0";
    
    public OAuth2Client(String proxyHost, Integer proxyPort, String keyStore, String keyStorePassword, String keyPassword) {
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        
        this.keyStore         = keyStore;
        this.keyStorePassword = keyStorePassword;
        this.keyPassword      = keyPassword;

        if (this.proxyHost != null && this.proxyPort != null) {
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(this.proxyHost, this.proxyPort));
            System.out.println("Proxy inizializzato");
        }
    }

    private HttpsURLConnection initializeConnection(String url) throws IOException {
        URL obj = new URL(url);
        HttpsURLConnection httpsURLConnection;

        if (proxy != null) {
            httpsURLConnection = (HttpsURLConnection) obj.openConnection(proxy);
        }
        else {
            httpsURLConnection = (HttpsURLConnection) obj.openConnection();
        }
        
        httpsURLConnection.setHostnameVerifier(getHostnameVerifier());
        httpsURLConnection.setSSLSocketFactory(getSslContext().getSocketFactory());

        httpsURLConnection.setRequestProperty("User-Agent", USER_AGENT);
        httpsURLConnection.setRequestProperty("Charset", "utf-8");
        
        return httpsURLConnection;
    }

    public SSLContext getSslContext() {
        
        SSLContext sslContext = null;
        
        TrustManager[] trustAllCerts = new X509TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }

            public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) {}

            public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType) {}
        } };

        try {
            sslContext = SSLContext.getInstance("TLSv1.2");
            //sslContext.init(null, trustAllCerts, null);
            
            KeyManagerFactory kmf = KeyManagerFactory.getInstance(this.keyManagerAlgorithm);
            KeyStore ks = KeyStore.getInstance(this.keyStoreType);
            ks.load(new FileInputStream(this.keyStore), this.keyStorePassword.toCharArray());
            kmf.init(ks, this.keyPassword.toCharArray());

            sslContext.init(null, trustAllCerts, null);
            
            SSLContext.setDefault(sslContext);

        }
        catch (NoSuchAlgorithmException ex) {
            System.err.println("Errore inizializzazione SSL: " + ex.getMessage());
        }
        catch (KeyManagementException ex) {
            System.err.println("Errore inizializzazione SSL: " + ex.getMessage());
        }
        catch (KeyStoreException ex) {
            System.err.println("Errore inizializzazione SSL: " + ex.getMessage());
        }
        catch (CertificateException ex) {
            System.err.println("Errore inizializzazione SSL: " + ex.getMessage());
        }
        catch (FileNotFoundException ex) {
            System.err.println("Errore inizializzazione SSL: " + ex.getMessage());
        }
        catch (IOException ex) {
            System.err.println("Errore inizializzazione SSL: " + ex.getMessage());
        }
        catch (UnrecoverableKeyException ex) {
            System.err.println("Errore inizializzazione SSL: " + ex.getMessage());
        }
        return sslContext;
    }

    private HostnameVerifier getHostnameVerifier() {
        return new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
                return true;
            }
        };
    }

    
    public AccessToken getAccessToken(String endPoint, String clientId, String clientSecret) throws IOException {

        HttpsURLConnection httpsURLConnection = initializeConnection(endPoint);

        //byte[] byteEncoded = new String(clientId + ":" + clientSecret).getBytes();
        //String basicAuth = encoder.encode(byteEncoded);

        httpsURLConnection.setRequestMethod("POST");
        //httpsURLConnection.setRequestProperty("Authorization", "Basic " + basicAuth);
        httpsURLConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

        httpsURLConnection.setDoOutput(true);

        HashMap<String, String> queryParams = new HashMap();
        queryParams.put("grant_type", "client_credentials");
        queryParams.put("client_id", clientId);
        queryParams.put("client_secret", clientSecret);
        String formEncodedData = getFormEncodedData(queryParams);

        DataOutputStream wr = new DataOutputStream(httpsURLConnection.getOutputStream());
        wr.writeBytes(formEncodedData);
        wr.flush();
        wr.close();

        responseHttpStatus = httpsURLConnection.getResponseCode();
        System.out.println("\nSending 'POST' request to URL: " + endPoint);
        System.out.println("Request: " + formEncodedData);
        System.out.println("Response Code: " + responseHttpStatus);

        BufferedReader in = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        //print result
        String jsonResponse = response.toString();
        System.out.println("Response: " + jsonResponse);
        AccessToken auth = gson.fromJson(jsonResponse, AccessToken.class);
        
        
        return auth;
    }

    private String getFormEncodedData(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");
            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }
        return result.toString();
    }

    public int getResponseHttpStatus() {
        return responseHttpStatus;
    }

}
