/**
 * InteractServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.client;
//FIXME: Controllare se serve questa classe
public class InteractServiceLocator extends org.apache.axis.client.Service implements InteractService {

    public InteractServiceLocator() {
    }


    public InteractServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public InteractServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }
/*
    @Override
    public HandlerRegistry getHandlerRegistry() {
        HandlerRegistry registry = super.getHandlerRegistry();
        QName portName = new javax.xml.namespace.QName("http://soap.api.interact.unicacorp.com", "InteractSoapServiceHttpSoap11Endpoint");
        List handlerChain = registry.getHandlerChain(portName);

        HandlerInfo hi = new HandlerInfo();
        hi.setHandlerClass(SoapMessageHandler.class);
        handlerChain.add(hi);        
 
       return registry;
    }
*/    
    // Use to get a proxy class for InteractSoapServiceHttpSoap11Endpoint
    private java.lang.String InteractSoapServiceHttpSoap11Endpoint_address = "http://ma.st-ibmcrm.eni.com:9080/interact/services/InteractService";

    public java.lang.String getInteractSoapServiceHttpSoap11EndpointAddress() {
        return InteractSoapServiceHttpSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String InteractSoapServiceHttpSoap11EndpointWSDDServiceName = "InteractSoapServiceHttpSoap11Endpoint";

    public java.lang.String getInteractSoapServiceHttpSoap11EndpointWSDDServiceName() {
        return InteractSoapServiceHttpSoap11EndpointWSDDServiceName;
    }

    public void setInteractSoapServiceHttpSoap11EndpointWSDDServiceName(java.lang.String name) {
        InteractSoapServiceHttpSoap11EndpointWSDDServiceName = name;
    }

    public InteractSoapServicePortType getInteractSoapServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(InteractSoapServiceHttpSoap11Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getInteractSoapServiceHttpSoap11Endpoint(endpoint);
    }

    public InteractSoapServicePortType getInteractSoapServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            InteractSoapServiceSoap11BindingStub _stub = new InteractSoapServiceSoap11BindingStub(portAddress, this);
            _stub.setPortName(getInteractSoapServiceHttpSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setInteractSoapServiceHttpSoap11EndpointEndpointAddress(java.lang.String address) {
        InteractSoapServiceHttpSoap11Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (InteractSoapServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                InteractSoapServiceSoap11BindingStub _stub = new InteractSoapServiceSoap11BindingStub(new java.net.URL(InteractSoapServiceHttpSoap11Endpoint_address), this);
                _stub.setPortName(getInteractSoapServiceHttpSoap11EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("InteractSoapServiceHttpSoap11Endpoint".equals(inputPortName)) {
            return getInteractSoapServiceHttpSoap11Endpoint();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://soap.api.interact.unicacorp.com", "InteractService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://soap.api.interact.unicacorp.com", "InteractSoapServiceHttpSoap11Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("InteractSoapServiceHttpSoap11Endpoint".equals(portName)) {
            setInteractSoapServiceHttpSoap11EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
