package com.techedge.mp.crm.adapter.client;
/**
 * InteractService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */


public interface InteractService extends javax.xml.rpc.Service {
    public java.lang.String getInteractSoapServiceHttpSoap11EndpointAddress();

    public InteractSoapServicePortType getInteractSoapServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public InteractSoapServicePortType getInteractSoapServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
