package com.techedge.mp.crm.adapter.client.handler;

import java.io.ByteArrayOutputStream;

import javax.xml.namespace.QName;
import javax.xml.rpc.JAXRPCException;
import javax.xml.rpc.handler.GenericHandler;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.MessageContext;
import javax.xml.rpc.handler.soap.SOAPMessageContext;

public class SoapMessageHandler extends GenericHandler {

    /**
     * 
     */
    //FIXME: controllare se serve serialVersionUID
    private static final long       serialVersionUID             = 2344096747378583010L;

    @Override
    public void init(HandlerInfo config) {
        super.init(config);
    }

    @Override
    public QName[] getHeaders() {
        return null;
    }

    public boolean handleRequest(MessageContext messageContext) throws JAXRPCException {

        System.out.println("HandlerChain [OAuthValidation]: handleRequest()......");

        SOAPMessageContext soapMessageContext = (SOAPMessageContext) messageContext;
        
        System.out.println("=========== SOAP REQUEST ===========");

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            soapMessageContext.getMessage().writeTo(baos);
            String responseContent = baos.toString();
            System.out.println(responseContent.replaceAll("/>", "/>\n"));
        }
        catch (Exception ex) {
            System.err.println("handleRequest error: " + ex.getMessage());
        }
        
        System.out.println("=========== END ===========");

        //continue other handler chain
        return true;
    }
    
    public boolean handleResponse(MessageContext messageContext) throws JAXRPCException {

        System.out.println("HandlerChain [ResponseHandler]: handleResponse()......");

        SOAPMessageContext soapMessageContext = (SOAPMessageContext) messageContext;

        System.out.println("=========== SOAP RESPONSE ===========");

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            soapMessageContext.getMessage().writeTo(baos);
            String responseContent = baos.toString();
            System.out.println(responseContent.replaceAll("/>", "/>\n"));
        }
        catch (Exception ex) {
            System.err.println("handleRequest error: " + ex.getMessage());
        }
        
        System.out.println("=========== END ===========");
        
        //continue other handler chain
        return true;
    }    


}
