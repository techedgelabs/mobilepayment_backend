package com.techedge.mp.crm.adapter.client;

import com.techedge.mp.crm.adapter.elements.BatchResponse;
import com.techedge.mp.crm.adapter.elements.CommandImpl;
import com.techedge.mp.crm.adapter.elements.GetOfferRequest;
import com.techedge.mp.crm.adapter.elements.NameValuePairImpl;
import com.techedge.mp.crm.adapter.elements.Response;

public class InteractSoapServicePortTypeProxy implements InteractSoapServicePortType {
  private String _endpoint = null;
  private InteractSoapServicePortType interactSoapServicePortType = null;
  
  public InteractSoapServicePortTypeProxy() {
    _initInteractSoapServicePortTypeProxy();
  }
  
  public InteractSoapServicePortTypeProxy(String endpoint) {
    _endpoint = endpoint;
    _initInteractSoapServicePortTypeProxy();
  }
  
  private void _initInteractSoapServicePortTypeProxy() {
    try {
      interactSoapServicePortType = (new InteractServiceLocator()).getInteractSoapServiceHttpSoap11Endpoint();
      if (interactSoapServicePortType != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)interactSoapServicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)interactSoapServicePortType)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (interactSoapServicePortType != null)
      ((javax.xml.rpc.Stub)interactSoapServicePortType)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public InteractSoapServicePortType getInteractSoapServicePortType() {
    if (interactSoapServicePortType == null)
      _initInteractSoapServicePortTypeProxy();
    return interactSoapServicePortType;
  }
  
  public Response getOffersForMultipleInteractionPoints(java.lang.String sessionID, GetOfferRequest[] requests) throws java.rmi.RemoteException{
    if (interactSoapServicePortType == null)
      _initInteractSoapServicePortTypeProxy();
    return interactSoapServicePortType.getOffersForMultipleInteractionPoints(sessionID, requests);
  }
  
  public Response getProfile(java.lang.String sessionID) throws java.rmi.RemoteException{
    if (interactSoapServicePortType == null)
      _initInteractSoapServicePortTypeProxy();
    return interactSoapServicePortType.getProfile(sessionID);
  }
  
  public Response getVersion() throws java.rmi.RemoteException{
    if (interactSoapServicePortType == null)
      _initInteractSoapServicePortTypeProxy();
    return interactSoapServicePortType.getVersion();
  }
  
  public Response postEvent(java.lang.String sessionID, java.lang.String eventName, NameValuePairImpl[] eventParameters) throws java.rmi.RemoteException{
    if (interactSoapServicePortType == null)
      _initInteractSoapServicePortTypeProxy();
    return interactSoapServicePortType.postEvent(sessionID, eventName, eventParameters);
  }
  
  public BatchResponse executeBatch(java.lang.String sessionID, CommandImpl[] commands) throws java.rmi.RemoteException{
    if (interactSoapServicePortType == null)
      _initInteractSoapServicePortTypeProxy();
    return interactSoapServicePortType.executeBatch(sessionID, commands);
  }
  
  public Response getOffers(java.lang.String sessionID, java.lang.String iPoint, int numberRequested) throws java.rmi.RemoteException{
    if (interactSoapServicePortType == null)
      _initInteractSoapServicePortTypeProxy();
    return interactSoapServicePortType.getOffers(sessionID, iPoint, numberRequested);
  }
  
  public Response setDebug(java.lang.String sessionID, boolean debug) throws java.rmi.RemoteException{
    if (interactSoapServicePortType == null)
      _initInteractSoapServicePortTypeProxy();
    return interactSoapServicePortType.setDebug(sessionID, debug);
  }
  
  public Response setAudience(java.lang.String sessionID, NameValuePairImpl[] audienceID, java.lang.String audienceLevel, NameValuePairImpl[] parameters) throws java.rmi.RemoteException{
    if (interactSoapServicePortType == null)
      _initInteractSoapServicePortTypeProxy();
    return interactSoapServicePortType.setAudience(sessionID, audienceID, audienceLevel, parameters);
  }
  
  public Response startSession(java.lang.String sessionID, boolean relyOnExistingSession, boolean debug, java.lang.String interactiveChannel, NameValuePairImpl[] audienceID, java.lang.String audienceLevel, NameValuePairImpl[] parameters) throws java.rmi.RemoteException{
    if (interactSoapServicePortType == null)
      _initInteractSoapServicePortTypeProxy();
    return interactSoapServicePortType.startSession(sessionID, relyOnExistingSession, debug, interactiveChannel, audienceID, audienceLevel, parameters);
  }
  
  public Response endSession(java.lang.String sessionID) throws java.rmi.RemoteException{
    if (interactSoapServicePortType == null)
      _initInteractSoapServicePortTypeProxy();
    return interactSoapServicePortType.endSession(sessionID);
  }
  
  
}