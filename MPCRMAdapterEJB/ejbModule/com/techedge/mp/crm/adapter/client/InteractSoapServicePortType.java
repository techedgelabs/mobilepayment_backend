/**
 * InteractSoapServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.client;

import com.techedge.mp.crm.adapter.elements.BatchResponse;
import com.techedge.mp.crm.adapter.elements.CommandImpl;
import com.techedge.mp.crm.adapter.elements.GetOfferRequest;
import com.techedge.mp.crm.adapter.elements.NameValuePairImpl;
import com.techedge.mp.crm.adapter.elements.Response;

public interface InteractSoapServicePortType extends java.rmi.Remote {
    public Response getOffersForMultipleInteractionPoints(java.lang.String sessionID, GetOfferRequest[] requests) throws java.rmi.RemoteException;
    public Response getProfile(java.lang.String sessionID) throws java.rmi.RemoteException;
    public Response getVersion() throws java.rmi.RemoteException;
    public Response postEvent(java.lang.String sessionID, java.lang.String eventName, NameValuePairImpl[] eventParameters) throws java.rmi.RemoteException;
    public BatchResponse executeBatch(java.lang.String sessionID, CommandImpl[] commands) throws java.rmi.RemoteException;
    public Response getOffers(java.lang.String sessionID, java.lang.String iPoint, int numberRequested) throws java.rmi.RemoteException;
    public Response setDebug(java.lang.String sessionID, boolean debug) throws java.rmi.RemoteException;
    public Response setAudience(java.lang.String sessionID, NameValuePairImpl[] audienceID, java.lang.String audienceLevel, NameValuePairImpl[] parameters) throws java.rmi.RemoteException;
    public Response startSession(java.lang.String sessionID, boolean relyOnExistingSession, boolean debug, java.lang.String interactiveChannel, NameValuePairImpl[] audienceID, java.lang.String audienceLevel, NameValuePairImpl[] parameters) throws java.rmi.RemoteException;
    public Response endSession(java.lang.String sessionID) throws java.rmi.RemoteException;
}
