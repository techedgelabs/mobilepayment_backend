package com.techedge.mp.crm.adapter.business;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.techedge.mp.core.business.CRMSfTokenServiceRemote;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.UserServiceRemote;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;

public class EJBHomeCache {

	final String parametersServiceRemoteJndi  = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/ParametersService!com.techedge.mp.core.business.ParametersServiceRemote";
	final String loggerServiceRemoteJndi      = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/LoggerService!com.techedge.mp.core.business.LoggerServiceRemote";
	final String userServiceRemoteJndi        = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/UserService!com.techedge.mp.core.business.UserServiceRemote";
	final String crmSfTokenServiceRemoteJndi  = "java:global/MPCoreBusinessEAR/MPCoreBusinessEJB/CRMSfTokenService!com.techedge.mp.core.business.CRMSfTokenServiceRemote";
	
	private static EJBHomeCache instance;
	
	protected Context context = null;
	
	protected ParametersServiceRemote parametersService = null;
	protected LoggerServiceRemote     loggerService     = null;
	protected UserServiceRemote       userService       = null;
	protected CRMSfTokenServiceRemote crmSfTokenService = null;
	
	private EJBHomeCache( ) throws InterfaceNotFoundException {
		
		final Hashtable<String, String> jndiProperties = new Hashtable<String, String>();
		
		jndiProperties.put(Context.URL_PKG_PREFIXES, "org.jboss.ejb.client.naming");
		
		try {
			context = new InitialContext(jndiProperties);
		} catch (NamingException e) {

			throw new InterfaceNotFoundException("Naming exception: " + e.getMessage());
		}
		
        try {
            loggerService = (LoggerServiceRemote)context.lookup(loggerServiceRemoteJndi);
        }
        catch (Exception ex) {

            throw new InterfaceNotFoundException(loggerServiceRemoteJndi);
        }

        try {
            parametersService = (ParametersServiceRemote)context.lookup(parametersServiceRemoteJndi);
		}
		catch (Exception ex) {

        	throw new InterfaceNotFoundException(parametersServiceRemoteJndi);
        }
        
        try {
        	crmSfTokenService = (CRMSfTokenServiceRemote)context.lookup(crmSfTokenServiceRemoteJndi);
		}
		catch (Exception ex) {

        	throw new InterfaceNotFoundException(crmSfTokenServiceRemoteJndi);
        }

		/*try {
            userService = (UserServiceRemote)context.lookup(userServiceRemoteJndi);
        }
		catch (Exception ex) {

        	throw new InterfaceNotFoundException(userServiceRemoteJndi);
        }*/
		
	}

	public static synchronized EJBHomeCache getInstance( ) throws InterfaceNotFoundException
	{
		if (instance == null)
			instance = new EJBHomeCache( );
		
		return instance;
	}

	public ParametersServiceRemote getParametersService( )
	{
		return parametersService;
	}
	
	public LoggerServiceRemote getLoggerService( )
	{
		return loggerService;
	}
	
	public UserServiceRemote getUserService( )
	{
		return userService;
	}

	public CRMSfTokenServiceRemote getCrmSfTokenService() {
		return crmSfTokenService;
	}
	
	 
}
