/**
 * RifornimentoPagamentoApp.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.clientcrmsf;

public class RifornimentoPagamentoApp  implements java.io.Serializable {
    private java.lang.String codice_fiscale;

    private java.util.Calendar data;

    private java.lang.String impianto;

    private java.lang.String prodotto;

    private boolean flag_pagamento;

    private java.lang.String modalita_pagamento;

    private int punti;

    private java.math.BigDecimal litri;

    private java.lang.String modalita_operativa;

    private java.lang.String cognome;

    private boolean fg_notifica;

    private boolean fg_1;

    private java.lang.String carta;

    private java.lang.String cluster;

    private java.math.BigDecimal importo;

    private boolean flg_privacy1;

    private boolean flg_privacy2;

    private java.lang.String num_carta_loyalty;

    private java.lang.String parameter1;

    private java.lang.String parameter2;

    public RifornimentoPagamentoApp() {
    }

    public RifornimentoPagamentoApp(
           java.lang.String codice_fiscale,
           java.util.Calendar data,
           java.lang.String impianto,
           java.lang.String prodotto,
           boolean flag_pagamento,
           java.lang.String modalita_pagamento,
           int punti,
           java.math.BigDecimal litri,
           java.lang.String modalita_operativa,
           java.lang.String cognome,
           boolean fg_notifica,
           boolean fg_1,
           java.lang.String carta,
           java.lang.String cluster,
           java.math.BigDecimal importo,
           boolean flg_privacy1,
           boolean flg_privacy2,
           java.lang.String num_carta_loyalty,
           java.lang.String parameter1,
           java.lang.String parameter2) {
           this.codice_fiscale = codice_fiscale;
           this.data = data;
           this.impianto = impianto;
           this.prodotto = prodotto;
           this.flag_pagamento = flag_pagamento;
           this.modalita_pagamento = modalita_pagamento;
           this.punti = punti;
           this.litri = litri;
           this.modalita_operativa = modalita_operativa;
           this.cognome = cognome;
           this.fg_notifica = fg_notifica;
           this.fg_1 = fg_1;
           this.carta = carta;
           this.cluster = cluster;
           this.importo = importo;
           this.flg_privacy1 = flg_privacy1;
           this.flg_privacy2 = flg_privacy2;
           this.num_carta_loyalty = num_carta_loyalty;
           this.parameter1 = parameter1;
           this.parameter2 = parameter2;
    }


    /**
     * Gets the codice_fiscale value for this RifornimentoPagamentoApp.
     * 
     * @return codice_fiscale
     */
    public java.lang.String getCodice_fiscale() {
        return codice_fiscale;
    }


    /**
     * Sets the codice_fiscale value for this RifornimentoPagamentoApp.
     * 
     * @param codice_fiscale
     */
    public void setCodice_fiscale(java.lang.String codice_fiscale) {
        this.codice_fiscale = codice_fiscale;
    }


    /**
     * Gets the data value for this RifornimentoPagamentoApp.
     * 
     * @return data
     */
    public java.util.Calendar getData() {
        return data;
    }


    /**
     * Sets the data value for this RifornimentoPagamentoApp.
     * 
     * @param data
     */
    public void setData(java.util.Calendar data) {
        this.data = data;
    }


    /**
     * Gets the impianto value for this RifornimentoPagamentoApp.
     * 
     * @return impianto
     */
    public java.lang.String getImpianto() {
        return impianto;
    }


    /**
     * Sets the impianto value for this RifornimentoPagamentoApp.
     * 
     * @param impianto
     */
    public void setImpianto(java.lang.String impianto) {
        this.impianto = impianto;
    }


    /**
     * Gets the prodotto value for this RifornimentoPagamentoApp.
     * 
     * @return prodotto
     */
    public java.lang.String getProdotto() {
        return prodotto;
    }


    /**
     * Sets the prodotto value for this RifornimentoPagamentoApp.
     * 
     * @param prodotto
     */
    public void setProdotto(java.lang.String prodotto) {
        this.prodotto = prodotto;
    }


    /**
     * Gets the flag_pagamento value for this RifornimentoPagamentoApp.
     * 
     * @return flag_pagamento
     */
    public boolean isFlag_pagamento() {
        return flag_pagamento;
    }


    /**
     * Sets the flag_pagamento value for this RifornimentoPagamentoApp.
     * 
     * @param flag_pagamento
     */
    public void setFlag_pagamento(boolean flag_pagamento) {
        this.flag_pagamento = flag_pagamento;
    }


    /**
     * Gets the modalita_pagamento value for this RifornimentoPagamentoApp.
     * 
     * @return modalita_pagamento
     */
    public java.lang.String getModalita_pagamento() {
        return modalita_pagamento;
    }


    /**
     * Sets the modalita_pagamento value for this RifornimentoPagamentoApp.
     * 
     * @param modalita_pagamento
     */
    public void setModalita_pagamento(java.lang.String modalita_pagamento) {
        this.modalita_pagamento = modalita_pagamento;
    }


    /**
     * Gets the punti value for this RifornimentoPagamentoApp.
     * 
     * @return punti
     */
    public int getPunti() {
        return punti;
    }


    /**
     * Sets the punti value for this RifornimentoPagamentoApp.
     * 
     * @param punti
     */
    public void setPunti(int punti) {
        this.punti = punti;
    }


    /**
     * Gets the litri value for this RifornimentoPagamentoApp.
     * 
     * @return litri
     */
    public java.math.BigDecimal getLitri() {
        return litri;
    }


    /**
     * Sets the litri value for this RifornimentoPagamentoApp.
     * 
     * @param litri
     */
    public void setLitri(java.math.BigDecimal litri) {
        this.litri = litri;
    }


    /**
     * Gets the modalita_operativa value for this RifornimentoPagamentoApp.
     * 
     * @return modalita_operativa
     */
    public java.lang.String getModalita_operativa() {
        return modalita_operativa;
    }


    /**
     * Sets the modalita_operativa value for this RifornimentoPagamentoApp.
     * 
     * @param modalita_operativa
     */
    public void setModalita_operativa(java.lang.String modalita_operativa) {
        this.modalita_operativa = modalita_operativa;
    }


    /**
     * Gets the cognome value for this RifornimentoPagamentoApp.
     * 
     * @return cognome
     */
    public java.lang.String getCognome() {
        return cognome;
    }


    /**
     * Sets the cognome value for this RifornimentoPagamentoApp.
     * 
     * @param cognome
     */
    public void setCognome(java.lang.String cognome) {
        this.cognome = cognome;
    }


    /**
     * Gets the fg_notifica value for this RifornimentoPagamentoApp.
     * 
     * @return fg_notifica
     */
    public boolean isFg_notifica() {
        return fg_notifica;
    }


    /**
     * Sets the fg_notifica value for this RifornimentoPagamentoApp.
     * 
     * @param fg_notifica
     */
    public void setFg_notifica(boolean fg_notifica) {
        this.fg_notifica = fg_notifica;
    }


    /**
     * Gets the fg_1 value for this RifornimentoPagamentoApp.
     * 
     * @return fg_1
     */
    public boolean isFg_1() {
        return fg_1;
    }


    /**
     * Sets the fg_1 value for this RifornimentoPagamentoApp.
     * 
     * @param fg_1
     */
    public void setFg_1(boolean fg_1) {
        this.fg_1 = fg_1;
    }


    /**
     * Gets the carta value for this RifornimentoPagamentoApp.
     * 
     * @return carta
     */
    public java.lang.String getCarta() {
        return carta;
    }


    /**
     * Sets the carta value for this RifornimentoPagamentoApp.
     * 
     * @param carta
     */
    public void setCarta(java.lang.String carta) {
        this.carta = carta;
    }


    /**
     * Gets the cluster value for this RifornimentoPagamentoApp.
     * 
     * @return cluster
     */
    public java.lang.String getCluster() {
        return cluster;
    }


    /**
     * Sets the cluster value for this RifornimentoPagamentoApp.
     * 
     * @param cluster
     */
    public void setCluster(java.lang.String cluster) {
        this.cluster = cluster;
    }


    /**
     * Gets the importo value for this RifornimentoPagamentoApp.
     * 
     * @return importo
     */
    public java.math.BigDecimal getImporto() {
        return importo;
    }


    /**
     * Sets the importo value for this RifornimentoPagamentoApp.
     * 
     * @param importo
     */
    public void setImporto(java.math.BigDecimal importo) {
        this.importo = importo;
    }


    /**
     * Gets the flg_privacy1 value for this RifornimentoPagamentoApp.
     * 
     * @return flg_privacy1
     */
    public boolean isFlg_privacy1() {
        return flg_privacy1;
    }


    /**
     * Sets the flg_privacy1 value for this RifornimentoPagamentoApp.
     * 
     * @param flg_privacy1
     */
    public void setFlg_privacy1(boolean flg_privacy1) {
        this.flg_privacy1 = flg_privacy1;
    }


    /**
     * Gets the flg_privacy2 value for this RifornimentoPagamentoApp.
     * 
     * @return flg_privacy2
     */
    public boolean isFlg_privacy2() {
        return flg_privacy2;
    }


    /**
     * Sets the flg_privacy2 value for this RifornimentoPagamentoApp.
     * 
     * @param flg_privacy2
     */
    public void setFlg_privacy2(boolean flg_privacy2) {
        this.flg_privacy2 = flg_privacy2;
    }


    /**
     * Gets the num_carta_loyalty value for this RifornimentoPagamentoApp.
     * 
     * @return num_carta_loyalty
     */
    public java.lang.String getNum_carta_loyalty() {
        return num_carta_loyalty;
    }


    /**
     * Sets the num_carta_loyalty value for this RifornimentoPagamentoApp.
     * 
     * @param num_carta_loyalty
     */
    public void setNum_carta_loyalty(java.lang.String num_carta_loyalty) {
        this.num_carta_loyalty = num_carta_loyalty;
    }


    /**
     * Gets the parameter1 value for this RifornimentoPagamentoApp.
     * 
     * @return parameter1
     */
    public java.lang.String getParameter1() {
        return parameter1;
    }


    /**
     * Sets the parameter1 value for this RifornimentoPagamentoApp.
     * 
     * @param parameter1
     */
    public void setParameter1(java.lang.String parameter1) {
        this.parameter1 = parameter1;
    }


    /**
     * Gets the parameter2 value for this RifornimentoPagamentoApp.
     * 
     * @return parameter2
     */
    public java.lang.String getParameter2() {
        return parameter2;
    }


    /**
     * Sets the parameter2 value for this RifornimentoPagamentoApp.
     * 
     * @param parameter2
     */
    public void setParameter2(java.lang.String parameter2) {
        this.parameter2 = parameter2;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof RifornimentoPagamentoApp)) return false;
        RifornimentoPagamentoApp other = (RifornimentoPagamentoApp) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codice_fiscale==null && other.getCodice_fiscale()==null) || 
             (this.codice_fiscale!=null &&
              this.codice_fiscale.equals(other.getCodice_fiscale()))) &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              this.data.equals(other.getData()))) &&
            ((this.impianto==null && other.getImpianto()==null) || 
             (this.impianto!=null &&
              this.impianto.equals(other.getImpianto()))) &&
            ((this.prodotto==null && other.getProdotto()==null) || 
             (this.prodotto!=null &&
              this.prodotto.equals(other.getProdotto()))) &&
            this.flag_pagamento == other.isFlag_pagamento() &&
            ((this.modalita_pagamento==null && other.getModalita_pagamento()==null) || 
             (this.modalita_pagamento!=null &&
              this.modalita_pagamento.equals(other.getModalita_pagamento()))) &&
            this.punti == other.getPunti() &&
            ((this.litri==null && other.getLitri()==null) || 
             (this.litri!=null &&
              this.litri.equals(other.getLitri()))) &&
            ((this.modalita_operativa==null && other.getModalita_operativa()==null) || 
             (this.modalita_operativa!=null &&
              this.modalita_operativa.equals(other.getModalita_operativa()))) &&
            ((this.cognome==null && other.getCognome()==null) || 
             (this.cognome!=null &&
              this.cognome.equals(other.getCognome()))) &&
            this.fg_notifica == other.isFg_notifica() &&
            this.fg_1 == other.isFg_1() &&
            ((this.carta==null && other.getCarta()==null) || 
             (this.carta!=null &&
              this.carta.equals(other.getCarta()))) &&
            ((this.cluster==null && other.getCluster()==null) || 
             (this.cluster!=null &&
              this.cluster.equals(other.getCluster()))) &&
            ((this.importo==null && other.getImporto()==null) || 
             (this.importo!=null &&
              this.importo.equals(other.getImporto()))) &&
            this.flg_privacy1 == other.isFlg_privacy1() &&
            this.flg_privacy2 == other.isFlg_privacy2() &&
            ((this.num_carta_loyalty==null && other.getNum_carta_loyalty()==null) || 
             (this.num_carta_loyalty!=null &&
              this.num_carta_loyalty.equals(other.getNum_carta_loyalty()))) &&
            ((this.parameter1==null && other.getParameter1()==null) || 
             (this.parameter1!=null &&
              this.parameter1.equals(other.getParameter1()))) &&
            ((this.parameter2==null && other.getParameter2()==null) || 
             (this.parameter2!=null &&
              this.parameter2.equals(other.getParameter2())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodice_fiscale() != null) {
            _hashCode += getCodice_fiscale().hashCode();
        }
        if (getData() != null) {
            _hashCode += getData().hashCode();
        }
        if (getImpianto() != null) {
            _hashCode += getImpianto().hashCode();
        }
        if (getProdotto() != null) {
            _hashCode += getProdotto().hashCode();
        }
        _hashCode += (isFlag_pagamento() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getModalita_pagamento() != null) {
            _hashCode += getModalita_pagamento().hashCode();
        }
        _hashCode += getPunti();
        if (getLitri() != null) {
            _hashCode += getLitri().hashCode();
        }
        if (getModalita_operativa() != null) {
            _hashCode += getModalita_operativa().hashCode();
        }
        if (getCognome() != null) {
            _hashCode += getCognome().hashCode();
        }
        _hashCode += (isFg_notifica() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isFg_1() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCarta() != null) {
            _hashCode += getCarta().hashCode();
        }
        if (getCluster() != null) {
            _hashCode += getCluster().hashCode();
        }
        if (getImporto() != null) {
            _hashCode += getImporto().hashCode();
        }
        _hashCode += (isFlg_privacy1() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isFlg_privacy2() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getNum_carta_loyalty() != null) {
            _hashCode += getNum_carta_loyalty().hashCode();
        }
        if (getParameter1() != null) {
            _hashCode += getParameter1().hashCode();
        }
        if (getParameter2() != null) {
            _hashCode += getParameter2().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RifornimentoPagamentoApp.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "rifornimentoPagamentoApp"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice_fiscale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "codice_fiscale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("impianto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "impianto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("prodotto");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "prodotto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flag_pagamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "flag_pagamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modalita_pagamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "modalita_pagamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("punti");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "punti"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("litri");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "litri"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("modalita_operativa");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "modalita_operativa"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cognome");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "cognome"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fg_notifica");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "fg_notifica"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fg_1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "fg_1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "carta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cluster");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "cluster"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("importo");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "importo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flg_privacy1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "flg_privacy1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flg_privacy2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "flg_privacy2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("num_carta_loyalty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "num_carta_loyalty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
