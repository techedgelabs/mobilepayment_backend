/**
 * Event.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.clientcrmsf;

public class Event  implements java.io.Serializable {
    private java.lang.String event_type;

    private com.techedge.mp.crm.adapter.clientcrmsf.NuovoCliente nuovoCliente;

    private com.techedge.mp.crm.adapter.clientcrmsf.AccreditoPuntiLoyalty accreditoPuntiLoyalty;

    private com.techedge.mp.crm.adapter.clientcrmsf.RifornimentoPagamentoApp rifornimentoPagamentoApp;

    private com.techedge.mp.crm.adapter.clientcrmsf.AssociazioneCartaPagamento associazioneCartaPagamento;

    private com.techedge.mp.crm.adapter.clientcrmsf.AssociazioneCartaLoyalty associazioneCartaLoyalty;

    public Event() {
    }

    public Event(
           java.lang.String event_type,
           com.techedge.mp.crm.adapter.clientcrmsf.NuovoCliente nuovoCliente,
           com.techedge.mp.crm.adapter.clientcrmsf.AccreditoPuntiLoyalty accreditoPuntiLoyalty,
           com.techedge.mp.crm.adapter.clientcrmsf.RifornimentoPagamentoApp rifornimentoPagamentoApp,
           com.techedge.mp.crm.adapter.clientcrmsf.AssociazioneCartaPagamento associazioneCartaPagamento,
           com.techedge.mp.crm.adapter.clientcrmsf.AssociazioneCartaLoyalty associazioneCartaLoyalty) {
           this.event_type = event_type;
           this.nuovoCliente = nuovoCliente;
           this.accreditoPuntiLoyalty = accreditoPuntiLoyalty;
           this.rifornimentoPagamentoApp = rifornimentoPagamentoApp;
           this.associazioneCartaPagamento = associazioneCartaPagamento;
           this.associazioneCartaLoyalty = associazioneCartaLoyalty;
    }


    /**
     * Gets the event_type value for this Event.
     * 
     * @return event_type
     */
    public java.lang.String getEvent_type() {
        return event_type;
    }


    /**
     * Sets the event_type value for this Event.
     * 
     * @param event_type
     */
    public void setEvent_type(java.lang.String event_type) {
        this.event_type = event_type;
    }


    /**
     * Gets the nuovoCliente value for this Event.
     * 
     * @return nuovoCliente
     */
    public com.techedge.mp.crm.adapter.clientcrmsf.NuovoCliente getNuovoCliente() {
        return nuovoCliente;
    }


    /**
     * Sets the nuovoCliente value for this Event.
     * 
     * @param nuovoCliente
     */
    public void setNuovoCliente(com.techedge.mp.crm.adapter.clientcrmsf.NuovoCliente nuovoCliente) {
        this.nuovoCliente = nuovoCliente;
    }


    /**
     * Gets the accreditoPuntiLoyalty value for this Event.
     * 
     * @return accreditoPuntiLoyalty
     */
    public com.techedge.mp.crm.adapter.clientcrmsf.AccreditoPuntiLoyalty getAccreditoPuntiLoyalty() {
        return accreditoPuntiLoyalty;
    }


    /**
     * Sets the accreditoPuntiLoyalty value for this Event.
     * 
     * @param accreditoPuntiLoyalty
     */
    public void setAccreditoPuntiLoyalty(com.techedge.mp.crm.adapter.clientcrmsf.AccreditoPuntiLoyalty accreditoPuntiLoyalty) {
        this.accreditoPuntiLoyalty = accreditoPuntiLoyalty;
    }


    /**
     * Gets the rifornimentoPagamentoApp value for this Event.
     * 
     * @return rifornimentoPagamentoApp
     */
    public com.techedge.mp.crm.adapter.clientcrmsf.RifornimentoPagamentoApp getRifornimentoPagamentoApp() {
        return rifornimentoPagamentoApp;
    }


    /**
     * Sets the rifornimentoPagamentoApp value for this Event.
     * 
     * @param rifornimentoPagamentoApp
     */
    public void setRifornimentoPagamentoApp(com.techedge.mp.crm.adapter.clientcrmsf.RifornimentoPagamentoApp rifornimentoPagamentoApp) {
        this.rifornimentoPagamentoApp = rifornimentoPagamentoApp;
    }


    /**
     * Gets the associazioneCartaPagamento value for this Event.
     * 
     * @return associazioneCartaPagamento
     */
    public com.techedge.mp.crm.adapter.clientcrmsf.AssociazioneCartaPagamento getAssociazioneCartaPagamento() {
        return associazioneCartaPagamento;
    }


    /**
     * Sets the associazioneCartaPagamento value for this Event.
     * 
     * @param associazioneCartaPagamento
     */
    public void setAssociazioneCartaPagamento(com.techedge.mp.crm.adapter.clientcrmsf.AssociazioneCartaPagamento associazioneCartaPagamento) {
        this.associazioneCartaPagamento = associazioneCartaPagamento;
    }


    /**
     * Gets the associazioneCartaLoyalty value for this Event.
     * 
     * @return associazioneCartaLoyalty
     */
    public com.techedge.mp.crm.adapter.clientcrmsf.AssociazioneCartaLoyalty getAssociazioneCartaLoyalty() {
        return associazioneCartaLoyalty;
    }


    /**
     * Sets the associazioneCartaLoyalty value for this Event.
     * 
     * @param associazioneCartaLoyalty
     */
    public void setAssociazioneCartaLoyalty(com.techedge.mp.crm.adapter.clientcrmsf.AssociazioneCartaLoyalty associazioneCartaLoyalty) {
        this.associazioneCartaLoyalty = associazioneCartaLoyalty;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Event)) return false;
        Event other = (Event) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.event_type==null && other.getEvent_type()==null) || 
             (this.event_type!=null &&
              this.event_type.equals(other.getEvent_type()))) &&
            ((this.nuovoCliente==null && other.getNuovoCliente()==null) || 
             (this.nuovoCliente!=null &&
              this.nuovoCliente.equals(other.getNuovoCliente()))) &&
            ((this.accreditoPuntiLoyalty==null && other.getAccreditoPuntiLoyalty()==null) || 
             (this.accreditoPuntiLoyalty!=null &&
              this.accreditoPuntiLoyalty.equals(other.getAccreditoPuntiLoyalty()))) &&
            ((this.rifornimentoPagamentoApp==null && other.getRifornimentoPagamentoApp()==null) || 
             (this.rifornimentoPagamentoApp!=null &&
              this.rifornimentoPagamentoApp.equals(other.getRifornimentoPagamentoApp()))) &&
            ((this.associazioneCartaPagamento==null && other.getAssociazioneCartaPagamento()==null) || 
             (this.associazioneCartaPagamento!=null &&
              this.associazioneCartaPagamento.equals(other.getAssociazioneCartaPagamento()))) &&
            ((this.associazioneCartaLoyalty==null && other.getAssociazioneCartaLoyalty()==null) || 
             (this.associazioneCartaLoyalty!=null &&
              this.associazioneCartaLoyalty.equals(other.getAssociazioneCartaLoyalty())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getEvent_type() != null) {
            _hashCode += getEvent_type().hashCode();
        }
        if (getNuovoCliente() != null) {
            _hashCode += getNuovoCliente().hashCode();
        }
        if (getAccreditoPuntiLoyalty() != null) {
            _hashCode += getAccreditoPuntiLoyalty().hashCode();
        }
        if (getRifornimentoPagamentoApp() != null) {
            _hashCode += getRifornimentoPagamentoApp().hashCode();
        }
        if (getAssociazioneCartaPagamento() != null) {
            _hashCode += getAssociazioneCartaPagamento().hashCode();
        }
        if (getAssociazioneCartaLoyalty() != null) {
            _hashCode += getAssociazioneCartaLoyalty().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Event.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "event"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("event_type");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "event_type"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nuovoCliente");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "nuovoCliente"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "nuovoCliente"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("accreditoPuntiLoyalty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "accreditoPuntiLoyalty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "accreditoPuntiLoyalty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("rifornimentoPagamentoApp");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "rifornimentoPagamentoApp"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "rifornimentoPagamentoApp"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("associazioneCartaPagamento");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "associazioneCartaPagamento"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "associazioneCartaPagamento"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("associazioneCartaLoyalty");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "associazioneCartaLoyalty"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "associazioneCartaLoyalty"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
