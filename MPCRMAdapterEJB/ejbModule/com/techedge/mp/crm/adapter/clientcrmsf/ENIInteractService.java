/**
 * ENIInteractService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.clientcrmsf;

public interface ENIInteractService extends java.rmi.Remote {
    public com.techedge.mp.crm.adapter.clientcrmsf.EventResponse[] notifyEvent(com.techedge.mp.crm.adapter.clientcrmsf.Event[] event, TechnicalHeader technicalHeader) throws java.rmi.RemoteException, com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault, com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault, com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault;
    public com.techedge.mp.crm.adapter.clientcrmsf.Offer[] getOffers(com.techedge.mp.crm.adapter.clientcrmsf.GetOffer[] offern, TechnicalHeader technicalHeader) throws java.rmi.RemoteException, com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault, com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault, com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault;
    public com.techedge.mp.crm.adapter.clientcrmsf.Promotion[] getPromotions(com.techedge.mp.crm.adapter.clientcrmsf.GetPromotion[] promotion, TechnicalHeader technicalHeader) throws java.rmi.RemoteException, com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault, com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault, com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault;
}
