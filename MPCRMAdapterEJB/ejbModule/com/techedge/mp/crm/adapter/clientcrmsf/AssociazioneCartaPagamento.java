/**
 * AssociazioneCartaPagamento.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.clientcrmsf;

public class AssociazioneCartaPagamento  implements java.io.Serializable {
    private java.lang.String codice_fiscale;

    private java.util.Calendar data;

    private boolean fg_notifica;

    private boolean fg_1;

    private java.lang.String carta;

    private java.lang.String cluster;

    private boolean flg_privacy1;

    private boolean flg_privacy2;

    private java.lang.String parameter1;

    private java.lang.String parameter2;

    public AssociazioneCartaPagamento() {
    }

    public AssociazioneCartaPagamento(
           java.lang.String codice_fiscale,
           java.util.Calendar data,
           boolean fg_notifica,
           boolean fg_1,
           java.lang.String carta,
           java.lang.String cluster,
           boolean flg_privacy1,
           boolean flg_privacy2,
           java.lang.String parameter1,
           java.lang.String parameter2) {
           this.codice_fiscale = codice_fiscale;
           this.data = data;
           this.fg_notifica = fg_notifica;
           this.fg_1 = fg_1;
           this.carta = carta;
           this.cluster = cluster;
           this.flg_privacy1 = flg_privacy1;
           this.flg_privacy2 = flg_privacy2;
           this.parameter1 = parameter1;
           this.parameter2 = parameter2;
    }


    /**
     * Gets the codice_fiscale value for this AssociazioneCartaPagamento.
     * 
     * @return codice_fiscale
     */
    public java.lang.String getCodice_fiscale() {
        return codice_fiscale;
    }


    /**
     * Sets the codice_fiscale value for this AssociazioneCartaPagamento.
     * 
     * @param codice_fiscale
     */
    public void setCodice_fiscale(java.lang.String codice_fiscale) {
        this.codice_fiscale = codice_fiscale;
    }


    /**
     * Gets the data value for this AssociazioneCartaPagamento.
     * 
     * @return data
     */
    public java.util.Calendar getData() {
        return data;
    }


    /**
     * Sets the data value for this AssociazioneCartaPagamento.
     * 
     * @param data
     */
    public void setData(java.util.Calendar data) {
        this.data = data;
    }


    /**
     * Gets the fg_notifica value for this AssociazioneCartaPagamento.
     * 
     * @return fg_notifica
     */
    public boolean isFg_notifica() {
        return fg_notifica;
    }


    /**
     * Sets the fg_notifica value for this AssociazioneCartaPagamento.
     * 
     * @param fg_notifica
     */
    public void setFg_notifica(boolean fg_notifica) {
        this.fg_notifica = fg_notifica;
    }


    /**
     * Gets the fg_1 value for this AssociazioneCartaPagamento.
     * 
     * @return fg_1
     */
    public boolean isFg_1() {
        return fg_1;
    }


    /**
     * Sets the fg_1 value for this AssociazioneCartaPagamento.
     * 
     * @param fg_1
     */
    public void setFg_1(boolean fg_1) {
        this.fg_1 = fg_1;
    }


    /**
     * Gets the carta value for this AssociazioneCartaPagamento.
     * 
     * @return carta
     */
    public java.lang.String getCarta() {
        return carta;
    }


    /**
     * Sets the carta value for this AssociazioneCartaPagamento.
     * 
     * @param carta
     */
    public void setCarta(java.lang.String carta) {
        this.carta = carta;
    }


    /**
     * Gets the cluster value for this AssociazioneCartaPagamento.
     * 
     * @return cluster
     */
    public java.lang.String getCluster() {
        return cluster;
    }


    /**
     * Sets the cluster value for this AssociazioneCartaPagamento.
     * 
     * @param cluster
     */
    public void setCluster(java.lang.String cluster) {
        this.cluster = cluster;
    }


    /**
     * Gets the flg_privacy1 value for this AssociazioneCartaPagamento.
     * 
     * @return flg_privacy1
     */
    public boolean isFlg_privacy1() {
        return flg_privacy1;
    }


    /**
     * Sets the flg_privacy1 value for this AssociazioneCartaPagamento.
     * 
     * @param flg_privacy1
     */
    public void setFlg_privacy1(boolean flg_privacy1) {
        this.flg_privacy1 = flg_privacy1;
    }


    /**
     * Gets the flg_privacy2 value for this AssociazioneCartaPagamento.
     * 
     * @return flg_privacy2
     */
    public boolean isFlg_privacy2() {
        return flg_privacy2;
    }


    /**
     * Sets the flg_privacy2 value for this AssociazioneCartaPagamento.
     * 
     * @param flg_privacy2
     */
    public void setFlg_privacy2(boolean flg_privacy2) {
        this.flg_privacy2 = flg_privacy2;
    }


    /**
     * Gets the parameter1 value for this AssociazioneCartaPagamento.
     * 
     * @return parameter1
     */
    public java.lang.String getParameter1() {
        return parameter1;
    }


    /**
     * Sets the parameter1 value for this AssociazioneCartaPagamento.
     * 
     * @param parameter1
     */
    public void setParameter1(java.lang.String parameter1) {
        this.parameter1 = parameter1;
    }


    /**
     * Gets the parameter2 value for this AssociazioneCartaPagamento.
     * 
     * @return parameter2
     */
    public java.lang.String getParameter2() {
        return parameter2;
    }


    /**
     * Sets the parameter2 value for this AssociazioneCartaPagamento.
     * 
     * @param parameter2
     */
    public void setParameter2(java.lang.String parameter2) {
        this.parameter2 = parameter2;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof AssociazioneCartaPagamento)) return false;
        AssociazioneCartaPagamento other = (AssociazioneCartaPagamento) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codice_fiscale==null && other.getCodice_fiscale()==null) || 
             (this.codice_fiscale!=null &&
              this.codice_fiscale.equals(other.getCodice_fiscale()))) &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              this.data.equals(other.getData()))) &&
            this.fg_notifica == other.isFg_notifica() &&
            this.fg_1 == other.isFg_1() &&
            ((this.carta==null && other.getCarta()==null) || 
             (this.carta!=null &&
              this.carta.equals(other.getCarta()))) &&
            ((this.cluster==null && other.getCluster()==null) || 
             (this.cluster!=null &&
              this.cluster.equals(other.getCluster()))) &&
            this.flg_privacy1 == other.isFlg_privacy1() &&
            this.flg_privacy2 == other.isFlg_privacy2() &&
            ((this.parameter1==null && other.getParameter1()==null) || 
             (this.parameter1!=null &&
              this.parameter1.equals(other.getParameter1()))) &&
            ((this.parameter2==null && other.getParameter2()==null) || 
             (this.parameter2!=null &&
              this.parameter2.equals(other.getParameter2())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodice_fiscale() != null) {
            _hashCode += getCodice_fiscale().hashCode();
        }
        if (getData() != null) {
            _hashCode += getData().hashCode();
        }
        _hashCode += (isFg_notifica() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isFg_1() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getCarta() != null) {
            _hashCode += getCarta().hashCode();
        }
        if (getCluster() != null) {
            _hashCode += getCluster().hashCode();
        }
        _hashCode += (isFlg_privacy1() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isFlg_privacy2() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        if (getParameter1() != null) {
            _hashCode += getParameter1().hashCode();
        }
        if (getParameter2() != null) {
            _hashCode += getParameter2().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(AssociazioneCartaPagamento.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "associazioneCartaPagamento"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice_fiscale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "codice_fiscale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fg_notifica");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "fg_notifica"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fg_1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "fg_1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "carta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cluster");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "cluster"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flg_privacy1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "flg_privacy1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("flg_privacy2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "flg_privacy2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
