/**
 * GetOffer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.clientcrmsf;

public class GetOffer  implements java.io.Serializable {
    private java.lang.String codice_fiscale;

    private java.util.Calendar data;

    private java.lang.Boolean fg_notifica;

    private java.lang.Boolean fg_1;

    private java.lang.String carta;

    private java.lang.String cluster;

    public GetOffer() {
    }

    public GetOffer(
           java.lang.String codice_fiscale,
           java.util.Calendar data,
           java.lang.Boolean fg_notifica,
           java.lang.Boolean fg_1,
           java.lang.String carta,
           java.lang.String cluster) {
           this.codice_fiscale = codice_fiscale;
           this.data = data;
           this.fg_notifica = fg_notifica;
           this.fg_1 = fg_1;
           this.carta = carta;
           this.cluster = cluster;
    }


    /**
     * Gets the codice_fiscale value for this GetOffer.
     * 
     * @return codice_fiscale
     */
    public java.lang.String getCodice_fiscale() {
        return codice_fiscale;
    }


    /**
     * Sets the codice_fiscale value for this GetOffer.
     * 
     * @param codice_fiscale
     */
    public void setCodice_fiscale(java.lang.String codice_fiscale) {
        this.codice_fiscale = codice_fiscale;
    }


    /**
     * Gets the data value for this GetOffer.
     * 
     * @return data
     */
    public java.util.Calendar getData() {
        return data;
    }


    /**
     * Sets the data value for this GetOffer.
     * 
     * @param data
     */
    public void setData(java.util.Calendar data) {
        this.data = data;
    }


    /**
     * Gets the fg_notifica value for this GetOffer.
     * 
     * @return fg_notifica
     */
    public java.lang.Boolean getFg_notifica() {
        return fg_notifica;
    }


    /**
     * Sets the fg_notifica value for this GetOffer.
     * 
     * @param fg_notifica
     */
    public void setFg_notifica(java.lang.Boolean fg_notifica) {
        this.fg_notifica = fg_notifica;
    }


    /**
     * Gets the fg_1 value for this GetOffer.
     * 
     * @return fg_1
     */
    public java.lang.Boolean getFg_1() {
        return fg_1;
    }


    /**
     * Sets the fg_1 value for this GetOffer.
     * 
     * @param fg_1
     */
    public void setFg_1(java.lang.Boolean fg_1) {
        this.fg_1 = fg_1;
    }


    /**
     * Gets the carta value for this GetOffer.
     * 
     * @return carta
     */
    public java.lang.String getCarta() {
        return carta;
    }


    /**
     * Sets the carta value for this GetOffer.
     * 
     * @param carta
     */
    public void setCarta(java.lang.String carta) {
        this.carta = carta;
    }


    /**
     * Gets the cluster value for this GetOffer.
     * 
     * @return cluster
     */
    public java.lang.String getCluster() {
        return cluster;
    }


    /**
     * Sets the cluster value for this GetOffer.
     * 
     * @param cluster
     */
    public void setCluster(java.lang.String cluster) {
        this.cluster = cluster;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetOffer)) return false;
        GetOffer other = (GetOffer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.codice_fiscale==null && other.getCodice_fiscale()==null) || 
             (this.codice_fiscale!=null &&
              this.codice_fiscale.equals(other.getCodice_fiscale()))) &&
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              this.data.equals(other.getData()))) &&
            ((this.fg_notifica==null && other.getFg_notifica()==null) || 
             (this.fg_notifica!=null &&
              this.fg_notifica.equals(other.getFg_notifica()))) &&
            ((this.fg_1==null && other.getFg_1()==null) || 
             (this.fg_1!=null &&
              this.fg_1.equals(other.getFg_1()))) &&
            ((this.carta==null && other.getCarta()==null) || 
             (this.carta!=null &&
              this.carta.equals(other.getCarta()))) &&
            ((this.cluster==null && other.getCluster()==null) || 
             (this.cluster!=null &&
              this.cluster.equals(other.getCluster())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCodice_fiscale() != null) {
            _hashCode += getCodice_fiscale().hashCode();
        }
        if (getData() != null) {
            _hashCode += getData().hashCode();
        }
        if (getFg_notifica() != null) {
            _hashCode += getFg_notifica().hashCode();
        }
        if (getFg_1() != null) {
            _hashCode += getFg_1().hashCode();
        }
        if (getCarta() != null) {
            _hashCode += getCarta().hashCode();
        }
        if (getCluster() != null) {
            _hashCode += getCluster().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetOffer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "getOffer"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("codice_fiscale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "codice_fiscale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fg_notifica");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "fg_notifica"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fg_1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "fg_1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("carta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "carta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("cluster");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "cluster"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
