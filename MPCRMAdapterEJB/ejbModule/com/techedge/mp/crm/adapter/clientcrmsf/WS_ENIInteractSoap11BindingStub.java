/**
 * WS_ENIInteractSoap11BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.clientcrmsf;

import org.apache.axis.AxisProperties;

import com.techedge.mp.adapter.clientcrmsf.model.SSLClientAuthenticationFactory;

public class WS_ENIInteractSoap11BindingStub extends org.apache.axis.client.Stub implements com.techedge.mp.crm.adapter.clientcrmsf.ENIInteractService {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static org.apache.axis.description.OperationDesc [] _operations;

    static {
        _operations = new org.apache.axis.description.OperationDesc[3];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        org.apache.axis.description.OperationDesc oper;
        org.apache.axis.description.ParameterDesc param;
        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("notifyEvent");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/Message/V1/", "event"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "event"), com.techedge.mp.crm.adapter.clientcrmsf.Event[].class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "eventResponse"));
        oper.setReturnClass(com.techedge.mp.crm.adapter.clientcrmsf.EventResponse[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/Message/V1/", "eventResponse"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "validationFault"),
                      "com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault",
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "validationFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "technicalFault"),
                      "com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault",
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "technicalFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "functionalFault"),
                      "com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault",
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "functionalFault"), 
                      true
                     ));
        _operations[0] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getOffers");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/Message/V1/", "offer"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "getOffer"), com.techedge.mp.crm.adapter.clientcrmsf.GetOffer[].class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "offer"));
        oper.setReturnClass(com.techedge.mp.crm.adapter.clientcrmsf.Offer[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/Message/V1/", "offer"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "validationFault"),
                      "com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault",
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "validationFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "technicalFault"),
                      "com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault",
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "technicalFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "functionalFault"),
                      "com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault",
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "functionalFault"), 
                      true
                     ));
        _operations[1] = oper;

        oper = new org.apache.axis.description.OperationDesc();
        oper.setName("getPromotions");
        param = new org.apache.axis.description.ParameterDesc(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/Message/V1/", "promotion"), org.apache.axis.description.ParameterDesc.IN, new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "getPromotion"), com.techedge.mp.crm.adapter.clientcrmsf.GetPromotion[].class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        oper.setReturnType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "promotion"));
        oper.setReturnClass(com.techedge.mp.crm.adapter.clientcrmsf.Promotion[].class);
        oper.setReturnQName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/Message/V1/", "promotion"));
        oper.setStyle(org.apache.axis.constants.Style.WRAPPED);
        oper.setUse(org.apache.axis.constants.Use.LITERAL);
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "validationFault"),
                      "com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault",
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "validationFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "technicalFault"),
                      "com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault",
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "technicalFault"), 
                      true
                     ));
        oper.addFault(new org.apache.axis.description.FaultDesc(
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "functionalFault"),
                      "com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault",
                      new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "functionalFault"), 
                      true
                     ));
        _operations[2] = oper;

    }

    public WS_ENIInteractSoap11BindingStub() throws org.apache.axis.AxisFault {
         this(null);
    }

    public WS_ENIInteractSoap11BindingStub(java.net.URL endpointURL, javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public WS_ENIInteractSoap11BindingStub(javax.xml.rpc.Service service) throws org.apache.axis.AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
            java.lang.Class cls;
            javax.xml.namespace.QName qName;
            javax.xml.namespace.QName qName2;
            java.lang.Class beansf = org.apache.axis.encoding.ser.BeanSerializerFactory.class;
            java.lang.Class beandf = org.apache.axis.encoding.ser.BeanDeserializerFactory.class;
            java.lang.Class enumsf = org.apache.axis.encoding.ser.EnumSerializerFactory.class;
            java.lang.Class enumdf = org.apache.axis.encoding.ser.EnumDeserializerFactory.class;
            java.lang.Class arraysf = org.apache.axis.encoding.ser.ArraySerializerFactory.class;
            java.lang.Class arraydf = org.apache.axis.encoding.ser.ArrayDeserializerFactory.class;
            java.lang.Class simplesf = org.apache.axis.encoding.ser.SimpleSerializerFactory.class;
            java.lang.Class simpledf = org.apache.axis.encoding.ser.SimpleDeserializerFactory.class;
            java.lang.Class simplelistsf = org.apache.axis.encoding.ser.SimpleListSerializerFactory.class;
            java.lang.Class simplelistdf = org.apache.axis.encoding.ser.SimpleListDeserializerFactory.class;
            qName = new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "faultDetail");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.FaultDetail.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "faultDetails");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.FaultDetail[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "faultDetail");
            qName2 = new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "faultDetail");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "faultParameter");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.FaultParameter.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "faultParameters");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.FaultParameter[].class;
            cachedSerClasses.add(cls);
            qName = new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "faultParameter");
            qName2 = new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "faultParameter");
            cachedSerFactories.add(new org.apache.axis.encoding.ser.ArraySerializerFactory(qName, qName2));
            cachedDeserFactories.add(new org.apache.axis.encoding.ser.ArrayDeserializerFactory());

            qName = new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "functionalFault");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "technicalFault");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "validationFault");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "accreditoPuntiLoyalty");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.AccreditoPuntiLoyalty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "associazioneCartaLoyalty");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.AssociazioneCartaLoyalty.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "associazioneCartaPagamento");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.AssociazioneCartaPagamento.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "event");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.Event.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "eventResponse");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.EventResponse.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "getOffer");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.GetOffer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "getPromotion");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.GetPromotion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "nuovoCliente");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.NuovoCliente.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "offer");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.Offer.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "promotion");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.Promotion.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

            qName = new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "rifornimentoPagamentoApp");
            cachedSerQNames.add(qName);
            cls = com.techedge.mp.crm.adapter.clientcrmsf.RifornimentoPagamentoApp.class;
            cachedSerClasses.add(cls);
            cachedSerFactories.add(beansf);
            cachedDeserFactories.add(beandf);

    }

    protected org.apache.axis.client.Call createCall() throws java.rmi.RemoteException {
        try {
            org.apache.axis.client.Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            java.util.Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            // All the type mapping information is registered
            // when the first call is made.
            // The type mapping information is actually registered in
            // the TypeMappingRegistry of the service, which
            // is the reason why registration is only needed for the first call.
            synchronized (this) {
                if (firstCall()) {
                    // must set encoding style before registering serializers
                    _call.setEncodingStyle(null);
                    for (int i = 0; i < cachedSerFactories.size(); ++i) {
                        java.lang.Class cls = (java.lang.Class) cachedSerClasses.get(i);
                        javax.xml.namespace.QName qName =
                                (javax.xml.namespace.QName) cachedSerQNames.get(i);
                        java.lang.Object x = cachedSerFactories.get(i);
                        if (x instanceof Class) {
                            java.lang.Class sf = (java.lang.Class)
                                 cachedSerFactories.get(i);
                            java.lang.Class df = (java.lang.Class)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                        else if (x instanceof javax.xml.rpc.encoding.SerializerFactory) {
                            org.apache.axis.encoding.SerializerFactory sf = (org.apache.axis.encoding.SerializerFactory)
                                 cachedSerFactories.get(i);
                            org.apache.axis.encoding.DeserializerFactory df = (org.apache.axis.encoding.DeserializerFactory)
                                 cachedDeserFactories.get(i);
                            _call.registerTypeMapping(cls, qName, sf, df, false);
                        }
                    }
                }
            }
            
            AxisProperties.setProperty("axis.socketSecureFactory", SSLClientAuthenticationFactory.class.getCanonicalName());
            
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new org.apache.axis.AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public com.techedge.mp.crm.adapter.clientcrmsf.EventResponse[] notifyEvent(com.techedge.mp.crm.adapter.clientcrmsf.Event[] event, TechnicalHeader technicalHeader ) throws java.rmi.RemoteException, com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault, com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault, com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/ws/EniStation/ENIInteract/V1/notifyEvent");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/Message/V1/", "notifyEvent"));
        setHeader("http://www.enistation.com/EniStation/serviceModel/technicalHeader","technicalHeader",technicalHeader);
        setRequestHeaders(_call);
        setAttachments(_call);
        
        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {event});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.techedge.mp.crm.adapter.clientcrmsf.EventResponse[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.techedge.mp.crm.adapter.clientcrmsf.EventResponse[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.techedge.mp.crm.adapter.clientcrmsf.EventResponse[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault) {
              throw (com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault) {
              throw (com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault) {
              throw (com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.techedge.mp.crm.adapter.clientcrmsf.Offer[] getOffers(com.techedge.mp.crm.adapter.clientcrmsf.GetOffer[] offer, TechnicalHeader technicalHeader) throws java.rmi.RemoteException, com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault, com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault, com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/ws/EniStation/ENIInteract/V1/getOffers");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/Message/V1/", "getOffers"));
        setHeader("http://www.enistation.com/EniStation/serviceModel/technicalHeader","technicalHeader",technicalHeader);
        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {offer});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.techedge.mp.crm.adapter.clientcrmsf.Offer[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.techedge.mp.crm.adapter.clientcrmsf.Offer[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.techedge.mp.crm.adapter.clientcrmsf.Offer[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault) {
              throw (com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault) {
              throw (com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault) {
              throw (com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

    public com.techedge.mp.crm.adapter.clientcrmsf.Promotion[] getPromotions(com.techedge.mp.crm.adapter.clientcrmsf.GetPromotion[] promotion, TechnicalHeader technicalHeader) throws java.rmi.RemoteException, com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault, com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault, com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault {
        if (super.cachedEndpoint == null) {
            throw new org.apache.axis.NoEndPointException();
        }
        org.apache.axis.client.Call _call = createCall();
        _call.setOperation(_operations[2]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("/ws/EniStation/ENIInteract/V1/getPromotions");
        _call.setEncodingStyle(null);
        _call.setProperty(org.apache.axis.client.Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(org.apache.axis.AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(org.apache.axis.soap.SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/Message/V1/", "getPromotions"));
        setHeader("http://www.enistation.com/EniStation/serviceModel/technicalHeader","technicalHeader",technicalHeader);
        setRequestHeaders(_call);
        setAttachments(_call);

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        java.lang.Object _resp = _call.invoke(new java.lang.Object[] {promotion});

        if (_resp instanceof java.rmi.RemoteException) {
            throw (java.rmi.RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (com.techedge.mp.crm.adapter.clientcrmsf.Promotion[]) _resp;
            } catch (java.lang.Exception _exception) {
                return (com.techedge.mp.crm.adapter.clientcrmsf.Promotion[]) org.apache.axis.utils.JavaUtils.convert(_resp, com.techedge.mp.crm.adapter.clientcrmsf.Promotion[].class);
            }
        }
  } catch (org.apache.axis.AxisFault axisFaultException) {
    if (axisFaultException.detail != null) {
        if (axisFaultException.detail instanceof java.rmi.RemoteException) {
              throw (java.rmi.RemoteException) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault) {
              throw (com.techedge.mp.crm.adapter.clientcrmsf.ValidationFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault) {
              throw (com.techedge.mp.crm.adapter.clientcrmsf.TechnicalFault) axisFaultException.detail;
         }
        if (axisFaultException.detail instanceof com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault) {
              throw (com.techedge.mp.crm.adapter.clientcrmsf.FunctionalFault) axisFaultException.detail;
         }
   }
  throw axisFaultException;
}
    }

}
