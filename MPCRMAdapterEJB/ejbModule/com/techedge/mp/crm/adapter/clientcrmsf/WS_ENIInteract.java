/**
 * WS_ENIInteract.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.clientcrmsf;

public interface WS_ENIInteract extends javax.xml.rpc.Service {
    public java.lang.String getWS_ENIInteractSoap11Port_V1Address();

    public com.techedge.mp.crm.adapter.clientcrmsf.ENIInteractService getWS_ENIInteractSoap11Port_V1() throws javax.xml.rpc.ServiceException;

    public com.techedge.mp.crm.adapter.clientcrmsf.ENIInteractService getWS_ENIInteractSoap11Port_V1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
