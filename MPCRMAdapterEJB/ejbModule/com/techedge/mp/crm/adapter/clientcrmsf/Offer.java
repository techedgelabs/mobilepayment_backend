/**
 * Offer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.clientcrmsf;

public class Offer  implements java.io.Serializable {
    private java.lang.String offer_code;

    private java.lang.String nome;

    private java.lang.String banner_id;

    private java.util.Calendar data_inizio;

    private java.util.Calendar data_fine;

    private java.lang.String descrizione;

    private java.lang.String tipo_offerta;

    private java.lang.String canale;

    private java.lang.String tipo_missione;

    private java.lang.Integer num_executed_steps;

    private java.lang.Integer num_tot_steps;

    private java.lang.String url;

    private java.lang.String parameter1;

    private java.lang.String parameter2;

    private java.lang.String parameter3;

    private java.math.BigDecimal parameter4;

    public Offer() {
    }

    public Offer(
           java.lang.String offer_code,
           java.lang.String nome,
           java.lang.String banner_id,
           java.util.Calendar data_inizio,
           java.util.Calendar data_fine,
           java.lang.String descrizione,
           java.lang.String tipo_offerta,
           java.lang.String canale,
           java.lang.String tipo_missione,
           java.lang.Integer num_executed_steps,
           java.lang.Integer num_tot_steps,
           java.lang.String url,
           java.lang.String parameter1,
           java.lang.String parameter2,
           java.lang.String parameter3,
           java.math.BigDecimal parameter4) {
           this.offer_code = offer_code;
           this.nome = nome;
           this.banner_id = banner_id;
           this.data_inizio = data_inizio;
           this.data_fine = data_fine;
           this.descrizione = descrizione;
           this.tipo_offerta = tipo_offerta;
           this.canale = canale;
           this.tipo_missione = tipo_missione;
           this.num_executed_steps = num_executed_steps;
           this.num_tot_steps = num_tot_steps;
           this.url = url;
           this.parameter1 = parameter1;
           this.parameter2 = parameter2;
           this.parameter3 = parameter3;
           this.parameter4 = parameter4;
    }


    /**
     * Gets the offer_code value for this Offer.
     * 
     * @return offer_code
     */
    public java.lang.String getOffer_code() {
        return offer_code;
    }


    /**
     * Sets the offer_code value for this Offer.
     * 
     * @param offer_code
     */
    public void setOffer_code(java.lang.String offer_code) {
        this.offer_code = offer_code;
    }


    /**
     * Gets the nome value for this Offer.
     * 
     * @return nome
     */
    public java.lang.String getNome() {
        return nome;
    }


    /**
     * Sets the nome value for this Offer.
     * 
     * @param nome
     */
    public void setNome(java.lang.String nome) {
        this.nome = nome;
    }


    /**
     * Gets the banner_id value for this Offer.
     * 
     * @return banner_id
     */
    public java.lang.String getBanner_id() {
        return banner_id;
    }


    /**
     * Sets the banner_id value for this Offer.
     * 
     * @param banner_id
     */
    public void setBanner_id(java.lang.String banner_id) {
        this.banner_id = banner_id;
    }


    /**
     * Gets the data_inizio value for this Offer.
     * 
     * @return data_inizio
     */
    public java.util.Calendar getData_inizio() {
        return data_inizio;
    }


    /**
     * Sets the data_inizio value for this Offer.
     * 
     * @param data_inizio
     */
    public void setData_inizio(java.util.Calendar data_inizio) {
        this.data_inizio = data_inizio;
    }


    /**
     * Gets the data_fine value for this Offer.
     * 
     * @return data_fine
     */
    public java.util.Calendar getData_fine() {
        return data_fine;
    }


    /**
     * Sets the data_fine value for this Offer.
     * 
     * @param data_fine
     */
    public void setData_fine(java.util.Calendar data_fine) {
        this.data_fine = data_fine;
    }


    /**
     * Gets the descrizione value for this Offer.
     * 
     * @return descrizione
     */
    public java.lang.String getDescrizione() {
        return descrizione;
    }


    /**
     * Sets the descrizione value for this Offer.
     * 
     * @param descrizione
     */
    public void setDescrizione(java.lang.String descrizione) {
        this.descrizione = descrizione;
    }


    /**
     * Gets the tipo_offerta value for this Offer.
     * 
     * @return tipo_offerta
     */
    public java.lang.String getTipo_offerta() {
        return tipo_offerta;
    }


    /**
     * Sets the tipo_offerta value for this Offer.
     * 
     * @param tipo_offerta
     */
    public void setTipo_offerta(java.lang.String tipo_offerta) {
        this.tipo_offerta = tipo_offerta;
    }


    /**
     * Gets the canale value for this Offer.
     * 
     * @return canale
     */
    public java.lang.String getCanale() {
        return canale;
    }


    /**
     * Sets the canale value for this Offer.
     * 
     * @param canale
     */
    public void setCanale(java.lang.String canale) {
        this.canale = canale;
    }


    /**
     * Gets the tipo_missione value for this Offer.
     * 
     * @return tipo_missione
     */
    public java.lang.String getTipo_missione() {
        return tipo_missione;
    }


    /**
     * Sets the tipo_missione value for this Offer.
     * 
     * @param tipo_missione
     */
    public void setTipo_missione(java.lang.String tipo_missione) {
        this.tipo_missione = tipo_missione;
    }


    /**
     * Gets the num_executed_steps value for this Offer.
     * 
     * @return num_executed_steps
     */
    public java.lang.Integer getNum_executed_steps() {
        return num_executed_steps;
    }


    /**
     * Sets the num_executed_steps value for this Offer.
     * 
     * @param num_executed_steps
     */
    public void setNum_executed_steps(java.lang.Integer num_executed_steps) {
        this.num_executed_steps = num_executed_steps;
    }


    /**
     * Gets the num_tot_steps value for this Offer.
     * 
     * @return num_tot_steps
     */
    public java.lang.Integer getNum_tot_steps() {
        return num_tot_steps;
    }


    /**
     * Sets the num_tot_steps value for this Offer.
     * 
     * @param num_tot_steps
     */
    public void setNum_tot_steps(java.lang.Integer num_tot_steps) {
        this.num_tot_steps = num_tot_steps;
    }


    /**
     * Gets the url value for this Offer.
     * 
     * @return url
     */
    public java.lang.String getUrl() {
        return url;
    }


    /**
     * Sets the url value for this Offer.
     * 
     * @param url
     */
    public void setUrl(java.lang.String url) {
        this.url = url;
    }


    /**
     * Gets the parameter1 value for this Offer.
     * 
     * @return parameter1
     */
    public java.lang.String getParameter1() {
        return parameter1;
    }


    /**
     * Sets the parameter1 value for this Offer.
     * 
     * @param parameter1
     */
    public void setParameter1(java.lang.String parameter1) {
        this.parameter1 = parameter1;
    }


    /**
     * Gets the parameter2 value for this Offer.
     * 
     * @return parameter2
     */
    public java.lang.String getParameter2() {
        return parameter2;
    }


    /**
     * Sets the parameter2 value for this Offer.
     * 
     * @param parameter2
     */
    public void setParameter2(java.lang.String parameter2) {
        this.parameter2 = parameter2;
    }


    /**
     * Gets the parameter3 value for this Offer.
     * 
     * @return parameter3
     */
    public java.lang.String getParameter3() {
        return parameter3;
    }


    /**
     * Sets the parameter3 value for this Offer.
     * 
     * @param parameter3
     */
    public void setParameter3(java.lang.String parameter3) {
        this.parameter3 = parameter3;
    }


    /**
     * Gets the parameter4 value for this Offer.
     * 
     * @return parameter4
     */
    public java.math.BigDecimal getParameter4() {
        return parameter4;
    }


    /**
     * Sets the parameter4 value for this Offer.
     * 
     * @param parameter4
     */
    public void setParameter4(java.math.BigDecimal parameter4) {
        this.parameter4 = parameter4;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Offer)) return false;
        Offer other = (Offer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.offer_code==null && other.getOffer_code()==null) || 
             (this.offer_code!=null &&
              this.offer_code.equals(other.getOffer_code()))) &&
            ((this.nome==null && other.getNome()==null) || 
             (this.nome!=null &&
              this.nome.equals(other.getNome()))) &&
            ((this.banner_id==null && other.getBanner_id()==null) || 
             (this.banner_id!=null &&
              this.banner_id.equals(other.getBanner_id()))) &&
            ((this.data_inizio==null && other.getData_inizio()==null) || 
             (this.data_inizio!=null &&
              this.data_inizio.equals(other.getData_inizio()))) &&
            ((this.data_fine==null && other.getData_fine()==null) || 
             (this.data_fine!=null &&
              this.data_fine.equals(other.getData_fine()))) &&
            ((this.descrizione==null && other.getDescrizione()==null) || 
             (this.descrizione!=null &&
              this.descrizione.equals(other.getDescrizione()))) &&
            ((this.tipo_offerta==null && other.getTipo_offerta()==null) || 
             (this.tipo_offerta!=null &&
              this.tipo_offerta.equals(other.getTipo_offerta()))) &&
            ((this.canale==null && other.getCanale()==null) || 
             (this.canale!=null &&
              this.canale.equals(other.getCanale()))) &&
            ((this.tipo_missione==null && other.getTipo_missione()==null) || 
             (this.tipo_missione!=null &&
              this.tipo_missione.equals(other.getTipo_missione()))) &&
            ((this.num_executed_steps==null && other.getNum_executed_steps()==null) || 
             (this.num_executed_steps!=null &&
              this.num_executed_steps.equals(other.getNum_executed_steps()))) &&
            ((this.num_tot_steps==null && other.getNum_tot_steps()==null) || 
             (this.num_tot_steps!=null &&
              this.num_tot_steps.equals(other.getNum_tot_steps()))) &&
            ((this.url==null && other.getUrl()==null) || 
             (this.url!=null &&
              this.url.equals(other.getUrl()))) &&
            ((this.parameter1==null && other.getParameter1()==null) || 
             (this.parameter1!=null &&
              this.parameter1.equals(other.getParameter1()))) &&
            ((this.parameter2==null && other.getParameter2()==null) || 
             (this.parameter2!=null &&
              this.parameter2.equals(other.getParameter2()))) &&
            ((this.parameter3==null && other.getParameter3()==null) || 
             (this.parameter3!=null &&
              this.parameter3.equals(other.getParameter3()))) &&
            ((this.parameter4==null && other.getParameter4()==null) || 
             (this.parameter4!=null &&
              this.parameter4.equals(other.getParameter4())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getOffer_code() != null) {
            _hashCode += getOffer_code().hashCode();
        }
        if (getNome() != null) {
            _hashCode += getNome().hashCode();
        }
        if (getBanner_id() != null) {
            _hashCode += getBanner_id().hashCode();
        }
        if (getData_inizio() != null) {
            _hashCode += getData_inizio().hashCode();
        }
        if (getData_fine() != null) {
            _hashCode += getData_fine().hashCode();
        }
        if (getDescrizione() != null) {
            _hashCode += getDescrizione().hashCode();
        }
        if (getTipo_offerta() != null) {
            _hashCode += getTipo_offerta().hashCode();
        }
        if (getCanale() != null) {
            _hashCode += getCanale().hashCode();
        }
        if (getTipo_missione() != null) {
            _hashCode += getTipo_missione().hashCode();
        }
        if (getNum_executed_steps() != null) {
            _hashCode += getNum_executed_steps().hashCode();
        }
        if (getNum_tot_steps() != null) {
            _hashCode += getNum_tot_steps().hashCode();
        }
        if (getUrl() != null) {
            _hashCode += getUrl().hashCode();
        }
        if (getParameter1() != null) {
            _hashCode += getParameter1().hashCode();
        }
        if (getParameter2() != null) {
            _hashCode += getParameter2().hashCode();
        }
        if (getParameter3() != null) {
            _hashCode += getParameter3().hashCode();
        }
        if (getParameter4() != null) {
            _hashCode += getParameter4().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Offer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "offer"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offer_code");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "offer_code"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nome");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "nome"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("banner_id");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "banner_id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data_inizio");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "data_inizio"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data_fine");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "data_fine"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("descrizione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "descrizione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo_offerta");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "tipo_offerta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("canale");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "canale"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo_missione");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "tipo_missione"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("num_executed_steps");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "num_executed_steps"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("num_tot_steps");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "num_tot_steps"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("url");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "url"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "decimal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
