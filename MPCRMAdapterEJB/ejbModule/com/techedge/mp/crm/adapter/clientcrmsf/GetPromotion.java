/**
 * GetPromotion.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.clientcrmsf;

public class GetPromotion  implements java.io.Serializable {
    private java.util.Calendar data;

    private java.lang.String parameter1;

    private java.lang.String parameter2;

    private java.lang.String parameter3;

    private java.lang.String parameter4;

    public GetPromotion() {
    }

    public GetPromotion(
           java.util.Calendar data,
           java.lang.String parameter1,
           java.lang.String parameter2,
           java.lang.String parameter3,
           java.lang.String parameter4) {
           this.data = data;
           this.parameter1 = parameter1;
           this.parameter2 = parameter2;
           this.parameter3 = parameter3;
           this.parameter4 = parameter4;
    }


    /**
     * Gets the data value for this GetPromotion.
     * 
     * @return data
     */
    public java.util.Calendar getData() {
        return data;
    }


    /**
     * Sets the data value for this GetPromotion.
     * 
     * @param data
     */
    public void setData(java.util.Calendar data) {
        this.data = data;
    }


    /**
     * Gets the parameter1 value for this GetPromotion.
     * 
     * @return parameter1
     */
    public java.lang.String getParameter1() {
        return parameter1;
    }


    /**
     * Sets the parameter1 value for this GetPromotion.
     * 
     * @param parameter1
     */
    public void setParameter1(java.lang.String parameter1) {
        this.parameter1 = parameter1;
    }


    /**
     * Gets the parameter2 value for this GetPromotion.
     * 
     * @return parameter2
     */
    public java.lang.String getParameter2() {
        return parameter2;
    }


    /**
     * Sets the parameter2 value for this GetPromotion.
     * 
     * @param parameter2
     */
    public void setParameter2(java.lang.String parameter2) {
        this.parameter2 = parameter2;
    }


    /**
     * Gets the parameter3 value for this GetPromotion.
     * 
     * @return parameter3
     */
    public java.lang.String getParameter3() {
        return parameter3;
    }


    /**
     * Sets the parameter3 value for this GetPromotion.
     * 
     * @param parameter3
     */
    public void setParameter3(java.lang.String parameter3) {
        this.parameter3 = parameter3;
    }


    /**
     * Gets the parameter4 value for this GetPromotion.
     * 
     * @return parameter4
     */
    public java.lang.String getParameter4() {
        return parameter4;
    }


    /**
     * Sets the parameter4 value for this GetPromotion.
     * 
     * @param parameter4
     */
    public void setParameter4(java.lang.String parameter4) {
        this.parameter4 = parameter4;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetPromotion)) return false;
        GetPromotion other = (GetPromotion) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.data==null && other.getData()==null) || 
             (this.data!=null &&
              this.data.equals(other.getData()))) &&
            ((this.parameter1==null && other.getParameter1()==null) || 
             (this.parameter1!=null &&
              this.parameter1.equals(other.getParameter1()))) &&
            ((this.parameter2==null && other.getParameter2()==null) || 
             (this.parameter2!=null &&
              this.parameter2.equals(other.getParameter2()))) &&
            ((this.parameter3==null && other.getParameter3()==null) || 
             (this.parameter3!=null &&
              this.parameter3.equals(other.getParameter3()))) &&
            ((this.parameter4==null && other.getParameter4()==null) || 
             (this.parameter4!=null &&
              this.parameter4.equals(other.getParameter4())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getData() != null) {
            _hashCode += getData().hashCode();
        }
        if (getParameter1() != null) {
            _hashCode += getParameter1().hashCode();
        }
        if (getParameter2() != null) {
            _hashCode += getParameter2().hashCode();
        }
        if (getParameter3() != null) {
            _hashCode += getParameter3().hashCode();
        }
        if (getParameter4() != null) {
            _hashCode += getParameter4().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetPromotion.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "getPromotion"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("data");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "data"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter1");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter1"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter2");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter2"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter3");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter3"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("parameter4");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/xmlns/EniStation/ENIInteract/V1/", "parameter4"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
