/**
 * ValidationFault.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.clientcrmsf;

public class ValidationFault  extends org.apache.axis.AxisFault  implements java.io.Serializable {
    private com.techedge.mp.crm.adapter.clientcrmsf.FaultDetail[] validationFailureDetails;

    public ValidationFault() {
    }

    public ValidationFault(
           com.techedge.mp.crm.adapter.clientcrmsf.FaultDetail[] validationFailureDetails) {
        this.validationFailureDetails = validationFailureDetails;
    }


    /**
     * Gets the validationFailureDetails value for this ValidationFault.
     * 
     * @return validationFailureDetails
     */
    public com.techedge.mp.crm.adapter.clientcrmsf.FaultDetail[] getValidationFailureDetails() {
        return validationFailureDetails;
    }


    /**
     * Sets the validationFailureDetails value for this ValidationFault.
     * 
     * @param validationFailureDetails
     */
    public void setValidationFailureDetails(com.techedge.mp.crm.adapter.clientcrmsf.FaultDetail[] validationFailureDetails) {
        this.validationFailureDetails = validationFailureDetails;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ValidationFault)) return false;
        ValidationFault other = (ValidationFault) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.validationFailureDetails==null && other.getValidationFailureDetails()==null) || 
             (this.validationFailureDetails!=null &&
              java.util.Arrays.equals(this.validationFailureDetails, other.getValidationFailureDetails())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getValidationFailureDetails() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getValidationFailureDetails());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getValidationFailureDetails(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ValidationFault.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "validationFault"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("validationFailureDetails");
        elemField.setXmlName(new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "validationFailureDetails"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "faultDetail"));
        elemField.setNillable(false);
        elemField.setItemQName(new javax.xml.namespace.QName("http://www.enistation.com/EniStation/serviceModel/fault", "faultDetail"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }


    /**
     * Writes the exception data to the faultDetails
     */
    public void writeDetails(javax.xml.namespace.QName qname, org.apache.axis.encoding.SerializationContext context) throws java.io.IOException {
        context.serialize(qname, null, this);
    }
}
