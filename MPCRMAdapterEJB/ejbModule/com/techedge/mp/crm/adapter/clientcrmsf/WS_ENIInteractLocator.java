/**
 * WS_ENIInteractLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.clientcrmsf;

import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.rpc.handler.HandlerInfo;
import javax.xml.rpc.handler.HandlerRegistry;

import com.techedge.mp.crm.adapter.clientoauth.OAuth2AuthorizationHandler;


public class WS_ENIInteractLocator extends org.apache.axis.client.Service implements com.techedge.mp.crm.adapter.clientcrmsf.WS_ENIInteract {

	private String _endpoint = null;
	
    public WS_ENIInteractLocator(String endpoint) {
    	_endpoint = endpoint;
    }


    public WS_ENIInteractLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WS_ENIInteractLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }
    
    @Override
    public HandlerRegistry getHandlerRegistry() {
        HandlerRegistry registry = super.getHandlerRegistry();
        QName portName = new javax.xml.namespace.QName("http://www.enistation.com/ws/EniStation/ENIInteract/V1/", "WS_ENIInteractSoap11Port_V1");
        List handlerChain = registry.getHandlerChain(portName);

        HandlerInfo hi = new HandlerInfo();
        hi.setHandlerClass(OAuth2AuthorizationHandler.class);
        handlerChain.add(hi);

        return registry;
    }

    // Use to get a proxy class for WS_ENIInteractSoap11Port_V1
    private java.lang.String WS_ENIInteractSoap11Port_V1_address = _endpoint;

    public java.lang.String getWS_ENIInteractSoap11Port_V1Address() {
        return WS_ENIInteractSoap11Port_V1_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WS_ENIInteractSoap11Port_V1WSDDServiceName = "WS_ENIInteractSoap11Port_V1";

    public java.lang.String getWS_ENIInteractSoap11Port_V1WSDDServiceName() {
        return WS_ENIInteractSoap11Port_V1WSDDServiceName;
    }

    public void setWS_ENIInteractSoap11Port_V1WSDDServiceName(java.lang.String name) {
        WS_ENIInteractSoap11Port_V1WSDDServiceName = name;
    }

    public com.techedge.mp.crm.adapter.clientcrmsf.ENIInteractService getWS_ENIInteractSoap11Port_V1() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(_endpoint);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWS_ENIInteractSoap11Port_V1(endpoint);
    }

    public com.techedge.mp.crm.adapter.clientcrmsf.ENIInteractService getWS_ENIInteractSoap11Port_V1(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.techedge.mp.crm.adapter.clientcrmsf.WS_ENIInteractSoap11BindingStub _stub = new com.techedge.mp.crm.adapter.clientcrmsf.WS_ENIInteractSoap11BindingStub(portAddress, this);
            _stub.setPortName(getWS_ENIInteractSoap11Port_V1WSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWS_ENIInteractSoap11Port_V1EndpointAddress(java.lang.String address) {
        WS_ENIInteractSoap11Port_V1_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.techedge.mp.crm.adapter.clientcrmsf.ENIInteractService.class.isAssignableFrom(serviceEndpointInterface)) {
                com.techedge.mp.crm.adapter.clientcrmsf.WS_ENIInteractSoap11BindingStub _stub = new com.techedge.mp.crm.adapter.clientcrmsf.WS_ENIInteractSoap11BindingStub(new java.net.URL(WS_ENIInteractSoap11Port_V1_address), this);
                _stub.setPortName(getWS_ENIInteractSoap11Port_V1WSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WS_ENIInteractSoap11Port_V1".equals(inputPortName)) {
            return getWS_ENIInteractSoap11Port_V1();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://www.enistation.com/ws/EniStation/ENIInteract/V1/", "WS_ENIInteract");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://www.enistation.com/ws/EniStation/ENIInteract/V1/", "WS_ENIInteractSoap11Port_V1"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WS_ENIInteractSoap11Port_V1".equals(portName)) {
            setWS_ENIInteractSoap11Port_V1EndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
