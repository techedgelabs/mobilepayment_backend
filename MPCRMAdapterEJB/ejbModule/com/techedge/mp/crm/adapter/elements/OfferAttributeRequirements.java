/**
 * OfferAttributeRequirements.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.elements;

public class OfferAttributeRequirements  implements java.io.Serializable {
    private NameValuePairImpl[] attributes;

    private OfferAttributeRequirements[] childRequirements;

    private java.lang.Integer numberRequested;

    public OfferAttributeRequirements() {
    }

    public OfferAttributeRequirements(
           NameValuePairImpl[] attributes,
           OfferAttributeRequirements[] childRequirements,
           java.lang.Integer numberRequested) {
           this.attributes = attributes;
           this.childRequirements = childRequirements;
           this.numberRequested = numberRequested;
    }


    /**
     * Gets the attributes value for this OfferAttributeRequirements.
     * 
     * @return attributes
     */
    public NameValuePairImpl[] getAttributes() {
        return attributes;
    }


    /**
     * Sets the attributes value for this OfferAttributeRequirements.
     * 
     * @param attributes
     */
    public void setAttributes(NameValuePairImpl[] attributes) {
        this.attributes = attributes;
    }

    public NameValuePairImpl getAttributes(int i) {
        return this.attributes[i];
    }

    public void setAttributes(int i, NameValuePairImpl _value) {
        this.attributes[i] = _value;
    }


    /**
     * Gets the childRequirements value for this OfferAttributeRequirements.
     * 
     * @return childRequirements
     */
    public OfferAttributeRequirements[] getChildRequirements() {
        return childRequirements;
    }


    /**
     * Sets the childRequirements value for this OfferAttributeRequirements.
     * 
     * @param childRequirements
     */
    public void setChildRequirements(OfferAttributeRequirements[] childRequirements) {
        this.childRequirements = childRequirements;
    }

    public OfferAttributeRequirements getChildRequirements(int i) {
        return this.childRequirements[i];
    }

    public void setChildRequirements(int i, OfferAttributeRequirements _value) {
        this.childRequirements[i] = _value;
    }


    /**
     * Gets the numberRequested value for this OfferAttributeRequirements.
     * 
     * @return numberRequested
     */
    public java.lang.Integer getNumberRequested() {
        return numberRequested;
    }


    /**
     * Sets the numberRequested value for this OfferAttributeRequirements.
     * 
     * @param numberRequested
     */
    public void setNumberRequested(java.lang.Integer numberRequested) {
        this.numberRequested = numberRequested;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OfferAttributeRequirements)) return false;
        OfferAttributeRequirements other = (OfferAttributeRequirements) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.attributes==null && other.getAttributes()==null) || 
             (this.attributes!=null &&
              java.util.Arrays.equals(this.attributes, other.getAttributes()))) &&
            ((this.childRequirements==null && other.getChildRequirements()==null) || 
             (this.childRequirements!=null &&
              java.util.Arrays.equals(this.childRequirements, other.getChildRequirements()))) &&
            ((this.numberRequested==null && other.getNumberRequested()==null) || 
             (this.numberRequested!=null &&
              this.numberRequested.equals(other.getNumberRequested())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getChildRequirements() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getChildRequirements());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getChildRequirements(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getNumberRequested() != null) {
            _hashCode += getNumberRequested().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OfferAttributeRequirements.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "OfferAttributeRequirements"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attributes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "attributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "NameValuePairImpl"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("childRequirements");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "childRequirements"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "OfferAttributeRequirements"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberRequested");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "numberRequested"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
