/**
 * OfferList.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.elements;

public class OfferList  implements java.io.Serializable {
    private java.lang.String defaultString;

    private java.lang.String interactionPointName;

    private Offer[] recommendedOffers;

    public OfferList() {
    }

    public OfferList(
           java.lang.String defaultString,
           java.lang.String interactionPointName,
           Offer[] recommendedOffers) {
           this.defaultString = defaultString;
           this.interactionPointName = interactionPointName;
           this.recommendedOffers = recommendedOffers;
    }


    /**
     * Gets the defaultString value for this OfferList.
     * 
     * @return defaultString
     */
    public java.lang.String getDefaultString() {
        return defaultString;
    }


    /**
     * Sets the defaultString value for this OfferList.
     * 
     * @param defaultString
     */
    public void setDefaultString(java.lang.String defaultString) {
        this.defaultString = defaultString;
    }


    /**
     * Gets the interactionPointName value for this OfferList.
     * 
     * @return interactionPointName
     */
    public java.lang.String getInteractionPointName() {
        return interactionPointName;
    }


    /**
     * Sets the interactionPointName value for this OfferList.
     * 
     * @param interactionPointName
     */
    public void setInteractionPointName(java.lang.String interactionPointName) {
        this.interactionPointName = interactionPointName;
    }


    /**
     * Gets the recommendedOffers value for this OfferList.
     * 
     * @return recommendedOffers
     */
    public Offer[] getRecommendedOffers() {
        return recommendedOffers;
    }


    /**
     * Sets the recommendedOffers value for this OfferList.
     * 
     * @param recommendedOffers
     */
    public void setRecommendedOffers(Offer[] recommendedOffers) {
        this.recommendedOffers = recommendedOffers;
    }

    public Offer getRecommendedOffers(int i) {
        return this.recommendedOffers[i];
    }

    public void setRecommendedOffers(int i, Offer _value) {
        this.recommendedOffers[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof OfferList)) return false;
        OfferList other = (OfferList) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.defaultString==null && other.getDefaultString()==null) || 
             (this.defaultString!=null &&
              this.defaultString.equals(other.getDefaultString()))) &&
            ((this.interactionPointName==null && other.getInteractionPointName()==null) || 
             (this.interactionPointName!=null &&
              this.interactionPointName.equals(other.getInteractionPointName()))) &&
            ((this.recommendedOffers==null && other.getRecommendedOffers()==null) || 
             (this.recommendedOffers!=null &&
              java.util.Arrays.equals(this.recommendedOffers, other.getRecommendedOffers())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDefaultString() != null) {
            _hashCode += getDefaultString().hashCode();
        }
        if (getInteractionPointName() != null) {
            _hashCode += getInteractionPointName().hashCode();
        }
        if (getRecommendedOffers() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRecommendedOffers());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRecommendedOffers(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(OfferList.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "OfferList"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("defaultString");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "defaultString"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interactionPointName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "interactionPointName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("recommendedOffers");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "recommendedOffers"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "Offer"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
