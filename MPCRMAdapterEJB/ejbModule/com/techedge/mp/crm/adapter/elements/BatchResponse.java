/**
 * BatchResponse.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.elements;

public class BatchResponse  implements java.io.Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -1909531392716501558L;

    private java.lang.Integer batchStatusCode;

    private Response[] responses;

    public BatchResponse() {
    }

    public BatchResponse(
           java.lang.Integer batchStatusCode,
           Response[] responses) {
           this.batchStatusCode = batchStatusCode;
           this.responses = responses;
    }


    /**
     * Gets the batchStatusCode value for this BatchResponse.
     * 
     * @return batchStatusCode
     */
    public java.lang.Integer getBatchStatusCode() {
        return batchStatusCode;
    }


    /**
     * Sets the batchStatusCode value for this BatchResponse.
     * 
     * @param batchStatusCode
     */
    public void setBatchStatusCode(java.lang.Integer batchStatusCode) {
        this.batchStatusCode = batchStatusCode;
    }


    /**
     * Gets the responses value for this BatchResponse.
     * 
     * @return responses
     */
    public Response[] getResponses() {
        return responses;
    }


    /**
     * Sets the responses value for this BatchResponse.
     * 
     * @param responses
     */
    public void setResponses(Response[] responses) {
        this.responses = responses;
    }

    public Response getResponses(int i) {
        return this.responses[i];
    }

    public void setResponses(int i, Response _value) {
        this.responses[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BatchResponse)) return false;
        BatchResponse other = (BatchResponse) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.batchStatusCode==null && other.getBatchStatusCode()==null) || 
             (this.batchStatusCode!=null &&
              this.batchStatusCode.equals(other.getBatchStatusCode()))) &&
            ((this.responses==null && other.getResponses()==null) || 
             (this.responses!=null &&
              java.util.Arrays.equals(this.responses, other.getResponses())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getBatchStatusCode() != null) {
            _hashCode += getBatchStatusCode().hashCode();
        }
        if (getResponses() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getResponses());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getResponses(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BatchResponse.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "BatchResponse"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("batchStatusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "batchStatusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("responses");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "responses"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "Response"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
