/**
 * Response.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.elements;

public class Response  implements java.io.Serializable {
    private AdvisoryMessage[] advisoryMessages;

    private OfferList[] allOfferLists;

    private java.lang.String apiVersion;

    private OfferList offerList;

    private NameValuePairImpl[] profileRecord;

    private java.lang.String sessionID;

    private java.lang.Integer statusCode;

    public Response() {
    }

    public Response(
           AdvisoryMessage[] advisoryMessages,
           OfferList[] allOfferLists,
           java.lang.String apiVersion,
           OfferList offerList,
           NameValuePairImpl[] profileRecord,
           java.lang.String sessionID,
           java.lang.Integer statusCode) {
           this.advisoryMessages = advisoryMessages;
           this.allOfferLists = allOfferLists;
           this.apiVersion = apiVersion;
           this.offerList = offerList;
           this.profileRecord = profileRecord;
           this.sessionID = sessionID;
           this.statusCode = statusCode;
    }


    /**
     * Gets the advisoryMessages value for this Response.
     * 
     * @return advisoryMessages
     */
    public AdvisoryMessage[] getAdvisoryMessages() {
        return advisoryMessages;
    }


    /**
     * Sets the advisoryMessages value for this Response.
     * 
     * @param advisoryMessages
     */
    public void setAdvisoryMessages(AdvisoryMessage[] advisoryMessages) {
        this.advisoryMessages = advisoryMessages;
    }

    public AdvisoryMessage getAdvisoryMessages(int i) {
        return this.advisoryMessages[i];
    }

    public void setAdvisoryMessages(int i, AdvisoryMessage _value) {
        this.advisoryMessages[i] = _value;
    }


    /**
     * Gets the allOfferLists value for this Response.
     * 
     * @return allOfferLists
     */
    public OfferList[] getAllOfferLists() {
        return allOfferLists;
    }


    /**
     * Sets the allOfferLists value for this Response.
     * 
     * @param allOfferLists
     */
    public void setAllOfferLists(OfferList[] allOfferLists) {
        this.allOfferLists = allOfferLists;
    }

    public OfferList getAllOfferLists(int i) {
        return this.allOfferLists[i];
    }

    public void setAllOfferLists(int i, OfferList _value) {
        this.allOfferLists[i] = _value;
    }


    /**
     * Gets the apiVersion value for this Response.
     * 
     * @return apiVersion
     */
    public java.lang.String getApiVersion() {
        return apiVersion;
    }


    /**
     * Sets the apiVersion value for this Response.
     * 
     * @param apiVersion
     */
    public void setApiVersion(java.lang.String apiVersion) {
        this.apiVersion = apiVersion;
    }


    /**
     * Gets the offerList value for this Response.
     * 
     * @return offerList
     */
    public OfferList getOfferList() {
        return offerList;
    }


    /**
     * Sets the offerList value for this Response.
     * 
     * @param offerList
     */
    public void setOfferList(OfferList offerList) {
        this.offerList = offerList;
    }


    /**
     * Gets the profileRecord value for this Response.
     * 
     * @return profileRecord
     */
    public NameValuePairImpl[] getProfileRecord() {
        return profileRecord;
    }


    /**
     * Sets the profileRecord value for this Response.
     * 
     * @param profileRecord
     */
    public void setProfileRecord(NameValuePairImpl[] profileRecord) {
        this.profileRecord = profileRecord;
    }

    public NameValuePairImpl getProfileRecord(int i) {
        return this.profileRecord[i];
    }

    public void setProfileRecord(int i, NameValuePairImpl _value) {
        this.profileRecord[i] = _value;
    }


    /**
     * Gets the sessionID value for this Response.
     * 
     * @return sessionID
     */
    public java.lang.String getSessionID() {
        return sessionID;
    }


    /**
     * Sets the sessionID value for this Response.
     * 
     * @param sessionID
     */
    public void setSessionID(java.lang.String sessionID) {
        this.sessionID = sessionID;
    }


    /**
     * Gets the statusCode value for this Response.
     * 
     * @return statusCode
     */
    public java.lang.Integer getStatusCode() {
        return statusCode;
    }


    /**
     * Sets the statusCode value for this Response.
     * 
     * @param statusCode
     */
    public void setStatusCode(java.lang.Integer statusCode) {
        this.statusCode = statusCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Response)) return false;
        Response other = (Response) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.advisoryMessages==null && other.getAdvisoryMessages()==null) || 
             (this.advisoryMessages!=null &&
              java.util.Arrays.equals(this.advisoryMessages, other.getAdvisoryMessages()))) &&
            ((this.allOfferLists==null && other.getAllOfferLists()==null) || 
             (this.allOfferLists!=null &&
              java.util.Arrays.equals(this.allOfferLists, other.getAllOfferLists()))) &&
            ((this.apiVersion==null && other.getApiVersion()==null) || 
             (this.apiVersion!=null &&
              this.apiVersion.equals(other.getApiVersion()))) &&
            ((this.offerList==null && other.getOfferList()==null) || 
             (this.offerList!=null &&
              this.offerList.equals(other.getOfferList()))) &&
            ((this.profileRecord==null && other.getProfileRecord()==null) || 
             (this.profileRecord!=null &&
              java.util.Arrays.equals(this.profileRecord, other.getProfileRecord()))) &&
            ((this.sessionID==null && other.getSessionID()==null) || 
             (this.sessionID!=null &&
              this.sessionID.equals(other.getSessionID()))) &&
            ((this.statusCode==null && other.getStatusCode()==null) || 
             (this.statusCode!=null &&
              this.statusCode.equals(other.getStatusCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAdvisoryMessages() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAdvisoryMessages());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAdvisoryMessages(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAllOfferLists() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAllOfferLists());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAllOfferLists(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getApiVersion() != null) {
            _hashCode += getApiVersion().hashCode();
        }
        if (getOfferList() != null) {
            _hashCode += getOfferList().hashCode();
        }
        if (getProfileRecord() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getProfileRecord());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getProfileRecord(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getSessionID() != null) {
            _hashCode += getSessionID().hashCode();
        }
        if (getStatusCode() != null) {
            _hashCode += getStatusCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Response.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "Response"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("advisoryMessages");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "advisoryMessages"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "AdvisoryMessage"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("allOfferLists");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "allOfferLists"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "OfferList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("apiVersion");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "apiVersion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offerList");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "offerList"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "OfferList"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("profileRecord");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "profileRecord"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "NameValuePairImpl"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "sessionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("statusCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "statusCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
