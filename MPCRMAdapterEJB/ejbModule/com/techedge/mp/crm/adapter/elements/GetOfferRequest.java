/**
 * GetOfferRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.elements;

public class GetOfferRequest  implements java.io.Serializable {
    private java.lang.Integer duplicationPolicy;

    private java.lang.String ipName;

    private java.lang.Integer numberRequested;

    private OfferAttributeRequirements offerAttributes;

    public GetOfferRequest() {
    }

    public GetOfferRequest(
           java.lang.Integer duplicationPolicy,
           java.lang.String ipName,
           java.lang.Integer numberRequested,
           OfferAttributeRequirements offerAttributes) {
           this.duplicationPolicy = duplicationPolicy;
           this.ipName = ipName;
           this.numberRequested = numberRequested;
           this.offerAttributes = offerAttributes;
    }


    /**
     * Gets the duplicationPolicy value for this GetOfferRequest.
     * 
     * @return duplicationPolicy
     */
    public java.lang.Integer getDuplicationPolicy() {
        return duplicationPolicy;
    }


    /**
     * Sets the duplicationPolicy value for this GetOfferRequest.
     * 
     * @param duplicationPolicy
     */
    public void setDuplicationPolicy(java.lang.Integer duplicationPolicy) {
        this.duplicationPolicy = duplicationPolicy;
    }


    /**
     * Gets the ipName value for this GetOfferRequest.
     * 
     * @return ipName
     */
    public java.lang.String getIpName() {
        return ipName;
    }


    /**
     * Sets the ipName value for this GetOfferRequest.
     * 
     * @param ipName
     */
    public void setIpName(java.lang.String ipName) {
        this.ipName = ipName;
    }


    /**
     * Gets the numberRequested value for this GetOfferRequest.
     * 
     * @return numberRequested
     */
    public java.lang.Integer getNumberRequested() {
        return numberRequested;
    }


    /**
     * Sets the numberRequested value for this GetOfferRequest.
     * 
     * @param numberRequested
     */
    public void setNumberRequested(java.lang.Integer numberRequested) {
        this.numberRequested = numberRequested;
    }


    /**
     * Gets the offerAttributes value for this GetOfferRequest.
     * 
     * @return offerAttributes
     */
    public OfferAttributeRequirements getOfferAttributes() {
        return offerAttributes;
    }


    /**
     * Sets the offerAttributes value for this GetOfferRequest.
     * 
     * @param offerAttributes
     */
    public void setOfferAttributes(OfferAttributeRequirements offerAttributes) {
        this.offerAttributes = offerAttributes;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof GetOfferRequest)) return false;
        GetOfferRequest other = (GetOfferRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.duplicationPolicy==null && other.getDuplicationPolicy()==null) || 
             (this.duplicationPolicy!=null &&
              this.duplicationPolicy.equals(other.getDuplicationPolicy()))) &&
            ((this.ipName==null && other.getIpName()==null) || 
             (this.ipName!=null &&
              this.ipName.equals(other.getIpName()))) &&
            ((this.numberRequested==null && other.getNumberRequested()==null) || 
             (this.numberRequested!=null &&
              this.numberRequested.equals(other.getNumberRequested()))) &&
            ((this.offerAttributes==null && other.getOfferAttributes()==null) || 
             (this.offerAttributes!=null &&
              this.offerAttributes.equals(other.getOfferAttributes())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getDuplicationPolicy() != null) {
            _hashCode += getDuplicationPolicy().hashCode();
        }
        if (getIpName() != null) {
            _hashCode += getIpName().hashCode();
        }
        if (getNumberRequested() != null) {
            _hashCode += getNumberRequested().hashCode();
        }
        if (getOfferAttributes() != null) {
            _hashCode += getOfferAttributes().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(GetOfferRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "GetOfferRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("duplicationPolicy");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "duplicationPolicy"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("ipName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "ipName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberRequested");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "numberRequested"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offerAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "offerAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "OfferAttributeRequirements"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
