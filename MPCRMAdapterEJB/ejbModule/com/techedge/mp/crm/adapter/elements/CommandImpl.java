/**
 * CommandImpl.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.elements;

public class CommandImpl  implements java.io.Serializable {
    private NameValuePairImpl[] audienceID;

    private java.lang.String audienceLevel;

    private java.lang.Boolean debug;

    private java.lang.String event;

    private NameValuePairImpl[] eventParameters;

    private GetOfferRequest[] getOfferRequests;

    private java.lang.String interactionPoint;

    private java.lang.String interactiveChannel;

    private java.lang.String methodIdentifier;

    private java.lang.Integer numberRequested;

    private java.lang.Boolean relyOnExistingSession;

    public CommandImpl() {
    }

    public CommandImpl(
           NameValuePairImpl[] audienceID,
           java.lang.String audienceLevel,
           java.lang.Boolean debug,
           java.lang.String event,
           NameValuePairImpl[] eventParameters,
           GetOfferRequest[] getOfferRequests,
           java.lang.String interactionPoint,
           java.lang.String interactiveChannel,
           java.lang.String methodIdentifier,
           java.lang.Integer numberRequested,
           java.lang.Boolean relyOnExistingSession) {
           this.audienceID = audienceID;
           this.audienceLevel = audienceLevel;
           this.debug = debug;
           this.event = event;
           this.eventParameters = eventParameters;
           this.getOfferRequests = getOfferRequests;
           this.interactionPoint = interactionPoint;
           this.interactiveChannel = interactiveChannel;
           this.methodIdentifier = methodIdentifier;
           this.numberRequested = numberRequested;
           this.relyOnExistingSession = relyOnExistingSession;
    }


    /**
     * Gets the audienceID value for this CommandImpl.
     * 
     * @return audienceID
     */
    public NameValuePairImpl[] getAudienceID() {
        return audienceID;
    }


    /**
     * Sets the audienceID value for this CommandImpl.
     * 
     * @param audienceID
     */
    public void setAudienceID(NameValuePairImpl[] audienceID) {
        this.audienceID = audienceID;
    }

    public NameValuePairImpl getAudienceID(int i) {
        return this.audienceID[i];
    }

    public void setAudienceID(int i, NameValuePairImpl _value) {
        this.audienceID[i] = _value;
    }


    /**
     * Gets the audienceLevel value for this CommandImpl.
     * 
     * @return audienceLevel
     */
    public java.lang.String getAudienceLevel() {
        return audienceLevel;
    }


    /**
     * Sets the audienceLevel value for this CommandImpl.
     * 
     * @param audienceLevel
     */
    public void setAudienceLevel(java.lang.String audienceLevel) {
        this.audienceLevel = audienceLevel;
    }


    /**
     * Gets the debug value for this CommandImpl.
     * 
     * @return debug
     */
    public java.lang.Boolean getDebug() {
        return debug;
    }


    /**
     * Sets the debug value for this CommandImpl.
     * 
     * @param debug
     */
    public void setDebug(java.lang.Boolean debug) {
        this.debug = debug;
    }


    /**
     * Gets the event value for this CommandImpl.
     * 
     * @return event
     */
    public java.lang.String getEvent() {
        return event;
    }


    /**
     * Sets the event value for this CommandImpl.
     * 
     * @param event
     */
    public void setEvent(java.lang.String event) {
        this.event = event;
    }


    /**
     * Gets the eventParameters value for this CommandImpl.
     * 
     * @return eventParameters
     */
    public NameValuePairImpl[] getEventParameters() {
        return eventParameters;
    }


    /**
     * Sets the eventParameters value for this CommandImpl.
     * 
     * @param eventParameters
     */
    public void setEventParameters(NameValuePairImpl[] eventParameters) {
        this.eventParameters = eventParameters;
    }

    public NameValuePairImpl getEventParameters(int i) {
        return this.eventParameters[i];
    }

    public void setEventParameters(int i, NameValuePairImpl _value) {
        this.eventParameters[i] = _value;
    }


    /**
     * Gets the getOfferRequests value for this CommandImpl.
     * 
     * @return getOfferRequests
     */
    public GetOfferRequest[] getGetOfferRequests() {
        return getOfferRequests;
    }


    /**
     * Sets the getOfferRequests value for this CommandImpl.
     * 
     * @param getOfferRequests
     */
    public void setGetOfferRequests(GetOfferRequest[] getOfferRequests) {
        this.getOfferRequests = getOfferRequests;
    }

    public GetOfferRequest getGetOfferRequests(int i) {
        return this.getOfferRequests[i];
    }

    public void setGetOfferRequests(int i, GetOfferRequest _value) {
        this.getOfferRequests[i] = _value;
    }


    /**
     * Gets the interactionPoint value for this CommandImpl.
     * 
     * @return interactionPoint
     */
    public java.lang.String getInteractionPoint() {
        return interactionPoint;
    }


    /**
     * Sets the interactionPoint value for this CommandImpl.
     * 
     * @param interactionPoint
     */
    public void setInteractionPoint(java.lang.String interactionPoint) {
        this.interactionPoint = interactionPoint;
    }


    /**
     * Gets the interactiveChannel value for this CommandImpl.
     * 
     * @return interactiveChannel
     */
    public java.lang.String getInteractiveChannel() {
        return interactiveChannel;
    }


    /**
     * Sets the interactiveChannel value for this CommandImpl.
     * 
     * @param interactiveChannel
     */
    public void setInteractiveChannel(java.lang.String interactiveChannel) {
        this.interactiveChannel = interactiveChannel;
    }


    /**
     * Gets the methodIdentifier value for this CommandImpl.
     * 
     * @return methodIdentifier
     */
    public java.lang.String getMethodIdentifier() {
        return methodIdentifier;
    }


    /**
     * Sets the methodIdentifier value for this CommandImpl.
     * 
     * @param methodIdentifier
     */
    public void setMethodIdentifier(java.lang.String methodIdentifier) {
        this.methodIdentifier = methodIdentifier;
    }


    /**
     * Gets the numberRequested value for this CommandImpl.
     * 
     * @return numberRequested
     */
    public java.lang.Integer getNumberRequested() {
        return numberRequested;
    }


    /**
     * Sets the numberRequested value for this CommandImpl.
     * 
     * @param numberRequested
     */
    public void setNumberRequested(java.lang.Integer numberRequested) {
        this.numberRequested = numberRequested;
    }


    /**
     * Gets the relyOnExistingSession value for this CommandImpl.
     * 
     * @return relyOnExistingSession
     */
    public java.lang.Boolean getRelyOnExistingSession() {
        return relyOnExistingSession;
    }


    /**
     * Sets the relyOnExistingSession value for this CommandImpl.
     * 
     * @param relyOnExistingSession
     */
    public void setRelyOnExistingSession(java.lang.Boolean relyOnExistingSession) {
        this.relyOnExistingSession = relyOnExistingSession;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof CommandImpl)) return false;
        CommandImpl other = (CommandImpl) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.audienceID==null && other.getAudienceID()==null) || 
             (this.audienceID!=null &&
              java.util.Arrays.equals(this.audienceID, other.getAudienceID()))) &&
            ((this.audienceLevel==null && other.getAudienceLevel()==null) || 
             (this.audienceLevel!=null &&
              this.audienceLevel.equals(other.getAudienceLevel()))) &&
            ((this.debug==null && other.getDebug()==null) || 
             (this.debug!=null &&
              this.debug.equals(other.getDebug()))) &&
            ((this.event==null && other.getEvent()==null) || 
             (this.event!=null &&
              this.event.equals(other.getEvent()))) &&
            ((this.eventParameters==null && other.getEventParameters()==null) || 
             (this.eventParameters!=null &&
              java.util.Arrays.equals(this.eventParameters, other.getEventParameters()))) &&
            ((this.getOfferRequests==null && other.getGetOfferRequests()==null) || 
             (this.getOfferRequests!=null &&
              java.util.Arrays.equals(this.getOfferRequests, other.getGetOfferRequests()))) &&
            ((this.interactionPoint==null && other.getInteractionPoint()==null) || 
             (this.interactionPoint!=null &&
              this.interactionPoint.equals(other.getInteractionPoint()))) &&
            ((this.interactiveChannel==null && other.getInteractiveChannel()==null) || 
             (this.interactiveChannel!=null &&
              this.interactiveChannel.equals(other.getInteractiveChannel()))) &&
            ((this.methodIdentifier==null && other.getMethodIdentifier()==null) || 
             (this.methodIdentifier!=null &&
              this.methodIdentifier.equals(other.getMethodIdentifier()))) &&
            ((this.numberRequested==null && other.getNumberRequested()==null) || 
             (this.numberRequested!=null &&
              this.numberRequested.equals(other.getNumberRequested()))) &&
            ((this.relyOnExistingSession==null && other.getRelyOnExistingSession()==null) || 
             (this.relyOnExistingSession!=null &&
              this.relyOnExistingSession.equals(other.getRelyOnExistingSession())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAudienceID() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAudienceID());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAudienceID(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getAudienceLevel() != null) {
            _hashCode += getAudienceLevel().hashCode();
        }
        if (getDebug() != null) {
            _hashCode += getDebug().hashCode();
        }
        if (getEvent() != null) {
            _hashCode += getEvent().hashCode();
        }
        if (getEventParameters() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getEventParameters());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getEventParameters(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getGetOfferRequests() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getGetOfferRequests());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getGetOfferRequests(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getInteractionPoint() != null) {
            _hashCode += getInteractionPoint().hashCode();
        }
        if (getInteractiveChannel() != null) {
            _hashCode += getInteractiveChannel().hashCode();
        }
        if (getMethodIdentifier() != null) {
            _hashCode += getMethodIdentifier().hashCode();
        }
        if (getNumberRequested() != null) {
            _hashCode += getNumberRequested().hashCode();
        }
        if (getRelyOnExistingSession() != null) {
            _hashCode += getRelyOnExistingSession().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(CommandImpl.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "CommandImpl"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audienceID");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "audienceID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "NameValuePairImpl"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("audienceLevel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "audienceLevel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("debug");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "debug"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("event");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "event"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("eventParameters");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "eventParameters"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "NameValuePairImpl"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("getOfferRequests");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "getOfferRequests"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "GetOfferRequest"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interactionPoint");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "interactionPoint"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("interactiveChannel");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "interactiveChannel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("methodIdentifier");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "methodIdentifier"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numberRequested");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "numberRequested"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("relyOnExistingSession");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "relyOnExistingSession"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
