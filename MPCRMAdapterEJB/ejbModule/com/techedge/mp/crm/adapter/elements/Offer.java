/**
 * Offer.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.techedge.mp.crm.adapter.elements;

public class Offer  implements java.io.Serializable {
    private NameValuePairImpl[] additionalAttributes;

    private java.lang.String description;

    private java.lang.String[] offerCode;

    private java.lang.String offerName;

    private java.lang.Integer score;

    private java.lang.String treatmentCode;

    public Offer() {
    }

    public Offer(
           NameValuePairImpl[] additionalAttributes,
           java.lang.String description,
           java.lang.String[] offerCode,
           java.lang.String offerName,
           java.lang.Integer score,
           java.lang.String treatmentCode) {
           this.additionalAttributes = additionalAttributes;
           this.description = description;
           this.offerCode = offerCode;
           this.offerName = offerName;
           this.score = score;
           this.treatmentCode = treatmentCode;
    }


    /**
     * Gets the additionalAttributes value for this Offer.
     * 
     * @return additionalAttributes
     */
    public NameValuePairImpl[] getAdditionalAttributes() {
        return additionalAttributes;
    }


    /**
     * Sets the additionalAttributes value for this Offer.
     * 
     * @param additionalAttributes
     */
    public void setAdditionalAttributes(NameValuePairImpl[] additionalAttributes) {
        this.additionalAttributes = additionalAttributes;
    }

    public NameValuePairImpl getAdditionalAttributes(int i) {
        return this.additionalAttributes[i];
    }

    public void setAdditionalAttributes(int i, NameValuePairImpl _value) {
        this.additionalAttributes[i] = _value;
    }


    /**
     * Gets the description value for this Offer.
     * 
     * @return description
     */
    public java.lang.String getDescription() {
        return description;
    }


    /**
     * Sets the description value for this Offer.
     * 
     * @param description
     */
    public void setDescription(java.lang.String description) {
        this.description = description;
    }


    /**
     * Gets the offerCode value for this Offer.
     * 
     * @return offerCode
     */
    public java.lang.String[] getOfferCode() {
        return offerCode;
    }


    /**
     * Sets the offerCode value for this Offer.
     * 
     * @param offerCode
     */
    public void setOfferCode(java.lang.String[] offerCode) {
        this.offerCode = offerCode;
    }

    public java.lang.String getOfferCode(int i) {
        return this.offerCode[i];
    }

    public void setOfferCode(int i, java.lang.String _value) {
        this.offerCode[i] = _value;
    }


    /**
     * Gets the offerName value for this Offer.
     * 
     * @return offerName
     */
    public java.lang.String getOfferName() {
        return offerName;
    }


    /**
     * Sets the offerName value for this Offer.
     * 
     * @param offerName
     */
    public void setOfferName(java.lang.String offerName) {
        this.offerName = offerName;
    }


    /**
     * Gets the score value for this Offer.
     * 
     * @return score
     */
    public java.lang.Integer getScore() {
        return score;
    }


    /**
     * Sets the score value for this Offer.
     * 
     * @param score
     */
    public void setScore(java.lang.Integer score) {
        this.score = score;
    }


    /**
     * Gets the treatmentCode value for this Offer.
     * 
     * @return treatmentCode
     */
    public java.lang.String getTreatmentCode() {
        return treatmentCode;
    }


    /**
     * Sets the treatmentCode value for this Offer.
     * 
     * @param treatmentCode
     */
    public void setTreatmentCode(java.lang.String treatmentCode) {
        this.treatmentCode = treatmentCode;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Offer)) return false;
        Offer other = (Offer) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.additionalAttributes==null && other.getAdditionalAttributes()==null) || 
             (this.additionalAttributes!=null &&
              java.util.Arrays.equals(this.additionalAttributes, other.getAdditionalAttributes()))) &&
            ((this.description==null && other.getDescription()==null) || 
             (this.description!=null &&
              this.description.equals(other.getDescription()))) &&
            ((this.offerCode==null && other.getOfferCode()==null) || 
             (this.offerCode!=null &&
              java.util.Arrays.equals(this.offerCode, other.getOfferCode()))) &&
            ((this.offerName==null && other.getOfferName()==null) || 
             (this.offerName!=null &&
              this.offerName.equals(other.getOfferName()))) &&
            ((this.score==null && other.getScore()==null) || 
             (this.score!=null &&
              this.score.equals(other.getScore()))) &&
            ((this.treatmentCode==null && other.getTreatmentCode()==null) || 
             (this.treatmentCode!=null &&
              this.treatmentCode.equals(other.getTreatmentCode())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAdditionalAttributes() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getAdditionalAttributes());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getAdditionalAttributes(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getDescription() != null) {
            _hashCode += getDescription().hashCode();
        }
        if (getOfferCode() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getOfferCode());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getOfferCode(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getOfferName() != null) {
            _hashCode += getOfferName().hashCode();
        }
        if (getScore() != null) {
            _hashCode += getScore().hashCode();
        }
        if (getTreatmentCode() != null) {
            _hashCode += getTreatmentCode().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Offer.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "Offer"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("additionalAttributes");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "additionalAttributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "NameValuePairImpl"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("description");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "description"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offerCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "offerCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offerName");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "offerName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("score");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "score"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("treatmentCode");
        elemField.setXmlName(new javax.xml.namespace.QName("http://api.interact.unicacorp.com/xsd", "treatmentCode"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
