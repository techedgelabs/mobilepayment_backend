package com.techedge.mp.crm.adapter.elements;

public enum ValueDataTypeEnum {
    DATA_TYPE_DATETIME("datetime"),
    DATA_TYPE_NUMERIC("numeric"),
    DATA_TYPE_STRING("string");

    private final String dataType;

    private ValueDataTypeEnum(String dataType) {

        this.dataType = dataType;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : dataType.equals(otherName);
    }

    public String getName() {
        return this.name();
    }

    public String getValue() {
        return this.dataType;
    }
}
