/*
 * JBoss, Home of Professional Open Source
 * Copyright 2012, Red Hat, Inc. and/or its affiliates, and individual
 * contributors by the @authors tag. See the copyright.txt in the 
 * distribution for a full listing of individual contributors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,  
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.techedge.mp.document.encoder.business;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLDecoder;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import sun.misc.BASE64Encoder;

import com.techedge.mp.document.encoder.business.interfaces.DocumentDetails;



@Stateless
@LocalBean
public class DocumentService implements DocumentServiceLocal, DocumentServiceRemote {



	@Override
	public DocumentDetails encodeFile(String filename) throws IOException{
		DocumentDetails documentDetails = new DocumentDetails();

		String urlTemplate = "file:"+ System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "document" + File.separator + filename;

		BASE64Encoder encodedFile = new BASE64Encoder();
		//URL url = new URL( "file:C:\\prova.txt" );
		URL url = new URL(urlTemplate);
		File file = new File( URLDecoder.decode( url.getFile(), "UTF-8" ) );
		byte[] bytes = loadFile(file);
		String encodedString = encodedFile.encode(bytes); 

		documentDetails.setDocumentBase64(encodedString);
		documentDetails.setDimension(file.length());
		documentDetails.setFilename(file.getName());
		return documentDetails ;
	}

	private static byte[] loadFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		long length = file.length();
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}
		byte[] bytes = new byte[(int)length];

		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length
				&& (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
			offset += numRead;
		}

		if (offset < bytes.length) {
			throw new IOException("Could not completely read file "+file.getName());
		}

		is.close();
		return bytes;
	}
}
