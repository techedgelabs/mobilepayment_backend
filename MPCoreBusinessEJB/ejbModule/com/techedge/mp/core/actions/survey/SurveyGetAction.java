package com.techedge.mp.core.actions.survey;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Survey;
import com.techedge.mp.core.business.interfaces.SurveyQuestion;
import com.techedge.mp.core.business.model.SurveyBean;
import com.techedge.mp.core.business.model.SurveyQuestionBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SurveyGetAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public SurveyGetAction() {}

    public Survey execute(String ticketId, String requestId, String code) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        Survey survey = new Survey();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isServiceTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");
                userTransaction.commit();

                survey.setStatusCode(ResponseHelper.SURVEY_INVALID_TICKET);
                return survey;
            }

            SurveyBean surveyBean = QueryRepository.findSurveyByCode(em, code);

            if (surveyBean == null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid survey code");
                userTransaction.commit();

                survey.setStatusCode(ResponseHelper.SURVEY_GET_SURVEY_INVALID_CODE);
                return survey;
            }
            
            if ( surveyBean.getStatus() != 1 ) {
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Survey inactive");
                userTransaction.commit();

                survey.setStatusCode(ResponseHelper.SURVEY_GET_SURVEY_INVALID_CODE);
                return survey;
            }

            Date now = new Date();
            
            if ( surveyBean.getStartDate().getTime() > now.getTime() || surveyBean.getEndDate().getTime() < now.getTime() ) {
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Survey time range invalid");
                userTransaction.commit();

                survey.setStatusCode(ResponseHelper.SURVEY_GET_SURVEY_INVALID_CODE);
                return survey;
            }
            
            survey.setCode(surveyBean.getCode());
            survey.setDescription(surveyBean.getDescription());
            survey.setNote(surveyBean.getNote());
            survey.setStartDate(surveyBean.getStartDate());
            survey.setEndDate(surveyBean.getEndDate());
            survey.setStatus(surveyBean.getStatus());

            for (SurveyQuestionBean surveyQuestionBean : surveyBean.getQuestionsList()) {
                SurveyQuestion question = new SurveyQuestion();
                question.setCode(surveyQuestionBean.getCode());
                question.setText(surveyQuestionBean.getText());
                question.setType(surveyQuestionBean.getType());
                question.setSequence(surveyQuestionBean.getSequence());
                survey.getQuestions().add(question);
            }
            
            survey.setStatusCode(ResponseHelper.SURVEY_GET_SURVEY_SUCCESS);

            userTransaction.commit();

            return survey;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
