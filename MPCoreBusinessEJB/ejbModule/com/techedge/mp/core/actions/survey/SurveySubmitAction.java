package com.techedge.mp.core.actions.survey;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SurveyAnswers;
import com.techedge.mp.core.business.model.SurveyBean;
import com.techedge.mp.core.business.model.SurveyQuestionBean;
import com.techedge.mp.core.business.model.SurveySubmissionAnswerBean;
import com.techedge.mp.core.business.model.SurveySubmissionBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SurveySubmitAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public SurveySubmitAction() {}

    public String execute(String ticketId, String requestId, String code, String key, SurveyAnswers answers) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isServiceTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");
                userTransaction.commit();

                return ResponseHelper.SURVEY_INVALID_TICKET;
            }

            // Verifica che il code sia associato a un questionario valido
            SurveyBean surveyBean = QueryRepository.findSurveyByCode(em, code);

            if (surveyBean == null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid survey code");
                userTransaction.commit();

                return ResponseHelper.SURVEY_SUBMIT_SURVEY_INVALID_CODE;
            }
            
            // Verifica che esista una transazione con id uguale a key
            Boolean keyExisting = checkKeyExistance(key);
            
            if ( keyExisting == false ) {
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid key");
                userTransaction.commit();

                return ResponseHelper.SURVEY_SUBMIT_SURVEY_INVALID_KEY;
            }
            
            // Verifica che non sia gi� stata fatta una valutazione per quel key
            SurveySubmissionBean submissionBean = QueryRepository.findSurveySubmissionByKey(em, key);
            
            if (submissionBean != null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Submission key " + key + " exists");
                userTransaction.commit();

                return ResponseHelper.SURVEY_SUBMIT_SURVEY_KEY_EXISTS;
                
            }

            submissionBean = new SurveySubmissionBean();
            submissionBean.setSurveyBean(surveyBean);
            submissionBean.setKey(key);
            submissionBean.setTimpestamp(new Date());
            
            em.persist(submissionBean);
            
            for (SurveyAnswers.Answer answer : answers.getAnswers()) {
                
                SurveyQuestionBean surveyQuestionBean = QueryRepository.findSurveyQuestionByCode(em, answer.getCode());
                
                SurveySubmissionAnswerBean answerBean = new SurveySubmissionAnswerBean();
                answerBean.setSurveyQuestionBean(surveyQuestionBean);
                answerBean.setSurveySubmissionBean(submissionBean);
                answerBean.setAnswer(answer.getValue());
                
                em.persist(answerBean);
            }

            em.merge(submissionBean);

            userTransaction.commit();

            return ResponseHelper.SURVEY_SUBMIT_SURVEY_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
    
    
    private Boolean checkKeyExistance(String key) {
        
        // Controlla se esiste una transazione post paid con mpTransactionID uguale a key
        PostPaidTransactionBean postPaidTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, key);
        if ( postPaidTransactionBean != null ) {
            System.out.println("postpaid found");
            return true;
        }

        PostPaidTransactionHistoryBean postPaidTransactionHistoryBean = QueryRepository.findPostPaidTransactionHistoryBeanByMPId(em, key);
        if (postPaidTransactionHistoryBean != null) {
            System.out.println("postpaid history found");
            return true;
        }

        // Controlla se esiste una transazione pre paid con transactionID uguale a key
        TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, key);
        if ( transactionBean != null ) {
            System.out.println("prepaid found");
            return true;
        }
        
        TransactionHistoryBean transactionHistoryBean = QueryRepository.findTransactionHistoryBeanById(em, key);
        if ( transactionHistoryBean != null ) {
            System.out.println("prepaid history found");
            return true;
        }
        
        System.out.println("transaction not found");
        
        return false;
    }
}
