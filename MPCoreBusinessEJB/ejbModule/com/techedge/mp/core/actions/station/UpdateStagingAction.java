package com.techedge.mp.core.actions.station;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.station.StationUpdateDetail;
import com.techedge.mp.core.business.model.station.StationStageBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UpdateStagingAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UpdateStagingAction() {}

    public String execute(String requestID, ArrayList<StationUpdateDetail> stationList) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();
            
            if (stationList == null || stationList.isEmpty()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                        "La lista delle stazioni da aggiornare è nulla o vuota");
                
                userTransaction.commit();
                return ResponseHelper.STATION_UPDATE_STAGING_STATIONS_LIST_EMPTY;
            }
            
            List<StationStageBean> stationStageBeanList = QueryRepository.findStationStageByRequest(em, requestID);
            
            if (stationStageBeanList != null && !stationStageBeanList.isEmpty()) {
                userTransaction.commit();
                return ResponseHelper.STATION_UPDATE_STAGING_REQUEST_ID_ALREADY_EXISTS;                
            }
            

            for (StationUpdateDetail stationUpdateDetail : stationList) {
                StationStageBean stationStageBean = new StationStageBean();
                stationStageBean.setAddress(stationUpdateDetail.getAddress());
                stationStageBean.setBusiness(stationUpdateDetail.isBusiness());
                stationStageBean.setCity(stationUpdateDetail.getCity());
                stationStageBean.setCountry(stationUpdateDetail.getCountry());
                stationStageBean.setLatitude(stationUpdateDetail.getLatitude());
                stationStageBean.setLongitude(stationUpdateDetail.getLongitude());
                stationStageBean.setLoyalty(stationUpdateDetail.isLoyalty());
                
                if (stationUpdateDetail.getAlias().isEmpty()) {
                    stationStageBean.setAlias(null);
                }
                else {
                    stationStageBean.setAlias(stationUpdateDetail.getAlias());
                }
                
                if (stationUpdateDetail.getMacKey().isEmpty()) {
                    stationStageBean.setMacKey(null);
                }
                else {
                    stationStageBean.setMacKey(stationUpdateDetail.getMacKey());
                }
                
                stationStageBean.setProvince(stationUpdateDetail.getProvince());
                stationStageBean.setStationID(stationUpdateDetail.getStationID());
                stationStageBean.setTemporarilyClosed(stationUpdateDetail.isTemporarilyClosed());
                stationStageBean.setValidityDateDetails(stationUpdateDetail.getValidityDateDetails());
                stationStageBean.setVoucherPayment(stationUpdateDetail.isVoucherPayment());
                stationStageBean.setRequestID(requestID);
                stationStageBean.setProcessed(false);
                
                em.persist(stationStageBean);
            }
            
            userTransaction.commit();
            return ResponseHelper.STATION_UPDATE_STAGING_SUCCESS;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                System.err.println("IllegalStateException: " + e.getMessage());
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                System.err.println("SecurityException: " + e.getMessage());
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                System.err.println("SystemException: " + e.getMessage());
            }

            String message = "FAILED authentication with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

}
