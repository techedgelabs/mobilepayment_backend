package com.techedge.mp.core.actions.station;

import java.awt.Color;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.station.StationChangeLogType;
import com.techedge.mp.core.business.model.DataAcquirerBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.station.StationChangeLogBean;
import com.techedge.mp.core.business.model.station.StationStageBean;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelSheetData;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.TransactionFinalStatusConverter;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendStationListPaymentResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendStationListPaymentResponseCode;
import com.techedge.mp.forecourt.adapter.business.interfaces.StationPayment;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UpdateMasterAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UpdateMasterAction() {}

    public String execute(String requestID, String secretKey, String reportInternalRecipient, String reportExternalRecipient, String groupAcquirer, EmailSenderRemote emailSender, 
            ForecourtInfoServiceRemote forecourtInfoService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();
            
            List<StationStageBean> stationStageBeanList = null;
            ArrayList<ReportChangeLog> reportChangelogList = new ArrayList<>();

            
            if (requestID == null) {
                stationStageBeanList = QueryRepository.findStationStageToProcess(em);
            }
            else {
                stationStageBeanList = QueryRepository.findStationStageToProcessByRequest(em, requestID);
            }
            
            List<StationBean> stationBeanList = QueryRepository.getAllStationBeans(em);
            
            EncryptionAES encryptionAES = new EncryptionAES();
            encryptionAES.loadSecretKey(secretKey);

            for (StationStageBean stationStageBean : stationStageBeanList) {
                boolean isNew = true;
                StationBean tmpStationBean = null;
                
                for (StationBean stationBean : stationBeanList) {
                    if (stationStageBean.getStationID().equals(stationBean.getStationID())) {
                        tmpStationBean = stationBean;
                        isNew = false;
                        break;
                    }
                }
                
                if (isNew) {
                    StationBean stationBean = new StationBean();
                    stationBean.setAddress(stationStageBean.getAddress());
                    stationBean.setCity(stationStageBean.getCity());
                    stationBean.setCountry(stationStageBean.getCountry());
                    stationBean.setLatitude(stationStageBean.getLatitude());
                    stationBean.setLongitude(stationStageBean.getLongitude());
                    stationBean.setLoyaltyActive(stationStageBean.getLoyalty());
                    stationBean.setVoucherActive(stationStageBean.getVoucherPayment());
                    stationBean.setProvince(stationStageBean.getProvince());
                    stationBean.setStationID(stationStageBean.getStationID());
                    stationBean.setBusinessActive(stationStageBean.getBusiness());
                    
                    int status = Station.STATION_STATUS_ACTIVE;
                    
                    stationBean.setStationStatus(status);
                    
                    if (stationStageBean.getAlias() != null && stationStageBean.getMacKey() != null) {
                        DataAcquirerBean dataAcquirerBean = createDataAcquirer(encryptionAES, groupAcquirer, stationStageBean);
                        stationBean.setDataAcquirer(dataAcquirerBean);
                        stationBean.setNewAcquirerActive(true);
                    }
                    else {
                        stationBean.setNewAcquirerActive(false);
                    }
                    
                    if (stationStageBean.getTemporarilyClosed()) {
                        stationBean.setNewAcquirerActive(false);
                        stationBean.setLoyaltyActive(false);
                        stationBean.setVoucherActive(false);
                        stationBean.setBusinessActive(false);
                    }
                    
                    stationBean.setOilAcquirerID(null);
                    stationBean.setNoOilAcquirerID(null);
                    stationBean.setNoOilShopLogin(null);
                    stationBean.setOilShopLogin(null);
                    stationBean.setPostpaidActive(true);
                    stationBean.setPrepaidActive(true);
                    stationBean.setShopActive(true);
                    stationBean.setRefuelingActive(true);
                    
                    em.persist(stationBean);
                    
                    stationStageBean.setProcessed(true);
                    em.merge(stationStageBean);
                    
                    createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "station_id", stationBean.getStationID(), null);
                    createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "loyalty", stationBean.getLoyaltyActive(), null);
                    createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "voucher", stationBean.getVoucherActive(), null);
                    createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "business", stationBean.getBusinessActive(), null);
                    createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "city", stationBean.getCity(), null);
                    createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "address", stationBean.getAddress(), null);
                    createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "province", stationBean.getProvince(), null);
                    createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "country", stationBean.getCountry(), null);
                    createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "latitude", stationBean.getLatitude(), null);
                    createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "longitude", stationBean.getLongitude(), null);
                    createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "acquirer", stationBean.getNewAcquirerActive(), null, reportChangelogList);
                    
                    if (stationBean.getDataAcquirer() != null) {
                        createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "acquirer_apikey", stationBean.getDataAcquirer().getApiKey(), null, null);
                        createChangeLog(StationChangeLogType.Added, stationStageBean, stationBean, "acquirer_secretkey", stationBean.getDataAcquirer().getEncodedSecretKey(), null, null);
                    }
                    
                }
                else {
                    boolean updating = false;
                    DataAcquirerBean dataAcquirerBean = null;
                    
                    if (stationStageBean.getAlias() != null && stationStageBean.getMacKey() != null) {
                        dataAcquirerBean = createDataAcquirer(encryptionAES, groupAcquirer, stationStageBean);
                    }

                    if (!Objects.equals(tmpStationBean.getLoyaltyActive(), stationStageBean.getLoyalty())) {
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "loyalty", stationStageBean.getLoyalty(), tmpStationBean.getLoyaltyActive());
                        updating = true;
                        tmpStationBean.setLoyaltyActive(stationStageBean.getLoyalty());
                    }
                    
                    if (!Objects.equals(tmpStationBean.getVoucherActive(), stationStageBean.getVoucherPayment())) {
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "voucher", stationStageBean.getVoucherPayment(), tmpStationBean.getVoucherActive());
                        updating = true;
                        tmpStationBean.setVoucherActive(stationStageBean.getVoucherPayment());
                    }
                    
                    if (!tmpStationBean.getNewAcquirerActive() && dataAcquirerBean != null && !stationStageBean.getTemporarilyClosed()) {
                            
                        Object newValue = Boolean.TRUE;
                        
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "acquirer", newValue, false, reportChangelogList);
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "acquirer_apikey", dataAcquirerBean.getApiKey(), null);
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "acquirer_secretkey", dataAcquirerBean.getEncodedSecretKey(), null);
                        
                        updating = true;
                        tmpStationBean.setDataAcquirer(dataAcquirerBean);
                        tmpStationBean.setNewAcquirerActive(true);
                    }

                    if (tmpStationBean.getNewAcquirerActive() && dataAcquirerBean == null) {
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "acquirer", false, true, reportChangelogList);
                        updating = true;
                        tmpStationBean.setNewAcquirerActive(false);
                    }

                    if (tmpStationBean.getNewAcquirerActive() && dataAcquirerBean != null) {
                        if (!dataAcquirerBean.getApiKey().equals(tmpStationBean.getDataAcquirer().getApiKey()) 
                                || !dataAcquirerBean.getEncodedSecretKey().equals(tmpStationBean.getDataAcquirer().getEncodedSecretKey())) {
                            
                            Object newValue = Boolean.TRUE;
                            
                            if (stationStageBean.getTemporarilyClosed()) {
                                newValue = Boolean.FALSE;
                            }
                            
                            createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "acquirer", newValue, true, reportChangelogList);
                            createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "acquirer_apikey", dataAcquirerBean.getApiKey(), null);
                            createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "acquirer_secretkey", dataAcquirerBean.getEncodedSecretKey(), null);
                            tmpStationBean.getDataAcquirer().setApiKey(dataAcquirerBean.getApiKey());
                            tmpStationBean.getDataAcquirer().setEncodedSecretKey(dataAcquirerBean.getEncodedSecretKey());
                            
                            updating = true;
                        }
                        
                        
                    }
                    
                    if (!Objects.equals(tmpStationBean.getBusinessActive(), stationStageBean.getBusiness())) {
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "business", stationStageBean.getBusiness(), tmpStationBean.getBusinessActive());
                        updating = true;
                        tmpStationBean.setBusinessActive(stationStageBean.getBusiness());
                    }

                    if (stationStageBean.getTemporarilyClosed()) {
                        updating = true;
                        tmpStationBean.setLoyaltyActive(false);
                        tmpStationBean.setVoucherActive(false);
                        tmpStationBean.setNewAcquirerActive(false);
                        tmpStationBean.setBusinessActive(false);
                        tmpStationBean.setStationStatus(Station.STATION_STATUS_ACTIVE);
                    }
                    else {
                        tmpStationBean.setStationStatus(Station.STATION_STATUS_ACTIVE);
                    }
                    
                    if (!Objects.equals(tmpStationBean.getAddress(), stationStageBean.getAddress())) {
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "address", stationStageBean.getAddress(), tmpStationBean.getAddress());
                        updating = true;
                        tmpStationBean.setAddress(stationStageBean.getAddress());
                    }
                
                    if (!Objects.equals(tmpStationBean.getCity(), stationStageBean.getCity())) {
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "city", stationStageBean.getCity(), tmpStationBean.getCity());
                        updating = true;
                        tmpStationBean.setCity(stationStageBean.getCity());
                    }
                
                    if (!Objects.equals(tmpStationBean.getProvince(), stationStageBean.getProvince())) {
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "province", stationStageBean.getProvince(), tmpStationBean.getProvince());
                        updating = true;
                        tmpStationBean.setProvince(stationStageBean.getProvince());
                    }
                
                    if (!Objects.equals(tmpStationBean.getCountry(), stationStageBean.getCountry())) {
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "country", stationStageBean.getCountry(), tmpStationBean.getCountry());
                        updating = true;
                        tmpStationBean.setCountry(stationStageBean.getCountry());
                    }
                
                    if (!Objects.equals(tmpStationBean.getLatitude(), stationStageBean.getLatitude())) {
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "latitude", stationStageBean.getLatitude(), tmpStationBean.getLatitude());
                        updating = true;
                        tmpStationBean.setLatitude(stationStageBean.getLatitude());
                    }
                
                    if (!Objects.equals(tmpStationBean.getLongitude(), stationStageBean.getLongitude())) {
                        createChangeLog(StationChangeLogType.Changed, stationStageBean, tmpStationBean, "longitude", stationStageBean.getLongitude(), tmpStationBean.getLongitude());
                        updating = true;
                        tmpStationBean.setLongitude(stationStageBean.getLongitude());
                    }

                    if (updating) {
                        em.merge(tmpStationBean);
                    }
                    
                    stationStageBean.setProcessed(true);
                    em.merge(stationStageBean);
                }
            }
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Aggiornamento tabella master stations");
            userTransaction.commit();
            
            userTransaction.begin();

            List<StationBean> updatedStationBeanList = QueryRepository.getAllActiveStationBeans(em);
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invio aggiornamento pv payment a Fortech");
            sendStationListToForecourt(updatedStationBeanList, forecourtInfoService, emailSender, userTransaction, reportInternalRecipient);
            userTransaction.commit();
            
            userTransaction.begin();
            
            updatedStationBeanList = QueryRepository.getAllActiveStationBeans(em);

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invio email per station finder");
            sendExternalReport(updatedStationBeanList, reportExternalRecipient, emailSender);
            
            if (!reportChangelogList.isEmpty()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invio email interna per aggiornamento pv payment");
                sendInternalReport(reportChangelogList, reportInternalRecipient, null, emailSender);
            }
            
            userTransaction.commit();
            
            return ResponseHelper.STATION_UPDATE_MASTER_SUCCESS;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                System.err.println("IllegalStateException: " + e.getMessage());
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                System.err.println("SecurityException: " + e.getMessage());
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                System.err.println("SystemException: " + e.getMessage());
            }

            String message = "FAILED authentication with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
    
    private void createChangeLog(StationChangeLogType changeLogType, StationStageBean stationStage, StationBean stationMaster, String fieldName, Object newValue, Object oldValue, 
            ArrayList<ReportChangeLog> reportChangelogList) {
        
        StationChangeLogBean changeLogBean = new StationChangeLogBean();
        changeLogBean.setChangeType(changeLogType);
        changeLogBean.setRequestID(stationStage.getRequestID());
        changeLogBean.setDateLog(new Date());
        changeLogBean.setStationID(stationStage.getStationID());
        changeLogBean.setFieldName(fieldName);
        changeLogBean.setNewValue(newValue);
        changeLogBean.setOldValue(oldValue);

        em.persist(changeLogBean);
        
        if (reportChangelogList != null) {
            ReportChangeLog reportChangeLog = new ReportChangeLog(changeLogBean, stationStage, stationMaster);
            reportChangelogList.add(reportChangeLog);
        }
    }
    
    private void createChangeLog(StationChangeLogType changeLogType, StationStageBean stationStage, StationBean stationMaster, String fieldName, Object newValue, Object oldValue) {
        createChangeLog(changeLogType, stationStage, stationMaster, fieldName, newValue, oldValue, null);
    }

    private DataAcquirerBean createDataAcquirer(EncryptionAES encryptionAES, String groupAcquirer, StationStageBean stationStageBean) throws Exception {
        DataAcquirerBean dataAcquirerBean = new DataAcquirerBean();
        dataAcquirerBean.setAcquirerID("CARTASI");
        dataAcquirerBean.setApiKey(stationStageBean.getAlias());
        dataAcquirerBean.setCurrency("978");
        dataAcquirerBean.setGroupAcquirer(groupAcquirer);
        dataAcquirerBean.setEncodedSecretKey(encryptionAES.encrypt(stationStageBean.getMacKey()));
        
        em.persist(dataAcquirerBean);
        
        return dataAcquirerBean;
    }
    
    private void sendInternalReport(ArrayList<ReportChangeLog> reportChangelogList, String reportRecipient, String errorMessage, EmailSenderRemote emailSender) throws IOException {
        
        if (reportRecipient == null || reportRecipient.trim().isEmpty()) {
            System.err.println("Invio fallito. Indirizzo di destinazione nullo o vuoto");
            return;
        }
        
        ExcelWorkBook ewb = new ExcelWorkBook();
        ExcelSheetData excelData = ewb.createSheetData();
        SimpleDateFormat sdfEmail = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfChangeLog = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        EmailType emailType = EmailType.UPDATE_STATIONS_LIST;
        List<Parameter> parameters = new ArrayList<Parameter>(0);
        List<Attachment> attachments = new ArrayList<Attachment>(0);
        String paramCount = errorMessage;
        
        if (errorMessage == null) {
            //Cell style for header row
            ExcelCellStyle csHeader = ewb.createCellStyle(true, 12, Color.WHITE, new Color(84, 130, 53), null);
            //Cell style for table row
            ExcelCellStyle csTable = ewb.createCellStyle(false, 12, Color.BLACK, new Color(230, 230, 230), null);

            String[] columnsHeading = new String[] { "Attivit�", "Riferimento Fortech", "Data", "Identificativo PV", "Indirizzo", "Citt�", "Provincia", "Acquirer", 
                    "Alias", "MacKey", "Business", "Stato Payment", "Refueling" };

            String stationID = null;
            int rowIndex = 0;
            int totalCounts = 0;
            
            for (ReportChangeLog reportChangeLog : reportChangelogList) {
                StationBean stationMaster = reportChangeLog.getStationMaster();
                StationStageBean stationStage = reportChangeLog.getStationStage();
                StationChangeLogBean changelog = reportChangeLog.getChangeLog();
                String eventTask = TransactionFinalStatusConverter.getReportText(changelog.getChangeType().name(), null);
                String event = changelog.getFieldName();
                String eventValue = TransactionFinalStatusConverter.getReportText(event, changelog.getNewValue());
                rowIndex = excelData.createRow();
                boolean business = false;
                boolean payment = false;
                boolean refueling = false;
                
                if (!Objects.equals(stationID, stationStage.getStationID())) {
                    stationID = stationStage.getStationID();
                    totalCounts++;
                }
                
                if (stationStage.getBusiness() != null) {
                    business = stationStage.getBusiness();
                }

                if (stationMaster.getNewAcquirerActive() != null) {
                    payment = stationMaster.getNewAcquirerActive();
                }

                if (stationMaster.getRefuelingActive() != null) {
                    refueling = stationMaster.getRefuelingActive();
                }

                excelData.addRowData(eventTask, rowIndex);
                excelData.addRowData(changelog.getRequestID(), rowIndex);
                excelData.addRowData(sdfChangeLog.format(changelog.getDateLog()), rowIndex);
                excelData.addRowData(stationID, rowIndex);
                excelData.addRowData(stationStage.getAddress(), rowIndex);
                excelData.addRowData(stationStage.getCity(), rowIndex);
                excelData.addRowData(stationStage.getProvince(), rowIndex);
                excelData.addRowData(eventValue, rowIndex);
                excelData.addRowData(stationStage.getAlias(), rowIndex);
                excelData.addRowData(stationStage.getMacKey(), rowIndex);
                excelData.addRowData(TransactionFinalStatusConverter.getReportText("business", String.valueOf(business)), rowIndex);
                excelData.addRowData(TransactionFinalStatusConverter.getReportText("payment", String.valueOf(payment)), rowIndex);
                excelData.addRowData(TransactionFinalStatusConverter.getReportText("refueling", String.valueOf(refueling)), rowIndex);
                
            }
            
            ewb.addSheet("Lista PV", columnsHeading, excelData, csHeader, csTable);
            paramCount = String.valueOf(totalCounts);

            // L'allegato deve essere inviato solo se � stato consumato almeno un voucher
            Attachment attachment = new Attachment();
            String dateFilename = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            attachment.setFileName("report_aggiornamento_pv_" + dateFilename + ".xlsx");
            //attachment.setContent(attachmentContent);
            byte[] bytes = ewb.getBytesToStream();
            attachment.setBytes(bytes);
            attachments.add(attachment);
        }
        
        parameters.add(new Parameter("DATE_OPERATION", sdfEmail.format(new Date())));
        parameters.add(new Parameter("STATIONS_LIST_COUNT", paramCount));
        
        System.out.println("Invio email");

        System.out.println("Sending email to: " + reportRecipient);
        String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, reportRecipient, parameters, attachments);
        System.out.println("SendEmailResult: " + sendEmailResult);
        
    }
    
    private void sendExternalReport(List<StationBean> stationBeanList, String reportRecipient, EmailSenderRemote emailSender) throws IOException {
        
        if (reportRecipient == null || reportRecipient.trim().isEmpty()) {
            System.err.println("Invio fallito. Indirizzo di destinazione nullo o vuoto");
            return;
        }
        
        ExcelWorkBook ewb = new ExcelWorkBook();
        ExcelSheetData excelData = ewb.createSheetData();
        SimpleDateFormat sdfEmail = new SimpleDateFormat("dd/MM/yyyy");
        //Cell style for header row
        ExcelCellStyle csHeader = ewb.createCellStyle(true, 12, Color.WHITE, new Color(84, 130, 53), null);
        //Cell style for table row
        ExcelCellStyle csTable = ewb.createCellStyle(false, 12, Color.BLACK, new Color(230, 230, 230), null);

        String[] columnsHeading = new String[] { "Codice PV", "Indirizzo", "Citt�", "Provincia", "Latitudine", "Longitudine", "Abilitato payment App ES+", 
                "Abilitato voucher App ES+", "Abilitato loyalty App ES+", "Abilitato App P.IVA" };

        int rowIndex = 0;
        int totalCounts = 0;
        String valueTrue = "SI";
        String valueFalse = "NO";
        
        if (stationBeanList != null) {
            totalCounts = stationBeanList.size();
        }
        
        for (StationBean stationBean : stationBeanList) {
            rowIndex = excelData.createRow();
            excelData.addRowData(stationBean.getStationID(), rowIndex);
            excelData.addRowData(stationBean.getAddress(), rowIndex);
            excelData.addRowData(stationBean.getCity(), rowIndex);
            excelData.addRowData(stationBean.getProvince(), rowIndex);
            
            String latitude = null;
            String longitude = null;
            
            if (stationBean.getLatitude() != null) {
                latitude = String.valueOf(stationBean.getLatitude());
            }
            
            if (stationBean.getLongitude() != null) {
                longitude = String.valueOf(stationBean.getLongitude());
            }
            
            excelData.addRowData(latitude, rowIndex);
            excelData.addRowData(longitude, rowIndex);
            
            String value = valueFalse;
            if (stationBean.getNewAcquirerActive() != null && stationBean.getNewAcquirerActive()) {
                value = valueTrue;
            }
            
            excelData.addRowData(value, rowIndex);
            
            value = valueFalse;
            if (stationBean.getVoucherActive() != null && stationBean.getVoucherActive()) {
                value = valueTrue;
            }
            
            excelData.addRowData(value, rowIndex);
            
            value = valueFalse;
            if (stationBean.getLoyaltyActive() != null && stationBean.getLoyaltyActive()) {
                value = valueTrue;
            }
            
            excelData.addRowData(value, rowIndex);
            
            value = valueFalse;
            if (stationBean.getBusinessActive() != null && stationBean.getBusinessActive()) {
                value = valueTrue;
            }
            
            excelData.addRowData(value, rowIndex);
        }
        
        ewb.addSheet("Lista PV", columnsHeading, excelData, csHeader, csTable);

        EmailType emailType = EmailType.UPDATE_STATIONS_LIST;
        List<Parameter> parameters = new ArrayList<Parameter>(0);

        parameters.add(new Parameter("DATE_OPERATION", sdfEmail.format(new Date())));
        parameters.add(new Parameter("STATIONS_LIST_COUNT", String.valueOf(totalCounts)));
        
        System.out.println("Invio email");

        List<Attachment> attachments = new ArrayList<Attachment>(0);

        // L'allegato deve essere inviato solo se � stato consumato almeno un voucher
        Attachment attachment = new Attachment();
        String dateFilename = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        attachment.setFileName("report_lista_pv_" + dateFilename + ".xlsx");
        //attachment.setContent(attachmentContent);
        byte[] bytes = ewb.getBytesToStream();
        attachment.setBytes(bytes);
        attachments.add(attachment);

        System.out.println("Sending email to: " + reportRecipient);
        String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, reportRecipient, parameters, attachments);
        System.out.println("SendEmailResult: " + sendEmailResult);
        
    }
    
    private void sendStationListToForecourt(List<StationBean> stationBeanList, ForecourtInfoServiceRemote forecourtInfoService, EmailSenderRemote emailSender, 
            UserTransaction userTransaction, String reportRecipient) throws IllegalStateException, SecurityException, SystemException, IOException {
        
        SendStationListPaymentResponse response = new SendStationListPaymentResponse();
        String requestID = String.valueOf(new Date().getTime());
        ArrayList<StationPayment> stationPaymentList = new ArrayList<>();
         
        if (stationBeanList != null) {
            for (StationBean stationBean : stationBeanList) {
                StationPayment stationPayment = new StationPayment();
                stationPayment.setStationID(stationBean.getStationID());
                
                if (stationBean.getNewAcquirerActive() != null && stationBean.getNewAcquirerActive()) {
                    stationPayment.setPayment(true);
                }
                else {
                    stationPayment.setPayment(false);
                }
                
                stationPaymentList.add(stationPayment);
            }
        }
        
        response = forecourtInfoService.sendStationListPayment(requestID, stationPaymentList);
        
        if (Objects.equals(response.getStatusCode(), SendStationListPaymentResponseCode.STATION_NOT_FOUND_404)) {
            String errorMessage = "Stazione/i non trovata/e nel backend Fortech: ";
            errorMessage += "<ul>";

            for (StationBean stationBean : stationBeanList) {
                for (String stationID : response.getStationNotFound()) {
                    if (stationBean.getStationID().equals(stationID)) {
                        stationBean.setStationStatus(Station.STATION_STATUS_NOT_ACTIVE);
                        em.merge(stationBean);
                        errorMessage += "<li>" + stationID + "</li>";
                    }
                }
            }
            
            errorMessage += "</ul>";
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendStationListToForecourt", requestID, null, errorMessage);
            sendInternalReport(null, reportRecipient, errorMessage, emailSender);
        }
        
        String statusCode = null;
        
        if (response.getStatusCode() != null) {
            statusCode = response.getStatusCode().value();
        }
        
        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "sendStationListToForecourt", requestID, null, statusCode);
    }

    private class ReportChangeLog {
        private StationChangeLogBean changeLog;
        private StationStageBean stationStage;
        private StationBean stationMaster;
        
        public ReportChangeLog(StationChangeLogBean changeLog, StationStageBean stationStage, StationBean stationMaster) {
            this.changeLog = changeLog;
            this.stationStage = stationStage;
            this.stationMaster = stationMaster;
        }
        
        public StationChangeLogBean getChangeLog() {
            return changeLog;
        }
        
        public StationStageBean getStationStage() {
           return stationStage;
        }
        
        public StationBean getStationMaster() {
            return stationMaster;
        }
    }
}
