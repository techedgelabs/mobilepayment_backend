package com.techedge.mp.core.actions.user.v2;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.AuthenticationResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltySessionBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2RefreshUserDataAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2RefreshUserDataAction() {}

    public AuthenticationResponse execute(String ticketId, String loyaltySessionId, String requestId, String deviceId, String deviceName,
            long maxPendingInterval, Integer ticketExpiryTime, Integer loyaltySessionExpiryTime, Long deployDateLong, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            AuthenticationResponse authenticationResponse = null;
            
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                authenticationResponse = new AuthenticationResponse();
                
                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_INVALID_TICKET);
                
                return authenticationResponse;
            }

            UserBean userBean = ticketBean.getUser();
            
            String email = userBean.getPersonalDataBean().getSecurityDataEmail();

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus == User.USER_STATUS_BLOCKED || userStatus == User.USER_STATUS_NEW || userStatus == User.USER_STATUS_CANCELLED) {

                // Un utente che si trova in questo stato non pu� effettuare il login
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User " + email + " in status "
                        + userStatus);
                authenticationResponse = new AuthenticationResponse();
                if (userStatus == User.USER_STATUS_NEW) {
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_NOT_VERIFIED);
                }
                else {
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_UNAUTHORIZED);
                }

                userTransaction.commit();

                return authenticationResponse;
            }

            if (userBean.getUserType() == User.USER_TYPE_REFUELING || userBean.getUserType() == User.USER_TYPE_REFUELING_NEW_ACQUIRER) {

                // L'utente � di tipo refueling e non pu� effutare l'accesso
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User " + email);
                authenticationResponse = new AuthenticationResponse();
                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_INVALID_TICKET);

                userTransaction.commit();

                return authenticationResponse;
            }


            // Rinnovo sessione loyalty
            
            Date creationTimestamp = new Date();
            Date lastUsedTimestamp = creationTimestamp;
            Date expiryTimestamp = DateHelper.addMinutesToDate(loyaltySessionExpiryTime, creationTimestamp);
            
            // Se la sessione loyalty esiste estendi la validit�, atrimenti crea una nuova sessione
            
            LoyaltySessionBean loyaltySessionBean = QueryRepository.findLoyaltySessionBySessionid(em, loyaltySessionId);
            
            if (loyaltySessionBean != null) {
                
                System.out.println("Rinnovata sessione loyalty per l'utente " + userBean.getPersonalDataBean().getSecurityDataEmail() + " (" + userBean.getId() + ")");
                
                loyaltySessionBean.setCreationTimestamp(creationTimestamp);
                loyaltySessionBean.setExpirationTimestamp(expiryTimestamp);
                loyaltySessionBean.setLastUsedTimestamp(lastUsedTimestamp);
                
                em.merge(loyaltySessionBean);
            }
            else {
                
                System.out.println("Creata sessione loyalty per l'utente " + userBean.getPersonalDataBean().getSecurityDataEmail() + " (" + userBean.getId() + ")");
                
                String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                
                if (userBean.getSource() != null && userBean.getSource().equals(User.USER_MULTICARD)) {
                    
                    System.out.println("Generazione sessione loyalty per utente Multicard");
                    
                    // Per l'utente multicard il campo fiscalcode deve essere valorizzato con l'email contenuta in UserExternalData
                    if (userBean.getUserExternalData() != null && !userBean.getUserExternalData().isEmpty()) {
                        
                        fiscalCode = userBean.getUserExternalData().iterator().next().getUuid();
                        userBean.setExternalUserId(fiscalCode);
                    }
                    else {
                        
                        System.out.println("UserExternalData non valido o non presente");
                    }
                }
                
                String newSessionId = new IdGenerator().generateId(16).substring(0, 32);
                loyaltySessionBean = new LoyaltySessionBean(newSessionId, userBean, creationTimestamp, lastUsedTimestamp, expiryTimestamp, fiscalCode);

                em.persist(loyaltySessionBean);
            }
            
            
            // Fix metodo di pagamento di default
            // Pu� esistere un solo metodo di pagamento di default
            // Se esistono pi� medoti di pagamento, viene mantenuto solo uno e per gli altri il flag viene impostato a false
            // Per la scelta del medoto di pagamento da lasciare come default hanno priorit� quelli in stato 2, poi quelli in stato 1 e infine quelli in stato 0
            
            // Conta il numero di metodi di pagamento di default
            int defaultCount = 0;
            for(PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                if (paymentInfoBean.getDefaultMethod() != null && paymentInfoBean.getDefaultMethod()) {
                    if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING) {
                        if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                            defaultCount++;
                        }
                    }
                }
            }
            
            //System.out.println("Rilevati " + defaultCount + " metodi di pagamento di default");
            
            if (defaultCount > 1) {
                
                System.out.println("Attivazione proceduda di bonifica motodi di pagamento di default");
                
                // Ricerca medodo di default in stato 2
                PaymentInfoBean paymentInfoBeanDefault = null;
                for(PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                    if (paymentInfoBean.getDefaultMethod() != null && paymentInfoBean.getDefaultMethod()) {
                        if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {
                            if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                                paymentInfoBeanDefault = paymentInfoBean;
                                break;
                            }
                        }
                    }
                }
                
                if (paymentInfoBeanDefault == null) {
                    // Ricerca medodo di default in stato 1
                    for(PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                        if (paymentInfoBean.getDefaultMethod() != null && paymentInfoBean.getDefaultMethod()) {
                            if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED) {
                                if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                                    paymentInfoBeanDefault = paymentInfoBean;
                                    break;
                                }
                            }
                        }
                    }
                    
                    if (paymentInfoBeanDefault == null) {
                        // Ricerca medodo di default in stato 0
                        for(PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                            if (paymentInfoBean.getDefaultMethod() != null && paymentInfoBean.getDefaultMethod()) {
                                if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING) {
                                    if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                                        paymentInfoBeanDefault = paymentInfoBean;
                                        break;
                                    }
                                }
                            }
                        }
                        
                        if (paymentInfoBeanDefault == null) {
                            
                            Boolean defaultFound = Boolean.FALSE;
                            for(PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                                if (paymentInfoBean.getDefaultMethod() != null && paymentInfoBean.getDefaultMethod()) {
                                    System.out.println("Rilevato metodo di pagamento di default");
                                    if (defaultFound) {
                                        System.out.println("Era stato gi� rilevato un metodo di pagamento di default, quindi il flag defaultMethod viene impostato a false");
                                        paymentInfoBean.setDefaultMethod(Boolean.FALSE);
                                    }
                                    else {
                                        System.out.println("Primo metodo di pagamento di default rilevato -> vadiabile defaultFound impostata a true");
                                        defaultFound = Boolean.TRUE;
                                    }
                                }
                            }
                        }
                        else {
                            System.out.println("Rilevato metodo di pagamento di default in stato 0 con id " + paymentInfoBeanDefault.getId());
                        }
                    }
                    else {
                        System.out.println("Rilevato metodo di pagamento di default in stato 1 con id " + paymentInfoBeanDefault.getId());
                    }
                }
                else {
                    System.out.println("Rilevato metodo di pagamento di default in stato 2 con id " + paymentInfoBeanDefault.getId());
                }
                
                
                if (paymentInfoBeanDefault != null) {
                    
                    System.out.println("Impostazione a false del flag dei metodi di default con id diverso da " + paymentInfoBeanDefault.getId());
                    
                    for(PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                        if (paymentInfoBean.getId() != paymentInfoBeanDefault.getId()) {
                            if (paymentInfoBean.getDefaultMethod() != null && paymentInfoBean.getDefaultMethod()) {
                                paymentInfoBean.setDefaultMethod(Boolean.FALSE);
                            }
                        }
                    }
                }

            }
            
            
            // Rinnovo ticket
            
            ticketBean.renew();
            em.merge(ticketBean);


            em.merge(userBean);

            // 4) restituisci il risultato
            authenticationResponse = new AuthenticationResponse();

            authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
            authenticationResponse.setTicketId(ticketBean.getTicketId());
            authenticationResponse.setLoyaltySessionId(loyaltySessionBean.getSessionId());

            //System.out.println("id: " + userBean.getId());

            User userOut = userBean.toUser();
            
            // Controlla se ci sono nuovi elementi nella tabella documenti che l'utente non ha ancora accettato
            UserCategoryBean userCategoryBean = null;
            
            Boolean isGuestFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.GUEST_FLOW.getCode());
            Boolean isMulticardFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.MULTICARD_FLOW.getCode());
            if (isGuestFlow) {
                userCategoryBean = QueryRepository.findUserCategoryByName(em, UserCategoryType.GUEST_FLOW.getCode());
            }
            else if (isMulticardFlow) {
                userCategoryBean = QueryRepository.findUserCategoryByName(em, UserCategoryType.MULTICARD_FLOW.getCode());
            }
            else {
                userCategoryBean = QueryRepository.findUserCategoryByName(em, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            }
            
            // Ricerca gli elementi della tabella documenti associati alla categoria 3 o con categoria null
            List<DocumentBean> documentBeanList = QueryRepository.findDocumentAll(em);
            for(DocumentBean documentBean : documentBeanList) {
                
                // Si escludono dall'elaborazione i documenti con groupCategory non null
                if (documentBean.getGroupCategory() == null || documentBean.getGroupCategory().isEmpty()) {
                
                    if (documentBean.getUserCategory() == null || documentBean.getUserCategory().equals(String.valueOf(userCategoryBean.getId()))) {
                        
                        List<DocumentAttributeBean> documentAttributeBeanList = documentBean.getDocumentCheck();
                        
                        for(DocumentAttributeBean documentAttributeBean : documentAttributeBeanList) {
                            
                            //System.out.println("In verifica " + documentAttributeBean.getCheckKey());
                            
                            if ((documentAttributeBean.getConditionText() != null && !documentAttributeBean.getConditionText().isEmpty()) ||
                                (documentAttributeBean.getSubTitle()      != null && !documentAttributeBean.getSubTitle().isEmpty())) {
                                
                                //System.out.println("In elaborazione " + documentAttributeBean.getCheckKey());
                                
                                // Se l'elemento non � presente tra i termini e condizioni associati all'utente allora va inserito con valid a false
                                String checkKey = documentAttributeBean.getCheckKey();
                                
                                Boolean checkKeyFound = false;
                                
                                Set<TermsOfService> termsOfServiceList = userOut.getPersonalData().getTermsOfServiceData();
                                for(TermsOfService termsOfService : termsOfServiceList) {
                                    
                                    if (termsOfService.getKeyval().equals(checkKey)) {
                                        checkKeyFound = true;
                                        //System.out.println(checkKey + " gi� presente");
                                        break;
                                    }
                                }
                                
                                if (!checkKeyFound) {
                                    
                                    //System.out.println(checkKey + " non trovato");
                                    
                                    TermsOfService termsOfServiceNew = new TermsOfService();
                                    termsOfServiceNew.setAccepted(false);
                                    termsOfServiceNew.setKeyval(checkKey);
                                    termsOfServiceNew.setPersonalData(userOut.getPersonalData());
                                    termsOfServiceNew.setValid(Boolean.FALSE);
                                    
                                    userOut.getPersonalData().getTermsOfServiceData().add(termsOfServiceNew);
                                    
                                    //System.out.println(checkKey + " inserito");
                                }
                            }
                        }
                    }
                }
                else {
                    
                    //System.out.println("Trovato documento " + documentBean.getDocumentkey() + " con groupCategory non nullo: " + documentBean.getGroupCategory());
                }
            }
            
            // I metodi di pagamento di tipo multicard devono essere filtrati in base al dpan attivo
            String activeDpan = userBean.getActiveMcCardDpan();
            
            System.out.println("ActiveDpan: " + activeDpan);
            
            for(Iterator<PaymentInfo> i = userOut.getPaymentData().iterator(); i.hasNext();) {
                PaymentInfo paymentInfo = i.next();
                
                if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                    
                    System.out.println("Trovata multicard con dpan " + paymentInfo.getToken());
                    
                    if (paymentInfo.getToken() != null && !paymentInfo.getToken().equals(activeDpan)) {
                    
                        System.out.println("Multicard non restituita");
                        
                        i.remove();
                    }
                }
            }
            
            userOut.setLoyaltyCheckEnabled(Boolean.FALSE);
            authenticationResponse.setUser(userOut);

            userTransaction.commit();

            return authenticationResponse;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                System.err.println("IllegalStateException: " + e.getMessage());
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                System.err.println("SecurityException: " + e.getMessage());
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                System.err.println("SystemException: " + e.getMessage());
            }

            String message = "FAILED refreshing user data with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
