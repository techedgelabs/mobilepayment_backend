package com.techedge.mp.core.actions.user.v2;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ExternalProviderAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.EncryptionRSA;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.parking.integration.business.ParkingServiceRemote;
import com.techedge.mp.parking.integration.business.interfaces.UserMyCiceroDataResult;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.RefuelingAuthentiationResult;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2ExternalProviderAuthenticationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2ExternalProviderAuthenticationAction() {}

    public ExternalProviderAuthenticationResponse execute(ParkingServiceRemote parkingService,RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, String username,String password,String accessToken, String externalProvider, String requestId, String deviceId, String deviceName,
            String deviceToken, String proxyHost, String proxyPort, String proxyNoHosts, long maxPendingInterval, Integer ticketExpiryTime, Integer loyaltySessionExpiryTime,
            Integer loginAttemptsLimit, Integer loginLockExpiryTime, List<String> userBlockExceptionList, UserCategoryService userCategoryService, UnavailabilityPeriodService unavailabilityPeriodService,
            DWHAdapterServiceRemote dwhAdapterService, PushNotificationServiceRemote pushNotificationService, Integer maxRetryAttemps, String sendingType,
            EmailSenderRemote emailSender, SmsServiceRemote smsService, Long deployDateLong, Boolean refuelingOauth2Active, String privateKeyPEM, StringSubstitution stringSubstitution) throws EJBException {
            
                
        UserMyCiceroDataResult userMyCiceroDataResult = null;
        
        RefuelingAuthentiationResult userEnjoyDataResult = null;
        
        try {
            
            //Se viene valorizzato accessToken siamo in presenza di un utente myCicero
            if(externalProvider.equals(User.USER_SOURCE_MYCICERO)){
                
                //TODO gestire eccezione token
                
                userMyCiceroDataResult = parkingService.retrieveUserData(accessToken);
                
                if (userMyCiceroDataResult == null) {
                    
                    System.err.println("externalProviderUserAccountData null");
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "externalProviderUserAccountData");

                    ExternalProviderAuthenticationResponse externalProviderAuthenticationResponse = new ExternalProviderAuthenticationResponse();
                    externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_PROVIDER_AUTH_FAILURE);
                    
                    return externalProviderAuthenticationResponse;
                }
                
                if(userMyCiceroDataResult != null && ! userMyCiceroDataResult.getSuccesso()){
                    
                    //TODO Gestire i messaggi di errori con i codici restituiti da MyCicero
                    
                    System.out.println("************** DEBUG userMyCiceroDataResult internalStatusCode: " + userMyCiceroDataResult.getInternalStatusCode());
                    System.out.println("************** DEBUG userMyCiceroDataResult MessaggioErrore:    " + userMyCiceroDataResult.getMessaggioErrore());
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "externalProviderUserAccountData: " + userMyCiceroDataResult.getMessaggioErrore());

                    ExternalProviderAuthenticationResponse externalProviderAuthenticationResponse = new ExternalProviderAuthenticationResponse();
                    externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_PROVIDER_LOGON_TICKET_ALREADY_IN_USE);
                    
                    if(userMyCiceroDataResult.getMessaggioErrore().equalsIgnoreCase("Token non valido")){
                        externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_PROVIDER_AUTH_INVALID_TICKET);
                    }
                    
                    return externalProviderAuthenticationResponse;
                }
            }
            else if(externalProvider.equals(User.USER_SOURCE_ENJOY)){
                
                String decryptedPassword = null;

                try {
                    // Effettua il decrypt della password passata in input dal servizio
                    EncryptionRSA encryption = new EncryptionRSA();
                    encryption.loadPrivateKey(privateKeyPEM);
                    decryptedPassword = encryption.decrypt(password);
                }
                catch (Exception ex) {
                    
                    ex.printStackTrace();
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Decryption error: " + ex.getMessage());

                    ExternalProviderAuthenticationResponse externalProviderAuthenticationResponse = new ExternalProviderAuthenticationResponse();
                    externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_PROVIDER_AUTH_LOGIN_ERROR);
                    
                    return externalProviderAuthenticationResponse;
                }
                
                System.out.println("decryptedPassword: " + decryptedPassword);
                
                Date now = new Date();
                String enjoyRequestId = "ENS-" + now.getTime();
                String encodedPassword = EncoderHelper.encodeMD5(decryptedPassword);
                System.out.println("plain password: "   + password);
                System.out.println("encoded password: " + encodedPassword);
                if (refuelingOauth2Active) {
                    System.out.println("Chiamata Enjoy 2.0");
                    userEnjoyDataResult = refuelingNotificationServiceOAuth2.refuelingAuthentication(enjoyRequestId, username, encodedPassword);
                }
                else {
                    System.out.println("Chiamata Enjoy 1.0");
                    userEnjoyDataResult = refuelingNotificationService.refuelingAuthentication(enjoyRequestId, username, encodedPassword);
                }
                
                if (!userEnjoyDataResult.getStatusCode().equals("USR-AUTH-200")) {
                    
                    if (userEnjoyDataResult.getStatusCode().equals("USR-AUTH-401")) {
                        
                        // L'utente Enjoy non possiede il codice fiscale
                        System.err.println("Errore di autenticazione sul backend Enjoy: " + userEnjoyDataResult.getStatusCode());
                        
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "externalProviderAuthentication error " + userEnjoyDataResult.getStatusCode());

                        ExternalProviderAuthenticationResponse externalProviderAuthenticationResponse = new ExternalProviderAuthenticationResponse();
                        externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_PROVIDER_AUTH_NO_FISCAL_CODE);
                        
                        return externalProviderAuthenticationResponse;
                    }
                    
                    // Credenziali errate o utente non esistente su backend Enjoy
                    System.err.println("Errore di autenticazione sul backend Enjoy: " + userEnjoyDataResult.getStatusCode());
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "externalProviderAuthentication error " + userEnjoyDataResult.getStatusCode());

                    ExternalProviderAuthenticationResponse externalProviderAuthenticationResponse = new ExternalProviderAuthenticationResponse();
                    externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_PROVIDER_AUTH_LOGIN_ERROR);
                    
                    return externalProviderAuthenticationResponse;
                }
            }
            else {
                
                // Eccezione per mancanza di informazioni causa mancata autenticazione di un external provider user authentication
                System.err.println("externalProviderUserAccountData null");
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "externalProviderUserAccountData Provider "+ externalProvider+" NOT FOUND...");

                ExternalProviderAuthenticationResponse externalProviderAuthenticationResponse = new ExternalProviderAuthenticationResponse();
                externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_PROVIDER_AUTH_FAILURE);
                
                return externalProviderAuthenticationResponse;
            }
            
            
        }

        catch (Exception ex) {
            
            ex.printStackTrace();
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, ex.getMessage());

            ExternalProviderAuthenticationResponse externalProviderAuthenticationResponse = new ExternalProviderAuthenticationResponse();
            externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_PROVIDER_AUTH_FAILURE);
            
            return externalProviderAuthenticationResponse;
        }
        
        
        
        
        UserTransaction userTransaction = context.getUserTransaction();
        
        try {
            
            userTransaction.begin();
            
            UserBean userBean = null;
            MobilePhone mobilePhone = new MobilePhone();
            
            try {
                if(externalProvider.equalsIgnoreCase(User.USER_SOURCE_MYCICERO)){
                    userBean = QueryRepository.findUserCustomerByEmail(em, userMyCiceroDataResult.getEmail());
                }
                else if(externalProvider.equalsIgnoreCase(User.USER_SOURCE_ENJOY)){
                    userBean = QueryRepository.findUserCustomerByEmail(em, userEnjoyDataResult.getEmail());                    
                }
            }
            catch (NonUniqueResultException nue) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", userMyCiceroDataResult.getEmail(), null, "Non unique record found !");
                throw new Exception(nue);
            }
            
            
            if (userBean != null) {
                
                userTransaction.commit();
                
                System.out.println("L'utente risulta gi� registrato");
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Esiste gi� un utente iscritto con l'email " + userBean.getPersonalDataBean().getSecurityDataEmail());

                ExternalProviderAuthenticationResponse externalProviderAuthenticationResponse = new ExternalProviderAuthenticationResponse();
                externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_PROVIDER_AUTH_USER_EXISTS);
                
                return externalProviderAuthenticationResponse;
            }
            else {
                
                // Utente non ancora registrato sul backend -> avvio processo di registrazione
                
                System.out.println("Prima login externalProvider nuovo utente");
                
                User user = new User();
                PersonalData personalData = new PersonalData();
                if(userMyCiceroDataResult!= null){
                    if (userMyCiceroDataResult.getName() != null)
                        personalData.setFirstName(userMyCiceroDataResult.getName());
                    if (userMyCiceroDataResult.getSurname() != null)
                        personalData.setLastName(userMyCiceroDataResult.getSurname());
                    if (userMyCiceroDataResult.getEmail() != null)
                    personalData.setSecurityDataEmail(userMyCiceroDataResult.getEmail());
                    if(userMyCiceroDataResult.getAreaCode() != null && ! userMyCiceroDataResult.getAreaCode().isEmpty())
                        mobilePhone.setPrefix(userMyCiceroDataResult.getAreaCode()); 
                    if (userMyCiceroDataResult.getPhoneNumber() != null)
                        mobilePhone.setNumber(userMyCiceroDataResult.getPhoneNumber());
                    mobilePhone.setStatus(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE);
                    user.getMobilePhoneList().add(mobilePhone);
                    user.setSource(User.USER_SOURCE_MYCICERO);
                }
                else if (userEnjoyDataResult!= null){
                    if (userEnjoyDataResult.getName() != null)
                        personalData.setFirstName(userEnjoyDataResult.getName());
                    if (userEnjoyDataResult.getSurname() != null)
                        personalData.setLastName(userEnjoyDataResult.getSurname());
                    if (userEnjoyDataResult.getFiscalCode() != null)
                        personalData.setFiscalCode(userEnjoyDataResult.getFiscalCode());
                    if (userEnjoyDataResult.getDateOfBirth() != null)
                        personalData.setBirthDate(userEnjoyDataResult.getDateOfBirth());
                    if (userEnjoyDataResult.getBirthMunicipality() != null)
                        personalData.setBirthMunicipality(userEnjoyDataResult.getBirthMunicipality());
                    if (userEnjoyDataResult.getBirthProvince() != null)
                        personalData.setBirthProvince(userEnjoyDataResult.getBirthProvince());
                    if (userEnjoyDataResult.getEmail() != null)
                    personalData.setSecurityDataEmail(userEnjoyDataResult.getEmail());
                    if (userEnjoyDataResult.getSex() != null)
                        personalData.setSex(userEnjoyDataResult.getSex());
                    if (userEnjoyDataResult.getPhoneNumber() != null)
                        mobilePhone.setNumber(userEnjoyDataResult.getPhoneNumber());
                    mobilePhone.setStatus(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE);
                    user.getMobilePhoneList().add(mobilePhone);
                    user.setSource(User.USER_SOURCE_ENJOY);
                }
                
                
                user.setPersonalData(personalData);
               
               // user.setUserStatus(User.USER_STATUS_SOCIAL_REGISTRATION); //Momentaneamente INfFO non necessaria da chiedere se possibile aggiungere il nuovo stato per utente esterno myCicero/Enjoy
                user.setUserStatusRegistrationCompleted(true);
                user.setEniStationUserType(Boolean.FALSE);
                
                ExternalProviderAuthenticationResponse externalProviderAuthenticationResponse = new ExternalProviderAuthenticationResponse();
                
                externalProviderAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_PROVIDER_AUTH_NEW_USER);
                externalProviderAuthenticationResponse.setUser(user);
                
                userTransaction.commit();
                
                return externalProviderAuthenticationResponse;
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                System.err.println("IllegalStateException: " + e.getMessage());
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                System.err.println("SecurityException: " + e.getMessage());
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                System.err.println("SystemException: " + e.getMessage());
            }

            String message = "FAILED authentication with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

}
