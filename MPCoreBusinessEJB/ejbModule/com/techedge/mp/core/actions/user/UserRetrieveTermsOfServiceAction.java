package com.techedge.mp.core.actions.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.DocumentCheckInfo;
import com.techedge.mp.core.business.interfaces.DocumentInfo;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveTermsOfServiceData;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserRetrieveTermsOfServiceAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserRetrieveTermsOfServiceAction() {}

    public RetrieveTermsOfServiceData execute(String ticketId, String requestId, UserCategoryType userCategoryType, UserCategoryService userCategoryService, Boolean isOptional)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                RetrieveTermsOfServiceData retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
                retrieveTermsOfServiceData.setStatusCode(ResponseHelper.USER_RETRIEVE_TERMS_SERVICE_INVALID_TICKET);
                return retrieveTermsOfServiceData;
            }

            if (ticketBean.isServiceTicket()) {

                // Se il servizio viene chiamato con il ticket di sistema siamo in iscrizione e devono essere restituiti tutti i documenti che matchano con la categoria dell'utente si sta creando

                RetrieveTermsOfServiceData retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
                List<DocumentInfo> documentInfoList = new ArrayList<DocumentInfo>(0);

                List<DocumentBean> documentBeanList = QueryRepository.findDocumentAll(em);

                if (documentBeanList != null) {

                    for (DocumentBean documentBeanFor : documentBeanList) {

                        //System.out.println("Trovato documentBean con userCategory " + documentBeanFor.getUserCategory());

                        Boolean addDocument = true;

                        if (documentBeanFor.getUserCategory() != null) {

                            addDocument = false;
                            
                            UserCategoryBean userCategoryBean = QueryRepository.findUserCategoryByName(em, userCategoryType.getCode());
                            
                            if (userCategoryBean == null) {
                                
                                System.out.println("userCategoryBean null");
                            }
                            else {
                                
                                if (documentBeanFor.getUserCategory().equals(String.valueOf(userCategoryBean.getId()))) {
                                    
                                    addDocument = true;
                                }
                            }
                        }

                        if (addDocument) {

                            //System.out.println("Documento inserito nella risposta");

                            DocumentInfo documentinfo = new DocumentInfo();
                            if (documentBeanFor.getTemplateFile() != null && !documentBeanFor.getTemplateFile().equals("")) {
                                documentinfo.setDocumentKey(documentBeanFor.getDocumentkey());
                            }
                            else {
                                documentinfo.setDocumentKey("");
                            }
                            documentinfo.setPosition(documentBeanFor.getPosition());
                            documentinfo.setTitle(documentBeanFor.getTitle());
                            List<DocumentCheckInfo> docCheckList = new ArrayList<DocumentCheckInfo>(0);
                            List<DocumentAttributeBean> docAttrList = documentBeanFor.getDocumentCheck();

                            Boolean dciFound = false;

                            for (DocumentAttributeBean dab : docAttrList) {
                                DocumentCheckInfo dci = new DocumentCheckInfo();
                                if (dab.getConditionText() == null || dab.getConditionText().equals("")) {
                                    dci.setConditionText(dab.getExtendedConditionText());
                                }
                                else {
                                    dci.setConditionText(dab.getConditionText());
                                }
                                dci.setMandatory(dab.isMandatory());
                                dci.setPosition(dab.getPosition());
                                dci.setId(dab.getCheckKey());
                                docCheckList.add(dci);

                                dciFound = true;
                            }

                            documentinfo.setCheckList(docCheckList);

                            if (dciFound == true) {
                                documentInfoList.add(documentinfo);
                            }
                        }
                        else {

                            //System.out.println("Documento non inserito nella risposta");
                        }

                    }
                }

                retrieveTermsOfServiceData.setDocuments(documentInfoList);
                retrieveTermsOfServiceData.setStatusCode(ResponseHelper.USER_RETRIEVE_TERMS_SERVICE_SUCCESS);

                userTransaction.commit();

                return retrieveTermsOfServiceData;
            }

            if (ticketBean.isCustomerTicket()) {
                // Se il servizio viene chiamato con il ticket di un utente devono essere restituiti solo i documenti in stato non valido
                UserBean userBean = ticketBean.getUser();
                Set<TermsOfServiceBean> termsOfServiceBeanData = userBean.getPersonalDataBean().getTermsOfServiceBeanData();

                RetrieveTermsOfServiceData retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
                List<DocumentInfo> documentInfoList = new ArrayList<DocumentInfo>(0);

                List<DocumentBean> documentBeanList = QueryRepository.findDocumentAll(em);

                if (documentBeanList != null) {

                    for (DocumentBean documentBeanFor : documentBeanList) {

                        //System.out.println("Trovato documentBean con userCategory " + documentBeanFor.getUserCategory());

                        Boolean addDocument = true;

                        if (documentBeanFor.getUserCategory() != null && !documentBeanFor.getUserCategory().equals("")) {

                            addDocument = false;

                            Long userCategoryId = Long.valueOf(documentBeanFor.getUserCategory());
                            
                            UserCategoryBean userCategoryBean = QueryRepository.findUserCategoryById(em, userCategoryId);
                            
                            if (userCategoryBean == null) {
                                
                                System.out.println("userCategoryBean null");
                            }
                            else {
                            
                                //System.out.println("Lo userType " + userBean.getUserType() + " appartiene alla categoria " + userCategoryBean.getName() + "?");
                                
                                Boolean check = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), userCategoryBean.getName());
                                
                                if (check) {
                                    //System.out.println("Si");
                                    addDocument = true;
                                }
                                else {
                                    //System.out.println("No");
                                }
                            }

                        }

                        if (addDocument) {

                            //System.out.println("Documento " + documentBeanFor.getDocumentkey() + " inserito nella risposta");

                            DocumentInfo documentinfo = new DocumentInfo();
                            if (documentBeanFor.getTemplateFile() != null && !documentBeanFor.getTemplateFile().equals("")) {
                                documentinfo.setDocumentKey(documentBeanFor.getDocumentkey());
                            }
                            else {
                                documentinfo.setDocumentKey("");
                            }
                            documentinfo.setPosition(documentBeanFor.getPosition());
                            documentinfo.setTitle(documentBeanFor.getTitle());
                            List<DocumentCheckInfo> docCheckList = new ArrayList<DocumentCheckInfo>(0);
                            List<DocumentAttributeBean> docAttrList = documentBeanFor.getDocumentCheck();

                            Boolean dciFound = false;

                            for (DocumentAttributeBean dab : docAttrList) {
                                
                                if (isOptional == null || !(Boolean.FALSE || Boolean.TRUE)) {
                                    isOptional = false;
                                }
                                
                                if (isOptional && !dab.isMandatory()) {

                                    TermsOfServiceBean termsOfServiceBeanSearch = null;
                                    for (TermsOfServiceBean termsOfServiceBean : termsOfServiceBeanData) {

                                        if (termsOfServiceBean.getKeyval().equals(dab.getCheckKey())) {
                                            termsOfServiceBeanSearch = termsOfServiceBean;

                                        }
                                    }

                                    DocumentCheckInfo dci = new DocumentCheckInfo();
                                    if (dab.getConditionText() == null || dab.getConditionText().equals("")) {
                                        dci.setConditionText(dab.getExtendedConditionText());
                                    }
                                    else {
                                        dci.setConditionText(dab.getConditionText());
                                    }
                                    dci.setMandatory(dab.isMandatory());
                                    dci.setPosition(dab.getPosition());
                                    dci.setId(dab.getCheckKey());
                                    docCheckList.add(dci);

                                    dciFound = true;
                                    
                                    if (termsOfServiceBeanSearch != null) {
                                        dci.setAccepted(termsOfServiceBeanSearch.getAccepted());
                                    }

                                }
                                
                                if (!isOptional) {
                                    
                                    TermsOfServiceBean termsOfServiceBeanSearch = null;
                                    for (TermsOfServiceBean termsOfServiceBean : termsOfServiceBeanData) {
    
                                        if (termsOfServiceBean.getKeyval().equals(dab.getCheckKey())) {
                                            termsOfServiceBeanSearch = termsOfServiceBean;
    
                                        }
                                    }
    
                                    if (termsOfServiceBeanSearch == null) {
            
                                        DocumentCheckInfo dci = new DocumentCheckInfo();
                                        if (dab.getConditionText() == null || dab.getConditionText().equals("")) {
                                            dci.setConditionText(dab.getExtendedConditionText());
                                        }
                                        else {
                                            dci.setConditionText(dab.getConditionText());
                                        }
                                        dci.setMandatory(dab.isMandatory());
                                        dci.setPosition(dab.getPosition());
                                        dci.setId(dab.getCheckKey());
                                        docCheckList.add(dci);
    
                                        dciFound = true;
                                    }
                                    else {
    
                                        if (termsOfServiceBeanSearch.getValid() == null || !termsOfServiceBeanSearch.getValid()) {
    
                                            DocumentCheckInfo dci = new DocumentCheckInfo();
                                            if (dab.getConditionText() == null || dab.getConditionText().equals("")) {
                                                dci.setConditionText(dab.getExtendedConditionText());
                                            }
                                            else {
                                                dci.setConditionText(dab.getConditionText());
                                            }
                                            dci.setMandatory(dab.isMandatory());
                                            dci.setPosition(dab.getPosition());
                                            dci.setId(dab.getCheckKey());
                                            dci.setAccepted(termsOfServiceBeanSearch.getAccepted());
                                            docCheckList.add(dci);
    
                                            dciFound = true;
                                        }
                                        else {
    
                                            dciFound = false;
                                        }
                                    }
                                }

                                documentinfo.setCheckList(docCheckList);

                                if (dciFound == true) {
                                    documentInfoList.add(documentinfo);
                                }
                            }
                        }
                    }
                }

                retrieveTermsOfServiceData.setDocuments(documentInfoList);
                retrieveTermsOfServiceData.setStatusCode(ResponseHelper.USER_RETRIEVE_TERMS_SERVICE_SUCCESS);

                userTransaction.commit();

                return retrieveTermsOfServiceData;
            }

            // Ticket non valido
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

            userTransaction.commit();

            RetrieveTermsOfServiceData retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
            retrieveTermsOfServiceData.setStatusCode(ResponseHelper.USER_RETRIEVE_TERMS_SERVICE_INVALID_TICKET);
            return retrieveTermsOfServiceData;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieve Terms of Service data with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
