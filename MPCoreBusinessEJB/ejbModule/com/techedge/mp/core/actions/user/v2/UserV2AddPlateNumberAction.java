package com.techedge.mp.core.actions.user.v2;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PlateNumberBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.PlateNumberHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2AddPlateNumberAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2AddPlateNumberAction() {}

    public String execute(String ticketId, String requestId, String plateNumber, String description) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_V2_ADD_PLATE_NUMBER_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_V2_ADD_PLATE_NUMBER_INVALID_TICKET;
            }
            
            if (userBean.getUserType().equals(User.USER_TYPE_GUEST)) {

                // Utente guest non abilitato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid userType: " + userBean.getUserType());

                userTransaction.commit();

                return ResponseHelper.USER_V2_ADD_PLATE_NUMBER_UNAUTHORIZED;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to add plate number in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_V2_ADD_PLATE_NUMBER_UNAUTHORIZED;
            }
            
            // Elimina gli spazi dalla targa e trasformazione minuscole in maiuscole
            plateNumber = PlateNumberHelper.clear(plateNumber);
            
            // Verifica la validit� del formato della targa
            if (!PlateNumberHelper.isValid(plateNumber)) {
                
                // La targa inserita non � valida
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid plate number: " + plateNumber);

                userTransaction.commit();

                return ResponseHelper.USER_V2_ADD_PLATE_NUMBER_INVALID_FORMAT;
            }

            // Controlla se l'utente ha gi� associato la stessa targa
            if (!userBean.getPlateNumberList().isEmpty()) {
                
                for(PlateNumberBean plateNumberBean : userBean.getPlateNumberList()) {
                    
                    if(plateNumberBean.getPlateNumber().equals(plateNumber)) {
                        
                        // L'utente ha gi� associato questa targa
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Plate number " + plateNumber + " found");

                        userTransaction.commit();

                        return ResponseHelper.USER_V2_ADD_PLATE_NUMBER_FOUND;
                    }
                }
            }
            
            // Cerca se esiste gi� una targa di default
            PlateNumberBean defaultPlateNumberBean = null;
            if (!userBean.getPlateNumberList().isEmpty()) {
                
                for(PlateNumberBean plateNumberBean : userBean.getPlateNumberList()) {
                    
                    if(plateNumberBean.getDefaultPlateNumber()) {
                        
                        defaultPlateNumberBean = plateNumberBean;
                        break;
                    }
                }
            }
            
            PlateNumberBean plateNumberBean = new PlateNumberBean();
            plateNumberBean.setPlateNumber(plateNumber);
            plateNumberBean.setDescription(description);
            plateNumberBean.setCreationTimestamp(new Date());
            plateNumberBean.setUserBean(userBean);
            plateNumberBean.setDefaultPlateNumber(Boolean.FALSE);
            
            if (defaultPlateNumberBean == null) {
                plateNumberBean.setDefaultPlateNumber(Boolean.TRUE);
            }
            
            em.persist(plateNumberBean);
            
            userBean.getPlateNumberList().add(plateNumberBean);
            em.merge(userBean);
            
            userTransaction.commit();

            return ResponseHelper.USER_V2_ADD_PLATE_NUMBER_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED removing plate number with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
