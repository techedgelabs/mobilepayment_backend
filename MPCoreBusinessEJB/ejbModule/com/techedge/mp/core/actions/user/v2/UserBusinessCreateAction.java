package com.techedge.mp.core.actions.user.v2;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.interfaces.user.UserDevice;
import com.techedge.mp.core.business.model.CityInfoBean;
import com.techedge.mp.core.business.model.EmailDomainBean;
import com.techedge.mp.core.business.model.HistoryPasswordBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.PlateNumberBean;
import com.techedge.mp.core.business.model.PrefixNumberBean;
import com.techedge.mp.core.business.model.ProvinceInfoBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserDeviceBean;
import com.techedge.mp.core.business.model.VerificationCodeBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.FiscalCodeHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.PlateNumberHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.QueryRepositoryBusiness;
import com.techedge.mp.core.business.utilities.VatNumberHelper;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserBusinessCreateAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private UserCategoryService userCategoryService;

    public UserBusinessCreateAction() {}

    public String execute(String ticketId, String requestId, User user, String checkFiscalCode, Integer verificationCodeExpiryTime, String activationLink, Double initialCap,
            Boolean createUserMobilePhoneMandatory, Boolean checkMobilePhoneUnique, EmailSenderRemote emailSender, String checkMailInBlacklist, Integer verificationNumberDevice,
            String checkBirthPlace, String sendingType, Integer maxRetryAttemps, SmsServiceRemote smsService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isServiceTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_CREATE_INVALID_TICKET;
            }

            // Verifica che non esista gi� un utente con quella email
            UserBean oldUserBean = QueryRepositoryBusiness.findNotCancelledUserBusinessByEmail(em, user.getPersonalData().getSecurityDataEmail());
                
            if (oldUserBean != null) {

                // Esiste gi� un utente con quella email
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User exists");

                userTransaction.commit();

                return ResponseHelper.USER_CREATE_USER_EXISTS;
            }
            
            System.out.println("Check codice fiscale - stato: " + checkFiscalCode);

            if (checkFiscalCode.equals("true")) {

                // Verifica che non esista gi� un utente con quel codice fiscale
                UserBean oldUserBeanFC = QueryRepositoryBusiness.findNotCancelledUserBusinessByFiscalCode(em, user.getPersonalData().getFiscalCode());

                if (oldUserBeanFC != null) {

                    // Esiste gi� un utente con quel codice fiscale
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "FiscalCode exists");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_FISCALCODE_EXISTS;
                }

                // Verifica che il codice fiscale sia compatibile con i dati anagrafici inseriti
                String nome = user.getPersonalData().getFirstName();
                String cognome = user.getPersonalData().getLastName();
                String sesso = user.getPersonalData().getSex();

                Calendar calendar = new GregorianCalendar();
                calendar.setTime(user.getPersonalData().getBirthDate());
                int giorno = calendar.get(Calendar.DAY_OF_MONTH);
                int mese = calendar.get(Calendar.MONTH) + 1;
                int anno = calendar.get(Calendar.YEAR);

                FiscalCodeHelper fiscalCodeHelper = new FiscalCodeHelper(nome, cognome, giorno, mese, anno, sesso);

                Boolean check = fiscalCodeHelper.verificaAnagrafica(user.getPersonalData().getFiscalCode());

                System.out.println(fiscalCodeHelper.toString());

                if (check != Boolean.TRUE) {

                    // Il codice fiscale non è compatibile con i dati anagrafici
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "FiscalCode mismatch");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_FISCALCODE_MISMATCH;
                }

                if (checkBirthPlace.equals("true")) {

                    String cityOfBirth = user.getPersonalData().getBirthMunicipality();
                    String provinceOfBirth = user.getPersonalData().getBirthProvince();
                    String fiscalCode = user.getPersonalData().getFiscalCode();

                    // Si converte il luogo di nascita in maiuscolo prima di effettuare la ricerca sul db
                    cityOfBirth = cityOfBirth.toUpperCase();

                    CityInfoBean cityInfoBean = QueryRepository.findCityByNameAndProvince(em, cityOfBirth, provinceOfBirth);

                    if (cityInfoBean == null) {
                        // Il codice fiscale non Ä� compatibile con i dati anagrafici
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "City " + cityOfBirth + " doesn't found in database");

                        userTransaction.commit();

                        return ResponseHelper.USER_CREATE_FISCALCODE_MISMATCH;
                    }

                    if (!cityInfoBean.getProvince().equals(provinceOfBirth)) {
                        // Il codice fiscale non � compatibile con i dati anagrafici
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Province " + provinceOfBirth + " doesn't match");

                        userTransaction.commit();

                        return ResponseHelper.USER_CREATE_FISCALCODE_MISMATCH;
                    }

                    if (cityInfoBean.getCode() != null) {
                        check = fiscalCodeHelper.verificaCodiceCatastale(fiscalCode, cityInfoBean.getCode());
                        String cityCode = fiscalCode.substring(11, 15);
                        if (check != Boolean.TRUE) {
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "City code in fiscalcode " + cityCode
                                    + " doesn't match");

                            userTransaction.commit();

                            return ResponseHelper.USER_CREATE_FISCALCODE_MISMATCH;
                        }
                    }
                }
            }

            //CONTROLLO MAIL IN BLACKLIST
            if (checkMailInBlacklist.equals("true")) {
                String[] splits = user.getPersonalData().getSecurityDataEmail().split("@");

                EmailDomainBean emailDomainBean = null;
                if (splits[1] != null) {
                    emailDomainBean = QueryRepository.findEmailDomainByEmail(em, splits[1]);
                }
                if (emailDomainBean != null) {
                    // Esiste già un utente con quel codice fiscale
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid mail");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_INVALID_MAIL;
                }
            }
            
            //CONTROLLO VALIDITA' PARTITA IVA
            String vatNumber = user.getPersonalDataBusinessList().get(0).getVatNumber();
            boolean validate = VatNumberHelper.validate(vatNumber, loggerService);
            
            if (!validate) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Vat number " + vatNumber + " is invalid");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_CREATE_INVALID_VAT_NUMBER;
            }

            String province = user.getPersonalDataBusinessList().get(0).getProvince();
            ProvinceInfoBean provinceInfoBean = QueryRepository.findProvinceByName(em, province);
            
            if (provinceInfoBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Province " + province + " doesn't found in database");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_CREATE_INVALID_PROVINCE;
            }
            
            String source = user.getSource();
            
            if (Objects.equals(source, "ENISTATION+")) {
                
                if (user.getUserStatus() != User.USER_STATUS_MIGRATING_BUSINESS) {
                    // Ticket non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, 
                            "Invalid user status, found: " + user.getUserStatus() + ", expected: " + User.USER_STATUS_MIGRATING_BUSINESS);

                    userTransaction.commit();

                    return ResponseHelper.USER_BUSINESS_CREATE_INVALID_SOURCE_TOKEN;
                }
                
                String sourceToken = user.getSourceToken();
                TicketBean sourceTicketBean = QueryRepository.findTicketBusinessById(em, sourceToken);
                
                if (sourceTicketBean == null || !sourceTicketBean.isBusinessTicket() || !sourceTicketBean.isValid()) {
                    // Ticket non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid source token");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_INVALID_REQUEST;
                }
                
                UserBean userCustomerBean = sourceTicketBean.getUser();
                
                if (userCustomerBean == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User customer not found by source token");

                    userTransaction.commit();

                    return ResponseHelper.USER_BUSINESS_CREATE_INVALID_SOURCE_TOKEN;
                }
                
                String userCustomerEmail = userCustomerBean.getPersonalDataBean().getSecurityDataEmail();
                String userCustomerFiscalCode = userCustomerBean.getPersonalDataBean().getFiscalCode();
                
                if (!Objects.equals(userCustomerEmail, user.getPersonalData().getSecurityDataEmail())) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User customer email doesn't match current user");

                    userTransaction.commit();

                    return ResponseHelper.USER_BUSINESS_CREATE_INVALID_SOURCE_USER_EMAIL;
                }

                if (!Objects.equals(userCustomerFiscalCode, user.getPersonalData().getFiscalCode())) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User customer fiscalcode doesn't match current user");

                    userTransaction.commit();

                    return ResponseHelper.USER_BUSINESS_CREATE_INVALID_SOURCE_USER_FISCALCODE;
                }
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Create user business from json");
                user.getUserDeviceList().clear();
                user.getMobilePhoneList().clear();
                // Crea l'utente
                UserBean userBean = UserBean.createMigrateBusinessUser(user, initialCap);
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "import user customer password");

                PersonalDataBean personalDataBean = userBean.getPersonalDataBean();
                personalDataBean.setSecurityDataPassword(userCustomerBean.getPersonalDataBean().getSecurityDataPassword());
                personalDataBean.setExpirationDateRescuePassword(null);
                personalDataBean.setRescuePassword(null);

                HistoryPasswordBean historyPasswordBean = new HistoryPasswordBean();
                historyPasswordBean.setPersonalDataBean(personalDataBean);
                historyPasswordBean.setPassword(personalDataBean.getSecurityDataPassword());
                personalDataBean.getHistoryPasswordBeanData().add(historyPasswordBean);

                userBean.setPersonalDataBean(personalDataBean);
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Import user device customer in status verified");

                for (UserDeviceBean userCustomerDeviceBean : userCustomerBean.getUserDeviceBean()) {
                    
                    if (userCustomerDeviceBean.getStatus() == UserDevice.USER_DEVICE_STATUS_VERIFIED) {
                        UserDeviceBean userDeviceBean = new UserDeviceBean();
                        userDeviceBean.setAssociationTimestamp(new Date());
                        userDeviceBean.setCreationTimestamp(new Date());
                        userDeviceBean.setDeviceId(user.getDeviceId());
                        userDeviceBean.setStatus(UserDevice.USER_DEVICE_STATUS_VERIFIED);
                        userDeviceBean.setUserBean(userBean);
                        
                        userBean.getUserDeviceBean().add(userDeviceBean);
                    }
                }

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Import user mobile phone customer in status active");

                for (MobilePhoneBean userCustomerMobilePhone : userCustomerBean.getMobilePhoneList()) {
                    
                    if (userCustomerMobilePhone.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_ACTIVE) {
                        MobilePhoneBean mobilePhoneBean = new MobilePhoneBean();
                        mobilePhoneBean.setCreationTimestamp(new Date());
                        mobilePhoneBean.setNumber(userCustomerMobilePhone.getNumber());
                        mobilePhoneBean.setPrefix(userCustomerMobilePhone.getPrefix());
                        mobilePhoneBean.setStatus(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE);
                        mobilePhoneBean.setUser(userBean);
                        
                        userBean.getMobilePhoneList().add(mobilePhoneBean);
                    }
                }

                // Approvazione automatica dei flag per il consenso alla geolocalizzazione e alla ricezione di notifiche
                TermsOfServiceBean geolocationTermsOfServiceBean = new TermsOfServiceBean();
                geolocationTermsOfServiceBean.setAccepted(Boolean.TRUE);
                geolocationTermsOfServiceBean.setKeyval("GEOLOCALIZZAZIONE_NEW_1");
                geolocationTermsOfServiceBean.setPersonalDataBean(userBean.getPersonalDataBean());
                geolocationTermsOfServiceBean.setValid(Boolean.TRUE);
                userBean.getPersonalDataBean().getTermsOfServiceBeanData().add(geolocationTermsOfServiceBean);

                TermsOfServiceBean notificationTermsOfServiceBean = new TermsOfServiceBean();
                notificationTermsOfServiceBean.setAccepted(Boolean.TRUE);
                notificationTermsOfServiceBean.setKeyval("NOTIFICATION_NEW_1");
                notificationTermsOfServiceBean.setPersonalDataBean(userBean.getPersonalDataBean());
                notificationTermsOfServiceBean.setValid(Boolean.TRUE);
                userBean.getPersonalDataBean().getTermsOfServiceBeanData().add(notificationTermsOfServiceBean);

                // Salva l'utente su db
                em.persist(userBean);

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Import user customer and creation new user business completed");
                
                // Rinnova il ticket
                ticketBean.renew();
                em.merge(ticketBean);

                userTransaction.commit();

                return ResponseHelper.USER_CREATE_SUCCESS;
            }

            if (user.getPersonalData().getSecurityDataPassword() == null || user.getPersonalData().getSecurityDataPassword().isEmpty()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User password is null or empty string");

                userTransaction.commit();

                return ResponseHelper.USER_CREATE_INVALID_REQUEST;
            }
            
            if (createUserMobilePhoneMandatory) {
                if (user.getMobilePhoneList().isEmpty()) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone not found");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_MOBILE_PHONE_NOT_FOUND;
                }

                if (user.getMobilePhoneList().size() > 1) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone size > 1");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_MOBILE_PHONE_INVALID_LIST_SIZE;
                }

                MobilePhone mobilePhone = user.getMobilePhoneList().get(0);

                if (mobilePhone.getPrefix() == null || mobilePhone.getNumber() == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone data invalid");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_MOBILE_PHONE_INVALID;
                }

                // Verifica che il prefisso sia valido
                PrefixNumberBean prefixNumberBean = QueryRepository.findPrefixByCode(em, mobilePhone.getPrefix());
                if (prefixNumberBean == null) {

                    // Prefisso non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Prefix " + mobilePhone.getPrefix() + " not valid");

                    userTransaction.commit();

                    return ResponseHelper.MOBILE_PHONE_UPDATE_INVALID_PREFIX;
                }

                if (checkMobilePhoneUnique) {
                    /*
                     * Il controllo deve essere fatto solo su numeri di telefono associati ad utenti che hanno giÅ• verificato l'email
                     * MobilePhoneBean mobilePhoneExist = QueryRepository.findMobilePhoneByNumberAndPrefix(em, mobilePhone.getNumber(), mobilePhone.getPrefix());
                     * if (mobilePhoneExist != null) {
                     * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone exists");
                     * userTransaction.commit();
                     * return ResponseHelper.USER_CREATE_MOBILE_PHONE_EXISTS;
                     * }
                     */

                    Boolean mobilePhoneFound = false;
                    List<MobilePhoneBean> mobilePhoneBeanList = QueryRepository.findAllActiveMobilePhoneByNumberAndPrefix(em, mobilePhone.getNumber(), mobilePhone.getPrefix());

                    if (mobilePhoneBeanList != null && !mobilePhoneBeanList.isEmpty()) {

                        for (MobilePhoneBean mobilePhoneBean : mobilePhoneBeanList) {

                            System.out.println("Trovato telefono per utente in stato " + mobilePhoneBean.getUser().getUserStatus());

                            if (mobilePhoneBean.getUser().getUserStatus() != User.USER_STATUS_NEW && mobilePhoneBean.getUser().getUserStatus() != User.USER_STATUS_CANCELLED) {

                                Integer userTypeFound = mobilePhoneBean.getUser().getUserType();
                                Boolean isBusiness = userCategoryService.isUserTypeInUserCategory(userTypeFound, UserCategoryType.BUSINESS.getCode());
                                
                                if (isBusiness) {
                                    System.out.println("Il telefono � gi�stato usato da un utente in stato " + mobilePhoneBean.getUser().getUserStatus());
                                    mobilePhoneFound = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (mobilePhoneFound) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone exists");
                        userTransaction.commit();
                        return ResponseHelper.USER_CREATE_MOBILE_PHONE_EXISTS;
                    }

                }

                user.getMobilePhoneList().get(0).setStatus(MobilePhone.MOBILE_PHONE_STATUS_PENDING);
                //String verificationCode = new IdGenerator().generateId(32).substring(0, 8).toUpperCase();
                String verificationCode = new IdGenerator().generateNumericId(8);
                System.out.println("Generato codice di verifica " + verificationCode + " per numero di telefono " + user.getMobilePhoneList().get(0).getNumber());
                user.getMobilePhoneList().get(0).setVerificationCode(verificationCode);

            }
            else {
                //Non essendo previsto da parametro il controllo del cellulare elimino il dato per evitare dati non corretti sul database
                System.out.println("Controllo cellulare non impostato elimino i dati per evitare dati non corretti sul database");
                if (!user.getMobilePhoneList().isEmpty()) {
                    user.getMobilePhoneList().clear();
                }
            }
            // Crea l'utente
            UserBean userBean = UserBean.createNewBusinessUser(user, initialCap);

            if (userBean.getDeviceID() != null) {
                List<UserBean> userBeanList = QueryRepository.findUserByDeviceId(em, userBean.getDeviceID());
                if (userBeanList != null && userBeanList.size() >= verificationNumberDevice) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Reached the usable device limit");
                    userTransaction.commit();
                    return ResponseHelper.USER_CREATE_MAX_USERDEVICE;
                }
            }

            if (!userBean.getMobilePhoneList().isEmpty()) {
                Date now = new Date();
                Date expiryDate = DateHelper.addMinutesToDate(verificationCodeExpiryTime, now);

                for (MobilePhoneBean mobilePhoneBean : userBean.getMobilePhoneList()) {
                    mobilePhoneBean.setCreationTimestamp(now);
                    mobilePhoneBean.setLastUsedTimestamp(now);
                    mobilePhoneBean.setExpirationTimestamp(expiryDate);
                }
            }

            // Approvazione automatica dei flag per il consenso alla geolocalizzazione e alla ricezione di notifiche

            TermsOfServiceBean geolocationTermsOfServiceBean = new TermsOfServiceBean();
            geolocationTermsOfServiceBean.setAccepted(Boolean.TRUE);
            geolocationTermsOfServiceBean.setKeyval("GEOLOCALIZZAZIONE_NEW_1");
            geolocationTermsOfServiceBean.setPersonalDataBean(userBean.getPersonalDataBean());
            geolocationTermsOfServiceBean.setValid(Boolean.TRUE);
            userBean.getPersonalDataBean().getTermsOfServiceBeanData().add(geolocationTermsOfServiceBean);

            TermsOfServiceBean notificationTermsOfServiceBean = new TermsOfServiceBean();
            notificationTermsOfServiceBean.setAccepted(Boolean.TRUE);
            notificationTermsOfServiceBean.setKeyval("NOTIFICATION_NEW_1");
            notificationTermsOfServiceBean.setPersonalDataBean(userBean.getPersonalDataBean());
            notificationTermsOfServiceBean.setValid(Boolean.TRUE);
            userBean.getPersonalDataBean().getTermsOfServiceBeanData().add(notificationTermsOfServiceBean);
            
            // Inserimento targa
            String plateNumber = user.getPersonalDataBusinessList().get(0).getLicensePlate();
            if (plateNumber != null && !plateNumber.isEmpty()) {
                
                // Elimina gli spazi dalla targa e trasformazione minuscole in maiuscole
                plateNumber = PlateNumberHelper.clear(plateNumber);
                
                // Verifica la validit� del formato della targa
                if (!PlateNumberHelper.isValid(plateNumber)) {
                    
                    // La targa inserita non � valida
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid plate number: " + plateNumber);

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_PLATE_NUMBER_INVALID;
                }
                
                String defaultDescription = "Targa";
                
                PlateNumberBean plateNumberBean = new PlateNumberBean();
                plateNumberBean.setPlateNumber(plateNumber);
                plateNumberBean.setDescription(defaultDescription);
                plateNumberBean.setCreationTimestamp(new Date());
                plateNumberBean.setUserBean(userBean);
                plateNumberBean.setDefaultPlateNumber(Boolean.TRUE);
                
                em.persist(plateNumberBean);
                
                userBean.getPlateNumberList().add(plateNumberBean);
            }

            // Salva l'utente su db
            em.persist(userBean);

            // Genera il codice per la verifica dell'email
            Date now = new Date();
            Date lastUsed = now;
            Date expiryDate = DateHelper.addMinutesToDate(verificationCodeExpiryTime, now);

            String newVerificationCodeId = new IdGenerator().generateId(10).substring(0, 10);

            VerificationCodeBean verificationCodeBean = new VerificationCodeBean(newVerificationCodeId, userBean, VerificationCodeBean.STATUS_NEW, now, lastUsed, expiryDate);

            em.persist(verificationCodeBean);

            // Invia l'email con il codice di attivazione
            if (emailSender != null) {

                EmailType emailType = EmailType.CONFIRM_EMAIL_BUSINESS;
                String to = user.getPersonalData().getSecurityDataEmail();
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
                parameters.add(new Parameter("ACTIVATION_LINK", activationLink));
                parameters.add(new Parameter("VERIFICATION_CODE", newVerificationCodeId));
                parameters.add(new Parameter("EMAIL", URLEncoder.encode(userBean.getPersonalDataBean().getSecurityDataEmail(), "UTF-8")));

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Sending email to "
                        + user.getPersonalData().getSecurityDataEmail());

                String result = emailSender.sendEmail(emailType, to, parameters);

                //String result = "disabled";

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "SendEmail result: " + result);

            }

            // Rinnova il ticket
            ticketBean.renew();
            em.merge(ticketBean);

            userTransaction.commit();

            return ResponseHelper.USER_CREATE_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
