package com.techedge.mp.core.actions.user.v2;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.PersonalDataBusiness;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PersonalDataBusinessBean;
import com.techedge.mp.core.business.model.ProvinceInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.FiscalCodeHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.VatNumberHelper;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserBusinessUpdateBusinessDataAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private UserCategoryService userCategoryService;

    public UserBusinessUpdateBusinessDataAction() {}

    public String execute(String ticketId, String requestId, PersonalDataBusiness personalDataBusiness) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();
            
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_TICKET;
            }
            
            Integer userType = userBean.getUserType();
            Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());
            
            if (!useBusiness) {

                // Utente guest non abilitato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid userType: " + userBean.getUserType());

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_UPDATE_BUSINESS_DATA_UNAUTHORIZED;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non può invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to modify business data in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_UPDATE_BUSINESS_DATA_UNAUTHORIZED;
            }
            
            //CONTROLLO VALIDITA' CODICE FISCALE
            if (personalDataBusiness.getFiscalCode() != null && !personalDataBusiness.getFiscalCode().isEmpty()) {
                
                Boolean validateCF  = Boolean.TRUE;
                String codiceFiscale = personalDataBusiness.getFiscalCode();
                codiceFiscale = codiceFiscale.toUpperCase();
                if (codiceFiscale.length() < 15) {
                    validateCF = Boolean.FALSE;
                }
                else {
                    String carattereDiControllo = FiscalCodeHelper.calcolaCarattereDiControllo(codiceFiscale.substring(0, 15));
                    if ( !carattereDiControllo.equals(codiceFiscale.substring(15))) {
                        validateCF = Boolean.FALSE;
                    }
                }
                
                boolean validateVat = VatNumberHelper.validate(personalDataBusiness.getFiscalCode(), loggerService);
                
                if (!validateCF && !validateVat) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Fiscalcode " + personalDataBusiness.getFiscalCode() + " is invalid");
    
                    userTransaction.commit();
    
                    return ResponseHelper.USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_FISCAL_CODE;
                }
            }
            
            //CONTROLLO VALIDITA' PARTITA IVA
            String vatNumber = personalDataBusiness.getVatNumber();
            boolean validate = VatNumberHelper.validate(vatNumber, loggerService);
            
            if (!validate) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Vat number " + vatNumber + " is invalid");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_VAT_NUMBER;
            }

            String province = personalDataBusiness.getProvince();
            ProvinceInfoBean provinceInfoBean = QueryRepository.findProvinceByName(em, province);
            
            if (provinceInfoBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Province " + province + " doesn't found in database");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_PROVINCE;
            }
            /*
            if (personalDataBusiness != null) {
                boolean error = false;
                Field[] fields = personalDataBusiness.getClass().getDeclaredFields();
                for (int i = 0; i < fields.length; i++) {
                    Field field = fields[i];
                    boolean accessible = field.isAccessible();
                    Object value = field.get(personalDataBusiness);

                    if (value == null || (value != null && value.toString().isEmpty())) {
                        error = true;
                    }

                    field.setAccessible(accessible);
                    
                    if (error) {
                        // Un utente che si trova in questo stato non può invocare questo servizio
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Field " + field.getName() + " in PersonalDataBusiness is null");

                        userTransaction.commit();

                        return ResponseHelper.USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_DATA;
                    }
                }
            }
            else {
                // Un utente che si trova in questo stato non può invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "PersonalDataBusiness is null");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_UPDATE_BUSINESS_DATA_INVALID_DATA;
            }
            */
        
            PersonalDataBusinessBean personalDataBusinessBean = userBean.getPersonalDataBusinessList().iterator().next();
            
            if (personalDataBusinessBean == null) {
                personalDataBusinessBean = new PersonalDataBusinessBean();
                personalDataBusinessBean.setUserBean(userBean);
                personalDataBusinessBean.setAddress(personalDataBusiness.getAddress());
                personalDataBusinessBean.setBusinessName(personalDataBusiness.getBusinessName());
                personalDataBusinessBean.setCity(personalDataBusiness.getCity());
                personalDataBusinessBean.setFirstName(personalDataBusiness.getFirstName());
                personalDataBusinessBean.setFiscalCode(personalDataBusiness.getFiscalCode());
                personalDataBusinessBean.setLastName(personalDataBusiness.getLastName());
                personalDataBusinessBean.setLicensePlate(personalDataBusiness.getLicensePlate());
                personalDataBusinessBean.setPecEmail(personalDataBusiness.getPecEmail());
                personalDataBusinessBean.setProvince(personalDataBusiness.getProvince());
                personalDataBusinessBean.setSdiCode(personalDataBusiness.getSdiCode());
                personalDataBusinessBean.setStreetNumber(personalDataBusiness.getStreetNumber());
                personalDataBusinessBean.setVatNumber(personalDataBusiness.getVatNumber());
                personalDataBusinessBean.setZipCode(personalDataBusiness.getZipCode());
                
                em.persist(personalDataBusinessBean);
            }
            else {
                personalDataBusinessBean.setAddress(personalDataBusiness.getAddress());
                personalDataBusinessBean.setBusinessName(personalDataBusiness.getBusinessName());
                personalDataBusinessBean.setCity(personalDataBusiness.getCity());
                personalDataBusinessBean.setFirstName(personalDataBusiness.getFirstName());
                personalDataBusinessBean.setFiscalCode(personalDataBusiness.getFiscalCode());
                personalDataBusinessBean.setLastName(personalDataBusiness.getLastName());
                personalDataBusinessBean.setLicensePlate(personalDataBusiness.getLicensePlate());
                personalDataBusinessBean.setPecEmail(personalDataBusiness.getPecEmail());
                personalDataBusinessBean.setProvince(personalDataBusiness.getProvince());
                personalDataBusinessBean.setSdiCode(personalDataBusiness.getSdiCode());
                personalDataBusinessBean.setStreetNumber(personalDataBusiness.getStreetNumber());
                personalDataBusinessBean.setVatNumber(personalDataBusiness.getVatNumber());
                personalDataBusinessBean.setZipCode(personalDataBusiness.getZipCode());
                
                em.merge(personalDataBusinessBean);
            }
            
            userTransaction.commit();

            return ResponseHelper.USER_CREATE_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
