package com.techedge.mp.core.actions.user.v2;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.UserV2Service;
import com.techedge.mp.core.business.exceptions.SocialAuthenticationException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SendValidationResult;
import com.techedge.mp.core.business.interfaces.SocialAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.user.SocialUserAccountData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.CityInfoBean;
import com.techedge.mp.core.business.model.EmailDomainBean;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.LoyaltyCardMatcherBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.PrefixNumberBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.FiscalCodeHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.ResendValidation;
import com.techedge.mp.core.business.utilities.SocialAuthenticationHelper;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnableLoyaltyCardResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SocialUserV2CreateAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private UserCategoryService userCategoryService;

    public SocialUserV2CreateAction() {}

    public String execute(String accessToken, String socialProvider, String ticketId, String requestId, User user, Integer userType, Long loyaltyCardId, String checkFiscalCode,
            Integer verificationCodeExpiryTime, String activationLink, Double initialCap, Boolean createUserMobilePhoneMandatory, Boolean checkMobilePhoneUnique,
            EmailSenderRemote emailSender, FidelityServiceRemote fidelityService, String checkMailInBlacklist, Integer verificationNumberDevice,
            Integer virtualizationAttemptsLeft, String checkBirthPlace, StringSubstitution stringSubstitution, String proxyHost, String proxyPort, String proxyNoHosts,
            String sendingType, Integer maxRetryAttemps, SmsServiceRemote smsService)
            throws EJBException {

        SocialUserAccountData socialUserAccountData = null;

        try {
            socialUserAccountData = SocialAuthenticationHelper.executeSocialAuthentication(socialProvider, accessToken, proxyHost, proxyPort, proxyNoHosts,requestId);
        }
        catch (SocialAuthenticationException saex) {

            SocialAuthenticationResponse socialAuthenticationResponse = new SocialAuthenticationResponse();

            if (saex != null && saex.getErrorCode() != null && saex.getErrorCode().equals(ResponseHelper.USER_CREATE_INVALID_TOKEN))
                socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_CREATE_INVALID_TOKEN);
            else
                socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_LOGIN_ERROR);
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, saex.getErrorMessage());

            return socialAuthenticationResponse.getStatusCode();
        }
        catch (Exception ex) {

            ex.printStackTrace();

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, ex.getMessage());

            SocialAuthenticationResponse socialAuthenticationResponse = new SocialAuthenticationResponse();
            socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_FAILURE);

            return socialAuthenticationResponse.getStatusCode();
        }

        if (socialUserAccountData == null) {

            System.err.println("socialUserAccountData null");

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "socialUserAccountData null");

            SocialAuthenticationResponse socialAuthenticationResponse = new SocialAuthenticationResponse();
            socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_FAILURE);

            return socialAuthenticationResponse.getStatusCode();
        }

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            UserSocialDataBean userSocialDataBean = null;
            try {
                userSocialDataBean = QueryRepository.findSocialUserByUUID(em, socialUserAccountData.getUserAccountId(), socialProvider);
            }
            catch (NonUniqueResultException nue) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", socialUserAccountData.getUserAccountId(), null, "Non unique record found !");
                userTransaction.commit();

                return ResponseHelper.USER_CREATE_FAILURE;

            }

            if (userSocialDataBean != null) {

                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", socialUserAccountData.getUserAccountId(), null, "Non unique record found !");
                userTransaction.commit();
                return ResponseHelper.USER_CREATE_USER_SOCIAL_EXISTS;
            }

            // Verifica il ticket
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isServiceTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_CREATE_INVALID_TICKET;
            }

            // Verifica che non esista gi� un utente con quella email
            if (socialUserAccountData.getEmail() != null) {
                if (!socialUserAccountData.getEmail().equals(user.getPersonalData().getSecurityDataEmail())) {
                    
                    // L'email restituita dalla social login non corrisponde a quella utilizzata per l'iscrizione
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", socialUserAccountData.getEmail(), null, "social email doesn't match with create email " + user.getPersonalData().getSecurityDataEmail());
                    
                    userTransaction.commit();
                    
                    return ResponseHelper.USER_CREATE_SOCIAL_EMAIL_NOT_MATCH;
                }
            }
            UserBean oldUserBean = QueryRepository.findNotCancelledUserCustomerByEmail(em, socialUserAccountData.getEmail());

            if (oldUserBean != null) {

                // Esiste gi� un utente con quella email
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User exists");

                userTransaction.commit();

                return ResponseHelper.USER_CREATE_USER_EXISTS;
            }

            System.out.println("Check codice fiscale - stato: " + checkFiscalCode);

            if (checkFiscalCode.equals("true")) {

                // Verifica che non esista gi� un utente con quel codice fiscale
                UserBean oldUserBeanFC = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, user.getPersonalData().getFiscalCode());

                if (oldUserBeanFC != null) {

                    // Esiste gi� un utente con quel codice fiscale
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "FiscalCode exists");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_FISCALCODE_EXISTS;
                }

                // Verifica che il codice fiscale sia compatibile con i dati anagrafici inseriti
                String nome = user.getPersonalData().getFirstName();
                String cognome = user.getPersonalData().getLastName();
                String sesso = user.getPersonalData().getSex();

                Calendar calendar = new GregorianCalendar();
                calendar.setTime(user.getPersonalData().getBirthDate());
                int giorno = calendar.get(Calendar.DAY_OF_MONTH);
                int mese = calendar.get(Calendar.MONTH) + 1;
                int anno = calendar.get(Calendar.YEAR);

                FiscalCodeHelper fiscalCodeHelper = new FiscalCodeHelper(nome, cognome, giorno, mese, anno, sesso);

                Boolean check = fiscalCodeHelper.verificaAnagrafica(user.getPersonalData().getFiscalCode());

                System.out.println(fiscalCodeHelper.toString());

                if (check != Boolean.TRUE) {

                    // Il codice fiscale non � compatibile con i dati anagrafici
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "FiscalCode mismatch");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_FISCALCODE_MISMATCH;
                }

                if (checkBirthPlace.equals("true")) {

                    String cityOfBirth = user.getPersonalData().getBirthMunicipality();
                    String provinceOfBirth = user.getPersonalData().getBirthProvince();
                    String fiscalCode = user.getPersonalData().getFiscalCode();

                    // Si converte il luogo di nascita in maiuscolo prima di effettuare la ricerca sul db
                    cityOfBirth = cityOfBirth.toUpperCase();

                    CityInfoBean cityInfoBean = QueryRepository.findCityByNameAndProvince(em, cityOfBirth, provinceOfBirth);

                    if (cityInfoBean == null) {
                        // Il codice fiscale non � compatibile con i dati anagrafici
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "City " + cityOfBirth + " doesn't found in database");

                        userTransaction.commit();

                        return ResponseHelper.USER_CREATE_FISCALCODE_MISMATCH;
                    }

                    if (!cityInfoBean.getProvince().equals(provinceOfBirth)) {
                        // Il codice fiscale non � compatibile con i dati anagrafici
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Province " + provinceOfBirth + " doesn't match");

                        userTransaction.commit();

                        return ResponseHelper.USER_CREATE_FISCALCODE_MISMATCH;
                    }

                    if (cityInfoBean.getCode() != null) {
                        check = fiscalCodeHelper.verificaCodiceCatastale(fiscalCode, cityInfoBean.getCode());
                        String cityCode = fiscalCode.substring(11, 15);
                        if (check != Boolean.TRUE) {
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "City code in fiscalcode " + cityCode
                                    + " doesn't match");

                            userTransaction.commit();

                            return ResponseHelper.USER_CREATE_FISCALCODE_MISMATCH;
                        }
                    }
                }
            }

            //CONTROLLO MAIL IN BLACKLIST
            if (checkMailInBlacklist.equals("true")) {
                String[] splits = user.getPersonalData().getSecurityDataEmail().split("@");

                EmailDomainBean emailDomainBean = null;
                if (splits[1] != null) {
                    emailDomainBean = QueryRepository.findEmailDomainByEmail(em, splits[1]);
                }
                if (emailDomainBean != null) {
                    // Esiste gi� un utente con quel codice fiscale
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid mail");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_INVALID_MAIL;
                }
            }

            // Controlla se l'utente sta effettuando la creazione da social
            if (user.getPersonalData().getSecurityDataPassword() == null || user.getPersonalData().getSecurityDataPassword().isEmpty()) {

                if (socialProvider == null || socialProvider.isEmpty()) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Password null and social not defined");
                    userTransaction.commit();
                    return ResponseHelper.USER_CREATE_INVALID_REQUEST;
                }

                user.getPersonalData().setSecurityDataPassword(UserV2Service.SOCIAL_USER_DEFAULT_PASSWORD);

            }

            if (createUserMobilePhoneMandatory) {
                if (!userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode())
                        && !userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode())) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User category not new flow");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_NOT_ENABLE;
                }

                if (user.getMobilePhoneList().isEmpty()) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone not found");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_MOBILE_PHONE_NOT_FOUND;
                }

                if (user.getMobilePhoneList().size() > 1) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone size > 1");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_MOBILE_PHONE_INVALID_LIST_SIZE;
                }

                MobilePhone mobilePhone = user.getMobilePhoneList().get(0);

                if (mobilePhone.getPrefix() == null || mobilePhone.getNumber() == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone data invalid");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_MOBILE_PHONE_INVALID;
                }

                /*
                 * if (!MobilePhone.isValidPrefix(mobilePhone.getPrefix()) || !MobilePhone.isValidNumber(mobilePhone.getNumber())) {
                 * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone data invalid");
                 * 
                 * userTransaction.commit();
                 * 
                 * return ResponseHelper.USER_CREATE_MOBILE_PHONE_INVALID;
                 * }
                 */

                // Verifica che il prefisso sia valido
                PrefixNumberBean prefixNumberBean = QueryRepository.findPrefixByCode(em, mobilePhone.getPrefix());
                if (prefixNumberBean == null) {

                    // Prefisso non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Prefix " + mobilePhone.getPrefix() + " not valid");

                    userTransaction.commit();

                    return ResponseHelper.MOBILE_PHONE_UPDATE_INVALID_PREFIX;
                }

                if (checkMobilePhoneUnique) {
                    /*
                     * Il controllo deve essere fatto solo su numeri di telefono associati ad utenti che hanno gi� verificato l'email
                     * MobilePhoneBean mobilePhoneExist = QueryRepository.findMobilePhoneByNumberAndPrefix(em, mobilePhone.getNumber(), mobilePhone.getPrefix());
                     * if (mobilePhoneExist != null) {
                     * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone exists");
                     * userTransaction.commit();
                     * return ResponseHelper.USER_CREATE_MOBILE_PHONE_EXISTS;
                     * }
                     */

                    Boolean mobilePhoneFound = false;
                    List<MobilePhoneBean> mobilePhoneBeanList = QueryRepository.findAllActiveMobilePhoneByNumberAndPrefix(em, mobilePhone.getNumber(), mobilePhone.getPrefix());

                    if (mobilePhoneBeanList != null && !mobilePhoneBeanList.isEmpty()) {

                        for (MobilePhoneBean mobilePhoneBean : mobilePhoneBeanList) {

                            System.out.println("Trovato telefono per utente in stato " + mobilePhoneBean.getUser().getUserStatus());

                            if (mobilePhoneBean.getUser().getUserStatus() != User.USER_STATUS_NEW && mobilePhoneBean.getUser().getUserStatus() != User.USER_STATUS_CANCELLED) {

                                System.out.println("Il telefono � gi� stato usato da un utente in stato " + mobilePhoneBean.getUser().getUserStatus());
                                mobilePhoneFound = true;
                                break;
                            }
                        }
                    }

                    if (mobilePhoneFound) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone exists");
                        userTransaction.commit();
                        return ResponseHelper.USER_CREATE_MOBILE_PHONE_EXISTS;
                    }

                }

                user.getMobilePhoneList().get(0).setStatus(MobilePhone.MOBILE_PHONE_STATUS_PENDING);
                //String verificationCode = new IdGenerator().generateId(32).substring(0, 8).toUpperCase();
                String verificationCode = new IdGenerator().generateNumericId(8);
                System.out.println("Generato codice di verifica " + verificationCode + " per numero di telefono " + user.getMobilePhoneList().get(0).getNumber());
                user.getMobilePhoneList().get(0).setVerificationCode(verificationCode);

            }
            else {
                //Non essendo previsto da parametro il controllo del cellulare elimino il dato per evitare dati non corretti sul database
                System.out.println("Controllo cellulare non impostato elimino i dati per evitare dati non corretti sul database");
                if (!user.getMobilePhoneList().isEmpty()) {
                    user.getMobilePhoneList().clear();
                }
            }

            // Verifica la validit� dei dati dell'utente che si sta creando
            // TODO
            user.setUserType(Integer.valueOf(userType));
            // Crea l'utente
            UserBean userBean = UserBean.createNewCustomerUser(user, initialCap);

            if (userBean.getDeviceID() != null) {
                List<UserBean> userBeanList = QueryRepository.findUserByDeviceId(em, userBean.getDeviceID());
                if (userBeanList != null && userBeanList.size() >= verificationNumberDevice) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Reached the usable device limit");
                    userTransaction.commit();
                    return ResponseHelper.USER_CREATE_MAX_USERDEVICE;
                }
            }

            if (!userBean.getMobilePhoneList().isEmpty()) {
                Date now = new Date();
                Date expiryDate = DateHelper.addMinutesToDate(verificationCodeExpiryTime, now);

                for (MobilePhoneBean mobilePhoneBean : userBean.getMobilePhoneList()) {
                    mobilePhoneBean.setCreationTimestamp(now);
                    mobilePhoneBean.setLastUsedTimestamp(now);
                    mobilePhoneBean.setExpirationTimestamp(expiryDate);
                }
            }

            if (virtualizationAttemptsLeft == null) {
                // Prefisso non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Parameter virtualization attempts null");

                userTransaction.commit();

                return ResponseHelper.USER_CREATE_VIRTUALIZATION_ATTEMPTS_NULL;
            }

            userBean.setVirtualizationAttemptsLeft(virtualizationAttemptsLeft);

            // Approvazione automatica dei flag per il consenso alla geolocalizzazione e alla ricezione di notifiche

            TermsOfServiceBean geolocationTermsOfServiceBean = new TermsOfServiceBean();
            geolocationTermsOfServiceBean.setAccepted(Boolean.TRUE);
            geolocationTermsOfServiceBean.setKeyval("GEOLOCALIZZAZIONE_NEW_1");
            geolocationTermsOfServiceBean.setPersonalDataBean(userBean.getPersonalDataBean());
            geolocationTermsOfServiceBean.setValid(Boolean.TRUE);
            userBean.getPersonalDataBean().getTermsOfServiceBeanData().add(geolocationTermsOfServiceBean);

            TermsOfServiceBean notificationTermsOfServiceBean = new TermsOfServiceBean();
            notificationTermsOfServiceBean.setAccepted(Boolean.TRUE);
            notificationTermsOfServiceBean.setKeyval("NOTIFICATION_NEW_1");
            notificationTermsOfServiceBean.setPersonalDataBean(userBean.getPersonalDataBean());
            notificationTermsOfServiceBean.setValid(Boolean.TRUE);
            userBean.getPersonalDataBean().getTermsOfServiceBeanData().add(notificationTermsOfServiceBean);

            // Salva l'utente su db
            em.persist(userBean);

            if (loyaltyCardId != null) {

                System.out.println("Richiesta associazione carta loyalty con id " + loyaltyCardId);

                // Associazione carta loyalty
                LoyaltyCardMatcherBean loyaltyCardMatcherBean = QueryRepository.findLoyaltyCardMatcherById(em, loyaltyCardId);

                if (loyaltyCardMatcherBean == null) {

                    // Carta loyalty non trovata
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "FiscalCode exists");
                }
                else {

                    String panCode = loyaltyCardMatcherBean.getPanCode();

                    System.out.println("Verifica carta loyalty " + panCode);

                    // TODO - Controlla se la carta � gi� associata a un altro utente - necessario?

                    Date now = new Date();

                    String operationID = new IdGenerator().generateId(16).substring(0, 33);
                    String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                    Boolean enable = true;
                    PartnerType partnerType = PartnerType.MP;
                    Long requestTimestamp = now.getTime();

                    EnableLoyaltyCardResult enableLoyaltyCardResult = new EnableLoyaltyCardResult();

                    try {

                        enableLoyaltyCardResult = fidelityService.enableLoyaltyCard(operationID, fiscalCode, panCode, enable, partnerType, requestTimestamp);
                    }
                    catch (Exception e) {

                        // Errore nell'associazione della carta loyalty
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Error enabling loyalty card");
                    }

                    // Verifica l'esito del check
                    String enableLoyaltyCardStatusCode = enableLoyaltyCardResult.getStatusCode();

                    if (!enableLoyaltyCardStatusCode.equals(FidelityResponse.ENABLE_LOYALTY_CARD_OK)) {

                        // Errore nell'associazione della carta loyalty
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error enabling loyalty card: "
                                + enableLoyaltyCardStatusCode);
                    }
                    else {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Associazione loyalty card: " + panCode);

                        // Associa la nuova carta loyalty all'utente
                        LoyaltyCardBean loyaltyCardBean = new LoyaltyCardBean();

                        loyaltyCardBean.setPanCode(panCode);
                        loyaltyCardBean.setEanCode("");
                        loyaltyCardBean.setStatus(LoyaltyCard.LOYALTY_CARD_STATUS_VALIDA);

                        loyaltyCardBean.setUserBean(userBean);

                        em.persist(loyaltyCardBean);

                        // Salva l'utente su db
                        em.merge(userBean);

                    }
                }
            }

            System.out.println("UserAccountId: " + socialUserAccountData.getUserAccountId());

            userSocialDataBean = new UserSocialDataBean();
            userSocialDataBean.setProvider(socialProvider);
            userSocialDataBean.setUuid(socialUserAccountData.getUserAccountId());
            userSocialDataBean.setUserBean(userBean);
            em.persist(userSocialDataBean);

            /*
             * Codice disattivato per verifica automatica email /*
             *
            // Genera il codice per la verifica dell'email
            Date now = new Date();
            Date lastUsed = now;
            Date expiryDate = DateHelper.addMinutesToDate(verificationCodeExpiryTime, now);

            String newVerificationCodeId = new IdGenerator().generateId(10).substring(0, 10);

            VerificationCodeBean verificationCodeBean = new VerificationCodeBean(newVerificationCodeId, userBean, VerificationCodeBean.STATUS_NEW, now, lastUsed, expiryDate);

            em.persist(verificationCodeBean);

            // Invia l'email con il codice di attivazione
            if (emailSender != null) {

                String keyFrom = emailSender.getSender();

                if (stringSubstitution != null) {
                    keyFrom = stringSubstitution.getValue(keyFrom, 1);
                }

                System.out.println("keyFrom: " + keyFrom);

                EmailType emailType = EmailType.CONFIRM_EMAIL_V2;
                String to = user.getPersonalData().getSecurityDataEmail();
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
                parameters.add(new Parameter("ACTIVATION_LINK", activationLink));
                parameters.add(new Parameter("VERIFICATION_CODE", newVerificationCodeId));
                parameters.add(new Parameter("EMAIL", URLEncoder.encode(userBean.getPersonalDataBean().getSecurityDataEmail(), "UTF-8")));

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Sending email to "
                        + user.getPersonalData().getSecurityDataEmail());

                String result = emailSender.sendEmail(emailType, keyFrom, to, parameters);

                //String result = "disabled";

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "SendEmail result: " + result);

            }*/
            
            /*
             * Nuovo codice per invio SMS di verifica telefono
             */
            userBean.setUserStatus(User.USER_STATUS_VERIFIED);
            em.persist(userBean);
            
            if (userBean.getMobilePhoneList().isEmpty()) {

                // Gestione degli utenti non ancora migrati
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone not found");
            }
            else {

                //Invio numero di telefono dopo l'update
                Object[] mobilePhoneArrayList = userBean.getMobilePhoneList().toArray();
                MobilePhoneBean mobilePhoneBean = (MobilePhoneBean) mobilePhoneArrayList[0];

                SendValidationResult result = ResendValidation.send(sendingType, maxRetryAttemps, userBean, mobilePhoneBean, loggerService, emailSender, 
                        smsService, userCategoryService, stringSubstitution);

                if (result.equals(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE)) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error in verification mobile phone. "
                            + result.getMessageCode() + " (" + result.getErrorCode() + ")");

                    userTransaction.commit();

                    return ResponseHelper.USER_CHECK_MOBILE_PHONE_FAILURE;
                }
            }

            // Rinnova il ticket
            ticketBean.renew();
            em.merge(ticketBean);

            userTransaction.commit();

            return ResponseHelper.USER_CREATE_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
