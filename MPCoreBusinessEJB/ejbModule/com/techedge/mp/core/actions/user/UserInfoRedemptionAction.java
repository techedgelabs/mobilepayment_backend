package com.techedge.mp.core.actions.user;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.loyalty.InfoRedemptionResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.InfoRedemptionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserInfoRedemptionAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserInfoRedemptionAction() {}

    public InfoRedemptionResponse execute(String ticketId, String requestId, FidelityServiceRemote fidelityService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                return createMessageError(requestId, userTransaction, "Invalid ticket", ResponseHelper.USER_INFO_REDEMPTION_INVALID_TICKET);
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Utente non valido
                return createMessageError(requestId, userTransaction, "User not found", ResponseHelper.USER_INFO_REDEMPTION_INVALID_TICKET);

            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                return createMessageError(requestId, userTransaction, "Unable to get redemption in status " + String.valueOf(userStatus),
                        ResponseHelper.USER_INFO_REDEMPTION_UNAUTHORIZED);
            }

            String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();

            if (fiscalCode == null) {
                // Un utente che si trova in questo stato non pu� invocare questo servizio
                return createMessageError(requestId, userTransaction, "Fiscal code null", ResponseHelper.USER_INFO_REDEMPTION_FISCAL_CODE_NULL);
            }

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            PartnerType partnerType = PartnerType.MP;
            long requestTimestamp = new Date().getTime();

            InfoRedemptionResult redemptionResult = new InfoRedemptionResult();
            
            try {
                redemptionResult = fidelityService.infoRedemption(operationID, partnerType, requestTimestamp, fiscalCode);
                System.out.println("RISULTATO: " + redemptionResult.getStatusCode());
            }
            catch (Exception ex) {
                return createMessageError(requestId, userTransaction, "Error requesting info about redemption: " + ex.getMessage(), ResponseHelper.USER_INFO_REDEMPTION_ERROR);
            }
            
            if (!redemptionResult.getStatusCode().equals(FidelityResponse.INFO_REDEMPTION_OK)) {
                
                if (redemptionResult.getStatusCode().equals(FidelityResponse.INFO_REDEMPTION_INVALID_BALANCE)) {
                    return createMessageError(requestId, userTransaction,
                            "Unable to get redemption: " + redemptionResult.getMessageCode() + " (" + redemptionResult.getStatusCode() + ")",
                            ResponseHelper.USER_INFO_REDEMPTION_INVALID_BALANCE);
                }
                
                if (redemptionResult.getStatusCode().equals(FidelityResponse.INFO_REDEMPTION_CUSTOMER_BLOCKED)) {
                    return createMessageError(requestId, userTransaction,
                            "Unable to get redemption: " + redemptionResult.getMessageCode() + " (" + redemptionResult.getStatusCode() + ")",
                            ResponseHelper.USER_INFO_REDEMPTION_CUSTOMER_BLOCKED);
                }

                if (redemptionResult.getStatusCode().equals(FidelityResponse.INFO_REDEMPTION_INVALID_FISCAL_CODE)) {
                    return createMessageError(requestId, userTransaction,
                            "Unable to get redemption: " + redemptionResult.getMessageCode() + " (" + redemptionResult.getStatusCode() + ")",
                            ResponseHelper.USER_INFO_REDEMPTION_INVALID_FISCAL_CODE);
                }
                
                return createMessageError(requestId, userTransaction, "Unable to get redemption: " + redemptionResult.getMessageCode() + " (" + redemptionResult.getStatusCode()
                        + ")", ResponseHelper.USER_INFO_REDEMPTION_GENERIC_ERROR);
            }

            InfoRedemptionResponse infoRedemptionResponse = new InfoRedemptionResponse();
            infoRedemptionResponse.setStatusCode(ResponseHelper.USER_INFO_REDEMPTION_SUCCESS);
            infoRedemptionResponse.setRedemptions(redemptionResult.getRedemptionList());

            userTransaction.commit();

            return infoRedemptionResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get active vouchers with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private InfoRedemptionResponse createMessageError(String requestId, UserTransaction userTransaction, String message, String statusCode) throws Exception {

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, message);

        userTransaction.commit();

        InfoRedemptionResponse infoRedemptionResponse = new InfoRedemptionResponse();
        infoRedemptionResponse.setStatusCode(statusCode);

        return infoRedemptionResponse;
    }

}
