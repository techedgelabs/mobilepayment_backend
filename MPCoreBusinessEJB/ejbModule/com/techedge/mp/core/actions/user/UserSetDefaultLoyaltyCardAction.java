package com.techedge.mp.core.actions.user;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnableLoyaltyCardResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserSetDefaultLoyaltyCardAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserSetDefaultLoyaltyCardAction() {}

    public String execute(String ticketId, String requestId, String panCode, String eanCode, FidelityServiceRemote fidelityService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_SET_DEFAULT_LOYALTY_CARD_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_SET_DEFAULT_LOYALTY_CARD_INVALID_TICKET;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to load voucher in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_SET_DEFAULT_LOYALTY_CARD_UNAUTHORIZED;
            }

            // TODO - Controlla se la carta � gi� associata a un altro utente - necessario?

            Date now = new Date();

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
            Boolean enable = true;
            PartnerType partnerType = PartnerType.MP;
            Long requestTimestamp = now.getTime();

            EnableLoyaltyCardResult enableLoyaltyCardResult = new EnableLoyaltyCardResult();

            try {

                enableLoyaltyCardResult = fidelityService.enableLoyaltyCard(operationID, fiscalCode, panCode, enable, partnerType, requestTimestamp);
            }
            catch (Exception e) {

                // Errore nell'associazione della carta loyalty
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error enabling loyalty card");

                userTransaction.commit();

                return ResponseHelper.USER_SET_DEFAULT_LOYALTY_CARD_FAILURE;
            }

            // Verifica l'esito del check
            String enableLoyaltyCardStatusCode = enableLoyaltyCardResult.getStatusCode();

            if (!enableLoyaltyCardStatusCode.equals(FidelityResponse.ENABLE_LOYALTY_CARD_OK)) {

                String responseError = ResponseHelper.USER_SET_DEFAULT_LOYALTY_CARD_FAILURE;
                
                if (enableLoyaltyCardStatusCode.equals(FidelityResponse.ENABLE_LOYALTY_CARD_MIGRATION_ERROR)) {
                    responseError = ResponseHelper.USER_SET_DEFAULT_LOYALTY_CARD_MIGRATION_ERROR;
                }

                if (enableLoyaltyCardStatusCode.equals(FidelityResponse.ENABLE_LOYALTY_CARD_MIGRATION_ONLINE_ERROR)) {
                    responseError = ResponseHelper.USER_SET_DEFAULT_LOYALTY_CARD_MIGRATION_ONLINE_ERROR;
                }

                // Error
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error enabling loyalty card: " + enableLoyaltyCardStatusCode);

                userTransaction.commit();

                return responseError;
            }
            else {
                
                // Se l'utente ha gi� una carta loyalty elimina l'associazione

                // Associa la nuova carta loyalty all'utente
                LoyaltyCardBean loyaltyCardBean = userBean.getFirstLoyaltyCard();
                
                if (loyaltyCardBean == null)
                {
                    loyaltyCardBean = new LoyaltyCardBean();
                    loyaltyCardBean.setPanCode(panCode);
                    
                    if (eanCode != null) {
                        loyaltyCardBean.setEanCode(eanCode);
                    }
                    loyaltyCardBean.setStatus(LoyaltyCard.LOYALTY_CARD_STATUS_VALIDA);

                    loyaltyCardBean.setUserBean(userBean);

                    em.persist(loyaltyCardBean);
                }
                else {
                    loyaltyCardBean.setPanCode(panCode);
                    
                    if (eanCode != null) {
                        loyaltyCardBean.setEanCode(eanCode);
                    }
                    loyaltyCardBean.setStatus(LoyaltyCard.LOYALTY_CARD_STATUS_VALIDA);

                    loyaltyCardBean.setUserBean(userBean);

                    em.merge(loyaltyCardBean);
                }

                userTransaction.commit();

                return ResponseHelper.USER_SET_DEFAULT_LOYALTY_CARD_SUCCESS;
            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED load voucher with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
