package com.techedge.mp.core.actions.user;

import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserUpdateEffectiveCapAction {

	@Resource
	private EJBContext context;

	@PersistenceContext( unitName = "CrudPU" )
	private EntityManager em;

	@EJB
	private LoggerService loggerService;


	public UserUpdateEffectiveCapAction() {
	}


	public String execute(
			Double amount,
			String shopTransactionID) throws EJBException {

		UserTransaction userTransaction = context.getUserTransaction();
		double epsilon = 0.0000001;
		try {
			userTransaction.begin();

			// Ricerca la transazione associata allo shopTransactionId
			TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, shopTransactionID);

			if ( transactionBean == null ) {

				// La transazione non esiste
				this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Transaction not found");

				userTransaction.commit();

				return ResponseHelper.UPDATE_EFFECTIVE_CAP_TRANSACTION_NOT_FOUND;
			}

			UserBean userBean = transactionBean.getUserBean();
			if ( userBean == null ) {

				// Non esiste nessun utente associato alla transazione
				this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "User not found");

				userTransaction.commit();

				return ResponseHelper.UPDATE_EFFECTIVE_CAP_USER_NOT_FOUND;
			}


			// Si ricavano il methodId e il methodType dalla transazione
			Long methodId = transactionBean.getPaymentMethodId();
			String methodType = transactionBean.getPaymentMethodType();

			PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(methodId, methodType);
			if ( paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED ) {

				if ( paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED ) {

					// Lo stato del metodo di pagamento selezionato � gi� stato validato
					this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Payment method status valid: no need to update effective cap");

					userTransaction.commit();

					return ResponseHelper.UPDATE_EFFECTIVE_CAP_SUCCESS;
				}
				else {

					// Lo stato del metodo di pagamento selezionato non risulta valido
					this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Payment method status not valid: " + paymentInfoBean.getStatus());

					userTransaction.commit();

					return ResponseHelper.UPDATE_EFFECTIVE_CAP_INVALID_PAYMENT_STATUS;
				}
			}
			else {

				// Il metodo di pagamento selezionato non � ancora stato verificato
				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Payment method is not verified");

				// Verifica lo stato della transazione
				Integer lastSequenceID = 0;
				TransactionStatusBean lastTransactionStatusBean = null;
				Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
				for( TransactionStatusBean transactionStatusBean : transactionStatusData ) {
					Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
					if ( lastSequenceID < transactionStatusBeanSequenceID ) {
						lastSequenceID = transactionStatusBeanSequenceID;
						lastTransactionStatusBean = transactionStatusBean;
					}
				}

				if ( !lastTransactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_CLOSED) ) {

					// La transazione non si trova in uno stato valido
					this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Transaction status not valid: " + lastTransactionStatusBean.getStatus());

					userTransaction.commit();

					return ResponseHelper.UPDATE_EFFECTIVE_CAP_WRONG_TRANSACTION_STATUS;
				}


				// Bisogna verificare che l'utente abbia un cap disponibile sufficiente per l'operazione
				if ( userBean.getCapEffective() < ( amount - epsilon) ) {

					// Il cap disponibile � inferiore a quello richiesto per il rifornimento
					this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Effective cap insufficient. Requested: " + amount + " Available: " + userBean.getCapEffective());

					userTransaction.commit();

					return ResponseHelper.UPDATE_EFFECTIVE_CAP_INSUFFICIENT_CAP;
				}
				else {

					this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Initial amount: " + transactionBean.getInitialAmount());
					this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Final amount: " + transactionBean.getFinalAmount());

					// L'utente possiede un cap disponibile superiore a quello richiesto per il rifornimento
					this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Sufficient available cap found. Requested: " + amount + " Available: " + userBean.getCapEffective());

					Double availableCap = userBean.getCapAvailable();
					Double effectiveCap = userBean.getCapEffective();

					if ( transactionBean.getFinalAmount() < transactionBean.getInitialAmount() ) {

						this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Final amount != Initial amount");

						Double refundAmount = transactionBean.getInitialAmount() - transactionBean.getFinalAmount();

						effectiveCap = effectiveCap - amount;
						availableCap = availableCap + refundAmount;

						if( availableCap < 0.0){
							userBean.setCapAvailable(0.0);
						}
						else{
							userBean.setCapAvailable(availableCap);
						}
						
						if(effectiveCap <0.0){
							userBean.setCapEffective(0.0);
						}
						else{
							userBean.setCapEffective(effectiveCap);	
						}


						em.merge(userBean);

						userTransaction.commit();

						return ResponseHelper.UPDATE_EFFECTIVE_CAP_SUCCESS;
					}
					else {

						if ( transactionBean.getFinalAmount() > transactionBean.getInitialAmount() ) {

							this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Final amount > Initial amount");

							userTransaction.commit();

							return ResponseHelper.UPDATE_EFFECTIVE_CAP_FINAL_AMOUNT_NOT_VALID;
						}
						else {

							this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Final amount == Initial amount");

							effectiveCap = effectiveCap - amount;

							userBean.setCapEffective(effectiveCap);

							em.merge(userBean);

							userTransaction.commit();

							return ResponseHelper.UPDATE_EFFECTIVE_CAP_SUCCESS;
						}

					}
				}
			}
		}
		catch (Exception ex2) {

			try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String message = "FAILED updating effective cap with message (" + ex2.getMessage() + ")";
			this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, message );

			throw new EJBException(ex2);
		}
	}
}
