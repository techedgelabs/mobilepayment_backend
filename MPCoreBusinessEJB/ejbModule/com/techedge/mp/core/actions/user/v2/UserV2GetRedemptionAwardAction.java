package com.techedge.mp.core.actions.user.v2;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AwardDetailData;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.RedemptionAwardResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.interfaces.AwardDetail;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCatalogResult;
import com.techedge.mp.dwh.adapter.interfaces.DwhGetAwardResult;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2GetRedemptionAwardAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;
    
    private static final String BRAND_ENI_GAS_E_LUCE = "Eni gas e luce";

    public UserV2GetRedemptionAwardAction() {}

    public RedemptionAwardResponse execute(String ticketId, String requestId, String awardId, String cardPartner, String pin) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        RedemptionAwardResponse redemptionAwardResponse = new RedemptionAwardResponse();
        
        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid()) { // || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                redemptionAwardResponse.setStatusCode(ResponseHelper.USER_V2_GET_REDEMPTION_AWARD_LIST_INVALID_TICKET);
                return redemptionAwardResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                redemptionAwardResponse.setStatusCode(ResponseHelper.USER_V2_GET_REDEMPTION_AWARD_LIST_INVALID_TICKET);
                userTransaction.commit();
                return redemptionAwardResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to retrieve redemptionAward, user  in status "
                        + userStatus);

                redemptionAwardResponse.setStatusCode(ResponseHelper.USER_V2_GET_REDEMPTION_AWARD_LIST_UNAUTHORIZED);

                userTransaction.commit();

                return redemptionAwardResponse;
            }

            //verifica pin
            if (pin == null) {
                return createMessageError(requestId, userTransaction, "PIN null", ResponseHelper.USER_REDEMPTION_INVALID_REDEMPTION_CODE, null);
            }

            if (userBean.getPaymentData() == null && userBean.getPaymentData().isEmpty()) {
                return createMessageError(requestId, userTransaction, "User not payment method", ResponseHelper.USER_REDEMPTION_FAILURE, null);
            }

            PaymentInfoBean voucherPayment = userBean.getVoucherPaymentMethod();

            if (voucherPayment == null) {
                return createMessageError(requestId, userTransaction, "Voucher Payment null", ResponseHelper.USER_REDEMPTION_ERROR_PIN_NOT_FOUND, null);
            }

            if (voucherPayment.getPin() == null) {
                return createMessageError(requestId, userTransaction, "PIN null", ResponseHelper.USER_REDEMPTION_ERROR_PIN_NOT_FOUND, null);
            }

            if (voucherPayment.getPinCheckAttemptsLeft() == 0) {
                voucherPayment.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

                // e tutti i metodi di pagamento verificati e non verificati sono cancellati
                for (PaymentInfoBean paymentInfo : userBean.getPaymentData()) {

                    if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                            && (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                        paymentInfo.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                        paymentInfo.setDefaultMethod(false);
                        em.merge(paymentInfo);
                    }
                }

                return createMessageError(requestId, userTransaction, "PinCheckAttemptsLeft = 0", ResponseHelper.USER_REDEMPTION_ERROR_PIN_ATTEMPTS_LEFT, 0);
            }

            if (!voucherPayment.getPin().equals(pin)) {

                Integer pinCheckAttemptsLeft = voucherPayment.getPinCheckAttemptsLeft() - 1;

                voucherPayment.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

                if (pinCheckAttemptsLeft == 0) {

                    // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                    voucherPayment.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

                    // e tutti i metodi di pagamento verificati e non verificati sono cancellati
                    for (PaymentInfoBean paymentInfo : userBean.getPaymentData()) {

                        if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                                && (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                            paymentInfo.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                            paymentInfo.setDefaultMethod(false);
                            em.merge(paymentInfo);
                        }
                    }

                    return createMessageError(requestId, userTransaction, "PinCheckAttemptsLeft = 0", ResponseHelper.USER_REDEMPTION_ERROR_PIN_ATTEMPTS_LEFT, 0);
                }
                else {

                    if (pinCheckAttemptsLeft == 1) {

                        return createMessageError(requestId, userTransaction, "PIN not valid", ResponseHelper.USER_REDEMPTION_ERROR_PIN_LAST_ATTEMPT, pinCheckAttemptsLeft);
                    }
                    else {

                        return createMessageError(requestId, userTransaction, "PIN not valid", ResponseHelper.USER_REDEMPTION_ERROR_PIN, pinCheckAttemptsLeft);
                    }
                }
            }
            
            // verifica pin
            
            Date now = new Date();
            Long requestTimestamp = now.getTime();
            
            LoyaltyCardBean loyaltyCardBean = userBean.getVirtualLoyaltyCard();
            
            if(loyaltyCardBean != null) {
                
                DWHAdapterServiceRemote dwhAdapterService = EJBHomeCache.getInstance().getDWHAdapterService();
                
                // Controllo valorizzazione cardPartner per Eni Gas&Luce (gestione retrocompatibilit�)
                if (cardPartner == null || cardPartner.isEmpty()) {
                    
                    DWHGetCatalogResult dwhGetCatalogResult = dwhAdapterService.getCatalog(loyaltyCardBean.getEanCode(), requestId, requestTimestamp);
                    
                    List<AwardDetail> awardDetailList = dwhGetCatalogResult.getAwardDetailList();
                    
                    if (awardDetailList == null || awardDetailList.isEmpty()) {

                        // Partner list vuota
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Award list is empty !");

                        userTransaction.commit();

                        redemptionAwardResponse.setStatusCode(ResponseHelper.USER_V2_GET_REDEMPTION_AWARD_LIST_FAILURE);
                        return redemptionAwardResponse;
                    }
                    else {

                        for (AwardDetail awardDetail : awardDetailList) {
                            
                            //System.out.println("awardId: " + awardDetail.getAwardId());
                            //System.out.println("input:   " + awardId);
                            
                            if (awardDetail.getAwardId().equals(Integer.parseInt(awardId))) {
                                
                                System.out.println("brandId: " + awardDetail.getBrandId());
                                System.out.println("brand:   " + awardDetail.getBrand());
                                
                                if (awardDetail.getBrand().equals(BRAND_ENI_GAS_E_LUCE)) {
                                    
                                    // Per il brand Eni Gas & Luce il cardPartner � obbligatorio
                                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "cardPartner empty for partner Eni G&L");

                                    userTransaction.commit();

                                    redemptionAwardResponse.setStatusCode(ResponseHelper.USER_V2_GET_REDEMPTION_AWARD_LIST_UPDATE_APP);
                                    return redemptionAwardResponse;
                                }
                                
                                break;
                            }
                        }
                    }
                }
                
                try {
                    DwhGetAwardResult dWHGetAwardResult = dwhAdapterService.getAwardResult(Integer.parseInt(awardId), loyaltyCardBean.getEanCode(), cardPartner, requestId, requestTimestamp);
                    
                    redemptionAwardResponse.setOrderId(dWHGetAwardResult.getOrderId());
                    redemptionAwardResponse.setVoucher(dWHGetAwardResult.getVoucher());
                    redemptionAwardResponse.setMessageCode(dWHGetAwardResult.getMessageCode());
                    redemptionAwardResponse.setStatusCode(dWHGetAwardResult.getStatusCode());
                }
                catch (Exception exc) {

                    System.out.println("Error loading loyalty credits: " + exc.getMessage());
                    
                    redemptionAwardResponse.setStatusCode(StatusHelper.LOYALTY_STATUS_TO_BE_VERIFIED);
                    redemptionAwardResponse.setMessageCode("CARICAMENTO DA VERIFICARE (" + exc.getMessage() + ")");
                    
                    String errorDescription = "999 Error loading loyalty credits (" + exc.getMessage() + ")";
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,errorDescription);
                    userTransaction.commit();
                    return redemptionAwardResponse;
                }
            }

            userTransaction.commit();
            return redemptionAwardResponse;
            
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            String message = "FAILED get landing  with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);
            
            throw new EJBException(ex2);
        }
    }

    private RedemptionAwardResponse createMessageError(String requestId, UserTransaction userTransaction, String message, String statusCode, Integer attemptsLeft) throws Exception {

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, message);

        userTransaction.commit();

        RedemptionAwardResponse redemptionAwardResponse = new RedemptionAwardResponse();
        redemptionAwardResponse.setStatusCode(statusCode);

        if (attemptsLeft != null) {
            redemptionAwardResponse.setCheckPinAttemptsLeft(attemptsLeft);
        }

        return redemptionAwardResponse;
    }
}
