package com.techedge.mp.core.actions.user.v2;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AwardDetailData;
import com.techedge.mp.core.business.interfaces.AwardDetailDataResponse;
import com.techedge.mp.core.business.interfaces.BrandDataDetail;
import com.techedge.mp.core.business.interfaces.CategoryBurnDataDetail;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.dwh.DWHBrandDetailBean;
import com.techedge.mp.core.business.model.dwh.DWHCategoryBurnDetailBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.interfaces.AwardDetail;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCatalogResult;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CategoryRedemptionDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.InfoRedemptionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2GetAwardListAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2GetAwardListAction() {}

    public AwardDetailDataResponse execute(String ticketId, String requestId, FidelityServiceRemote fidelityService, DWHAdapterServiceRemote dwhAdapterService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        AwardDetailDataResponse awardDetailDataResponse = new AwardDetailDataResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid()) { // || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                awardDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_AWARD_LIST_INVALID_TICKET);
                return awardDetailDataResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                
                awardDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_AWARD_LIST_INVALID_TICKET);
                return awardDetailDataResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to retrieve award list, user  in status " + userStatus);

                userTransaction.commit();
                
                awardDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_AWARD_LIST_UNAUTHORIZED);
                return awardDetailDataResponse;
            }
            
            // Ottieni la carta loyalty dell'utente
            LoyaltyCardBean loyaltyCardBean = userBean.getVirtualLoyaltyCard();
            if (loyaltyCardBean == null) {

                // L'utente non ha una carta dematerializzata associata
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Virtual card not found");

                awardDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_AWARD_LIST_UNAUTHORIZED);

                userTransaction.commit();

                return awardDetailDataResponse;
            }
            
            // Impostazione parametri per invocazione servizi remoti
            Date now = new Date();
            String card = loyaltyCardBean.getEanCode();
            Long requestTimestamp = now.getTime();
            
            
            // RedemptionList Info
            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            PartnerType partnerType = PartnerType.MP;

            InfoRedemptionResult redemptionResult = new InfoRedemptionResult();
            String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
            
            try {
                redemptionResult = fidelityService.infoRedemption(operationID, partnerType, requestTimestamp, fiscalCode);
                System.out.println("RISULTATO: " + redemptionResult.getStatusCode());
            }
            catch (Exception ex) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error requesting info about redemption: "+ ex.getMessage());        
                userTransaction.commit();
                awardDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_AWARD_LIST_FAILURE);
                return awardDetailDataResponse;       
            }
            
            if (!redemptionResult.getStatusCode().equals(FidelityResponse.INFO_REDEMPTION_OK)) {
                
                if (redemptionResult.getStatusCode().equals(FidelityResponse.INFO_REDEMPTION_INVALID_BALANCE)) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to get redemption: " + redemptionResult.getMessageCode() + " (" + redemptionResult.getStatusCode()+")");
                    userTransaction.commit();
                    awardDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_AWARD_LIST_INVALID_BALANCE);
                    return awardDetailDataResponse; 
                }
                
                if (redemptionResult.getStatusCode().equals(FidelityResponse.INFO_REDEMPTION_CUSTOMER_BLOCKED)) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,  "Unable to get redemption: " + redemptionResult.getMessageCode() + " (" + redemptionResult.getStatusCode() + ")");
                    userTransaction.commit();
                    awardDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_AWARD_LIST_CUSTOMER_BLOCKED);
                    return awardDetailDataResponse;
                }

                if (redemptionResult.getStatusCode().equals(FidelityResponse.INFO_REDEMPTION_INVALID_FISCAL_CODE)) {
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,  "Unable to get redemption: " + redemptionResult.getMessageCode() + " (" + redemptionResult.getStatusCode() + ")");
                    userTransaction.commit();
                    awardDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_AWARD_LIST_FISCAL_CODE);
                    return awardDetailDataResponse;
                }
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,  "Unable to get redemption: " + redemptionResult.getMessageCode() + " (" + redemptionResult.getStatusCode()+ ")");
                userTransaction.commit();
                awardDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_AWARD_LIST_GENERIC_ERROR);
                return awardDetailDataResponse;
            }


            List<DWHBrandDetailBean> dwhBrandDetailList = QueryRepository.findAllDwhBrandDetailBean(em);
            List<DWHCategoryBurnDetailBean> dwhCategoryBurnDetailList = QueryRepository.findAllDwhCategoryBurnDetailBean(em);
            //List<DWHAwardDetailBean> dwhAwardDetailList = QueryRepository.findAllDwhAwardDetail(em);
            
            // Chiamata al DWH per recuperare la lista aggiornata dei premi
            
            DWHGetCatalogResult dwhGetCatalogResult = dwhAdapterService.getCatalog(card, requestId, requestTimestamp);
            
            List<AwardDetail> awardDetailList = dwhGetCatalogResult.getAwardDetailList();
            
            if (awardDetailList == null || awardDetailList.isEmpty()) {

                // Partner list vuota
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Award list is empty !");

                //awardResponse.setStatusCode(ResponseHelper.USER_V2_GET_AWARD_LIST_NOTFOUND);
                //return awardResponse;
            }
            else{

                for (AwardDetail awardDetail : awardDetailList) {

                    AwardDetailData awardDataElement = new AwardDetailData();

                    awardDataElement.setAwardId(awardDetail.getAwardId());
                    awardDataElement.setBrandId(awardDetail.getBrandId());
                    awardDataElement.setBrand(awardDetail.getBrand());
                    awardDataElement.setCancelable(awardDetail.getCancelable());
                    awardDataElement.setCategoryId(awardDetail.getCategoryId());
                    awardDataElement.setCategory(awardDetail.getCategory());
                    awardDataElement.setContribution(awardDetail.getContribution());
                    awardDataElement.setDateStartRedemption(awardDetail.getDateStartRedemption());
                    awardDataElement.setDateStopRedemption(awardDetail.getDateStopRedemption());
                    awardDataElement.setDescription(awardDetail.getDescription());
                    awardDataElement.setDescriptionShort(awardDetail.getDescriptionShort());
                    awardDataElement.setPoint(awardDetail.getPoint());
                    awardDataElement.setValue(awardDetail.getValue());
                    awardDataElement.setRedeemable(awardDetail.getRedeemable());
                    awardDataElement.setType(awardDetail.getType());
                    awardDataElement.setTangible(awardDetail.getTangible());
                    awardDataElement.setUrlImageAward(awardDetail.getUrlImageAward());
                    awardDataElement.setUrlImageBrand(awardDetail.getUrlImageBrand());
                    awardDataElement.setPointPartner(awardDetail.getPointPartner());
                    awardDataElement.setChannelRedemption(awardDetail.getChannelRedemption());
                    awardDataElement.setTypePointPartner(awardDetail.getTypePointPartner());
                    
                    awardDetailDataResponse.getAwardDetailList().add(awardDataElement);
                }
            }
            
            if (dwhBrandDetailList == null || dwhBrandDetailList.isEmpty()) {

                // Partner list vuota
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Brand list is empty !");

                //userTransaction.commit();

                //awardResponse.setStatusCode(ResponseHelper.USER_V2_GET_BRAND_LIST_NOTFOUND);
                //return awardResponse;
            }
            else{
                for (DWHBrandDetailBean dwhBrandDetailBean : dwhBrandDetailList) {
                    BrandDataDetail brandDataDetailElement = new BrandDataDetail();

                    brandDataDetailElement.setBrandId(dwhBrandDetailBean.getBrandId());
                    brandDataDetailElement.setBrand(dwhBrandDetailBean.getBrand());
                    brandDataDetailElement.setBrandUrl(dwhBrandDetailBean.getBrandUrl());

                    awardDetailDataResponse.getBrandDataDetailList().add(brandDataDetailElement);
                }
            }
            
            if (dwhCategoryBurnDetailList == null || dwhCategoryBurnDetailList.isEmpty()) {

                // Partner list vuota
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Category burn list is empty !");

                //userTransaction.commit();

                //awardResponse.setStatusCode(ResponseHelper.USER_V2_GET_CATEGORY_LIST_NOTFOUND);
                //return awardResponse;
            }
            else{
                for (DWHCategoryBurnDetailBean dwhCategoryBurnDetailBean : dwhCategoryBurnDetailList) {
                    CategoryBurnDataDetail categoryBurnDataDetailElement = new CategoryBurnDataDetail();

                    categoryBurnDataDetailElement.setCategoryId(dwhCategoryBurnDetailBean.getCategoryId());
                    categoryBurnDataDetailElement.setCategory(dwhCategoryBurnDetailBean.getCategory());
                    categoryBurnDataDetailElement.setCategoryImageUrl(dwhCategoryBurnDetailBean.getCategoryImageUrl());
                    
                    awardDetailDataResponse.getCategoryBurnDataDetailList().add(categoryBurnDataDetailElement);
                }
            }

            /* Modifica annullata
            // Il servizio deve restituire solo il voucher carburante con saldo punti maggiore escludendo quello con saldo a 0
            RedemptionDetail redemptionDetailMax = null;
            Integer maxPoints = 0;
            
            List<RedemptionDetail> redemptionDetailList = redemptionResult.getRedemptionList();
            for(RedemptionDetail redemptionDetail : redemptionDetailList) {
                Integer points = redemptionDetail.getPoints();
                if(points > 0 && points > maxPoints) {
                    maxPoints = points;
                    redemptionDetailMax = redemptionDetail;
                }
            }
            
            if (redemptionDetailMax != null) {
                awardDetailDataResponse.getRedemptionDetailList().add(redemptionDetailMax);
            }
            */
            
            List<RedemptionDetail> redemptionDetailList = redemptionResult.getRedemptionList();
            for(RedemptionDetail redemptionDetail : redemptionDetailList) {
                awardDetailDataResponse.getRedemptionDetailList().add(redemptionDetail);
            }
            
            List<CategoryRedemptionDetail> categoryRedemptionDetailList = redemptionResult.getCategoryRedemptionList();
            if (categoryRedemptionDetailList != null && !categoryRedemptionDetailList.isEmpty()) {
                for(CategoryRedemptionDetail categoryRedemptionDetail : categoryRedemptionDetailList) {
                    if (categoryRedemptionDetail.getCategoryCode().equals("SOSTA")) {
                        for(RedemptionDetail redemptionDetail : categoryRedemptionDetail.getRedemptionList()) {
                            System.out.println("Trovato taglio voucher sosta " + redemptionDetail.getCode());
                            awardDetailDataResponse.getParkingRedemptionDetailList().add(redemptionDetail);
                        }
                    }
                }
            }
            
            awardDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_AWARD_LIST_SUCCESS);

            userTransaction.commit();

            return awardDetailDataResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get awards  with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
