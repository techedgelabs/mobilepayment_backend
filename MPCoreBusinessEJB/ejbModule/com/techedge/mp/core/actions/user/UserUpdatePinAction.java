package com.techedge.mp.core.actions.user;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.exceptions.PaymentInfoNotFoundException;
import com.techedge.mp.core.business.exceptions.PinException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.interfaces.user.UserUpdatePinResponse;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.PinHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserUpdatePinAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserUpdatePinAction() {
    }
    
    
    public UserUpdatePinResponse execute(
    		String ticketId,
    		String requestId,
    		Long cardId,
    		String cardType,
    		String oldPin,
			String newPin,
//			Double maxCheckAmount,
//			Integer pinMaxAttempts,
			Integer pinCheckMaxAttempts,
			UserCategoryService userCategoryService) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	UserUpdatePinResponse userUpdatePinResponse = new UserUpdatePinResponse();
    	
    	
    	try {
    		userTransaction.begin();
    		
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket" );
				
    			userTransaction.commit();
    			userUpdatePinResponse.setStatusCode(ResponseHelper.USER_PIN_INVALID_TICKET);
    			return  userUpdatePinResponse; //ResponseHelper.USER_PIN_INVALID_TICKET;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if ( userBean == null ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found" );
				
    			userTransaction.commit();
    			
    			userUpdatePinResponse.setStatusCode(ResponseHelper.USER_PIN_INVALID_TICKET);
    			return  userUpdatePinResponse; //ResponseHelper.USER_PIN_INVALID_TICKET;
    		}
    		
    		
    		// Verifica lo stato dell'utente
    		Integer userStatus = userBean.getUserStatus();
    		if ( userStatus != User.USER_STATUS_VERIFIED ) {
    			
    			// Un utente che si trova in questo stato non pu� invocare questo servizio
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update user pin in status " + userStatus );
				
    			userTransaction.commit();
    			
    			userUpdatePinResponse.setStatusCode(ResponseHelper.USER_PIN_UNAUTHORIZED);
    			return  userUpdatePinResponse; //ResponseHelper.USER_PIN_INVALID_TICKET;
    			
    		}
    		
    		
    		if ( oldPin == null ) {
    			
    			// Per modificare il pin � obbligatorio indicare il vecchio pin
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Old pin not present" );
				
    			userTransaction.commit();
    			
    			userUpdatePinResponse.setStatusCode(ResponseHelper.USER_PIN_OLD_DB_LESS);
    			return  userUpdatePinResponse;
    			
    		}
    		    		
			String checkPin = PinHelper.checkPin(newPin, null);
			if (!checkPin.equals("OK")){
				
				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Old pin not present" );
				
    			userTransaction.commit();
    			userUpdatePinResponse.setStatusCode(ResponseHelper.USER_PIN_NOT_STRONG);
    			return  userUpdatePinResponse;
				
			}
    		
			
			// Switch implementazione per utenti che eseguono il nuovo flusso voucher
            
            Integer userType = userBean.getUserType();
            Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());
            Boolean useNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());
            
            if (useNewFlow || useNewAcquirerFlow || useBusiness) {
			    
			    System.out.println("Utilizzo nuovo flusso voucher o flusso business");
			    
			    PaymentInfoBean voucherPaymentInfoBean = userBean.getVoucherPaymentMethod();
			    
			    if (voucherPaymentInfoBean == null) {
			        
			        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Old pin not present" );
	                
	                userTransaction.commit();
	                userUpdatePinResponse.setStatusCode(ResponseHelper.USER_PIN_UNAUTHORIZED);
	                return  userUpdatePinResponse;
			    }
			    
			    try {
			        userBean.updatePinNew(voucherPaymentInfoBean, oldPin, newPin, pinCheckMaxAttempts);
			    }
			    catch (PaymentInfoNotFoundException e) {
                    
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, e.getMessage() );
                    
                    userTransaction.commit();
                    
                    userUpdatePinResponse.setStatusCode(ResponseHelper.USER_PIN_METHOD_NOT_FOUND);
                    return  userUpdatePinResponse;
                    
                }
                catch (PinException e) {
                    
                    System.out.println( "PinException message: " + e.getStatusMessage() );
                    
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, e.getStatusMessage() );
                    
                    // Aggiorna i dati dell'utente
                    em.merge(userBean);
                    
                    userTransaction.commit();
                    
                    userUpdatePinResponse.setStatusCode(e.getStatusCode());
                    userUpdatePinResponse.setPinCheckMaxAttempts(e.getPinCheckAttemptsLeft());
                    return  userUpdatePinResponse;
                    
                }
			    
			    
			    // Aggiorna i dati dell'utente
                em.merge(userBean);
                
                // Rinnova il ticket
                ticketBean.renew();
                em.merge(ticketBean);
                
                userTransaction.commit();
                
                userUpdatePinResponse.setStatusCode(ResponseHelper.USER_PIN_SUCCESS);
                if(voucherPaymentInfoBean != null){
                    userUpdatePinResponse.setPinCheckMaxAttempts(voucherPaymentInfoBean.getPinCheckAttemptsLeft());
                }
                return  userUpdatePinResponse;
			}
			else {
			    
			    System.out.println("Utilizzo vecchio flusso voucher");
			    
			    // Controlli spostati dall'adapter al core
			    if(cardId == null || cardId.equals("") || cardType == null || cardType.equals("")) {
		            
		            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Old pin not present" );
	                
	                userTransaction.commit();
	                userUpdatePinResponse.setStatusCode(ResponseHelper.USER_PIN_METHOD_NOT_FOUND);
	                return  userUpdatePinResponse;
		        }
			    
    			PaymentInfoBean paymentInfoBean;
        			
    			try {				
    				 paymentInfoBean = userBean.updatePin(cardId, cardType, oldPin, newPin, pinCheckMaxAttempts);
    			}
    			catch (PaymentInfoNotFoundException e) {
    				
    				this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, e.getMessage() );
    				
    				userTransaction.commit();
    				
    				userUpdatePinResponse.setStatusCode(ResponseHelper.USER_PIN_METHOD_NOT_FOUND);
        			return  userUpdatePinResponse;
    				
    			}
    			catch (PinException e) {
    				
    				System.out.println( "PinException message: " + e.getStatusMessage() );
    				
    				this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, e.getStatusMessage() );
    				
    				// Aggiorna i dati dell'utente
    	    		em.merge(userBean);
    	    		
    				userTransaction.commit();
    				
    				userUpdatePinResponse.setStatusCode(e.getStatusCode());
    				userUpdatePinResponse.setPinCheckMaxAttempts(e.getPinCheckAttemptsLeft());
        			return  userUpdatePinResponse;
    				
    			}
    			
        		
        		// Aggiorna i dati dell'utente
        		em.merge(userBean);
        		
        		// Rinnova il ticket
        		ticketBean.renew();
        		em.merge(ticketBean);
        		
        		userTransaction.commit();
        		
        		userUpdatePinResponse.setStatusCode(ResponseHelper.USER_PIN_SUCCESS);
        		if(paymentInfoBean != null){
        			userUpdatePinResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
        		}
    			return  userUpdatePinResponse;
			}
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED pin update with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
