package com.techedge.mp.core.actions.user.v2;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.VirtualizationAttemptsLeftActionResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2VirtualizationAttemptsLeftActionAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2VirtualizationAttemptsLeftActionAction() {}

    public VirtualizationAttemptsLeftActionResponse execute(String ticketId, String requestId, String action, Integer defaultVirtualizationAttmeptsLeft) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
                virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_INVALID_TICKET);

                return virtualizationAttemptsLeftActionResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
                virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_INVALID_TICKET);

                return virtualizationAttemptsLeftActionResponse;
            }
            
            if (userBean.getUserType().equals(User.USER_TYPE_GUEST)) {

                // UserType non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid userType: " + userBean.getUserType());

                userTransaction.commit();

                VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
                virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_UNAUTHORIZED);

                return virtualizationAttemptsLeftActionResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update user fields in status " + userStatus);

                userTransaction.commit();

                VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
                virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_INVALID_TICKET);

                return virtualizationAttemptsLeftActionResponse;
            }

            if (!userBean.getDepositCardStepCompleted()) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User hasn't complete deposit card step");

                userTransaction.commit();

                VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
                virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_INVALID_TICKET);

                return virtualizationAttemptsLeftActionResponse;
            }

            if (userBean.getVirtualizationAttemptsLeft() == null) {
                
                System.out.println("VirtualizationAttemptsLeft null -> impostato a " + defaultVirtualizationAttmeptsLeft);
                
                // Impostazione del numero di tentativi di virtualizzazione al valore massimo per i vecchi utenti che stanno facendo la migrazione
                userBean.setVirtualizationAttemptsLeft(defaultVirtualizationAttmeptsLeft);
            }

            Integer virtualizationAttemptsLeft = userBean.getVirtualizationAttemptsLeft();
            if (action.equals(VirtualizationActionEnum.RETRIEVE.getValue())) {

                userTransaction.commit();

                VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
                virtualizationAttemptsLeftActionResponse.setAttemptsLeft(virtualizationAttemptsLeft);
                virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_SUCCESS);

                return virtualizationAttemptsLeftActionResponse;
            }
            else if (action.equals(VirtualizationActionEnum.DECREMENT.getValue())) {

                if (virtualizationAttemptsLeft == 0) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                            "virtualizationAttemptsLeft = 0, there are no attempts left");

                    userTransaction.commit();

                    VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
                    virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_NOATTEMPTS);

                    return virtualizationAttemptsLeftActionResponse;
                }

                virtualizationAttemptsLeft--;
                userBean.setVirtualizationAttemptsLeft(virtualizationAttemptsLeft);
                if (userBean.getVirtualizationAttemptsLeft() == 0) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                            "virtualizationAttemptsLeft 1 -> 0, there are no more attempts left");

                    userTransaction.commit();

                    VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
                    virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_NOMOREATTEMPTS);

                    return virtualizationAttemptsLeftActionResponse;
                }

                userTransaction.commit();

                VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
                virtualizationAttemptsLeftActionResponse.setAttemptsLeft(virtualizationAttemptsLeft);
                virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_SUCCESS);

                return virtualizationAttemptsLeftActionResponse;
            }
            else {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid action");

                userTransaction.commit();

                VirtualizationAttemptsLeftActionResponse virtualizationAttemptsLeftActionResponse = new VirtualizationAttemptsLeftActionResponse();
                virtualizationAttemptsLeftActionResponse.setStatusCode(ResponseHelper.USER_V2_VIRTUALIZATIONATTEMPTSLEFTACTION_FAILURE);

                return virtualizationAttemptsLeftActionResponse;
            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                System.err.println("IllegalStateException: " + e.getMessage());
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                System.err.println("SecurityException: " + e.getMessage());
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                System.err.println("SystemException: " + e.getMessage());
            }

            String message = "FAILED authentication with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    public enum VirtualizationActionEnum {
        RETRIEVE("RETRIEVE"), DECREMENT("DECREMENT");

        private String value;

        private VirtualizationActionEnum(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

    }
}
