package com.techedge.mp.core.actions.user.v2;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AppLink;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MissionDetailDataResponse;
import com.techedge.mp.core.business.interfaces.MissionInfoDataDetail;
import com.techedge.mp.core.business.interfaces.ParameterNotification;
import com.techedge.mp.core.business.interfaces.PromotionInfoDataDetail;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.GetOffersResult;
import com.techedge.mp.core.business.interfaces.crm.OfferSF;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfPromotionsBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2GetMissionSfListAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2GetMissionSfListAction() {}

    public MissionDetailDataResponse execute(String ticketId, String requestId, CRMServiceRemote crmService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        MissionDetailDataResponse missionDetailDataResponse = new MissionDetailDataResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                missionDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_MISSION_LIST_INVALID_TICKET);

                userTransaction.commit();

                return missionDetailDataResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                missionDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_MISSION_LIST_INVALID_TICKET);

                userTransaction.commit();

                return missionDetailDataResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to retrieve mission list, user  in status "
                        + userStatus);

                missionDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_MISSION_LIST_UNAUTHORIZED);

                userTransaction.commit();

                return missionDetailDataResponse;
            }

            // Recupera la lista delle promozioni dal database
            List<CRMSfPromotionsBean> crmSfPromotionBeanList = QueryRepository.findAllCrmSfPromotions(em);

            for (CRMSfPromotionsBean crmSfPromotionsBean : crmSfPromotionBeanList) {

                PromotionInfoDataDetail promotionInfoDataDetail = new PromotionInfoDataDetail();

                AppLink appLinkPromotion = new AppLink();
                appLinkPromotion.setType(PushNotificationContentType.INTERNAL);
                appLinkPromotion.setLocation(crmSfPromotionsBean.getUrl());

                appLinkPromotion.getParameters().add(new ParameterNotification("parameter1", crmSfPromotionsBean.getParameter1()==null?"n/a":crmSfPromotionsBean.getParameter1()));
                appLinkPromotion.getParameters().add(new ParameterNotification("parameter2", crmSfPromotionsBean.getParameter2()==null?"n/a":crmSfPromotionsBean.getParameter2()));
                appLinkPromotion.getParameters().add(new ParameterNotification("parameter3", crmSfPromotionsBean.getParameter3()==null?"n/a":crmSfPromotionsBean.getParameter3()));
                appLinkPromotion.getParameters().add(new ParameterNotification("parameter4", crmSfPromotionsBean.getParameter4()==null?"0.0":crmSfPromotionsBean.getParameter4().toString()));
                
                //:TODO aggiunta inutile per risolvere bug android              
                /*appLinkPromotion.getParameters().add(new ParameterNotification("C_1", "n/a"));
                appLinkPromotion.getParameters().add(new ParameterNotification("C_2", "n/a"));
                appLinkPromotion.getParameters().add(new ParameterNotification("C_3", "0.0"));
                appLinkPromotion.getParameters().add(new ParameterNotification("C_4", "0.0"));
                appLinkPromotion.getParameters().add(new ParameterNotification("C_5", "n/a"));
                appLinkPromotion.getParameters().add(new ParameterNotification("C_6", "n/a"));
                appLinkPromotion.getParameters().add(new ParameterNotification("C_7", "n/a"));
                appLinkPromotion.getParameters().add(new ParameterNotification("C_8", "n/a"));
                appLinkPromotion.getParameters().add(new ParameterNotification("C_9", "n/a")); */

                promotionInfoDataDetail.setAppLinkInfo(appLinkPromotion);
                promotionInfoDataDetail.setCode(crmSfPromotionsBean.getOfferCode());
                promotionInfoDataDetail.setDescription("prova");
                promotionInfoDataDetail.setImageUrl(crmSfPromotionsBean.getBannerId());
                promotionInfoDataDetail.setName(crmSfPromotionsBean.getNome());
                promotionInfoDataDetail.setStartDate(crmSfPromotionsBean.getDataInizio());
                promotionInfoDataDetail.setEndDate(crmSfPromotionsBean.getDataFine());

                if(crmSfPromotionsBean.getBannerId().equalsIgnoreCase(""))
                	System.out.println("Promotion id:"+crmSfPromotionsBean.getId()+" getBannerId() is '' not added");
                else
                	missionDetailDataResponse.getPromotionList().add(promotionInfoDataDetail);
            }

            // Recupera la lista delle missioni dal CRM SF
            
        	boolean notificationFlag = false;
            boolean paymentCardFlag = false;
            String brand = null;
            ClusterType cluster = ClusterType.ENIPAY;

            if (userBean.getVirtualizationCompleted()) {
                cluster = ClusterType.YOUENI;
            }
        	
        	Date date = new Date();
            
            GetOffersResult getOffersResult=new GetOffersResult();
            
            String reqId = new IdGenerator().generateId(16).substring(0, 32);
            /*:TODO da capire requestId e se notificationFlag, paymentCardFlag, brand sono necessari ? */
            getOffersResult = crmService.getMissionsSf(reqId, userBean.getPersonalDataBean().getFiscalCode(), date, notificationFlag, paymentCardFlag, brand, cluster.getValue());
            
            if (getOffersResult.getSuccess()){
            	
            	for(OfferSF offer:getOffersResult.getOffers()){
            		
            		MissionInfoDataDetail missionInfoDataDetail = new MissionInfoDataDetail();

                    missionInfoDataDetail.setAppLink(new AppLink());

                    //HashMap<String, Object> offerParams = offer.getAdditionalAttributes();
                    if (offer.getDescription() != null) {
                        missionInfoDataDetail.setDescription(offer.getDescription());
                    }
                    
                    
                    //missionInfoDataDetail.getAppLink().getParameters().add(new ParameterNotification("UrlApp", offer.getUrl()));
                    
                    
                    //:TODO non so ???
                    //missionInfoDataDetail.getAppLink().getParameters().add(new ParameterNotification("textAPP", "" ));
                    
                    System.out.println("###parameters offerSF");
                    missionInfoDataDetail.getAppLink().getParameters().add(new ParameterNotification("parameter1", offer.getParameter1()==null?"":offer.getParameter1() ));
                    missionInfoDataDetail.getAppLink().getParameters().add(new ParameterNotification("parameter2", offer.getParameter2()==null?"":offer.getParameter2() ));
                    missionInfoDataDetail.getAppLink().getParameters().add(new ParameterNotification("paramater3", offer.getParameter3()==null?"":offer.getParameter3() ));
                    missionInfoDataDetail.getAppLink().getParameters().add(new ParameterNotification("paramater4", offer.getParameter4()==null?"":offer.getParameter4().toString()));
                    
                    
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        
                    missionInfoDataDetail.setEndDate(offer.getEndDate());
                    missionInfoDataDetail.setStartDate(offer.getStartDate());
                    
                    if (offer.getUrl()!=null && !offer.getUrl().isEmpty()) {
                        missionInfoDataDetail.getAppLink().setLocation(offer.getUrl().trim());
                        missionInfoDataDetail.getAppLink().setType(PushNotificationContentType.INTERNAL);
                    }
                    
                    if (offer.getNumExecutedSteps()!=null) {
                        Integer valueInteger = new BigDecimal(offer.getNumExecutedSteps()).intValue();
                        missionInfoDataDetail.setStepCompleted(valueInteger);
                    }
                    
                    if (offer.getNumTotalSteps()!=null) {
                        Integer valueInteger = new BigDecimal(offer.getNumTotalSteps()).intValue();
                        missionInfoDataDetail.setStepObjective(valueInteger);
                    }
                    
                    if (missionInfoDataDetail.getStepObjective()!=null&&missionInfoDataDetail.getStepObjective()>0)
                    	missionInfoDataDetail.setType("step");
                    else
                    	missionInfoDataDetail.setType("timed");
                    
                    missionInfoDataDetail.setName(offer.getName());
                    missionInfoDataDetail.setCode(offer.getOfferCode());

                    if (missionInfoDataDetail.getType() != null) {
                        
                        if (missionInfoDataDetail.getType().equalsIgnoreCase("step")) {
                            //If stepCompleted >= stepObjective ---> Completed Mission
                            if (missionInfoDataDetail.getStepCompleted() >= missionInfoDataDetail.getStepObjective()) {
                                missionDetailDataResponse.getCompletedMissionDetailList().add(missionInfoDataDetail);
                            }
                            else {
                                missionDetailDataResponse.getMissionDetailList().add(missionInfoDataDetail);
                            }
                        }
                        else if (missionInfoDataDetail.getType().equalsIgnoreCase("timed")) {
                            Date now = new Date();
                            
                          //If endDate <= now ---> Completed Mission
                            if (missionInfoDataDetail.getEndDate().before(now)) {
                                missionDetailDataResponse.getCompletedMissionDetailList().add(missionInfoDataDetail);
                            }
                            else {
                                missionDetailDataResponse.getMissionDetailList().add(missionInfoDataDetail);
                            }
                        }
                    }
            		
            	}
            	
            }

//            Date startDate = new Date(System.currentTimeMillis());
//            
//            MissionInfoDataDetail missionInfoElement1 = new MissionInfoDataDetail();
//            MissionInfoDataDetail missionInfoElement2 = new MissionInfoDataDetail();
//            
//            MissionInfoDataDetail completedMissions1 = new MissionInfoDataDetail();
//            
//            AppLink appLinkPromotion = new AppLink();
//            appLinkPromotion.setType(PushNotificationContentType.INTERNAL);
//            appLinkPromotion.setLocation("http://5.10.69.210/ENI_VoucherCinema/index.php");
//            ParameterNotification parameterNotificationPromo01 = new ParameterNotification("C_5", "8My8Y030fK");
//            appLinkPromotion.getParameters().add(parameterNotificationPromo01);
//            ParameterNotification parameterNotificationPromo02 = new ParameterNotification("C_7", "n/a");
//            appLinkPromotion.getParameters().add(parameterNotificationPromo02);
//           
//            
//            AppLink appLinkMission01 = new AppLink();
//            appLinkMission01.setType(PushNotificationContentType.INTERNAL);
//            appLinkMission01.setLocation("http://5.10.69.210/ENI_VoucherCinema/index.php");
//            ParameterNotification parameterNotification01 = new ParameterNotification("C_5", "8My8Y030fK");
//            appLinkMission01.getParameters().add(parameterNotification01);
//            
//            missionInfoElement1.setAppLinkInfo(appLinkMission01);
//            missionInfoElement1.setCode("Codice 1");
//            missionInfoElement1.setDescription("Descrizione 1");
//            missionInfoElement1.setName("Voucher Cinema 2x1");
//            missionInfoElement1.setType("step");
//            missionInfoElement1.setStepCompleted(1);
//            missionInfoElement1.setStepObjective(4);
//            missionInfoElement1.setStartDate(startDate);
//            missionInfoElement1.setEndDate(startDate);
//            
//            missionDetailDataResponse.getMissionDetailList().add(missionInfoElement1);
//            
//            
//            AppLink appLinkMission02 = new AppLink();
//            appLinkMission02.setType(PushNotificationContentType.INTERNAL);
//            appLinkMission02.setLocation("http://5.10.69.210/ENI_VoucherCinema/index.php");
//            ParameterNotification parameterNotification02 = new ParameterNotification("C_5", "8My8Y030fK");
//            appLinkMission02.getParameters().add(parameterNotification02);
//            
//            missionInfoElement2.setAppLinkInfo(appLinkMission02);
//            missionInfoElement2.setCode("000001475");
//            missionInfoElement2.setDescription("Descrizione 2");
//            missionInfoElement2.setName("5� di sconto pagando con Eni Pay");
//            missionInfoElement2.setType("timed");
//            missionInfoElement2.setStepCompleted(4);
//            missionInfoElement2.setStepObjective(8);
//            missionInfoElement2.setStartDate(startDate);
//            missionInfoElement2.setEndDate(startDate);
//            
//            missionDetailDataResponse.getMissionDetailList().add(missionInfoElement2);
//            
//            
//            AppLink appLinkCompletedMission01 = new AppLink();
//            appLinkCompletedMission01.setType(PushNotificationContentType.INTERNAL);
//            appLinkCompletedMission01.setLocation("http://5.10.69.210/ENI_VoucherCinema/index.php");
//            ParameterNotification parameterNotification03 = new ParameterNotification("C_5", "8My8Y030fK");
//            appLinkCompletedMission01.getParameters().add(parameterNotification03);
//            
//            completedMissions1.setAppLinkInfo(appLinkCompletedMission01);
//            completedMissions1.setCode("000001475");
//            completedMissions1.setDescription("Descrizione completed mission 1");
//            completedMissions1.setName("5� di sconto pagando con Eni Pay");
//            completedMissions1.setType("timed");
//            completedMissions1.setStepCompleted(4);
//            completedMissions1.setStepObjective(8);
//            completedMissions1.setStartDate(startDate);
//            completedMissions1.setEndDate(startDate);
//            
//            missionDetailDataResponse.getCompletedMissionDetailList().add(completedMissions1);

            missionDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_MISSION_LIST_SUCCESS);

            userTransaction.commit();
            return missionDetailDataResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get Mission List Action  with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
