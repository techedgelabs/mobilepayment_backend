package com.techedge.mp.core.actions.user;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PrefixNumberResult;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.PrefixNumberBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserRetrieveAllPrefixAction {
    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserRetrieveAllPrefixAction() {}

    public PrefixNumberResult execute(String ticketId, String requestId) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        PrefixNumberResult result = new PrefixNumberResult();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid()/* || !ticketBean.isCustomerTicket()*/) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                result.setStatusCode(ResponseHelper.PREFIX_RETRIEVE_INVALID_TICKET);
                return result;
            }

            List<PrefixNumberBean> prefixNumberList = QueryRepository.findAllPrefix(em);
            List<String> list = new ArrayList<String>(0);
            if (prefixNumberList != null && !prefixNumberList.isEmpty()) {
                for (PrefixNumberBean item : prefixNumberList) {
                    list.add(item.getCode());
                }
            }
            result.setListPrefix(list);
            result.setStatusCode(ResponseHelper.PREFIX_RETRIEVE_SUCCESS);
            userTransaction.commit();

            return result;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieve prefix with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
