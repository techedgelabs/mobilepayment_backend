package com.techedge.mp.core.actions.user.v2;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.DpanStatusEnum;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.DpanDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetMcCardStatusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2SetActiveDpanAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2SetActiveDpanAction() {}

    public String execute(String ticketId, String requestId, String dpan, String status,
            UserCategoryService userCategoryService, CardInfoServiceRemote cardInfoService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_SET_ACTIVE_DPAN_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_SET_ACTIVE_DPAN_INVALID_TICKET;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to set default plate number in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_SET_ACTIVE_DPAN_FAILURE;
            }
            
            userBean.setActiveMcCardDpan(dpan);
            
            
            // Recupero pan carta
            String pan = "";
            Date now = new Date();
            
            String userID = userBean.getPersonalDataBean().getFiscalCode();
            
            Boolean isMulticardFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.MULTICARD_FLOW.getCode());
            if (isMulticardFlow) {
                if (userBean.getExternalUserId() != null && !userBean.getExternalUserId().isEmpty()) {
                    userID = userBean.getExternalUserId();
                }
            }
            
            // Allineamento dpan carte
            GetMcCardStatusResult getMcCardStatusResult = new GetMcCardStatusResult();

            try {
                PartnerType partnerType = PartnerType.EM;

                String operationId      = new IdGenerator().generateId(16).substring(0, 32);
                String userId           = userID;
                Long csRequestTimestamp = now.getTime();
                
                getMcCardStatusResult = cardInfoService.getMcCardStatus(operationId, userId, partnerType, csRequestTimestamp);
            }
            catch (Exception ex) {
                getMcCardStatusResult.setStatus("ERROR");
            }
            
            if (getMcCardStatusResult.getStatus() != null) {
                
                for (DpanDetail dpanDetail : getMcCardStatusResult.getDpanDetailList()) {
                    
                    if (dpan.equals(dpanDetail.getMcCardDpan())) {
                        
                        System.out.println("Allineamento pan multicard");
                        pan = dpanDetail.getMcCardPan();
                        System.out.println("pan: " + pan);
                        break;
                    }
                }
            }
            
            // Aggiorna lo stato della carta
            // PRE_ENROLLED = Pre-Enrolled
            // ACTIVATED    = Attivato
            // BLOCKED      = Bloccato
            // CANCELED     = Cancellato
            // CANCELLED    = Cancellato
            // REASSIGNED   = Carta associata ad altro dispositivo
            for(PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                
                if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD) &&
                    paymentInfoBean.getToken() != null &&
                    paymentInfoBean.getToken().equals(dpan)) {
                    
                    Boolean mergeRequired = Boolean.FALSE;
                    
                    Integer oldStatus = paymentInfoBean.getStatus();
                    Integer newStatus = this.mapStatusFromDpan(status);
                    
                    if (oldStatus != newStatus) {
                        paymentInfoBean.setStatus(newStatus);
                        mergeRequired = Boolean.TRUE;
                        System.out.println("Aggiornato stato multicard con dpan " + dpan + " da " + oldStatus + " a " + newStatus);
                    }
                    
                    String oldPan = paymentInfoBean.getPan();
                    String newPan = pan;
                    
                    if (oldPan != newPan) {
                        paymentInfoBean.setPan(newPan);
                        mergeRequired = Boolean.TRUE;
                        System.out.println("Aggiornato pan multicard con dpan " + dpan + " da " + oldPan + " a " + newPan);
                    }
                    
                    if (mergeRequired) {
                        em.merge(paymentInfoBean);
                    }
                }
            }
            
            // Se l'utente � di tipo MULTICARD e lo stato � BLOCKED, CANCELLED (CANCELED) o REASSIGNED l'utente va riportato allo step per rifare l'enrollment
            if (isMulticardFlow) {
                
                System.out.println("Utente MyMulticard");
                
                if (status.equals(DpanStatusEnum.BLOCKED.name())   ||
                    status.equals(DpanStatusEnum.CANCELLED.name()) ||
                    status.equals("CANCELED")                      ||
                    status.equals(DpanStatusEnum.REASSIGNED.name())) {
                    
                    System.out.println("Stato multicard : " + status + " -> utente riportato allo step di enrollment");
                    
                    userBean.unsetRegistrationCompleted();
                    userBean.setDepositCardStepCompleted(Boolean.FALSE);
                }
            }
            
            em.merge(userBean);
            
            userTransaction.commit();

            return ResponseHelper.USER_SET_ACTIVE_DPAN_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED setting dpan with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
    
    private Integer mapStatusFromDpan(String status) {
        if (status.equals(DpanStatusEnum.PRE_ENROLLED.name())) {
            return PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED;
        }
        else if (status.equals(DpanStatusEnum.ACTIVATED.name())) {
            return PaymentInfo.PAYMENTINFO_STATUS_VERIFIED;
        }
        else if (status.equals(DpanStatusEnum.CANCELLED.name())) {
            return PaymentInfo.PAYMENTINFO_STATUS_CANCELED;
        }
        else if (status.equals("CANCELED")) {
            return PaymentInfo.PAYMENTINFO_STATUS_CANCELED;
        }
        else if (status.equals(DpanStatusEnum.BLOCKED.name())) {
            return PaymentInfo.PAYMENTINFO_STATUS_BLOCKED;
        }
        else if (status.equals(DpanStatusEnum.REASSIGNED.name())) {
            return PaymentInfo.PAYMENTINFO_STATUS_CANCELED;
        }
        else {
            return PaymentInfo.PAYMENTINFO_STATUS_CANCELED;
        }
    }
}
