package com.techedge.mp.core.actions.user.v2;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.LandingDataResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.LandingDataBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.UserTypeBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2GetLandingAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2GetLandingAction() {}

    public LandingDataResponse execute(String ticketId, String requestId, String sectionID, Long deployDateLong, Long guestLandingActivationTimestamp, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        LandingDataResponse response = new LandingDataResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid()) { // || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_INVALID_TICKET);
                return response;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_INVALID_TICKET);
                return response;
            }
            
            /* Gestione landing per utenti guest */
            Date now = new Date();
            Long currentTimestamp = now.getTime();
            System.out.println("currentTimestamp: " + currentTimestamp + ", guestLandingActivationTimestamp: " + guestLandingActivationTimestamp);
            
            if (currentTimestamp > guestLandingActivationTimestamp) {
                Boolean isGuestFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.GUEST_FLOW.getCode());
                if (isGuestFlow && sectionID.equals("HOME_REFUELING")) {
                    
                    // Se l'utente � di tipo guest e non ha ancora visualizzato la pagina di landing bisogna restituire la landing HOME_GUEST
                    //  e impostare a true il flag di visualizzazione
                    
                     if (!userBean.getLandingDisplayed()) {
                         sectionID = "HOME-GUEST";
                         userBean.setLandingDisplayed(Boolean.TRUE);
                         em.merge(userBean);
                        
                         System.out.println("Landing guest restituita e flag di visualizzazione settato");
                     }
                     else {
                         System.out.println("Landing guest gi� visualizzata");
                     }
                }
            }

            LandingDataBean landingDataBean = QueryRepository.findLandingPageByName(em, sectionID);

            if (landingDataBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "landing page not found");

                userTransaction.commit();
                response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_FAILURE);
                return response;
            }

            if (!landingDataBean.getStatus().equals("1")) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "landing page status not 1");

                userTransaction.commit();
                response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_SUCCESS);
                return response;
            }
            
            // Se il servizio � chiamato con un ticket di servizio allora pu� restituire solo la landing REGISTRATION
            if (!ticketBean.isCustomerTicket()) {
                
                if (sectionID.equals("REGISTRATION-ENISTATION")) {
                    
                    userTransaction.commit();
                    response.setLandingData(landingDataBean.toLandingData());
                    response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_SUCCESS);
                    return response;
                }
                else {
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "system user not authorized to get landing " + sectionID);

                    userTransaction.commit();
                    response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_UNAUTHORIZED);
                    return response;
                }
            }
            
            UserTypeBean userTypeBean = QueryRepository.findUserTypeByCode(em, userBean.getUserType());

            if (userTypeBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "user type not valid");

                userTransaction.commit();
                response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_UNAUTHORIZED);
                return response;
            }

            LandingDataBean landingDataBeanResult = null;

            if (landingDataBean.getUserCategoryBean() != null && userTypeBean.getUserCategories() != null && !userTypeBean.getUserCategories().isEmpty()) {
                for (UserCategoryBean item : userTypeBean.getUserCategories()) {
                    if (item.getName().equals(landingDataBean.getUserCategoryBean().getName())) {
                        landingDataBeanResult = landingDataBean;
                    }
                }
            }
            else {
                landingDataBeanResult = landingDataBean;
            }

            if (landingDataBeanResult == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "user category not valid");

                userTransaction.commit();
                response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_SUCCESS);
                return response;
            }
            
            // La landing UPDATE-PRIVACY deve essere restituita solo se l'utente sta migrando da EniPay
            if (sectionID.equals("UPDATE-PRIVACY")) {
                
                // Se l'utente non ha il telefono associato deve vedere la pagina MIGRATION-OLD-USER per la migrazione da vecchio utente EniPay
                MobilePhoneBean activeMobilePhone = userBean.activeMobilePhone();
                if(activeMobilePhone == null) {
                    
                    LandingDataBean landingDataBeanMigOldUser = QueryRepository.findLandingPageByName(em, "MIGRATION-OLD-USER");

                    if (landingDataBeanMigOldUser == null) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "old user migration landing page not found");

                        userTransaction.commit();
                        response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_SUCCESS);
                        return response;
                    }

                    if (!landingDataBeanMigOldUser.getStatus().equals("1")) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "old user migration landing page status not 1");

                        userTransaction.commit();
                        response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_SUCCESS);
                        return response;
                    }
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Utente migrato da EniPay (vecchio flusso) -> visualizzazione landing MIGRATION-OLD-USER");
                    
                    userTransaction.commit();
                    response.setLandingData(landingDataBeanMigOldUser.toLandingData());
                    response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_SUCCESS);
                    return response;
                }
                
                System.out.println("Verifica loyaltyCheckEnabled - createDate: " + userBean.getCreateDate().getTime());
                System.out.println("DeployDate: " + deployDateLong);
                
                if ((userBean.getCreateDate().getTime() <= deployDateLong)) {
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Utente migrato da EniPay -> visualizzazione landing UPDATE-PRIVACY");
                    
                    userTransaction.commit();
                    response.setLandingData(landingDataBeanResult.toLandingData());
                    response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_SUCCESS);
                    return response;
                }
                else {
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Utente non migrato da EniPay -> landing UPDATE-PRIVACY da non visualizzare");
                    
                    userTransaction.commit();
                    response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_SUCCESS);
                    return response;
                }
            }

            userTransaction.commit();
            response.setLandingData(landingDataBeanResult.toLandingData());
            response.setStatusCode(ResponseHelper.USER_V2_GETLANDING_SUCCESS);
            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get landing  with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
