package com.techedge.mp.core.actions.user;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.loyalty.RedemptionResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.loyalty.RedemptionTransactionBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserRedemptionAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserRedemptionAction() {}

    public RedemptionResponse execute(String ticketId, String requestId, Integer redemptionCode, FidelityServiceRemote fidelityService, 
            String pin, Integer reconciliationAttemptsLeft) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                return createMessageError(requestId, userTransaction, "Invalid ticket", ResponseHelper.USER_REDEMPTION_INVALID_TICKET, null);
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Utente non valido
                return createMessageError(requestId, userTransaction, "User not found", ResponseHelper.USER_REDEMPTION_INVALID_TICKET, null);
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                return createMessageError(requestId, userTransaction, "Unable to get redemption in status " + String.valueOf(userStatus),
                        ResponseHelper.USER_REDEMPTION_UNAUTHORIZED, null);
            }

            String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();

            if (fiscalCode == null) {
                return createMessageError(requestId, userTransaction, "Fiscal code null", ResponseHelper.USER_REDEMPTION_INVALID_FISCAL_CODE, null);
            }

            if (redemptionCode == null) {
                return createMessageError(requestId, userTransaction, "Redemption code null", ResponseHelper.USER_REDEMPTION_INVALID_REDEMPTION_CODE, null);
            }

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            PartnerType partnerType = PartnerType.MP;
            long requestTimestamp = new Date().getTime();

            if (pin == null) {
                return createMessageError(requestId, userTransaction, "PIN null", ResponseHelper.USER_REDEMPTION_INVALID_REDEMPTION_CODE, null);
            }

            if (userBean.getPaymentData() == null && userBean.getPaymentData().isEmpty()) {
                return createMessageError(requestId, userTransaction, "User not payment method", ResponseHelper.USER_REDEMPTION_FAILURE, null);
            }

            PaymentInfoBean voucherPayment = userBean.getVoucherPaymentMethod();

            if (voucherPayment == null) {
                return createMessageError(requestId, userTransaction, "Voucher Payment null", ResponseHelper.USER_REDEMPTION_ERROR_PIN_NOT_FOUND, null);
            }
            
            if (voucherPayment.getPin() == null) {
                return createMessageError(requestId, userTransaction, "PIN null", ResponseHelper.USER_REDEMPTION_ERROR_PIN_NOT_FOUND, null);
            }

            if (voucherPayment.getPinCheckAttemptsLeft() == 0) {
                voucherPayment.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
                
                // e tutti i metodi di pagamento verificati e non verificati sono cancellati
                for (PaymentInfoBean paymentInfo : userBean.getPaymentData()) {

                    if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                            && (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                        paymentInfo.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                        paymentInfo.setDefaultMethod(false);
                        em.merge(paymentInfo);
                    }
                }                    
                
                return createMessageError(requestId, userTransaction, "PinCheckAttemptsLeft = 0", ResponseHelper.USER_REDEMPTION_ERROR_PIN_ATTEMPTS_LEFT, 0);
            }

            if (!voucherPayment.getPin().equals(pin)) {
                
                Integer pinCheckAttemptsLeft = voucherPayment.getPinCheckAttemptsLeft() - 1;
                
                voucherPayment.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);
                
                if ( pinCheckAttemptsLeft == 0 ) {
                    
                    // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                    voucherPayment.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

                    // e tutti i metodi di pagamento verificati e non verificati sono cancellati
                    for (PaymentInfoBean paymentInfo : userBean.getPaymentData()) {

                        if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                                && (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                            paymentInfo.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                            paymentInfo.setDefaultMethod(false);
                            em.merge(paymentInfo);
                        }
                    }                    

                    return createMessageError(requestId, userTransaction, "PinCheckAttemptsLeft = 0", ResponseHelper.USER_REDEMPTION_ERROR_PIN_ATTEMPTS_LEFT, 0);
                }
                else {
                    
                    if ( pinCheckAttemptsLeft == 1 ) {
                        
                        return createMessageError(requestId, userTransaction, "PIN not valid", ResponseHelper.USER_REDEMPTION_ERROR_PIN_LAST_ATTEMPT, pinCheckAttemptsLeft);
                    }
                    else {
                        
                        return createMessageError(requestId, userTransaction, "PIN not valid", ResponseHelper.USER_REDEMPTION_ERROR_PIN, pinCheckAttemptsLeft);
                    }
                }
            }

            RedemptionTransactionBean redemptionTransactionBean = new RedemptionTransactionBean();
            redemptionTransactionBean.setOperationID(operationID);
            redemptionTransactionBean.setRedemptionCode(redemptionCode);
            redemptionTransactionBean.setRequestTimestamp(new Date(requestTimestamp));
            redemptionTransactionBean.setUserBean(userBean);
            redemptionTransactionBean.setToReconcilie(false);
            redemptionTransactionBean.setReconciliationAttemptsLeft(reconciliationAttemptsLeft);

            em.persist(redemptionTransactionBean);
            
            try {
                RedemptionResult redemptionResult = fidelityService.redemption(operationID, partnerType, requestTimestamp, fiscalCode, redemptionCode);

                if (!redemptionResult.getStatusCode().equals(FidelityResponse.REDEMPTION_OK)) {
                    
                    redemptionTransactionBean.setStatusCode(redemptionResult.getStatusCode());
                    redemptionTransactionBean.setMessageCode(redemptionResult.getMessageCode());
                    
                    em.merge(redemptionTransactionBean);

                    String responseCode = ResponseHelper.USER_REDEMPTION_GENERIC_ERROR;
                    
                    if (redemptionResult.getStatusCode().equals(FidelityResponse.REDEMPTION_INVALID_BALANCE)) {
                        responseCode = ResponseHelper.USER_REDEMPTION_INVALID_BALANCE;
                    }

                    if (redemptionResult.getStatusCode().equals(FidelityResponse.REDEMPTION_INVALID_FISCAL_CODE)) {
                        responseCode = ResponseHelper.USER_REDEMPTION_INVALID_FISCAL_CODE;
                    }

                    if (redemptionResult.getStatusCode().equals(FidelityResponse.REDEMPTION_CUSTOMER_BLOCKED)) {
                        responseCode = ResponseHelper.USER_REDEMPTION_CUSTOMER_BLOCKED;
                    }
                    
                    return createMessageError(requestId, userTransaction, "Unable to redemption: " + redemptionResult.getMessageCode() + " (" + redemptionResult.getStatusCode() + ")",
                            responseCode, null);
                }

                VoucherDetail voucherDetail = redemptionResult.getVoucher();
                VoucherBean voucherBean = new VoucherBean();
                voucherBean.setCode(voucherDetail.getVoucherCode());
                voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                voucherBean.setInitialValue(voucherDetail.getInitialValue());
                voucherBean.setIsCombinable(voucherDetail.getIsCombinable());
                voucherBean.setMinAmount(voucherDetail.getMinAmount());
                voucherBean.setMinQuantity(voucherDetail.getMinQuantity());
                voucherBean.setPromoCode(voucherDetail.getPromoCode());
                voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                voucherBean.setPromoPartner(voucherDetail.getPromoPartner());
                voucherBean.setStatus(voucherDetail.getVoucherStatus());
                voucherBean.setType(voucherDetail.getVoucherType());
                voucherBean.setUserBean(userBean);
                voucherBean.setValidPV(voucherDetail.getValidPV());
                voucherBean.setValue(voucherDetail.getVoucherValue());
                voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());

                em.persist(voucherBean);

                redemptionTransactionBean.setBalance(redemptionResult.getBalance());
                redemptionTransactionBean.setBalanceAmount(redemptionResult.getBalanceAmount());
                redemptionTransactionBean.setCredits(redemptionResult.getCredits());
                redemptionTransactionBean.setCsTransactionID(redemptionResult.getCsTransactionID());
                redemptionTransactionBean.setMarketingMsg(redemptionResult.getMarketingMsg());
                redemptionTransactionBean.setMessageCode(redemptionResult.getMessageCode());
                redemptionTransactionBean.setStatusCode(redemptionResult.getStatusCode());
                redemptionTransactionBean.setVoucherBean(voucherBean);
                redemptionTransactionBean.setWarningMsg(redemptionResult.getWarningMsg());

                em.merge(redemptionTransactionBean);

                RedemptionResponse redemptionResponse = new RedemptionResponse();
                redemptionResponse.setStatusCode(ResponseHelper.USER_REDEMPTION_SUCCESS);
                redemptionResponse.setVoucher(redemptionResult.getVoucher());
                redemptionResponse.setCheckPinAttemptsLeft(voucherPayment.getPinCheckAttemptsLeft());
                userTransaction.commit();
                
                return redemptionResponse;
            }
            catch (FidelityServiceException ex) {
                
                redemptionTransactionBean.setToReconcilie(true);
                redemptionTransactionBean.setStatusCode(FidelityResponse.REDEMPTION_GENERIC_ERROR);
                redemptionTransactionBean.setMessageCode(ex.getMessage());
                
                em.merge(redemptionTransactionBean);

                return createMessageError(requestId, userTransaction, "Unable to redemption: " + ex.getMessage(), ResponseHelper.USER_REDEMPTION_FAILURE, null);
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get active vouchers with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private RedemptionResponse createMessageError(String requestId, UserTransaction userTransaction, String message, String statusCode, Integer attemptsLeft) throws Exception {

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, message);

        userTransaction.commit();

        RedemptionResponse redemptionResponse = new RedemptionResponse();
        redemptionResponse.setStatusCode(statusCode);
        
        if (attemptsLeft != null) {
            redemptionResponse.setCheckPinAttemptsLeft(attemptsLeft);
        }

        return redemptionResponse;
    }

}
