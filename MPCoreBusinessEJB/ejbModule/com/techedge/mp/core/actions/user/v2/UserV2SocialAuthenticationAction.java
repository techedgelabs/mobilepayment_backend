package com.techedge.mp.core.actions.user.v2;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.exceptions.SocialAuthenticationException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SendValidationResult;
import com.techedge.mp.core.business.interfaces.SocialAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.SocialUserAccountData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.interfaces.user.UserDevice;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.model.LastLoginDataBean;
import com.techedge.mp.core.business.model.LastLoginErrorDataBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.UserDeviceBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltySessionBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.ResendValidation;
import com.techedge.mp.core.business.utilities.SocialAuthenticationHelper;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.Platform;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2SocialAuthenticationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2SocialAuthenticationAction() {}

    private String createDeviceEndpoint(String deviceToken, String deviceData, String deviceFamily, String requestId, PushNotificationServiceRemote pushNotificationService) {
        Platform platform = null;

        if (deviceFamily.equals("AND")) {
            platform = Platform.GCM;
        }

        if (deviceFamily.equals("IPH")) {
            platform = Platform.APNS;
        }

        if (deviceFamily.equals("WPH")) {
            platform = Platform.WNS;
        }

        if (platform != null) {
            PushNotificationResult endpointResult = pushNotificationService.createEndpoint(deviceToken, deviceData, platform);

            if (endpointResult.getStatusCode().equals(com.techedge.mp.pushnotification.adapter.interfaces.StatusCode.PUSH_NOTIFICATION_CREATE_ENDPOINT_SUCCESS)) {
                //pushNotificationService.publishMessage(endpointResult.getArnEndpoint(), "Messaggio di prova", platform);
                return endpointResult.getArnEndpoint();
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Publish message failed! " + endpointResult.getMessage());

                return null;
            }
        }
        else {
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "deviceFamily (" + deviceFamily
                    + ") from requestID doesn't match a valid Platform");

            return null;
        }
    }
    
    
    public SocialAuthenticationResponse execute(String accessToken, String socialProvider, String requestId, String deviceId, String deviceName,
            String deviceToken, String proxyHost, String proxyPort, String proxyNoHosts, long maxPendingInterval, Integer ticketExpiryTime, Integer loyaltySessionExpiryTime,
            Integer loginAttemptsLimit, Integer loginLockExpiryTime, List<String> userBlockExceptionList, UserCategoryService userCategoryService, UnavailabilityPeriodService unavailabilityPeriodService,
            DWHAdapterServiceRemote dwhAdapterService, PushNotificationServiceRemote pushNotificationService, Integer maxRetryAttemps, String sendingType,
            EmailSenderRemote emailSender, SmsServiceRemote smsService, Long deployDateLong, StringSubstitution stringSubstitution) throws EJBException {

                
        SocialUserAccountData socialUserAccountData = null;
        
        try {
            socialUserAccountData = SocialAuthenticationHelper.executeSocialAuthentication(socialProvider, accessToken, proxyHost, proxyPort, proxyNoHosts,requestId);
        }
        catch (SocialAuthenticationException saex) {
            
            //saex.printStackTrace();
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, saex.getErrorMessage());

            SocialAuthenticationResponse socialAuthenticationResponse = new SocialAuthenticationResponse();
            socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_LOGIN_ERROR);
            
            return socialAuthenticationResponse;
        }
        catch (Exception ex) {
            
            ex.printStackTrace();
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, ex.getMessage());

            SocialAuthenticationResponse socialAuthenticationResponse = new SocialAuthenticationResponse();
            socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_FAILURE);
            
            return socialAuthenticationResponse;
        }
        
        if (socialUserAccountData == null) {
            
            System.err.println("socialUserAccountData null");
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "socialUserAccountData null");

            SocialAuthenticationResponse socialAuthenticationResponse = new SocialAuthenticationResponse();
            socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_FAILURE);
            
            return socialAuthenticationResponse;
        }
        
        
        UserTransaction userTransaction = context.getUserTransaction();
        
        try {

            userTransaction.begin();
            
            UserSocialDataBean userSocialDataBean = null;
            try {
                userSocialDataBean = QueryRepository.findSocialUserByUUID(em, socialUserAccountData.getUserAccountId(), socialProvider);
            }
            catch (NonUniqueResultException nue) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", socialUserAccountData.getUserAccountId(), null, "Non unique record found !");
                throw new Exception(nue);
            }
            
            
            if (userSocialDataBean == null || (userSocialDataBean!=null && userSocialDataBean.getUserBean() == null)) {

                // Utente social non ancora registrato sul backend -> avvio processo di registrazione
                
                System.out.println("Prima login social nuovo utente");
                
                User user = new User();

                PersonalData personalData = new PersonalData();
                if (socialUserAccountData.getFirstName() != null)
                    personalData.setFirstName(socialUserAccountData.getFirstName());
                if (socialUserAccountData.getLastName() != null)
                    personalData.setLastName(socialUserAccountData.getLastName());
                if (socialUserAccountData.getGender() != null)
                    personalData.setSex(socialUserAccountData.getGender().startsWith("f") ? "F" : "M");
                if (socialUserAccountData.getBirthday() != null) {
                    personalData.setBirthDate(DateHelper.convertToDateOfBirth(socialUserAccountData.getBirthday()));
                }
                
                // personalData.setBirthMunicipality((userDetail.getCityOfBirth()
                // == null) ? "" : userDetail.getCityOfBirth());
                // personalData.setBirthProvince((userDetail.getCountryOfBirth()
                // == null) ? "" : userDetail.getCountryOfBirth());
                personalData.setSecurityDataEmail(socialUserAccountData.getEmail());
                user.setPersonalData(personalData);

                user.setUserStatus(User.USER_STATUS_SOCIAL_REGISTRATION);
                user.setUserStatusRegistrationCompleted(true);
                user.setEniStationUserType(Boolean.FALSE);
                
                SocialAuthenticationResponse socialAuthenticationResponse = new SocialAuthenticationResponse();
                socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_NEW_USER);
                socialAuthenticationResponse.setUser(user);
                
                userTransaction.commit();

                return socialAuthenticationResponse;
            }

            UserBean userBean = userSocialDataBean.getUserBean();
           
            
            
            // Se l'utente esiste bisogna verificare che si sia registrato con lo stesso social con il quale sta effettuando la login
            
            String providerFound = userBean.getSocialProvider();
            if (providerFound == null || !providerFound.equals(socialProvider)) {
                
                System.out.println("Utente non social o registrato con un altro social (" + providerFound + ")");
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong social (" + providerFound + ")");

                SocialAuthenticationResponse socialAuthenticationResponse = new SocialAuthenticationResponse();
                socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_WRONG_SOCIAL);
                
                userTransaction.commit();
                
                return socialAuthenticationResponse;
            }
                
                
            SocialAuthenticationResponse socialAuthenticationResponse = null;

            // Verifica lo stato dell'utente
            
            Integer userStatus = userBean.getUserStatus();
            if (userStatus == User.USER_STATUS_BLOCKED || userStatus == User.USER_STATUS_NEW) {

                // Un utente che si trova in questo stato non pu� effettuare il login
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User ID " + userBean.getId() + " Social UUID "+userSocialDataBean.getUuid() +" in status "
                        + userStatus);
                socialAuthenticationResponse = new SocialAuthenticationResponse();
                if (userStatus == User.USER_STATUS_NEW) {
                    socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_NOT_VERIFIED);
                }
                else {
                    socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_UNAUTHORIZED);
                }

                userTransaction.commit();

                return socialAuthenticationResponse;
            }

            
            // Verifica il tipo dell'utente
            
            if (userBean.getUserType() == User.USER_TYPE_REFUELING || userBean.getUserType() == User.USER_TYPE_REFUELING_NEW_ACQUIRER || userBean.getUserType() == User.USER_TYPE_SERVICE) {

                // L'utente � di tipo refueling e non pu� effutare l'accesso
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User ID " + userBean.getId() + " Social UUID "+userSocialDataBean.getUuid() +", type: " + userBean.getUserType());
                socialAuthenticationResponse = new SocialAuthenticationResponse();
                socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_INVALID_TICKET);

                userTransaction.commit();

                return socialAuthenticationResponse;
            }

            // Genera il token della sessione loyalty

            Date creationTimestamp = new Date();
            Date lastUsedTimestamp = creationTimestamp;
            Date expiryTimestamp = DateHelper.addMinutesToDate(loyaltySessionExpiryTime, creationTimestamp);
            String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
            
            if (userBean.getSource() != null && userBean.getSource().equals(User.USER_MULTICARD)) {
                
                System.out.println("Generazione sessione loyalty per utente Multicard");
                
                // Per l'utente multicard il campo fiscalcode deve essere valorizzato con l'email contenuta in UserExternalData
                if (userBean.getUserExternalData() != null && !userBean.getUserExternalData().isEmpty()) {
                    
                    fiscalCode = userBean.getUserExternalData().iterator().next().getUuid();
                    userBean.setExternalUserId(fiscalCode);
                }
                else {
                    
                    System.out.println("UserExternalData non valido o non presente");
                }
            }

            String sessionId = new IdGenerator().generateId(16).substring(0, 32);

            LoyaltySessionBean loyaltySessionBean = new LoyaltySessionBean(sessionId, userBean, creationTimestamp, lastUsedTimestamp, expiryTimestamp, fiscalCode);

            em.persist(loyaltySessionBean);

            System.out.println("Creata sessione loyalty per l'utente " + userBean.getPersonalDataBean().getSecurityDataEmail() + " (" + userBean.getId() + ")");

            Date checkDate = new Date();

            LastLoginErrorDataBean lastLoginErrorDataBean = userBean.getLastLoginErrorDataBean();

            String deviceFamily = requestId.substring(0, 3).toUpperCase();

            // Controlla se l'utente ha superato il numero di tentativi ed � ancora nell'elapsed time;
            
            if (lastLoginErrorDataBean != null && lastLoginErrorDataBean.getAttempt() > loginAttemptsLimit && lastLoginErrorDataBean.getTime().after(checkDate)) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Login Locked access: User ID " + userBean.getId() + " Social UUID "+userSocialDataBean.getUuid() +" in status "
                        + userStatus);
                socialAuthenticationResponse = new SocialAuthenticationResponse();
                socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_LOCKED);

                userTransaction.commit();

                return socialAuthenticationResponse;
            }

            // 1) crea un nuovo ticket
            User user = userBean.toUser();

            Integer ticketType = 0;
            if (user.getUserType() == User.USER_TYPE_CUSTOMER || user.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER) {
                ticketType = TicketBean.TICKET_TYPE_CUSTOMER;
            }
            else {
                ticketType = TicketBean.TICKET_TYPE_TESTER;
            }

            Date now = new Date();
            Date lastUsed = now;
            Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

            String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

            TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);

            em.persist(ticketBean);

            /*
             * // 2) rimuovi il vecchio lastLoginData LastLoginDataBean
             * lastLoginDataBean = userBean.getLastLoginDataBean(); if (
             * lastLoginDataBean != null) { userBean.setLastLoginDataBean(null);
             * em.remove(lastLoginDataBean); }
             * 
             * 
             * // 3) inserisci il nuovo lastLoginData lastLoginDataBean = new
             * LastLoginDataBean(); lastLoginDataBean.setDeviceId(deviceId);
             * lastLoginDataBean.setDeviceName(deviceName); // TODO memorizzare
             * anche latitudine e longitudine del device
             * //lastLoginDataBean.setLatitude(latitude);
             * //lastLoginDataBean.setLongitude(longitude);
             * lastLoginDataBean.setTime(new Timestamp((new Date()).getTime()));
             * em.persist(lastLoginDataBean);
             * 
             * userBean.setLastLoginDataBean(lastLoginDataBean);
             */

            // 2 e 3 Sostituzione vecchio lastLoginData o creazione nuovo
            String newEndpoint = null;
            Boolean deviceFound = false;
            Boolean userStatusSet = false;
            Boolean endpointUpdateNeeded = false;
            Integer outputStatus = User.USER_STATUS_VERIFIED;
            MobilePhoneBean activeMobilePhone = userBean.activeMobilePhone();

            System.out.println("Stato utente: " + userBean.getUserStatus() + ", activeMobilePhone found: " + (activeMobilePhone != null));
            if (activeMobilePhone != null) {
                System.out.println("Phone status: " + activeMobilePhone.getStatus() + "Phone number: " + activeMobilePhone.getNumber());
            }

            if (userBean.getUserType() != User.USER_TYPE_TESTER && userBean.getUserStatus() == User.USER_STATUS_VERIFIED) {

                SendValidationResult result = new SendValidationResult();
                UserDeviceBean userDeviceBean = new UserDeviceBean();

                // Verifica se l'utente ha gi� associato un dispositivo con il device id passato nella richiesta
                Set<UserDeviceBean> userDeviceBeanList = userBean.getUserDeviceBean();

                if (userDeviceBeanList != null) {

                    for (UserDeviceBean item : userDeviceBeanList) {

                        // Esamina i device che non sono stati cancellati
                        if (item.getDeviceId().equals(deviceId) && item.getStatus() != UserDevice.USER_DEVICE_STATUS_CANCELLED) {

                            // Se l'utente ha gi� verificato il device non � necessario fare nessuna attivit�
                            if (item.getStatus() == UserDevice.USER_DEVICE_STATUS_VERIFIED) {

                                // Se il device non � ancora stato associato all'utente effettua l'associazione e invia l'sms di verifica
                                if (deviceToken != null && userBean.getLastLoginDataBean() != null) {

                                    // Confronta l'ultimo token utilizzato dall'utente con quello passato in input (TODO - verificare se questo controllo � necessario)
                                    String lastToken = userBean.getLastLoginDataBean().getDeviceToken();

                                    if (lastToken != null && !lastToken.equals(deviceToken)) {

                                        // Se sono diversi cancella il vecchio endpoint e generane un altro
                                        String lastEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();

                                        if (lastEndpoint != null) {

                                            pushNotificationService.removeEndpoint(lastEndpoint);
                                        }

                                        newEndpoint = createDeviceEndpoint(deviceToken, deviceName, deviceFamily, requestId, pushNotificationService);
                                    }
                                    else {

                                        // Se sono uguali rigenera l'endpoint
                                        newEndpoint = createDeviceEndpoint(deviceToken, deviceName, deviceFamily, requestId, pushNotificationService);
                                    }

                                }

                                System.out.println("Trovato stato verificato");
                                item.setDeviceEndpoint(newEndpoint);
                                
                                System.out.println("Aggiornamento timestamp ultimo utilizzo");
                                Date currentDate = new Timestamp((new Date()).getTime());
                                item.setLastUsedTimestamp(currentDate);
                                
                                em.merge(item);
                                
                                endpointUpdateNeeded = true;
                                deviceFound = true;
                                userStatusSet = true;
                                break;
                            }

                            // Se il device si trova in stato pending rimanda l'sms per la verifica (TODO capire se � necessario impostare un numero massimo di sms)
                            if (item.getStatus() == UserDevice.USER_DEVICE_STATUS_PENDING) {

                                System.out.println("Trovato stato pending");
                                deviceFound = true;

                                if (activeMobilePhone != null && activeMobilePhone.getNumber() != null) {
                                    outputStatus = User.USER_STATUS_DEVICE_TO_VERIFIED;

                                    userDeviceBean = item;
                                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                                            "Send SMS verification for device " + userDeviceBean.getDeviceId());

                                    // Invio sms di conferma
                                    result = ResendValidation.sendForDevice(sendingType, maxRetryAttemps, userBean, userDeviceBean, loggerService, emailSender, 
                                            smsService, userCategoryService, stringSubstitution);
                                    break;
                                }
                            }
                        }
                    }
                }

                if (!deviceFound) {

                    System.out.println("Device non trovato");

                    System.out.println("Crea il nuovo device");

                    // Crea un nuovo device e genera il codice di verifica e invialo via sms
                    String verificationCode = new IdGenerator().generateNumericId(8);

                    Date currentDate = new Timestamp((new Date()).getTime());
                    
                    userDeviceBean.setCreationTimestamp(currentDate);
                    userDeviceBean.setAssociationTimestamp(null);
                    userDeviceBean.setLastUsedTimestamp(currentDate);

                    System.out.println("Generato codice di verifica " + verificationCode + " per device");

                    userDeviceBean.setVerificationCode(verificationCode);
                    userDeviceBean.setDeviceId(deviceId);
                    userDeviceBean.setDeviceName(deviceName);
                    userDeviceBean.setDeviceFamily(deviceFamily);
                    userDeviceBean.setDeviceEndpoint(newEndpoint);
                    userDeviceBean.setDeviceToken(deviceToken);
                    userDeviceBean.setStatus(UserDevice.USER_DEVICE_STATUS_PENDING);
                    userBean.getUserDeviceBean().add(userDeviceBean);
                    userDeviceBean.setUserBean(userBean);
                    
                    if (userBean.getPersonalDataBean().getSecurityDataEmail().equals("info@trustmyphone.com")) {
                        userDeviceBean.setStatus(UserDevice.USER_DEVICE_STATUS_VERIFIED);
                        
                        result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_SUCCESS);
                    }
                    else {
                        
                        if (activeMobilePhone != null && activeMobilePhone.getNumber() != null) {

                            outputStatus = User.USER_STATUS_DEVICE_TO_VERIFIED;

                            //Invio sms di conferma
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Send SMS verification for device "
                                    + userDeviceBean.getDeviceId());

                            result = ResendValidation.sendForDevice(sendingType, maxRetryAttemps, userBean, userDeviceBean, loggerService, emailSender, 
                                    smsService, userCategoryService, stringSubstitution);
                        }
                    }
                    
                    socialAuthenticationResponse = new SocialAuthenticationResponse();

                    if (result != null && result.getStatusCode() != null && result.getStatusCode().equals(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE)) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error sending verification mobile phone. "
                                + result.getMessageCode() + " (" + result.getErrorCode() + ")");

                        userTransaction.commit();

                        socialAuthenticationResponse.setStatusCode(ResponseHelper.MOBILE_PHONE_UPDATE_SENDING_FAILURE);
                        socialAuthenticationResponse.setTicketId(ticketBean.getTicketId());
                        socialAuthenticationResponse.setUser(userBean.toUser());
                        return socialAuthenticationResponse;
                    }
                }
            }

            LastLoginDataBean lastLoginDataBean = userBean.getLastLoginDataBean();
            if (lastLoginDataBean != null) {

                userBean.getLastLoginDataBean().setDeviceId(deviceId);
                userBean.getLastLoginDataBean().setDeviceName(deviceName);
                userBean.getLastLoginDataBean().setDeviceToken(deviceToken);
                userBean.getLastLoginDataBean().setDeviceFamily(deviceFamily);
                if (endpointUpdateNeeded) {
                    userBean.getLastLoginDataBean().setDeviceEndpoint(newEndpoint);
                }
                userBean.getLastLoginDataBean().setTime(new Timestamp((new Date()).getTime()));
            }
            else {

                lastLoginDataBean = new LastLoginDataBean();
                lastLoginDataBean.setDeviceId(deviceId);
                lastLoginDataBean.setDeviceName(deviceName);
                lastLoginDataBean.setDeviceToken(deviceToken);
                lastLoginDataBean.setDeviceFamily(deviceFamily);
                lastLoginDataBean.setDeviceEndpoint(null);

                // TODO memorizzare anche latitudine e longitudine del device
                // lastLoginDataBean.setLatitude(latitude);
                // lastLoginDataBean.setLongitude(longitude);
                lastLoginDataBean.setTime(new Timestamp((new Date()).getTime()));
                em.persist(lastLoginDataBean);

                userBean.setLastLoginDataBean(lastLoginDataBean);
            }

            // Cancellazione delle informazioni per il recupero della password
            PersonalDataBean personalDataBean = userBean.getPersonalDataBean();
            personalDataBean.setExpirationDateRescuePassword(null);
            personalDataBean.setRescuePassword(null);
            // TODO verificare se serve em.merge(personalDataBean);

            if (userStatusSet || userBean.getUserStatus() == User.USER_STATUS_TEMPORARY_PASSWORD) {
                userBean.setUserStatus(User.USER_STATUS_VERIFIED);
                user.setUserStatus(User.USER_STATUS_VERIFIED);
            }
            if (lastLoginErrorDataBean != null) {
                userBean.getLastLoginErrorDataBean().resetAttempts();
            }

            // Impostazione del flag oldUser per migrazione utenti
            if (userBean.getOldUser() == null) {

                Integer userType = userBean.getUserType();
                Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());

                if (useNewFlow) {

                    System.out.println("Primo accesso nuovo flusso");

                    // Se l'utente appartiene alla nuova categoria, ma non ha numeri di telefono associati, allora oldUser va impostato a true
                    if (userBean.getMobilePhoneList().isEmpty()) {
                        userBean.setOldUser(true);
                        System.out.println("OldUser impostato a true");
                    }
                    else {
                        userBean.setOldUser(false);
                        System.out.println("OldUser impostato a false");
                    }

                    // Modifica per resettare il flag di registrazione completata per i vecchi utenti che utilizzano il nuovo flusso voucher
                    if (userBean.getMobilePhoneList().isEmpty() && userBean.getUserStatusRegistrationCompleted() == true) {

                        // Se un utente non ha numeri di telefono associati (nemmeno in stato cancellato) allora � un utente da migrare
                        System.out.println("Utente con registrazione completata da migrare");
                        userBean.unsetRegistrationCompleted();
                    }
                }
            }

            em.merge(userBean);

            // 4) restituisci il risultato
            socialAuthenticationResponse = new SocialAuthenticationResponse();

            socialAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_SOCIAL_AUTH_SUCCESS);
            socialAuthenticationResponse.setTicketId(ticketBean.getTicketId());
            socialAuthenticationResponse.setLoyaltySessionId(sessionId);

            System.out.println("id: " + userBean.getId());

            User userOut = userBean.toUser();
            
            // Controlla se ci sono nuovi elementi nellla tabella documenti che l'utente non ha ancora accettato
            UserCategoryBean userCategoryBean = QueryRepository.findUserCategoryByName(em, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            
            // Ricerca gli elementi della tabella documenti associati alla categoria 3 o con categoria null
            List<DocumentBean> documentBeanList = QueryRepository.findDocumentAll(em);
            for(DocumentBean documentBean : documentBeanList) {
                
                // Si escludono dall'elaborazione i documenti con groupCategory non null
                if (documentBean.getGroupCategory() == null || documentBean.getGroupCategory().isEmpty()) {
                
                    if (documentBean.getUserCategory() == null || documentBean.getUserCategory().equals(String.valueOf(userCategoryBean.getId()))) {
                        
                        List<DocumentAttributeBean> documentAttributeBeanList = documentBean.getDocumentCheck();
                        
                        for(DocumentAttributeBean documentAttributeBean : documentAttributeBeanList) {
                            
                            //System.out.println("In verifica " + documentAttributeBean.getCheckKey());
                            
                            if ((documentAttributeBean.getConditionText() != null && !documentAttributeBean.getConditionText().isEmpty()) ||
                                (documentAttributeBean.getSubTitle()      != null && !documentAttributeBean.getSubTitle().isEmpty())) {
                                
                                //System.out.println("In elaborazione " + documentAttributeBean.getCheckKey());
                                
                                // Se l'elemento non � presente tra i termini e condizioni associati all'utente allora va inserito con valid a false
                                String checkKey = documentAttributeBean.getCheckKey();
                                
                                Boolean checkKeyFound = false;
                                
                                Set<TermsOfService> termsOfServiceList = userOut.getPersonalData().getTermsOfServiceData();
                                for(TermsOfService termsOfService : termsOfServiceList) {
                                    
                                    if (termsOfService.getKeyval().equals(checkKey)) {
                                        checkKeyFound = true;
                                        //System.out.println(checkKey + " gi� presente");
                                        break;
                                    }
                                }
                                
                                if (!checkKeyFound) {
                                    
                                    //System.out.println(checkKey + " non trovato");
                                    
                                    TermsOfService termsOfServiceNew = new TermsOfService();
                                    termsOfServiceNew.setAccepted(false);
                                    termsOfServiceNew.setKeyval(checkKey);
                                    termsOfServiceNew.setPersonalData(userOut.getPersonalData());
                                    termsOfServiceNew.setValid(Boolean.FALSE);
                                    
                                    userOut.getPersonalData().getTermsOfServiceData().add(termsOfServiceNew);
                                    
                                    //System.out.println(checkKey + " inserito");
                                }
                            }
                        }
                    }
                }
                else {
                    
                    System.out.println("Trovato documento " + documentBean.getDocumentkey() + " con groupCategory non nullo: " + documentBean.getGroupCategory());
                }
            }
            
            userOut.setUserStatus(outputStatus);
            
            Boolean loyaltyCheckEnabled = Boolean.TRUE;
            
            System.out.println("Verifica loyaltyCheckEnabled - createDate: " + userOut.getCreateDate().getTime() + ", deployDate: " + deployDateLong);
            
            if ((userOut.getEniStationUserType() != null && userOut.getEniStationUserType() == Boolean.TRUE) || userOut.getCreateDate().getTime() <= deployDateLong) {
                loyaltyCheckEnabled = Boolean.FALSE;
            }
            userOut.setLoyaltyCheckEnabled(loyaltyCheckEnabled);
            socialAuthenticationResponse.setUser(userOut);

            userTransaction.commit();

            return socialAuthenticationResponse;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                System.err.println("IllegalStateException: " + e.getMessage());
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                System.err.println("SecurityException: " + e.getMessage());
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                System.err.println("SystemException: " + e.getMessage());
            }

            String message = "FAILED authentication with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

}
