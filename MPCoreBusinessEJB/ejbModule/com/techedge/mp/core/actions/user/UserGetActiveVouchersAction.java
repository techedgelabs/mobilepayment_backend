package com.techedge.mp.core.actions.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GetActiveVouchersData;
import com.techedge.mp.core.business.interfaces.PromotionType;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltySessionBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserGetActiveVouchersAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserGetActiveVouchersAction() {}

    public GetActiveVouchersData execute(String ticketId, String requestId, Boolean refresh, FidelityServiceRemote fidelityService, String icon, String iconMastercard, 
            String iconMakerfaire2017, String iconMotorshow2017, String loyaltySessionID, Integer loyaltySessionExpiryTime) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                return createMessageError(requestId, userTransaction, ErrorLevel.INFO, "execute", "Invalid ticket", ResponseHelper.USER_GET_ACTIVE_VOUCHERS_INVALID_TICKET);
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Utente non valido
                return createMessageError(requestId, userTransaction, ErrorLevel.INFO, "execute", "User not found", ResponseHelper.USER_GET_ACTIVE_VOUCHERS_INVALID_TICKET);

            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                return createMessageError(requestId, userTransaction, ErrorLevel.INFO, "execute", "Unable to get active vouchers in status " + String.valueOf(userStatus),
                        ResponseHelper.USER_GET_ACTIVE_VOUCHERS_UNAUTHORIZED);

            }
            
            if (loyaltySessionID != null) {
                LoyaltySessionBean loyaltySessionBean = QueryRepository.findLoyaltySessionBySessionid(em, loyaltySessionID);
                
                if (loyaltySessionBean != null) {
                    loyaltySessionBean.renew(loyaltySessionExpiryTime);
                    em.merge(loyaltySessionBean);
                    System.out.println("Rinnovata loyaltySessionID: " + loyaltySessionID);
                }
                else {
                    System.out.println("LoyaltySessionBean nullo. Rinnovo non fatto");
                }
            }
            else {
                System.out.println("loyaltySessionID nullo. Rinnovo non fatto");
            }

            // Estrazione dei voucher associati all'utente
            //Set<VoucherBean> voucherBeanList = userBean.getVoucherList();

            if (userBean.getVoucherList().isEmpty()) {

                // L'utente non ha voucher associati
                return createMessageError(requestId, userTransaction, ErrorLevel.INFO, "execute", "No voucher found", ResponseHelper.USER_GET_ACTIVE_VOUCHERS_SUCCESS);
            }
            else {

                if (refresh == true) {

                    // Prima di restituire i voucher bisogna verificarne lo stato

                    Date now = new Date();

                    String operationID = new IdGenerator().generateId(16).substring(0, 33);
                    PartnerType partnerType = PartnerType.MP;
                    VoucherConsumerType voucherConsumerType = VoucherConsumerType.ENI;
                    Long requestTimestamp = now.getTime();
                    Integer maxVoucherCount = 200;
                    Integer voucherCount = 0;

                    List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);

                    for (VoucherBean voucherBean : userBean.getVoucherList()) {

                        if (voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO) || voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_DA_CONFERMARE)) {

                            System.out.println("Voucher Bean Code " + voucherBean.getCode());
                            VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                            voucherCodeDetail.setVoucherCode(voucherBean.getCode());
                            voucherCodeList.add(voucherCodeDetail);
                            
                            voucherCount++;
                        }
                        
                        if (voucherCount >= maxVoucherCount) {
                            break;
                        }
                    }

                    if (voucherCodeList.isEmpty()) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "No voucher found");

                        userTransaction.commit();

                        GetActiveVouchersData getActiveVouchersData = new GetActiveVouchersData();
                        getActiveVouchersData.setStatusCode(ResponseHelper.USER_GET_ACTIVE_VOUCHERS_SUCCESS);

                        return getActiveVouchersData;
                    }

                    CheckVoucherResult checkVoucherResult = new CheckVoucherResult();

                    try {

                        checkVoucherResult = fidelityService.checkVoucher(operationID, voucherConsumerType, partnerType, requestTimestamp, voucherCodeList);
                    }
                    catch (Exception e) {

                        return createMessageError(requestId, userTransaction, ErrorLevel.ERROR, "execute", "Error checking vouchers",
                                ResponseHelper.USER_GET_ACTIVE_VOUCHERS_ERROR);
                    }

                    // Verifica l'esito del check
                    String checkVoucherStatusCode = checkVoucherResult.getStatusCode();

                    if (!checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_OK)) {
                        
                        if (checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_EMPTY_LIST)) {

                            // Max limit reached
                            return createMessageError(requestId, userTransaction, ErrorLevel.ERROR, "execute", "Error checking voucher: voucher list to check is empty",
                                    ResponseHelper.USER_GET_ACTIVE_VOUCHERS_EMPTY_LIST);
                        }
                        
                        if (checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_MAX_LIMIT_REACHED)) {

                            // Max limit reached
                            return createMessageError(requestId, userTransaction, ErrorLevel.ERROR, "execute", "Error checking voucher: max limit reached",
                                    ResponseHelper.USER_GET_ACTIVE_VOUCHERS_MAX_LIMIT_REACHED);
                        }
                            // Error
                        return createMessageError(requestId, userTransaction, ErrorLevel.ERROR, "execute", "Error checking voucher: " + checkVoucherStatusCode,
                                ResponseHelper.USER_GET_ACTIVE_VOUCHERS_GENERIC_ERROR);
                    }
                    else {

                        // Aggiorna lo stato dei voucher dell'utente
                        for (VoucherBean voucherBean : userBean.getVoucherList()) {

                            //System.out.println("Trovato voucher: " + voucherBean.getCode() + " in stato: " + voucherBean.getStatus());

                            if (voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO) || voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_ESAURITO)
                                    || voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_DA_CONFERMARE)) {

                                // Ricerca il voucher nella risposta del servizio di verifica
                                for (VoucherDetail voucherDetail : checkVoucherResult.getVoucherList()) {

                                    if (voucherDetail.getVoucherCode().equals(voucherBean.getCode())) {

                                        System.out.println("Voucher trovato nella risposta del servizio di check");

                                        String voucherIcon = icon;
                                        if (voucherDetail.getPromoCode().equals(PromotionType.MASTERCARD.getVoucherCode())) {
                                            voucherIcon = iconMastercard;
                                        }
                                        if (voucherDetail.getPromoCode().equals(PromotionType.MAKERFAIRE2017.getVoucherCode())) {
                                            voucherIcon = iconMakerfaire2017;
                                        }
                                        if (voucherDetail.getPromoCode().equals(PromotionType.MOTORSHOW2017.getVoucherCode())) {
                                            voucherIcon = iconMotorshow2017;
                                        }
                                        
                                        // Aggiorna il voucher con i dati restituiti dal servizio
                                        updateVoucher(voucherBean, voucherDetail, voucherIcon);

                                        System.out.println("Voucher aggiornato");
                                    }
                                }
                            }
                            else {

                                //System.out.println("Voucher non valido");
                            }
                        }

                        // Aggiorna l'utente
                        em.merge(userBean);

                        // Restituisci i voucher in output
                        return returnVoucher(userTransaction, userBean, icon, iconMastercard, iconMakerfaire2017, iconMotorshow2017);
                    }
                }
                else {

                    return returnVoucher(userTransaction, userBean, icon, iconMastercard, iconMakerfaire2017, iconMotorshow2017);
                }

            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get active vouchers with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private GetActiveVouchersData returnVoucher(UserTransaction userTransaction, UserBean userBean, String icon, String iconMastercard,
            String iconMakerfaire2017, String iconMotorshow2017) throws RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException {

        GetActiveVouchersData getActiveVouchersData = new GetActiveVouchersData();

        System.out.println("Creazione della risposta");

        Double total = 0.0;

        for (VoucherBean voucherBean : userBean.getVoucherList()) {

            //System.out.println("Trovato voucher: " + voucherBean.getCode());

            if (voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO)) {

                //System.out.println("Voucher valido");
                
                String voucherIcon = icon;
                if (voucherBean.getPromoCode().equals(PromotionType.MASTERCARD.getVoucherCode())) {
                    voucherIcon = iconMastercard;
                }
                if (voucherBean.getPromoCode().equals(PromotionType.MAKERFAIRE2017.getVoucherCode())) {
                    voucherIcon = iconMakerfaire2017;
                }
                if (voucherBean.getPromoCode().equals(PromotionType.MOTORSHOW2017.getVoucherCode())) {
                    voucherIcon = iconMotorshow2017;
                }

                Voucher voucher = createVoucher(voucherBean, voucherIcon);

                total = total + voucherBean.getVoucherBalanceDue();

                if (voucher.getPromoPartner() != null && voucher.getPromoPartner().equals("MYCICERO")) {
                    getActiveVouchersData.getParkingVouchers().add(voucher);
                }
                else {
                    getActiveVouchersData.getVouchers().add(voucher);
                }
            }
        }

        userTransaction.commit();

        getActiveVouchersData.sortVouchers();
        getActiveVouchersData.setTotal(total);
        getActiveVouchersData.setStatusCode(ResponseHelper.USER_GET_ACTIVE_VOUCHERS_SUCCESS);

        return getActiveVouchersData;
    }

    private GetActiveVouchersData createMessageError(String requestId, UserTransaction userTransaction, ErrorLevel errorLevel, String methodName, String message, String statusCode)
            throws Exception {

        this.loggerService.log(errorLevel, this.getClass().getSimpleName(), methodName, requestId, null, message);

        userTransaction.commit();

        GetActiveVouchersData getActiveVouchersData = new GetActiveVouchersData();
        getActiveVouchersData.setStatusCode(statusCode);

        return getActiveVouchersData;
    }

    private Voucher createVoucher(VoucherBean voucherBean, String icon) {

        Voucher voucher = new Voucher();

        voucher.setConsumedValue(voucherBean.getConsumedValue());
        voucher.setExpirationDate(voucherBean.getExpirationDate());
        voucher.setInitialValue(voucherBean.getInitialValue());
        voucher.setPromoCode(voucherBean.getPromoCode());
        voucher.setPromoDescription(voucherBean.getPromoDescription());
        voucher.setPromoDoc(voucherBean.getPromoDoc());
        voucher.setVoucherBalanceDue(voucherBean.getVoucherBalanceDue());
        voucher.setCode(voucherBean.getCode());
        voucher.setStatus(voucherBean.getStatus());
        voucher.setType(voucherBean.getType());
        voucher.setValue(voucherBean.getValue());
        voucher.setIsCombinable(voucherBean.getIsCombinable());
        voucher.setMinAmount(voucherBean.getMinAmount());
        voucher.setMinQuantity(voucherBean.getMinQuantity());
        voucher.setPromoPartner(voucherBean.getPromoPartner());
        voucher.setVoucherBalanceDue(voucherBean.getVoucherBalanceDue());
        voucher.setValidPV(voucherBean.getValidPV());
        voucher.setIcon(icon);

        return voucher;
    }

    private void updateVoucher(VoucherBean voucherBean, VoucherDetail voucherDetail, String icon) {

        double consumedValue = voucherDetail.getConsumedValue().doubleValue();

        if (consumedValue == 0.0) {
            consumedValue = voucherDetail.getVoucherValue().doubleValue() - voucherDetail.getVoucherBalanceDue().doubleValue();
        }

        voucherBean.setConsumedValue(consumedValue);
        voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
        voucherBean.setInitialValue(voucherDetail.getInitialValue());
        voucherBean.setPromoCode(voucherDetail.getPromoCode());
        voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
        voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
        voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
        voucherBean.setCode(voucherDetail.getVoucherCode());
        voucherBean.setStatus(voucherDetail.getVoucherStatus());
        voucherBean.setType(voucherDetail.getVoucherType());
        voucherBean.setValue(voucherDetail.getVoucherValue());
        voucherBean.setMinAmount(voucherDetail.getMinAmount());
        voucherBean.setMinQuantity(voucherDetail.getMinQuantity());
        voucherBean.setIsCombinable(voucherDetail.getIsCombinable());
        voucherBean.setValidPV(voucherDetail.getValidPV());
        voucherBean.setIcon(icon);
        /*
         * for (ProductDetail productDetail : voucherDetail.getValidProduct()) {
         * voucherBean.getValidProducts().add(productDetail.getProductCode());
         * }
         * 
         * for (RefuelMode refuelMode : voucherDetail.getValidRefuelMode()) {
         * voucherBean.getValidRefuelMode().add(refuelMode.getValue());
         * }
         */
    }
}
