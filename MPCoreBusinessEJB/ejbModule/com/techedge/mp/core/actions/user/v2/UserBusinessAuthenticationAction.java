package com.techedge.mp.core.actions.user.v2;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.AuthenticationBusinessResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SendValidationResult;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.interfaces.user.UserDevice;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.model.LastLoginDataBean;
import com.techedge.mp.core.business.model.LastLoginErrorDataBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.UserDeviceBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.QueryRepositoryBusiness;
import com.techedge.mp.core.business.utilities.ResendValidation;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserBusinessAuthenticationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserBusinessAuthenticationAction() {}

    public AuthenticationBusinessResponse execute(String email, String passwordHash, String privateKeyPEM, String requestId, String deviceId, String deviceName, long maxPendingInterval, 
            Integer ticketExpiryTime, Integer loginAttemptsLimit, Integer loginLockExpiryTime, UserCategoryService userCategoryService, Integer maxRetryAttemps, String sendingType, 
            EmailSenderRemote emailSender, SmsServiceRemote smsService, String source) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            AuthenticationBusinessResponse authenticationResponse = null;

            UserBean userBean = QueryRepositoryBusiness.findNotCancelledUserBusinessByEmail(em, email);
            
            if (userBean == null) {

                if (Objects.equals(source, "ENISTATION+")) {
                    authenticationResponse = authenticationCustomer(email, passwordHash, requestId, ticketExpiryTime, loginAttemptsLimit, userTransaction);
                    
                    userTransaction.commit();

                    return authenticationResponse;
                }
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Login non valido: " + email);
                authenticationResponse = new AuthenticationBusinessResponse();
                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);
                
                userTransaction.commit();

                return authenticationResponse;
            }
            
            if (!Objects.equals(source, userBean.getSource())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Utente con source invalido: " + email);
                authenticationResponse = new AuthenticationBusinessResponse();
                authenticationResponse.setStatusCode(ResponseHelper.USER_BUSINESS_AUTH_INVALID_ENISTATION_USER);
                
                userTransaction.commit();

                return authenticationResponse;
            }
            
            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus == User.USER_STATUS_BLOCKED || userStatus == User.USER_STATUS_NEW || userStatus == User.USER_STATUS_CANCELLED) {

                // Un utente che si trova in questo stato non pu� effettuare il login
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User " + email + " in status "
                        + userStatus);
                authenticationResponse = new AuthenticationBusinessResponse();
                if (userStatus == User.USER_STATUS_NEW) {
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_NOT_VERIFIED);
                }
                else {
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);
                }

                userTransaction.commit();

                return authenticationResponse;
            }
            
            Integer userType = userBean.getUserType();
            Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());

            if (!useBusiness && userBean.getUserType() != User.USER_TYPE_SERVICE) {

                // L'utente non è del tipo BUSINESS e non pu� effutare l'accesso
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User " + email + " (type: " + userBean.getUserType() + ")") ;
                authenticationResponse = new AuthenticationBusinessResponse();
                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);

                userTransaction.commit();

                return authenticationResponse;
            }

            if (userBean.getUserType() == User.USER_TYPE_SERVICE) {

                // Per ottimizzare il flusso di autenticazione i controlli sul
                // lastLoginData e LastoLoginErrorData non sono effettuati sull'utente
                // di sistema
                if (userBean.getPersonalDataBean().getSecurityDataPassword().equals(passwordHash)) {

                    // 1) crea un nuovo ticket
                    User user = userBean.toUser();

                    Integer ticketType = TicketBean.TICKET_TYPE_SERVICE;

                    Date now = new Date();
                    Date lastUsed = now;
                    Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

                    String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

                    TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);

                    em.persist(ticketBean);

                    // 2) restituisci il risultato
                    authenticationResponse = new AuthenticationBusinessResponse();

                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
                    authenticationResponse.setTicketId(ticketBean.getTicketId());
                    authenticationResponse.setUser(user);

                    userTransaction.commit();

                    return authenticationResponse;
                }
                else {

                    // La password inserita non è corretta
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong password for user " + email);
                    authenticationResponse = new AuthenticationBusinessResponse();
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);

                    userTransaction.commit();

                    return authenticationResponse;
                }
            }
            else {

                Date checkDate = new Date();
                String deviceFamily = requestId.substring(0, 3).toUpperCase();
                LastLoginErrorDataBean lastLoginErrorDataBean = userBean.getLastLoginErrorDataBean();

                if (userBean.getPersonalDataBean().getSecurityDataPassword().equals(passwordHash)) {

                    // Controllare se l'utente ha superato il numero di tentativi ed � ancora nell'elapsed time;
                    if (lastLoginErrorDataBean != null && lastLoginErrorDataBean.getAttempt() > loginAttemptsLimit && lastLoginErrorDataBean.getTime().after(checkDate)) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Login Locked access: User " + email + " in status "
                                + userStatus);
                        authenticationResponse = new AuthenticationBusinessResponse();
                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_LOCKED);

                        userTransaction.commit();

                        return authenticationResponse;
                    }

                    // 1) crea un nuovo ticket
                    User user = userBean.toUser();

                    Integer ticketType = TicketBean.TICKET_TYPE_CUSTOMER;
                    Date now = new Date();
                    Date lastUsed = now;
                    Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

                    String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

                    TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);

                    em.persist(ticketBean);

                    // 2 e 3 Sostituzione vecchio lastLoginData o creazione nuovo
                    Boolean userStatusSet = false;
                    Integer outputStatus = User.USER_STATUS_VERIFIED;
                    Boolean deviceFound = false;
                    MobilePhoneBean activeMobilePhone = userBean.activeMobilePhone();

                    if (userBean.getUserStatus() == User.USER_STATUS_VERIFIED) {

                        SendValidationResult result = new SendValidationResult();
                        UserDeviceBean userDeviceBean = new UserDeviceBean();

                        // Verifica se l'utente ha gi� associato un dispositivo con il device id passato nella richiesta
                        Set<UserDeviceBean> userDeviceBeanList = userBean.getUserDeviceBean();

                        if (userDeviceBeanList != null) {

                            for (UserDeviceBean item : userDeviceBeanList) {
                                
                                // Esamina i device che non sono stati cancellati
                                if (item.getDeviceId().equals(deviceId) && item.getStatus() != UserDevice.USER_DEVICE_STATUS_CANCELLED) {
                                    
                                    if (userBean.getPersonalDataBean().getSecurityDataEmail().equals("demo@demo.com")) {
                                        
                                        System.out.println("Verifica automatica device");
                                        
                                        item.setStatus(UserDevice.USER_DEVICE_STATUS_VERIFIED);
                                        
                                        deviceFound = true;
                                        
                                        continue;
                                    }
                                    
                                    if (item.getStatus() == UserDevice.USER_DEVICE_STATUS_VERIFIED) {
                                        deviceFound = true;
                                    }
                                    
                                    // Se il device si trova in stato pending rimanda l'sms per la verifica (TODO capire se è necessario impostare un numero massimo di sms)
                                    if (item.getStatus() == UserDevice.USER_DEVICE_STATUS_PENDING) {

                                        //System.out.println("Trovato stato pending");
                                        deviceFound = true;

                                        if (activeMobilePhone != null && activeMobilePhone.getNumber() != null) {
                                            outputStatus = User.USER_STATUS_DEVICE_TO_VERIFIED;

                                            userDeviceBean = item;
                                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                                                    "Send SMS verification for device " + userDeviceBean.getDeviceId());

                                            // Invio sms di conferma
                                            result = ResendValidation.sendForDevice(sendingType, maxRetryAttemps, userBean, userDeviceBean, loggerService, emailSender, 
                                                    smsService, userCategoryService, null);
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if (!deviceFound) {

                            //System.out.println("Device non trovato");

                            //System.out.println("Crea il nuovo device");

                            // Crea un nuovo device e genera il codice di verifica e invialo via sms
                            String verificationCode = new IdGenerator().generateNumericId(8);

                            Date currentDate = new Timestamp((new Date()).getTime());
                            
                            userDeviceBean.setCreationTimestamp(currentDate);
                            userDeviceBean.setAssociationTimestamp(null);
                            userDeviceBean.setLastUsedTimestamp(currentDate);

                            //System.out.println("Generato codice di verifica " + verificationCode + " per device");

                            userDeviceBean.setVerificationCode(verificationCode);
                            userDeviceBean.setDeviceId(deviceId);
                            userDeviceBean.setDeviceName(deviceName);
                            userDeviceBean.setDeviceFamily(deviceFamily);
                            userDeviceBean.setDeviceEndpoint(null);
                            userDeviceBean.setDeviceToken(null);
                            
                            
                            userDeviceBean.setStatus(UserDevice.USER_DEVICE_STATUS_PENDING);
                            
                            if (userBean.getPersonalDataBean().getSecurityDataEmail().equals("demo@demo.com")) {
                                userDeviceBean.setStatus(UserDevice.USER_DEVICE_STATUS_VERIFIED);
                                System.out.println("Utente speciale demo@demo.com verifica automatica device");
                                
                                userBean.getUserDeviceBean().add(userDeviceBean);
                                userDeviceBean.setUserBean(userBean);
                            }
                            else {
                                
                                userBean.getUserDeviceBean().add(userDeviceBean);
                                userDeviceBean.setUserBean(userBean);
                                
                                //em.persist(userDeviceBean);
                                
                                if (activeMobilePhone != null && activeMobilePhone.getNumber() != null) {
    
                                    outputStatus = User.USER_STATUS_DEVICE_TO_VERIFIED;
    
                                    //Invio sms di conferma
                                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Send SMS verification for device "
                                            + userDeviceBean.getDeviceId());
    
                                    result = ResendValidation.sendForDevice(sendingType, maxRetryAttemps, userBean, userDeviceBean, loggerService, emailSender, 
                                            smsService, userCategoryService, null);
                                
                                    authenticationResponse = new AuthenticationBusinessResponse();
        
                                    if (result != null && result.getStatusCode() != null && result.getStatusCode().equals(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE)) {
        
                                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error sending verification mobile phone. "
                                                + result.getMessageCode() + " (" + result.getErrorCode() + ")");
        
                                        userTransaction.commit();
        
                                        authenticationResponse.setStatusCode(ResponseHelper.MOBILE_PHONE_UPDATE_SENDING_FAILURE);
                                        authenticationResponse.setTicketId(ticketBean.getTicketId());
                                        authenticationResponse.setUser(userBean.toUser());
                                        return authenticationResponse;
                                    }
                                }
                            }
                        }
                    }

                    LastLoginDataBean lastLoginDataBean = userBean.getLastLoginDataBean();
                    if (lastLoginDataBean != null) {

                        userBean.getLastLoginDataBean().setDeviceId(deviceId);
                        userBean.getLastLoginDataBean().setDeviceName(deviceName);
                        userBean.getLastLoginDataBean().setDeviceToken(null);
                        userBean.getLastLoginDataBean().setDeviceFamily(deviceFamily);
                        userBean.getLastLoginDataBean().setTime(new Timestamp((new Date()).getTime()));
                    }
                    else {

                        lastLoginDataBean = new LastLoginDataBean();
                        lastLoginDataBean.setDeviceId(deviceId);
                        lastLoginDataBean.setDeviceName(deviceName);
                        lastLoginDataBean.setDeviceToken(null);
                        lastLoginDataBean.setDeviceFamily(deviceFamily);
                        lastLoginDataBean.setDeviceEndpoint(null);

                        lastLoginDataBean.setTime(new Timestamp((new Date()).getTime()));
                        em.persist(lastLoginDataBean);

                        userBean.setLastLoginDataBean(lastLoginDataBean);
                    }

                    // Cancellazione delle informazioni per il recupero della password
                    PersonalDataBean personalDataBean = userBean.getPersonalDataBean();
                    personalDataBean.setExpirationDateRescuePassword(null);
                    personalDataBean.setRescuePassword(null);
                    // TODO verificare se serve em.merge(personalDataBean);

                    if (userStatusSet || userBean.getUserStatus() == User.USER_STATUS_TEMPORARY_PASSWORD) {
                        userBean.setUserStatus(User.USER_STATUS_VERIFIED);
                        user.setUserStatus(User.USER_STATUS_VERIFIED);
                    }
                    if (lastLoginErrorDataBean != null) {
                        userBean.getLastLoginErrorDataBean().resetAttempts();
                    }

                    em.merge(userBean);

                    // 4) restituisci il risultato
                    authenticationResponse = new AuthenticationBusinessResponse();

                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
                    authenticationResponse.setTicketId(ticketBean.getTicketId());

                    //System.out.println("id: " + userBean.getId());

                    User userOut = userBean.toUser();
                    
                    // Controlla se ci sono nuovi elementi nellla tabella documenti che l'utente non ha ancora accettato
                    UserCategoryBean userCategoryBean = QueryRepository.findUserCategoryByName(em, UserCategoryType.BUSINESS.getCode());
                    
                    // Ricerca gli elementi della tabella documenti associati alla categoria 3 o con categoria null
                    List<DocumentBean> documentBeanList = QueryRepository.findDocumentAll(em);
                    for(DocumentBean documentBean : documentBeanList) {
                        
                        // Si escludono dall'elaborazione i documenti con groupCategory non null
                        if (documentBean.getGroupCategory() == null || documentBean.getGroupCategory().isEmpty()) {
                        
                            if (documentBean.getUserCategory() == null || documentBean.getUserCategory().equals(String.valueOf(userCategoryBean.getId()))) {
                                
                                List<DocumentAttributeBean> documentAttributeBeanList = documentBean.getDocumentCheck();
                                
                                for(DocumentAttributeBean documentAttributeBean : documentAttributeBeanList) {
                                    
                                    //System.out.println("In verifica " + documentAttributeBean.getCheckKey());
                                    
                                    if ((documentAttributeBean.getConditionText() != null && !documentAttributeBean.getConditionText().isEmpty()) ||
                                        (documentAttributeBean.getSubTitle()      != null && !documentAttributeBean.getSubTitle().isEmpty())) {
                                        
                                        //System.out.println("In elaborazione " + documentAttributeBean.getCheckKey());
                                        
                                        // Se l'elemento non � presente tra i termini e condizioni associati all'utente allora va inserito con valid a false
                                        String checkKey = documentAttributeBean.getCheckKey();
                                        
                                        Boolean checkKeyFound = false;
                                        
                                        Set<TermsOfService> termsOfServiceList = userOut.getPersonalData().getTermsOfServiceData();
                                        for(TermsOfService termsOfService : termsOfServiceList) {
                                            
                                            if (termsOfService.getKeyval().equals(checkKey)) {
                                                checkKeyFound = true;
                                                //System.out.println(checkKey + " giÃ  presente");
                                                break;
                                            }
                                        }
                                        
                                        if (!checkKeyFound) {
                                            
                                            //System.out.println(checkKey + " non trovato");
                                            
                                            TermsOfService termsOfServiceNew = new TermsOfService();
                                            termsOfServiceNew.setAccepted(false);
                                            termsOfServiceNew.setKeyval(checkKey);
                                            termsOfServiceNew.setPersonalData(userOut.getPersonalData());
                                            termsOfServiceNew.setValid(Boolean.FALSE);
                                            
                                            userOut.getPersonalData().getTermsOfServiceData().add(termsOfServiceNew);
                                            
                                            //System.out.println(checkKey + " inserito");
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                    boolean paymentMethodFound = false;
                    
                    if (!userBean.getUserStatusRegistrationCompleted() && (userBean.getSource() != null && userBean.getSource().equals("ENISTATION+"))) {
                        UserBean userCustomerBean = QueryRepository.findNotCancelledUserCustomerByEmail(em, email);

                        if (userCustomerBean != null) {
                            paymentMethodFound = !userCustomerBean.getPaymentMethodTypeCreditCardList().isEmpty();
                        }
                    }
                    
                    userOut.setSourcePaymentMethodFound(paymentMethodFound);

                    userOut.setUserStatus(outputStatus);
                    
                    authenticationResponse.setUser(userOut);

                    userTransaction.commit();

                    return authenticationResponse;
                }
                else {

                    if (userBean.getPersonalDataBean().getExpirationDateRescuePassword() != null
                            && userBean.getPersonalDataBean().getExpirationDateRescuePassword().after(checkDate)
                            && userBean.getPersonalDataBean().getRescuePassword().equals(passwordHash)) {

                        // Tutti i ticket attivi associati all'utente devono essere
                        // invalidati
                        List<TicketBean> ticketBeanList = QueryRepository.findTicketByUserAndCheckDate(em, userBean, checkDate);

                        if (ticketBeanList != null && ticketBeanList.size() > 0) {

                            for (TicketBean ticketBeanLoop : ticketBeanList) {

                                ticketBeanLoop.setExpirationTimestamp(checkDate);
                                em.merge(ticketBeanLoop);
                            }
                        }

                        User user = userBean.toUser();

                        Integer ticketType = TicketBean.TICKET_TYPE_CUSTOMER;
                        Date now = new Date();
                        Date lastUsed = now;
                        Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

                        String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

                        TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);

                        em.persist(ticketBean);

                        // 4) aggiorna i dati lastLoginDevice e lastLoginTime con i valori
                        // passati in input
                        LastLoginDataBean lastLoginDataBean = userBean.getLastLoginDataBean();
                        if (lastLoginDataBean != null) {

                            lastLoginDataBean.setDeviceId(deviceId);
                            lastLoginDataBean.setDeviceName(deviceName);
                            lastLoginDataBean.setDeviceToken(null);
                            lastLoginDataBean.setDeviceFamily(deviceFamily);
                            lastLoginDataBean.setDeviceEndpoint(null);
                            lastLoginDataBean.setTime(new Timestamp((new Date()).getTime()));
                        }
                        else {
                            lastLoginDataBean = new LastLoginDataBean();
                            lastLoginDataBean.setDeviceId(deviceId);
                            lastLoginDataBean.setDeviceName(deviceName);
                            lastLoginDataBean.setDeviceToken(null);
                            lastLoginDataBean.setDeviceFamily(deviceFamily);
                            lastLoginDataBean.setTime(new Timestamp((new Date()).getTime()));
                            em.persist(lastLoginDataBean);

                            userBean.setLastLoginDataBean(lastLoginDataBean);
                        }

                        userBean.setUserStatus(User.USER_STATUS_TEMPORARY_PASSWORD);
                        user.setUserStatus(User.USER_STATUS_TEMPORARY_PASSWORD);

                        if (lastLoginErrorDataBean != null) {
                            userBean.getLastLoginErrorDataBean().resetAttempts();
                        }

                        em.merge(userBean);

                        // 5) restituisci il risultato
                        authenticationResponse = new AuthenticationBusinessResponse();

                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
                        authenticationResponse.setTicketId(ticketBean.getTicketId());
                        authenticationResponse.setUser(user);

                        userTransaction.commit();

                        return authenticationResponse;
                    }
                    else {

                        // Aggiornamento dei tentativi di accesso dell'utente;
                        if (lastLoginErrorDataBean != null) {

                            //System.out.println("lastLoginErrorDataBean != null");
                            userBean.getLastLoginErrorDataBean().addAttempt();

                            if (userBean.getLastLoginErrorDataBean().getAttempt() > loginAttemptsLimit) {

                                Date now = new Date();
                                Date expiryDate = DateHelper.addMinutesToDate(loginLockExpiryTime, now);
                                userBean.getLastLoginErrorDataBean().setTime(expiryDate);
                            }
                            //System.out.println("attempt: " + userBean.getLastLoginErrorDataBean().getAttempt());
                        }
                        else {

                            //System.out.println("lastLoginErrorDataBean == null");
                            lastLoginErrorDataBean = new LastLoginErrorDataBean();
                            lastLoginErrorDataBean.addAttempt();

                            em.persist(lastLoginErrorDataBean);

                            userBean.setLastLoginErrorDataBean(lastLoginErrorDataBean);
                        }

                        em.merge(userBean);
                        
                        
                        // La password inserita non � corretta
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong password for user " + email);

                        String statusCode = ResponseHelper.USER_AUTH_LOGIN_ERROR;
                        
                        if (Objects.equals(source, "ENISTATION+")) {
                            statusCode = ResponseHelper.USER_BUSINESS_AUTH_INVALID_ENISTATION_USER;
                        }
                        
                        authenticationResponse = new AuthenticationBusinessResponse();
                        authenticationResponse.setStatusCode(statusCode);

                        userTransaction.commit();

                        return authenticationResponse;
                    }
                }
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                System.err.println("IllegalStateException: " + e.getMessage());
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                System.err.println("SecurityException: " + e.getMessage());
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                System.err.println("SystemException: " + e.getMessage());
            }

            String message = "FAILED authentication with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
    
    private AuthenticationBusinessResponse authenticationCustomer(String email, String passwordHash, String requestId, Integer ticketExpiryTime, Integer loginAttemptsLimit, 
            UserTransaction userTransaction) throws Exception {
        AuthenticationBusinessResponse authenticationResponse = new AuthenticationBusinessResponse();
        
        UserBean userBean = QueryRepository.findNotCancelledUserCustomerByEmail(em, email);
        
        if (userBean == null) {

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Login non valido: " + email);
            authenticationResponse = new AuthenticationBusinessResponse();
            authenticationResponse.setStatusCode(ResponseHelper.USER_BUSINESS_AUTH_INVALID_ENISTATION_USER);

            return authenticationResponse;
        }
        
        // Verifica lo stato dell'utente
        Integer userStatus = userBean.getUserStatus();
        if (userStatus == User.USER_STATUS_BLOCKED || userStatus == User.USER_STATUS_NEW || userStatus == User.USER_STATUS_CANCELLED) {

            // Un utente che si trova in questo stato non pu� effettuare il login
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User " + email + " in status "
                    + userStatus);
            authenticationResponse = new AuthenticationBusinessResponse();
            if (userStatus == User.USER_STATUS_NEW) {
                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_NOT_VERIFIED);
            }
            else {
                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);
            }

            return authenticationResponse;
        }
        
        if (!userBean.getPersonalDataBean().getSecurityDataPassword().equals(passwordHash)) {
            // La password inserita non � corretta
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong password for user " + email);

            authenticationResponse = new AuthenticationBusinessResponse();
            authenticationResponse.setStatusCode(ResponseHelper.USER_BUSINESS_AUTH_INVALID_ENISTATION_USER);

            return authenticationResponse;
        }
        else {
            LastLoginErrorDataBean lastLoginErrorDataBean = userBean.getLastLoginErrorDataBean();
            Date checkDate = new Date();
            // Controllare se l'utente ha superato il numero di tentativi ed � ancora nell'elapsed time;
            if (lastLoginErrorDataBean != null && lastLoginErrorDataBean.getAttempt() > loginAttemptsLimit && lastLoginErrorDataBean.getTime().after(checkDate)) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Login Locked access: User " + email + " in status "
                        + userStatus);
                authenticationResponse = new AuthenticationBusinessResponse();
                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_LOCKED);

                return authenticationResponse;
            }
        }
        
        User user = userBean.toUser();
        user.setUserStatus(User.USER_STATUS_MIGRATING_BUSINESS);
        user.setSource("ENISTATION+");
        
        boolean paymentMethodFound = !userBean.getPaymentMethodTypeCreditCardList().isEmpty();        
        user.setSourcePaymentMethodFound(paymentMethodFound);
        
        Date now = new Date();
        Date lastUsed = now;
        Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

        String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

        TicketBean sourceToken = new TicketBean(newTicketId, userBean, TicketBean.TICKET_TYPE_BUSINESS, now, lastUsed, expiryDate);

        em.persist(sourceToken);
        
        authenticationResponse.setSourceToken(sourceToken.getTicketId());
        authenticationResponse.setUser(user);
        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
        return authenticationResponse;
    }

}
