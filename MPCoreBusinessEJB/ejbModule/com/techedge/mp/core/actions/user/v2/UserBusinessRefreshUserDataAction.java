package com.techedge.mp.core.actions.user.v2;

import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AuthenticationBusinessResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserBusinessRefreshUserDataAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserBusinessRefreshUserDataAction() {}

    public AuthenticationBusinessResponse execute(String ticketId, String requestId, Integer ticketExpiryTime) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            AuthenticationBusinessResponse authenticationResponse = null;
            
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                authenticationResponse = new AuthenticationBusinessResponse();
                
                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_INVALID_TICKET);
                
                return authenticationResponse;
            }

            UserBean userBean = ticketBean.getUser();
            
            String email = userBean.getPersonalDataBean().getSecurityDataEmail();

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus == User.USER_STATUS_BLOCKED || userStatus == User.USER_STATUS_NEW || userStatus == User.USER_STATUS_CANCELLED) {

                // Un utente che si trova in questo stato non pu� effettuare il login
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User " + email + " in status "
                        + userStatus);
                authenticationResponse = new AuthenticationBusinessResponse();
                if (userStatus == User.USER_STATUS_NEW) {
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_NOT_VERIFIED);
                }
                else {
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_UNAUTHORIZED);
                }

                userTransaction.commit();

                return authenticationResponse;
            }

            if (userBean.getUserType() == User.USER_TYPE_REFUELING || userBean.getUserType() == User.USER_TYPE_REFUELING_NEW_ACQUIRER) {

                // L'utente � di tipo refueling e non pu� effutare l'accesso
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User " + email);
                authenticationResponse = new AuthenticationBusinessResponse();
                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_INVALID_TICKET);

                userTransaction.commit();

                return authenticationResponse;
            }


            // Rinnovo ticket
            
            ticketBean.renew();
            em.merge(ticketBean);


            em.merge(userBean);

            // 4) restituisci il risultato
            authenticationResponse = new AuthenticationBusinessResponse();

            authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
            authenticationResponse.setTicketId(ticketBean.getTicketId());

            //System.out.println("id: " + userBean.getId());

            User userOut = userBean.toUser();
            
            // Controlla se ci sono nuovi elementi nella tabella documenti che l'utente non ha ancora accettato
            UserCategoryBean userCategoryBean = null;
            if (userBean.getUserType() == User.USER_TYPE_GUEST) {
                userCategoryBean = QueryRepository.findUserCategoryByName(em, UserCategoryType.GUEST_FLOW.getCode());
            }
            else {
                userCategoryBean = QueryRepository.findUserCategoryByName(em, UserCategoryType.BUSINESS.getCode());
            }
            
            // Ricerca gli elementi della tabella documenti associati alla categoria 3 o con categoria null
            List<DocumentBean> documentBeanList = QueryRepository.findDocumentAll(em);
            for(DocumentBean documentBean : documentBeanList) {
                
                // Si escludono dall'elaborazione i documenti con groupCategory non null
                if (documentBean.getGroupCategory() == null || documentBean.getGroupCategory().isEmpty()) {
                
                    if (documentBean.getUserCategory() == null || documentBean.getUserCategory().equals(String.valueOf(userCategoryBean.getId()))) {
                        
                        List<DocumentAttributeBean> documentAttributeBeanList = documentBean.getDocumentCheck();
                        
                        for(DocumentAttributeBean documentAttributeBean : documentAttributeBeanList) {
                            
                            //System.out.println("In verifica " + documentAttributeBean.getCheckKey());
                            
                            if ((documentAttributeBean.getConditionText() != null && !documentAttributeBean.getConditionText().isEmpty()) ||
                                (documentAttributeBean.getSubTitle()      != null && !documentAttributeBean.getSubTitle().isEmpty())) {
                                
                                //System.out.println("In elaborazione " + documentAttributeBean.getCheckKey());
                                
                                // Se l'elemento non � presente tra i termini e condizioni associati all'utente allora va inserito con valid a false
                                String checkKey = documentAttributeBean.getCheckKey();
                                
                                Boolean checkKeyFound = false;
                                
                                Set<TermsOfService> termsOfServiceList = userOut.getPersonalData().getTermsOfServiceData();
                                for(TermsOfService termsOfService : termsOfServiceList) {
                                    
                                    if (termsOfService.getKeyval().equals(checkKey)) {
                                        checkKeyFound = true;
                                        //System.out.println(checkKey + " gi� presente");
                                        break;
                                    }
                                }
                                
                                if (!checkKeyFound) {
                                    
                                    //System.out.println(checkKey + " non trovato");
                                    
                                    TermsOfService termsOfServiceNew = new TermsOfService();
                                    termsOfServiceNew.setAccepted(false);
                                    termsOfServiceNew.setKeyval(checkKey);
                                    termsOfServiceNew.setPersonalData(userOut.getPersonalData());
                                    termsOfServiceNew.setValid(Boolean.FALSE);
                                    
                                    userOut.getPersonalData().getTermsOfServiceData().add(termsOfServiceNew);
                                    
                                    //System.out.println(checkKey + " inserito");
                                }
                            }
                        }
                    }
                }
                else {
                    
                    //System.out.println("Trovato documento " + documentBean.getDocumentkey() + " con groupCategory non nullo: " + documentBean.getGroupCategory());
                }
            }
            
            boolean paymentMethodFound = false;
            String sourceTokenId = null;
            
            if (!userBean.getUserStatusRegistrationCompleted() && (userBean.getSource() != null && userBean.getSource().equals("ENISTATION+"))) {
                UserBean userCustomerBean = QueryRepository.findNotCancelledUserCustomerByEmail(em, email);

                if (userCustomerBean != null) {
                    paymentMethodFound = !userCustomerBean.getPaymentMethodTypeCreditCardList().isEmpty();
                }
                
                List<TicketBean> sourceTokenList = QueryRepository.findTicketByUser(em, userBean);
                
                if (sourceTokenList != null && !sourceTokenList.isEmpty()) {
                    TicketBean sourceToken = sourceTokenList.get(0);
                    sourceToken.renew();
                    em.merge(sourceToken);
                    sourceTokenId = sourceToken.getTicketId();
                }
                else {
                    String newTicketId = new IdGenerator().generateId(16).substring(0, 32);
                    Date now = new Date();
                    Date lastUsed = now;
                    Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);
                    TicketBean sourceToken = new TicketBean(newTicketId, userBean, TicketBean.TICKET_TYPE_BUSINESS, now, lastUsed, expiryDate);
                    em.persist(sourceToken);
                    sourceTokenId = sourceToken.getTicketId();
                }
            }
            
            userOut.setSourcePaymentMethodFound(paymentMethodFound);
            userOut.setSourceToken(sourceTokenId);
            userOut.setLoyaltyCheckEnabled(Boolean.FALSE);
            authenticationResponse.setUser(userOut);

            userTransaction.commit();

            return authenticationResponse;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                System.err.println("IllegalStateException: " + e.getMessage());
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                System.err.println("SecurityException: " + e.getMessage());
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                System.err.println("SystemException: " + e.getMessage());
            }

            String message = "FAILED refreshing user data with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
