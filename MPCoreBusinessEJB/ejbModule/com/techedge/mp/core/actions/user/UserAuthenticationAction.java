package com.techedge.mp.core.actions.user;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.AuthenticationResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.LastLoginDataBean;
import com.techedge.mp.core.business.model.LastLoginErrorDataBean;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserAuthenticationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserAuthenticationAction() {}

    public AuthenticationResponse execute(String email, String password, String requestId, String deviceId, String deviceName, long maxPendingInterval,
            Integer ticketExpiryTime, Integer loginAttemptsLimit, Integer loginLockExpiryTime, List<String> userBlockExceptionList,
            UserCategoryService userCategoryService, UnavailabilityPeriodService unavailabilityPeriodService)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            AuthenticationResponse authenticationResponse = null;

            UserBean userBean = null;

            // Controlla se esiste un utente con username e password inseriti
            userBean = QueryRepository.findUserCustomerByEmail(em, email);

            if (userBean == null) {

                System.out.println("user not found");

                // Se l'utente non esiste resitituisci il messaggio di errore
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User " + email + " not found");
                authenticationResponse = new AuthenticationResponse();
                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);

                userTransaction.commit();
                return authenticationResponse;
            }
            else {

                if (userBlockExceptionList.contains(userBean.getPersonalDataBean().getSecurityDataEmail())) {

                    System.out.println(userBean.getPersonalDataBean().getSecurityDataEmail() + " presente in lista utenti con eccezioni blocco");
                }
                else {
                    // Controllo su disponibilit� del servizio di creazione transazioni prepaid
                    ServiceAvailabilityData serviceAvailabilityData = unavailabilityPeriodService.retrieveServiceAvailability("LOGIN", new Date());
                    if (serviceAvailabilityData != null) {

                        // Servizio non disponibile
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "AuthenticationService unavailable");

                        userTransaction.commit();

                        authenticationResponse = new AuthenticationResponse();
                        authenticationResponse.setStatusCode(serviceAvailabilityData.getStatusCode());
                        return authenticationResponse;
                    }
                }

                // Verifica lo stato dell'utente
                Integer userStatus = userBean.getUserStatus();
                if (userStatus == User.USER_STATUS_BLOCKED || userStatus == User.USER_STATUS_NEW || userStatus == User.USER_STATUS_CANCELLED) {

                    // Un utente che si trova in questo stato non pu� effettuare il login
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User " + email + " in status "
                            + userStatus);
                    authenticationResponse = new AuthenticationResponse();
                    if (userStatus == User.USER_STATUS_NEW) {
                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_NOT_VERIFIED);
                    }
                    else {
                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_UNAUTHORIZED);
                    }

                    userTransaction.commit();

                    return authenticationResponse;
                }

                if (userBean.getUserType() == User.USER_TYPE_REFUELING) {

                    // L'utente � di tipo refueling e non pu� effutare l'accesso
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User " + email);
                    authenticationResponse = new AuthenticationResponse();
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_INVALID_TICKET);

                    userTransaction.commit();

                    return authenticationResponse;
                }
                
                Boolean isNewAcquirerFlow = false;
                isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());

                if (isNewAcquirerFlow) {
                    
                    // L'utente appartiene alla categoria del nuovo acquirer e non pu� effettuare la login con questo servizio
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid userCategory for userType: " + userBean.getUserType());
                    authenticationResponse = new AuthenticationResponse();
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_UNAUTHORIZED);
                    
                    userTransaction.commit();

                    return authenticationResponse;
                }

                if (userBean.getUserType() == User.USER_TYPE_SERVICE) {

                    // Per ottimizzare il flusso di autenticazione i controlli sul
                    // lastLoginData e LastoLoginErrorData non sono effettuati sull'utente
                    // di sistema
                    if (userBean.getPersonalDataBean().getSecurityDataPassword().equals(password)) {

                        // 1) crea un nuovo ticket
                        User user = userBean.toUser();

                        Integer ticketType = TicketBean.TICKET_TYPE_SERVICE;

                        Date now = new Date();
                        Date lastUsed = now;
                        Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

                        String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

                        TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);

                        em.persist(ticketBean);

                        // 2) restituisci il risultato
                        authenticationResponse = new AuthenticationResponse();

                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
                        authenticationResponse.setTicketId(ticketBean.getTicketId());
                        authenticationResponse.setUser(user);

                        userTransaction.commit();

                        return authenticationResponse;
                    }
                    else {

                        // La password inserita non � corretta
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong password for user " + email);
                        authenticationResponse = new AuthenticationResponse();
                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);

                        userTransaction.commit();

                        return authenticationResponse;
                    }
                }
                else {

                    Date checkDate = new Date();

                    LastLoginErrorDataBean lastLoginErrorDataBean = userBean.getLastLoginErrorDataBean();

                    if (userBean.getPersonalDataBean().getSecurityDataPassword().equals(password)) {

                        // Controllare se l'utente ha superato il numero di tentativi ed � ancora nell'elapsed time;
                        if (lastLoginErrorDataBean != null && lastLoginErrorDataBean.getAttempt() > loginAttemptsLimit
                                && lastLoginErrorDataBean.getTime().after(checkDate)) {

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Login Locked access: User " + email
                                    + " in status " + userStatus);
                            authenticationResponse = new AuthenticationResponse();
                            authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_LOCKED);

                            userTransaction.commit();

                            return authenticationResponse;
                        }

                        // 1) crea un nuovo ticket
                        User user = userBean.toUser();

                        Integer ticketType = 0;
                        if (user.getUserType() == User.USER_TYPE_CUSTOMER) {
                            ticketType = TicketBean.TICKET_TYPE_CUSTOMER;
                        }
                        else {
                            ticketType = TicketBean.TICKET_TYPE_TESTER;
                        }

                        Date now = new Date();
                        Date lastUsed = now;
                        Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

                        String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

                        TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);

                        em.persist(ticketBean);

                        /*
                         * // 2) rimuovi il vecchio lastLoginData LastLoginDataBean
                         * lastLoginDataBean = userBean.getLastLoginDataBean(); if (
                         * lastLoginDataBean != null) { userBean.setLastLoginDataBean(null);
                         * em.remove(lastLoginDataBean); }
                         * 
                         * 
                         * // 3) inserisci il nuovo lastLoginData lastLoginDataBean = new
                         * LastLoginDataBean(); lastLoginDataBean.setDeviceId(deviceId);
                         * lastLoginDataBean.setDeviceName(deviceName); // TODO memorizzare
                         * anche latitudine e longitudine del device
                         * //lastLoginDataBean.setLatitude(latitude);
                         * //lastLoginDataBean.setLongitude(longitude);
                         * lastLoginDataBean.setTime(new Timestamp((new Date()).getTime()));
                         * em.persist(lastLoginDataBean);
                         * 
                         * userBean.setLastLoginDataBean(lastLoginDataBean);
                         */

                        // 2 e 3 Sostituzione vecchio lastLoginData o creazione nuovo
                        LastLoginDataBean lastLoginDataBean = userBean.getLastLoginDataBean();
                        if (lastLoginDataBean != null) {
                            userBean.getLastLoginDataBean().setDeviceId(deviceId);
                            userBean.getLastLoginDataBean().setDeviceName(deviceName);
                            // TODO memorizzare anche latitudine e longitudine del device
                            // userBean.getLastLoginDataBean().setLatitude(latitude);
                            // userBean.getLastLoginDataBean().setLongitude(longitude);
                            userBean.getLastLoginDataBean().setTime(new Timestamp((new Date()).getTime()));
                            // TODO verificare se serve em.merge(lastLoginDataBean);
                        }
                        else {
                            lastLoginDataBean = new LastLoginDataBean();
                            lastLoginDataBean.setDeviceId(deviceId);
                            lastLoginDataBean.setDeviceName(deviceName);
                            // TODO memorizzare anche latitudine e longitudine del device
                            // lastLoginDataBean.setLatitude(latitude);
                            // lastLoginDataBean.setLongitude(longitude);
                            lastLoginDataBean.setTime(new Timestamp((new Date()).getTime()));
                            em.persist(lastLoginDataBean);

                            userBean.setLastLoginDataBean(lastLoginDataBean);
                        }

                        // Cancellazione delle informazioni per il recupero della password
                        PersonalDataBean personalDataBean = userBean.getPersonalDataBean();
                        personalDataBean.setExpirationDateRescuePassword(null);
                        personalDataBean.setRescuePassword(null);
                        // TODO verificare se serve em.merge(personalDataBean);

                        userBean.setUserStatus(User.USER_STATUS_VERIFIED);
                        user.setUserStatus(User.USER_STATUS_VERIFIED);

                        if (lastLoginErrorDataBean != null) {
                            userBean.getLastLoginErrorDataBean().resetAttempts();
                        }

                        // Impostazione del flag oldUser per migrazione utenti
                        if (userBean.getOldUser() == null) {

                            Integer userType = userBean.getUserType();
                            Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());

                            if (useNewFlow) {

                                System.out.println("Primo accesso nuovo flusso");

                                // Se l'utente appartiene alla nuova categoria, ma non ha numeri di telefono associati, allora oldUser va impostato a true
                                if (userBean.getMobilePhoneList().isEmpty()) {
                                    userBean.setOldUser(true);
                                    System.out.println("OldUser impostato a true");
                                }
                                else {
                                    userBean.setOldUser(false);
                                    System.out.println("OldUser impostato a false");
                                }

                                // Modifica per resettare il flag di registrazione completata per i vecchi utenti che utilizzano il nuovo flusso voucher
                                if (userBean.getMobilePhoneList().isEmpty() && userBean.getUserStatusRegistrationCompleted() == true) {

                                    // Se un utente non ha numeri di telefono associati (nemmeno in stato cancellato) allora � un utente da migrare
                                    System.out.println("Utente con registrazione completata da migrare");
                                    userBean.unsetRegistrationCompleted();

                                    // Questa operazione � stata annullata perch� i vecchi token saranno utilizzabili anche con il nuovo codice esercente
                                    /*
                                    // Tutti i metodi di pagamento in stato 1 o 2 sono cancellati
                                    for(PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                                        
                                        paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                                        em.merge(paymentInfoBean);
                                    }
                                    */
                                }
                            }
                        }

                        em.merge(userBean);

                        // 4) restituisci il risultato
                        authenticationResponse = new AuthenticationResponse();

                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
                        authenticationResponse.setTicketId(ticketBean.getTicketId());
                        authenticationResponse.setUser(userBean.toUser());

                        userTransaction.commit();

                        return authenticationResponse;
                    }
                    else {

                        if (userBean.getPersonalDataBean().getExpirationDateRescuePassword() != null
                                && userBean.getPersonalDataBean().getExpirationDateRescuePassword().after(checkDate)
                                && userBean.getPersonalDataBean().getRescuePassword().equals(password)) {

                            // Tutti i ticket attivi associati all'utente devono essere
                            // invalidati
                            List<TicketBean> ticketBeanList = QueryRepository.findTicketByUserAndCheckDate(em, userBean, checkDate);

                            if (ticketBeanList != null && ticketBeanList.size() > 0) {

                                for (TicketBean ticketBeanLoop : ticketBeanList) {

                                    ticketBeanLoop.setExpirationTimestamp(checkDate);
                                    em.merge(ticketBeanLoop);
                                }
                            }

                            User user = userBean.toUser();

                            Integer ticketType = 0;
                            if (user.getUserType() == User.USER_TYPE_CUSTOMER)
                                ticketType = TicketBean.TICKET_TYPE_CUSTOMER;
                            else
                                ticketType = TicketBean.TICKET_TYPE_TESTER;

                            Date now = new Date();
                            Date lastUsed = now;
                            Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

                            String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

                            TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);

                            em.persist(ticketBean);

                            // 4) aggiorna i dati lastLoginDevice e lastLoginTime con i valori
                            // passati in input
                            LastLoginDataBean lastLoginDataBean = userBean.getLastLoginDataBean();
                            if (lastLoginDataBean != null) {
                                userBean.getLastLoginDataBean().setDeviceId(deviceId);
                                userBean.getLastLoginDataBean().setDeviceName(deviceName);
                                // TODO memorizzare anche latitudine e longitudine del device
                                // userBean.getLastLoginDataBean().setLatitude(latitude);
                                // userBean.getLastLoginDataBean().setLongitude(longitude);
                                userBean.getLastLoginDataBean().setTime(new Timestamp((new Date()).getTime()));
                                // TODO verificare se serve em.merge(lastLoginDataBean);
                            }
                            else {
                                lastLoginDataBean = new LastLoginDataBean();
                                lastLoginDataBean.setDeviceId(deviceId);
                                lastLoginDataBean.setDeviceName(deviceName);
                                // TODO memorizzare anche latitudine e longitudine del device
                                // lastLoginDataBean.setLatitude(latitude);
                                // lastLoginDataBean.setLongitude(longitude);
                                lastLoginDataBean.setTime(new Timestamp((new Date()).getTime()));
                                em.persist(lastLoginDataBean);

                                userBean.setLastLoginDataBean(lastLoginDataBean);
                            }

                            userBean.setUserStatus(User.USER_STATUS_TEMPORARY_PASSWORD);
                            user.setUserStatus(User.USER_STATUS_TEMPORARY_PASSWORD);

                            if (lastLoginErrorDataBean != null) {
                                userBean.getLastLoginErrorDataBean().resetAttempts();
                            }

                            em.merge(userBean);

                            // 5) restituisci il risultato
                            authenticationResponse = new AuthenticationResponse();

                            authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
                            authenticationResponse.setTicketId(ticketBean.getTicketId());
                            authenticationResponse.setUser(user);

                            userTransaction.commit();

                            return authenticationResponse;
                        }
                        else {

                            // Aggiornamento dei tentativi di accesso dell'utente;
                            if (lastLoginErrorDataBean != null) {

                                System.out.println("lastLoginErrorDataBean != null");
                                userBean.getLastLoginErrorDataBean().addAttempt();

                                if (userBean.getLastLoginErrorDataBean().getAttempt() > loginAttemptsLimit) {

                                    Date now = new Date();
                                    Date expiryDate = DateHelper.addMinutesToDate(loginLockExpiryTime, now);
                                    userBean.getLastLoginErrorDataBean().setTime(expiryDate);
                                }
                                System.out.println("attempt: " + userBean.getLastLoginErrorDataBean().getAttempt());
                            }
                            else {

                                System.out.println("lastLoginErrorDataBean == null");
                                lastLoginErrorDataBean = new LastLoginErrorDataBean();
                                lastLoginErrorDataBean.addAttempt();

                                em.persist(lastLoginErrorDataBean);

                                userBean.setLastLoginErrorDataBean(lastLoginErrorDataBean);
                            }

                            em.merge(userBean);

                            // La password inserita non � corretta
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong password for user " + email);

                            authenticationResponse = new AuthenticationResponse();
                            authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);

                            userTransaction.commit();

                            return authenticationResponse;
                        }
                    }
                }
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED authentication with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
