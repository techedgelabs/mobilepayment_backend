package com.techedge.mp.core.actions.user.v2;


import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ExternalAuthenticationResponse;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.UserExternalDataBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltySessionBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.interfaces.AuthorizationPlusResult;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2ExternalAuthenticationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2ExternalAuthenticationAction() {}

    public ExternalAuthenticationResponse execute(String accessToken, String provider, Double initialCap, String requestId, String deviceId, String deviceName, String deviceToken,
            Integer loyaltySessionExpiryTime, Integer ticketExpiryTime, UserCategoryService userCategoryService) throws EJBException {

        AuthorizationPlusResult authorizationPlusResult = null;

        //Controllo accessToken
        try {
            authorizationPlusResult = EJBHomeCache.getInstance().getAuthorizationPlusService().authorizationPlus(accessToken);
        }
        catch (Exception ex) {

            ex.printStackTrace();

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, ex.getMessage());

            ExternalAuthenticationResponse externalAuthenticationResponse = new ExternalAuthenticationResponse();
            externalAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_AUTH_FAILURE);

            return externalAuthenticationResponse;
        }

        // Se � NULL oppure i campi di risposta relativi all'utente sono vuoti, ritorno FAILURE

        if (authorizationPlusResult == null) {

            System.err.println("authorizationPlusResult null");

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "authorizationPlusResult null");

            ExternalAuthenticationResponse externalAuthenticationResponse = new ExternalAuthenticationResponse();
            externalAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_AUTH_FAILURE);

            return externalAuthenticationResponse;
        }

        if ((authorizationPlusResult != null && authorizationPlusResult.getRequest() == null)
                || (authorizationPlusResult != null && authorizationPlusResult.getRequest() != null && authorizationPlusResult.getRequest().getUser() == null)) {
            System.err.println("authorizationPlusResult request or user null");

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "authorizationPlusResult null");

            ExternalAuthenticationResponse externalAuthenticationResponse = new ExternalAuthenticationResponse();
            externalAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_AUTH_FAILURE);

            return externalAuthenticationResponse;
        }

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            UserExternalDataBean userExternalDataBean = null;
            
            //L'utente che mi ritorna dalla chiamata, viene fatta la ricerca se gi� esiste quell'utente nel DB
            
            try {
                userExternalDataBean = QueryRepository.findExternalUserByUUID(em, authorizationPlusResult.getRequest().getUser().getEmailAddress(), provider);
            }
            catch (NonUniqueResultException nue) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", authorizationPlusResult.getRequest().getUser().getEmailAddress(), null,
                        "Non unique record found !");
                throw new Exception(nue);
            }

            if (userExternalDataBean == null || userExternalDataBean.getUserBean() == null||userExternalDataBean.getUserBean().getUserStatus() == User.USER_STATUS_CANCELLED) {

                // Utente External non ancora registrato sul backend -> avvio processo di registrazione

                System.out.println("Prima login " + provider + " nuovo utente");

                User user = new User();

                Calendar birthDate = new GregorianCalendar(1970, 01, 01);

                String randomNumber = new IdGenerator().generateNumericId(12);
                String randomPassword = new IdGenerator().generateId(12);

                //randomPassword = "Qwerty1234";

                String username = provider.toLowerCase() + "_" + randomNumber + "@aaaaa.aa";
                String encodedPassword = EncoderHelper.encode(randomPassword);

                System.out.println("pass: " + randomPassword);

                PersonalData personalData = new PersonalData();
                if (authorizationPlusResult.getRequest().getUser().getFirstName() != null)
                    personalData.setFirstName(authorizationPlusResult.getRequest().getUser().getFirstName());
                if (authorizationPlusResult.getRequest().getUser().getLastName() != null)
                    personalData.setLastName(authorizationPlusResult.getRequest().getUser().getLastName());
                personalData.setSecurityDataEmail(authorizationPlusResult.getRequest().getUser().getEmailAddress());

                personalData.setBirthDate(birthDate.getTime());
                personalData.setBirthProvince("RM");
                personalData.setSex("M");
                personalData.setBirthMunicipality("Roma");
                personalData.setFiscalCode("XXXXXXXXXXXXXXXX");
                personalData.setLanguage("IT");
                personalData.setSecurityDataEmail(username);
                personalData.setSecurityDataPassword(encodedPassword);

                user.setUserStatus(User.USER_STATUS_VERIFIED);
                user.setUserStatusRegistrationCompleted(true);
                user.setEniStationUserType(Boolean.FALSE);

                user.setUserStatusRegistrationCompleted(Boolean.FALSE);

                user.setUserType(User.USER_TYPE_MULTICARD);

                user.setCapAvailable(initialCap);
                user.setCapEffective(initialCap);
                Calendar createDate = new GregorianCalendar();
                user.setCreateDate(createDate.getTime());
                user.setUseVoucher(Boolean.FALSE);
                user.setVirtualizationAttemptsLeft(5);
                user.setPersonalData(personalData);
                user.setSource(User.USER_MULTICARD);
                UserBean userBean = new UserBean(user);
                userBean.setDeviceID(deviceId);

                // Associazione telefono fittizio
                Date now = new Date();
                String number = "300000000000";
                String prefix = "+39";
                Integer status = MobilePhone.MOBILE_PHONE_STATUS_ACTIVE;

                MobilePhoneBean mobilePhoneBean = new MobilePhoneBean();
                mobilePhoneBean.setCreationTimestamp(now);
                mobilePhoneBean.setExpirationTimestamp(now);
                mobilePhoneBean.setLastUsedTimestamp(now);
                mobilePhoneBean.setNumber(number);
                mobilePhoneBean.setPrefix(prefix);
                mobilePhoneBean.setStatus(status);
                mobilePhoneBean.setUser(userBean);
                mobilePhoneBean.setVerificationCode("aaaaaaaaaaaaaaaa");

                userBean.getMobilePhoneList().add(mobilePhoneBean);
                
                // Associazione pin fittizio
                PaymentInfoBean paymentInfoBean = userBean.addNewPendingPaymentInfo(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER, "0000000000000000", Boolean.FALSE, 5, Double.valueOf(0), "", "242", "", "");
                paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED);
                
                userExternalDataBean = new UserExternalDataBean();
                userExternalDataBean.setProvider(provider);
                userExternalDataBean.setUserBean(userBean);
                userExternalDataBean.setUuid(authorizationPlusResult.getRequest().getUser().getEmailAddress());
                
                // Salva l'utente su db
                em.persist(userBean);
                em.persist(userExternalDataBean);
                em.persist(paymentInfoBean);

                ExternalAuthenticationResponse externalAuthenticationResponse = new ExternalAuthenticationResponse();
                externalAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_AUTH_SUCCESS);
                externalAuthenticationResponse.setUser(userBean.toUser());

                userTransaction.commit();

                return externalAuthenticationResponse;
            }

            UserBean userBean = userExternalDataBean.getUserBean();

            // Se l'utente esiste bisogna verificare che si sia registrato con lo stesso external con il quale sta effettuando la login

            String providerFound = userBean.getExternalProvider();

            if (providerFound == null || !providerFound.equals(provider)) {

                System.out.println("Utente non " + userExternalDataBean.getProvider() + " o registrato con un altro " + provider + " (" + providerFound + ")");

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong (" + providerFound + ")");

                ExternalAuthenticationResponse externalAuthenticationResponse = new ExternalAuthenticationResponse();
                externalAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_AUTH_WRONG_EXTERNAL);

                userTransaction.commit();

                return externalAuthenticationResponse;
            }

            ExternalAuthenticationResponse externalAuthenticationResponse = null;

            // Verifica lo stato dell'utente

            Integer userStatus = userBean.getUserStatus();
            if (userStatus == User.USER_STATUS_BLOCKED || userStatus == User.USER_STATUS_NEW) {

                // Un utente che si trova in questo stato non pu� effettuare il login
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User ID " + userBean.getId()
                        + userExternalDataBean.getProvider() + " UUID " + userExternalDataBean.getUuid() + " in status " + userStatus);
                externalAuthenticationResponse = new ExternalAuthenticationResponse();
                if (userStatus == User.USER_STATUS_NEW) {
                    externalAuthenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_NOT_VERIFIED);
                }
                else {
                    externalAuthenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_UNAUTHORIZED);
                }

                userTransaction.commit();

                return externalAuthenticationResponse;
            }

            // Verifica il tipo dell'utente

            if (userBean.getUserType() == User.USER_TYPE_REFUELING || userBean.getUserType() == User.USER_TYPE_REFUELING_NEW_ACQUIRER
                    || userBean.getUserType() == User.USER_TYPE_SERVICE) {

                // L'utente � di tipo refueling e non pu� effutare l'accesso
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User ID " + userBean.getId() + " "
                        + userExternalDataBean.getProvider() + " UUID " + userExternalDataBean.getUuid() + ", type: " + userBean.getUserType());
                externalAuthenticationResponse = new ExternalAuthenticationResponse();
                externalAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_AUTH_INVALID_TICKET);

                userTransaction.commit();

                return externalAuthenticationResponse;
            }
            
            
            // Genera il token della sessione loyalty

            Date creationTimestamp = new Date();
            Date lastUsedTimestamp = creationTimestamp;
            Date expiryTimestamp = DateHelper.addMinutesToDate(loyaltySessionExpiryTime, creationTimestamp);
            String fiscalCode = userExternalDataBean.getUuid();
            userBean.setExternalUserId(fiscalCode);

            String sessionId = new IdGenerator().generateId(16).substring(0, 32);

            LoyaltySessionBean loyaltySessionBean = new LoyaltySessionBean(sessionId, userBean, creationTimestamp, lastUsedTimestamp, expiryTimestamp, fiscalCode);

            em.persist(loyaltySessionBean);

            System.out.println("Creata sessione loyalty per l'utente " + userBean.getPersonalDataBean().getSecurityDataEmail() + " (" + userBean.getId() + ")");
            

            // 1) crea un nuovo ticket
            User user = userBean.toUser();

            Integer ticketType = 0;
            if (user.getUserType() == User.USER_TYPE_CUSTOMER || user.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER || user.getUserType() == User.USER_TYPE_MULTICARD) {
                ticketType = TicketBean.TICKET_TYPE_CUSTOMER;
            }
            else {
                ticketType = TicketBean.TICKET_TYPE_TESTER;
            }

            Date now = new Date();
            Date lastUsed = now;
            Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

            String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

            TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);

            em.persist(ticketBean);

            Integer outputStatus = User.USER_STATUS_VERIFIED;

       
            em.merge(userBean);

            // 4) restituisci il risultato
            externalAuthenticationResponse = new ExternalAuthenticationResponse();

            externalAuthenticationResponse.setStatusCode(ResponseHelper.USER_V2_EXTERNAL_AUTH_SUCCESS);
            externalAuthenticationResponse.setTicketId(ticketBean.getTicketId());

            System.out.println("id: " + userBean.getId());

            User userOut = userBean.toUser();

            // Controlla se ci sono nuovi elementi nellla tabella documenti che l'utente non ha ancora accettato
            UserCategoryBean userCategoryBean = QueryRepository.findUserCategoryByName(em, UserCategoryType.MULTICARD_FLOW.getCode());

            // Ricerca gli elementi della tabella documenti associati alla categoria 3 o con categoria null
            List<DocumentBean> documentBeanList = QueryRepository.findDocumentAll(em);
            for (DocumentBean documentBean : documentBeanList) {

                // Si escludono dall'elaborazione i documenti con groupCategory non null
                if (documentBean.getGroupCategory() == null || documentBean.getGroupCategory().isEmpty()) {

                    if (documentBean.getUserCategory() == null || documentBean.getUserCategory().equals(String.valueOf(userCategoryBean.getId()))) {

                        List<DocumentAttributeBean> documentAttributeBeanList = documentBean.getDocumentCheck();

                        for (DocumentAttributeBean documentAttributeBean : documentAttributeBeanList) {

                            //System.out.println("In verifica " + documentAttributeBean.getCheckKey());

                            if ((documentAttributeBean.getConditionText() != null && !documentAttributeBean.getConditionText().isEmpty())
                                    || (documentAttributeBean.getSubTitle() != null && !documentAttributeBean.getSubTitle().isEmpty())) {

                                //System.out.println("In elaborazione " + documentAttributeBean.getCheckKey());

                                // Se l'elemento non � presente tra i termini e condizioni associati all'utente allora va inserito con valid a false
                                String checkKey = documentAttributeBean.getCheckKey();

                                Boolean checkKeyFound = false;

                                Set<TermsOfService> termsOfServiceList = userOut.getPersonalData().getTermsOfServiceData();
                                for (TermsOfService termsOfService : termsOfServiceList) {

                                    if (termsOfService.getKeyval().equals(checkKey)) {
                                        checkKeyFound = true;
                                        //System.out.println(checkKey + " gi� presente");
                                        break;
                                    }
                                }

                                if (!checkKeyFound) {

                                    //System.out.println(checkKey + " non trovato");

                                    TermsOfService termsOfServiceNew = new TermsOfService();
                                    termsOfServiceNew.setAccepted(false);
                                    termsOfServiceNew.setKeyval(checkKey);
                                    termsOfServiceNew.setPersonalData(userOut.getPersonalData());
                                    termsOfServiceNew.setValid(Boolean.FALSE);

                                    userOut.getPersonalData().getTermsOfServiceData().add(termsOfServiceNew);

                                    //System.out.println(checkKey + " inserito");
                                }
                            }
                        }
                    }
                }
                else {

                    System.out.println("Trovato documento " + documentBean.getDocumentkey() + " con groupCategory non nullo: " + documentBean.getGroupCategory());
                }
            }

            userOut.setUserStatus(outputStatus);

            externalAuthenticationResponse.setUser(userOut);

            userTransaction.commit();

            return externalAuthenticationResponse;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                System.err.println("IllegalStateException: " + e.getMessage());
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                System.err.println("SecurityException: " + e.getMessage());
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                System.err.println("SystemException: " + e.getMessage());
            }

            String message = "FAILED authentication with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

}
