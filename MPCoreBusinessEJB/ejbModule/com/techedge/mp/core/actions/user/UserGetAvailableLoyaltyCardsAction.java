package com.techedge.mp.core.actions.user;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GetAvailableLoyaltyCardsData;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.LoyaltyCardMatcherBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.PanHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CardDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetLoyaltyCardListResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserGetAvailableLoyaltyCardsAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserGetAvailableLoyaltyCardsAction() {}

    public GetAvailableLoyaltyCardsData execute(String ticketId, String requestId, String fiscalcode, FidelityServiceRemote fidelityService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (fiscalcode != null && !fiscalcode.equals("")) {

                // Se viene inserito in input anche il codice fiscale, allor ala richiesta viene effettuata mediante il ticket di servizio;
                // in questo caso, i dati restituiti in output devono essere cifrati

                System.out.println("Chiamato getAvailableLoyaltyCards con ticket di sistema");

                if (ticketBean == null || !ticketBean.isValid()) {

                    // Ticket non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                    userTransaction.commit();

                    GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = new GetAvailableLoyaltyCardsData();
                    getAvailableLoyaltyCardsData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_INVALID_TICKET);

                    return getAvailableLoyaltyCardsData;
                }
                else if (!ticketBean.isServiceTicket()) {
                    // Ticket non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid request");

                    userTransaction.commit();

                    GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = new GetAvailableLoyaltyCardsData();
                    getAvailableLoyaltyCardsData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_INVALID_REQUEST);

                    return getAvailableLoyaltyCardsData;
                }

                Date now = new Date();
                String fiscalCode = fiscalcode;
                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                PartnerType partnerType = PartnerType.MP;
                Long requestTimestamp = now.getTime();
                GetLoyaltyCardListResult getLoyaltyCardListResult;

                try {
                    getLoyaltyCardListResult = fidelityService.getLoyaltyCardList(operationID, fiscalCode, partnerType, requestTimestamp);
                }
                catch (Exception ex) {

                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null,
                            "Errore nella connessione al Fidelity Service: " + ex.getMessage());

                    userTransaction.commit();

                    GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = new GetAvailableLoyaltyCardsData();
                    getAvailableLoyaltyCardsData.setStatusCode(ResponseHelper.SYSTEM_ERROR);

                    return getAvailableLoyaltyCardsData;
                }

                if (!getLoyaltyCardListResult.getStatusCode().equals(FidelityResponse.GET_AVAILABLE_LOYALTY_CARD_OK)) {
                    GetAvailableLoyaltyCardsData getAvailableLoyaltyCardData = new GetAvailableLoyaltyCardsData();

                    if (getLoyaltyCardListResult.getStatusCode().equals(FidelityResponse.GET_AVAILABLE_LOYALTY_CARD_MIGRATION_ERROR)) {
                        getAvailableLoyaltyCardData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_MIGRATION_ERROR);
                    }
                    else {
                        getAvailableLoyaltyCardData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_INVALID_CF);
                    }

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                            "Loyalty Cards Lists error: " + getLoyaltyCardListResult.getMessageCode());
                    /*
                     * if (!getLoyaltyCardListResult.getStatusCode().equals(FidelityResponse.GET_AVAILABLE_LOYALTY_CARD_CF_NOT_FOUND)) {
                     * getAvailableLoyaltyCardData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_CF_NOT_FOUND);
                     * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "No fiscal code found");
                     * 
                     * }
                     * else {
                     * getAvailableLoyaltyCardData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_GENERIC_ERROR);
                     * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Generic error");
                     * }
                     */

                    userTransaction.commit();
                    return getAvailableLoyaltyCardData;
                }

                GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = new GetAvailableLoyaltyCardsData();

                // memorizza su db im pancode associato alla richiesta
                for (CardDetail cardDetail : getLoyaltyCardListResult.getCardList()) {

                    LoyaltyCardMatcherBean loyaltyCardMatcherBean = new LoyaltyCardMatcherBean();
                    loyaltyCardMatcherBean.setEanCode(cardDetail.getEanCode());
                    loyaltyCardMatcherBean.setPanCode(cardDetail.getPanCode());
                    loyaltyCardMatcherBean.setCardType(cardDetail.getCardType());

                    loyaltyCardMatcherBean.setOperationID(operationID);

                    em.persist(loyaltyCardMatcherBean);
                }

                // recupera dal db i pancode associati alla richiesta

                List<LoyaltyCardMatcherBean> loyaltyCardMatcherBeanList = QueryRepository.findLoyaltyCardMatcherByOperationID(em, operationID);

                for (LoyaltyCardMatcherBean loyaltyCardMatcherBean : loyaltyCardMatcherBeanList) {

                    LoyaltyCard loyaltyCard = new LoyaltyCard();

                    loyaltyCard.setId(loyaltyCardMatcherBean.getId());
                    loyaltyCard.setEanCode(PanHelper.maskLoyaltyCode(loyaltyCardMatcherBean.getEanCode()));
                    loyaltyCard.setPanCode(PanHelper.maskLoyaltyCode(loyaltyCardMatcherBean.getPanCode()));
                    loyaltyCard.setType(loyaltyCardMatcherBean.getCardType());
                    loyaltyCard.setDefaultCard(false);

                    getAvailableLoyaltyCardsData.getLoyaltyCards().add(loyaltyCard);
                }

                getAvailableLoyaltyCardsData.setBalance(getLoyaltyCardListResult.getBalance());

                userTransaction.commit();

                getAvailableLoyaltyCardsData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_SUCCESS);

                return getAvailableLoyaltyCardsData;
            }
            else {

                System.out.println("Chiamato getAvailableLoyaltyCards con ticket utente");

                if (ticketBean == null || !ticketBean.isValid()) {

                    // Ticket non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                    userTransaction.commit();

                    GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = new GetAvailableLoyaltyCardsData();
                    getAvailableLoyaltyCardsData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_INVALID_TICKET);

                    return getAvailableLoyaltyCardsData;
                }
                else if (!ticketBean.isCustomerTicket()) {
                    // Ticket non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid request");

                    userTransaction.commit();

                    GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = new GetAvailableLoyaltyCardsData();
                    getAvailableLoyaltyCardsData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_INVALID_REQUEST);

                    return getAvailableLoyaltyCardsData;
                }

                UserBean userBean = ticketBean.getUser();
                if (userBean == null) {

                    // Ticket non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                    userTransaction.commit();

                    GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = new GetAvailableLoyaltyCardsData();
                    getAvailableLoyaltyCardsData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_INVALID_TICKET);

                    return getAvailableLoyaltyCardsData;
                }

                // Verifica lo stato dell'utente
                Integer userStatus = userBean.getUserStatus();
                if (userStatus != User.USER_STATUS_VERIFIED) {

                    // Un utente che si trova in questo stato non pu� invocare questo servizio
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to get active vouchers in status " + userStatus);

                    userTransaction.commit();

                    GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = new GetAvailableLoyaltyCardsData();
                    getAvailableLoyaltyCardsData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_UNAUTHORIZED);

                    return getAvailableLoyaltyCardsData;
                }

                Date now = new Date();
                String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                PartnerType partnerType = PartnerType.MP;
                Long requestTimestamp = now.getTime();
                GetLoyaltyCardListResult getLoyaltyCardListResult;

                try {
                    getLoyaltyCardListResult = fidelityService.getLoyaltyCardList(operationID, fiscalCode, partnerType, requestTimestamp);
                }
                catch (Exception ex) {

                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null,
                            "Errore nella connessione al Fidelity Service: " + ex.getMessage());

                    userTransaction.commit();

                    GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = new GetAvailableLoyaltyCardsData();
                    getAvailableLoyaltyCardsData.setStatusCode(ResponseHelper.SYSTEM_ERROR);

                    return getAvailableLoyaltyCardsData;
                }

                if (!getLoyaltyCardListResult.getStatusCode().equals(FidelityResponse.GET_AVAILABLE_LOYALTY_CARD_OK)) {
                    GetAvailableLoyaltyCardsData getAvailableLoyaltyCardData = new GetAvailableLoyaltyCardsData();

                    if (getLoyaltyCardListResult.getStatusCode().equals(FidelityResponse.GET_AVAILABLE_LOYALTY_CARD_MIGRATION_ERROR)) {
                        getAvailableLoyaltyCardData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_MIGRATION_ERROR);
                    }
                    else {
                        getAvailableLoyaltyCardData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_INVALID_CF);
                    }

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                            "Loyalty Cards Lists error: " + getLoyaltyCardListResult.getMessageCode());

                    /*
                     * if (!getLoyaltyCardListResult.getStatusCode().equals(FidelityResponse.GET_AVAILABLE_LOYALTY_CARD_CF_NOT_FOUND)) {
                     * getAvailableLoyaltyCardData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_CF_NOT_FOUND);
                     * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "No fiscal code found");
                     * 
                     * }
                     * else {
                     * getAvailableLoyaltyCardData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_GENERIC_ERROR);
                     * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Generic error");
                     * }
                     */

                    userTransaction.commit();
                    return getAvailableLoyaltyCardData;
                }

                // Calcolo carta di default
                String defaultPanCode = null;
                for (LoyaltyCardBean loyaltyCardBean : userBean.getLoyaltyCardList()) {
                    defaultPanCode = loyaltyCardBean.getPanCode();
                    break;
                }

                GetAvailableLoyaltyCardsData getAvailableLoyaltyCardsData = new GetAvailableLoyaltyCardsData();

                for (CardDetail cardDetail : getLoyaltyCardListResult.getCardList()) {

                    LoyaltyCard loyaltyCard = new LoyaltyCard();
                    loyaltyCard.setEanCode(cardDetail.getEanCode());
                    loyaltyCard.setPanCode(cardDetail.getPanCode());
                    loyaltyCard.setType(cardDetail.getCardType());

                    if (defaultPanCode != null && defaultPanCode.equals(loyaltyCard.getPanCode())) {
                        loyaltyCard.setDefaultCard(true);
                    }
                    else {
                        loyaltyCard.setDefaultCard(false);
                    }

                    getAvailableLoyaltyCardsData.getLoyaltyCards().add(loyaltyCard);
                }

                getAvailableLoyaltyCardsData.setBalance(getLoyaltyCardListResult.getBalance());

                userTransaction.commit();

                getAvailableLoyaltyCardsData.setStatusCode(ResponseHelper.USER_GET_AVAILABLE_LOYALTY_CARDS_SUCCESS);

                return getAvailableLoyaltyCardsData;
            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get available loyalty card with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
