package com.techedge.mp.core.actions.user.v2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.ResetPinCodeBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2RequestResetPinAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserV2RequestResetPinAction() {
    }
    
    
    public String execute(String ticketId, String requestId, EmailSenderRemote emailSender, Integer rescuePasswordExpirationTime, UserCategoryService userCategoryService) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		// Verifica il ticket
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);
    		
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket" );
    			
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_REQUEST_RESEND_PIN_INVALID_TICKET;
    		}
    		
	    	UserBean userBean = ticketBean.getUser();
		    
    		if (userBean == null) {
    			
    			// L'utente non esiste
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User doesn't exist" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_REQUEST_RESEND_PIN_UNAUTHORIZED;
    		}
    		
            Integer userType = userBean.getUserType();
            Boolean newAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            
            if (!newAcquirerFlow) {

                // Utente non abilitato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid userType: " + userBean.getUserType());

                userTransaction.commit();

                return ResponseHelper.USER_REQUEST_RESEND_PIN_UNAUTHORIZED;
            }

    		if (userBean.getUserStatus() != User.USER_STATUS_VERIFIED) {
    			
    			// Lo stato dell'utente non consente il reset del pin
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User status not valid for operation" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_REQUEST_RESEND_PIN_UNAUTHORIZED;
    		}
    		
    		String socialProvider = userBean.getSocialProvider();
    		
    		if (socialProvider == null || socialProvider.isEmpty()) {
                
                // Il servizio pu� essere utilizzato solo da utenti social
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not social" );
                
                userTransaction.commit();
                
                return ResponseHelper.USER_REQUEST_RESEND_PIN_UNAUTHORIZED;
            }

    		/*
    		PaymentInfoBean creditVoucherPaymentInfoBean = userBean.getVoucherPaymentMethod();
    		
    		if (creditVoucherPaymentInfoBean == null) {
                
                // l'utente non ha ancora inserito il pin
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not found" );
                
                userTransaction.commit();
                
                return ResponseHelper.USER_REQUEST_RESEND_PIN_UNAUTHORIZED;
            }
    		*/
    		
    		String resetPinCode = new IdGenerator().generateId(10).substring(0, 8);
    		
    		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "reset code: "+ resetPinCode );
    		
    		ResetPinCodeBean resetPinCodeBean = new ResetPinCodeBean();
    		resetPinCodeBean.setCode(resetPinCode);
    		resetPinCodeBean.setStatus(ResetPinCodeBean.STATUS_NEW);
    		resetPinCodeBean.setUser(userBean);
    		
    		Date now = new Date();
    		resetPinCodeBean.setCreationTimestamp(now);
    		resetPinCodeBean.setLastUsedTimestamp(now);
			Date expiryDate = DateHelper.addMinutesToDate(rescuePasswordExpirationTime, now);
			resetPinCodeBean.setExpirationTimestamp(expiryDate);
			
    		// Salva i dati dell'utente su db
    		em.persist(resetPinCodeBean);
    				
    		    		
    		// Invia l'email con il codice di attivazione
    		if (emailSender != null) {
    			EmailType emailType = EmailType.REQUEST_RESET_PIN_V2;
    			
                String keyFrom = emailSender.getSender();
                String to = userBean.getPersonalDataBean().getSecurityDataEmail();
    			List<Parameter> parameters = new ArrayList<Parameter>(0);
    			
    			parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
    			parameters.add(new Parameter("RESET_PIN_CODE", resetPinCode));
    			parameters.add(new Parameter("EMAIL", to));
    			
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Sending email to " + to );
    			
    			String result = emailSender.sendEmail(emailType, keyFrom, to, parameters);
    				
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "SendEmail result: " + result );
   			
    		}
    		
    		// Rinnova il ticket
    		ticketBean.renew();
    		em.merge(ticketBean);
    		
    		userTransaction.commit();
    		
    		return ResponseHelper.USER_REQUEST_RESEND_PIN_SUCCESS;
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED request reset pin with message (" + ex2.getMessage() + ")";
    		this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
