package com.techedge.mp.core.actions.user;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.Device;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveUserDeviceData;
import com.techedge.mp.core.business.interfaces.user.UserDevice;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserDeviceBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserRetrieveActiveDeviceAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserRetrieveActiveDeviceAction() {}

    public RetrieveUserDeviceData execute(String ticketId, String requestId) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                RetrieveUserDeviceData retrieveUserDeviceData = new RetrieveUserDeviceData();
                retrieveUserDeviceData.setStatusCode(ResponseHelper.USER_RETRIEVE_ACTIVE_INVALID_TICKET);
                return retrieveUserDeviceData;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                RetrieveUserDeviceData retrieveUserDeviceData = new RetrieveUserDeviceData();
                retrieveUserDeviceData.setStatusCode(ResponseHelper.USER_RETRIEVE_ACTIVE_INVALID_TICKET);
                return retrieveUserDeviceData;
            }

            List<UserDeviceBean> listUserDeviceBeans = QueryRepository.findDeviceByUserID(em, userBean);
            List<Device> listDevice = new ArrayList<>(0);
            if (listUserDeviceBeans != null && !listUserDeviceBeans.isEmpty()) {
                for (UserDeviceBean item : listUserDeviceBeans) {
                    if (item.getStatus().equals(UserDevice.USER_DEVICE_STATUS_VERIFIED)) {
                        Device device = new Device();
                        device.setAssociationTime(item.getAssociationTimestamp());
                        device.setCreationTime(item.getCreationTimestamp());
                        device.setDeviceID(item.getDeviceId());
                        device.setDeviceName(item.getDeviceName());
                        device.setLastLoginTime(item.getLastUsedTimestamp());
                        listDevice.add(device);
                    }
                }
            }

            RetrieveUserDeviceData retrieveUserDeviceData = new RetrieveUserDeviceData();

            retrieveUserDeviceData.setUserDeviceList(listDevice);
            retrieveUserDeviceData.setStatusCode(ResponseHelper.USER_RETRIEVE_ACTIVE_DEVICE_SUCCESS);

            userTransaction.commit();

            return retrieveUserDeviceData;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieve cities data with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
