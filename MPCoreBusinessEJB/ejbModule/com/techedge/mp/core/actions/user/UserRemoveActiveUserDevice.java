package com.techedge.mp.core.actions.user;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.UserDevice;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserDeviceBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserRemoveActiveUserDevice {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserRemoveActiveUserDevice() {}

    public String execute(String ticketId, String requestId, String userDeviceID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_REMOVE_DEVICE_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_REMOVE_DEVICE_INVALID_TICKET;
            }

            if (userDeviceID == null || (userDeviceID != null && userDeviceID.isEmpty())) {
                //UserDevice 
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "UserDeviceID not found");

                userTransaction.commit();

                return ResponseHelper.USER_REMOVE_DEVICE_FAILURE;
            }

            // Non � possibile rimuovere metodi di pagamento diversi da credit_card
            if (userBean.getUserDeviceBean() == null || userBean.getUserDeviceBean().isEmpty()) {

                //UserDevice 
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "List UserDevice not found");

                userTransaction.commit();

                return ResponseHelper.USER_REMOVE_DEVICE_FAILURE;
            }
            
            // Imposta lo stato del device da rimuovere in cancellato
            UserDeviceBean deviceToRemove = null;
            
            for (UserDeviceBean item : userBean.getUserDeviceBean()) {
                if (item.getDeviceId().equals(userDeviceID) && item.getStatus().equals(UserDevice.USER_DEVICE_STATUS_VERIFIED)) {
                    item.setStatus(UserDevice.USER_DEVICE_STATUS_CANCELLED);
                    deviceToRemove = item;
                }
            }

            if (deviceToRemove == null) {
                
                //UserDevice 
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "UserDevice to remove not found");

                userTransaction.commit();

                return ResponseHelper.USER_REMOVE_DEVICE_FAILURE;
            }
            
            // Invalidazione di tutti i ticket associati all'utente
            System.out.println("Invalidazione dei ticket associati all'utente");
            
            // Se il ticket � valido imposta il timeout di fine validit� un'ora indietro rispetto all'ora attuale
            Date expirationTimestamp = new Date();
            expirationTimestamp.setTime(expirationTimestamp.getTime() - 3600000);
            
            List<TicketBean> ticketBeanList = QueryRepository.findTicketByUser(em, userBean);
            for(TicketBean ticketCheckBean : ticketBeanList) {
                if (ticketCheckBean.isValid()) {
                    System.out.println("Invalidazione ticket " + ticketCheckBean.getTicketId());
                    ticketCheckBean.setExpirationTimestamp(expirationTimestamp);
                }
            }

            em.merge(deviceToRemove);

            userTransaction.commit();
            
            //UserDevice 
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "UserDeviceID verified not found ");

            return ResponseHelper.USER_REMOVE_DEVICE_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED remove UserDevice with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
