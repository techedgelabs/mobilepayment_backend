package com.techedge.mp.core.actions.user.v2;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.DocumentCheckInfo;
import com.techedge.mp.core.business.interfaces.DocumentInfo;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveTermsOfServiceData;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2RetrieveTermsOfServiceByGroupAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2RetrieveTermsOfServiceByGroupAction() {}

    public RetrieveTermsOfServiceData execute(String ticketId, String requestId, UserCategoryType userCategoryType, UserCategoryService userCategoryService, String group)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || ticketBean.isServiceTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                RetrieveTermsOfServiceData retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
                retrieveTermsOfServiceData.setStatusCode(ResponseHelper.USER_RETRIEVE_TERMS_SERVICE_INVALID_TICKET);
                return retrieveTermsOfServiceData;
            }

            // Se il servizio viene chiamato con il ticket di un utente devono essere restituiti solo i documenti in stato non valido
            UserBean userBean = ticketBean.getUser();
            Set<TermsOfServiceBean> termsOfServiceBeanData = userBean.getPersonalDataBean().getTermsOfServiceBeanData();

            RetrieveTermsOfServiceData retrieveTermsOfServiceData = new RetrieveTermsOfServiceData();
            retrieveTermsOfServiceData.setStatusCode(ResponseHelper.USER_RETRIEVE_TERMS_SERVICE_SUCCESS);
            
            List<DocumentInfo> documentInfoList = new ArrayList<DocumentInfo>(0);

            List<DocumentBean> documentBeanList = QueryRepository.findDocumentAll(em);

            if (documentBeanList != null) {

                for (DocumentBean documentBeanFor : documentBeanList) {
                    
                    if (group != null && group.equals("ALL")) {
                        
                        // Se l'utente non ha effettuato la virtualizzazione della carta loyalty il regolamento You&Eni non deve essere visibile
                        
                        if (userBean.getVirtualizationCompleted()) {
                            
                            //System.out.println("L'utente ha effettuato la virtualizzazione della carta");
                            //System.out.println("Restituzione di tutti i termsOfService");
                        }
                        else {
                            
                            //System.out.println("L'utente non ha effettuato la virtualizzazione della carta");
                            
                            if (documentBeanFor.getGroupCategory() != null && documentBeanFor.getGroupCategory().equals("LOYALTY")) {
                                
                                //System.out.println("documentBean LOYALTY non restituito");
                                continue;
                            }
                        }
                    }
                    else {
                        
                        if (documentBeanFor.getGroupCategory() == null || documentBeanFor.getGroupCategory().isEmpty()) {
                            
                            //System.out.println("Trovato documentBean con groupCategory null");
                            continue;
                        }
                        
                        if (!documentBeanFor.getGroupCategory().equals(group)) {
                            
                            //System.out.println("Trovato documentBean con groupCategory non valido: " + documentBeanFor.getGroupCategory());
                            continue;
                        }
                    }

                    //System.out.println("Trovato documentBean con userCategory " + documentBeanFor.getUserCategory());

                    Boolean addDocument = true;

                    if (documentBeanFor.getUserCategory() != null && !documentBeanFor.getUserCategory().equals("")) {

                        addDocument = false;
                        
                        Long userCategoryId = Long.valueOf(documentBeanFor.getUserCategory());
                        
                        UserCategoryBean userCategoryBean = QueryRepository.findUserCategoryById(em, userCategoryId);
                        
                        if (userCategoryBean == null) {
                            
                            //System.out.println("userCategoryBean null");
                        }
                        else {
                        
                            //System.out.println("Lo userType " + userBean.getUserType() + " appartiene alla categoria " + userCategoryBean.getName() + "?");
                            
                            Boolean check = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), userCategoryBean.getName());
                            
                            if (check) {
                                //System.out.println("Si");
                                addDocument = true;
                            }
                            else {
                                //System.out.println("No");
                            }
                        }

                    }

                    if (addDocument) {

                        //System.out.println("Documento " + documentBeanFor.getDocumentkey() + " inserito nella risposta");

                        DocumentInfo documentinfo = new DocumentInfo();
                        if (documentBeanFor.getTemplateFile() != null && !documentBeanFor.getTemplateFile().equals("")) {
                            documentinfo.setDocumentKey(documentBeanFor.getDocumentkey());
                        }
                        else {
                            documentinfo.setDocumentKey("");
                        }
                        documentinfo.setPosition(documentBeanFor.getPosition());
                        documentinfo.setTitle(documentBeanFor.getTitle());
                        if (documentBeanFor.getSubTitle() != null && !documentBeanFor.getSubTitle().equals("")) {
                            documentinfo.setSubTitle(documentBeanFor.getSubTitle());
                        }
                        else {
                            documentinfo.setSubTitle("");
                        }
                        List<DocumentCheckInfo> docCheckList = new ArrayList<DocumentCheckInfo>(0);
                        List<DocumentAttributeBean> docAttrList = documentBeanFor.getDocumentCheck();

                        Boolean dciFound = false;

                        for (DocumentAttributeBean dab : docAttrList) {
                            
                            //System.out.println("2");
                            DocumentCheckInfo dci = new DocumentCheckInfo();
                            if (dab.getConditionText() == null || dab.getConditionText().equals("")) {
                                dci.setConditionText(dab.getExtendedConditionText());
                            }
                            else {
                                dci.setConditionText(dab.getConditionText());
                            }
                            dci.setMandatory(dab.isMandatory());
                            dci.setPosition(dab.getPosition());
                            if (dab.getSubTitle() != null && !dab.getSubTitle().equals("")) {
                                dci.setSubTitle(dab.getSubTitle());
                            }
                            else {
                                dci.setSubTitle("");
                            }
                            dci.setId(dab.getCheckKey());
                            docCheckList.add(dci);

                            dciFound = true;
                            
                            /*
                            
                            //System.out.println("1");
                            TermsOfServiceBean termsOfServiceBeanSearch = null;
                            for (TermsOfServiceBean termsOfServiceBean : termsOfServiceBeanData) {

                                if (termsOfServiceBean.getKeyval().equals(dab.getCheckKey())) {
                                    termsOfServiceBeanSearch = termsOfServiceBean;

                                }
                            }
                            
                            dciFound = true;
                            
                            if (termsOfServiceBeanSearch == null) {
                                //System.out.println("2");
                                DocumentCheckInfo dci = new DocumentCheckInfo();
                                if (dab.getConditionText() == null || dab.getConditionText().equals("")) {
                                    dci.setConditionText(dab.getExtendedConditionText());
                                }
                                else {
                                    dci.setConditionText(dab.getConditionText());
                                }
                                dci.setMandatory(dab.isMandatory());
                                dci.setPosition(dab.getPosition());
                                if (dab.getSubTitle() != null && !dab.getSubTitle().equals("")) {
                                    dci.setSubTitle(dab.getSubTitle());
                                }
                                else {
                                    dci.setSubTitle("");
                                }
                                dci.setId(dab.getCheckKey());
                                docCheckList.add(dci);

                                dciFound = true;
                            }
                            else {
                                //System.out.println("3");
                                if (termsOfServiceBeanSearch.getValid() == null || !termsOfServiceBeanSearch.getValid()) {
                                    //System.out.println("4");
                                    DocumentCheckInfo dci = new DocumentCheckInfo();
                                    if (dab.getConditionText() == null || dab.getConditionText().equals("")) {
                                        dci.setConditionText(dab.getExtendedConditionText());
                                    }
                                    else {
                                        dci.setConditionText(dab.getConditionText());
                                    }
                                    dci.setMandatory(dab.isMandatory());
                                    dci.setPosition(dab.getPosition());
                                    if (dab.getSubTitle() != null && !dab.getSubTitle().equals("")) {
                                        dci.setSubTitle(dab.getSubTitle());
                                    }
                                    else {
                                        dci.setSubTitle("");
                                    }
                                    dci.setId(dab.getCheckKey());
                                    dci.setAccepted(termsOfServiceBeanSearch.getAccepted());
                                    docCheckList.add(dci);

                                    dciFound = true;
                                }
                                else {
                                    //System.out.println("5");
                                    dciFound = false;
                                }
                            }
                            */
                            
                            documentinfo.setCheckList(docCheckList);
                            //System.out.println("6");
                            if (dciFound == true) {
                                documentInfoList.add(documentinfo);
                            }
                        }
                    }
                }

                retrieveTermsOfServiceData.setDocuments(documentInfoList);
                retrieveTermsOfServiceData.setStatusCode(ResponseHelper.USER_RETRIEVE_TERMS_SERVICE_SUCCESS);
            }

            userTransaction.commit();

            return retrieveTermsOfServiceData;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieve Terms of Service data by group with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
