package com.techedge.mp.core.actions.user;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SendValidationResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.interfaces.user.UserDevice;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserDeviceBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.model.VerificationCodeBean;
import com.techedge.mp.core.business.utilities.AsyncDWHService;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.QueryRepositoryBusiness;
import com.techedge.mp.core.business.utilities.ResendValidation;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.Platform;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserValidateFieldAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserValidateFieldAction() {}

    public String execute(String ticketId, String requestId, String deviceId, String verificationType, String sendingType, String verificationField, String verificationCode,
            Integer maxRetryAttemps, Integer reconciliationMaxAttempts, Integer verificationNumberDevice, UserService userService, EmailSenderRemote emailSender, SmsServiceRemote smsService, 
            PushNotificationServiceRemote pushNotificationService, UserCategoryService userCategoryService, StringSubstitution stringSubstitution)
            throws EJBException {

        final UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null
                    || !ticketBean.isValid()
                    || ((verificationType.equals("email_address") && !ticketBean.isServiceTicket()) || (verificationType.equals("phone_number") && !ticketBean.isCustomerTicket()) || (verificationType.equals("device_number") && !ticketBean.isCustomerTicket()))) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_CHECK_INVALID_TICKET;
            }

            if (verificationType.equals("email_address")) {

                // Ricerca l'utente associato all'email
                String email = verificationField;
                
                // Controllo del verificationCode
                VerificationCodeBean verificationCodeBean = QueryRepository.findVerificationCodeBeanById(em, verificationCode);

                if (verificationCodeBean == null || !verificationCodeBean.isValid()) {

                    // VerificationCode non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid verificationCode");

                    userTransaction.commit();

                    return ResponseHelper.USER_CHECK_FAILURE;
                }
                
                UserBean userBean = verificationCodeBean.getUser();
                
                if (userBean == null || userBean.getUserStatus() == User.USER_STATUS_CANCELLED) {

                    // L'utente non esiste
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                    userTransaction.commit();

                    return ResponseHelper.USER_CHECK_FAILURE;
                }

                if (userBean.getUserStatus() != User.USER_STATUS_NEW) {

                    // Lo stato dell'utente � diverso da creato
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid status: expecting new");

                    userTransaction.commit();

                    return ResponseHelper.USER_CHECK_FAILURE;
                }

                if (!userBean.getPersonalDataBean().getSecurityDataEmail().equals(email)) {

                    // L'email ricevuta in input � diversa da quella dell'utente associato al codice
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User email doesn't match input email");

                    userTransaction.commit();

                    return ResponseHelper.USER_CHECK_FAILURE;
                }

                // Verificare che il telefono che l'utente ha inserito in iscrizione non sia stato gi� utilizzato da un utente

                MobilePhoneBean mobilePhoneBeanUser = null;

                for (MobilePhoneBean mobilePhoneBean : userBean.getMobilePhoneList()) {

                    if (mobilePhoneBean.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_PENDING) {

                        mobilePhoneBeanUser = mobilePhoneBean;
                    }
                }

                if (mobilePhoneBeanUser != null) {

                    System.out.println("Verifica se il numero di telefono risulta gi� associato a un utente verificato");

                    Boolean mobilePhoneFound = false;
                    List<MobilePhoneBean> mobilePhoneBeanList = QueryRepository.findAllActiveMobilePhoneByNumberAndPrefix(em, mobilePhoneBeanUser.getNumber(),
                            mobilePhoneBeanUser.getPrefix());

                    Integer userType = userBean.getUserType();
                    Boolean useNewAcquirer = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
                    Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());

                    if (mobilePhoneBeanList != null && !mobilePhoneBeanList.isEmpty()) {

                        for (MobilePhoneBean mobilePhoneBean : mobilePhoneBeanList) {

                            System.out.println("Trovato telefono per utente in stato " + mobilePhoneBean.getUser().getUserStatus());

                            if (mobilePhoneBean.getUser().getUserStatus() != User.USER_STATUS_NEW && mobilePhoneBean.getUser().getUserStatus() != User.USER_STATUS_CANCELLED) {

                                Integer userTypeFound = mobilePhoneBean.getUser().getUserType();
                                Boolean userFoundNewAcquirer = userCategoryService.isUserTypeInUserCategory(userTypeFound, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
                                Boolean userFoundBusiness    = userCategoryService.isUserTypeInUserCategory(userTypeFound, UserCategoryType.BUSINESS.getCode());
                                
                                if( (useNewAcquirer && userFoundNewAcquirer) || (useBusiness && userFoundBusiness) ) {
                                
                                    System.out.println("Il telefono � gi� stato usato da un utente in stato " + mobilePhoneBean.getUser().getUserStatus());
                                    mobilePhoneFound = true;
                                    break;
                                }
                            }
                        }
                    }
                    
                    if (mobilePhoneFound) {

                        // VerificationCode associato a un altro utente
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone used by another user");

                        userTransaction.commit();

                        return ResponseHelper.USER_CHECK_FAILURE;
                    }
                    
                }

                // Aggiorna lo stato del VerificationCode
                verificationCodeBean.setStatus(VerificationCodeBean.STATUS_USED);
                em.merge(verificationCodeBean);

                // Modifica lo stato dell'utente in verificato
                userBean.setStatusToVerified();

                // Aggiorna i dati dell'utente
                em.merge(userBean);

                if (userBean.getMobilePhoneList().isEmpty()) {

                    // Gestione degli utenti non ancora migrati
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone not found");
                }
                else {

                    //Invio numero di telefono dopo l'update
                    Object[] mobilePhoneArrayList = userBean.getMobilePhoneList().toArray();
                    MobilePhoneBean mobilePhoneBean = (MobilePhoneBean) mobilePhoneArrayList[0];

                    SendValidationResult result = ResendValidation.send(sendingType, maxRetryAttemps, userBean, mobilePhoneBean, loggerService, emailSender, 
                            smsService, userCategoryService, stringSubstitution);

                    if (result.equals(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE)) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error in verification mobile phone. "
                                + result.getMessageCode() + " (" + result.getErrorCode() + ")");

                        userTransaction.commit();

                        return ResponseHelper.USER_CHECK_MOBILE_PHONE_FAILURE;
                    }
                }

                userTransaction.commit();

                return ResponseHelper.USER_CHECK_SUCCESS;
            }

            else if (verificationType.equals("phone_number")) {

                // Ricerca l'utente associato al numero di telefono
                String phone_number = verificationField;

                final UserBean userBean = ticketBean.getUser();

                if (userBean == null) {

                    // L'utente non esiste
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                    userTransaction.commit();

                    return ResponseHelper.USER_CHECK_FAILURE;
                }
                else {

                    if (userBean.getUserStatus() != User.USER_STATUS_VERIFIED) {

                        // Lo stato dell'utente � diverso da verificato
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid status: expecting verified");

                        userTransaction.commit();

                        return ResponseHelper.USER_CHECK_FAILURE;
                    }
                    else {

                        // Ricerca tra i numeri associati all'utente quello che si sta verificando
                        MobilePhoneBean mobilePhoneBeanToValidate = null;

                        for (MobilePhoneBean mobilePhoneBean : userBean.getMobilePhoneList()) {

                            if (mobilePhoneBean.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_PENDING) {

                                String mobilePhoneNumberFull = mobilePhoneBean.getPrefix() + mobilePhoneBean.getNumber();

                                if (mobilePhoneNumberFull.equals(phone_number)) {

                                    mobilePhoneBeanToValidate = mobilePhoneBean;
                                }
                            }
                        }

                        if (mobilePhoneBeanToValidate == null) {

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone pending not found");

                            userTransaction.commit();

                            return ResponseHelper.USER_CHECK_MOBILE_PHONE_FAILURE;
                        }

                        /*
                        if (mobilePhoneBeanToValidate.getStatus() != MobilePhone.MOBILE_PHONE_STATUS_PENDING) {

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid status: expecting pending");

                            userTransaction.commit();

                            return ResponseHelper.USER_CHECK_MOBILE_PHONE_FAILURE;
                        }
                        */

                        if (!mobilePhoneBeanToValidate.getVerificationCode().toUpperCase().equals(verificationCode.toUpperCase())) {

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Verification Code not valid");

                            userTransaction.commit();

                            return ResponseHelper.USER_CHECK_WRONG_CODE;
                        }

                        if (!mobilePhoneBeanToValidate.isValid()) {

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile Phone expired");

                            mobilePhoneBeanToValidate.setStatus(MobilePhone.MOBILE_PHONE_STATUS_CANCELED);
                            em.merge(mobilePhoneBeanToValidate);
                            userTransaction.commit();

                            return ResponseHelper.USER_CHECK_MOBILE_PHONE_FAILURE;
                        }

                        for (MobilePhoneBean item : userBean.getMobilePhoneList()) {
                            if (item.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE)) {
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Number " + item.getNumber()
                                        + " change status active to status canceled");
                                item.setStatus(MobilePhone.MOBILE_PHONE_STATUS_CANCELED);
                                em.merge(item);
                            }
                        }

                        // Attiva il device che sta utilizzando l'utente se non � gi� attivo
                        System.out.println("Verifica device da attivare - device id " + deviceId);

                        for (UserDeviceBean userDeviceBean : userBean.getUserDeviceBean()) {

                            System.out.println("Device " + userDeviceBean.getDeviceId());

                            if (userDeviceBean.getDeviceId().equals(deviceId) && userDeviceBean.getStatus() == UserDevice.USER_DEVICE_STATUS_PENDING) {
                                Date associationTimestamp = new Date();
                                userDeviceBean.setAssociationTimestamp(associationTimestamp);
                                userDeviceBean.setStatus(UserDevice.USER_DEVICE_STATUS_VERIFIED);
                                em.merge(userDeviceBean);
                                System.out.println("Device " + deviceId + " attivato");
                                break;
                            }
                        }

                        // Aggiorna lo stato del VerificationCode
                        mobilePhoneBeanToValidate.setStatus(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE);
                        em.merge(mobilePhoneBeanToValidate);

                        userBean.setStatusToVerified();
                        // Aggiorna i dati dell'utente
                        em.merge(userBean);
                        em.flush();
                        
                        String email = userBean.getPersonalDataBean().getSecurityDataEmail();
                        
                        UserBean userBeanBusiness = QueryRepositoryBusiness.findNotCancelledUserBusinessByEmailAndSource(em, email, "ENISTATION+");
                        
                        if (userBeanBusiness != null) {
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Trovato utente business collegato. "
                                    + "Aggiornamento automatico del numero di cellulare");
                            
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                                    "Eliminazione di tutti mobile phone dell'utente business collegato...");
                            Query query = em.createQuery("delete from MobilePhoneBean m where m.user = :userBean");
                            query.setParameter("userBean", userBeanBusiness);
                            int rows = query.executeUpdate();
                            em.flush();
        
                            if (rows <= 0) {
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Eliminazione dei records presenti non andata a buon fine");
                            }
                            else {
                                for (MobilePhoneBean mobilePhoneBean : userBean.getMobilePhoneList()) {
                                    
                                    if (mobilePhoneBean.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE)) {
                                        
                                        MobilePhoneBean userBusinessMobilePhoneBean = new MobilePhoneBean();
                                        userBusinessMobilePhoneBean.setCreationTimestamp(new Date());
                                        userBusinessMobilePhoneBean.setNumber(mobilePhoneBean.getNumber());
                                        userBusinessMobilePhoneBean.setPrefix(mobilePhoneBean.getPrefix());
                                        userBusinessMobilePhoneBean.setStatus(mobilePhoneBean.getStatus());
                                        userBusinessMobilePhoneBean.setUser(userBeanBusiness);
                                        
                                        em.persist(userBusinessMobilePhoneBean);
                                    }
                                }
                            }
                        }
                        
                        
                        Boolean isBusiness = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(),UserCategoryType.BUSINESS.getCode());

                        if (userBean.getUserStatusRegistrationCompleted() && !userBean.getUserType().equals(User.USER_TYPE_GUEST) && !isBusiness) {
                            
                            List<TermsOfServiceBean> listTermOfService = QueryRepository.findTermOfServiceByPersonalDataId(em, userBean.getPersonalDataBean());

                            if (listTermOfService == null) {
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "flagPrivacy null");
                                userTransaction.commit();

                                return ResponseHelper.USER_CHECK_FAILURE;
                            }
                            
                            List<MobilePhoneBean> listMobilePhone = new ArrayList<MobilePhoneBean>();
                            String codCard = null;
                            
                            for (MobilePhoneBean item : userBean.getMobilePhoneList()) {
                                listMobilePhone.add(item);
                            }
                            
                            for (LoyaltyCardBean item : userBean.getLoyaltyCardList()) {
                                if (item.getStatus().equals(LoyaltyCard.LOYALTY_CARD_STATUS_VALIDA)) {
                                    codCard = item.getPanCode();
                                }
                            }
                            
                            List<UserSocialDataBean> userSocialDataList = new ArrayList<UserSocialDataBean>();
                            
                            for (UserSocialDataBean item : userBean.getUserSocialData()) {
                                userSocialDataList.add(item);
                            }
                            
                            AsyncDWHService asyncDWHService = new AsyncDWHService(userTransaction, em, userBean, listMobilePhone, listTermOfService, userSocialDataList, requestId, codCard, reconciliationMaxAttempts);
                            
                            new Thread(asyncDWHService, "setUserDataPlus (UserValidateFieldAction)").start();
                        }

                        userTransaction.commit();

                        return ResponseHelper.USER_CHECK_SUCCESS;
                    }
                }
            }

            else if (verificationType.equals("device_number")) {

                // Ricerca l'utente associato al ticket
                String deviceID = verificationField;

                UserBean userBean = ticketBean.getUser();

                if (userBean == null) {

                    // L'utente non esiste
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                    userTransaction.commit();

                    return ResponseHelper.USER_CHECK_FAILURE;
                }
                else {

                    if (userBean.getUserStatus() != User.USER_STATUS_VERIFIED) {

                        // Lo stato dell'utente � diverso da verificato
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid status: expecting verified");

                        userTransaction.commit();

                        return ResponseHelper.USER_CHECK_FAILURE;
                    }
                    else {

                        // Ricerca tra i device associati all'utente quello che si sta verificando
                        UserDeviceBean userDeviceBeanToValidate = null;

                        for (UserDeviceBean userDeviceBean : userBean.getUserDeviceBean()) {

                            if (userDeviceBean.getDeviceId().equals(deviceID) && userDeviceBean.getStatus() == UserDevice.USER_DEVICE_STATUS_PENDING) {
                                userDeviceBeanToValidate = userDeviceBean;
                                break;
                            }
                        }

                        if (userDeviceBeanToValidate == null) {

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pending device not found");

                            userTransaction.commit();

                            return ResponseHelper.USER_CHECK_DEVICEID_FAILURE;
                        }

                        if (!userDeviceBeanToValidate.getVerificationCode().toUpperCase().equals(verificationCode.toUpperCase())) {

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Verification code not valid");

                            userTransaction.commit();

                            return ResponseHelper.USER_CHECK_WRONG_CODE;
                        }
                        
                        // Conta il numero di device verificati associati all'utente
                        int deviceUserCount = 0;
                        if (userBean.getUserDeviceBean() != null) {
                            for(UserDeviceBean userDeviceBeanCheck : userBean.getUserDeviceBean()) {
                                if(userDeviceBeanCheck.getStatus() == UserDevice.USER_DEVICE_STATUS_VERIFIED) {
                                    deviceUserCount++;
                                }
                            }
                        }
                        
                        // Se l'utente ha gi� raggiunto il numero massimo di dispositivi associati bisogna prima eliminare quello utilizzato meno recentemente
                        if (deviceUserCount >= verificationNumberDevice) {
                            
                            System.out.println("Superato numero massimo di device associabili allo stesso utente");
                            
                            // Estrai il device dell'utente utilizzato meno recentemente
                            UserDeviceBean oldUserDeviceBean = null;
                            Date timestampCheck = new Date();
                            
                            System.out.println("Ricerca device da cancellare");
                            
                            for(UserDeviceBean userDeviceBeanCheck : userBean.getUserDeviceBean()) {
                                
                                if(userDeviceBeanCheck.getStatus() == UserDevice.USER_DEVICE_STATUS_VERIFIED) {
                                    
                                    // Se il device ha il timestamp di ultimo utilizzo null, allora � quello da eliminare
                                    if(userDeviceBeanCheck.getLastUsedTimestamp() == null) {
                                        
                                        System.out.println("Trovato device " + userDeviceBeanCheck.getDeviceId() + " con lastUserTimestamp null");
                                        oldUserDeviceBean = userDeviceBeanCheck;
                                        break;
                                    }
                                    
                                    System.out.println("Check device      " + userDeviceBeanCheck.getDeviceId());
                                    System.out.println("LastUsedTimestamp " + userDeviceBeanCheck.getLastUsedTimestamp() + "(" + userDeviceBeanCheck.getLastUsedTimestamp().getTime() + ")");
                                    System.out.println("TimestampCheck    " + timestampCheck                             + "(" + timestampCheck.getTime()                             + ")");
                                    
                                    if(userDeviceBeanCheck.getLastUsedTimestamp().getTime() < timestampCheck.getTime()) {
                                        
                                        System.out.println("Aggiornamento old device da cancellare: " + userDeviceBeanCheck.getDeviceId());
                                        oldUserDeviceBean = userDeviceBeanCheck;
                                        timestampCheck = userDeviceBeanCheck.getLastUsedTimestamp();
                                    }
                                }
                            }
                            
                            if (oldUserDeviceBean != null) {
                                
                                // Trovato device da cancellare
                                System.out.println("Cancellazione device: " + oldUserDeviceBean.getDeviceId());
                                oldUserDeviceBean.setStatus(UserDevice.USER_DEVICE_STATUS_CANCELLED);
                                
                                // Invalidazione di tutti i ticket associati all'utente
                                System.out.println("Invalidazione dei ticket associati all'utente");
                                
                                // Se il ticket � valido imposta il timeout di fine validit� un'ora indietro rispetto all'ora attuale
                                Date expirationTimestamp = new Date();
                                expirationTimestamp.setTime(expirationTimestamp.getTime() - 3600000);
                                
                                List<TicketBean> ticketBeanList = QueryRepository.findTicketByUser(em, userBean);
                                for(TicketBean ticketCheckBean : ticketBeanList) {
                                    if (ticketCheckBean.isValid()) {
                                        System.out.println("Invalidazione ticket " + ticketCheckBean.getTicketId());
                                        ticketCheckBean.setExpirationTimestamp(expirationTimestamp);
                                    }
                                }
                            }
                        }
                        else {
                            
                            if (userBean.getUserDeviceBean() == null) {
                                System.out.println("L'utente non ha ancora associato nessun device");
                            }
                            else {
                                System.out.println("L'utente ha associato " + deviceUserCount + " device (<" + verificationNumberDevice + ") -> cancellazione non necessaria");
                            }
                        }
                        

                        // Imposta in stato cancelled per tutti i device in stato pending (TODO - verificare se corretto)
                        for (UserDeviceBean item : userBean.getUserDeviceBean()) {
                            if (item.getStatus().equals(UserDevice.USER_DEVICE_STATUS_PENDING)) {
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "DeviceID" + item.getDeviceId()
                                        + " change status active to status canceled");
                                item.setStatus(UserDevice.USER_DEVICE_STATUS_CANCELLED);
                                em.merge(item);
                            }
                        }

                        // Aggiorna lo stato del VerificationCode
                        userDeviceBeanToValidate.setStatus(UserDevice.USER_DEVICE_STATUS_VERIFIED);
                        userDeviceBeanToValidate.setAssociationTimestamp(new Timestamp((new Date()).getTime()));
                        // Se il device non � ancora stato associato all'utente effettua l'associazione e invia l'sms di verifica

                        String deviceToken = userDeviceBeanToValidate.getDeviceToken();
                        String deviceData = userDeviceBeanToValidate.getDeviceName();

                        if (deviceToken != null && userBean.getLastLoginDataBean() != null) {

                            // Confronta l'ultimo token utilizzato dall'utente con quello passato in input (TODO - verificare se questo controllo � necessario)
                            String lastToken = userBean.getLastLoginDataBean().getDeviceToken();

                            if (lastToken != null && !lastToken.equals(deviceToken)) {

                                // Se sono diversi cancella il vecchio endpoint e generane un altro
                                String lastEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();

                                if (lastEndpoint != null) {

                                    pushNotificationService.removeEndpoint(lastEndpoint);
                                }

                                createDeviceEndpoint(deviceToken, deviceData, userDeviceBeanToValidate.getDeviceFamily(), requestId,
                                        pushNotificationService);
                            }
                            else {

                                // Se sono uguali rigenera l'endpoint
                                createDeviceEndpoint(deviceToken, deviceData, userDeviceBeanToValidate.getDeviceFamily(), requestId,
                                        pushNotificationService);
                            }

                        }

                        em.merge(userDeviceBeanToValidate);

                        //userBean.setStatusToVerified();
                        // Aggiorna i dati dell'utente
                        em.merge(userBean);
                        em.flush();
                        
                        String email = userBean.getPersonalDataBean().getSecurityDataEmail();
                        
                        UserBean userBeanBusiness = QueryRepositoryBusiness.findNotCancelledUserBusinessByEmailAndSource(em, email, "ENISTATION+");
                        
                        if (userBeanBusiness != null) {
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Trovato utente business collegato. "
                                    + "Aggiornamento automatico del device");
                            
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                                    "Eliminazione di tutti i device dell'utente business collegato...");
                            Query query = em.createQuery("delete from UserDeviceBean u where u.userBean = :userBean");
                            query.setParameter("userBean", userBeanBusiness);
                            int rows = query.executeUpdate();
                            em.flush();
        
                            if (rows <= 0) {
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Eliminazione dei records presenti non andata a buon fine");
                            }
                            else {
                                for (UserDeviceBean userDeviceBean : userBean.getUserDeviceBean()) {
                                    
                                    if (userDeviceBean.getStatus().equals(UserDevice.USER_DEVICE_STATUS_VERIFIED)) {
                                    
                                        UserDeviceBean userBusinessDeviceBean = new UserDeviceBean();
                                        userBusinessDeviceBean.setAssociationTimestamp(new Date());
                                        userBusinessDeviceBean.setCreationTimestamp(new Date());
                                        userBusinessDeviceBean.setDeviceId(userDeviceBean.getDeviceId());
                                        userBusinessDeviceBean.setStatus(userDeviceBean.getStatus());
                                        userBusinessDeviceBean.setUserBean(userBeanBusiness);
                                        
                                        em.persist(userBusinessDeviceBean);
                                    }
                                }
                            }
                        }

                        userTransaction.commit();

                        return ResponseHelper.USER_CHECK_SUCCESS;
                    }
                }
            }

            else {

                // Tipo di verifica non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid verificationType");

                userTransaction.commit();

                return ResponseHelper.USER_CHECK_INVALID_REQUEST;
            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED field validation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private String createDeviceEndpoint(String deviceToken, String deviceData, String deviceFamily, String requestId, PushNotificationServiceRemote pushNotificationService) {
        Platform platform = null;

        if (deviceFamily.equals("AND")) {
            platform = Platform.GCM;
        }

        if (deviceFamily.equals("IPH")) {
            platform = Platform.APNS;
        }

        if (deviceFamily.equals("WPH")) {
            platform = Platform.WNS;
        }

        if (platform != null) {
            PushNotificationResult endpointResult = pushNotificationService.createEndpoint(deviceToken, deviceData, platform);

            if (endpointResult.getStatusCode().equals(com.techedge.mp.pushnotification.adapter.interfaces.StatusCode.PUSH_NOTIFICATION_CREATE_ENDPOINT_SUCCESS)) {
                //pushNotificationService.publishMessage(endpointResult.getArnEndpoint(), "Messaggio di prova", platform);
                return endpointResult.getArnEndpoint();
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Publish message failed! " + endpointResult.getMessage());

                return null;
            }
        }
        else {
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "deviceFamily (" + deviceFamily
                    + ") from requestID doesn't match a valid Platform");

            return null;
        }
    }

}
