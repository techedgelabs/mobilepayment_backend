package com.techedge.mp.core.actions.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.exceptions.PromoVoucherException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PromotionVoucherStatus;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.model.PromoVoucherBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserLoadPromoVoucherAction {

    @Resource
    private EJBContext              context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager           em;

    @EJB
    private LoggerService           loggerService;

    @SuppressWarnings("serial")
    private HashMap<String, String> voucherStatus = new HashMap<String, String>() {
                                                      {
                                                          put(Voucher.VOUCHER_STATUS_ANNULLATO, ResponseHelper.USER_LOAD_VOUCHER_CANCELED);
                                                          put(Voucher.VOUCHER_STATUS_CANCELLATO, ResponseHelper.USER_LOAD_VOUCHER_REMOVED);
                                                          put(Voucher.VOUCHER_STATUS_DA_CONFERMARE, ResponseHelper.USER_LOAD_VOUCHER_TO_CONFIRM);
                                                          put(Voucher.VOUCHER_STATUS_ESAURITO, ResponseHelper.USER_LOAD_VOUCHER_SPENT);
                                                          put(Voucher.VOUCHER_STATUS_INESISTENTE, ResponseHelper.USER_LOAD_VOUCHER_INEXISTENT);
                                                          put(Voucher.VOUCHER_STATUS_SCADUTO, ResponseHelper.USER_LOAD_VOUCHER_EXPIRED);
                                                          put(Voucher.VOUCHER_STATUS_VALIDO, ResponseHelper.USER_LOAD_VOUCHER_VALID);
                                                      }
                                                  };

    public UserLoadPromoVoucherAction() {}

    public String execute(String voucherCode, String verificationValue, Long userId, FidelityServiceRemote fidelityService) throws EJBException {
        // Verifica la validit� del voucher
        Date now = new Date();

        String operationID = new IdGenerator().generateId(16).substring(0, 33);
        PartnerType partnerType = PartnerType.MP;
        VoucherConsumerType voucherConsumerType = VoucherConsumerType.ENI;
        Long requestTimestamp = now.getTime();
        CheckVoucherResult checkVoucherResult;

        List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);

        VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
        voucherCodeDetail.setVoucherCode(voucherCode);
        voucherCodeList.add(voucherCodeDetail);

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();
            
            PromoVoucherBean promoVoucherBean = QueryRepository.findPromoVoucherByCode(em, voucherCode);
            
            if (promoVoucherBean == null) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "No promo voucher found: " + voucherCode);
                userTransaction.commit();
                return ResponseHelper.USER_LOAD_VOUCHER_PROMOTION_VOUCHER_CODE_NOT_VALID;
            }

            UserBean userBean = QueryRepository.findUserById(em, userId);
            
            if (userBean == null) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "No user found for promo voucher: " + userId);
                userTransaction.commit();
                return ResponseHelper.USER_LOAD_VOUCHER_USER_NOT_ASSIGNED;
            }
                        
            promoVoucherBean.assignUser(userBean, verificationValue);
            em.merge(promoVoucherBean);

            try {
                fidelityService = EJBHomeCache.getInstance().getFidelityService();
                checkVoucherResult = fidelityService.checkVoucher(operationID, voucherConsumerType, partnerType, requestTimestamp, voucherCodeList);
            }
            catch (Exception ex) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Error checking voucher: " + ex.getMessage());
                userTransaction.commit();
                return ResponseHelper.USER_LOAD_VOUCHER_TO_RETRY;
            }

            // Verifica l'esito del check
            String checkVoucherStatusCode = checkVoucherResult.getStatusCode();

            if (!checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_OK)) {

                if (checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_MAX_LIMIT_REACHED)) {

                    // Max limit reached
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error checking voucher: max limit reached");
                    userTransaction.commit();
                    return ResponseHelper.USER_LOAD_VOUCHER_MAX_REACHED;
                }
                else {

                    // Error
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error checking voucher: " + checkVoucherStatusCode);
                    userTransaction.commit();
                    return ResponseHelper.USER_LOAD_VOUCHER_GENERIC_ERROR;
                }
            }
            else {

                // Verifica che il check abbia restituito dei risultati
                if (checkVoucherResult.getVoucherList().isEmpty()) {

                    // Empty list
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error checking voucher: empty voucher list in response");
                    userTransaction.commit();
                    return ResponseHelper.USER_LOAD_VOUCHER_VOUCHER_LIST_EMPTY;
                }
                else {

                    // Verifica lo stato del voucher
                    VoucherDetail voucherDetail = checkVoucherResult.getVoucherList().get(0);

                    if (!voucherDetail.getVoucherStatus().equals(Voucher.VOUCHER_STATUS_VALIDO)) {
                        
                        promoVoucherBean.setStatus(PromotionVoucherStatus.INVALID.getValue());
                        promoVoucherBean.setUserBean(null);
                        promoVoucherBean.setVerificationValue(null);
                        em.merge(promoVoucherBean);                        

                        // Invalid voucher
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                                "Error checking voucher: voucher status " + voucherDetail.getVoucherStatus());

                        if (voucherStatus.containsKey(voucherDetail.getVoucherStatus())) {
                            userTransaction.commit();
                            return voucherStatus.get(voucherDetail.getVoucherStatus());
                        }
                        else {
                            //return ResponseHelper.USER_LOAD_VOUCHER_STATUS_UNKNOW;
                            userTransaction.commit();
                            return ResponseHelper.USER_LOAD_VOUCHER_NOT_VALID;
                        }
                    }
                    else {

                        try {
                            promoVoucherBean.consume(userBean);
                            em.merge(promoVoucherBean);

                            VoucherBean newVoucherBean = new VoucherBean();

                            newVoucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                            newVoucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                            newVoucherBean.setInitialValue(voucherDetail.getInitialValue());
                            newVoucherBean.setPromoCode(voucherDetail.getPromoCode());
                            newVoucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                            newVoucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                            newVoucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                            newVoucherBean.setCode(voucherDetail.getVoucherCode());
                            newVoucherBean.setStatus(voucherDetail.getVoucherStatus());
                            newVoucherBean.setType(voucherDetail.getVoucherType());
                            newVoucherBean.setValue(voucherDetail.getVoucherValue());

                            newVoucherBean.setUserBean(userBean);
                            em.persist(newVoucherBean);
                        }
                        catch (PromoVoucherException ex) {
                            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Promo voucher gi� assegnato: " + voucherCode);
                            userTransaction.commit();
                            return ResponseHelper.USER_LOAD_VOUCHER_ERROR;
                        }

                        userTransaction.commit();
                        return ResponseHelper.USER_LOAD_VOUCHER_SUCCESS;
                    }
                }
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user update user payment data with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }

    }
}
