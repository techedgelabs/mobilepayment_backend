package com.techedge.mp.core.actions.user.v2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.QueryRepositoryBusiness;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserBusinessRescuePasswordAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserBusinessRescuePasswordAction() {
    }
    
    
    public String execute(String ticketId, String requestId, String i_mail, EmailSenderRemote emailSender, Integer rescuePasswordExpirationTime,
    		String passwordRecoveryLink, UserCategoryService userCategoryService) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		// Verifica il ticket
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);
    		
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isServiceTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket" );
    			
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_BUSINESS_RESCUE_PASSWORD_INVALID_TICKET;
    		}
    		
    		// Verifica che esista gi� un utente con quella email
	    	UserBean userBean = QueryRepositoryBusiness.findNotCancelledUserBusinessByEmail(em, i_mail);
		    
    		if (userBean == null) {
    			
    			// Esiste gi� un utente con quella email
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User doesn't exist" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_BUSINESS_RESCUE_PASSWORD_NOT_EXISTS;
    		}
    		
            Integer userType = userBean.getUserType();
            Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());
            
            if (!useBusiness) {

                // Utente customer non abilitato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid userType: " + userBean.getUserType());

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_RESCUE_PASSWORD_NOT_EXISTS;
            }

    		if ( (userBean.getUserStatus() != User.USER_STATUS_TEMPORARY_PASSWORD) && (userBean.getUserStatus() != User.USER_STATUS_VERIFIED) ) {
    			
    			// Lo stato dell'utente non consente il recupero della password
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User status not valid for operation" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_BUSINESS_RESCUE_PASSWORD_NOT_EXISTS;
    		}
    		
    		
    		String temporaryRescuePassword = new IdGenerator().generateId(10).substring(0, 8);
    		
    		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Temporary password: "+ temporaryRescuePassword );
    		
    		String encodedPassword = EncoderHelper.encode(temporaryRescuePassword);
    		
    		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Temporary encoded password: "+ encodedPassword );
    		
    		PersonalDataBean personalDataBean = userBean.getPersonalDataBean();
			personalDataBean.setRescuePassword(encodedPassword);
    		
    		Date now = new Date();
			Date expiryDate = DateHelper.addMinutesToDate(rescuePasswordExpirationTime, now);
			personalDataBean.setExpirationDateRescuePassword( expiryDate);
			
    		// Salva i dati dell'utente su db
    		em.merge(personalDataBean);
    				
    		    		
    		// Invia l'email con il codice di attivazione
    		if (emailSender != null) {
    			EmailType emailType = EmailType.RESCUE_PASSWORD_EMAIL_BUSINESS;
    			
                String keyFrom = emailSender.getSender();
                String to = userBean.getPersonalDataBean().getSecurityDataEmail();
    			List<Parameter> parameters = new ArrayList<Parameter>(0);
    			
    			parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
    			parameters.add(new Parameter("PASSWORD_RECOVERY_LINK", passwordRecoveryLink));
    			parameters.add(new Parameter("VERIFICATION_CODE", temporaryRescuePassword));
    			parameters.add(new Parameter("EMAIL", to));
    			
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Sending email to " + to );
    			
    			String result = emailSender.sendEmail(emailType, keyFrom, to, parameters);
    				
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "SendEmail result: " + result );
   			
    		}
    		
    		// Rinnova il ticket
    		ticketBean.renew();
    		em.merge(ticketBean);
    		
    		userTransaction.commit();
    		
    		return ResponseHelper.USER_BUSINESS_RESCUE_PASSWORD_SUCCESS;
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
    		this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
