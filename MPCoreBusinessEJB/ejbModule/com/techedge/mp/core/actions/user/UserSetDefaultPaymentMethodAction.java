package com.techedge.mp.core.actions.user;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserSetDefaultPaymentMethodAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserSetDefaultPaymentMethodAction() {
    }
    
    
    public String execute(
    		String ticketId,
			String requestId,
			Long   paymentMethodId,
			String paymentMethodType,
			String shopLogin) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		// Verifica il ticket
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_SET_DEFAULT_PAYMENT_METHOD_INVALID_TICKET;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if (userBean == null) {
    			
    			// L'utente non esiste
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_SET_DEFAULT_PAYMENT_METHOD_INVALID_TICKET;
    		}
    		
    		PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
    		if (paymentInfoBean == null) {
    			
    			// Payment info non trovato
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment info not found");
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_SET_DEFAULT_PAYMENT_METHOD_NOT_FOUND;
    		}
    		
    		if (paymentInfoBean.getDefaultMethod() == true ) {
    			
    			// Il metodo di pagamento � gi� quello di default
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment info default");
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_SET_DEFAULT_PAYMENT_METHOD_SUCCESS;
    		}
    		else {
    			
    			// Setta come di default il metodo di pagamento in input e rimuovi il flag da tutti gli altri
				for (PaymentInfoBean paymentInfoBeanTemp : userBean.getPaymentData()) {
					
					if ( paymentInfoBeanTemp.getId() == paymentMethodId && paymentInfoBeanTemp.getType().equals(paymentMethodType) ) {
						
						paymentInfoBeanTemp.setDefaultMethod(true);
					}
					else {
						
						if ( paymentInfoBeanTemp.getDefaultMethod() == true ) {
							
							paymentInfoBeanTemp.setDefaultMethod(false);
						}
					}
				}
			}
    		
    		em.merge(userBean);
    		
    		userTransaction.commit();
    		
    		return ResponseHelper.USER_SET_DEFAULT_PAYMENT_METHOD_SUCCESS;
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED setting default payment method with message (" + ex2.getMessage() + ")";
    		this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
