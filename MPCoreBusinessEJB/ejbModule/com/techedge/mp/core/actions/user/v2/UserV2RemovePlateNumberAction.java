package com.techedge.mp.core.actions.user.v2;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PlateNumberBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2RemovePlateNumberAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2RemovePlateNumberAction() {}

    public String execute(String ticketId, String requestId, Long id) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_V2_REMOVE_PLATE_NUMBER_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_V2_REMOVE_PLATE_NUMBER_INVALID_TICKET;
            }
            
            if (userBean.getUserType().equals(User.USER_TYPE_GUEST)) {

                // Utente guest non abilitato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid userType: " + userBean.getUserType());

                userTransaction.commit();

                return ResponseHelper.USER_V2_REMOVE_PLATE_NUMBER_UNAUTHORIZED;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to delete plate number in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_V2_REMOVE_PLATE_NUMBER_UNAUTHORIZED;
            }
            
            // Cerca la targa tra quelle associate all'utente
            PlateNumberBean plateNumberBeanToRemove = null;
            
            if (!userBean.getPlateNumberList().isEmpty()) {
                
                for(PlateNumberBean plateNumberBean : userBean.getPlateNumberList()) {
                    
                    if(plateNumberBean.getId() == id) {
                        
                        plateNumberBeanToRemove = plateNumberBean;
                        break;
                    }
                }
            }
            
            if (plateNumberBeanToRemove == null) {
                
                // La targa non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Plate number " + id + " not found");

                userTransaction.commit();

                return ResponseHelper.USER_V2_REMOVE_PLATE_NUMBER_WRONG;
            }
            
            if (plateNumberBeanToRemove.getUserBean().getId() != userBean.getId()) {
                
                // La targa non � associata all'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Plate number " + id + " not found for user");

                userTransaction.commit();

                return ResponseHelper.USER_V2_REMOVE_PLATE_NUMBER_WRONG;
            }
            
            userBean.getPlateNumberList().remove(plateNumberBeanToRemove);
            em.merge(userBean);
            
            // Se si rimuove la targa di default bisogna impostare un altro default
            if (plateNumberBeanToRemove.getDefaultPlateNumber()) {
                
                if (!userBean.getPlateNumberList().isEmpty()) {
                    
                    for(PlateNumberBean plateNumberBean : userBean.getPlateNumberList()) {
                        
                        plateNumberBean.setDefaultPlateNumber(Boolean.TRUE);
                        em.merge(plateNumberBean);
                        break;
                    }
                }
            }
            
            em.remove(plateNumberBeanToRemove);
            
            userTransaction.commit();

            return ResponseHelper.USER_V2_REMOVE_PLATE_NUMBER_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED removing plate number with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
