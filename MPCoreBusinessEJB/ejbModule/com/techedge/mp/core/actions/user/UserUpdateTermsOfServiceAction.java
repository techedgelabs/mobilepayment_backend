package com.techedge.mp.core.actions.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.utilities.AsyncDWHService;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserUpdateTermsOfServiceAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserUpdateTermsOfServiceAction() {}

    public String execute(String ticketId, String requestId, List<TermsOfService> termsOfServiceList, Integer reconciliationMaxAttempts, UserCategoryService userCategoryService) throws EJBException {

        final UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                return ResponseHelper.USER_UPDATE_TERMS_OF_SERVICE_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_UPDATE_TERMS_OF_SERVICE_INVALID_TICKET;
            }

            for (TermsOfService termsOfService : termsOfServiceList) {

                // Controlla se l'utente ha gi� un elemento con quella chiave
                String keyval = termsOfService.getKeyval();

                System.out.println("Elaborazione TermsOfService con keyval " + keyval);

                TermsOfServiceBean termsOfServiceBeanSearch = null;
                Set<TermsOfServiceBean> termsOfServiceBeanData = userBean.getPersonalDataBean().getTermsOfServiceBeanData();
                for (TermsOfServiceBean termsOfServiceBean : termsOfServiceBeanData) {
                    if (termsOfServiceBean.getKeyval().equals(keyval)) {
                        termsOfServiceBeanSearch = termsOfServiceBean;
                    }
                }

                if (termsOfServiceBeanSearch == null) {

                    System.out.println("Elemento TermsOfService non trovato -> create");

                    TermsOfServiceBean termsOfServiceBean = new TermsOfServiceBean();
                    termsOfServiceBean.setKeyval(keyval);
                    termsOfServiceBean.setAccepted(termsOfService.getAccepted());
                    termsOfServiceBean.setValid(Boolean.TRUE);
                    termsOfServiceBean.setPersonalDataBean(userBean.getPersonalDataBean());
                    userBean.getPersonalDataBean().getTermsOfServiceBeanData().add(termsOfServiceBean);

                    em.persist(termsOfServiceBean);
                }
                else {

                    System.out.println("Elemento TermsOfService trovato -> update");

                    termsOfServiceBeanSearch.setAccepted(termsOfService.getAccepted());
                    termsOfServiceBeanSearch.setValid(Boolean.TRUE);

                    em.merge(termsOfServiceBeanSearch);
                }
            }
            
            // Se non esiste un elemento con keyval GEOLOCALIZZAZIONE_NEW_1 bisogna inserirlo
            TermsOfServiceBean termsOfServiceBeanGeolocalization = null;
            for (TermsOfServiceBean termsOfServiceBean : userBean.getPersonalDataBean().getTermsOfServiceBeanData()) {
                if (termsOfServiceBean.getKeyval().equals("GEOLOCALIZZAZIONE_NEW_1")) {
                    termsOfServiceBeanGeolocalization = termsOfServiceBean;
                    break;
                }
            }
            
            if (termsOfServiceBeanGeolocalization == null) {
                
                // Se non esiste un elemento con keyval GEOLOCALIZZAZIONE_NEW_1 bisogna inserirlo
                TermsOfServiceBean geolocationTermsOfServiceBean = new TermsOfServiceBean();
                geolocationTermsOfServiceBean.setAccepted(Boolean.TRUE);
                geolocationTermsOfServiceBean.setKeyval("GEOLOCALIZZAZIONE_NEW_1");
                geolocationTermsOfServiceBean.setPersonalDataBean(userBean.getPersonalDataBean());
                geolocationTermsOfServiceBean.setValid(Boolean.TRUE);
                userBean.getPersonalDataBean().getTermsOfServiceBeanData().add(geolocationTermsOfServiceBean);
                
                em.persist(geolocationTermsOfServiceBean);
            }
            
            
            // Se non esiste un elemento con keyval NOTIFICATION_NEW_1 bisogna inserirlo
            TermsOfServiceBean termsOfServiceBeanNotification = null;
            for (TermsOfServiceBean termsOfServiceBean : userBean.getPersonalDataBean().getTermsOfServiceBeanData()) {
                if (termsOfServiceBean.getKeyval().equals("NOTIFICATION_NEW_1")) {
                    termsOfServiceBeanNotification = termsOfServiceBean;
                    break;
                }
            }
            
            if (termsOfServiceBeanNotification == null) {
                
                // Se non esiste un elemento con keyval NOTIFICATION_NEW_1 bisogna inserirlo
                TermsOfServiceBean notificationTermsOfServiceBean = new TermsOfServiceBean();
                notificationTermsOfServiceBean.setAccepted(Boolean.TRUE);
                notificationTermsOfServiceBean.setKeyval("NOTIFICATION_NEW_1");
                notificationTermsOfServiceBean.setPersonalDataBean(userBean.getPersonalDataBean());
                notificationTermsOfServiceBean.setValid(Boolean.TRUE);
                userBean.getPersonalDataBean().getTermsOfServiceBeanData().add(notificationTermsOfServiceBean);
                
                em.persist(notificationTermsOfServiceBean);
            }

            Integer userType = userBean.getUserType();
            Boolean useNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            
            if (useNewAcquirerFlow && userBean.getUserStatusRegistrationCompleted() && !userBean.getUserType().equals(User.USER_TYPE_GUEST)) {

                List<TermsOfServiceBean> listTermOfService = QueryRepository.findTermOfServiceByPersonalDataId(em, userBean.getPersonalDataBean());

                if (listTermOfService == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "flagPrivacy null");
                    userTransaction.commit();
                    return ResponseHelper.USER_CHECK_FAILURE;
                }
                
                List<MobilePhoneBean> listMobilePhone = new ArrayList<MobilePhoneBean>();
                String codCard = null;
                
                for (MobilePhoneBean item : userBean.getMobilePhoneList()) {
                    listMobilePhone.add(item);
                }
                
                for (LoyaltyCardBean item : userBean.getLoyaltyCardList()) {
                    if (item.getStatus().equals(LoyaltyCard.LOYALTY_CARD_STATUS_VALIDA)) {
                        codCard = item.getPanCode();
                    }
                }
                
                List<UserSocialDataBean> userSocialDataList = new ArrayList<UserSocialDataBean>();
                
                for (UserSocialDataBean item : userBean.getUserSocialData()) {
                    userSocialDataList.add(item);
                }
                
                AsyncDWHService asyncDWHService = new AsyncDWHService(userTransaction, em, userBean, listMobilePhone, listTermOfService, userSocialDataList, requestId, codCard, reconciliationMaxAttempts);
                
                new Thread(asyncDWHService, "setUserDataPlus (UserUpdateTermsOfServiceAction)").start();
            }

            // Aggiorna i dati dell'utente
            em.merge(userBean);

            // Rinnova il ticket
            ticketBean.renew();
            em.merge(ticketBean);

            userTransaction.commit();

            return ResponseHelper.USER_UPDATE_TERMS_OF_SERVICE_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED terms of service update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
