package com.techedge.mp.core.actions.user.v2;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.LoadVoucherEventCampaignResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.EventCampaignBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.voucher.VoucherEventCampaignBean;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CreateVoucherPromotionalResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserLoadVoucherEventCompaignAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserLoadVoucherEventCompaignAction() {}

    public LoadVoucherEventCampaignResponse execute(String ticketId, String requestId, String voucherCode, Integer maxVoucherCount, FidelityServiceRemote fidelityService,
            UserCategoryService userCategoryService, Long validInterval) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            LoadVoucherEventCampaignResponse loadVoucherEventCampaignResponse = new LoadVoucherEventCampaignResponse();
            
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_INVALID_TICKET);
                
                return loadVoucherEventCampaignResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_INVALID_TICKET);
                
                return loadVoucherEventCampaignResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to load voucher in status " + userStatus);

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_UNAUTHORIZED);
                
                return loadVoucherEventCampaignResponse;
            }

            Boolean isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());

            if (!isNewAcquirerFlow) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "User is not in new aquirer flow category: " + userBean.getUserType());

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_V2_LOAD_VOUCHER_EVENT_COMPAIGN_UNAUTHORIZED);
                
                return loadVoucherEventCampaignResponse;
            }
            
            if (!voucherCode.matches("^[a-zA-Z]+-(.)*")) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "voucher code format error: " + voucherCode);

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_VOUCHER_CODE_INVALID_FORMAT);
                
                return loadVoucherEventCampaignResponse;
            }

            // Verifica che l'utente non abbia gi� associati troppi voucher
            List<VoucherBean> voucherBeanList = QueryRepository.findActiveVoucherByUserBean(em, userBean);

            Integer voucherCount = voucherBeanList.size();

            System.out.println("voucherCount: " + voucherCount);
            System.out.println("maxVoucherCount: " + maxVoucherCount);

            if (voucherCount >= maxVoucherCount) {

                // Voucher gi� utilizzato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Reached max voucher number");

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_MAX_REACHED);
                
                return loadVoucherEventCampaignResponse;
            }

            // Verifica sul codice ricevuto in input

            if (voucherCode.indexOf("-") == -1) {

                // Formato codice non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid format voucherCode");

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_VOUCHER_CODE_INVALID_FORMAT);
                
                return loadVoucherEventCampaignResponse;
            }

            Date now = new Date();
            
            if (voucherCode != null && voucherCode.startsWith("GL")) {
                
                // Verifica che il servizio sia stato chiamato nel week-end
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(now);
                int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
                if (dayOfWeek != Calendar.FRIDAY && dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY) {
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid day of week: " + dayOfWeek + " (date: " + now + ")");
    
                    userTransaction.commit();
    
                    loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_CAMPAIGN_NOT_ACTIVE);
                    
                    return loadVoucherEventCampaignResponse;
                }
            }

            String[] voucherCodeSplit = voucherCode.split("-");

            String eventCompaignPromoCode = voucherCodeSplit[0];

            EventCampaignBean eventCampaignBean = QueryRepository.findEventCampaignByPromoCode(em, eventCompaignPromoCode);

            if (eventCampaignBean == null) {

                // Nessun eventCampaignBean trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EventCampaign with code: " + eventCompaignPromoCode
                        + " not found");

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_NO_VALID_CAMPAIGN);
                
                return loadVoucherEventCampaignResponse;
            }

            if (now.getTime() < eventCampaignBean.getStartData().getTime() || now.getTime() > eventCampaignBean.getEndData().getTime()) {

                // EventCampaignBean non valida alla data attuale
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EventCampaign with code: " + eventCompaignPromoCode
                        + " not active");

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_CAMPAIGN_NOT_ACTIVE);
                
                return loadVoucherEventCampaignResponse;

            }

            
            if (eventCampaignBean.getCode().equals("CP")) {
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EventCampaign with code: " + eventCompaignPromoCode
                        + " registrationTimestamp check disabled");
            }
            else {
                
                Date registrationTimestamp = userBean.getUserStatusRegistrationTimestamp();
                
                if (registrationTimestamp == null || registrationTimestamp.getTime() < eventCampaignBean.getStartData().getTime()
                        || registrationTimestamp.getTime() > eventCampaignBean.getEndData().getTime()) {
    
                    // L'utente non ha completato la registrazione durante il periodo di validit� della campagna 
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                            "UserBean registration timestamp not compatible with EventCampaign " + eventCompaignPromoCode + "");
    
                    userTransaction.commit();
    
                    loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_USER_REGISTRATION_NOT_IN_PERIOD);
                    
                    return loadVoucherEventCampaignResponse;
    
                }
            }

            VoucherEventCampaignBean voucherEventCampaignBean = QueryRepository.findVoucherEventCampaign(em, userBean, eventCampaignBean);

            if (voucherEventCampaignBean != null) {

                // L'utente ha gi� usufruito della promozione
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "UserBean has already participated in this campaign");

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_USED);
                
                return loadVoucherEventCampaignResponse;
            }

            EncryptionAES encryptionAES = new EncryptionAES();
            encryptionAES.loadSecretKey(eventCampaignBean.getDecodeSecretKey());

            String voucherCodeDecrypt = null;
            
            try {
            
                voucherCodeDecrypt = encryptionAES.decrypt(voucherCodeSplit[1]);
            }
            catch (Exception ex) {
                
                ex.printStackTrace();
                
                voucherCodeDecrypt = null;
            }

            if (voucherCodeDecrypt == null || voucherCodeDecrypt.isEmpty()) {

                //  Errore nella procedura di decodifica del codice
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Decrypt failed");

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_DECRYPT_ERROR);
                
                return loadVoucherEventCampaignResponse;
            }

            String[] splitCode = voucherCodeDecrypt.split("_");

            if (splitCode.length != 5) {

                // Formato del codice decrittato non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid format decrypted voucherCode");

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_INVALID_STRING);
                
                return loadVoucherEventCampaignResponse;
            }

            //String eventCode = splitCode[0];
            String campaignCode = splitCode[1];
            String timestamp = splitCode[2];
            //String progressive = splitCode[3];
            //String idDevice = splitCode[4];

            if (!eventCompaignPromoCode.equals(campaignCode)) {

                //  Il codice campagna non matcha con il promoCode del voucher
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "EventCampaign don't match with CampaignCode");

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_CODE_NOT_MATCH_WITH_PROMO_CODE);
                
                return loadVoucherEventCampaignResponse;
            }

            double timestampDouble = Double.parseDouble(timestamp);
            Long timestampLong = (long) timestampDouble;
            
            System.out.println("voucher timestamp: " + timestampLong);
            System.out.println("validInterval: " + validInterval);
            System.out.println("sum: " + (timestampLong + validInterval));
            System.out.println("now: " + (now.getTime() / 1000));
            
            if (timestampLong + validInterval < (now.getTime() / 1000)) {

                //  Il voucher non risulta pi� valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Voucher code expired");

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_EXPIRED_CODE);
                
                return loadVoucherEventCampaignResponse;
            }

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            PartnerType partnerType = PartnerType.MP;
            VoucherConsumerType voucherType = VoucherConsumerType.ENI;
            long requestTimestamp = new Date().getTime();
            String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();

            if (eventCampaignBean.getTotalAmount() == null) {

                //  Invalid totalAmount for EventCampaign
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid totalAmount for EventCampaign "
                        + eventCompaignPromoCode);

                userTransaction.commit();

                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_NOT_VALID);
                
                return loadVoucherEventCampaignResponse;
            }

            BigDecimal totalAmount = BigDecimal.valueOf(eventCampaignBean.getTotalAmount());
            String promoCode = eventCampaignBean.getPromoCode();

            CreateVoucherPromotionalResult createVoucherResult = new CreateVoucherPromotionalResult();

            String response;

            try {

                createVoucherResult = fidelityService.createVoucherPromotional(operationID, voucherType, partnerType, requestTimestamp, fiscalCode, promoCode, totalAmount);

                if (createVoucherResult.getStatusCode().equals(FidelityResponse.CREATE_VOUCHER_PROMOTIONAL_OK)) {

                    response = ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_SUCCESS;
                    System.out.println("Creazione del voucher promozionale (" + promoCode + ") per l'utente (" + userBean.getId() + ")");
                }
                else {
                    
                    if (createVoucherResult.getStatusCode().equals(FidelityResponse.CREATE_VOUCHER_PROMOTIONAL_FISCAL_CODE_NOT_FOUND)) {
                        response = ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_FISCAL_CODE_NOT_FOUND;
                        System.out.println("Errore nella creazione del voucher promozionale (" + promoCode + ") per l'utente (" + userBean.getId() + "): "
                                + createVoucherResult.getMessageCode());
                    }
                    
                    if (createVoucherResult.getStatusCode().equals(FidelityResponse.CREATE_VOUCHER_PROMOTIONAL_INVALID_FISCAL_CODE)) {
                        response = ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_INVALID_FISCAL_CODE;
                        System.out.println("Errore nella creazione del voucher promozionale (" + promoCode + ") per l'utente (" + userBean.getId() + "): "
                                + createVoucherResult.getMessageCode());
                    }
                    
                    if (createVoucherResult.getStatusCode().equals(FidelityResponse.CREATE_VOUCHER_PROMOTIONAL_INVALID_PROMO_CODE)) {
                        response = ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_INVALID_PROMO_CODE;
                        System.out.println("Errore nella creazione del voucher promozionale (" + promoCode + ") per l'utente (" + userBean.getId() + "): "
                                + createVoucherResult.getMessageCode());
                    }
                    
                    response = ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_GENERIC_ERROR;
                    System.out.println("Errore nella creazione del voucher promozionale (" + promoCode + ") per l'utente (" + userBean.getId() + "): "
                            + createVoucherResult.getMessageCode());
                }

                LoadVoucherEventCampaignResponse associateResponse = new LoadVoucherEventCampaignResponse();

                if (response.equals(ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_SUCCESS)) {
                    associateResponse = associateToUser(userBean, createVoucherResult.getVoucher(), eventCampaignBean, now, voucherCode);
                }
                else {
                    associateResponse.setStatusCode(response);
                }
                
                userTransaction.commit();

                return associateResponse;

            }
            catch (Exception ex) {

                System.err.println("Errore nella creazione del voucher promozionale (" + eventCampaignBean.getPromoCode() + "): " + ex.getMessage());

                createVoucherResult.setStatusCode(FidelityResponse.CREATE_VOUCHER_PROMOTIONAL_GENERIC_ERROR);
                createVoucherResult.setMessageCode(ex.getMessage());
                createVoucherResult.setCsTransactionID(null);
                
                loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_ERROR);
                
                userTransaction.commit();

                return loadVoucherEventCampaignResponse;
            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED load voucher with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    public LoadVoucherEventCampaignResponse associateToUser(UserBean userBean, VoucherDetail voucherDetail, EventCampaignBean eventCampaignBean, Date insertTimestamp, String request) {

        LoadVoucherEventCampaignResponse loadVoucherEventCampaignResponse = new LoadVoucherEventCampaignResponse();

        try {

            VoucherBean voucherBean = new VoucherBean();
            voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
            voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
            voucherBean.setInitialValue(voucherDetail.getInitialValue());
            voucherBean.setPromoCode(voucherDetail.getPromoCode());
            voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
            voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
            voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
            voucherBean.setCode(voucherDetail.getVoucherCode());
            voucherBean.setStatus(voucherDetail.getVoucherStatus());
            voucherBean.setType(voucherDetail.getVoucherType());
            voucherBean.setValue(voucherDetail.getVoucherValue());
            voucherBean.setIsCombinable(voucherDetail.getIsCombinable());
            voucherBean.setMinAmount(voucherDetail.getMinAmount());
            voucherBean.setMinQuantity(voucherDetail.getMinQuantity());
            voucherBean.setPromoPartner(voucherDetail.getPromoPartner());
            voucherBean.setUserBean(userBean);

            em.persist(voucherBean);

            VoucherEventCampaignBean voucherEventCampaignBean = new VoucherEventCampaignBean();
            voucherEventCampaignBean.setEventCampaignBean(eventCampaignBean);
            voucherEventCampaignBean.setInsertTimestamp(insertTimestamp);
            voucherEventCampaignBean.setRequest(request);
            voucherEventCampaignBean.setStatus("1");
            voucherEventCampaignBean.setUserBean(userBean);
            voucherEventCampaignBean.setVoucherBean(voucherBean);
            em.persist(voucherEventCampaignBean);

            System.out.println("Associato voucher promozionale (" + voucherDetail.getPromoCode() + ") all'utente (" + userBean.getId() + ")");
            
            
            loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_SUCCESS);
            loadVoucherEventCampaignResponse.setPromoName(voucherDetail.getPromoDescription());
            
            return loadVoucherEventCampaignResponse;
        }
        catch (Exception ex) {
            
            System.err.println("Errore nell'associazione voucher promozionale (" + voucherDetail.getPromoCode() + ") all'utente (" + userBean.getId() + ")");
            loadVoucherEventCampaignResponse.setStatusCode(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_ASSOCIATION_ERROR);
        }

        return loadVoucherEventCampaignResponse;
    }

}
