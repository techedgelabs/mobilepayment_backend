package com.techedge.mp.core.actions.user;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SmsStatusType;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.SmsLogBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserUpdateSmsLogAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserUpdateSmsLogAction() {
    }
    
    
    public String execute(
    		String correlationID, String destinationAddress, Integer maxRetryAttemps, String message, String mtMessageID,  
    		Integer statusCode, Integer responseCode, Integer reasonCode, String responseMessage, Date operatorTimestamp, Date providerTimestamp, String operator, boolean firstUpdate) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		if (firstUpdate) {
    		    
    		    MobilePhoneBean mobilePhoneBean = QueryRepository.findMobilePhoneByNumber(em, destinationAddress.substring(2));
    		    
    		    if (mobilePhoneBean == null) {
                    // Ticket non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User not found" );
                    
                    userTransaction.commit();
                    
                    return ResponseHelper.USER_UPDATE_SMS_LOG_INVALID_USER;
    		    }
    		    
                SmsLogBean smsLogBean = new SmsLogBean();
                smsLogBean.setCorrelationID(correlationID);
                smsLogBean.setStatus(SmsStatusType.PENDING.getCode());
                smsLogBean.setCreationTimestamp(new Date());
                smsLogBean.setDestinationAddress(destinationAddress);
                smsLogBean.setRetryAttemptsLeft(maxRetryAttemps);
                smsLogBean.setUser(mobilePhoneBean.getUser());
                smsLogBean.setMessage(message);
                
                em.persist(smsLogBean);
    		    
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Creazione del record con correlationID: " + correlationID
                        + " e destinatario: " + destinationAddress);
                
                userTransaction.commit();
                
                return ResponseHelper.USER_UPDATE_SMS_LOG_SUCCESS;
    		}
    		
    		
    		
    		SmsLogBean smsLogBean = QueryRepository.findSmsLogByCorrelationID(em, correlationID);
    		
            if ( smsLogBean == null ) {
                
                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "CorrelationID not found" );
                
                userTransaction.commit();
                
                return ResponseHelper.USER_UPDATE_SMS_LOG_INVALID_CORRELATIONID;
            }
            

    		smsLogBean.setMtMessageID(mtMessageID);
    		
    		if (statusCode != null) {
    		    smsLogBean.setStatus(statusCode);
    		}
    		
    		if (responseCode != null) {
    		    smsLogBean.setResponseCode(responseCode);
    		}
    		
    		if (responseMessage != null) {
    		    smsLogBean.setResponseMessage(responseMessage);
    		}
    		
    		smsLogBean.setReasonCode(reasonCode);
    		smsLogBean.setOperatorTimestamp(operatorTimestamp);
    		smsLogBean.setProviderTimestamp(providerTimestamp);
    		smsLogBean.setOperator(operator);
    		em.merge(smsLogBean);
    		
    		userTransaction.commit();
    		
    		return ResponseHelper.USER_UPDATE_SMS_LOG_SUCCESS;
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String errorMessage = "FAILED update sms log with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, errorMessage );
			    
	        throw new EJBException(ex2);
    	}
    }
}
