package com.techedge.mp.core.actions.user;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.CityInfo;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveCitiesData;
import com.techedge.mp.core.business.model.CityInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserRetrieveCitiesDataAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserRetrieveCitiesDataAction() {
    }
    
    
    public RetrieveCitiesData execute(
    		String ticketId,
			String requestId,
			String searchKey,
			Integer maxResults) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);
		    
    		if ( ticketBean == null || !ticketBean.isValid() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket" );
				
    			userTransaction.commit();
    			
    			RetrieveCitiesData retrieveCitiesData = new RetrieveCitiesData();
    			retrieveCitiesData.setStatusCode(ResponseHelper.RETRIEVE_CITIES_INVALID_TICKET);
    			return retrieveCitiesData;
    		}
    		
    		if ( searchKey == null || searchKey.length() < 2 ) {
    			
    			// Chiave di ricerca non valida
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid search key" );
				
    			userTransaction.commit();
    			
    			RetrieveCitiesData retrieveCitiesData = new RetrieveCitiesData();
    			retrieveCitiesData.setStatusCode(ResponseHelper.RETRIEVE_CITIES_FAILED);
    			return retrieveCitiesData;
    		}
    		
    		
    		// Genera la risposta con l'elenco delle citt�
    		
    		RetrieveCitiesData retrieveCitiesData = new RetrieveCitiesData();
    		
    		List<CityInfoBean> cityInfoBeanList = QueryRepository.findCitiesByKey(em, searchKey, maxResults);
		    
    		if ( cityInfoBeanList != null ) {
    			
    			for( CityInfoBean cityInfoBean : cityInfoBeanList ) {
    				
    				CityInfo cityInfo = cityInfoBean.toCityInfo();
    				
    				retrieveCitiesData.getCities().add(cityInfo);
    			}
    		}
    		
    		retrieveCitiesData.setStatusCode(ResponseHelper.RETRIEVE_CITIES_SUCCESS);
    		
    		userTransaction.commit();
    		
    		return retrieveCitiesData;
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED retrieve cities data with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
