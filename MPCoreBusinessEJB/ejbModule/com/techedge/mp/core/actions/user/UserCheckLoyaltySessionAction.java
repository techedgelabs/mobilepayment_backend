package com.techedge.mp.core.actions.user;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltySessionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserCheckLoyaltySessionAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserCheckLoyaltySessionAction() {
    }
    
    
    public String execute(String sessionId, String fiscalCode, Integer sessionExpiryTime) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		/* Modifica controllo di corrispondenza sessionId e fiscalcode */
    		/*
    		LoyaltySessionBean loyaltySessionBean = QueryRepository.findLoyaltySessionByFiscalCode(em, sessionId, fiscalCode);
    		
    		if (loyaltySessionBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Session not found");
                
                userTransaction.commit();
                
                return ResponseHelper.USER_CHECK_LOYALTY_SESSION_NOT_FOUND;    		    
    		}
    		*/
    		
    		LoyaltySessionBean loyaltySessionBean = QueryRepository.findLoyaltySessionBySessionid(em, sessionId);
    		
    		if (loyaltySessionBean == null) {
    		    
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Session not found");
                
                userTransaction.commit();
                
                return ResponseHelper.USER_CHECK_LOYALTY_SESSION_NOT_FOUND;
            }
    		
    		if (loyaltySessionBean.getFiscalCode() == null || !loyaltySessionBean.getFiscalCode().equalsIgnoreCase(fiscalCode)) {
    		    
    		    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Session Id with different fiscalCode (" + loyaltySessionBean.getFiscalCode() +")");
                
                userTransaction.commit();
                
                return ResponseHelper.USER_CHECK_LOYALTY_SESSION_NOT_FOUND;
    		}
    		
			UserBean userBean = loyaltySessionBean.getUserBean();
			
			// Verifica lo stato dell'utente
			if ( userBean.getUserStatus() != User.USER_STATUS_VERIFIED ) {
				
				// Un utente che si trova in questo stato non pu� invocare questo servizio
				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User in status invalid " + userBean.getUserStatus());
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_CHECK_LOYALTY_SESSION_INVALID_USER_STATUS;
			}
			
			if (!loyaltySessionBean.isValid()) {
                
                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Session expired");
                
                userTransaction.commit();
                
                return ResponseHelper.USER_CHECK_LOYALTY_SESSION_EXPIRED;
            }
			
			loyaltySessionBean.renew(sessionExpiryTime);
			em.merge(loyaltySessionBean);
			
    		userTransaction.commit();
    		
			return ResponseHelper.USER_CHECK_LOYALTY_SESSION_SUCCESS;
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED check authorization with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
