package com.techedge.mp.core.actions.user.v2;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PlateNumberBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2SetDefaultPlateNumberAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2SetDefaultPlateNumberAction() {}

    public String execute(String ticketId, String requestId, Long id) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_V2_SET_DEFAULT_PLATE_NUMBER_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_V2_SET_DEFAULT_PLATE_NUMBER_INVALID_TICKET;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to set default plate number in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_V2_SET_DEFAULT_PLATE_NUMBER_UNAUTHORIZED;
            }
            
            // Cerca la targa tra quelle associate all'utente
            PlateNumberBean plateNumberBeanToUpdate = null;
            
            if (!userBean.getPlateNumberList().isEmpty()) {
                
                for(PlateNumberBean plateNumberBean : userBean.getPlateNumberList()) {
                    
                    if(plateNumberBean.getId() == id) {
                        
                        plateNumberBeanToUpdate = plateNumberBean;
                        break;
                    }
                }
            }
            
            if (plateNumberBeanToUpdate == null) {
                
                // La targa non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Plate number " + id + " not found");

                userTransaction.commit();

                return ResponseHelper.USER_V2_SET_DEFAULT_PLATE_NUMBER_WRONG;
            }
            
            if (plateNumberBeanToUpdate.getUserBean().getId() != userBean.getId()) {
                
                // La targa non � associata all'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Plate number " + id + " not found for user");

                userTransaction.commit();

                return ResponseHelper.USER_V2_SET_DEFAULT_PLATE_NUMBER_WRONG;
            }
            
            if (plateNumberBeanToUpdate.getDefaultPlateNumber()) {
                
                // La targa � gi� quella di default
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Plate number " + id + " is default");

                userTransaction.commit();

                return ResponseHelper.USER_V2_SET_DEFAULT_PLATE_NUMBER_SUCCESS;
            }
            
            
            // Prima bisogna eliminare il vecchio flag di default
            if (!userBean.getPlateNumberList().isEmpty()) {
                
                for(PlateNumberBean plateNumberBean : userBean.getPlateNumberList()) {
                    
                    if (plateNumberBean.getId() == plateNumberBeanToUpdate.getId()) {
                        
                        plateNumberBean.setDefaultPlateNumber(Boolean.TRUE);
                    }
                    else {
                        
                        plateNumberBean.setDefaultPlateNumber(Boolean.FALSE);
                    }
                }
            }
            
            em.merge(userBean);
            
            userTransaction.commit();

            return ResponseHelper.USER_V2_SET_DEFAULT_PLATE_NUMBER_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED setting default plate number with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
