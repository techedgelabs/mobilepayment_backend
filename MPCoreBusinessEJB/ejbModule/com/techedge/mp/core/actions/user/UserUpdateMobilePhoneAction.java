package com.techedge.mp.core.actions.user;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SendValidationResult;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.PrefixNumberBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.ResendValidation;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserUpdateMobilePhoneAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private UserCategoryService userCategoryService;

    @EJB
    private LoggerService       loggerService;

    public UserUpdateMobilePhoneAction() {}

    public String execute(String ticketId, String requestId, String mobilePhonePrefix, String mobilePhoneNumber, Boolean checkMobilePhoneUnique,
            Integer verificationCodeExpiryTime, Integer maxRetryAttemps, String sendingType, UserService userService, EmailSenderRemote emailSender, 
            SmsServiceRemote smsService, UserCategoryService userCategoryService, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                return ResponseHelper.MOBILE_PHONE_UPDATE_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.MOBILE_PHONE_UPDATE_INVALID_TICKET;
            }

            Integer userType = userBean.getUserType();
            String userCategoryNewFlow = UserCategoryType.NEW_PAYMENT_FLOW.getCode();
            String userCategoryNewAcquirer = UserCategoryType.NEW_ACQUIRER_FLOW.getCode();
            Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, userCategoryNewFlow);
            Boolean useNewAcquirer = userCategoryService.isUserTypeInUserCategory(userType, userCategoryNewAcquirer);
            Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());

            if (userType.equals(User.USER_TYPE_GUEST)) {
                
                // L'utente guest non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update mobile phone for user of type " + userType);

                userTransaction.commit();

                return ResponseHelper.MOBILE_PHONE_UPDATE_UNAUTHORIZED;
            }
            
            if (!useNewFlow && !useNewAcquirer && !useBusiness) {

                // Un utente che esegue il vecchio flusso voucher non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update mobile phone for user of type " + userType);

                userTransaction.commit();

                return ResponseHelper.MOBILE_PHONE_UPDATE_UNAUTHORIZED;
            }

            System.out.println("User type: " + userType + "  User new acquirer: " + useNewAcquirer + "    User new flow: " + useNewFlow);
            
            if (useBusiness && Objects.equals(userBean.getSource(), "ENISTATION+")) {
                // L'utente social non pu� modificare la password
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update mobile phone for user business with source ENISTATION+" );
                
                userTransaction.commit();
                
                return ResponseHelper.MOBILE_PHONE_UPDATE_INVALID_USER_SOURCE;
            }
            
            
            // Verifica che il prefisso sia valido
            PrefixNumberBean prefixNumberBean = QueryRepository.findPrefixByCode(em, mobilePhonePrefix);
            if (prefixNumberBean == null) {

                // Prefisso non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Prefix " + mobilePhonePrefix + " not valid");

                userTransaction.commit();

                return ResponseHelper.MOBILE_PHONE_UPDATE_INVALID_PREFIX;
            }

            // Verifica che il numero di telefono non sia associato a un altro utente
            if (checkMobilePhoneUnique) {

                System.out.println("Controllo di univocit� del numero di cellulare");
                
                /*
                MobilePhoneBean mobilePhoneExist = QueryRepository.findActiveMobilePhoneByNumberAndPrefix(em, mobilePhoneNumber, mobilePhonePrefix);
                if (mobilePhoneExist != null) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone exists");

                    userTransaction.commit();

                    return ResponseHelper.MOBILE_PHONE_UPDATE_NUMBER_EXISTS;
                }
                */
                
                Boolean mobilePhoneFound = false;
                List<MobilePhoneBean> mobilePhoneBeanList = QueryRepository.findAllActiveMobilePhoneByNumberAndPrefix(em, mobilePhoneNumber, mobilePhonePrefix);

                if (mobilePhoneBeanList != null && !mobilePhoneBeanList.isEmpty()) {

                    for (MobilePhoneBean mobilePhoneBean : mobilePhoneBeanList) {

                        System.out.println("Trovato telefono per utente in stato " + mobilePhoneBean.getUser().getUserStatus());

                        if (mobilePhoneBean.getUser().getUserStatus() != User.USER_STATUS_NEW && mobilePhoneBean.getUser().getUserStatus() != User.USER_STATUS_CANCELLED) {

                            Integer userTypeFound = mobilePhoneBean.getUser().getUserType();
                            
                            Boolean userFoundNewAcquirer = userCategoryService.isUserTypeInUserCategory(userTypeFound, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
                            Boolean userFoundBusiness    = userCategoryService.isUserTypeInUserCategory(userTypeFound, UserCategoryType.BUSINESS.getCode());
                            
                            if( (useNewAcquirer && userFoundNewAcquirer) || (useBusiness && userFoundBusiness) ) {
                            
                                System.out.println("Il telefono � gi� stato usato da un utente in stato " + mobilePhoneBean.getUser().getUserStatus());
                                mobilePhoneFound = true;
                                break;
                            }
                        }
                    }
                }

                if (mobilePhoneFound) {
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone exists");

                    userTransaction.commit();

                    return ResponseHelper.MOBILE_PHONE_UPDATE_NUMBER_EXISTS;
                }
            }

            Set<MobilePhoneBean> mobilePhoneList = userBean.getMobilePhoneList();

            if (!mobilePhoneList.isEmpty()) {

                for (MobilePhoneBean item : mobilePhoneList) {
                    if (item.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_PENDING)) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Number " + item.getNumber()
                                + " change status pending to status canceled");
                        item.setStatus(MobilePhone.MOBILE_PHONE_STATUS_CANCELED);
                    }
                    /*
                     * if (item.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_PENDING)) {
                     * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Exist mobile phone pending");
                     * userTransaction.commit();
                     * return ResponseHelper.MOBILE_PHONE_UPDATE_CHECK_FAILURE;
                     * }
                     * else if (item.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE)) {
                     * if (item.getNumber().equals(mobilePhoneNumber) && item.getPrefix().equals(mobilePhonePrefix)) {
                     * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "This number is active");
                     * userTransaction.commit();
                     * return ResponseHelper.MOBILE_PHONE_UPDATE_CHECK_FAILURE;
                     * }
                     * }
                     */
                }
            }

            MobilePhoneBean newNumber = new MobilePhoneBean();
            newNumber.setNumber(mobilePhoneNumber);
            newNumber.setUser(userBean);
            //String verificationCode = new IdGenerator().generateId(32).substring(0, 8).toUpperCase();
            String verificationCode = new IdGenerator().generateNumericId(8);
            System.out.println("Generato codice di verifica " + verificationCode + " per cellulare " + newNumber.getNumber());
            newNumber.setVerificationCode(verificationCode);
            Calendar now = Calendar.getInstance();
            Date expiryDate = DateHelper.addMinutesToDate(verificationCodeExpiryTime, now.getTime());
            newNumber.setCreationTimestamp(now.getTime());
            newNumber.setLastUsedTimestamp(now.getTime());
            newNumber.setExpirationTimestamp(expiryDate);
            newNumber.setPrefix(mobilePhonePrefix);

            newNumber.setStatus(MobilePhone.MOBILE_PHONE_STATUS_PENDING);
            mobilePhoneList.add(newNumber);

            userBean.setMobilePhoneList(mobilePhoneList);
            // Aggiorna il numero di telefono
            em.merge(userBean);

            // Rinnova il ticket
            ticketBean.renew();
            em.merge(ticketBean);

            //Invio numero di telefono dopo l'update
            SendValidationResult result = ResendValidation.send(sendingType, maxRetryAttemps, userBean, newNumber, loggerService, emailSender, smsService, 
                    userCategoryService, stringSubstitution);

            if (result.equals(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error in verification mobile phone. "
                        + result.getMessageCode() + " (" + result.getErrorCode() + ")");

                userTransaction.commit();

                return ResponseHelper.MOBILE_PHONE_UPDATE_SENDING_FAILURE;
            }

            userTransaction.commit();

            return ResponseHelper.MOBILE_PHONE_UPDATE_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED mobile phone update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
