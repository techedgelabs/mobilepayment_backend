package com.techedge.mp.core.actions.user.v2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserBusinessClonePaymentMethodAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserBusinessClonePaymentMethodAction() {}

    public String execute(String ticketId, String requestId, String encodedPin, EmailSenderRemote emailSender) throws EJBException {
        
        UserTransaction userTransaction = context.getUserTransaction();

        try {
            
            userTransaction.begin();
            
            // Verifica il ticket
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_CLONE_PAYMENT_METHOD_INVALID_TICKET;
            }
            
            UserBean userBean = ticketBean.getUser();
            
            if (userBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found by ticket");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_CLONE_PAYMENT_METHOD_INVALID_TICKET;
            }
            
            UserBean userCustomerBean = QueryRepository.findUserCustomerByEmail(em, userBean.getPersonalDataBean().getSecurityDataEmail());
            
            if (userCustomerBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User customer not found by source token");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_CLONE_PAYMENT_METHOD_INVALID_SOURCE_TOKEN;
            }
            
            String userCustomerEncodedPin = userCustomerBean.getPaymentEncodedPin();
            
            if (!Objects.equals(encodedPin, userCustomerEncodedPin)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User customer payment pin doesn't match");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_CLONE_PAYMENT_METHOD_INVALID_PIN;
            }

            List<PaymentInfoBean> userCustomerPaymentMethodCreditCardList = userCustomerBean.getPaymentMethodTypeCreditCardList();

            if (userCustomerPaymentMethodCreditCardList.isEmpty()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User customer payment credit card is empty");

                userTransaction.commit();

                return ResponseHelper.USER_BUSINESS_CLONE_PAYMENT_METHOD_CREDIT_CARD_NOT_FOUND;
            }
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Import user customer payments in status verified");
            
            PaymentInfoBean userCustomerVoucherPaymentInfoBean = userCustomerBean.getVoucherPaymentMethod();
            PaymentInfoBean paymentInfoBean = new PaymentInfoBean();
            paymentInfoBean.setBrand(userCustomerVoucherPaymentInfoBean.getBrand());
            paymentInfoBean.setCardBin(userCustomerVoucherPaymentInfoBean.getCardBin());
            paymentInfoBean.setAttemptsLeft(userCustomerVoucherPaymentInfoBean.getAttemptsLeft());
            paymentInfoBean.setCheckAmount(userCustomerVoucherPaymentInfoBean.getCheckAmount());
            paymentInfoBean.setCheckBankTransactionID(userCustomerVoucherPaymentInfoBean.getCheckBankTransactionID());
            paymentInfoBean.setCheckCurrency(userCustomerVoucherPaymentInfoBean.getCheckCurrency());
            paymentInfoBean.setCheckShopLogin(userCustomerVoucherPaymentInfoBean.getCheckShopLogin());
            paymentInfoBean.setCheckShopTransactionID(userCustomerVoucherPaymentInfoBean.getCheckShopTransactionID());
            paymentInfoBean.setDefaultMethod(userCustomerVoucherPaymentInfoBean.getDefaultMethod());
            paymentInfoBean.setExpirationDate(userCustomerVoucherPaymentInfoBean.getExpirationDate());
            paymentInfoBean.setInsertTimestamp(userCustomerVoucherPaymentInfoBean.getInsertTimestamp());
            paymentInfoBean.setMessage(userCustomerVoucherPaymentInfoBean.getMessage());
            paymentInfoBean.setPan(userCustomerVoucherPaymentInfoBean.getPan());
            paymentInfoBean.setPin(userCustomerVoucherPaymentInfoBean.getPin());
            paymentInfoBean.setPinCheckAttemptsLeft(userCustomerVoucherPaymentInfoBean.getPinCheckAttemptsLeft());
            paymentInfoBean.setStatus(userCustomerVoucherPaymentInfoBean.getStatus());
            paymentInfoBean.setToken(userCustomerVoucherPaymentInfoBean.getToken());
            paymentInfoBean.setToken3ds(userCustomerVoucherPaymentInfoBean.getToken3ds());
            paymentInfoBean.setType(userCustomerVoucherPaymentInfoBean.getType());
            paymentInfoBean.setUser(userBean);
            paymentInfoBean.setVerifiedTimestamp(userCustomerVoucherPaymentInfoBean.getVerifiedTimestamp());

            em.persist(paymentInfoBean);
            
            for (PaymentInfoBean userCustomerPaymentInfoBean : userCustomerPaymentMethodCreditCardList) {
                paymentInfoBean = new PaymentInfoBean();
                paymentInfoBean.setBrand(userCustomerPaymentInfoBean.getBrand());
                paymentInfoBean.setCardBin(userCustomerPaymentInfoBean.getCardBin());
                paymentInfoBean.setAttemptsLeft(userCustomerPaymentInfoBean.getAttemptsLeft());
                paymentInfoBean.setCheckAmount(userCustomerPaymentInfoBean.getCheckAmount());
                paymentInfoBean.setCheckBankTransactionID(userCustomerPaymentInfoBean.getCheckBankTransactionID());
                paymentInfoBean.setCheckCurrency(userCustomerPaymentInfoBean.getCheckCurrency());
                paymentInfoBean.setCheckShopLogin(userCustomerPaymentInfoBean.getCheckShopLogin());
                paymentInfoBean.setCheckShopTransactionID(userCustomerPaymentInfoBean.getCheckShopTransactionID());
                paymentInfoBean.setDefaultMethod(userCustomerPaymentInfoBean.getDefaultMethod());
                paymentInfoBean.setExpirationDate(userCustomerPaymentInfoBean.getExpirationDate());
                paymentInfoBean.setInsertTimestamp(userCustomerPaymentInfoBean.getInsertTimestamp());
                paymentInfoBean.setMessage(userCustomerPaymentInfoBean.getMessage());
                paymentInfoBean.setPan(userCustomerPaymentInfoBean.getPan());
                paymentInfoBean.setPin(userCustomerPaymentInfoBean.getPin());
                paymentInfoBean.setPinCheckAttemptsLeft(userCustomerPaymentInfoBean.getPinCheckAttemptsLeft());
                paymentInfoBean.setStatus(userCustomerPaymentInfoBean.getStatus());
                paymentInfoBean.setToken(userCustomerPaymentInfoBean.getToken());
                paymentInfoBean.setToken3ds(userCustomerPaymentInfoBean.getToken3ds());
                paymentInfoBean.setType(userCustomerPaymentInfoBean.getType());
                paymentInfoBean.setUser(userBean);
                paymentInfoBean.setVerifiedTimestamp(userCustomerPaymentInfoBean.getVerifiedTimestamp());

                em.persist(paymentInfoBean);
            }
            
            userBean.setUserStatusRegistrationCompleted(Boolean.TRUE);
            userBean.setUserStatusRegistrationTimestamp(new Date());

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Impostato il flag di registrazione completa per l'utente business");
            
            if (emailSender != null) {

                System.out.println("sending confirm registration email");

                String keyFrom = emailSender.getSender();
                
                EmailType emailType = EmailType.CONFIRM_REGISTRATION_BUSINESS;
                String to = userBean.getPersonalDataBean().getSecurityDataEmail();
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("WELCOME", userBean.getPersonalDataBean().getFirstName()));

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Sending email to "
                        + userBean.getPersonalDataBean().getSecurityDataEmail());

                String result = emailSender.sendEmail(emailType, keyFrom, to, parameters);

                //String result = "disabled";

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "SendEmail result: " + result);

            }
            
            userTransaction.commit();

            return ResponseHelper.USER_BUSINESS_CLONE_PAYMENT_METHOD_SUCCESS;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                System.err.println("IllegalStateException: " + e.getMessage());
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                System.err.println("SecurityException: " + e.getMessage());
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                System.err.println("SystemException: " + e.getMessage());
            }

            String message = "FAILED refreshing user data with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
