package com.techedge.mp.core.actions.user;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SendValidationResult;
import com.techedge.mp.core.business.interfaces.ValidationType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserDevice;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserDeviceBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.ResendValidation;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserResendValidationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserResendValidationAction() {}

    public String execute(String ticketId, String requestId, String id, String validitationType, String sendingType, Integer maxRetryAttemps, EmailSenderRemote emailSender,
            SmsServiceRemote smsService, UserCategoryService userCategoryService, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_REQU_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_REQU_INVALID_TICKET;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && validitationType.equals(ValidationType.MOBILE_PHONE.getValue())) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User status not verified");

                userTransaction.commit();

                return ResponseHelper.USER_LOAD_VOUCHER_UNAUTHORIZED;
            }

            if (!validitationType.equals(ValidationType.MOBILE_PHONE.getValue()) && !validitationType.equals(ValidationType.DEVICE.getValue())) {
                // MobilePhone non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid validation type(" + validitationType + ")");

                userTransaction.commit();

                return ResponseHelper.USER_RESEND_VALIDATATION_INVALID_TYPE;
            }

            if (validitationType.equals(ValidationType.MOBILE_PHONE.getValue())) {

                MobilePhoneBean mobilePhoneBean = QueryRepository.findMobilePhoneById(em, Long.parseLong(id));

                if (mobilePhoneBean == null) {

                    // MobilePhone non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid mobile phone (null)");

                    userTransaction.commit();

                    return ResponseHelper.USER_RESEND_VALIDATATION_FAILURE;
                }

                if (mobilePhoneBean != null && !mobilePhoneBean.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_PENDING)) {

                    // MobilePhone non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid mobile phone. Status not pending("
                            + mobilePhoneBean.getStatus() + ")");

                    userTransaction.commit();

                    return ResponseHelper.USER_RESEND_VALIDATATION_STATUS_NOT_PENDING;
                }

                boolean isAssociated = false;
                for (MobilePhoneBean mobilePhoneBeanTmp : userBean.getMobilePhoneList()) {
                    if (mobilePhoneBeanTmp.getPrefix().equals(mobilePhoneBean.getPrefix()) && mobilePhoneBeanTmp.getNumber().equals(mobilePhoneBean.getNumber())) {
                        isAssociated = true;
                        break;
                    }
                }

                if (!isAssociated) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid mobile phone. Not associated to user ticket");

                    userTransaction.commit();

                    return ResponseHelper.USER_RESEND_VALIDATATION_NOT_ASSOCIATED;
                }

                SendValidationResult result = ResendValidation.send(sendingType, maxRetryAttemps, userBean, mobilePhoneBean, loggerService, emailSender, smsService, 
                        userCategoryService, stringSubstitution);

                if (result.equals(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE)) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                            "Error in verification mobile phone. " + result.getMessageCode() + " (" + result.getErrorCode() + ")");

                    userTransaction.commit();

                    return ResponseHelper.MOBILE_PHONE_UPDATE_SENDING_FAILURE;
                }

                userTransaction.commit();
                return ResponseHelper.USER_RESEND_VALIDATATION_SUCCESS;
            }
            else {
                if (validitationType.equals(ValidationType.DEVICE.getValue())) {
                    
                    if (!userBean.getUserStatus().equals(User.USER_STATUS_VERIFIED)) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User status not valid:" + userBean.getUserStatus());

                        userTransaction.commit();

                        return ResponseHelper.USER_RESEND_DEVICE_VALIDATATION_FAILURE;
                    }
                    
                    //UserDeviceBean userDeviceBean = QueryRepository.findDeviceByDeviceId(em, id);
                    UserDeviceBean userDeviceBean = null;
                    
                    for(UserDeviceBean userDeviceBeanTemp : userBean.getUserDeviceBean()) {
                        if (userDeviceBeanTemp.getDeviceId().equals(id) && userDeviceBeanTemp.getStatus() == UserDevice.USER_DEVICE_STATUS_PENDING) {
                            userDeviceBean = userDeviceBeanTemp;
                            break;
                        }
                    }
                    
                    
                    if (userDeviceBean == null) {

                        // UserDevice non valido
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "userDeviceBean not exist");

                        userTransaction.commit();

                        return ResponseHelper.USER_RESEND_DEVICE_VALIDATATION_FAILURE;
                    }

                    if (userDeviceBean != null && !userDeviceBean.getStatus().equals(UserDevice.USER_DEVICE_STATUS_PENDING)) {

                        // UserDevice non valido
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                                "Invalid device. Status not pending(" + userDeviceBean.getStatus() + ")");

                        userTransaction.commit();

                        return ResponseHelper.USER_RESEND_DEVICE_VALIDATATION_STATUS_NOT_PENDING;
                    }
                    
                    if(!userBean.getUserDeviceBean().contains(userDeviceBean)){ 
                        // UserDevice non valido
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "userBean not have this userDevice");

                        userTransaction.commit();

                        return ResponseHelper.USER_RESEND_DEVICE_VALIDATATION_FAILURE;
                    }

                    boolean isAssociated = false;
                    for (UserDeviceBean item : userBean.getUserDeviceBean()) {
                        if (item.getDeviceId().equals(userDeviceBean.getDeviceId())) {
                            isAssociated = true;
                            break;
                        }
                    }

                    if (!isAssociated) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid device. Not associated to user ticket");

                        userTransaction.commit();

                        return ResponseHelper.USER_RESEND_DEVICE_VALIDATATION_NOT_ASSOCIATED;
                    }

                    SendValidationResult result = ResendValidation.sendForDevice(sendingType, maxRetryAttemps, userBean, userDeviceBean, loggerService, emailSender, 
                            smsService, userCategoryService, stringSubstitution);

                    if (result.equals(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE)) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                                "Error in verification mobile phone. " + result.getMessageCode() + " (" + result.getErrorCode() + ")");

                        userTransaction.commit();

                        return ResponseHelper.MOBILE_PHONE_UPDATE_SENDING_FAILURE;
                    }

                    userTransaction.commit();
                    return ResponseHelper.USER_RESEND_DEVICE_VALIDATATION_SUCCESS;
                }
                else {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "ValidationField not valid");

                    userTransaction.commit();

                    return ResponseHelper.USER_RESEND_DEVICE_VALIDATATION_FAILURE;
                }

            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED load voucher with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

}
