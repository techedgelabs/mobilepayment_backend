package com.techedge.mp.core.actions.user.v2;

import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.HistoryPasswordBean;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.utilities.AsyncDWHService;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.QueryRepositoryBusiness;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2UpdatePasswordAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2UpdatePasswordAction() {}

    public String execute(String ticketId, String requestId, String oldPassword, String newPassword, Integer passwordHistoryLenght, 
            Integer reconciliationMaxAttempts, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_PWD_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_PWD_INVALID_TICKET;
            }
            
            Boolean isMulticardFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.MULTICARD_FLOW.getCode());

            if (isMulticardFlow) {

                // L'utente guest non pu� modificare la password
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Password recovery not enabled for multicard users");

                userTransaction.commit();

                return ResponseHelper.USER_PWD_INVALID_USER_TYPE_MULTICARD;
            }
            
            if (userBean.getUserType().equals(User.USER_TYPE_GUEST)) {

                // L'utente guest non pu� modificare la password
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Password recovery not enabled for guest users");

                userTransaction.commit();

                return ResponseHelper.USER_PWD_INVALID_USER_TYPE_GUEST;
            }
            
            boolean isBusiness = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.BUSINESS.getCode());
            
            if (isBusiness && Objects.equals(userBean.getSource(), "ENISTATION+")) {
                // L'utente social non pu� modificare la password
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Password recovery not enabled for user business with source ENISTATION+" );
                
                userTransaction.commit();
                
                return ResponseHelper.USER_PWD_INVALID_USER_SOURCE;
            }
            
            // Verifica che l'utente non sia di tipo social
            if (userBean.getSocialProvider() != null) {
                
                // L'utente social non pu� modificare la password
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Password recovery not enabled for social users" );
                
                userTransaction.commit();
                
                return ResponseHelper.USER_PWD_INVALID_USER_TYPE_SOCIAL;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update user password in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_PWD_UNAUTHORIZED;
            }
            
            String socialProvider = null;
            if (!userBean.getUserSocialData().isEmpty()) {
                for(UserSocialDataBean userSocialDataBean : userBean.getUserSocialData()) {
                    socialProvider = userSocialDataBean.getProvider();
                }
            }
            
            if (socialProvider != null && !socialProvider.isEmpty()) {
                
                // Gli utenti social non possono richiamare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update user password for social user");

                userTransaction.commit();

                return ResponseHelper.USER_PWD_UNAUTHORIZED;
            }

            // Verifica la nuova password
            // TODO

            if (userBean.getPersonalDataBean().getSecurityDataEmail().equals(newPassword)) {

                // La nuova password coincide con l'email dell'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "New password = email");

                userTransaction.commit();

                return ResponseHelper.USER_PWD_USERNAME_PASSWORD_EQUALS;
            }

            Date checkDate = new Date();

            if (userBean.getPersonalDataBean().getExpirationDateRescuePassword() != null) {

                System.out.println("Expiration date rescue password: " + userBean.getPersonalDataBean().getExpirationDateRescuePassword());
                System.out.println("CheckDate: " + checkDate);

                if (userBean.getPersonalDataBean().getExpirationDateRescuePassword().before(checkDate)) {

                    System.out.println("before");
                }
                else {

                    System.out.println("after");
                }
            }

            if (!userBean.getPersonalDataBean().getSecurityDataPassword().equals(oldPassword)
                    && (userBean.getPersonalDataBean().getExpirationDateRescuePassword() == null
                            || userBean.getPersonalDataBean().getExpirationDateRescuePassword().before(checkDate) || !userBean.getPersonalDataBean().getRescuePassword().equals(
                            oldPassword))) {

                // La old password non coincide con quella dell'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Old password wrong");

                userTransaction.commit();

                return ResponseHelper.USER_PWD_OLD_ERROR;

            }

            // Codifica la password
            String encodedPassword = EncoderHelper.encode(newPassword);
            if (userBean.getPersonalDataBean().getSecurityDataPassword().equals(encodedPassword)) {

                // La nuova password coincide con quella vecchia
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "New password = old password");

                userTransaction.commit();

                return ResponseHelper.USER_PWD_OLD_NEW_EQUALS;
            }
            ////////////////////////////////////

            Set<HistoryPasswordBean> historyPasswordBeanData = userBean.getPersonalDataBean().getHistoryPasswordBeanData();
            Set<HistoryPasswordBean> historyPasswordBeanDataTemp = new HashSet<HistoryPasswordBean>(0);

            HistoryPasswordBean historyPasswordBeanTemp = new HistoryPasswordBean();

            HistoryPasswordBean historyPasswordBeanNew;

            if (historyPasswordBeanData.size() >= passwordHistoryLenght) {

                for (int i = 0; i < passwordHistoryLenght; i++) {

                    historyPasswordBeanTemp = null;

                    for (HistoryPasswordBean historyPasswordBean : historyPasswordBeanData) {

                        if (historyPasswordBeanTemp == null || historyPasswordBeanTemp.getId() < historyPasswordBean.getId()) {
                            historyPasswordBeanTemp = historyPasswordBean;
                        }
                    }

                    historyPasswordBeanDataTemp.add(historyPasswordBeanTemp);

                    historyPasswordBeanData.remove(historyPasswordBeanTemp);

                    if (historyPasswordBeanTemp.getPassword().equals(encodedPassword)) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "New password = history password");

                        userTransaction.commit();

                        return ResponseHelper.USER_PWD_HISTORY_NEW_EQUALS;
                    }

                    if (i == passwordHistoryLenght - 1) {

                        em.remove(historyPasswordBeanTemp);

                        historyPasswordBeanNew = new HistoryPasswordBean();
                        historyPasswordBeanNew.setPersonalDataBean(userBean.getPersonalDataBean());
                        historyPasswordBeanNew.setPassword(encodedPassword);

                        em.persist(historyPasswordBeanNew);

                    }
                }
            }
            else {

                for (HistoryPasswordBean historyPasswordBean : historyPasswordBeanData) {

                    if (historyPasswordBean.getPassword().equals(encodedPassword)) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "New password = history password");

                        userTransaction.commit();

                        return ResponseHelper.USER_PWD_HISTORY_NEW_EQUALS;
                    }
                }

                historyPasswordBeanNew = new HistoryPasswordBean();
                historyPasswordBeanNew.setPersonalDataBean(userBean.getPersonalDataBean());
                historyPasswordBeanNew.setPassword(encodedPassword);

                em.persist(historyPasswordBeanNew);

            }

            // Elimina la password temporanea
            PersonalDataBean personalDataBean = userBean.getPersonalDataBean();
            personalDataBean.setRescuePassword(null);
            personalDataBean.setExpirationDateRescuePassword(null);
            em.merge(personalDataBean);

            // Aggiorna la password
            userBean.getPersonalDataBean().setSecurityDataPassword(encodedPassword);

            if (userBean.getUserStatus() == User.USER_STATUS_TEMPORARY_PASSWORD) {
                userBean.setUserStatus(User.USER_STATUS_VERIFIED);
            }

            // Aggiorna i dati dell'utente
            em.merge(userBean);

            // Rinnova il ticket
            ticketBean.renew();
            em.merge(ticketBean);
            
            String email = userBean.getPersonalDataBean().getSecurityDataEmail();
            
            UserBean userBeanBusiness = QueryRepositoryBusiness.findNotCancelledUserBusinessByEmailAndSource(em, email, "ENISTATION+");
            
            if (userBeanBusiness != null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Trovato utente business collegato. "
                        + "Aggiornamento automatico password");
            
                userBeanBusiness.getPersonalDataBean().setSecurityDataPassword(encodedPassword);
                
                em.merge(userBeanBusiness);
            }

            boolean newAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            
            if (newAcquirerFlow && userBean.getVirtualizationCompleted()) {

                AsyncDWHService asyncDWHService = new AsyncDWHService(userTransaction, em, userBean, requestId, encodedPassword, reconciliationMaxAttempts);
                
                new Thread(asyncDWHService, "changeUserPasswordPlus (UserV2UpdatePasswordAction)").start();
            }

            userTransaction.commit();

            return ResponseHelper.USER_PWD_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED password update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
