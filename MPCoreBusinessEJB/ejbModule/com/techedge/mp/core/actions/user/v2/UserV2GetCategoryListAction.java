package com.techedge.mp.core.actions.user.v2;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.CategoryEarnDataDetail;
import com.techedge.mp.core.business.interfaces.CategoryEarnDataDetailResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.dwh.DWHCategoryEarnDetailBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2GetCategoryListAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2GetCategoryListAction() {}

    public CategoryEarnDataDetailResponse execute(String ticketId, String requestId) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        CategoryEarnDataDetailResponse categoryDataDetailResponse = new CategoryEarnDataDetailResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid()) { // || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                categoryDataDetailResponse.setStatusCode(ResponseHelper.USER_V2_GET_CATEGORY_LIST_INVALID_TICKET);
                return categoryDataDetailResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                categoryDataDetailResponse.setStatusCode(ResponseHelper.USER_V2_GET_CATEGORY_LIST_INVALID_TICKET);
                userTransaction.commit();
                return categoryDataDetailResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to retrieve category list, user  in status "
                        + userStatus);

                categoryDataDetailResponse.setStatusCode(ResponseHelper.USER_V2_GET_CATEGORY_LIST_UNAUTHORIZED);

                userTransaction.commit();

                return categoryDataDetailResponse;
            }
            
            List<DWHCategoryEarnDetailBean> dwhCategoryDetailList = QueryRepository.findAllDwhCategoryEarnDetailBean(em);

            if (dwhCategoryDetailList == null || dwhCategoryDetailList.isEmpty()) {

                // Partner list vuota
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Partner list is empty !");

                userTransaction.commit();

                categoryDataDetailResponse.setStatusCode(ResponseHelper.USER_V2_GET_CATEGORY_LIST_NOTFOUND);
                return categoryDataDetailResponse;
            }

            else {

                List<CategoryEarnDataDetail> categoryDataDetailList = new ArrayList<CategoryEarnDataDetail>(0);

                for (DWHCategoryEarnDetailBean dwhCategoryDetailBean : dwhCategoryDetailList) {
                    CategoryEarnDataDetail categoryDataDetailElement = new CategoryEarnDataDetail();

                    categoryDataDetailElement.setCategoryId(dwhCategoryDetailBean.getCategoryId());
                    categoryDataDetailElement.setCategory(dwhCategoryDetailBean.getCategory());
                    categoryDataDetailElement.setCategoryImageUrl(dwhCategoryDetailBean.getCategoryImageUrl());
                    
                    categoryDataDetailList.add(categoryDataDetailElement);
                    categoryDataDetailResponse.setCategoryDataDetail(categoryDataDetailList);
                }

                categoryDataDetailResponse.setCategoryDataDetail(categoryDataDetailList);
                categoryDataDetailResponse.setStatusCode(ResponseHelper.USER_V2_GET_CATEGORY_LIST_SUCCESS);
            }

            userTransaction.commit();
            return categoryDataDetailResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get landing  with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
