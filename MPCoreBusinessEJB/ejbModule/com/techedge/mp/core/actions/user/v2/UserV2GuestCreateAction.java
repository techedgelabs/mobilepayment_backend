package com.techedge.mp.core.actions.user.v2;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.CreateUserGuestResult;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CaptchaHelper;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2GuestCreateAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private UserCategoryService userCategoryService;

    public UserV2GuestCreateAction() {}

    public CreateUserGuestResult execute(String ticketId, String requestId, String deviceId, String captchaCode, Double initialCap, Boolean checkDeviceIdUnique, StringSubstitution stringSubstitution, String captchaSecretAndroid, String captchaSecretIOS, String urlCapthaVerification, String proxyHost, String proxyPort, String proxyNoHosts) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            
            CreateUserGuestResult createUserGuestResult = null;
            
            userTransaction.begin();

            // Verifica il ticket
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isServiceTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                
                createUserGuestResult = new CreateUserGuestResult();
                createUserGuestResult.setStatusCode(ResponseHelper.USER_V2_GUEST_CREATE_INVALID_TICKET);
                
                return createUserGuestResult;
            }
            
            String captchaSecret = null;
            if (requestId.startsWith("AND")) {
                captchaSecret = captchaSecretAndroid;
            }
            if (requestId.startsWith("IPH")) {
                captchaSecret = captchaSecretIOS;
            }
            
            if (captchaSecret == null) {

                // Invalid request
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid request");

                userTransaction.commit();
                
                createUserGuestResult = new CreateUserGuestResult();
                createUserGuestResult.setStatusCode(ResponseHelper.USER_REQU_INVALID_REQUEST);
                
                return createUserGuestResult;
            }

            // Verifica il codice captcha
            Proxy proxy = new Proxy(proxyHost, proxyPort, proxyNoHosts);
            proxy.setHttp();
            
            boolean captchaValid = CaptchaHelper.checkCaptcha(captchaCode, captchaSecret, urlCapthaVerification);
            if (!captchaValid) {
                
                proxy.unsetHttp();
                
                // Codice captcha non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid captcha");

                userTransaction.commit();

                createUserGuestResult = new CreateUserGuestResult();
                createUserGuestResult.setStatusCode(ResponseHelper.USER_V2_GUEST_CREATE_INVALID_CAPTCHA);
                
                return createUserGuestResult;
            }
            
            proxy.unsetHttp();
            
            /*
            // Verifica che non esista gi� un utente guest con quel device id
            UserBean oldUserBean = QueryRepository.findUserByEmail(em, username);

            if (oldUserBean != null) {

                // Esiste gi� un utente con quella email
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User exists");

                userTransaction.commit();

                return ResponseHelper.ADMIN_CREATE_REFUELING_USER_USER_EXISTS;
            }
            */

            String firstname = "Guest";
            String lastname  = "Guest";
            String sex       = "M";

            Calendar birthDate = new GregorianCalendar(1970, 01, 01);
            
            String randomNumber   = new IdGenerator().generateNumericId(12);
            String randomPassword = new IdGenerator().generateId(12);
            
            randomPassword = "Qwerty1234";
            System.out.println("pass: " + randomPassword);
            
            String username        = "guest_" + randomNumber + "@aaaaa.aa";
            String encodedPassword = EncoderHelper.encode(randomPassword);

            User user = new User();
            user.setPersonalData(new PersonalData());
            user.getPersonalData().setFirstName(firstname);
            user.getPersonalData().setLastName(lastname);
            user.getPersonalData().setBirthDate(birthDate.getTime());
            user.getPersonalData().setBirthProvince("RM");
            user.getPersonalData().setSex(sex);
            user.getPersonalData().setBirthMunicipality("Roma");
            user.getPersonalData().setFiscalCode("XXXXXXXXXXXXXXXX");
            user.getPersonalData().setLanguage("IT");
            user.getPersonalData().setSecurityDataEmail(username);
            user.getPersonalData().setSecurityDataPassword(encodedPassword);
            user.setUserStatus(User.USER_STATUS_VERIFIED);
            user.setUserStatusRegistrationCompleted(Boolean.FALSE);
            user.setUserType(User.USER_TYPE_GUEST);
            user.setCapAvailable(initialCap);
            user.setCapEffective(initialCap);
            Calendar createDate = new GregorianCalendar();
            user.setCreateDate(createDate.getTime());
            user.setUseVoucher(Boolean.FALSE);
            user.setVirtualizationAttemptsLeft(5);
            
            UserBean userBean = new UserBean(user);
            userBean.setDeviceID(deviceId);
            
            Date now = new Date();
            String number = "300000000000";
            String prefix = "+39";
            Integer status = MobilePhone.MOBILE_PHONE_STATUS_ACTIVE;
            
            MobilePhoneBean mobilePhoneBean = new MobilePhoneBean();
            mobilePhoneBean.setCreationTimestamp(now);
            mobilePhoneBean.setExpirationTimestamp(now);
            mobilePhoneBean.setLastUsedTimestamp(now);
            mobilePhoneBean.setNumber(number);
            mobilePhoneBean.setPrefix(prefix);
            mobilePhoneBean.setStatus(status);
            mobilePhoneBean.setUser(userBean);
            mobilePhoneBean.setVerificationCode("aaaaaaaaaaaaaaaa");
            
            userBean.getMobilePhoneList().add(mobilePhoneBean);

            // Salva l'utente su db
            em.persist(userBean);

            userTransaction.commit();

            createUserGuestResult = new CreateUserGuestResult();
            createUserGuestResult.setStatusCode(ResponseHelper.USER_V2_GUEST_CREATE_SUCCESS);
            createUserGuestResult.setUsername(username);
            createUserGuestResult.setEncodedPassword(encodedPassword);
            
            return createUserGuestResult;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user guest creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
