package com.techedge.mp.core.actions.user;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PaymentInfoResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.DpanDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetMcCardStatusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserRetrievePaymentDataAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserRetrievePaymentDataAction() {
    }
    
    
    public PaymentInfoResponse execute(
    		String ticketId,
			String requestId,
			Long cardId,
			String cardType,
			long maxPendingInterval,
			UserCategoryService userCategoryService,
			CardInfoServiceRemote cardInfoService) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		PaymentInfoResponse paymentInfoResponse = null;
    		
    		// Verifica il ticket
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");
				
    			userTransaction.commit();
    			
    			paymentInfoResponse = new PaymentInfoResponse();
    			paymentInfoResponse.setStatusCode(ResponseHelper.USER_RETRIEVE_PAYMENT_INVALID_TICKET);
    			return paymentInfoResponse;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if (userBean == null) {
    			
    			// L'utente non esiste
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");
				
    			userTransaction.commit();
    			
    			paymentInfoResponse = new PaymentInfoResponse();
    			paymentInfoResponse.setStatusCode(ResponseHelper.USER_RETRIEVE_PAYMENT_INVALID_TICKET);
    			return paymentInfoResponse;
    		}
    		
    		PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(cardId, cardType);
    		if (paymentInfoBean == null) {
    			
    			// Payment info non trovato
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment info not found");
				
    			userTransaction.commit();
    			
    			paymentInfoResponse = new PaymentInfoResponse();
    			paymentInfoResponse.setStatusCode(ResponseHelper.USER_RETRIEVE_PAYMENT_FAILED);
    			return paymentInfoResponse;
    		}
    		
    		/*
    		// Se lo stato dell'oggetto paymentInfo � pending da un tempo superiore
    		// al periodo di timeout, modifica lo stato in error e imposta un messaggio opportuno
    		PaymentInfo paymentInfo = null;
    		if ( paymentInfoBean != null ) {
    			
    			if ( paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING ) {
    				
    				ShopTransactionDataBean shopTransactionDataBean = QueryRepository.findShopTransactionBeanDataByPaymentInfo(em, paymentInfoBean);
    				
    				if ( shopTransactionDataBean != null ) {
    				
    					Date creationTimestamp = shopTransactionDataBean.getCreationTimestamp();
    					
    					long now = (new Date()).getTime();
    					long creationTime = creationTimestamp.getTime();
    					long pendingInterval = now - creationTime;
    					
    					if ( pendingInterval > maxPendingInterval ) {
    						
    						// E' stato superato il limite dell'intervallo di pending
    						this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Deposit card timeout for user " + userBean.getPersonalDataBean().getSecurityDataEmail());
    						
    						shopTransactionDataBean.setStatusToTimeout();
    						em.merge(shopTransactionDataBean);
    						
    						paymentInfoBean.setStatusToError();
    						em.merge(paymentInfoBean);
    					}
    				}
    			}
    		}
    		*/
    		
    		// Se il metodo di pagamento � di tipo MULTICARD bisogna verificare che il pan sia corretto
    		Date now = new Date();
    		
    		String userID = userBean.getPersonalDataBean().getFiscalCode();
    		
    		Boolean isMulticardFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.MULTICARD_FLOW.getCode());
            if (isMulticardFlow) {
        		if (userBean.getExternalUserId() != null && !userBean.getExternalUserId().isEmpty()) {
        		    userID = userBean.getExternalUserId();
        		}
            }
    		
    		// Allineamento dpan carte
            System.out.println("Allineamento dpan carte");
            
            if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                
                GetMcCardStatusResult getMcCardStatusResult = new GetMcCardStatusResult();
    
                try {
                    PartnerType partnerType = PartnerType.EM;
    
                    String operationId      = new IdGenerator().generateId(16).substring(0, 32);
                    String userId           = userID;
                    Long csRequestTimestamp = now.getTime();
                    
                    getMcCardStatusResult = cardInfoService.getMcCardStatus(operationId, userId, partnerType, csRequestTimestamp);
                }
                catch (Exception ex) {
                    getMcCardStatusResult.setStatus("ERROR");
                }
                
                if (getMcCardStatusResult.getStatus() != null) {
                    
                    for (DpanDetail dpanDetail : getMcCardStatusResult.getDpanDetailList()) {
                        
                        if (paymentInfoBean.getToken() != null && paymentInfoBean.getToken().equals(dpanDetail.getMcCardDpan())) {
                            
                            if (!paymentInfoBean.getPan().equals(dpanDetail.getMcCardPan())) {
                                paymentInfoBean.setPan(dpanDetail.getMcCardPan());
                                System.out.println("Allineamento pan multicard");
                            }
                        }
                    }
                }
            }
    		
    		PaymentInfo paymentInfo = paymentInfoBean.toPaymentInfo();
    		
    		// Creazione della risposta
    		paymentInfoResponse = new PaymentInfoResponse();
    		paymentInfoResponse.setStatusCode(ResponseHelper.USER_RETRIEVE_PAYMENT_SUCCESS);
    		paymentInfoResponse.setPaymentInfo(paymentInfo);
    		
    		userTransaction.commit();
    		
    		return paymentInfoResponse;

    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED authentication with message (" + ex2.getMessage() + ")";
    		this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
