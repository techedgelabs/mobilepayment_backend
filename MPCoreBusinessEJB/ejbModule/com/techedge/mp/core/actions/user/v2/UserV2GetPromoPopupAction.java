package com.techedge.mp.core.actions.user.v2;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GetPromoPopupDataResponse;
import com.techedge.mp.core.business.interfaces.LandingDataResponse;
import com.techedge.mp.core.business.interfaces.PromoPopupData;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.LandingDataBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.PromoPopupBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.UserTypeBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2GetPromoPopupAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2GetPromoPopupAction() {}

    public GetPromoPopupDataResponse execute(String ticketId, String requestId, String sectionID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        
        GetPromoPopupDataResponse response = new GetPromoPopupDataResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid()) { // || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.USER_V2_GETPROMOPOPUP_INVALID_TICKET);
                return response;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                response.setStatusCode(ResponseHelper.USER_V2_GETPROMOPOPUP_INVALID_TICKET);
                return response;
            }
            
            Date now = new Date();
            
            PromoPopupBean promoPopupBean = QueryRepository.findPromoPopupBySection(em, sectionID, now);

            if (promoPopupBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Promo popup not found for sectionId " + sectionID);

                userTransaction.commit();
                response.setStatusCode(ResponseHelper.USER_V2_GETPROMOPOPUP_FAILURE);
                return response;
            }
            
            if (promoPopupBean.getAcceptRequired()) {
                
                System.out.println("Verifica accettazione condizioni");
                
                // Verifica che l'utente abbia accettato di ricevere comunicazioni da parte di Eni
                List<TermsOfServiceBean> termsOfServiceBeanList = QueryRepository.findTermOfServiceByPersonalDataId(em, userBean.getPersonalDataBean());
                for(TermsOfServiceBean termsOfServiceBean : termsOfServiceBeanList) {
                    if (termsOfServiceBean.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                        if (termsOfServiceBean.getAccepted()) {
                            break;
                        }
                        else {
                            // Ticket non valido
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "L'utente non ha accettato di ricevere comunicazioni da parte di Eni -> Promo popup non visualizzata");
    
                            userTransaction.commit();
                            response.setStatusCode(ResponseHelper.USER_V2_GETPROMOPOPUP_FAILURE);
                            return response;
                        }
                    }
                }
            }
            else {
                
                System.out.println("Accettazione condizioni non richiesta");
            }
            
            PromoPopupData promoPopupData = new PromoPopupData();
            
            promoPopupData.setBannerUrl(promoPopupBean.getBannerUrl());
            promoPopupData.setBody(promoPopupBean.getBody());
            promoPopupData.setPromoId(promoPopupBean.getPromoId());
            promoPopupData.setShowAlways(promoPopupBean.getShowAlways());
            promoPopupData.setType(promoPopupBean.getType());
            promoPopupData.setUrl(promoPopupBean.getUrl());
            
            response.setPromoPopupData(promoPopupData);
            response.setStatusCode(ResponseHelper.USER_V2_GETPROMOPOPUP_SUCCESS);

            userTransaction.commit();
            
            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get promo popup with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
