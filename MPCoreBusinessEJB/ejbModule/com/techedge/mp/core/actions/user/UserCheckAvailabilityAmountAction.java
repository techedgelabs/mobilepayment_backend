package com.techedge.mp.core.actions.user;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.CheckAvailabilityAmountData;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GetActiveVouchersData;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserCheckAvailabilityAmountAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserCheckAvailabilityAmountAction() {}

    public CheckAvailabilityAmountData execute(String ticketId, String requestId, Double amountRefuel, Double ratioThreshold, Double voucherTransactionMinAmount,
            UserService userService, String loyaltySessionID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        CheckAvailabilityAmountData result = new CheckAvailabilityAmountData();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                result.setStatusCode(ResponseHelper.USER_CHECK_AVAILABILITY_AMOUNT_INVALID_TICKET);
                return result;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                result.setStatusCode(ResponseHelper.USER_CHECK_AVAILABILITY_AMOUNT_INVALID_TICKET);
                return result;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update user pin in status " + userStatus);

                userTransaction.commit();

                result.setStatusCode(ResponseHelper.USER_CHECK_AVAILABILITY_AMOUNT_UNAUTHORIZED);
                return result;
            }

            // Verifica che l'utente abbia gi� inserito il pin 
            PaymentInfoBean voucherPaymentInfoBean = userBean.getVoucherPaymentMethod();

            if (voucherPaymentInfoBean == null || voucherPaymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not present");

                userTransaction.commit();

                result.setStatusCode(ResponseHelper.USER_CHECK_AVAILABILITY_AMOUNT_UNAUTHORIZED);
                return result;
            }

            // Calcola l'importo voucher disponibile per l'utente
            GetActiveVouchersData getActiveVouchersData = userService.getActiveVouchers(ticketId, requestId, true, loyaltySessionID);

            Double amountVoucher = 0.0;
            if (getActiveVouchersData.getTotal() != null) {
                amountVoucher = getActiveVouchersData.getTotal();
            }

            if (amountVoucher >= amountRefuel) {

                System.out.println("L'utente ha credito voucher sufficiente");

                result.setMaxAmount(amountVoucher);
                result.setThresholdAmount(0.0);
                result.setAllowResize(true);
            }
            else {

                System.out.println("L'utente non ha credito voucher sufficiente");

                Boolean paymentInfoBeanFound = userBean.paymentCardExists();

                // Se l'utente non ha credito sufficiente e non ha un metodo di pagamento associato bisogna restituire un errore
                if (amountVoucher < voucherTransactionMinAmount && !paymentInfoBeanFound) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Insufficient credit voucher and no payment card");

                    userTransaction.commit();

                    result.setStatusCode(ResponseHelper.USER_CHECK_AVAILABILITY_AMOUNT_UNAUTHORIZED);
                    return result;
                }

                Double diff = amountRefuel - amountVoucher;

                System.out.println("amount - total: " + diff.toString());

                Double threshold = Math.ceil(amountRefuel - amountVoucher);

                System.out.println("Threshold: " + threshold.toString());

                if (threshold < voucherTransactionMinAmount) {
                    threshold = voucherTransactionMinAmount;

                    System.out.println("Threshold upd: " + threshold.toString());
                }

                Double ratio = threshold / amountRefuel;

                System.out.println("ratio: " + ratio.toString() + ", ratio_threshold: " + ratioThreshold.toString());
                /*
                if ( ratio <= ratioThreshold ) {
                    
                    System.out.println("Allow resize true");
                    result.setAllowResize(true);
                }
                else {
                    
                    System.out.println("Allow resize false");
                    result.setAllowResize(false);
                }
                */

                // Algoritmo allowResize
                // Il campo � true se l'utente non ha una carta associata, ha credito voucher non inferirore alla soglia e vuole rifornire pi� di quando ha
                if (!paymentInfoBeanFound && amountVoucher >= ratioThreshold && amountRefuel > amountVoucher) {
                    System.out.println("Allow resize true");
                    result.setAllowResize(true);

                }
                else {
                    System.out.println("Allow resize false");
                    result.setAllowResize(false);

                }

                result.setMaxAmount(amountVoucher);
                result.setThresholdAmount(threshold);

            }

            result.setStatusCode(ResponseHelper.USER_CHECK_AVAILABILITY_AMOUNT_SUCCESS);

            // Rinnova il ticket
            //logger.log(Level.INFO, "Rinnova il ticket");
            ticketBean.renew();
            em.merge(ticketBean);
            userTransaction.commit();

            return result;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED check availability amount with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
