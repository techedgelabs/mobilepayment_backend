package com.techedge.mp.core.actions.user;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.OperationType;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserCheckAuthorizationAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserCheckAuthorizationAction() {
    }
    
    
    public String execute(
    		String ticketId,
    		Integer operationType) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		if ( operationType == OperationType.RETRIEVE_STATION ) {
    		
	    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId) ;
	    		
	    		if ( ticketBean == null || !ticketBean.isValid() ) {
					
					// Ticket non valido
					this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", ticketId, null, "Invalid ticket");
					
	    			userTransaction.commit();
	    			
	    			return ResponseHelper.CHECK_AUTHORIZATION_INVALID_TICKET;
				}
				
				
				if ( !ticketBean.isCustomerTicket()) {
					
					// Se la richiesta � effettuata con un ticket di sistema rispondi positivamente
					
					userTransaction.commit();
					
					return ResponseHelper.CHECK_AUTHORIZATION_SUCCESS;
				}
				else {
					// Se la richiesta � effettuata con un ticket utente bisogna verificarne lo stato
					UserBean userBean = ticketBean.getUser();
					if ( userBean == null ) {
						
						// Ticket non valido
						this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", ticketId, null, "Invalid ticket");
						
		    			userTransaction.commit();
		    			
		    			return ResponseHelper.CHECK_AUTHORIZATION_INVALID_TICKET;
					}
					
					// Verifica lo stato dell'utente
					Integer userStatus = userBean.getUserStatus();
					if ( userStatus != User.USER_STATUS_VERIFIED ) {
						
						// Un utente che si trova in questo stato non pu� invocare questo servizio
						this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", ticketId, null, "Unable to execute in status " + userStatus);
						
		    			userTransaction.commit();
		    			
		    			return ResponseHelper.CHECK_AUTHORIZATION_FAILED;
					}
				}
			}
    		
    		
    		if ( operationType == OperationType.CREATE_TRANSACTION ) {
        		
	    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId) ;
	    		
	    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
					
					// Ticket non valido
					this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", ticketId, null, "Invalid ticket");
					
	    			userTransaction.commit();
	    			
	    			return ResponseHelper.CHECK_AUTHORIZATION_INVALID_TICKET;
				}
				
				
				UserBean userBean = ticketBean.getUser();
				if ( userBean == null ) {
					
					// Ticket non valido
					this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", ticketId, null, "Invalid ticket");
					
	    			userTransaction.commit();
	    			
	    			return ResponseHelper.CHECK_AUTHORIZATION_INVALID_TICKET;
				}
				
				// Verifica lo stato dell'utente
				Integer userStatus = userBean.getUserStatus();
				if ( userStatus != User.USER_STATUS_VERIFIED ) {
					
					// Un utente che si trova in questo stato non pu� invocare questo servizio
					this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", ticketId, null, "Unable to execute in status " + userStatus);
					
	    			userTransaction.commit();
	    			
	    			return ResponseHelper.CHECK_AUTHORIZATION_FAILED;
				}
			}
			
			// TODO Verifica che l'utente non abbia gi� una transazione in stato pending
			
    		userTransaction.commit();
    		
			return ResponseHelper.CHECK_AUTHORIZATION_SUCCESS;
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED check authorization with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", ticketId, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
