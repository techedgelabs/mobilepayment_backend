package com.techedge.mp.core.actions.user;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.CRMSfNotifyEvent;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.crm.EVENT_TYPE;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserRemovePaymentMethodAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    @EJB
    private CRMService                   crmService;
    
    @EJB
    private ParametersService parametersService;
    
    private final String 				PARAM_CRM_SF_ACTIVE="CRM_SF_ACTIVE";
  
    public UserRemovePaymentMethodAction() {
    }
    
    
    public String execute(String ticketId, String requestId, Long paymentMethodId, String paymentMethodType, String shopLogin, UserCategoryService userCategoryService) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		// Verifica il ticket
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_REMOVE_PAYMENT_METHOD_INVALID_TICKET;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if (userBean == null) {
    			
    			// L'utente non esiste
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_REMOVE_PAYMENT_METHOD_INVALID_TICKET;
    		}
    		
            Integer userType = userBean.getUserType();
            Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());
            
            if (useBusiness) {
    		
        		int activeCreditCard = 0;
        		
        		if (userBean.getPaymentData() != null) {
            		for (PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                        if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                                && paymentInfoBean.getStatus().equals(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {
                            
                            activeCreditCard++;
                        }
                    }
        		}
        		
        		if (activeCreditCard <= 1) {
                    // Payment info non trovato
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to cancel default payment credit_card");
                    
                    userTransaction.commit();
                    
                    return ResponseHelper.USER_REMOVE_PAYMENT_METHOD_FAILED;
        		}
            }
    		/*
    		 * Codice rivisitato
    		 *
    		PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
    		if (paymentInfoBean == null || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_CANCELED) {
    			
    			// Payment info non trovato
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment info not found");
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_REMOVE_PAYMENT_METHOD_NOT_FOUND;
    		}
    		
    		Boolean isDefault = paymentInfoBean.getDefaultMethod();
    		
    		PaymentInfoBean paymentInfoBeanToRemove = userBean.removePaymentInfo(paymentInfoBean);
    		if (paymentInfoBeanToRemove == null) {
    			
    			// Payment info non trovato
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment info not found");
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_REMOVE_PAYMENT_METHOD_NOT_FOUND;
    		}
    		
    		
    		if ( isDefault ) {
    			
    			// Se � stata eliminata la carta di default bisogna definirne un'altra
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment info not found");
    			
    			userBean.setNewDefaultPaymentMethod();
    		}
    		*/
    		
    		// Non � possibile rimuovere metodi di pagamento diversi da credit_card
    		if ( !paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD ) ) {
    		    
    		    // Payment info non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to cancel payment info type different from credit_card");
                
                userTransaction.commit();
                
                return ResponseHelper.USER_REMOVE_PAYMENT_METHOD_FAILED;
    		}
    		
    		PaymentInfoBean paymentInfoBeanToRemove = userBean.removePaymentInfo(paymentMethodId, paymentMethodType);
    		if (paymentInfoBeanToRemove == null) {
    			
    			// Payment info non trovato
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment info not found");
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_REMOVE_PAYMENT_METHOD_NOT_FOUND;
    		}
    		
    		// Invia la notifica al CRM solo se l'utente ha gi� completato l'iscrizione e se la cancellazione � relativa a un metodo di pagamento precedentemente attivo
    		if (userBean.getUserStatusRegistrationCompleted() != null && userBean.getUserStatusRegistrationCompleted() && !useBusiness && paymentInfoBeanToRemove.getBrand() != null && !paymentInfoBeanToRemove.getBrand().equals("")) {
    			
    	    	String crmSfActive="0";
    	    	try {
    	    		crmSfActive = parametersService.getParamValue(PARAM_CRM_SF_ACTIVE);
    			} catch (ParameterNotFoundException e) {
    				e.printStackTrace();
    			}
    		
                Date timestamp = new Date();
                final String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                boolean flagNotification = false;
                boolean flagCreditCard = false;
                String cardBrand = paymentInfoBeanToRemove.getBrand();
                ClusterType cluster = ClusterType.ENIPAY;
                final Long sourceID = paymentInfoBeanToRemove.getId();
                final CRMEventSourceType sourceType = CRMEventSourceType.PAYMENT_INFO;
                boolean privacyFlag1 = false;
                boolean privacyFlag2 = false;
                
                List<TermsOfServiceBean> listTermOfService = QueryRepository.findTermOfServiceByPersonalDataId(em, userBean.getPersonalDataBean());
                if (listTermOfService == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "flagPrivacy null");
                }
                else {
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("NOTIFICATION_NEW_1")) {
                            flagNotification = item.getAccepted();
                            break;
                        }
                    }
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                            privacyFlag1 = item.getAccepted();
                            break;
                        }
                    }
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                            privacyFlag2 = item.getAccepted();
                            break;
                        }
                    }
                }
                
                if (userBean.getVirtualizationCompleted()) {
                    cluster = ClusterType.YOUENI;
                }
                
                //Nuova implementazione sf
                //recupero parametro CRM_SF_ACTIVE per la gestione della nuova chiamata al CRM SF
                //0 disattivato invio sf
                //1 attivo solo invio sf
                //2 attivi entrambi i crm ibm,sf
               
                if(crmSfActive.equalsIgnoreCase("0")||crmSfActive.equalsIgnoreCase("2")){
    
	                final UserProfile userProfile = UserProfile.getUserProfileForEventCreditCard(fiscalCode, timestamp, flagNotification, flagCreditCard, cardBrand, cluster);
	                
	                new Thread(new Runnable() {
	                    
	                    @Override
	                    public void run() {
	                        String crmResponse = crmService.sendNotifyEventOffer(fiscalCode, InteractionPointType.CREDIT_CARD, userProfile, sourceID, sourceType);
	    
	                        if (!crmResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
	                            System.err.println("Error in sending crm notification (" + crmResponse + ")");
	                        }           
	                    }
	                }, "executeBatchGetOffers (UserRemovePaymentMethodAction)").start();
                }
                
                if(crmSfActive.equalsIgnoreCase("1")||crmSfActive.equalsIgnoreCase("2")){
                	
                	System.out.println("##################################################");
                	System.out.println("fiscalCode:" + fiscalCode);
                	System.out.println("timestamp:" + timestamp);
                	System.out.println("userBean.getPersonalDataBean().getLastName():" +userBean.getPersonalDataBean().getLastName() );
                	System.out.println("getSecurityDataEmail():" +userBean.getPersonalDataBean().getSecurityDataEmail());
                	System.out.println("getBirthDate():" + userBean.getPersonalDataBean().getBirthDate());
                	System.out.println("flagNotification:" + flagNotification);
                	System.out.println("flagCreditCard:" + flagCreditCard);
                	System.out.println("cardBrand:" + cardBrand);
                	System.out.println("cluster.getValue():" +cluster.getValue() );
                	System.out.println("userBean.getActiveMobilePhone():" +userBean.getActiveMobilePhone() );
                	
                	String panCode="";
                	if(userBean.getVirtualLoyaltyCard()!=null){
                		System.out.println("getPanCode():" +userBean.getVirtualLoyaltyCard().getPanCode() );
                		panCode=userBean.getVirtualLoyaltyCard().getPanCode();
                	}else
                		System.out.println("getPanCode():null");
                		
                	System.out.println("EVENT_TYPE.ASSOCIAZIONE_CARTA_PAGAMENTO.getValue():" + EVENT_TYPE.ASSOCIAZIONE_CARTA_PAGAMENTO.getValue());
                	System.out.println("##################################################");
                	
                	String reqId = new IdGenerator().generateId(16).substring(0, 32);
                	
                	/*:TODO da verificare requestId  capire come passare i campi null... come fanno a capire che � un evento d cancellazione? */
                	final CRMSfNotifyEvent crmSfNotifyEvent= new CRMSfNotifyEvent(reqId, fiscalCode, timestamp, "", "", false, 
                			1, 1D, "", "", userBean.getPersonalDataBean().getLastName(), "", 
                			null, flagNotification, flagCreditCard, cardBrand==null?"":cardBrand, 
                			cluster.getValue(), 0D, privacyFlag1, privacyFlag2, userBean.getActiveMobilePhone(), panCode, 
                			EVENT_TYPE.ASSOCIAZIONE_CARTA_PAGAMENTO.getValue(), "", "", "");
	    
	                new Thread(new Runnable() {
	                    
	                    @Override
	                    public void run() {
	                    	NotifyEventResponse crmResponse = crmService.sendNotifySfEventOffer(crmSfNotifyEvent.getRequestId(), crmSfNotifyEvent.getFiscalCode(), crmSfNotifyEvent.getDate(), 
	                        		crmSfNotifyEvent.getStationId(), crmSfNotifyEvent.getProductId(), crmSfNotifyEvent.getPaymentFlag(), crmSfNotifyEvent.getCredits(), 
	                        		crmSfNotifyEvent.getQuantity(), crmSfNotifyEvent.getRefuelMode(), crmSfNotifyEvent.getFirstName(), crmSfNotifyEvent.getLastName(), 
	                        		crmSfNotifyEvent.getEmail(), crmSfNotifyEvent.getBirthDate(), crmSfNotifyEvent.getNotificationFlag(), crmSfNotifyEvent.getPaymentCardFlag(), 
	                        		crmSfNotifyEvent.getBrand(), crmSfNotifyEvent.getCluster(),crmSfNotifyEvent.getAmount(), crmSfNotifyEvent.getPrivacyFlag1(), 
	                        		crmSfNotifyEvent.getPrivacyFlag2(), crmSfNotifyEvent.getMobilePhone(), crmSfNotifyEvent.getLoyaltyCard(), crmSfNotifyEvent.getEventType(), 
	                        		crmSfNotifyEvent.getParameter1(), crmSfNotifyEvent.getParameter2(), crmSfNotifyEvent.getPaymentMode());
	    
	                    	System.out.println("crmResponse succes:" + crmResponse.getSuccess());
		                      System.out.println("crmResponse errorCode:" + crmResponse.getErrorCode());
		                      System.out.println("crmResponse message:" + crmResponse.getMessage());
		                      System.out.println("crmResponse requestId:" + crmResponse.getRequestId());
	                    }
	                }, "executeBatchGetOffersSF (UserRemovePaymentMethodAction)").start();
                }
    		}
    		
    		em.merge(userBean);
    		
    		userTransaction.commit();
    		
    		return ResponseHelper.USER_REMOVE_PAYMENT_METHOD_SUCCESS;
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED remove payment method with message (" + ex2.getMessage() + ")";
    		this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
