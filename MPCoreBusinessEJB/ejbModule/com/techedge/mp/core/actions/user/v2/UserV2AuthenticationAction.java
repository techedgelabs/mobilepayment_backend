package com.techedge.mp.core.actions.user.v2;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.AuthenticationResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.SendValidationResult;
import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;
import com.techedge.mp.core.business.interfaces.TermsOfService;
import com.techedge.mp.core.business.interfaces.user.PersonalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.interfaces.user.UserDevice;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.model.LastLoginDataBean;
import com.techedge.mp.core.business.model.LastLoginErrorDataBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.UserDeviceBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltySessionBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.EncryptionRSA;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.ResendValidation;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.interfaces.DWHAdapterResult;
import com.techedge.mp.dwh.adapter.interfaces.UserDetail;
import com.techedge.mp.dwh.adapter.utilities.StatusCode;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.Platform;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2AuthenticationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2AuthenticationAction() {}

    public AuthenticationResponse execute(String email, String passwordHash, String passwordEncrypted, String privateKeyPEM, String requestId, String deviceId, String deviceName,
            String deviceToken, long maxPendingInterval, Integer ticketExpiryTime, Integer loyaltySessionExpiryTime, Integer loginAttemptsLimit, Integer loginLockExpiryTime,
            List<String> userBlockExceptionList, UserCategoryService userCategoryService, UnavailabilityPeriodService unavailabilityPeriodService,
            DWHAdapterServiceRemote dwhAdapterService, PushNotificationServiceRemote pushNotificationService, Integer maxRetryAttemps, String sendingType,
            EmailSenderRemote emailSender, SmsServiceRemote smsService, Long deployDateLong, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            AuthenticationResponse authenticationResponse = null;

            UserBean userBean = null;

            // Controlla se esiste un utente con username e password inseriti
            userBean = QueryRepository.findNotCancelledUserCustomerByEmail(em, email);

            if (userBean == null) {

                // TODO - Se passwordEncrypted � vuoto restituire subito l'errore per utente non trovato

                // Se l'utente non esiste sul backend effettua una ricerca sul server DWH
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User " + email + " not found");

                authenticationResponse = new AuthenticationResponse();
                String decryptedPassword = null;

                try {
                    // Effettua il decrypt della password passata in input dal servizio
                    EncryptionRSA encryption = new EncryptionRSA();
                    encryption.loadPrivateKey(privateKeyPEM);
                    decryptedPassword = encryption.decrypt(passwordEncrypted);
                }
                catch (Exception ex) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Decryption error: " + ex.getMessage());
                    authenticationResponse = new AuthenticationResponse();
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);

                    userTransaction.commit();

                    return authenticationResponse;
                }

                DWHAdapterResult result = dwhAdapterService.checkLegacyPasswordPlus(email, decryptedPassword, requestId);

                if (!result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS)) {

                    if (result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_REG_LITE)) {
                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_DWH_REG_LITE);
                    }
                    else {
                        if (result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR)) {
                            authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);
                        }
                        else {
                            // Se l'utente non esiste nemmeno sul server DWH allora bisogna restituire un errore per login fallito
                            authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);
                        }
                    }
                }
                else {
                    
                    if (result.getUserDetail() == null || (result.getUserDetail() != null && result.getUserDetail().getFiscalCode() == null)) {
                        
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "user detail error: user detail null or fiscalcode null");
                        
                        userTransaction.commit();
                        
                        authenticationResponse = new AuthenticationResponse();
                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_INVALID_USER);
                        
                        return authenticationResponse;
                    }
                    
                    
                    // Controllo per blocco migrazione utente che si trova in blacklist sul backend Enistation
                    if (result.getUserDetail().getFlagBlackList()) {
                        
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "user in blacklist in Enistation backend");
                        
                        userTransaction.commit();
                        
                        authenticationResponse = new AuthenticationResponse();
                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_DWH_BLACKLIST);
                        
                        return authenticationResponse;
                    }
                    
                    
                    UserBean userFound = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, result.getUserDetail().getFiscalCode());
                    
                    if (userFound != null
                            && (userFound.getUserStatus() != User.USER_STATUS_BLOCKED && userFound.getUserStatus() != User.USER_STATUS_NEW && userFound.getUserStatus() != User.USER_STATUS_CANCELLED)) {
                        
                        // Esiste gi� un utente con il codice fiscale specificato
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Fiscalcode already used. Logon with Eni Pay credentials");

                        userTransaction.commit();

                        authenticationResponse = new AuthenticationResponse();
                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_INVALID_USER);
                        return authenticationResponse;
                    }
                    
                    // SE l'utente esiste sul server DWN restituisci le informazioni anagrafiche e imposta lo stato per attivare il processo di migrazione
                    UserDetail userDetail = result.getUserDetail();
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
                    User user = new User();

                    PersonalData personalData = new PersonalData();
                    personalData.setFirstName((userDetail.getName() == null) ? "" : userDetail.getName().trim());
                    personalData.setLastName((userDetail.getSurname() == null) ? "" : userDetail.getSurname().trim());
                    personalData.setFiscalCode((userDetail.getFiscalCode() == null) ? "" : userDetail.getFiscalCode().trim());
                    personalData.setSex((userDetail.getGender() == null) ? "" : userDetail.getGender().trim());
                    personalData.setBirthDate((userDetail.getDateOfBirth() == null) ? new Date() : userDetail.getDateOfBirth());
                    personalData.setBirthMunicipality((userDetail.getCityOfBirth() == null) ? "" : userDetail.getCityOfBirth().trim());
                    personalData.setBirthProvince((userDetail.getCountryOfBirth() == null) ? "" : userDetail.getCountryOfBirth().trim());
                    personalData.setSecurityDataEmail((email == null) ? "" : email);
                    user.setPersonalData(personalData);

                    MobilePhone mobilePhone = new MobilePhone();
                    mobilePhone.setNumber(userDetail.getMobilePhoneNumber().trim());
                    mobilePhone.setStatus(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE);
                    user.getMobilePhoneList().add(mobilePhone);

                    user.setUserStatus(User.USER_STATUS_MIGRATING);
                    user.setUserStatusRegistrationCompleted(true);
                    user.setEniStationUserType(Boolean.TRUE);

                    authenticationResponse.setUser(user);
                }

                userTransaction.commit();

                return authenticationResponse;
            }
            
            if (userBlockExceptionList.contains(userBean.getPersonalDataBean().getSecurityDataEmail())) {

                //System.out.println(userBean.getPersonalDataBean().getSecurityDataEmail() + " presente in lista utenti con eccezioni blocco");
            }
            else {

                // Controllo per verificare se un utente � abilitato all'autenticazione 
                ServiceAvailabilityData serviceAvailabilityData = unavailabilityPeriodService.retrieveServiceAvailability("LOGIN", new Date());
                if (serviceAvailabilityData != null) {

                    // Servizio non disponibile
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "AuthenticationService unavailable");

                    userTransaction.commit();

                    authenticationResponse = new AuthenticationResponse();
                    authenticationResponse.setStatusCode(serviceAvailabilityData.getStatusCode());
                    return authenticationResponse;
                }
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus == User.USER_STATUS_BLOCKED || userStatus == User.USER_STATUS_NEW || userStatus == User.USER_STATUS_CANCELLED) {

                // Un utente che si trova in questo stato non pu� effettuare il login
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User " + email + " in status "
                        + userStatus);
                authenticationResponse = new AuthenticationResponse();
                if (userStatus == User.USER_STATUS_NEW) {
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_NOT_VERIFIED);
                }
                else {
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_UNAUTHORIZED);
                }

                userTransaction.commit();

                return authenticationResponse;
            }

            Integer userType = userBean.getUserType();
            Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());

            if (userBean.getUserType() == User.USER_TYPE_REFUELING || userBean.getUserType() == User.USER_TYPE_REFUELING_NEW_ACQUIRER || useBusiness) {

                // L'utente � di tipo refueling e non pu� effutare l'accesso
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unauthorized access: User " + email);
                authenticationResponse = new AuthenticationResponse();
                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_INVALID_TICKET);

                userTransaction.commit();

                return authenticationResponse;
            }

            if (userBean.getUserType() == User.USER_TYPE_SERVICE) {

                // Per ottimizzare il flusso di autenticazione i controlli sul
                // lastLoginData e LastoLoginErrorData non sono effettuati sull'utente
                // di sistema
                if (userBean.getPersonalDataBean().getSecurityDataPassword().equals(passwordHash)) {

                    // 1) crea un nuovo ticket
                    User user = userBean.toUser();

                    Integer ticketType = TicketBean.TICKET_TYPE_SERVICE;

                    Date now = new Date();
                    Date lastUsed = now;
                    Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

                    String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

                    TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);

                    em.persist(ticketBean);

                    // 2) restituisci il risultato
                    authenticationResponse = new AuthenticationResponse();

                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
                    authenticationResponse.setTicketId(ticketBean.getTicketId());
                    authenticationResponse.setUser(user);

                    userTransaction.commit();

                    return authenticationResponse;
                }
                else {

                    // La password inserita non � corretta
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong password for user " + email);
                    authenticationResponse = new AuthenticationResponse();
                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);

                    userTransaction.commit();

                    return authenticationResponse;
                }
            }
            else {

                // Genera il token della sessione loyalty

                Date creationTimestamp = new Date();
                Date lastUsedTimestamp = creationTimestamp;
                Date expiryTimestamp = DateHelper.addMinutesToDate(loyaltySessionExpiryTime, creationTimestamp);
                String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                
                if (userBean.getSource() != null && userBean.getSource().equals(User.USER_MULTICARD)) {
                    
                    System.out.println("Generazione sessione loyalty per utente Multicard");
                    
                    // Per l'utente multicard il campo fiscalcode deve essere valorizzato con l'email contenuta in UserExternalData
                    if (userBean.getUserExternalData() != null && !userBean.getUserExternalData().isEmpty()) {
                        
                        fiscalCode = userBean.getUserExternalData().iterator().next().getUuid();
                        
                        userBean.setExternalUserId(fiscalCode);
                    }
                    else {
                        
                        System.out.println("UserExternalData non valido o non presente");
                    }
                }

                String sessionId = new IdGenerator().generateId(16).substring(0, 32);

                LoyaltySessionBean loyaltySessionBean = new LoyaltySessionBean(sessionId, userBean, creationTimestamp, lastUsedTimestamp, expiryTimestamp, fiscalCode);

                em.persist(loyaltySessionBean);

                //System.out.println("Creata sessione loyalty per l'utente " + userBean.getPersonalDataBean().getSecurityDataEmail() + " (" + userBean.getId() + ")");

                Date checkDate = new Date();

                LastLoginErrorDataBean lastLoginErrorDataBean = userBean.getLastLoginErrorDataBean();

                String deviceFamily = requestId.substring(0, 3).toUpperCase();

                if (userBean.getPersonalDataBean().getSecurityDataPassword().equals(passwordHash)) {

                    // Controllare se l'utente ha superato il numero di tentativi ed � ancora nell'elapsed time;
                    if (lastLoginErrorDataBean != null && lastLoginErrorDataBean.getAttempt() > loginAttemptsLimit && lastLoginErrorDataBean.getTime().after(checkDate)) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Login Locked access: User " + email + " in status "
                                + userStatus);
                        authenticationResponse = new AuthenticationResponse();
                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_LOCKED);

                        userTransaction.commit();

                        return authenticationResponse;
                    }

                    // 1) crea un nuovo ticket
                    User user = userBean.toUser();

                    Integer ticketType = 0;
                    if (user.getUserType() == User.USER_TYPE_CUSTOMER || user.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER) {
                        ticketType = TicketBean.TICKET_TYPE_CUSTOMER;
                    }
                    else {
                        ticketType = TicketBean.TICKET_TYPE_TESTER;
                    }

                    Date now = new Date();
                    Date lastUsed = now;
                    Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

                    String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

                    TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);

                    em.persist(ticketBean);

                    /*
                     * // 2) rimuovi il vecchio lastLoginData LastLoginDataBean
                     * lastLoginDataBean = userBean.getLastLoginDataBean(); if (
                     * lastLoginDataBean != null) { userBean.setLastLoginDataBean(null);
                     * em.remove(lastLoginDataBean); }
                     * 
                     * 
                     * // 3) inserisci il nuovo lastLoginData lastLoginDataBean = new
                     * LastLoginDataBean(); lastLoginDataBean.setDeviceId(deviceId);
                     * lastLoginDataBean.setDeviceName(deviceName); // TODO memorizzare
                     * anche latitudine e longitudine del device
                     * //lastLoginDataBean.setLatitude(latitude);
                     * //lastLoginDataBean.setLongitude(longitude);
                     * lastLoginDataBean.setTime(new Timestamp((new Date()).getTime()));
                     * em.persist(lastLoginDataBean);
                     * 
                     * userBean.setLastLoginDataBean(lastLoginDataBean);
                     */

                    // 2 e 3 Sostituzione vecchio lastLoginData o creazione nuovo
                    String newEndpoint = null;
                    Boolean deviceFound = false;
                    Boolean userStatusSet = false;
                    Boolean endpointUpdateNeeded = false;
                    Integer outputStatus = User.USER_STATUS_VERIFIED;
                    MobilePhoneBean activeMobilePhone = userBean.activeMobilePhone();

                    /*
                    System.out.println("Stato utente: " + userBean.getUserStatus() + ", activeMobilePhone found: " + (activeMobilePhone != null));
                    if (activeMobilePhone != null) {
                        System.out.println("Phone status: " + activeMobilePhone.getStatus() + "Phone number: " + activeMobilePhone.getNumber());
                    }
                    */

                    if (userBean.getUserType() != User.USER_TYPE_TESTER && userBean.getUserStatus() == User.USER_STATUS_VERIFIED) {

                        SendValidationResult result = new SendValidationResult();
                        UserDeviceBean userDeviceBean = new UserDeviceBean();

                        // Verifica se l'utente ha gi� associato un dispositivo con il device id passato nella richiesta
                        Set<UserDeviceBean> userDeviceBeanList = userBean.getUserDeviceBean();

                        if (userDeviceBeanList != null) {

                            for (UserDeviceBean item : userDeviceBeanList) {
                                
                                // Esamina i device che non sono stati cancellati
                                if (item.getDeviceId().equals(deviceId) && item.getStatus() != UserDevice.USER_DEVICE_STATUS_CANCELLED) {
                                    
                                    // Se l'utente ha gi� verificato il device non � necessario fare nessuna attivit�
                                    if (item.getStatus() == UserDevice.USER_DEVICE_STATUS_VERIFIED) {

                                        // Se il device non � ancora stato associato all'utente effettua l'associazione e invia l'sms di verifica
                                        if (deviceToken != null && userBean.getLastLoginDataBean() != null) {

                                            // Confronta l'ultimo token utilizzato dall'utente con quello passato in input (TODO - verificare se questo controllo � necessario)
                                            String lastToken = userBean.getLastLoginDataBean().getDeviceToken();

                                            if (lastToken != null && !lastToken.equals(deviceToken)) {

                                                // Se sono diversi cancella il vecchio endpoint e generane un altro
                                                String lastEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();

                                                if (lastEndpoint != null) {

                                                    pushNotificationService.removeEndpoint(lastEndpoint);
                                                }

                                                newEndpoint = createDeviceEndpoint(deviceToken, deviceName, deviceFamily, requestId, pushNotificationService);
                                            }
                                            else {

                                                // Se sono uguali rigenera l'endpoint
                                                newEndpoint = createDeviceEndpoint(deviceToken, deviceName, deviceFamily, requestId, pushNotificationService);
                                            }

                                        }

                                        //System.out.println("Trovato stato verificato");
                                        item.setDeviceEndpoint(newEndpoint);
                                        
                                        //System.out.println("Aggiornamento timestamp ultimo utilizzo");
                                        Date currentDate = new Timestamp((new Date()).getTime());
                                        item.setLastUsedTimestamp(currentDate);
                                        
                                        em.merge(item);
                                        
                                        endpointUpdateNeeded = true;
                                        deviceFound = true;
                                        userStatusSet = true;
                                        break;
                                    }

                                    // Se il device si trova in stato pending rimanda l'sms per la verifica (TODO capire se � necessario impostare un numero massimo di sms)
                                    if (item.getStatus() == UserDevice.USER_DEVICE_STATUS_PENDING) {

                                        //System.out.println("Trovato stato pending");
                                        deviceFound = true;

                                        if (activeMobilePhone != null && activeMobilePhone.getNumber() != null) {
                                            outputStatus = User.USER_STATUS_DEVICE_TO_VERIFIED;

                                            userDeviceBean = item;
                                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                                                    "Send SMS verification for device " + userDeviceBean.getDeviceId());

                                            // Invio sms di conferma
                                            result = ResendValidation.sendForDevice(sendingType, maxRetryAttemps, userBean, userDeviceBean, loggerService, emailSender, 
                                                    smsService, userCategoryService, stringSubstitution);
                                            break;
                                        }
                                    }
                                }
                            }
                        }

                        if (!deviceFound) {

                            //System.out.println("Device non trovato");

                            //System.out.println("Crea il nuovo device");

                            // Crea un nuovo device e genera il codice di verifica e invialo via sms
                            String verificationCode = new IdGenerator().generateNumericId(8);

                            Date currentDate = new Timestamp((new Date()).getTime());
                            
                            userDeviceBean.setCreationTimestamp(currentDate);
                            userDeviceBean.setAssociationTimestamp(null);
                            userDeviceBean.setLastUsedTimestamp(currentDate);

                            //System.out.println("Generato codice di verifica " + verificationCode + " per device");

                            userDeviceBean.setVerificationCode(verificationCode);
                            userDeviceBean.setDeviceId(deviceId);
                            userDeviceBean.setDeviceName(deviceName);
                            userDeviceBean.setDeviceFamily(deviceFamily);
                            userDeviceBean.setDeviceEndpoint(newEndpoint);
                            userDeviceBean.setDeviceToken(deviceToken);
                            
                            userDeviceBean.setStatus(UserDevice.USER_DEVICE_STATUS_PENDING);
                            userBean.getUserDeviceBean().add(userDeviceBean);
                            userDeviceBean.setUserBean(userBean);
                            
                            //em.persist(userDeviceBean);
                            
                            if (userBean.getUserType() == User.USER_TYPE_GUEST || userBean.getUserType() == User.USER_TYPE_MULTICARD ||
                                    userBean.getPersonalDataBean().getSecurityDataEmail().equals("info@trustmyphone.com") ||
                                    userBean.getPersonalDataBean().getSecurityDataEmail().equals("esf_test_es@test.test") ||
                                    userBean.getPersonalDataBean().getSecurityDataEmail().equals("esf_test_espiva@test.test")) {
                                
                                System.out.println("Verifica automatica device");
                                
                                userDeviceBean.setStatus(UserDevice.USER_DEVICE_STATUS_VERIFIED);
                                
                                if (userBean.getUserType() == User.USER_TYPE_MULTICARD) {
                                    userBean.setSource(User.USER_MULTICARD);
                                }
                                
                                result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_SUCCESS);
                            }
                            else {
                                
                                /*Verifica se il device � associato ad un tipo utente esterno  MYCICERO, in questo caso il device risulata gi� verificato
                                se � lo stesso adoperato in fase di registrazione nuovo utente MYCICERO. Per gli altri device lo stato sar� da verificare */
                                if( userBean.getSource() != null && userBean.getSource().equalsIgnoreCase(User.USER_SOURCE_MYCICERO) && user.getDeviceId() != null && user.getDeviceId().equals(deviceId)){
                                    
                                    //Verifica se il device � associato ad un tipo utente esterno  MYCICERO, in questo caso il device risulata gi� verificato
                                    System.out.println("**************** DEBUG INFO::     Authentication user type:: "+ userBean.getSource());
                                    if(userBean.getSource().equalsIgnoreCase(User.USER_SOURCE_MYCICERO)){
                                        userDeviceBean.setStatus(UserDevice.USER_DEVICE_STATUS_VERIFIED);
                                    }
                                    
                                    UserDeviceBean userDeviceBeanMyCicero = QueryRepository.findDeviceByDeviceId(em, deviceId);
                                    if(userDeviceBeanMyCicero == null){
                                        em.persist(userDeviceBean);
                                        result.setStatusCode(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_SUCCESS);
                                    }
                                }
                                else {
                                    
                                    if (activeMobilePhone != null && activeMobilePhone.getNumber() != null /*&& ! userBean.getSource().equalsIgnoreCase(User.USER_SOURCE_MYCICERO)*/) {
        
                                        outputStatus = User.USER_STATUS_DEVICE_TO_VERIFIED;
        
                                        //Invio sms di conferma
                                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Send SMS verification for device "
                                                + userDeviceBean.getDeviceId());
        
                                        result = ResendValidation.sendForDevice(sendingType, maxRetryAttemps, userBean, userDeviceBean, loggerService, emailSender, 
                                                smsService, userCategoryService, stringSubstitution);
                                    }
                                }
                            }
                            
                            authenticationResponse = new AuthenticationResponse();

                            if (result != null && result.getStatusCode() != null && result.getStatusCode().equals(ResponseHelper.MOBILE_PHONE_SEND_VALIDATION_FAILURE)) {

                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error sending verification mobile phone. "
                                        + result.getMessageCode() + " (" + result.getErrorCode() + ")");

                                userTransaction.commit();

                                authenticationResponse.setStatusCode(ResponseHelper.MOBILE_PHONE_UPDATE_SENDING_FAILURE);
                                authenticationResponse.setTicketId(ticketBean.getTicketId());
                                authenticationResponse.setUser(userBean.toUser());
                                return authenticationResponse;
                            }
                        }
                    }

                    LastLoginDataBean lastLoginDataBean = userBean.getLastLoginDataBean();
                    if (lastLoginDataBean != null) {

                        userBean.getLastLoginDataBean().setDeviceId(deviceId);
                        userBean.getLastLoginDataBean().setDeviceName(deviceName);
                        userBean.getLastLoginDataBean().setDeviceToken(deviceToken);
                        userBean.getLastLoginDataBean().setDeviceFamily(deviceFamily);
                        if (endpointUpdateNeeded) {
                            userBean.getLastLoginDataBean().setDeviceEndpoint(newEndpoint);
                        }
                        userBean.getLastLoginDataBean().setTime(new Timestamp((new Date()).getTime()));
                    }
                    else {

                        lastLoginDataBean = new LastLoginDataBean();
                        lastLoginDataBean.setDeviceId(deviceId);
                        lastLoginDataBean.setDeviceName(deviceName);
                        lastLoginDataBean.setDeviceToken(deviceToken);
                        lastLoginDataBean.setDeviceFamily(deviceFamily);
                        lastLoginDataBean.setDeviceEndpoint(null);

                        // TODO memorizzare anche latitudine e longitudine del device
                        // lastLoginDataBean.setLatitude(latitude);
                        // lastLoginDataBean.setLongitude(longitude);
                        lastLoginDataBean.setTime(new Timestamp((new Date()).getTime()));
                        em.persist(lastLoginDataBean);

                        userBean.setLastLoginDataBean(lastLoginDataBean);
                    }

                    // Cancellazione delle informazioni per il recupero della password
                    PersonalDataBean personalDataBean = userBean.getPersonalDataBean();
                    personalDataBean.setExpirationDateRescuePassword(null);
                    personalDataBean.setRescuePassword(null);
                    // TODO verificare se serve em.merge(personalDataBean);

                    if (userStatusSet || userBean.getUserStatus() == User.USER_STATUS_TEMPORARY_PASSWORD) {
                        userBean.setUserStatus(User.USER_STATUS_VERIFIED);
                        user.setUserStatus(User.USER_STATUS_VERIFIED);
                    }
                    if (lastLoginErrorDataBean != null) {
                        userBean.getLastLoginErrorDataBean().resetAttempts();
                    }

                    // Impostazione del flag oldUser per migrazione utenti
                    if (userBean.getOldUser() == null) {

                        Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());

                        if (useNewFlow) {

                            //System.out.println("Primo accesso nuovo flusso");

                            // Se l'utente appartiene alla nuova categoria, ma non ha numeri di telefono associati, allora oldUser va impostato a true
                            if (userBean.getMobilePhoneList().isEmpty()) {
                                userBean.setOldUser(true);
                                //System.out.println("OldUser impostato a true");
                            }
                            else {
                                userBean.setOldUser(false);
                                //System.out.println("OldUser impostato a false");
                            }

                            // Modifica per resettare il flag di registrazione completata per i vecchi utenti che utilizzano il nuovo flusso voucher
                            if (userBean.getMobilePhoneList().isEmpty() && userBean.getUserStatusRegistrationCompleted() == true) {

                                // Se un utente non ha numeri di telefono associati (nemmeno in stato cancellato) allora � un utente da migrare
                                //System.out.println("Utente con registrazione completata da migrare");
                                userBean.unsetRegistrationCompleted();

                                // Questa operazione � stata annullata perch� i vecchi token saranno utilizzabili anche con il nuovo codice esercente
                                /*
                                // Tutti i metodi di pagamento in stato 1 o 2 sono cancellati
                                for(PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                                    
                                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                                    em.merge(paymentInfoBean);
                                }
                                */
                            }
                        }
                    }

                    em.merge(userBean);

                    // 4) restituisci il risultato
                    authenticationResponse = new AuthenticationResponse();

                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
                    authenticationResponse.setTicketId(ticketBean.getTicketId());
                    authenticationResponse.setLoyaltySessionId(sessionId);

                    //System.out.println("id: " + userBean.getId());

                    User userOut = userBean.toUser();
                    
                    // Controlla se ci sono nuovi elementi nellla tabella documenti che l'utente non ha ancora accettato
                    UserCategoryBean userCategoryBean = null;
                    
                    Boolean isGuestFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.GUEST_FLOW.getCode());
                    Boolean isMulticardFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.MULTICARD_FLOW.getCode());
                    if (isGuestFlow) {
                        userCategoryBean = QueryRepository.findUserCategoryByName(em, UserCategoryType.GUEST_FLOW.getCode());
                    }
                    else if (isMulticardFlow) {
                        userCategoryBean = QueryRepository.findUserCategoryByName(em, UserCategoryType.MULTICARD_FLOW.getCode());
                    }
                    else {
                        userCategoryBean = QueryRepository.findUserCategoryByName(em, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
                    }
                    
                    // Ricerca gli elementi della tabella documenti associati alla categoria 3 o con categoria null
                    List<DocumentBean> documentBeanList = QueryRepository.findDocumentAll(em);
                    for(DocumentBean documentBean : documentBeanList) {
                        
                        // Si escludono dall'elaborazione i documenti con groupCategory non null
                        if (documentBean.getGroupCategory() == null || documentBean.getGroupCategory().isEmpty()) {
                        
                            if (documentBean.getUserCategory() == null || documentBean.getUserCategory().equals(String.valueOf(userCategoryBean.getId()))) {
                                
                                List<DocumentAttributeBean> documentAttributeBeanList = documentBean.getDocumentCheck();
                                
                                for(DocumentAttributeBean documentAttributeBean : documentAttributeBeanList) {
                                    
                                    //System.out.println("In verifica " + documentAttributeBean.getCheckKey());
                                    
                                    if ((documentAttributeBean.getConditionText() != null && !documentAttributeBean.getConditionText().isEmpty()) ||
                                        (documentAttributeBean.getSubTitle()      != null && !documentAttributeBean.getSubTitle().isEmpty())) {
                                        
                                        //System.out.println("In elaborazione " + documentAttributeBean.getCheckKey());
                                        
                                        // Se l'elemento non � presente tra i termini e condizioni associati all'utente allora va inserito con valid a false
                                        String checkKey = documentAttributeBean.getCheckKey();
                                        
                                        Boolean checkKeyFound = false;
                                        
                                        Set<TermsOfService> termsOfServiceList = userOut.getPersonalData().getTermsOfServiceData();
                                        for(TermsOfService termsOfService : termsOfServiceList) {
                                            
                                            if (termsOfService.getKeyval().equals(checkKey)) {
                                                checkKeyFound = true;
                                                //System.out.println(checkKey + " gi� presente");
                                                break;
                                            }
                                        }
                                        
                                        if (!checkKeyFound) {
                                            
                                            //System.out.println(checkKey + " non trovato");
                                            
                                            TermsOfService termsOfServiceNew = new TermsOfService();
                                            termsOfServiceNew.setAccepted(false);
                                            termsOfServiceNew.setKeyval(checkKey);
                                            termsOfServiceNew.setPersonalData(userOut.getPersonalData());
                                            termsOfServiceNew.setValid(Boolean.FALSE);
                                            
                                            userOut.getPersonalData().getTermsOfServiceData().add(termsOfServiceNew);
                                            
                                            //System.out.println(checkKey + " inserito");
                                        }
                                    }
                                }
                            }
                        }
                        else {
                            
                            //System.out.println("Trovato documento " + documentBean.getDocumentkey() + " con groupCategory non nullo: " + documentBean.getGroupCategory());
                        }
                    }
                    
                    userOut.setUserStatus(outputStatus);
                    
                    Boolean loyaltyCheckEnabled = Boolean.TRUE;
                    
                    // I metodi di pagamento di tipo multicard devono essere filtrati in base al dpan attivo
                    String activeDpan = userBean.getActiveMcCardDpan();
                    
                    System.out.println("ActiveDpan: " + activeDpan);
                    
                    for(Iterator<PaymentInfo> i = userOut.getPaymentData().iterator(); i.hasNext();) {
                        PaymentInfo paymentInfo = i.next();
                        
                        if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                            
                            System.out.println("Trovata multicard con dpan " + paymentInfo.getToken());
                            
                            if (paymentInfo.getToken() != null && !paymentInfo.getToken().equals(activeDpan)) {
                            
                                System.out.println("Multicard non restituita");
                                
                                i.remove();
                            }
                        }
                    }
                    
                    //System.out.println("Verifica loyaltyCheckEnabled - createDate: " + userOut.getCreateDate().getTime() + ", deployDate: " + deployDateLong);
                    
                    if ((userOut.getEniStationUserType() != null && userOut.getEniStationUserType() == Boolean.TRUE) || userOut.getCreateDate().getTime() <= deployDateLong) {
                        loyaltyCheckEnabled = Boolean.FALSE;
                    }
                    userOut.setLoyaltyCheckEnabled(loyaltyCheckEnabled);
                    authenticationResponse.setUser(userOut);

                    userTransaction.commit();

                    return authenticationResponse;
                }
                else {

                    if (userBean.getPersonalDataBean().getExpirationDateRescuePassword() != null
                            && userBean.getPersonalDataBean().getExpirationDateRescuePassword().after(checkDate)
                            && userBean.getPersonalDataBean().getRescuePassword().equals(passwordHash)) {

                        // Tutti i ticket attivi associati all'utente devono essere
                        // invalidati
                        List<TicketBean> ticketBeanList = QueryRepository.findTicketByUserAndCheckDate(em, userBean, checkDate);

                        if (ticketBeanList != null && ticketBeanList.size() > 0) {

                            for (TicketBean ticketBeanLoop : ticketBeanList) {

                                ticketBeanLoop.setExpirationTimestamp(checkDate);
                                em.merge(ticketBeanLoop);
                            }
                        }

                        User user = userBean.toUser();

                        Integer ticketType = 0;
                        if (user.getUserType() == User.USER_TYPE_CUSTOMER)
                            ticketType = TicketBean.TICKET_TYPE_CUSTOMER;
                        else
                            ticketType = TicketBean.TICKET_TYPE_TESTER;

                        Date now = new Date();
                        Date lastUsed = now;
                        Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

                        String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

                        TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);

                        em.persist(ticketBean);

                        // 4) aggiorna i dati lastLoginDevice e lastLoginTime con i valori
                        // passati in input
                        LastLoginDataBean lastLoginDataBean = userBean.getLastLoginDataBean();
                        if (lastLoginDataBean != null) {

                            String newEndpoint = null;

                            if (deviceToken != null) {
                                String lastToken = lastLoginDataBean.getDeviceToken();

                                if (lastToken != null && !lastToken.equals(deviceToken)) {
                                    String lastEndpoint = lastLoginDataBean.getDeviceEndpoint();

                                    if (lastEndpoint != null) {
                                        pushNotificationService.removeEndpoint(lastEndpoint);
                                    }

                                    newEndpoint = createDeviceEndpoint(deviceToken, deviceName, deviceFamily, requestId, pushNotificationService);
                                }
                                else {
                                    newEndpoint = createDeviceEndpoint(deviceToken, deviceName, deviceFamily, requestId, pushNotificationService);
                                }
                            }

                            lastLoginDataBean.setDeviceId(deviceId);
                            lastLoginDataBean.setDeviceName(deviceName);
                            lastLoginDataBean.setDeviceToken(deviceToken);
                            lastLoginDataBean.setDeviceFamily(deviceFamily);
                            lastLoginDataBean.setDeviceEndpoint(newEndpoint);

                            // TODO memorizzare anche latitudine e longitudine del device
                            // userBean.getLastLoginDataBean().setLatitude(latitude);
                            // userBean.getLastLoginDataBean().setLongitude(longitude);
                            userBean.getLastLoginDataBean().setTime(new Timestamp((new Date()).getTime()));
                            // TODO verificare se serve em.merge(lastLoginDataBean);
                        }
                        else {
                            lastLoginDataBean = new LastLoginDataBean();
                            lastLoginDataBean.setDeviceId(deviceId);
                            lastLoginDataBean.setDeviceName(deviceName);
                            lastLoginDataBean.setDeviceToken(deviceToken);
                            lastLoginDataBean.setDeviceFamily(deviceFamily);

                            String newEndpoint = createDeviceEndpoint(deviceToken, deviceName, deviceFamily, requestId, pushNotificationService);
                            lastLoginDataBean.setDeviceEndpoint(newEndpoint);

                            // TODO memorizzare anche latitudine e longitudine del device
                            // lastLoginDataBean.setLatitude(latitude);
                            // lastLoginDataBean.setLongitude(longitude);
                            lastLoginDataBean.setTime(new Timestamp((new Date()).getTime()));
                            em.persist(lastLoginDataBean);

                            userBean.setLastLoginDataBean(lastLoginDataBean);
                        }

                        userBean.setUserStatus(User.USER_STATUS_TEMPORARY_PASSWORD);
                        user.setUserStatus(User.USER_STATUS_TEMPORARY_PASSWORD);

                        if (lastLoginErrorDataBean != null) {
                            userBean.getLastLoginErrorDataBean().resetAttempts();
                        }

                        em.merge(userBean);

                        // 5) restituisci il risultato
                        authenticationResponse = new AuthenticationResponse();

                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
                        authenticationResponse.setTicketId(ticketBean.getTicketId());
                        authenticationResponse.setLoyaltySessionId(sessionId);
                        authenticationResponse.setUser(user);

                        userTransaction.commit();

                        return authenticationResponse;
                    }
                    else {

                        // Aggiornamento dei tentativi di accesso dell'utente;
                        if (lastLoginErrorDataBean != null) {

                            //System.out.println("lastLoginErrorDataBean != null");
                            userBean.getLastLoginErrorDataBean().addAttempt();

                            if (userBean.getLastLoginErrorDataBean().getAttempt() > loginAttemptsLimit) {

                                Date now = new Date();
                                Date expiryDate = DateHelper.addMinutesToDate(loginLockExpiryTime, now);
                                userBean.getLastLoginErrorDataBean().setTime(expiryDate);
                            }
                            //System.out.println("attempt: " + userBean.getLastLoginErrorDataBean().getAttempt());
                        }
                        else {

                            //System.out.println("lastLoginErrorDataBean == null");
                            lastLoginErrorDataBean = new LastLoginErrorDataBean();
                            lastLoginErrorDataBean.addAttempt();

                            em.persist(lastLoginErrorDataBean);

                            userBean.setLastLoginErrorDataBean(lastLoginErrorDataBean);
                        }

                        em.merge(userBean);
                        
                        
                        // Se l'utenza esiste anche su DWH ed ha lo stesso codice fiscale bisogna dire all'utente di effettuare la login con le credenziali Eni Pay
                        
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Ricerca utente " + email + " su DWH");

                        authenticationResponse = new AuthenticationResponse();
                        String decryptedPassword = null;

                        try {
                            // Effettua il decrypt della password passata in input dal servizio
                            EncryptionRSA encryption = new EncryptionRSA();
                            encryption.loadPrivateKey(privateKeyPEM);
                            decryptedPassword = encryption.decrypt(passwordEncrypted);
                        }
                        catch (Exception ex) {
                            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Decryption error: " + ex.getMessage());
                            authenticationResponse = new AuthenticationResponse();
                            authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);

                            userTransaction.commit();

                            return authenticationResponse;
                        }

                        DWHAdapterResult result = dwhAdapterService.checkLegacyPasswordPlus(email, decryptedPassword, requestId);

                        if (!result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS)) {

                            if (result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_REG_LITE)) {
                                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_DWH_REG_LITE);
                            }
                            else {
                                if (result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR)) {
                                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);
                                }
                                else {
                                    // Se l'utente non esiste nemmeno sul server DWH allora bisogna restituire un errore per login fallito
                                    authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);
                                }
                            }
                        }
                        else {
                            
                            if (result.getUserDetail() == null || (result.getUserDetail() != null && result.getUserDetail().getFiscalCode() == null)) {
                                
                                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "user detail error: user detail null or fiscalcode null");
                                
                                userTransaction.commit();
                                
                                authenticationResponse = new AuthenticationResponse();
                                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_INVALID_USER);
                                
                                return authenticationResponse;
                            }
                            
                            
                            // Controllo per blocco migrazione utente che si trova in blacklist sul backend Enistation
                            if (result.getUserDetail().getFlagBlackList()) {
                                
                                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "user in blacklist in Enistation backend");
                                
                                userTransaction.commit();
                                
                                authenticationResponse = new AuthenticationResponse();
                                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_DWH_BLACKLIST);
                                
                                return authenticationResponse;
                            }
                            
                            
                            // Se il codice fiscale dell'utente che si sta loggando � lo stesso dell'utente restituito dal DWH allora bisogna rispondere di effettuare la login con le crdenziali Eni Pay
                            
                            if (userBean.getPersonalDataBean().getFiscalCode().equals(result.getUserDetail().getFiscalCode())) {
                                
                                // Esiste gi� un utente con il codice fiscale specificato
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Fiscalcode already used. Logon with Eni Pay credentials");

                                userTransaction.commit();

                                authenticationResponse = new AuthenticationResponse();
                                authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_INVALID_USER);
                                
                                return authenticationResponse;
                            }
                            
                            
                        }
                        
                        // La password inserita non � corretta
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong password for user " + email);

                        authenticationResponse = new AuthenticationResponse();
                        authenticationResponse.setStatusCode(ResponseHelper.USER_AUTH_LOGIN_ERROR);

                        userTransaction.commit();

                        return authenticationResponse;
                    }
                }
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                System.err.println("IllegalStateException: " + e.getMessage());
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                System.err.println("SecurityException: " + e.getMessage());
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                System.err.println("SystemException: " + e.getMessage());
            }

            String message = "FAILED authentication with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private String createDeviceEndpoint(String deviceToken, String deviceData, String deviceFamily, String requestId, PushNotificationServiceRemote pushNotificationService) {
        Platform platform = null;

        if (deviceFamily.equals("AND")) {
            platform = Platform.GCM;
        }

        if (deviceFamily.equals("IPH")) {
            platform = Platform.APNS;
        }

        if (deviceFamily.equals("WPH")) {
            platform = Platform.WNS;
        }

        if (platform != null) {
            PushNotificationResult endpointResult = pushNotificationService.createEndpoint(deviceToken, deviceData, platform);

            if (endpointResult.getStatusCode().equals(com.techedge.mp.pushnotification.adapter.interfaces.StatusCode.PUSH_NOTIFICATION_CREATE_ENDPOINT_SUCCESS)) {
                //pushNotificationService.publishMessage(endpointResult.getArnEndpoint(), "Messaggio di prova", platform);
                return endpointResult.getArnEndpoint();
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Publish message failed! " + endpointResult.getMessage());

                return null;
            }
        }
        else {
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "deviceFamily (" + deviceFamily
                    + ") from requestID doesn't match a valid Platform");

            return null;
        }
    }
}
