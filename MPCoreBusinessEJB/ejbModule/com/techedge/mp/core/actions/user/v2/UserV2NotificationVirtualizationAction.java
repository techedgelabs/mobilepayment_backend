package com.techedge.mp.core.actions.user.v2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.RefuelingServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.CRMSfNotifyEvent;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.utilities.AsyncDWHService;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.utilities.crm.EVENT_TYPE;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CardDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetLoyaltyCardListResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2NotificationVirtualizationAction {

    private final String GROUP_CATEGORY_LOYALTY = "LOYALTY";
    
    
    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    @EJB
    private CRMService    crmService;
    
    @EJB
    private RefuelingServiceRemote refuelingService;
    
    @EJB
    private ParametersService parametersService;
    
    private final String 				PARAM_CRM_SF_ACTIVE="CRM_SF_ACTIVE";

    public UserV2NotificationVirtualizationAction() {}

    public String execute(String ticketId, final String requestId, final Integer reconciliationMaxAttempts, EmailSenderRemote emailSender, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_V2_NOTIFICATION_VIRTUALIZATION_INVALID_TICKET;
            }

            final UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_V2_NOTIFICATION_VIRTUALIZATION_INVALID_TICKET;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update user fields in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_V2_NOTIFICATION_VIRTUALIZATION_UNAUTHORIZED;
            }

            /*
            if (!userBean.getUserStatusRegistrationCompleted()) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User hasn't complete registration");

                userTransaction.commit();

                return ResponseHelper.USER_V2_NOTIFICATION_VIRTUALIZATION_UNAUTHORIZED;
            }
            */
            
            
            if (userBean.getVoucherPaymentMethod() == null || (userBean.getVoucherPaymentMethod() != null && userBean.getVoucherPaymentMethod().getPin() == null)
                    || (userBean.getVoucherPaymentMethod() != null && userBean.getVoucherPaymentMethod().getPin().isEmpty())) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not found");

                userTransaction.commit();

                return ResponseHelper.USER_V2_NOTIFICATION_VIRTUALIZATION_UNAUTHORIZED;
            }
            
            // Verifica che l'utente abbia superato lo step di verifica della carta di pagamento
            if (userBean.getDepositCardStepCompleted() == null || !userBean.getDepositCardStepCompleted()) {
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Credit card step not completed");

                userTransaction.commit();

                return ResponseHelper.USER_V2_NOTIFICATION_VIRTUALIZATION_UNAUTHORIZED;
            }
            
            
            if (userBean.getVirtualizationCompleted() == null) {
                userBean.setVirtualizationCompleted(false);
            }

            if (userBean.getVirtualizationCompleted()) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Loyalty Card already virtualized");

                userTransaction.commit();

                return ResponseHelper.USER_V2_NOTIFICATION_VIRTUALIZATION_SUCCESS;
            }

            FidelityServiceRemote fidelityService = EJBHomeCache.getInstance().getFidelityService();

            if (fidelityService == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "FidelityService null");

                userTransaction.commit();

                return ResponseHelper.USER_V2_NOTIFICATION_VIRTUALIZATION_FAILURE;
            }

            Date now = new Date();
            final String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            PartnerType partnerType = PartnerType.MP;
            Long requestTimestamp = now.getTime();
            GetLoyaltyCardListResult getLoyaltyCardListResult;

            try {
                getLoyaltyCardListResult = fidelityService.getLoyaltyCardList(operationID, fiscalCode, partnerType, requestTimestamp);
            }
            catch (Exception ex) {

                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Errore nella connessione al Fidelity Service: " + ex.getMessage());

                userTransaction.commit();

                return ResponseHelper.SYSTEM_ERROR;
            }

            if (getLoyaltyCardListResult == null || (getLoyaltyCardListResult != null && getLoyaltyCardListResult.getCardList().isEmpty())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Loyalty card null");

                userTransaction.commit();

                return ResponseHelper.USER_V2_NOTIFICATION_VIRTUALIZATION_FAILURE;
            }
            CardDetail cardDetail = null;
            for (CardDetail item : getLoyaltyCardListResult.getCardList()) {
                if (item.getVirtual().equals("1")) {
                    cardDetail = item;
                }
            }
            if (cardDetail == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "FidelityService null");

                userTransaction.commit();

                return ResponseHelper.USER_V2_NOTIFICATION_VIRTUALIZATION_FAILURE;
            }

            final String codCard = cardDetail.getPanCode();
            
            /* Inizio vecchio codice per associazione nuova carta loyalty
            LoyaltyCardBean loyaltyCardBean = new LoyaltyCardBean();
            loyaltyCardBean.setCardType(cardDetail.getCardType());
            loyaltyCardBean.setEanCode(cardDetail.getEanCode());
            loyaltyCardBean.setPanCode(cardDetail.getPanCode());
            if (cardDetail.getVirtual().equals("1")) {
                loyaltyCardBean.setIsVirtual(Boolean.TRUE);
            }
            else if (cardDetail.getCardType().equals("0")) {
                loyaltyCardBean.setIsVirtual(Boolean.FALSE);

            }
            loyaltyCardBean.setStatus(LoyaltyCard.LOYALTY_CARD_STATUS_VALIDA);
            loyaltyCardBean.setUserBean(userBean);
            em.persist(loyaltyCardBean);
            /* Fine vecchio codice per associazione nuova carta loyalty */
            
            // Associa la nuova carta loyalty all'utente
            LoyaltyCardBean loyaltyCardBean = userBean.getFirstLoyaltyCard();
            
            if (loyaltyCardBean == null) {
                
                System.out.println("Vecchia loyalty non trovata -> creazione nuova loyalty");
                
                loyaltyCardBean = new LoyaltyCardBean();
                loyaltyCardBean.setCardType(cardDetail.getCardType());
                loyaltyCardBean.setEanCode(cardDetail.getEanCode());
                loyaltyCardBean.setPanCode(cardDetail.getPanCode());
                if (cardDetail.getVirtual().equals("1")) {
                    loyaltyCardBean.setIsVirtual(Boolean.TRUE);
                }
                else if (cardDetail.getCardType().equals("0")) {
                    loyaltyCardBean.setIsVirtual(Boolean.FALSE);

                }
                loyaltyCardBean.setStatus(LoyaltyCard.LOYALTY_CARD_STATUS_VALIDA);
                loyaltyCardBean.setUserBean(userBean);
                em.persist(loyaltyCardBean);
            }
            else {
                
                System.out.println("Vecchia loyalty trovata -> aggiornamento dati loyalty");
                
                loyaltyCardBean.setCardType(cardDetail.getCardType());
                loyaltyCardBean.setEanCode(cardDetail.getEanCode());
                loyaltyCardBean.setPanCode(cardDetail.getPanCode());
                if (cardDetail.getVirtual().equals("1")) {
                    loyaltyCardBean.setIsVirtual(Boolean.TRUE);
                }
                else if (cardDetail.getCardType().equals("0")) {
                    loyaltyCardBean.setIsVirtual(Boolean.FALSE);

                }
                loyaltyCardBean.setStatus(LoyaltyCard.LOYALTY_CARD_STATUS_VALIDA);
                loyaltyCardBean.setUserBean(userBean);
                em.merge(loyaltyCardBean);
            }
            

            Date registrationDate = new Date();
            
            userBean.setVirtualizationCompleted(true);
            userBean.setVirtualizationTimestamp(registrationDate);
            
            Boolean sendEmailRegistrationCompleted = Boolean.FALSE;
            
            if (userBean.getUserStatusRegistrationCompleted() == null || !userBean.getUserStatusRegistrationCompleted()) {
                
                userBean.setUserStatusRegistrationCompleted(Boolean.TRUE);
                userBean.setUserStatusRegistrationTimestamp(registrationDate);
                
                sendEmailRegistrationCompleted = Boolean.TRUE;
            }
            
            // Approvazione termini e condizioni specifiche per le loyalty
            List<DocumentBean> documentBeanList = QueryRepository.findDocumentByGroupCategory(em, this.GROUP_CATEGORY_LOYALTY);
            if(documentBeanList != null && !documentBeanList.isEmpty()) {
                
                for(DocumentBean documentBean : documentBeanList) {
                    
                    if (documentBean.getDocumentCheck() != null && !documentBean.getDocumentCheck().isEmpty()) {
                        
                        DocumentAttributeBean documentAttributeBean = documentBean.getDocumentCheck().get(0);
                        
                        Boolean documentFound = Boolean.FALSE;
                        
                        // Cerca il documento tra quelli gi� associati all'utente
                        Set<TermsOfServiceBean> termsOfServiceBeanList = userBean.getPersonalDataBean().getTermsOfServiceBeanData();
                        
                        if(termsOfServiceBeanList != null && !termsOfServiceBeanList.isEmpty()) {
                            
                            for(TermsOfServiceBean termsOfServiceBean : termsOfServiceBeanList) {
                                
                                if (termsOfServiceBean.getKeyval().equals(documentAttributeBean.getCheckKey())) {
                                    
                                    documentFound = Boolean.TRUE;
                                    
                                    if (!termsOfServiceBean.getAccepted()) {
                                        
                                        System.out.println("Documento " + documentAttributeBean.getCheckKey() + " trovato con flag false -> aggiornamento");
                                        
                                        termsOfServiceBean.setAccepted(Boolean.TRUE);
                                        em.merge(termsOfServiceBean);
                                    }
                                    break;
                                }
                            }
                        }
                        
                        // Sei il documento non � stato trovato deve essere aggiunto
                        if (!documentFound) {
                            
                            System.out.println("Documento " + documentAttributeBean.getCheckKey() + " non trovato -> creazione");
                            
                            TermsOfServiceBean termsOfServiceBean = new TermsOfServiceBean();
                            termsOfServiceBean.setAccepted(Boolean.TRUE);
                            termsOfServiceBean.setKeyval(documentAttributeBean.getCheckKey());
                            termsOfServiceBean.setValid(Boolean.TRUE);
                            termsOfServiceBean.setPersonalDataBean(userBean.getPersonalDataBean());
                            em.persist(termsOfServiceBean);
                            
                            userBean.getPersonalDataBean().getTermsOfServiceBeanData().add(termsOfServiceBean);
                            em.merge(userBean);
                        }
                    }
                }
            }
            

            List<TermsOfServiceBean> listTermOfService = QueryRepository.findTermOfServiceByPersonalDataId(em, userBean.getPersonalDataBean());

            if (listTermOfService == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "flagPrivacy null");
                userTransaction.commit();
                return ResponseHelper.USER_CHECK_FAILURE;
            }

            em.merge(userBean);

            Date timestamp = new Date();
            String name = userBean.getPersonalDataBean().getFirstName();
            String surname = userBean.getPersonalDataBean().getLastName();
            String email = userBean.getPersonalDataBean().getSecurityDataEmail();
            Date dateOfBirth = userBean.getPersonalDataBean().getBirthDate();
            boolean flagNotification = false;
            boolean flagCreditCard = false;
            boolean flagPayment = false;
            String cardBrand = null;
            ClusterType cluster = ClusterType.YOUENI;
            final Long sourceID = userBean.getId();
            final CRMEventSourceType sourceType = CRMEventSourceType.USER;
            boolean privacyFlag1 = false;
            boolean privacyFlag2 = false;

            if (listTermOfService != null) {
                for (TermsOfServiceBean item : listTermOfService) {
                    if (item.getKeyval().equals("NOTIFICATION_NEW_1")) {
                        flagNotification = item.getAccepted();
                        break;
                    }
                }
                for (TermsOfServiceBean item : listTermOfService) {
                    if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                        privacyFlag1 = item.getAccepted();
                        break;
                    }
                }
                for (TermsOfServiceBean item : listTermOfService) {
                    if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                        privacyFlag2 = item.getAccepted();
                        break;
                    }
                }
            }

            if (userBean.getPaymentMethodTypeCreditCard() != null) {
                    flagCreditCard = true;
                    cardBrand = userBean.getPaymentMethodTypeCreditCard().getBrand();
            }
            
            String crmSfActive="0";
            try {
                crmSfActive = parametersService.getParamValue(PARAM_CRM_SF_ACTIVE);
            } catch (ParameterNotFoundException e) {
                e.printStackTrace();
            }
            
            //Nuova implementazione sf
            //recupero parametro CRM_SF_ACTIVE per la gestione della nuova chiamata al CRM SF
            //0 disattivato invio sf
            //1 attivo solo invio sf
            //2 attivi entrambi i crm ibm,sf
            
            if (sendEmailRegistrationCompleted && !userBean.getUserType().equals(User.USER_TYPE_GUEST)) {
            	
                if(crmSfActive.equalsIgnoreCase("0")||crmSfActive.equalsIgnoreCase("2")){
                
	                final UserProfile userProfile = UserProfile.getUserProfileForEventRegistration(fiscalCode, timestamp, name, surname, email, dateOfBirth, flagNotification, 
	                        flagCreditCard, cardBrand, cluster);
	    
	                
	                new Thread(new Runnable() {
	                    
	                    @Override
	                    public void run() {
	                        String crmResponse = crmService.sendNotifyEventOffer(fiscalCode, InteractionPointType.REGISTRATION, userProfile, sourceID, sourceType);
	                        
	                        if (!crmResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
	                            System.err.println("Error in sending crm notification (" + crmResponse + ")");
	                        }
	                    }
	                }, "executeBatchGetOffers (UserV2NotificationVirtualizationAction)").start();
                }
                if(crmSfActive.equalsIgnoreCase("1")||crmSfActive.equalsIgnoreCase("2")){
                	System.out.println("########################################");
                	System.out.println("fiscalCode:" + fiscalCode);
                	System.out.println("timestamp:" +timestamp );
                	System.out.println("flagPayment:" + flagPayment);
                	System.out.println("getFirstName():" +userBean.getPersonalDataBean().getFirstName());
                	System.out.println("surname:" + surname );
                	System.out.println("getSecurityDataEmail():" + userBean.getPersonalDataBean().getSecurityDataEmail());
                	System.out.println("getBirthDate():" + userBean.getPersonalDataBean().getBirthDate());
                	System.out.println("flagNotification:" + flagNotification);
                	System.out.println("flagCreditCard:" + flagCreditCard);
                	System.out.println("cardBrand:" + cardBrand);
                	System.out.println("cluster.getValue():" + cluster.getValue());
                	System.out.println("getActiveMobilePhone():" + userBean.getActiveMobilePhone());
                	System.out.println("pancode:" + codCard);
                	System.out.println("EVENT_TYPE.ISCRIZIONE_NUOVO_CLIENTE.getValue():" +EVENT_TYPE.ISCRIZIONE_NUOVO_CLIENTE.getValue() );
                	System.out.println("########################################");
                	
                	String reqId = new IdGenerator().generateId(16).substring(0, 32);
                	
                	/*:TODO da verificare requestId */
                	final CRMSfNotifyEvent crmSfNotifyEvent= new CRMSfNotifyEvent(reqId, fiscalCode, timestamp, "", "", flagPayment, 
                			0, 0D, "", userBean.getPersonalDataBean().getFirstName(), surname, userBean.getPersonalDataBean().getSecurityDataEmail(), 
                			userBean.getPersonalDataBean().getBirthDate(), flagNotification, flagCreditCard, cardBrand==null?"":cardBrand, 
                			cluster.getValue(), 0D, privacyFlag1, privacyFlag2, userBean.getActiveMobilePhone(), codCard,
                			EVENT_TYPE.ISCRIZIONE_NUOVO_CLIENTE.getValue(), "", "", "");
                	
	                new Thread(new Runnable() {
	                    
	                    @Override
	                    public void run() {
	                    	NotifyEventResponse crmResponse = crmService.sendNotifySfEventOffer(crmSfNotifyEvent.getRequestId(), crmSfNotifyEvent.getFiscalCode(), crmSfNotifyEvent.getDate(), 
	                        		crmSfNotifyEvent.getStationId(), crmSfNotifyEvent.getProductId(), crmSfNotifyEvent.getPaymentFlag(), crmSfNotifyEvent.getCredits(), 
	                        		crmSfNotifyEvent.getQuantity(), crmSfNotifyEvent.getRefuelMode(), crmSfNotifyEvent.getFirstName(), crmSfNotifyEvent.getLastName(), 
	                        		crmSfNotifyEvent.getEmail(), crmSfNotifyEvent.getBirthDate(), crmSfNotifyEvent.getNotificationFlag(), crmSfNotifyEvent.getPaymentCardFlag(), 
	                        		crmSfNotifyEvent.getBrand(), crmSfNotifyEvent.getCluster(),crmSfNotifyEvent.getAmount(), crmSfNotifyEvent.getPrivacyFlag1(), 
	                        		crmSfNotifyEvent.getPrivacyFlag2(), crmSfNotifyEvent.getMobilePhone(), crmSfNotifyEvent.getLoyaltyCard(), crmSfNotifyEvent.getEventType(), 
	                        		crmSfNotifyEvent.getParameter1(), crmSfNotifyEvent.getParameter2(), crmSfNotifyEvent.getPaymentMode());
	    
	                       
	                      System.out.println("crmResponse succes:" + crmResponse.getSuccess());
	                      System.out.println("crmResponse errorCode:" + crmResponse.getErrorCode());
	                      System.out.println("crmResponse message:" + crmResponse.getMessage());
	                      System.out.println("crmResponse requestId:" + crmResponse.getRequestId());
	                        
	                    }
	                }, "executeBatchGetOffersSF (UserV2NotificationVirtualizationAction)").start();
                }
                
                
                /*
                if(userBean.getSource() != null && userBean.getSource().equals(User.USER_SOURCE_ENJOY)){
                    new Thread(new Runnable() {
                        
                        @Override
                        public void run() {
                            String notifySubscriptionResponse = refuelingService.notifySubscription(inputRequestId, userBean.getPersonalDataBean().getFiscalCode());
                            System.out.println("**** INFO from UserV2NotificationVirtualizationAction ***** Calling notifySubscription for user enjoy notification (" + notifySubscriptionResponse + ")");
                            if (!notifySubscriptionResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
                                System.err.println("Error in sending subscription notification (" + notifySubscriptionResponse + ")");
                            }
                        }
                    }, "executeBatchNotifySubscription (UserV2NotificationVirtualizationAction)").start();
                }
                */
            }else if (!sendEmailRegistrationCompleted && !userBean.getUserType().equals(User.USER_TYPE_GUEST)){
                
                if(crmSfActive.equalsIgnoreCase("1")||crmSfActive.equalsIgnoreCase("2")){
                	System.out.println("Evento Associazione carta loyalty crm sf");
                	
                	System.out.println("########################################");
                	System.out.println("fiscalCode:" + fiscalCode);
                	System.out.println("timestamp:" +timestamp );
                	System.out.println("flagPayment:" + flagPayment);
                	System.out.println("getFirstName():" +userBean.getPersonalDataBean().getFirstName());
                	System.out.println("surname:" + surname );
                	System.out.println("getSecurityDataEmail():" + userBean.getPersonalDataBean().getSecurityDataEmail());
                	System.out.println("getBirthDate():" + userBean.getPersonalDataBean().getBirthDate());
                	System.out.println("flagNotification:" + flagNotification);
                	System.out.println("flagCreditCard:" + flagCreditCard);
                	System.out.println("cardBrand:" + cardBrand);
                	System.out.println("cluster.getValue():" + cluster.getValue());
                	System.out.println("getActiveMobilePhone():" + userBean.getActiveMobilePhone());
                	System.out.println("pancode:" + codCard);
                	System.out.println("EVENT_TYPE.ASSOCIAZIONE_CARTA_LOYALTY:" +EVENT_TYPE.ASSOCIAZIONE_CARTA_LOYALTY.getValue() );
                	System.out.println("########################################");
                	
                	String reqId = new IdGenerator().generateId(16).substring(0, 32);
                	
                	/*:TODO da verificare requestId */
                	final CRMSfNotifyEvent crmSfNotifyEvent= new CRMSfNotifyEvent(reqId, fiscalCode, timestamp, "", "", false, 
                			0, 0D, "", "", surname, "", 
                			null, flagNotification, flagCreditCard, "", 
                			cluster.getValue(), 0D, true, true, "", codCard,
                			EVENT_TYPE.ASSOCIAZIONE_CARTA_LOYALTY.getValue(), "", "", "");
        
                    new Thread(new Runnable() {
                        
                        @Override
                        public void run() {
                        	NotifyEventResponse crmResponse = crmService.sendNotifySfEventOffer(crmSfNotifyEvent.getRequestId(), crmSfNotifyEvent.getFiscalCode(), crmSfNotifyEvent.getDate(), 
                            		crmSfNotifyEvent.getStationId(), crmSfNotifyEvent.getProductId(), crmSfNotifyEvent.getPaymentFlag(), crmSfNotifyEvent.getCredits(), 
                            		crmSfNotifyEvent.getQuantity(), crmSfNotifyEvent.getRefuelMode(), crmSfNotifyEvent.getFirstName(), crmSfNotifyEvent.getLastName(), 
                            		crmSfNotifyEvent.getEmail(), crmSfNotifyEvent.getBirthDate(), crmSfNotifyEvent.getNotificationFlag(), crmSfNotifyEvent.getPaymentCardFlag(), 
                            		crmSfNotifyEvent.getBrand(), crmSfNotifyEvent.getCluster(),crmSfNotifyEvent.getAmount(), crmSfNotifyEvent.getPrivacyFlag1(), 
                            		crmSfNotifyEvent.getPrivacyFlag2(), crmSfNotifyEvent.getMobilePhone(), crmSfNotifyEvent.getLoyaltyCard(), crmSfNotifyEvent.getEventType(), 
                            		crmSfNotifyEvent.getParameter1(), crmSfNotifyEvent.getParameter2(), crmSfNotifyEvent.getPaymentMode());
        
                            System.out.println("crmResponse succes:" + crmResponse.getSuccess());
    	                      System.out.println("crmResponse errorCode:" + crmResponse.getErrorCode());
    	                      System.out.println("crmResponse message:" + crmResponse.getMessage());
    	                      System.out.println("crmResponse requestId:" + crmResponse.getRequestId());
                        }
                    }, "executeBatchGetOffersSF (UserV2NotificationVirtualizationAction)").start();
                }
            }
            else {
                
                if (sendEmailRegistrationCompleted) {
                    System.out.println("L'utente ha gi� completato la registrazione. L'invio dei dati al CRM non viene effettuato in questo step.");
                }
                if (userBean.getUserType().equals(User.USER_TYPE_GUEST)) {
                    System.out.println("Utente di tipo guest. L'invio dei dati al CRM non viene effettuato in questo step.");
                }
            }
            
            if (!userBean.getUserType().equals(User.USER_TYPE_GUEST)) {
                
                List<MobilePhoneBean> listMobilePhone = new ArrayList<MobilePhoneBean>();
                
                for (MobilePhoneBean item : userBean.getMobilePhoneList()) {
                    listMobilePhone.add(item);
                }
                
                List<UserSocialDataBean> userSocialDataList = new ArrayList<UserSocialDataBean>();
                
                for (UserSocialDataBean item : userBean.getUserSocialData()) {
                    userSocialDataList.add(item);
                }
                
                AsyncDWHService asyncDWHService = new AsyncDWHService(userTransaction, em, userBean, listMobilePhone, listTermOfService, userSocialDataList, requestId, codCard, reconciliationMaxAttempts);
                
                new Thread(asyncDWHService, "setUserDataPlus (UserV2NotificationVirtualizationAction)").start();
                
    
                if (sendEmailRegistrationCompleted) {
                    // Invio email di conferma registrazione
                    sendEmail(emailSender, userBean, requestId, stringSubstitution);
                }
            }

            userTransaction.commit();

            return ResponseHelper.USER_V2_NOTIFICATION_VIRTUALIZATION_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED notification virtualization (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private void sendEmail(EmailSenderRemote emailSender, UserBean userBean, String requestId, StringSubstitution stringSubstitution) {

        // Invia l'email di conferma iscrizione

        if (emailSender != null) {

            System.out.println("sending confirm registration email");

            String keyFrom = emailSender.getSender();
            
            if (stringSubstitution != null) {
                keyFrom = stringSubstitution.getValue(keyFrom, 1);
            }
            
            System.out.println("keyFrom: " + keyFrom);
            
            EmailType emailType = EmailType.CONFIRM_REGISTRATION_V2;
            String to = userBean.getPersonalDataBean().getSecurityDataEmail();
            List<Parameter> parameters = new ArrayList<Parameter>(0);

            parameters.add(new Parameter("WELCOME", userBean.getPersonalDataBean().getFirstName()));

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Sending email to "
                    + userBean.getPersonalDataBean().getSecurityDataEmail());

            String result = emailSender.sendEmail(emailType, keyFrom, to, parameters);

            //String result = "disabled";

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "SendEmail result: " + result);

        }
    }
}
