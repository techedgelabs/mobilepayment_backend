package com.techedge.mp.core.actions.user;

import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.ValidatePaymentMethodResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserValidatePaymentMethodAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserValidatePaymentMethodAction() {
    }
    
    
    public ValidatePaymentMethodResponse execute(
    		String ticketId,
			String requestId,
			Long   cardId,
			String cardType,
			Double verificationAmount) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	ValidatePaymentMethodResponse validatePaymentMethodResponse = new ValidatePaymentMethodResponse();
    	
    	try {
    		userTransaction.begin();
    		
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket" );
				
    			userTransaction.commit();
    			
    			validatePaymentMethodResponse.setStatusCode(ResponseHelper.USER_VALIDATE_PAYMENT_METHOD_INVALID_TICKET);
    			return validatePaymentMethodResponse;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if ( userBean == null ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found" );
				
    			userTransaction.commit();
    			
    			validatePaymentMethodResponse.setStatusCode(ResponseHelper.USER_VALIDATE_PAYMENT_METHOD_INVALID_TICKET);
    			return validatePaymentMethodResponse;
    		}
    		
    		
    		// Verifica lo stato dell'utente
    		Integer userStatus = userBean.getUserStatus();
    		if ( userStatus != User.USER_STATUS_VERIFIED ) {
    			
    			// Un utente che si trova in questo stato non pu� invocare questo servizio
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to validate payment method in status " + userStatus );
				
    			userTransaction.commit();
    			
    			validatePaymentMethodResponse.setStatusCode(ResponseHelper.USER_VALIDATE_PAYMENT_METHOD_UNAUTHORIZED);
    			return validatePaymentMethodResponse;
    		}
		    
    		// Non � possibile verificare metodi di pagamento diversi da credit_card
            if ( !cardType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD ) ) {
                
                // Payment info non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to validate payment info type different from credit_card");
                
                userTransaction.commit();
                
                validatePaymentMethodResponse.setStatusCode(ResponseHelper.USER_VALIDATE_PAYMENT_METHOD_NOT_FOUND);
                return validatePaymentMethodResponse;
            }

    		PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(cardId, cardType);
    		if (paymentInfoBean == null) {
    			
    			// Payment info non trovato
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment info not found");
				
    			userTransaction.commit();
    			
    			validatePaymentMethodResponse.setStatusCode(ResponseHelper.USER_VALIDATE_PAYMENT_METHOD_NOT_FOUND);
    			return validatePaymentMethodResponse;
    		}
    		
    		
    			
    		if ( paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED ) {
    			
    			// Payment info status not valid
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment info status not valid: " + paymentInfoBean.getStatus());
				
    			userTransaction.commit();
    			
    			validatePaymentMethodResponse.setStatusCode(ResponseHelper.USER_VALIDATE_PAYMENT_METHOD_STATUS_NOT_VALID);
    			return validatePaymentMethodResponse;
    		}
    		
    		
    		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Check amount verification. VerificationAmount: " + verificationAmount + " CheckAmount: " + paymentInfoBean.getCheckAmount());
    		
    		double epsilon = 0.0000001;
    		
			if ( verificationAmount >= ( paymentInfoBean.getCheckAmount() + epsilon ) ||
				 verificationAmount <= ( paymentInfoBean.getCheckAmount() - epsilon ) ) {
				
				// Si sottrae 1 al numero di tentativi residui
    			Integer attemptsLeft = paymentInfoBean.getAttemptsLeft();
    			
    			if ( attemptsLeft > 0 ) {
    				attemptsLeft = attemptsLeft - 1;
    			}
    			
    			// Si aggiorna il numero di tentativi residui
    			paymentInfoBean.setAttemptsLeft(attemptsLeft);
    			
    			if ( attemptsLeft == 0 ) {
    				
    				// L'utente ha esaurito il numero di tentativi di inserimento pin, perci� il metodo di pagamento viene bloccato
    				paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
    				
    				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "No more attempts. Payment method blocked." );
    				
    				if (paymentInfoBean.getDefaultMethod()) {
    				    Set<PaymentInfoBean> paymentInfoBeanList = paymentInfoBean.getUser().getPaymentData();
    				    
    				    for (PaymentInfoBean tmpPaymentInfoBean : paymentInfoBeanList) {
                            if (tmpPaymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)  && 
                                    (tmpPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || 
                                    tmpPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {
                                        
                                tmpPaymentInfoBean.setDefaultMethod(true);
                                em.merge(tmpPaymentInfoBean);
                                break;
                            }
                        }
    				}
    				/*
    				 * Lo storno in questa fase non � pi� necessario
    				// L'importo precedentemente addebitato all'utente per la verifica del metodo di pagamento viene stornato
    				DWHAdapterServiceRemote gpService = EJBHomeCache.getInstance().getGpService();
    	    		
    	    		if ( gpService == null ) {
    	    			
    	    			this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Payment Service unavailable");
    	    		}
    	    		else {
    	    			
    	    			GestPayData callRefundResult = gpService.callRefund(paymentInfoBean.getCheckAmount(), paymentInfoBean.getCheckShopTransactionID(), paymentInfoBean.getCheckShopLogin(), paymentInfoBean.getCheckCurrency(), paymentInfoBean.getCheckBankTransactionID());
    	    			
    	    			// TODO controllo esito operazione
    	    		}
    				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "TODO - Refund check amount: " + paymentInfoBean.getCheckAmount());
    				 *
    				 */
    			}
    			
    			// Si aggiorna il metodo di pagamento sul db
    			em.merge(paymentInfoBean);
    			
    			userTransaction.commit();
    			
    			validatePaymentMethodResponse.setAttemptsLeft(attemptsLeft);
    			validatePaymentMethodResponse.setStatusCode(ResponseHelper.USER_VALIDATE_PAYMENT_METHOD_FAILED);
    			return validatePaymentMethodResponse;
			}
			else {
				
				/*
				 * Lo storno in questa fase non � pi� necessario
				// TODO L'importo precedentemente addebitato all'utente per la verifica del metodo di pagamento viene stornato
				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "TODO - Refund check amount: " + paymentInfoBean.getCheckAmount());
				 *
				 */
				
				// Il metodo di pagamento passa in stato verificato
				paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED);
				paymentInfoBean.setVerifiedTimestamp(new Date());
				
				// Si aggiorna il metodo di pagamento sul db
    			em.merge(paymentInfoBean);
    			
    			userTransaction.commit();
    			
    			validatePaymentMethodResponse.setAttemptsLeft(paymentInfoBean.getAttemptsLeft());
    			validatePaymentMethodResponse.setStatusCode(ResponseHelper.USER_VALIDATE_PAYMENT_METHOD_SUCCESS);
    			return validatePaymentMethodResponse;
				
			}
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED payment method validation with message (" + ex2.getMessage() + ")";
    		this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
			     
	        throw new EJBException(ex2);
    	}
    }
}
