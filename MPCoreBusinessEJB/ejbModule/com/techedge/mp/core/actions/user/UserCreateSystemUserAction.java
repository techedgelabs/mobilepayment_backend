package com.techedge.mp.core.actions.user;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.UserBean;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserCreateSystemUserAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserCreateSystemUserAction() {
    }
    
    
    public String execute() throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		UserBean userBean = new UserBean();
			
			String encodedPassword = "3zYGDSewI+6PCWLX8jfacA==";
			
			Date birthDate = null;
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			try {
		 
				birthDate = formatter.parse("01/01/1950");
		 
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			PersonalDataBean personalDataBean = new PersonalDataBean();
	    	
			personalDataBean.setFirstName("MPSystemUser");
			personalDataBean.setLastName("MPSystemUser");
			personalDataBean.setFiscalCode("XXXXXXXXXXXXXXXX");
			personalDataBean.setBirthDate(birthDate);
			personalDataBean.setBirthMunicipality("Roma");
			personalDataBean.setBirthProvince("RM");
			personalDataBean.setLanguage("IT");
			personalDataBean.setSex("M");
			personalDataBean.setSecurityDataEmail("MPSystemUser");
			personalDataBean.setSecurityDataPassword(encodedPassword);
			personalDataBean.setAddress(null);
			personalDataBean.setBillingAddress(null);
			
			userBean.setPersonalDataBean(personalDataBean);
			userBean.setUserStatus(User.USER_STATUS_VERIFIED);
			userBean.setUserStatusRegistrationCompleted(true);
			userBean.setUserType(User.USER_TYPE_SERVICE);
			
			em.persist(personalDataBean);
			
			em.persist(userBean);
			
			userTransaction.commit();
			
			return "OK";
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED user system creation with message (" + ex2.getMessage() + ")";
    		this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
