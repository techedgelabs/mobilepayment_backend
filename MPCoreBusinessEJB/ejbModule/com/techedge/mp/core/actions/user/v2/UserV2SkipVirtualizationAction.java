package com.techedge.mp.core.actions.user.v2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.RefuelingServiceRemote;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.UserV2Service;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.CRMSfNotifyEvent;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.utilities.AsyncDWHService;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.utilities.crm.EVENT_TYPE;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2SkipVirtualizationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;
    
    @EJB
    private CRMService    crmService;
    
    @EJB
    private RefuelingServiceRemote refuelingService;
    
    @EJB
    private ParametersService parametersService;
    
    private final String 				PARAM_CRM_SF_ACTIVE="CRM_SF_ACTIVE";

    public UserV2SkipVirtualizationAction() {}

    public String execute(String ticketId, String requestId, Integer reconciliationMaxAttempts, UserV2Service userV2Service, UserCategoryService userCategoryService, 
            EmailSenderRemote emailSender, StringSubstitution stringSubstitution) throws EJBException {

        final UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_SKIP_VIRTUALIZATION_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_SKIP_VIRTUALIZATION_INVALID_TICKET;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to skip payment method configuration in status "
                        + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_SKIP_VIRTUALIZATION_UNAUTHORIZED;

            }

            // Il servizio per lo skip della virtualizzazione pu� essere chiamato solo dagli utenti confgurati per l'utilizzo del nuovo acquirer
            Integer userType = userBean.getUserType();
            Boolean useNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());

            if (!useNewAcquirerFlow) {

                // Un utente che esegue il vecchio flusso voucher non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Unable to skip virtualization for user of type " + userType);

                userTransaction.commit();

                return ResponseHelper.USER_SKIP_VIRTUALIZATION_UNAUTHORIZED;
            }
            

            // Verifica che l'utente abbia gi� inserito il pin 
            PaymentInfoBean voucherPaymentInfoBean = userBean.getVoucherPaymentMethod();

            if (voucherPaymentInfoBean == null || voucherPaymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not present");

                userTransaction.commit();

                return ResponseHelper.USER_SKIP_VIRTUALIZATION_UNAUTHORIZED;
            }

            
            // Verifica che l'utente abbia superato lo step di verifica della carta di pagamento
            if (userBean.getDepositCardStepCompleted() == null || !userBean.getDepositCardStepCompleted()) {
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Credit card step not completed");

                userTransaction.commit();

                return ResponseHelper.USER_SKIP_VIRTUALIZATION_UNAUTHORIZED;
            }
            
            Boolean sendEmailRegistrationCompleted = Boolean.FALSE;
            Query query = em.createQuery("select userStatusRegistrationCompleted from UserBean where id = :userId");
            query.setParameter("userId", userBean.getId());

            Boolean userRegistrationCompleted = (Boolean) query.getSingleResult();
            
            if (!userRegistrationCompleted) {
                
                if (!userBean.getUserType().equals(User.USER_TYPE_GUEST)) {
                    
                    System.out.println("Skip virtualizzazione -> imposta il flag registrationCompleted a true");
                    final String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                   
                    //userBean.setUserStatusRegistrationCompleted(Boolean.TRUE);
                    //userBean.setUserStatusRegistrationTimestamp(new Date());
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                            "Aggiornamento dei campi userstatus_registrationcompleted e userstatus_registrationtimestamp nella tabella Users...");
                    
                    query = em.createQuery("update UserBean set userStatusRegistrationCompleted = true, userStatusRegistrationTimestamp = :registrationTimestamp where id = :userId "
                            + "and userStatusRegistrationCompleted = false");
                    query.setParameter("registrationTimestamp", new Date());
                    query.setParameter("userId", userBean.getId());
                    
                    int rows = query.executeUpdate();
                    //userTransaction.commit();
         
                    if (rows > 0) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Aggiornamento registrationCompleted eseguito");
                        
                    	String crmSfActive="0";
                    	try {
                    		crmSfActive = parametersService.getParamValue(PARAM_CRM_SF_ACTIVE);
                		} catch (ParameterNotFoundException e) {
                			e.printStackTrace();
                		}
                    
                        //userTransaction.begin();
                        
                        //userBean = QueryRepository.findUserByFiscalCode(em, fiscalCode);
                        
                        sendEmailRegistrationCompleted = Boolean.TRUE;
                        
                        List<TermsOfServiceBean> listTermOfService = QueryRepository.findTermOfServiceByPersonalDataId(em, userBean.getPersonalDataBean());
        
                        if (listTermOfService == null) {
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "flagPrivacy null");
                            userTransaction.commit();
                            return ResponseHelper.USER_CHECK_FAILURE;
                        }
        
                        Date timestamp = new Date();
                        String name = userBean.getPersonalDataBean().getFirstName();
                        String surname = userBean.getPersonalDataBean().getLastName();
                        String email = userBean.getPersonalDataBean().getSecurityDataEmail();
                        Date dateOfBirth = userBean.getPersonalDataBean().getBirthDate();
                        boolean flagNotification = false;
                        boolean flagCreditCard = false;
                        boolean flagPayment = false;
                        String cardBrand = null;
                        ClusterType cluster = ClusterType.ENIPAY;
                        final Long sourceID = userBean.getId();
                        final CRMEventSourceType sourceType = CRMEventSourceType.USER;
                        boolean privacyFlag1 = false;
                        boolean privacyFlag2 = false;
        
                        if (listTermOfService != null) {
                            for (TermsOfServiceBean item : listTermOfService) {
                                if (item.getKeyval().equals("NOTIFICATION_NEW_1")) {
                                    flagNotification = item.getAccepted();
                                    break;
                                }
                            }
                            for (TermsOfServiceBean item : listTermOfService) {
                                if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                                    privacyFlag1 = item.getAccepted();
                                    break;
                                }
                            }
                            for (TermsOfServiceBean item : listTermOfService) {
                                if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                                    privacyFlag2 = item.getAccepted();
                                    break;
                                }
                            }
                        }
        
                        if (userBean.getPaymentMethodTypeCreditCard() != null) {
                            flagCreditCard = true;
                            cardBrand = userBean.getPaymentMethodTypeCreditCard().getBrand();
                        }
                        
                        //Nuova implementazione sf
                        //recupero parametro CRM_SF_ACTIVE per la gestione della nuova chiamata al CRM SF
                        //0 disattivato invio sf
                        //1 attivo solo invio sf
                        //2 attivi entrambi i crm ibm,sf
                       
                        if(crmSfActive.equalsIgnoreCase("0")||crmSfActive.equalsIgnoreCase("2")){
                        
	                        final UserProfile userProfile = UserProfile.getUserProfileForEventRegistration(fiscalCode, timestamp, name, surname, email, dateOfBirth, flagNotification, 
	                                flagCreditCard, cardBrand, cluster);
	        
	                        
	                        new Thread(new Runnable() {
	                            
	                            @Override
	                            public void run() {
	                                String crmResponse = crmService.sendNotifyEventOffer(fiscalCode, InteractionPointType.REGISTRATION, userProfile, sourceID, sourceType);
	        
	                                if (!crmResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
	                                    System.err.println("Error in sending crm notification (" + crmResponse + ")");
	                                }
	                            }
	                        }, "executeBatchGetOffers (UserV2SkipVirtualizationAction)").start();
                        }
                        if(crmSfActive.equalsIgnoreCase("1")||crmSfActive.equalsIgnoreCase("2")){
                        	
                        	System.out.println("##################################################");
                        	System.out.println("fiscalCode:" + fiscalCode);
                        	System.out.println("timestamp:" + timestamp);
                        	System.out.println("flagPayment:" + flagPayment);
                        	System.out.println("surname:" +surname );
                        	System.out.println("getSecurityDataEmail():" +userBean.getPersonalDataBean().getSecurityDataEmail());
                        	System.out.println("getBirthDate():" + userBean.getPersonalDataBean().getBirthDate());
                        	System.out.println("flagNotification:" + flagNotification);
                        	System.out.println("flagCreditCard:" + flagCreditCard);
                        	System.out.println("cardBrand:" + cardBrand);
                        	System.out.println("cluster.getValue():" +cluster.getValue() );
                        	System.out.println("userBean.getActiveMobilePhone():" +userBean.getActiveMobilePhone() );
                        	String panCode="";
                        	if(userBean.getVirtualLoyaltyCard()!=null){
                        		System.out.println("panCode :" +userBean.getVirtualLoyaltyCard().getPanCode() );
                        		panCode=userBean.getVirtualLoyaltyCard().getPanCode();
                        	}else
                        		System.out.println("panCode: null");
                        	System.out.println("userBean.getVirtualLoyaltyCard().getPanCode():" +panCode );
                        	System.out.println("EVENT_TYPE.ISCRIZIONE_NUOVO_CLIENTE.getValue():" + EVENT_TYPE.ISCRIZIONE_NUOVO_CLIENTE.getValue());
                        	System.out.println("##################################################");
                        	
                        	String reqId = new IdGenerator().generateId(16).substring(0, 32);
                        	
                        	/*:TODO da verificare requestId */
                        	final CRMSfNotifyEvent crmSfNotifyEvent= new CRMSfNotifyEvent(reqId, fiscalCode, timestamp, "", "", flagPayment, 
                        			0, 0D, "", userBean.getPersonalDataBean().getFirstName(), surname, userBean.getPersonalDataBean().getSecurityDataEmail(), 
                        			userBean.getPersonalDataBean().getBirthDate(), flagNotification, flagCreditCard, cardBrand==null?"":cardBrand, 
                        			cluster.getValue(), 0D, privacyFlag1, privacyFlag2, userBean.getActiveMobilePhone(), panCode, 
                        			EVENT_TYPE.ISCRIZIONE_NUOVO_CLIENTE.getValue(), "", "", "");
        	    
        	                new Thread(new Runnable() {
        	                    
        	                    @Override
        	                    public void run() {
        	                    	NotifyEventResponse crmResponse = crmService.sendNotifySfEventOffer(crmSfNotifyEvent.getRequestId(), crmSfNotifyEvent.getFiscalCode(), crmSfNotifyEvent.getDate(), 
        	                        		crmSfNotifyEvent.getStationId(), crmSfNotifyEvent.getProductId(), crmSfNotifyEvent.getPaymentFlag(), crmSfNotifyEvent.getCredits(), 
        	                        		crmSfNotifyEvent.getQuantity(), crmSfNotifyEvent.getRefuelMode(), crmSfNotifyEvent.getFirstName(), crmSfNotifyEvent.getLastName(), 
        	                        		crmSfNotifyEvent.getEmail(), crmSfNotifyEvent.getBirthDate(), crmSfNotifyEvent.getNotificationFlag(), crmSfNotifyEvent.getPaymentCardFlag(), 
        	                        		crmSfNotifyEvent.getBrand(), crmSfNotifyEvent.getCluster(),crmSfNotifyEvent.getAmount(), crmSfNotifyEvent.getPrivacyFlag1(), 
        	                        		crmSfNotifyEvent.getPrivacyFlag2(), crmSfNotifyEvent.getMobilePhone(), crmSfNotifyEvent.getLoyaltyCard(), crmSfNotifyEvent.getEventType(), 
        	                        		crmSfNotifyEvent.getParameter1(), crmSfNotifyEvent.getParameter2(), crmSfNotifyEvent.getPaymentMode());
        	    
        	                    	System.out.println("crmResponse succes:" + crmResponse.getSuccess());
        		                      System.out.println("crmResponse errorCode:" + crmResponse.getErrorCode());
        		                      System.out.println("crmResponse message:" + crmResponse.getMessage());
        		                      System.out.println("crmResponse requestId:" + crmResponse.getRequestId());
        	                    }
        	                }, "executeBatchGetOffersSF (UserV2SkipVirtualizationAction)").start();
                        }
                        
                        /*
                        if(userBean.getSource() != null && userBean.getSource().equals(User.USER_SOURCE_ENJOY)){
                            new Thread(new Runnable() {
                                
                                @Override
                                public void run() {
                                    String notifySubscriptionResponse = refuelingService.notifySubscription(inputRequestId, userBean.getPersonalDataBean().getFiscalCode());
                                    System.out.println("**** INFO from UseV2SkipVirtualizationAction ***** Calling notifySubscription for user enjoy notification (" + notifySubscriptionResponse + ")");
                                    if (!notifySubscriptionResponse.equals(StatusHelper.REFUELING_SUBSCRIPTION_SUCCESS)) {
                                        System.err.println("Error in sending subscription notification (" + notifySubscriptionResponse + ")");
                                    }
                                }
                            }, "executeBatchNotifySubscription (UserV2SkipVirtualizationAction)").start();
                        }
                        */
                        
                        List<MobilePhoneBean> listMobilePhone = new ArrayList<MobilePhoneBean>();
                        
                        for (MobilePhoneBean item : userBean.getMobilePhoneList()) {
                            listMobilePhone.add(item);
                        }
                        
                        List<UserSocialDataBean> userSocialDataList = new ArrayList<UserSocialDataBean>();
                        
                        for (UserSocialDataBean item : userBean.getUserSocialData()) {
                            userSocialDataList.add(item);
                        }
                        
                        AsyncDWHService asyncDWHService = new AsyncDWHService(userTransaction, em, userBean, listMobilePhone, listTermOfService, userSocialDataList, requestId, "", reconciliationMaxAttempts);
                        
                        new Thread(asyncDWHService, "setUserDataPlus (UserV2SkipVirtualizationAction)").start();
                    }
                    else {
                        // anomalia
                        System.out.println("Chiamata anomala: eseguito skip virtualizzazione con flag userStatusRegistrationCompleted gi� impostato a true");
                    }
                }
                else {
                    
                    // anomalia
                    System.out.println("Chiamata anomala: eseguito skip virtualizzazione per utente guest");
                    
                    userBean.setUserStatusRegistrationCompleted(Boolean.TRUE);
                    userBean.setUserStatusRegistrationTimestamp(new Date());
    
                    em.merge(userBean);
                }
            }
            else {
                
                // anomalia
                System.out.println("Chiamata anomala: eseguito skip virtualizzazione con flag userStatusRegistrationCompleted gi� impostato a true");
            }

            // Rinnova il ticket
            ticketBean.renew();
            em.merge(ticketBean);
            
            if (sendEmailRegistrationCompleted) {
                // Invio email di conferma registrazione
                sendEmail(emailSender, userBean, requestId, stringSubstitution);
            }

            userTransaction.commit();

            return ResponseHelper.USER_SKIP_VIRTUALIZATION_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED skip vortualization with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
    
    private void sendEmail(EmailSenderRemote emailSender, UserBean userBean, String requestId, StringSubstitution stringSubstitution) {

        // Invia l'email di conferma iscrizione

        if (emailSender != null) {

            System.out.println("sending confirm registration email");

            String keyFrom = emailSender.getSender();
            
            if (stringSubstitution != null) {
                keyFrom = stringSubstitution.getValue(keyFrom, 1);
            }
            
            System.out.println("keyFrom: " + keyFrom);
            EmailType emailType = EmailType.CONFIRM_REGISTRATION_V2;
            String to = userBean.getPersonalDataBean().getSecurityDataEmail();
            List<Parameter> parameters = new ArrayList<Parameter>(0);

            parameters.add(new Parameter("WELCOME", userBean.getPersonalDataBean().getFirstName()));

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Sending email to "
                    + userBean.getPersonalDataBean().getSecurityDataEmail());

            String result = emailSender.sendEmail(emailType, keyFrom, to, parameters);

            //String result = "disabled";

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "SendEmail result: " + result);

        }
    }
}
