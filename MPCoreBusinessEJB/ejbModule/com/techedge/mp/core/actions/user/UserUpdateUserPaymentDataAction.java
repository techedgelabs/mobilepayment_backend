package com.techedge.mp.core.actions.user;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PromoUsersOnHoldStatus;
import com.techedge.mp.core.business.interfaces.PromotionType;
import com.techedge.mp.core.business.interfaces.PromotionVoucherStatus;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.CardDepositTransactionBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PromoUsersOnHoldBean;
import com.techedge.mp.core.business.model.PromotionBean;
import com.techedge.mp.core.business.model.ShopTransactionDataBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.PanHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserUpdateUserPaymentDataAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    @EJB
    private UserService   userService;

//    @SuppressWarnings("serial")
//    private HashMap<String, String> voucherStatus = new HashMap<String, String>() {
//                                                      {
//                                                          put(Voucher.VOUCHER_STATUS_ANNULLATO, ResponseHelper.USER_LOAD_VOUCHER_CANCELED);
//                                                          put(Voucher.VOUCHER_STATUS_CANCELLATO, ResponseHelper.USER_LOAD_VOUCHER_REMOVED);
//                                                          put(Voucher.VOUCHER_STATUS_DA_CONFERMARE, ResponseHelper.USER_LOAD_VOUCHER_TO_CONFIRM);
//                                                          put(Voucher.VOUCHER_STATUS_ESAURITO, ResponseHelper.USER_LOAD_VOUCHER_SPENT);
//                                                          put(Voucher.VOUCHER_STATUS_INESISTENTE, ResponseHelper.USER_LOAD_VOUCHER_INEXISTENT);
//                                                          put(Voucher.VOUCHER_STATUS_SCADUTO, ResponseHelper.USER_LOAD_VOUCHER_EXPIRED);
//                                                          put(Voucher.VOUCHER_STATUS_VALIDO, ResponseHelper.USER_LOAD_VOUCHER_VALID);
//                                                      }
//                                                  };

    public UserUpdateUserPaymentDataAction() {}

    public String execute(String transactionType, String transactionResult, String shopTransactionID, String bankTransactionID, String authorizationCode, String currency,
            String amount, String country, String buyerName, String buyerEmail, String errorCode, String errorDescription, String alertCode, String alertDescription,
            String TransactionKey, String token, String tokenExpiryMonth, String tokenExpiryYear, String cardBin, String TDLevel, Integer pinMaxAttempts, String shopLoginCheck,
            String shopLoginCheckNewFlow, boolean checkPaymentMethodExists, String acquirerId, String groupAcquirer, String encodedSecretKey, EmailSenderRemote emailSender, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();

            if (gpService == null) {

                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Payment Service unavailable");
            }
            else {

                // Memorizzazione su db della richiesta di deposito carta
                CardDepositTransactionBean cardDepositTransactionBean = new CardDepositTransactionBean();

                cardDepositTransactionBean.setCreationTimestamp(new Date());
                cardDepositTransactionBean.setTransactionType(transactionType);
                cardDepositTransactionBean.setTransactionResult(transactionResult);
                cardDepositTransactionBean.setShopTransactionID(shopTransactionID);
                cardDepositTransactionBean.setBankTransactionID(bankTransactionID);
                cardDepositTransactionBean.setAuthorizationCode(authorizationCode);
                cardDepositTransactionBean.setCurrency(currency);
                cardDepositTransactionBean.setAmount(amount);
                cardDepositTransactionBean.setCountry(country);
                cardDepositTransactionBean.setBuyerName(buyerName);
                cardDepositTransactionBean.setBuyerEmail(buyerEmail);
                cardDepositTransactionBean.setErrorCode(errorCode);
                cardDepositTransactionBean.setErrorDescription(errorDescription);
                cardDepositTransactionBean.setAlertCode(alertCode);
                cardDepositTransactionBean.setAlertDescription(alertDescription);
                cardDepositTransactionBean.setTransactionKey(TransactionKey);
                cardDepositTransactionBean.setToken(token);
                cardDepositTransactionBean.setTokenExpiryMonth(tokenExpiryMonth);
                cardDepositTransactionBean.setTokenExpiryYear(tokenExpiryYear);
                cardDepositTransactionBean.setCardBIN(cardBin);
                cardDepositTransactionBean.setTdLevel(TDLevel);

                em.persist(cardDepositTransactionBean);

                // Ricerca dell'oggetto con lo shopTransactionId restituito
                ShopTransactionDataBean shopTransactionDataBean = QueryRepository.findShopTransactionBeanDataByTransactionIdAndStatus(em, shopTransactionID,
                        ShopTransactionDataBean.STATUS_NEW);

                if (shopTransactionDataBean == null) {

                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "No deposit card process for shopTransactionId");
                }
                else {

                    PaymentInfoBean paymentInfoBean = shopTransactionDataBean.getPaymentInfo();

                    if (paymentInfoBean == null) {

                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "No paymentData for shopTransactionId");
                    }
                    else {

                        if (paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_PENDING) {

                            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null,
                                    "No pending paymentData for shopTransactionId");
                        }
                        else {

                            if (transactionResult == "OK") {

                                boolean checkPayment = false;

                                UserBean userBean = paymentInfoBean.getUser();
                                Integer userType = userBean.getUserType();
                                Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());
                                Boolean useNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());

                                if (useNewFlow && (shopLoginCheckNewFlow == null || shopLoginCheckNewFlow.equals(""))) {

                                    String pan = token.substring(0, 2) + "XXXXXXXXXX" + token.substring(12, 16);
                                    paymentInfoBean.setExpirationDate(null);
                                    paymentInfoBean.setPan(pan);
                                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                    paymentInfoBean.setMessage("Invalid shopLoginCheck");
                                    paymentInfoBean.setToken(token);
                                    paymentInfoBean.setBrand(PanHelper.getBrand(token));
                                    paymentInfoBean.setCardBin(cardBin);
                                    paymentInfoBean.setCheckShopTransactionID(shopTransactionID);
                                    paymentInfoBean.setCheckBankTransactionID(bankTransactionID);
                                    paymentInfoBean.setCheckCurrency(currency);
                                    paymentInfoBean.setCheckShopLogin(shopLoginCheckNewFlow);
                                    paymentInfoBean.setDefaultMethod(false);

                                    System.out.println("Invalid shopLoginCheckNewFlow: " + shopLoginCheckNewFlow);

                                    em.merge(paymentInfoBean);

                                    GestPayData deletePagamResult = gpService.deletePagam(new Double(amount), shopTransactionID, shopLoginCheck, currency, bankTransactionID, null, acquirerId, groupAcquirer, encodedSecretKey);

                                    System.out.println("Cancellazione Autorizzazione: " + deletePagamResult.getErrorDescription() + " (" + deletePagamResult.getErrorCode() + ")");

                                    userTransaction.commit();

                                    return "OK";
                                }

                                // Blocco per carte non italiane
                                if (country == null || !country.equals("ITALIA")) {

                                    String pan = token.substring(0, 2) + "XXXXXXXXXX" + token.substring(12, 16);
                                    paymentInfoBean.setExpirationDate(null);
                                    paymentInfoBean.setPan(pan);
                                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                    paymentInfoBean.setMessage("Autorizzazione negata");
                                    paymentInfoBean.setToken(token);
                                    paymentInfoBean.setBrand(PanHelper.getBrand(token));
                                    paymentInfoBean.setCardBin(cardBin);
                                    paymentInfoBean.setCheckShopTransactionID(shopTransactionID);
                                    paymentInfoBean.setCheckBankTransactionID(bankTransactionID);
                                    paymentInfoBean.setCheckCurrency(currency);
                                    paymentInfoBean.setCheckShopLogin(shopLoginCheckNewFlow);
                                    paymentInfoBean.setDefaultMethod(false);

                                    if (country != null) {
                                        System.out.println("Invalid country: " + country);
                                    }
                                    else {
                                        System.out.println("Invalid country: null");
                                    }

                                    em.merge(paymentInfoBean);

                                    GestPayData deletePagamResult = gpService.deletePagam(new Double(amount), shopTransactionID, shopLoginCheck, currency, bankTransactionID, null, acquirerId, groupAcquirer, encodedSecretKey);

                                    System.out.println("Cancellazione Autorizzazione: " + deletePagamResult.getErrorDescription() + " (" + deletePagamResult.getErrorCode() + ")");

                                    userTransaction.commit();

                                    return "OK";
                                }
                                else {
                                    System.out.println("Country rilevata: " + country);
                                }

                                if (checkPaymentMethodExists) {

                                    PaymentInfoBean paymentMethodExist = QueryRepository.findPaymentMethodTokenExists(em, token/* , paymentInfoBean.getUser() */);

                                    if (paymentMethodExist != null) {
                                        String pan = token.substring(0, 2) + "XXXXXXXXXX" + token.substring(12, 16);
                                        paymentInfoBean.setExpirationDate(null);
                                        paymentInfoBean.setPan(pan);
                                        paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                        paymentInfoBean.setMessage("Carta di pagamento in uso");
                                        paymentInfoBean.setToken(token);
                                        paymentInfoBean.setBrand(PanHelper.getBrand(token));
                                        paymentInfoBean.setCardBin(cardBin);
                                        paymentInfoBean.setCheckShopTransactionID(shopTransactionID);
                                        paymentInfoBean.setCheckBankTransactionID(bankTransactionID);
                                        paymentInfoBean.setCheckCurrency(currency);
                                        paymentInfoBean.setCheckShopLogin(shopLoginCheck);
                                        paymentInfoBean.setDefaultMethod(false);

                                        System.out.println("Payment method token (" + paymentInfoBean.getToken() + ") already used from other user ("
                                                + paymentMethodExist.getUser().getId() + ")");

                                        em.merge(paymentInfoBean);

                                        GestPayData deletePagamResult = gpService.deletePagam(new Double(amount), shopTransactionID, shopLoginCheck, currency, bankTransactionID,
                                                null, acquirerId, groupAcquirer, encodedSecretKey);

                                        System.out.println("Cancellazione Autorizzazione: " + deletePagamResult.getErrorDescription() + " (" + deletePagamResult.getErrorCode()
                                                + ")");

                                        userTransaction.commit();

                                        return "OK";
                                    }
                                }

                                if (TDLevel.equals(PaymentInfo.PAYMENTINFO_TD_LEVEL_HALF)) {

                                    System.out.println("3DSECURE NON PRESENTE. PAGAMENTO NON VERIFICATO");

                                    Integer expiryYear = new Integer(tokenExpiryYear) + 2000;
                                    Integer expiryMonth = new Integer(tokenExpiryMonth);

                                    // Associa la carta di credito all'utente
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.set(Calendar.YEAR, expiryYear);
                                    calendar.set(Calendar.MONTH, expiryMonth);
                                    calendar.set(Calendar.DAY_OF_MONTH, 1);
                                    calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
                                    Date creditCardExpirationDate = calendar.getTime();
                                    String pan = token.substring(0, 2) + "XXXXXXXXXX" + token.substring(12, 16);

                                    paymentInfoBean.setExpirationDate(creditCardExpirationDate);
                                    paymentInfoBean.setPan(pan);
                                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED);
                                    paymentInfoBean.setMessage(transactionResult);
                                    paymentInfoBean.setToken(token);
                                    paymentInfoBean.setBrand(PanHelper.getBrand(token));
                                    paymentInfoBean.setCardBin(cardBin);

                                    Double checkAmount = paymentInfoBean.getCheckAmount();

                                    // 2) Si effettua un pagamento dell'importo check amount sul metodo di pagamento associato al paymentInfoBean
                                    if (useNewFlow) {
                                        System.out.println("Utilizzo shopLoginCheck per nuovo flusso");
                                        shopLoginCheck = shopLoginCheckNewFlow;
                                    }

                                    //String checkShopTransactionID = new IdGenerator().generateId(16).substring(0, 32);
                                    paymentInfoBean.setCheckShopTransactionID(shopTransactionID);
                                    paymentInfoBean.setCheckBankTransactionID(bankTransactionID);
                                    paymentInfoBean.setCheckCurrency(currency);
                                    paymentInfoBean.setCheckShopLogin(shopLoginCheck);

                                    /*
                                     * GestPayData callPagamResult = gpService.callPagam( checkAmount, checkShopTransactionID, shopLogin, currency, token);
                                     * if ( !callPagamResult.getTransactionResult().equals("OK") ) {
                                     * 
                                     * // Non � stato possibile autorizzare il check amount
                                     * paymentInfoBean.setExpirationDate(null);
                                     * paymentInfoBean.setPan(null);
                                     * paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                     * paymentInfoBean.setMessage("Unable to authorize check amount");
                                     * paymentInfoBean.setToken(null);
                                     * paymentInfoBean.setDefaultMethod(false);
                                     * }
                                     * else {
                                     */
                                    GestPayData callSettleResult = gpService.callSettle(checkAmount, shopTransactionID, shopLoginCheck, currency, acquirerId, groupAcquirer, encodedSecretKey,
                                            token, authorizationCode, bankTransactionID, null, null, 0.0, 0.0);
                                    if (!callSettleResult.getTransactionResult().equals("OK")) {

                                        // Non � stato possibile movimentare il check amount
                                        paymentInfoBean.setExpirationDate(null);
                                        paymentInfoBean.setPan(null);
                                        paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                        paymentInfoBean.setMessage("Unable to settle check amount");
                                        paymentInfoBean.setToken(null);
                                        paymentInfoBean.setDefaultMethod(false);
                                    }
                                    else {

                                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Settle check amount: "
                                                + checkAmount);

                                        // 3) Si valorizzano il check amount, i tentativi residui e l'insert timestamp in paymentInfoBean e si modifica lo stato in not verified
                                        paymentInfoBean.setCheckAmount(checkAmount);
                                        paymentInfoBean.setAttemptsLeft(pinMaxAttempts);
                                        paymentInfoBean.setInsertTimestamp(new Date());

                                        // 4) Si effettua lo storno del pagamento dopo un intervallo di attesa di quualche secondo
                                        System.out.println("pre-refund start sleep");
                                        try {
                                            Thread.sleep(2000);
                                        }
                                        catch (InterruptedException ex) {
                                            Thread.currentThread().interrupt();
                                        }
                                        System.out.println("pre-refund end sleep");
                                        
                                        GestPayData callRefundResult = gpService.callRefund(checkAmount, shopTransactionID, shopLoginCheck, currency, token, authorizationCode, bankTransactionID, null, acquirerId, groupAcquirer, encodedSecretKey);

                                        System.out.println("Storno Pagamento: " + callRefundResult.getErrorDescription() + " (" + callRefundResult.getErrorCode() + ")");

                                        paymentInfoBean.setDefaultMethod(false);

                                        checkPayment = true;
                                    }
                                }
                                else {

                                    System.out.println("3DSECURE PRESENTE. PAGAMENTO VERIFICATO");

                                    Integer expiryYear = new Integer(tokenExpiryYear) + 2000;
                                    Integer expiryMonth = new Integer(tokenExpiryMonth);

                                    // Associa la carta di credito all'utente
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.set(Calendar.YEAR, expiryYear);
                                    calendar.set(Calendar.MONTH, expiryMonth);
                                    calendar.set(Calendar.DAY_OF_MONTH, 1);
                                    calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
                                    Date creditCardExpirationDate = calendar.getTime();
                                    String pan = token.substring(0, 2) + "XXXXXXXXXX" + token.substring(12, 16);

                                    if (useNewFlow) {
                                        System.out.println("Utilizzo shopLoginCheck per nuovo flusso");
                                        shopLoginCheck = shopLoginCheckNewFlow;
                                    }

                                    paymentInfoBean.setExpirationDate(creditCardExpirationDate);
                                    paymentInfoBean.setPan(pan);
                                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED);
                                    paymentInfoBean.setMessage(transactionResult);
                                    paymentInfoBean.setToken(token);
                                    paymentInfoBean.setBrand(PanHelper.getBrand(token));
                                    paymentInfoBean.setCardBin(cardBin);
                                    paymentInfoBean.setCheckShopTransactionID(shopTransactionID);
                                    paymentInfoBean.setCheckBankTransactionID(bankTransactionID);
                                    paymentInfoBean.setCheckCurrency(currency);
                                    paymentInfoBean.setCheckShopLogin(shopLoginCheck);
                                    paymentInfoBean.setAttemptsLeft(pinMaxAttempts);
                                    paymentInfoBean.setInsertTimestamp(new Date());
                                    paymentInfoBean.setDefaultMethod(false);
                                    checkPayment = true;

                                    GestPayData deletePagamResult = gpService.deletePagam(new Double(amount), shopTransactionID, shopLoginCheck, currency, bankTransactionID, null, "BANCASELLA", groupAcquirer, encodedSecretKey);

                                    System.out.println("Cancellazione Autorizzazione: " + deletePagamResult.getErrorDescription() + " (" + deletePagamResult.getErrorCode() + ")");

                                }

                                if (checkPayment) {

                                    Boolean firstRegistration = userBean.setRegistrationCompleted();

                                    // Se l'utente appartiene alla nuova categoria e ha il flag oldUser a true bisogna impostare oldUser a false
                                    if (useNewFlow && userBean.getOldUser()) {

                                        System.out.println("L'utente ha completato la migrazione da vecchio a nuovo");
                                        userBean.setOldUser(false);
                                    }

                                    PaymentInfoBean defaultPaymentInfoBean = userBean.findDefaultPaymentInfoBean();

                                    boolean defaultMethod = false;
                                    if (defaultPaymentInfoBean == null) {
                                        System.out.println("Nessun metodo di pagamento di default trovato");
                                        defaultMethod = true;
                                    }
                                    else {
                                        System.out.println("Trovato metodo di pagamento di default con id " + defaultPaymentInfoBean.getId());
                                    }

                                    paymentInfoBean.setDefaultMethod(defaultMethod);

                                    if (allowPromotion(userBean)) {
                                        PromotionBean promotionBean = null;
                                        System.out.println("L'utente " + userBean.getPersonalDataBean().getSecurityDataEmail() + " ha accesso alle promozioni");
                                        System.out.println("Inizio controllo promozione di benvenuto");

                                        //if (firstRegistration) {

                                        promotionBean = QueryRepository.findPromotionByCode(em, PromotionType.WELCOME.getPromoCode());

                                        if (promotionBean != null) {

                                            // Verifica che l'utente non abbia gi� usufruito della promozione di benvenuto
                                            Boolean welcomePromoFound = false;

                                            for (VoucherBean voucherBean : userBean.getVoucherList()) {

                                                if (voucherBean.getPromoCode().equals(PromotionType.WELCOME.getVoucherCode())) {

                                                    System.out.println("L'utente ha gi� usufruito della promozione di benvenuto");
                                                    welcomePromoFound = true;
                                                    break;
                                                }
                                            }

                                            if (!welcomePromoFound) {

                                                /*  E' stata cambiata la procedura di assegnazione del voucher promozionale per i nuovi iscritti.
                                                 *  La nuova procedura prevede a prescindere l'inserimento dell'utente nella tabella degli utenti in attesa del voucher promozionale
                                                 *  In seguito il job relativo gestir� tutta la procedura di assegnazione
                                                 */

                                                PromoUsersOnHoldBean promoUsersOnHoldBean = new PromoUsersOnHoldBean();
                                                promoUsersOnHoldBean.setInsertedData(new Date());
                                                promoUsersOnHoldBean.setPromotionBean(promotionBean);
                                                promoUsersOnHoldBean.setUserBean(userBean);
                                                promoUsersOnHoldBean.setStatus(PromotionVoucherStatus.NEW.getValue());
                                                em.persist(promoUsersOnHoldBean);
                                                System.out.println("Utente inserito nella tabella degli utenti in attesa di voucher per promozione di benvenuto");

                                                /*
                                                List<PromoVoucherBean> promoVoucherBeanList = QueryRepository.findPromoVoucherNewByPromotion(em, promotionBean);
                                                boolean addToOnHold = true;
                                                String verificationValue = paymentInfoBean.getToken();

                                                if (promoVoucherBeanList != null && !promoVoucherBeanList.isEmpty()) {
                                                    for (PromoVoucherBean promoVoucherBean : promoVoucherBeanList) {
                                                        String voucherCode = promoVoucherBean.getCode();
                                                        System.out.println("Voucher promozione di benvenuto: " + voucherCode);

                                                        PromoVoucherBean tmpPromoVoucherBean = QueryRepository.findPromoVoucherByUserAndPromotion(em, promotionBean,
                                                                userBean);

                                                        if (tmpPromoVoucherBean != null) {
                                                            System.out.println("Verification Value:" + tmpPromoVoucherBean.getVerificationValue());
                                                            System.out.println("Voucher Code:" + tmpPromoVoucherBean.getCode());
                                                            System.out.println("User Id:" + tmpPromoVoucherBean.getUserBean().getId());
                                                            System.out.println("L'utente ha gi� usufruito della promozione: " + verificationValue);
                                                            addToOnHold = false;
                                                            break;
                                                        }
                                                        else {
                                                            String result = userService.loadPromoVoucher(voucherCode, userBean.getId(), verificationValue);
                                                            System.out.println("loadVoucher: " + result);
                                                            if (result.equals(ResponseHelper.USER_LOAD_VOUCHER_SUCCESS)) {
                                                                addToOnHold = false;
                                                                break;
                                                            }
                                                            else {
                                                                System.out.println("Errore nell'assegnazione del voucher promozione all'utente (" + result + ")");
                                                                if (voucherStatus.containsValue(result)) {
                                                                    promoVoucherBean.setStatus(PromotionVoucherStatus.INVALID.getValue());
                                                                    promoVoucherBean.setUserBean(null);
                                                                    promoVoucherBean.setVerificationValue(null);
                                                                    em.merge(promoVoucherBean);
                                                                    System.out.println("Impostato stato INVALID al voucher promozionale " + voucherCode);
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                else {
                                                    System.out.println("Nessun voucher trovato per la promozione " + PromotionType.WELCOME.getCode() + " di benvenuto");
                                                    PromoVoucherBean tmpPromoVoucherBean = QueryRepository.findPromoVoucherByUserAndVerificationValue(em, promotionBean, userBean,
                                                            verificationValue);

                                                    if (tmpPromoVoucherBean != null) {
                                                        System.out.println("Codice token gi� utilizzato per accedere alla promozione: " + verificationValue);
                                                        addToOnHold = false;
                                                    }
                                                }

                                                if (addToOnHold) {
                                                    PromoUsersOnHoldBean promoUsersOnHoldBean = new PromoUsersOnHoldBean();
                                                    promoUsersOnHoldBean.setInsertedData(new Date());
                                                    promoUsersOnHoldBean.setPromotionBean(promotionBean);
                                                    promoUsersOnHoldBean.setUserBean(userBean);
                                                    promoUsersOnHoldBean.setStatus(PromoUsersOnHoldStatus.NEW.getValue());
                                                    em.persist(promoUsersOnHoldBean);
                                                    System.out.println("Utente inserito nella tabella degli utenti in attesa di voucher per promozione di benvenuto");
                                                }
                                                */
                                            }
                                            else {
                                                System.out.println("Promozione " + PromotionType.WELCOME.getPromoCode() + " gi� utilizzata dall'utente");
                                            }
                                        }
                                        else {
                                            System.out.println("Promozione " + PromotionType.WELCOME.getPromoCode() + " di benvenuto non attiva o scaduta");
                                        }

                                        //}
                                        //else {
                                        //    System.out.println("Promozione " + PromotionType.WELCOME.getCode() + " di benvenuto non valida per l'utente " + userBean.getId());
                                        //}

                                        System.out.println("Inizio controllo promozione Mastercard");
                                        if (paymentInfoBean.getBrand().equals(PanHelper.BRAND_MASTERCARD)) {
                                            System.out.println("Trovata carta Mastercard. Inizio controllo per promozione");
                                            promotionBean = QueryRepository.findPromotionByCode(em, PromotionType.MASTERCARD.getPromoCode());

                                            if (promotionBean != null) {

                                                // Verifica che l'utente non abbia gi� usufruito della promozione di benvenuto
                                                Boolean mastercardPromoFound = false;

                                                for (VoucherBean voucherBean : userBean.getVoucherList()) {

                                                    if (voucherBean.getPromoCode().equals(PromotionType.MASTERCARD.getVoucherCode())) {

                                                        System.out.println("L'utente ha gi� usufruito della promozione Mastercard");
                                                        mastercardPromoFound = true;
                                                        break;
                                                    }
                                                }

                                                if (!mastercardPromoFound) {

                                                    /*  E' stata cambiata la procedura di assegnazione del voucher promozionale per i nuovi iscritti.
                                                     *  La nuova procedura prevede a prescindere l'inserimento dell'utente nella tabella degli utenti in attesa del voucher promozionale
                                                     *  In seguito il job relativo gestir� tutta la procedura di assegnazione
                                                     */

                                                    PromoUsersOnHoldBean promoUsersOnHoldBean = new PromoUsersOnHoldBean();
                                                    promoUsersOnHoldBean.setInsertedData(new Date());
                                                    promoUsersOnHoldBean.setPromotionBean(promotionBean);
                                                    promoUsersOnHoldBean.setUserBean(userBean);
                                                    promoUsersOnHoldBean.setStatus(PromoUsersOnHoldStatus.NEW.getValue());
                                                    em.persist(promoUsersOnHoldBean);
                                                    System.out.println("Utente inserito nella tabella degli utenti in attesa di voucher per promozione Mastercard");

                                                    /*
                                                    List<PromoVoucherBean> promoVoucherBeanList = QueryRepository.findPromoVoucherNewByPromotion(em, promotionBean);
                                                    boolean addToOnHold = true;
                                                    String verificationValue = paymentInfoBean.getToken();

                                                    if (promoVoucherBeanList != null && !promoVoucherBeanList.isEmpty()) {
                                                        for (PromoVoucherBean promoVoucherBean : promoVoucherBeanList) {
                                                            String voucherCode = promoVoucherBean.getCode();
                                                            System.out.println("Voucher promozione Mastercard: " + voucherCode);

                                                            PromoVoucherBean tmpPromoVoucherBean = QueryRepository.findPromoVoucherByUserAndPromotion(em, promotionBean,
                                                                    userBean);

                                                            if (tmpPromoVoucherBean != null) {
                                                                System.out.println("Verification Value:" + tmpPromoVoucherBean.getVerificationValue());
                                                                System.out.println("Voucher Code:" + tmpPromoVoucherBean.getCode());
                                                                System.out.println("User Id:" + tmpPromoVoucherBean.getUserBean().getId());
                                                                System.out.println("L'utente ha gi� usufruito della promozione: " + verificationValue);
                                                                addToOnHold = false;
                                                                break;
                                                            }
                                                            else {
                                                                String result = userService.loadPromoVoucher(voucherCode, userBean.getId(), verificationValue);
                                                                System.out.println("loadVoucher: " + result);
                                                                if (result.equals(ResponseHelper.USER_LOAD_VOUCHER_SUCCESS)) {
                                                                    addToOnHold = false;
                                                                    break;
                                                                }
                                                                else {
                                                                    System.out.println("Errore nell'assegnazione del voucher promozione all'utente (" + result + ")");
                                                                    if (voucherStatus.containsValue(result)) {
                                                                        promoVoucherBean.setStatus(PromotionVoucherStatus.INVALID.getValue());
                                                                        promoVoucherBean.setUserBean(null);
                                                                        promoVoucherBean.setVerificationValue(null);
                                                                        em.merge(promoVoucherBean);
                                                                        System.out.println("Impostato stato INVALID al voucher promozionale " + voucherCode);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                    else {
                                                        System.out.println("Nessun voucher trovato per la promozione " + PromotionType.MASTERCARD.getCode() + " Mastercard");
                                                        PromoVoucherBean tmpPromoVoucherBean = QueryRepository.findPromoVoucherByUserAndVerificationValue(em, promotionBean, userBean,
                                                                verificationValue);

                                                        if (tmpPromoVoucherBean != null) {
                                                            System.out.println("Codice token gi� utilizzato per accedere alla promozione: " + verificationValue);
                                                            addToOnHold = false;
                                                        }
                                                    }

                                                    if (addToOnHold) {
                                                        PromoUsersOnHoldBean promoUsersOnHoldBean = new PromoUsersOnHoldBean();
                                                        promoUsersOnHoldBean.setInsertedData(new Date());
                                                        promoUsersOnHoldBean.setPromotionBean(promotionBean);
                                                        promoUsersOnHoldBean.setUserBean(userBean);
                                                        promoUsersOnHoldBean.setStatus(PromoUsersOnHoldStatus.NEW.getValue());
                                                        em.persist(promoUsersOnHoldBean);
                                                        System.out.println("Utente inserito nella tabella degli utenti in attesa di voucher per promozione Mastercard");
                                                    }
                                                    */
                                                }
                                                else {
                                                    System.out.println("Promozione " + PromotionType.MASTERCARD.getPromoCode() + " gi� utilizzata dall'utente");
                                                }
                                            }
                                            else {
                                                System.out.println("Promozione " + PromotionType.MASTERCARD.getPromoCode() + " Mastercard non attiva o scaduta");
                                            }
                                        }
                                        else {
                                            System.out.println("Nessuna carta Mastercard trovata");
                                        }

                                    }
                                    else {
                                        System.out.println("L'utente " + userBean.getPersonalDataBean().getSecurityDataEmail() + " non ha accesso alle promozioni");
                                    }

                                    if (firstRegistration) {
                                        em.merge(userBean);

                                        //System.out.println("firstRegistration");

                                        //sendEmail(emailSender, userBean, shopTransactionID);
                                    }
                                }
                            }
                            else {

                                paymentInfoBean.setExpirationDate(null);
                                paymentInfoBean.setPan(null);
                                paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                paymentInfoBean.setMessage(errorDescription);
                                paymentInfoBean.setToken(null);
                                paymentInfoBean.setDefaultMethod(false);

                            }

                            System.out.println("Pagamento di default: " + paymentInfoBean.getDefaultMethod());
                            // Salva l'oggetto paymentInfoBean su db
                            em.merge(paymentInfoBean);

                            // Aggiorna lo stato l'oggetto shopTransactionDataBean e salvalo su db
                            shopTransactionDataBean.setStatusToUsed();
                            em.merge(shopTransactionDataBean);
                        }
                    }
                }
            }

            userTransaction.commit();

            return "OK";

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user update user payment data with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, message);

            throw new EJBException(ex2);
        }
    }

    private boolean allowPromotion(UserBean userBean) {
        /*File fileAllowPromotion = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "promotion" + File.separator + "allow");
        boolean allow = false;

        if (fileAllowPromotion.exists() && fileAllowPromotion.length() == 0) {
            allow = true;
        }
        else {
            try {
                FileInputStream input = new FileInputStream(fileAllowPromotion);

                InputStreamReader is = new InputStreamReader(input);
                BufferedReader br = new BufferedReader(is);
                String read;

                while ((read = br.readLine()) != null) {
                    System.out.println("Utente Autorizzato: " + read + " -- Utente: " + userBean.getPersonalDataBean().getSecurityDataEmail());
                    if (read.equals(userBean.getPersonalDataBean().getSecurityDataEmail())) {
                        allow = true;
                        break;
                    }
                }

                br.close();
            }
            catch (IOException ex) {
                allow = false;
            }
        }

        return allow;*/
        return true;
    }
}
