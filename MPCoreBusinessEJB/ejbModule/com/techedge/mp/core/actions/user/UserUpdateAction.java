package com.techedge.mp.core.actions.user;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.Address;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.AddressBean;
import com.techedge.mp.core.business.model.PersonalDataBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserUpdateAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserUpdateAction() {
    }
    
    
    public String execute(
    		String ticketId,
    		String requestId,
    		User user) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		// Verifica il ticket
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_UPDATE_INVALID_TICKET;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if (userBean == null) {
    			
    			// L'utente non esiste
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");
				
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_UPDATE_INVALID_TICKET;
    		}
    		
    		// Verifica lo stato dell'utente
    		Integer userStatus = userBean.getUserStatus();
    		if ( userStatus != User.USER_STATUS_VERIFIED ) {
    			
    			// Un utente che si trova in questo stato non pu� invocare questo servizio
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update user in status " + userStatus);
    			
    			userTransaction.commit();
    			
    			return ResponseHelper.USER_UPDATE_UNAUTHORIZED;
    		}
    		
    		
    		// Verifica la validit� dei dati
    		// TODO
    		PersonalDataBean personalDataBean = userBean.getPersonalDataBean();
    		
    		if ( user.getPersonalData().getFirstName() != null )
    			personalDataBean.setFirstName(user.getPersonalData().getFirstName());
    		
    		if ( user.getPersonalData().getLastName() != null )
    			personalDataBean.setLastName(user.getPersonalData().getLastName());
    		
    		if ( user.getPersonalData().getFiscalCode() != null )
    			personalDataBean.setFiscalCode(user.getPersonalData().getFiscalCode());
    		
    		if ( user.getPersonalData().getBirthDate()!=null )
    			personalDataBean.setBirthDate(user.getPersonalData().getBirthDate());
    		
    		if ( user.getPersonalData().getBirthMunicipality() != null )
    			personalDataBean.setBirthMunicipality(user.getPersonalData().getBirthMunicipality());
    		
    		if ( user.getPersonalData().getBirthProvince() != null )
    			personalDataBean.setBirthProvince(user.getPersonalData().getBirthProvince());
    		
    		if ( user.getPersonalData().getLanguage() != null )
    			personalDataBean.setLanguage(user.getPersonalData().getLanguage());
    		
    		if ( user.getPersonalData().getSex() != null )
    			personalDataBean.setSex(user.getPersonalData().getSex());
    		
//    		if ( user.getPersonalData().getSecurityDataEmail() != null )
//    			personalDataBean.setSecurityDataEmail(user.getPersonalData().getSecurityDataEmail());

    		if ( user.getPersonalData().getAddress() != null ){
    			
    			//AddressBean addressBean_obj;// = new AddressBean(user.getPersonalData().getAddress());
    			//addressBean_obj.setId(id_address);
    			Address adr = user.getPersonalData().getAddress();
    			AddressBean addressBean_obj = userBean.getPersonalDataBean().getAddress();
    			if(addressBean_obj != null){
//    				System.out.println("1a: "+addressBean_obj );
    			addressBean_obj.setCity(adr.getCity());
    			addressBean_obj.setCountryCodeId(adr.getCountryCodeId());
    			addressBean_obj.setCountryCodeName(adr.getCountryCodeName());
    			addressBean_obj.setHouseNumber(adr.getHouseNumber());
    			addressBean_obj.setRegion(adr.getRegion());
    			addressBean_obj.setStreet(adr.getStreet());
    			addressBean_obj.setZipcode(adr.getZipcode());
    			}else{
//    				System.out.println("2a: "+addressBean_obj);
    				addressBean_obj = new AddressBean(user.getPersonalData().getAddress());
    				personalDataBean.setAddress(addressBean_obj);
    			}
    		//	personalDataBean.setAddress(addressBean_obj);
    		//	personalDataBean.setAddress(new AddressBean(user.getPersonalData().getAddress()));
    		}
    		
    		if ( user.getPersonalData().getBillingAddress() != null ){
//    			long id_billingaddress = userBean.getPersonalDataBean().getBillingAddress().getId();
//    			AddressBean billaddressBean_obj = new AddressBean(user.getPersonalData().getBillingAddress());
    			Address billadr = user.getPersonalData().getBillingAddress();
    			AddressBean billaddressBean_obj = userBean.getPersonalDataBean().getBillingAddress();
    			if (billaddressBean_obj != null){
//    				System.out.println("1b: "+ billaddressBean_obj);
    			 billaddressBean_obj.setCity(billadr.getCity());
    			 billaddressBean_obj.setCountryCodeId(billadr.getCountryCodeId());
    			 billaddressBean_obj.setCountryCodeName(billadr.getCountryCodeName());
    			 billaddressBean_obj.setHouseNumber(billadr.getHouseNumber());
    			 billaddressBean_obj.setRegion(billadr.getRegion());
    			 billaddressBean_obj.setStreet(billadr.getStreet());
    			 billaddressBean_obj.setZipcode(billadr.getZipcode());
    			 } else{
//    				 System.out.println("2b");
    				 billaddressBean_obj = new AddressBean(user.getPersonalData().getBillingAddress());
//    				 System.out.println("2b:" + billaddressBean_obj);
    				 personalDataBean.setBillingAddress(billaddressBean_obj);
    			 }
//    			billaddressBean_obj.setId(id_billingaddress);
//    			personalDataBean.setBillingAddress(billaddressBean_obj);
    		}
    		
    		if ( user.getContactDataMobilephone() != null )
    			userBean.setContactDataMobilephone(user.getContactDataMobilephone());
    		
    		
    		// Aggiorna i dati dell'utente
    		em.merge(userBean);
    		
    		// Rinnova il ticket
    		ticketBean.renew();
    		em.merge(ticketBean);
    		
    		userTransaction.commit();
    		
    		return ResponseHelper.USER_UPDATE_SUCCESS;
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED user update with message (" + ex2.getMessage() + ")";
    		this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
