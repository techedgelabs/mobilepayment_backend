package com.techedge.mp.core.actions.user;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserCancelMobilePhoneUpdateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserCancelMobilePhoneUpdateAction() {}

    public String execute(String ticketId, String requestId, Long id) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                return ResponseHelper.USER_PIN_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_PIN_INVALID_TICKET;
            }

            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to cancel mobile phone update in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_PIN_INVALID_TICKET;
            }

            MobilePhoneBean mobilePhoneBean = QueryRepository.findMobilePhoneById(em, id);

            if (mobilePhoneBean == null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone not found");
                userTransaction.commit();
                return ResponseHelper.MOBILE_PHONE_CANCEL_CHECK_FAILURE;
            }

            if (mobilePhoneBean.getUser() != userBean) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone not valid for this user");
                userTransaction.commit();
                return ResponseHelper.MOBILE_PHONE_CANCEL_CHECK_FAILURE;
            }

            if (mobilePhoneBean.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_PENDING)) {

                mobilePhoneBean.setStatus(MobilePhone.MOBILE_PHONE_STATUS_CANCELED);
                // Aggiorna il numero di telefono
                em.merge(mobilePhoneBean);

                // Rinnova il ticket
                ticketBean.renew();
                em.merge(ticketBean);

                userTransaction.commit();
                return ResponseHelper.MOBILE_PHONE_CANCEL_SUCCESS;
            }
            else {
                userTransaction.commit();
                return ResponseHelper.MOBILE_PHONE_CANCEL_FAILURE;
            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED cancel mobile phone with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
