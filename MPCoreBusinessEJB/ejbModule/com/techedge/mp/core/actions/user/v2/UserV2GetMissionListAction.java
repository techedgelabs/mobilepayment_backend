package com.techedge.mp.core.actions.user.v2;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AppLink;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MissionDetailDataResponse;
import com.techedge.mp.core.business.interfaces.MissionInfoDataDetail;
import com.techedge.mp.core.business.interfaces.ParameterNotification;
import com.techedge.mp.core.business.interfaces.PromotionInfoDataDetail;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.GetMissionsResponse;
import com.techedge.mp.core.business.interfaces.crm.Offer;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crm.CRMPromotionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2GetMissionListAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2GetMissionListAction() {}

    public MissionDetailDataResponse execute(String ticketId, String requestId, CRMServiceRemote crmService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        MissionDetailDataResponse missionDetailDataResponse = new MissionDetailDataResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                missionDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_MISSION_LIST_INVALID_TICKET);

                userTransaction.commit();

                return missionDetailDataResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                missionDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_MISSION_LIST_INVALID_TICKET);

                userTransaction.commit();

                return missionDetailDataResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to retrieve mission list, user  in status "
                        + userStatus);

                missionDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_MISSION_LIST_UNAUTHORIZED);

                userTransaction.commit();

                return missionDetailDataResponse;
            }

            // Recupera la lista delle promozioni dal database
            List<CRMPromotionBean> crmPromotionBeanList = QueryRepository.findAllCrmPromotions(em);

            for (CRMPromotionBean crmPromotionBean : crmPromotionBeanList) {

                PromotionInfoDataDetail promotionInfoDataDetail = new PromotionInfoDataDetail();

                AppLink appLinkPromotion = new AppLink();
                appLinkPromotion.setType(PushNotificationContentType.INTERNAL);
                appLinkPromotion.setLocation(crmPromotionBean.getLink());

                ParameterNotification parameterNotificationC2 = new ParameterNotification("C_2", crmPromotionBean.getC_2());
                ParameterNotification parameterNotificationC3 = new ParameterNotification("C_3", crmPromotionBean.getC_3());
                ParameterNotification parameterNotificationC4 = new ParameterNotification("C_4", crmPromotionBean.getC_4());
                ParameterNotification parameterNotificationC5 = new ParameterNotification("C_5", crmPromotionBean.getC_5());
                ParameterNotification parameterNotificationC6 = new ParameterNotification("C_6", crmPromotionBean.getC_6());
                ParameterNotification parameterNotificationC7 = new ParameterNotification("C_7", crmPromotionBean.getC_7());
                ParameterNotification parameterNotificationC8 = new ParameterNotification("C_8", crmPromotionBean.getC_8());
                ParameterNotification parameterNotificationC9 = new ParameterNotification("C_9", crmPromotionBean.getC_9());

                appLinkPromotion.getParameters().add(parameterNotificationC2);
                appLinkPromotion.getParameters().add(parameterNotificationC3);
                appLinkPromotion.getParameters().add(parameterNotificationC4);
                appLinkPromotion.getParameters().add(parameterNotificationC5);
                appLinkPromotion.getParameters().add(parameterNotificationC6);
                appLinkPromotion.getParameters().add(parameterNotificationC7);
                appLinkPromotion.getParameters().add(parameterNotificationC8);
                appLinkPromotion.getParameters().add(parameterNotificationC9);

                promotionInfoDataDetail.setAppLinkInfo(appLinkPromotion);
                promotionInfoDataDetail.setCode(crmPromotionBean.getCode());
                promotionInfoDataDetail.setDescription(crmPromotionBean.getDescription());
                promotionInfoDataDetail.setImageUrl(crmPromotionBean.getImageUrl());
                promotionInfoDataDetail.setName(crmPromotionBean.getName());
                promotionInfoDataDetail.setStartDate(crmPromotionBean.getStartDate());
                promotionInfoDataDetail.setEndDate(crmPromotionBean.getEndDate());

                missionDetailDataResponse.getPromotionList().add(promotionInfoDataDetail);
            }

            // Recupera la lista delle missioni dal CRM

            GetMissionsResponse getMissionResponse = new GetMissionsResponse();

            getMissionResponse = crmService.getMissions(ticketId, ticketId);

            List<Offer> offerList = getMissionResponse.getOffersList();

            if (getMissionResponse.getStatusCode().equals(ResponseHelper.CRM_GET_MISSIONS_SUCCESS)) {
               
                for (Offer offer : getMissionResponse.getOffersList()) {

                    MissionInfoDataDetail missionInfoDataDetail = new MissionInfoDataDetail();

                    missionInfoDataDetail.setAppLink(new AppLink());

                    HashMap<String, Object> offerParams = offer.getAdditionalAttributes();
                    if (offer.getDescription() != null) {
                        missionInfoDataDetail.setDescription(offer.getDescription());
                    }
                    for (String key : offerParams.keySet()) {
                        Object value = offerParams.get(key);
                        String valueToString = value != null ? value.toString() : "";
                        if (valueToString.equals("n/a")) {
                            value = "";
                        }
                        if (key.matches("^C_[0-9]+$")) {
                            missionInfoDataDetail.getAppLink().getParameters().add(new ParameterNotification(key, valueToString));
                        }
                        if (key.equals("UrlApp") && !valueToString.isEmpty()) {
                            missionInfoDataDetail.getAppLink().setLocation(valueToString.trim());
                            missionInfoDataDetail.getAppLink().setType(PushNotificationContentType.INTERNAL);
                        }
                        else if (key.equals("textAPP") && !valueToString.isEmpty()) {
                            missionInfoDataDetail.setDescription(valueToString);
                        }
                        else if (key.equals("ExpirationDate") && !valueToString.isEmpty()) {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                            if (value.getClass().equals(Date.class)) {
                                missionInfoDataDetail.setEndDate((Date) value);
                            }
                            else {
                                try {
                                    Date startDate = sdf.parse((String) value);
                                    missionInfoDataDetail.setStartDate(startDate);
                                }
                                catch (ParseException e) {
                                    System.err.println("Errore nel parsing della data: " + e.getMessage());
                                    missionInfoDataDetail.setStartDate(null);
                                }
                            }

                        }
                        else if (key.equals("TipoMissione") && !valueToString.isEmpty()) {

                            if (valueToString.equalsIgnoreCase("a step")) {
                                missionInfoDataDetail.setType("step");
                            }
                            else if (valueToString.equalsIgnoreCase("temporizzata")) {
                                missionInfoDataDetail.setType("timed");
                            }
                        }
                        else if (key.equals("NumeroOperazioniEseguite") && !valueToString.isEmpty()) {
                            Integer valueInteger = new BigDecimal(valueToString).intValue();
                            missionInfoDataDetail.setStepCompleted(valueInteger);
                        }
                        else if (key.equals("NumeroOperazioniObiettivo") && !valueToString.isEmpty()) {
                            Integer valueInteger = new BigDecimal(valueToString).intValue();
                            missionInfoDataDetail.setStepObjective(valueInteger);
                        }
                    }

                    missionInfoDataDetail.setName(offer.getOfferName());
                    missionInfoDataDetail.setCode(offer.getOfferCodeToString("-"));

                    if (missionInfoDataDetail.getType() != null) {
                        
                        if (missionInfoDataDetail.getType().equalsIgnoreCase("step")) {
                            //If stepCompleted >= stepObjective ---> Completed Mission
                            if (missionInfoDataDetail.getStepCompleted() >= missionInfoDataDetail.getStepObjective()) {
                                missionDetailDataResponse.getCompletedMissionDetailList().add(missionInfoDataDetail);
                            }
                            else {
                                missionDetailDataResponse.getMissionDetailList().add(missionInfoDataDetail);
                            }
                        }
                        else if (missionInfoDataDetail.getType().equalsIgnoreCase("timed")) {
                            Date now = new Date();
                            
                          //If endDate <= now ---> Completed Mission
                            if (missionInfoDataDetail.getEndDate().before(now)) {
                                missionDetailDataResponse.getCompletedMissionDetailList().add(missionInfoDataDetail);
                            }
                            else {
                                missionDetailDataResponse.getMissionDetailList().add(missionInfoDataDetail);
                            }
    
                        }
                    }

                }

            }

//            Date startDate = new Date(System.currentTimeMillis());
//            
//            MissionInfoDataDetail missionInfoElement1 = new MissionInfoDataDetail();
//            MissionInfoDataDetail missionInfoElement2 = new MissionInfoDataDetail();
//            
//            MissionInfoDataDetail completedMissions1 = new MissionInfoDataDetail();
//            
//            AppLink appLinkPromotion = new AppLink();
//            appLinkPromotion.setType(PushNotificationContentType.INTERNAL);
//            appLinkPromotion.setLocation("http://5.10.69.210/ENI_VoucherCinema/index.php");
//            ParameterNotification parameterNotificationPromo01 = new ParameterNotification("C_5", "8My8Y030fK");
//            appLinkPromotion.getParameters().add(parameterNotificationPromo01);
//            ParameterNotification parameterNotificationPromo02 = new ParameterNotification("C_7", "n/a");
//            appLinkPromotion.getParameters().add(parameterNotificationPromo02);
//           
//            
//            AppLink appLinkMission01 = new AppLink();
//            appLinkMission01.setType(PushNotificationContentType.INTERNAL);
//            appLinkMission01.setLocation("http://5.10.69.210/ENI_VoucherCinema/index.php");
//            ParameterNotification parameterNotification01 = new ParameterNotification("C_5", "8My8Y030fK");
//            appLinkMission01.getParameters().add(parameterNotification01);
//            
//            missionInfoElement1.setAppLinkInfo(appLinkMission01);
//            missionInfoElement1.setCode("Codice 1");
//            missionInfoElement1.setDescription("Descrizione 1");
//            missionInfoElement1.setName("Voucher Cinema 2x1");
//            missionInfoElement1.setType("step");
//            missionInfoElement1.setStepCompleted(1);
//            missionInfoElement1.setStepObjective(4);
//            missionInfoElement1.setStartDate(startDate);
//            missionInfoElement1.setEndDate(startDate);
//            
//            missionDetailDataResponse.getMissionDetailList().add(missionInfoElement1);
//            
//            
//            AppLink appLinkMission02 = new AppLink();
//            appLinkMission02.setType(PushNotificationContentType.INTERNAL);
//            appLinkMission02.setLocation("http://5.10.69.210/ENI_VoucherCinema/index.php");
//            ParameterNotification parameterNotification02 = new ParameterNotification("C_5", "8My8Y030fK");
//            appLinkMission02.getParameters().add(parameterNotification02);
//            
//            missionInfoElement2.setAppLinkInfo(appLinkMission02);
//            missionInfoElement2.setCode("000001475");
//            missionInfoElement2.setDescription("Descrizione 2");
//            missionInfoElement2.setName("5� di sconto pagando con Eni Pay");
//            missionInfoElement2.setType("timed");
//            missionInfoElement2.setStepCompleted(4);
//            missionInfoElement2.setStepObjective(8);
//            missionInfoElement2.setStartDate(startDate);
//            missionInfoElement2.setEndDate(startDate);
//            
//            missionDetailDataResponse.getMissionDetailList().add(missionInfoElement2);
//            
//            
//            AppLink appLinkCompletedMission01 = new AppLink();
//            appLinkCompletedMission01.setType(PushNotificationContentType.INTERNAL);
//            appLinkCompletedMission01.setLocation("http://5.10.69.210/ENI_VoucherCinema/index.php");
//            ParameterNotification parameterNotification03 = new ParameterNotification("C_5", "8My8Y030fK");
//            appLinkCompletedMission01.getParameters().add(parameterNotification03);
//            
//            completedMissions1.setAppLinkInfo(appLinkCompletedMission01);
//            completedMissions1.setCode("000001475");
//            completedMissions1.setDescription("Descrizione completed mission 1");
//            completedMissions1.setName("5� di sconto pagando con Eni Pay");
//            completedMissions1.setType("timed");
//            completedMissions1.setStepCompleted(4);
//            completedMissions1.setStepObjective(8);
//            completedMissions1.setStartDate(startDate);
//            completedMissions1.setEndDate(startDate);
//            
//            missionDetailDataResponse.getCompletedMissionDetailList().add(completedMissions1);

            missionDetailDataResponse.setStatusCode(ResponseHelper.USER_V2_GET_MISSION_LIST_SUCCESS);

            userTransaction.commit();
            return missionDetailDataResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get Mission List Action  with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
