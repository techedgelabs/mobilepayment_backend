package com.techedge.mp.core.actions.user;

import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserRefundAvailableCapAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public UserRefundAvailableCapAction() {
    }
    
    
    public String execute(String shopTransactionID) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
		try {
    		userTransaction.begin();
    		
    		// Ricerca la transazione associata allo shopTransactionId
    		TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, shopTransactionID);
    		
    		if ( transactionBean == null ) {
    			
    			// La transazione non esiste
    			this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Transaction not found");
    			
    			userTransaction.commit();
    			
    			return ResponseHelper.REFUND_AVAILABLE_CAP_TRANSACTION_NOT_FOUND;
    		}
    		
    		UserBean userBean = transactionBean.getUserBean();
    		if ( userBean == null ) {
    			
    			// Non esiste nessun utente associato alla transazione
    			this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "User not found");
    			
    			userTransaction.commit();
    			
    			return ResponseHelper.REFUND_AVAILABLE_CAP_USER_NOT_FOUND;
    		}
    		
    		
    		// Si ricavano il methodId e il methodType dalla transazione
    		Long methodId = transactionBean.getPaymentMethodId();
    		String methodType = transactionBean.getPaymentMethodType();
    		
    		PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(methodId, methodType);
    		if ( paymentInfoBean != null && paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED ) {
    			
    			if ( paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED ) {
    				
    				// Lo stato del metodo di pagamento selezionato � gi� stato validato
        			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Payment method status valid: no refund needed");
        			
        			userTransaction.commit();
        			
        			return ResponseHelper.REFUND_AVAILABLE_CAP_SUCCESS;
    			}
    			else {
    				
    				// Lo stato del metodo di pagamento selezionato non risulta valido
        			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Payment method status not valid: " + paymentInfoBean.getStatus());
        			
        			userTransaction.commit();
        			
        			return ResponseHelper.REFUND_AVAILABLE_CAP_INVALID_PAYMENT_STATUS;
    			}
    		}
    		else {
    			
				// Il metodo di pagamento selezionato non � ancora stato verificato
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Payment method is not verified");
    			
    			// Verifica lo stato della transazione
        		Integer lastSequenceID = 0;
    			TransactionStatusBean lastTransactionStatusBean = null;
    			Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
    			for( TransactionStatusBean transactionStatusBean : transactionStatusData ) {
    				Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
    				if ( lastSequenceID < transactionStatusBeanSequenceID ) {
    					lastSequenceID = transactionStatusBeanSequenceID;
    					lastTransactionStatusBean = transactionStatusBean;
    				}
    			}
    			
        		if ( !lastTransactionStatusBean.getStatus().equals(StatusHelper.STATUS_END_REFUEL_RECEIVED) 
        				&& !lastTransactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED)
        				&& !lastTransactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED)) {
        			
        			// La transazione non si trova in uno stato valido
        			this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Transaction status not valid: " + lastTransactionStatusBean.getStatus());
        			
        			userTransaction.commit();
        			
        			return ResponseHelper.REFUND_AVAILABLE_CAP_WRONG_TRANSACTION_STATUS;
        		}
	        		
	        	
        		Double refundAmount = transactionBean.getInitialAmount();
        		
        		if ( refundAmount > 0 ) {
        			
        			Double availableCap = userBean.getCapAvailable();
        			availableCap = availableCap + refundAmount;
        			userBean.setCapAvailable(availableCap);
        			
        			em.merge(userBean);
        		}
        		
        		userTransaction.commit();
        		
        		
        	    return ResponseHelper.REFUND_AVAILABLE_CAP_SUCCESS;
    		}
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED refund available cap with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
