package com.techedge.mp.core.actions.user.v2;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PartnerActionUrlResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.dwh.DWHPartnerDetailBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2GetUrlAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2GetUrlAction() {}

    public PartnerActionUrlResponse execute(String ticketId, String requestId) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        PartnerActionUrlResponse partnerActionUrlResponse = new PartnerActionUrlResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid()) { // || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                partnerActionUrlResponse.setStatusCode(ResponseHelper.USER_V2_GET_PARTNER_ACTION_URL_INVALID_TICKET);
                return partnerActionUrlResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                partnerActionUrlResponse.setStatusCode(ResponseHelper.USER_V2_GET_PARTNER_ACTION_URL_NOTFOUND);
                userTransaction.commit();
                return partnerActionUrlResponse;
            }
            
         // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update user password in status " + userStatus);

                partnerActionUrlResponse.setStatusCode(ResponseHelper.USER_V2_GET_PARTNER_ACTION_URL_UNAUTHORIZED);
                
                userTransaction.commit();

                return partnerActionUrlResponse;
            }
            
            List<DWHPartnerDetailBean> dwhPrtnerDetailList = QueryRepository.findAllDwhPartnerDetail(em);

            if (dwhPrtnerDetailList == null || dwhPrtnerDetailList.isEmpty()) {

                // Partner list vuota
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Partner url not found !");

                userTransaction.commit();

                partnerActionUrlResponse.setStatusCode(ResponseHelper.USER_V2_GET_PARTNER_ACTION_URL_NOTFOUND);
                return partnerActionUrlResponse;
            }

            else {

                    partnerActionUrlResponse.setPartnerActionUrl("static url www.google.com");
                    partnerActionUrlResponse.setStatusCode(ResponseHelper.USER_V2_GET_PARTNER_ACTION_URL_SUCCESS);
                }

            userTransaction.commit();
            return partnerActionUrlResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get landing  with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
