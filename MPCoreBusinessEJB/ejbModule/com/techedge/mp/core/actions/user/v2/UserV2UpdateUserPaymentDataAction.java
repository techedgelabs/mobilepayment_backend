package com.techedge.mp.core.actions.user.v2;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.CRMSfNotifyEvent;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.interfaces.user.UserTypeEnum;
import com.techedge.mp.core.business.model.CardDepositTransactionBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.ShopTransactionDataBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.PanHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.crm.EVENT_TYPE;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2UpdateUserPaymentDataAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    @EJB
    private UserService   userService;
    
    @EJB
    private CRMService   crmService;
    
    @EJB
    private ParametersService parametersService;
    
    private final String 				PARAM_CRM_SF_ACTIVE="CRM_SF_ACTIVE";
    

//    @SuppressWarnings("serial")
//    private HashMap<String, String> voucherStatus = new HashMap<String, String>() {
//                                                      {
//                                                          put(Voucher.VOUCHER_STATUS_ANNULLATO, ResponseHelper.USER_LOAD_VOUCHER_CANCELED);
//                                                          put(Voucher.VOUCHER_STATUS_CANCELLATO, ResponseHelper.USER_LOAD_VOUCHER_REMOVED);
//                                                          put(Voucher.VOUCHER_STATUS_DA_CONFERMARE, ResponseHelper.USER_LOAD_VOUCHER_TO_CONFIRM);
//                                                          put(Voucher.VOUCHER_STATUS_ESAURITO, ResponseHelper.USER_LOAD_VOUCHER_SPENT);
//                                                          put(Voucher.VOUCHER_STATUS_INESISTENTE, ResponseHelper.USER_LOAD_VOUCHER_INEXISTENT);
//                                                          put(Voucher.VOUCHER_STATUS_SCADUTO, ResponseHelper.USER_LOAD_VOUCHER_EXPIRED);
//                                                          put(Voucher.VOUCHER_STATUS_VALIDO, ResponseHelper.USER_LOAD_VOUCHER_VALID);
//                                                      }
//                                                  };

    public UserV2UpdateUserPaymentDataAction() {}

    public String execute(String transactionType, String transactionResult, String shopTransactionID, String bankTransactionID, String authorizationCode, String currency,
            String amount, String country, String buyerName, String buyerEmail, String errorCode, String errorDescription, String alertCode, String alertDescription,
            String TransactionKey, String token, String tokenExpiryMonth, String tokenExpiryYear, String cardBin, String TDLevel, String pan, String brand, Integer pinMaxAttempts,
            String shopLoginCheckNewAcquirer, String uicCodeRefundCartaSi, boolean checkPaymentMethodExists, String acquirerIdCartaSi,
            String groupAcquirerCartaSi, String encodedSecretKeyCartaSi, EmailSenderRemote emailSender, UserCategoryService userCategoryService, String hash) throws EJBException {
        
        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            GPServiceRemote gpService = EJBHomeCache.getInstance().getGpService();

            if (gpService == null) {

                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Payment Service unavailable");
            }
            else {

                // Memorizzazione su db della richiesta di deposito carta
                CardDepositTransactionBean cardDepositTransactionBean = new CardDepositTransactionBean();

                cardDepositTransactionBean.setCreationTimestamp(new Date());
                cardDepositTransactionBean.setTransactionType(transactionType);
                cardDepositTransactionBean.setTransactionResult(transactionResult);
                cardDepositTransactionBean.setShopTransactionID(shopTransactionID);
                cardDepositTransactionBean.setBankTransactionID(bankTransactionID);
                cardDepositTransactionBean.setAuthorizationCode(authorizationCode);
                cardDepositTransactionBean.setCurrency(currency);
                cardDepositTransactionBean.setAmount(amount);
                cardDepositTransactionBean.setCountry(country);
                cardDepositTransactionBean.setBuyerName(buyerName);
                cardDepositTransactionBean.setBuyerEmail(buyerEmail);
                cardDepositTransactionBean.setErrorCode(errorCode);
                cardDepositTransactionBean.setErrorDescription(errorDescription);
                cardDepositTransactionBean.setAlertCode(alertCode);
                cardDepositTransactionBean.setAlertDescription(alertDescription);
                cardDepositTransactionBean.setTransactionKey(TransactionKey);
                cardDepositTransactionBean.setToken(token);
                cardDepositTransactionBean.setTokenExpiryMonth(tokenExpiryMonth);
                cardDepositTransactionBean.setTokenExpiryYear(tokenExpiryYear);
                cardDepositTransactionBean.setCardBIN(cardBin);
                cardDepositTransactionBean.setTdLevel(TDLevel);
                cardDepositTransactionBean.setHash(hash);

                em.persist(cardDepositTransactionBean);

                // Ricerca dell'oggetto con lo shopTransactionId restituito
                ShopTransactionDataBean shopTransactionDataBean = QueryRepository.findShopTransactionBeanDataByTransactionIdAndStatus(em, shopTransactionID,
                        ShopTransactionDataBean.STATUS_NEW);

                if (shopTransactionDataBean == null) {

                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "No deposit card process for shopTransactionId");
                }
                else {

                    PaymentInfoBean paymentInfoBean = shopTransactionDataBean.getPaymentInfo();

                    if (paymentInfoBean == null) {

                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "No paymentData for shopTransactionId");
                    }
                    else {

                        if (paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_PENDING) {

                            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null,
                                    "No pending paymentData for shopTransactionId");
                        }
                        else {

                            if (transactionResult.equals("OK")) {

                                boolean checkPayment = false;

                                UserBean userBean = paymentInfoBean.getUser();
                                Integer userType = userBean.getUserType();
                                
                                Boolean useNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
                                Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());

                                currency = uicCodeRefundCartaSi;
                                
                                if (!useNewAcquirerFlow && !useBusiness) {

                                    paymentInfoBean.setExpirationDate(null);
                                    paymentInfoBean.setPan(pan);
                                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                    paymentInfoBean.setMessage("Invalid shopLoginCheck");
                                    paymentInfoBean.setToken(token);
                                    paymentInfoBean.setBrand(PanHelper.generateBrand(brand));
                                    paymentInfoBean.setCardBin(cardBin);
                                    paymentInfoBean.setCheckShopTransactionID(shopTransactionID);
                                    paymentInfoBean.setCheckBankTransactionID(bankTransactionID);
                                    paymentInfoBean.setCheckCurrency(currency);
                                    paymentInfoBean.setCheckShopLogin(shopLoginCheckNewAcquirer);
                                    paymentInfoBean.setDefaultMethod(false);
                                    paymentInfoBean.setHash(hash);

                                    System.out.println("User not enabled - wrong category");

                                    em.merge(paymentInfoBean);

                                    GestPayData deletePagamResult = gpService.deletePagam(new Double(amount), shopTransactionID, shopLoginCheckNewAcquirer, currency, 
                                            bankTransactionID, null, acquirerIdCartaSi, null, null);

                                    System.out.println("Cancellazione Autorizzazione: " + deletePagamResult.getErrorDescription() + " (" + deletePagamResult.getErrorCode() + ")");

                                    userTransaction.commit();

                                    return "OK";
                                }
                                
                                // Blocco per carte non italiane
                                if (country == null || (!country.equals("ITALIA") && !country.equals("ITA"))) {

                                    paymentInfoBean.setExpirationDate(null);
                                    paymentInfoBean.setPan(pan);
                                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                    paymentInfoBean.setMessage("Autorizzazione negata");
                                    paymentInfoBean.setToken(token);
                                    paymentInfoBean.setBrand(PanHelper.generateBrand(brand));
                                    paymentInfoBean.setCardBin(cardBin);
                                    paymentInfoBean.setCheckShopTransactionID(shopTransactionID);
                                    paymentInfoBean.setCheckBankTransactionID(bankTransactionID);
                                    paymentInfoBean.setCheckCurrency(currency);
                                    paymentInfoBean.setCheckShopLogin(shopLoginCheckNewAcquirer);
                                    paymentInfoBean.setDefaultMethod(false);
                                    paymentInfoBean.setHash(hash);
                                    
                                    if (country != null) {
                                        System.out.println("Invalid country: " + country);
                                    }
                                    else {
                                        System.out.println("Invalid country: null");
                                    }

                                    em.merge(paymentInfoBean);

                                    GestPayData deletePagamResult = null;
                                    
                                    deletePagamResult = gpService.deletePagam(new Double(amount), shopTransactionID, shopLoginCheckNewAcquirer, currency, bankTransactionID, null, 
                                            acquirerIdCartaSi, groupAcquirerCartaSi, encodedSecretKeyCartaSi);

                                    System.out.println("Cancellazione Autorizzazione: " + deletePagamResult.getErrorDescription() + " (" + deletePagamResult.getErrorCode() + ")");

                                    userTransaction.commit();

                                    return "OK";
                                }
                                else {
                                    System.out.println("Country rilevata: " + country);
                                }
                                
                                // Blocco per brand non valido
                                if (brand == null || brand.isEmpty()) {

                                    paymentInfoBean.setExpirationDate(null);
                                    paymentInfoBean.setPan(pan);
                                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                    paymentInfoBean.setMessage("Invalid brand");
                                    paymentInfoBean.setToken(token);
                                    paymentInfoBean.setBrand(PanHelper.generateBrand(brand));
                                    paymentInfoBean.setCardBin(cardBin);
                                    paymentInfoBean.setCheckShopTransactionID(shopTransactionID);
                                    paymentInfoBean.setCheckBankTransactionID(bankTransactionID);
                                    paymentInfoBean.setCheckCurrency(currency);
                                    paymentInfoBean.setCheckShopLogin(shopLoginCheckNewAcquirer);
                                    paymentInfoBean.setDefaultMethod(false);
                                    paymentInfoBean.setHash(hash);
                                    
                                    if (country != null) {
                                        System.out.println("Invalid country: " + country);
                                    }
                                    else {
                                        System.out.println("Invalid country: null");
                                    }

                                    em.merge(paymentInfoBean);

                                    GestPayData deletePagamResult = null;
                                    
                                    deletePagamResult = gpService.deletePagam(new Double(amount), shopTransactionID, shopLoginCheckNewAcquirer, currency, bankTransactionID, null, 
                                            acquirerIdCartaSi, groupAcquirerCartaSi, encodedSecretKeyCartaSi);

                                    System.out.println("Cancellazione Autorizzazione: " + deletePagamResult.getErrorDescription() + " (" + deletePagamResult.getErrorCode() + ")");

                                    userTransaction.commit();

                                    return "OK";
                                }
                                else {
                                    System.out.println("Brand rilevato: " + brand);
                                }


                                if (checkPaymentMethodExists) {

                                    // Verifica se il metodo di pagamento risulta gi� associato a un utente
                                    PaymentInfoBean paymentMethodExist = QueryRepository.findPaymentMethodHashExists(em, hash);

                                    if (paymentMethodExist != null) {
                                        
                                        // Se il metodo di pagamento risulta gi� presente bisogna controllare se l'utente
                                        //  associato � lo stesso che sta effettuando il deposito.
                                        // In questo caso l'utente non va bloccato, ma bisogna cancellare il vecchio metodo
                                        //  e inserire il nuovo, perch� si tratta di un rinnovo.
                                        System.out.println("User id:         " + userBean.getId());
                                        System.out.println("Payment user id: " + paymentMethodExist.getUser().getId());
                                        
                                        if (userBean.getId() == paymentMethodExist.getUser().getId()) {
                                            
                                            System.out.println("Sostituzione metodo di pagamento: il vecchio metodo viene impostato in stato cancellato e viene inserito il nuovo");
                                            
                                            paymentMethodExist.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                                            paymentMethodExist.setMessage("Carta di pagamento sostituita");
                                            
                                            em.merge(paymentMethodExist);
                                        }
                                        else {
                                            
                                            paymentInfoBean.setExpirationDate(null);
                                            paymentInfoBean.setPan(pan);
                                            paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                            paymentInfoBean.setMessage("Carta di pagamento gi� utilizzata");
                                            paymentInfoBean.setToken(token);
                                            paymentInfoBean.setBrand(PanHelper.generateBrand(brand));
                                            paymentInfoBean.setCardBin(cardBin);
                                            paymentInfoBean.setCheckShopTransactionID(shopTransactionID);
                                            paymentInfoBean.setCheckBankTransactionID(bankTransactionID);
                                            paymentInfoBean.setCheckCurrency(currency);
                                            paymentInfoBean.setCheckShopLogin(shopLoginCheckNewAcquirer);
                                            paymentInfoBean.setDefaultMethod(false);
                                            paymentInfoBean.setHash(hash);
                                            
                                            System.out.println("Payment method (hash: " + paymentInfoBean.getHash() + ") already used by other user ("
                                                    + paymentMethodExist.getUser().getId() + ")");
    
                                            em.merge(paymentInfoBean);
    
                                            GestPayData deletePagamResult = null;
                                            
                                            deletePagamResult = gpService.deletePagam(new Double(amount), shopTransactionID, shopLoginCheckNewAcquirer, currency, bankTransactionID,
                                                        null, acquirerIdCartaSi, groupAcquirerCartaSi, encodedSecretKeyCartaSi);
    
                                            System.out.println("Cancellazione Autorizzazione: " + deletePagamResult.getErrorDescription() + " (" + deletePagamResult.getErrorCode()
                                                    + ")");
    
                                            userTransaction.commit();
    
                                            return "OK";
                                        }
                                    }
                                }

                                if (TDLevel.equals(PaymentInfo.PAYMENTINFO_TD_LEVEL_HALF)) {

                                    System.out.println("3DSECURE NON PRESENTE. PAGAMENTO NON VERIFICATO");

                                    Integer expiryYear = new Integer(tokenExpiryYear) + 2000;
                                    Integer expiryMonth = new Integer(tokenExpiryMonth);

                                    // Associa la carta di credito all'utente
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.set(Calendar.YEAR, expiryYear);
                                    calendar.set(Calendar.MONTH, expiryMonth);
                                    calendar.set(Calendar.DAY_OF_MONTH, 1);
                                    calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
                                    Date creditCardExpirationDate = calendar.getTime();

                                    paymentInfoBean.setExpirationDate(creditCardExpirationDate);
                                    paymentInfoBean.setPan(pan);
                                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED);
                                    paymentInfoBean.setMessage(transactionResult);
                                    paymentInfoBean.setToken(token);
                                    paymentInfoBean.setBrand(PanHelper.generateBrand(brand));
                                    paymentInfoBean.setCardBin(cardBin);
                                    paymentInfoBean.setHash(hash);
                                    
                                    Double checkAmount = paymentInfoBean.getCheckAmount();

                                    // 2) Si effettua un pagamento dell'importo check amount sul metodo di pagamento associato al paymentInfoBean

                                    //String checkShopTransactionID = new IdGenerator().generateId(16).substring(0, 32);
                                    paymentInfoBean.setCheckShopTransactionID(shopTransactionID);
                                    paymentInfoBean.setCheckBankTransactionID(bankTransactionID);
                                    paymentInfoBean.setCheckCurrency(currency);
                                    paymentInfoBean.setCheckShopLogin(shopLoginCheckNewAcquirer);

                                    /*
                                     * GestPayData callPagamResult = gpService.callPagam( checkAmount, checkShopTransactionID, shopLogin, currency, token);
                                     * if ( !callPagamResult.getTransactionResult().equals("OK") ) {
                                     * 
                                     * // Non � stato possibile autorizzare il check amount
                                     * paymentInfoBean.setExpirationDate(null);
                                     * paymentInfoBean.setPan(null);
                                     * paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                     * paymentInfoBean.setMessage("Unable to authorize check amount");
                                     * paymentInfoBean.setToken(null);
                                     * paymentInfoBean.setDefaultMethod(false);
                                     * }
                                     * else {
                                     */
                                    
                                    GestPayData callSettleResult = null;
                                    
                                    callSettleResult = gpService.callSettle(checkAmount, shopTransactionID, shopLoginCheckNewAcquirer, currency, acquirerIdCartaSi, 
                                            groupAcquirerCartaSi, encodedSecretKeyCartaSi, token, authorizationCode, bankTransactionID, null,
                                            null, 0.0, 0.0);
                                    
                                    if (!callSettleResult.getTransactionResult().equals("OK")) {

                                        // Non � stato possibile movimentare il check amount
                                        paymentInfoBean.setExpirationDate(null);
                                        paymentInfoBean.setPan(null);
                                        paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                        paymentInfoBean.setMessage("Unable to settle check amount");
                                        paymentInfoBean.setToken(null);
                                        paymentInfoBean.setDefaultMethod(false);
                                    }
                                    else {

                                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Settle check amount: "
                                                + checkAmount);

                                        // 3) Si valorizzano il check amount, i tentativi residui e l'insert timestamp in paymentInfoBean e si modifica lo stato in not verified
                                        paymentInfoBean.setCheckAmount(checkAmount);
                                        paymentInfoBean.setAttemptsLeft(pinMaxAttempts);
                                        paymentInfoBean.setInsertTimestamp(new Date());

                                        // 4) Si effettua lo storno del pagamento dopo un intervallo di attesa di quualche secondo
                                        System.out.println("pre-refund start sleep");
                                        try {
                                            Thread.sleep(2000);
                                        }
                                        catch (InterruptedException ex) {
                                            Thread.currentThread().interrupt();
                                        }
                                        System.out.println("pre-refund end sleep");
                                        
                                        GestPayData callRefundResult = gpService.callRefund(checkAmount, shopTransactionID, shopLoginCheckNewAcquirer, currency, token, authorizationCode, bankTransactionID, 
                                                null, acquirerIdCartaSi, groupAcquirerCartaSi, encodedSecretKeyCartaSi);

                                        System.out.println("Storno Pagamento: " + callRefundResult.getErrorDescription() + " (" + callRefundResult.getErrorCode() + ")");

                                        paymentInfoBean.setDefaultMethod(false);

                                        checkPayment = true;
                                    }
                                }
                                else {

                                    System.out.println("3DSECURE PRESENTE. PAGAMENTO VERIFICATO");

                                    Integer expiryYear = new Integer(tokenExpiryYear) + 2000;
                                    Integer expiryMonth = new Integer(tokenExpiryMonth);

                                    // Associa la carta di credito all'utente
                                    Calendar calendar = Calendar.getInstance();
                                    calendar.set(Calendar.YEAR, expiryYear);
                                    calendar.set(Calendar.MONTH, expiryMonth);
                                    calendar.set(Calendar.DAY_OF_MONTH, 1);
                                    calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
                                    Date creditCardExpirationDate = calendar.getTime();

                                    paymentInfoBean.setExpirationDate(creditCardExpirationDate);
                                    paymentInfoBean.setPan(pan);
                                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED);
                                    paymentInfoBean.setMessage(transactionResult);
                                    paymentInfoBean.setToken(token);
                                    paymentInfoBean.setBrand(PanHelper.generateBrand(brand));
                                    paymentInfoBean.setCardBin(cardBin);
                                    paymentInfoBean.setCheckShopTransactionID(shopTransactionID);
                                    paymentInfoBean.setCheckBankTransactionID(bankTransactionID);
                                    paymentInfoBean.setCheckCurrency(currency);
                                    paymentInfoBean.setCheckShopLogin(shopLoginCheckNewAcquirer);
                                    paymentInfoBean.setAttemptsLeft(pinMaxAttempts);
                                    paymentInfoBean.setInsertTimestamp(new Date());
                                    paymentInfoBean.setDefaultMethod(false);
                                    paymentInfoBean.setHash(hash);
                                    checkPayment = true;

                                    GestPayData deletePagamResult = null;
                                    
                                    deletePagamResult = gpService.deletePagam(new Double(amount), shopTransactionID, shopLoginCheckNewAcquirer, currency, bankTransactionID, null, 
                                            acquirerIdCartaSi, groupAcquirerCartaSi, encodedSecretKeyCartaSi);

                                    System.out.println("Cancellazione Autorizzazione: " + deletePagamResult.getErrorDescription() + " (" + deletePagamResult.getErrorCode() + ")");

                                }

                                if (checkPayment) {

                                    //Boolean firstRegistration = userBean.setRegistrationCompleted();

                                    // Se l'utente appartiene alla nuova categoria e ha il flag oldUser a true bisogna impostare oldUser a false
                                    if (userBean.getOldUser() != null && userBean.getOldUser()) {

                                        System.out.println("L'utente ha completato la migrazione da vecchio a nuovo");
                                        userBean.setOldUser(false);
                                    }

                                    PaymentInfoBean defaultPaymentInfoBean = userBean.findDefaultPaymentInfoBean();

                                    boolean defaultMethod = false;
                                    if (defaultPaymentInfoBean == null) {
                                        System.out.println("Nessun metodo di pagamento di default trovato");
                                        defaultMethod = true;
                                    }
                                    else {
                                        System.out.println("Trovato metodo di pagamento di default con id " + defaultPaymentInfoBean.getId());
                                    }

                                    paymentInfoBean.setDefaultMethod(defaultMethod);

                                    /*
                                    if (firstRegistration) {
                                        em.merge(userBean);

                                        //System.out.println("firstRegistration");

                                        //sendEmail(emailSender, userBean, shopTransactionID);
                                    }
                                    */
                                    
                                    if (userBean.getDepositCardStepCompleted() == null || !userBean.getDepositCardStepCompleted()) {
                                        
                                        System.out.println("Primo deposito carta di pagamento -> imposta il flag depositCardStepCompleted a true");
                                        
                                        userBean.setDepositCardStepCompleted(Boolean.TRUE);
                                        userBean.setDepositCardStepTimestamp(new Date());
                                        
                                        em.merge(userBean);
                                    }
                                    
                                    if (userBean.getUserStatusRegistrationCompleted() && !useBusiness &&
                                            userBean.getUserType() != User.USER_TYPE_REFUELING_NEW_ACQUIRER && userBean.getUserType() != User.USER_TYPE_REFUELING_OAUTH2) {
                                        
                                        Date timestamp = new Date();
                                        final String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                                        boolean flagNotification = false;
                                        boolean flagCreditCard = false;
                                        String cardBrand = null;
                                        ClusterType cluster = ClusterType.ENIPAY;
                                        final Long sourceID = paymentInfoBean.getId();
                                        final CRMEventSourceType sourceType = CRMEventSourceType.PAYMENT_INFO;
                                        boolean privacyFlag1 = false;
                                        boolean privacyFlag2 = false;
                                        
                                        Set<TermsOfServiceBean> listTermOfService = userBean.getPersonalDataBean().getTermsOfServiceBeanData();
                                        if (listTermOfService == null) {
                                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "flagPrivacy null");
                                        }
                                        else {
                                            for (TermsOfServiceBean item : listTermOfService) {
                                                if (item.getKeyval().equals("NOTIFICATION_NEW_1")) {
                                                    flagNotification = item.getAccepted();
                                                    break;
                                                }
                                            }
                                            for (TermsOfServiceBean item : listTermOfService) {
                                                if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                                                    privacyFlag1 = item.getAccepted();
                                                    break;
                                                }
                                            }
                                            for (TermsOfServiceBean item : listTermOfService) {
                                                if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                                                    privacyFlag2 = item.getAccepted();
                                                    break;
                                                }
                                            }
                                        }
                                        
                                        if (paymentInfoBean.getStatus().equals(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {
                                            flagCreditCard = true;
                                            cardBrand = paymentInfoBean.getBrand();
                                        }

                                        if (userBean.getVirtualizationCompleted()) {
                                            cluster = ClusterType.YOUENI;
                                        }
                                        
                                    	String crmSfActive="0";
                                    	try {
                                    		crmSfActive = parametersService.getParamValue(PARAM_CRM_SF_ACTIVE);
                                		} catch (ParameterNotFoundException e) {
                                			e.printStackTrace();
                                		}
                                    	
                                        //Nuova implementazione sf
                                        //recupero parametro CRM_SF_ACTIVE per la gestione della nuova chiamata al CRM SF
                                        //0 disattivato invio sf
                                        //1 attivo solo invio sf
                                        //2 attivi entrambi i crm ibm,sf
                                       
                                        if(crmSfActive.equalsIgnoreCase("0")||crmSfActive.equalsIgnoreCase("2")){

	                                        final UserProfile userProfile = UserProfile.getUserProfileForEventCreditCard(fiscalCode, timestamp, flagNotification, flagCreditCard, 
	                                                cardBrand, cluster);
	                                        
	                                        new Thread(new Runnable() {
	                                            
	                                            @Override
	                                            public void run() {
	                                                String crmResponse = crmService.sendNotifyEventOffer(fiscalCode, InteractionPointType.CREDIT_CARD, userProfile, sourceID, sourceType);
	
	                                                if (!crmResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
	                                                    System.err.println("Error in sending crm notification (" + crmResponse + ")");
	                                                }
	                                            }
	                                        }, "executeBatchGetOffers (UserV2UpdateUserPaymentDataAction)").start();
                                        }
                                        if(crmSfActive.equalsIgnoreCase("1")||crmSfActive.equalsIgnoreCase("2")){
                                        	
                                        	System.out.println("##################################################");
                                        	System.out.println("fiscalCode:" + fiscalCode);
                                        	System.out.println("timestamp:" + timestamp);
                                        	System.out.println("flagNotification:" + flagNotification);
                                        	System.out.println("flagCreditCard:" + flagCreditCard);
                                        	System.out.println("cardBrand:" + cardBrand);
                                        	System.out.println("cluster.getValue():" +cluster.getValue() );
                                        	System.out.println("userBean.getActiveMobilePhone():" +userBean.getActiveMobilePhone() );
                                       
                                        	String panCode="";
                                        	if(userBean.getVirtualLoyaltyCard()!=null){
                                        		System.out.println("getPanCode():" +userBean.getVirtualLoyaltyCard().getPanCode() );
                                        		panCode=userBean.getVirtualLoyaltyCard().getPanCode();
                                        	}else
                                        		System.out.println("getPanCode():null");
                                        	
                                        	System.out.println("EVENT_TYPE.ASSOCIAZIONE_CARTA_PAGAMENTO.getValue():" + EVENT_TYPE.ASSOCIAZIONE_CARTA_PAGAMENTO.getValue());
                                        	System.out.println("##################################################");
                                        	
                                        	String requestId = new IdGenerator().generateId(16).substring(0, 32);
                                        	/*:TODO da verificare requestId */
                                        	final CRMSfNotifyEvent crmSfNotifyEvent= new CRMSfNotifyEvent(requestId, fiscalCode, timestamp, "", "", false, 
                                        			0, 0D, "", "", "", "", 
                                        			null, flagNotification, flagCreditCard, cardBrand==null?"":cardBrand, 
                                        			cluster.getValue(), 0D, privacyFlag1, privacyFlag2, userBean.getActiveMobilePhone(), panCode, 
                                        			EVENT_TYPE.ASSOCIAZIONE_CARTA_PAGAMENTO.getValue(), "", "", "");
                                        	
                        	                new Thread(new Runnable() {
                        	                    
                        	                    @Override
                        	                    public void run() {
                        	                    	NotifyEventResponse crmResponse = crmService.sendNotifySfEventOffer(crmSfNotifyEvent.getRequestId(), crmSfNotifyEvent.getFiscalCode(), crmSfNotifyEvent.getDate(), 
                        	                        		crmSfNotifyEvent.getStationId(), crmSfNotifyEvent.getProductId(), crmSfNotifyEvent.getPaymentFlag(), crmSfNotifyEvent.getCredits(), 
                        	                        		crmSfNotifyEvent.getQuantity(), crmSfNotifyEvent.getRefuelMode(), crmSfNotifyEvent.getFirstName(), crmSfNotifyEvent.getLastName(), 
                        	                        		crmSfNotifyEvent.getEmail(), crmSfNotifyEvent.getBirthDate(), crmSfNotifyEvent.getNotificationFlag(), crmSfNotifyEvent.getPaymentCardFlag(), 
                        	                        		crmSfNotifyEvent.getBrand(), crmSfNotifyEvent.getCluster(),crmSfNotifyEvent.getAmount(), crmSfNotifyEvent.getPrivacyFlag1(), 
                        	                        		crmSfNotifyEvent.getPrivacyFlag2(), crmSfNotifyEvent.getMobilePhone(), crmSfNotifyEvent.getLoyaltyCard(), crmSfNotifyEvent.getEventType(), 
                        	                        		crmSfNotifyEvent.getParameter1(), crmSfNotifyEvent.getParameter2(), crmSfNotifyEvent.getPaymentMode());
                        	    
                        	                    	System.out.println("crmResponse succes:" + crmResponse.getSuccess());
                        		                      System.out.println("crmResponse errorCode:" + crmResponse.getErrorCode());
                        		                      System.out.println("crmResponse message:" + crmResponse.getMessage());
                        		                      System.out.println("crmResponse requestId:" + crmResponse.getRequestId());
                        	                    }
                        	                }, "executeBatchGetOffersSF (UserV2UpdateUserPaymentDataAction)").start();
                                        }
                                        
                                        
                                    }
                                }
                                
                                if (useBusiness) {
                                    
                                    if (userBean.getDepositCardStepCompleted() == null || !userBean.getDepositCardStepCompleted()) {
                                        
                                        System.out.println("Primo deposito carta di pagamento -> imposta il flag depositCardStepCompleted a true");
                                        
                                        userBean.setDepositCardStepCompleted(Boolean.TRUE);
                                        userBean.setDepositCardStepTimestamp(new Date());
                                        
                                        em.merge(userBean);
                                    }
                                    
                                    if (!userBean.getUserStatusRegistrationCompleted()) {

                                        userBean.setRegistrationCompleted();
                                        userBean.setUserStatusRegistrationTimestamp(new Date());
                                        
                                        em.merge(userBean);
                                        
                                        System.out.println("Impostato il flag di registrazione completa per l'utente business");
                                        
                                        if (emailSender != null) {
    
                                            System.out.println("sending confirm registration email");
    
                                            String keyFrom = emailSender.getSender();
                                            
                                            EmailType emailType = EmailType.CONFIRM_REGISTRATION_BUSINESS;
                                            String to = userBean.getPersonalDataBean().getSecurityDataEmail();
                                            List<Parameter> parameters = new ArrayList<Parameter>(0);
    
                                            parameters.add(new Parameter("WELCOME", userBean.getPersonalDataBean().getFirstName()));
    
                                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Sending email to "
                                                    + userBean.getPersonalDataBean().getSecurityDataEmail());
    
                                            String result = emailSender.sendEmail(emailType, keyFrom, to, parameters);
    
                                            //String result = "disabled";
    
                                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "SendEmail result: " + result);
    
                                        }
                                    }
                                }
                            }
                            else {

                                paymentInfoBean.setExpirationDate(null);
                                paymentInfoBean.setPan(null);
                                paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_ERROR);
                                paymentInfoBean.setMessage(errorDescription);
                                paymentInfoBean.setToken(null);
                                paymentInfoBean.setDefaultMethod(false);

                            }

                            System.out.println("Pagamento di default: " + paymentInfoBean.getDefaultMethod());
                            // Salva l'oggetto paymentInfoBean su db
                            em.merge(paymentInfoBean);

                            // Aggiorna lo stato l'oggetto shopTransactionDataBean e salvalo su db
                            shopTransactionDataBean.setStatusToUsed();
                            em.merge(shopTransactionDataBean);
                        }
                    }
                }
            }

            userTransaction.commit();

            return "OK";

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user update user payment data with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, message);

            throw new EJBException(ex2);
        }
    }
    /*
    private boolean allowPromotion(UserBean userBean) {
        File fileAllowPromotion = new File(System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "promotion" + File.separator + "allow");
        boolean allow = false;

        if (fileAllowPromotion.exists() && fileAllowPromotion.length() == 0) {
            allow = true;
        }
        else {
            try {
                FileInputStream input = new FileInputStream(fileAllowPromotion);

                InputStreamReader is = new InputStreamReader(input);
                BufferedReader br = new BufferedReader(is);
                String read;

                while ((read = br.readLine()) != null) {
                    System.out.println("Utente Autorizzato: " + read + " -- Utente: " + userBean.getPersonalDataBean().getSecurityDataEmail());
                    if (read.equals(userBean.getPersonalDataBean().getSecurityDataEmail())) {
                        allow = true;
                        break;
                    }
                }

                br.close();
            }
            catch (IOException ex) {
                allow = false;
            }
        }

        return allow;
        return true;
    }
    */
}
