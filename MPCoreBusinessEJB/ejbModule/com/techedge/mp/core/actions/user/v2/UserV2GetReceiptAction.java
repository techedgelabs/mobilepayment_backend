package com.techedge.mp.core.actions.user.v2;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.xmp.impl.Base64;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GetReceiptDataDetail;
import com.techedge.mp.core.business.interfaces.GetReceiptDataResponse;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.MailParameter;
import com.techedge.mp.core.business.mail.MailStyle;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.PrePaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionAdditionalDataBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionAdditionalDataBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.utilities.Proxy;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.utilities.TransactionFinalStatusConverter;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2GetReceiptAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2GetReceiptAction() {}

    public GetReceiptDataResponse execute(String ticketId, String requestId, String type, String transactionId,
            String proxyHost, String proxyPort, String proxyNoHosts, UserCategoryService userCategoryService, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        GetReceiptDataResponse getReceiptResponse = new GetReceiptDataResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);
            
            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {
                
                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                getReceiptResponse.setStatusCode(ResponseHelper.USER_V2_GET_RECEIPT_INVALID_TICKET);

                userTransaction.commit();

                return getReceiptResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                getReceiptResponse.setStatusCode(ResponseHelper.USER_V2_GET_RECEIPT_INVALID_TICKET);

                userTransaction.commit();

                return getReceiptResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non può invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to retrieve mission list, user  in status "
                        + userStatus);

                getReceiptResponse.setStatusCode(ResponseHelper.USER_V2_GET_RECEIPT_UNAUTHORIZED);

                userTransaction.commit();

                return getReceiptResponse;
            }

            // Recupero la receipt dal TransactionBean se tipo = Prepaid, o dal PostPaidTransactionBean se tipo richiesta è postpaid
            
            GetReceiptDataDetail receiptDataDetail = new GetReceiptDataDetail();
            
            if(type != null && type.equals("PRE-PAY")){ 
                
             // Ricerca la transazione associata al transactionID
                TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, transactionId);
                
                if (transactionBean == null) {

                    // Transaction not found
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionId, null, "Transaction not found.");

                    userTransaction.commit();
                    
                    getReceiptResponse.setStatusCode(StatusHelper.PERSIST_PAYMENT_COMPLETION_STATUS_ERROR);
                    return getReceiptResponse;

                }
                
               //Controllo che 'utente richiedente sia il proprietario della transazione. 
                if(transactionBean.getUserBean().getId()!=userBean.getId()){
                    // Utente non è proprietario della transazione
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionId, null, "User not owner transaction.");

                    userTransaction.commit();
                    
                    getReceiptResponse.setStatusCode(StatusHelper.USER_TRANSACTION_AUTH_FAILURE);
                    return getReceiptResponse;
                }
                
                // Crea il nuovo stato
                Integer sequenceID = 0;
                Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
                for (TransactionStatusBean transactionStatusBean : transactionStatusData) {
                    Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
                    if (sequenceID < transactionStatusBeanSequenceID) {
                        sequenceID = transactionStatusBeanSequenceID;
                    }
                }
                sequenceID = sequenceID + 1;

                Date now = new Date();
                Timestamp timestamp = new java.sql.Timestamp(now.getTime());
                String requestID = String.valueOf(now.getTime());

                TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

                transactionStatusBean.setTransactionBean(transactionBean);
                transactionStatusBean.setSequenceID(sequenceID);
                transactionStatusBean.setTimestamp(timestamp);
                transactionStatusBean.setRequestID(requestID);
                
                byte[] pdfPrePaidReceiptBytes = this.getPrePaidSummaryNewAcquirerFlowPdf(transactionBean, transactionStatusBean, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);
                if(pdfPrePaidReceiptBytes!= null){
                    
                    byte[] encodedBase64 = Base64.encode(pdfPrePaidReceiptBytes);
                    String documentBase64 = new String(encodedBase64);
                    receiptDataDetail.setDimension(encodedBase64.length);
                    receiptDataDetail.setFilename(transactionId.concat(".pdf"));
                    receiptDataDetail.setDocumentBase64(documentBase64);
                    
                }
                
            }
            else if(type != null && type.equals("POST-PAY")){
               
                // Ricerca la transazione postpaid associata al transactionID
                PostPaidTransactionBean postPaidTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, transactionId);
                
                if (postPaidTransactionBean == null) {

                    // Transaction not found
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionId, null, "Post paid transaction not found.");

                    userTransaction.commit();
                    
                    getReceiptResponse.setStatusCode(StatusHelper.PERSIST_PAYMENT_COMPLETION_STATUS_ERROR);
                    return getReceiptResponse;

                }
                
                PostPaidTransactionEventBean lastPostPaidTransactionEventBean = postPaidTransactionBean.getLastPostPaidTransactionEventBean();
                
                byte[] pdfPostPaidReceiptBytes = this.getPostPaidSummaryNewAcquirerFlow(postPaidTransactionBean, lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);
                
                if(pdfPostPaidReceiptBytes!= null){
                    
                    byte[] encodedBase64 = Base64.encode(pdfPostPaidReceiptBytes);
                    String documentBase64 = new String(encodedBase64);
                    receiptDataDetail.setDimension(encodedBase64.length);
                    receiptDataDetail.setFilename(transactionId.concat(".pdf"));
                    receiptDataDetail.setDocumentBase64(documentBase64);
                    
                }
            }
            else{
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", transactionId, null, "Type paid transaction not found.");

                userTransaction.commit();
                
                getReceiptResponse.setStatusCode(StatusHelper.PERSIST_PAYMENT_COMPLETION_STATUS_ERROR);
                return getReceiptResponse;
                
            }
            
            
              //Dati stub
//            receiptDataDetail.setDimension(16);
//            receiptDataDetail.setDocumentBase64("32784632784632786846328");
//            receiptDataDetail.setFilename("EJB receipt-456.pdf");
            
            getReceiptResponse.setGetReceiptDataDetail(receiptDataDetail);
            
            if(getReceiptResponse.getGetReceiptDataDetail()!=null){
                getReceiptResponse.setStatusCode(ResponseHelper.USER_V2_GET_RECEIPT_SUCCESS);

            }
            else{
                getReceiptResponse.setStatusCode(ResponseHelper.USER_V2_GET_RECEIPT_NOTFOUND);
            }
            
            
            userTransaction.commit();
            return getReceiptResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED UserV2GetReceiptAction  with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
    
    public static byte[] getPrePaidSummaryNewAcquirerFlowPdf(TransactionBean transactionBean, TransactionStatusBean transactionStatusBean,
            String proxyHost, String proxyPort, String proxyNoHosts, UserCategoryService userCategoryService, StringSubstitution stringSubstitution) {
        // Invio dello scontrino del rifornimento pre paid

        System.out.println("Invio scontrino in formato pdf del rifornimento pre paid");

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");

        boolean newAcquirerFlow = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
        
        EmailType emailType = EmailType.TRANSACTION_SUMMARY;
        
        if (newAcquirerFlow) {
            emailType = EmailType.TRANSACTION_SUMMARY_V2;
        }
        
        UserBean userBean = transactionBean.getUserBean();
        String to = userBean.getPersonalDataBean().getSecurityDataEmail();
        //List<MailParameter> mainTable = new ArrayList<MailParameter>(0);
        List<MailParameter> headerTable = new ArrayList<MailParameter>(0);
        List<List<MailParameter>> contentTableList = new ArrayList<List<MailParameter>>(0);
        //mainTable.add(new MailParameter("Nome", userBean.getPersonalDataBean().getFirstName()));

        String status = (transactionBean.getFinalStatusType() != null) ? transactionBean.getFinalStatusType() : StatusHelper.FINAL_STATUS_TYPE_ERROR;
        String subStatus = null;

        if (transactionStatusBean != null) {
            subStatus = transactionStatusBean.getStatus();
        }
        else {
            if (transactionBean.getLastTransactionStatus() != null) {
                subStatus = transactionBean.getLastTransactionStatus().getStatus();
            }
        }

        String messageName = "Ciao " + userBean.getPersonalDataBean().getFirstName() + ",";
        String messageStatus = TransactionFinalStatusConverter.getMailTransactionMessageText(status, subStatus);

        //if (message != null && !message.equals("")) {
        //    mainTable.add(new MailParameter("Messaggio", message));
        //}

        double totalAmount = 0.0;
        double bankAmount = 0.0;
        String stringAddress = transactionBean.getStationBean().getFullAddress();
        String stringAuthorizationCode = "";
        String stringPan = "";
        String stringBankTransactionId = "";

        if (transactionBean.getFinalAmount() != null) {
            totalAmount = transactionBean.getFinalAmount().doubleValue();
            bankAmount = totalAmount;
        }

        /*
        if (transactionBean.getToken() != null) {
            stringPan = transactionBean.getToken().substring(0, 2) + "XXXXXXXXXX" + transactionBean.getToken().substring(12, 16);
        }
        */
        Long   paymentMethodId   = transactionBean.getPaymentMethodId();
        String paymentMethodType = transactionBean.getPaymentMethodType();
        PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
        if(paymentInfoBean != null) {
            stringPan =  paymentInfoBean.getPan();
        }

        stringBankTransactionId = transactionBean.getBankTansactionID();
        stringAuthorizationCode = transactionBean.getAuthorizationCode();

        System.out.println("AuthorizationCode: " + transactionBean.getAuthorizationCode());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String date = simpleDateFormat.format(transactionBean.getCreationTimestamp());

        headerTable.add(new MailParameter("RICEVUTA RIFORNIMENTO", null));
        headerTable.add(new MailParameter("Esito transazione", TransactionFinalStatusConverter.getMailTransactionStatusText(status, subStatus)));
        List<MailParameter> contentTable = new ArrayList<MailParameter>(0);
        contentTable.add(new MailParameter("CODICE PV", transactionBean.getStationBean().getStationID()));
        contentTable.add(new MailParameter("INDIRIZZO IMPIANTO", stringAddress));
        contentTable.add(new MailParameter("NUMERO EROGATORE", transactionBean.getPumpNumber().toString()));
        //contentTable.add(new MailParameter("CODICE EROGATORE", transactionBean.getPumpID()));

        if (transactionBean.getFuelQuantity() != null) {
            String stringFuelQuantity = numberFormat.format(transactionBean.getFuelQuantity());
            contentTable.add(new MailParameter("QUANTITA' EROGATA (lt)", stringFuelQuantity));
        }

        if (transactionBean.getProductDescription() != null && !transactionBean.getProductDescription().equals("")) {
            contentTable.add(new MailParameter("TIPO DI CARBURANTE", transactionBean.getProductDescription()));
        }

        if (transactionBean.getFuelAmount() != null) {
            contentTable.add(new MailParameter("IMPORTO CARBURANTE ()", currencyFormat.format(transactionBean.getFuelAmount())));
        }
        
        for(TransactionAdditionalDataBean transactionAdditionalDataBean : transactionBean.getTransactionAdditionalDataBeanList()) {
            String dataKey   = transactionAdditionalDataBean.getDataKey();
            String dataValue = transactionAdditionalDataBean.getDataValue();
            if (dataKey.equalsIgnoreCase("merchantAddress") ||
                dataKey.equalsIgnoreCase("cardType") ||
                dataKey.equalsIgnoreCase("merchantName") ||
                dataKey.equalsIgnoreCase("cardExpDt") ||
                dataKey.equalsIgnoreCase("productIDListString")) {
            }
            else {
                
                String dataLabel = dataKey;
                if (dataKey.equalsIgnoreCase("mcMaskedPan")) {
                    dataLabel = "CARTA MULTICARD";
                }
                else if (dataKey.equalsIgnoreCase("addInfo")) {
                    dataLabel = "INFO";
                }
                else if (dataKey.equalsIgnoreCase("km")) {
                    dataLabel = "KM";
                }
                contentTable.add(new MailParameter(dataLabel, dataValue));
            }
        }

        contentTableList.add(contentTable);
        List<MailParameter> secondContentTable = new ArrayList<MailParameter>(0);

        secondContentTable.add(new MailParameter("DATA TRANSAZIONE", date));
        secondContentTable.add(new MailParameter("CODICE TRANSAZIONE", transactionBean.getTransactionID()));

        if (stringBankTransactionId != null && !stringBankTransactionId.equals("")) {
            secondContentTable.add(new MailParameter("NUMERO OPERAZIONE", stringBankTransactionId));
        }

        if (stringAuthorizationCode != null && !stringAuthorizationCode.equals("")) {
            secondContentTable.add(new MailParameter("CODICE AUTORIZZAZIONE", stringAuthorizationCode));
        }

        if (totalAmount > 0.0 && transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL)) {
            secondContentTable.add(new MailParameter("IMPORTO TOTALE ()", currencyFormat.format(totalAmount)));
        }

        contentTableList.add(secondContentTable);

        List<MailParameter> thirdContentTable = new ArrayList<MailParameter>(0);

        double totalConsumed = 0.0;

        if (!transactionBean.getPrePaidConsumeVoucherBeanList().isEmpty()) {

            List<Object[]> voucherDetailList = new ArrayList<Object[]>(0);

            for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {

                if (prePaidConsumeVoucherBean.getWarningMsg() != null) {
                    //String value = prePaidConsumeVoucherBean.getWarningMsg();
                    //String label = "";
                    String value = currencyFormat.format(0.0);
                    String label = prePaidConsumeVoucherBean.getWarningMsg();
                    Object[] detail = new Object[] { label, value };
                    voucherDetailList.add(detail);
                }

                if (prePaidConsumeVoucherBean.getTotalConsumed() != null && prePaidConsumeVoucherBean.getTotalConsumed().doubleValue() > 0.0) {

                    totalConsumed += prePaidConsumeVoucherBean.getTotalConsumed().doubleValue();

                    for (PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean : prePaidConsumeVoucherBean.getPrePaidConsumeVoucherDetailBean()) {
                        if (prePaidConsumeVoucherDetailBean.getConsumedValue() == null || prePaidConsumeVoucherDetailBean.getConsumedValue().doubleValue() == 0.0) {
                            continue;
                        }

                        String value = currencyFormat.format(prePaidConsumeVoucherDetailBean.getConsumedValue());
                        String label = prePaidConsumeVoucherDetailBean.getPromoDescription();
                        label = prePaidConsumeVoucherDetailBean.getPromoDescription() + "\nCodice: " + prePaidConsumeVoucherDetailBean.getVoucherCode();

                        if (prePaidConsumeVoucherBean.getMarketingMsg() != null) {
                            //label += "<br>" + prePaidConsumeVoucherBean.getMarketingMsg();
                            label += "\n\n" + prePaidConsumeVoucherBean.getMarketingMsg();
                        }

                        Object[] detail = new Object[] { label, value };
                        voucherDetailList.add(detail);
                    }
                }
            }

            if (totalConsumed > 0.0 || !voucherDetailList.isEmpty()) {

                //contentTable.add(new MailParameter("VOUCHER (&euro;)", ""));
                thirdContentTable.add(new MailParameter("DETTAGLIO VOUCHER CONSUMATI ()", ""));
                bankAmount -= totalConsumed;

                for (Object[] voucherDetail : voucherDetailList) {
                    thirdContentTable.add(new MailParameter(voucherDetail[0].toString(), voucherDetail[1].toString(), 1));
                }
            }
            contentTableList.add(thirdContentTable);
        }
        
        if (!transactionBean.getPrePaidLoadLoyaltyCreditsBeanList().isEmpty()) {

            List<MailParameter> fourthContentTable = new ArrayList<MailParameter>(0);
            
            Integer totalCredits = 0;
            //List<String[][]> loyaltyDetailList = new ArrayList<String[][]>(0);
            List<ArrayList<String[]>> loyaltyDetailList = new ArrayList<ArrayList<String[]>>(0);

            for (PrePaidLoadLoyaltyCreditsBean prePaidLoadLoyaltyCreditsBean : transactionBean.getPrePaidLoadLoyaltyCreditsBeanList()) {

                if (prePaidLoadLoyaltyCreditsBean != null && prePaidLoadLoyaltyCreditsBean.getTransactionBean().getFinalStatusType().equals("SUCCESSFUL")) {

                    String labelPan = "CODICE";
                    String labelPoint = "PUNTI";
                    String labelBalance = "SALDO PUNTI";
                    String labelWarning = "";
                    String valuePan = null;
                    String valuePoint = null;
                    String valueBalance = null;
                    String valueWarning = null;

                    if (prePaidLoadLoyaltyCreditsBean.getEanCode() != null) {
                        valuePan = prePaidLoadLoyaltyCreditsBean.getEanCode();
                    }

                    if (prePaidLoadLoyaltyCreditsBean.getBalance() != null) {
                        valueBalance = prePaidLoadLoyaltyCreditsBean.getBalance().toString();
                    }

                    if (prePaidLoadLoyaltyCreditsBean.getCredits() != null) {
                        totalCredits += prePaidLoadLoyaltyCreditsBean.getCredits();
                        valuePoint = prePaidLoadLoyaltyCreditsBean.getCredits().toString();
                    }

                    if (!prePaidLoadLoyaltyCreditsBean.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {
                        if (prePaidLoadLoyaltyCreditsBean.getMessageCode() != null) {
                            valueWarning = prePaidLoadLoyaltyCreditsBean.getMessageCode();
                        }
                        else if (prePaidLoadLoyaltyCreditsBean.getWarningMsg() != null) {
                            valueWarning = prePaidLoadLoyaltyCreditsBean.getWarningMsg();
                        }

                        valuePoint = null;
                    }

                    if (prePaidLoadLoyaltyCreditsBean.getWarningMsg() != null) {
                        valueWarning = prePaidLoadLoyaltyCreditsBean.getWarningMsg();
                    }

                    if (valuePoint != null) {
                        ArrayList<String[]> detail = new ArrayList<String[]>(0);
                        detail.add(new String[] { labelPan, valuePan });
                        detail.add(new String[] { labelPoint, valuePoint });
                        detail.add(new String[] { labelBalance, valueBalance });

                        loyaltyDetailList.add(detail);
                    }

                    if (valueWarning != null) {
                        ArrayList<String[]> detail = new ArrayList<String[]>(0);
                        detail.add(new String[] { labelWarning, valueWarning });
                        loyaltyDetailList.add(detail);
                    }
                }
            }

            if (!loyaltyDetailList.isEmpty()) {
                fourthContentTable.add(new MailParameter("PUNTI FEDELTA'", ""));

                for (ArrayList<String[]> loyaltyDetail : loyaltyDetailList) {
                    for (String[] detail : loyaltyDetail) {
                        fourthContentTable.add(new MailParameter(detail[0], detail[1], 1));
                    }
                }
            }
            
            contentTableList.add(fourthContentTable);
        }

        if (transactionBean.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) && bankAmount > 0.0) {
            
            List<MailParameter> fifthContentTable = new ArrayList<MailParameter>(0);
            
            fifthContentTable.add(new MailParameter("CARTA DI PAGAMENTO", stringPan));

            if (stringBankTransactionId != null && !stringBankTransactionId.equals("")) {
                fifthContentTable.add(new MailParameter("NUMERO OPERAZIONE BANCARIA", stringBankTransactionId));
            }

            if (stringAuthorizationCode != null && !stringAuthorizationCode.equals("")) {
                fifthContentTable.add(new MailParameter("CODICE AUTORIZZAZIONE", stringAuthorizationCode));
            }

            //contentTable.add(new MailParameter("IMPORTO ADDEBITATO (&euro;)", currencyFormat.format(bankAmount)));
            fifthContentTable.add(new MailParameter("IMPORTO ADDEBITATO ()", currencyFormat.format(bankAmount)));

            contentTableList.add(fifthContentTable);
        }

        List<Parameter> bodyContent = new ArrayList<Parameter>(0);
        String fileStylePath = System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "mail" + File.separator + "mail_pdf.properties";
        //String keyFrom = emailSender.getSender();
        String keySubject = "Eni Pay";
        String keyAttachmentName = "enipay";
        
        
        if (newAcquirerFlow) {
            fileStylePath = System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "mail" + File.separator + "mail_pdf_new.properties";
            
            if (stringSubstitution != null) {
               // keyFrom = stringSubstitution.getValue(keyFrom, 1);
                keySubject = stringSubstitution.getValue(keySubject, 1);
                keyAttachmentName = stringSubstitution.getValue(keyAttachmentName, 1);
            }
        }
        
        //System.out.println("keyFrom: " + keyFrom);
        System.out.println("keySubject: " + keySubject);
        System.out.println("keyAttachmentName: " + keyAttachmentName);

        bodyContent.add(new Parameter("NAME", messageName));
        bodyContent.add(new Parameter("MESSAGE", messageStatus));

        try {
            String dateBody = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
            String dateFilename = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            
            byte[] pdfBytes = createPdf(fileStylePath, headerTable, contentTableList, proxyHost, proxyPort, proxyNoHosts);
            List<Attachment> attachments = new ArrayList<Attachment>(0);

            Attachment attachment = new Attachment();
            attachment.setFileName("transazione_" + keyAttachmentName + "_" + dateFilename + ".pdf");
            attachment.setBytes(pdfBytes);
            attachments.add(attachment);

            //System.out.println("Sending email to " + to);

            String subject = keySubject + " - transazione del " + dateBody;
            //String result = emailSender.sendEmail(emailType, to, null, null, subject, bodyContent);
            //String result = emailSender.sendEmailWithAttachments(emailType, keyFrom, to, subject, bodyContent, attachments);

            return pdfBytes;
           
        }
        catch (DocumentException ex) {
            System.err.println("Errore nella creazione dello scontrino in formato pdf: " + ex.getMessage());
            //return new byte[]{};
            return null;
        }
    }
    
    @SuppressWarnings("unused")
    public static byte[] createPdf(String fileStylePath, List<MailParameter> headerTable, List<List<MailParameter>> contentTable, String proxyHost, String proxyPort,
            String proxyNoHosts) throws DocumentException {

        Proxy proxy = new Proxy(proxyHost, proxyPort, proxyNoHosts);
        proxy.setHttp();

        MailStyle style = new MailStyle(fileStylePath);
        Document doc = new Document();
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PdfWriter writer = PdfWriter.getInstance(doc, baos);

        int[] headerTableKeyFontColor = style.getStyle4ListInt("headerTable.keyFont.color");
        int[] headerTableValueFontColor = style.getStyle4ListInt("headerTable.valueFont.color");
        int headerTableCellHeight = style.getStyle4Int("headerTable.cell.height");
        Font headerTableKeyFont = FontFactory.getFont(style.getStyle("headerTable.keyFont.family"), style.getStyle4Int("headerTable.keyFont.size"),
                style.getStyle4Int("headerTable.keyFont.style"), new BaseColor(headerTableKeyFontColor[0], headerTableKeyFontColor[1], headerTableKeyFontColor[2]));
        Font headerTableValueFont = FontFactory.getFont(style.getStyle("headerTable.valueFont.family"), style.getStyle4Int("headerTable.valueFont.size"),
                style.getStyle4Int("headerTable.valueFont.style"), new BaseColor(headerTableValueFontColor[0], headerTableValueFontColor[1], headerTableValueFontColor[2]));

        Font headerTableTitleFont = FontFactory.getFont(style.getStyle("headerTable.title.family"), style.getStyle4Int("headerTable.title.size"),
                style.getStyle4Int("headerTable.title.style"), new BaseColor(headerTableKeyFontColor[0], headerTableKeyFontColor[1], headerTableKeyFontColor[2]));

        int contentTableCellHeight = style.getStyle4Int("contentTable.cell.height");
        int[] headerTableCellBackground = style.getStyle4ListInt("headerTable.cell.background");
        int headerTableLogoPaddingBottom = style.getStyle4Int("headerTable.logo.paddingBottom");
        int headerTableCellBorder = style.getStyle4Int("headerTable.cell.border");
        int[] headerTableCellBorderColor = style.getStyle4ListInt("headerTable.cell.borderColor");
        String headerTableLogoUrl = style.getStyle("headerTable.logo.url");
        int headerTableCellPadding = style.getStyle4Int("headerTable.cell.padding");
        int[] contentTableCellBackground = style.getStyle4ListInt("contentTable.cell.background");
        int contentTableCellBorder = style.getStyle4Int("contentTable.cell.border");
        int[] contentTableCellBorderColor = style.getStyle4ListInt("contentTable.cell.borderColor");
        int[] contentTableSeparatorBackground = style.getStyle4ListInt("contentTable.separator.background");
        int contentTableSeparatorHeight = style.getStyle4Int("contentTable.separator.height");
        int[] contentTableKeyFontColor = style.getStyle4ListInt("contentTable.keyFont.color");
        int[] contentTableSubKeyFontColor = style.getStyle4ListInt("contentTable.subKeyFont.color");
        int[] contentTableValueFontColor = style.getStyle4ListInt("contentTable.valueFont.color");
        Font contentTableKeyFont = FontFactory.getFont(style.getStyle("contentTable.keyFont.family"), style.getStyle4Int("contentTable.keyFont.size"),
                style.getStyle4Int("contentTable.keyFont.style"), new BaseColor(contentTableKeyFontColor[0], contentTableKeyFontColor[1], contentTableKeyFontColor[2]));
        Font contentTableSubKeyFont = FontFactory.getFont(style.getStyle("contentTable.subKeyFont.family"), style.getStyle4Int("contentTable.subKeyFont.size"),
                style.getStyle4Int("contentTable.subKeyFont.style"), new BaseColor(contentTableSubKeyFontColor[0], contentTableSubKeyFontColor[1], contentTableSubKeyFontColor[2]));
        Font contentTableValueFont = FontFactory.getFont(style.getStyle("contentTable.valueFont.family"), style.getStyle4Int("contentTable.valueFont.size"),
                style.getStyle4Int("contentTable.valueFont.style"), new BaseColor(contentTableValueFontColor[0], contentTableValueFontColor[1], contentTableValueFontColor[2]));
        int contentTableCellPadding = style.getStyle4Int("contentTable.cell.padding");

        doc.open();

        PdfPTable tableHeader = new PdfPTable(1);

        if (headerTableLogoUrl != null && !headerTableLogoUrl.isEmpty()) {
            try {
                URL urlLogo = new URL(headerTableLogoUrl);
                PdfPCell imageCell = new PdfPCell();
                imageCell.setBorder(0);
                imageCell.setPaddingBottom(headerTableLogoPaddingBottom);
                Image image = Image.getInstance(urlLogo);
                imageCell.addElement(image);
                tableHeader.addCell(imageCell);
            }
            catch (IOException ex) {
                System.err.println("Errore nel caricamento del logo (" + headerTableLogoUrl + "): " + ex.getMessage());
            }

        }

        for (MailParameter param : headerTable) {
            PdfPCell cell = new PdfPCell();
            cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
            cell.setBorder(headerTableCellBorder);
            cell.setBorderColor(new BaseColor(headerTableCellBorderColor[0], headerTableCellBorderColor[1], headerTableCellBorderColor[2]));
            cell.setBackgroundColor(new BaseColor(headerTableCellBackground[0], headerTableCellBackground[1], headerTableCellBackground[2]));
            cell.setPadding(headerTableCellPadding);
            cell.setMinimumHeight(headerTableCellHeight);
            Paragraph paragraph = new Paragraph();
            Phrase phraseName = new Phrase();
            paragraph.setAlignment(Element.ALIGN_LEFT);
            if (param.getValue() == null) {
                phraseName = new Phrase(param.getName(), headerTableTitleFont);
                paragraph.setAlignment(Element.ALIGN_CENTER);
            }
            else {
                phraseName = new Phrase(param.getName() + ": ", headerTableKeyFont);
            }
            Phrase phraseValue = new Phrase(param.getValue(), headerTableValueFont);
            paragraph.add(phraseName);
            paragraph.add(phraseValue);
            cell.addElement(paragraph);
            tableHeader.addCell(cell);
        }
        List<PdfPTable> tableContentList = new ArrayList<PdfPTable>(0);
        for (List<MailParameter> contentTableItem : contentTable) {
            PdfPTable tableContent = new PdfPTable(2);
            tableContent.setWidthPercentage(100);

            for (MailParameter param : contentTableItem) {
                PdfPCell keyCell = new PdfPCell();
                PdfPCell valueCell = new PdfPCell();
                int colspan = 0;
                int keyHorizontalAlignment = Element.ALIGN_LEFT;
                int valueHorizontalAlignment = Element.ALIGN_RIGHT;
                boolean emptyBorder = false;
                String keyString = param.getName().toUpperCase();
                String valueString = param.getValue();
                Font keyFont = contentTableKeyFont;
                int[] cellBackground = contentTableCellBackground;
                int cellHeight = contentTableCellHeight;

                if (param.getName().equals("SEPARATOR")) {
                    cellBackground = contentTableSeparatorBackground;
                    cellHeight = contentTableSeparatorHeight;
                    colspan = 2;
                    keyString = "";
                    valueString = "";
                    emptyBorder = true;
                }
                else {
                    if (param.getName() == null || param.getName().equals("")) {
                        colspan = 2;
                        keyString = param.getValue();
                        keyFont = contentTableSubKeyFont;
                    }
                    else {
                        if (param.getLevel() > 0) {
                            keyFont = contentTableSubKeyFont;
                            keyHorizontalAlignment = Element.ALIGN_RIGHT;
                            keyString = param.getName();
                        }
                    }
                }

                Paragraph keyParagraph = new Paragraph(keyString, keyFont);
                keyParagraph.setAlignment(keyHorizontalAlignment);
                keyCell.setBackgroundColor(new BaseColor(cellBackground[0], cellBackground[1], cellBackground[2]));
                keyCell.setBorder(contentTableCellBorder);
                keyCell.setBorderColor(new BaseColor(contentTableCellBorderColor[0], contentTableCellBorderColor[1], contentTableCellBorderColor[2]));
                keyCell.setPadding(contentTableCellPadding);
                keyCell.setHorizontalAlignment(keyHorizontalAlignment);
                keyCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                keyCell.addElement(keyParagraph);

                Paragraph valueParagraph = new Paragraph(valueString, contentTableValueFont);
                valueParagraph.setAlignment(valueHorizontalAlignment);
                valueCell.setBackgroundColor(new BaseColor(cellBackground[0], cellBackground[1], cellBackground[2]));
                valueCell.setBorder(contentTableCellBorder);
                valueCell.setBorderColor(new BaseColor(contentTableCellBorderColor[0], contentTableCellBorderColor[1], contentTableCellBorderColor[2]));
                valueCell.setPadding(contentTableCellPadding);
                valueCell.setHorizontalAlignment(valueHorizontalAlignment);
                valueCell.setVerticalAlignment(Element.ALIGN_MIDDLE);
                valueCell.addElement(valueParagraph);

                if (emptyBorder) {
                    keyCell.setBorder(0);
                    valueCell.setBorder(0);
                }

                if (contentTableCellHeight > 0) {
                    keyCell.setMinimumHeight(cellHeight);
                    valueCell.setMinimumHeight(cellHeight);
                }

                if (colspan > 0) {
                    keyCell.setColspan(colspan);
                    valueCell = null;
                    tableContent.addCell(keyCell);
                }
                else {
                    tableContent.addCell(keyCell);
                    tableContent.addCell(valueCell);
                }

            }
            tableContentList.add(tableContent);
        }

        for (PdfPTable table : tableContentList) {
            PdfPCell cell = new PdfPCell();
            cell.setBorder(headerTableCellBorder);
            cell.setBorderColor(new BaseColor(headerTableCellBorderColor[0], headerTableCellBorderColor[1], headerTableCellBorderColor[2]));
            cell.setBackgroundColor(new BaseColor(headerTableCellBackground[0], headerTableCellBackground[1], headerTableCellBackground[2]));
            cell.setPadding(headerTableCellPadding);
            //            cell.setPaddingTop(20);
            //            cell.setPaddingBottom(20);
            cell.addElement(table);
            tableHeader.addCell(cell);
        }

        doc.add(tableHeader);

        doc.close();

        proxy.unsetHttp();

        byte[] bytes;
        bytes = baos.toByteArray();
        return bytes;

    }
    
    public static byte[] getPostPaidSummaryNewAcquirerFlow(PostPaidTransactionBean poPTransactionBean,
            PostPaidTransactionEventBean poPTransactionEventBean, String proxyHost, String proxyPort, String proxyNoHosts, 
            UserCategoryService userCategoryService, StringSubstitution stringSubstitution) {
        // Invio della mail con lo scontrino del rifornimento post paid

        System.out.println("Invio della mail con lo scontrino in formato pdf del rifornimento post paid");

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");

        boolean newAcquirerFlow = userCategoryService.isUserTypeInUserCategory(poPTransactionBean.getUserBean().getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
        
        EmailType emailType = EmailType.TRANSACTION_SUMMARY;
        
        if (newAcquirerFlow) {
            emailType = EmailType.TRANSACTION_SUMMARY_V2;
        }
        UserBean userBean = poPTransactionBean.getUserBean();
        String to = userBean.getPersonalDataBean().getSecurityDataEmail();
        //List<MailParameter> mainTable = new ArrayList<MailParameter>(0);
        List<MailParameter> headerTable = new ArrayList<MailParameter>(0);
        List<List<MailParameter>> contentTableList = new ArrayList<List<MailParameter>>(0);

        //mainTable.add(new MailParameter("Nome", userBean.getPersonalDataBean().getFirstName()));

        String status = poPTransactionBean.getMpTransactionStatus();
        String event = null;
        String eventCode = null;
        String eventResult = null;

        if (poPTransactionEventBean != null) {
            event = poPTransactionEventBean.getEvent();
            eventResult = poPTransactionEventBean.getResult();
            eventCode = poPTransactionEventBean.getNewState();
        }
        else {
            if (poPTransactionBean.getLastPostPaidTransactionEventBean() != null) {
                event = poPTransactionBean.getLastPostPaidTransactionEventBean().getEvent();
                eventResult = poPTransactionBean.getLastPostPaidTransactionEventBean().getResult();
                eventCode = poPTransactionBean.getLastPostPaidTransactionEventBean().getNewState();
            }
        }

        //String message = TransactionFinalStatusConverter.getMailTransactionMessageText(status, subStatus, subStatusResult);

        String messageName = "Ciao " + userBean.getPersonalDataBean().getFirstName() + ",";
        String messageStatus = TransactionFinalStatusConverter.getMailTransactionMessageText(status, event, eventCode, eventResult);

        //if (message != null && !message.equals("")) {
        //    mainTable.add(new MailParameter("Messaggio", message));
        //}

        double totalAmount = 0.0;
        double bankAmount = 0.0;
        String labelPumpOrCash = "CODICE EROGATORE";
        String labelPumpOrCashNumber = "NUMERO EROGATORE";
        String stringAddress = poPTransactionBean.getStationBean().getFullAddress();
        String stringAuthorizationCode = "";
        String stringPan = "";
        String stringBankTransactionId = "";

        if (poPTransactionBean.getAmount() != null) {
            totalAmount = poPTransactionBean.getAmount().doubleValue();
            bankAmount = totalAmount;
        }

        if (poPTransactionBean.getToken() != null) {
            PaymentInfoBean paymentInfoBean = poPTransactionBean.getUserBean().findPaymentInfoBean(poPTransactionBean.getPaymentMethodId(),
                    poPTransactionBean.getPaymentMethodType());

            if (paymentInfoBean != null) {
                stringPan = paymentInfoBean.getPan();
            }
            //.substring(0, 2) + "XXXXXXXXXX" + poPTransactionBean.getToken().substring(12, 16);
        }

        stringBankTransactionId = poPTransactionBean.getBankTansactionID();
        stringAuthorizationCode = poPTransactionBean.getAuthorizationCode();

        if (poPTransactionBean.getSource().equals("CASH REGISTER")) {
            labelPumpOrCash = "Codice cassa";
            labelPumpOrCashNumber = "Numero cassa";
        }

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
        String date = simpleDateFormat.format(poPTransactionBean.getCreationTimestamp());

        //EMAIL NUOVA

        headerTable.add(new MailParameter("RICEVUTA RIFORNIMENTO", null));
        headerTable.add(new MailParameter("Esito transazione", TransactionFinalStatusConverter.getMailTransactionStatusText(status, event, eventCode, eventResult)));
        List<MailParameter> contentTable = new ArrayList<MailParameter>(0);
        contentTable.add(new MailParameter("CODICE PV", poPTransactionBean.getStationBean().getStationID()));
        contentTable.add(new MailParameter("INDIRIZZO IMPIANTO", stringAddress));

        contentTable.add(new MailParameter(labelPumpOrCashNumber, poPTransactionBean.getSourceNumber()));

        //contentTable.add(new MailParameter(labelPumpOrCash, poPTransactionBean.getSourceID()));

        if (poPTransactionBean.getRefuelBean() != null) {

            //contentTable.add(new MailParameter("SEPARATOR"));
            for (PostPaidRefuelBean postPaidRefuelBean : poPTransactionBean.getRefuelBean()) {

                if (/* status.equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID) && */postPaidRefuelBean.getFuelQuantity() != null) {
                    String stringFuelQuantity = numberFormat.format(postPaidRefuelBean.getFuelQuantity());
                    contentTable.add(new MailParameter("QUANTITA' EROGATA (lt)", stringFuelQuantity));
                }

                if (/* status.equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID) && */postPaidRefuelBean.getProductDescription() != null
                        && !postPaidRefuelBean.getProductDescription().isEmpty()) {
                    contentTable.add(new MailParameter("TIPO DI CARBURANTE", postPaidRefuelBean.getProductDescription()));
                }

                if (/* status.equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID) && */postPaidRefuelBean.getFuelAmount() != null) {
                    //contentTable.add(new MailParameter("IMPORTO CARBURANTE (&euro;)", currencyFormat.format(postPaidRefuelBean.getFuelAmount())));
                    if (status.equals(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED)) {
                        contentTable.add(new MailParameter("IMPORTO CARBURANTE ()", currencyFormat.format(-postPaidRefuelBean.getFuelAmount())));
                    }
                    else {
                        contentTable.add(new MailParameter("IMPORTO CARBURANTE ()", currencyFormat.format(postPaidRefuelBean.getFuelAmount())));
                    }
                }

            }
        }
        
        for(PostPaidTransactionAdditionalDataBean postPaidTransactionAdditionalDataBean : poPTransactionBean.getPostPaidTransactionAdditionalDataBeanList()) {
            String dataKey   = postPaidTransactionAdditionalDataBean.getDataKey();
            String dataValue = postPaidTransactionAdditionalDataBean.getDataValue();
            if (dataKey.equalsIgnoreCase("merchantAddress") ||
                dataKey.equalsIgnoreCase("cardType") ||
                dataKey.equalsIgnoreCase("merchantName") ||
                dataKey.equalsIgnoreCase("cardExpDt") ||
                dataKey.equalsIgnoreCase("productIDListString")) {
            }
            else {
                
                String dataLabel = dataKey;
                if (dataKey.equalsIgnoreCase("mcMaskedPan")) {
                    dataLabel = "CARTA MULTICARD";
                }
                else if (dataKey.equalsIgnoreCase("addInfo")) {
                    dataLabel = "INFO";
                }
                else if (dataKey.equalsIgnoreCase("km")) {
                    dataLabel = "KM";
                }
                contentTable.add(new MailParameter(dataLabel, dataValue));
            }
        }

        contentTableList.add(contentTable);
        List<MailParameter> secondContentTable = new ArrayList<MailParameter>(0);

        secondContentTable.add(new MailParameter("DATA TRANSAZIONE", date));
        secondContentTable.add(new MailParameter("CODICE TRANSAZIONE", poPTransactionBean.getMpTransactionID()));

        if (stringBankTransactionId != null && !stringBankTransactionId.equals("")) {
            secondContentTable.add(new MailParameter("NUMERO OPERAZIONE", stringBankTransactionId));
        }

        if (stringAuthorizationCode != null && !stringAuthorizationCode.equals("")) {
            secondContentTable.add(new MailParameter("CODICE AUTORIZZAZIONE", stringAuthorizationCode));
        }

        if (totalAmount > 0.0) {
            /*
            if (status.equals(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED)) {
                secondContentTable.add(new MailParameter("IMPORTO TOTALE ()", currencyFormat.format(-totalAmount)));
            }
            else {
                secondContentTable.add(new MailParameter("IMPORTO TOTALE ()", currencyFormat.format(totalAmount)));
            }
            */
            secondContentTable.add(new MailParameter("IMPORTO TOTALE ()", currencyFormat.format(totalAmount)));
        }
        contentTableList.add(secondContentTable);

        List<MailParameter> thirdContentTable = new ArrayList<MailParameter>(0);
        if (poPTransactionBean.getCartBean() != null) {

            double amount = 0.0;
            for (PostPaidCartBean postPaidCartBean : poPTransactionBean.getCartBean()) {
                double tmpAmount = 0.0;
                if (postPaidCartBean.getAmount() != null) {
                    tmpAmount = postPaidCartBean.getAmount();
                }
                amount += tmpAmount;
                /*
                 * if (postPaidCartBean.getProductDescription() != null && !postPaidCartBean.getProductDescription().isEmpty()) {
                 * contentTable.add(new MailParameter("PRODOTTO", postPaidCartBean.getProductDescription()));
                 * }
                 * 
                 * if (postPaidCartBean.getQuantity() != null) {
                 * String stringQuantity = numberFormat.format(postPaidCartBean.getQuantity());
                 * contentTable.add(new MailParameter("QUANTITA' (pz)", stringQuantity));
                 * }
                 * 
                 * if (postPaidCartBean.getAmount() != null) {
                 * String stringAmount = currencyFormat.format(postPaidCartBean.getAmount());
                 * contentTable.add(new MailParameter("IMPORTO PRODOTTO ()", stringAmount));
                 * }
                 */
            }

            if (amount > 0.0) {
                //contentTable.add(new MailParameter("IMPORTO SHOP (&euro;)", currencyFormat.format(amount)));
                thirdContentTable.add(new MailParameter("IMPORTO SHOP ()", currencyFormat.format(amount)));
            }
        }

        contentTableList.add(thirdContentTable);
        List<MailParameter> fourContentTable = new ArrayList<MailParameter>(0);

        if ((status.equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID) || status.equals(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED))
                && !poPTransactionBean.getPostPaidConsumeVoucherBeanList().isEmpty()) {

            double totalConsumed = 0.0;
            List<Object[]> voucherDetailList = new ArrayList<Object[]>(0);

            for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : poPTransactionBean.getPostPaidConsumeVoucherBeanList()) {

                if (postPaidConsumeVoucherBean.getWarningMsg() != null) {
                    //String value = postPaidConsumeVoucherBean.getWarningMsg();
                    //String label = "";
                    String value = currencyFormat.format(0.0);
                    String label = postPaidConsumeVoucherBean.getWarningMsg();
                    Object[] detail = new Object[] { label, value };
                    voucherDetailList.add(detail);
                }

                if (postPaidConsumeVoucherBean != null
                        && postPaidConsumeVoucherBean.getTotalConsumed() != null
                        && !postPaidConsumeVoucherBean.getTotalConsumed().equals(new Double(0))
                        && ((postPaidConsumeVoucherBean.getPostPaidTransactionBean().getMpTransactionStatus().equals("PAID") || postPaidConsumeVoucherBean.getPostPaidTransactionBean().getMpTransactionStatus().equals(
                                "0013")))) {

                    totalConsumed += postPaidConsumeVoucherBean.getTotalConsumed().doubleValue();

                    for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {
                        if (postPaidConsumeVoucherDetailBean.getConsumedValue() == null || postPaidConsumeVoucherDetailBean.getConsumedValue().equals(0.0)) {
                            continue;
                        }

                        String value = currencyFormat.format(postPaidConsumeVoucherDetailBean.getConsumedValue());
                        String label = "";

                        if (postPaidConsumeVoucherDetailBean.getPromoDescription() != null) {
                            label = postPaidConsumeVoucherDetailBean.getPromoDescription() + "\nCodice: " + postPaidConsumeVoucherDetailBean.getVoucherCode();

                        }

                        if (postPaidConsumeVoucherBean.getMarketingMsg() != null) {
                            //label += "<br>" + postPaidConsumeVoucherBean.getMarketingMsg();
                            label += "\n\n" + postPaidConsumeVoucherBean.getMarketingMsg();
                        }

                        Object[] detail = new Object[] { label, value };
                        voucherDetailList.add(detail);
                    }
                }

                else if (postPaidConsumeVoucherBean != null
                        && ((postPaidConsumeVoucherBean.getPostPaidTransactionBean().getMpTransactionStatus().equals("REVERSED") || postPaidConsumeVoucherBean.getPostPaidTransactionBean().getMpTransactionStatus().equals(
                                "0015")))) {
                    totalConsumed -= postPaidConsumeVoucherBean.getPostPaidTransactionBean().getAmount().doubleValue();

                    for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {
                        if (postPaidConsumeVoucherDetailBean.getConsumedValue() == null || postPaidConsumeVoucherDetailBean.getConsumedValue().equals(0.0)) {
                            continue;
                        }

                        String value = currencyFormat.format(-postPaidConsumeVoucherDetailBean.getConsumedValue());
                        String label = "";

                        if (postPaidConsumeVoucherDetailBean.getPromoDescription() != null) {
                            label = postPaidConsumeVoucherDetailBean.getPromoDescription() + "\nCodice: " + postPaidConsumeVoucherDetailBean.getVoucherCode();

                        }

                        if (postPaidConsumeVoucherBean.getMarketingMsg() != null) {
                            //label += "<br>" + postPaidConsumeVoucherBean.getMarketingMsg();
                            label += "\n\n" + postPaidConsumeVoucherBean.getMarketingMsg();
                        }

                        Object[] detail = new Object[] { label, value };
                        voucherDetailList.add(detail);
                    }
                }
            }

            if (totalConsumed > 0.0 || !voucherDetailList.isEmpty()) {

                //contentTable.add(new MailParameter("VOUCHER (&euro;)", ""));
                fourContentTable.add(new MailParameter("DETTAGLIO VOUCHER CONSUMATI ()", ""));
                bankAmount -= totalConsumed;

                for (Object[] voucherDetail : voucherDetailList) {
                    fourContentTable.add(new MailParameter(voucherDetail[0].toString(), voucherDetail[1].toString(), 1));
                }
            }

        }

        contentTableList.add(fourContentTable);
        List<MailParameter> fifthContentTable = new ArrayList<MailParameter>(0);

        if (status.equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID) && !poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().isEmpty()) {

            Integer totalCredits = 0;
            //List<String[][]> loyaltyDetailList = new ArrayList<String[][]>(0);
            List<ArrayList<String[]>> loyaltyDetailList = new ArrayList<ArrayList<String[]>>(0);

            for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {

                if (postPaidLoadLoyaltyCreditsBean != null
                        /*&& postPaidLoadLoyaltyCreditsBean.getCredits() != null*/
                        && (postPaidLoadLoyaltyCreditsBean.getPostPaidTransactionBean().getMpTransactionStatus().equals("PAID") || postPaidLoadLoyaltyCreditsBean.getPostPaidTransactionBean().getMpTransactionStatus().equals(
                                "0013"))) {

                    String labelPan = "CODICE";
                    String labelPoint = "PUNTI";
                    String labelBalance = "SALDO PUNTI";
                    String labelWarning = "";
                    String valuePan = null;
                    String valuePoint = null;
                    String valueBalance = null;
                    String valueWarning = null;

                    if (postPaidLoadLoyaltyCreditsBean.getEanCode() != null) {
                        valuePan = postPaidLoadLoyaltyCreditsBean.getEanCode();
                    }

                    if (postPaidLoadLoyaltyCreditsBean.getBalance() != null) {
                        valueBalance = postPaidLoadLoyaltyCreditsBean.getBalance().toString();
                    }

                    if (postPaidLoadLoyaltyCreditsBean.getCredits() != null) {
                        totalCredits += postPaidLoadLoyaltyCreditsBean.getCredits();
                        valuePoint = postPaidLoadLoyaltyCreditsBean.getCredits().toString();
                    }

                    if (!postPaidLoadLoyaltyCreditsBean.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {
                        if (postPaidLoadLoyaltyCreditsBean.getMessageCode() != null) {
                            valueWarning = postPaidLoadLoyaltyCreditsBean.getMessageCode();
                        }
                        else if (postPaidLoadLoyaltyCreditsBean.getWarningMsg() != null) {
                            valueWarning = postPaidLoadLoyaltyCreditsBean.getWarningMsg();
                        }

                        valuePoint = null;
                    }

                    if (postPaidLoadLoyaltyCreditsBean.getWarningMsg() != null) {
                        valueWarning = postPaidLoadLoyaltyCreditsBean.getWarningMsg();
                    }

                    if (valuePoint != null) {
                        ArrayList<String[]> detail = new ArrayList<String[]>(0);
                        detail.add(new String[] { labelPan, valuePan });
                        detail.add(new String[] { labelPoint, valuePoint });
                        detail.add(new String[] { labelBalance, valueBalance });

                        loyaltyDetailList.add(detail);
                    }

                    if (valueWarning != null) {
                        ArrayList<String[]> detail = new ArrayList<String[]>(0);
                        detail.add(new String[] { labelWarning, valueWarning });
                        loyaltyDetailList.add(detail);
                    }
                }
            }

            if (!loyaltyDetailList.isEmpty()) {
                fifthContentTable.add(new MailParameter("PUNTI FEDELTA'", ""));

                for (ArrayList<String[]> loyaltyDetail : loyaltyDetailList) {
                    for (String[] detail : loyaltyDetail) {
                        fifthContentTable.add(new MailParameter(detail[0], detail[1], 1));
                    }
                }
                /*
                 * for (String[][] loyaltyDetail : loyaltyDetailList) {
                 * for (String[] detail : loyaltyDetail) {
                 * contentTable.add(new MailParameter(detail[0], detail[1], 1));
                 * }
                 * }
                 */
            }
        }
        contentTableList.add(fifthContentTable);
        List<MailParameter> sixtyContentTable = new ArrayList<MailParameter>(0);
        if (poPTransactionBean.getMpTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID) &&(poPTransactionBean.getPaymentMethodType() == null || 
                poPTransactionBean.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD))) {

            sixtyContentTable.add(new MailParameter("CARTA DI PAGAMENTO", stringPan));

            if (stringBankTransactionId != null && !stringBankTransactionId.equals("")) {
                sixtyContentTable.add(new MailParameter("NUMERO OPERAZIONE BANCARIA", stringBankTransactionId));
            }

            if (stringAuthorizationCode != null && !stringAuthorizationCode.equals("")) {
                sixtyContentTable.add(new MailParameter("CODICE AUTORIZZAZIONE", stringAuthorizationCode));
            }

            if (bankAmount > 0) {
                sixtyContentTable.add(new MailParameter("IMPORTO ADDEBITATO ()", currencyFormat.format(bankAmount)));

            }
        }

        //        if (status.equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID)) {
        //            //contentTable.add(new MailParameter("IMPORTO ADDEBITATO (&euro;)", currencyFormat.format(bankAmount)));
        //            contentTable.add(new MailParameter("SEPARATOR"));
        //            //contentTable.add(new MailParameter("IMPORTO TOTALE (&euro;)", currencyFormat.format(totalAmount)));
        //            contentTable.add(new MailParameter("IMPORTO TOTALE ()", currencyFormat.format(totalAmount)));
        //        }
        contentTableList.add(sixtyContentTable);

        List<Parameter> bodyContent = new ArrayList<Parameter>(0);

        String fileStylePath = System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "mail" + File.separator + "mail_pdf.properties";
        //String keyFrom = emailSender.getSender();
        String keySubject = "Eni Pay";
        String keyAttachmentName = "enipay";
        
        
        if (newAcquirerFlow) {
            fileStylePath = System.getProperty("jboss.home.dir") + File.separator + "content" + File.separator + "mail" + File.separator + "mail_pdf_new.properties";
            
            if (stringSubstitution != null) {
                //keyFrom = stringSubstitution.getValue(keyFrom, 1);
                keySubject = stringSubstitution.getValue(keySubject, 1);
                keyAttachmentName = stringSubstitution.getValue(keyAttachmentName, 1);
            }
        }
        
        //System.out.println("keyFrom: " + keyFrom);
        System.out.println("keySubject: " + keySubject);
        System.out.println("keyAttachmentName: " + keyAttachmentName);
        
        bodyContent.add(new Parameter("NAME", messageName));
        bodyContent.add(new Parameter("MESSAGE", messageStatus));

        try {
            String dateBody = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
            String dateFilename = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            byte[] pdfBytes = createPdf(fileStylePath, headerTable, contentTableList, proxyHost, proxyPort, proxyNoHosts);
            List<Attachment> attachments = new ArrayList<Attachment>(0);

            Attachment attachment = new Attachment();
            attachment.setFileName("transazione_" + keyAttachmentName + "_" + dateFilename + ".pdf");
            attachment.setBytes(pdfBytes);
            attachments.add(attachment);

            //System.out.println("Sending email to " + to);

            String subject = keySubject + " - transazione del " + dateBody;
            //String result = emailSender.sendEmail(emailType, to, null, null, subject, bodyContent);
            //String result = emailSender.sendEmailWithAttachments(emailType, keyFrom, to, subject, bodyContent, attachments);

            return pdfBytes;
            //return (result.equals("SEND_MAIL_200"));
        }
        catch (DocumentException ex) {
            System.err.println("Errore nella creazione dello scontrino in formato pdf: " + ex.getMessage());
            //return new byte[]{};
            return null;
        }
    }
}
