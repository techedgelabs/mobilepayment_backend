package com.techedge.mp.core.actions.user;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.EmailDomainBean;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.LoyaltyCardMatcherBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.PrefixNumberBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VerificationCodeBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.FiscalCodeHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnableLoyaltyCardResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserCreateAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private UserCategoryService userCategoryService;

    public UserCreateAction() {}

    public String execute(String ticketId, String requestId, User user, Integer userType, Long loyaltyCardId, String checkFiscalCode, Integer verificationCodeExpiryTime,
            String activationLink, Double initialCap, Boolean createUserMobilePhoneMandatory, Boolean checkMobilePhoneUnique, EmailSenderRemote emailSender,
            FidelityServiceRemote fidelityService, String checkMailInBlacklist) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isServiceTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_CREATE_INVALID_TICKET;
            }

            // Verifica che non esista gi� un utente con quella email
            UserBean oldUserBean = QueryRepository.findUserCustomerByEmail(em, user.getPersonalData().getSecurityDataEmail());

            if (oldUserBean != null) {

                // Esiste gi� un utente con quella email
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User exists");

                userTransaction.commit();

                return ResponseHelper.USER_CREATE_USER_EXISTS;
            }

            System.out.println("Check codice fiscale - stato: " + checkFiscalCode);

            if (checkFiscalCode.equals("true")) {

                // Verifica che non esista gi� un utente con quel codice fiscale
                UserBean oldUserBeanFC = QueryRepository.findUserCustomerByFiscalCode(em, user.getPersonalData().getFiscalCode());

                if (oldUserBeanFC != null) {

                    // Esiste gi� un utente con quel codice fiscale
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "FiscalCode exists");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_FISCALCODE_EXISTS;
                }

                // Verifica che il codice fiscale sia compatibile con i dati anagrafici inseriti
                String nome = user.getPersonalData().getFirstName();
                String cognome = user.getPersonalData().getLastName();
                String sesso = user.getPersonalData().getSex();

                Calendar calendar = new GregorianCalendar();
                calendar.setTime(user.getPersonalData().getBirthDate());
                int giorno = calendar.get(Calendar.DAY_OF_MONTH);
                int mese = calendar.get(Calendar.MONTH) + 1;
                int anno = calendar.get(Calendar.YEAR);

                FiscalCodeHelper fiscalCodeHelper = new FiscalCodeHelper(nome, cognome, giorno, mese, anno, sesso);

                Boolean check = fiscalCodeHelper.verificaAnagrafica(user.getPersonalData().getFiscalCode());

                System.out.println(fiscalCodeHelper.toString());

                if (check != Boolean.TRUE) {

                    // Il codice fiscale non � compatibile con i dati anagrafici
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "FiscalCode mismatch");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_FISCALCODE_MISMATCH;
                }
            }

            //CONTROLLO MAIL IN BLACKLIST
            if (checkMailInBlacklist.equals("true")) {
                String[] splits = user.getPersonalData().getSecurityDataEmail().split("@");

                EmailDomainBean emailDomainBean = null;
                if (splits[1] != null) {
                    emailDomainBean = QueryRepository.findEmailDomainByEmail(em, splits[1]);
                }
                if (emailDomainBean != null) {
                    // Esiste gi� un utente con quel codice fiscale
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid mail");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_INVALID_MAIL;
                }
            }

            if (createUserMobilePhoneMandatory) {
                if (!userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode())) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User ctegory not new flow");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_NOT_ENABLE;
                }

                if (user.getMobilePhoneList().isEmpty()) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone not found");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_MOBILE_PHONE_NOT_FOUND;
                }

                if (user.getMobilePhoneList().size() > 1) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone size > 1");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_MOBILE_PHONE_INVALID_LIST_SIZE;
                }

                MobilePhone mobilePhone = user.getMobilePhoneList().get(0);

                if (mobilePhone.getPrefix() == null || mobilePhone.getNumber() == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone data invalid");

                    userTransaction.commit();

                    return ResponseHelper.USER_CREATE_MOBILE_PHONE_INVALID;
                }

                /*
                 * if (!MobilePhone.isValidPrefix(mobilePhone.getPrefix()) || !MobilePhone.isValidNumber(mobilePhone.getNumber())) {
                 * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone data invalid");
                 * 
                 * userTransaction.commit();
                 * 
                 * return ResponseHelper.USER_CREATE_MOBILE_PHONE_INVALID;
                 * }
                 */

                // Verifica che il prefisso sia valido
                PrefixNumberBean prefixNumberBean = QueryRepository.findPrefixByCode(em, mobilePhone.getPrefix());
                if (prefixNumberBean == null) {

                    // Prefisso non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Prefix " + mobilePhone.getPrefix() + " not valid");

                    userTransaction.commit();

                    return ResponseHelper.MOBILE_PHONE_UPDATE_INVALID_PREFIX;
                }

                if (checkMobilePhoneUnique) {
                    /* Il controllo deve essere fatto solo su numeri di telefono associati ad utenti che hanno gi� verificato l'email
                    MobilePhoneBean mobilePhoneExist = QueryRepository.findMobilePhoneByNumberAndPrefix(em, mobilePhone.getNumber(), mobilePhone.getPrefix());
                    if (mobilePhoneExist != null) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone exists");
                        userTransaction.commit();
                        return ResponseHelper.USER_CREATE_MOBILE_PHONE_EXISTS;
                    }
                    */

                    Boolean mobilePhoneFound = false;
                    List<MobilePhoneBean> mobilePhoneBeanList = QueryRepository.findAllActiveMobilePhoneByNumberAndPrefix(em, mobilePhone.getNumber(), mobilePhone.getPrefix());

                    if (mobilePhoneBeanList != null && !mobilePhoneBeanList.isEmpty()) {

                        for (MobilePhoneBean mobilePhoneBean : mobilePhoneBeanList) {

                            System.out.println("Trovato telefono per utente in stato " + mobilePhoneBean.getUser().getUserStatus());

                            if (mobilePhoneBean.getUser().getUserStatus() != User.USER_STATUS_NEW) {

                                System.out.println("Il telefono � gi� stato usato da un utente che ha verificato l'email");
                                mobilePhoneFound = true;
                                break;
                            }
                        }
                    }

                    if (mobilePhoneFound) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Mobile phone exists");
                        userTransaction.commit();
                        return ResponseHelper.USER_CREATE_MOBILE_PHONE_EXISTS;
                    }

                }

                user.getMobilePhoneList().get(0).setStatus(MobilePhone.MOBILE_PHONE_STATUS_PENDING);
                //String verificationCode = new IdGenerator().generateId(32).substring(0, 8).toUpperCase();
                String verificationCode = new IdGenerator().generateNumericId(8);
                System.out.println("Generato codice di verifica " + verificationCode + " per numero di telefono " + user.getMobilePhoneList().get(0).getNumber());
                user.getMobilePhoneList().get(0).setVerificationCode(verificationCode);

            }
            else {
                //Non essendo previsto da parametro il controllo del cellulare elimino il dato per evitare dati non corretti sul database
                System.out.println("Controllo cellulare non impostato elimino i dati per evitare dati non corretti sul database");
                if (!user.getMobilePhoneList().isEmpty()) {
                    user.getMobilePhoneList().clear();
                }
            }
            // Verifica la validit� dei dati dell'utente che si sta creando
            // TODO
            user.setUserType(User.USER_TYPE_CUSTOMER);
            // Crea l'utente
            UserBean userBean = UserBean.createNewCustomerUser(user, initialCap);

            if (!userBean.getMobilePhoneList().isEmpty()) {
                Date now = new Date();
                Date expiryDate = DateHelper.addMinutesToDate(verificationCodeExpiryTime, now);

                for (MobilePhoneBean mobilePhoneBean : userBean.getMobilePhoneList()) {
                    mobilePhoneBean.setCreationTimestamp(now);
                    mobilePhoneBean.setLastUsedTimestamp(now);
                    mobilePhoneBean.setExpirationTimestamp(expiryDate);
                }
            }

            // Salva l'utente su db
            em.persist(userBean);

            if (loyaltyCardId != null) {

                System.out.println("Richiesta associazione carta loyalty con id " + loyaltyCardId);

                // Associazione carta loyalty
                LoyaltyCardMatcherBean loyaltyCardMatcherBean = QueryRepository.findLoyaltyCardMatcherById(em, loyaltyCardId);

                if (loyaltyCardMatcherBean == null) {

                    // Carta loyalty non trovata
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "FiscalCode exists");
                }
                else {

                    String panCode = loyaltyCardMatcherBean.getPanCode();

                    System.out.println("Verifica carta loyalty " + panCode);

                    // TODO - Controlla se la carta � gi� associata a un altro utente - necessario?

                    Date now = new Date();

                    String operationID = new IdGenerator().generateId(16).substring(0, 33);
                    String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                    Boolean enable = true;
                    PartnerType partnerType = PartnerType.MP;
                    Long requestTimestamp = now.getTime();

                    EnableLoyaltyCardResult enableLoyaltyCardResult = new EnableLoyaltyCardResult();

                    try {

                        enableLoyaltyCardResult = fidelityService.enableLoyaltyCard(operationID, fiscalCode, panCode, enable, partnerType, requestTimestamp);
                    }
                    catch (Exception e) {

                        // Errore nell'associazione della carta loyalty
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Error enabling loyalty card");
                    }

                    // Verifica l'esito del check
                    String enableLoyaltyCardStatusCode = enableLoyaltyCardResult.getStatusCode();

                    if (!enableLoyaltyCardStatusCode.equals(FidelityResponse.ENABLE_LOYALTY_CARD_OK)) {

                        // Errore nell'associazione della carta loyalty
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error enabling loyalty card: "
                                + enableLoyaltyCardStatusCode);
                    }
                    else {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Associazione loyalty card: " + panCode);

                        // Associa la nuova carta loyalty all'utente
                        LoyaltyCardBean loyaltyCardBean = new LoyaltyCardBean();

                        loyaltyCardBean.setPanCode(panCode);
                        loyaltyCardBean.setEanCode("");
                        loyaltyCardBean.setStatus(LoyaltyCard.LOYALTY_CARD_STATUS_VALIDA);

                        loyaltyCardBean.setUserBean(userBean);

                        em.persist(loyaltyCardBean);

                        // Salva l'utente su db
                        em.merge(userBean);

                    }
                }
            }

            // Genera il codice per la verifica dell'email
            Date now = new Date();
            Date lastUsed = now;
            Date expiryDate = DateHelper.addMinutesToDate(verificationCodeExpiryTime, now);

            String newVerificationCodeId = new IdGenerator().generateId(10).substring(0, 10);

            VerificationCodeBean verificationCodeBean = new VerificationCodeBean(newVerificationCodeId, userBean, VerificationCodeBean.STATUS_NEW, now, lastUsed, expiryDate);

            em.persist(verificationCodeBean);

            // Invia l'email con il codice di attivazione
            if (emailSender != null) {

                EmailType emailType = EmailType.CONFIRM_EMAIL;
                String to = user.getPersonalData().getSecurityDataEmail();
                List<Parameter> parameters = new ArrayList<Parameter>(0);

                parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
                parameters.add(new Parameter("ACTIVATION_LINK", activationLink));
                parameters.add(new Parameter("VERIFICATION_CODE", newVerificationCodeId));
                parameters.add(new Parameter("EMAIL", URLEncoder.encode(userBean.getPersonalDataBean().getSecurityDataEmail(), "UTF-8")));

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Sending email to "
                        + user.getPersonalData().getSecurityDataEmail());

                String result = emailSender.sendEmail(emailType, to, parameters);

                //String result = "disabled";

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "SendEmail result: " + result);

            }

            // Rinnova il ticket
            ticketBean.renew();
            em.merge(ticketBean);

            userTransaction.commit();

            return ResponseHelper.USER_CREATE_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
