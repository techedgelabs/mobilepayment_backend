package com.techedge.mp.core.actions.user.v2;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.utilities.AsyncDWHService;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2UpdateUsersDataAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2UpdateUsersDataAction() {}

    public String execute(String ticketId, String requestId, Integer reconciliationMaxAttempts, HashMap<String, Object> usersData) throws EJBException {

        final UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_V2_UPDATE_USERS_DATA_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_V2_UPDATE_USERS_DATA_INVALID_TICKET;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update user fields in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_V2_UPDATE_USERS_DATA_UNAUTHORIZED;
            }

            for (String key : usersData.keySet()) {
                if (key.equals("virtualizationCompleted")) {
                    userBean.setVirtualizationCompleted(true);
                    userBean.setVirtualizationTimestamp(new Date());
                }
            }

            em.merge(userBean);

            if (userBean.getUserStatusRegistrationCompleted()) {
                
                if (!userBean.getUserType().equals(User.USER_TYPE_GUEST)) {
                    
                    List<TermsOfServiceBean> listTermOfService = QueryRepository.findTermOfServiceByPersonalDataId(em, userBean.getPersonalDataBean());
                    
                    if (listTermOfService == null) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "flagPrivacy null");
                        userTransaction.commit();
                        return ResponseHelper.USER_CHECK_FAILURE;
                    }
    
                    
                    List<MobilePhoneBean> listMobilePhone = new ArrayList<MobilePhoneBean>();
                    String codCard = null;
                    
                    for (MobilePhoneBean item : userBean.getMobilePhoneList()) {
                        listMobilePhone.add(item);
                    }
                    
                    for (LoyaltyCardBean item : userBean.getLoyaltyCardList()) {
                        if (item.getStatus().equals(LoyaltyCard.LOYALTY_CARD_STATUS_VALIDA)) {
                            codCard = item.getPanCode();
                        }
                    }
                    
                    List<UserSocialDataBean> userSocialDataList = new ArrayList<UserSocialDataBean>();
                    
                    for (UserSocialDataBean item : userBean.getUserSocialData()) {
                        userSocialDataList.add(item);
                    }
                    
                    AsyncDWHService asyncDWHService = new AsyncDWHService(userTransaction, em, userBean, listMobilePhone, listTermOfService, userSocialDataList, requestId, codCard, reconciliationMaxAttempts);
                    
                    new Thread(asyncDWHService, "setUserDataPlus (UserV2UpdateUsersDataAction)").start();
                }
            }

            userTransaction.commit();

            return ResponseHelper.USER_V2_UPDATE_USERS_DATA_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED password update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
