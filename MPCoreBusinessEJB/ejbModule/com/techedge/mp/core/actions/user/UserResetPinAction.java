package com.techedge.mp.core.actions.user;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.ResetPinCodeBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.PinHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserResetPinAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserResetPinAction() {}

    public String execute(String ticketId, String requestId, String password, String newPin, Integer pinCheckMaxAttempts, UserCategoryService userCategoryService)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_RESET_PIN_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_RESET_PIN_INVALID_TICKET;
            }
            
            if (userBean.getUserType().equals(User.USER_TYPE_GUEST)) {

                // Gli utenti guest non possono resettare il pin
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid userType: " + userBean.getUserType());

                userTransaction.commit();

                return ResponseHelper.USER_RESET_PIN_UNAUTHORIZED;
            }
            
            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update user pin in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_RESET_PIN_UNAUTHORIZED;

            }
            
            // Il servizio di reset del pin pu� essere chiamato solo dagli utenti che eseguono il nuovo flusso voucher

            Integer userType = userBean.getUserType();
            String userCategoryNewFlow = UserCategoryType.NEW_PAYMENT_FLOW.getCode();
            String userCategoryNewAcquirer = UserCategoryType.NEW_ACQUIRER_FLOW.getCode();
            Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, userCategoryNewFlow);
            Boolean useNewAcquirer = userCategoryService.isUserTypeInUserCategory(userType, userCategoryNewAcquirer);
            Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());

            if (!useNewFlow && !useNewAcquirer && !useBusiness) {

                // Un utente che esegue il vecchio flusso voucher non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to reset pin for user of type " + userType);

                userTransaction.commit();

                return ResponseHelper.USER_RESET_PIN_UNAUTHORIZED;
            }

            System.out.println("User type: " + userType + "  User new acquirer: " + useNewAcquirer + "    User new flow: " + useNewFlow);

            
            String socialProvider = userBean.getSocialProvider();
            
            if (socialProvider != null) {
            
                // Se l'utente � di tipo social bisogna verificare che il campo password sia uguale al campo code della tabella ResetPinCodeBean
                ResetPinCodeBean resetPinCodeBean = QueryRepository.findValidResetPinCode(em, password, userBean);
                
                if (resetPinCodeBean == null) {
                    
                    // Il codice di reset inserito non � valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong reset code");
    
                    userTransaction.commit();
    
                    return ResponseHelper.USER_RESET_PIN_WRONG_CODE;
                }
            }
            else {
                
                // altrimenti bisogna verificare che sia uguale alla password dell'utente
                if (!userBean.getPersonalDataBean().getSecurityDataPassword().equals(password)) {
    
                    // La password inserita � errata
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Wrong password");
    
                    userTransaction.commit();
    
                    return ResponseHelper.USER_RESET_PIN_WRONG_PASSWORD;
                }
            }

            // Se l'utente ha gi� un pin bisogna cancellarlo
            PaymentInfoBean voucherPaymentInfoBean = userBean.getVoucherPaymentMethod();

            if (voucherPaymentInfoBean != null && voucherPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                // Imposta lo stato del metodo di pagamento credit_voucher a CANCELED
                voucherPaymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                voucherPaymentInfoBean.setDefaultMethod(false);
                em.merge(voucherPaymentInfoBean);
            }

            String checkPin = PinHelper.checkPin(newPin, null);
            if (!checkPin.equals("OK")) {
                // Il pin inserito non � sufficientemente sicuro
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not strong");
                userTransaction.commit();
                return ResponseHelper.USER_INSERT_PAYMENT_METHOD_INVALID_PIN;
            }
            String encodedNewPin = EncoderHelper.encode(newPin);

            // Se l'utente non ha ancora nessun metodo di pagamento di default associato allora quello inserito diventa quello di default
            Boolean defaultMethod = false;
            if (userBean.findDefaultPaymentInfoBean() == null) {
                defaultMethod = true;
            }
            else {
                if (userBean.findDefaultPaymentInfoBean().getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {
                    defaultMethod = false;
                }
            }

            if (defaultMethod == true) {
                System.out.println("Non � stato trovato un metodo di pagamento di default");
            }

            System.out.println("Inserimento credit voucher");

            // Bisogna verificare che l'utente non abbia gi� associato il pin

            String encodedPin = userBean.getEncodedPin();

            if (encodedPin != null && !encodedPin.equals("")) {

                // Errore Pin gi� inserito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin already inserted");

                userTransaction.commit();

                return ResponseHelper.USER_INSERT_PAYMENT_METHOD_PIN_EXISTS;
            }

            // Imposta lo stato di tutti gli altri metodi di pagamento in stato 1 e 2 a CANCELED
            for (PaymentInfoBean oldPaymentInfoBean : userBean.getPaymentData()) {

                if (oldPaymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                        && (oldPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || oldPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                    oldPaymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                    oldPaymentInfoBean.setDefaultMethod(false);
                    em.merge(oldPaymentInfoBean);
                }
            }
            PaymentInfoBean paymentInfoBean = null;
            // Crea il nuovo metodo di pagamento
            paymentInfoBean = userBean.addNewPendingPaymentInfo(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER, encodedNewPin, Boolean.FALSE, pinCheckMaxAttempts, Double.valueOf(0),
                    "", "242", "", "");

            paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED);

            // Aggiorna le informazioni sui metodi di pagamento su db
            if (defaultMethod == true) {

                for (PaymentInfoBean paymentInfoBeanTemp : userBean.getPaymentData()) {

                    if (paymentInfoBeanTemp.getId() != paymentInfoBean.getId()) {

                        em.persist(paymentInfoBeanTemp);
                    }
                }
            }

            System.out.println("Salvataggio nuovo metodo di pagamento");

            // Memorizza su db l'informazione sulla carta
            em.persist(paymentInfoBean);
            em.merge(userBean);

            // Rinnova il ticket
            //logger.log(Level.INFO, "Rinnova il ticket");
            ticketBean.renew();
            em.merge(ticketBean);
            userTransaction.commit();

            return ResponseHelper.USER_RESET_PIN_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED pin update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
