package com.techedge.mp.core.actions.user.v2;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GetHomePartnerListResult;
import com.techedge.mp.core.business.interfaces.HomePartner;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserV2GetHomePartnerListAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserV2GetHomePartnerListAction() {}

    public GetHomePartnerListResult execute(String ticketId, String requestId, ParametersService parametersService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        GetHomePartnerListResult getHomePartnerListResult = new GetHomePartnerListResult();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid()) { // || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                getHomePartnerListResult.setStatusCode(ResponseHelper.USER_V2_GET_PARTNER_LIST_INVALID_TICKET);
                return getHomePartnerListResult;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                getHomePartnerListResult.setStatusCode(ResponseHelper.USER_V2_GET_PARTNER_LIST_INVALID_TICKET);
                return getHomePartnerListResult;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED && userStatus != User.USER_STATUS_TEMPORARY_PASSWORD) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to retrieve partner list, user in status "
                        + userStatus);

                userTransaction.commit();

                getHomePartnerListResult.setStatusCode(ResponseHelper.USER_V2_GET_PARTNER_LIST_UNAUTHORIZED);
                return getHomePartnerListResult;
            }
            
            List<HomePartner> homePartnerList = parametersService.getHomePartnerList();
            
            for(HomePartner homePartner : homePartnerList) {
                getHomePartnerListResult.getHomePartnerList().add(homePartner);
            }

            getHomePartnerListResult.setStatusCode(ResponseHelper.USER_V2_GET_PARTNER_LIST_SUCCESS);

            userTransaction.commit();

            return getHomePartnerListResult;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get home partner with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
