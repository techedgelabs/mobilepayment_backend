package com.techedge.mp.core.actions.user;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserSkipPaymentMethodConfigurationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

//    @SuppressWarnings("serial")
//    private HashMap<String, String> voucherStatus = new HashMap<String, String>() {
//                                                      {
//                                                          put(Voucher.VOUCHER_STATUS_ANNULLATO, ResponseHelper.USER_LOAD_VOUCHER_CANCELED);
//                                                          put(Voucher.VOUCHER_STATUS_CANCELLATO, ResponseHelper.USER_LOAD_VOUCHER_REMOVED);
//                                                          put(Voucher.VOUCHER_STATUS_DA_CONFERMARE, ResponseHelper.USER_LOAD_VOUCHER_TO_CONFIRM);
//                                                          put(Voucher.VOUCHER_STATUS_ESAURITO, ResponseHelper.USER_LOAD_VOUCHER_SPENT);
//                                                          put(Voucher.VOUCHER_STATUS_INESISTENTE, ResponseHelper.USER_LOAD_VOUCHER_INEXISTENT);
//                                                          put(Voucher.VOUCHER_STATUS_SCADUTO, ResponseHelper.USER_LOAD_VOUCHER_EXPIRED);
//                                                          put(Voucher.VOUCHER_STATUS_VALIDO, ResponseHelper.USER_LOAD_VOUCHER_VALID);
//                                                      }
//                                                  };

    public UserSkipPaymentMethodConfigurationAction() {}

    public String execute(String ticketId, String requestId, UserService userService, UserCategoryService userCategoryService, EmailSenderRemote emailSender) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_SKIP_PAYMENT_METHOD_CONFIGURATION_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_SKIP_PAYMENT_METHOD_CONFIGURATION_INVALID_TICKET;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to skip payment method configuration in status "
                        + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_SKIP_PAYMENT_METHOD_CONFIGURATION_UNAUTHORIZED;

            }

            // Il servizio per lo skip della configurazione del metodo di pagamento pu� essere chiamato solo dagli utenti che eseguono il nuovo flusso voucher

            Integer userType = userBean.getUserType();
            Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());
            Boolean useNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());

            if (!useNewFlow && !useNewAcquirerFlow && !useBusiness) {

                // Un utente che esegue il vecchio flusso voucher non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Unable to skip payment method configuration for user of type " + userType);

                userTransaction.commit();

                return ResponseHelper.USER_SKIP_PAYMENT_METHOD_CONFIGURATION_UNAUTHORIZED;
            }

            System.out.println("Utilizzo nuovo flusso voucher");

            // Verifica che l'utente abbia gi� inserito il pin 
            PaymentInfoBean voucherPaymentInfoBean = userBean.getVoucherPaymentMethod();

            if (voucherPaymentInfoBean == null || voucherPaymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not present");

                userTransaction.commit();

                return ResponseHelper.USER_SKIP_PAYMENT_METHOD_CONFIGURATION_UNAUTHORIZED;
            }

            // Se l'utente appartiene alla nuova categoria e ha il flag oldUser a true bisogna impostare oldUser a false
            if (useNewFlow && userBean.getOldUser()) {

                System.out.println("L'utente ha completato la migrazione da vecchio a nuovo");
                userBean.setOldUser(false);
            }

            if (useNewAcquirerFlow || useBusiness) {
                
                // Se l'utente appartiene al flusso per il nuovo acquirer bisogna impostare il flag che indica che lo step del deposito carta � stato superato
                if (userBean.getDepositCardStepCompleted() == null || !userBean.getDepositCardStepCompleted()) {
                    
                    System.out.println("Skip deposito carta di pagamento -> imposta il flag depositCardStepCompleted a true");
                    
                    userBean.setDepositCardStepCompleted(Boolean.TRUE);
                    userBean.setDepositCardStepTimestamp(new Date());
                    
                    em.merge(userBean);
                }
                else {
                    
                    // anomalia
                    System.out.println("Chiamata anomala: eseguito skip deposito carta di pagamento con flag depositCardStepCompleted gi� impostato a true");
                }
            }
            else {
                
                // Imposta il flag di registrazione completata a true
                Boolean firstRegistration = userBean.setRegistrationCompleted();
                
                if (firstRegistration) {
                    em.merge(userBean);
    
                    //System.out.println("firstRegistration");
    
                    //sendEmail(emailSender, userBean, requestId);
                }
            }

            // Aggiorna i dati dell'utente
            em.merge(userBean);

            // Rinnova il ticket
            ticketBean.renew();
            em.merge(ticketBean);

            userTransaction.commit();

            return ResponseHelper.USER_SKIP_PAYMENT_METHOD_CONFIGURATION_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED pin update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
