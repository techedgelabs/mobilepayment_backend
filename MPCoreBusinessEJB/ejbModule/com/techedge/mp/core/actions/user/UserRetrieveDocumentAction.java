package com.techedge.mp.core.actions.user;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveDocumentData;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.document.encoder.business.DocumentServiceRemote;
import com.techedge.mp.document.encoder.business.interfaces.DocumentDetails;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserRetrieveDocumentAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserRetrieveDocumentAction() {}

    public RetrieveDocumentData execute(String ticketId, String requestId, String documentId, DocumentServiceRemote documentService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                RetrieveDocumentData retrieveDocumentData = new RetrieveDocumentData();
                retrieveDocumentData.setStatusCode(ResponseHelper.USER_RETRIEVE_DOCUMENT_INVALID_TICKET);
                return retrieveDocumentData;
            }

            if (documentId == null) {

                // Chiave di ricerca non valida
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid document Id");

                userTransaction.commit();

                RetrieveDocumentData retrieveDocumentData = new RetrieveDocumentData();
                retrieveDocumentData.setStatusCode(ResponseHelper.USER_RETRIEVE_DOCUMENT_FAILURE);
                return retrieveDocumentData;
            }

            DocumentBean documentBean = QueryRepository.findDocumentByKey(em, documentId);
            if (documentBean == null) {

                // Chiave di ricerca non valida
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid document Id");

                userTransaction.commit();

                RetrieveDocumentData retrieveDocumentData = new RetrieveDocumentData();
                retrieveDocumentData.setStatusCode(ResponseHelper.USER_RETRIEVE_DOCUMENT_FAILURE);
                return retrieveDocumentData;
            }

            RetrieveDocumentData retrieveDocumentData = new RetrieveDocumentData();

            DocumentDetails documentDetails = documentService.encodeFile(documentBean.getTemplateFile());
            retrieveDocumentData.setDimension(documentDetails.getDimension());
            retrieveDocumentData.setFilename(documentDetails.getFilename());
            retrieveDocumentData.setDocumentBase64(documentDetails.getDocumentBase64());
            retrieveDocumentData.setStatusCode(ResponseHelper.USER_RETRIEVE_DOCUMENT_SUCCESS);

            userTransaction.commit();

            return retrieveDocumentData;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieve cities data with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
