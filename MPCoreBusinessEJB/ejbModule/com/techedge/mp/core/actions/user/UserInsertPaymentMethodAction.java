package com.techedge.mp.core.actions.user;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.PaymentResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.ShopTransactionDataBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.EncoderHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.PinHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.RandomGenerator;
import com.techedge.mp.payment.adapter.business.GSServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GetTokenResponse;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserInsertPaymentMethodAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserInsertPaymentMethodAction() {}

    public PaymentResponse execute(String ticketId, String requestId, String paymentMethodType, String newPin, String shopLogin, String shopLoginNewFlow,
            Integer pinCheckMaxAttempts, String checkAmountValue, Double maxCheckAmount, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            PaymentResponse managePaymentResponse = null;

            // Verifica il ticket
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                managePaymentResponse = new PaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_INVALID_TICKET);
                return managePaymentResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                managePaymentResponse = new PaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_INVALID_TICKET);
                return managePaymentResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to manage payment in status " + userStatus);

                userTransaction.commit();

                managePaymentResponse = new PaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_UNAUTHORIZED);
                return managePaymentResponse;
            }
            
            Boolean isMulticardFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.MULTICARD_FLOW.getCode());
            if (isMulticardFlow) {

                // L'utente � di tipo multicard e non pu� associare metodi diversi da multicard
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "UserCategory is multicard");

                userTransaction.commit();

                managePaymentResponse = new PaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_UNAUTHORIZED);
                return managePaymentResponse;
            }
            
            // Genera uno shopTransactionID
            String shopTransactionId = new IdGenerator().generateId(16).substring(0, 32);

            // Switch implementazione per utenti che eseguono il nuovo flusso voucher

            Integer userType = userBean.getUserType();
            Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());

            String secureString = "";
            PaymentInfoBean paymentInfoBean = null;

            if (useNewFlow) {

                System.out.println("Utilizzo nuovo flusso voucher");

                String encodedNewPin = EncoderHelper.encode(newPin);

                // Se l'utente non ha ancora nessun metodo di pagamento di default associato allora quello inserito diventa quello di default
                Boolean defaultMethod = false;
                if (userBean.findDefaultPaymentInfoBean() == null) {
                    defaultMethod = true;
                }
                else if (userBean.findDefaultPaymentInfoBean().getType().equals("credit_voucher")) {
                    userBean.findDefaultPaymentInfoBean().setDefaultMethod(false);
                    defaultMethod = true;

                }

                if (defaultMethod == true) {
                    System.out.println("Non � stato trovato un metodo di pagamento di default");
                }

                if (paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {

                    // Il flusso � lo stesso del vecchio caso, ma in pi� va verificato che l'utente abbia gi�
                    //  un metodo di pagamento associato di tipo credit_voucher e va verificato che il pin di input
                    //  sia quello associato a questo metodo

                    System.out.println("Inserimento credit card");

                    String encodedPin = userBean.getEncodedPin();

                    if (encodedPin == null || encodedPin.equals("")) {

                        // Errore Pin non ancora inserito
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not inserted");

                        userTransaction.commit();
                        managePaymentResponse = new PaymentResponse();
                        managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_UNAUTHORIZED);
                        return managePaymentResponse;
                    }

                    if (!encodedNewPin.equals(userBean.getEncodedPin())) {

                        // Errore Pin errato
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not correct");

                        for (PaymentInfoBean item : userBean.getPaymentData()) {
                            if (item.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER) && item.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {
                                paymentInfoBean = item;
                            }
                        }

                        managePaymentResponse = new PaymentResponse();

                        if (paymentInfoBean != null) {
                            // Si sottrae uno al numero di tentativi residui
                            Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
                            if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                                pinCheckAttemptsLeft--;
                            }
                            else {
                                pinCheckAttemptsLeft = 0;
                            }

                            if (pinCheckAttemptsLeft == 0) {

                                // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                                paymentInfoBean.setDefaultMethod(false);
                                paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

                                // e tutti i metodi di pagamento verificati e non verificati sono cancellati
                                for (PaymentInfoBean paymentInfo : userBean.getPaymentData()) {

                                    if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                                            && (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                                        paymentInfo.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                                        paymentInfo.setDefaultMethod(false);
                                        em.merge(paymentInfo);
                                    }
                                }
                            }
                            managePaymentResponse.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

                            paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

                            em.merge(paymentInfoBean);
                        }
                        userTransaction.commit();
                        managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_WRONG_PIN);
                        return managePaymentResponse;
                    }

                    // Ottieni il valore della secureString
                    GSServiceRemote gsService = null;

                    try {
                        gsService = EJBHomeCache.getInstance().getGsService();
                    }
                    catch (InterfaceNotFoundException ex) {

                        try {
                            userTransaction.rollback();
                        }
                        catch (IllegalStateException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        catch (SecurityException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        catch (SystemException e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        String message = "FAILED manage payment with message (" + ex.getMessage() + ")";
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

                        throw new EJBException(ex);
                    }

                    String operationId = null; // non pi� utilizzato

                    Double checkAmount = 0.12; // valore di default
                    if (checkAmountValue.equals("rand")) {

                        try {
                            checkAmount = RandomGenerator.generateRandom(maxCheckAmount);
                        }
                        catch (Exception e) {
                            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Error parsing checkAmount: " + e.getMessage());
                            checkAmount = 0.12;
                        }
                    }
                    else {

                        try {
                            checkAmount = Double.valueOf(checkAmountValue);
                        }
                        catch (Exception e) {
                            checkAmount = 0.12;
                        }
                    }

                    String stringCheckAmount = String.valueOf(checkAmount);

                    // TODO gestire le eccezioni
                    GetTokenResponse getTokenResponse = gsService.getToken(ticketId, requestId, operationId, shopTransactionId, shopLoginNewFlow, stringCheckAmount);

                    String bankTransactionId = "";
                    String currency = "";

                    if (getTokenResponse != null) {

                        secureString = getTokenResponse.getSecureString();
                        bankTransactionId = getTokenResponse.getBankTransactionId();
                        currency = getTokenResponse.getCurrency();
                    }

                    if (secureString == null) {

                        // Errore nella comunicazione
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Connection error with payment system");

                        userTransaction.commit();

                        managePaymentResponse = new PaymentResponse();
                        managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_CONNECTION_ERROR);
                        managePaymentResponse.setShopLogin(shopLoginNewFlow);
                        managePaymentResponse.setSecureString(secureString);
                        return managePaymentResponse;
                    }

                    paymentInfoBean = userBean.addNewPendingPaymentInfo(paymentMethodType, encodedNewPin, defaultMethod, pinCheckMaxAttempts, checkAmount, bankTransactionId,
                            currency, shopLoginNewFlow, shopTransactionId);

                    // Aggiorna le informazioni sui metodi di pagamento su db
                    if (defaultMethod == true) {

                        for (PaymentInfoBean paymentInfoBeanTemp : userBean.getPaymentData()) {

                            if (paymentInfoBeanTemp.getId() != paymentInfoBean.getId()) {

                                em.persist(paymentInfoBeanTemp);
                            }
                        }
                    }

                    System.out.println("Salvataggio nuovo metodo di pagamento");

                    // Memorizza su db l'informazione sulla carta
                    em.persist(paymentInfoBean);
                    em.merge(userBean);

                    // Registra su db l'associazione tra lo shopTransactionID e l'utente
                    ShopTransactionDataBean shopTransactionData = ShopTransactionDataBean.createNewShopTransactionDataBean(shopTransactionId, paymentInfoBean);

                    em.merge(shopTransactionData);

                    // Rinnova il ticket
                    //logger.log(Level.INFO, "Rinnova il ticket");
                    ticketBean.renew();
                    em.merge(ticketBean);

                }
                else {

                    // Viene generato il metodo di pagamento di tipo credit_voucher che avr� il solo scopo
                    //  di memorizzare il pin dell'utente

                    System.out.println("Inserimento credit voucher");

                    String checkPin = PinHelper.checkPin(newPin, null);
                    if (!checkPin.equals("OK")) {

                        // Il pin inserito non � sufficientemente sicuro
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not strong");

                        userTransaction.commit();
                        managePaymentResponse = new PaymentResponse();
                        managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_INVALID_PIN);
                        return managePaymentResponse;
                    }

                    // Bisogna verificare che l'utente non abbia gi� associato il pin

                    String encodedPin = userBean.getEncodedPin();

                    if (encodedPin != null && !encodedPin.equals("")) {

                        // Errore Pin gi� inserito
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin already inserted");

                        userTransaction.commit();
                        managePaymentResponse = new PaymentResponse();
                        managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_FAILED);
                        return managePaymentResponse;
                    }
                    /*
                    // Imposta lo stato di tutti gli altri metodi di pagamento in stato 1 e 2 a CANCELED
                    for (PaymentInfoBean oldPaymentInfoBean : userBean.getPaymentData()) {

                        if (oldPaymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                                && (oldPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || oldPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                            oldPaymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                            oldPaymentInfoBean.setDefaultMethod(false);
                            em.merge(oldPaymentInfoBean);
                        }
                    }
                    */
                    // Crea il nuovo metodo di pagamento
                    paymentInfoBean = userBean.addNewPendingPaymentInfo(paymentMethodType, encodedNewPin, Boolean.FALSE, pinCheckMaxAttempts, Double.valueOf(0), "", "242", "", "");

                    paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_VERIFIED);

                    // Aggiorna le informazioni sui metodi di pagamento su db
                    if (defaultMethod == true) {

                        for (PaymentInfoBean paymentInfoBeanTemp : userBean.getPaymentData()) {

                            if (paymentInfoBeanTemp.getId() != paymentInfoBean.getId()) {

                                em.persist(paymentInfoBeanTemp);
                            }
                        }
                    }

                    System.out.println("Salvataggio nuovo metodo di pagamento");
                    
                    
                    // Se l'utente ha gi� un metodo di pagamento verificato o non verificato si imposta il flag di registrazione completata
                    Boolean paymentMethodFound = false;
                    for (PaymentInfoBean oldPaymentInfoBean : userBean.getPaymentData()) {

                        if (oldPaymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                                && (oldPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || oldPaymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                            paymentMethodFound = true;
                        }
                    }
                    
                    if (paymentMethodFound) {
                        
                        // Imposta il flag di registrazione completata a true
                        userBean.setRegistrationCompleted();
                        
                        // Se l'utente appartiene alla nuova categoria e ha il flag oldUser a true bisogna impostare oldUser a false
                        if (userBean.getOldUser() ) {
                            
                            System.out.println("L'utente ha completato la migrazione da vecchio a nuovo");
                            userBean.setOldUser(false);
                        }
                    }
                    
                    // Memorizza su db l'informazione sulla carta
                    em.persist(paymentInfoBean);
                    em.merge(userBean);

                    // Rinnova il ticket
                    //logger.log(Level.INFO, "Rinnova il ticket");
                    ticketBean.renew();
                    em.merge(ticketBean);
                }
                
                userTransaction.commit();

                managePaymentResponse = new PaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_SUCCESS);
                managePaymentResponse.setShopLogin(shopLoginNewFlow);
                managePaymentResponse.setSecureString(secureString);
                managePaymentResponse.setPaymentMethodId(paymentInfoBean.getId());
                managePaymentResponse.setPaymentMethodType(paymentInfoBean.getType());

                return managePaymentResponse;

            }
            else {

                System.out.println("Utilizzo vecchio flusso voucher");

                // Verifica che il campo paymentMethodType sia uguale a credit_card

                if (!paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {

                    // Non � possibile associare metodi di pagamento diversi da credit_card per gli utenti che utilizzano il vecchio flusso
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable insert paymentMethod of type " + paymentMethodType);

                    userTransaction.commit();

                    managePaymentResponse = new PaymentResponse();
                    managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_UNAUTHORIZED);
                    return managePaymentResponse;
                }

                String checkPin = PinHelper.checkPin(newPin, null);
                if (!checkPin.equals("OK")) {

                    // Il pin inserito non � sufficientemente sicuro
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not strong");

                    userTransaction.commit();
                    managePaymentResponse = new PaymentResponse();
                    managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_INVALID_PIN);
                    return managePaymentResponse;
                }

                // Ottieni il valore della secureString
                GSServiceRemote gsService = null;

                try {
                    gsService = EJBHomeCache.getInstance().getGsService();
                }
                catch (InterfaceNotFoundException ex) {

                    try {
                        userTransaction.rollback();
                    }
                    catch (IllegalStateException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (SecurityException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    catch (SystemException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

                    String message = "FAILED manage payment with message (" + ex.getMessage() + ")";
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

                    throw new EJBException(ex);
                }

                String operationId = null; // non pi� utilizzato

                Double checkAmount = 0.12; // valore di default
                if (checkAmountValue.equals("rand")) {

                    try {
                        checkAmount = RandomGenerator.generateRandom(maxCheckAmount);
                    }
                    catch (Exception e) {
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Error parsing checkAmount: " + e.getMessage());
                        checkAmount = 0.12;
                    }
                }
                else {

                    try {
                        checkAmount = Double.valueOf(checkAmountValue);
                    }
                    catch (Exception e) {
                        checkAmount = 0.12;
                    }
                }

                String stringCheckAmount = String.valueOf(checkAmount);

                // TODO gestire le eccezioni
                GetTokenResponse getTokenResponse = gsService.getToken(ticketId, requestId, operationId, shopTransactionId, shopLogin, stringCheckAmount);

                String bankTransactionId = "";
                String currency = "";

                if (getTokenResponse != null) {

                    secureString = getTokenResponse.getSecureString();
                    bankTransactionId = getTokenResponse.getBankTransactionId();
                    currency = getTokenResponse.getCurrency();
                }

                if (secureString == null) {

                    // Errore nella comunicazione
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, "Connection error with payment system");

                    userTransaction.commit();

                    managePaymentResponse = new PaymentResponse();
                    managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_CONNECTION_ERROR);
                    managePaymentResponse.setShopLogin(shopLogin);
                    managePaymentResponse.setSecureString(secureString);
                    return managePaymentResponse;
                }

                // Se l'utente non ha ancora nessun metodo di pagamento di default associato allora quello inserito diventa quello di default
                Boolean defaultMethod = false;
                if (userBean.findDefaultPaymentInfoBean() == null) {
                    defaultMethod = true;
                }

                if (defaultMethod == true) {
                    System.out.println("Non � stato trovato un metodo di pagamento di default");
                }

                // Associa all'utente le informazioni sulla carta con stato in validazione
                String encodedNewPin = EncoderHelper.encode(newPin);

                paymentInfoBean = userBean.addNewPendingPaymentInfo(paymentMethodType, encodedNewPin, defaultMethod, pinCheckMaxAttempts, checkAmount, bankTransactionId, currency,
                        shopLogin, shopTransactionId);

                // Aggiorna le informazioni sui metodi di pagamento su db
                if (defaultMethod == true) {

                    for (PaymentInfoBean paymentInfoBeanTemp : userBean.getPaymentData()) {

                        if (paymentInfoBeanTemp.getId() != paymentInfoBean.getId()) {

                            em.persist(paymentInfoBeanTemp);
                        }
                    }
                }

                // Memorizza su db l'informazione sulla carta
                em.persist(paymentInfoBean);
                em.merge(userBean);

                // Registra su db l'associazione tra lo shopTransactionID e l'utente
                ShopTransactionDataBean shopTransactionData = ShopTransactionDataBean.createNewShopTransactionDataBean(shopTransactionId, paymentInfoBean);

                em.merge(shopTransactionData);

                // Rinnova il ticket
                //logger.log(Level.INFO, "Rinnova il ticket");
                ticketBean.renew();
                em.merge(ticketBean);
                
                userTransaction.commit();

                managePaymentResponse = new PaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_SUCCESS);
                managePaymentResponse.setShopLogin(shopLogin);
                managePaymentResponse.setSecureString(secureString);
                managePaymentResponse.setPaymentMethodId(paymentInfoBean.getId());
                managePaymentResponse.setPaymentMethodType(paymentInfoBean.getType());

                return managePaymentResponse;
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED insert payment method with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
