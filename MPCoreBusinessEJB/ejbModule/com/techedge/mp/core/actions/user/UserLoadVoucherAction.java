package com.techedge.mp.core.actions.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PromoCodeModeBean;
import com.techedge.mp.core.business.model.PromoVoucherBean;
import com.techedge.mp.core.business.model.PromotionBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserLoadVoucherAction {

    @Resource
    private EJBContext              context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager           em;

    @EJB
    private LoggerService           loggerService;

    @SuppressWarnings("serial")
    private HashMap<String, String> voucherStatus = new HashMap<String, String>() {
                                                      {
                                                          put(Voucher.VOUCHER_STATUS_ANNULLATO, ResponseHelper.USER_LOAD_VOUCHER_CANCELED);
                                                          put(Voucher.VOUCHER_STATUS_CANCELLATO, ResponseHelper.USER_LOAD_VOUCHER_REMOVED);
                                                          put(Voucher.VOUCHER_STATUS_DA_CONFERMARE, ResponseHelper.USER_LOAD_VOUCHER_TO_CONFIRM);
                                                          put(Voucher.VOUCHER_STATUS_ESAURITO, ResponseHelper.USER_LOAD_VOUCHER_SPENT);
                                                          put(Voucher.VOUCHER_STATUS_INESISTENTE, ResponseHelper.USER_LOAD_VOUCHER_INEXISTENT);
                                                          put(Voucher.VOUCHER_STATUS_SCADUTO, ResponseHelper.USER_LOAD_VOUCHER_EXPIRED);
                                                          put(Voucher.VOUCHER_STATUS_VALIDO, ResponseHelper.USER_LOAD_VOUCHER_VALID);
                                                          put(Voucher.VOUCHER_STATUS_NON_UTILIZZABILE_SUL_CANALE, ResponseHelper.USER_LOAD_VOUCHER_CAN_NOT_BE_USED_ON_THE_CHANNEL);
                                                      }
                                                  };

    public UserLoadVoucherAction() {}

    public String execute(String ticketId, String requestId, String voucherCode, Integer maxVoucherCount, FidelityServiceRemote fidelityService, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.USER_LOAD_VOUCHER_INVALID_TICKET;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.USER_LOAD_VOUCHER_INVALID_TICKET;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to load voucher in status " + userStatus);

                userTransaction.commit();

                return ResponseHelper.USER_LOAD_VOUCHER_UNAUTHORIZED;
            }

            // Verifica che l'utente non abbia gi� associati troppi voucher
            List<VoucherBean> voucherBeanList = QueryRepository.findActiveVoucherByUserBean(em, userBean);

            Integer voucherCount = voucherBeanList.size();

            System.out.println("voucherCount: " + voucherCount);
            System.out.println("maxVoucherCount: " + maxVoucherCount);

            if (voucherCount >= maxVoucherCount) {

                // Voucher gi� utilizzato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Reached max voucher number");

                userTransaction.commit();

                return ResponseHelper.USER_LOAD_VOUCHER_MAX_REACHED;
            }
            
            // Controlla se il voucher � stato gi� utilizzato
            VoucherBean voucherBean = QueryRepository.findVoucherByCode(em, voucherCode);
            
            if (voucherBean != null) {
                
                if (!voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_CANCELLATO)) {
                    
                    if (voucherBean.getUserBean() != null && voucherBean.getUserBean().getUserType() != User.USER_TYPE_GUEST) {
                        
                        // Voucher gi� assegnato a un utente non guest
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Voucher already used");
    
                        userTransaction.commit();
    
                        return ResponseHelper.USER_LOAD_VOUCHER_ALREADY_USED;
                    }
                }
                
                /*
                // Se il voucher risulta gi� associato a un utente guest pu� essere riassegnato
                if (voucherBean.getUserBean() != null && voucherBean.getUserBean().getUserType() != User.USER_TYPE_GUEST) {
                    
                    // Voucher gi� assegnato a un utente non guest
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Voucher already assigned to customer user");

                    userTransaction.commit();

                    return ResponseHelper.USER_LOAD_VOUCHER_ALREADY_USED;
                }
                else {
                    
                    if (!voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_CANCELLATO)) {
    
                        // Voucher gi� utilizzato
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Voucher already used");
    
                        userTransaction.commit();
    
                        return ResponseHelper.USER_LOAD_VOUCHER_ALREADY_USED;
                    }
                }
                */
            }

            //se il codice del voucher che l'utente sta inserendo � associato a una promozione:
            PromotionBean promoBean = QueryRepository.findPromotionByVoucherCode(em, voucherCode);

            if (promoBean != null) {

                // Voucher associato alla promozione
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Voucher associated to promotion " + promoBean.getCode());

                Date currentDate = new Date();

                if (!isPromotionActive(currentDate, promoBean.getStartData())) {

                    // Promozione non attiva
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Promotion not Active");

                    userTransaction.commit();

                    return ResponseHelper.USER_LOAD_VOUCHER_PROMOTION_NOT_ACTIVE;
                }

                if (!isWithinRange(currentDate, promoBean.getStartData(), promoBean.getEndData())) {

                    // Promozione scaduta
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Promotion expired");

                    userTransaction.commit();

                    return ResponseHelper.USER_LOAD_VOUCHER_PROMOTION_EXPIRED;
                }

                List<PromoVoucherBean> promoVoucherBeanList = QueryRepository.findPromoVoucherByUser(em, userBean);

                if (promoVoucherBeanList == null) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not have Voucher");

                    userTransaction.commit();

                    return ResponseHelper.USER_LOAD_VOUCHER_USER_NOT_ASSIGNED;

                }
                else {

                    if (!isVoucherCodePresent(promoVoucherBeanList, voucherCode)) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Voucher code " + voucherCode + " not valid for User");

                        userTransaction.commit();

                        return ResponseHelper.USER_LOAD_VOUCHER_ERROR_USER_CODE_NOT_VALID;
                    }
                }
            }

            // Verifica la validit� del voucher
            Date now = new Date();

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            PartnerType partnerType = PartnerType.MP;
            VoucherConsumerType voucherConsumerType = VoucherConsumerType.ENI;
            Long requestTimestamp = now.getTime();

            List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);

            VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
            voucherCodeDetail.setVoucherCode(voucherCode);
            voucherCodeList.add(voucherCodeDetail);

            CheckVoucherResult checkVoucherResult = new CheckVoucherResult();

            try {
                checkVoucherResult = fidelityService.checkVoucher(operationID, voucherConsumerType, partnerType, requestTimestamp, voucherCodeList);
            }
            catch (Exception e) {

                // Voucher gi� utilizzato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error checking voucher: " + e.getMessage());

                userTransaction.commit();

                return ResponseHelper.USER_LOAD_VOUCHER_ERROR;
            }

            // Verifica l'esito del check
            String checkVoucherStatusCode = checkVoucherResult.getStatusCode();
            
            
            if (!checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_OK)) {
                
                if (checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_EMPTY_LIST)) {

                    // Max limit reached
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error checking voucher: voucher list to check is empty");
                    userTransaction.commit();
                    return ResponseHelper.USER_LOAD_VOUCHER_EMPTY_LIST;
                }
                
                if (checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_MAX_LIMIT_REACHED)) {

                    // Max limit reached
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error checking voucher: max limit reached");
                    userTransaction.commit();
                    return ResponseHelper.USER_LOAD_VOUCHER_MAX_LIMIT_REACHED;
                }
                    // Error
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error checking voucher: " + checkVoucherStatusCode);
                userTransaction.commit();
                return ResponseHelper.USER_LOAD_VOUCHER_GENERIC_ERROR;
            }
            else {

                // Verifica che il check abbia restituito dei risultati
                if (checkVoucherResult.getVoucherList().isEmpty()) {

                    // Empty list
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error checking voucher: empty voucher list in response");

                    userTransaction.commit();

                    return ResponseHelper.USER_LOAD_VOUCHER_VOUCHER_LIST_EMPTY;
                }
                else {

                    // Verifica lo stato del voucher
                    VoucherDetail voucherDetail = checkVoucherResult.getVoucherList().get(0);

                    if (!voucherDetail.getVoucherStatus().equals(Voucher.VOUCHER_STATUS_VALIDO)) {

                        // Invalid voucher
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error checking voucher: voucher status "
                                + voucherDetail.getVoucherStatus());

                        userTransaction.commit();

                        if (voucherStatus.containsKey(voucherDetail.getVoucherStatus()))
                            return voucherStatus.get(voucherDetail.getVoucherStatus());
                        else
                            //return ResponseHelper.USER_LOAD_VOUCHER_STATUS_UNKNOW;
                            return ResponseHelper.USER_LOAD_VOUCHER_NOT_VALID;
                    }

                    else {

                        // Gestione voucher promozione con mode CF_DISTINCT
                        String promoCode = voucherDetail.getPromoCode();
                        
                        PromoCodeModeBean promoCodeModeBean = QueryRepository.findByModeAndPromoCode(em, "CF_DISTINCT", promoCode);
                        
                        if (promoCodeModeBean != null) {
                            
                            // L'utente guest non pu� associare questo tipo di voucher
                            Boolean isGuestFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.GUEST_FLOW.getCode());
                            if (isGuestFlow) {
                                // Empty list
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error loading voucher: " + promoCode + " voucher not enabled for guest user");
                                userTransaction.commit();
                                return ResponseHelper.USER_LOAD_VOUCHER_USER_GUEST_NOT_ENABLED;
                            }
                            
                            // Gli altri utenti possono associare il voucher solo se non hanno gi� associato un voucher della stessa promozione
                            String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                            int promoVoucherCount = QueryRepository.countVoucherByPromoCodeAndUserFiscalCode(em, fiscalCode, promoCode);
                            if (promoVoucherCount > 0) {
                                // Empty list
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Error loading voucher: " + promoCode + " voucher already associated. Count: " + promoVoucherCount);
                                userTransaction.commit();
                                return ResponseHelper.USER_LOAD_VOUCHER_USER_NOT_ENABLED;
                            }
                        }
                        
                        // Aggiorniamo il voucher precedente se era in stato 'C' o associa il nuovo voucher all'utente  
                        if (voucherBean != null && voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_CANCELLATO)) {

                            updateVoucherToUser(userBean, voucherBean, voucherDetail);
                        }
                        else {

                            createNewVoucherForUser(userBean, voucherDetail);
                        }

                        userTransaction.commit();

                        return ResponseHelper.USER_LOAD_VOUCHER_SUCCESS;
                    }
                }
            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED load voucher with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private void updateVoucherToUser(UserBean userBean, VoucherBean voucherBean, VoucherDetail voucherDetail) {

        voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
        voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
        voucherBean.setInitialValue(voucherDetail.getInitialValue());
        voucherBean.setPromoCode(voucherDetail.getPromoCode());
        voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
        voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
        voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
        voucherBean.setCode(voucherDetail.getVoucherCode());
        voucherBean.setStatus(voucherDetail.getVoucherStatus());
        voucherBean.setType(voucherDetail.getVoucherType());
        voucherBean.setValue(voucherDetail.getVoucherValue());
        voucherBean.setUserBean(userBean);

        em.merge(voucherBean);
    }

    private void createNewVoucherForUser(UserBean userBean, VoucherDetail voucherDetail) {

        VoucherBean newVoucherBean = associateVoucherToUser(userBean, voucherDetail);
        em.persist(newVoucherBean);
    }

    private VoucherBean associateVoucherToUser(UserBean userBean, VoucherDetail voucherDetail) {

        VoucherBean newVoucherBean = new VoucherBean();

        newVoucherBean.setConsumedValue(voucherDetail.getConsumedValue());
        newVoucherBean.setExpirationDate(voucherDetail.getExpirationDate());
        newVoucherBean.setInitialValue(voucherDetail.getInitialValue());
        newVoucherBean.setPromoCode(voucherDetail.getPromoCode());
        newVoucherBean.setPromoDescription(voucherDetail.getPromoDescription());
        newVoucherBean.setPromoDoc(voucherDetail.getPromoDoc());
        newVoucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
        newVoucherBean.setCode(voucherDetail.getVoucherCode());
        newVoucherBean.setStatus(voucherDetail.getVoucherStatus());
        newVoucherBean.setType(voucherDetail.getVoucherType());
        newVoucherBean.setValue(voucherDetail.getVoucherValue());

        newVoucherBean.setUserBean(userBean);
        return newVoucherBean;
    }

    private boolean isPromotionActive(Date date, Date startDate) {

        return !(date.before(startDate));
    }

    private boolean isWithinRange(Date testDate, Date startDate, Date endDate) {
        return !(testDate.before(startDate) || testDate.after(endDate));
    }

    private boolean isVoucherCodePresent(List<PromoVoucherBean> promoVoucherList, String voucherCode) {
        return indexOf(promoVoucherList, voucherCode) >= 0;
    }

    /**
     * Returns the index of the first occurrence of the specified element
     * in this list, or -1 if this list does not contain the element.
     */
    public int indexOf(List<PromoVoucherBean> promoVoucherList, String voucherCode) {

        for (int i = 0; i < promoVoucherList.size(); i++) {
            if (voucherCode.equals(promoVoucherList.get(i).getCode())) {

                return i;
            }
        }

        return -1;
    }
}
