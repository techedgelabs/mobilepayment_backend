package com.techedge.mp.core.actions.user;

import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UserCheckTransactionAuthorizationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UserCheckTransactionAuthorizationAction() {}

    public String execute(Double amount, String shopTransactionID, String valuta, String token, String operation, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Ricerca la transazione associata allo shopTransactionId
            TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, shopTransactionID);

            if (transactionBean == null) {

                // La transazione non esiste
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Transaction not found");

                userTransaction.commit();

                return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_NOT_FOUND;
            }

            UserBean userBean = transactionBean.getUserBean();
            if (userBean == null) {

                // Non esiste nessun utente associato alla transazione
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_USER_NOT_FOUND;
            }

            if (operation.equals("callPagam")) {

                // Si ricavano il methodId e il methodType dalla transazione
                Long methodId = transactionBean.getPaymentMethodId();
                String methodType = transactionBean.getPaymentMethodType();

                System.out.println("Verifica autorizzazione servizio callPagam");
                System.out.println("methodId: " + methodId);
                System.out.println("methodType: " + methodType);

                if (methodType == null || methodType.isEmpty() || methodType.equals("")) {

                    System.out.println("Pagamento con solo voucher");

                    // Verifica che l'utente sia abilitato al nuovo flusso voucher
                    Integer userType = userBean.getUserType();
                    Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());
                    
                    if (!useNewFlow) {

                        // L'utente non � abilitato al nuovo flusso voucher
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "User not enabled to new voucher flow");

                        userTransaction.commit();

                        return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_UNAUTHORIZED;
                    }

                    // Verifica che l'utente abbia gi� inserito il pin
                    String encodedPin = userBean.getEncodedPin();

                    if (encodedPin == null || encodedPin.equals("")) {

                        // Errore Pin non ancora inserito
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Pin not inserted");

                        userTransaction.commit();

                        return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_UNAUTHORIZED;
                    }
                    
                    userTransaction.commit();

                    return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_SUCCESS;
                }
                else {

                    PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(methodId, methodType);
                    if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                        // Il metodo di pagamento selezionato risulta verificato
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Payment method is verified");

                        userTransaction.commit();

                        if (userBean.getUserType() == User.USER_TYPE_TESTER) {

                            // Se l'utente � di tipo tester bisogna restituire uno status code specifico
                            return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_TESTER;
                        }

                        return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_SUCCESS;
                    }
                    else {

                        if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED) {

                            // Il metodo di pagamento selezionato non � ancora stato verificato
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Payment method is not verified");

                            // Verifica lo stato della transazione
                            Integer lastSequenceID = 0;
                            TransactionStatusBean lastTransactionStatusBean = null;
                            Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
                            for (TransactionStatusBean transactionStatusBean : transactionStatusData) {
                                Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
                                if (lastSequenceID < transactionStatusBeanSequenceID) {
                                    lastSequenceID = transactionStatusBeanSequenceID;
                                    lastTransactionStatusBean = transactionStatusBean;
                                }
                            }

                            if (lastTransactionStatusBean == null) {

                                // La transazione non ha stati associati
                                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Transaction status not found");

                                userTransaction.commit();

                                return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_WRONG_STATUS;
                            }
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Transaction status found");

                            if (!lastTransactionStatusBean.getStatus().equals(StatusHelper.STATUS_NEW_REFUELING_REQUEST)) {

                                // La transazione non si trova in uno stato valido
                                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Transaction status not valid: "
                                        + lastTransactionStatusBean.getStatus());

                                userTransaction.commit();

                                return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_WRONG_STATUS;
                            }
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Transaction status valid");

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null,
                                    "check cap: available: " + userBean.getCapAvailable() + " amount: " + amount);

                            // Bisogna verificare che l'utente abbia un cap disponibile sufficiente per l'operazione
                            double epsilon = 0.0000001;

                            if (userBean.getCapAvailable() < (amount - epsilon)) {

                                // Il cap disponibile � inferiore a quello richiesto per il rifornimento
                                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null,
                                        "Available cap insufficient. Requested: " + amount + " Available: " + userBean.getCapAvailable());

                                userTransaction.commit();

                                return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_INSUFFICIENT_CAP;
                            }
                            else {

                                // L'utente possiede un cap disponibile superiore a quello richiesto per il rifornimento
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", shopTransactionID, null,
                                        "Sufficient available cap found. Requested: " + amount + " Available: " + userBean.getCapAvailable());

                                userTransaction.commit();

                                return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_SUCCESS;
                            }
                        }
                        else {

                            // Lo stato del metodo di pagamento selezionato non � valido
                            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, "Payment method status not valid: "
                                    + paymentInfoBean.getStatus());

                            userTransaction.commit();

                            return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_UNAUTHORIZED;
                        }
                    }
                }
            }
            else {

                userTransaction.commit();

                if (userBean.getUserType() == User.USER_TYPE_TESTER) {

                    // Se l'utente � di tipo tester bisogna restituire uno status code specifico
                    return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_TESTER;
                }

                return ResponseHelper.CHECK_TRANSACTION_AUTHORIZATION_SUCCESS;
            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            ex2.printStackTrace(System.err);

            String message = "FAILED check transaction authorization with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", shopTransactionID, null, message);

            throw new EJBException(ex2);
        }
    }
}
