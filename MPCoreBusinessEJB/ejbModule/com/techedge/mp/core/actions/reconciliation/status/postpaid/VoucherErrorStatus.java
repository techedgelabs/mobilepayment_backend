package com.techedge.mp.core.actions.reconciliation.status.postpaid;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.actions.reconciliation.status.EventResult;
import com.techedge.mp.core.actions.reconciliation.status.EventResultType;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.EsPromotionBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetSrcTransactionStatusMessageResponse;
import com.techedge.mp.forecourt.integration.shop.interfaces.SrcTransactionDetail;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

public class VoucherErrorStatus extends ReconciliationPostPaidTransactionStatus {

    public VoucherErrorStatus(EntityManager entityManager, GPServiceRemote gpService, ForecourtPostPaidServiceRemote forecourtPPService, FidelityServiceRemote fidelityService, 
            EmailSenderRemote emailSenderService, String proxyHost, String proxyPort, String proxyNoHosts, String secretKey, 
            UserCategoryService userCategoryService, StringSubstitution stringSubstitution) {

        super(entityManager, gpService, forecourtPPService, fidelityService, emailSenderService, proxyHost, proxyPort, proxyNoHosts, secretKey, userCategoryService, stringSubstitution);
    }

    @Override
    public ReconciliationInfo reconciliateCreditCardFlow(TransactionInterfaceBean transactionInterfaceBean) {
        /*
        PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
        String requestID = String.valueOf(new Date().getTime());
        sequenceID = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getSequenceID() + 1;
        
        newStatus = StatusHelper.POST_PAID_STATUS_RECONCILIATION_REQU;
        oldStatus = null;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_RECONCILIATION;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        PostPaidTransactionEventBean postPaidTransactionEventBean = null;
        
        postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, 
                eventTimestamp, transactionEvent, postPaidTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);
        
        entityManager.persist(postPaidTransactionEventBean);
       
        oldStatus = newStatus;
        
        try {
            
            GetSrcTransactionStatusMessageResponse forecourtResponse = forecourtPPService.getTransactionStatus(requestID, postPaidTransactionBean.getMpTransactionID(), 
                    postPaidTransactionBean.getSrcTransactionID());
            System.out.println("chiamata a getTransactionStatus del ForecourtPostPaid: " + forecourtResponse.getStatusCode());

            
            if (forecourtResponse == null || forecourtResponse.getStatusCode().equals(StatusHelper.POST_PAID_TRANSACTION_STATUS_NOT_RECOGNIZED)) {
                System.out.println("Errore nella operazione di riconciliazione - "
                        + "risposta Forecourt nulla o non disponibile (transazione: " + postPaidTransactionBean.getTransactionID() + ")");

                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                decreaseAttempts(postPaidTransactionBean);
                return reconciliationInfo;
            }
            
            String GFGTransactionStatus = forecourtResponse.getSrcTransactionDetail().getSrcTransactionStatus();
            
            double amount = 0.0;
            PartnerType partnerType = PartnerType.MP;

            for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidTransactionBean.getPostPaidConsumeVoucherBeanList()) {
            
                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                Long requestTimestamp = new Date().getTime();
                String operationIDtoCheck = postPaidConsumeVoucherBean.getOperationID();
                
                System.out.println("Controllo consumo voucher transazione "  + postPaidTransactionBean.getMpTransactionID());
                CheckConsumeVoucherTransactionResult checkConsumeVoucherTransactionResult = fidelityService.checkConsumeVoucherTransaction(operationID, operationIDtoCheck, 
                        partnerType, requestTimestamp);
                List <VoucherDetail> voucherDetailList = checkConsumeVoucherTransactionResult.getVoucherList();
                
                for (VoucherDetail voucherDetail : voucherDetailList) {
                    if (voucherDetail.getConsumedValue().doubleValue() > 0.0) {
                        System.out.println("Trovato voucher consumato: " + voucherDetail.getVoucherCode());
                        amount += voucherDetail.getConsumedValue().doubleValue();
                    }
                }
            }
            
            if (amount == 0.0) {
                System.out.println("Storno consumo voucher non necessario. Importo consumato 0.0");
                postPaidTransactionBean.setVoucherReconcile(false);
                entityManager.merge(postPaidTransactionBean);

                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                return reconciliationInfo;
            }
                        
            if (postPaidTransactionBean.getVoucherReconcile() || (GFGTransactionStatus != null && (GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED)
                    || GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED))))  {
            
                sequenceID += 1;
                this.revertConsumeVoucher(postPaidTransactionBean, sequenceID);
                postPaidTransactionBean.setVoucherReconcile(false);
                entityManager.merge(postPaidTransactionBean);
            }
            else {
                System.out.println("Storno consumo voucher non necessario. Importo consumato " + amount);
            }
        }
        catch (Exception ex) {
            System.out.println("Exception revertConsumeVoucher: " + ex.getMessage());
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(postPaidTransactionBean);
        }
        */
        PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
        String requestID = String.valueOf(new Date().getTime());
        sequenceID = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getSequenceID() + 1;
        
        newStatus = StatusHelper.POST_PAID_STATUS_RECONCILIATION_REQU;
        oldStatus = null;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_RECONCILIATION;
        String errorCode = null;
        String errorDescription = null;
        String resultString = "OK";
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        PostPaidTransactionEventBean postPaidTransactionEventBean = null;
        
        postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, 
                eventTimestamp, transactionEvent, postPaidTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);
        
        entityManager.persist(postPaidTransactionEventBean);
       
        oldStatus = newStatus;

        EventResult verifiyVoucher = verifyVoucherTransaction(postPaidTransactionBean);
    
        if (verifiyVoucher.getResult().equals(EventResultType.TO_RETRY)) {
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(postPaidTransactionBean);
            return reconciliationInfo;
        }
        
        Double amount = (Double) verifiyVoucher.getAmount();
        
        if (amount == 0.0) {
            System.out.println("Storno consumo voucher non necessario. Importo consumato 0.0");
            postPaidTransactionBean.setVoucherReconcile(false);
            entityManager.merge(postPaidTransactionBean);

            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(verifiyVoucher.getResult().getCode());
        }
        else {
            sequenceID += 1;
            EventResult revertVoucher = this.revertConsumeVoucher(postPaidTransactionBean, sequenceID);
            
            if (revertVoucher.getResult().equals(EventResultType.TO_RETRY)) {
                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(revertVoucher.getResult().getCode());
                decreaseAttempts(postPaidTransactionBean);
                return reconciliationInfo;
            }
            
            postPaidTransactionBean.setVoucherReconcile(false);
            entityManager.merge(postPaidTransactionBean);
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(revertVoucher.getResult().getCode());
        }
        
        return reconciliationInfo;
    }
    
    @Override
    public ReconciliationInfo reconciliateVoucherFlow(TransactionInterfaceBean transactionInterfaceBean) {
        PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
        String requestID = String.valueOf(new Date().getTime());
        sequenceID = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getSequenceID() + 1;
        
        newStatus = StatusHelper.POST_PAID_STATUS_RECONCILIATION_REQU;
        oldStatus = null;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_RECONCILIATION;
        String errorCode = null;
        String errorDescription = null;
        String resultString = "OK";
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        PostPaidTransactionEventBean postPaidTransactionEventBean = null;
        
        postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, 
                eventTimestamp, transactionEvent, postPaidTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);
        
        entityManager.persist(postPaidTransactionEventBean);
       
        oldStatus = newStatus;
        
        GetSrcTransactionStatusMessageResponse forecourtResponse = forecourtPPService.getTransactionStatus(requestID, postPaidTransactionBean.getMpTransactionID(), 
                postPaidTransactionBean.getSrcTransactionID());
        System.out.println("chiamata a getTransactionStatus del ForecourtPostPaid: " + forecourtResponse.getStatusCode());
        /*
        GetSrcTransactionStatusMessageResponse forecourtResponse = new GetSrcTransactionStatusMessageResponse();
        forecourtResponse.setStatusCode(StatusHelper.POST_PAID_TRANSACTION_STATUS_OK);
        forecourtResponse.setSrcTransactionDetail(new SrcTransactionDetail() {
            @Override
            public String getSrcTransactionStatus() {
                return StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED;
                //return StatusHelper.POST_PAID_FINAL_STATUS_PAID;
            }
        });
        */
        if (forecourtResponse == null) {
            System.out.println("Errore nella operazione di riconciliazione - "
                    + "risposta Forecourt nulla o non disponibile (transazione: " + postPaidTransactionBean.getTransactionID() + ")");

            errorCode = "7777";
            errorDescription = (forecourtResponse == null) ? "forecourtResponse nullo" : forecourtResponse.getMessageCode();
            resultString = "ERROR";

            postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, 
                    eventTimestamp, transactionEvent, postPaidTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);
            
            entityManager.persist(postPaidTransactionEventBean);
           
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(postPaidTransactionBean);
            return reconciliationInfo;
        }
        
        
        
        String GFGTransactionStatus = null;
        
        if (forecourtResponse != null) {
            
            if (forecourtResponse.getSrcTransactionDetail() != null) {
                GFGTransactionStatus = forecourtResponse.getSrcTransactionDetail().getSrcTransactionStatus();
            }
            else {
                
                if (forecourtResponse.getStatusCode().equals(StatusHelper.POST_PAID_TRANSACTION_STATUS_NOT_RECOGNIZED)) {
                    GFGTransactionStatus = StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED;
                }
                else {
                    postPaidTransactionBean.setToReconcile(false);
                    postPaidTransactionBean.setTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_NOT_RECONCILIABLE);
                    entityManager.merge(postPaidTransactionBean);

                    reconciliationInfo.setFinalStatusType(StatusHelper.POST_PAID_FINAL_STATUS_NOT_RECONCILIABLE);
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                    //Email.sendPostPaidSummary(emailSenderService, postPaidTransactionBean, postPaidTransactionEventBean, proxyHost, proxyPort);
                    return reconciliationInfo;
                }
            }
        }
        
        if (GFGTransactionStatus != null && GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID))  {
            postPaidTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
            postPaidTransactionBean.setNotificationPaid(true);
            postPaidTransactionBean.setVoucherReconcile(false);
            entityManager.merge(postPaidTransactionBean);
            
            if ( postPaidTransactionBean.getUserBean().getUserType() == User.USER_TYPE_CUSTOMER || postPaidTransactionBean.getUserBean().getUserType() == User.USER_TYPE_VOUCHER_TESTER
                    || postPaidTransactionBean.getUserBean().getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER ) { 
                Email.sendPostPaidSummary(emailSenderService, postPaidTransactionBean, postPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, stringSubstitution);
            }
            
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
            return reconciliationInfo;
        }
        
        if (GFGTransactionStatus != null && (GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED)
        || GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED)))  {
            EventResult verifiyLoyalty = verifyLoyaltyTransaction(postPaidTransactionBean);
            Integer credits = (Integer) verifiyLoyalty.getAmount();
            
            if (credits == 0) {
                System.out.println("Storno caricamento punti non necessario. Importo caricato 0");
                postPaidTransactionBean.setLoyaltyReconcile(false);
                entityManager.merge(postPaidTransactionBean);
            }
            else {
                sequenceID += 1;
                EventResult revertLoyalty = this.revertLoadLoyaltyCredits(postPaidTransactionBean, sequenceID);
                
                if (revertLoyalty.getResult().equals(EventResultType.TO_RETRY)) {
                    postPaidTransactionBean.setLoyaltyReconcile(true);
                    decreaseAttempts(postPaidTransactionBean);
                }
                else {
                    postPaidTransactionBean.setLoyaltyReconcile(false);
                }

                entityManager.merge(postPaidTransactionBean);
            }
        }
        else {
            System.out.println("Storno caricamento punti non eseguito. GFGTransactionStatus: " + GFGTransactionStatus);
        }        
        
        EventResult verifiyVoucher = verifyVoucherTransaction(postPaidTransactionBean);
        
        if (verifiyVoucher.getResult().equals(EventResultType.TO_RETRY)) {
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(postPaidTransactionBean);
            return reconciliationInfo;
        }
        
        Double amount = (Double) verifiyVoucher.getAmount();
        
        if (amount == 0.0) {
            System.out.println("Storno consumo voucher non necessario. Importo consumato 0.0");
            postPaidTransactionBean.setVoucherReconcile(false);
            
            entityManager.merge(postPaidTransactionBean);

            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(verifiyVoucher.getResult().getCode());
        }
        else {

            sequenceID += 1;
            EventResult revertVoucher = this.revertConsumeVoucher(postPaidTransactionBean, sequenceID);
            
            if (revertVoucher.getResult().equals(EventResultType.TO_RETRY)) {
                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(revertVoucher.getResult().getCode());
                decreaseAttempts(postPaidTransactionBean);
                return reconciliationInfo;
            }
            
            postPaidTransactionBean.setVoucherReconcile(false);
            entityManager.merge(postPaidTransactionBean);
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(revertVoucher.getResult().getCode());
        }
        
        /************************************************************/
        /* Impostazione flag toBeProcessed per promo Vodafone Black */
        
        // Se per l'utente � presente una riga nella tabella ES_PROMOTION
        // con intervallo di valifit� che comprende la data di creazione
        // della transazione bisogna impostare il campo toBeProcessed
        // a true
        
        EsPromotionBean esPromotionBean = QueryRepository.findEsPromotionToBeProcessedWithNullVoucherCodeByDate(entityManager, postPaidTransactionBean.getUserBean(), postPaidTransactionBean.getCreationTimestamp());
        if (esPromotionBean != null) {
            System.out.println("Trovata riga EsPromotionBean con flag da impostare");
            esPromotionBean.setToBeProcessed(Boolean.TRUE);
            entityManager.merge(esPromotionBean);
        }
        
        /************************************************************/
        
        if ( postPaidTransactionBean.getUserBean().getUserType() == User.USER_TYPE_CUSTOMER || postPaidTransactionBean.getUserBean().getUserType() == User.USER_TYPE_VOUCHER_TESTER
                || postPaidTransactionBean.getUserBean().getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER ) { 
            Email.sendPostPaidSummary(emailSenderService, postPaidTransactionBean, postPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                    userCategoryService, stringSubstitution);
        }
        
        return reconciliationInfo;
    }
    
    public boolean checkAttemptsLeft(TransactionInterfaceBean transactionInterfaceBean) {
        boolean check = true;
        
        if (transactionInterfaceBean.getReconciliationAttemptsLeft() <= 0) {
            //updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
            PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
            postPaidTransactionBean.setVoucherReconcile(false);
            entityManager.merge(postPaidTransactionBean);
            check = false;
        }
        
        return check;
    }
    

    @Override
    public String getStatus() {
        return StatusHelper.POST_PAID_STATUS_VOUCHER_REVERSE;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "POST PAID VOUCHER ERROR";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<PostPaidTransactionBean> transactionBeanList = QueryRepository.findPostPaidTransactionsVoucherReconciliation(entityManager);
        return transactionBeanList;
    }

}
