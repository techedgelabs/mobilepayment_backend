package com.techedge.mp.core.actions.reconciliation.status.user;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationMessage;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.pushnotification.adapter.interfaces.StatusCode;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public class PushNotificationErrorStatus extends ReconciliationUserStatus {

    public PushNotificationErrorStatus(EntityManager entityManager, DWHAdapterServiceRemote dwhAdapterService, CRMAdapterServiceRemote crmAdapterService, 
            PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, ParametersServiceRemote parametersService, String proxyHost, String proxyPort, String proxyNoHosts, 
            Integer pushNotificationExpiryTime, Integer reconciliationAttemptsLeft, Boolean refuelingOauth2Active) {
        
        super(entityManager, dwhAdapterService, crmAdapterService, pushNotificationService, fidelityService, emailSender, refuelingNotificationService, refuelingNotificationServiceOAuth2, parametersService, 
                proxyHost, proxyPort, proxyNoHosts, pushNotificationExpiryTime, reconciliationAttemptsLeft, refuelingOauth2Active);
    }

    @Override
    public ReconciliationInfo reconciliate(UserReconciliationInterfaceBean userReconciliationInterfaceBean) {
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        PushNotificationBean pushNotificationBean = (PushNotificationBean) userReconciliationInterfaceBean;
        UserBean userBean = pushNotificationBean.getUserBean();
        String arnEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();
        PushNotificationStatusType statusCode = null;
        String statusMessage = null;
        String publishMessageID = null;
        Date sendingTimestamp = new Date();
        String message = pushNotificationBean.getMessage();
        String title = pushNotificationBean.getTitle();

        PushNotificationMessage notificationMessage = new PushNotificationMessage();
        notificationMessage.setMessage(pushNotificationBean.getId(), message, title);

        if (arnEndpoint == null) {

            System.err.println("User arn endpoint not found");

            decreaseAttempts(pushNotificationBean);

            statusCode = PushNotificationStatusType.ERROR;
            statusMessage = "User arn endpoint not found";
            
            pushNotificationBean.setStatusCode(statusCode);
            pushNotificationBean.setStatusMessage(statusMessage);
            pushNotificationBean.setSendingTimestamp(sendingTimestamp);
            
            entityManager.merge(pushNotificationBean);
            
            reconciliationInfo.setFinalStatusType(pushNotificationBean.getFinalStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

            return reconciliationInfo;
        }

        System.out.println("chiamata a publishMessage di AWS");
        
        PushNotificationResult pushNotificationResult = pushNotificationService.publishMessage(arnEndpoint, notificationMessage, false, false);
        publishMessageID = pushNotificationResult.getMessageId();
        sendingTimestamp = pushNotificationResult.getRequestTimestamp();

        System.out.println("risposta di AWS: " + pushNotificationResult.getMessage() + " (" + pushNotificationResult.getStatusCode() + ")");
        
        if (pushNotificationResult.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_SUCCESS)) {
            statusCode = PushNotificationStatusType.DELIVERED;
            pushNotificationBean.setToReconcilie(false);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
        }
        else {
            statusCode = PushNotificationStatusType.ERROR;
            statusMessage = pushNotificationResult.getMessage();
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(pushNotificationBean);
        }
        
        pushNotificationBean.setStatusCode(statusCode);
        pushNotificationBean.setStatusMessage(statusMessage);
        pushNotificationBean.setPublishMessageID(publishMessageID);
        pushNotificationBean.setSendingTimestamp(sendingTimestamp);
        
        entityManager.merge(pushNotificationBean);

        reconciliationInfo.setFinalStatusType(pushNotificationBean.getFinalStatus());

        return reconciliationInfo;
    }

    @Override
    public String getStatus() {
        return StatusHelper.PUSH_NOTIFICATION_STATUS_ERROR;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "PUSH NOTIFICATION";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<PushNotificationBean> pushNotificationBeanList = QueryRepository.findPushNotificationToReconcilie(entityManager);
        return pushNotificationBeanList;
    }

}
