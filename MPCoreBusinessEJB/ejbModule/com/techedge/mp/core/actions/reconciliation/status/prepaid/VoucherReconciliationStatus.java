package com.techedge.mp.core.actions.reconciliation.status.prepaid;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public class VoucherReconciliationStatus extends ReconciliationPrePaidTransactionStatus {

    public VoucherReconciliationStatus(EntityManager entityManager, GPServiceRemote gpService, TransactionServiceRemote transactionService, 
            ForecourtInfoServiceRemote forecourtService, FidelityServiceRemote fidelityService, CRMServiceRemote crmService, EmailSenderRemote emailSender, String proxyHost, 
            String proxyPort, String proxyNoHosts, String secretKey, UserCategoryService userCategoryService, StringSubstitution stringSubstitution, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2) {
        
        super(entityManager, gpService, transactionService, forecourtService, fidelityService, crmService, emailSender, proxyHost, proxyPort, proxyNoHosts, secretKey, 
                userCategoryService, stringSubstitution, refuelingNotificationService, refuelingNotificationServiceOAuth2);
    }

    @Override
    public ReconciliationInfo reconciliateCreditCardFlow(TransactionInterfaceBean transactionInterfaceBean) {
        TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
        
        String operationID = null;
        Long requestTimestamp = null;
        PartnerType partnerType = PartnerType.MP;
        
        System.out.println("Controllo se sono stati consumati voucher");
        
        for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {
            boolean consumed = false;

            if (prePaidConsumeVoucherBean.getOperationType().equals("CONSUME")) {
                operationID = new IdGenerator().generateId(16).substring(0, 33);
                requestTimestamp = new Date().getTime();
                String operationIDtoCheck = prePaidConsumeVoucherBean.getOperationID();
                String operationIDtoReverse = operationIDtoCheck;
                
                try {
                    CheckConsumeVoucherTransactionResult checkConsumeVoucherTransactionResult = fidelityService.checkConsumeVoucherTransaction(operationID, operationIDtoCheck, partnerType, requestTimestamp);
                    List <VoucherDetail> voucherDetailList = checkConsumeVoucherTransactionResult.getVoucherList();
                                        
                    if (voucherDetailList != null) {
                        for (VoucherDetail voucherDetail : voucherDetailList) {
                            if (voucherDetail.getConsumedValue().doubleValue() > 0.0) {
                                System.out.println("Trovato voucher consumato: " + voucherDetail.getVoucherCode());
                                consumed = true;
                            }
                        }
                    }
                }
                catch (FidelityServiceException ex) {
                    System.err.println("Errore nella chiamata al servizio checkConsumeVoucherTransaction: " + ex.getMessage());
                    reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                    decreaseAttempts(transactionBean);
                    return reconciliationInfo;
                }

                if (consumed) {
                    operationID = new IdGenerator().generateId(16).substring(0, 33);
                    requestTimestamp = new Date().getTime();
            
                    System.out.println("Storno consume voucher");
                    System.out.println("chiamata a Quenit per eseguire lo strono eventuale del voucher");
            
                    try {
                        ReverseConsumeVoucherTransactionResult reverseConsumeVoucherTransactionResult = fidelityService.reverseConsumeVoucherTransaction(operationID, operationIDtoReverse,
                                partnerType, requestTimestamp);
                        if (!reverseConsumeVoucherTransactionResult.getStatusCode().equals(FidelityResponse.REVERSE_CONSUME_VOUCHER_OK)) {
                            
                            if (reverseConsumeVoucherTransactionResult.getStatusCode().equals(FidelityResponse.REVERSE_CONSUME_VOUCHER_TRANSACTION_ALREADY_REVERSED)) {
                                System.err.println("Transazione gi� stornata nella chiamata al servizio checkConsumeVoucherTransaction: " + reverseConsumeVoucherTransactionResult.getMessageCode()
                                        + " (" + reverseConsumeVoucherTransactionResult.getStatusCode() + ")");
                                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                                reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                                transactionBean.setVoucherReconciliation(false);
                                entityManager.merge(transactionBean);
                                return reconciliationInfo;
                            }
                            else {
                                System.out.println("Strono non eseguito. Codice Stato: " + reverseConsumeVoucherTransactionResult.getStatusCode());
                                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                                reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                                transactionBean.setVoucherReconciliation(false);
                                entityManager.merge(transactionBean);
                                return reconciliationInfo;
                            }
                            
                        }
                        else {
                            transactionBean.setVoucherReconciliation(false);
                            PrePaidConsumeVoucherBean reversePrePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean();
                            reversePrePaidConsumeVoucherBean.setCsTransactionID(reverseConsumeVoucherTransactionResult.getCsTransactionID());
                            reversePrePaidConsumeVoucherBean.setMessageCode(reverseConsumeVoucherTransactionResult.getMessageCode());
                            reversePrePaidConsumeVoucherBean.setOperationID(operationID);
                            reversePrePaidConsumeVoucherBean.setOperationIDReversed(operationIDtoReverse);
                            reversePrePaidConsumeVoucherBean.setOperationType("REVERSE");
                            reversePrePaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
                            reversePrePaidConsumeVoucherBean.setStatusCode(reverseConsumeVoucherTransactionResult.getStatusCode());
                            reversePrePaidConsumeVoucherBean.setTotalConsumed(0.0);
                            reversePrePaidConsumeVoucherBean.setTransactionBean(transactionBean);
            
                            entityManager.persist(reversePrePaidConsumeVoucherBean);
                            entityManager.merge(transactionBean);
                        }
                    }
                    catch (Exception ex) {
                        reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                        decreaseAttempts(transactionBean);
                        return reconciliationInfo;
                    }
                }
                else {
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                    reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                    transactionBean.setVoucherReconciliation(false);
                    entityManager.merge(transactionBean);
                    return reconciliationInfo;
                }
            }
            else {
                System.out.println("Nessun voucher risulta essere stato consumato");
            }
        }

        return reconciliationInfo;
    }

    @Override
    public ReconciliationInfo reconciliateVoucherFlow(TransactionInterfaceBean transactionInterfaceBean) {
        System.out.println("Stato di riconciliazione non pi� necessaria per il nuovo flusso. Se sei qui hai un problema!!!");
        
        TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
        transactionBean.setVoucherReconciliation(false);
        entityManager.merge(transactionBean);
        
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        reconciliationInfo.setFinalStatusType(transactionInterfaceBean.getTransactionStatus());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
        return reconciliationInfo;
    }
    
    @Override
    public String getStatus() {
        return StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "VOUCHER RECONCILIATION";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsByFinalStatusTypeAndVoucherReconciliation(entityManager, getStatus(), true);
        return transactionBeanList;
    }

}
