package com.techedge.mp.core.actions.reconciliation.status.postpaid;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.actions.reconciliation.status.EventResult;
import com.techedge.mp.core.actions.reconciliation.status.EventResultType;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.EsPromotionBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventBean;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetSrcTransactionStatusMessageResponse;
import com.techedge.mp.forecourt.integration.shop.interfaces.SendMPTransactionResultMessageResponse;
import com.techedge.mp.forecourt.integration.shop.interfaces.SrcTransactionDetail;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;
import com.techedge.mp.payment.adapter.wss2s.xsd.EventsType;
import com.techedge.mp.payment.adapter.wss2s.xsd.EventsType.Event;

public class PaymentErrorStatus extends ReconciliationPostPaidTransactionStatus {

    public PaymentErrorStatus(EntityManager entityManager, GPServiceRemote gpService, ForecourtPostPaidServiceRemote forecourtPPService, FidelityServiceRemote fidelityService, 
            EmailSenderRemote emailSenderService, String proxyHost, String proxyPort, String proxyNoHosts, String secretKey, 
            UserCategoryService userCategoryService, StringSubstitution stringSubstitution) {

        super(entityManager, gpService, forecourtPPService, fidelityService, emailSenderService, proxyHost, proxyPort, proxyNoHosts, secretKey, userCategoryService, stringSubstitution);
    }

    @Override
    public ReconciliationInfo reconciliateCreditCardFlow(TransactionInterfaceBean transactionInterfaceBean) {
        PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
        String requestID = String.valueOf(new Date().getTime());
        sequenceID = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getSequenceID();
        Integer sequencePaymentID = 1;

        String decodedSecretKey = "";
        try {
            EncryptionAES encryptionAES = new EncryptionAES();
            encryptionAES.loadSecretKey(this.secretKey);
            decodedSecretKey = encryptionAES.decrypt(postPaidTransactionBean.getEncodedSecretKey());
        }
        catch (Exception e) {
            if (transactionInterfaceBean.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {
                System.err.println("Errore nel decrypt della secretKey: " + e.getMessage());
                e.printStackTrace();
            }
        }
        
        if (postPaidTransactionBean.getLastPostPaidTransactionPaymentEvent() != null) {
            sequencePaymentID = postPaidTransactionBean.getLastPostPaidTransactionPaymentEvent().getSequence();
        }

        newStatus = StatusHelper.POST_PAID_STATUS_RECONCILIATION_REQU;
        oldStatus = null;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_RECONCILIATION;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        String eventType = "STO";
        boolean flagNotificationGFG = postPaidTransactionBean.getGFGNotification();
        boolean transactionStateNull = false;
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        PostPaidTransactionEventBean postPaidTransactionEventBean = null;
        
        GetSrcTransactionStatusMessageResponse forecourtResponse = forecourtPPService.getTransactionStatus(requestID, postPaidTransactionBean.getMpTransactionID(),
                postPaidTransactionBean.getSrcTransactionID());
        System.out.println("chiamata a getTransactionStatus del ForecourtPostPaid: " + forecourtResponse.getStatusCode());
        /*
        GetSrcTransactionStatusMessageResponse forecourtResponse = new GetSrcTransactionStatusMessageResponse();
        forecourtResponse.setStatusCode(StatusHelper.POST_PAID_TRANSACTION_STATUS_OK);
        forecourtResponse.setSrcTransactionDetail(new SrcTransactionDetail() {
            @Override
            public String getSrcTransactionStatus() {
                return StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED;
                //return StatusHelper.POST_PAID_FINAL_STATUS_PAID;
            }
        });
        */
        
        // Gestione differenziata casistica transazione non riconosciuta
        //if (forecourtResponse == null || forecourtResponse.getStatusCode().equals(StatusHelper.POST_PAID_TRANSACTION_STATUS_NOT_RECOGNIZED)) {
        if (forecourtResponse == null) {
            System.out.println("Errore nella operazione di riconciliazione - " + "risposta Forecourt nulla o non disponibile (transazione: "
                    + postPaidTransactionBean.getTransactionID() + ")");

            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(postPaidTransactionBean);
            return reconciliationInfo;
        }

        if (forecourtResponse.getStatusCode().equals(StatusHelper.POST_PAID_TRANSACTION_STATUS_NOT_AVAILABLE)
                || forecourtResponse.getStatusCode().equals(StatusHelper.POST_PAID_TRANSACTION_STATUS_KO)) {
            System.out.println("Transazione non riconosciuta dal Forecourt (transazione: " + postPaidTransactionBean.getTransactionID() + ")");

            sequenceID += 1;
            errorCode = "8888";
            errorDescription = forecourtResponse.getMessageCode();
            eventResult = "KO";

            postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, eventTimestamp, transactionEvent,
                    postPaidTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

            entityManager.persist(postPaidTransactionEventBean);

            postPaidTransactionBean.setToReconcile(false);
            postPaidTransactionBean.setTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_NOT_RECONCILIABLE);
            entityManager.merge(postPaidTransactionBean);

            reconciliationInfo.setFinalStatusType(StatusHelper.POST_PAID_FINAL_STATUS_NOT_RECONCILIABLE);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
            //Email.sendPostPaidSummary(emailSenderService, postPaidTransactionBean, postPaidTransactionEventBean, proxyHost, proxyPort);
            return reconciliationInfo;
        }

        SrcTransactionDetail transactionDetail = forecourtResponse.getSrcTransactionDetail();
        
        // Gestione caso di transazione non riconosciuta sul GFG
        if (transactionDetail == null) {
            transactionDetail = new SrcTransactionDetail();
            transactionDetail.setSrcTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);
        }

        if (transactionDetail.getSrcTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)) {
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            return reconciliationInfo;
        }

        GestPayData gestPayData = gpService.callReadTrx(postPaidTransactionBean.getShopLogin(), postPaidTransactionBean.getMpTransactionID(),
                postPaidTransactionBean.getBankTansactionID(), postPaidTransactionBean.getAcquirerID(), postPaidTransactionBean.getGroupAcquirer(),
                decodedSecretKey);
        
        if (gestPayData != null) {
            System.out.println("Banca TransactionState: " + gestPayData.getTransactionState());
        }
        else {
            System.out.println("Banca TransactionState: not found");
        }
        
        if (transactionDetail.getSrcTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID)) {
            
            postPaidTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
            
            sequenceID += 1;

            postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, eventTimestamp, transactionEvent,
                    postPaidTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

            entityManager.persist(postPaidTransactionEventBean);

            oldStatus = newStatus;

            EventResult verifyVoucher = verifyVoucherTransaction(postPaidTransactionBean);
            
            if (verifyVoucher.getResult().equals(EventResultType.SUCCESS)) {
                Double amount = (Double) verifyVoucher.getAmount();
                
                if (amount == 0.0) {
                    System.out.println("Storno consumo voucher non necessario. Importo consumato 0.0");
                    postPaidTransactionBean.setVoucherReconcile(false);
                    entityManager.merge(postPaidTransactionBean);
                }
                else {
                    sequenceID += 1;
                    EventResult revertVoucher = this.revertConsumeVoucher(postPaidTransactionBean, sequenceID);
                    
                    if (revertVoucher.getResult().equals(EventResultType.TO_RETRY)) {
                        postPaidTransactionBean.setVoucherReconcile(true);
                    }
                    else {
                        postPaidTransactionBean.setVoucherReconcile(false);
                    }
                }
                
                entityManager.merge(postPaidTransactionBean);
            }

            EventResult verifiyLoyalty = verifyLoyaltyTransaction(postPaidTransactionBean);
            Integer credits = (Integer) verifiyLoyalty.getAmount();
            
            if (credits == 0) {
                System.out.println("Storno caricamento punti non necessario. Importo caricato 0");
                postPaidTransactionBean.setLoyaltyReconcile(false);
                entityManager.merge(postPaidTransactionBean);
            }
            else {
                sequenceID += 1;
                EventResult revertLoyalty = this.revertLoadLoyaltyCredits(postPaidTransactionBean, sequenceID);
                
                if (revertLoyalty.getResult().equals(EventResultType.TO_RETRY)) {
                    postPaidTransactionBean.setLoyaltyReconcile(true);
                }
                else {
                    postPaidTransactionBean.setLoyaltyReconcile(false);
                }

                entityManager.merge(postPaidTransactionBean);
            }

            if (!flagNotificationGFG) {
                sequenceID += 1;
                eventType = "MOV";
                SendMPTransactionResultMessageResponse notifyResponse = notifyPostPaidPGFG(transactionInterfaceBean, gestPayData, eventType,
                        StatusHelper.POST_PAID_STATUS_NOTIFICATION_SENT_AGAIN, sequenceID);
                //SendMPTransactionResultMessageResponse notifyResponse = new SendMPTransactionResultMessageResponse();
                //notifyResponse.setStatusCode(StatusHelper.GFG_NOTIFICATION_SUCCESS);
                //System.out.println("Simulazione di chiamata al sistema di notifica GFC: " + notifyResponse.getStatusCode());

                if (notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR)) {
                    postPaidTransactionBean.setNotificationPaid(false);
                    postPaidTransactionBean.setToReconcile(true);
                    decreaseAttempts(postPaidTransactionBean);
                    reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                    return reconciliationInfo;

                }
                else {
                    postPaidTransactionBean.setNotificationPaid(true);
                    postPaidTransactionBean.setToReconcile(false);
                    postPaidTransactionBean.setTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
                    if (notifyResponse.getElectronicInvoiceID() != null && !notifyResponse.getElectronicInvoiceID().isEmpty()) {
                        postPaidTransactionBean.setGFGElectronicInvoiceID(notifyResponse.getElectronicInvoiceID());
                    }
                    reconciliationInfo.setFinalStatusType(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                }
            }
            else {
                postPaidTransactionBean.setToReconcile(false);
                postPaidTransactionBean.setTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
                reconciliationInfo.setFinalStatusType(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
            }

            entityManager.merge(postPaidTransactionBean);
            
            if ( postPaidTransactionBean.getUserBean().getUserType() == User.USER_TYPE_CUSTOMER 
                    || postPaidTransactionBean.getUserBean().getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER ) {
                Email.sendPostPaidSummary(emailSenderService, postPaidTransactionBean, postPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, stringSubstitution);
            }

            return reconciliationInfo;
        }

        if (transactionDetail.getSrcTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED)
                || transactionDetail.getSrcTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED)) {

            if (gestPayData == null || gestPayData.getTransactionState() == null || gestPayData.getTransactionState().isEmpty()) {
                //errorCode = gestPayData.getErrorCode();
                //errorDescription = gestPayData.getErrorDescription();
                //eventResult = gestPayData.getTransactionResult();
                sequenceID += 1;
                //flagNotificationGFG = true;
                transactionStateNull = true;

                postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, eventTimestamp,
                        transactionEvent, postPaidTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                entityManager.persist(postPaidTransactionEventBean);

            }
            else {
                sequenceID += 1;

                postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, eventTimestamp,
                        transactionEvent, postPaidTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                entityManager.persist(postPaidTransactionEventBean);

                oldStatus = newStatus;
                
                System.out.println("gestPayData.transactionState: " + gestPayData.getTransactionState());
                System.out.println("gestPayData.eventAmount: " + gestPayData.getEventAmount("MOV"));
                System.out.println("gestPayData.authorizationCode: " + gestPayData.getAuthorizationCode());
                
                String authorizationCode = postPaidTransactionBean.getAuthorizationCode();
                if (authorizationCode == null || authorizationCode.isEmpty()) {
                    authorizationCode = gestPayData.getAuthorizationCode();
                }
                String bankTransactionId = postPaidTransactionBean.getBankTansactionID();
                if (bankTransactionId == null || bankTransactionId.isEmpty()) {
                    bankTransactionId = gestPayData.getBankTransactionID();
                }

                if (gestPayData.getTransactionState().equals("MOV") && gestPayData.getEventAmount("MOV").doubleValue() > 0.0) {
                    GestPayData gestPayDataREFUNDResponse = gpService.callRefund(gestPayData.getEventAmount("MOV").doubleValue(), postPaidTransactionBean.getMpTransactionID(),
                            postPaidTransactionBean.getShopLogin(), postPaidTransactionBean.getCurrency(), postPaidTransactionBean.getToken(), authorizationCode,
                            bankTransactionId, postPaidTransactionBean.getStationBean().getStationID(), postPaidTransactionBean.getAcquirerID(),
                            postPaidTransactionBean.getGroupAcquirer(), decodedSecretKey);

                    if (gestPayDataREFUNDResponse == null) {
                        gestPayDataREFUNDResponse = new GestPayData();
                        gestPayDataREFUNDResponse.setTransactionResult("ERROR");
                        gestPayDataREFUNDResponse.setErrorCode("9999");
                        gestPayDataREFUNDResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
                    }

                    System.out.println("Operazione GestPay REFUND: " + gestPayDataREFUNDResponse.getTransactionResult());

                    errorCode = gestPayDataREFUNDResponse.getErrorCode();
                    errorDescription = gestPayDataREFUNDResponse.getErrorDescription();
                    eventResult = gestPayDataREFUNDResponse.getTransactionResult();
                    newStatus = StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE;
                    eventTimestamp = new Timestamp(new Date().getTime());
                    transactionEvent = StatusHelper.POST_PAID_EVENT_BE_REVERSE;
                    sequenceID += 1;
                    sequencePaymentID += 1;

                    postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, eventTimestamp,
                            transactionEvent, postPaidTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                    entityManager.persist(postPaidTransactionEventBean);

                    PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, postPaidTransactionBean,
                            gestPayDataREFUNDResponse, "STO");

                    entityManager.persist(postPaidTransactionPaymentEventBean);
                }

                if (gestPayData.getTransactionState().equals("AUT") && gestPayData.getEventAmount("AUT").doubleValue() > 0.0) {
                    GestPayData gestPayDataDELETEResponse = gpService.deletePagam(gestPayData.getEventAmount("AUT").doubleValue(), postPaidTransactionBean.getMpTransactionID(),
                            postPaidTransactionBean.getShopLogin(), postPaidTransactionBean.getCurrency(), postPaidTransactionBean.getBankTansactionID(), null,
                            postPaidTransactionBean.getAcquirerID(), postPaidTransactionBean.getGroupAcquirer(), decodedSecretKey);

                    if (gestPayDataDELETEResponse == null) {
                        gestPayDataDELETEResponse = new GestPayData();
                        gestPayDataDELETEResponse.setTransactionResult("ERROR");
                        gestPayDataDELETEResponse.setErrorCode("9999");
                        gestPayDataDELETEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
                    }

                    System.out.println("Operazione GestPay AUTH DEL: " + gestPayDataDELETEResponse.getTransactionResult());

                    errorCode = gestPayDataDELETEResponse.getErrorCode();
                    errorDescription = gestPayDataDELETEResponse.getErrorDescription();
                    eventResult = gestPayDataDELETEResponse.getTransactionResult();
                    newStatus = StatusHelper.POST_PAID_STATUS_PAY_AUTH_DELETE;
                    eventTimestamp = new Timestamp(new Date().getTime());
                    transactionEvent = StatusHelper.POST_PAID_EVENT_BE_AUTH_DEL;
                    sequenceID += 1;
                    sequencePaymentID += 1;

                    postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, eventTimestamp,
                            transactionEvent, postPaidTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                    entityManager.persist(postPaidTransactionEventBean);

                    PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, postPaidTransactionBean,
                            gestPayDataDELETEResponse, "DEL");

                    entityManager.persist(postPaidTransactionPaymentEventBean);
                }

                if (gestPayData.getTransactionState().equals("STO") && gestPayData.getEventAmount("STO").doubleValue() > 0.0) {
                    errorCode = null;
                    errorDescription = null;
                    eventResult = "OK";
                    newStatus = StatusHelper.POST_PAID_STATUS_REFUND;
                    eventTimestamp = new Timestamp(new Date().getTime());
                    transactionEvent = StatusHelper.POST_PAID_EVENT_BE_REVERSE;
                    sequenceID += 1;

                    postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, eventTimestamp,
                            transactionEvent, postPaidTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                    entityManager.persist(postPaidTransactionEventBean);

                }
            }

            oldStatus = newStatus;

            EventResult verifiyVoucher = verifyVoucherTransaction(postPaidTransactionBean);
            
            if (verifiyVoucher.getResult().equals(EventResultType.SUCCESS)) {
                Double amount = (Double) verifiyVoucher.getAmount();
                
                if (amount == 0.0) {
                    System.out.println("Storno consumo voucher non necessario. Importo consumato 0.0");
                    postPaidTransactionBean.setVoucherReconcile(false);
                    entityManager.merge(postPaidTransactionBean);
                }
                else {
                    sequenceID += 1;
                    EventResult revertVoucher = this.revertConsumeVoucher(postPaidTransactionBean, sequenceID);
                    
                    if (revertVoucher.getResult().equals(EventResultType.TO_RETRY)) {
                        postPaidTransactionBean.setVoucherReconcile(true);
                    }
                    else {
                        postPaidTransactionBean.setVoucherReconcile(false);
                    }
                }
                
                entityManager.merge(postPaidTransactionBean);
            }

            EventResult verifiyLoyalty = verifyLoyaltyTransaction(postPaidTransactionBean);
            Integer credits = (Integer) verifiyLoyalty.getAmount();
            
            if (credits == 0) {
                System.out.println("Storno caricamento punti non necessario. Importo caricato 0");
                postPaidTransactionBean.setLoyaltyReconcile(false);
                entityManager.merge(postPaidTransactionBean);
            }
            else {
                sequenceID += 1;
                EventResult revertLoyalty = this.revertLoadLoyaltyCredits(postPaidTransactionBean, sequenceID);
                
                if (revertLoyalty.getResult().equals(EventResultType.TO_RETRY)) {
                    postPaidTransactionBean.setLoyaltyReconcile(true);
                }
                else {
                    postPaidTransactionBean.setLoyaltyReconcile(false);
                }

                entityManager.merge(postPaidTransactionBean);
            }


            if (eventResult.equals("ERROR")) {
                postPaidTransactionBean.setToReconcile(true);
                decreaseAttempts(postPaidTransactionBean);

                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                return reconciliationInfo;
            }

            if (!transactionStateNull && errorCode != null && !errorCode.equals("0")) {

                postPaidTransactionBean.setToReconcile(false);
                postPaidTransactionBean.setTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_NOT_RECONCILIABLE);
                entityManager.merge(postPaidTransactionBean);

                reconciliationInfo.setFinalStatusType(StatusHelper.POST_PAID_FINAL_STATUS_NOT_RECONCILIABLE);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                //Email.sendPostPaidSummary(emailSenderService, postPaidTransactionBean, postPaidTransactionEventBean, proxyHost, proxyPort); 
                return reconciliationInfo;
            }

            if (!flagNotificationGFG) {
                sequenceID += 1;
                SendMPTransactionResultMessageResponse notifyResponse = notifyPostPaidPGFG(transactionInterfaceBean, gestPayData, eventType,
                        StatusHelper.POST_PAID_STATUS_NOTIFICATION_SENT_AGAIN, sequenceID);
                /*
                 * SendMPTransactionResultMessageResponse notifyResponse = new SendMPTransactionResultMessageResponse();
                 * notifyResponse.setStatusCode(StatusHelper.GFG_NOTIFICATION_SUCCESS);
                 * System.out.println("Simulazione di chiamata al sistema di notifica GFC: " + notifyResponse.getStatusCode());
                 */

                if (notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR)) {
                    postPaidTransactionBean.setNotificationPaid(false);
                    postPaidTransactionBean.setToReconcile(true);
                    decreaseAttempts(postPaidTransactionBean);
                    reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                    return reconciliationInfo;
                }
                else {
                    postPaidTransactionBean.setNotificationPaid(true);
                    postPaidTransactionBean.setToReconcile(false);
                    postPaidTransactionBean.setTransactionStatus(transactionDetail.getSrcTransactionStatus());
                    reconciliationInfo.setFinalStatusType(transactionDetail.getSrcTransactionStatus());
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                }
            }
            else {
                postPaidTransactionBean.setNotificationPaid(true);
                postPaidTransactionBean.setToReconcile(false);
                postPaidTransactionBean.setTransactionStatus(transactionDetail.getSrcTransactionStatus());
                reconciliationInfo.setFinalStatusType(transactionDetail.getSrcTransactionStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
            }

            entityManager.merge(postPaidTransactionBean);
            
            /************************************************************/
            /* Impostazione flag toBeProcessed per promo Vodafone Black */
            
            // Se per l'utente � presente una riga nella tabella ES_PROMOTION
            // con intervallo di valifit� che comprende la data di creazione
            // della transazione bisogna impostare il campo toBeProcessed
            // a true
            
            EsPromotionBean esPromotionBean = QueryRepository.findEsPromotionToBeProcessedWithNullVoucherCodeByDate(entityManager, postPaidTransactionBean.getUserBean(), postPaidTransactionBean.getCreationTimestamp());
            if (esPromotionBean != null) {
                System.out.println("Trovata riga EsPromotionBean con flag da impostare");
                esPromotionBean.setToBeProcessed(Boolean.TRUE);
                entityManager.merge(esPromotionBean);
            }
            
            /************************************************************/

            if ( postPaidTransactionBean.getUserBean().getUserType() == User.USER_TYPE_CUSTOMER || postPaidTransactionBean.getUserBean().getUserType() == User.USER_TYPE_VOUCHER_TESTER
                    || postPaidTransactionBean.getUserBean().getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER ) { 
                Email.sendPostPaidSummary(emailSenderService, postPaidTransactionBean, postPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, stringSubstitution);
            }
            
            return reconciliationInfo;
        }

        reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
        return reconciliationInfo;
    }

    @Override
    public ReconciliationInfo reconciliateVoucherFlow(TransactionInterfaceBean transactionInterfaceBean) {
        PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
        postPaidTransactionBean.setToReconcile(false);
        postPaidTransactionBean.setVoucherReconcile(true);
        entityManager.merge(postPaidTransactionBean);
        
        VoucherErrorStatus voucherErrorStatus = new VoucherErrorStatus(entityManager, gpService, forecourtPPService, fidelityService, emailSenderService, 
                proxyHost, proxyPort, proxyNoHosts, secretKey, userCategoryService, stringSubstitution);
        ReconciliationInfo reconciliationInfo = voucherErrorStatus.reconciliate(transactionInterfaceBean, this.paymentFlowType);
        return reconciliationInfo;
    }
    
    public boolean checkAttemptsLeft(TransactionInterfaceBean transactionInterfaceBean) {
        boolean check = true;
        
        if (transactionInterfaceBean.getReconciliationAttemptsLeft() <= 0) {
            //updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
            PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
            postPaidTransactionBean.setToReconcile(false);
            entityManager.merge(postPaidTransactionBean);            
            check = false;
        }
        
        return check;
    }

    @Override
    public String getStatus() {
        return StatusHelper.POST_PAID_FINAL_STATUS_UNPAID;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "POST PAID PAYEMENT ERROR";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<PostPaidTransactionBean> transactionBeanList = QueryRepository.findPostPaidTransactionsPaymentReconciliation(entityManager);
        return transactionBeanList;
    }

}
