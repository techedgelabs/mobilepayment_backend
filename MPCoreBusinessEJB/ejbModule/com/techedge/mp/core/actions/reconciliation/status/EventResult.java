package com.techedge.mp.core.actions.reconciliation.status;


public class EventResult {

    private EventResultType result;
    private String          errorCode;
    private Object          amount;

    public EventResult(EventResultType result) {
        setResult(result);
        setAmount(0.0);
        setErrorCode("0");
    }

    public void setResult(EventResultType result) {
        this.result = result;
    }

    public EventResultType getResult() {
        return this.result;
    }

    /*public String getResultCode() {
         return this.result.getCode();
     }*/

    public void setAmount(Object amount) {
        this.amount = amount;
    }

    public Object getAmount() {
        return amount;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }
}

