package com.techedge.mp.core.actions.reconciliation.status.parking;


import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.actions.parking.v2.PARKING_TRANSACTION_ITEM_STATUS;
import com.techedge.mp.core.actions.parking.v2.PARKING_TRANSACTION_STATUS;
import com.techedge.mp.core.actions.parking.v2.PARKING_TRANSACTION_STATUS_CONST;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.ParkingTransactionInterfaceBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemEventBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

public class PaymentDeleteErrorStatus extends ReconciliationParkingStatus {

    public PaymentDeleteErrorStatus(EntityManager entityManager, GPServiceRemote gpService, EmailSenderRemote emailSender, ParametersServiceRemote parametersService, String proxyHost, String proxyPort, String proxyNoHosts, Integer reconciliationAttemptsLeft, String parkingDecodedSecretKey) {

        super(entityManager, gpService, emailSender, parametersService, proxyHost, proxyPort, proxyNoHosts, reconciliationAttemptsLeft, parkingDecodedSecretKey);
    }

    @Override
    public ReconciliationInfo reconciliate(ParkingTransactionInterfaceBean parkingTransactionInterfaceBean) {
        
        ParkingTransactionBean parkingTransactionBean = (ParkingTransactionBean) parkingTransactionInterfaceBean;
        
        GestPayData gestPayData = null;
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        
        // Cicla su tutti gli item della transazione
        for (ParkingTransactionItemBean parkingTransactionItemBean : parkingTransactionBean.getParkingTransactionItemList()) {
        
            if (parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue())) {
            
                System.out.println("Trovato item in stato CANCELED");
                
                System.out.println("Ottieni lo stato della transazione di pagamento");
                
                GestPayData gestPayDataReadTrx = gpService.callReadTrx(parkingTransactionItemBean.getShopLogin(), parkingTransactionItemBean.getParkingTransactionItemId(),
                        parkingTransactionItemBean.getBankTansactionID(), parkingTransactionItemBean.getAcquirerID(), parkingTransactionItemBean.getGroupAcquirer(), parkingDecodedSecretKey);

                if (gestPayDataReadTrx == null) {
                    System.out.println("errore nella operazione di riconciliazione, chiamata callReadTrx nulla verso l'acquirer(transazione: "
                            + parkingTransactionItemBean.getParkingTransactionItemId() + ")");
                    
                    decreaseAttempts(parkingTransactionInterfaceBean);

                    reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                    return reconciliationInfo;
                }
                
                if (gestPayDataReadTrx.getErrorCode().equals("2") && gestPayDataReadTrx.getErrorDescription().equalsIgnoreCase("Nessun ordine trovato")) {
                    
                    System.out.println("Non � stata trovata nessuna transazione di pagamento associata a questo item");
                    break;
                }
                    
                if (gestPayDataReadTrx.getTransactionState().equals("AUT") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK")) {
                
                    System.out.println("La transazione di pagamento si trova in stato AUT --> Cancella l'autorizzazione e crea l'evento CAN");
                    
                    System.out.println("Recupera l'evento AUT");
                    ParkingTransactionItemEventBean autParkingTransactionItemEventBean = null;
                    
                    for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : parkingTransactionItemBean.getParkingTransactionItemEventList()) {
                        
                        if(parkingTransactionItemEventBean.getEventType().equals("AUT") && parkingTransactionItemEventBean.getTransactionResult().equals("OK")) {
                            autParkingTransactionItemEventBean = parkingTransactionItemEventBean;
                            break;
                        }
                    }
                    
                    if (autParkingTransactionItemEventBean == null) {
                        System.out.println("Evento AUT non trovato");
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);

                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                    
                    // Se la transazione esiste e si trova in stato autorizzato cancella l'autorizzazione, crea lo stato CAN e chiudi l'item
                    PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(entityManager, parkingTransactionItemBean.getPaymentMethodId(), parkingTransactionItemBean.getPaymentMethodType());

                    if (paymentInfoBean == null) {
                        System.out.println("Metodo di pagamento non trovato");
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);

                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                    
                    Integer eventSequenceID = parkingTransactionItemBean.getLastEventSequenceID() + 1;
                    Integer statusSequenceID = parkingTransactionBean.getLastStatusSequenceID() + 1;

                    Date now = new Date();
                    String requestId = String.valueOf(now.getTime());
                    GestPayData gestPayDataDELETEResponse = doPaymentAuthDelete(parkingTransactionItemBean, paymentInfoBean, parkingTransactionBean.getUserBean(), paymentInfoBean.getId(),
                            autParkingTransactionItemEventBean.getEventAmount().doubleValue(), statusSequenceID, requestId, eventSequenceID, gpService,
                            parkingDecodedSecretKey);

                    System.out.println("Importo di Preautorizzazione :: " + autParkingTransactionItemEventBean.getEventAmount().doubleValue());
                    System.out.println("Tentativo di Storno => importo :: " + autParkingTransactionItemEventBean.getEventAmount().doubleValue());

                    if (gestPayDataDELETEResponse.getTransactionResult().equals("ERROR")) {

                        System.out.println("Errore nella cancellazione dell'autorizzazione del pagamento");
                        
                        decreaseAttempts(parkingTransactionInterfaceBean);
                        /*
                        ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH ERROR",
                                gestPayDataDELETEResponse.getErrorDescription());
                        entityManager.persist(parkingTransactionStatusBean);
                        */
                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                        return reconciliationInfo;
                    }

                    if (gestPayDataDELETEResponse.getTransactionResult().equals("KO")) {

                        System.out.println("KO nella cancellazione dell'autorizzazione del pagamento");
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);
                        /*
                        ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH KO",
                                gestPayDataDELETEResponse.getErrorDescription());
                        entityManager.persist(parkingTransactionStatusBean);
                        */
                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                }
                else {
                    
                    if (gestPayDataReadTrx.getTransactionState().equals("CAN") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK")) {
                        
                        // se la transazione � stata cancellata cerca l'evento CAN e se non esiste generalo e chiudi l'item
                        System.out.println("La transazione di pagamento si trova in stato CAN --> Crea l'evento CAN se non esiste");
                        
                        System.out.println("Recupera l'evento CAN");
                        ParkingTransactionItemEventBean canParkingTransactionItemEventBean = null;
                        
                        for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : parkingTransactionItemBean.getParkingTransactionItemEventList()) {
                            
                            if(parkingTransactionItemEventBean.getEventType().equals("CAN") && parkingTransactionItemEventBean.getTransactionResult().equals("OK")) {
                                canParkingTransactionItemEventBean = parkingTransactionItemEventBean;
                                break;
                            }
                        }
                        
                        if (canParkingTransactionItemEventBean == null) {
                            System.out.println("Evento CAN non trovato");
                            
                            Integer canStatusSequenceID = parkingTransactionBean.getLastStatusSequenceID() + 1;
                            Double amount = Double.parseDouble(gestPayDataReadTrx.getAmount());
                            
                            canParkingTransactionItemEventBean = this.generateParkingTransactionItemEvent(amount, canStatusSequenceID, parkingTransactionItemBean, gestPayDataReadTrx,
                                    "CAN");

                            entityManager.persist(canParkingTransactionItemEventBean);
                            
                        }
                        else {
                            System.out.println("Evento CAN gi� presente");
                        }
                    }
                    else {
                        
                        // se la transazione si trova in uno degli altri stati non bisogna fare nulla
                    }
                }
            }
            
            
            // TODO: Gestire gli altri possibili stati degli item
            
            if (parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue())) {
                
                System.out.println("Trovato item in stato AUTHORIZED");
                
                System.out.println("Ottieni lo stato della transazione di pagamento");
            }
            
            if (parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.ESTIMATED.getValue())) {
                
                System.out.println("Trovato item in stato ESTIMATED");
                
                System.out.println("Ottieni lo stato della transazione di pagamento");
            }
            
            if (parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue())) {
                
                System.out.println("Trovato item in stato SETTLED");
                
            }
            
            if (parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.UNAUTHORIZED.getValue())) {
                
                System.out.println("Trovato item in stato UNAUTHORIZED");
                
            }

        }
        
        reconciliationInfo.setFinalStatusType(StatusHelper.PARKING_STATUS_FAILED);
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
        
        parkingTransactionInterfaceBean.setToReconcilie(Boolean.FALSE);

        return reconciliationInfo;
        
        /*
        // Riconciliazione fallita
        parkingTransactionInterfaceBean.setToReconcilie(false);
        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
        entityManager.merge(parkingTransactionInterfaceBean);

        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

        return reconciliationInfo;
        */
        
        /*
        // Riconciliazione da riprovare
        decreaseAttempts(parkingTransactionInterfaceBean);

        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

        return reconciliationInfo;
        */
        
        
        /*
        // Riconciliazione completata con successo
        reconciliationInfo.setFinalStatusType(StatusHelper.PARKING_STATUS_FAILED);
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
        
        parkingTransactionInterfaceBean.setToReconcilie(Boolean.FALSE);

        return reconciliationInfo;
        */
    }

    @Override
    public String getStatus() {
        return StatusHelper.PARKING_STATUS_FAILED;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "PARKING PAYMENT DELETE ERROR";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<ParkingTransactionBean> parkingTransactionBeanList = QueryRepository.findParkingTransactionToBeReconciled(entityManager, getStatus());
        return parkingTransactionBeanList;
    }
}
