package com.techedge.mp.core.actions.reconciliation.status.postpaid;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.actions.reconciliation.status.EventResult;
import com.techedge.mp.core.actions.reconciliation.status.EventResultType;
import com.techedge.mp.core.actions.reconciliation.status.ReconciliationTransactionStatus;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PersonalDataBusinessBean;
import com.techedge.mp.core.business.model.PlateNumberBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityConstants;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.LoadLoyaltyCreditsResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.ElectronicInvoice;
import com.techedge.mp.forecourt.integration.shop.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.integration.shop.interfaces.SendMPTransactionResultMessageResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

public abstract class ReconciliationPostPaidTransactionStatus extends ReconciliationTransactionStatus {

    protected ForecourtPostPaidServiceRemote forecourtPPService  = null;
    protected GPServiceRemote                gpService           = null;
    protected FidelityServiceRemote          fidelityService     = null;
    protected Integer                        sequenceID          = 0;
    protected String                         newStatus           = null;
    protected String                         oldStatus           = null;
    protected String                         secretKey           = null;
    protected UserCategoryService            userCategoryService = null;
    protected StringSubstitution             stringSubstitution  = null;

    public ReconciliationPostPaidTransactionStatus(EntityManager entityManager, GPServiceRemote gpService, ForecourtPostPaidServiceRemote forecourtPPService, 
            FidelityServiceRemote fidelityService, EmailSenderRemote emailSenderService, String proxyHost, String proxyPort, String proxyNoHosts, String secretKey, 
            UserCategoryService userCategoryService, StringSubstitution stringSubstitution) {

        super(entityManager, emailSenderService, proxyHost, proxyPort, proxyNoHosts);

        this.forecourtPPService = forecourtPPService;
        this.gpService = gpService;
        this.fidelityService = fidelityService;
        this.secretKey = secretKey;
        this.userCategoryService = userCategoryService;
        this.stringSubstitution = stringSubstitution;
    }

    public int decreaseAttempts(TransactionInterfaceBean transactionInterfaceBean) {

        System.out.println("decreaseAttempts");

        int attempts = transactionInterfaceBean.getReconciliationAttemptsLeft().intValue();
        int returnAttempts;

        if (attempts < 1)
            return -1;

        //returnAttempts = transactionService.updateReconciliationAttempts(transactionBean.getTransactionID(), (attempts - 1));
        transactionInterfaceBean.setReconciliationAttemptsLeft((attempts - 1));
        entityManager.merge(transactionInterfaceBean);
        returnAttempts = (attempts - 1);

        System.out.println("Decremento dei tentativi di riconciliazione: " + attempts + " -> " + returnAttempts);

        return returnAttempts;
    }

    public SendMPTransactionResultMessageResponse notifyPostPaidPGFG(TransactionInterfaceBean transactionInterfaceBean, GestPayData gestPayData, String eventType,
            String eventStatusOk, String eventStatusError, String eventStatusCancelled, Integer eventSequenceID) {

        PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
        List<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail> vouchers = new ArrayList<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail>(0);

        for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidTransactionBean.getPostPaidConsumeVoucherBeanList()) {

            for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {

                com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail voucherDetail = new com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail();

                if (postPaidConsumeVoucherDetailBean.getConsumedValue() == null || postPaidConsumeVoucherDetailBean.getConsumedValue().equals(new Double("0.0"))) {
                    continue;
                }

                voucherDetail.setPromoCode(postPaidConsumeVoucherDetailBean.getPromoCode());
                voucherDetail.setPromoDescription(postPaidConsumeVoucherDetailBean.getPromoDescription());
                voucherDetail.setVoucherAmount(postPaidConsumeVoucherDetailBean.getConsumedValue());
                voucherDetail.setVoucherCode(postPaidConsumeVoucherDetailBean.getVoucherCode());

                System.out.println("voucher - promoCode: " + voucherDetail.getPromoCode() + ", promoDescription: " + voucherDetail.getPromoDescription() + ", voucherAmount: "
                        + voucherDetail.getVoucherAmount() + ", voucherCode: " + voucherDetail.getVoucherCode());

                vouchers.add(voucherDetail);
            }
        }

        SendMPTransactionResultMessageResponse sendMPTransactionResultMessageResponse = null;
        String notifyResponse = null;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = null;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION;
        boolean loyaltyCredits = isCreditsLoyaltyLoaded(postPaidTransactionBean);
        String requestID = String.valueOf(eventTimestamp.getTime());
        String stationID = postPaidTransactionBean.getStationBean().getStationID();
        String mpTransactionID = postPaidTransactionBean.getMpTransactionID();
        String srcTransactionID = postPaidTransactionBean.getSrcTransactionID();
        String transactionResult = postPaidTransactionBean.getMpTransactionStatus();
        PaymentTransactionResult paymentTransactionResult = generatePaymentTransactionResult(postPaidTransactionBean, gestPayData, postPaidTransactionBean.getAmount(), eventType);

        try {
            
            ElectronicInvoice electronicInvoice = null;
            
            Integer userType = postPaidTransactionBean.getUserBean().getUserType();            
            Boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());
            
            if (useBusiness) {
                System.out.println("Utente business. Creazione della fattura elettronica");
                PersonalDataBusinessBean personalDataBusinessBean = postPaidTransactionBean.getUserBean().getPersonalDataBusinessList().iterator().next();
            
                if (personalDataBusinessBean == null) {
                    throw new Exception("No business data for user: " + postPaidTransactionBean.getUserBean().getPersonalDataBean().getSecurityDataEmail());
                }

                electronicInvoice = new ElectronicInvoice();
                
                electronicInvoice.setAddress(personalDataBusinessBean.getAddress());
                electronicInvoice.setBusinessName(personalDataBusinessBean.getBusinessName());
                electronicInvoice.setCity(personalDataBusinessBean.getCity());
                electronicInvoice.setCountry(personalDataBusinessBean.getCountry());
                electronicInvoice.setEmailAdddress(postPaidTransactionBean.getUserBean().getPersonalDataBean().getSecurityDataEmail());
                
                // Ricerca la targa preferita
                String licensePlate = personalDataBusinessBean.getLicensePlate();
                for(PlateNumberBean plateNumberBean : postPaidTransactionBean.getUserBean().getPlateNumberList()) {
                    
                    if (plateNumberBean.getDefaultPlateNumber()) {
                        
                        licensePlate = plateNumberBean.getPlateNumber();
                        break;
                    }
                }
                
                electronicInvoice.setLicensePlate(licensePlate);
                electronicInvoice.setName(personalDataBusinessBean.getFirstName());
                electronicInvoice.setPecEmailAdddress(personalDataBusinessBean.getPecEmail());
                electronicInvoice.setProvince(personalDataBusinessBean.getProvince());
                electronicInvoice.setSdiCode(personalDataBusinessBean.getSdiCode());
                electronicInvoice.setStreetNumber(personalDataBusinessBean.getStreetNumber());
                electronicInvoice.setSurname(personalDataBusinessBean.getLastName());
                electronicInvoice.setVatNumber(personalDataBusinessBean.getVatNumber());
                electronicInvoice.setZipCode(personalDataBusinessBean.getZipCode());
                electronicInvoice.setFiscalCode(personalDataBusinessBean.getFiscalCode());
                
            }
            else {
                System.out.println("Utente non business. Creazione della fattura elettronica saltata");
            }
            
            sendMPTransactionResultMessageResponse = forecourtPPService.sendMPTransactionResult(requestID, stationID, mpTransactionID, srcTransactionID, transactionResult,
                    paymentTransactionResult, loyaltyCredits, vouchers, electronicInvoice);
            notifyResponse = sendMPTransactionResultMessageResponse.getStatusCode();
        }
        catch (Exception ex) {
            System.err.println("Exception in sendMpTransactionResult: " + ex.getLocalizedMessage());

            notifyResponse = "MESSAGE_ERROR_500";
            errorCode = "9999";
            errorDescription = "Error in sendMpTransactionResult (" + ex.getMessage() + ")";
            sendMPTransactionResultMessageResponse.setStatusCode(notifyResponse);
            sendMPTransactionResultMessageResponse.setMessageCode(ex.getMessage());
        }

        //if (!sendMPTransactionResultMessageResponse.getStatusCode().equals("MESSAGE_RECEIVED_200")) {}

        System.out.println("sendMPTransactionResult: " + notifyResponse);

        if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS)) {
            newStatus = eventStatusOk;
            eventResult = "OK";
        }
        else if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR)) {
            newStatus = (eventStatusError != null) ? eventStatusError : eventStatusOk;
            eventResult = "ERROR";
        }
        else if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
            newStatus = (eventStatusError != null) ? eventStatusError : eventStatusOk;
            eventResult = "ERROR";
            errorDescription = notifyResponse;
        }
        else {
            newStatus = (eventStatusCancelled != null) ? eventStatusCancelled : eventStatusOk;
            eventResult = "KO";
            errorDescription = notifyResponse;
        }

        PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID,
                eventTimestamp, transactionEvent, postPaidTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        oldStatus = newStatus;

        entityManager.persist(postPaidTransactionEventBean);

        return sendMPTransactionResultMessageResponse;
    }

    public SendMPTransactionResultMessageResponse notifyPostPaidPGFG(TransactionInterfaceBean transactionInterfaceBean, GestPayData gestPayData, String eventType,
            String eventStatusOk, Integer eventSequenceID) {

        return notifyPostPaidPGFG(transactionInterfaceBean, gestPayData, eventType, eventStatusOk, eventStatusOk, eventStatusOk, eventSequenceID);
    }

    private boolean isCreditsLoyaltyLoaded(PostPaidTransactionBean poPTransactionBean) {
        boolean loaded = false;

        for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {
            if (postPaidLoadLoyaltyCreditsBean.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {
                loaded = true;
                break;
            }
        }
        return loaded;
    }

    protected PaymentTransactionResult generatePaymentTransactionResult(PostPaidTransactionBean poPTransactionBean, GestPayData gestPayData, Double amount, String event) {
        PaymentTransactionResult paymentTransactionResult = new PaymentTransactionResult();
        paymentTransactionResult.setAmount(amount);
        paymentTransactionResult.setCurrency(poPTransactionBean.getCurrency());
        paymentTransactionResult.setAcquirerID(poPTransactionBean.getAcquirerID());
        paymentTransactionResult.setEventType(event);
        paymentTransactionResult.setShopLogin(poPTransactionBean.getShopLogin());
        paymentTransactionResult.setPaymentMode(poPTransactionBean.getPaymentMode());
        if (gestPayData != null) {
            paymentTransactionResult.setAuthorizationCode(gestPayData.getAuthorizationCode());
            paymentTransactionResult.setBankTransactionID(gestPayData.getBankTransactionID());
            paymentTransactionResult.setErrorCode(gestPayData.getErrorCode());
            paymentTransactionResult.setErrorDescription(gestPayData.getErrorDescription());
            paymentTransactionResult.setShopTransactionID(gestPayData.getShopTransactionID());
            paymentTransactionResult.setTransactionResult(gestPayData.getTransactionResult());
        }
        else {
            paymentTransactionResult.setAuthorizationCode(poPTransactionBean.getAuthorizationCode());
            paymentTransactionResult.setBankTransactionID(poPTransactionBean.getBankTansactionID());
            paymentTransactionResult.setShopTransactionID(poPTransactionBean.getMpTransactionID());
            paymentTransactionResult.setTransactionResult("KO");
        }

        return paymentTransactionResult;
    }

    protected PostPaidTransactionEventBean generateTransactionEvent(Integer eventSequenceID, String stateType, String requestID, Date eventTimestamp, String event,
            PostPaidTransactionBean poPTransactionBean, String result, String eventNewState, String eventOldState, String errorCode, String errorDescription) {

        PostPaidTransactionEventBean postPaidTransactionEventBean = new PostPaidTransactionEventBean();
        postPaidTransactionEventBean.setSequenceID(eventSequenceID);
        postPaidTransactionEventBean.setStateType(stateType);
        postPaidTransactionEventBean.setRequestID(requestID);
        postPaidTransactionEventBean.setEventTimestamp(eventTimestamp);
        postPaidTransactionEventBean.setEvent(event);
        postPaidTransactionEventBean.setTransactionBean(poPTransactionBean);
        postPaidTransactionEventBean.setResult(result);
        postPaidTransactionEventBean.setNewState(eventNewState);
        postPaidTransactionEventBean.setOldState(eventOldState);
        postPaidTransactionEventBean.setErrorCode(errorCode);
        postPaidTransactionEventBean.setErrorDescription(errorDescription);

        return postPaidTransactionEventBean;
    }

    protected PostPaidTransactionPaymentEventBean generateTransactionPaymentEvent(Integer eventSequenceID, PostPaidTransactionBean poPTransactionBean, GestPayData gestPayData,
            String event) {

        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean = new PostPaidTransactionPaymentEventBean();
        postPaidTransactionPaymentEventBean.setSequence(eventSequenceID);
        postPaidTransactionPaymentEventBean.setErrorCode(gestPayData.getErrorCode());
        postPaidTransactionPaymentEventBean.setErrorDescription(gestPayData.getErrorDescription());
        postPaidTransactionPaymentEventBean.setTransactionBean(poPTransactionBean);
        postPaidTransactionPaymentEventBean.setTransactionResult(gestPayData.getTransactionResult());
        postPaidTransactionPaymentEventBean.setAuthorizationCode(gestPayData.getAuthorizationCode());
        postPaidTransactionPaymentEventBean.setEventType(event);

        return postPaidTransactionPaymentEventBean;
    }

    public void updateFinalStatus(TransactionInterfaceBean transactionInterfaceBean, String finalStatus) {
        PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
        postPaidTransactionBean.setTransactionStatus(finalStatus);
        postPaidTransactionBean.setToReconcile(false);

        entityManager.merge(postPaidTransactionBean);
    }

    protected EventResult verifyVoucherTransaction(PostPaidTransactionBean postPaidTransactionBean) {

        PartnerType partnerType = PartnerType.MP;
        Double amount = 0.0;
        EventResult eventResult = new EventResult(EventResultType.SUCCESS);
        List<String> reversedOperationId = new ArrayList<String>();

        try {

            for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidTransactionBean.getPostPaidConsumeVoucherBeanList()) {

                if (postPaidConsumeVoucherBean.getOperationType().equals("REVERSE")
                /*&& postPaidConsumeVoucherBean.getStatusCode().equals(FidelityResponse.REVERSE_CONSUME_VOUCHER_OK)*/) {
                    reversedOperationId.add(postPaidConsumeVoucherBean.getOperationIDReversed());
                }
            }
            System.out.println("Trovate " + reversedOperationId.size() + " transazioni stornate");

            for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidTransactionBean.getPostPaidConsumeVoucherBeanList()) {

                if (!postPaidConsumeVoucherBean.getOperationType().equals("CONSUME") && postPaidConsumeVoucherBean.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {
                    System.out.println("Operatione non valida! OperationType: " + postPaidConsumeVoucherBean.getOperationType() + "  StatusCode: "
                            + postPaidConsumeVoucherBean.getStatusCode());
                    continue;
                }

                if (reversedOperationId.contains(postPaidConsumeVoucherBean.getOperationID())) {
                    System.out.println("OperationId gi� stornata: " + postPaidConsumeVoucherBean.getOperationID());
                    continue;
                }

                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                Long requestTimestamp = new Date().getTime();
                String operationIDtoCheck = postPaidConsumeVoucherBean.getOperationID();

                System.out.println("Controllo consumo voucher transazione " + postPaidTransactionBean.getMpTransactionID());
                CheckConsumeVoucherTransactionResult checkConsumeVoucherTransactionResult = fidelityService.checkConsumeVoucherTransaction(operationID, operationIDtoCheck,
                        partnerType, requestTimestamp);
                List<VoucherDetail> voucherDetailList = checkConsumeVoucherTransactionResult.getVoucherList();

                for (VoucherDetail voucherDetail : voucherDetailList) {
                    if (voucherDetail.getConsumedValue().doubleValue() > 0.0) {
                        System.out.println("Trovato voucher consumato: " + voucherDetail.getVoucherCode());
                        amount += voucherDetail.getConsumedValue().doubleValue();
                    }
                }
            }
        }
        catch (Exception ex) {
            System.out.println("Exception checkConsumeVoucherTransaction: " + ex.getMessage());
            eventResult.setResult(EventResultType.TO_RETRY);
        }

        eventResult.setAmount(amount);
        return eventResult;
    }

    protected EventResult verifyLoyaltyTransaction(PostPaidTransactionBean postPaidTransactionBean) {

        PartnerType partnerType = PartnerType.MP;
        Integer credits = 0;
        EventResult eventResult = new EventResult(EventResultType.SUCCESS);
        List<String> reversedOperationId = new ArrayList<String>();

        try {

            for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : postPaidTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {

                if (postPaidLoadLoyaltyCreditsBean.getOperationType().equals("REVERSE")
                /*&& postPaidLoadLoyaltyCreditsBean.getStatusCode().equals(FidelityResponse.REVERSE_LOAD_LOYALTY_CREDITS_OK)*/) {
                    reversedOperationId.add(postPaidLoadLoyaltyCreditsBean.getOperationIDReversed());
                }
            }

            for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : postPaidTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {

                if (!postPaidLoadLoyaltyCreditsBean.getOperationType().equals("LOAD") && postPaidLoadLoyaltyCreditsBean.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {
                    System.out.println("Operatione non valida! OperationType: " + postPaidLoadLoyaltyCreditsBean.getOperationType() + "  StatusCode: "
                            + postPaidLoadLoyaltyCreditsBean.getStatusCode());
                    continue;
                }

                if (reversedOperationId.contains(postPaidLoadLoyaltyCreditsBean.getOperationID())) {
                    System.out.println("OperationId gi� stornata: " + postPaidLoadLoyaltyCreditsBean.getOperationID());
                    continue;
                }
                
                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                Long requestTimestamp = new Date().getTime();
                String operationIDtoCheck = postPaidLoadLoyaltyCreditsBean.getOperationID();

                System.out.println("Controllo caricamento punti transazione " + postPaidTransactionBean.getMpTransactionID());
                CheckLoadLoyaltyCreditsTransactionResult checkLoadLoyaltyCreditsTransactionResult = fidelityService.checkLoadLoyaltyCreditsTransaction(operationID,
                        operationIDtoCheck, partnerType, requestTimestamp);
                credits += checkLoadLoyaltyCreditsTransactionResult.getCredits();

            }
        }
        catch (Exception ex) {
            System.out.println("Exception revertLoadLoyaltyCredits: " + ex.getMessage());
            eventResult.setResult(EventResultType.TO_RETRY);
        }

        eventResult.setAmount(credits);
        return eventResult;
    }

    protected EventResult loadLoyaltyCredits(PostPaidTransactionBean postPaidTransactionBean, Integer eventSequenceID) {
        String refuelMode = null;
        int totalTransactionTask = 1;
        UserBean userBean = postPaidTransactionBean.getUserBean();
        String errorCode = null;
        String errorDescription = null;
        String resultString = "OK";
        EventResult eventResult = new EventResult(EventResultType.SUCCESS);
        newStatus = StatusHelper.POST_PAID_STATUS_CREDITS_LOAD;
        eventSequenceID += 1;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY;
        String requestID = String.valueOf(new Date().getTime());
        PartnerType partnerType = PartnerType.MP;
        String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;
        String mpTransactionID = postPaidTransactionBean.getMpTransactionID();
        String stationID = postPaidTransactionBean.getStationBean().getStationID();
        String BIN = null;
        PaymentInfoBean paymentInfoBean = null;

        if (postPaidTransactionBean.getPaymentMethodId() == null && postPaidTransactionBean.getPaymentMethodType() == null) {

            paymentInfoBean = userBean.findDefaultPaymentInfoBean();
        }
        else {

            paymentInfoBean = userBean.findPaymentInfoBean(postPaidTransactionBean.getPaymentMethodId(), postPaidTransactionBean.getPaymentMethodType());
        }

        BIN = paymentInfoBean.getCardBin();

        if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {
            paymentMode = FidelityConstants.PAYMENT_METHOD_VOUCHER;
        }
        else if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
            paymentMode = FidelityConstants.PAYMENT_METHOD_MULTICARD;
        }
        else if (BIN != null && QueryRepository.findCardBinExists(entityManager, BIN)) {
            System.out.println("BIN valido (" + BIN + ")");
            paymentMode = FidelityConstants.PAYMENT_METHOD_ENI_CARD;
        }
        else {
            System.out.println("BIN Non Valido (" + BIN + ")");
        }

        if (postPaidTransactionBean.getRefuelBean().size() > 1) {
            totalTransactionTask = postPaidTransactionBean.getRefuelBean().size();
        }
        else {
            refuelMode = FidelityConstants.REFUEL_MODE_ENI_CAFE_ONLY;
        }

        PostPaidRefuelBean[] refuelBeanArray = new PostPaidRefuelBean[postPaidTransactionBean.getRefuelBean().size()];
        postPaidTransactionBean.getRefuelBean().toArray(refuelBeanArray);

        for (int index = 0; index < totalTransactionTask; index++) {

            Boolean productBDFound = Boolean.FALSE;
            
            refuelMode = null;
            List<ProductDetail> productList = new ArrayList<ProductDetail>(0);
            if (!postPaidTransactionBean.getRefuelBean().isEmpty()) {
                PostPaidRefuelBean refuelBean = refuelBeanArray[index];
                System.out.println("trovato refuel mode: " + refuelBean.getRefuelMode());

                if (refuelBean.getRefuelMode().equalsIgnoreCase("servito")) {
                    refuelMode = FidelityConstants.REFUEL_MODE_SERVITO;
                }

                if (refuelBean.getRefuelMode().equalsIgnoreCase("fai_da_te")) {
                    refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_POSTPAY;
                }

                if (refuelMode == null) {
                    System.out.println("Tipo di rifornimento sconosciuto. Operazione scartata!");
                }

                ProductDetail productDetail = new ProductDetail();
                productDetail.setAmount(refuelBean.getFuelAmount());

                System.out.println("trovato product id: " + refuelBean.getProductId());

                if (refuelBean.getProductId().equals("SP")) {

                    // sp
                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_SP);
                }
                else {

                    if (refuelBean.getProductId().equals("GG")) {

                        // gasolio
                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GASOLIO);
                    }
                    else {

                        if (refuelBean.getProductId().equals("BS")) {

                            // blue_super
                            productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_SUPER);
                        }
                        else {

                            if (refuelBean.getProductId().equals("BD")) {

                                // blue_diesel
                                productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_DIESEL);
                                productBDFound = Boolean.TRUE;
                            }
                            else {

                                if (refuelBean.getProductId().equals("MT")) {

                                    // metano
                                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_METANO);
                                }
                                else {

                                    if (refuelBean.getProductId().equals("GP")) {

                                        // gpl
                                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GPL);
                                    }
                                    else {

                                        if (refuelBean.getProductId().equals("AD")) {

                                            // ???
                                            productDetail.setProductCode(null);
                                        }
                                        else {

                                            // non_oil
                                            productDetail.setProductCode(null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                productDetail.setQuantity(refuelBean.getFuelQuantity());
                productList.add(productDetail);
            }

            if (index == 0 && !postPaidTransactionBean.getCartBean().isEmpty()) {
                System.out.println("trovato shop");

                if (refuelMode == null) {
                    refuelMode = FidelityConstants.REFUEL_MODE_ENI_CAFE_ONLY;
                }

                boolean validProductFound = false;
                Double shopAmount = 0.0;
                Integer shopCount = 0;

                for (PostPaidCartBean postPaidCartBean : postPaidTransactionBean.getCartBean()) {

                    if (postPaidCartBean.getProductId().equals(FidelityConstants.PRODUCT_CODE_NON_OIL))     // PRODOTTI NON OIL 70
                    {
                        validProductFound = true;
                        shopAmount = shopAmount + postPaidCartBean.getAmount();
                        shopCount = shopCount + postPaidCartBean.getQuantity();

                        System.out.println("amount: " + postPaidCartBean.getAmount() + ", count: " + postPaidCartBean.getQuantity());
                    }
                }

                if (validProductFound) {
                    ProductDetail shopProductDetail = new ProductDetail();

                    shopProductDetail.setAmount(shopAmount);
                    shopProductDetail.setProductCode(FidelityConstants.PRODUCT_CODE_NON_OIL);
                    shopProductDetail.setQuantity(Double.valueOf(shopCount));

                    productList.add(shopProductDetail);
                }
            }

            System.out.println("Controllo caricamento punti carta fedelt�");
            
            // Se l'utente ha una carta loyalty associata allora bisogna effettuare anche il caricamento dei punti
            /* Inizio modifica per utilizzare solo le nuove carte dematerializzate
             * LoyaltyCardBean loyaltyCardBean = userBean.getFirstLoyaltyCard();
             * Fine modifica
             */
            LoyaltyCardBean loyaltyCardBean = userBean.getVirtualLoyaltyCard();

            if (!productList.isEmpty() && (loyaltyCardBean != null || (refuelMode == FidelityConstants.REFUEL_MODE_SERVITO && productBDFound))) {

                String fiscalCode = "";
                String panCode    = "0000000000000000000";
                String eanCode    = "0000000000000";
                
                if (loyaltyCardBean != null) {
                    System.out.println("Trovata carta fedelt�");
                    panCode = loyaltyCardBean.getPanCode();
                    eanCode = loyaltyCardBean.getEanCode();
                }
                else {
                    System.out.println("Carta fedelt� non presente ma rifornimento in servito -> invio richiesta con fiscalCode");
                    fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                }

                // Chimamata al servizio loadLoyaltyCredits

                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                Long requestTimestamp = new Date().getTime();

                PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean();
                postPaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                postPaidLoadLoyaltyCreditsBean.setOperationType("LOAD");
                postPaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                postPaidLoadLoyaltyCreditsBean.setEanCode(eanCode);

                try {

                    System.out.println("Chiamata servizio di carico punti");

                    LoadLoyaltyCreditsResult loadLoyaltyCreditsResult = fidelityService.loadLoyaltyCredits(operationID, mpTransactionID, stationID, panCode,
                            BIN, refuelMode, paymentMode, "it", partnerType, requestTimestamp, fiscalCode, productList);

                    System.out.println("Risposta servizio di carico punti: " + loadLoyaltyCreditsResult.getStatusCode() + " (" + loadLoyaltyCreditsResult.getMessageCode() + ")");

                    postPaidLoadLoyaltyCreditsBean.setBalance(loadLoyaltyCreditsResult.getBalance());
                    postPaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                    postPaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                    postPaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                    postPaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                    postPaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                    postPaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                    postPaidLoadLoyaltyCreditsBean.setCsTransactionID(loadLoyaltyCreditsResult.getCsTransactionID());
                    postPaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                    postPaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                    postPaidLoadLoyaltyCreditsBean.setMessageCode(loadLoyaltyCreditsResult.getMessageCode());
                    postPaidLoadLoyaltyCreditsBean.setStatusCode(loadLoyaltyCreditsResult.getStatusCode());
                    postPaidLoadLoyaltyCreditsBean.setWarningMsg(loadLoyaltyCreditsResult.getWarningMsg());

                    postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(postPaidTransactionBean);
                    entityManager.persist(postPaidLoadLoyaltyCreditsBean);

                    if (loadLoyaltyCreditsResult.getVoucherList() != null && !loadLoyaltyCreditsResult.getVoucherList().isEmpty()) {
                        for(VoucherDetail voucherDetail : loadLoyaltyCreditsResult.getVoucherList()) {
                            PostPaidLoadLoyaltyCreditsVoucherBean postPaidLoadLoyaltyCreditsVoucherBean = new PostPaidLoadLoyaltyCreditsVoucherBean();
                            postPaidLoadLoyaltyCreditsVoucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                            postPaidLoadLoyaltyCreditsVoucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                            postPaidLoadLoyaltyCreditsVoucherBean.setInitialValue(voucherDetail.getInitialValue());
                            postPaidLoadLoyaltyCreditsVoucherBean.setPromoCode(voucherDetail.getPromoCode());
                            postPaidLoadLoyaltyCreditsVoucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                            postPaidLoadLoyaltyCreditsVoucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                            postPaidLoadLoyaltyCreditsVoucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                            postPaidLoadLoyaltyCreditsVoucherBean.setCode(voucherDetail.getVoucherCode());
                            postPaidLoadLoyaltyCreditsVoucherBean.setStatus(voucherDetail.getVoucherStatus());
                            postPaidLoadLoyaltyCreditsVoucherBean.setType(voucherDetail.getVoucherType());
                            postPaidLoadLoyaltyCreditsVoucherBean.setValue(voucherDetail.getVoucherValue());
                            postPaidLoadLoyaltyCreditsVoucherBean.setPostPaidLoadLoyaltyCreditsBean(postPaidLoadLoyaltyCreditsBean);
                            entityManager.persist(postPaidLoadLoyaltyCreditsVoucherBean);
                            postPaidLoadLoyaltyCreditsBean.getPostPaidLoadLoyaltyCreditsVoucherBean().add(postPaidLoadLoyaltyCreditsVoucherBean);
                            
                        }
                    }
                    
                    entityManager.merge(postPaidLoadLoyaltyCreditsBean);
                    
                    postPaidTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(postPaidLoadLoyaltyCreditsBean);

                    //em.persist(postPaidLoadLoyaltyCreditsBean);
                    //em.merge(poPTransactionBean);

                    if (!loadLoyaltyCreditsResult.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {
                        errorCode = loadLoyaltyCreditsResult.getStatusCode();
                        errorDescription = loadLoyaltyCreditsResult.getMessageCode();
                        resultString = "KO";
                    }
                    else {
                        errorCode = null;
                        errorDescription = null;
                        resultString = "OK";
                    }

                }
                catch (Exception e) {

                    System.out.println("Error loading loyalty credits: " + e.getMessage());

                    postPaidLoadLoyaltyCreditsBean.setStatusCode(StatusHelper.LOYALTY_STATUS_TO_BE_VERIFIED);
                    postPaidLoadLoyaltyCreditsBean.setMessageCode("CARICAMENTO DA VERIFICARE (" + e.getMessage() + ")");
                    postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(postPaidTransactionBean);
                    postPaidTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(postPaidLoadLoyaltyCreditsBean);

                    errorCode = "9999";
                    errorDescription = "Error loading loyalty credits (" + e.getMessage() + ")";
                    resultString = "ERROR";
                    eventResult.setResult(EventResultType.TO_RETRY);
                    eventResult.setErrorCode(errorCode);
                }

                PostPaidTransactionEventBean postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION,
                        requestID, eventTimestamp, transactionEvent, postPaidTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);

                entityManager.persist(postPaidTransactionEventBean);

            }
        }

        oldStatus = newStatus;
        return eventResult;
    }

    protected EventResult revertConsumeVoucher(PostPaidTransactionBean poPTransactionBean, Integer eventSequenceID) {

        System.out.println("Storno consume voucher transazione " + poPTransactionBean.getMpTransactionID());

        ArrayList<PostPaidConsumeVoucherBean> reversedPostPaidConsumeVoucherBeanList = new ArrayList<PostPaidConsumeVoucherBean>(0);
        String errorCode = null;
        String errorDescription = null;
        String resultString = "OK";
        EventResult eventResult = new EventResult(EventResultType.SUCCESS);
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_VOUCHER_REVERSE;
        String requestID = String.valueOf(new Date().getTime());
        newStatus = StatusHelper.POST_PAID_STATUS_VOUCHER_REVERSE;

        if (poPTransactionBean.getPostPaidConsumeVoucherBeanList().isEmpty()) {
            System.out.println("Storno consumo voucher non necessario. Nessun voucher trovato");
        }

        List<String> reversedOperationId = new ArrayList<String>();

        for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : poPTransactionBean.getPostPaidConsumeVoucherBeanList()) {

            if (postPaidConsumeVoucherBean.getOperationType().equals("REVERSE")
            /*&& postPaidConsumeVoucherBean.getStatusCode().equals(FidelityResponse.REVERSE_CONSUME_VOUCHER_OK)*/) {
                reversedOperationId.add(postPaidConsumeVoucherBean.getOperationIDReversed());
            }
        }

        for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : poPTransactionBean.getPostPaidConsumeVoucherBeanList()) {
            // Bisogna effettuare lo storno solo se l'operazione di consumo voucher � andata a buon fine

            System.out.println("consume voucher status code: " + postPaidConsumeVoucherBean.getStatusCode());

            if (!postPaidConsumeVoucherBean.getOperationType().equals("CONSUME") && postPaidConsumeVoucherBean.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {
                System.out.println("Operatione non valida! OperationType: " + postPaidConsumeVoucherBean.getOperationType() + "  StatusCode: "
                        + postPaidConsumeVoucherBean.getStatusCode());
                continue;
            }

            if (reversedOperationId.contains(postPaidConsumeVoucherBean.getOperationID())) {
                System.out.println("OperationId gi� stornata: " + postPaidConsumeVoucherBean.getOperationID());
                continue;
            }

            // Chimata al servizio reverseConsumeVoucherTransaction

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            String operationIDtoReverse = postPaidConsumeVoucherBean.getOperationID();
            PartnerType partnerType = PartnerType.MP;
            Long requestTimestamp = new Date().getTime();

            try {

                ReverseConsumeVoucherTransactionResult reverseConsumeVoucherTransactionResult = fidelityService.reverseConsumeVoucherTransaction(operationID, operationIDtoReverse,
                        partnerType, requestTimestamp);

                // Aggiungi una riga per loggare l'esito dell'operazione di storno
                PostPaidConsumeVoucherBean reversePostPaidConsumeVoucherBean = new PostPaidConsumeVoucherBean();
                reversePostPaidConsumeVoucherBean.setCsTransactionID(reverseConsumeVoucherTransactionResult.getCsTransactionID());
                reversePostPaidConsumeVoucherBean.setMessageCode(reverseConsumeVoucherTransactionResult.getMessageCode());
                reversePostPaidConsumeVoucherBean.setOperationID(operationID);
                reversePostPaidConsumeVoucherBean.setOperationIDReversed(operationIDtoReverse);
                reversePostPaidConsumeVoucherBean.setOperationType("REVERSE");
                reversePostPaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
                reversePostPaidConsumeVoucherBean.setStatusCode(reverseConsumeVoucherTransactionResult.getStatusCode());
                reversePostPaidConsumeVoucherBean.setTotalConsumed(0.0);
                reversePostPaidConsumeVoucherBean.setPostPaidTransactionBean(poPTransactionBean);

                reversedPostPaidConsumeVoucherBeanList.add(reversePostPaidConsumeVoucherBean);
                //poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(reversePostPaidConsumeVoucherBean);

                // Ripristina il valore iniziale dei voucher consumati

                String operationID_check = new IdGenerator().generateId(16).substring(0, 33);
                VoucherConsumerType voucherConsumerType_check = VoucherConsumerType.ENI;
                PartnerType partnerType_check = PartnerType.MP;
                Long requestTimestamp_check = new Date().getTime();

                List<VoucherCodeDetail> voucherCodeList_check = new ArrayList<VoucherCodeDetail>(0);

                for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {

                    VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                    voucherCodeDetail.setVoucherCode(postPaidConsumeVoucherDetailBean.getVoucherCode());
                    voucherCodeList_check.add(voucherCodeDetail);
                }

                CheckVoucherResult checkVoucherResult = new CheckVoucherResult();

                try {

                    checkVoucherResult = fidelityService.checkVoucher(operationID_check, voucherConsumerType_check, partnerType_check, requestTimestamp_check,
                            voucherCodeList_check);
                    // Verifica l'esito del check
                    String checkVoucherStatusCode = checkVoucherResult.getStatusCode();

                    if (!checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_OK)) {
                        // Error
                        System.out.println("Error checking voucher: " + checkVoucherStatusCode);
                        //throw new FidelityServiceException("Error checking voucher: " + checkVoucherStatusCode);
                    }
                    else {

                        // Aggiorna lo stato dei voucher dell'utente

                        // Ricerca il voucher nella risposta del servizio di verifica
                        for (VoucherDetail voucherDetail : checkVoucherResult.getVoucherList()) {

                            System.out.println("Elaborazione voucher: " + voucherDetail.getVoucherCode() + " in stato: " + voucherDetail.getVoucherStatus());

                            for (VoucherBean voucherBean : poPTransactionBean.getUserBean().getVoucherList()) {

                                if (voucherBean.getCode().equals(voucherDetail.getVoucherCode())) {

                                    System.out.println("Aggiornamento Voucher " + voucherDetail.getVoucherCode());

                                    // Aggiorna il voucher con i dati restituiti dal servizio
                                    voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                                    voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                                    voucherBean.setInitialValue(voucherDetail.getInitialValue());
                                    voucherBean.setPromoCode(voucherDetail.getPromoCode());
                                    voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                                    voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                                    voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                                    voucherBean.setCode(voucherDetail.getVoucherCode());
                                    voucherBean.setStatus(voucherDetail.getVoucherStatus());
                                    voucherBean.setType(voucherDetail.getVoucherType());
                                    voucherBean.setValue(voucherDetail.getVoucherValue());

                                    System.out.println("Voucher aggiornato");
                                }
                            }
                        }

                        // Aggiorna l'utente
                        entityManager.merge(poPTransactionBean.getUserBean());
                    }
                }
                catch (Exception ex) {
                    System.out.println("Error in checkVoucherResult: " + ex.getMessage());
                    //throw new FidelityServiceException("Error in checkVoucherResult: " + e.getMessage());
                }

                if (!reverseConsumeVoucherTransactionResult.getStatusCode().equals(FidelityResponse.REVERSE_CONSUME_VOUCHER_TRANSACTION_OK)) {

                    System.out.println("Error in reverseConsumeVoucherTransaction: " + reverseConsumeVoucherTransactionResult.getStatusCode());

                    errorCode = reverseConsumeVoucherTransactionResult.getStatusCode();
                    errorDescription = reverseConsumeVoucherTransactionResult.getMessageCode();
                    resultString = "KO";
                }

                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);

                entityManager.persist(postPaidTransactionEventBean);
            }
            catch (FidelityServiceException ex) {
                PostPaidConsumeVoucherBean newPostPaidConsumeVoucherBean = new PostPaidConsumeVoucherBean();
                newPostPaidConsumeVoucherBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
                newPostPaidConsumeVoucherBean.setMessageCode("STORNO DA VERIFICARE (" + ex.getMessage() + ")");
                newPostPaidConsumeVoucherBean.setTotalConsumed(postPaidConsumeVoucherBean.getTotalConsumed());
                newPostPaidConsumeVoucherBean.setPostPaidTransactionBean(poPTransactionBean);
                newPostPaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
                newPostPaidConsumeVoucherBean.setOperationID(operationID);
                newPostPaidConsumeVoucherBean.setOperationIDReversed(operationIDtoReverse);
                newPostPaidConsumeVoucherBean.setTotalConsumed(0.0);
                newPostPaidConsumeVoucherBean.setOperationType(postPaidConsumeVoucherBean.getOperationType());

                reversedPostPaidConsumeVoucherBeanList.add(newPostPaidConsumeVoucherBean);
                //poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(newPostPaidConsumeVoucherBean);

                errorCode = "9999";
                errorDescription = ex.getMessage();
                resultString = "ERROR";
                eventResult.setResult(EventResultType.TO_RETRY);
                eventResult.setErrorCode(errorCode);

                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);

                entityManager.persist(postPaidTransactionEventBean);
            }
            catch (Exception ex) {
                errorCode = "9999";
                errorDescription = ex.getMessage();
                resultString = "ERROR";

                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);

                entityManager.persist(postPaidTransactionEventBean);
            }

            eventSequenceID += 1;
        }

        for (PostPaidConsumeVoucherBean revertedPostPaidConsumeVoucherBean : reversedPostPaidConsumeVoucherBeanList) {
            poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(revertedPostPaidConsumeVoucherBean);
        }

        eventSequenceID = sequenceID;
        oldStatus = newStatus;

        return eventResult;

    }

    public EventResult revertLoadLoyaltyCredits(PostPaidTransactionBean poPTransactionBean, Integer eventSequenceID) {

        System.out.println("Storno load loyalty credits transazione " + poPTransactionBean.getMpTransactionID());

        ArrayList<PostPaidLoadLoyaltyCreditsBean> revertedPostPaidLoadLoyaltyCreditsBeanList = new ArrayList<PostPaidLoadLoyaltyCreditsBean>(0);
        String errorCode = null;
        String errorDescription = null;
        String resultString = "OK";
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY_REVERSE;
        String requestID = String.valueOf(new Date().getTime());
        EventResult eventResult = new EventResult(EventResultType.SUCCESS);

        if (poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().isEmpty()) {
            System.out.println("Storno caricamento punti non necessario. Nessun caricamento trovato");
        }

        List<String> reversedOperationId = new ArrayList<String>();

        for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {

            if (postPaidLoadLoyaltyCreditsBean.getOperationType().equals("REVERSE")
            /*&& postPaidLoadLoyaltyCreditsBean.getStatusCode().equals(FidelityResponse.REVERSE_LOAD_LOYALTY_CREDITS_OK)*/) {
                reversedOperationId.add(postPaidLoadLoyaltyCreditsBean.getOperationIDReversed());
            }
        }

        for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {

            System.out.println("load loyalty credits status code: " + postPaidLoadLoyaltyCreditsBean.getStatusCode());
            // Bisogna effettuare lo storno solo se l'operazione di caricamento loyalty � andata a buon fine
            // Chimamata al servizio reverseLoadLoyaltyCreditsTransaction

            if (!postPaidLoadLoyaltyCreditsBean.getOperationType().equals("LOAD")
                    && postPaidLoadLoyaltyCreditsBean.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {
                System.out.println("Operatione non valida! OperationType: " + postPaidLoadLoyaltyCreditsBean.getOperationType() + "  StatusCode: "
                        + postPaidLoadLoyaltyCreditsBean.getStatusCode());
                continue;
            }

            if (reversedOperationId.contains(postPaidLoadLoyaltyCreditsBean.getOperationID())) {
                System.out.println("OperationId gi� stornata: " + postPaidLoadLoyaltyCreditsBean.getOperationID());
                continue;
            }

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            String operationIDtoReverse = postPaidLoadLoyaltyCreditsBean.getOperationID();
            PartnerType partnerType = PartnerType.MP;
            Long requestTimestamp = new Date().getTime();
            newStatus = StatusHelper.POST_PAID_STATUS_LOYALTY_REVERSE;

            try {
                ReverseLoadLoyaltyCreditsTransactionResult reverseLoadLoyaltyCreditsTransactionResult = fidelityService.reverseLoadLoyaltyCreditsTransaction(operationID,
                        operationIDtoReverse, partnerType, requestTimestamp);

                // Aggiungi una riga per loggare l'esito dell'operazione di storno

                PostPaidLoadLoyaltyCreditsBean reversePostPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean();
                //reversePostPaidLoadLoyaltyCreditsBean.setBalance(reverseLoadLoyaltyCreditsTransactionResult.getBalance());
                //reversePostPaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                //reversePostPaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                reversePostPaidLoadLoyaltyCreditsBean.setCsTransactionID(reverseLoadLoyaltyCreditsTransactionResult.getCsTransactionID());
                //reversePostPaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                //reversePostPaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                reversePostPaidLoadLoyaltyCreditsBean.setMessageCode(reverseLoadLoyaltyCreditsTransactionResult.getMessageCode());
                reversePostPaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                reversePostPaidLoadLoyaltyCreditsBean.setOperationIDReversed(operationIDtoReverse);
                reversePostPaidLoadLoyaltyCreditsBean.setOperationType("REVERSE");
                reversePostPaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                reversePostPaidLoadLoyaltyCreditsBean.setStatusCode(reverseLoadLoyaltyCreditsTransactionResult.getStatusCode());

                reversePostPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);

                revertedPostPaidLoadLoyaltyCreditsBeanList.add(reversePostPaidLoadLoyaltyCreditsBean);

                if (!reverseLoadLoyaltyCreditsTransactionResult.getStatusCode().equals(FidelityResponse.REVERSE_LOAD_LOYALTY_CREDITS_OK)) {

                    System.out.println("Error in reverseLoadLoyaltyCreditsTransaction: " + reverseLoadLoyaltyCreditsTransactionResult.getStatusCode());

                    errorCode = reverseLoadLoyaltyCreditsTransactionResult.getStatusCode();
                    errorDescription = reverseLoadLoyaltyCreditsTransactionResult.getMessageCode();
                    resultString = "KO";
                }

                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);

                entityManager.persist(postPaidTransactionEventBean);

            }
            catch (FidelityServiceException ex) {
                PostPaidLoadLoyaltyCreditsBean newPostPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean();
                newPostPaidLoadLoyaltyCreditsBean.setStatusCode(StatusHelper.LOYALTY_STATUS_TO_BE_VERIFIED);
                newPostPaidLoadLoyaltyCreditsBean.setMessageCode("STORNO DA VERIFICARE (" + ex.getMessage() + ")");
                newPostPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);
                newPostPaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                newPostPaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                newPostPaidLoadLoyaltyCreditsBean.setOperationIDReversed(operationIDtoReverse);
                newPostPaidLoadLoyaltyCreditsBean.setOperationType(postPaidLoadLoyaltyCreditsBean.getOperationType());

                errorCode = "9999";
                errorDescription = ex.getMessage();
                eventResult.setResult(EventResultType.TO_RETRY);
                eventResult.setErrorCode(errorCode);
                resultString = "ERROR";

                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);

                entityManager.persist(postPaidTransactionEventBean);
            }
            catch (Exception ex) {
                errorCode = "9999";
                errorDescription = ex.getMessage();
                resultString = "ERROR";

                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);

                entityManager.persist(postPaidTransactionEventBean);
            }

            eventSequenceID += 1;
        }

        for (PostPaidLoadLoyaltyCreditsBean revertedPostPaidLoadLoyaltyCreditsBean : revertedPostPaidLoadLoyaltyCreditsBeanList) {
            poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(revertedPostPaidLoadLoyaltyCreditsBean);
        }

        eventSequenceID = sequenceID;
        oldStatus = newStatus;

        return eventResult;
    }

}
