package com.techedge.mp.core.actions.reconciliation.status.voucher;

import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionEventBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.EventType;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

public class PaymentAuthErrorStatus extends ReconciliationVoucherTransactionStatus {

    public PaymentAuthErrorStatus(EntityManager entityManager, GPServiceRemote gpService, FidelityServiceRemote fidelityService, 
            EmailSenderRemote emailSenderService, String proxyHost, String proxyPort, String proxyNoHosts) {
    
        super(entityManager, gpService, fidelityService, emailSenderService, proxyHost, proxyPort, proxyNoHosts);
    }

    @Override
    public ReconciliationInfo reconciliateCreditCardFlow(TransactionInterfaceBean transactionInterfaceBean) {
        System.out.println("Riconciliazione non disponibile per questo tipo di utente!");
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        reconciliationInfo.setFinalStatusType(transactionInterfaceBean.getTransactionStatus());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
        return reconciliationInfo;
    }
    
    @Override
    public ReconciliationInfo reconciliateVoucherFlow(TransactionInterfaceBean transactionInterfaceBean) {
        VoucherTransactionBean voucherTransactionBean = (VoucherTransactionBean) transactionInterfaceBean;
        GestPayData gestPayData = null;
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        reconciliationInfo.setTransactionID(voucherTransactionBean.getTransactionID());
        
        VoucherTransactionStatusBean voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(StatusHelper.VOUCHER_STATUS_RECONCILIATION, "START RECONCILIATION", null);
        entityManager.merge(voucherTransactionStatusBean);

        System.out.println("chiamata a callReadTrx di Banca Sella");
        gestPayData = gpService.callReadTrx(voucherTransactionBean.getShopLogin(), voucherTransactionBean.getTransactionID(), voucherTransactionBean.getBankTansactionID(),
                voucherTransactionBean.getAcquirerID(), voucherTransactionBean.getGroupAcquirer(), voucherTransactionBean.getEncodedSecretKey());
        
        if (gestPayData == null) {
            System.out.println("errore nella operazione di riconciliazione, chiamata callReadTrx nulla verso Banca Sella (transazione: " + voucherTransactionBean.getTransactionID() + ")");
            reconciliationInfo.setFinalStatusType(voucherTransactionBean.getFinalStatusType());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(voucherTransactionBean);
            return reconciliationInfo;
        }

        if (gestPayData.getTransactionState().equals("AUT") && gestPayData.getTransactionResult().equalsIgnoreCase("OK")) {
            System.out.println("chiamata a deletePagam di Banca Sella");
            String status = StatusHelper.VOUCHER_STATUS_CAN_OK;
            
            gestPayData = gpService.deletePagam(voucherTransactionBean.getInitialAmount(), voucherTransactionBean.getTransactionID(), voucherTransactionBean.getShopLogin(), voucherTransactionBean.getCurrency(),
                    voucherTransactionBean.getBankTansactionID(), "RECONCILE", voucherTransactionBean.getAcquirerID(), voucherTransactionBean.getGroupAcquirer(), voucherTransactionBean.getEncodedSecretKey());

            if (gestPayData == null) {
                System.out.println("errore nella operazione di riconciliazione, chiamata deletePagam nulla verso Banca Sella (transazione: " + voucherTransactionBean.getTransactionID() + ")");
                // la cancellazione non � andata a buon fine
                reconciliationInfo.setFinalStatusType(voucherTransactionBean.getFinalStatusType());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                decreaseAttempts(voucherTransactionBean);
                status = StatusHelper.VOUCHER_STATUS_CAN_ERROR;
            }
            
            String errorCode = gestPayData.getErrorCode();
            String errorMessage = gestPayData.getErrorDescription();
            reconciliationInfo = new ReconciliationInfo();

            if (errorCode.equals("0")) {
                updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_CANCELED);
                reconciliationInfo.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_CANCELED);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                Email.sendVoucherSummary(emailSenderService, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
            }
            else if (errorCode.equals("9999")) {
                reconciliationInfo.setFinalStatusType(voucherTransactionBean.getFinalStatusType());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                decreaseAttempts(voucherTransactionBean);
            }
            else {
                updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_NOT_RECONCILIABLE);
                reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                status = StatusHelper.VOUCHER_STATUS_CAN_ERROR;
            }

            voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(status, null, null);
            entityManager.persist(voucherTransactionStatusBean);
            VoucherTransactionEventBean voucherTransactionEventBean = voucherTransactionBean.addVoucherTransactionEventBean(EventType.CAN, voucherTransactionBean.getAmount(), 
                    errorCode, errorMessage, gestPayData.getTransactionResult());
            entityManager.persist(voucherTransactionEventBean);
            entityManager.merge(voucherTransactionBean);
            
            return reconciliationInfo;
        }
        else {
            reconciliationInfo = new ReconciliationInfo();
            updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_CANCELED);
            reconciliationInfo.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_CANCELED);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
            Email.sendVoucherSummary(emailSenderService, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);

            return reconciliationInfo;
        }
    }
    
    public boolean checkAttemptsLeft(TransactionInterfaceBean transactionInterfaceBean) {
        boolean check = true;
        
        if (transactionInterfaceBean.getReconciliationAttemptsLeft() <= 0) {
            updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);

            check = false;
        }
        
        return check;
    }
    
    @Override
    public int decreaseAttempts(TransactionInterfaceBean transactionInterfaceBean) {
        return 0;
    }    
    
    @Override
    public String getStatus() {
        return StatusHelper.VOUCHER_FINAL_STATUS_ERROR_PAY_AUTH;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "ERROR PAYMENT AUTHORIZATION";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<VoucherTransactionBean> transactionBeanList = QueryRepository.findVoucherTransactionsByFinalStatusType(entityManager, getStatus());
        return transactionBeanList;
    }

}
