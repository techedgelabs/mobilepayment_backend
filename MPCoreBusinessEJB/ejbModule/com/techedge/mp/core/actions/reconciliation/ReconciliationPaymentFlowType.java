package com.techedge.mp.core.actions.reconciliation;

public enum ReconciliationPaymentFlowType {
    FLOW_VOUCHER,
    FLOW_CREDIT_CARD_CARTASI,
    FLOW_CREDIT_CARD_BANCASELLA
}
