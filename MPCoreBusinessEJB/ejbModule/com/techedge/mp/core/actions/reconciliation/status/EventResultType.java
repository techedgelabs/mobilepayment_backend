package com.techedge.mp.core.actions.reconciliation.status;

import com.techedge.mp.core.business.interfaces.ResponseHelper;

public enum EventResultType {
    SUCCESS(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS), 
    FAILURE(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE), 
    CANCELLED(ResponseHelper.RECONCILIATION_TRANSACTION_CANCELLED), 
    TO_RETRY(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

    private final String code;

    EventResultType(final String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

}