package com.techedge.mp.core.actions.reconciliation.status.user;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.model.dwh.DWHEventBean;
import com.techedge.mp.core.business.model.dwh.DWHEventOperationBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.interfaces.DWHAdapterResult;
import com.techedge.mp.dwh.adapter.utilities.StatusCode;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public class DWHEventErrorStatus extends ReconciliationUserStatus {
    
    private String[] messageErrorKeywords = new String[] {
            "Gateway Time-out",
            "Proxy Error",
            "Service Unavailable",
            "java.lang.IllegalStateException",
            "already exists",
            "Bad Gateway",
            "java.lang.NullPointerException",
            "java.io.IOException",
            "java.net.ConnectException",
            "java.sql.SQLException"
    };

    public DWHEventErrorStatus(EntityManager entityManager, DWHAdapterServiceRemote dwhAdapterService, CRMAdapterServiceRemote crmAdapterService, 
            PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, ParametersServiceRemote parametersService, String proxyHost, String proxyPort, String proxyNoHosts, 
            Integer pushNotificationExpiryTime, Integer reconciliationAttemptsLeft, Boolean refuelingOauth2Active) {

        super(entityManager, dwhAdapterService, crmAdapterService, pushNotificationService, fidelityService, emailSender, refuelingNotificationService, refuelingNotificationServiceOAuth2, parametersService, proxyHost, 
                proxyPort, proxyNoHosts, pushNotificationExpiryTime, reconciliationAttemptsLeft, refuelingOauth2Active);
    }

    @Override
    public ReconciliationInfo reconciliate(UserReconciliationInterfaceBean userReconciliationInterfaceBean) {
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();

        UserBean userBean = userReconciliationInterfaceBean.getUserBean();
        
        List<DWHEventBean> dwhEventBeanList = QueryRepository.findDWHEventByUser(entityManager, userBean);
        
        Boolean userBlock = false;
        String mobilePhone = null;
        Date now = new Date();
        HashMap<String, Boolean> flagPrivacy = new HashMap<>(0);
        String requestId = String.valueOf(new Date().getTime());
        Long requestTimestamp = now.getTime();
        String name = userBean.getPersonalDataBean().getFirstName();
        String email = userBean.getPersonalDataBean().getSecurityDataEmail();
        String password = userBean.getPersonalDataBean().getSecurityDataPassword();
        String surname = userBean.getPersonalDataBean().getLastName();
        String gender = userBean.getPersonalDataBean().getSex();
        String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
        Date dateOfBirth = userBean.getPersonalDataBean().getBirthDate();
        String cityOfBirth = userBean.getPersonalDataBean().getBirthMunicipality();
        String countryOfBirth = userBean.getPersonalDataBean().getBirthProvince();
        String codCarta = "";
        if (userBean.getVirtualLoyaltyCard() != null) {
            codCarta = userBean.getVirtualLoyaltyCard().getPanCode();
        }

        for (MobilePhoneBean item : userBean.getMobilePhoneList()) {
            if (item.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE)) {
                mobilePhone = item.getPrefix() + item.getNumber();
                break;
            }
        }

        if (userBean.getUserStatus().equals(User.USER_STATUS_BLOCKED)) {
            userBlock = true;
        }

        List<TermsOfServiceBean> listTermOfService = QueryRepository.findTermOfServiceByPersonalDataId(entityManager, userBean.getPersonalDataBean());
        if (listTermOfService != null) {
            for (TermsOfServiceBean item : listTermOfService) {
                if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                    flagPrivacy.put("privacy_eni", item.getAccepted());
                }
                if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                    flagPrivacy.put("flg_privacy_partner", item.getAccepted());
                    flagPrivacy.put("flg_privacy_analisi", item.getAccepted());
                }
                //if (item.getKeyval().equals("GEOLOCALIZZAZIONE_NEW_1")) {
                    flagPrivacy.put("flg_privacy_geo", Boolean.TRUE);
                //}
            }
        }

        System.out.println("chiamata a setUserDataPlus di Serijakala");
        
        String campaignType = "NOLOY";
        if(userBean.getVirtualizationCompleted() != null && userBean.getVirtualizationCompleted() == Boolean.TRUE) {    
            campaignType = "YOUCA";
        }
        
        Boolean isSocial  = Boolean.FALSE;
        String socialType = "";
        String idSocial   = null;
        
        String socialProvider = null;
        if (!userBean.getUserSocialData().isEmpty()) {
            for(UserSocialDataBean userSocialDataBean : userBean.getUserSocialData()) {
                socialProvider = userSocialDataBean.getProvider();
                idSocial       = userSocialDataBean.getUuid();
            }
        }
        
        if (socialProvider != null) {
            if (socialProvider.equalsIgnoreCase("GOOGLE")) {
                isSocial   = Boolean.TRUE;
                socialType = "google";
                password   = "";
            }
            if (socialProvider.equalsIgnoreCase("FACEBOOK")) {
                isSocial   = Boolean.TRUE;
                socialType = "facebook";
                password   = "";
            }
        }
        
        String source = userBean.getSource();
        if (source != null) {
            if (source.equalsIgnoreCase("ENJOY")) {
                isSocial             = Boolean.FALSE;
                socialType           = "enjoy";
            }
            if (source.equalsIgnoreCase("MYCICERO")) {
                isSocial             = Boolean.FALSE;
                socialType           = "mycicero";
            }
        }

        DWHAdapterResult result = dwhAdapterService.setUserDataPlus(email, password, name, surname, gender, fiscalCode, dateOfBirth, cityOfBirth, countryOfBirth, mobilePhone,
                codCarta, flagPrivacy, requestId, requestTimestamp, userBlock, campaignType, isSocial, socialType, idSocial);
        
        System.out.println("risposta del DWH: " + result.getMessageCode() + " (" + result.getStatusCode() + ")");

        DWHEventBean dwhEventBeanWork = (DWHEventBean) userReconciliationInterfaceBean;
        
        DWHEventOperationBean dWHEventOperationBean = new DWHEventOperationBean();
        dWHEventOperationBean.setRequestTimestamp(now);
        dWHEventOperationBean.setDWHEventBean(dwhEventBeanWork);
        dWHEventOperationBean.setRequestId(requestId);
        dWHEventOperationBean.setStatusCode(result.getStatusCode());
        
        int messageLength = result.getMessageCode().length();
        String messageCode = result.getMessageCode();
        if (messageLength > 250) {
            messageCode = result.getMessageCode().substring(0, 250);
        }
        dWHEventOperationBean.setMessageCode(messageCode);

        if (result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS)) {
            
            dwhEventBeanWork.setToReconcilie(false);
            dwhEventBeanWork.setFinalStatus(StatusHelper.DWH_EVENT_STATUS_SUCCESS);
            entityManager.merge(dwhEventBeanWork);
            
            for (DWHEventBean dwhEventBean : dwhEventBeanList) {
                dwhEventBean.setToReconcilie(false);
                dwhEventBean.setFinalStatus(StatusHelper.DWH_EVENT_STATUS_SUCCESS);
                entityManager.merge(dwhEventBean);
            }

            reconciliationInfo.setFinalStatusType(StatusHelper.DWH_EVENT_STATUS_SUCCESS);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
        }
        else {
            if (result.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR)) {
                boolean keywordFound = false;
                for (String keyword : messageErrorKeywords) {
                    if (result.getMessageCode().contains(keyword)) {
                        decreaseAttempts(dwhEventBeanWork);
                        keywordFound = true;
                        reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                        break;
                    }
                }
                
                if (!keywordFound) {
                    dwhEventBeanWork.setToReconcilie(false);
                    dwhEventBeanWork.setFinalStatus(StatusHelper.DWH_EVENT_STATUS_FAILED);
                    entityManager.merge(dwhEventBeanWork);
                    
                    for (DWHEventBean dwhEventBean : dwhEventBeanList) {
                        dwhEventBean.setToReconcilie(false);
                        dwhEventBean.setFinalStatus(StatusHelper.DWH_EVENT_STATUS_FAILED);
                        entityManager.merge(dwhEventBean);
                    }

                    reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                }
            }
            else {
                
                dwhEventBeanWork.setToReconcilie(false);
                dwhEventBeanWork.setFinalStatus(StatusHelper.DWH_EVENT_STATUS_FAILED);
                entityManager.merge(dwhEventBeanWork);
                
                for (DWHEventBean dwhEventBean : dwhEventBeanList) {
                    dwhEventBean.setToReconcilie(false);
                    dwhEventBean.setFinalStatus(StatusHelper.DWH_EVENT_STATUS_FAILED);
                    entityManager.merge(dwhEventBean);
                }

                reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
            }
        }
        
        entityManager.persist(dWHEventOperationBean);
        dwhEventBeanWork.getEventOperationListList().add(dWHEventOperationBean);
        entityManager.merge(dwhEventBeanWork);

        return reconciliationInfo;
    }

    @Override
    public String getStatus() {
        return StatusHelper.DWH_EVENT_STATUS_ERROR;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "DWH EVENT";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<DWHEventBean> dwhEventBeanList = QueryRepository.findDWHEventToReconcilie(entityManager);
        return dwhEventBeanList;
    }
}
