package com.techedge.mp.core.actions.reconciliation;

import java.awt.Color;
import java.lang.reflect.Constructor;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.actions.reconciliation.status.parking.ReconciliationParkingStatus;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationParkingData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationParkingSummary;
import com.techedge.mp.core.business.model.ParkingTransactionInterfaceBean;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelSheetData;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.utilities.TransactionFinalStatusConverter;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ReconciliationParkingAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    public ReconciliationParkingAction() {}

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public ReconciliationParkingSummary execute(EmailSenderRemote emailSender, GPServiceRemote gpService, ParametersServiceRemote parametersService,
            String proxyHost, String proxyPort, String proxyNoHosts, Integer reconciliationAttemptsLeft, String reconciliationRecipient,
            List<ReconciliationParkingData> parkingReconciliationInterfaceIDList, StringSubstitution stringSubstitution, String parkingDecodedSecretKey) {

        ReconciliationParkingSummary reconciliationSummary = new ReconciliationParkingSummary();

        UserTransaction userTransaction = context.getUserTransaction();

        String[][] reconciliationTypes = new String[][] {
                new String[] { "com.techedge.mp.core.actions.reconciliation.status.parking.PaymentErrorStatus", "PAYMENT_ERROR", ReconciliationParkingSummary.SUMMARY_STATUS_PAYMENT_ERROR },
                new String[] { "com.techedge.mp.core.actions.reconciliation.status.parking.PaymentDeleteErrorStatus", "PAYMENT_DELETE_ERROR", ReconciliationParkingSummary.SUMMARY_STATUS_PAYMENT_DELETE_ERROR } };

        HashMap<String, Integer> statusCounts = new HashMap<String, Integer>();
        
        int totalCounts     = 0;
        int failedCounts    = 0;
        int pendingCounts   = 0;
        int completedCounts = 0;
        
        //New Workbook
        ExcelWorkBook ewb = new ExcelWorkBook();
        ExcelSheetData excelData = ewb.createSheetData();
        SimpleDateFormat sdfTransaction = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat sdfEmail = new SimpleDateFormat("dd/MM/yyyy");

        try {

            userTransaction.begin();

            for (int i = 0; i < reconciliationTypes.length; i++) {
                String fileClass = reconciliationTypes[i][0];
                String errorType = reconciliationTypes[i][1];
                String summaryType = reconciliationTypes[i][2];
                Class obj = null;
                try {
                    obj = Class.forName(fileClass);
                    ReconciliationParkingStatus reconciliationParkingStatus;
                    Constructor objConstructor;
                    objConstructor = obj.getConstructor(EntityManager.class, GPServiceRemote.class, EmailSenderRemote.class, ParametersServiceRemote.class, String.class, String.class, String.class,
                            Integer.class, String.class);

                    reconciliationParkingStatus = (ReconciliationParkingStatus) objConstructor.newInstance(em, gpService, emailSender, parametersService, proxyHost, proxyPort, proxyNoHosts,
                            reconciliationAttemptsLeft, parkingDecodedSecretKey);

                    String label = reconciliationParkingStatus.getName();
                    //String status = reconciliationUserStatus.getStatus();
                    List<ParkingTransactionInterfaceBean> transactionList = null;
                    List<ParkingTransactionInterfaceBean> tmpTransactionList = reconciliationParkingStatus.getTransactionsList();

                    if (parkingReconciliationInterfaceIDList != null && !parkingReconciliationInterfaceIDList.isEmpty() && tmpTransactionList != null) {

                        transactionList = new ArrayList<ParkingTransactionInterfaceBean>(0);

                        for (ParkingTransactionInterfaceBean parkingTransactionInterfaceBean : tmpTransactionList) {

                            if (errorType != null && parkingTransactionInterfaceBean != null) {
                                List<String> result = null;

                                for (ReconciliationParkingData item : parkingReconciliationInterfaceIDList) {
                                    if (item.getReconciliationType().equals(errorType)) {
                                        result = item.getTransactionList();
                                    }
                                }
                                if (result != null) {
                                    for (String transactionID : result) {
                                        System.out.println("ID: " + parkingTransactionInterfaceBean.getId());
                                        if (transactionID.equals(Long.valueOf(parkingTransactionInterfaceBean.getId()).toString())) {
                                            System.out.println("TROVATA");
                                            transactionList.add(parkingTransactionInterfaceBean);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        transactionList = tmpTransactionList;
                    }

                    if (transactionList == null) {
                        System.out.println("Nessuna transazione di sosta trovata per lo stato " + label);
                        statusCounts.put(errorType, 0);
                        continue;
                    }

                    //userTransaction.begin();
                    System.out.println("Trovate " + transactionList.size() + " transazioni di sosta per lo stato " + label);
                    statusCounts.put(errorType, transactionList.size());
                    totalCounts += transactionList.size();

                    for (int index = 0; index < transactionList.size(); index++) {

                        ParkingTransactionInterfaceBean parkingReconciliationInterfaceBean = transactionList.get(index);

                        String fullName = parkingReconciliationInterfaceBean.getUserBean().getPersonalDataBean().getFirstName() + " "
                                + parkingReconciliationInterfaceBean.getUserBean().getPersonalDataBean().getLastName();

                        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();

                        String statusBefore = parkingReconciliationInterfaceBean.getFinalStatus();
                        String attemptsLeft = "";

                        System.out.println("Inizio riconciliazione per la transazione " + parkingReconciliationInterfaceBean.getId());
                        System.out.println("Stato iniziale transazione: " + parkingReconciliationInterfaceBean.getFinalStatus());

                        try {
                            if (reconciliationParkingStatus.checkAttemptsLeft(parkingReconciliationInterfaceBean)) {

                                reconciliationInfo = reconciliationParkingStatus.reconciliate(parkingReconciliationInterfaceBean);
                            }
                            else {
                                reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                            }
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                            System.err.println("Errore nella procedura di rincociliazione: " + ex.getMessage());
                            reconciliationInfo.setFinalStatusType(statusBefore);
                            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                            reconciliationParkingStatus.decreaseAttempts(parkingReconciliationInterfaceBean);

                            if (!reconciliationParkingStatus.checkAttemptsLeft(parkingReconciliationInterfaceBean)) {
                                reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                            }
                        }

                        System.out.println("ID Transazione: " + parkingReconciliationInterfaceBean.getId());
                        reconciliationInfo.setTransactionID(String.valueOf(parkingReconciliationInterfaceBean.getId()));
                        reconciliationSummary.addToSummary(summaryType, reconciliationInfo);
                        System.out.println("Stato finale riconciliazione: " + reconciliationInfo.getFinalStatusType());
                        System.out.println("Fine riconciliazione per la transazione " + parkingReconciliationInterfaceBean.getId());

                        String statusAfter = parkingReconciliationInterfaceBean.getFinalStatus();

                        if (parkingReconciliationInterfaceBean.getReconciliationAttemptsLeft() != null && attemptsLeft.equals("")) {
                            attemptsLeft = parkingReconciliationInterfaceBean.getReconciliationAttemptsLeft().toString();
                        }

                        int rowIndex = excelData.createRow();
                        
                        // Id
                        excelData.addRowData(String.valueOf(parkingReconciliationInterfaceBean.getId()), rowIndex);
                        String timestamp = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(parkingReconciliationInterfaceBean.getTimestamp());
                        String cityName = parkingReconciliationInterfaceBean.getCityName();
                        String parkingZoneName = parkingReconciliationInterfaceBean.getParkingZoneName();
                        String parkingId = parkingReconciliationInterfaceBean.getParkingId();
                        
                        String finalPrice = "";
                        if (parkingReconciliationInterfaceBean.getFinalPrice() != null) {
                            finalPrice = String.valueOf(parkingReconciliationInterfaceBean.getFinalPrice().setScale(2, BigDecimal.ROUND_HALF_EVEN));
                        }
                        
                        String statusAfterSuffix = "";
                        if (!parkingReconciliationInterfaceBean.getFinalStatus().equals(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE)) {
                            if (parkingReconciliationInterfaceBean.getToReconcilie()) {
                                statusAfterSuffix = "ERROR";
                            }
                            else {
                                statusAfterSuffix = "SUCCESS";
                            }
                        }

                        excelData.addRowData(timestamp, rowIndex);
                        excelData.addRowData(cityName, rowIndex);
                        excelData.addRowData(parkingZoneName, rowIndex);
                        excelData.addRowData(parkingId, rowIndex);
                        excelData.addRowData(finalPrice, rowIndex);
                        excelData.addRowData(fullName, rowIndex);
                        excelData.addRowData(parkingReconciliationInterfaceBean.getUserBean().getPersonalDataBean().getSecurityDataEmail(), rowIndex);
                        excelData.addRowData(TransactionFinalStatusConverter.getReconciliationParkingText(statusBefore, "ERROR"), rowIndex);
                        excelData.addRowData(TransactionFinalStatusConverter.getReconciliationParkingText(statusAfter, statusAfterSuffix), rowIndex);
                        excelData.addRowData(attemptsLeft, rowIndex);
                        excelData.addRowData(TransactionFinalStatusConverter.getReconciliationParkingText(reconciliationInfo.getStatusCode(), null), rowIndex);
                        
                        if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS)) {
                            completedCounts++;
                        }
                        
                        if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE)) {
                            failedCounts++;
                        }
                        
                        if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY)) {
                            pendingCounts++;
                        }

                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    System.err.println("ERRORE (" + ex.toString() + "): " + ex.getMessage());
                }
            }

            //totalCounts = 0;

            if (totalCounts > 0 && (reconciliationRecipient != null && !reconciliationRecipient.isEmpty())) {
                try {
                    //Cell style for header row
                    ExcelCellStyle csHeader = ewb.createCellStyle(true, 12, Color.WHITE, new Color(84, 130, 53), null);
                    //Cell style for table row
                    ExcelCellStyle csTable = ewb.createCellStyle(false, 12, Color.BLACK, new Color(230, 230, 230), null);

                    String[] columnsHeading = new String[] { "ID Operazione", "Timestamp Operazione", "Citt�", "Zona di sosta", "Id sosta", "Frezzo finale", "Utente", "Email", "Stato Operazione Iniziale",
                            "Stato Finale", "Tentativi Rimasti", "Stato Riconciliazione" };

                    ewb.addSheet("Transazioni Reconciliate", columnsHeading, excelData, csHeader, csTable);

                    EmailType emailType = EmailType.RECONCILIATION_PARKING_REPORT_V2;
                    List<Parameter> parameters = new ArrayList<Parameter>(0);

                    parameters.add(new Parameter("DATE_OPERATION", sdfEmail.format(new Date())));
                    parameters.add(new Parameter("TRANSACTION_PROCESSED", String.valueOf(totalCounts)));
                    parameters.add(new Parameter("TRANSACTION_FAILED", String.valueOf(failedCounts)));
                    parameters.add(new Parameter("TRANSACTION_PENDING", String.valueOf(pendingCounts)));
                    parameters.add(new Parameter("TRANSACTION_COMPLETED", String.valueOf(completedCounts)));

                    for (int i = 0; i < reconciliationTypes.length; i++) {
                        parameters.add(new Parameter(reconciliationTypes[i][1], statusCounts.get(reconciliationTypes[i][1]).toString()));
                    }

                    System.out.println("Invio email");

                    List<Attachment> attachments = new ArrayList<Attachment>(0);

                    String keyFrom = emailSender.getSender();
                    
                    if (stringSubstitution != null) {
                        keyFrom = stringSubstitution.getValue(keyFrom, 1);
                    }
                    
                    System.out.println("keyFrom: " + keyFrom);

                    // L'allegato deve essere inviato solo se � stato consumato almeno un voucher
                    Attachment attachment = new Attachment();
                    attachment.setFileName("report_transazioni_sosta_riconciliate.xlsx");
                    //attachment.setContent(attachmentContent);
                    byte[] bytes = ewb.getBytesToStream();
                    attachment.setBytes(bytes);
                    attachments.add(attachment);

                    System.out.println("Sending email to: " + reconciliationRecipient);
                    //String subject = "Eni Pay - report delle transazioni pre-paid riconciliate " + sdfEmail.format(new Date());
                    String subject = "Eni Station + Report delle transazioni di sosta riconciliate " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
                    String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, keyFrom, reconciliationRecipient, null, null, subject, parameters, attachments);
                    System.out.println("SendEmailResult: " + sendEmailResult);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    System.err.println("Errore nell'invio della email: " + ex.getMessage());
                }

            }

            System.out.println("Commit of transaction");
            userTransaction.commit();

        }
        catch (Exception ex) {
            reconciliationSummary.setStatusCode(ResponseHelper.SYSTEM_ERROR);

            try {
                userTransaction.commit();
            }
            catch (Exception ex2) {}
        }

        return reconciliationSummary;
    }

}
