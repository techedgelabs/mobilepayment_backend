package com.techedge.mp.core.actions.reconciliation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.TransactionService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationDetail;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.adapter.business.interfaces.RefuelDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendPaymentTransactionResultResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionReconciliationResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ReconciliationDetailAction {

	@Resource
	private EJBContext context;

	@PersistenceContext( unitName = "CrudPU" )
	private EntityManager em_crud;

	@PersistenceContext( unitName = "LogPU" )
	private EntityManager em_log;

	@EJB
	private LoggerService loggerService;

	final String transactionServiceJndi = "java:app/MPCoreBusinessEJB/TransactionService!com.techedge.mp.core.business.TransactionService";
	private TransactionService transactionService;
	private GPServiceRemote gpServiceRemote;
	private ForecourtInfoServiceRemote forecourtService;


	public ReconciliationDetailAction() {
	}


	public ReconciliationDetail execute(
			//    		String adminTicketId,
			String requestId,
			String transactionID,
			ForecourtInfoServiceRemote forecourtInfoService) throws EJBException {

		UserTransaction userTransaction = context.getUserTransaction();
		ReconciliationDetail reconciliationDetail = new ReconciliationDetail();

		this.transactionService = (TransactionService)context.lookup(transactionServiceJndi);
		//this.gpServiceRemote = gpServiceRemote;
		this.forecourtService = forecourtInfoService;

		try {
			System.out.println("Step pre B: "+ forecourtInfoService);
			
			List<String> transactionIDList = new ArrayList<String>(0);
			transactionIDList.add(transactionID);
			
			TransactionReconciliationResponse transactionReconciliationResponse = forecourtInfoService.transactionReconciliation(
					requestId,
					transactionIDList);

			//Per ogni transazione occorre effettuare il controllo con il backend;
			System.out.println("Step B: ");
			List<TransactionDetail> transactionDetails = transactionReconciliationResponse.getTransactionDetails();
			
			System.out.println("Step B:1 ");
			
			System.out.println("transactionDetails size:" + transactionDetails.size());

			RefuelDetail refuelDetail;
			for(TransactionDetail td: transactionDetails){
				
				System.out.println("Inizio loop transactionDetails");
				
				if ( td.getStatusCode().equals("TRANSACTION_NOT_RECOGNIZED_400")){
					
					System.out.println("TRANSACTION_NOT_RECOGNIZED_400");
					
					//reconciliationDetail.setFinalStatusType(transactionBean.getFinalStatusType());
					reconciliationDetail.setStatusCode(ResponseHelper.RECONCILIATION_DETAIL_FAILURE);

				}
				else{
					
					System.out.println("Transaction found");
					
					refuelDetail = td.getRefuelDetail();
					
					if (refuelDetail != null){
						reconciliationDetail.setAmount(refuelDetail.getAmount());
						reconciliationDetail.setFuelQuantity(refuelDetail.getFuelQuantity());
						reconciliationDetail.setProductDescription(refuelDetail.getProductDescription());
						reconciliationDetail.setProductID(refuelDetail.getProductID());
						reconciliationDetail.setTimestampEndRefuel(refuelDetail.getTimestampEndRefuel());
						reconciliationDetail.setTransactionID(td.getTransactionID());
						reconciliationDetail.setStatusCode(ResponseHelper.RECONCILIATION_DETAIL_SUCCESS);
					}
					else{
						reconciliationDetail.setStatusCode(ResponseHelper.RECONCILIATION_DETAIL_SUCCESS);
					}
				}
			}


			return reconciliationDetail;


		}
		catch (Exception ex2) {

			//			try {
			//				//non faccio modifiche dirette dunque non serve il rollback
			//			//	userTransaction.rollback();
			//			} catch (IllegalStateException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			} catch (SecurityException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			} catch (SystemException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			}

			String message = "FAILED reconciliation DETAIL  log with message (" + ex2.getMessage() + ")";
			this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );

			throw new EJBException(ex2);
		}
	}



	private void reconcileMissingNotifGFG(TransactionBean transactionBean,
			TransactionDetail td, ReconciliationInfo reconciliationInfo) {

		GestPayData gestPayData = new GestPayData();


		gestPayData.setAmount(String.valueOf(transactionBean.getFinalAmount()));
		//gestPayData.setTransactionID(transactionBean.getTransactionID());


		gestPayData.setAuthorizationCode(transactionBean.getAuthorizationCode());
		gestPayData.setBankTransactionID(transactionBean.getBankTansactionID());
		Set<TransactionEventBean> transactionEventsBeanData ;
		transactionEventsBeanData = transactionBean.getTransactionEventBeanData();
		TransactionEventBean transactionEventBean = new TransactionEventBean();

		for(TransactionEventBean teb: transactionEventsBeanData){

			if(transactionEventBean == null || transactionEventBean.getSequenceID()<teb.getSequenceID()){
				transactionEventBean = teb;
			}
		}

		gestPayData.setTransactionType(transactionEventBean.getEventType());
		gestPayData.setErrorDescription(transactionEventBean.getErrorDescription());
		gestPayData.setErrorCode(transactionEventBean.getErrorCode());
		gestPayData.setTransactionResult(transactionEventBean.getTransactionResult());


		this.notifyGFG(transactionBean, reconciliationInfo, gestPayData);


	}


	private void reconcilePaymentAuthDelete(TransactionBean transactionBean, TransactionStatusBean transactionStatusBean, TransactionDetail transactionDetail, ReconciliationInfo reconciliationInfo ) {
		GestPayData gestPayData = null;
		if ( transactionDetail.getStatusCode().equals("TRANSACTION_NOT_RECOGNIZED_400")){
			System.out.println("Step PaymentAuthDelete A1: "+transactionBean.getTransactionID());
			reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
		}

		if ( transactionDetail.getStatusCode().equals("MESSAGE_RECEIVED_200")){
			System.out.println("Step PaymentAuthDelete B1: "+transactionBean.getTransactionID());
			RefuelDetail refuelDetail = transactionDetail.getRefuelDetail();
			if(refuelDetail==null){ //esiste la transazione ma non c'� stato rifornimento;

				// la cancellazione non � andata a buon fine
				System.out.println("Step GenericFault B2: "+transactionBean.getTransactionID());
				reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
				reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
			}

			if(refuelDetail!=null){
				System.out.println("Step PaymentAuthDelete B3: "+transactionBean.getTransactionID());
				if(!(refuelDetail.getAmount()> 0.0)){
					//annulla il pagamento e notifica;
					System.out.println("Step PaymentAuthDelete B4a: "+transactionBean.getTransactionID());
					gestPayData = 	gpServiceRemote.deletePagam(transactionBean.getInitialAmount(), 
							transactionBean.getTransactionID(),
							transactionBean.getShopLogin(), 
							transactionBean.getCurrency(),
							transactionBean.getBankTansactionID(),
							"RECONCILE",
							transactionBean.getAcquirerID(),
							transactionBean.getGroupAcquirer(),
							transactionBean.getEncodedSecretKey());

					if (gestPayData== null){
						System.out.println("Step PaymentAuthDelete B4a null: "+transactionBean.getTransactionID());
						// la cancellazione non � andata a buon fine
						reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
						reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
					}
					else {
						System.out.println("Step PaymentAuthDelete B4a ok: "+transactionBean.getTransactionID());
						this.procedeWithDeletion(transactionBean, reconciliationInfo, true, gestPayData, refuelDetail);
					}
				}else{
					//completa il pagamento e notifica
					System.out.println("Step PaymentAuthDelete B4b: "+transactionBean.getTransactionID());
					gestPayData = 	gpServiceRemote.callSettle(refuelDetail.getAmount(),
							transactionBean.getTransactionID(), 
							transactionBean.getShopLogin(), 
							transactionBean.getCurrency(),
							transactionBean.getAcquirerID(),
                            transactionBean.getGroupAcquirer(),
                            transactionBean.getEncodedSecretKey(),
                            transactionBean.getToken(),
                            transactionBean.getAuthorizationCode(),
                            transactionBean.getBankTansactionID(),
                            transactionBean.getRefuelMode(),
                            transactionBean.getProductID(),
                            transactionBean.getFuelQuantity(),
                            transactionBean.getUnitPrice());
					if (gestPayData== null){
						System.out.println("Step PaymentAuthDelete B4b null: "+transactionBean.getTransactionID());
						// la cancellazione non � andata a buon fine
						reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
						reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
					}
					else {
						System.out.println("Step PaymentAuthDelete B4b ok: "+transactionBean.getTransactionID());
						this.procedeWithPayment(transactionBean, reconciliationInfo, true, gestPayData, refuelDetail);
					}
				}
			}
		}
	}


	private void reconcilePumpGenericFault(TransactionBean transactionBean, TransactionStatusBean transactionStatusBean, TransactionDetail transactionDetail, ReconciliationInfo reconciliationInfo ) {

		/* Se il dettaglio corrisponde a Notrecognized allora viene cancellata l'autorizzazione;
		 * 
		 */
		GestPayData gestPayData = null;
		if ( transactionDetail.getStatusCode().equals("TRANSACTION_NOT_RECOGNIZED_400")){
			//occorre eliminare l'autorizzazione e settare lo stato intermedio a reconcyle e quello principale a ...;
			System.out.println("Step GenericFault A1: "+transactionBean.getTransactionID());
			gestPayData = 	gpServiceRemote.deletePagam(transactionBean.getInitialAmount(), 
					transactionBean.getTransactionID(),
					transactionBean.getShopLogin(), 
					transactionBean.getCurrency(),
					transactionBean.getBankTansactionID(),
					"RECONCILE",
                    transactionBean.getAcquirerID(),
                    transactionBean.getGroupAcquirer(),
                    transactionBean.getEncodedSecretKey());

			if (gestPayData== null){
				// la cancellazione non � andata a buon fine
				System.out.println("Step GenericFault A2: "+transactionBean.getTransactionID());
				reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
				reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
			}
			else {
				System.out.println("Step GenericFault A3: "+transactionBean.getTransactionID());
				this.procedeWithDeletion(transactionBean, reconciliationInfo, false, gestPayData, null);
			}
		}

		/* Se il dettaglio � positivo allora viene cancellata/completata in base agli stati di forecourt;
		 * 
		 */

		if ( transactionDetail.getStatusCode().equals("MESSAGE_RECEIVED_200")){
			System.out.println("Step GenericFault B1: "+transactionBean.getTransactionID());
			RefuelDetail refuelDetail = transactionDetail.getRefuelDetail();
			if(refuelDetail==null){ //esiste la transazione ma non c'� stato rifornimento;
				gestPayData = 	gpServiceRemote.deletePagam(transactionBean.getInitialAmount(), 
						transactionBean.getTransactionID(),
						transactionBean.getShopLogin(), 
						transactionBean.getCurrency(),
						transactionBean.getBankTansactionID(),
						"RECONCILE",
                        transactionBean.getAcquirerID(),
                        transactionBean.getGroupAcquirer(),
                        transactionBean.getEncodedSecretKey());

				if (gestPayData== null){
					// la cancellazione non � andata a buon fine
					System.out.println("Step GenericFault B2: "+transactionBean.getTransactionID());
					reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
					reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
				}
				else {
					System.out.println("Step GenericFault B3: "+transactionBean.getTransactionID());
					this.procedeWithDeletion(transactionBean, reconciliationInfo, true, gestPayData, refuelDetail);

				}
			}

			if(refuelDetail!=null){
				System.out.println("Step GenericFault B4: "+transactionBean.getTransactionID());
				if(!(refuelDetail.getAmount()> 0.0)){
					//annulla il pagamento e notifica;
					System.out.println("Step GenericFault B4a: "+transactionBean.getTransactionID());
					gestPayData = 	gpServiceRemote.deletePagam(transactionBean.getInitialAmount(), 
							transactionBean.getTransactionID(),
							transactionBean.getShopLogin(), 
							transactionBean.getCurrency(),
							transactionBean.getBankTansactionID(),
							"RECONCILE",
                            transactionBean.getAcquirerID(),
                            transactionBean.getGroupAcquirer(),
                            transactionBean.getEncodedSecretKey());

					if (gestPayData== null){
						System.out.println("Step GenericFault B4a null: "+transactionBean.getTransactionID());
						// la cancellazione non � andata a buon fine
						reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
						reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
					}
					else {
						System.out.println("Step GenericFault B4a ok: "+transactionBean.getTransactionID());
						this.procedeWithDeletion(transactionBean, reconciliationInfo, true, gestPayData, refuelDetail);
					}
				}else{
					//completa il pagamento e notifica
					System.out.println("Step GenericFault B4b: "+transactionBean.getTransactionID());
					gestPayData = 	gpServiceRemote.callSettle(refuelDetail.getAmount(),
							transactionBean.getTransactionID(), 
							transactionBean.getShopLogin(), 
							transactionBean.getCurrency(),
							transactionBean.getAcquirerID(),
                            transactionBean.getGroupAcquirer(),
                            transactionBean.getEncodedSecretKey(),
                            transactionBean.getToken(),
                            transactionBean.getAuthorizationCode(),
                            transactionBean.getBankTansactionID(),
                            transactionBean.getRefuelMode(),
                            transactionBean.getProductID(),
                            transactionBean.getFuelQuantity(),
                            transactionBean.getUnitPrice());
					if (gestPayData== null){
						System.out.println("Step GenericFault B4b null: "+transactionBean.getTransactionID());
						// la cancellazione non � andata a buon fine
						reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
						reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
					}
					else {
						System.out.println("Step GenericFault B4b ok: "+transactionBean.getTransactionID());
						this.procedeWithPayment(transactionBean, reconciliationInfo, true, gestPayData, refuelDetail);
					}
				}


			}


		}


	}



	private void procedeWithDeletion(TransactionBean transactionBean, ReconciliationInfo reconciliationInfo, boolean notifyFlag , GestPayData gestPayData, RefuelDetail refuelDetail) {
		
		String errorCode    = gestPayData.getErrorCode();
		String errorMessage = gestPayData.getErrorDescription();
		
		String transactionResult = "";
		if ( errorCode.equals("0") ) {
			transactionResult = "ok";
		}
		else {
			transactionResult = "ko";
		}
		
		String response = transactionService.persistPaymentDeletionStatus(
				transactionResult, 
				StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_RECONCILE, 
				errorCode,
				errorMessage,
				transactionBean.getTransactionID(), 
				transactionBean.getBankTansactionID());


		if (response.equals(StatusHelper.PERSIST_PAYMENT_DELETION_STATUS_SUCCESS)){
			reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_FAILED);
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
			if (notifyFlag){
				this.notifyGFG(transactionBean, reconciliationInfo, gestPayData);
			}

		}else{

			reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
		}

	}

	private void notifyGFG(TransactionBean transactionBean, ReconciliationInfo reconciliationInfo, GestPayData gestPayData ) {
		
		SendPaymentTransactionResultResponse sendPaymentTransactionResult = new SendPaymentTransactionResultResponse();

		PaymentTransactionResult paymentTransactionResult = new PaymentTransactionResult();

		paymentTransactionResult.setAuthorizationCode(gestPayData.getAuthorizationCode());
		paymentTransactionResult.setBankTransactionID(gestPayData.getBankTransactionID());
		paymentTransactionResult.setEventType(gestPayData.getTransactionType());
		paymentTransactionResult.setErrorDescription(gestPayData.getErrorDescription());
		paymentTransactionResult.setErrorCode(gestPayData.getErrorCode());
		paymentTransactionResult.setTransactionResult(gestPayData.getTransactionResult());

		Date now = new Date();
		String requestID = String.valueOf(now.getTime());

		sendPaymentTransactionResult = forecourtService.sendPaymentTransactionResult(
				requestID,
				transactionBean.getTransactionID(),
				Double.valueOf(gestPayData.getAmount()),
				paymentTransactionResult);
	}

	private void procedeWithPayment(TransactionBean transactionBean, ReconciliationInfo reconciliationInfo, boolean notifyFlag , GestPayData gestPayData, RefuelDetail refuelDetail) {
		
		String errorCode    = gestPayData.getErrorCode();
		String errorMessage = gestPayData.getErrorDescription();
		
		String transactionResult = "";
		if ( errorCode.equals("0") ) {
			transactionResult = "ok";
		}
		else {
			transactionResult = "ko";
		}
		
		String response = transactionService.persistPaymentCompletionStatus(
				transactionResult, 
				StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_RECONCILE, 
				errorCode,
				errorMessage,
				transactionBean.getTransactionID(), 
				transactionBean.getBankTansactionID(),
				refuelDetail.getAmount());


		if (response.equals(StatusHelper.PERSIST_PAYMENT_COMPLETION_STATUS_SUCCESS)){
			reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_FAILED);
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
			if (notifyFlag){
				this.notifyGFG(transactionBean, reconciliationInfo, gestPayData);
			}

		}else{

			reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
		}
	}


	private void reconcilePaymentAuthDeleteRefuel(TransactionBean transactionBean, ReconciliationInfo reconciliationInfo, boolean notify_flag ) {
		GestPayData gestPayData = null;

		//annulla il pagamento e notifica;
		System.out.println("Step PaymentAuthDeleteRefuel D4a: "+transactionBean.getTransactionID());
		gestPayData = gpServiceRemote.deletePagam(transactionBean.getInitialAmount(), 
				transactionBean.getTransactionID(),
				transactionBean.getShopLogin(), 
				transactionBean.getCurrency(),
				transactionBean.getBankTansactionID(),
				"RECONCILE",
                transactionBean.getAcquirerID(),
                transactionBean.getGroupAcquirer(),
                transactionBean.getEncodedSecretKey());

		if (gestPayData== null){
			System.out.println("Step PaymentAuthDeleteRefuel D4a null: "+transactionBean.getTransactionID());
			// la cancellazione non � andata a buon fine
			reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
		}
		else {
			System.out.println("Step PaymentAuthDeleteRefuel D4a ok: "+transactionBean.getTransactionID());
			this.procedeWithDeletion(transactionBean, reconciliationInfo, notify_flag, gestPayData, null);
		}



	}


}
