package com.techedge.mp.core.actions.reconciliation.status.user;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.RefuelingSubscriptionsBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.NotifySubscriptionResult;

public class RefuelingSubscriptionError extends ReconciliationUserStatus {
    public RefuelingSubscriptionError(EntityManager entityManager, DWHAdapterServiceRemote dwhAdapterService, CRMAdapterServiceRemote crmAdapterService, 
            PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, ParametersServiceRemote parametersService, String proxyHost, String proxyPort, String proxyNoHosts, 
            Integer pushNotificationExpiryTime, Integer reconciliationAttemptsLeft, Boolean refuelingOauth2Active) {
        
        super(entityManager, dwhAdapterService, crmAdapterService, pushNotificationService, fidelityService, emailSender, refuelingNotificationService, refuelingNotificationServiceOAuth2, parametersService, proxyHost, 
                proxyPort, proxyNoHosts, pushNotificationExpiryTime, reconciliationAttemptsLeft, refuelingOauth2Active);
    }

    @Override
    public ReconciliationInfo reconciliate(UserReconciliationInterfaceBean userReconciliationInterfaceBean) {
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        RefuelingSubscriptionsBean refuelingSubscriptionsBean = (RefuelingSubscriptionsBean) userReconciliationInterfaceBean;
        UserBean userBean = userReconciliationInterfaceBean.getUserBean();
        NotifySubscriptionResult result = new NotifySubscriptionResult();

        Date now = new Date();
        Long requestTimestamp = now.getTime();
        String requestId = "ENS-".concat(requestTimestamp.toString());
        
        System.out.println("chiamata a notifySubscription di REFUELING");

        if (refuelingOauth2Active) {
            System.out.println("Chiamata Enjoy 2.0");
            result =  refuelingNotificationServiceOAuth2.notifySubscription(requestId, userBean.getPersonalDataBean().getFiscalCode());
        }
        else {
            System.out.println("Chiamata Enjoy 1.0");
            result =  refuelingNotificationService.notifySubscription(requestId, userBean.getPersonalDataBean().getFiscalCode());
        }
        
        System.out.println("risposta di REFUELING: " + result.getStatusMessage() + " (" + result.getStatusCode() + ")");
        
        refuelingSubscriptionsBean.setRequestId(requestId);
        refuelingSubscriptionsBean.setStatusCode(result.getStatusCode());
        refuelingSubscriptionsBean.setStatusMessage(result.getStatusMessage());
        
        if(result != null && result.getStatusCode().equals(StatusHelper.REFUELING_SUBSCRIPTION_ERROR)) {
            decreaseAttempts(refuelingSubscriptionsBean);
            
            reconciliationInfo.setFinalStatusType(refuelingSubscriptionsBean.getStatusCode());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
        }
        else{
            refuelingSubscriptionsBean.setToReconcile(false);
            
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
            reconciliationInfo.setFinalStatusType(refuelingSubscriptionsBean.getFinalStatus());
        }

        entityManager.merge(refuelingSubscriptionsBean);
        
        userBean.setSourceNotified(Boolean.TRUE);
        entityManager.merge(userBean);
        
        
        return reconciliationInfo;
    }
    
    @Override
    public String getStatus() {
        return StatusHelper.REFUELING_SUBSCRIPTION_ERROR;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "REFUELING SUBSCRIPTION ERROR";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<RefuelingSubscriptionsBean> redemptionTransactionBeanList = QueryRepository.findRefuelingSubscriptionToReconcilie(entityManager);
        return redemptionTransactionBeanList;
    }
    
}
