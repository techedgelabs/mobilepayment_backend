package com.techedge.mp.core.actions.reconciliation.status;

import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.actions.reconciliation.ReconciliationPaymentFlowType;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.email.sender.business.EmailSenderRemote;

public abstract class ReconciliationTransactionStatus {

    protected EntityManager                 entityManager      = null;
    protected String                        proxyHost          = null;
    protected String                        proxyPort          = null;
    protected String                        proxyNoHosts       = null;
    protected EmailSenderRemote             emailSenderService = null;
    protected ReconciliationPaymentFlowType paymentFlowType    = null;

    public ReconciliationTransactionStatus(EntityManager entityManager, EmailSenderRemote emailSender, String proxyHost, String proxyPort, String proxyNoHosts) {
        this.entityManager = entityManager;
        this.emailSenderService = emailSender;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.proxyNoHosts = proxyNoHosts;
    }

    public ReconciliationInfo reconciliate(TransactionInterfaceBean transactionInterfaceBean, ReconciliationPaymentFlowType paymentFlowType) {
        this.paymentFlowType = paymentFlowType;
        
        if (paymentFlowType.equals(ReconciliationPaymentFlowType.FLOW_VOUCHER)) {
            return reconciliateVoucherFlow(transactionInterfaceBean);
        }
        else {
            return reconciliateCreditCardFlow(transactionInterfaceBean);
        }
    }
    
    protected abstract ReconciliationInfo reconciliateCreditCardFlow(TransactionInterfaceBean transactionInterfaceBean);
    
    protected abstract ReconciliationInfo reconciliateVoucherFlow(TransactionInterfaceBean transactionInterfaceBean);

    public abstract boolean checkAttemptsLeft(TransactionInterfaceBean transactionInterfaceBean);

    public abstract int decreaseAttempts(TransactionInterfaceBean transactionInterfaceBean);

    public abstract String getStatus();

    public abstract String getSubStatus();

    public abstract String getName();

    @SuppressWarnings("rawtypes")
    public abstract List getTransactionsList();

    
}
