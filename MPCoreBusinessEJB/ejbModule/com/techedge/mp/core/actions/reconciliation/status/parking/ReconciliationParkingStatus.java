package com.techedge.mp.core.actions.reconciliation.status.parking;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.actions.parking.v2.PARKING_TRANSACTION_ITEM_EVENT_STATUS;
import com.techedge.mp.core.actions.parking.v2.PARKING_TRANSACTION_ITEM_STATUS_STATUS;
import com.techedge.mp.core.actions.parking.v2.PARKING_TRANSACTION_STATUS_CONST;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.ParkingTransactionInterfaceBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemEventBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionStatusBean;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

public abstract class ReconciliationParkingStatus {

    protected GPServiceRemote               gpService                     = null;
    protected EntityManager                 entityManager                 = null;
    protected EmailSenderRemote             emailSender                   = null;
    protected ParametersServiceRemote       parametersService             = null;
    protected String                        proxyHost                     = null;
    protected String                        proxyPort                     = null;
    protected String                        proxyNoHosts                  = null;
    protected Integer                       reconciliationAttemptsLeft    = null;
    protected String                        parkingDecodedSecretKey       = null;
    
    protected PARKING_TRANSACTION_ITEM_STATUS_STATUS newStatus       = null;
    protected PARKING_TRANSACTION_ITEM_STATUS_STATUS oldStatus       = null;
    
    protected static final String PAYMENT_REJECTED_ERROR_CODE    = "19";
    protected static final String PAYMENT_NOT_SUFFICIENT_FUNDS_1 = "not sufficient funds";
    protected static final String PAYMENT_NOT_SUFFICIENT_FUNDS_2 = "not+sufficient+funds";
    protected static final String PARKING_PARAM_ATTEMPTS         = "PARKING_ATTEMPTS";     
    protected static final String PARKING_PARAM_INTERVAL_RETRY   = "PARKING_INTERVAL_RETRY"; 

    public ReconciliationParkingStatus(EntityManager entityManager, GPServiceRemote gpService, EmailSenderRemote emailSender, ParametersServiceRemote parametersService, 
            String proxyHost, String proxyPort, String proxyNoHosts, Integer reconciliationAttemptsLeft, String parkingDecodedSecretKey) {

        this.gpService = gpService;
        this.entityManager = entityManager;
        this.emailSender = emailSender;
        this.parametersService = parametersService;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.proxyNoHosts = proxyNoHosts;
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
        this.parkingDecodedSecretKey = parkingDecodedSecretKey;
    }

    public abstract ReconciliationInfo reconciliate(ParkingTransactionInterfaceBean parkingTransactionInterfaceBean) throws Exception;

    public boolean checkAttemptsLeft(ParkingTransactionInterfaceBean parkingTransactionInterfaceBean) {
        boolean check = true;

        if (parkingTransactionInterfaceBean.getReconciliationAttemptsLeft() <= 0) {
            parkingTransactionInterfaceBean.setToReconcilie(false);
            parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
            entityManager.merge(parkingTransactionInterfaceBean);

            check = false;
        }

        return check;
    }

    public int decreaseAttempts(ParkingTransactionInterfaceBean parkingTransactionInterfaceBean) {
        System.out.println("decreaseAttempts");

        int attempts = parkingTransactionInterfaceBean.getReconciliationAttemptsLeft().intValue();
        int returnAttempts;

        if (attempts < 1)
            return -1;

        parkingTransactionInterfaceBean.setReconciliationAttemptsLeft((attempts - 1));
        entityManager.merge(parkingTransactionInterfaceBean);
        returnAttempts = (attempts - 1);

        System.out.println("Decremento dei tentativi di riconciliazione: " + attempts + " -> " + returnAttempts);

        return returnAttempts;
    }

    public abstract String getStatus();

    public abstract String getSubStatus();

    public abstract String getName();

    @SuppressWarnings("rawtypes")
    public abstract List getTransactionsList();
    
    
    protected GestPayData doPaymentAuthDelete(ParkingTransactionItemBean parkingTransactionItemBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double amount, Integer statusSequenceID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        ParkingTransactionItemEventBean parkingTransactionItemEventBean = null;
        ParkingTransactionStatusBean parkingTransactionItemStatusBean = null;

        newStatus = PARKING_TRANSACTION_ITEM_STATUS_STATUS.PAYMENT_AUTHORIZATION_DELETED;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        PARKING_TRANSACTION_ITEM_EVENT_STATUS transactionEvent = PARKING_TRANSACTION_ITEM_EVENT_STATUS.CANCELLED;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataDELETEResponse = gpService.deletePagam(amount, parkingTransactionItemBean.getParkingTransactionItemId(), parkingTransactionItemBean.getShopLogin(),
                parkingTransactionItemBean.getCurrency(), parkingTransactionItemBean.getBankTansactionID(), null, parkingTransactionItemBean.getAcquirerID(),
                parkingTransactionItemBean.getGroupAcquirer(), encodedSecretKey);

        if (gestPayDataDELETEResponse == null) {
            gestPayDataDELETEResponse = new GestPayData();
            gestPayDataDELETEResponse.setTransactionResult("ERROR");
            gestPayDataDELETEResponse.setErrorCode("9999");
            gestPayDataDELETEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            // TODO Definire il flusso della riconciliazione
            //poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH DEL: " + gestPayDataDELETEResponse.getTransactionResult());

        errorCode = gestPayDataDELETEResponse.getErrorCode();
        errorDescription = gestPayDataDELETEResponse.getErrorDescription();
        eventResult = gestPayDataDELETEResponse.getTransactionResult();

        parkingTransactionItemStatusBean = this.generateParkingTransactionStatusEvent(parkingTransactionItemBean.getParkingTransactionBean(), requestID,
                PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.RECONCILIATION, newStatus.getValue(), errorDescription);

        entityManager.persist(parkingTransactionItemStatusBean);

        parkingTransactionItemEventBean = this.generateParkingTransactionItemEvent(amount, eventSequenceID, parkingTransactionItemBean, gestPayDataDELETEResponse,
                transactionEvent.getValue());

        entityManager.persist(parkingTransactionItemEventBean);

        oldStatus = newStatus;

        return gestPayDataDELETEResponse;
    }
    
    
    protected ParkingTransactionStatusBean generateParkingTransactionStatusEvent(ParkingTransactionBean parkingTransactionBean,
            String requestID,String level, String status, String subStatus, String subStatusDescription) {

        ParkingTransactionStatusBean parkingTransactionItemStatusBean = new ParkingTransactionStatusBean();
        Integer statusSequenceID = parkingTransactionBean.getLastStatusSequenceID() + 1;

        parkingTransactionItemStatusBean.setSequenceID(statusSequenceID);
        parkingTransactionItemStatusBean.setRequestID(requestID);
        parkingTransactionItemStatusBean.setTimestamp(new Timestamp(new Date().getTime()));
        parkingTransactionItemStatusBean.setStatus(status);
        parkingTransactionItemStatusBean.setSubStatus(subStatus);
        parkingTransactionItemStatusBean.setSubStatusDescription(subStatusDescription);
        parkingTransactionItemStatusBean.setParkingTransactionBean(parkingTransactionBean);
        parkingTransactionItemStatusBean.setLevel(level);

        return parkingTransactionItemStatusBean;
    }

    protected ParkingTransactionItemEventBean generateParkingTransactionItemEvent(Double amount, Integer eventSequenceID, ParkingTransactionItemBean parkingTransactionItemBean,
            GestPayData gestPayData, String event) {

        ParkingTransactionItemEventBean parkingTransactionItemEventBean = new ParkingTransactionItemEventBean();

        parkingTransactionItemEventBean.setSequenceID(eventSequenceID);
        parkingTransactionItemEventBean.setErrorCode(gestPayData.getErrorCode());
        parkingTransactionItemEventBean.setErrorDescription(gestPayData.getErrorDescription());
        parkingTransactionItemEventBean.setParkingTransactionItemBean(parkingTransactionItemBean);
        parkingTransactionItemEventBean.setTransactionResult(gestPayData.getTransactionResult());
        parkingTransactionItemEventBean.setEventType(event);
        parkingTransactionItemEventBean.setEventAmount(amount);

        return parkingTransactionItemEventBean;
    }
    
    protected boolean insufficientFunds(GestPayData gestPayDataAUTHResponse) {
        return gestPayDataAUTHResponse.getErrorCode().equals(PAYMENT_REJECTED_ERROR_CODE)
                && (gestPayDataAUTHResponse.getErrorDescription().equals(PAYMENT_NOT_SUFFICIENT_FUNDS_1) || gestPayDataAUTHResponse.getErrorDescription().equals(
                        PAYMENT_NOT_SUFFICIENT_FUNDS_2));
    }
}
