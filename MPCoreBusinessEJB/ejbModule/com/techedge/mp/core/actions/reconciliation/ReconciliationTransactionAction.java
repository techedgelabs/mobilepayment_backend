package com.techedge.mp.core.actions.reconciliation;

import java.awt.Color;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.actions.reconciliation.status.ReconciliationTransactionStatus;
import com.techedge.mp.core.actions.reconciliation.status.postpaid.ReconciliationPostPaidTransactionStatus;
import com.techedge.mp.core.actions.reconciliation.status.voucher.ReconciliationVoucherTransactionStatus;
import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationTransactionSummary;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.EsPromotionBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelSheetData;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.utilities.TransactionFinalStatusConverter;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ReconciliationTransactionAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    public ReconciliationTransactionAction() {}

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public ReconciliationTransactionSummary execute(GPServiceRemote gpService, TransactionServiceRemote transactionService, ForecourtInfoServiceRemote forecourtService,
            ForecourtPostPaidServiceRemote forecourtPPService, FidelityServiceRemote fidelityService, CRMServiceRemote crmService, EmailSenderRemote emailSender, UserCategoryService userCategoryService, 
            String proxyHost, String proxyPort, String proxyNoHosts, String reconciliationRecipient, List<String> transactionsIDList, String secretKey, StringSubstitution stringSubstitution, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2) {

        ReconciliationTransactionSummary reconciliationTransactionSummary = new ReconciliationTransactionSummary();

        UserTransaction userTransaction = context.getUserTransaction();

        String[][] reconciliationTypes = new String[][] { 
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.prepaid.AbendStatus", "PREPAID_ABEND", ReconciliationTransactionSummary.SUMMARY_PREPAID_ABEND},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.prepaid.GenericFaultStatus", "PREPAID_ERROR", ReconciliationTransactionSummary.SUMMARY_PREPAID_ERROR},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.prepaid.MissingNotificationStatus", "PREPAID_MISSING_NOTIFICATION", ReconciliationTransactionSummary.SUMMARY_PREPAID_MISSING_NOTIFICATION},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.prepaid.MissingPayAuthDeleteAfterRefuelStatus", "PREPAID_MISSING_PAYAUTH_DELETE_AFTER_REFUEL", ReconciliationTransactionSummary.SUMMARY_PREPAID_MISSING_PAYAUTH_DELETE_AFTER_REFUEL},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.prepaid.MissingPayAuthDeleteBeforeRefuelStatus", "PREPAID_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL",ReconciliationTransactionSummary.SUMMARY_PREPAID_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.prepaid.MissingPaymentStatus", "PREPAID_MISSING_PAYMENT", ReconciliationTransactionSummary.SUMMARY_PREPAID_MISSING_PAYMENT},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.prepaid.VoucherReconciliationStatus", "PREPAID_VOUCHER_ERROR", ReconciliationTransactionSummary.SUMMARY_PREPAID_VOUCHER_ERROR},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.prepaid.LoyaltyReconciliationStatus", "PREPAID_LOYALTY_ERROR", ReconciliationTransactionSummary.SUMMARY_PREPAID_LOYALTY_ERROR},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.postpaid.PaymentErrorStatus", "POSTPAID_PAYMENT_ERROR", ReconciliationTransactionSummary.SUMMARY_POSTPAID_PAYMENT_ERROR},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.postpaid.VoucherErrorStatus", "POSTPAID_VOUCHER_ERROR", ReconciliationTransactionSummary.SUMMARY_POSTPAID_VOUCHER_ERROR},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.postpaid.LoyaltyErrorStatus", "POSTPAID_LOYALTY_ERROR", ReconciliationTransactionSummary.SUMMARY_POSTPAID_LOYALTY_ERROR},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.voucher.PaymentAuthErrorStatus", "VOUCHER_PAYMENT_AUTH_ERROR", ReconciliationTransactionSummary.SUMMARY_VOUCHER_PAYMENT_AUTH_ERROR},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.voucher.VoucherCreateErrorStatus", "VOUCHER_CREATE_ERROR", ReconciliationTransactionSummary.SUMMARY_VOUCHER_VOUCHER_CREATE_ERROR},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.voucher.PaymentSettleErrorStatus", "VOUCHER_PAYMENT_SETTLE_ERROR", ReconciliationTransactionSummary.SUMMARY_VOUCHER_PAYMENT_SETTLE_ERROR},
                new String [] {"com.techedge.mp.core.actions.reconciliation.status.voucher.VoucherDeleteErrorStatus", "VOUCHER_DELETE_ERROR", ReconciliationTransactionSummary.SUMMARY_VOUCHER_VOUCHER_DELETE_ERROR}
        };
        
        HashMap<String, Integer> statusCounts = new HashMap<String, Integer>();
        int totalCounts = 0;
        //New Workbook
        ExcelWorkBook ewb = new ExcelWorkBook();
        ExcelSheetData excelData = ewb.createSheetData();
        SimpleDateFormat sdfTransaction = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat sdfEmail = new SimpleDateFormat("dd/MM/yyyy");

        
        try {
            
            userTransaction.begin();

            for (int i = 0; i < reconciliationTypes.length; i++) {
                String fileClass = reconciliationTypes[i][0];
                String errorType = reconciliationTypes[i][1];
                String summaryType = reconciliationTypes[i][2];
                Class obj = null;
                try {
                    obj = Class.forName(fileClass);
                    ReconciliationTransactionStatus reconciliationTransactionStatus;
                    Constructor objConstructor;

                    if (fileClass.contains("postpaid")) {
                        objConstructor = obj.getConstructor(EntityManager.class, GPServiceRemote.class, ForecourtPostPaidServiceRemote.class, FidelityServiceRemote.class,
                                EmailSenderRemote.class, String.class, String.class, String.class, String.class, UserCategoryService.class, StringSubstitution.class);

                        reconciliationTransactionStatus = (ReconciliationPostPaidTransactionStatus) objConstructor.newInstance(em, gpService, forecourtPPService, fidelityService,
                                emailSender, proxyHost, proxyPort, proxyNoHosts, secretKey, userCategoryService, stringSubstitution);
                    }
                    else if (fileClass.contains("voucher")) {
                        objConstructor = obj.getConstructor(EntityManager.class, GPServiceRemote.class, FidelityServiceRemote.class, EmailSenderRemote.class, String.class,
                                String.class, String.class);

                        reconciliationTransactionStatus = (ReconciliationVoucherTransactionStatus) objConstructor.newInstance(em, gpService, fidelityService, emailSender,
                                proxyHost, proxyPort, proxyNoHosts);
                    }
                    else {
                        objConstructor = obj.getConstructor(EntityManager.class, GPServiceRemote.class, TransactionServiceRemote.class, ForecourtInfoServiceRemote.class,
                                FidelityServiceRemote.class, CRMServiceRemote.class, EmailSenderRemote.class, String.class, String.class, String.class, String.class, 
                                UserCategoryService.class, StringSubstitution.class, RefuelingNotificationServiceRemote.class, RefuelingNotificationServiceOAuth2Remote.class);

                        reconciliationTransactionStatus = (ReconciliationTransactionStatus) objConstructor.newInstance(em, gpService, transactionService, forecourtService,
                                fidelityService, crmService, emailSender, proxyHost, proxyPort, proxyNoHosts, secretKey, userCategoryService, stringSubstitution, 
                                refuelingNotificationService, refuelingNotificationServiceOAuth2);
                    }

                    String label = reconciliationTransactionStatus.getName();
                    String status = reconciliationTransactionStatus.getStatus();
                    List<TransactionInterfaceBean> transactionList = null;
                    List<TransactionInterfaceBean> tmpTransactionList = reconciliationTransactionStatus.getTransactionsList();

                    if (transactionsIDList != null && !transactionsIDList.isEmpty() && tmpTransactionList != null) {

                        transactionList = new ArrayList<TransactionInterfaceBean>(0);

                        for (TransactionInterfaceBean transactionInterfaceBean : tmpTransactionList) {
                            for (String transactionID : transactionsIDList) {
                                if (transactionID.equals(transactionInterfaceBean.getTransactionID())) {
                                    transactionList.add(transactionInterfaceBean);
                                }
                            }
                        }
                    }
                    else {
                        transactionList = tmpTransactionList;
                    }

                    if (transactionList == null) {
                        System.out.println("Nessuna transazione trovata per lo stato " + label);
                        continue;
                    }
                    
                    //userTransaction.begin();
                    System.out.println("Trovate " + transactionList.size() + " transazioni per lo stato " + label);
                    statusCounts.put(errorType, transactionList.size());
                    totalCounts += transactionList.size();
                    
                    for (int index = 0; index < transactionList.size(); index++) {
                        
                        TransactionInterfaceBean transactionInterfaceBean = (TransactionInterfaceBean) transactionList.get(index);

                        String fullName = transactionInterfaceBean.getUserBean().getPersonalDataBean().getFirstName() + " "
                                + transactionInterfaceBean.getUserBean().getPersonalDataBean().getLastName();
                        String initialAmount = "0.0";
                        String amount = "0.0";
                        String attemptsLeft = "";
                        //ArrayList<String> rowData = new ArrayList<String>(0);
                        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
                        Integer userType = transactionInterfaceBean.getUserBean().getUserType();
                        Boolean userNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());
                        Boolean userNewAcquirer = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
                        Boolean userGuest = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.GUEST_FLOW.getCode());
                        //String paymentMethodType = transactionInterfaceBean.getPaymentMethodType();
                        String acquirer = transactionInterfaceBean.getAcquirerID();

                        if (!fileClass.contains("postpaid") && !transactionInterfaceBean.getTransactionStatus().equals(status)) {
                            System.out.println("Transazione " + transactionInterfaceBean.getTransactionID() + " non del tipo \"" + label + "\"");

                            reconciliationInfo.setTransactionID(transactionInterfaceBean.getTransactionID());
                            reconciliationInfo.setFinalStatusType(transactionInterfaceBean.getTransactionStatus());
                            reconciliationTransactionSummary.addToSummary(ReconciliationTransactionSummary.SUMMARY_STATUS_NOT_IDENTIFIED, reconciliationInfo);
                            continue;
                        }
                        
                        String statusBefore = transactionInterfaceBean.getTransactionStatus();
                        /*String statusEventBefore = transactionInterfaceBean.getLastStatusEvent();
                        String statusEventCodeBefore = transactionInterfaceBean.getLastStatusEventCode();
                        String statusEventResultBefore = transactionInterfaceBean.getLastStatusEventResult();*/
                        /*boolean gfgNotificationBefore = false;
                        
                        if (transactionInterfaceBean.getGFGNotification() != null) {
                            gfgNotificationBefore = transactionInterfaceBean.getGFGNotification().booleanValue();
                        }*/

                        System.out.println("Inizio riconciliazione per la transazione " + transactionInterfaceBean.getTransactionID());
                        System.out.println("Stato iniziale riconciliazione: " + transactionInterfaceBean.getTransactionStatus());

                        try {
                            if (reconciliationTransactionStatus.checkAttemptsLeft(transactionInterfaceBean)) {

                                System.out.println("Riconciliazione per utenti del " + (userNewFlow ? "nuovo" : "vecchio") + " flusso");
                                System.out.println("Riconciliazione per utenti del " + (userNewAcquirer ? "cartas�" : "voucher") + " flusso");
                                /*
                                if (acquirer != null && acquirer.equals("BANCASELLA") && !fileClass.contains("voucher")) {
                                    userNewFlow = false;
                                }
                                
                                if (acquirer != null && acquirer.equals("CARTASI")) {
                                    userNewFlow = false;
                                }
                                
                                System.out.println("Transazione del " + (userNewFlow ? "nuovo" : "vecchio") + " flusso");
                                */
                                ReconciliationPaymentFlowType paymentFlowType;
                                
                                if (userNewFlow || userGuest) {
                                    paymentFlowType = ReconciliationPaymentFlowType.FLOW_VOUCHER;
                                }
                                else {
                                    paymentFlowType = ReconciliationPaymentFlowType.FLOW_CREDIT_CARD_BANCASELLA;
                                    
                                    if (userNewAcquirer) {
                                        if ((acquirer != null && (acquirer.equals("CARTASI") || acquirer.equals("MULTICARD")))
                                                || (transactionInterfaceBean.getPaymentMethodType() != null
                                                    && transactionInterfaceBean.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD))) {
                                            paymentFlowType = ReconciliationPaymentFlowType.FLOW_CREDIT_CARD_CARTASI;
                                        }
                                        else {
                                            paymentFlowType = ReconciliationPaymentFlowType.FLOW_VOUCHER;
                                        }
                                    }
                                }
                                
                                reconciliationInfo = reconciliationTransactionStatus.reconciliate(transactionInterfaceBean, paymentFlowType);
                                
                                if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS) &&
                                    !transactionInterfaceBean.getAcquirerID().equals("MASTERCARD") &&
                                    !transactionInterfaceBean.getTransactionCategory().equals(TransactionCategoryType.TRANSACTION_BUSINESS) &&
                                    transactionInterfaceBean.getAmount() != null &&
                                    transactionInterfaceBean.getAmount() > 0) {
                                    
                                    /************************************************************/
                                    /* Impostazione flag toBeProcessed per promo Vodafone Black */
                                    
                                    // Se per l'utente � presente una riga nella tabella ES_PROMOTION
                                    // con intervallo di valifit� che comprende la data di creazione
                                    // della transazione bisogna impostare il campo toBeProcessed
                                    // a true
                                    
                                    EsPromotionBean esPromotionBean = QueryRepository.findEsPromotionToBeProcessedWithNullVoucherCodeByDate(em, transactionInterfaceBean.getUserBean(), transactionInterfaceBean.getCreationTimestamp());
                                    if (esPromotionBean != null) {
                                        System.out.println("Trovata riga EsPromotionBean con flag da impostare");
                                        esPromotionBean.setToBeProcessed(Boolean.TRUE);
                                        em.merge(esPromotionBean);
                                    }
                                    
                                    /************************************************************/
                                }
                            }
                            else {
                                reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                            }
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                            System.err.println("Errore nella procedura di riconciliazione: " + ex.getMessage());
                            reconciliationInfo.setFinalStatusType(statusBefore);
                            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                            reconciliationTransactionStatus.decreaseAttempts(transactionInterfaceBean);

                            if (!reconciliationTransactionStatus.checkAttemptsLeft(transactionInterfaceBean)) {
                                reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                            }
                        }
                        
                        System.out.println("TransactionID: " + transactionInterfaceBean.getTransactionID());
                        reconciliationInfo.setTransactionID(transactionInterfaceBean.getTransactionID());
                        reconciliationTransactionSummary.addToSummary(summaryType, reconciliationInfo);
                        System.out.println("Stato finale riconciliazione: " + reconciliationInfo.getFinalStatusType());
                        System.out.println("Fine riconciliazione per la transazione " + transactionInterfaceBean.getTransactionID());

                        String statusAfter = transactionInterfaceBean.getTransactionStatus();
                        String statusEventAfter = transactionInterfaceBean.getLastStatusEvent();
                        String statusEventCodeAfter = transactionInterfaceBean.getLastStatusEventCode();
                        String statusEventResultAfter = transactionInterfaceBean.getLastStatusEventResult();
                        boolean gfgNotificationAfter = false;
                        
                        if (transactionInterfaceBean.getGFGNotification() != null) {
                            gfgNotificationAfter = transactionInterfaceBean.getGFGNotification().booleanValue();
                        }
                                                    
                        String timeEndRefuel = "";

                        if (transactionInterfaceBean.getEndRefuelTimestamp() != null) {
                            timeEndRefuel = sdfTransaction.format(transactionInterfaceBean.getEndRefuelTimestamp());
                        }

                        if (transactionInterfaceBean.getInitialAmount() != null) {
                            initialAmount = transactionInterfaceBean.getInitialAmount().toString();
                        }

                        if (transactionInterfaceBean.getAmount() != null) {
                            amount = transactionInterfaceBean.getAmount().toString();
                        }

                        if (transactionInterfaceBean.getReconciliationAttemptsLeft() != null && attemptsLeft.equals("")) {
                            attemptsLeft = transactionInterfaceBean.getReconciliationAttemptsLeft().toString();
                        }

                        int rowIndex = excelData.createRow();
                        excelData.addRowData(transactionInterfaceBean.getTransactionID(), rowIndex);
                        
                        TransactionCategoryType transactionCategoryType = transactionInterfaceBean.getTransactionCategory();
                        String transactionCategory = "Eni Station +";
                        if (transactionCategoryType != null && transactionCategoryType.equals(TransactionCategoryType.TRANSACTION_BUSINESS)) {
                            transactionCategory = "Eni Station p.iva";
                        }
                        excelData.addRowData(transactionCategory, rowIndex);
                        
                        excelData.addRowData(transactionInterfaceBean.getServerName(), rowIndex);
                        
                        excelData.addRowData(initialAmount, rowIndex);
                        excelData.addRowData(amount, rowIndex);
                        
                        /*if (statusBefore.equals(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL) && !gfgNotificationBefore)  {
                            excelData.addRowData("Notifica GFG non inviata", rowIndex);
                        }
                        else if (errorType.equals("POSTPAID_VOUCHER_ERROR")) {
                            excelData.addRowData("Elaborazione del consumo voucher", rowIndex);
                        }
                        else if (errorType.equals("POSTPAID_LOYALTY_ERROR")) {
                                excelData.addRowData("Elaborazione del caricamento punti", rowIndex);
                        }
                        else {
                            excelData.addRowData(TransactionFinalStatusConverter.getReportText(statusBefore, statusEventBefore, statusEventCodeBefore, statusEventResultBefore), rowIndex);    
                        }*/
                        
                        excelData.addRowData(TransactionFinalStatusConverter.getReconciliationTransactionText(errorType, null), rowIndex);

                        if (errorType.equals("PREPAID_MISSING_NOTIFICATION") && statusAfter.equals(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL) && !gfgNotificationAfter)  {
                            excelData.addRowData("Notifica GFG non inviata", rowIndex);
                        }
                        /*else if (errorType.equals("POSTPAID_VOUCHER_ERROR")) {
                            PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
                            
                            if (postPaidTransactionBean.getVoucherReconcile()) {
                                excelData.addRowData("Elaborazione del consumo voucher", rowIndex);
                            }
                            else {
                                excelData.addRowData(TransactionFinalStatusConverter.getReportText(statusAfter, statusEventAfter, statusEventCodeAfter, statusEventResultAfter), rowIndex);
                            }
                        }*/
                        else if (errorType.equals("PREPAID_LOYALTY_ERROR")) {
                            TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
                            
                            if (transactionBean.getLoyaltyReconcile()) {
                                excelData.addRowData(TransactionFinalStatusConverter.getReconciliationTransactionText(errorType, null), rowIndex);
                            }
                            else {
                                excelData.addRowData(TransactionFinalStatusConverter.getReconciliationTransactionText(statusAfter, statusEventAfter, statusEventCodeAfter, statusEventResultAfter), rowIndex);
                            }
                        }
                        else if (errorType.equals("POSTPAID_LOYALTY_ERROR")) {
                            PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
                            
                            if (postPaidTransactionBean.getLoyaltyReconcile()) {
                                excelData.addRowData(TransactionFinalStatusConverter.getReconciliationTransactionText(errorType, null), rowIndex);
                            }
                            else {
                                excelData.addRowData(TransactionFinalStatusConverter.getReconciliationTransactionText(statusAfter, statusEventAfter, statusEventCodeAfter, statusEventResultAfter), rowIndex);
                            }
                        }
                        //else if (statusAfter.equals(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL) && gfgNotificationAfter)  {
                        //    excelData.addRowData("Notifica GFG inviata", rowIndex);
                        //}*/
                        else {
                            //excelData.addRowData(TransactionFinalStatusConverter.getReportText(statusAfter, statusEventAfter, statusEventCodeAfter, statusEventResultAfter), rowIndex);
                            excelData.addRowData(TransactionFinalStatusConverter.getReconciliationTransactionText(statusAfter, statusEventAfter, statusEventCodeAfter, statusEventResultAfter), rowIndex);
                        }

                        excelData.addRowData(TransactionFinalStatusConverter.getReportText(statusAfter, statusEventAfter, statusEventCodeAfter, statusEventResultAfter), rowIndex);
                        
                        excelData.addRowData(timeEndRefuel, rowIndex);
                        excelData.addRowData(attemptsLeft, rowIndex);
                        excelData.addRowData(fullName, rowIndex);
                        if ( transactionInterfaceBean.getStationBean() != null ) {
                            excelData.addRowData(transactionInterfaceBean.getStationBean().getStationID(), rowIndex);
                            excelData.addRowData(transactionInterfaceBean.getStationBean().getFullAddress(), rowIndex);
                        }
                        else {
                            excelData.addRowData("", rowIndex);
                            excelData.addRowData("", rowIndex);
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    System.err.println("ERRORE (" + ex.toString() + "): " + ex.getMessage());
                }
            }

            //totalCounts = 0;

            if (totalCounts > 0 && (reconciliationRecipient != null && !reconciliationRecipient.isEmpty())) {
                try {
                    //Cell style for header row
                    ExcelCellStyle csHeader = ewb.createCellStyle(true, 12, Color.WHITE, new Color(84, 130, 53), null);
                    //Cell style for table row
                    ExcelCellStyle csTable = ewb.createCellStyle(false, 12, Color.BLACK, new Color(230, 230, 230), null);

                    String[] columnsHeading = new String[] { "Transazione", "Tipo Transazione", "Server", "Importo Preautorizzato", "Importo Effettivo", "Stato Operazione Iniziale",  
                            "Stato Operazione Finale", "Stato Riconciliazione", "Timestamp Fine Rifornimento", "Tentativi Rimasti", "Utente", "PV", "Indirizzo PV" };

                    ewb.addSheet("Transazioni Riconciliate", columnsHeading, excelData, csHeader, csTable);

                    EmailType emailType = EmailType.RECONCILIATION_TRANSACTION_REPORT_V2;
                    List<Parameter> parameters = new ArrayList<Parameter>(0);

                    parameters.add(new Parameter("DATE_OPERATION", sdfEmail.format(new Date())));
                    parameters.add(new Parameter("TRANSACTION_PROCESSED", String.valueOf(totalCounts)));
                    
                    for (int i = 0; i < reconciliationTypes.length; i++) {
                        parameters.add(new Parameter(reconciliationTypes[i][1], statusCounts.get(reconciliationTypes[i][1]).toString()));
                    }

                    System.out.println("Invio email");
                    
                    String keyFrom = emailSender.getSender();
                    
                    if (stringSubstitution != null) {
                        keyFrom = stringSubstitution.getValue(keyFrom, 1);
                    }
                    
                    System.out.println("keyFrom: " + keyFrom);
                    
                    List<Attachment> attachments = new ArrayList<Attachment>(0);

                    // L'allegato deve essere inviato solo se � stato consumato almeno un voucher
                    Attachment attachment = new Attachment();
                    attachment.setFileName("report_transazioni_riconciliate.xlsx");
                    //attachment.setContent(attachmentContent);
                    byte[] bytes = ewb.getBytesToStream();
                    attachment.setBytes(bytes);
                    attachments.add(attachment);

                    System.out.println("Sending email to: " + reconciliationRecipient);
                    //String subject = "Eni Pay - report delle transazioni pre-paid riconciliate " + sdfEmail.format(new Date());
                    String subject = "Eni Station + Report delle transazioni riconciliate " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
                    String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, keyFrom, reconciliationRecipient, null, null, subject, parameters, attachments);
                    System.out.println("SendEmailResult: " + sendEmailResult);
                }
                catch (Exception ex) {
                    System.err.println("Errore nell'invio della email: " + ex.getMessage());
                }

            }

            System.out.println("Commit of transaction");
            userTransaction.commit();                    

        }
        catch (Exception ex) {
            reconciliationTransactionSummary.setStatusCode(ResponseHelper.SYSTEM_ERROR);

            try {
                userTransaction.commit();
            }
            catch (Exception ex2) {}
        }

        return reconciliationTransactionSummary;
    }
}
