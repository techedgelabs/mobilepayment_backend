package com.techedge.mp.core.actions.reconciliation;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.TransactionService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfoData;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.adapter.business.interfaces.RefuelDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendPaymentTransactionResultResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionReconciliationResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ReconciliationReconcileAction {

	@Resource
	private EJBContext context;

	@PersistenceContext( unitName = "CrudPU" )
	private EntityManager em_crud;

	@PersistenceContext( unitName = "LogPU" )
	private EntityManager em_log;

	@EJB
	private LoggerService loggerService;

	final String transactionServiceJndi = "java:app/MPCoreBusinessEJB/TransactionService!com.techedge.mp.core.business.TransactionService";
	private TransactionService transactionService;
	final String userServiceJndi = "java:app/MPCoreBusinessEJB/UserService!com.techedge.mp.core.business.UserService";
	private UserService userService;

	private GPServiceRemote gpServiceRemote;
	private ForecourtInfoServiceRemote forecourtService;


	public ReconciliationReconcileAction() {
	}


	public ReconciliationInfoData execute(
			//    		String adminTicketId,
			String requestId,
			List<String> transactionsID,
			ForecourtInfoServiceRemote forecourtInfoService,
			GPServiceRemote gpServiceRemote) throws EJBException {

		UserTransaction userTransaction = context.getUserTransaction();
		ReconciliationInfoData reconciliationInfoData = new ReconciliationInfoData();

		this.transactionService = (TransactionService)context.lookup(transactionServiceJndi);
		this.userService = (UserService)context.lookup(userServiceJndi);
		this.gpServiceRemote = gpServiceRemote;
		this.forecourtService = forecourtInfoService;

		try {
			userTransaction.begin();

			TransactionBean transactionBean;

			System.out.println("Step A: " + transactionsID.size());
			
			List<String> transactionsIDElab = new ArrayList<String>(0);

			for(String s:transactionsID) {

				System.out.println("Step A: entra nel for ");
				transactionBean = QueryRepository.findTransactionBeanById(em_crud, s);

				System.out.println("Step A: query eseguita" + transactionBean);
				if(transactionBean==null) {

					System.out.println("Step A1: " + s);
					//popolare l'errore "non esistente" ;
					ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
					reconciliationInfo.setTransactionID(s);
					reconciliationInfo.setFinalStatusType(null);
					reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_INVALID_ID);
					reconciliationInfoData.getReconciliationInfoList().add(reconciliationInfo);
					continue;
				} 

				if( !( transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_ERROR) 
						|| transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL)
						|| transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_AFTER_REFUEL)
						|| transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_NOTIFICATION)
						|| transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT))) {

					System.out.println("Step A2: " + transactionBean.getTransactionID());

					//popolare l'errore "non riconciliabile";
					ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
					reconciliationInfo.setTransactionID(s);
					reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
					reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_INVALID_ID);
					reconciliationInfoData.getReconciliationInfoList().add(reconciliationInfo);
					continue;
				}

				System.out.println("Step A3 added: " + transactionBean.getTransactionID());
				transactionsIDElab.add(s);
			}

			userTransaction.commit();

			if (transactionsIDElab.size() == 0) {

				System.out.println("Step A4: non ci sono transazioni valide da elaborare ");
				reconciliationInfoData.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_INVALID_ID);
				//reconciliationInfoData.setReconciliationInfoList(null);

				return reconciliationInfoData;

			}
			else {

				System.out.println("Step pre B: " + forecourtInfoService);
				TransactionReconciliationResponse transactionReconciliationResponse = forecourtInfoService.transactionReconciliation(
						requestId,
						transactionsIDElab );

				//Per ogni transazione occorre effettuare il controllo con il backend;
				System.out.println("Step B: ");
				List<TransactionDetail> transactionDetails = transactionReconciliationResponse.getTransactionDetails();

				System.out.println("Step B:1 " + transactionDetails.size());
				TransactionStatusBean transactionStatusBean = null;

				Set<TransactionStatusBean> transactionStatusBeanData;
				boolean flag_found = false;

				for (TransactionDetail td: transactionDetails) {

					System.out.println("Step B:2 " + td.getTransactionID());

					System.out.println("Step B:3 " + td.getTransactionID());
					ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
					reconciliationInfo.setTransactionID(td.getTransactionID());

					System.out.println("Step B:4 " + td.getTransactionID());
					userTransaction.begin();
					transactionBean = QueryRepository.findTransactionBeanById(em_crud, td.getTransactionID());
					//userTransaction.commit();

					System.out.println("Step B:5 " + transactionBean.getTransactionID());

					if (transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_ERROR)){
						//userTransaction.begin();
						transactionStatusBeanData = transactionBean.getTransactionStatusBeanData();

						//userTransaction.commit();

						transactionStatusBean = null;

						System.out.println("Step B:5 " + transactionBean.getTransactionID() + "_" + transactionStatusBeanData);
						System.out.println("Step B:6 "  + transactionBean.getTransactionID() + "_" + transactionStatusBeanData.size());

						for(TransactionStatusBean tsb: transactionStatusBeanData) {

							System.out.println("Step B:7 " + tsb.getSequenceID());
							if(transactionStatusBean == null || transactionStatusBean.getSequenceID() < tsb.getSequenceID()) {
								transactionStatusBean = tsb;
							}
						}

						userTransaction.commit();

						if(transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PUMP_GENERIC_FAULT)){ //0017
							System.out.println("Step B1 GenericFault: " + transactionBean.getTransactionID());
							this.reconcilePumpGenericFault(transactionBean, transactionStatusBean, td, reconciliationInfo);
							reconciliationInfoData.getReconciliationInfoList().add(reconciliationInfo);
							flag_found = true;
						}
						
						if(transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_AUTHORIZATION_NOT_DELETED)){ //0006
							System.out.println("Step B2 AuthDelete: " + transactionBean.getTransactionID());
							this.reconcilePaymentAuthDelete(transactionBean, transactionStatusBean, td, reconciliationInfo);
							reconciliationInfoData.getReconciliationInfoList().add(reconciliationInfo);
							flag_found = true;
						}
						
						if(transactionStatusBean.getStatus().equals(StatusHelper.STATUS_TRANSACTION_STATUS_REQUEST_KO)){ //0012
							System.out.println("Step B3 TransactionStatusRequestKo: " + transactionBean.getTransactionID());
							this.reconcilePumpGenericFault(transactionBean, transactionStatusBean, td, reconciliationInfo);
							reconciliationInfoData.getReconciliationInfoList().add(reconciliationInfo);
							flag_found = true;
						}

					}
					else{
						userTransaction.commit();
					}
					
					

					if (transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_NOTIFICATION)){

						//if(transactionStatusBean.getStatus().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_NOTIFICATION)){ //MISSING_NOTIFICATION
						System.out.println("Step C MissingNotification: " + transactionBean.getTransactionID());
						this.reconcileMissingNotifGFG(transactionBean, td, reconciliationInfo);
						reconciliationInfoData.getReconciliationInfoList().add(reconciliationInfo);
						flag_found = true;
						//}

					}

					if (transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL)){

						//if(transactionStatusBean.getStatus().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_NOTIFICATION)){ //MISSING_NOTIFICATION
						System.out.println("Step D PAYAUTH_DELETE_BEFORE_REFUEL: " + transactionBean.getTransactionID());
						this.reconcilePaymentAuthDeleteRefuel(transactionBean, reconciliationInfo, false);
						reconciliationInfoData.getReconciliationInfoList().add(reconciliationInfo);
						flag_found = true;
						//}

					}

					if (transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_AFTER_REFUEL)){

						//if(transactionStatusBean.getStatus().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_NOTIFICATION)){ //MISSING_NOTIFICATION
						System.out.println("Step D PAYAUTH_DELETE_AFTER_REFUEL: " + transactionBean.getTransactionID());
						this.reconcilePaymentAuthDeleteRefuel(transactionBean, reconciliationInfo, true);
						reconciliationInfoData.getReconciliationInfoList().add(reconciliationInfo);
						flag_found = true;
						//}

					}

					if (transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT)){

						System.out.println("Step E MISSING_PAYMENT: " + transactionBean.getTransactionID());
						this.reconcileMissingPayment(transactionBean, td, reconciliationInfo);
						reconciliationInfoData.getReconciliationInfoList().add(reconciliationInfo);
						flag_found = true;

					}

					if (!flag_found){

						System.out.println("Invalid action: " + transactionBean.getTransactionID());

						reconciliationInfo = new ReconciliationInfo();
						reconciliationInfo.setTransactionID(transactionBean.getTransactionID());
						reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
						reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_INVALID_ACTION);
						reconciliationInfoData.getReconciliationInfoList().add(reconciliationInfo);
					}


				}


				reconciliationInfoData.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_SUCCESS);

				return reconciliationInfoData;
			}

		}
		catch (Exception ex2) {

			//			try {
			//				//non faccio modifiche dirette dunque non serve il rollback
			//			//	userTransaction.rollback();
			//			} catch (IllegalStateException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			} catch (SecurityException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			} catch (SystemException e) {
			//				// TODO Auto-generated catch block
			//				e.printStackTrace();
			//			}

			String message = "FAILED reconciliation  log with message (" + ex2.getMessage() + ")";
			this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );

			throw new EJBException(ex2);
		}
	}



	private void reconcileMissingNotifGFG(TransactionBean transactionBean,
			TransactionDetail td, ReconciliationInfo reconciliationInfo) {

		GestPayData gestPayData = new GestPayData();


		gestPayData.setAmount(String.valueOf(transactionBean.getFinalAmount()));
		//gestPayData.setTransactionID(transactionBean.getTransactionID());


		gestPayData.setAuthorizationCode(transactionBean.getAuthorizationCode());
		gestPayData.setBankTransactionID(transactionBean.getBankTansactionID());
		Set<TransactionEventBean> transactionEventsBeanData ;
		UserTransaction userTransaction = context.getUserTransaction();
		try {
			
			userTransaction.begin();
			System.out.println("Step A: entra nel for ");
			TransactionBean tempTransactionBean = QueryRepository.findTransactionBeanById(em_crud, transactionBean.getTransactionID());
		
		transactionEventsBeanData = tempTransactionBean.getTransactionEventBeanData();
		TransactionEventBean transactionEventBean = null;

		for(TransactionEventBean teb: transactionEventsBeanData){

			if(transactionEventBean == null || transactionEventBean.getSequenceID()<teb.getSequenceID()){
				transactionEventBean = teb;
			}
		}
	
		gestPayData.setTransactionType(transactionEventBean.getEventType());
		gestPayData.setErrorDescription(transactionEventBean.getErrorDescription());
		gestPayData.setErrorCode(transactionEventBean.getErrorCode());
		gestPayData.setTransactionResult(transactionEventBean.getTransactionResult());
		userTransaction.commit();

		this.notifyGFG(transactionBean, reconciliationInfo, gestPayData);
		}
		catch (Exception ex2) {


			String message = "FAILED reconciliation  log with message (" + ex2.getMessage() + ")";
			this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", "", null, message );

			throw new EJBException(ex2);
		}

	}


	private void reconcilePaymentAuthDelete(TransactionBean transactionBean, TransactionStatusBean transactionStatusBean, TransactionDetail transactionDetail, ReconciliationInfo reconciliationInfo ) {
		GestPayData gestPayData = null;
		if ( transactionDetail.getStatusCode().equals("TRANSACTION_NOT_RECOGNIZED_400")){
			System.out.println("Step PaymentAuthDelete A1: "+transactionBean.getTransactionID());
			reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
		}

		if ( transactionDetail.getStatusCode().equals("MESSAGE_RECEIVED_200")){
			System.out.println("Step PaymentAuthDelete B1: "+transactionBean.getTransactionID());
			RefuelDetail refuelDetail = transactionDetail.getRefuelDetail();
			if(refuelDetail==null){ //esiste la transazione ma non c'� stato rifornimento;
				//
				System.out.println("Step D PAYAUTH_DELETE_BEFORE_REFUEL: " + transactionBean.getTransactionID());
				this.reconcilePaymentAuthDeleteRefuel(transactionBean, reconciliationInfo, false);


				// la cancellazione non � andata a buon fine
				//				System.out.println("Step GenericFault B2: "+transactionBean.getTransactionID());
				//				reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
				//				reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
			}

			if(refuelDetail!=null){
				System.out.println("Step PaymentAuthDelete B3: "+transactionBean.getTransactionID());
				if(!(refuelDetail.getAmount()> 0.0)){
					//annulla il pagamento e notifica;
					System.out.println("Step PaymentAuthDelete B4a: "+transactionBean.getTransactionID());
					gestPayData = 	gpServiceRemote.deletePagam(transactionBean.getInitialAmount(), 
							transactionBean.getTransactionID(),
							transactionBean.getShopLogin(), 
							transactionBean.getCurrency(),
							transactionBean.getBankTansactionID(),
							"RECONCILE",
                            transactionBean.getAcquirerID(),
                            transactionBean.getGroupAcquirer(),
                            transactionBean.getEncodedSecretKey());

					if (gestPayData== null){
						System.out.println("Step PaymentAuthDelete B4a null: "+transactionBean.getTransactionID());
						// la cancellazione non � andata a buon fine
						reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
						reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
					}
					else {
						System.out.println("Step PaymentAuthDelete B4a ok: "+transactionBean.getTransactionID());
						this.procedeWithDeletion(transactionBean, reconciliationInfo, true, gestPayData, refuelDetail);
					}
				}else{
					//completa il pagamento e notifica
					System.out.println("Step PaymentAuthDelete B4b: "+transactionBean.getTransactionID());
					gestPayData = 	gpServiceRemote.callSettle(refuelDetail.getAmount(),
							transactionBean.getTransactionID(), 
							transactionBean.getShopLogin(), 
							transactionBean.getCurrency(),
							transactionBean.getAcquirerID(),
                            transactionBean.getGroupAcquirer(),
                            transactionBean.getEncodedSecretKey(),
                            transactionBean.getToken(),
                            transactionBean.getAuthorizationCode(),
                            transactionBean.getBankTansactionID(),
                            transactionBean.getRefuelMode(),
                            transactionBean.getProductID(),
                            transactionBean.getFuelQuantity(),
                            transactionBean.getUnitPrice());
					if (gestPayData== null){
						System.out.println("Step PaymentAuthDelete B4b null: "+transactionBean.getTransactionID());
						// la cancellazione non � andata a buon fine
						reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
						reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
					}
					else {
						System.out.println("Step PaymentAuthDelete B4b ok: "+transactionBean.getTransactionID());
						this.procedeWithPayment(transactionBean, reconciliationInfo, true, gestPayData, refuelDetail);
					}
				}
			}
		}
	}


	private void reconcileMissingPayment(TransactionBean transactionBean, TransactionDetail transactionDetail, ReconciliationInfo reconciliationInfo ) {

		GestPayData gestPayData = null;

		if ( transactionDetail.getStatusCode().equals("TRANSACTION_NOT_RECOGNIZED_400")) {

			System.out.println("Step reconcileMissingPayment A1: " + transactionBean.getTransactionID());
			reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
		}

		if ( transactionDetail.getStatusCode().equals("MESSAGE_RECEIVED_200")) {

			// TODO Se il pagamento � gi� stato effettuato non deve essere fatto nuovamente; verificare con il servizio di gestpay

			System.out.println("Step reconcileMissingPayment B1: " + transactionBean.getTransactionID());

			RefuelDetail refuelDetail = transactionDetail.getRefuelDetail();
			if ( refuelDetail==null ) {

				// C'� un'incongruenza tra quanto registrato sul backend e quanto restituito dal forecourt; la transazione dovrebbe avere l'oggetto refuelDetail non null
				System.out.println("Step reconcileMissingPayment B2a: il forecourt ha restituito un refuelDetail null");
				reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
				reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
			}
			else {

				System.out.println("Step reconcileMissingPayment B2b: "+transactionBean.getTransactionID());

				if(!(refuelDetail.getAmount() > 0.0)) {

					// C'� un'incongruenza tra quanto registrato sul backend e quanto restituito dal forecourt; la transazione dovrebbe avere l'oggetto refuelDetail non null
					System.out.println("Step reconcileMissingPayment B3a: il forecourt ha restituito un refuelDetail con importo pari a 0");
					reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
					reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
				}
				else {

					//completa il pagamento e notifica
					System.out.println("Step reconcileMissingPayment B3b: " + transactionBean.getTransactionID());

					gestPayData = gpServiceRemote.callSettle(
							refuelDetail.getAmount(),
							transactionBean.getTransactionID(), 
							transactionBean.getShopLogin(), 
							transactionBean.getCurrency(),
							transactionBean.getAcquirerID(),
                            transactionBean.getGroupAcquirer(),
                            transactionBean.getEncodedSecretKey(),
                            transactionBean.getToken(),
                            transactionBean.getAuthorizationCode(),
                            transactionBean.getBankTansactionID(),
                            transactionBean.getRefuelMode(),
                            transactionBean.getProductID(),
                            transactionBean.getFuelQuantity(),
                            transactionBean.getUnitPrice());

					if (gestPayData== null || !gestPayData.getTransactionResult().equals("OK")) {

						System.out.println("Step reconcileMissingPayment B4a gestPayData null - la movimentazione non � andata a buon fine: " + transactionBean.getTransactionID());
						// la movimentazione non � andata a buon fine
						reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
						reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
					}
					else {
						System.out.println("Step reconcileMissingPayment B4b ok: " + transactionBean.getTransactionID());
						this.procedeWithPayment(transactionBean, reconciliationInfo, true, gestPayData, refuelDetail);
					}
				}
			}
		}
	}


	private void reconcilePumpGenericFault(TransactionBean transactionBean, TransactionStatusBean transactionStatusBean, TransactionDetail transactionDetail, ReconciliationInfo reconciliationInfo ) {

		/* Se il dettaglio corrisponde a Notrecognized allora viene cancellata l'autorizzazione;
		 * 
		 */
		GestPayData gestPayData = null;
		if ( transactionDetail.getStatusCode().equals("TRANSACTION_NOT_RECOGNIZED_400")){
			//occorre eliminare l'autorizzazione e settare lo stato intermedio a reconcyle e quello principale a ...;
			System.out.println("Step GenericFault A1: "+transactionBean.getTransactionID());
			gestPayData = 	gpServiceRemote.deletePagam(transactionBean.getInitialAmount(), 
					transactionBean.getTransactionID(),
					transactionBean.getShopLogin(), 
					transactionBean.getCurrency(),
					transactionBean.getBankTansactionID(),
					"RECONCILE",
                    transactionBean.getAcquirerID(),
                    transactionBean.getGroupAcquirer(),
                    transactionBean.getEncodedSecretKey());

			if (gestPayData== null){
				// la cancellazione non � andata a buon fine
				System.out.println("Step GenericFault A2: "+transactionBean.getTransactionID());
				reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
				reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
			}
			else {
				System.out.println("Step GenericFault A3: "+transactionBean.getTransactionID());
				this.procedeWithDeletion(transactionBean, reconciliationInfo, false, gestPayData, null);
			}
		}

		/* Se il dettaglio � positivo allora viene cancellata/completata in base agli stati di forecourt;
		 * 
		 */

		if ( transactionDetail.getStatusCode().equals("MESSAGE_RECEIVED_200")){
			System.out.println("Step GenericFault B1: "+transactionBean.getTransactionID());
			RefuelDetail refuelDetail = transactionDetail.getRefuelDetail();
			if(refuelDetail==null){ //esiste la transazione ma non c'� stato rifornimento;
				gestPayData = 	gpServiceRemote.deletePagam(transactionBean.getInitialAmount(), 
						transactionBean.getTransactionID(),
						transactionBean.getShopLogin(), 
						transactionBean.getCurrency(),
						transactionBean.getBankTansactionID(),
						"RECONCILE",
                        transactionBean.getAcquirerID(),
                        transactionBean.getGroupAcquirer(),
                        transactionBean.getEncodedSecretKey());

				if (gestPayData== null){
					// la cancellazione non � andata a buon fine
					System.out.println("Step GenericFault B2: "+transactionBean.getTransactionID());
					reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
					reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
				}
				else {
					System.out.println("Step GenericFault B3: "+transactionBean.getTransactionID());
					this.procedeWithDeletion(transactionBean, reconciliationInfo, true, gestPayData, refuelDetail);

				}
			}

			if(refuelDetail!=null){
				System.out.println("Step GenericFault B4: "+transactionBean.getTransactionID());
				if(!(refuelDetail.getAmount()> 0.0)){
					//annulla il pagamento e notifica;
					System.out.println("Step GenericFault B4a: "+transactionBean.getTransactionID());
					gestPayData = 	gpServiceRemote.deletePagam(transactionBean.getInitialAmount(), 
							transactionBean.getTransactionID(),
							transactionBean.getShopLogin(), 
							transactionBean.getCurrency(),
							transactionBean.getBankTansactionID(),
							"RECONCILE",
                            transactionBean.getAcquirerID(),
                            transactionBean.getGroupAcquirer(),
                            transactionBean.getEncodedSecretKey());

					if (gestPayData== null){
						System.out.println("Step GenericFault B4a null: "+transactionBean.getTransactionID());
						// la cancellazione non � andata a buon fine
						reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
						reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
					}
					else {
						System.out.println("Step GenericFault B4a ok: "+transactionBean.getTransactionID());
						this.procedeWithDeletion(transactionBean, reconciliationInfo, true, gestPayData, refuelDetail);
					}
				}
				else
				{	
					//completa il pagamento e notifica
					Date now = new Date();
			    	String requestID = String.valueOf(now.getTime());
			    	String productID = refuelDetail.getProductID();
			    	
					String response = transactionService.persistEndRefuelReceivedStatus(
							requestID,
							transactionBean.getTransactionID(),
							(Double) refuelDetail.getAmount(),
							(Double) refuelDetail.getFuelQuantity(),
							refuelDetail.getFuelType(),
							refuelDetail.getProductDescription(),
							productID,
							refuelDetail.getTimestampEndRefuel(),
							refuelDetail.getUnitPrice(),
							"getTransactionStatus");
					
					if ( response == ResponseHelper.SYSTEM_ERROR ) {
						
						System.out.println("Step GenericFault B4b System Error: "+transactionBean.getTransactionID());
						// l'inserimento dello stato di fine rifornimento non � andato a buon fine
						reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
						reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
					}
					else {
						
						System.out.println("Step GenericFault B4b: "+transactionBean.getTransactionID());
						gestPayData = 	gpServiceRemote.callSettle(refuelDetail.getAmount(),
								transactionBean.getTransactionID(), 
								transactionBean.getShopLogin(), 
								transactionBean.getCurrency(),
								transactionBean.getAcquirerID(),
	                            transactionBean.getGroupAcquirer(),
	                            transactionBean.getEncodedSecretKey(),
	                            transactionBean.getToken(),
	                            transactionBean.getAuthorizationCode(),
	                            transactionBean.getBankTansactionID(),
	                            transactionBean.getRefuelMode(),
	                            transactionBean.getProductID(),
	                            transactionBean.getFuelQuantity(),
	                            transactionBean.getUnitPrice());
						if (gestPayData== null){
							System.out.println("Step GenericFault B4b null: "+transactionBean.getTransactionID());
							// il pagamento non � andato a buon fine
							reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
							reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
						}
						else {
							System.out.println("Step GenericFault B4b ok: "+transactionBean.getTransactionID());
							this.procedeWithPayment(transactionBean, reconciliationInfo, true, gestPayData, refuelDetail);
						}
					}
				}
			}
		}
	}


	private void notifyGFG(TransactionBean transactionBean, ReconciliationInfo reconciliationInfo, GestPayData gestPayData ) {

		SendPaymentTransactionResultResponse sendPaymentTransactionResult = new SendPaymentTransactionResultResponse();

		PaymentTransactionResult paymentTransactionResult = new PaymentTransactionResult();

		paymentTransactionResult.setAuthorizationCode(gestPayData.getAuthorizationCode());
		paymentTransactionResult.setBankTransactionID(gestPayData.getBankTransactionID());
		paymentTransactionResult.setEventType(gestPayData.getTransactionType());
		paymentTransactionResult.setErrorDescription(gestPayData.getErrorDescription());
		paymentTransactionResult.setErrorCode(gestPayData.getErrorCode());
		paymentTransactionResult.setTransactionResult(gestPayData.getTransactionResult());

		Date now = new Date();
		String requestID = String.valueOf(now.getTime());

		sendPaymentTransactionResult = forecourtService.sendPaymentTransactionResult(
				requestID,
				transactionBean.getTransactionID(),
				Double.valueOf(gestPayData.getAmount()),
				paymentTransactionResult);
		
		if(sendPaymentTransactionResult.getStatusCode().equals("TRANSACTION_NOT_RECOGNIZED_400")){
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
		}
		else{
			reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_SUCCESS);
		}
	}


	private void procedeWithPayment(TransactionBean transactionBean, ReconciliationInfo reconciliationInfo, boolean notifyFlag , GestPayData gestPayData, RefuelDetail refuelDetail) {

		System.out.println("call procedeWithPayment");
		
		String errorCode         = gestPayData.getErrorCode();
		String errorMessage      = gestPayData.getErrorDescription();
		String transactionResult = "";
		if ( errorCode.equals("0") ) {
			transactionResult = "ok";
		}
		else {
			transactionResult = "ko";
		}

		String response = transactionService.persistPaymentCompletionStatus(
				transactionResult, 
				StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_RECONCILE, 
				errorCode,
				errorMessage,
				transactionBean.getTransactionID(), 
				transactionBean.getBankTansactionID(),
				refuelDetail.getAmount());

		System.out.println("response: " + response);

		if (response.equals(StatusHelper.PERSIST_PAYMENT_COMPLETION_STATUS_SUCCESS)) {
			TransactionBean transactionBeanPost;
			try{
				UserTransaction userTransaction = context.getUserTransaction();

				userTransaction.begin();
				transactionBeanPost = QueryRepository.findTransactionBeanById(em_crud, transactionBean.getTransactionID());
				userTransaction.commit();
				//reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
				reconciliationInfo.setFinalStatusType(transactionBeanPost.getFinalStatusType());
				reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_SUCCESS);

				if (notifyFlag){
					this.notifyGFG(transactionBean, reconciliationInfo, gestPayData);
				}

			}
			catch (Exception ex2) {
				reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
				reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);

			}

		}
		else {

			reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
		}
	}


	private void reconcilePaymentAuthDeleteRefuel(TransactionBean transactionBean, ReconciliationInfo reconciliationInfo, boolean notify_flag ) {

		GestPayData gestPayData = null;

		//annulla il pagamento e notifica;
		System.out.println("Step PaymentAuthDeleteRefuel D4a: " + transactionBean.getTransactionID());

		gestPayData = gpServiceRemote.deletePagam(
				transactionBean.getInitialAmount(), 
				transactionBean.getTransactionID(),
				transactionBean.getShopLogin(), 
				transactionBean.getCurrency(),
				transactionBean.getBankTansactionID(),
				"RECONCILE",
                transactionBean.getAcquirerID(),
                transactionBean.getGroupAcquirer(),
                transactionBean.getEncodedSecretKey());

		if (gestPayData== null) {

			System.out.println("Step PaymentAuthDeleteRefuel D4a null: "+transactionBean.getTransactionID());
			// la cancellazione non � andata a buon fine
			reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
		}
		else {
			System.out.println("Step PaymentAuthDeleteRefuel D4a ok: "+transactionBean.getTransactionID());
			this.procedeWithDeletion(transactionBean, reconciliationInfo, notify_flag, gestPayData, null);
		}	
	}


	private void procedeWithDeletion(TransactionBean transactionBean, ReconciliationInfo reconciliationInfo, boolean notifyFlag , GestPayData gestPayData, RefuelDetail refuelDetail){

		System.out.println("notifyFlag: " + notifyFlag);

		String statusCode        = "";
		String transactionResult = "ok";
		
		if ( notifyFlag == true ) {
			statusCode        = "ok_za";
		}
		else {
			statusCode = "ok";
		}

		System.out.println("statusCode: " + statusCode);
		
		String errorCode    = gestPayData.getErrorCode();
		String errorMessage = gestPayData.getErrorDescription();

		String response = transactionService.persistPaymentDeletionStatus(
				transactionResult, 
				StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_RECONCILE, 
				errorCode,
				errorMessage,
				transactionBean.getTransactionID(), 
				transactionBean.getBankTansactionID());

		if (response.equals(StatusHelper.PERSIST_PAYMENT_DELETION_STATUS_SUCCESS)) {
			TransactionBean transactionBeanPost;

			try{
				UserTransaction userTransaction = context.getUserTransaction();

				userTransaction.begin();
				transactionBeanPost = QueryRepository.findTransactionBeanById(em_crud, transactionBean.getTransactionID());
				userTransaction.commit();
				//reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
				reconciliationInfo.setFinalStatusType(transactionBeanPost.getFinalStatusType());
				reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_SUCCESS);



				this.userService.refundAvailableCap(transactionBean.getTransactionID());

				if (notifyFlag) {
					this.notifyGFG(transactionBean, reconciliationInfo, gestPayData);
				}

			}
			catch (Exception ex2) {
				reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
				reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
			}

		}
		else {

			reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
			reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_RECONCILE_FAILURE);
		}

	}

}
