package com.techedge.mp.core.actions.reconciliation;

import java.awt.Color;
import java.lang.reflect.Constructor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.actions.reconciliation.status.user.ReconciliationUserStatus;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationUserData;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationUserSummary;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelSheetData;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.utilities.TransactionFinalStatusConverter;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ReconciliationUserAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    public ReconciliationUserAction() {}

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public ReconciliationUserSummary execute(DWHAdapterServiceRemote dwhAdapterService, CRMAdapterServiceRemote crmAdapterService,
            PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, ParametersServiceRemote parametersService, String proxyHost, String proxyPort, String proxyNoHosts, 
            Integer pushNotificationExpiryTime, Integer reconciliationAttemptsLeft, String reconciliationRecipient, List<ReconciliationUserData> userReconciliationInterfaceIDList, 
            StringSubstitution stringSubstitution, Boolean refuelingOauth2Active) {

        ReconciliationUserSummary reconciliationSummary = new ReconciliationUserSummary();

        UserTransaction userTransaction = context.getUserTransaction();

        String[][] reconciliationTypes = new String[][] {
                new String[] { "com.techedge.mp.core.actions.reconciliation.status.user.DWHEventErrorStatus", "DWH_EVENT_ERROR", ReconciliationUserSummary.SUMMARY_DWH_EVENT_ERROR },
                new String[] { "com.techedge.mp.core.actions.reconciliation.status.user.PushNotificationErrorStatus", "PUSH_NOTIFICATION_ERROR",
                        ReconciliationUserSummary.SUMMARY_PUSH_NOTIFICATION_ERROR },
                new String[] { "com.techedge.mp.core.actions.reconciliation.status.user.CRMSfEventErrorStatus", "CRM_SF_OFFER_ERROR",
                        ReconciliationUserSummary.SUMMARY_CRM_OFFER_ERROR },
                new String[] { "com.techedge.mp.core.actions.reconciliation.status.user.CRMEventErrorStatus", "CRM_OFFER_ERROR",
                        ReconciliationUserSummary.SUMMARY_CRM_OFFER_ERROR },
                new String[] { "com.techedge.mp.core.actions.reconciliation.status.user.CRMOfferVoucherPromotionalErrorStatus", "CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR",
                        ReconciliationUserSummary.SUMMARY_CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR },
                new String[] { "com.techedge.mp.core.actions.reconciliation.status.user.RedemptionErrorStatus", "REDEMPTION_ERROR",
                        ReconciliationUserSummary.SUMMARY_REDEMPTION_ERROR },
                new String[] { "com.techedge.mp.core.actions.reconciliation.status.user.RefuelingSubscriptionError", "REFUELING_SUBSCRIPTION_ERROR",
                        ReconciliationUserSummary.SUMMARY_REFUELING_SUBSCRIPTION_ERROR } 
        };

        HashMap<String, Integer> statusCounts = new HashMap<String, Integer>();
        
        int totalCounts     = 0;
        int failedCounts    = 0;
        int pendingCounts   = 0;
        int cancelledCounts = 0;
        int completedCounts = 0;
        int exceptionCounts = 0;
        
        //New Workbook
        ExcelWorkBook ewb = new ExcelWorkBook();
        ExcelSheetData excelData = ewb.createSheetData();
        SimpleDateFormat sdfTransaction = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat sdfEmail = new SimpleDateFormat("dd/MM/yyyy");

        try {

            userTransaction.begin();

            for (int i = 0; i < reconciliationTypes.length; i++) {
                String fileClass = reconciliationTypes[i][0];
                String errorType = reconciliationTypes[i][1];
                String summaryType = reconciliationTypes[i][2];
                Class obj = null;
                try {
                    obj = Class.forName(fileClass);
                    ReconciliationUserStatus reconciliationUserStatus;
                    Constructor objConstructor;

                    objConstructor = obj.getConstructor(EntityManager.class, DWHAdapterServiceRemote.class, CRMAdapterServiceRemote.class, PushNotificationServiceRemote.class,
                            FidelityServiceRemote.class, EmailSenderRemote.class, RefuelingNotificationServiceRemote.class, RefuelingNotificationServiceOAuth2Remote.class, ParametersServiceRemote.class, String.class, String.class, String.class, Integer.class,
                            Integer.class, Boolean.class);

                    reconciliationUserStatus = (ReconciliationUserStatus) objConstructor.newInstance(em, dwhAdapterService, crmAdapterService, pushNotificationService,
                            fidelityService, emailSender, refuelingNotificationService, refuelingNotificationServiceOAuth2, parametersService, proxyHost, proxyPort, proxyNoHosts, pushNotificationExpiryTime, reconciliationAttemptsLeft, refuelingOauth2Active);

                    String label = reconciliationUserStatus.getName();
                    //String status = reconciliationUserStatus.getStatus();
                    List<UserReconciliationInterfaceBean> transactionList = null;
                    List<UserReconciliationInterfaceBean> tmpTransactionList = reconciliationUserStatus.getTransactionsList();

                    if (userReconciliationInterfaceIDList != null && !userReconciliationInterfaceIDList.isEmpty() && tmpTransactionList != null) {

                        transactionList = new ArrayList<UserReconciliationInterfaceBean>(0);

                        for (UserReconciliationInterfaceBean userReconciliationInterfaceBean : tmpTransactionList) {

                            if (errorType != null && userReconciliationInterfaceIDList != null) {
                                List<String> result = null;

                                for (ReconciliationUserData item : userReconciliationInterfaceIDList) {
                                    if (item.getReconciliationType().equals(errorType)) {
                                        result = item.getTransactionList();
                                    }
                                }
                                if (result != null) {
                                    for (String transactionID : result) {
                                        System.out.println("ID: " + userReconciliationInterfaceBean.getId());
                                        if (transactionID.equals(userReconciliationInterfaceBean.getId().toString())) {
                                            System.out.println("TROVATA");
                                            transactionList.add(userReconciliationInterfaceBean);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else {
                        transactionList = tmpTransactionList;
                    }

                    if (transactionList == null) {
                        System.out.println("Nessuna operazione trovata per lo stato " + label);
                        statusCounts.put(errorType, 0);
                        continue;
                    }

                    //userTransaction.begin();
                    System.out.println("Trovate " + transactionList.size() + " operazioni per lo stato " + label);
                    statusCounts.put(errorType, transactionList.size());
                    totalCounts += transactionList.size();
                    
                    int blockCount = 0;

                    for (int index = 0; index < transactionList.size(); index++) {

                        UserReconciliationInterfaceBean userReconciliationInterfaceBean = transactionList.get(index);

                        String fullName = userReconciliationInterfaceBean.getUserBean().getPersonalDataBean().getFirstName() + " "
                                + userReconciliationInterfaceBean.getUserBean().getPersonalDataBean().getLastName();

                        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();

                        String statusBefore = userReconciliationInterfaceBean.getFinalStatus();
                        String attemptsLeft = "";

                        System.out.println("Inizio riconciliazione per l'operazione " + userReconciliationInterfaceBean.getId());
                        System.out.println("Stato iniziale operazione: " + userReconciliationInterfaceBean.getFinalStatus());

                        try {
                            if (reconciliationUserStatus.checkAttemptsLeft(userReconciliationInterfaceBean)) {

                                reconciliationInfo = reconciliationUserStatus.reconciliate(userReconciliationInterfaceBean);
                            }
                            else {
                                reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                            }
                        }
                        catch (Exception ex) {
                            ex.printStackTrace();
                            System.err.println("Errore nella procedura di rincociliazione: " + ex.getMessage());
                            reconciliationInfo.setFinalStatusType(statusBefore);
                            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                            reconciliationUserStatus.decreaseAttempts(userReconciliationInterfaceBean);

                            if (!reconciliationUserStatus.checkAttemptsLeft(userReconciliationInterfaceBean)) {
                                reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                            }
                        }

                        System.out.println("ID Operazione: " + userReconciliationInterfaceBean.getId());
                        reconciliationInfo.setTransactionID(String.valueOf(userReconciliationInterfaceBean.getId()));
                        reconciliationSummary.addToSummary(summaryType, reconciliationInfo);
                        System.out.println("Stato finale riconciliazione: " + reconciliationInfo.getFinalStatusType());
                        System.out.println("Fine riconciliazione per l'operazione " + userReconciliationInterfaceBean.getId());

                        String statusAfter = userReconciliationInterfaceBean.getFinalStatus();

                        if (userReconciliationInterfaceBean.getReconciliationAttemptsLeft() != null && attemptsLeft.equals("")) {
                            attemptsLeft = userReconciliationInterfaceBean.getReconciliationAttemptsLeft().toString();
                        }

                        int rowIndex = excelData.createRow();
                        
                        // Sistema
                        excelData.addRowData(userReconciliationInterfaceBean.getServerEndpoint(), rowIndex);
                        
                        // Id
                        excelData.addRowData(String.valueOf(userReconciliationInterfaceBean.getId()), rowIndex);
                        String operationDate = "";

                        if (userReconciliationInterfaceBean.getOperationTimestamp() != null) {
                            operationDate = sdfTransaction.format(userReconciliationInterfaceBean.getOperationTimestamp());
                        }

                        excelData.addRowData(operationDate, rowIndex);
                        excelData.addRowData(TransactionFinalStatusConverter.getReconciliationUserText(userReconciliationInterfaceBean.getOperationName()), rowIndex);
                        excelData.addRowData(fullName, rowIndex);
                        excelData.addRowData(userReconciliationInterfaceBean.getUserBean().getPersonalDataBean().getSecurityDataEmail(), rowIndex);
                        excelData.addRowData(TransactionFinalStatusConverter.getReconciliationUserText(statusBefore, userReconciliationInterfaceBean.getOperationName()), rowIndex);
                        excelData.addRowData(TransactionFinalStatusConverter.getReconciliationUserText(statusAfter, userReconciliationInterfaceBean.getOperationName()), rowIndex);
                        excelData.addRowData(userReconciliationInterfaceBean.getFinalStatusMessage(), rowIndex);
                        excelData.addRowData(attemptsLeft, rowIndex);
                        excelData.addRowData(TransactionFinalStatusConverter.getReconciliationUserText(reconciliationInfo.getStatusCode(), null), rowIndex);
                        
                        if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS)) {
                            completedCounts++;
                        }
                        
                        if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE)) {
                            failedCounts++;
                        }
                        
                        if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY)) {
                            pendingCounts++;
                        }
                        
                        if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_CANCELLED)) {
                            cancelledCounts++;
                        }
                        
                        blockCount++;
                        if (label.equals("CRM SF EVENT") && blockCount >= 100) {
                            System.out.println("Riconciliazione CRM EVENT - elemento " + blockCount + " --> exit");
                            break;
                        }
                        else {
                            System.out.println("Riconciliazione CRM EVENT - elemento " + blockCount);
                        }
                    }
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    System.err.println("ERRORE (" + ex.toString() + "): " + ex.getMessage());
                    exceptionCounts++;
                }
            }

            //totalCounts = 0;

            if (totalCounts > 0 && (reconciliationRecipient != null && !reconciliationRecipient.isEmpty())) {
                try {
                    //Cell style for header row
                    ExcelCellStyle csHeader = ewb.createCellStyle(true, 12, Color.WHITE, new Color(84, 130, 53), null);
                    //Cell style for table row
                    ExcelCellStyle csTable = ewb.createCellStyle(false, 12, Color.BLACK, new Color(230, 230, 230), null);

                    String[] columnsHeading = new String[] { "Sistema", "ID Operazione", "Timestamp Operazione", "Operazione", "Utente", "Email", "Stato Operazione Iniziale",
                            "Stato Operazione Finale", "Messaggio", "Tentativi Rimasti", "Stato Riconciliazione" };

                    ewb.addSheet("Transazioni Reconciliate", columnsHeading, excelData, csHeader, csTable);

                    EmailType emailType = EmailType.RECONCILIATION_USER_REPORT_V2;
                    List<Parameter> parameters = new ArrayList<Parameter>(0);

                    parameters.add(new Parameter("DATE_OPERATION", sdfEmail.format(new Date())));
                    parameters.add(new Parameter("TRANSACTION_PROCESSED", String.valueOf(totalCounts)));
                    parameters.add(new Parameter("TRANSACTION_FAILED", String.valueOf(failedCounts)));
                    parameters.add(new Parameter("TRANSACTION_PENDING", String.valueOf(pendingCounts)));
                    parameters.add(new Parameter("TRANSACTION_CANCELLED", String.valueOf(cancelledCounts)));
                    parameters.add(new Parameter("TRANSACTION_COMPLETED", String.valueOf(completedCounts)));
                    parameters.add(new Parameter("TRANSACTION_EXCEPTION", String.valueOf(exceptionCounts)));

                    for (int i = 0; i < reconciliationTypes.length; i++) {
                        parameters.add(new Parameter(reconciliationTypes[i][1], statusCounts.get(reconciliationTypes[i][1]).toString()));
                    }

                    System.out.println("Invio email");

                    List<Attachment> attachments = new ArrayList<Attachment>(0);

                    String keyFrom = emailSender.getSender();
                    
                    if (stringSubstitution != null) {
                        keyFrom = stringSubstitution.getValue(keyFrom, 1);
                    }
                    
                    System.out.println("keyFrom: " + keyFrom);

                    // L'allegato deve essere inviato solo se � stato consumato almeno un voucher
                    Attachment attachment = new Attachment();
                    attachment.setFileName("report_operazioni_riconciliate.xlsx");
                    //attachment.setContent(attachmentContent);
                    byte[] bytes = ewb.getBytesToStream();
                    attachment.setBytes(bytes);
                    attachments.add(attachment);

                    System.out.println("Sending email to: " + reconciliationRecipient);
                    //String subject = "Eni Pay - report delle transazioni pre-paid riconciliate " + sdfEmail.format(new Date());
                    String subject = "Eni Station + Report delle operazioni riconciliate " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date());
                    String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, keyFrom, reconciliationRecipient, null, null, subject, parameters, attachments);
                    System.out.println("SendEmailResult: " + sendEmailResult);
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                    System.err.println("Errore nell'invio della email: " + ex.getMessage());
                }

            }

            System.out.println("Commit of transaction");
            userTransaction.commit();

        }
        catch (Exception ex) {
            reconciliationSummary.setStatusCode(ResponseHelper.SYSTEM_ERROR);

            try {
                userTransaction.commit();
            }
            catch (Exception ex2) {}
        }

        return reconciliationSummary;
    }

}
