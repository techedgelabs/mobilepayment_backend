package com.techedge.mp.core.actions.reconciliation.status.prepaid;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import com.techedge.mp.core.actions.reconciliation.ReconciliationPaymentFlowType;
import com.techedge.mp.core.actions.reconciliation.status.EventResult;
import com.techedge.mp.core.actions.reconciliation.status.EventResultType;
import com.techedge.mp.core.actions.reconciliation.status.ReconciliationTransactionStatus;
import com.techedge.mp.core.actions.transaction.TransactionRefuelSendNotification;
import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.CRMSfNotifyEvent;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.ParameterBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.PrePaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.RefuelDetailConverter;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.utilities.crm.EVENT_TYPE;
import com.techedge.mp.core.business.utilities.crm.PaymentModeUtil;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityConstants;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.LoadLoyaltyCreditsResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.adapter.business.interfaces.RefuelDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendPaymentTransactionResultResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public abstract class ReconciliationPrePaidTransactionStatus extends ReconciliationTransactionStatus {

    protected GPServiceRemote                          gpService                          = null;
    protected TransactionServiceRemote                 transactionService                 = null;
    protected ForecourtInfoServiceRemote               forecourtService                   = null;
    protected FidelityServiceRemote                    fidelityService                    = null;
    protected CRMServiceRemote                         crmService                         = null;

    protected String                                   secretKey                          = null;
    protected UserCategoryService                      userCategoryService                = null;
    protected StringSubstitution                       stringSubstitution                 = null;
    protected RefuelingNotificationServiceRemote       refuelingNotificationService       = null;
    protected RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2 = null;
    
    private final String 				PARAM_CRM_SF_ACTIVE="CRM_SF_ACTIVE";

    public ReconciliationPrePaidTransactionStatus(EntityManager entityManager, GPServiceRemote gpService, TransactionServiceRemote transactionService, ForecourtInfoServiceRemote forecourtService, 
            FidelityServiceRemote fidelityService, CRMServiceRemote crmService, EmailSenderRemote emailSender, String proxyHost, String proxyPort, String proxyNoHosts, 
            String secretKey, UserCategoryService userCategoryService, StringSubstitution stringSubstitution, RefuelingNotificationServiceRemote refuelingNotificationService, 
            RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2) {

        super(entityManager, emailSender, proxyHost, proxyPort, proxyNoHosts);

        this.gpService = gpService;
        this.transactionService = transactionService;
        this.forecourtService = forecourtService;
        this.fidelityService = fidelityService;
        this.crmService = crmService;

        this.secretKey = secretKey;
        this.userCategoryService = userCategoryService;
        this.stringSubstitution = stringSubstitution;
        this.refuelingNotificationService = refuelingNotificationService;
        this.refuelingNotificationServiceOAuth2 = refuelingNotificationServiceOAuth2;
    }

    protected ReconciliationInfo payTransactionCompletion(TransactionInterfaceBean transactionInterfaceBean, GestPayData gestPayData, Double amount, String initialFinalStatus) {

        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        String errorCode = gestPayData.getErrorCode();
        String errorMessage = gestPayData.getErrorDescription();

        String transactionResult = "";
        if (errorCode.equals("0")) {
            transactionResult = "ok";
        }
        else {
            transactionResult = "ko";

        }

        reconciliationInfo.setFinalStatusType(initialFinalStatus);
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

        if (this.paymentFlowType.equals(ReconciliationPaymentFlowType.FLOW_VOUCHER)) {
            System.out.println("Finalizzazione per nuovo flusso di pagamento");
            reconciliationInfo = finalizeTransactionVoucherFlow((TransactionBean) transactionInterfaceBean, amount, transactionResult,
                    StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_RECONCILE, errorCode, errorMessage);
        }
        else {
            System.out.println("Finalizzazione per vecchio flusso di pagamento");
            //transactionService.persistPaymentCompletionStatus(transactionResult, StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_RECONCILE, errorCode, errorMessage,
            //        transactionInterfaceBean.getTransactionID(), transactionInterfaceBean.getBankTansactionID(), amount);
            finalizeTransactionCreditCard((TransactionBean) transactionInterfaceBean, transactionResult, StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_RECONCILE, errorCode,
                    errorMessage, amount);
        }

        if (errorCode.equals("0")) {

            updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
            reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);

            String stringDebug = (this.paymentFlowType.equals(ReconciliationPaymentFlowType.FLOW_VOUCHER)) ? "consumeVoucher" : "callSettle";
            System.out.println(stringDebug + " OK --> Invia notifica pagamento al GFG");
            gestPayData.setAmount(amount.toString());
            SendPaymentTransactionResultResponse sendPaymentTransactionResultResponse = notifyGFG(transactionInterfaceBean, gestPayData);
            if (sendPaymentTransactionResultResponse.getStatusCode().contains("200")) {
                //updateNotification(transactionInterfaceBean, true);
                TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
                transactionBean.setGFGNotification(true);
                if (sendPaymentTransactionResultResponse.getElectronicInvoiceID() != null && !sendPaymentTransactionResultResponse.getElectronicInvoiceID().trim().isEmpty()) {
                    transactionBean.setGFGElectronicInvoiceID(sendPaymentTransactionResultResponse.getElectronicInvoiceID().trim());
                }
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
            }
            else if (sendPaymentTransactionResultResponse.getStatusCode().contains("500")) {
                decreaseAttempts(transactionInterfaceBean);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            }
            else if (sendPaymentTransactionResultResponse.getStatusCode().contains("400")) {
                updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
            }
        }
        return reconciliationInfo;
    }

    private ReconciliationInfo finalizeTransactionCreditCard(TransactionBean transactionBean, String statusCode, String subStatusCode, String errorCode, String errorMessage,
            Double effectiveAmount) {

        Boolean isNewFlow = false;
        Boolean isNewAcquirerFlow = false;
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();

        if (transactionBean == null) {

            // Transaction not found
            System.err.println("Transaction not found");

            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
            return reconciliationInfo;
        }

        PrePaidLoadLoyaltyCreditsBean crmPrePaidLoadLoyaltyCredits = null;

        // Crea il nuovo stato
        Integer sequenceID = 0;
        Set<TransactionStatusBean> transactionStatusData = transactionBean.getTransactionStatusBeanData();
        for (TransactionStatusBean transactionStatusBean : transactionStatusData) {
            Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
            if (sequenceID < transactionStatusBeanSequenceID) {
                sequenceID = transactionStatusBeanSequenceID;
            }
        }
        sequenceID = sequenceID + 1;

        Date now = new Date();
        Timestamp timestamp = new java.sql.Timestamp(now.getTime());
        String requestID = String.valueOf(now.getTime());

        TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

        transactionStatusBean.setTransactionBean(transactionBean);
        transactionStatusBean.setSequenceID(sequenceID);
        transactionStatusBean.setTimestamp(timestamp);
        transactionStatusBean.setRequestID(requestID);

        Integer eventSequenceID = 0;
        Set<TransactionEventBean> transactionEventData = transactionBean.getTransactionEventBeanData();
        for (TransactionEventBean transactionEventBean : transactionEventData) {
            Integer transactionEventBeanSequenceID = transactionEventBean.getSequenceID();
            if (eventSequenceID < transactionEventBeanSequenceID) {
                eventSequenceID = transactionEventBeanSequenceID;
            }
        }
        eventSequenceID = eventSequenceID + 1;

        TransactionEventBean transactionEventBean = new TransactionEventBean();
        transactionEventBean.setSequenceID(eventSequenceID);
        transactionEventBean.setTransactionBean(transactionBean);
        transactionEventBean.setTransactionResult(statusCode.toUpperCase());
        transactionEventBean.setErrorCode(errorCode);
        transactionEventBean.setErrorDescription(errorMessage);

        if (effectiveAmount == 0.0) {
            if (subStatusCode.equals(StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_CONSUME_VOUCHER_FAULT)) {
                System.out.println(this.getClass().getSimpleName() + " --- " + "effectiveAmount = 0.0  -->  inserita riga MOV con risultato KO con amount "
                        + transactionBean.getInitialAmount());

                transactionEventBean.setEventType("MOV");
                transactionEventBean.setEventAmount(transactionBean.getInitialAmount());
            }
            else {
                transactionEventBean.setEventType("CAN");
                transactionEventBean.setEventAmount(transactionBean.getInitialAmount());
            }
        }
        else {

            System.out.println(this.getClass().getSimpleName() + " --- " + "effectiveAmount > 0.0  -->  inserita riga MOV con amount " + effectiveAmount);

            transactionEventBean.setEventType("MOV");
            transactionEventBean.setEventAmount(effectiveAmount);
        }

        Double totalAmount = effectiveAmount;

        isNewFlow = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.NEW_PAYMENT_FLOW.getCode());
        isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
        if (isNewFlow) {

            System.out.println("Rilevato nuovo flusso di pagamento: totalAmount = effectiveAmount");
        }
        else {

            if (isNewAcquirerFlow && transactionBean.getPaymentMethodType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {

                System.out.println("Rilevato flusso di pagamento con nuovo acquirer: totalAmount = voucher consumati");

                totalAmount = 0.0;
            }
            else {

                System.out.println("Rilevato vecchio flusso di pagamento: totalAmount = effectiveAmount + voucher consumati");
            }

            if (transactionBean.getUseVoucher() && transactionBean.getPrePaidConsumeVoucherBeanList().size() > 0) {
                for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {
                    if (prePaidConsumeVoucherBean.getOperationType().equals("CONSUME") && prePaidConsumeVoucherBean.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {
                        totalAmount = totalAmount + prePaidConsumeVoucherBean.getTotalConsumed();
                    }
                }
            }
        }

        Boolean isRefuelingOAuth2 = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.UTENTI_REFUELING_OAUTH2.getCode());

        if (statusCode.toLowerCase().equals("ok")) {

            // Caricamento punti in modalit� prepaid

            System.out.println("Controllo caricamento punti carta fedelt�");

            LoyaltyCardBean loyaltyCardBean = transactionBean.getUserBean().getVirtualLoyaltyCard();

            if (loyaltyCardBean != null) {

                String refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_PREPAY;

                List<ProductDetail> productList = new ArrayList<ProductDetail>(0);

                ProductDetail productDetail = new ProductDetail();
                productDetail.setAmount(transactionBean.getFinalAmount());

                String productID = transactionBean.getProductID();

                System.out.println("trovato product id: " + productID);

                String productCode = this.getProductCode(productID);
                productDetail.setProductCode(productCode);

                System.out.println("trovato product code: " + productCode);

                productDetail.setQuantity(transactionBean.getFuelQuantity());

                productList.add(productDetail);

                System.out.println("Trovata carta fedelt�");

                // Chimamata al servizio loadLoyaltyCredits

                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                PartnerType partnerType = PartnerType.MP;
                Long requestTimestamp = new Date().getTime();
                String stationID = transactionBean.getStationBean().getStationID();
                String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;

                Long paymentMethodId = transactionBean.getPaymentMethodId();
                String paymentMethodType = transactionBean.getPaymentMethodType();
                String BIN = "";

                if (paymentMethodId != null && paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {

                    PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(entityManager, paymentMethodId, paymentMethodType);

                    if (paymentInfoBean != null) {

                        BIN = paymentInfoBean.getCardBin();

                        if (BIN != null && QueryRepository.findCardBinExists(entityManager, BIN)) {
                            System.out.println("BIN valido (" + BIN + ")");
                            paymentMode = FidelityConstants.PAYMENT_METHOD_ENI_CARD;
                        }
                        else {
                            System.out.println("BIN Non Valido (" + BIN + ")");
                        }
                    }
                    else {
                        System.out.println("paymentInfoBean null");
                    }
                }
                else {
                    System.out.println("paymentMethodId null o pagato con voucher");
                }

                PrePaidLoadLoyaltyCreditsBean prePaidLoadLoyaltyCreditsBean = new PrePaidLoadLoyaltyCreditsBean();
                prePaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                prePaidLoadLoyaltyCreditsBean.setOperationType("LOAD");
                prePaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                prePaidLoadLoyaltyCreditsBean.setEanCode(loyaltyCardBean.getEanCode());

                try {

                    System.out.println("Chiamata servizio di carico punti");

                    LoadLoyaltyCreditsResult loadLoyaltyCreditsResult = fidelityService.loadLoyaltyCredits(operationID, transactionBean.getTransactionID(), stationID,
                            loyaltyCardBean.getPanCode(), BIN, refuelMode, paymentMode, "it", partnerType, requestTimestamp, "", productList);

                    System.out.println("Risposta servizio di carico punti: " + loadLoyaltyCreditsResult.getStatusCode() + " (" + loadLoyaltyCreditsResult.getMessageCode() + ")");

                    prePaidLoadLoyaltyCreditsBean.setBalance(loadLoyaltyCreditsResult.getBalance());
                    prePaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                    prePaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                    prePaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                    prePaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                    prePaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                    prePaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                    prePaidLoadLoyaltyCreditsBean.setCsTransactionID(loadLoyaltyCreditsResult.getCsTransactionID());
                    prePaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                    prePaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                    prePaidLoadLoyaltyCreditsBean.setMessageCode(loadLoyaltyCreditsResult.getMessageCode());
                    prePaidLoadLoyaltyCreditsBean.setStatusCode(loadLoyaltyCreditsResult.getStatusCode());
                    prePaidLoadLoyaltyCreditsBean.setWarningMsg(loadLoyaltyCreditsResult.getWarningMsg());

                    prePaidLoadLoyaltyCreditsBean.setTransactionBean(transactionBean);

                    transactionBean.getPrePaidLoadLoyaltyCreditsBeanList().add(prePaidLoadLoyaltyCreditsBean);

                    crmPrePaidLoadLoyaltyCredits = prePaidLoadLoyaltyCreditsBean;
                    //em.persist(postPaidLoadLoyaltyCreditsBean);
                    //em.merge(poPTransactionBean);

                }
                catch (Exception e) {

                    System.err.println(this.getClass().getSimpleName() + " --- Error loading loyalty credits");

                    prePaidLoadLoyaltyCreditsBean.setStatusCode(StatusHelper.LOYALTY_STATUS_TO_BE_VERIFIED);
                    prePaidLoadLoyaltyCreditsBean.setMessageCode("OPERAZIONE DI CARICAMENTO DA VERIFICARE (" + e.getMessage() + ")");
                    prePaidLoadLoyaltyCreditsBean.setTransactionBean(transactionBean);
                    transactionBean.getPrePaidLoadLoyaltyCreditsBeanList().add(prePaidLoadLoyaltyCreditsBean);
                    transactionBean.setLoyaltyReconcile(true);
                }
            }

            transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_TRANSACTION_CLOSED);
            transactionStatusBean.setSubStatus(subStatusCode);
            transactionStatusBean.setSubStatusDescription("Payment transaction succesful");

            //transactionBean.setFinalAmount(effectiveAmount);
            transactionBean.setFinalAmount(totalAmount);
            transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
            
            Boolean isRefueling = false;
            if (transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_REFUELING) ||
                transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_REFUELING_NEW_ACQUIRER) ||
                transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_REFUELING_OAUTH2)) {
                isRefueling = true;
            }
            
            Boolean isBusiness = userCategoryService.isUserTypeInUserCategory(transactionBean.getUserBean().getUserType(), UserCategoryType.BUSINESS.getCode());
            
            if (!isRefueling && !isBusiness) {
                
                //recupero il CRM_SF_ACTIVE per la nuova implementazione CRM SF
        	    String crmSfActive = null;
        	    List<ParameterBean> parameterBeanList = QueryRepository.findParameterBeanByName(entityManager, PARAM_CRM_SF_ACTIVE);
        	    if (parameterBeanList != null &&  parameterBeanList.size() > 0) {
        	    	crmSfActive = parameterBeanList.get(0).getValue();
        	    }
    
                //chiamata al servizio di notifica per transazioni refueling
    
                final String fiscalCode = transactionBean.getUserBean().getPersonalDataBean().getFiscalCode();
                Date timestamp2 = transactionBean.getCreationTimestamp();
                String surname = transactionBean.getUserBean().getPersonalDataBean().getLastName();
                String pv = transactionBean.getStationBean().getStationID();
                String product = transactionBean.getProductDescription();
                final Long sourceID = transactionBean.getId();
                final CRMEventSourceType sourceType = CRMEventSourceType.PREPAID;
                boolean flagPayment = true;
                Integer loyaltyCredits = 0;
                Double refuelQuantity = transactionBean.getFuelQuantity();
                Double amount = transactionBean.getFinalAmount();
                
                //recupero la modalita di pagamento prepaid
                String paymentMode=PaymentModeUtil.getPaymentModePrepaid(transactionBean);
                
                String refuelMode = transactionBean.getRefuelMode();
                boolean flagNotification = false;
                boolean flagCreditCard = false;
                String cardBrand = null;
                ClusterType cluster = ClusterType.ENIPAY;
                boolean privacyFlag1 = false;
                boolean privacyFlag2 = false;
    
                if (crmPrePaidLoadLoyaltyCredits != null) {
                    loyaltyCredits = crmPrePaidLoadLoyaltyCredits.getCredits();
                }
    
                Set<TermsOfServiceBean> listTermOfService = transactionBean.getUserBean().getPersonalDataBean().getTermsOfServiceBeanData();
                if (listTermOfService == null) {
                    System.err.println(this.getClass().getSimpleName() + " --- flagPrivacy null");
                }
                else {
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("NOTIFICATION_NEW_1")) {
                            flagNotification = item.getAccepted();
                            break;
                        }
                    }
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                            privacyFlag1 = item.getAccepted();
                            break;
                        }
                    }
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                            privacyFlag2 = item.getAccepted();
                            break;
                        }
                    }
                }
    
                if (transactionBean.getUserBean().getPaymentMethodTypeCreditCard() != null) {
                    flagCreditCard = true;
                    cardBrand = transactionBean.getUserBean().getPaymentMethodTypeCreditCard().getBrand();
                }
    
                if (transactionBean.getUserBean().getVirtualizationCompleted()) {
                    cluster = ClusterType.YOUENI;
                }
                
                
                //Nuova implementazione sf
                //recupero parametro CRM_SF_ACTIVE per la gestione della nuova chiamata al CRM SF
                //0 disattivato invio sf
                //1 attivo solo invio sf
                //2 attivi entrambi i crm ibm,sf
               
                if(crmSfActive.equalsIgnoreCase("0")||crmSfActive.equalsIgnoreCase("2")){
    
    	            final UserProfile userProfile = UserProfile.getUserProfileForEventRefueling(fiscalCode, timestamp2, surname, pv, product, flagPayment, loyaltyCredits, refuelQuantity,
    	                    amount, refuelMode, flagNotification, flagCreditCard, cardBrand, cluster);
    	
    	            new Thread(new Runnable() {
    	
    	                @Override
    	                public void run() {
    	                    String crmResponse = crmService.sendNotifyEventOffer(fiscalCode, InteractionPointType.REFUELING, userProfile, sourceID, sourceType);
    	
    	                    if (!crmResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
    	                        System.err.println("Error in send crm notification (" + crmResponse + ")");
    	                    }
    	                }
    	            }, "executeBatchGetOffers (TransactionPersistPaymentCompletionStatusAction)").start();
                }
                
                if(crmSfActive.equalsIgnoreCase("1")||crmSfActive.equalsIgnoreCase("2")){
                	
                	System.out.println("##################################################");
                	System.out.println("fiscalCode:" + fiscalCode);
                	System.out.println("timestamp:" + timestamp);
                	System.out.println("pv:" +pv );
                	System.out.println("product:" + product);
                	System.out.println("flagPayment:" + flagPayment);
                	System.out.println("loyaltyCredits:" +loyaltyCredits );
                	System.out.println("refuelQuantity:" +refuelQuantity );
                	System.out.println("refuelMode:" + refuelMode);
                	System.out.println("surname:" +surname );
                	System.out.println("flagNotification:" + flagNotification);
                	System.out.println("flagCreditCard:" + flagCreditCard);
                	System.out.println("cardBrand:" + cardBrand);
                	System.out.println("cluster.getValue():" +cluster.getValue() );
                	System.out.println("amount:" +amount );
                	System.out.println("paymentMode:" +paymentMode );
                	System.out.println("EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue():" + EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue());
                	System.out.println("##################################################");
                	
                	String reqId = new IdGenerator().generateId(16).substring(0, 32);
                	/*:TODO da verificare requestId */
                	final CRMSfNotifyEvent crmSfNotifyEvent= new CRMSfNotifyEvent(reqId, fiscalCode, timestamp, pv, product, flagPayment, 
                			loyaltyCredits, refuelQuantity, refuelMode, "", surname, "", 
                			null, flagNotification, flagCreditCard, cardBrand==null?"":cardBrand, 
                			cluster.getValue(), amount, privacyFlag1, privacyFlag2, "", "", 
                			EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue(), "", "", paymentMode);
                	
                    new Thread(new Runnable() {
                        
                        @Override
                        public void run() {
                        	NotifyEventResponse crmResponse = crmService.sendNotifySfEventOffer(crmSfNotifyEvent.getRequestId(), crmSfNotifyEvent.getFiscalCode(), crmSfNotifyEvent.getDate(), 
                            		crmSfNotifyEvent.getStationId(), crmSfNotifyEvent.getProductId(), crmSfNotifyEvent.getPaymentFlag(), crmSfNotifyEvent.getCredits(), 
                            		crmSfNotifyEvent.getQuantity(), crmSfNotifyEvent.getRefuelMode(), crmSfNotifyEvent.getFirstName(), crmSfNotifyEvent.getLastName(), 
                            		crmSfNotifyEvent.getEmail(), crmSfNotifyEvent.getBirthDate(), crmSfNotifyEvent.getNotificationFlag(), crmSfNotifyEvent.getPaymentCardFlag(), 
                            		crmSfNotifyEvent.getBrand(), crmSfNotifyEvent.getCluster(),crmSfNotifyEvent.getAmount(), crmSfNotifyEvent.getPrivacyFlag1(), 
                            		crmSfNotifyEvent.getPrivacyFlag2(), crmSfNotifyEvent.getMobilePhone(), crmSfNotifyEvent.getLoyaltyCard(), crmSfNotifyEvent.getEventType(), 
                            		crmSfNotifyEvent.getParameter1(), crmSfNotifyEvent.getParameter2(), crmSfNotifyEvent.getPaymentMode());
        
                        	System.out.println("crmResponse succes:" + crmResponse.getSuccess());
    	                      System.out.println("crmResponse errorCode:" + crmResponse.getErrorCode());
    	                      System.out.println("crmResponse message:" + crmResponse.getMessage());
    	                      System.out.println("crmResponse requestId:" + crmResponse.getRequestId());
                        }
                    }, "executeBatchGetOffersSF (TransactionPersistPaymentCompletionStatusAction)").start();
                }
            }
            

            String srcTransactionID = transactionBean.getSrcTransactionID();
            if (srcTransactionID != null && !srcTransactionID.equals("")) {

                System.out.println("Trovata transazione refueling -> invio notifica");

                com.techedge.mp.refueling.integration.entities.RefuelDetail fuelDeatail = RefuelDetailConverter.createRefuelDetail(transactionBean);

                String status = transactionBean.getLastTransactionStatus().getStatus();

                System.out.println("Status: " + status);

                String mpTransactionStatus = "END_REFUELING";

                System.out.println("Inizio chiamata servizio di notifica per Rifornimento completato con successo");

                Runnable r = new TransactionRefuelSendNotification(requestID, srcTransactionID, transactionBean.getTransactionID(), mpTransactionStatus, fuelDeatail,
                        refuelingNotificationService, refuelingNotificationServiceOAuth2, isRefuelingOAuth2, null);

                new Thread(r).start();
            }
        }
        else {

            // TODO mappare tutti gli stati della risposta GestPay
            transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_TRANSACTION_NOT_CLOSED);
            transactionStatusBean.setSubStatus(subStatusCode);
            transactionStatusBean.setSubStatusDescription("Fault (application error or system error)");

            transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT);

            //chiamata al servizio di notifica per transazioni refueling

            String srcTransactionID = transactionBean.getSrcTransactionID();
            if (srcTransactionID != null && !srcTransactionID.equals("")) {

                System.out.println("Trovata transazione refueling -> invio notifica");

                com.techedge.mp.refueling.integration.entities.RefuelDetail fuelDeatail = RefuelDetailConverter.createRefuelDetail(transactionBean);

                String status = transactionBean.getLastTransactionStatus().getStatus();

                System.out.println("Status: " + status);

                String mpTransactionStatus = "RECONCILIATION";

                System.out.println("Inizio chiamata servizio di notifica per Errore movimentazione importo");

                Runnable r = new TransactionRefuelSendNotification(requestID, srcTransactionID, transactionBean.getTransactionID(), mpTransactionStatus, fuelDeatail,
                        refuelingNotificationService, refuelingNotificationServiceOAuth2, isRefuelingOAuth2, null);

                new Thread(r).start();
            }
        }

        UserBean userBean = transactionBean.getUserBean();

        entityManager.merge(transactionBean);

        entityManager.persist(transactionStatusBean);
        entityManager.persist(transactionEventBean);

        if (userBean.getUserType() == User.USER_TYPE_CUSTOMER || userBean.getUserType() == User.USER_TYPE_VOUCHER_TESTER
                || userBean.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER) {

            // Invio della mail con lo scontrino del rifornimento
            if (emailSenderService != null) {
                Email.sendPrePaidSummary(emailSenderService, transactionBean, transactionStatusBean, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);
            }
        }

        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
        return reconciliationInfo;

    }

    private String getProductCode(String productID) {

        if (productID == null || productID.isEmpty()) {
            return null;
        }

        if (productID.equals("SP")) {
            return FidelityConstants.PRODUCT_CODE_SP;
        }

        if (productID.equals("GG")) {
            return FidelityConstants.PRODUCT_CODE_GASOLIO;
        }

        if (productID.equals("BS")) {
            return FidelityConstants.PRODUCT_CODE_BLUE_SUPER;
        }

        if (productID.equals("BD")) {
            return FidelityConstants.PRODUCT_CODE_BLUE_DIESEL;
        }

        if (productID.equals("MT")) {
            return FidelityConstants.PRODUCT_CODE_METANO;
        }

        if (productID.equals("GP")) {
            return FidelityConstants.PRODUCT_CODE_GPL;
        }

        if (productID.equals("AD")) {
            return null;
        }

        return null;
    }

    private ReconciliationInfo finalizeTransactionVoucherFlow(TransactionBean transactionBean, Double effectiveAmount, String statusCode, String subStatusCode, String errorCode,
            String errorMessage) {

        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        Integer sequenceID = 0;

        if (transactionBean.getLastTransactionStatus() != null) {
            sequenceID = transactionBean.getLastTransactionStatus().getSequenceID() + 1;
        }

        Date now = new Date();
        Timestamp timestamp = new java.sql.Timestamp(now.getTime());
        String requestID = String.valueOf(now.getTime());

        TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

        transactionStatusBean.setTransactionBean(transactionBean);
        transactionStatusBean.setSequenceID(sequenceID);
        transactionStatusBean.setTimestamp(timestamp);
        transactionStatusBean.setRequestID(requestID);

        Integer eventSequenceID = 0;

        if (transactionBean.getLastTransactionEvent() != null) {
            eventSequenceID = transactionBean.getLastTransactionEvent().getSequenceID() + 1;
        }

        TransactionEventBean transactionEventBean = new TransactionEventBean();
        transactionEventBean.setSequenceID(eventSequenceID);
        transactionEventBean.setTransactionBean(transactionBean);
        transactionEventBean.setTransactionResult(statusCode.toUpperCase());
        transactionEventBean.setErrorCode(errorCode);
        transactionEventBean.setErrorDescription(errorMessage);

        if (effectiveAmount == 0.0) {

            System.out.println("effectiveAmount = 0.0  -->  inserita riga CAN con amount " + transactionBean.getInitialAmount());

            transactionEventBean.setEventType("CAN");
            transactionEventBean.setEventAmount(transactionBean.getInitialAmount());
        }
        else {

            System.out.println("effectiveAmount > 0.0  -->  inserita riga MOV con amount " + effectiveAmount);

            transactionEventBean.setEventType("MOV");
            transactionEventBean.setEventAmount(effectiveAmount);
        }

        Double totalAmount = effectiveAmount;
        boolean sendEmail = false;

        if (statusCode.toLowerCase().equals("ok")) {

            transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_TRANSACTION_CLOSED);
            transactionStatusBean.setSubStatus(subStatusCode);
            transactionStatusBean.setSubStatusDescription("Payment transaction succesful");

            //transactionBean.setFinalAmount(effectiveAmount);
            transactionBean.setFinalAmount(totalAmount);
            transactionBean.setVoucherReconciliation(false);
            transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);

            reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);

            sendEmail = true;
            
            Boolean isRefueling = false;
            if (transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_REFUELING) ||
                transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_REFUELING_NEW_ACQUIRER) ||
                transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_REFUELING_OAUTH2)) {
                isRefueling = true;
            }

            if (!transactionBean.getUserBean().getUserType().equals(User.USER_TYPE_GUEST) && !isRefueling) {
            	
            	//recupero il CRM_SF_ACTIVE per la nuova implementazione CRM SF
        	    String crmSfActive = null;
        	    List<ParameterBean> parameterBeanList = QueryRepository.findParameterBeanByName(entityManager, PARAM_CRM_SF_ACTIVE);
        	    if (parameterBeanList != null &&  parameterBeanList.size() > 0) {
        	    	crmSfActive = parameterBeanList.get(0).getValue();
        	    }

                String fiscalCode = transactionBean.getUserBean().getPersonalDataBean().getFiscalCode();
                Date timestamp2 = transactionBean.getCreationTimestamp();
                String surname = transactionBean.getUserBean().getPersonalDataBean().getLastName();
                String pv = transactionBean.getStationBean().getStationID();
                String product = transactionBean.getProductDescription();
                Long sourceID = transactionBean.getId();
                CRMEventSourceType sourceType = CRMEventSourceType.PREPAID;
                boolean flagPayment = true;
                Integer loyaltyCredits = 0;
                Double refuelQuantity = transactionBean.getFuelQuantity();
                Double amount = transactionBean.getFinalAmount();
                //recupero la modalita di pagamento prepaid
                String paymentMode=PaymentModeUtil.getPaymentModePrepaid(transactionBean);
                String refuelMode = transactionBean.getRefuelMode();
                boolean flagNotification = false;
                boolean flagCreditCard = false;
                String cardBrand = null;
                ClusterType cluster = ClusterType.ENIPAY;
                boolean privacyFlag1 = false;
                boolean privacyFlag2 = false;

                Set<TermsOfServiceBean> listTermOfService = transactionBean.getUserBean().getPersonalDataBean().getTermsOfServiceBeanData();
                if (listTermOfService == null) {
                    System.err.println("flagPrivacy null");
                }
                else {
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("NOTIFICATION_NEW_1")) {
                            flagNotification = item.getAccepted();
                            break;
                        }
                    }
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                            privacyFlag1 = item.getAccepted();
                            break;
                        }
                    }
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                            privacyFlag2 = item.getAccepted();
                            break;
                        }
                    }
                }

                if (transactionBean.getUserBean().getPaymentMethodTypeCreditCard() != null) {
                    flagCreditCard = true;
                    cardBrand = transactionBean.getUserBean().getPaymentMethodTypeCreditCard().getBrand();
                }

                if (transactionBean.getUserBean().getVirtualizationCompleted()) {
                    cluster = ClusterType.YOUENI;
                }

                //Nuova implementazione sf
                //recupero parametro CRM_SF_ACTIVE per la gestione della nuova chiamata al CRM SF
                //0 disattivato invio sf
                //1 attivo solo invio sf
                //2 attivi entrambi i crm ibm,sf
               
                if(crmSfActive.equalsIgnoreCase("0")||crmSfActive.equalsIgnoreCase("2")){
	                UserProfile userProfile = UserProfile.getUserProfileForEventRefueling(fiscalCode, timestamp2, surname, pv, product, flagPayment, loyaltyCredits, refuelQuantity,
	                        amount, refuelMode, flagNotification, flagCreditCard, cardBrand, cluster);
	
	                String crmResponse = crmService.sendNotifyEventOffer(fiscalCode, InteractionPointType.REFUELING, userProfile, sourceID, sourceType);
	                
	                if (!crmResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
	                    System.err.println("Error in send crm notification (" + crmResponse + ")");
	                }
                }
                if(crmSfActive.equalsIgnoreCase("1")||crmSfActive.equalsIgnoreCase("2")){
                	
                	System.out.println("##################################################");
                	System.out.println("fiscalCode:" + fiscalCode);
                	System.out.println("timestamp:" + timestamp);
                	System.out.println("pv:" +pv );
                	System.out.println("product:" + product);
                	System.out.println("flagPayment:" + flagPayment);
                	System.out.println("loyaltyCredits:" +loyaltyCredits );
                	System.out.println("refuelQuantity:" +refuelQuantity );
                	System.out.println("refuelMode:" + refuelMode);
                	System.out.println("surname:" +surname );
                	System.out.println("flagNotification:" + flagNotification);
                	System.out.println("flagCreditCard:" + flagCreditCard);
                	System.out.println("cardBrand:" + cardBrand);
                	System.out.println("cluster.getValue():" +cluster.getValue() );
                	System.out.println("amount:" +amount );
                	System.out.println("paymentMode:" +paymentMode );
                	System.out.println("EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue():" + EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue());
                	System.out.println("##################################################");
                	
                	String requestId = new IdGenerator().generateId(16).substring(0, 32);
                	/*:TODO da verificare requestId */
                	final CRMSfNotifyEvent crmSfNotifyEvent= new CRMSfNotifyEvent(requestId, fiscalCode, timestamp, pv, product, flagPayment, 
                			loyaltyCredits, refuelQuantity, refuelMode, "", surname, "", 
                			null, flagNotification, flagCreditCard, cardBrand==null?"":cardBrand, 
                			cluster.getValue(), amount, privacyFlag1, privacyFlag2, "", "", 
                			EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue(), "", "", paymentMode);
                	
	                new Thread(new Runnable() {
	                    
	                    @Override
	                    public void run() {
	                    	NotifyEventResponse crmResponse = crmService.sendNotifySfEventOffer(crmSfNotifyEvent.getRequestId(), crmSfNotifyEvent.getFiscalCode(), crmSfNotifyEvent.getDate(), 
	                        		crmSfNotifyEvent.getStationId(), crmSfNotifyEvent.getProductId(), crmSfNotifyEvent.getPaymentFlag(), crmSfNotifyEvent.getCredits(), 
	                        		crmSfNotifyEvent.getQuantity(), crmSfNotifyEvent.getRefuelMode(), crmSfNotifyEvent.getFirstName(), crmSfNotifyEvent.getLastName(), 
	                        		crmSfNotifyEvent.getEmail(), crmSfNotifyEvent.getBirthDate(), crmSfNotifyEvent.getNotificationFlag(), crmSfNotifyEvent.getPaymentCardFlag(), 
	                        		crmSfNotifyEvent.getBrand(), crmSfNotifyEvent.getCluster(),crmSfNotifyEvent.getAmount(), crmSfNotifyEvent.getPrivacyFlag1(), 
	                        		crmSfNotifyEvent.getPrivacyFlag2(), crmSfNotifyEvent.getMobilePhone(), crmSfNotifyEvent.getLoyaltyCard(), crmSfNotifyEvent.getEventType(), 
	                        		crmSfNotifyEvent.getParameter1(), crmSfNotifyEvent.getParameter2(), crmSfNotifyEvent.getPaymentMode());
	    
	                    	System.out.println("crmResponse succes:" + crmResponse.getSuccess());
		                      System.out.println("crmResponse errorCode:" + crmResponse.getErrorCode());
		                      System.out.println("crmResponse message:" + crmResponse.getMessage());
		                      System.out.println("crmResponse requestId:" + crmResponse.getRequestId());
	                    }
	                }, "executeBatchGetOffersSF (ReconciliationPrePaidTransactionStatus)").start();
                }
                

               
            }
            else {
                System.out.println("Notifica di rifornimento non inviata al CRM per utente guest");
            }
        }
        else {

            // TODO mappare tutti gli stati della risposta GestPay
            transactionStatusBean.setStatus(StatusHelper.STATUS_PAYMENT_TRANSACTION_NOT_CLOSED);
            transactionStatusBean.setSubStatus(subStatusCode);
            transactionStatusBean.setSubStatusDescription("Fault (application error or system error)");

            transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);

            reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
        }

        UserBean userBean = transactionBean.getUserBean();

        entityManager.merge(transactionBean);

        entityManager.persist(transactionStatusBean);
        entityManager.persist(transactionEventBean);

        if (userBean.getUserType() == User.USER_TYPE_CUSTOMER || userBean.getUserType() == User.USER_TYPE_VOUCHER_TESTER
                || userBean.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER) {

            // Invio della mail con lo scontrino del rifornimento
            if (sendEmail && emailSenderService != null) {
                Email.sendPrePaidSummary(emailSenderService, transactionBean, transactionStatusBean, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);
            }
        }

        return reconciliationInfo;
    }

    protected ReconciliationInfo deleteTransactionCompletion(TransactionInterfaceBean transactionInterfaceBean, GestPayData gestPayData, boolean sendNotification,
            String initialFinalStatus) {

        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        String errorCode = gestPayData.getErrorCode();
        String errorMessage = gestPayData.getErrorDescription();

        String transactionResult = "";
        if (errorCode.equals("0")) {
            transactionResult = "ok";
        }
        else {
            transactionResult = "ko";

        }

        reconciliationInfo.setFinalStatusType(initialFinalStatus);
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

        String response = transactionService.persistPaymentDeletionStatus(transactionResult, StatusHelper.SUBSTATUS_PAYMENT_TRANSACTION_RECONCILE, errorCode, errorMessage,
                transactionInterfaceBean.getTransactionID(), transactionInterfaceBean.getBankTansactionID());

        System.out.println("persistPaymentDeletionStatus response: " + response);

        if (response.equals(StatusHelper.PERSIST_PAYMENT_DELETION_STATUS_SUCCESS)) {

            updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
            reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_FAILED);

            if (sendNotification) {
                String stringDebug = (this.paymentFlowType.equals(ReconciliationPaymentFlowType.FLOW_VOUCHER)) ? "cancelPreAuthorizationConsumeVoucher" : "deletePagam";
                System.out.println(stringDebug + " OK --> Invia notifica pagamento al GFG (amount: " + gestPayData.getAmount() + ")");
                SendPaymentTransactionResultResponse sendPaymentTransactionResultResponse = notifyGFG(transactionInterfaceBean, gestPayData);
                if (sendPaymentTransactionResultResponse.getStatusCode().contains("200")) {
                    updateNotification(transactionInterfaceBean, true);
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                }
                else if (sendPaymentTransactionResultResponse.getStatusCode().contains("500")) {
                    decreaseAttempts(transactionInterfaceBean);
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                }
                else if (sendPaymentTransactionResultResponse.getStatusCode().contains("400")) {
                    updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                    reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                }
            }
            else {
                updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_FAILED);
                reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_FAILED);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
            }
        }
        return reconciliationInfo;
    }

    public SendPaymentTransactionResultResponse notifyGFG(TransactionInterfaceBean transactionInterfaceBean, GestPayData gestPayData) {

        SendPaymentTransactionResultResponse sendPaymentTransactionResult = new SendPaymentTransactionResultResponse();

        PaymentTransactionResult paymentTransactionResult = new PaymentTransactionResult();

        paymentTransactionResult.setAuthorizationCode(gestPayData.getAuthorizationCode());
        paymentTransactionResult.setBankTransactionID(gestPayData.getBankTransactionID());
        paymentTransactionResult.setEventType(gestPayData.getTransactionType());
        paymentTransactionResult.setErrorDescription(gestPayData.getErrorDescription());
        paymentTransactionResult.setErrorCode(gestPayData.getErrorCode());
        paymentTransactionResult.setTransactionResult(gestPayData.getTransactionResult());
        Double amount = Double.valueOf(gestPayData.getAmount());

        Date now = new Date();
        String requestID = String.valueOf(now.getTime());

        sendPaymentTransactionResult = forecourtService.sendPaymentTransactionResult(requestID, transactionInterfaceBean.getTransactionID(), amount, paymentTransactionResult);

        System.out.println("Chimata al Forecourt di notifica: " + sendPaymentTransactionResult.getStatusCode());

        return sendPaymentTransactionResult;
    }

    public boolean checkAttemptsLeft(TransactionInterfaceBean transactionInterfaceBean) {
        boolean check = true;

        if (transactionInterfaceBean.getReconciliationAttemptsLeft() <= 0) {
            updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);

            check = false;
        }

        return check;
    }

    public int decreaseAttempts(TransactionInterfaceBean transactionInterfaceBean) {

        System.out.println("decreaseAttempts");

        int attempts = transactionInterfaceBean.getReconciliationAttemptsLeft().intValue();
        int returnAttempts;

        if (attempts < 1)
            return -1;

        //returnAttempts = transactionService.updateReconciliationAttempts(transactionBean.getTransactionID(), (attempts - 1));
        transactionInterfaceBean.setReconciliationAttemptsLeft((attempts - 1));
        entityManager.merge(transactionInterfaceBean);
        returnAttempts = (attempts - 1);

        System.out.println("Decremento dei tentativi di riconciliazione: " + attempts + " -> " + returnAttempts);

        return returnAttempts;
    }

    public void updateFinalStatus(TransactionInterfaceBean transactionInterfaceBean, String finalStatus) {
        transactionInterfaceBean.setTransactionStatus(finalStatus);
        entityManager.merge(transactionInterfaceBean);

    }

    protected void updateAmount(TransactionInterfaceBean transactionInterfaceBean, Double amount) {
        transactionInterfaceBean.setAmount(amount);
        entityManager.merge(transactionInterfaceBean);
    }

    protected void updateNotification(TransactionInterfaceBean transactionInterfaceBean, boolean notification) {
        TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
        transactionBean.setGFGNotification(notification);
        entityManager.merge(transactionBean);
    }

    protected void updateTransactionRefuelDetail(TransactionBean transactionBean, RefuelDetail refuelDetail) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        if (refuelDetail == null) {
            System.out.println("RefuelDetail null aggiornamento dati transazione non eseguito!");
            return;
        }

        try {
            if (transactionBean.getEndRefuelTimestamp() == null && refuelDetail.getTimestampEndRefuel() != null) {
                transactionBean.setEndRefuelTimestamp(sdf.parse(refuelDetail.getTimestampEndRefuel()));
            }
        }
        catch (ParseException ex) {
            System.out.println("Errore nel parsing del timestamp del end refuel: " + ex.getMessage());
        }

        if (transactionBean.getFuelQuantity() == null && refuelDetail.getFuelQuantity() != null) {
            transactionBean.setFuelQuantity(new Double(refuelDetail.getFuelQuantity()));
        }

        if (transactionBean.getFuelAmount() == null && refuelDetail.getAmount() != null) {
            transactionBean.setFuelAmount(refuelDetail.getAmount());
        }

        if (transactionBean.getProductDescription() == null && refuelDetail.getProductDescription() != null) {
            transactionBean.setProductDescription(refuelDetail.getProductDescription());
        }

        if (transactionBean.getProductID() == null && refuelDetail.getProductID() != null) {
            transactionBean.setProductID(refuelDetail.getProductID());
        }
        
        if (transactionBean.getUnitPrice() == null && refuelDetail.getUnitPrice() != null) {
            transactionBean.setUnitPrice(refuelDetail.getUnitPrice());
        }

        entityManager.merge(transactionBean);
    }

    protected EventResult verifyVoucherTransaction(TransactionBean transactionBean) {

        PartnerType partnerType = PartnerType.MP;
        Double totalAmount = 0.0;
        EventResult eventResult = new EventResult(EventResultType.SUCCESS);
        List<String> reversedOperationId = new ArrayList<String>();

        try {

            for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {

                if (prePaidConsumeVoucherBean.getOperationType().equals("REVERSE")) {
                    reversedOperationId.add(prePaidConsumeVoucherBean.getOperationIDReversed());
                }
            }
            System.out.println("Trovate " + reversedOperationId.size() + " transazioni stornate");

            for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {

                if (!prePaidConsumeVoucherBean.getOperationType().equals("CONSUME")) {
                    continue;
                }

                if (reversedOperationId.contains(prePaidConsumeVoucherBean.getOperationID())) {
                    System.out.println("OperationId gi� stornata: " + prePaidConsumeVoucherBean.getOperationID());
                    continue;
                }

                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                Long requestTimestamp = new Date().getTime();
                String operationIDtoCheck = prePaidConsumeVoucherBean.getOperationID();
                Double amount = 0.0;

                System.out.println("Controllo consumo voucher transazione " + transactionBean.getTransactionID());
                CheckConsumeVoucherTransactionResult checkConsumeVoucherTransactionResult = fidelityService.checkConsumeVoucherTransaction(operationID, operationIDtoCheck,
                        partnerType, requestTimestamp);

                List<VoucherDetail> voucherDetailList = new ArrayList<VoucherDetail>();

                if (checkConsumeVoucherTransactionResult.getStatusCode().equals(FidelityResponse.CHECK_VOUCHER_GENERIC_ERROR)) {
                    System.err.println("Errore nella chiamata al servizio checkConsumeVoucherTransaction: " + checkConsumeVoucherTransactionResult.getMessageCode() + " ("
                            + checkConsumeVoucherTransactionResult.getStatusCode() + ")");
                    eventResult.setResult(EventResultType.TO_RETRY);
                    return eventResult;
                }

                if (checkConsumeVoucherTransactionResult.getVoucherList() != null) {
                    voucherDetailList = checkConsumeVoucherTransactionResult.getVoucherList();
                }

                for (VoucherDetail voucherDetail : voucherDetailList) {
                    if (voucherDetail.getConsumedValue().doubleValue() > 0.0) {
                        System.out.println("Trovato voucher consumato: " + voucherDetail.getVoucherCode());
                        amount += voucherDetail.getConsumedValue().doubleValue();

                        Set<PrePaidConsumeVoucherDetailBean> prePaidConsumeVoucherDetailBeanList = prePaidConsumeVoucherBean.getPrePaidConsumeVoucherDetailBean();

                        PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBeanFound = null;

                        if (prePaidConsumeVoucherDetailBeanList != null && !prePaidConsumeVoucherDetailBeanList.isEmpty()) {
                            for (PrePaidConsumeVoucherDetailBean PrePaidConsumeVoucherDetailBean : prePaidConsumeVoucherDetailBeanList) {
                                if (PrePaidConsumeVoucherDetailBean.getVoucherCode().equals(voucherDetail.getVoucherCode())) {
                                    prePaidConsumeVoucherDetailBeanFound = PrePaidConsumeVoucherDetailBean;
                                    break;
                                }
                            }
                        }

                        if (prePaidConsumeVoucherDetailBeanFound == null) {

                            System.out.println("Dettaglio consumo voucher non trovato -> creazione");

                            PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean = new PrePaidConsumeVoucherDetailBean();

                            prePaidConsumeVoucherDetailBean.setConsumedValue(voucherDetail.getConsumedValue());
                            prePaidConsumeVoucherDetailBean.setExpirationDate(voucherDetail.getExpirationDate());
                            prePaidConsumeVoucherDetailBean.setInitialValue(voucherDetail.getInitialValue());
                            prePaidConsumeVoucherDetailBean.setPromoCode(voucherDetail.getPromoCode());
                            prePaidConsumeVoucherDetailBean.setPromoDescription(voucherDetail.getPromoDescription());
                            prePaidConsumeVoucherDetailBean.setPromoDoc(voucherDetail.getPromoDoc());
                            prePaidConsumeVoucherDetailBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                            prePaidConsumeVoucherDetailBean.setVoucherCode(voucherDetail.getVoucherCode());
                            prePaidConsumeVoucherDetailBean.setVoucherStatus(voucherDetail.getVoucherStatus());
                            prePaidConsumeVoucherDetailBean.setVoucherType(voucherDetail.getVoucherType());
                            prePaidConsumeVoucherDetailBean.setVoucherValue(voucherDetail.getVoucherValue());
                            prePaidConsumeVoucherDetailBean.setPrePaidConsumeVoucherBean(prePaidConsumeVoucherBean);

                            entityManager.persist(prePaidConsumeVoucherDetailBean);

                            prePaidConsumeVoucherBean.getPrePaidConsumeVoucherDetailBean().add(prePaidConsumeVoucherDetailBean);
                        }
                        else {

                            System.out.println("Dettaglio consumo voucher trovato -> aggiornamento");

                            prePaidConsumeVoucherDetailBeanFound.setConsumedValue(voucherDetail.getConsumedValue());
                            prePaidConsumeVoucherDetailBeanFound.setExpirationDate(voucherDetail.getExpirationDate());
                            prePaidConsumeVoucherDetailBeanFound.setInitialValue(voucherDetail.getInitialValue());
                            prePaidConsumeVoucherDetailBeanFound.setPromoCode(voucherDetail.getPromoCode());
                            prePaidConsumeVoucherDetailBeanFound.setPromoDescription(voucherDetail.getPromoDescription());
                            prePaidConsumeVoucherDetailBeanFound.setPromoDoc(voucherDetail.getPromoDoc());
                            prePaidConsumeVoucherDetailBeanFound.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                            prePaidConsumeVoucherDetailBeanFound.setVoucherCode(voucherDetail.getVoucherCode());
                            prePaidConsumeVoucherDetailBeanFound.setVoucherStatus(voucherDetail.getVoucherStatus());
                            prePaidConsumeVoucherDetailBeanFound.setVoucherType(voucherDetail.getVoucherType());
                            prePaidConsumeVoucherDetailBeanFound.setVoucherValue(voucherDetail.getVoucherValue());
                            prePaidConsumeVoucherDetailBeanFound.setPrePaidConsumeVoucherBean(prePaidConsumeVoucherBean);

                            entityManager.merge(prePaidConsumeVoucherDetailBeanFound);
                        }

                        // Aggiorna le informazioni sul voucher associato all'utente

                        System.out.println("Aggiornamento voucher utente");

                        for (VoucherBean voucherBean : transactionBean.getUserBean().getVoucherList()) {

                            if (voucherBean.getCode().equals(voucherDetail.getVoucherCode())) {

                                System.out.println("Aggiornamento voucher " + voucherDetail.getVoucherCode());

                                voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                                voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                                voucherBean.setInitialValue(voucherDetail.getInitialValue());
                                voucherBean.setPromoCode(voucherDetail.getPromoCode());
                                voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                                voucherBean.setCode(voucherDetail.getVoucherCode());
                                voucherBean.setStatus(voucherDetail.getVoucherStatus());
                                voucherBean.setType(voucherDetail.getVoucherType());
                                voucherBean.setValue(voucherDetail.getVoucherValue());
                            }
                        }

                        entityManager.merge(transactionBean.getUserBean());
                    }
                }

                if (amount > 0.0) {
                    prePaidConsumeVoucherBean.setMessageCode("TRANSAZIONE ESEGUITA");
                    prePaidConsumeVoucherBean.setStatusCode(FidelityResponse.CONSUME_VOUCHER_OK);
                    prePaidConsumeVoucherBean.setTotalConsumed(amount);
                    prePaidConsumeVoucherBean.setCsTransactionID(checkConsumeVoucherTransactionResult.getCsTransactionID());

                    entityManager.merge(prePaidConsumeVoucherBean);
                }

                totalAmount += amount;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Exception checkConsumeVoucherTransaction: " + ex.getMessage());
            eventResult.setResult(EventResultType.TO_RETRY);
        }

        eventResult.setAmount(totalAmount);
        return eventResult;
    }

    protected EventResult voucherTransaction(TransactionBean transactionBean, Double amount) {
        EventResult eventResult = new EventResult(EventResultType.CANCELLED);

        if (amount == null || amount <= 0.0) {
            return eventResult;
        }

        UserBean userBean = transactionBean.getUserBean();
        PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(transactionBean.getPaymentMethodId(), transactionBean.getPaymentMethodType());

        System.out.println("Pagamento con voucher");

        Boolean noVoucher = true;
        String operationID = new IdGenerator().generateId(16).substring(0, 33);
        PartnerType partnerType = PartnerType.MP;
        VoucherConsumerType voucherType = VoucherConsumerType.ENI;
        Long requestTimestamp = new Date().getTime();
        String stationID = transactionBean.getStationBean().getStationID();
        String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;
        String BIN = paymentInfoBean.getCardBin();
        Integer maxVoucherCount = 200;
        Integer voucherCount = 0;
        List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);
        String refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_PREPAY;
        String language = FidelityConstants.LANGUAGE_ITALIAN;

        if (BIN != null && QueryRepository.findCardBinExists(entityManager, BIN)) {
            System.out.println("BIN valido (" + BIN + ")");
            paymentMode = FidelityConstants.PAYMENT_METHOD_ENI_CARD;
        }
        else {
            System.out.println("BIN Non Valido (" + BIN + ")");
        }

        for (VoucherBean voucherBean : userBean.getVoucherList()) {
            if (voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO)) {

                System.out.println("Voucher inserito in richiesta");

                VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                voucherCodeDetail.setVoucherCode(voucherBean.getCode());
                voucherCodeList.add(voucherCodeDetail);

                noVoucher = false;

                voucherCount++;
            }

            if (voucherCount >= maxVoucherCount) {
                break;
            }
        }

        if (noVoucher == true) {
            System.out.println("No voucher found");
            return eventResult;
        }

        List<ProductDetail> productList = getProductList(transactionBean, amount);

        ConsumeVoucherResult consumeVoucherResult = new ConsumeVoucherResult();

        Double totalConsumed = 0.0;
        PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = new PrePaidConsumeVoucherBean();
        prePaidConsumeVoucherBean.setOperationID(operationID);
        prePaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
        prePaidConsumeVoucherBean.setOperationType("CONSUME");
        prePaidConsumeVoucherBean.setTransactionBean(transactionBean);

        try {

            consumeVoucherResult = fidelityService.consumeVoucher(operationID, transactionBean.getTransactionID(), voucherType, stationID, refuelMode, paymentMode, language,
                    partnerType, requestTimestamp, productList, voucherCodeList, FidelityConstants.CONSUME_TYPE_PARTIAL, null);

            prePaidConsumeVoucherBean.setCsTransactionID(consumeVoucherResult.getCsTransactionID());
            prePaidConsumeVoucherBean.setMarketingMsg(consumeVoucherResult.getMarketingMsg());
            prePaidConsumeVoucherBean.setMessageCode(consumeVoucherResult.getMessageCode());
            prePaidConsumeVoucherBean.setStatusCode(consumeVoucherResult.getStatusCode());
            prePaidConsumeVoucherBean.setWarningMsg(consumeVoucherResult.getWarningMsg());
            entityManager.persist(prePaidConsumeVoucherBean);

            for (VoucherDetail voucherDetail : consumeVoucherResult.getVoucherList()) {

                if (voucherDetail.getConsumedValue() == 0.0) {

                    System.out.println(voucherDetail.getVoucherCode() + ": Valore consumato 0.0 -> aggiornamento non necessario");
                    continue;
                }

                PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean = new PrePaidConsumeVoucherDetailBean();

                prePaidConsumeVoucherDetailBean.setConsumedValue(voucherDetail.getConsumedValue());
                prePaidConsumeVoucherDetailBean.setExpirationDate(voucherDetail.getExpirationDate());
                prePaidConsumeVoucherDetailBean.setInitialValue(voucherDetail.getInitialValue());
                prePaidConsumeVoucherDetailBean.setPromoCode(voucherDetail.getPromoCode());
                prePaidConsumeVoucherDetailBean.setPromoDescription(voucherDetail.getPromoDescription());
                prePaidConsumeVoucherDetailBean.setPromoDoc(voucherDetail.getPromoDoc());
                prePaidConsumeVoucherDetailBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                prePaidConsumeVoucherDetailBean.setVoucherCode(voucherDetail.getVoucherCode());
                prePaidConsumeVoucherDetailBean.setVoucherStatus(voucherDetail.getVoucherStatus());
                prePaidConsumeVoucherDetailBean.setVoucherType(voucherDetail.getVoucherType());
                prePaidConsumeVoucherDetailBean.setVoucherValue(voucherDetail.getVoucherValue());
                prePaidConsumeVoucherDetailBean.setPrePaidConsumeVoucherBean(prePaidConsumeVoucherBean);
                entityManager.persist(prePaidConsumeVoucherDetailBean);

                prePaidConsumeVoucherBean.getPrePaidConsumeVoucherDetailBean().add(prePaidConsumeVoucherDetailBean);

                totalConsumed = totalConsumed + voucherDetail.getConsumedValue();

                // Aggiorna le informazioni sul voucher associato all'utente

                System.out.println("Aggiornamento voucher utente");

                for (VoucherBean voucherBean : userBean.getVoucherList()) {

                    if (voucherBean.getCode().equals(voucherDetail.getVoucherCode())) {

                        System.out.println("Aggiornamento voucher " + voucherDetail.getVoucherCode());

                        voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                        voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                        voucherBean.setInitialValue(voucherDetail.getInitialValue());
                        voucherBean.setPromoCode(voucherDetail.getPromoCode());
                        voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                        voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                        voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                        voucherBean.setCode(voucherDetail.getVoucherCode());
                        voucherBean.setStatus(voucherDetail.getVoucherStatus());
                        voucherBean.setType(voucherDetail.getVoucherType());
                        voucherBean.setValue(voucherDetail.getVoucherValue());
                    }
                }
            }

            // Aggiornamento dei dati utente
            entityManager.merge(userBean);

            prePaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
            transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);

            System.out.println("Totale pagato con voucher: " + totalConsumed);

            if (totalConsumed > 0) {
                System.out.println("Pagamento con voucher: importo " + totalConsumed);
                transactionBean.setVoucherStatus(StatusHelper.VOUCHER_STATUS_OK);

                eventResult.setResult(EventResultType.SUCCESS);
                eventResult.setAmount(totalConsumed);
            }
            else {
                System.out.println("Pagamento con voucher: importo nullo");
                transactionBean.setVoucherStatus(StatusHelper.VOUCHER_STATUS_NOT_APPLICABLE);

                eventResult.setResult(EventResultType.CANCELLED);
                eventResult.setAmount(totalConsumed);
            }

            entityManager.persist(prePaidConsumeVoucherBean);
            entityManager.merge(transactionBean);
        }
        catch (Exception e) {
            System.err.println("Error consuming vouchers: " + e.getMessage());

            prePaidConsumeVoucherBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
            prePaidConsumeVoucherBean.setMessageCode("STORNO DA VERIFICARE (" + e.getMessage() + ")");
            prePaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
            prePaidConsumeVoucherBean.setTransactionBean(transactionBean);
            transactionBean.setVoucherReconciliation(true);
            transactionBean.setVoucherStatus(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
            transactionBean.getPrePaidConsumeVoucherBeanList().add(prePaidConsumeVoucherBean);

            entityManager.persist(prePaidConsumeVoucherBean);
            entityManager.merge(transactionBean);

            eventResult.setResult(EventResultType.FAILURE);
            eventResult.setErrorCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
            eventResult.setAmount(0.0);
        }

        return eventResult;
    }

    private List<ProductDetail> getProductList(TransactionBean transactionBean, Double amount) {
        List<ProductDetail> productList = new ArrayList<ProductDetail>(0);

        ProductDetail productDetail = new ProductDetail();
        productDetail.setAmount(amount);

        if (transactionBean.getProductID() == null) {
            productDetail.setProductCode(null);
        }
        else {
            if (transactionBean.getProductID().equals("SP")) {

                // sp
                productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_SP);
            }
            else {

                if (transactionBean.getProductID().equals("GG")) {

                    // gasolio
                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GASOLIO);
                }
                else {

                    if (transactionBean.getProductID().equals("BS")) {

                        // blue_super
                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_SUPER);
                    }
                    else {

                        if (transactionBean.getProductID().equals("BD")) {

                            // blue_diesel
                            productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_DIESEL);
                        }
                        else {

                            if (transactionBean.getProductID().equals("MT")) {

                                // metano
                                productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_METANO);
                            }
                            else {

                                if (transactionBean.getProductID().equals("GP")) {

                                    // gpl
                                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GPL);
                                }
                                else {

                                    if (transactionBean.getProductID().equals("AD")) {

                                        // ???
                                        productDetail.setProductCode(null);
                                    }
                                    else {

                                        // non_oil
                                        productDetail.setProductCode(null);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        productDetail.setQuantity(transactionBean.getFuelQuantity());
        productList.add(productDetail);

        return productList;

    }

}
