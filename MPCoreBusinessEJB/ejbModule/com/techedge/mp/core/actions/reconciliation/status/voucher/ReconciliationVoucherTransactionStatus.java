package com.techedge.mp.core.actions.reconciliation.status.voucher;

import javax.persistence.EntityManager;

import com.techedge.mp.core.actions.reconciliation.status.ReconciliationTransactionStatus;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;


public abstract class ReconciliationVoucherTransactionStatus extends ReconciliationTransactionStatus  {

    protected GPServiceRemote            gpService          = null;
    protected FidelityServiceRemote      fidelityService    = null;
    protected Integer sequenceID = 0;
    protected String newStatus = null;
    protected String oldStatus = null;

    
    public ReconciliationVoucherTransactionStatus(EntityManager entityManager, GPServiceRemote gpService, FidelityServiceRemote fidelityService, 
            EmailSenderRemote emailSenderService, String proxyHost, String proxyPort, String proxyNoHosts) {
        
        super(entityManager, emailSenderService, proxyHost, proxyPort, proxyNoHosts);
        
        this.gpService = gpService;
        this.fidelityService = fidelityService;
    }

    @Override
    public boolean checkAttemptsLeft(TransactionInterfaceBean transactionInterfaceBean) {
        boolean check = true;
        
        if (transactionInterfaceBean.getReconciliationAttemptsLeft() <= 0) {
            updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);

            check = false;
        }
        
        
        return check;
    }
    
    public void updateFinalStatus(TransactionInterfaceBean transactionInterfaceBean, String finalStatus) {
        transactionInterfaceBean.setTransactionStatus(finalStatus);
        entityManager.merge(transactionInterfaceBean);

    }
    
    
}
