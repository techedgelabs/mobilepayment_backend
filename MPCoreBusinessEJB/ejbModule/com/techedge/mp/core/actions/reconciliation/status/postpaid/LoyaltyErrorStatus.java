package com.techedge.mp.core.actions.reconciliation.status.postpaid;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.actions.reconciliation.status.EventResult;
import com.techedge.mp.core.actions.reconciliation.status.EventResultType;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetSrcTransactionStatusMessageResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

public class LoyaltyErrorStatus extends ReconciliationPostPaidTransactionStatus {

    public LoyaltyErrorStatus(EntityManager entityManager, GPServiceRemote gpService, ForecourtPostPaidServiceRemote forecourtPPService, FidelityServiceRemote fidelityService, 
            EmailSenderRemote emailSenderService, String proxyHost, String proxyPort, String proxyNoHosts, String secretKey, 
            UserCategoryService userCategoryService, StringSubstitution stringSubstitution) {

        super(entityManager, gpService, forecourtPPService, fidelityService, emailSenderService, proxyHost, proxyPort, proxyNoHosts, secretKey, userCategoryService, stringSubstitution);
    }

    @Override
    public ReconciliationInfo reconciliateCreditCardFlow(TransactionInterfaceBean transactionInterfaceBean) {
        /*
        PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
        String requestID = String.valueOf(new Date().getTime());
        sequenceID = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getSequenceID() + 1;
        
        newStatus = StatusHelper.POST_PAID_STATUS_RECONCILIATION_REQU;
        oldStatus = null;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_RECONCILIATION;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        PostPaidTransactionEventBean postPaidTransactionEventBean = null;
        
        postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, 
                eventTimestamp, transactionEvent, postPaidTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);
        
        entityManager.persist(postPaidTransactionEventBean);
       
        oldStatus = newStatus;

        try {
            GetSrcTransactionStatusMessageResponse forecourtResponse = forecourtPPService.getTransactionStatus(requestID, postPaidTransactionBean.getMpTransactionID(), 
                    postPaidTransactionBean.getSrcTransactionID());
            System.out.println("chiamata a getTransactionStatus del ForecourtPostPaid: " + forecourtResponse.getStatusCode());

            
            if (forecourtResponse == null || forecourtResponse.getStatusCode().equals(StatusHelper.POST_PAID_TRANSACTION_STATUS_NOT_RECOGNIZED)) {
                System.out.println("Errore nella operazione di riconciliazione - "
                        + "risposta Forecourt nulla o non disponibile (transazione: " + postPaidTransactionBean.getTransactionID() + ")");

                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                decreaseAttempts(postPaidTransactionBean);
                return reconciliationInfo;
            }
            
            String GFGTransactionStatus = forecourtResponse.getSrcTransactionDetail().getSrcTransactionStatus();
            PartnerType partnerType = PartnerType.MP;
            Integer credits = 0;
            
            for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : postPaidTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {
                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                Long requestTimestamp = new Date().getTime();
                String operationIDtoCheck = postPaidLoadLoyaltyCreditsBean.getOperationID();
                
                System.out.println("Controllo caricamento punti transazione "  + postPaidTransactionBean.getMpTransactionID());
                CheckLoadLoyaltyCreditsTransactionResult checkLoadLoyaltyCreditsTransactionResult = fidelityService.checkLoadLoyaltyCreditsTransaction(operationID, operationIDtoCheck, 
                        partnerType, requestTimestamp);
                credits += checkLoadLoyaltyCreditsTransactionResult.getCredits();
                
            }
            
            if (credits == 0) {
                System.out.println("Storno caricamento punti non necessario. Importo caricato 0");
                postPaidTransactionBean.setLoyaltyReconcile(false);
                entityManager.merge(postPaidTransactionBean);

                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                return reconciliationInfo;
            }
                        
            if (postPaidTransactionBean.getLoyaltyReconcile() || (GFGTransactionStatus != null && (GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED)
                    || GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED))))  {
            
                sequenceID += 1;
                this.revertLoadLoyaltyCredits(postPaidTransactionBean, sequenceID);
                postPaidTransactionBean.setLoyaltyReconcile(false);
                entityManager.merge(postPaidTransactionBean);
            }
            else {
                System.out.println("Storno caricamento punti non necessario. Importo caricato " + credits);
            }
        }
        catch (Exception ex) {
            System.out.println("Exception revertLoadLoyaltyCredits: " + ex.getMessage());
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(postPaidTransactionBean);
        }
        */
        PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
        String requestID = String.valueOf(new Date().getTime());
        sequenceID = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getSequenceID() + 1;
        
        newStatus = StatusHelper.POST_PAID_STATUS_RECONCILIATION_REQU;
        oldStatus = null;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_RECONCILIATION;
        String errorCode = null;
        String errorDescription = null;
        String resultString = "OK";
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        PostPaidTransactionEventBean postPaidTransactionEventBean = null;
        
        postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, 
                eventTimestamp, transactionEvent, postPaidTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);
        
        entityManager.persist(postPaidTransactionEventBean);
       
        oldStatus = newStatus;

        GetSrcTransactionStatusMessageResponse forecourtResponse = forecourtPPService.getTransactionStatus(requestID, postPaidTransactionBean.getMpTransactionID(), 
                postPaidTransactionBean.getSrcTransactionID());
        System.out.println("chiamata a getTransactionStatus del ForecourtPostPaid: " + forecourtResponse.getStatusCode());

        transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY;
        newStatus = StatusHelper.POST_PAID_STATUS_CREDITS_LOAD;        
        
        if (forecourtResponse == null || forecourtResponse.getStatusCode().equals(StatusHelper.POST_PAID_TRANSACTION_STATUS_NOT_RECOGNIZED)) {
            System.out.println("Errore nella operazione di riconciliazione - "
                    + "risposta Forecourt nulla o non disponibile (transazione: " + postPaidTransactionBean.getTransactionID() + ")");

            errorCode = "8888";
            errorDescription = (forecourtResponse == null) ? "forecourtResponse nullo" : forecourtResponse.getMessageCode();
            resultString = "ERROR";
            sequenceID += 1;
            
            postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, 
                    eventTimestamp, transactionEvent, postPaidTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);
            
            entityManager.persist(postPaidTransactionEventBean);
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(postPaidTransactionBean);
            return reconciliationInfo;
        }
        
        String GFGTransactionStatus = null;
        
        if (forecourtResponse != null && forecourtResponse.getSrcTransactionDetail() != null) {
            GFGTransactionStatus = forecourtResponse.getSrcTransactionDetail().getSrcTransactionStatus();
        }
        
        if (GFGTransactionStatus != null && (GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED)
                || GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED)))  {
        
            EventResult verifiyLoyalty = verifyLoyaltyTransaction(postPaidTransactionBean);
            
            if (verifiyLoyalty.getResult().equals(EventResultType.TO_RETRY)) {
                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(verifiyLoyalty.getResult().getCode());
                decreaseAttempts(postPaidTransactionBean);
                return reconciliationInfo;
            }
            
            Integer credits = (Integer) verifiyLoyalty.getAmount();

            if (credits == 0) {
                System.out.println("Storno caricamento punti non necessario. Crediti caricati 0");
    
                postPaidTransactionBean.setLoyaltyReconcile(false);
                entityManager.merge(postPaidTransactionBean);
    
                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(verifiyLoyalty.getResult().getCode());
            }
            else {
                sequenceID += 1;
                EventResult revertLoyalty = this.revertLoadLoyaltyCredits(postPaidTransactionBean, sequenceID);
                
                if (revertLoyalty.getResult().equals(EventResultType.SUCCESS)) {
                    postPaidTransactionBean.setLoyaltyReconcile(false);
                    entityManager.merge(postPaidTransactionBean);
                }
                
                if (revertLoyalty.getResult().equals(EventResultType.TO_RETRY)) {
                    decreaseAttempts(postPaidTransactionBean);
                }
    
                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(revertLoyalty.getResult().getCode());            
            }
        }
        else if (GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID)) {
            sequenceID += 1;
            EventResult loadCredits = loadLoyaltyCredits(postPaidTransactionBean, sequenceID);
            
            if (loadCredits.getResult().equals(EventResultType.SUCCESS)) {
                postPaidTransactionBean.setLoyaltyReconcile(false);
                entityManager.merge(postPaidTransactionBean);
            }
            
            if (loadCredits.getResult().equals(EventResultType.TO_RETRY)) {
                decreaseAttempts(postPaidTransactionBean);
            }
    
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(loadCredits.getResult().getCode());
        }
        else {
            System.out.println("Storno caricamento punti non eseguito. GFGTransactionStatus: " + GFGTransactionStatus);
            
            errorCode = "8888";
            errorDescription = (GFGTransactionStatus == null) ? "GFGTransactionStatus nullo" : "GFGTransactionStatus : " + GFGTransactionStatus + " non valido";
            resultString = "ERROR";
            sequenceID += 1;
            
            postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, 
                    eventTimestamp, transactionEvent, postPaidTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);
            
            entityManager.persist(postPaidTransactionEventBean);
            
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(postPaidTransactionBean);
        }
        
        return reconciliationInfo;
    }
    
    @Override
    public ReconciliationInfo reconciliateVoucherFlow(TransactionInterfaceBean transactionInterfaceBean) {
        PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
        String requestID = String.valueOf(new Date().getTime());
        sequenceID = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getSequenceID() + 1;
        
        newStatus = StatusHelper.POST_PAID_STATUS_RECONCILIATION_REQU;
        oldStatus = null;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_RECONCILIATION;
        String errorCode = null;
        String errorDescription = null;
        String resultString = "OK";
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        PostPaidTransactionEventBean postPaidTransactionEventBean = null;
        
        postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, 
                eventTimestamp, transactionEvent, postPaidTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);
        
        entityManager.persist(postPaidTransactionEventBean);
       
        oldStatus = newStatus;

        GetSrcTransactionStatusMessageResponse forecourtResponse = forecourtPPService.getTransactionStatus(requestID, postPaidTransactionBean.getMpTransactionID(), 
                postPaidTransactionBean.getSrcTransactionID());
        System.out.println("chiamata a getTransactionStatus del ForecourtPostPaid: " + forecourtResponse.getStatusCode());

        transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY;
        newStatus = StatusHelper.POST_PAID_STATUS_CREDITS_LOAD;        
        
        if (forecourtResponse == null || forecourtResponse.getStatusCode().equals(StatusHelper.POST_PAID_TRANSACTION_STATUS_NOT_RECOGNIZED)) {
            System.out.println("Errore nella operazione di riconciliazione - "
                    + "risposta Forecourt nulla o non disponibile (transazione: " + postPaidTransactionBean.getTransactionID() + ")");

            errorCode = "8888";
            errorDescription = (forecourtResponse == null) ? "forecourtResponse nullo" : forecourtResponse.getMessageCode();
            resultString = "ERROR";
            sequenceID += 1;
            
            postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, 
                    eventTimestamp, transactionEvent, postPaidTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);
            
            entityManager.persist(postPaidTransactionEventBean);
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(postPaidTransactionBean);
            return reconciliationInfo;
        }
        
        String GFGTransactionStatus = null;
        
        if (forecourtResponse != null && forecourtResponse.getSrcTransactionDetail() != null) {
            GFGTransactionStatus = forecourtResponse.getSrcTransactionDetail().getSrcTransactionStatus();
        }
        
        if (GFGTransactionStatus != null && (GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED)
                || GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED)))  {
        
            EventResult verifiyLoyalty = verifyLoyaltyTransaction(postPaidTransactionBean);
            
            if (verifiyLoyalty.getResult().equals(EventResultType.TO_RETRY)) {
                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(verifiyLoyalty.getResult().getCode());
                decreaseAttempts(postPaidTransactionBean);
                return reconciliationInfo;
            }
            
            Integer credits = (Integer) verifiyLoyalty.getAmount();

            if (credits == 0) {
                System.out.println("Storno caricamento punti non necessario. Crediti caricati 0");
    
                postPaidTransactionBean.setLoyaltyReconcile(false);
                entityManager.merge(postPaidTransactionBean);
    
                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(verifiyLoyalty.getResult().getCode());
            }
            else {
                sequenceID += 1;
                EventResult revertLoyalty = this.revertLoadLoyaltyCredits(postPaidTransactionBean, sequenceID);
                
                if (revertLoyalty.getResult().equals(EventResultType.SUCCESS)) {
                    postPaidTransactionBean.setLoyaltyReconcile(false);
                    entityManager.merge(postPaidTransactionBean);
                }
                
                if (revertLoyalty.getResult().equals(EventResultType.TO_RETRY)) {
                    decreaseAttempts(postPaidTransactionBean);
                }
    
                reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
                reconciliationInfo.setStatusCode(revertLoyalty.getResult().getCode());            
            }
        }
        else if (GFGTransactionStatus.equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID)) {
            sequenceID += 1;
            EventResult loadCredits = loadLoyaltyCredits(postPaidTransactionBean, sequenceID);
            
            if (loadCredits.getResult().equals(EventResultType.SUCCESS)) {
                postPaidTransactionBean.setLoyaltyReconcile(false);
                entityManager.merge(postPaidTransactionBean);
            }
            
            if (loadCredits.getResult().equals(EventResultType.TO_RETRY)) {
                decreaseAttempts(postPaidTransactionBean);
            }

            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(loadCredits.getResult().getCode());
        }
        else {
            System.out.println("Storno caricamento punti non eseguito. GFGTransactionStatus: " + GFGTransactionStatus);
            
            errorCode = "8888";
            errorDescription = (GFGTransactionStatus == null) ? "GFGTransactionStatus nullo" : "GFGTransactionStatus : " + GFGTransactionStatus + " non valido";
            resultString = "ERROR";
            sequenceID += 1;
            
            postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION, requestID, 
                    eventTimestamp, transactionEvent, postPaidTransactionBean, resultString, newStatus, oldStatus, errorCode, errorDescription);
            
            entityManager.persist(postPaidTransactionEventBean);
            
            reconciliationInfo.setFinalStatusType(postPaidTransactionBean.getTransactionStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(postPaidTransactionBean);
        }

        
        return reconciliationInfo;
    }

    public boolean checkAttemptsLeft(TransactionInterfaceBean transactionInterfaceBean) {
        boolean check = true;
        
        if (transactionInterfaceBean.getReconciliationAttemptsLeft() <= 0) {
            //updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
            PostPaidTransactionBean postPaidTransactionBean = (PostPaidTransactionBean) transactionInterfaceBean;
            postPaidTransactionBean.setLoyaltyReconcile(false);
            entityManager.merge(postPaidTransactionBean);
            check = false;
        }
        
        return check;
    }
    
    @Override
    public String getStatus() {
        return StatusHelper.POST_PAID_STATUS_LOYALTY_REVERSE;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "POST PAID LOYALTY ERROR";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<PostPaidTransactionBean> transactionBeanList = QueryRepository.findPostPaidTransactionsLoyaltyReconciliation(entityManager);
        return transactionBeanList;
    }

}
