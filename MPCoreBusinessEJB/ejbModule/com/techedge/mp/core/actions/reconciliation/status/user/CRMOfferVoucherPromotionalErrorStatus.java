package com.techedge.mp.core.actions.reconciliation.status.user;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.crm.CRMOfferVoucherPromotionalBean;
import com.techedge.mp.core.business.model.voucher.VoucherPromotionalBean;
import com.techedge.mp.core.business.model.voucher.VoucherPromotionalDetailBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CreateVoucherPromotionalResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public class CRMOfferVoucherPromotionalErrorStatus extends ReconciliationUserStatus {

    public CRMOfferVoucherPromotionalErrorStatus(EntityManager entityManager, DWHAdapterServiceRemote dwhAdapterService, CRMAdapterServiceRemote crmAdapterService, 
            PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, ParametersServiceRemote parametersService, String proxyHost, String proxyPort, String proxyNoHosts, 
            Integer pushNotificationExpiryTime, Integer reconciliationAttemptsLeft, Boolean refuelingOauth2Active) {
        
        super(entityManager, dwhAdapterService, crmAdapterService, pushNotificationService, fidelityService, emailSender, refuelingNotificationService, refuelingNotificationServiceOAuth2, parametersService, proxyHost, 
                proxyPort, proxyNoHosts, pushNotificationExpiryTime, reconciliationAttemptsLeft, refuelingOauth2Active);
    }

    @Override
    public ReconciliationInfo reconciliate(UserReconciliationInterfaceBean userReconciliationInterfaceBean) {
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        CRMOfferVoucherPromotionalBean crmOfferVoucherPromotionalBean = (CRMOfferVoucherPromotionalBean) userReconciliationInterfaceBean;
        
        
        String operationID = new IdGenerator().generateId(16).substring(0, 33);
        PartnerType partnerType = PartnerType.MP;
        VoucherConsumerType voucherType = VoucherConsumerType.ENI;
        long requestTimestamp = new Date().getTime();
        String fiscalCode = null;
        BigDecimal totalAmount = null;
        String promoCode = null;
        UserBean userBean = crmOfferVoucherPromotionalBean.getUserBean();
        
        if (userBean != null) {
            fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
        }
        
        if (crmOfferVoucherPromotionalBean.getVoucherAmount() != null) {
            totalAmount = BigDecimal.valueOf(crmOfferVoucherPromotionalBean.getVoucherAmount());
        }
        
        if (crmOfferVoucherPromotionalBean.getVoucherPromoCode() != null) {
            promoCode = crmOfferVoucherPromotionalBean.getVoucherPromoCode();
        }
        
        if (crmOfferVoucherPromotionalBean.getVoucherPromotionalBean() == null) {
            VoucherPromotionalBean voucherPromotionalBean = new VoucherPromotionalBean();
            voucherPromotionalBean.setFiscalCode(fiscalCode);
            voucherPromotionalBean.setPartnerType(partnerType);
            voucherPromotionalBean.setPromoCode(promoCode);
            voucherPromotionalBean.setTotalAmount(totalAmount.doubleValue());
            
            entityManager.persist(voucherPromotionalBean);
            crmOfferVoucherPromotionalBean.setVoucherPromotionalBean(voucherPromotionalBean);
        }
                
        CreateVoucherPromotionalResult createVoucherResult = new CreateVoucherPromotionalResult();
        VoucherPromotionalDetailBean voucherPromotionalDetailBean = new VoucherPromotionalDetailBean();
        voucherPromotionalDetailBean.setOperationID(operationID);
        voucherPromotionalDetailBean.setRequestTimestamp(new Date(requestTimestamp));
        voucherPromotionalDetailBean.setVoucherPromotionalBean(crmOfferVoucherPromotionalBean.getVoucherPromotionalBean());

        entityManager.persist(voucherPromotionalDetailBean);
        
        try {
            createVoucherResult = fidelityService.createVoucherPromotional(operationID, voucherType, partnerType, requestTimestamp, fiscalCode, promoCode, totalAmount);
            
            if (createVoucherResult.getStatusCode().equals(FidelityResponse.CREATE_VOUCHER_PROMOTIONAL_OK)) {
                VoucherDetail voucherDetail = createVoucherResult.getVoucher();
                try {
                    VoucherBean voucherBean = new VoucherBean();
                    voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                    voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                    voucherBean.setInitialValue(voucherDetail.getInitialValue());
                    voucherBean.setPromoCode(voucherDetail.getPromoCode());
                    voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                    voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                    voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                    voucherBean.setCode(voucherDetail.getVoucherCode());
                    voucherBean.setStatus(voucherDetail.getVoucherStatus());
                    voucherBean.setType(voucherDetail.getVoucherType());
                    voucherBean.setValue(voucherDetail.getVoucherValue());
                    voucherBean.setIsCombinable(voucherDetail.getIsCombinable());
                    voucherBean.setMinAmount(voucherDetail.getMinAmount());
                    voucherBean.setMinQuantity(voucherDetail.getMinQuantity());
                    voucherBean.setPromoPartner(voucherDetail.getPromoPartner());
                    voucherBean.setUserBean(userBean);
            
                    entityManager.persist(voucherBean);
            
                    crmOfferVoucherPromotionalBean.getVoucherPromotionalBean().setVoucherBean(voucherBean);
                    entityManager.merge(crmOfferVoucherPromotionalBean.getVoucherPromotionalBean());                
                    
                    crmOfferVoucherPromotionalBean.setVoucherPromotionalToReconcilie(false);
                    entityManager.merge(crmOfferVoucherPromotionalBean);
                    
                    System.out.println("Associato voucher promozionale (" + promoCode + ") a l'utente (" + crmOfferVoucherPromotionalBean.getUserBean().getId() + ")");
                    
                    reconciliationInfo.setFinalStatusType(StatusHelper.CRM_OFFER_VOUCHER_PROMOTIONAL_SUCCESS);
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                
                }
                catch (Exception ex) {
                    System.err.println("Errore nell'associazione voucher promozionale (" + voucherDetail.getPromoCode() + ") a l'utente (" + userBean.getId() + ")");
                    createVoucherResult.setStatusCode(FidelityResponse.CREATE_VOUCHER_PROMOTIONAL_GENERIC_ERROR);
                    createVoucherResult.setMessageCode(ex.getMessage());
                    createVoucherResult.setCsTransactionID(null);
                    
                    decreaseAttempts(crmOfferVoucherPromotionalBean);
                    
                    reconciliationInfo.setFinalStatusType(StatusHelper.CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR);
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                }
                
            }
            else {
                System.out.println("Errore nella creazione del voucher promozionale (" + promoCode + ") per l'utente (" + crmOfferVoucherPromotionalBean.getUserBean().getId() + "): "
                + createVoucherResult.getMessageCode());
                
                decreaseAttempts(crmOfferVoucherPromotionalBean);
                
                reconciliationInfo.setFinalStatusType(StatusHelper.CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            }
        }
        catch (FidelityServiceException ex) {
            System.err.println("Errore nella creazione del voucher promozionale (" + promoCode + "): " + ex.getMessage());
            createVoucherResult.setStatusCode(FidelityResponse.CREATE_VOUCHER_PROMOTIONAL_GENERIC_ERROR);
            createVoucherResult.setMessageCode(ex.getMessage());
            createVoucherResult.setCsTransactionID(null);
            
            decreaseAttempts(crmOfferVoucherPromotionalBean);
            
            reconciliationInfo.setFinalStatusType(StatusHelper.CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
        }
        
        voucherPromotionalDetailBean.setCsTransactionID(createVoucherResult.getCsTransactionID());
        voucherPromotionalDetailBean.setMessageCode(createVoucherResult.getMessageCode());
        voucherPromotionalDetailBean.setStatusCode(createVoucherResult.getStatusCode());
        
        entityManager.persist(voucherPromotionalDetailBean);
        
        crmOfferVoucherPromotionalBean.getVoucherPromotionalBean().getVoucherPromotionalDetailBeanList().add(voucherPromotionalDetailBean);
        
        return reconciliationInfo;
    }

    @Override
    public String getStatus() {
        return StatusHelper.CRM_OFFER_VOUCHER_PROMOTIONAL_ERROR;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "CRM OFFER VOUCHER PROMOTIONAL ERROR";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<CRMOfferVoucherPromotionalBean> crmOfferVoucherPromotionalBeanList = QueryRepository.findCRMOfferVoucherPromotionalToReconcilie(entityManager);
        return crmOfferVoucherPromotionalBeanList;
    }

}
