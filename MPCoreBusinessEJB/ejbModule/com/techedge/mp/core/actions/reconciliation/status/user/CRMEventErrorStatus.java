package com.techedge.mp.core.actions.reconciliation.status.user;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.crm.CommandType;
import com.techedge.mp.core.business.interfaces.crm.Response;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;
import com.techedge.mp.core.business.model.crm.CRMEventBean;
import com.techedge.mp.core.business.model.crm.CRMEventOperationBean;
import com.techedge.mp.core.business.model.crm.CRMOfferBean;
import com.techedge.mp.core.business.model.crm.CRMOfferParametersBean;
import com.techedge.mp.core.business.model.crm.CRMOfferVoucherPromotionalBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.CreateVoucherPromotional;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationMessage;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public class CRMEventErrorStatus extends ReconciliationUserStatus {

    private final static String PARAM_CRM_INTERACTION_POINT_REGISTRATION    = "CRM_INTERACTION_POINT_REGISTRATION";
    private final static String PARAM_CRM_INTERACTION_POINT_LOYALTY_CREDITS = "CRM_INTERACTION_POINT_LOYALTY_CREDITS";
    private final static String PARAM_CRM_INTERACTION_POINT_REFUELING       = "CRM_INTERACTION_POINT_REFUELING";
    private final static String PARAM_CRM_INTERACTION_POINT_CREDIT_CARD     = "CRM_INTERACTION_POINT_CREDIT_CARD";
    
    public CRMEventErrorStatus(EntityManager entityManager, DWHAdapterServiceRemote dwhAdapterService, CRMAdapterServiceRemote crmAdapterService, 
            PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, RefuelingNotificationServiceRemote refuelingNotificationService, 
            RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, ParametersServiceRemote parametersService, String proxyHost, String proxyPort, String proxyNoHosts,
            Integer pushNotificationExpiryTime, Integer reconciliationAttemptsLeft, Boolean refuelingOauth2Active) {

        super(entityManager, dwhAdapterService, crmAdapterService, pushNotificationService, fidelityService, emailSender, refuelingNotificationService, refuelingNotificationServiceOAuth2, parametersService, proxyHost, 
                proxyPort, proxyNoHosts, pushNotificationExpiryTime, reconciliationAttemptsLeft, refuelingOauth2Active);
    }

    @Override
    public ReconciliationInfo reconciliate(UserReconciliationInterfaceBean userReconciliationInterfaceBean) throws Exception {
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        CRMEventBean crmEventBean = (CRMEventBean) userReconciliationInterfaceBean;

        String interactionPointRegistration = null;
        String interactionPointRefueling = null;
        String interactionPointLoyalty = null;
        String interactionPointCreditCard = null;

        try {
            interactionPointRegistration = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_REGISTRATION);
            interactionPointRefueling = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_REFUELING);
            interactionPointLoyalty = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_LOYALTY_CREDITS);
            interactionPointCreditCard = this.parametersService.getParamValue(PARAM_CRM_INTERACTION_POINT_CREDIT_CARD);
        }
        catch (ParameterNotFoundException ex) {
            System.err.println("Errore nella lettura dei parametri: " + ex.getMessage());
            throw ex;
        }

        String sessionID = crmEventBean.getSessionID();
        String audienceID = "999";
        boolean useExecuteBatch = true;
        String interactionPoint = crmEventBean.getInteractionPointToString();
        UserProfile userProfile = null;
        UserBean userBean = crmEventBean.getUserBean();
        String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
        Response responseOffers = null;
        HashMap<UserProfile.Parameter, Object> commandParameters = crmEventBean.getOperationBean(CommandType.START_SESSION).getCommandParameters();
        List<CRMOfferBean> crmOffersList = new ArrayList<CRMOfferBean>();

        if (commandParameters.isEmpty()) {
            System.out.println("Start Session command hasn't parameters");

            crmEventBean.setToReconcilie(false);
            entityManager.merge(crmEventBean);

            reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

            return reconciliationInfo;
        }

        if (interactionPoint.equals(interactionPointRegistration)) {
            Date timestamp = (Date) getCommandParameter(commandParameters, UserProfile.Parameter.DATA, Date.class); //commandParameters.get(UserProfile.Parameter.DATA);
            String name = (String) getCommandParameter(commandParameters, UserProfile.Parameter.NOME, String.class);   //commandParameters.get(UserProfile.Parameter.NOME);
            String surname = (String) getCommandParameter(commandParameters, UserProfile.Parameter.COGNOME, String.class);
            String email = (String) getCommandParameter(commandParameters, UserProfile.Parameter.INDIRIZZO_EMAIL, String.class);
            Date dateOfBirth = (Date) getCommandParameter(commandParameters, UserProfile.Parameter.DATA_DI_NASCITA, Date.class);
            Boolean flagNotification = (Boolean) getCommandParameter(commandParameters, UserProfile.Parameter.FG_NOTIFICA, Boolean.class);
            Boolean flagCreditCard = (Boolean) getCommandParameter(commandParameters, UserProfile.Parameter.FG_1, Boolean.class);
            String cardBrand = (String) getCommandParameter(commandParameters, UserProfile.Parameter.CARTA, String.class);
            ClusterType cluster = ClusterType.getValueOf(((Integer) getCommandParameter(commandParameters, UserProfile.Parameter.CLUSTER, Integer.class)).toString());

            userProfile = UserProfile.getUserProfileForEventRegistration(fiscalCode, timestamp, name, surname, email, dateOfBirth, flagNotification, 
                    flagCreditCard, cardBrand, cluster);
        }
        
        if (interactionPoint.equals(interactionPointCreditCard)) {
            audienceID = fiscalCode;
            Date timestamp = (Date) getCommandParameter(commandParameters, UserProfile.Parameter.DATA, Date.class); //commandParameters.get(UserProfile.Parameter.DATA);
            Boolean flagNotification = (Boolean) getCommandParameter(commandParameters, UserProfile.Parameter.FG_NOTIFICA, Boolean.class);
            Boolean flagCreditCard = (Boolean) getCommandParameter(commandParameters, UserProfile.Parameter.FG_1, Boolean.class);
            String cardBrand = (String) getCommandParameter(commandParameters, UserProfile.Parameter.CARTA, String.class);
            ClusterType cluster = ClusterType.getValueOf(((Integer) getCommandParameter(commandParameters, UserProfile.Parameter.CLUSTER, Integer.class)).toString());

            userProfile = UserProfile.getUserProfileForEventCreditCard(fiscalCode, timestamp, flagNotification, flagCreditCard, cardBrand, cluster);
        }

        if (interactionPoint.equals(interactionPointRefueling) || interactionPoint.equals(interactionPointLoyalty)) {
            audienceID = fiscalCode;
            Date timestamp = (Date) getCommandParameter(commandParameters, UserProfile.Parameter.DATA, Date.class);  //commandParameters.get(UserProfile.Parameter.DATA);
            String pv = (String) getCommandParameter(commandParameters, UserProfile.Parameter.IMPIANTO, String.class);    //commandParameters.get(UserProfile.Parameter.IMPIANTO);
            String product = (String) getCommandParameter(commandParameters, UserProfile.Parameter.PRODOTTO, String.class);   //commandParameters.get(UserProfile.Parameter.PRODOTTO);
            Boolean flagPayment = (Boolean) getCommandParameter(commandParameters, UserProfile.Parameter.FLAG_PAGAMENTO, Boolean.class); //commandParameters.get(UserProfile.Parameter.FLAG_PAGAMENTO);
            Integer loyaltyCredits = (Integer) getCommandParameter(commandParameters, UserProfile.Parameter.PUNTI, Integer.class);  //commandParameters.get(UserProfile.Parameter.PUNTI);
            Double refuelQuantity = (Double) getCommandParameter(commandParameters, UserProfile.Parameter.LITRI, Double.class);    //commandParameters.get(UserProfile.Parameter.LITRI);
            String refuelMode = (String) getCommandParameter(commandParameters, UserProfile.Parameter.MODALITA_OPERATIVA, String.class);    //commandParameters.get(UserProfile.Parameter.MODALITA_OPERATIVA);
            Boolean flagNotification = (Boolean) getCommandParameter(commandParameters, UserProfile.Parameter.FG_NOTIFICA, Boolean.class);
            Boolean flagCreditCard = (Boolean) getCommandParameter(commandParameters, UserProfile.Parameter.FG_1, Boolean.class);
            String cardBrand = (String) getCommandParameter(commandParameters, UserProfile.Parameter.CARTA, String.class);
            ClusterType cluster = ClusterType.getValueOf(((Integer) getCommandParameter(commandParameters, UserProfile.Parameter.CLUSTER, Integer.class)).toString());
            String surname = (String) getCommandParameter(commandParameters, UserProfile.Parameter.COGNOME, String.class);
            Double amount = (Double) getCommandParameter(commandParameters, UserProfile.Parameter.IMPORTO, Double.class);
            
            userProfile = UserProfile.getUserProfileForEventRefueling(fiscalCode, timestamp, surname, pv, product, flagPayment, loyaltyCredits, 
                    refuelQuantity, amount, refuelMode, flagNotification, flagCreditCard, cardBrand, cluster);
        }

        if (useExecuteBatch) {
            System.out.println("chiamata a executeBatch del CRM");

            List<Response> responseBatch = crmAdapterService.executeBatchGetOffers(sessionID, fiscalCode, audienceID, interactionPoint, userProfile.getParameters());
            Response responseStartSession = responseBatch.get(0);
            CRMEventOperationBean operationStartSessionBean = crmEventBean.getOperationBean(CommandType.START_SESSION);
            operationStartSessionBean.setStatusCode(responseStartSession.getStatusCode());
            operationStartSessionBean.setRequestTimestamp(new Date());
            entityManager.merge(operationStartSessionBean);

            if (!responseStartSession.getStatusCode().equals(com.techedge.mp.core.business.interfaces.crm.StatusCode.SUCCESS)) {

                if (responseStartSession.getAdvisoryMessages().size() > 0) {
                    String message = responseStartSession.getAdvisoryMessages().get(0).getMessage() + " (" + responseStartSession.getAdvisoryMessages().get(0).getDetailMessage()
                            + ")";
                    System.err.println("Error in InteractAPI(executeBatchGetOffers): " + message);

                    operationStartSessionBean.setExtendedMessage(responseStartSession.getAdvisoryMessages().get(0).getMessage());
                    operationStartSessionBean.setMessageCode(responseStartSession.getAdvisoryMessages().get(0).getMessageCode());
                    operationStartSessionBean.setExtendedMessageDetail(responseStartSession.getAdvisoryMessages().get(0).getDetailMessage());
                    operationStartSessionBean.setMessageStatusLevel(responseStartSession.getAdvisoryMessages().get(0).getStatusLevel().getValue());
                    entityManager.merge(operationStartSessionBean);
                }

                decreaseAttempts(crmEventBean);

                reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                return reconciliationInfo;
            }
            else {
                operationStartSessionBean.setExtendedMessage(null);
                operationStartSessionBean.setMessageCode(null);
                operationStartSessionBean.setExtendedMessageDetail(null);
                operationStartSessionBean.setMessageStatusLevel(null);
                entityManager.merge(operationStartSessionBean);
            }

            responseOffers = responseBatch.get(1);
            CRMEventOperationBean operationGetOffersBean = crmEventBean.getOperationBean(CommandType.GET_OFFERS);

            if (responseOffers.getStatusCode() != null) {

                if (operationGetOffersBean == null) {
                    operationGetOffersBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.GET_OFFERS, responseOffers);
                    entityManager.persist(operationGetOffersBean);
                }
                else {
                    operationGetOffersBean.setStatusCode(responseOffers.getStatusCode());
                    operationGetOffersBean.setRequestTimestamp(new Date());
                    entityManager.merge(operationGetOffersBean);
                }

                if (!responseOffers.getStatusCode().equals(com.techedge.mp.core.business.interfaces.crm.StatusCode.SUCCESS)) {

                    if (responseOffers.getAdvisoryMessages().size() > 0) {
                        String message = responseOffers.getAdvisoryMessages().get(0).getMessage() + " (" + responseOffers.getAdvisoryMessages().get(0).getDetailMessage() + ")";
                        System.err.println("Error in InteractAPI(executeBatchGetOffers): " + message);

                        operationGetOffersBean.setExtendedMessage(responseOffers.getAdvisoryMessages().get(0).getMessage());
                        operationGetOffersBean.setMessageCode(responseOffers.getAdvisoryMessages().get(0).getMessageCode());
                        operationGetOffersBean.setExtendedMessageDetail(responseOffers.getAdvisoryMessages().get(0).getDetailMessage());
                        operationGetOffersBean.setMessageStatusLevel(responseOffers.getAdvisoryMessages().get(0).getStatusLevel().getValue());
                        entityManager.merge(operationGetOffersBean);
                    }

                    decreaseAttempts(userReconciliationInterfaceBean);

                    reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                    return reconciliationInfo;
                }
                else {
                    operationGetOffersBean.setExtendedMessage(null);
                    operationGetOffersBean.setMessageCode(null);
                    operationGetOffersBean.setExtendedMessageDetail(null);
                    operationGetOffersBean.setMessageStatusLevel(null);
                    entityManager.merge(operationGetOffersBean);
                }
            }
            crmOffersList = CRMOfferBean.createOffers(crmEventBean, responseOffers, false);

            for (CRMOfferBean crmOfferBean : crmOffersList) {
                entityManager.merge(crmOfferBean);
            }
        }
        else {
            Response responseStartSession = crmAdapterService.startSession(sessionID, fiscalCode, audienceID, userProfile.getParameters());
            CRMEventOperationBean operationStartSessionBean = crmEventBean.getOperationBean(CommandType.START_SESSION);
            operationStartSessionBean.setStatusCode(responseStartSession.getStatusCode());
            entityManager.merge(operationStartSessionBean);

            if (!responseStartSession.getStatusCode().equals(com.techedge.mp.core.business.interfaces.crm.StatusCode.SUCCESS)) {

                if (responseStartSession.getAdvisoryMessages().size() > 0) {
                    String message = responseStartSession.getAdvisoryMessages().get(0).getMessage() + " (" + responseStartSession.getAdvisoryMessages().get(0).getDetailMessage()
                            + ")";

                    System.err.println("Error in InteractAPI(startSession): " + message);

                    operationStartSessionBean.setExtendedMessage(responseStartSession.getAdvisoryMessages().get(0).getMessage());
                    operationStartSessionBean.setMessageCode(responseStartSession.getAdvisoryMessages().get(0).getMessageCode());
                    operationStartSessionBean.setExtendedMessageDetail(responseStartSession.getAdvisoryMessages().get(0).getDetailMessage());
                    operationStartSessionBean.setMessageStatusLevel(responseStartSession.getAdvisoryMessages().get(0).getStatusLevel().getValue());
                    entityManager.merge(operationStartSessionBean);
                }

                decreaseAttempts(userReconciliationInterfaceBean);

                reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                return reconciliationInfo;
            }
            else {
                operationStartSessionBean.setExtendedMessage(null);
                operationStartSessionBean.setMessageCode(null);
                operationStartSessionBean.setExtendedMessageDetail(null);
                operationStartSessionBean.setMessageStatusLevel(null);
                entityManager.merge(operationStartSessionBean);
            }

            responseOffers = crmAdapterService.getOffers(sessionID, interactionPoint, 1);
            CRMEventOperationBean operationGetOffersBean = crmEventBean.getOperationBean(CommandType.GET_OFFERS);

            if (responseOffers.getStatusCode() != null) {

                if (operationGetOffersBean == null) {
                    operationGetOffersBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.GET_OFFERS, responseOffers);
                    entityManager.persist(operationGetOffersBean);
                }
                else {
                    operationGetOffersBean.setStatusCode(responseOffers.getStatusCode());
                    operationGetOffersBean.setRequestTimestamp(new Date());
                    entityManager.merge(operationGetOffersBean);
                }

                if (!responseOffers.getStatusCode().equals(com.techedge.mp.core.business.interfaces.crm.StatusCode.SUCCESS)) {

                    if (responseOffers.getAdvisoryMessages().size() > 0) {
                        String message = responseOffers.getAdvisoryMessages().get(0).getMessage() + " (" + responseOffers.getAdvisoryMessages().get(0).getDetailMessage() + ")";

                        System.err.println("Error in InteractAPI(startSession): " + message);

                        operationGetOffersBean.setExtendedMessage(responseOffers.getAdvisoryMessages().get(0).getMessage());
                        operationGetOffersBean.setMessageCode(responseOffers.getAdvisoryMessages().get(0).getMessageCode());
                        operationGetOffersBean.setExtendedMessageDetail(responseOffers.getAdvisoryMessages().get(0).getDetailMessage());
                        operationGetOffersBean.setMessageStatusLevel(responseOffers.getAdvisoryMessages().get(0).getStatusLevel().getValue());
                        entityManager.merge(operationGetOffersBean);
                    }

                    decreaseAttempts(userReconciliationInterfaceBean);

                    reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                    return reconciliationInfo;
                }
                else {
                    operationGetOffersBean.setExtendedMessage(null);
                    operationGetOffersBean.setMessageCode(null);
                    operationGetOffersBean.setExtendedMessageDetail(null);
                    operationGetOffersBean.setMessageStatusLevel(null);
                    entityManager.merge(operationGetOffersBean);
                }
            }
            crmOffersList = CRMOfferBean.createOffers(crmEventBean, responseOffers, false);

            for (CRMOfferBean crmOfferBean : crmOffersList) {
                entityManager.merge(crmOfferBean);
            }

            @SuppressWarnings("unused")
            Response responseEndSession = crmAdapterService.endSession(sessionID);
        }

        if (responseOffers.getOfferList() == null || responseOffers.getOfferList().getRecommendedOffers().size() == 0) {
            System.out.println("Nessuna offerta ricevuta da comunicare");
            crmEventBean.setToReconcilie(false);
            entityManager.merge(crmEventBean);

            reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);

            return reconciliationInfo;
        }

        for (CRMOfferBean crmOfferBean : crmOffersList) {
            if (crmOfferBean.getDescription().equalsIgnoreCase("VOUCHER")) {
                System.out.println("Trovata offerta voucher");
                Double amount = 0.0;
                String promoCode = "VCRM";

                for (CRMOfferParametersBean parametersBean : crmOfferBean.getCRMParametersBeanList()) {
                    if (parametersBean.getParameterName().equals("VoucherAmount")) {
                        if (parametersBean.getParameterValue() != null) {
                            amount = new Double((String) parametersBean.getParameterValue());
                        }
                    }
                    if (parametersBean.getParameterName().equals("VoucherPromoCode")) {
                        if (parametersBean.getParameterValue() != null) {
                            promoCode = (String) parametersBean.getParameterValue();
                        }
                    }
                }

                CRMOfferVoucherPromotionalBean crmOfferVoucherPromotionalBean = new CRMOfferVoucherPromotionalBean();
                crmOfferVoucherPromotionalBean.setReconciliationAttemptsLeft(reconciliationAttemptsLeft);
                crmOfferVoucherPromotionalBean.setCrmOfferBean(crmOfferBean);

                CreateVoucherPromotional createVoucherPromotional = new CreateVoucherPromotional(entityManager, fidelityService, userBean);
                String createResponse = createVoucherPromotional.create(promoCode, amount, crmOfferVoucherPromotionalBean);
                String associateResponse = null;

                if (createResponse.equals(ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_SUCCESS)) {
                    associateResponse = createVoucherPromotional.associateToUser(createVoucherPromotional.getCreateVoucherResult());
                }

                System.out.println("VoucherPromotionalBean: " + createVoucherPromotional.getVoucherPromotionalBean().toString());
                System.out.println("eventBean.getOfferBeanList(): " + crmEventBean.getOfferBeanList().size());

                for (CRMOfferBean offerBean : crmEventBean.getOfferBeanList()) {
                    System.out.println("offerBean: getOfferName(): " + offerBean.getOfferName() + "  -  getDescription(): " + offerBean.getDescription());
                    if (offerBean.getDescription().equalsIgnoreCase("VOUCHER")) {
                        crmOfferBean = offerBean;
                        break;
                    }
                }

                if (createResponse.equals(ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_CREATE_ERROR)
                        || associateResponse.equals(ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_ASSIGN_ERROR)) {
                    crmOfferVoucherPromotionalBean.setVoucherPromotionalToReconcilie(true);
                }
                else {
                    crmOfferVoucherPromotionalBean.setVoucherPromotionalToReconcilie(false);
                }

                entityManager.persist(crmOfferVoucherPromotionalBean);

                crmOfferBean.setCrmOfferVoucherPromotionalBean(crmOfferVoucherPromotionalBean);
                entityManager.merge(crmOfferBean);
            }

            String arnEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();
            PushNotificationContentType contentType = PushNotificationContentType.TEXT;
            PushNotificationStatusType statusCode = null;
            String statusMessage = null;
            String publishMessageID = null;
            Date sendingTimestamp = new Date();
            PushNotificationSourceType source = PushNotificationSourceType.CRM_INBOUND;
            String message = crmOfferBean.getOfferName();
            String title = crmOfferBean.getOfferName();

            PushNotificationBean pushNotificationBean = null;

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(sendingTimestamp);
            calendar.add(Calendar.SECOND, pushNotificationExpiryTime);

            pushNotificationBean = new PushNotificationBean();
            pushNotificationBean.setTitle(title);
            pushNotificationBean.setMessage(message);
            pushNotificationBean.setSendingTimestamp(sendingTimestamp);
            pushNotificationBean.setExpiringTimestamp(calendar.getTime());
            pushNotificationBean.setSource(source);
            pushNotificationBean.setContentType(contentType);
            pushNotificationBean.setUser(userBean);
            pushNotificationBean.setRetryAttemptsLeft(reconciliationAttemptsLeft);
            pushNotificationBean.setToReconcilie(false);
            pushNotificationBean.setEndpoint(arnEndpoint);

            entityManager.persist(pushNotificationBean);

            crmEventBean.setPushNotificationBean(pushNotificationBean);
            entityManager.merge(crmEventBean);

            PushNotificationMessage notificationMessage = new PushNotificationMessage();
            notificationMessage.setMessage(pushNotificationBean.getId(), message, title);

            if (arnEndpoint == null) {

                System.err.println("User arn endpoint not found");

                statusCode = PushNotificationStatusType.ERROR;
                statusMessage = "User arn endpoint not found";

                pushNotificationBean.setStatusCode(statusCode);
                pushNotificationBean.setStatusMessage(statusMessage);
                pushNotificationBean.setToReconcilie(true);

                entityManager.merge(pushNotificationBean);
            }

            PushNotificationResult pushNotificationResult = pushNotificationService.publishMessage(arnEndpoint, notificationMessage, false, false);
            publishMessageID = pushNotificationResult.getMessageId();
            sendingTimestamp = pushNotificationResult.getRequestTimestamp();

            if (pushNotificationResult != null && pushNotificationResult.getStatusCode() != null
                    && pushNotificationResult.getStatusCode().equals(com.techedge.mp.pushnotification.adapter.interfaces.StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_SUCCESS)) {
                statusCode = PushNotificationStatusType.DELIVERED;
                pushNotificationBean.setPublishMessageID(publishMessageID);
                pushNotificationBean.setSendingTimestamp(sendingTimestamp);

            }
            else {
                statusCode = PushNotificationStatusType.ERROR;
                pushNotificationBean.setToReconcilie(true);
            }

            statusMessage = pushNotificationResult.getMessage();

            pushNotificationBean.setStatusCode(statusCode);
            pushNotificationBean.setStatusMessage(statusMessage);

            entityManager.merge(pushNotificationBean);
        }

        crmEventBean.setToReconcilie(false);
        entityManager.merge(crmEventBean);

        reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);

        return reconciliationInfo;
    }

    @Override
    public String getStatus() {
        return StatusHelper.CRM_EVENT_STATUS_ERROR;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "CRM EVENT";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<CRMEventBean> crmEventBeanList = QueryRepository.findCRMEventToReconcilie(entityManager);
        return crmEventBeanList;
    }

    @SuppressWarnings("rawtypes")
    private Object getCommandParameter(HashMap<UserProfile.Parameter, Object> commandParameters, UserProfile.Parameter parameter, Class objectType) throws Exception {
        Object value = null;
        Object parameterValue = commandParameters.get(parameter);

        if (objectType.equals(Date.class)) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            value = sdf.parse((String) parameterValue);

        }
        else if (objectType.equals(Boolean.class)) {
            String stringParameterValue = (String) parameterValue;
            value = (stringParameterValue.equals("1") || stringParameterValue.equalsIgnoreCase("true") ? Boolean.TRUE : Boolean.FALSE);
        }
        else if (objectType.equals(Integer.class)) {
            value = Integer.valueOf((String) parameterValue);
        }
        else if (objectType.equals(Double.class)) {
            value = new Double((String) parameterValue);
        }
        else {
            value = (String) parameterValue;
        }

        return value;
    }

}
