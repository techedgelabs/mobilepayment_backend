package com.techedge.mp.core.actions.reconciliation.status.user;

import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public abstract class ReconciliationUserStatus {

    protected EntityManager                            entityManager                      = null;
    protected DWHAdapterServiceRemote                  dwhAdapterService                  = null;
    protected CRMAdapterServiceRemote                  crmAdapterService                  = null;
    protected PushNotificationServiceRemote            pushNotificationService            = null;
    protected FidelityServiceRemote                    fidelityService                    = null;
    protected EmailSenderRemote                        emailSender                        = null;
    protected RefuelingNotificationServiceRemote       refuelingNotificationService       = null;
    protected RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2 = null;
    protected ParametersServiceRemote                  parametersService                  = null;
    protected String                                   proxyHost                          = null;
    protected String                                   proxyPort                          = null;
    protected String                                   proxyNoHosts                       = null;
    protected Integer                                  pushNotificationExpiryTime         = null;
    protected Integer                                  reconciliationAttemptsLeft         = null;
    protected Boolean                                  refuelingOauth2Active              = null;

    public ReconciliationUserStatus(EntityManager entityManager, DWHAdapterServiceRemote dwhAdapterService, CRMAdapterServiceRemote crmAdapterService, 
            PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, ParametersServiceRemote parametersService, String proxyHost, String proxyPort, String proxyNoHosts, 
            Integer pushNotificationExpiryTime, Integer reconciliationAttemptsLeft, Boolean refuelingOauth2Active) {

        this.entityManager = entityManager;
        this.dwhAdapterService = dwhAdapterService;
        this.crmAdapterService = crmAdapterService;
        this.pushNotificationService = pushNotificationService;
        this.fidelityService = fidelityService;
        this.emailSender = emailSender;
        this.refuelingNotificationService = refuelingNotificationService;
        this.refuelingNotificationServiceOAuth2 = refuelingNotificationServiceOAuth2;
        this.parametersService = parametersService;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.proxyNoHosts = proxyNoHosts;
        this.reconciliationAttemptsLeft = reconciliationAttemptsLeft;
        this.pushNotificationExpiryTime = pushNotificationExpiryTime;
        this.refuelingOauth2Active = refuelingOauth2Active;
    }

    public abstract ReconciliationInfo reconciliate(UserReconciliationInterfaceBean userReconciliationInterfaceBean) throws Exception;

    public boolean checkAttemptsLeft(UserReconciliationInterfaceBean userReconciliationInterfaceBean) {
        boolean check = true;

        if (userReconciliationInterfaceBean.getReconciliationAttemptsLeft() <= 0) {
            userReconciliationInterfaceBean.setToReconcilie(false);
            entityManager.merge(userReconciliationInterfaceBean);

            check = false;
        }

        return check;
    }

    public int decreaseAttempts(UserReconciliationInterfaceBean userReconciliationInterfaceBean) {
        System.out.println("decreaseAttempts");

        int attempts = userReconciliationInterfaceBean.getReconciliationAttemptsLeft().intValue();
        int returnAttempts;

        if (attempts < 1)
            return -1;

        userReconciliationInterfaceBean.setReconciliationAttemptsLeft((attempts - 1));
        entityManager.merge(userReconciliationInterfaceBean);
        returnAttempts = (attempts - 1);

        System.out.println("Decremento dei tentativi di riconciliazione: " + attempts + " -> " + returnAttempts);

        return returnAttempts;
    }

    public abstract String getStatus();

    public abstract String getSubStatus();

    public abstract String getName();

    @SuppressWarnings("rawtypes")
    public abstract List getTransactionsList();
}
