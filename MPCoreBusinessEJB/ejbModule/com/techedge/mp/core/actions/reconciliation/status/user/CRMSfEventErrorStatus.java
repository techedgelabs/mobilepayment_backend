package com.techedge.mp.core.actions.reconciliation.status.user;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import org.json.JSONException;
import org.json.JSONObject;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.crm.CRMSfNotifyEvent;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;
import com.techedge.mp.core.business.model.crm.CRMEventBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfEventBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.crm.adapter.interfaces.NotifyEventResult;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public class CRMSfEventErrorStatus extends ReconciliationUserStatus {

    public CRMSfEventErrorStatus(EntityManager entityManager, DWHAdapterServiceRemote dwhAdapterService, CRMAdapterServiceRemote crmAdapterService, PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, ParametersServiceRemote parametersService, String proxyHost, String proxyPort, String proxyNoHosts, Integer pushNotificationExpiryTime, Integer reconciliationAttemptsLeft, Boolean refuelingOauth2Active) {

        super(entityManager, dwhAdapterService, crmAdapterService, pushNotificationService, fidelityService, emailSender, refuelingNotificationService, refuelingNotificationServiceOAuth2, parametersService, proxyHost, proxyPort, proxyNoHosts, pushNotificationExpiryTime, reconciliationAttemptsLeft, refuelingOauth2Active);
    }

    @Override
    public ReconciliationInfo reconciliate(UserReconciliationInterfaceBean userReconciliationInterfaceBean) throws Exception {
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        CRMSfEventBean crmSfEventBean = (CRMSfEventBean) userReconciliationInterfaceBean;

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat sdf_complex = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
        
        String requestJson = crmSfEventBean.getRequestJson();

        JSONObject jsonObj = new JSONObject(requestJson);
        String requestId = getJsonStringProperty(jsonObj, "requestId");
        String fiscalCode = getJsonStringProperty(jsonObj, "fiscalCode");
        String dateString = getJsonStringProperty(jsonObj, "date");
        
        Date date = null;
        try {
            date = sdf.parse(dateString);
        }
        catch (ParseException ex) {
            
            System.out.println("Errore nel parsing della data -> utilizzo secondo formato.");
            
            try {
                date = sdf_complex.parse(dateString);
                System.out.println("Parsing della data effettuato con successo utilizzando il secondo formato.");
            }
            catch (ParseException ex2) {
                
                System.out.println("Errore nel parsing della data utilizzo il secondo formato.");
                
                NotifyEventResponse notifyEventResponse = new NotifyEventResponse();
                
                notifyEventResponse.setSuccess(false);
                notifyEventResponse.setErrorCode("500");
                notifyEventResponse.setMessage(ex.getMessage());
                notifyEventResponse.setRequestId(requestId);
                crmSfEventBean.setToReconcilie(false);
                
                crmSfEventBean.setSuccess(notifyEventResponse.getSuccess());
                crmSfEventBean.setErrorCode(notifyEventResponse.getErrorCode());
                crmSfEventBean.setMessage(notifyEventResponse.getMessage());
                entityManager.merge(crmSfEventBean);

                reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                
                System.out.println("crmResponse succes:" + notifyEventResponse.getSuccess());
                System.out.println("crmResponse errorCode:" + notifyEventResponse.getErrorCode());
                System.out.println("crmResponse message:" + notifyEventResponse.getMessage());
                System.out.println("crmResponse requestId:" + notifyEventResponse.getRequestId());

                reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);

                entityManager.merge(crmSfEventBean);

                return reconciliationInfo;
            }
        }
        
        String stationId = getJsonStringProperty(jsonObj, "stationId");
        String productId = getJsonStringProperty(jsonObj, "productId");
        Boolean paymentFlag = getJsonBooleanProperty(jsonObj, "paymentFlag");
        Integer credits = getJsonIntegerProperty(jsonObj, "credits");
        
        Double quantity = null;
        Object quantityObject = jsonObj.get("quantity");
        if (quantityObject instanceof Double) {
            quantity = (Double) quantityObject;
        }
        else {
            if (quantityObject instanceof Integer) {
                Integer quantityInteger = (Integer) quantityObject;
                quantity = Double.parseDouble(quantityInteger.toString());
            }
            else {
                System.out.println("Errore conversione quantity: impossibile convertire " + quantityObject.toString() + " a Double");
            }
        }
        
        String refuelMode = getJsonStringProperty(jsonObj, "refuelMode");
        String firstName = getJsonStringProperty(jsonObj, "firstName");
        String lastName = getJsonStringProperty(jsonObj, "lastName");
        String email = getJsonStringProperty(jsonObj, "email");
        String birthDateString = getJsonStringProperty(jsonObj, "birthDate");
        Date birthDate = null;
        if (birthDateString != null && !birthDateString.isEmpty()) {
            birthDate = sdf.parse(birthDateString);
        }
        Boolean notificationFlag = getJsonBooleanProperty(jsonObj, "notificationFlag");
        Boolean paymentCardFlag = getJsonBooleanProperty(jsonObj, "paymentCardFlag");
        String brand = getJsonStringProperty(jsonObj, "brand");
        String cluster = getJsonStringProperty(jsonObj, "cluster");
        
        Double amount = null;
        Object amountObject = jsonObj.get("amount");
        if (amountObject instanceof Double) {
            amount = (Double) amountObject;
        }
        else {
            if (amountObject instanceof Integer) {
                Integer amountInteger = (Integer) amountObject;
                amount = Double.parseDouble(amountInteger.toString());
            }
            else {
                System.out.println("Errore conversione amount: impossibile convertire " + amountObject.toString() + " a Double");
            }
        }
        
        Boolean privacyFlag1 = getJsonBooleanProperty(jsonObj, "privacyFlag1");
        Boolean privacyFlag2 = getJsonBooleanProperty(jsonObj, "privacyFlag2");
        String mobilePhone = getJsonStringProperty(jsonObj, "mobilePhone");
        String loyaltyCard = getJsonStringProperty(jsonObj, "loyaltyCard");
        String eventType = getJsonStringProperty(jsonObj, "eventType");
        String parameter1 = getJsonStringProperty(jsonObj, "parameter1");
        String parameter2 = getJsonStringProperty(jsonObj, "parameter2");
        String paymentMode = getJsonStringProperty(jsonObj, "paymentMode");

        NotifyEventResponse notifyEventResponse = new NotifyEventResponse();

        try {

            NotifyEventResult response = crmAdapterService.notifyEvent(requestId, fiscalCode, date, stationId, productId, paymentFlag, credits, quantity, refuelMode, firstName,
                    lastName, email, birthDate, notificationFlag, paymentCardFlag, brand, cluster, amount, privacyFlag1, privacyFlag2, mobilePhone, loyaltyCard, eventType,
                    parameter1, parameter2, paymentMode);

            System.out.println("risposta del CRM SF: success=" + response.getSuccess() + " errorCode=" + response.getErrorCode() + " message=" + response.getMessage());

            notifyEventResponse.setSuccess(response.getSuccess());
            notifyEventResponse.setErrorCode(response.getErrorCode());
            notifyEventResponse.setMessage(response.getMessage());
            notifyEventResponse.setRequestId(requestId);

            if (response.getSuccess()) {

                crmSfEventBean.setToReconcilie(false);
                crmSfEventBean.setSuccess(notifyEventResponse.getSuccess());
                crmSfEventBean.setErrorCode(notifyEventResponse.getErrorCode());
                crmSfEventBean.setMessage(notifyEventResponse.getMessage());
                entityManager.merge(crmSfEventBean);

                reconciliationInfo.setFinalStatusType("SUCCESS");
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
            }
            else {

                crmSfEventBean.setToReconcilie(false);
                crmSfEventBean.setSuccess(notifyEventResponse.getSuccess());
                crmSfEventBean.setErrorCode(notifyEventResponse.getErrorCode());
                crmSfEventBean.setMessage(notifyEventResponse.getMessage());
                entityManager.merge(crmSfEventBean);

                reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
            }
        }
        catch (Exception ex) {

            System.out.println("Rilevata eccezione sul flusso di riconciliazione CRM SF: " + ex.getMessage());

            notifyEventResponse.setSuccess(false);
            notifyEventResponse.setErrorCode("500");
            notifyEventResponse.setMessage(ex.getMessage());
            notifyEventResponse.setRequestId(requestId);

            System.out.println("La transazione resta da riconciliare e si decrementa il numero di tentativi residui");

            decreaseAttempts(crmSfEventBean);

            crmSfEventBean.setToReconcilie(true);
            reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

        }

        System.out.println("crmResponse succes:" + notifyEventResponse.getSuccess());
        System.out.println("crmResponse errorCode:" + notifyEventResponse.getErrorCode());
        System.out.println("crmResponse message:" + notifyEventResponse.getMessage());
        System.out.println("crmResponse requestId:" + notifyEventResponse.getRequestId());

        reconciliationInfo.setFinalStatusType(userReconciliationInterfaceBean.getFinalStatus());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);

        entityManager.merge(crmSfEventBean);

        return reconciliationInfo;
    }

    @Override
    public String getStatus() {
        return StatusHelper.CRM_EVENT_STATUS_ERROR;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "CRM SF EVENT";
    }
    
    public static String getJsonStringProperty(JSONObject jsonObject, String propertyName) {
        String propertyValue = "";
        try {
            propertyValue = (String) jsonObject.get(propertyName);
        }
        catch(JSONException jex) {
        }
        return propertyValue;
    }
    
    public static Boolean getJsonBooleanProperty(JSONObject jsonObject, String propertyName) {
        Boolean propertyValue = null;
        try {
            propertyValue = (Boolean) jsonObject.get(propertyName);
        }
        catch(JSONException jex) {
        }
        return propertyValue;
    }
    
    public static Integer getJsonIntegerProperty(JSONObject jsonObject, String propertyName) {
        Integer propertyValue = null;
        try {
            propertyValue = (Integer) jsonObject.get(propertyName);
        }
        catch(JSONException jex) {
        }
        return propertyValue;
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<CRMSfEventBean> crmSfEventBeanList = QueryRepository.findCRMSfEventToReconcilie(entityManager);
        return crmSfEventBeanList;
    }

}
