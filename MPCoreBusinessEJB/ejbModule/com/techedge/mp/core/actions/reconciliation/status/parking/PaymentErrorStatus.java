package com.techedge.mp.core.actions.reconciliation.status.parking;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.actions.parking.v2.PARKING_TRANSACTION_ITEM_EVENT_STATUS;
import com.techedge.mp.core.actions.parking.v2.PARKING_TRANSACTION_ITEM_STATUS;
import com.techedge.mp.core.actions.parking.v2.PARKING_TRANSACTION_ITEM_STATUS_STATUS;
import com.techedge.mp.core.actions.parking.v2.PARKING_TRANSACTION_STATUS_CONST;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.CardDepositTransactionBean;
import com.techedge.mp.core.business.model.ParkingTransactionInterfaceBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionItemEventBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionStatusBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

public class PaymentErrorStatus extends ReconciliationParkingStatus {

    public PaymentErrorStatus(EntityManager entityManager, GPServiceRemote gpService, EmailSenderRemote emailSender, ParametersServiceRemote parametersService, String proxyHost, String proxyPort, String proxyNoHosts, Integer reconciliationAttemptsLeft, String secretKey) {

        super(entityManager, gpService, emailSender, parametersService, proxyHost, proxyPort, proxyNoHosts, reconciliationAttemptsLeft, secretKey);
    }

    @Override
    public ReconciliationInfo reconciliate(ParkingTransactionInterfaceBean parkingTransactionInterfaceBean) {
        
        ParkingTransactionBean parkingTransactionBean = (ParkingTransactionBean) parkingTransactionInterfaceBean;
        
        GestPayData gestPayData = null;
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        
        System.out.println("**********************************");
        System.out.println("* Elaborazione record cancellati *");
        System.out.println("**********************************");
        
        // Cicla su tutti gli item della transazione
        for (ParkingTransactionItemBean parkingTransactionItemBean : parkingTransactionBean.getParkingTransactionItemList()) {
            
            System.out.println("Elaborazione item con id: " + parkingTransactionItemBean.getId());
            
            // Se l'item si trova in stato CANCELED bisogna verificare che sia stato effettivamente cancellato
            if (parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue())) {
            
                System.out.println("Trovato item in stato CANCELED");
                
                System.out.println("Ottieni lo stato della transazione di pagamento");
                
                GestPayData gestPayDataReadTrx = gpService.callReadTrx(parkingTransactionItemBean.getShopLogin(), parkingTransactionItemBean.getParkingTransactionItemId(),
                        parkingTransactionItemBean.getBankTansactionID(), parkingTransactionItemBean.getAcquirerID(), parkingTransactionItemBean.getGroupAcquirer(), parkingDecodedSecretKey);

                if (gestPayDataReadTrx == null) {
                    System.out.println("errore nella operazione di riconciliazione, chiamata callReadTrx nulla verso l'acquirer(transazione: "
                            + parkingTransactionItemBean.getParkingTransactionItemId() + ")");
                    
                    decreaseAttempts(parkingTransactionInterfaceBean);

                    reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                    return reconciliationInfo;
                }
                
                if (gestPayDataReadTrx.getErrorCode().equals("2") && gestPayDataReadTrx.getErrorDescription().equalsIgnoreCase("Nessun ordine trovato")) {
                    
                    System.out.println("Non � stata trovata nessuna transazione di pagamento associata a questo item");
                    
                    // Se esiste l'evento AUT crea il corrispondente evento CAN
                    System.out.println("Recupera l'evento AUT");
                    ParkingTransactionItemEventBean autParkingTransactionItemEventBean = null;
                    
                    for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : parkingTransactionItemBean.getParkingTransactionItemEventList()) {
                        
                        if(parkingTransactionItemEventBean.getEventType().equals("AUT")) {
                            autParkingTransactionItemEventBean = parkingTransactionItemEventBean;
                            break;
                        }
                    }
                    
                    if (autParkingTransactionItemEventBean != null) {
                        System.out.println("Evento AUT trovato");
                        
                        ParkingTransactionItemEventBean canParkingTransactionItemEventBean = new ParkingTransactionItemEventBean();

                        Integer canEventSequenceID = parkingTransactionItemBean.getLastEventSequenceID() + 1;
                        
                        canParkingTransactionItemEventBean.setSequenceID(canEventSequenceID);
                        canParkingTransactionItemEventBean.setErrorCode("0");
                        canParkingTransactionItemEventBean.setErrorDescription("");
                        canParkingTransactionItemEventBean.setParkingTransactionItemBean(parkingTransactionItemBean);
                        canParkingTransactionItemEventBean.setTransactionResult("OK");
                        canParkingTransactionItemEventBean.setEventType("CAN");
                        canParkingTransactionItemEventBean.setEventAmount(autParkingTransactionItemEventBean.getEventAmount());

                        entityManager.persist(canParkingTransactionItemEventBean);
                    }
                    
                    continue;
                }
                    
                if (gestPayDataReadTrx.getTransactionState().equals("AUT") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK")) {
                
                    System.out.println("La transazione di pagamento si trova in stato AUT --> Cancella l'autorizzazione e crea l'evento CAN");
                    
                    System.out.println("Recupera l'evento AUT");
                    ParkingTransactionItemEventBean autParkingTransactionItemEventBean = null;
                    
                    for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : parkingTransactionItemBean.getParkingTransactionItemEventList()) {
                        
                        if(parkingTransactionItemEventBean.getEventType().equals("AUT")) {
                            autParkingTransactionItemEventBean = parkingTransactionItemEventBean;
                            break;
                        }
                    }
                    
                    if (autParkingTransactionItemEventBean == null) {
                        System.out.println("Evento AUT non trovato");
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);

                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                    
                    // Se la transazione esiste e si trova in stato autorizzato cancella l'autorizzazione, crea lo stato CAN e chiudi l'item
                    PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(entityManager, parkingTransactionItemBean.getPaymentMethodId(), parkingTransactionItemBean.getPaymentMethodType());

                    if (paymentInfoBean == null) {
                        System.out.println("Metodo di pagamento non trovato");
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);

                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                    
                    Integer eventSequenceID = parkingTransactionItemBean.getLastEventSequenceID() + 1;
                    Integer statusSequenceID = parkingTransactionBean.getLastStatusSequenceID() + 1;

                    Date now = new Date();
                    String requestId = String.valueOf(now.getTime());
                    GestPayData gestPayDataDELETEResponse = doPaymentAuthDelete(parkingTransactionItemBean, paymentInfoBean, parkingTransactionBean.getUserBean(), paymentInfoBean.getId(),
                            autParkingTransactionItemEventBean.getEventAmount().doubleValue(), statusSequenceID, requestId, eventSequenceID, gpService,
                            parkingDecodedSecretKey);

                    System.out.println("Importo di Preautorizzazione :: " + autParkingTransactionItemEventBean.getEventAmount().doubleValue());
                    System.out.println("Tentativo di Storno => importo :: " + autParkingTransactionItemEventBean.getEventAmount().doubleValue());

                    if (gestPayDataDELETEResponse.getTransactionResult().equals("ERROR")) {

                        System.out.println("Errore nella cancellazione dell'autorizzazione del pagamento");
                        
                        decreaseAttempts(parkingTransactionInterfaceBean);
                        /*
                        ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH ERROR",
                                gestPayDataDELETEResponse.getErrorDescription());
                        entityManager.persist(parkingTransactionStatusBean);
                        */
                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                        return reconciliationInfo;
                    }

                    if (gestPayDataDELETEResponse.getTransactionResult().equals("KO")) {

                        System.out.println("KO nella cancellazione dell'autorizzazione del pagamento");
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);
                        /*
                        ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH KO",
                                gestPayDataDELETEResponse.getErrorDescription());
                        entityManager.persist(parkingTransactionStatusBean);
                        */
                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                }
                else {
                    
                    if (gestPayDataReadTrx.getTransactionState().equals("CAN") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK")) {
                        
                        // se la transazione � stata cancellata cerca l'evento CAN e se non esiste generalo e chiudi l'item
                        System.out.println("La transazione di pagamento si trova in stato CAN --> Crea l'evento CAN se non esiste");
                        
                        System.out.println("Recupera l'evento CAN");
                        ParkingTransactionItemEventBean canParkingTransactionItemEventBean = null;
                        
                        for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : parkingTransactionItemBean.getParkingTransactionItemEventList()) {
                            
                            if(parkingTransactionItemEventBean.getEventType().equals("CAN") && parkingTransactionItemEventBean.getTransactionResult().equals("OK")) {
                                canParkingTransactionItemEventBean = parkingTransactionItemEventBean;
                                break;
                            }
                        }
                        
                        if (canParkingTransactionItemEventBean == null) {
                            System.out.println("Evento CAN non trovato");
                            
                            Integer canEventSequenceID = parkingTransactionItemBean.getLastEventSequenceID() + 1;
                            Double amount = Double.parseDouble(gestPayDataReadTrx.getAmount());
                            
                            canParkingTransactionItemEventBean = this.generateParkingTransactionItemEvent(amount, canEventSequenceID, parkingTransactionItemBean, gestPayDataReadTrx,
                                    "CAN");

                            entityManager.persist(canParkingTransactionItemEventBean);
                            
                        }
                        else {
                            System.out.println("Evento CAN gi� presente");
                        }
                    }
                    else {
                        
                        // se la transazione si trova in uno degli altri stati non bisogna fare nulla
                    }
                }
            }
        }
        
        // Recupera il prezzo finale della sosta
        BigDecimal finalPrice = parkingTransactionBean.getFinalPrice();
        Double _parkingPrice = finalPrice.doubleValue();
        
        System.out.println("Importo totale sosta: " + finalPrice);
        
        // Ordina gli item per timestamp
        List<ParkingTransactionItemBean> orderedParkingTransactionItemList = new ArrayList<ParkingTransactionItemBean>(0);
        orderedParkingTransactionItemList.addAll(parkingTransactionBean.getParkingTransactionItemList());

        Collections.sort(orderedParkingTransactionItemList, new Comparator<ParkingTransactionItemBean>() {
            @Override
            public int compare(ParkingTransactionItemBean parkingTransactionItemBean1, ParkingTransactionItemBean parkingTransactionItemBean2) {

                return (parkingTransactionItemBean1.getCreationTimestamp().compareTo(parkingTransactionItemBean2.getCreationTimestamp()));
            }
        });

        System.out.println("************************************************");
        System.out.println("* Elaborazione autorizzazioni e movimentazioni *");
        System.out.println("************************************************");
        
        // Cicla gli item in stato AUTHORIZED e SETTLED
        if (_parkingPrice > 0.0) {

            System.out.println("Importo sosta " + _parkingPrice);

            // Movimenta gli AUTHORIZED se non sono stati gi� movimentati e verifica la movimentazione dei SETTLED fino ad arrivare all'importo della sosta

            boolean stopLooping = false;
            for (ParkingTransactionItemBean parkingTransactionItemBean : orderedParkingTransactionItemList) {

                if (stopLooping)
                    break;

                System.out.println("Elaborazione item con id: " + parkingTransactionItemBean.getId());
                
                System.out.println("parkingPrice residuo            : " + _parkingPrice);
                _parkingPrice = (double) Math.round(_parkingPrice * 100) / 100;
                System.out.println("parkingPrice residuo arrotondato: " + _parkingPrice);
                
                if (parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue())) {
                        
                    System.out.println("Trovato item in stato SETTLED");
                    
                    PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(entityManager, parkingTransactionItemBean.getPaymentMethodId(),
                            parkingTransactionItemBean.getPaymentMethodType());

                    if (paymentInfoBean == null) {

                        System.out.println("Non � stato trovato il metodo di pagamento associato all'item");
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);

                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                    
                    System.out.println("Ottieni lo stato della transazione di pagamento");
                    
                    GestPayData gestPayDataReadTrx = gpService.callReadTrx(parkingTransactionItemBean.getShopLogin(), parkingTransactionItemBean.getParkingTransactionItemId(),
                            parkingTransactionItemBean.getBankTansactionID(), parkingTransactionItemBean.getAcquirerID(), parkingTransactionItemBean.getGroupAcquirer(), parkingDecodedSecretKey);

                    if (gestPayDataReadTrx == null) {
                        System.out.println("errore nella operazione di riconciliazione, chiamata callReadTrx nulla verso l'acquirer(transazione: "
                                + parkingTransactionItemBean.getParkingTransactionItemId() + ")");
                        
                        decreaseAttempts(parkingTransactionInterfaceBean);

                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                        return reconciliationInfo;
                    }
                    
                    if (gestPayDataReadTrx.getErrorCode().equals("2") && gestPayDataReadTrx.getErrorDescription().equalsIgnoreCase("Nessun ordine trovato")) {
                        
                        System.out.println("Non � stata trovata nessuna transazione di pagamento associata a un item SETTLED --> NOT_RECONCILIABLE");
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);

                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                    
                    if (gestPayDataReadTrx.getTransactionState().equals("MOV") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK")) {
                            
                        System.out.println("La transazione di pagamento si trova in stato MOV --> Scala l'importo dal parkingPrice");
                    
                        System.out.println("Recupera l'evento MOV");
                        ParkingTransactionItemEventBean movParkingTransactionItemEventBean = null;
                        
                        for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : parkingTransactionItemBean.getParkingTransactionItemEventList()) {
                            
                            if(parkingTransactionItemEventBean.getEventType().equals("MOV")) {
                                movParkingTransactionItemEventBean = parkingTransactionItemEventBean;
                                break;
                            }
                        }
                        
                        if (movParkingTransactionItemEventBean == null) {
                            System.out.println("Evento MOV non trovato");
                            
                            parkingTransactionInterfaceBean.setToReconcilie(false);
                            parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                            entityManager.merge(parkingTransactionInterfaceBean);

                            reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                            return reconciliationInfo;
                        }
                        
                        Double eventAmount = movParkingTransactionItemEventBean.getEventAmount();
                        
                        _parkingPrice = _parkingPrice - eventAmount;
                        if (_parkingPrice == 0.0) {
                            stopLooping = true;
                        }
                        else {
                            stopLooping = false;
                        }
                    }
                    else {
                    
                        System.out.println("Transazione di pagamento in stato " + gestPayDataReadTrx.getTransactionState() + " e result " + gestPayDataReadTrx.getTransactionResult());
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);

                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                }
                else {
                    
                    if (parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue())) {
                        
                        System.out.println("Trovato item in stato AUTHORIZED");
                        
                        if (parkingTransactionItemBean.getPaymentMethodId() == null || parkingTransactionItemBean.getPaymentMethodType() == null) {
                            System.out.println("Rilevato item in stato AUTHORIZED senza l'operazione di autorizzazione (importo inferiore a soglia) --> skip");
                            continue;
                        }
                        
                        PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(entityManager, parkingTransactionItemBean.getPaymentMethodId(),
                                parkingTransactionItemBean.getPaymentMethodType());

                        if (paymentInfoBean == null) {

                            System.out.println("Non � stato trovato il metodo di pagamento associato all'item");
                            
                            parkingTransactionInterfaceBean.setToReconcilie(false);
                            parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                            entityManager.merge(parkingTransactionInterfaceBean);

                            reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                            return reconciliationInfo;
                        }
                        
                        System.out.println("Ottieni lo stato della transazione di pagamento");
                        
                        GestPayData gestPayDataReadTrx = gpService.callReadTrx(parkingTransactionItemBean.getShopLogin(), parkingTransactionItemBean.getParkingTransactionItemId(),
                                parkingTransactionItemBean.getBankTansactionID(), parkingTransactionItemBean.getAcquirerID(), parkingTransactionItemBean.getGroupAcquirer(), parkingDecodedSecretKey);

                        if (gestPayDataReadTrx == null) {
                            System.out.println("errore nella operazione di riconciliazione, chiamata callReadTrx nulla verso l'acquirer(transazione: "
                                    + parkingTransactionItemBean.getParkingTransactionItemId() + ")");
                            
                            decreaseAttempts(parkingTransactionInterfaceBean);

                            reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                            return reconciliationInfo;
                        }
                        
                        if (gestPayDataReadTrx.getErrorCode().equals("2") && gestPayDataReadTrx.getErrorDescription().equalsIgnoreCase("Nessun ordine trovato")) {
                            
                            System.out.println("Non � stata trovata nessuna transazione di pagamento associata a un item SETTLED --> NOT_RECONCILIABLE");
                            
                            parkingTransactionInterfaceBean.setToReconcilie(false);
                            parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                            entityManager.merge(parkingTransactionInterfaceBean);

                            reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                            return reconciliationInfo;
                        }
                        
                        if (gestPayDataReadTrx.getTransactionState().equals("AUT") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK")) {
                            
                            System.out.println("Recupera l'evento AUT");
                            ParkingTransactionItemEventBean autParkingTransactionItemEventBean = null;
                            
                            for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : parkingTransactionItemBean.getParkingTransactionItemEventList()) {
                                
                                if(parkingTransactionItemEventBean.getEventType().equals("AUT")) {
                                    autParkingTransactionItemEventBean = parkingTransactionItemEventBean;
                                    break;
                                }
                            }
                            
                            if (autParkingTransactionItemEventBean == null) {
                                System.out.println("Evento AUT non trovato");
                                
                                parkingTransactionInterfaceBean.setToReconcilie(false);
                                parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                                entityManager.merge(parkingTransactionInterfaceBean);

                                reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                                return reconciliationInfo;
                            }
                            
                            // Movimenta la transazione e scala l'importo dal parkingPrice
                            if (_parkingPrice <= autParkingTransactionItemEventBean.getEventAmount()) {
                                
                                System.out.println("Movimentazione di " + _parkingPrice + " di " + autParkingTransactionItemEventBean.getEventAmount() + " autorizzati");

                                Integer movStatusSequenceID = parkingTransactionBean.getLastStatusSequenceID() + 1;
                                Integer movEventSequenceID  = parkingTransactionItemBean.getLastEventSequenceID() + 1;
                                
                                Date now = new Date();
                                String requestId = String.valueOf(now.getTime());
                                
                                GestPayData gestPayDataSETTLEResponse = this.doPaymentSettle(parkingTransactionItemBean, paymentInfoBean, parkingTransactionBean.getUserBean(), paymentInfoBean.getId(),
                                        _parkingPrice, movStatusSequenceID, requestId, movEventSequenceID, gpService, parkingDecodedSecretKey);

                                if (gestPayDataSETTLEResponse.getTransactionResult().equals("ERROR")) {

                                    System.out.println("Errore nella movimentazione");
                                    
                                    decreaseAttempts(parkingTransactionInterfaceBean);

                                    reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                                    return reconciliationInfo;
                                }

                                if (gestPayDataSETTLEResponse.getTransactionResult().equals("KO")) {

                                    System.out.println("Ko nella movimentazione");
                                    
                                    parkingTransactionInterfaceBean.setToReconcilie(false);
                                    parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                                    entityManager.merge(parkingTransactionInterfaceBean);

                                    reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                                    return reconciliationInfo;
                                }

                                System.out.println("Movimentazione con carta di credito effettuata con successo");

                                //Imposto lo status del parkingTransactionItem a SETTLED se la transazione avviene con successo
                                parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue());
                                entityManager.merge(parkingTransactionItemBean);
                                
                                _parkingPrice = 0.0;

                                stopLooping = true;
                                break;
                            }
                            else {
                                //l'importo preautorizzato dell'item � inferiore al valore dell'importo sosta parkingPrice
                                
                                System.out.println("Movimentazione di " + autParkingTransactionItemEventBean.getEventAmount() + " di " + autParkingTransactionItemEventBean.getEventAmount() + " autorizzati");

                                Integer movStatusSequenceID = parkingTransactionBean.getLastStatusSequenceID() + 1;
                                Integer movEventSequenceID  = parkingTransactionItemBean.getLastEventSequenceID() + 1;
                                
                                Date now = new Date();
                                String requestId = String.valueOf(now.getTime());
                                
                                GestPayData gestPayDataSETTLEResponse = this.doPaymentSettle(parkingTransactionItemBean, paymentInfoBean, parkingTransactionBean.getUserBean(), paymentInfoBean.getId(),
                                        autParkingTransactionItemEventBean.getEventAmount(), movStatusSequenceID, requestId, movEventSequenceID, gpService, parkingDecodedSecretKey);

                                if (gestPayDataSETTLEResponse.getTransactionResult().equals("ERROR")) {

                                    System.out.println("Errore nella movimentazione");
                                    
                                    decreaseAttempts(parkingTransactionInterfaceBean);

                                    reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                                    return reconciliationInfo;
                                }

                                if (gestPayDataSETTLEResponse.getTransactionResult().equals("KO")) {

                                    System.out.println("Ko nella movimentazione");
                                    
                                    parkingTransactionInterfaceBean.setToReconcilie(false);
                                    parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                                    entityManager.merge(parkingTransactionInterfaceBean);

                                    reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                                    return reconciliationInfo;
                                }

                                //Imposto lo status del parkingTransactionItem a SETTLED se la transazione avviene con successo
                                parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue());
                                entityManager.merge(parkingTransactionItemBean);
                                
                                //aggiornamento del prezzo sosta residuo
                                _parkingPrice = _parkingPrice - autParkingTransactionItemEventBean.getEventAmount();
                                stopLooping = false;
                            }
                        }
                        else {
                            
                            if (gestPayDataReadTrx.getTransactionState().equals("MOV") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK")) {
                                
                                System.out.println("La transazione di pagamento si trova in stato MOV --> Scala l'importo dal parkingPrice");
                                
                                System.out.println("Recupera l'evento MOV");
                                ParkingTransactionItemEventBean movParkingTransactionItemEventBean = null;
                                
                                for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : parkingTransactionItemBean.getParkingTransactionItemEventList()) {
                                    
                                    if(parkingTransactionItemEventBean.getEventType().equals("MOV")) {
                                        movParkingTransactionItemEventBean = parkingTransactionItemEventBean;
                                        break;
                                    }
                                }
                                
                                if (movParkingTransactionItemEventBean == null) {
                                    System.out.println("Evento MOV non trovato");
                                    
                                    parkingTransactionInterfaceBean.setToReconcilie(false);
                                    parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                                    entityManager.merge(parkingTransactionInterfaceBean);

                                    reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                                    return reconciliationInfo;
                                }
                                
                                ParkingTransactionItemEventBean newMovParkingTransactionItemEventBean = this.generateParkingTransactionItemEvent(movParkingTransactionItemEventBean.getEventAmount(),
                                        movParkingTransactionItemEventBean.getSequenceID() + 1, parkingTransactionItemBean, gestPayDataReadTrx, "MOV");
                                entityManager.persist(newMovParkingTransactionItemEventBean);
                                
                                parkingTransactionItemBean.getParkingTransactionItemEventList().add(newMovParkingTransactionItemEventBean);
                                parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue());
                                entityManager.merge(parkingTransactionItemBean);
                                
                                Double eventAmount = movParkingTransactionItemEventBean.getEventAmount();
                                
                                _parkingPrice = _parkingPrice - eventAmount;
                                if (_parkingPrice == 0.0) {
                                    stopLooping = true;
                                }
                                else  {
                                    stopLooping = false;
                                }
                            }
                            else {
                                
                                System.out.println("Transazione di pagamento in stato " + gestPayDataReadTrx.getTransactionState() + " e result " + gestPayDataReadTrx.getTransactionResult());
                                
                                parkingTransactionInterfaceBean.setToReconcilie(false);
                                parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                                entityManager.merge(parkingTransactionInterfaceBean);

                                reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                                return reconciliationInfo;
                            }
                        }
                    }
                    else {
                        
                        System.out.println("Trovato item in stato " + parkingTransactionItemBean.getParkingItemStatus() + " --> non gestito in questa fase");
                    }
                }
            }
        }
        
        
        // Gli eventuali item AUTHORIZED residui devono essere cancellati
        System.out.println("****************************************");
        System.out.println("* Cancellazione autorizzazioni residue *");
        System.out.println("****************************************");
        
        for (ParkingTransactionItemBean parkingTransactionItemBean : orderedParkingTransactionItemList) {
            
            System.out.println("Elaborazione item con id: " + parkingTransactionItemBean.getId());
            
            if (parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue())) {
                    
                System.out.println("Trovato item in stato AUTHORIZED");
                
                System.out.println("Ottieni lo stato della transazione di pagamento");
                
                GestPayData gestPayDataReadTrx = gpService.callReadTrx(parkingTransactionItemBean.getShopLogin(), parkingTransactionItemBean.getParkingTransactionItemId(),
                        parkingTransactionItemBean.getBankTansactionID(), parkingTransactionItemBean.getAcquirerID(), parkingTransactionItemBean.getGroupAcquirer(), parkingDecodedSecretKey);

                if (gestPayDataReadTrx == null) {
                    System.out.println("errore nella operazione di riconciliazione, chiamata callReadTrx nulla verso l'acquirer(transazione: "
                            + parkingTransactionItemBean.getParkingTransactionItemId() + ")");
                    
                    decreaseAttempts(parkingTransactionInterfaceBean);

                    reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                    return reconciliationInfo;
                }
                
                if (gestPayDataReadTrx.getErrorCode().equals("2") && gestPayDataReadTrx.getErrorDescription().equalsIgnoreCase("Nessun ordine trovato")) {
                    
                    System.out.println("Non � stata trovata nessuna transazione di pagamento associata a questo item");
                    
                    // Se esiste l'evento AUT crea il corrispondente evento CAN
                    System.out.println("Recupera l'evento AUT");
                    ParkingTransactionItemEventBean autParkingTransactionItemEventBean = null;
                    
                    for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : parkingTransactionItemBean.getParkingTransactionItemEventList()) {
                        
                        if(parkingTransactionItemEventBean.getEventType().equals("AUT")) {
                            autParkingTransactionItemEventBean = parkingTransactionItemEventBean;
                            break;
                        }
                    }
                    
                    if (autParkingTransactionItemEventBean != null) {
                        System.out.println("Evento AUT trovato");
                        
                        ParkingTransactionItemEventBean canParkingTransactionItemEventBean = new ParkingTransactionItemEventBean();

                        Integer canEventSequenceID = parkingTransactionItemBean.getLastEventSequenceID() + 1;
                        
                        canParkingTransactionItemEventBean.setSequenceID(canEventSequenceID);
                        canParkingTransactionItemEventBean.setErrorCode("0");
                        canParkingTransactionItemEventBean.setErrorDescription("");
                        canParkingTransactionItemEventBean.setParkingTransactionItemBean(parkingTransactionItemBean);
                        canParkingTransactionItemEventBean.setTransactionResult("OK");
                        canParkingTransactionItemEventBean.setEventType("CAN");
                        canParkingTransactionItemEventBean.setEventAmount(autParkingTransactionItemEventBean.getEventAmount());

                        entityManager.persist(canParkingTransactionItemEventBean);
                        
                        parkingTransactionItemBean.getParkingTransactionItemEventList().add(canParkingTransactionItemEventBean);
                    }
                    
                    parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue());
                    entityManager.merge(parkingTransactionItemBean);
                    
                    continue;
                }
                    
                if (gestPayDataReadTrx.getTransactionState().equals("CAN") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK")) {
                    
                    System.out.println("La transazione di pagamento si trova in stato CAN --> Crea l'evento CAN se non esiste");
                    
                    // Se esiste l'evento AUT crea il corrispondente evento CAN
                    System.out.println("Recupera l'evento AUT");
                    ParkingTransactionItemEventBean autParkingTransactionItemEventBean = null;
                    
                    for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : parkingTransactionItemBean.getParkingTransactionItemEventList()) {
                        
                        if(parkingTransactionItemEventBean.getEventType().equals("AUT")) {
                            autParkingTransactionItemEventBean = parkingTransactionItemEventBean;
                            break;
                        }
                    }
                    
                    if (autParkingTransactionItemEventBean != null) {
                        System.out.println("Evento AUT trovato");
                        
                        ParkingTransactionItemEventBean canParkingTransactionItemEventBean = new ParkingTransactionItemEventBean();

                        Integer canEventSequenceID = parkingTransactionItemBean.getLastEventSequenceID() + 1;
                        
                        canParkingTransactionItemEventBean.setSequenceID(canEventSequenceID);
                        canParkingTransactionItemEventBean.setErrorCode("0");
                        canParkingTransactionItemEventBean.setErrorDescription("");
                        canParkingTransactionItemEventBean.setParkingTransactionItemBean(parkingTransactionItemBean);
                        canParkingTransactionItemEventBean.setTransactionResult("OK");
                        canParkingTransactionItemEventBean.setEventType("CAN");
                        canParkingTransactionItemEventBean.setEventAmount(autParkingTransactionItemEventBean.getEventAmount());

                        entityManager.persist(canParkingTransactionItemEventBean);
                        
                        parkingTransactionItemBean.getParkingTransactionItemEventList().add(canParkingTransactionItemEventBean);
                    }
                    
                    parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue());
                    entityManager.merge(parkingTransactionItemBean);
                    
                    continue;
                }
                
                if (gestPayDataReadTrx.getTransactionState().equals("AUT") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK")) {
                    
                    System.out.println("La transazione di pagamento si trova in stato AUT --> Cancella l'autorizzazione e crea l'evento CAN se non esiste");
                    
                    System.out.println("Recupera l'evento AUT");
                    ParkingTransactionItemEventBean autParkingTransactionItemEventBean = null;
                    
                    for(ParkingTransactionItemEventBean parkingTransactionItemEventBean : parkingTransactionItemBean.getParkingTransactionItemEventList()) {
                        
                        if(parkingTransactionItemEventBean.getEventType().equals("AUT")) {
                            autParkingTransactionItemEventBean = parkingTransactionItemEventBean;
                            break;
                        }
                    }
                    
                    if (autParkingTransactionItemEventBean == null) {
                        System.out.println("Evento AUT non trovato");
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);

                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                    
                    PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(entityManager, parkingTransactionItemBean.getPaymentMethodId(), parkingTransactionItemBean.getPaymentMethodType());

                    if (paymentInfoBean == null) {
                        System.out.println("Metodo di pagamento non trovato");
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);

                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                    
                    Integer eventSequenceID = parkingTransactionItemBean.getLastEventSequenceID() + 1;
                    Integer statusSequenceID = parkingTransactionBean.getLastStatusSequenceID() + 1;

                    Date now = new Date();
                    String requestId = String.valueOf(now.getTime());
                    GestPayData gestPayDataDELETEResponse = doPaymentAuthDelete(parkingTransactionItemBean, paymentInfoBean, parkingTransactionBean.getUserBean(), paymentInfoBean.getId(),
                            autParkingTransactionItemEventBean.getEventAmount().doubleValue(), statusSequenceID, requestId, eventSequenceID, gpService,
                            parkingDecodedSecretKey);

                    System.out.println("Importo di Preautorizzazione :: " + autParkingTransactionItemEventBean.getEventAmount().doubleValue());
                    System.out.println("Tentativo di Storno => importo :: " + autParkingTransactionItemEventBean.getEventAmount().doubleValue());

                    if (gestPayDataDELETEResponse.getTransactionResult().equals("ERROR")) {

                        System.out.println("Errore nella cancellazione dell'autorizzazione del pagamento");
                        
                        decreaseAttempts(parkingTransactionInterfaceBean);
                        /*
                        ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH ERROR",
                                gestPayDataDELETEResponse.getErrorDescription());
                        entityManager.persist(parkingTransactionStatusBean);
                        */
                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                        return reconciliationInfo;
                    }

                    if (gestPayDataDELETEResponse.getTransactionResult().equals("KO")) {

                        System.out.println("KO nella cancellazione dell'autorizzazione del pagamento");
                        
                        parkingTransactionInterfaceBean.setToReconcilie(false);
                        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                        entityManager.merge(parkingTransactionInterfaceBean);
                        /*
                        ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                                PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH KO",
                                gestPayDataDELETEResponse.getErrorDescription());
                        entityManager.persist(parkingTransactionStatusBean);
                        */
                        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                        return reconciliationInfo;
                    }
                    
                    parkingTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.CANCELED.getValue());
                    entityManager.merge(parkingTransactionItemBean);
                    
                    continue;

                }

                System.out.println("Trovata transazione di pagamento in stato " + gestPayDataReadTrx.getTransactionState() + " --> NOT_RECONCILIABLE");
                
                parkingTransactionInterfaceBean.setToReconcilie(false);
                parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                entityManager.merge(parkingTransactionInterfaceBean);

                reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                return reconciliationInfo;
            }
            else {
                
                System.out.println("Trovato item in stato " + parkingTransactionItemBean.getParkingItemStatus() + " --> non gestito in questa fase");
            }
        }
        
        
        // Se resta dell'importo residuo deve essere creato un nuovo item e deve essere fatta l'autorizzazione e la movimentazione
        System.out.println("********************************************************");
        System.out.println("* Autorizzazione e movimentazione dell'importo residuo *");
        System.out.println("********************************************************");
        
        System.out.println("Import residuo: " + _parkingPrice);
        
        if (_parkingPrice > 0.0) {
            
            System.out.println("parkingPrice residuo            : " + _parkingPrice);
            _parkingPrice = (double) Math.round(_parkingPrice * 100) / 100;
            System.out.println("parkingPrice residuo arrotondato: " + _parkingPrice);

            System.out.println("Cerca l'ultimo item movimentato e utilizzalo per generare il nuovo item");
            
            ParkingTransactionItemBean lastSettledParkingTransactionItemBean = null;
            for (ParkingTransactionItemBean parkingTransactionItemBean : orderedParkingTransactionItemList) {
                if (parkingTransactionItemBean.getParkingItemStatus().equals(PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue())) {
                    lastSettledParkingTransactionItemBean = parkingTransactionItemBean;
                }
            }

            if (lastSettledParkingTransactionItemBean == null) {

                System.out.println("Non � stato teovato nessun item SETTLED --> NOT_RECONCILIABLE");
                
                parkingTransactionInterfaceBean.setToReconcilie(false);
                parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                entityManager.merge(parkingTransactionInterfaceBean);

                reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                return reconciliationInfo;
            }
            
            System.out.println("Trovato lastSettledParkingTransactionItemBean con id: " + lastSettledParkingTransactionItemBean.getId());
            
            System.out.println("Generazione del nuovo item");
            
            String parkingTransactionItemId = new IdGenerator().generateId(16).substring(0, 30);
            
            BigDecimal price = BigDecimal.valueOf(_parkingPrice).setScale(2, RoundingMode.HALF_EVEN);
            
            ParkingTransactionItemBean newParkingAuthorizedTransactionItemBean = new ParkingTransactionItemBean();
            
            newParkingAuthorizedTransactionItemBean.setAcquirerID(lastSettledParkingTransactionItemBean.getAcquirerID());
            newParkingAuthorizedTransactionItemBean.setAuthorizationCode(lastSettledParkingTransactionItemBean.getAuthorizationCode());
            newParkingAuthorizedTransactionItemBean.setBankTansactionID(lastSettledParkingTransactionItemBean.getBankTansactionID());
//            newParkingAuthorizedTransactionItemBean.setCreationTimestamp(new Timestamp(Calendar.getInstance().getTimeInMillis()));
            newParkingAuthorizedTransactionItemBean.setCurrency(lastSettledParkingTransactionItemBean.getCurrency());
//            newParkingAuthorizedTransactionItemBean.setCurrentPrice(null);
            newParkingAuthorizedTransactionItemBean.setGroupAcquirer(lastSettledParkingTransactionItemBean.getGroupAcquirer());
//            newParkingAuthorizedTransactionItemBean.setPan(lastSettledParkingTransactionItemBean.getPan());
//            newParkingAuthorizedTransactionItemBean.setParkingEndTime(parkingTransactionBean.getFinalParkingEndTime());
            newParkingAuthorizedTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue());

//            newParkingAuthorizedTransactionItemBean.setParkingStartTime(lastSettledParkingTransactionItemBean.getParkingStartTime());
//            newParkingAuthorizedTransactionItemBean.setParkingTimeCorrection("none");
            newParkingAuthorizedTransactionItemBean.setParkingTransactionItemId(parkingTransactionItemId);
            newParkingAuthorizedTransactionItemBean.setPaymentMethodId(lastSettledParkingTransactionItemBean.getPaymentMethodId());
            newParkingAuthorizedTransactionItemBean.setPaymentMethodType(lastSettledParkingTransactionItemBean.getPaymentMethodType());
            newParkingAuthorizedTransactionItemBean.setPaymentToken(lastSettledParkingTransactionItemBean.getPaymentToken());
//            newParkingAuthorizedTransactionItemBean.setPreviousPrice(null);
            newParkingAuthorizedTransactionItemBean.setPrice(price);
//            newParkingAuthorizedTransactionItemBean.setPriceDifference(null);
//            newParkingAuthorizedTransactionItemBean.setReconciliationAttemptsLeft(5);
//            newParkingAuthorizedTransactionItemBean.setRequestedEndTime(lastSettledParkingTransactionItemBean.getRequestedEndTime());
            newParkingAuthorizedTransactionItemBean.setServerName(lastSettledParkingTransactionItemBean.getServerName());
            newParkingAuthorizedTransactionItemBean.setShopLogin(lastSettledParkingTransactionItemBean.getShopLogin());
            
            newParkingAuthorizedTransactionItemBean.setParkingTransactionBean(parkingTransactionBean);
            parkingTransactionBean.getParkingTransactionItemList().add(newParkingAuthorizedTransactionItemBean);
            
            System.out.println("PaymentMethodId:   " + newParkingAuthorizedTransactionItemBean.getPaymentMethodId());
            System.out.println("PaymentMethodType: " + newParkingAuthorizedTransactionItemBean.getPaymentMethodType());
            
            PaymentInfoBean lastPaymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(entityManager, newParkingAuthorizedTransactionItemBean.getPaymentMethodId(),
                    newParkingAuthorizedTransactionItemBean.getPaymentMethodType());
            
            //PaymentInfoBean paymentInfoBean = parkingTransactionBean.getUserBean().findPaymentInfoBean(newParkingAuthorizedTransactionItemBean.getPaymentMethodId(),
            //        newParkingAuthorizedTransactionItemBean.getPaymentMethodType());

            if (lastPaymentInfoBean == null) {

                System.out.println("Non � stato trovato il metodo di pagamento associato all'item");
                
                parkingTransactionInterfaceBean.setToReconcilie(false);
                parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                entityManager.merge(parkingTransactionInterfaceBean);

                reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                return reconciliationInfo;
            }
            
            Integer eventSequenceID = 1;
            Integer statusSequenceID = parkingTransactionBean.getLastStatusSequenceID() + 1;
            
            Date now = new Date();
            String requestId = String.valueOf(now.getTime());

            //Preautorizzazione di importo pari al residuo (parkingPrice)
            GestPayData gestPayDataAUTHResponse = this.doPaymentAuth(newParkingAuthorizedTransactionItemBean, lastPaymentInfoBean, parkingTransactionBean.getUserBean(), lastPaymentInfoBean.getId(), _parkingPrice,
                    statusSequenceID, requestId, eventSequenceID, gpService, parkingDecodedSecretKey);

            if (gestPayDataAUTHResponse.getTransactionResult().equals("ERROR")) {

                System.out.println("Errore nell'autorizzazione del pagamento");
                
                decreaseAttempts(parkingTransactionInterfaceBean);
                /*
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH ERROR",
                        gestPayDataDELETEResponse.getErrorDescription());
                entityManager.persist(parkingTransactionStatusBean);
                */
                reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                return reconciliationInfo;
            }

            if (gestPayDataAUTHResponse.getTransactionResult().equals("KO")) {

                System.out.println("KO nell'autorizzazione del pagamento");
                
                parkingTransactionInterfaceBean.setToReconcilie(false);
                parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                entityManager.merge(parkingTransactionInterfaceBean);
                /*
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH KO",
                        gestPayDataDELETEResponse.getErrorDescription());
                entityManager.persist(parkingTransactionStatusBean);
                */
                reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                
                if (insufficientFunds(gestPayDataAUTHResponse)) {
                    UserBean userBean = parkingTransactionBean.getUserBean();
                    userBean.setUserStatus(User.USER_STATUS_BLOCKED);
                    entityManager.merge(userBean);
                }

                return reconciliationInfo;
            }

            System.out.println("Pagamento con carta di credito effettuato con successo");
            
            //Imposto lo status del parkingTransactionItem a AUTHORIZED se la transazione avviene con successo
            newParkingAuthorizedTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.AUTHORIZED.getValue());
            entityManager.merge(newParkingAuthorizedTransactionItemBean);

            //Movimentazione dell'importo pari a parkingPrice
            System.out.println("Movimentazione di " + _parkingPrice + " di " + _parkingPrice + " autorizzati");

            GestPayData gestPayDataSETTLEResponse = this.doPaymentSettle(newParkingAuthorizedTransactionItemBean, lastPaymentInfoBean, parkingTransactionBean.getUserBean(), lastPaymentInfoBean.getId(),
                    _parkingPrice, statusSequenceID, requestId, eventSequenceID, gpService, parkingDecodedSecretKey);

            if (gestPayDataSETTLEResponse.getTransactionResult().equals("ERROR")) {

                System.out.println("Errore nella movimentazione del pagamento");
                
                decreaseAttempts(parkingTransactionInterfaceBean);
                /*
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH ERROR",
                        gestPayDataDELETEResponse.getErrorDescription());
                entityManager.persist(parkingTransactionStatusBean);
                */
                reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

                return reconciliationInfo;
            }

            if (gestPayDataSETTLEResponse.getTransactionResult().equals("KO")) {

                System.out.println("KO nella movimentazione del pagamento");
                
                parkingTransactionInterfaceBean.setToReconcilie(false);
                parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
                entityManager.merge(parkingTransactionInterfaceBean);
                /*
                ParkingTransactionStatusBean parkingTransactionStatusBean = generateParkingTransactionStatusEvent(parkingTransactionBean, requestId,
                        PARKING_TRANSACTION_STATUS_CONST.ERROR, PARKING_TRANSACTION_STATUS_CONST.START_PARKING_TRANSACTION, "GestPay Action DELETE AUTH KO",
                        gestPayDataDELETEResponse.getErrorDescription());
                entityManager.persist(parkingTransactionStatusBean);
                */
                reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
                
                return reconciliationInfo;
            }

            //Imposto lo status del parkingTransactionItem a SETTLED se la transazione avviene con successo
            newParkingAuthorizedTransactionItemBean.setParkingItemStatus(PARKING_TRANSACTION_ITEM_STATUS.SETTLED.getValue());
            entityManager.merge(newParkingAuthorizedTransactionItemBean);
        }
        
        reconciliationInfo.setFinalStatusType(StatusHelper.PARKING_STATUS_ENDED);
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
        
        parkingTransactionInterfaceBean.setToReconcilie(Boolean.FALSE);

        return reconciliationInfo;
        
        /*
        // Riconciliazione fallita
        parkingTransactionInterfaceBean.setToReconcilie(false);
        parkingTransactionInterfaceBean.setFinalStatus(StatusHelper.PARKING_STATUS_NOT_RECONCILIABLE);
        entityManager.merge(parkingTransactionInterfaceBean);

        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

        return reconciliationInfo;
        */
        
        /*
        // Riconciliazione da riprovare
        decreaseAttempts(parkingTransactionInterfaceBean);

        reconciliationInfo.setFinalStatusType(parkingTransactionInterfaceBean.getFinalStatus());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

        return reconciliationInfo;
        */
        
        
        /*
        // Riconciliazione completata con successo
        reconciliationInfo.setFinalStatusType(StatusHelper.PARKING_STATUS_FAILED);
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
        
        parkingTransactionInterfaceBean.setToReconcilie(Boolean.FALSE);

        return reconciliationInfo;
        */
    }

    @Override
    public String getStatus() {
        return StatusHelper.PARKING_STATUS_ENDED;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "PARKING PAYMENT ERROR";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<ParkingTransactionBean> parkingTransactionBeanList = QueryRepository.findParkingTransactionToBeReconciled(entityManager, getStatus());
        return parkingTransactionBeanList;
    }
    
    private GestPayData doPaymentAuth(ParkingTransactionItemBean parkingTransactionItemBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double amount, Integer statusSequenceID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        ParkingTransactionItemEventBean parkingTransactionItemEventBean = null;
        ParkingTransactionStatusBean parkingTransactionItemStatusBean = null;

        newStatus = PARKING_TRANSACTION_ITEM_STATUS_STATUS.PAYMENT_AUTHORIZED;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        PARKING_TRANSACTION_ITEM_EVENT_STATUS transactionEvent = PARKING_TRANSACTION_ITEM_EVENT_STATUS.AUTHORIZED;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        String paymentMethodExpiration = "";

        if (paymentInfoBean != null) {

            CardDepositTransactionBean cardDepositTransactionBean = QueryRepository.findCardDepositTransactionByShopTransactionID(entityManager, paymentInfoBean.getCheckShopTransactionID());
            if (cardDepositTransactionBean != null) {

                String tokenExpiryMonth = cardDepositTransactionBean.getTokenExpiryMonth();
                String tokenExpiryYear = cardDepositTransactionBean.getTokenExpiryYear();

                paymentMethodExpiration = "20" + tokenExpiryYear + tokenExpiryMonth;
            }
        }

        Extension[] extension_array = {};

        GestPayData gestPayDataAUTHResponse = gpService.callPagam(amount, parkingTransactionItemBean.getParkingTransactionItemId(), parkingTransactionItemBean.getShopLogin(),
                parkingTransactionItemBean.getCurrency(), paymentInfoBean.getToken(), null, parkingTransactionItemBean.getAcquirerID(),
                parkingTransactionItemBean.getGroupAcquirer(), encodedSecretKey, paymentMethodExpiration, extension_array, "CUSTOMER");

        if (gestPayDataAUTHResponse == null) {
            gestPayDataAUTHResponse = new GestPayData();
            gestPayDataAUTHResponse.setTransactionResult("ERROR");
            gestPayDataAUTHResponse.setErrorCode("9999");
            gestPayDataAUTHResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            // TODO Definire il flusso della riconciliazione
            //poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH: " + gestPayDataAUTHResponse.getTransactionResult());

        errorCode = gestPayDataAUTHResponse.getErrorCode();
        errorDescription = gestPayDataAUTHResponse.getErrorDescription();
        eventResult = gestPayDataAUTHResponse.getTransactionResult();

        parkingTransactionItemStatusBean = this.generateParkingTransactionStatusEvent(parkingTransactionItemBean.getParkingTransactionBean(), requestID,
                PARKING_TRANSACTION_STATUS_CONST.INFO, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, newStatus.getValue(), "Authorization Payment successfully created");

        entityManager.persist(parkingTransactionItemStatusBean);

        parkingTransactionItemEventBean = this.generateParkingTransactionItemEvent(amount, eventSequenceID, parkingTransactionItemBean, gestPayDataAUTHResponse,
                transactionEvent.getValue());

        entityManager.persist(parkingTransactionItemEventBean);

        parkingTransactionItemBean.setPaymentMethodId(paymentMethodId);
        parkingTransactionItemBean.setPaymentToken(paymentInfoBean.getToken());
        parkingTransactionItemBean.setBankTansactionID(gestPayDataAUTHResponse.getBankTransactionID());
        parkingTransactionItemBean.setAuthorizationCode(gestPayDataAUTHResponse.getAuthorizationCode());

        oldStatus = newStatus;

        return gestPayDataAUTHResponse;
    }
    
    private GestPayData doPaymentSettle(ParkingTransactionItemBean parkingTransactionItemBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double amount, Integer statusSequenceID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        ParkingTransactionItemEventBean parkingTransactionItemEventBean = null;
        ParkingTransactionStatusBean parkingTransactionItemStatusBean = null;

        newStatus = PARKING_TRANSACTION_ITEM_STATUS_STATUS.PAYMENT_SETTLED;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        PARKING_TRANSACTION_ITEM_EVENT_STATUS transactionEvent = PARKING_TRANSACTION_ITEM_EVENT_STATUS.SETTLED;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataSETTLEResponse = gpService.callSettle(amount, parkingTransactionItemBean.getParkingTransactionItemId(), parkingTransactionItemBean.getShopLogin(),
                parkingTransactionItemBean.getCurrency(), parkingTransactionItemBean.getAcquirerID(), parkingTransactionItemBean.getGroupAcquirer(), encodedSecretKey,
                paymentInfoBean.getToken(), parkingTransactionItemBean.getAuthorizationCode(), parkingTransactionItemBean.getBankTansactionID(), null, null, 0.0, 0.0);

        if (gestPayDataSETTLEResponse == null) {
            gestPayDataSETTLEResponse = new GestPayData();
            gestPayDataSETTLEResponse.setTransactionResult("ERROR");
            gestPayDataSETTLEResponse.setErrorCode("9999");
            gestPayDataSETTLEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            // TODO Definire il flusso della riconciliazione
            //poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay SETTLE: " + gestPayDataSETTLEResponse.getTransactionResult());

        errorCode = gestPayDataSETTLEResponse.getErrorCode();
        errorDescription = gestPayDataSETTLEResponse.getErrorDescription();
        eventResult = gestPayDataSETTLEResponse.getTransactionResult();

        parkingTransactionItemStatusBean = this.generateParkingTransactionStatusEvent(parkingTransactionItemBean.getParkingTransactionBean(), requestID,
                PARKING_TRANSACTION_STATUS_CONST.INFO, PARKING_TRANSACTION_STATUS_CONST.END_PARKING_TRANSACTION, newStatus.getValue(), "Payment successfully created");

        entityManager.persist(parkingTransactionItemStatusBean);

        parkingTransactionItemEventBean = this.generateParkingTransactionItemEvent(amount, eventSequenceID, parkingTransactionItemBean, gestPayDataSETTLEResponse,
                transactionEvent.getValue());
        entityManager.persist(parkingTransactionItemEventBean);

        parkingTransactionItemBean.getParkingTransactionItemEventList().add(parkingTransactionItemEventBean);

        oldStatus = newStatus;

        return gestPayDataSETTLEResponse;
    }
}
