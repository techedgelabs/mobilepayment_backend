package com.techedge.mp.core.actions.reconciliation.status.prepaid;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PrePaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityConstants;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.LoadLoyaltyCreditsResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public class LoyaltyReconciliationStatus extends ReconciliationPrePaidTransactionStatus {

    public LoyaltyReconciliationStatus(EntityManager entityManager, GPServiceRemote gpService, TransactionServiceRemote transactionService, 
            ForecourtInfoServiceRemote forecourtService, FidelityServiceRemote fidelityService, CRMServiceRemote crmService, EmailSenderRemote emailSender, String proxyHost, 
            String proxyPort, String proxyNoHosts, String secretKey, UserCategoryService userCategoryService, StringSubstitution stringSubstitution, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2) {
        
        super(entityManager, gpService, transactionService, forecourtService, fidelityService, crmService, emailSender, proxyHost, proxyPort, proxyNoHosts, secretKey, 
                userCategoryService, stringSubstitution, refuelingNotificationService, refuelingNotificationServiceOAuth2);
    }

    @Override
    public ReconciliationInfo reconciliateCreditCardFlow(TransactionInterfaceBean transactionInterfaceBean) {
        TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
        
        String operationID = null;
        Long requestTimestamp = null;
        PartnerType partnerType = PartnerType.MP;
        
        System.out.println("Controllo se c'� stato il caricamento di punti");
        
        for (PrePaidLoadLoyaltyCreditsBean prePaidLoadLoyaltyCreditsBean : transactionBean.getPrePaidLoadLoyaltyCreditsBeanList()) {

            if (prePaidLoadLoyaltyCreditsBean.getOperationType().equals("LOAD")) {
                
                operationID = new IdGenerator().generateId(16).substring(0, 33);
                requestTimestamp = new Date().getTime();
                String operationIDtoCheck = prePaidLoadLoyaltyCreditsBean.getOperationID();
                
                try {
                    CheckLoadLoyaltyCreditsTransactionResult checkLoadLoyaltyCreditsTransactionResult = fidelityService.checkLoadLoyaltyCreditsTransaction(operationID, operationIDtoCheck, partnerType, requestTimestamp);
                    
                    if (checkLoadLoyaltyCreditsTransactionResult == null) {
                        
                        System.err.println("Errore nella chiamata al servizio checkLoadLoyaltyCreditsTransactionResult: null");
                        reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                        decreaseAttempts(transactionBean);
                        return reconciliationInfo;
                    }
                    
                    System.out.println("checkLoadLoyaltyCreditsTransactionResult.getStatusCode: " + checkLoadLoyaltyCreditsTransactionResult.getStatusCode());
                    
                    if (checkLoadLoyaltyCreditsTransactionResult.getStatusCode().equals(FidelityResponse.CHECK_LOAD_LOYALTY_CREDITS_TRANSACTION_OK)) {
                        
                        System.out.println("Il caricamento punti risulta gi� effettuato con successo");
                        
                        prePaidLoadLoyaltyCreditsBean.setBalance(checkLoadLoyaltyCreditsTransactionResult.getBalance());
                        prePaidLoadLoyaltyCreditsBean.setBalanceAmount(checkLoadLoyaltyCreditsTransactionResult.getBalanceAmount());
                        prePaidLoadLoyaltyCreditsBean.setCardClassification(checkLoadLoyaltyCreditsTransactionResult.getCardClassification());
                        prePaidLoadLoyaltyCreditsBean.setCardCodeIssuer(checkLoadLoyaltyCreditsTransactionResult.getCardCodeIssuer());
                        prePaidLoadLoyaltyCreditsBean.setCardStatus(checkLoadLoyaltyCreditsTransactionResult.getCardStatus());
                        prePaidLoadLoyaltyCreditsBean.setCardType(checkLoadLoyaltyCreditsTransactionResult.getCardType());
                        prePaidLoadLoyaltyCreditsBean.setCredits(checkLoadLoyaltyCreditsTransactionResult.getCredits());
                        prePaidLoadLoyaltyCreditsBean.setCsTransactionID(checkLoadLoyaltyCreditsTransactionResult.getCsTransactionID());
                        prePaidLoadLoyaltyCreditsBean.setEanCode(checkLoadLoyaltyCreditsTransactionResult.getEanCode());
                        prePaidLoadLoyaltyCreditsBean.setMarketingMsg(checkLoadLoyaltyCreditsTransactionResult.getMarketingMsg());
                        prePaidLoadLoyaltyCreditsBean.setMessageCode(checkLoadLoyaltyCreditsTransactionResult.getMessageCode());
                        prePaidLoadLoyaltyCreditsBean.setStatusCode(checkLoadLoyaltyCreditsTransactionResult.getStatusCode());
                        
                        entityManager.merge(prePaidLoadLoyaltyCreditsBean);
                        
                        transactionBean.setLoyaltyReconcile(Boolean.FALSE);
                        entityManager.merge(transactionBean);
                        
                        reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                        
                        return reconciliationInfo;
                    }
                    else {
                        
                        if (checkLoadLoyaltyCreditsTransactionResult.getStatusCode().equals(FidelityResponse.CHECK_LOAD_LOYALTY_CREDITS_TRANSACTION_ID_REQUEST_NOT_FOUND)) {
                            
                            // Caricamento punti in modalit� prepaid
                            
                            System.out.println("Caricamento punti carta fedelt�");
                            
                            LoyaltyCardBean loyaltyCardBean = transactionBean.getUserBean().getVirtualLoyaltyCard();
    
                            if (loyaltyCardBean != null) {
                                
                                String refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_PREPAY;
                                
                                List<ProductDetail> productList = new ArrayList<ProductDetail>(0);
                                
                                ProductDetail productDetail = new ProductDetail();
                                productDetail.setAmount(transactionBean.getFinalAmount());
                                
                                String productID = transactionBean.getProductID();
                                
                                System.out.println("trovato product id: " + productID);
    
                                String productCode = this.getProductCode(productID);
                                productDetail.setProductCode(productCode);
                                
                                System.out.println("trovato product code: " + productCode);
                                
                                productDetail.setQuantity(transactionBean.getFuelQuantity());
                                
                                productList.add(productDetail);
                                
                         
                                // Chimamata al servizio loadLoyaltyCredits
    
                                String newOperationID = new IdGenerator().generateId(16).substring(0, 33);
                                Long newRequestTimestamp = new Date().getTime();
                                String stationID = transactionBean.getStationBean().getStationID();
                                String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;
        
                                Long   paymentMethodId   = transactionBean.getPaymentMethodId();
                                String paymentMethodType = transactionBean.getPaymentMethodType();
                                String BIN               = "";
                                
                                if (paymentMethodId != null && paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {
                                
                                    PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(entityManager, paymentMethodId, paymentMethodType);
                                    
                                    if (paymentInfoBean != null) {
                                        
                                        BIN = paymentInfoBean.getCardBin();
                    
                                        if (BIN != null && QueryRepository.findCardBinExists(entityManager, BIN)) {
                                            System.out.println("BIN valido (" + BIN + ")");
                                            paymentMode = FidelityConstants.PAYMENT_METHOD_ENI_CARD;
                                        }
                                        else {
                                            System.out.println("BIN Non Valido (" + BIN + ")");
                                        }
                                    }
                                    else {
                                        System.out.println("paymentInfoBean null");
                                    }
                                }
                                else {
                                    System.out.println("paymentMethodId null o pagato con voucher");
                                }
        
                                prePaidLoadLoyaltyCreditsBean.setOperationID(newOperationID);
                                prePaidLoadLoyaltyCreditsBean.setOperationType("LOAD");
                                prePaidLoadLoyaltyCreditsBean.setRequestTimestamp(newRequestTimestamp);
        
                                try {
        
                                    System.out.println("Chiamata servizio di carico punti");
        
                                    LoadLoyaltyCreditsResult loadLoyaltyCreditsResult = fidelityService.loadLoyaltyCredits(newOperationID, transactionBean.getTransactionID(), stationID,
                                            loyaltyCardBean.getPanCode(), BIN, refuelMode, paymentMode, "it", partnerType, newRequestTimestamp, "", productList);
        
                                    System.out.println("Risposta servizio di carico punti: " + loadLoyaltyCreditsResult.getStatusCode() + " (" + loadLoyaltyCreditsResult.getMessageCode()
                                            + ")");
        
                                    prePaidLoadLoyaltyCreditsBean.setBalance(loadLoyaltyCreditsResult.getBalance());
                                    prePaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                                    prePaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                                    prePaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                                    prePaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                                    prePaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                                    prePaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                                    prePaidLoadLoyaltyCreditsBean.setCsTransactionID(loadLoyaltyCreditsResult.getCsTransactionID());
                                    prePaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                                    prePaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                                    prePaidLoadLoyaltyCreditsBean.setMessageCode(loadLoyaltyCreditsResult.getMessageCode());
                                    prePaidLoadLoyaltyCreditsBean.setStatusCode(loadLoyaltyCreditsResult.getStatusCode());
                                    prePaidLoadLoyaltyCreditsBean.setWarningMsg(loadLoyaltyCreditsResult.getWarningMsg());
                                    
                                    entityManager.merge(prePaidLoadLoyaltyCreditsBean);
                                    
                                    transactionBean.setLoyaltyReconcile(Boolean.FALSE);
                                    entityManager.merge(transactionBean);
        
                                    System.err.println("Error loading loyalty credits: " + loadLoyaltyCreditsResult.getStatusCode() + " " + loadLoyaltyCreditsResult.getMessageCode());
                                        
                                    reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                                    
                                    return reconciliationInfo;
                                }
                                catch (Exception e) {
        
                                    System.err.println("Error loading loyalty credits");
                                    
                                    reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                                    decreaseAttempts(transactionBean);
                                    return reconciliationInfo;
                                }
                            }
                            else {
                                
                                System.err.println("Loyalty card not found");
                                
                                reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                                
                                return reconciliationInfo;
                            }
                        }
                        else {
                            
                            System.err.println("Errore nella chiamata al servizio checkLoadLoyaltyCreditsTransactionResult: " + checkLoadLoyaltyCreditsTransactionResult.getStatusCode());

                            reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                            
                            return reconciliationInfo;
                        }
                    }
                }
                catch (FidelityServiceException ex) {
                    
                    System.err.println("Errore nella chiamata al servizio checkLoadLoyaltyCreditsTransactionResult: " + ex.getMessage());
                    
                    reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                    decreaseAttempts(transactionBean);
                    return reconciliationInfo;
                }
            }
            else {
                System.out.println("Rilevata operazione diversa da LOAD");
            }
        }

        return reconciliationInfo;
    }

    @Override
    public ReconciliationInfo reconciliateVoucherFlow(TransactionInterfaceBean transactionInterfaceBean) {
        /*
        System.out.println("Stato di riconciliazione non pi� necessaria per il nuovo flusso. Se sei qui hai un problema!!!");
        
        TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
        transactionBean.setVoucherReconciliation(false);
        entityManager.merge(transactionBean);
        
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        reconciliationInfo.setFinalStatusType(transactionInterfaceBean.getTransactionStatus());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
        return reconciliationInfo;
        */
        
        // Il flusso di riconciliazione � lo stesso del caso di transazione pagata con carta di credito
        return reconciliateCreditCardFlow(transactionInterfaceBean);
    }
    
    @Override
    public String getStatus() {
        return StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "LOYALTY RECONCILIATION";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsByFinalStatusTypeAndLoyaltyReconciliation(entityManager, getStatus(), true);
        System.out.println("Trovate " + transactionBeanList.size() + " transazioni");
        return transactionBeanList;
    }
    
    private String getProductCode(String productID) {
        
        if (productID == null || productID.isEmpty()) {
            return null;
        }
        
        if (productID.equals("SP")) {
            return FidelityConstants.PRODUCT_CODE_SP;
        }

        if (productID.equals("GG")) {
            return FidelityConstants.PRODUCT_CODE_GASOLIO;
        }

        if (productID.equals("BS")) {
            return FidelityConstants.PRODUCT_CODE_BLUE_SUPER;
        }

        if (productID.equals("BD")) {
            return FidelityConstants.PRODUCT_CODE_BLUE_DIESEL;
        }

        if (productID.equals("MT")) {
            return FidelityConstants.PRODUCT_CODE_METANO;
        }

        if (productID.equals("GP")) {
            return FidelityConstants.PRODUCT_CODE_GPL;
        }

        if (productID.equals("AD")) {
            return null;
        }
        
        return null;
    }

}
