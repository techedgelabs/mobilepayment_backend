package com.techedge.mp.core.actions.reconciliation.status.prepaid;

import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.SendPaymentTransactionResultResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public class MissingNotificationStatus extends ReconciliationPrePaidTransactionStatus {

    public MissingNotificationStatus(EntityManager entityManager, GPServiceRemote gpService, TransactionServiceRemote transactionService, 
            ForecourtInfoServiceRemote forecourtService, FidelityServiceRemote fidelityService, CRMServiceRemote crmService, EmailSenderRemote emailSender, String proxyHost, 
            String proxyPort, String proxyNoHosts, String secretKey, UserCategoryService userCategoryService, StringSubstitution stringSubstitution, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2) {
        
        super(entityManager, gpService, transactionService, forecourtService, fidelityService, crmService, emailSender, proxyHost, proxyPort, proxyNoHosts, secretKey, 
                userCategoryService, stringSubstitution, refuelingNotificationService, refuelingNotificationServiceOAuth2);
    }

    @Override
    public ReconciliationInfo reconciliateCreditCardFlow(TransactionInterfaceBean transactionInterfaceBean) {
        TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
        ReconciliationInfo reconciliationInfo;
        String transactionType = "MOV";
        int sequenceId = 0;
        String amount = "0";
        
        if (transactionBean.getFinalAmount() != null) {
            amount = transactionBean.getFinalAmount().toString();
        }
        List<TransactionEventBean> transactionBeanList = QueryRepository.findTransactionEventByTransaction(entityManager, transactionBean);

        for (TransactionEventBean transactionEventBean : transactionBeanList) {
            if (transactionEventBean.getSequenceID().intValue() > sequenceId) {
                transactionType = transactionEventBean.getEventType();
            }
            sequenceId++;
        }
        
        GestPayData gestPayData = new GestPayData();
        gestPayData.setAuthorizationCode(transactionBean.getAuthorizationCode());
        gestPayData.setBankTransactionID(transactionBean.getBankTansactionID());
        gestPayData.setTransactionType(transactionType);
        gestPayData.setErrorDescription(null);
        gestPayData.setErrorCode("0");
        gestPayData.setTransactionResult("OK");
        gestPayData.setAmount(amount);
        
        reconciliationInfo = updateTransaction(transactionBean, gestPayData);
        return reconciliationInfo;
    }
    
    @Override
    public ReconciliationInfo reconciliateVoucherFlow(TransactionInterfaceBean transactionInterfaceBean) {
        TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
        ReconciliationInfo reconciliationInfo;
        String transactionType = "CAN";
        String amount = "0.0";
        
        if (transactionBean.getFinalAmount() != null) {
            amount = transactionBean.getFinalAmount().toString();
        }

        GestPayData gestPayData = new GestPayData();
        gestPayData.setAuthorizationCode(transactionBean.getAuthorizationCode());
        gestPayData.setBankTransactionID(transactionBean.getBankTansactionID());
        gestPayData.setTransactionType(transactionType);
        gestPayData.setErrorDescription(null);
        gestPayData.setErrorCode("0");
        gestPayData.setTransactionResult("OK");
        gestPayData.setAmount(amount);
        
        reconciliationInfo = updateTransaction(transactionBean, gestPayData);
        return reconciliationInfo;
    }
    
    
    private ReconciliationInfo updateTransaction(TransactionBean transactionBean, GestPayData gestPayData) {
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);

        System.out.println("callSeattle OK --> Invia notifica pagamento al GFG");
        SendPaymentTransactionResultResponse sendPaymentTransactionResultResponse = notifyGFG(transactionBean, gestPayData);
        if (sendPaymentTransactionResultResponse.getStatusCode().contains("200")) {
            reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
            transactionBean.setGFGNotification(true);
            if (sendPaymentTransactionResultResponse.getElectronicInvoiceID() != null && !sendPaymentTransactionResultResponse.getElectronicInvoiceID().trim().isEmpty()) {
                transactionBean.setGFGElectronicInvoiceID(sendPaymentTransactionResultResponse.getElectronicInvoiceID().trim());
            }
        }
        else if (sendPaymentTransactionResultResponse.getStatusCode().contains("500")) {
            decreaseAttempts(transactionBean);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);    
        }
        else if (sendPaymentTransactionResultResponse.getStatusCode().contains("400")) {
            updateFinalStatus(transactionBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
            reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
        }
        
        return reconciliationInfo;
    }

    @Override
    public String getStatus() {
        return StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "MISSING NOTIFICATION";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsByFinalStatusTypeAndNotification(entityManager, getStatus(), false);
        return transactionBeanList;
    }


}
