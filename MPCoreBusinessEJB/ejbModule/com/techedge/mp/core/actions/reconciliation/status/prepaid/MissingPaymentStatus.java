package com.techedge.mp.core.actions.reconciliation.status.prepaid;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.actions.reconciliation.ReconciliationPaymentFlowType;
import com.techedge.mp.core.actions.reconciliation.status.EventResult;
import com.techedge.mp.core.actions.reconciliation.status.EventResultType;
import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCancelPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.TransactionConsumeVoucherPreAuthResponse;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.voucher.VoucherCommonOperations;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.RefuelDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.TransactionReconciliationResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public class MissingPaymentStatus extends ReconciliationPrePaidTransactionStatus {

    public MissingPaymentStatus(EntityManager entityManager, GPServiceRemote gpService, TransactionServiceRemote transactionService, ForecourtInfoServiceRemote forecourtService,
            FidelityServiceRemote fidelityService, CRMServiceRemote crmService, EmailSenderRemote emailSender, String proxyHost, String proxyPort, String proxyNoHosts, 
            String secretKey, UserCategoryService userCategoryService, StringSubstitution stringSubstitution, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2) {

        super(entityManager, gpService, transactionService, forecourtService, fidelityService, crmService, emailSender, proxyHost, proxyPort, proxyNoHosts, secretKey, 
                userCategoryService, stringSubstitution, refuelingNotificationService, refuelingNotificationServiceOAuth2);
    }

    @Override
    public ReconciliationInfo reconciliateCreditCardFlow(TransactionInterfaceBean transactionInterfaceBean) {
        TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
        GestPayData gestPayData = null;
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();

        List<String> transactionIdList = new ArrayList<String>();
        transactionIdList.add(transactionBean.getTransactionID());
        String requestID = String.valueOf(new Date().getTime());

        System.out.println("chiamata a transactionReconciliation del Forecourt");
        TransactionReconciliationResponse forecourtResponse = forecourtService.transactionReconciliation(requestID, transactionIdList);

        if (forecourtResponse == null || forecourtResponse.getTransactionDetails().isEmpty()) {
            System.out.println("errore nella operazione di riconciliazione - risposta Forecourt nulla o vuota (transazione: " + transactionBean.getTransactionID() + ")");
            // la cancellazione non � andata a buon fine
            reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(transactionBean);
            return reconciliationInfo;
        }

        TransactionDetail transactionDetail = forecourtResponse.getTransactionDetails().get(0);
        RefuelDetail refuelDetail = transactionDetail.getRefuelDetail();
        updateTransactionRefuelDetail(transactionBean, refuelDetail);

        double initialAmount = transactionInterfaceBean.getInitialAmount();
        double finalAmount = refuelDetail.getAmount().doubleValue();

        System.out.println("Initial amount: " + initialAmount + ", final amount: " + finalAmount);

        if (finalAmount > initialAmount) {

            System.out.println("Errore elettrovalvola: final amount maggiore dell'initial amount");

            finalAmount = initialAmount;

            System.out.println("Initial amount: " + initialAmount + ", final amount: " + finalAmount);
        }

        double effectiveAmount = finalAmount;

        if (paymentFlowType.equals(ReconciliationPaymentFlowType.FLOW_CREDIT_CARD_CARTASI) && !transactionInterfaceBean.getAcquirerID().equals("MULTICARD")) {

            System.out.println("Pagamento tramite CARTASI consumo voucher previsto");
            System.out.println("Controllo se sono stati consumati voucher");
            EventResult verifyVoucher = verifyVoucherTransaction(transactionBean);

            if (verifyVoucher.getResult().equals(EventResultType.TO_RETRY)) {
                decreaseAttempts(transactionInterfaceBean);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                return reconciliationInfo;
            }

            Double voucherAmount = (Double) verifyVoucher.getAmount();
            effectiveAmount -= voucherAmount;
            System.out.println("Importo voucher consumati " + voucherAmount);
        }
        else {
            if (transactionInterfaceBean.getAcquirerID().equals("MULTICARD")) {
                System.out.println("Pagamento tramite MULTICARD -> consumo voucher non previsto");
            }
            else {
                System.out.println("Pagamento tramite BANCASELLA (Refueling) -> consumo voucher non previsto");
            }
        }

        System.out.println("Amount iniziale: " + finalAmount + ", Amound residuo (dopo controllo voucher): " + effectiveAmount);
        String decodedSecretKey = "";
        boolean callDeletePagam = true;

        try {
            EncryptionAES encryptionAES = new EncryptionAES();
            encryptionAES.loadSecretKey(this.secretKey);
            decodedSecretKey = encryptionAES.decrypt(transactionBean.getEncodedSecretKey());
        }
        catch (Exception e) {
            System.err.println("errore nella decrypt della chiave per cartas�: " + e.getMessage() + " (transazione: " + transactionInterfaceBean.getTransactionID() + ")");
        }

        System.out.println("chiamata a callReadTrx dell'acquirer");
        reconciliationInfo = new ReconciliationInfo();
        GestPayData gestPayDataReadTrx = gpService.callReadTrx(transactionBean.getShopLogin(), transactionBean.getTransactionID(), transactionBean.getBankTansactionID(),
                transactionBean.getAcquirerID(), transactionBean.getGroupAcquirer(), decodedSecretKey);

        if (gestPayDataReadTrx == null) {
            System.out.println("errore nella operazione di riconciliazione, chiamata callReadTrx nulla verso l'acquirer(transazione: "
                    + transactionInterfaceBean.getTransactionID() + ")");
            reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(transactionBean);
            return reconciliationInfo;
        }

        if (effectiveAmount > 0) {

            // Verifica lo stato della transazione

            if (gestPayDataReadTrx.getTransactionState().equals("AUT") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK")) {

                // Se la transazione di pagamento risulta in stato autorizzato, procedi con la movimentazione

                if (paymentFlowType.equals(ReconciliationPaymentFlowType.FLOW_CREDIT_CARD_CARTASI) && !transactionInterfaceBean.getAcquirerID().equals("MULTICARD")) {

                    // Si controlla se � possibile pagare tramite voucher
                    EventResult eventResult = voucherTransaction(transactionBean, effectiveAmount);
                    
                    if (eventResult.getResult().equals(EventResultType.SUCCESS)) {
                        System.out.println("Pagamento con voucher: " + eventResult.getAmount());
                        System.out.println("Amount iniziale: " + finalAmount + ", Amound residuo (dopo consumo voucher): " + effectiveAmount);
                        effectiveAmount -= (Double) eventResult.getAmount();
                    }

                }

                if (effectiveAmount > 0) {

                    callDeletePagam = false;
                    
                    System.out.println("Amount residuo > 0 --> chiama callSettle");

                    System.out.println("chiamata a callSettle dell'acquirer");
                    gestPayData = gpService.callSettle(effectiveAmount, transactionBean.getTransactionID(), transactionBean.getShopLogin(), transactionBean.getCurrency(),
                            transactionBean.getAcquirerID(), transactionBean.getGroupAcquirer(), decodedSecretKey,
                            transactionBean.getToken(), transactionBean.getAuthorizationCode(), transactionBean.getBankTansactionID(), transactionBean.getRefuelMode(),
                            transactionBean.getProductID(), transactionBean.getFuelQuantity(), transactionBean.getUnitPrice());

                    if (gestPayData != null) {

                        String errorCode = gestPayData.getErrorCode();
                        String errorMessage = gestPayData.getErrorDescription();
                        System.out.println("risposta dell'acquirer a callSettle: " + errorMessage + "(" + errorCode + ")");

                        if (!errorCode.equals("0")) {
                            updateFinalStatus(transactionBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                            reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                        }
                        else {
                            updateAmount(transactionBean, finalAmount);
                            reconciliationInfo = payTransactionCompletion(transactionBean, gestPayData, effectiveAmount, getStatus());

                            if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS)) {
                                System.out.println("operazione di riconciliazione eseguita (transazione: " + transactionBean.getTransactionID() + ")");
                            }
                            else {
                                System.out.println("errore nella operazione di riconciliazione durante l'aggiornamento dati Backend (transazione: "
                                        + transactionBean.getTransactionID() + ")");
                            }
                        }
                    }
                    else {

                        System.out.println("errore nella operazione di riconciliazione, chiamata callSettle nulla verso l'acquirer (transazione: "
                                + transactionBean.getTransactionID() + ")");

                        // l'operazione non � andata a buon fine
                        reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                        decreaseAttempts(transactionBean);
                    }
                }
            }
            else {

                System.out.println("La transazione " + transactionBean.getTransactionID() + " non risulta in stato AUT, ma " + gestPayDataReadTrx.getTransactionState());

                if (gestPayDataReadTrx.getTransactionState() != null && gestPayDataReadTrx.getTransactionState().equals("MOV")) {

                    // la transazione si pu� chiudere come riconciliata
                    callDeletePagam = false;
                    gestPayData = new GestPayData();

                    gestPayData.setErrorCode("0");
                    gestPayData.setErrorDescription("");
                    gestPayData.setAuthorizationCode(gestPayDataReadTrx.getAuthorizationCode());
                    gestPayData.setBankTransactionID(gestPayDataReadTrx.getBankTransactionID());
                    gestPayData.setTransactionType(gestPayDataReadTrx.getTransactionType());
                    gestPayData.setTransactionResult(gestPayDataReadTrx.getTransactionResult());
                    gestPayData.setAmount(gestPayDataReadTrx.getAmount());

                    updateAmount(transactionBean, finalAmount);
                    reconciliationInfo = payTransactionCompletion(transactionBean, gestPayData, effectiveAmount, getStatus());

                    if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS)) {
                        System.out.println("operazione di riconciliazione eseguita (transazione: " + transactionBean.getTransactionID() + ")");
                    }
                    else {
                        System.out.println("errore nella operazione di riconciliazione durante l'aggiornamento dati Backend (transazione: " + transactionBean.getTransactionID()
                                + ")");
                    }
                }
                else {

                    System.out.println("errore nella operazione di riconciliazione, chiamata callSettle nulla verso Banca Sella (transazione: "
                            + transactionBean.getTransactionID() + ")");

                    // l'operazione non � andata a buon fine
                    reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                    decreaseAttempts(transactionBean);
                }
            }
        }

        if (callDeletePagam && (gestPayDataReadTrx.getTransactionState().equals("AUT") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK"))) {

            System.out.println("Amount residuo == 0 --> chiama deletePagam");

            gestPayData = gpService.deletePagam(transactionBean.getInitialAmount(), transactionBean.getTransactionID(), transactionBean.getShopLogin(), transactionBean.getCurrency(), null, null, transactionBean.getAcquirerID(),
                    transactionBean.getGroupAcquirer(), decodedSecretKey);

            if (gestPayData != null) {

                String errorCode = gestPayData.getErrorCode();
                String errorMessage = gestPayData.getErrorDescription();
                System.out.println("risposta dell'acquirer a deletePagam: " + errorMessage + "(" + errorCode + ")");

                if (!errorCode.equals("0")) {
                    updateFinalStatus(transactionBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                    reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                    reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
                }
                else {
                    updateAmount(transactionBean, finalAmount);
                    reconciliationInfo = deleteTransactionCompletion(transactionBean, gestPayData, true, getStatus());

                    if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS)) {
                        System.out.println("operazione di riconciliazione eseguita (transazione: " + transactionBean.getTransactionID() + ")");
                    }
                    else {
                        System.out.println("errore nella operazione di riconciliazione durante l'aggiornamento dati Backend (transazione: " + transactionBean.getTransactionID()
                                + ")");
                    }
                }
            }
            else {

                System.out.println("errore nella operazione di riconciliazione, chiamata deletePagam nulla verso l'acquirer (transazione: " + transactionBean.getTransactionID()
                        + ")");

                // l'operazione non � andata a buon fine
                reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                decreaseAttempts(transactionBean);
            }
        }
        
        if (callDeletePagam && (gestPayDataReadTrx.getTransactionState().equals("CAN") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK"))) {
            
            System.out.println("Cancellazione gi� effettuata");
            
            reconciliationInfo = payTransactionCompletion(transactionBean, gestPayDataReadTrx, finalAmount, getStatus());
        }

        return reconciliationInfo;
    }

    @Override
    public ReconciliationInfo reconciliateVoucherFlow(TransactionInterfaceBean transactionInterfaceBean) {
        TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
        List<String> transactionIdList = new ArrayList<String>();
        transactionIdList.add(transactionBean.getTransactionID());
        String requestID = String.valueOf(new Date().getTime());

        if (transactionBean.getVoucherReconciliation()) {
            transactionBean.setVoucherReconciliation(false);
            entityManager.merge(transactionBean);
            System.out.println("il campo voucherReconciliation non serve a true");
        }

        System.out.println("chiamata a transactionReconciliation del Forecourt");
        TransactionReconciliationResponse forecourtResponse = forecourtService.transactionReconciliation(requestID, transactionIdList);
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();

        if (forecourtResponse == null || forecourtResponse.getTransactionDetails().isEmpty()) {
            System.out.println("errore nella operazione di riconciliazione - risposta Forecourt nulla o vuota (transazione: " + transactionBean.getTransactionID() + ")");
            // la cancellazione non � andata a buon fine
            reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(transactionBean);
            return reconciliationInfo;
        }

        TransactionDetail transactionDetail = forecourtResponse.getTransactionDetails().get(0);
        RefuelDetail refuelDetail = transactionDetail.getRefuelDetail();
        updateTransactionRefuelDetail(transactionBean, refuelDetail);

        if (refuelDetail == null || refuelDetail.getAmount() == null || refuelDetail.getAmount() == 0.0) {
            System.out.println("Importo = 0");
            System.out.println("chiamata a cancelPreAuthorizationConsumeVoucher di Quenit");

            TransactionCancelPreAuthorizationConsumeVoucherResponse response = VoucherCommonOperations.cancelPreAuthorizationConsume(transactionBean, fidelityService,
                    entityManager);

            if (response.getFidelityStatusCode() == null) {
                System.out.println("errore nella operazione di riconciliazione, chiamata cancelPreAuthorizationConsumeVoucher nulla verso Quenit " + "(transazione: "
                        + transactionBean.getTransactionID() + ")");
                // la cancellazione non � andata a buon fine
                reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                decreaseAttempts(transactionBean);
                return reconciliationInfo;
            }

            String transactionType = "CAN";
            String amount = "0.0";
            String errorCode = response.getFidelityStatusCode();
            String transactionResult = "OK";

            if (transactionBean.getFinalAmount() != null) {
                amount = transactionBean.getFinalAmount().toString();
            }

            if (!errorCode.equals("0")) {
                transactionResult = "KO";
            }

            GestPayData gestPayData = new GestPayData();
            gestPayData.setAuthorizationCode(transactionBean.getAuthorizationCode());
            gestPayData.setBankTransactionID(transactionBean.getBankTansactionID());
            gestPayData.setTransactionType(transactionType);
            gestPayData.setErrorDescription(response.getFidelityStatusMessage());
            gestPayData.setErrorCode(errorCode);
            gestPayData.setTransactionResult(transactionResult);
            gestPayData.setAmount(amount);

            reconciliationInfo = deleteTransactionCompletion(transactionBean, gestPayData, false, getStatus());
            if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS)) {
                System.out.println("operazione di riconciliazione eseguita (transazione: " + transactionBean.getTransactionID() + ")");
            }
            else {
                System.out.println("errore nella operazione di riconciliazione durante l'aggiornamento dati Backend (transazione: " + transactionBean.getTransactionID() + ")");
            }

            return reconciliationInfo;
        }

        System.out.println("Importo > 0.0");

        double finalAmount = refuelDetail.getAmount().doubleValue();

        System.out.println("Controllo un eventuale consumo voucher");
        EventResult verifyVoucher = verifyVoucherTransaction(transactionBean);
        Double voucherAmount = (Double) verifyVoucher.getAmount();
        System.out.println("Importo voucher consumati " + voucherAmount);
        System.out.println("Amount iniziale: " + finalAmount + ", Voucher Amount : " + voucherAmount);

        if (verifyVoucher.getResult().equals(EventResultType.TO_RETRY)) {
            decreaseAttempts(transactionInterfaceBean);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            return reconciliationInfo;
        }

        if (voucherAmount == 0.0) {
            System.out.println("Verifica voucher consumati = 0.0 --> chiamata  consumeVoucher");
            System.out.println("chiamata a consumeVoucher di Quenit");

            TransactionConsumeVoucherPreAuthResponse response = VoucherCommonOperations.consumePreAuthorization(transactionBean, finalAmount, fidelityService, entityManager);

            if (response.getFidelityStatusCode() == null) {
                System.out.println("errore nella operazione di riconciliazione, chiamata consumeVoucher nulla verso Quenit " + "(transazione: "
                        + transactionBean.getTransactionID() + ")");
                // la cancellazione non � andata a buon fine
                reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                decreaseAttempts(transactionBean);
                return reconciliationInfo;
            }

            String transactionType = "CAN";
            String amount = String.valueOf(finalAmount);
            String errorCode = response.getFidelityStatusCode();
            String transactionResult = "OK";

            if (!errorCode.equals("0")) {
                transactionResult = "KO";
            }

            GestPayData gestPayData = new GestPayData();
            gestPayData.setAuthorizationCode(transactionBean.getAuthorizationCode());
            gestPayData.setBankTransactionID(transactionBean.getBankTansactionID());
            gestPayData.setTransactionType(transactionType);
            gestPayData.setErrorDescription(response.getFidelityStatusMessage());
            gestPayData.setErrorCode(errorCode);
            gestPayData.setTransactionResult(transactionResult);
            gestPayData.setAmount(amount);

            updateAmount(transactionBean, finalAmount);

            reconciliationInfo = payTransactionCompletion(transactionBean, gestPayData, finalAmount, getStatus());
        }
        else {
            System.out.println("Verifica voucher consumati > 0.0 --> chiamata consumeVoucher non necessaria");
            GestPayData gestPayData = new GestPayData();
            gestPayData.setAuthorizationCode(transactionBean.getAuthorizationCode());
            gestPayData.setBankTransactionID(transactionBean.getBankTansactionID());
            gestPayData.setTransactionType("CAN");
            gestPayData.setErrorDescription(null);
            gestPayData.setErrorCode("0");
            gestPayData.setTransactionResult("OK");
            gestPayData.setAmount(String.valueOf(finalAmount));

            reconciliationInfo = payTransactionCompletion(transactionBean, gestPayData, finalAmount, getStatus());

            /*GestPayData gestPayData = new GestPayData();
            gestPayData.setAuthorizationCode(transactionBean.getAuthorizationCode());
            gestPayData.setBankTransactionID(transactionBean.getBankTansactionID());
            gestPayData.setTransactionType("CAN");
            gestPayData.setErrorDescription(null);
            gestPayData.setErrorCode("0");
            gestPayData.setTransactionResult("OK");
            gestPayData.setAmount(String.valueOf(finalAmount));
            
            System.out.println("Invia notifica pagamento al GFG");
            SendPaymentTransactionResultResponse sendPaymentTransactionResultResponse = notifyGFG(transactionInterfaceBean, gestPayData);
            if (sendPaymentTransactionResultResponse.getStatusCode().contains("200")) {
                updateNotification(transactionBean, true);
                updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
            }
            else if (sendPaymentTransactionResultResponse.getStatusCode().contains("500")) {
                decreaseAttempts(transactionInterfaceBean);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            }
            else if (sendPaymentTransactionResultResponse.getStatusCode().contains("400")) {
                updateFinalStatus(transactionInterfaceBean, StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                reconciliationInfo.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
            }*/
        }

        return reconciliationInfo;
    }

    @Override
    public String getStatus() {
        return StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "MISSING PAYMENT";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsByFinalStatusType(entityManager, getStatus());
        return transactionBeanList;
    }

}
