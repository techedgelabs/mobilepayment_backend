package com.techedge.mp.core.actions.reconciliation.status.prepaid;

import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.TransactionServiceRemote;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCancelPreAuthorizationConsumeVoucherResponse;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.voucher.VoucherCommonOperations;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public class MissingPayAuthDeleteBeforeRefuelStatus extends ReconciliationPrePaidTransactionStatus {

    public MissingPayAuthDeleteBeforeRefuelStatus(EntityManager entityManager, GPServiceRemote gpService, TransactionServiceRemote transactionService, 
            ForecourtInfoServiceRemote forecourtService, FidelityServiceRemote fidelityService, CRMServiceRemote crmService, EmailSenderRemote emailSender, String proxyHost, 
            String proxyPort, String proxyNoHosts, String secretKey, UserCategoryService userCategoryService, StringSubstitution stringSubstitution, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2) {
        
        super(entityManager, gpService, transactionService, forecourtService, fidelityService, crmService, emailSender, proxyHost, proxyPort, proxyNoHosts, secretKey, 
                userCategoryService, stringSubstitution, refuelingNotificationService, refuelingNotificationServiceOAuth2);
    }

    @Override
    public ReconciliationInfo reconciliateCreditCardFlow(TransactionInterfaceBean transactionInterfaceBean) {
        TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
        GestPayData gestPayData = null;
        ReconciliationInfo reconciliationInfo;
        
        String decodedSecretKey = "";
        try {
            EncryptionAES encryptionAES = new EncryptionAES();
            encryptionAES.loadSecretKey(this.secretKey);
            if (transactionBean.getEncodedSecretKey() != null) {
                decodedSecretKey = encryptionAES.decrypt(transactionBean.getEncodedSecretKey());
            }
            else {
                System.out.println("EncodedSecretKey non valorizzato per la transazione " + transactionBean.getTransactionID());
            }
        }
        catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        System.out.println("chiamata a callReadTrx dell'acquirer");
        reconciliationInfo = new ReconciliationInfo();
        GestPayData gestPayDataReadTrx = gpService.callReadTrx(transactionBean.getShopLogin(), transactionBean.getTransactionID(), transactionBean.getBankTansactionID(),
                transactionBean.getAcquirerID(), transactionBean.getGroupAcquirer(), decodedSecretKey);

        if (gestPayDataReadTrx == null) {
            System.out.println("errore nella operazione di riconciliazione, chiamata callReadTrx nulla verso l'acquirer(transazione: "
                    + transactionInterfaceBean.getTransactionID() + ")");
            reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(transactionBean);
            return reconciliationInfo;
        }

        if (gestPayDataReadTrx.getTransactionState().equals("AUT") && gestPayDataReadTrx.getTransactionResult().equalsIgnoreCase("OK")) {
        
            System.out.println("chiamata a deletePagam di Banca Sella");
            gestPayData = gpService.deletePagam(transactionBean.getInitialAmount(), transactionBean.getTransactionID(), transactionBean.getShopLogin(), transactionBean.getCurrency(),
                    transactionBean.getBankTansactionID(), "RECONCILE", transactionBean.getAcquirerID(), transactionBean.getGroupAcquirer(), decodedSecretKey);
    
            if (gestPayData == null) {
                System.out.println("errore nella operazione di riconciliazione, chiamata deletePagam nulla verso Banca Sella (transazione: " + transactionBean.getTransactionID() + ")");
                // la cancellazione non � andata a buon fine
                reconciliationInfo = new ReconciliationInfo();
                reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
                decreaseAttempts(transactionBean);
                return reconciliationInfo;
            }
            else {
                reconciliationInfo = deleteTransactionCompletion(transactionInterfaceBean, gestPayData, true, getStatus());
                if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS)) {
                    System.out.println("operazione di riconciliazione eseguita (transazione: " + transactionBean.getTransactionID() + ")");
                }
                else {
                    System.out.println("errore nella operazione di riconciliazione, aggiornamento dati MP (transazione: " + transactionBean.getTransactionID() + ", status: " + reconciliationInfo.getStatusCode() + ")");
                }
            }
        }
        else {
            reconciliationInfo = deleteTransactionCompletion(transactionInterfaceBean, gestPayDataReadTrx, true, getStatus());
            if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS)) {
                System.out.println("operazione di riconciliazione eseguita (transazione: " + transactionBean.getTransactionID() + ")");
            }
            else {
                System.out.println("errore nella operazione di riconciliazione, aggiornamento dati MP (transazione: " + transactionBean.getTransactionID() + ", status: " + reconciliationInfo.getStatusCode() + ")");
            }
        }

        return reconciliationInfo;
    }

    @Override
    public ReconciliationInfo reconciliateVoucherFlow(TransactionInterfaceBean transactionInterfaceBean) {
        TransactionBean transactionBean = (TransactionBean) transactionInterfaceBean;
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        System.out.println("chiamata a cancelPreAuthorizationConsumeVoucher di Quenit");
        TransactionCancelPreAuthorizationConsumeVoucherResponse response = VoucherCommonOperations.cancelPreAuthorizationConsume(transactionBean, fidelityService, entityManager);

        if (response.getFidelityStatusCode() == null) {
            System.out.println("errore nella operazione di riconciliazione, chiamata cancelPreAuthorizationConsumeVoucher nulla verso Quenit "
                    + "(transazione: " + transactionBean.getTransactionID() + ")");
            // la cancellazione non � andata a buon fine
            reconciliationInfo.setFinalStatusType(transactionBean.getFinalStatusType());
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(transactionBean);
            return reconciliationInfo;
        }

        String transactionType = "CAN";
        String amount = "0.0";
        String errorCode = response.getFidelityStatusCode();
        String transactionResult = "OK";
        
        if (transactionBean.getFinalAmount() != null) {
            amount = transactionBean.getFinalAmount().toString();
        }

        if (!errorCode.equals("0")) {
            transactionResult = "KO";
        }

        GestPayData gestPayData = new GestPayData();
        gestPayData.setAuthorizationCode(transactionBean.getAuthorizationCode());
        gestPayData.setBankTransactionID(transactionBean.getBankTansactionID());
        gestPayData.setTransactionType(transactionType);
        gestPayData.setErrorDescription(response.getFidelityStatusMessage());
        gestPayData.setErrorCode(errorCode);
        gestPayData.setTransactionResult(transactionResult);
        gestPayData.setAmount(amount);
        
        reconciliationInfo = deleteTransactionCompletion(transactionInterfaceBean, gestPayData, true, getStatus());
        if (reconciliationInfo.getStatusCode().equals(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS)) {
            System.out.println("operazione di riconciliazione eseguita (transazione: " + transactionBean.getTransactionID() + ")");
        }
        else {
            System.out.println("errore nella operazione di riconciliazione durante l'aggiornamento dati Backend (transazione: " + transactionBean.getTransactionID() + ")");
        }

        return reconciliationInfo;
    }
        

    @Override
    public String getStatus() {
        return StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYAUTH_DELETE_BEFORE_REFUEL;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "MISSING PAYAUTH BEFORE REFUEL";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsByFinalStatusType(entityManager, getStatus());
        return transactionBeanList;
    }
}
