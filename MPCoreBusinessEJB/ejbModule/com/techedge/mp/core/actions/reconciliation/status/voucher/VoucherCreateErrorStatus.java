package com.techedge.mp.core.actions.reconciliation.status.voucher;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.TransactionInterfaceBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionEventBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionOperationBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionStatusBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.EventType;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.DeleteVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

public class VoucherCreateErrorStatus extends ReconciliationVoucherTransactionStatus {

    private String deleteVoucherStatus = "ERROR";
    private String deletePagamStatus   = "ERROR";

    public VoucherCreateErrorStatus(EntityManager entityManager, GPServiceRemote gpService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSenderService, String proxyHost, String proxyPort, String proxyNoHosts) {

        super(entityManager, gpService, fidelityService, emailSenderService, proxyHost, proxyPort, proxyNoHosts);
    }

    @Override
    public ReconciliationInfo reconciliateCreditCardFlow(TransactionInterfaceBean transactionInterfaceBean) {
        System.out.println("Riconciliazione non disponibile per questo tipo di utente!");
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        reconciliationInfo.setFinalStatusType(transactionInterfaceBean.getTransactionStatus());
        reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
        return reconciliationInfo;
    }
    
    @Override
    public ReconciliationInfo reconciliateVoucherFlow(TransactionInterfaceBean transactionInterfaceBean) {
        VoucherTransactionBean voucherTransactionBean = (VoucherTransactionBean) transactionInterfaceBean;
        GestPayData gestPayData = null;
        ReconciliationInfo reconciliationInfo = null;
        VoucherTransactionOperationBean createVoucherTransactionOperationBean = getLastCreateTransactionOperation(voucherTransactionBean);

        VoucherTransactionStatusBean voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(StatusHelper.VOUCHER_STATUS_RECONCILIATION, "START RECONCILIATION", null);
        entityManager.persist(voucherTransactionStatusBean);

        String status = StatusHelper.VOUCHER_STATUS_DELETE_OK;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = null;
        Double eventAmount = voucherTransactionBean.getAmount();



        if (createVoucherTransactionOperationBean == null) {
            System.out.println("Opsss e' successo qualcosa di strano!!!! VoucherTransactionOperationBean nullo, come facciamo ad essere qui????");
            status = StatusHelper.VOUCHER_STATUS_DELETE_ERROR;
            eventResult = "ERROR";
        }
        else {
            System.out.println("chiamata a deleteVoucher di Quenit");

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            Long requestTimestamp = new Date().getTime();
            String createVoucherOperationID = createVoucherTransactionOperationBean.getOperationID();
            PartnerType partnerType = PartnerType.MP;

            DeleteVoucherResult deleteVoucherResult = new DeleteVoucherResult();

            VoucherTransactionOperationBean voucherTransactionOperationBean = new VoucherTransactionOperationBean();
            voucherTransactionOperationBean.setOperationID(operationID);
            voucherTransactionOperationBean.setOperationIDReversed(createVoucherOperationID);
            voucherTransactionOperationBean.setRequestTimestamp(new Date());
            voucherTransactionOperationBean.setOperationType("DELETE");
            voucherTransactionOperationBean.setVoucherTransactionBean(voucherTransactionBean);

            errorCode = deleteVoucherResult.getStatusCode();
            errorDescription = deleteVoucherResult.getMessageCode();
            eventAmount = voucherTransactionOperationBean.getAmount();

            if (eventAmount == null) {
                eventAmount = 0.0;
            }
            
            try {

                deleteVoucherResult = fidelityService.deleteVoucher(createVoucherOperationID, operationID, partnerType, requestTimestamp);

                voucherTransactionOperationBean.setCsTransactionID(deleteVoucherResult.getCsTransactionID());
                voucherTransactionOperationBean.setMessageCode(deleteVoucherResult.getMessageCode());
                voucherTransactionOperationBean.setStatusCode(deleteVoucherResult.getStatusCode());

                errorCode = deleteVoucherResult.getStatusCode();
                errorDescription = deleteVoucherResult.getMessageCode();

                if (deleteVoucherResult.getStatusCode().equals(FidelityResponse.DELETE_VOUCHER_OK)
                        || deleteVoucherResult.getStatusCode().equals(FidelityResponse.DELETE_VOUCHER_TRANSACTION_ALREADY_DELETED)
                        || deleteVoucherResult.getStatusCode().equals(FidelityResponse.DELETE_VOUCHER_TRANSACTION_NOT_FOUND)) {

                    eventResult = "OK";

                    // Ripristina il valore iniziale dei voucher consumati
                    System.out.println("chiamata alla checkVoucher di Quenit per rispristinare lo stato del voucher");

                    String operationID_check = new IdGenerator().generateId(16).substring(0, 33);
                    VoucherConsumerType voucherConsumerType_check = VoucherConsumerType.ENI;
                    PartnerType partnerType_check = PartnerType.MP;
                    Long requestTimestamp_check = new Date().getTime();

                    List<VoucherCodeDetail> voucherCodeList_check = new ArrayList<VoucherCodeDetail>(0);
                    VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                    voucherCodeDetail.setVoucherCode(voucherTransactionBean.getVoucherCode());
                    voucherCodeList_check.add(voucherCodeDetail);

                    CheckVoucherResult checkVoucherResult = new CheckVoucherResult();

                    try {

                        checkVoucherResult = fidelityService.checkVoucher(operationID_check, voucherConsumerType_check, partnerType_check, requestTimestamp_check,
                                voucherCodeList_check);
                        // Verifica l'esito del check
                        String checkVoucherStatusCode = checkVoucherResult.getStatusCode();

                        if (!checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_OK)) {
                            // Error
                            System.out.println("Error checking voucher: " + checkVoucherStatusCode);
                            //throw new FidelityServiceException("Error checking voucher: " + checkVoucherStatusCode);
                        }
                        else {

                            // Aggiorna lo stato dei voucher dell'utente

                            // Ricerca il voucher nella risposta del servizio di verifica
                            VoucherDetail voucherDetail = checkVoucherResult.getVoucherList().get(0);
                            System.out.println("Elaborazione voucher: " + voucherDetail.getVoucherCode() + " in stato: " + voucherDetail.getVoucherStatus());

                            for (VoucherBean voucherBean : voucherTransactionBean.getUserBean().getVoucherList()) {

                                if (voucherBean.getCode().equals(voucherDetail.getVoucherCode())) {

                                    System.out.println("Aggiornamento Voucher " + voucherDetail.getVoucherCode());

                                    // Aggiorna il voucher con i dati restituiti dal servizio
                                    voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                                    voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                                    voucherBean.setInitialValue(voucherDetail.getInitialValue());
                                    voucherBean.setPromoCode(voucherDetail.getPromoCode());
                                    voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                                    voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                                    voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                                    voucherBean.setCode(voucherDetail.getVoucherCode());
                                    voucherBean.setStatus(voucherDetail.getVoucherStatus());
                                    voucherBean.setType(voucherDetail.getVoucherType());
                                    voucherBean.setValue(voucherDetail.getVoucherValue());

                                    System.out.println("Voucher aggiornato");
                                }
                            }

                            // Aggiorna l'utente
                            entityManager.merge(voucherTransactionBean.getUserBean());

                        }
                    }
                    catch (Exception ex) {
                        System.out.println("Error in checkVoucherResult: " + ex.getMessage());
                        //throw new FidelityServiceException("Error in checkVoucherResult: " + e.getMessage());
                    }
                }
                else {
                    eventResult = "ERROR";
                    status = StatusHelper.VOUCHER_STATUS_DELETE_ERROR;
                }
            }
            catch (Exception e) {

                System.err.println("Error deleting voucher: " + e.getMessage());

                errorCode = FidelityResponse.DELETE_VOUCHER_SYSTEM_ERROR;
                errorDescription = e.getMessage();
                eventResult = "ERROR";
                status = StatusHelper.VOUCHER_STATUS_DELETE_ERROR;

                voucherTransactionOperationBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
                voucherTransactionOperationBean.setMessageCode("STATO DA VERIFICARE (" + e.getMessage() + ")");
                deleteVoucherResult.setStatusCode(errorCode);
                deleteVoucherResult.setMessageCode(errorDescription);
            }

            voucherTransactionBean.getTransactionOperationBeanList().add(voucherTransactionOperationBean);
            entityManager.persist(voucherTransactionOperationBean);
        }

        deleteVoucherStatus = eventResult;
        voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(status, null, null);
        entityManager.persist(voucherTransactionStatusBean);
        VoucherTransactionEventBean voucherTransactionEventBean = voucherTransactionBean.addVoucherTransactionEventBean(EventType.DEL, eventAmount, errorCode, errorDescription,
                eventResult);
        entityManager.persist(voucherTransactionEventBean);
        entityManager.merge(voucherTransactionBean);

        status = StatusHelper.VOUCHER_STATUS_CAN_OK;
        errorCode = null;
        errorDescription = null;
        eventResult = "ERROR";

        System.out.println("chiamata a callReadTrx di Banca Sella");
        gestPayData = gpService.callReadTrx(voucherTransactionBean.getShopLogin(), voucherTransactionBean.getTransactionID(), voucherTransactionBean.getBankTansactionID(),
                voucherTransactionBean.getAcquirerID(), voucherTransactionBean.getGroupAcquirer(), voucherTransactionBean.getEncodedSecretKey());

        if (gestPayData == null) {
            System.out.println("errore nella operazione di riconciliazione, chiamata callReadTrx nulla verso Banca Sella (transazione: "
                    + voucherTransactionBean.getTransactionID() + ")");
            eventResult = "ERROR";
            status = StatusHelper.VOUCHER_STATUS_CAN_ERROR;
        }
        else {
            if (gestPayData.getTransactionState().equals("CAN") && gestPayData.getTransactionResult().equalsIgnoreCase("OK")) {
                System.out.println("Cancellazione dell'autorizzazione gi� eseguita sulla transazione");
                eventResult = "OK";
            }

            if (gestPayData.getTransactionState().equals("AUT") && gestPayData.getTransactionResult().equalsIgnoreCase("OK")) {
                System.out.println("chiamata a deletePagam di Banca Sella");

                gestPayData = gpService.deletePagam(voucherTransactionBean.getInitialAmount(), voucherTransactionBean.getTransactionID(), voucherTransactionBean.getShopLogin(),
                        voucherTransactionBean.getCurrency(), voucherTransactionBean.getBankTansactionID(), "RECONCILE", voucherTransactionBean.getAcquirerID(), voucherTransactionBean.getGroupAcquirer(), voucherTransactionBean.getEncodedSecretKey());

                if (gestPayData == null) {
                    System.out.println("errore nella operazione di riconciliazione, chiamata deletePagam nulla verso Banca Sella (transazione: "
                            + voucherTransactionBean.getTransactionID() + ")");
                    eventResult = "ERROR";
                    status = StatusHelper.VOUCHER_STATUS_CAN_ERROR;
                }
                else {
                    errorCode = gestPayData.getErrorCode();
                    errorDescription = gestPayData.getErrorDescription();

                    if (errorCode.equals("0")) {
                        eventResult = "OK";
                    }
                    else {
                        eventResult = "KO";
                        status = StatusHelper.VOUCHER_STATUS_CAN_ERROR;
                    }
                }
            }
        }
        
        deletePagamStatus = eventResult;
        voucherTransactionStatusBean = voucherTransactionBean.addVoucherTransactionStatusBean(status, null, null);
        entityManager.persist(voucherTransactionStatusBean);
        voucherTransactionEventBean = voucherTransactionBean.addVoucherTransactionEventBean(EventType.CAN, eventAmount, errorCode, errorDescription, eventResult);
        entityManager.persist(voucherTransactionEventBean);
        entityManager.merge(voucherTransactionBean);

        reconciliationInfo = finalizeReconciliationInfo(voucherTransactionBean);

        if (eventResult.equals("OK")) {
            Email.sendVoucherSummary(emailSenderService, voucherTransactionBean, proxyHost, proxyPort, proxyNoHosts);
        }
        
        return reconciliationInfo;
    }

    private VoucherTransactionOperationBean getLastCreateTransactionOperation(VoucherTransactionBean voucherTransactionBean) {
        for (VoucherTransactionOperationBean voucherTransactionOperationBean : voucherTransactionBean.getTransactionOperationBeanList()) {
            if (voucherTransactionOperationBean.getOperationType().equals("CREATE")) {
                return voucherTransactionOperationBean;
            }
        }

        return null;
    }

    private ReconciliationInfo finalizeReconciliationInfo(VoucherTransactionBean voucherTransactionBean) {
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        reconciliationInfo.setTransactionID(voucherTransactionBean.getTransactionID());

        if (deleteVoucherStatus.equals("OK") && deletePagamStatus.equals("OK")) {
            updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_CANCELED);
            reconciliationInfo.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_CANCELED);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
        }

        if (deleteVoucherStatus.equals("OK") && deletePagamStatus.equals("ERROR")) {
            updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE);
            reconciliationInfo.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(voucherTransactionBean);
        }

        if (deleteVoucherStatus.equals("OK") && deletePagamStatus.equals("KO")) {
            updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_NOT_RECONCILIABLE);
            reconciliationInfo.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_NOT_RECONCILIABLE);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
        }

        if (deleteVoucherStatus.equals("KO") && deletePagamStatus.equals("KO")) {
            updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_NOT_RECONCILIABLE);
            reconciliationInfo.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_NOT_RECONCILIABLE);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
        }

        if (deleteVoucherStatus.equals("KO") && deletePagamStatus.equals("OK")) {
            updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_NOT_RECONCILIABLE);
            reconciliationInfo.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_NOT_RECONCILIABLE);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);
        }

        if (deleteVoucherStatus.equals("KO") && deletePagamStatus.equals("ERROR")) {
            updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE);
            reconciliationInfo.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(voucherTransactionBean);
        }

        if (deleteVoucherStatus.equals("ERROR") && deletePagamStatus.equals("OK")) {
            updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE);
            reconciliationInfo.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(voucherTransactionBean);
        }

        if (deleteVoucherStatus.equals("ERROR") && deletePagamStatus.equals("KO")) {
            updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE);
            reconciliationInfo.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(voucherTransactionBean);
        }

        if (deleteVoucherStatus.equals("ERROR") && deletePagamStatus.equals("ERROR")) {
            updateFinalStatus(voucherTransactionBean, StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE);
            reconciliationInfo.setFinalStatusType(StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
            decreaseAttempts(voucherTransactionBean);
        }

        return reconciliationInfo;
    }
    
    @Override
    public String getStatus() {
        return StatusHelper.VOUCHER_FINAL_STATUS_ERROR_CREATE;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "ERROR CREATE VOUCHER";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<VoucherTransactionBean> transactionBeanList = QueryRepository.findVoucherTransactionsByFinalStatusType(entityManager, getStatus());
        return transactionBeanList;
    }

    @Override
    public int decreaseAttempts(TransactionInterfaceBean transactionInterfaceBean) {
        return 0;
    }

}
