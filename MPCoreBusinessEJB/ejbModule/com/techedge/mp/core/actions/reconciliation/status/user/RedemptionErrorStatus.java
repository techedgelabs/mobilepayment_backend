package com.techedge.mp.core.actions.reconciliation.status.user;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;

import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationInfo;
import com.techedge.mp.core.business.model.UserReconciliationInterfaceBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.loyalty.RedemptionTransactionBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.RedemptionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public class RedemptionErrorStatus extends ReconciliationUserStatus {

    public RedemptionErrorStatus(EntityManager entityManager, DWHAdapterServiceRemote dwhAdapterService, CRMAdapterServiceRemote crmAdapterService, 
            PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, ParametersServiceRemote parametersService, String proxyHost, String proxyPort, String proxyNoHosts, 
            Integer pushNotificationExpiryTime, Integer reconciliationAttemptsLeft, Boolean refuelingOauth2Active) {
        
        super(entityManager, dwhAdapterService, crmAdapterService, pushNotificationService, fidelityService, emailSender, refuelingNotificationService, refuelingNotificationServiceOAuth2, parametersService, proxyHost, 
                proxyPort, proxyNoHosts, pushNotificationExpiryTime, reconciliationAttemptsLeft, refuelingOauth2Active);
    }

    @Override
    public ReconciliationInfo reconciliate(UserReconciliationInterfaceBean userReconciliationInterfaceBean) {
        ReconciliationInfo reconciliationInfo = new ReconciliationInfo();
        RedemptionTransactionBean redemptionTransactionBean = (RedemptionTransactionBean) userReconciliationInterfaceBean;
        
        String operationID = new IdGenerator().generateId(16).substring(0, 33);
        long requestTimestamp = new Date().getTime();
        PartnerType partnerType = PartnerType.MP; 
        String fiscalCode = redemptionTransactionBean.getUserBean().getPersonalDataBean().getFiscalCode();
        Integer redemptionCode = redemptionTransactionBean.getRedemptionCode();
        
        redemptionTransactionBean.setOperationID(operationID);
        redemptionTransactionBean.setRequestTimestamp(new Date(requestTimestamp));

        entityManager.persist(redemptionTransactionBean);
        
        System.out.println("chiamata al servizio redemption del fidelity");
        
        try {
            RedemptionResult redemptionResult = fidelityService.redemption(operationID, partnerType, requestTimestamp, fiscalCode, redemptionCode);

            if (!redemptionResult.getStatusCode().equals(FidelityResponse.REDEMPTION_OK)) {
                
                System.err.println("Errore redemption: " + redemptionResult.getMessageCode() + " (" + redemptionResult.getStatusCode() + ")");
                
                redemptionTransactionBean.setStatusCode(redemptionResult.getStatusCode());
                redemptionTransactionBean.setMessageCode(redemptionResult.getMessageCode());
                redemptionTransactionBean.setToReconcilie(false);
                
                entityManager.merge(redemptionTransactionBean);
                
                reconciliationInfo.setFinalStatusType(StatusHelper.REDEMPTION_STATUS_ERROR);
                reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_FAILURE);

                return reconciliationInfo;
            }

            VoucherDetail voucherDetail = redemptionResult.getVoucher();
            VoucherBean voucherBean = new VoucherBean();
            voucherBean.setCode(voucherDetail.getVoucherCode());
            voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
            voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
            voucherBean.setInitialValue(voucherDetail.getInitialValue());
            voucherBean.setIsCombinable(voucherDetail.getIsCombinable());
            voucherBean.setMinAmount(voucherDetail.getMinAmount());
            voucherBean.setMinQuantity(voucherDetail.getMinQuantity());
            voucherBean.setPromoCode(voucherDetail.getPromoCode());
            voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
            voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
            voucherBean.setPromoPartner(voucherDetail.getPromoPartner());
            voucherBean.setStatus(voucherDetail.getVoucherStatus());
            voucherBean.setType(voucherDetail.getVoucherType());
            voucherBean.setUserBean(redemptionTransactionBean.getUserBean());
            voucherBean.setValidPV(voucherDetail.getValidPV());
            voucherBean.setValue(voucherDetail.getVoucherValue());
            voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());

            entityManager.persist(voucherBean);

            redemptionTransactionBean.setBalance(redemptionResult.getBalance());
            redemptionTransactionBean.setBalanceAmount(redemptionResult.getBalanceAmount());
            redemptionTransactionBean.setCredits(redemptionResult.getCredits());
            redemptionTransactionBean.setCsTransactionID(redemptionResult.getCsTransactionID());
            redemptionTransactionBean.setMarketingMsg(redemptionResult.getMarketingMsg());
            redemptionTransactionBean.setMessageCode(redemptionResult.getMessageCode());
            redemptionTransactionBean.setStatusCode(redemptionResult.getStatusCode());
            redemptionTransactionBean.setVoucherBean(voucherBean);
            redemptionTransactionBean.setWarningMsg(redemptionResult.getWarningMsg());
            redemptionTransactionBean.setToReconcilie(false);
            
            entityManager.merge(redemptionTransactionBean);
            
            reconciliationInfo.setFinalStatusType(StatusHelper.REDEMPTION_STATUS_SUCCESS);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_SUCCESS);
        }
        catch (FidelityServiceException ex) {
            System.err.println("Unable to redemption: " + ex.getMessage());
            
            decreaseAttempts(redemptionTransactionBean);

            reconciliationInfo.setFinalStatusType(StatusHelper.REDEMPTION_STATUS_SUCCESS);
            reconciliationInfo.setStatusCode(ResponseHelper.RECONCILIATION_TRANSACTION_TO_RETRY);
        }
        
        return reconciliationInfo;
    }

    @Override
    public String getStatus() {
        return StatusHelper.REDEMPTION_STATUS_ERROR;
    }

    @Override
    public String getSubStatus() {
        return null;
    }

    @Override
    public String getName() {
        return "REDEMPTION ERROR";
    }

    @SuppressWarnings("rawtypes")
    @Override
    public List getTransactionsList() {
        List<RedemptionTransactionBean> redemptionTransactionBeanList = QueryRepository.findRedemptionToReconcilie(entityManager);
        return redemptionTransactionBeanList;
    }

}
