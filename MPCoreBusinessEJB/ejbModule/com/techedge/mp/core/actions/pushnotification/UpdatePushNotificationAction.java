package com.techedge.mp.core.actions.pushnotification;

import java.util.Calendar;
import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UpdatePushNotificationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UpdatePushNotificationAction() {}

    public String execute(Long pushNotificationID, String publishMessageID, String endpoint, PushNotificationSourceType source, String title, String message,
            PushNotificationContentType contentType, Date sendingTimestamp, PushNotificationStatusType statusCode, String statusMessage, Long userID, Integer expiryTime,
            Integer maxRetryAttemps) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            PushNotificationBean pushNotificationBean = QueryRepository.findPushNotificationById(em, pushNotificationID);
            UserBean userBean = QueryRepository.findUserById(em, userID);

            if (pushNotificationBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Push notification not found");

                userTransaction.commit();

                return ResponseHelper.USER_UPDATE_PUSH_NOTIFICATION_FAILURE;
            }

            if (userBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Push notification user not found");

                userTransaction.commit();

                return ResponseHelper.USER_UPDATE_PUSH_NOTIFICATION_FAILURE;
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(sendingTimestamp);
            calendar.add(Calendar.SECOND, expiryTime);

            pushNotificationBean.setEndpoint(endpoint);
            pushNotificationBean.setPublishMessageID(publishMessageID);
            pushNotificationBean.setTitle(title);
            pushNotificationBean.setMessage(message);
            pushNotificationBean.setSendingTimestamp(sendingTimestamp);
            pushNotificationBean.setExpiringTimestamp(calendar.getTime());
            pushNotificationBean.setSource(source);
            if (contentType != null) {
                pushNotificationBean.setContentType(contentType);
            }
            pushNotificationBean.setStatusCode(statusCode);
            pushNotificationBean.setStatusMessage(statusMessage);
            pushNotificationBean.setUser(userBean);
            pushNotificationBean.setRetryAttemptsLeft(maxRetryAttemps);
            
            if (statusCode.equals(PushNotificationStatusType.ERROR)) {
                pushNotificationBean.setToReconcilie(true);
            }
            else {
                pushNotificationBean.setToReconcilie(false);
            }

            em.merge(pushNotificationBean);

            userTransaction.commit();

            return ResponseHelper.USER_UPDATE_PUSH_NOTIFICATION_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String errorMessage = "FAILED update push notification log with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, errorMessage);

            throw new EJBException(ex2);
        }
    }
}
