package com.techedge.mp.core.actions.pushnotification;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AppLink;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ParameterNotification;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundDeliveryStatusType;
import com.techedge.mp.core.business.interfaces.pushnotification.NotificationResponse;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crm.CRMEventBean;
import com.techedge.mp.core.business.model.crm.CRMOfferBean;
import com.techedge.mp.core.business.model.crm.CRMOfferParametersBean;
import com.techedge.mp.core.business.model.crm.CRMOutboundBean;
import com.techedge.mp.core.business.model.loyalty.EventNotificationBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class RetrieveNotificationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public RetrieveNotificationAction() {}

    public NotificationResponse execute(String ticketId, String requestId, Long notificationId) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                return createMessageError(requestId, userTransaction, "Invalid ticket", ResponseHelper.EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_INVALID_TICKET);
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Utente non valido
                return createMessageError(requestId, userTransaction, "User not found", ResponseHelper.EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_INVALID_TICKET);

            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                return createMessageError(requestId, userTransaction, "Unable to retrive notification for user in status " + String.valueOf(userStatus),
                        ResponseHelper.EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_UNAUTHORIZED);
            }

            PushNotificationBean pushNotificationBean = QueryRepository.findPushNotificationById(em, notificationId);

            if (pushNotificationBean == null) {

                // Push Notification  non valida
                return createMessageError(requestId, userTransaction, "Push Notification not found", ResponseHelper.EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_NOT_FOUND);

            }

            NotificationResponse notificationResponse = new NotificationResponse();

            if (pushNotificationBean.getSource().equals(PushNotificationSourceType.LOYALTY)) {

                System.out.println("Trovata push notification di tipo Loyalty");
                
                pushNotificationBean.setNotificationRead(true);
                pushNotificationBean.setNotificationReadTimestamp(new Date());
                pushNotificationBean.setStatusMessage(null);
                pushNotificationBean.setStatusCode(PushNotificationStatusType.DELIVERED);
                em.merge(pushNotificationBean);

                EventNotificationBean eventNotificationBean = QueryRepository.findEventNotificationByPushNotification(em, pushNotificationBean);

                notificationResponse.setId(pushNotificationBean.getId());
                notificationResponse.setSource(pushNotificationBean.getSource());
                notificationResponse.setTitle(pushNotificationBean.getTitle());
                String text = pushNotificationBean.getMessage().replaceAll("(\\w|=|@|\\+|\\ |\\-)*text=", "");
                notificationResponse.setText(text);
                notificationResponse.setSendDate(pushNotificationBean.getSendingTimestamp());
                notificationResponse.setExpireDate(pushNotificationBean.getExpiringTimestamp());
                notificationResponse.setAppLink(new AppLink());

                Set<ParameterNotification> listParameter = new HashSet<>(0);

                if (eventNotificationBean != null) {
                    //if (eventNotificationBean.getEventType().equals(EventNotificationType.LOYALTY)) {
                        notificationResponse.setShowDialog(false);
                        notificationResponse.getAppLink().setType(PushNotificationContentType.SECTION);
                        notificationResponse.setActionText("Scontrino");
                        
                        pushNotificationBean.setContentType(PushNotificationContentType.SECTION);
                        em.merge(pushNotificationBean);
                    //}
                    //else {
                    //    notificationResponse.setShowDialog(false);
                    //}
                    notificationResponse.getAppLink().setLocation("loyaltyTransactionDetail");

                    ParameterNotification eventNotificationParameter = new ParameterNotification("eventNotificationID", String.valueOf(eventNotificationBean.getId()));
                    listParameter.add(eventNotificationParameter);
                }
                notificationResponse.getAppLink().setParameters(listParameter);
                notificationResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_SUCCESS);
            }

            if (pushNotificationBean.getSource().equals(PushNotificationSourceType.CRM_INBOUND)) {

                System.out.println("Trovata push notification di tipo Crm Inbound");
                
                pushNotificationBean.setNotificationRead(true);
                pushNotificationBean.setNotificationReadTimestamp(new Date());
                pushNotificationBean.setStatusMessage(null);
                pushNotificationBean.setStatusCode(PushNotificationStatusType.DELIVERED);
                em.merge(pushNotificationBean);

                CRMEventBean crmEventBean = QueryRepository.findCRMEventByPushNotification(em, pushNotificationBean);

                notificationResponse.setId(pushNotificationBean.getId());
                notificationResponse.setSource(pushNotificationBean.getSource());
                notificationResponse.setTitle(pushNotificationBean.getTitle());
                notificationResponse.setText(pushNotificationBean.getMessage().replaceAll("(\\w|=|@|\\+|\\ |\\-)*text=", ""));
                notificationResponse.setSendDate(pushNotificationBean.getSendingTimestamp());
                notificationResponse.setExpireDate(pushNotificationBean.getExpiringTimestamp());
                //notificationResponse.setAppLink(new AppLink());
                //notificationResponse.getAppLink().setLocation(null);
                notificationResponse.setAppLink(null);
                notificationResponse.setShowDialog(true);

                //Set<ParameterNotification> listParameter = new HashSet<>(0);

                if (crmEventBean != null) {
                    for (CRMOfferBean crmOfferBean : crmEventBean.getOfferBeanList()) {
                        for (CRMOfferParametersBean parametersBean : crmOfferBean.getCRMParametersBeanList()) {
                            String location = (String) parametersBean.getParameterValue();

                            if (parametersBean.getParameterName().equals("UrlApp") && (location != null && !location.isEmpty() && !location.equals("n/a"))) {
                                if (notificationResponse.getAppLink() == null) {
                                    notificationResponse.setAppLink(new AppLink());
                                }
                                notificationResponse.getAppLink().setType(PushNotificationContentType.INTERNAL);
                                notificationResponse.getAppLink().setLocation(location);
                                notificationResponse.setActionText("Apri");
                                notificationResponse.setShowDialog(false);

                                pushNotificationBean.setContentType(PushNotificationContentType.INTERNAL);
                                em.merge(pushNotificationBean);
                                
                                //break;
                            }
                            else if (parametersBean.getParameterName().equals("TextAPP") && (location != null && !location.equals("n/a"))) {
                                notificationResponse.setText(notificationResponse.getText() + "\n" + location);
                                //break;
                            }
                            else if (parametersBean.getParameterName().matches("C_[0-9]+")) {
                                ParameterNotification parameterNotification = new ParameterNotification(parametersBean.getParameterName(), location);
                                if (notificationResponse.getAppLink() == null) {
                                    notificationResponse.setAppLink(new AppLink());
                                }
                                notificationResponse.getAppLink().getParameters().add(parameterNotification);
                            }
                        }
                    }
                }
                else {
                    String message = pushNotificationBean.getMessage();
                    
                    if (message != null && (message.startsWith("http") || message.startsWith("https"))) {
                        notificationResponse.setAppLink(new AppLink());
                        notificationResponse.getAppLink().setType(PushNotificationContentType.INTERNAL);
                        notificationResponse.getAppLink().setLocation(message);
                        notificationResponse.setActionText("Apri");
                        notificationResponse.setText(null);
                        notificationResponse.setShowDialog(false);
                    }
                }

                //notificationResponse.getAppLink().setParameters(listParameter);
                notificationResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_SUCCESS);
            }

            if (pushNotificationBean.getSource().equals(PushNotificationSourceType.CRM_OUTBOUND)) {

                System.out.println("Trovata push notification di tipo Crm Outbound");
                /*
                pushNotificationBean.setNotificationRead(true);
                pushNotificationBean.setNotificationReadTimestamp(new Date());
                pushNotificationBean.setStatusMessage(null);
                pushNotificationBean.setStatusCode(PushNotificationStatusType.DELIVERED);
                em.merge(pushNotificationBean);
                */
                String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                CRMOutboundBean crmOutboundBean = QueryRepository.findCRMOutboundByPushNotificationAndFiscalCode(em, pushNotificationBean, fiscalCode);

                notificationResponse.setId(pushNotificationBean.getId());
                notificationResponse.setSource(pushNotificationBean.getSource());
                notificationResponse.setTitle(pushNotificationBean.getTitle());
                notificationResponse.setText(pushNotificationBean.getMessage());
                notificationResponse.setSendDate(pushNotificationBean.getSendingTimestamp());
                notificationResponse.setExpireDate(pushNotificationBean.getExpiringTimestamp());
                notificationResponse.setAppLink(null);
                notificationResponse.setShowDialog(true);

                if (crmOutboundBean != null) {

                    crmOutboundBean.setDeliveryId(CrmOutboundDeliveryStatusType.READ);
                    crmOutboundBean.setDeliveryMessage("Notifica letta con successo");
                    em.merge(crmOutboundBean);
                    // se msg contiene una url
                    String message = crmOutboundBean.getMsg();

                    if (message != null && (message.startsWith("http") || message.startsWith("https"))) {
                        notificationResponse.setAppLink(new AppLink());
                        notificationResponse.getAppLink().setType(PushNotificationContentType.INTERNAL);
                        notificationResponse.getAppLink().setLocation(message);
                        notificationResponse.setActionText("Apri");
                        notificationResponse.setText(crmOutboundBean.getMsg());
                        notificationResponse.setShowDialog(false);

                        ParameterNotification parameterNotificationC2 = new ParameterNotification("C_2", crmOutboundBean.getC2());
                        notificationResponse.getAppLink().getParameters().add(parameterNotificationC2);

                        ParameterNotification parameterNotificationC3 = new ParameterNotification("C_3", crmOutboundBean.getC3());
                        notificationResponse.getAppLink().getParameters().add(parameterNotificationC3);

                        ParameterNotification parameterNotificationC4 = new ParameterNotification("C_4", crmOutboundBean.getC4());
                        notificationResponse.getAppLink().getParameters().add(parameterNotificationC4);

                        ParameterNotification parameterNotificationC5 = new ParameterNotification("C_5", crmOutboundBean.getC5());
                        notificationResponse.getAppLink().getParameters().add(parameterNotificationC5);

                        ParameterNotification parameterNotificationC6 = new ParameterNotification("C_6", crmOutboundBean.getC6());
                        notificationResponse.getAppLink().getParameters().add(parameterNotificationC6);

                        ParameterNotification parameterNotificationC7 = new ParameterNotification("C_7", crmOutboundBean.getC7());
                        notificationResponse.getAppLink().getParameters().add(parameterNotificationC7);

                        ParameterNotification parameterNotificationC8 = new ParameterNotification("C_8", crmOutboundBean.getC8());
                        notificationResponse.getAppLink().getParameters().add(parameterNotificationC8);

                        ParameterNotification parameterNotificationC9 = new ParameterNotification("C_9", crmOutboundBean.getC9());
                        notificationResponse.getAppLink().getParameters().add(parameterNotificationC9);

                        ParameterNotification parameterNotificationC10 = new ParameterNotification("C_10", crmOutboundBean.getC10());
                        notificationResponse.getAppLink().getParameters().add(parameterNotificationC10);
                        
                        pushNotificationBean.setContentType(PushNotificationContentType.INTERNAL);
                        em.merge(pushNotificationBean);
                    }
                    else {
                        notificationResponse.setText(message);
                    }
                }
                else {
                    String message = pushNotificationBean.getMessage();
                    
                    if (message != null && (message.startsWith("http") || message.startsWith("https"))) {
                        notificationResponse.setAppLink(new AppLink());
                        notificationResponse.getAppLink().setType(PushNotificationContentType.INTERNAL);
                        notificationResponse.getAppLink().setLocation(message);
                        notificationResponse.setActionText("Apri");
                        notificationResponse.setText(null);
                        notificationResponse.setShowDialog(false);
                    }
                }

                notificationResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_SUCCESS);
            }
            
            if (pushNotificationBean.getSource().equals(PushNotificationSourceType.CRM_ES)) {

                System.out.println("Trovata push notification di tipo Crm ES (Vodafone Black Edition)");
                
                pushNotificationBean.setNotificationRead(true);
                pushNotificationBean.setNotificationReadTimestamp(new Date());
                pushNotificationBean.setStatusMessage(null);
                pushNotificationBean.setStatusCode(PushNotificationStatusType.DELIVERED);
                em.merge(pushNotificationBean);

                System.out.println("Message: " + pushNotificationBean.getMessage());
                
                notificationResponse.setId(pushNotificationBean.getId());
                notificationResponse.setSource(pushNotificationBean.getSource());
                notificationResponse.setTitle(pushNotificationBean.getTitle());
                notificationResponse.setText(pushNotificationBean.getMessage().replaceAll("(\\w|=|@|\\+|\\ |\\-)*text=", ""));
                notificationResponse.setSendDate(pushNotificationBean.getSendingTimestamp());
                notificationResponse.setExpireDate(pushNotificationBean.getExpiringTimestamp());
                notificationResponse.setAppLink(null);
                notificationResponse.setShowDialog(true);

                String message = pushNotificationBean.getMessage();
                
                if (message != null && (message.startsWith("http") || message.startsWith("https"))) {
                    notificationResponse.setAppLink(new AppLink());
                    notificationResponse.getAppLink().setType(PushNotificationContentType.INTERNAL);
                    notificationResponse.getAppLink().setLocation(message);
                    notificationResponse.setActionText("Apri");
                    notificationResponse.setText(null);
                    notificationResponse.setShowDialog(false);
                    
                    ParameterNotification parameterNotificationC2 = new ParameterNotification("C_2", userBean.getPersonalDataBean().getFirstName());
                    notificationResponse.getAppLink().getParameters().add(parameterNotificationC2);
                }

                notificationResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_RETRIEVE_NOTIFICATION_SUCCESS);
            }

            userTransaction.commit();
            return notificationResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED get active vouchers with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", notificationId.toString(), null, message);

            throw new EJBException(ex2);

        }

    }

    private NotificationResponse createMessageError(String requestId, UserTransaction userTransaction, String message, String statusCode) throws Exception {

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, message);

        userTransaction.commit();

        NotificationResponse response = new NotificationResponse();
        response.setStatusCode(statusCode);

        return response;
    }

}
