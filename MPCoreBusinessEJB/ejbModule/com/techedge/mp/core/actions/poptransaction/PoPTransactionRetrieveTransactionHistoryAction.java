package com.techedge.mp.core.actions.poptransaction;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ConstantsHelper;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ProductStatisticsInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatisticsObject;
import com.techedge.mp.core.business.interfaces.parking.ParkingTransaction;
import com.techedge.mp.core.business.interfaces.postpaid.PaymentHistory;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCartData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionInfo;
import com.techedge.mp.core.business.interfaces.postpaid.RetrieveTransactionHistoryResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.TransactionStatusHistoryBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.TransactionFinalStatusConverter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPTransactionRetrieveTransactionHistoryAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public PoPTransactionRetrieveTransactionHistoryAction() {}

    public RetrieveTransactionHistoryResponse execute(String requestID, String ticketID, Date startDate, Date endDate, int itemForPage, int pageNumber, 
            UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();
            RetrieveTransactionHistoryResponse retrieveTransactionHistoryResponse = new RetrieveTransactionHistoryResponse();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                retrieveTransactionHistoryResponse.setStatusCode(ResponseHelper.RETRIEVE_TRANSACTION_HISTORY_INVALID_TICKET);
                return retrieveTransactionHistoryResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                retrieveTransactionHistoryResponse.setStatusCode(ResponseHelper.USER_REQU_UNAUTHORIZED);
                return retrieveTransactionHistoryResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo
                // servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create transaction history in status " + userStatus);

                userTransaction.commit();

                retrieveTransactionHistoryResponse.setStatusCode(ResponseHelper.USER_REQU_UNAUTHORIZED);
                return retrieveTransactionHistoryResponse;
            }
            
            Boolean isNewAcquirerFlow = false;
            isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());

            // TODO
            int pages = 1;
            boolean errorFlag = true;
            PaymentHistory paymentHistory = null;
            ParkingTransaction parkingTransaction = null;
            PostPaidTransactionInfo postPaidTransactionInfo = null;
            List<ProductStatisticsInfo> statistics = new ArrayList<ProductStatisticsInfo>(0);
            List<PaymentHistory> paymentHistoryList = new ArrayList<PaymentHistory>(0);
            List<ParkingTransaction> parkingHistoryList = new ArrayList<ParkingTransaction>(0);

            // Estrazione transazioni PRE-PAID (table TRANSACTIONS);
            List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsByUserBeanAndDate(em, userBean, startDate, this.addDays(endDate, 1), itemForPage, false);

            if (!transactionBeanList.isEmpty()) {

                TransactionBean tmpTransactionBean = null;

                boolean flagFirst = true;

                for (TransactionBean transactionBean : transactionBeanList) {

                    // Si estrae la prima
                    tmpTransactionBean = transactionBean;

                    if (flagFirst == true && tmpTransactionBean == null) {

                        flagFirst = false;

                    }
                    else {

                        errorFlag = false;

                        paymentHistory = new PaymentHistory();
                        postPaidTransactionInfo = new PostPaidTransactionInfo();

                        // Si estrae lo stato pi� recente associato alla transazione

                        Integer lastSequenceID = 0;
                        TransactionStatusBean lastTransactionStatusBean = null;
                        Set<TransactionStatusBean> transactionStatusData = tmpTransactionBean.getTransactionStatusBeanData();
                        for (TransactionStatusBean transactionStatusBean : transactionStatusData) {
                            Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
                            if (lastSequenceID < transactionStatusBeanSequenceID) {
                                lastSequenceID = transactionStatusBeanSequenceID;
                                lastTransactionStatusBean = transactionStatusBean;
                            }
                        }

                        paymentHistory.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_FUEL);
                        paymentHistory.setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_PRE_PAY);

                        String statusTitle = null;
                        String statusDescription = null;

                        if (tmpTransactionBean.getFinalStatusType() != null) {
                            //TransactionStatusHistoryBean transactionStatusHistoryBean = transactionHistoryBean.getLastTransactionStatusHistory();
                            String status = tmpTransactionBean.getFinalStatusType();
                            String subStatus = lastTransactionStatusBean.getStatus();                        
                            statusTitle = TransactionFinalStatusConverter.getReceiptTitleText(status, subStatus);
                            statusDescription = TransactionFinalStatusConverter.getReceiptDescriptionText(status, subStatus);
                            
                            /*if (transactionHistoryBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL)) {

                                if (subStatus.equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED) || subStatus.equals(StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED)) {
                                    subStatus = StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED;
                                    statusTitle = TransactionFinalStatusConverter.getReceiptTitleText(status, subStatus, null);
                                    statusDescription = TransactionFinalStatusConverter.getReceiptDescriptionText(status, subStatus, null);
                                }
                            }
                            
                            if (transactionHistoryBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_FAILED)) {
                                statusTitle = TransactionFinalStatusConverter.getReceiptTitleText(status, subStatus, null);
                                statusDescription = TransactionFinalStatusConverter.getReceiptDescriptionText(status, subStatus, null);
                            }*/
                        }
                        
                        postPaidTransactionInfo.setStatusTitle(statusTitle);
                        postPaidTransactionInfo.setStatusDescription(statusDescription);
                        postPaidTransactionInfo.setMpTransactionID(tmpTransactionBean.getTransactionID());

                        if (lastTransactionStatusBean != null) {
                            postPaidTransactionInfo.setStatus(lastTransactionStatusBean.getStatus());
                            postPaidTransactionInfo.setSubStatus(lastTransactionStatusBean.getSubStatus());
                        }

                        postPaidTransactionInfo.setAmount(tmpTransactionBean.getFinalAmount());
                        postPaidTransactionInfo.setCreationTimestamp(tmpTransactionBean.getCreationTimestamp());
                        postPaidTransactionInfo.setStation(tmpTransactionBean.getStationBean().toStation());
                        postPaidTransactionInfo.setCart(null);
                        postPaidTransactionInfo.setRefuel(null);
                        paymentHistory.setTransaction(postPaidTransactionInfo);

                        paymentHistoryList.add(paymentHistory);

                    }

                }
            }

            // Estrazione transazioni storicizzate PRE-PAID (table
            // TRANSACTIONS_HISTORY);
            List<TransactionHistoryBean> transactionHistoryBeanList = QueryRepository.findTransactionsHistoryByUserBeanAndDate(em, userBean, startDate, this.addDays(endDate, 1), itemForPage, false);

            if (!transactionHistoryBeanList.isEmpty()) {

                TransactionHistoryBean tmpTransactionHistoryBean = null;

                boolean flagFirst = true;

                for (TransactionHistoryBean transactionHistoryBean : transactionHistoryBeanList) {

                    // Si estrae la prima
                    tmpTransactionHistoryBean = transactionHistoryBean;

                    if (flagFirst == true && tmpTransactionHistoryBean == null) {

                        flagFirst = false;

                    }
                    else {

                        errorFlag = false;

                        paymentHistory = new PaymentHistory();
                        postPaidTransactionInfo = new PostPaidTransactionInfo();

                        // Si estrae lo stato pi� recente associato alla transazione

                        Integer lastSequenceID = 0;
                        TransactionStatusHistoryBean lastTransactionStatusHistoryBean = null;
                        Set<TransactionStatusHistoryBean> transactionStatusData = tmpTransactionHistoryBean.getTransactionStatusHistoryBeanData();
                        for (TransactionStatusHistoryBean transactionStatusHistoryBean : transactionStatusData) {
                            Integer transactionStatusBeanSequenceID = transactionStatusHistoryBean.getSequenceID();
                            if (lastSequenceID < transactionStatusBeanSequenceID) {
                                lastSequenceID = transactionStatusBeanSequenceID;
                                lastTransactionStatusHistoryBean = transactionStatusHistoryBean;
                            }
                        }

                        paymentHistory.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_FUEL);
                        paymentHistory.setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_PRE_PAY);

                        String statusTitle = null;
                        String statusDescription = null;

                        if (transactionHistoryBean.getFinalStatusType() != null) {
                            //TransactionStatusHistoryBean transactionStatusHistoryBean = transactionHistoryBean.getLastTransactionStatusHistory();
                            String status = transactionHistoryBean.getFinalStatusType();
                            String subStatus = lastTransactionStatusHistoryBean.getStatus();                        
                            statusTitle = TransactionFinalStatusConverter.getReceiptTitleText(status, subStatus);
                            statusDescription = TransactionFinalStatusConverter.getReceiptDescriptionText(status, subStatus);
                            
                            /*if (transactionHistoryBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL)) {

                                if (subStatus.equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED) || subStatus.equals(StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED)) {
                                    subStatus = StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED;
                                    statusTitle = TransactionFinalStatusConverter.getReceiptTitleText(status, subStatus, null);
                                    statusDescription = TransactionFinalStatusConverter.getReceiptDescriptionText(status, subStatus, null);
                                }
                            }
                            
                            if (transactionHistoryBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_FAILED)) {
                                statusTitle = TransactionFinalStatusConverter.getReceiptTitleText(status, subStatus, null);
                                statusDescription = TransactionFinalStatusConverter.getReceiptDescriptionText(status, subStatus, null);
                            }*/
                        }
                        
                        postPaidTransactionInfo.setStatusTitle(statusTitle);
                        postPaidTransactionInfo.setStatusDescription(statusDescription);

                        postPaidTransactionInfo.setMpTransactionID(tmpTransactionHistoryBean.getTransactionID());

                        if (lastTransactionStatusHistoryBean != null) {
                            postPaidTransactionInfo.setStatus(lastTransactionStatusHistoryBean.getStatus());
                            postPaidTransactionInfo.setSubStatus(lastTransactionStatusHistoryBean.getSubStatus());
                        }

                        postPaidTransactionInfo.setStatus(lastTransactionStatusHistoryBean.getStatus());
                        postPaidTransactionInfo.setSubStatus(lastTransactionStatusHistoryBean.getSubStatus());
                        postPaidTransactionInfo.setAmount(tmpTransactionHistoryBean.getFinalAmount());
                        postPaidTransactionInfo.setCreationTimestamp(tmpTransactionHistoryBean.getCreationTimestamp());
                        postPaidTransactionInfo.setStation(tmpTransactionHistoryBean.getStationBean().toStation());
                        postPaidTransactionInfo.setCart(null);
                        postPaidTransactionInfo.setRefuel(null);
                        paymentHistory.setTransaction(postPaidTransactionInfo);

                        paymentHistoryList.add(paymentHistory);

                    }

                }
            }

            // Estrazione transazioni post-paid (table POSTPAIDTRANSACTIONS);
            List<PostPaidTransactionBean> poPTransactionBeanList = QueryRepository.findPostPaidTransactionsByUserBeanAndDate(em, userBean, startDate, this.addDays(endDate, 1), itemForPage, false);

            if (!poPTransactionBeanList.isEmpty()) {

                PostPaidTransactionBean tmpPostPaidTransactionBean = null;

                boolean flagFirst = true;

                for (PostPaidTransactionBean postPaidTransactionBean : poPTransactionBeanList) {

                    // Si estrae la prima
                    tmpPostPaidTransactionBean = postPaidTransactionBean;

                    if (flagFirst == true && tmpPostPaidTransactionBean == null) {
                        flagFirst = false;
                    }
                    else {
                        errorFlag = false;

                        // Si estrae lo stato pi� recente associato alla transazione
                        paymentHistory = new PaymentHistory();
                        postPaidTransactionInfo = new PostPaidTransactionInfo();

                        paymentHistory.setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_POST_PAY);

                        if (tmpPostPaidTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {
                            paymentHistory.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_FUEL);
                        }
                        else {
                            paymentHistory.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_SHOP);
                        }

                        String statusTitle = null;
                        String statusDescription = null;

                        if (postPaidTransactionBean.getMpTransactionStatus() != null) {
                            PostPaidTransactionEventBean poPTransactionEventBean = postPaidTransactionBean.getLastPostPaidTransactionEventBean();
                            String status = postPaidTransactionBean.getMpTransactionStatus();
                            String event = null;
                            String eventCode = null;
                            String eventResult = null;
                            
                            if (poPTransactionEventBean != null) {
                                event = poPTransactionEventBean.getEvent();
                                eventCode = poPTransactionEventBean.getNewState();
                                eventResult = poPTransactionEventBean.getResult();
                            }
                            
                            statusTitle = TransactionFinalStatusConverter.getReceiptTitleText(status, event, eventCode, eventResult);
                            statusDescription = TransactionFinalStatusConverter.getReceiptDescriptionText(status, event, eventCode, eventResult);
                        }
                        
                        postPaidTransactionInfo.setStatusTitle(statusTitle);
                        postPaidTransactionInfo.setStatusDescription(statusDescription);

                        postPaidTransactionInfo.setMpTransactionID(tmpPostPaidTransactionBean.getMpTransactionID());
                        postPaidTransactionInfo.setStatus(tmpPostPaidTransactionBean.getMpTransactionStatus());
                        postPaidTransactionInfo.setSubStatus(null);
                        postPaidTransactionInfo.setAmount(tmpPostPaidTransactionBean.getAmount());
                        postPaidTransactionInfo.setCreationTimestamp(tmpPostPaidTransactionBean.getCreationTimestamp());
                        postPaidTransactionInfo.setStation(tmpPostPaidTransactionBean.getStationBean().toStation());

                        if (tmpPostPaidTransactionBean.getCartBean() != null) {
                            for (PostPaidCartBean postPaidCartBean : tmpPostPaidTransactionBean.getCartBean()) {

                                PostPaidCartData postPaidCartData = new PostPaidCartData();
                                postPaidCartData.setAmount(postPaidCartBean.getAmount());
                                postPaidCartData.setProductDescription(postPaidCartBean.getProductDescription());
                                postPaidCartData.setProductId(postPaidCartBean.getProductId());
                                postPaidCartData.setQuantity(postPaidCartBean.getQuantity());
                                postPaidTransactionInfo.getCart().add(postPaidCartData);
                            }
                        }

                        // PostPaidRefuelData postPaidRefuelData = new PostPaidRefuelData();
                        if (tmpPostPaidTransactionBean.getRefuelBean() != null) {
                            for (PostPaidRefuelBean postPaidRefuelBean : tmpPostPaidTransactionBean.getRefuelBean()) {

                                PostPaidRefuelData postPaidRefuelData = new PostPaidRefuelData();
                                postPaidRefuelData.setFuelAmount(postPaidRefuelBean.getFuelAmount());
                                postPaidRefuelData.setFuelQuantity(postPaidRefuelBean.getFuelQuantity());
                                postPaidRefuelData.setFuelType(postPaidRefuelBean.getFuelType());
                                postPaidRefuelData.setProductDescription(postPaidRefuelBean.getProductDescription());
                                postPaidRefuelData.setProductId(postPaidRefuelBean.getProductId());
                                postPaidRefuelData.setPumpId(postPaidRefuelBean.getPumpId());
                                postPaidTransactionInfo.getRefuel().add(postPaidRefuelData);
                            }
                        }

                        paymentHistory.setTransaction(postPaidTransactionInfo);

                        paymentHistoryList.add(paymentHistory);
                    }
                }
            }

            // Estrazione transazioni storicizzate post-paid (table
            // POSTPAIDTRANSACTIONS_HISTORY);
            List<PostPaidTransactionHistoryBean> poPTransactionHistoryBeanList = QueryRepository.findPostPaidTransactionsHistoryByUserBeanAndDate(em, userBean, startDate, this.addDays(endDate, 1), itemForPage, false);

            if (!poPTransactionHistoryBeanList.isEmpty()) {

                PostPaidTransactionHistoryBean tmpPostPaidTransactionHistoryBean = null;

                boolean flagFirst = true;

                for (PostPaidTransactionHistoryBean postPaidTransactionHistoryBean : poPTransactionHistoryBeanList) {

                    // Si estrae la prima
                    tmpPostPaidTransactionHistoryBean = postPaidTransactionHistoryBean;

                    if (flagFirst == true && tmpPostPaidTransactionHistoryBean == null) {
                        flagFirst = false;
                    }
                    else {
                        errorFlag = false;

                        // Si estrae lo stato pi� recente associato alla transazione
                        paymentHistory = new PaymentHistory();
                        postPaidTransactionInfo = new PostPaidTransactionInfo();

                        paymentHistory.setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_POST_PAY);

                        if (tmpPostPaidTransactionHistoryBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {
                            paymentHistory.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_FUEL);
                        }
                        else {
                            paymentHistory.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_SHOP);
                        }

                        postPaidTransactionInfo.setMpTransactionID(tmpPostPaidTransactionHistoryBean.getMpTransactionID());
                        postPaidTransactionInfo.setStatus(tmpPostPaidTransactionHistoryBean.getMpTransactionStatus());
                        postPaidTransactionInfo.setSubStatus(null);
                        postPaidTransactionInfo.setAmount(tmpPostPaidTransactionHistoryBean.getAmount());
                        postPaidTransactionInfo.setCreationTimestamp(tmpPostPaidTransactionHistoryBean.getCreationTimestamp());
                        postPaidTransactionInfo.setStation(tmpPostPaidTransactionHistoryBean.getStationBean().toStation());

                        if (tmpPostPaidTransactionHistoryBean.getCartHistoryBean() != null) {
                            for (PostPaidCartHistoryBean postPaidCartHistoryBean : tmpPostPaidTransactionHistoryBean.getCartHistoryBean()) {

                                PostPaidCartData postPaidCartData = new PostPaidCartData();
                                postPaidCartData.setAmount(postPaidCartHistoryBean.getAmount());
                                postPaidCartData.setProductDescription(postPaidCartHistoryBean.getProductDescription());
                                postPaidCartData.setProductId(postPaidCartHistoryBean.getProductId());
                                postPaidCartData.setQuantity(postPaidCartHistoryBean.getQuantity());
                                postPaidTransactionInfo.getCart().add(postPaidCartData);
                            }
                        }

                        // PostPaidRefuelData postPaidRefuelData = new PostPaidRefuelData();
                        if (tmpPostPaidTransactionHistoryBean.getRefuelHistoryBean() != null) {
                            for (PostPaidRefuelHistoryBean postPaidRefuelHistoryBean : tmpPostPaidTransactionHistoryBean.getRefuelHistoryBean()) {

                                PostPaidRefuelData postPaidRefuelData = new PostPaidRefuelData();
                                postPaidRefuelData.setFuelAmount(postPaidRefuelHistoryBean.getFuelAmount());
                                postPaidRefuelData.setFuelQuantity(postPaidRefuelHistoryBean.getFuelQuantity());
                                postPaidRefuelData.setFuelType(postPaidRefuelHistoryBean.getFuelType());
                                postPaidRefuelData.setProductDescription(postPaidRefuelHistoryBean.getProductDescription());
                                postPaidRefuelData.setProductId(postPaidRefuelHistoryBean.getProductId());
                                postPaidRefuelData.setPumpId(postPaidRefuelHistoryBean.getPumpId());
                                postPaidTransactionInfo.getRefuel().add(postPaidRefuelData);
                            }
                        }

                        paymentHistory.setTransaction(postPaidTransactionInfo);

                        paymentHistoryList.add(paymentHistory);
                    }
                }
            }
            
            List<VoucherTransactionBean> voucherTransactionBeanList = new ArrayList<VoucherTransactionBean>();
            
            if (!isNewAcquirerFlow) {
                // Estrazione transazioni VOUCHER (table VOUCHER_TRANSACTIONS);
                voucherTransactionBeanList = QueryRepository.findVoucherTransactionsByUserBeanAndDate(em, userBean, startDate, this.addDays(endDate, 1), itemForPage, false);
            }

            if (!voucherTransactionBeanList.isEmpty()) {

                VoucherTransactionBean tmpVoucherTransactionBean = null;

                boolean flagFirst = true;

                for (VoucherTransactionBean voucherTransactionBean : voucherTransactionBeanList) {

                    // Si estrae la prima
                    tmpVoucherTransactionBean = voucherTransactionBean;

                    if (flagFirst == true && tmpVoucherTransactionBean == null) {

                        flagFirst = false;

                    }
                    else {

                        errorFlag = false;

                        paymentHistory = new PaymentHistory();
                        postPaidTransactionInfo = new PostPaidTransactionInfo();

                        // Si estrae lo stato pi� recente associato alla transazione

                        Integer lastSequenceID = 0;
                        VoucherTransactionStatusBean lastVoucherTransactionStatusBean = null;
                        List<VoucherTransactionStatusBean> voucherTransactionStatusData = tmpVoucherTransactionBean.getTransactionStatusBeanList();
                        for (VoucherTransactionStatusBean voucherTransactionStatusBean : voucherTransactionStatusData) {
                            Integer voucherTransactionStatusBeanSequenceID = voucherTransactionStatusBean.getSequenceID();
                            if (lastSequenceID < voucherTransactionStatusBeanSequenceID) {
                                lastSequenceID = voucherTransactionStatusBeanSequenceID;
                                lastVoucherTransactionStatusBean = voucherTransactionStatusBean;
                            }
                        }

                        paymentHistory.setSourceType(ConstantsHelper.TRANSACTION_SOURCE_TYPE_VOUCHER);
                        paymentHistory.setSourceStatus(ConstantsHelper.TRANSACTION_SOURCE_STATUS_VOUCHER);

                        String statusTitle = null;
                        String statusDescription = null;

                        if (tmpVoucherTransactionBean.getFinalStatusType() != null) {
                            
                            String status = tmpVoucherTransactionBean.getFinalStatusType();
                            
                            String subStatus = "";
                            if ( lastVoucherTransactionStatusBean != null ) {
                                subStatus = lastVoucherTransactionStatusBean.getStatus();
                            }
                            
                            statusTitle = TransactionFinalStatusConverter.getReceiptTitleText(status, subStatus);
                            statusDescription = TransactionFinalStatusConverter.getReceiptDescriptionText(status, subStatus);
                        }
                        
                        postPaidTransactionInfo.setStatusTitle(statusTitle);
                        postPaidTransactionInfo.setStatusDescription(statusDescription);
                        postPaidTransactionInfo.setMpTransactionID(tmpVoucherTransactionBean.getTransactionID());

                        if (lastVoucherTransactionStatusBean != null) {
                            postPaidTransactionInfo.setStatus(lastVoucherTransactionStatusBean.getStatus());
                            postPaidTransactionInfo.setSubStatus(lastVoucherTransactionStatusBean.getSubStatus());
                        }

                        postPaidTransactionInfo.setAmount(tmpVoucherTransactionBean.getAmount());
                        postPaidTransactionInfo.setCreationTimestamp(tmpVoucherTransactionBean.getCreationTimestamp());
                        postPaidTransactionInfo.setStation(null);
                        postPaidTransactionInfo.setCart(null);
                        postPaidTransactionInfo.setRefuel(null);
                        paymentHistory.setTransaction(postPaidTransactionInfo);

                        paymentHistoryList.add(paymentHistory);

                    }

                }
            }
            
            // Estrazione transazioni di sosta (table PARKING_TRANSACTIONS);
            List<ParkingTransactionBean> parkingTransactionBeanList = QueryRepository.findParkingTransactionsByUserBeanAndDate(em, userBean, startDate, this.addDays(endDate, 1), itemForPage, false);

            if (!parkingTransactionBeanList.isEmpty()) {

                ParkingTransactionBean tmpParkingTransactionBean = null;

                boolean flagFirst = true;

                for (ParkingTransactionBean parkingTransactionBean : parkingTransactionBeanList) {

                    System.out.println("ParkingTransactionId: " + parkingTransactionBean.getParkingTransactionId());
                    
                    // Si estrae la prima
                    tmpParkingTransactionBean = parkingTransactionBean;

                    if (flagFirst == true && tmpParkingTransactionBean == null) {

                        flagFirst = false;

                    }
                    else {

                        errorFlag = false;

                        parkingTransaction = tmpParkingTransactionBean.toParkingTransaction();

                        parkingHistoryList.add(parkingTransaction);

                    }

                }
            }
            

            if (errorFlag == true) {
                retrieveTransactionHistoryResponse.setPaymentHistory(paymentHistoryList);
                retrieveTransactionHistoryResponse.setParkingHistory(parkingHistoryList);
                retrieveTransactionHistoryResponse.setPagesTotal(pages);
                retrieveTransactionHistoryResponse.setPageOffset(pageNumber);
                retrieveTransactionHistoryResponse.setStatusCode(ResponseHelper.RETRIEVE_TRANSACTION_HISTORY_SUCCESS);

                userTransaction.commit();

                return retrieveTransactionHistoryResponse;
            }
            else {
                
                // Ordinamento transazioni di sosta
                Collections.sort(parkingHistoryList, new Comparator<ParkingTransaction>() {
                    public int compare(ParkingTransaction synchronizedListOne, ParkingTransaction synchronizedListTwo) {
                        // use instanceof to verify the references are indeed of the type in
                        // question
                        return synchronizedListOne.getTimestamp().compareTo(synchronizedListTwo.getTimestamp());
                    }
                });
                Collections.reverse(parkingHistoryList);
                retrieveTransactionHistoryResponse.setParkingHistory(parkingHistoryList);
                
                // Ordinamento transazioni di rifornimento
                Collections.sort(paymentHistoryList, new Comparator<PaymentHistory>() {
                    public int compare(PaymentHistory synchronizedListOne, PaymentHistory synchronizedListTwo) {
                        // use instanceof to verify the references are indeed of the type in
                        // question
                        return synchronizedListOne.getTransaction().getCreationTimestamp().compareTo(synchronizedListTwo.getTransaction().getCreationTimestamp());
                    }
                });
                Collections.reverse(paymentHistoryList);
                int sizeList = paymentHistoryList.size();

                if (itemForPage == -1) {
                    itemForPage = sizeList;
                }

                if (sizeList > itemForPage) {

                    pages = sizeList / itemForPage;

                    if (sizeList % itemForPage > 0) {
                        pages = pages + 1;
                    }
                }

                int fromSubset;
                int toSubset;

                if (pageNumber >= 0 && itemForPage > 0) {

                    fromSubset = (pageNumber) * itemForPage;
                    toSubset = fromSubset + itemForPage;

                    if (toSubset < sizeList) {

                        retrieveTransactionHistoryResponse.setPaymentHistory(new ArrayList<PaymentHistory>(paymentHistoryList.subList(fromSubset, toSubset)));
                        retrieveTransactionHistoryResponse.setStatusCode(ResponseHelper.RETRIEVE_TRANSACTION_HISTORY_SUCCESS);

                    }
                    else {
                        if (fromSubset < sizeList) {

                            retrieveTransactionHistoryResponse.setPaymentHistory(new ArrayList<PaymentHistory>(paymentHistoryList.subList(fromSubset, sizeList)));
                            retrieveTransactionHistoryResponse.setStatusCode(ResponseHelper.RETRIEVE_TRANSACTION_HISTORY_SUCCESS);
                        }
                        else {

                            retrieveTransactionHistoryResponse.setPaymentHistory(paymentHistoryList);
                            retrieveTransactionHistoryResponse.setStatusCode(ResponseHelper.RETRIEVE_TRANSACTION_HISTORY_SUCCESS);
                            pageNumber = 0;
                            pages = 1;
                        }
                    }
                }
                else {
                    pages = 0;
                    pageNumber = sizeList;
                    retrieveTransactionHistoryResponse.setPaymentHistory(paymentHistoryList);
                    retrieveTransactionHistoryResponse.setStatusCode(ResponseHelper.RETRIEVE_TRANSACTION_HISTORY_SUCCESS);
                }

                retrieveTransactionHistoryResponse.setPagesTotal(pages);
                retrieveTransactionHistoryResponse.setPageOffset(pageNumber);
                
                
                // Estrazione delle statistiche mensili
                Date currentMonthStartDate  = startDate;
                Date currentMonthEndDate    = endDate;
                
                Calendar aCalendar = Calendar.getInstance();
                aCalendar.setTime(currentMonthEndDate);
                aCalendar.set(Calendar.HOUR_OF_DAY,23);
                aCalendar.set(Calendar.MINUTE,59);
                aCalendar.set(Calendar.SECOND,59);
                aCalendar.set(Calendar.MILLISECOND,999);
                currentMonthEndDate = aCalendar.getTime();
                
                aCalendar.setTime(startDate);
                aCalendar.set(Calendar.DATE, 1);
                aCalendar.add(Calendar.DAY_OF_MONTH, -1);
                aCalendar.set(Calendar.HOUR_OF_DAY,23);
                aCalendar.set(Calendar.MINUTE,59);
                aCalendar.set(Calendar.SECOND,59);
                aCalendar.set(Calendar.MILLISECOND,999);
                Date previousMonthEndDate = aCalendar.getTime();
                aCalendar.set(Calendar.DATE, 1);
                aCalendar.set(Calendar.HOUR_OF_DAY,0);
                aCalendar.set(Calendar.MINUTE,0);
                aCalendar.set(Calendar.SECOND,0);
                aCalendar.set(Calendar.MILLISECOND,0);
                Date previousMonthStartDate = aCalendar.getTime();
                
                System.out.println("Elaborazione statistiche mensili");
                System.out.println("Current month");
                System.out.println("Start date: " + currentMonthStartDate);
                System.out.println("End date:   " + currentMonthEndDate);
                System.out.println("");
                System.out.println("Previous month");
                System.out.println("Start date: " + previousMonthStartDate);
                System.out.println("End date:   " + previousMonthEndDate);
                
                List<StatisticsObject> statisticsPrepaidCurrentMonth          = QueryRepository.getStatisticsPrepaidByUser(em, userBean, currentMonthStartDate, currentMonthEndDate);
                List<StatisticsObject> statisticsPrepaidPreviousMonth         = QueryRepository.getStatisticsPrepaidByUser(em, userBean, previousMonthStartDate, previousMonthEndDate);
                List<StatisticsObject> statisticsPrepaidHistoryCurrentMonth   = QueryRepository.getStatisticsPrepaidHistoryByUser(em, userBean, currentMonthStartDate, currentMonthEndDate);
                List<StatisticsObject> statisticsPrepaidHistoryPreviousMonth  = QueryRepository.getStatisticsPrepaidHistoryByUser(em, userBean, previousMonthStartDate, previousMonthEndDate);
                List<StatisticsObject> statisticsPostpaidCurrentMonth         = QueryRepository.getStatisticsPostpaidByUser(em, userBean, currentMonthStartDate, currentMonthEndDate);
                List<StatisticsObject> statisticsPostpaidPreviousMonth        = QueryRepository.getStatisticsPostpaidByUser(em, userBean, previousMonthStartDate, previousMonthEndDate);
                List<StatisticsObject> statisticsPostpaidHistoryCurrentMonth  = QueryRepository.getStatisticsPostpaidHistoryByUser(em, userBean, currentMonthStartDate, currentMonthEndDate);
                List<StatisticsObject> statisticsPostpaidHistoryPreviousMonth = QueryRepository.getStatisticsPostpaidHistoryByUser(em, userBean, previousMonthStartDate, previousMonthEndDate);
                
                List<StatisticsObject> statisticsCurrentMonth = statisticsPrepaidCurrentMonth;
                statisticsCurrentMonth.addAll(statisticsPrepaidHistoryCurrentMonth);
                statisticsCurrentMonth.addAll(statisticsPostpaidCurrentMonth);
                statisticsCurrentMonth.addAll(statisticsPostpaidHistoryCurrentMonth);
                
                List<StatisticsObject> statisticsPreviousMonth = statisticsPrepaidPreviousMonth;
                statisticsPreviousMonth.addAll(statisticsPrepaidHistoryPreviousMonth);
                statisticsPreviousMonth.addAll(statisticsPostpaidPreviousMonth);
                statisticsPreviousMonth.addAll(statisticsPostpaidHistoryPreviousMonth);
                
                HashMap<String, ProductStatisticsInfo> hashProduct = new HashMap<String, ProductStatisticsInfo>();
                
                for(StatisticsObject statisticsObject : statisticsCurrentMonth) {
                    
                    // Conversione descrizioni prodotti
                    statisticsObject.productDescription = convertDescription(statisticsObject.productDescription);
                    
                    if (hashProduct.containsKey(statisticsObject.productDescription)) {
                        
                        ProductStatisticsInfo productStatisticsInfo = hashProduct.get(statisticsObject.productDescription);
                        Double quantity = productStatisticsInfo.getQuantity();
                        Double amount = productStatisticsInfo.getAmount();
                        quantity = quantity + statisticsObject.quantity;
                        amount = amount + statisticsObject.amount;
                        productStatisticsInfo.setQuantity(quantity);
                        productStatisticsInfo.setAmount(amount);
                        hashProduct.put(statisticsObject.productDescription, productStatisticsInfo);
                    }
                    else {
                        
                        ProductStatisticsInfo productStatisticsInfo = new ProductStatisticsInfo();
                        productStatisticsInfo.setProductDescription(statisticsObject.productDescription);
                        productStatisticsInfo.setQuantity(statisticsObject.quantity);
                        productStatisticsInfo.setAmount(statisticsObject.amount);
                        productStatisticsInfo.setPreviousQuantity(0.0);
                        productStatisticsInfo.setPreviousAmount(0.0);
                        hashProduct.put(statisticsObject.productDescription, productStatisticsInfo);
                    }
                }
                
                for(StatisticsObject statisticsObject : statisticsPreviousMonth) {
                    
                    // Conversione descrizioni prodotti
                    statisticsObject.productDescription = convertDescription(statisticsObject.productDescription);
                    
                    if (hashProduct.containsKey(statisticsObject.productDescription)) {
                        
                        ProductStatisticsInfo productStatisticsInfo = hashProduct.get(statisticsObject.productDescription);
                        Double previousQuantity = productStatisticsInfo.getPreviousQuantity();
                        Double previousAmount = productStatisticsInfo.getPreviousAmount();
                        previousQuantity = previousQuantity + statisticsObject.quantity;
                        previousAmount = previousAmount + statisticsObject.amount;
                        productStatisticsInfo.setPreviousQuantity(previousQuantity);
                        productStatisticsInfo.setPreviousAmount(previousAmount);
                        hashProduct.put(statisticsObject.productDescription, productStatisticsInfo);
                    }
                    else {
                        
                        ProductStatisticsInfo productStatisticsInfo = new ProductStatisticsInfo();
                        productStatisticsInfo.setProductDescription(statisticsObject.productDescription);
                        productStatisticsInfo.setQuantity(0.0);
                        productStatisticsInfo.setAmount(0.0);
                        productStatisticsInfo.setPreviousQuantity(statisticsObject.quantity);
                        productStatisticsInfo.setPreviousAmount(statisticsObject.amount);
                        hashProduct.put(statisticsObject.productDescription, productStatisticsInfo);
                    }
                }
                
                for(ProductStatisticsInfo productStatisticsInfo : hashProduct.values()) {
                    
                    statistics.add(productStatisticsInfo);
                }
                
                /*
                ProductStatisticsInfo productStatisticsInfoSuper = new ProductStatisticsInfo();
                productStatisticsInfoSuper.setProductDescription("Super");
                productStatisticsInfoSuper.setQuantity(20.0);
                productStatisticsInfoSuper.setPreviousQuantity(15.0);
                statistics.add(productStatisticsInfoSuper);
                
                ProductStatisticsInfo productStatisticsInfoDiesel = new ProductStatisticsInfo();
                productStatisticsInfoDiesel.setProductDescription("Diesel");
                productStatisticsInfoDiesel.setQuantity(20.0);
                productStatisticsInfoDiesel.setPreviousQuantity(15.0);
                statistics.add(productStatisticsInfoDiesel);
                */
                
                retrieveTransactionHistoryResponse.setStatistics(statistics);
                
                userTransaction.commit();

                return retrieveTransactionHistoryResponse;
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieving transaction history with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

    private Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); // minus number would decrement the days
        return cal.getTime();
    }

    private String convertDescription(String productDescription) {
        
        if( productDescription.equals("SenzaPb") ) {
            return "Benzina";
        }
        
        if( productDescription.equals("BluSuper") ) {
            return "Benzina";
        }
        
        if( productDescription.equals("Gasolio") ) {
            return "Diesel";
        }
        
        if( productDescription.equals("BluDiesel") ) {
            return "Diesel";
        }
        
        return productDescription;
    }
}
