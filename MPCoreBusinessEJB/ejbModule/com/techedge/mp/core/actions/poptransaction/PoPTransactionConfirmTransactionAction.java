package com.techedge.mp.core.actions.poptransaction;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPTransactionConfirmTransactionAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public PoPTransactionConfirmTransactionAction() {
    }


    public String execute(
    		String ticketID,
			String requestID,
			String transactionID) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);
		    
    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.PP_TRANSACTION_CONFIRM_INVALID_TICKET;
    		}
    		
    		UserBean userBean = ticketBean.getUser();
    		if ( userBean == null ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.PP_TRANSACTION_CONFIRM_INVALID_TICKET;
    		}
    		
    		
    		// Verifica lo stato dell'utente
    		Integer userStatus = userBean.getUserStatus();
    		if ( userStatus != User.USER_STATUS_VERIFIED ) {
    			
    			// Un utente che si trova in questo stato non pu� invocare questo servizio
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to confirm transaction: user in status " + userStatus );
				
    			userTransaction.commit();
    			
                return ResponseHelper.PP_TRANSACTION_CONFIRM_UNAUTHORIZED;
    		}
    		
    		
    		// Ricerca la transazione attiva (Prepaid)
    		List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);
		    // Ricerca la transazione attiva (PostPaid)
    		 PostPaidTransactionBean postPaidTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, transactionID);
    		
		    if ( transactionBeanList.isEmpty() && postPaidTransactionBean == null) {
    			
    			// Non esiste una transazione attiva
		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction not found" );
				
    			userTransaction.commit();
    			
    			return ResponseHelper.PP_TRANSACTION_CONFIRM_FAILURE;
    		}
		    else if ( !transactionBeanList.isEmpty()) {

				TransactionBean pendingTransactionBean = null;

				for( TransactionBean transactionBean : transactionBeanList ) {

					// Si estrae la prima
					pendingTransactionBean = transactionBean;
				}

				if ( pendingTransactionBean == null ) {
					
					// Non esiste una transazione attiva
			    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong id in confirm transaction" );
					
	    			userTransaction.commit();
	    			
	    			return ResponseHelper.PP_TRANSACTION_CONFIRM_WRONG_ID;
				}
				else {

					// Si verifica il refuelID
					if ( !pendingTransactionBean.getTransactionID().equals(transactionID) ) {

						// Non esiste una transazione attiva
				    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong id in confirm transaction" );
						
		    			userTransaction.commit();
		    			
		    			return ResponseHelper.PP_TRANSACTION_CONFIRM_WRONG_ID;
					}
					else {

						// Si verificano gli stati della transazione

						Boolean statusFound = false;
						String lastStatus = "";

						for ( TransactionStatusBean transactionStatusBean : pendingTransactionBean.getTransactionStatusBeanData() ) {

							if ( transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_NOT_AUTHORIZED) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_AUTHORIZATION_NOT_DELETED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_TRANSACTION_STATUS_REQUEST_KO ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_CLOSED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_NOT_CLOSED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_NOT_DELETED ) ||
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PUMP_NOT_ENABLED) || 
									transactionStatusBean.getStatus().equals(StatusHelper.STATUS_TRANSACTION_ABEND)
									) {

								statusFound = true;
								lastStatus = transactionStatusBean.getStatus();
							}
						}

						if ( statusFound == false ) {

							System.out.println( "last status: " + lastStatus );

							// Non esiste una transazione attiva
					    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to confirm transaction in status " + lastStatus );
							
			    			userTransaction.commit();
			    			
			    			return ResponseHelper.PP_TRANSACTION_CONFIRM_FAILURE;
						}


						// Imposta il flag confirmed a true
						Long id = pendingTransactionBean.getId();
						
						int count = QueryRepository.confirmTransaction(em, id);
						
						System.out.println("confirmTransaction result: " + count);
						
						em.refresh(pendingTransactionBean);
						
						System.out.println("confirmTransaction confirm new value: " + pendingTransactionBean.getConfirmed());
						
						//pendingTransactionBean.setConfirmed(true);

						//em.merge(pendingTransactionBean);
						
						userTransaction.commit();

						return ResponseHelper.PP_TRANSACTION_CONFIRM_SUCCESS;
					}
				}
			}
		    else if(postPaidTransactionBean != null){
		    	
		    	if ( postPaidTransactionBean.getUserBean().getId()!=userBean.getId() ) {

					// Non esiste una transazione attiva
			    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User Id wrong in confirm transaction" );
					
	    			userTransaction.commit();
	    			
	    			return ResponseHelper.PP_TRANSACTION_CONFIRM_WRONG_ID;
				}
				else {
					if (postPaidTransactionBean.getMpTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)){
						System.out.println( "last status: " + postPaidTransactionBean.getMpTransactionStatus() );
						// Non esiste una transazione attiva
				    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to confirm transaction in status " + postPaidTransactionBean.getMpTransactionStatus() );
					
		    			userTransaction.commit();
		    			
		    			return ResponseHelper.PP_TRANSACTION_CONFIRM_FAILURE;
						
					}
					
					postPaidTransactionBean.setNotificationUser(true);
					em.merge(postPaidTransactionBean);
					userTransaction.commit();

					return ResponseHelper.PP_TRANSACTION_CONFIRM_SUCCESS;
					
				}
		    	
		    }
		    
		    return ResponseHelper.PP_TRANSACTION_CONFIRM_WRONG_ID;
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED confirm transaction with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message );
			    
	        throw new EJBException(ex2);
    	}
		
    }
}
