package com.techedge.mp.core.actions.poptransaction;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidReverseShopTransactionResponse;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPTransactionReverseShopAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public PoPTransactionReverseShopAction() {}

    public PostPaidReverseShopTransactionResponse execute(String requestID, String mpTransactionID, GPServiceRemote gpService, ForecourtPostPaidServiceRemote forecourtPPService,
            FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, String proxyHost, String proxyPort, String proxyNoHosts, 
            UserCategoryService userCategoryService, StringSubstitution stringSubstitution, EncryptionAES encryptionAES)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            PostPaidReverseShopTransactionResponse poPReverseShopTransactionResponse = new PostPaidReverseShopTransactionResponse();

            PostPaidTransactionBean poPTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, mpTransactionID);

            if (poPTransactionBean == null) {

                // Non esiste nessuna transazione postpaid associato al valore mpTransactionID inserito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Transazione pop non trovata: " + mpTransactionID);

                userTransaction.commit();

                poPReverseShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REVERSE_NOT_RECOGNIZED);
                return poPReverseShopTransactionResponse;
            }

            userTransaction.commit();

            // Verificare se la transazione pu� essere stornata
            // TODO

            /*
             * // Se i controlli di stornabilit� sono superati il backend avvia un thread separato per lo storno e restituisce al gestionale il messaggio di storno effettuato
             * popTransactionBean
             * forecourtPPService
             * gpService
             * em
             */
            
            String decodedSecretKey = encryptionAES.decrypt(poPTransactionBean.getEncodedSecretKey());

            Runnable r = new PoPTransactionReverseShopThread(poPTransactionBean, requestID, forecourtPPService, gpService, loggerService, userTransaction, fidelityService,
                    emailSender, em, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution, decodedSecretKey);

            new Thread(r).start();

            poPReverseShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REVERSE_SUCCESS);
            return poPReverseShopTransactionResponse;

            /*
             * Set <PostPaidTransactionEventBean> postPadTransactionEventBeanList = poPTransactionBean.getPostPaidTransactionEventBean();
             * // Crea il nuovo stato
             * Integer sequenceID = 0;
             * String oldStatus= "";
             * String newStatus= "";
             * for( PostPaidTransactionEventBean postPaidTransactionEventBean : postPadTransactionEventBeanList ) {
             * Integer transactionEventBeanSequenceID = postPaidTransactionEventBean.getSequenceID();
             * if ( sequenceID < transactionEventBeanSequenceID ) {
             * sequenceID = transactionEventBeanSequenceID;
             * oldStatus = postPaidTransactionEventBean.getNewState();
             * }
             * }
             * sequenceID = sequenceID + 1;
             * 
             * 
             * ///
             * Set <PostPaidTransactionPaymentEventBean> postPaidTransactionPaymentEventBeanList = poPTransactionBean.getPostPaidTransactionPaymentEventBean();
             * Integer sequencePaymentID = 0;
             * for( PostPaidTransactionPaymentEventBean postPaidTransactionEventBean : postPaidTransactionPaymentEventBeanList ) {
             * Integer transactionPaymentEventBeanSequenceID = postPaidTransactionEventBean.getSequence();
             * if ( sequencePaymentID < transactionPaymentEventBeanSequenceID ) {
             * sequencePaymentID = transactionPaymentEventBeanSequenceID;
             * }
             * }
             * sequencePaymentID = sequencePaymentID + 1;
             * ///
             * 
             * 
             * 
             * 
             * Date now = new Date();
             * Timestamp eventTimestamp = new java.sql.Timestamp(now.getTime());
             * PostPaidTransactionEventBean postPaidTransactionEventBean;
             * PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
             * PaymentTransactionResult paymentTransactionResult = new PaymentTransactionResult();
             * 
             * //(Eventuale storno) in base alla risposta del forecourt!
             * //Verifica lo stato sul server Forecourt;
             * String getStationPumpRequestId = String.valueOf(new Date().getTime());
             * GetSrcTransactionStatusMessageResponse getSrcTransactionStatusMessageResponse =
             * forecourtPPService.getTransactionStatus(
             * getStationPumpRequestId,
             * poPTransactionBean.getMpTransactionID(),
             * poPTransactionBean.getSrcTransactionID());
             * 
             * if (!getSrcTransactionStatusMessageResponse.getStatusCode().equals("MESSAGE_RECEIVED_200")){
             * //
             * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
             * "Non � stato possibile eseguire lo storno:recupero stato fallito " + mpTransactionID );
             * System.out.println("Non � stato possibile eseguire lo storno: recupero stato fallito");
             * userTransaction.commit();
             * poPReverseShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REVERSE_FAILURE);
             * return poPReverseShopTransactionResponse;
             * 
             * }
             * //
             * if (getSrcTransactionStatusMessageResponse.getSrcTransactionDetail().getSrcTransactionStatus().equals("REVERSED")){
             * 
             * 
             * GestPayData gestPayDataREFUNDResponse = gpService.callRefund(
             * poPTransactionBean.getAmount(),
             * poPTransactionBean.getMpTransactionID(),
             * poPTransactionBean.getShopLogin(),
             * poPTransactionBean.getCurrency(),
             * poPTransactionBean.getBankTansactionID());
             * 
             * 
             * if(!gestPayDataREFUNDResponse.getTransactionResult().equals("OK")){
             * newStatus = StatusHelper.POST_PAID_STATUS_PAY_DELETE;
             * postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID,
             * StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION,
             * requestID,
             * eventTimestamp,
             * StatusHelper.POST_PAID_EVENT_BE_REVERSE,
             * poPTransactionBean,
             * "KO",
             * newStatus,
             * oldStatus,
             * gestPayDataREFUNDResponse.getErrorCode(),
             * gestPayDataREFUNDResponse.getErrorDescription());
             * em.persist(postPaidTransactionEventBean);
             * 
             * sequencePaymentID = sequencePaymentID+1;
             * postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataREFUNDResponse, "STO");
             * em.persist(postPaidTransactionPaymentEventBean);
             * 
             * userTransaction.commit();
             * //return null;
             * poPReverseShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REVERSE_FAILURE);
             * paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataREFUNDResponse, "REVERSE");
             * 
             * poPReverseShopTransactionResponse.setPaymentTransactionResult(paymentTransactionResult);
             * return poPReverseShopTransactionResponse;
             * 
             * }
             * else{
             * 
             * newStatus = StatusHelper.POST_PAID_STATUS_REFUND;
             * postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID,
             * StatusHelper.POST_PAID_EVENT_TYPE_RECONCILIATION,
             * requestID,
             * eventTimestamp,
             * StatusHelper.POST_PAID_EVENT_BE_REVERSE,
             * poPTransactionBean,
             * "OK",
             * newStatus,
             * oldStatus,
             * gestPayDataREFUNDResponse.getErrorCode(),
             * gestPayDataREFUNDResponse.getErrorDescription());
             * em.persist(postPaidTransactionEventBean);
             * 
             * sequencePaymentID = sequencePaymentID+1;
             * postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataREFUNDResponse, "STO");
             * em.persist(postPaidTransactionPaymentEventBean);
             * 
             * poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED);
             * poPTransactionBean.setLastModifyTimestamp(eventTimestamp);
             * 
             * em.persist(poPTransactionBean);
             * userTransaction.commit();
             * //return null;
             * poPReverseShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REVERSE_SUCCESS);
             * paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataREFUNDResponse, "REVERSE");
             * 
             * poPReverseShopTransactionResponse.setPaymentTransactionResult(paymentTransactionResult);
             * return poPReverseShopTransactionResponse;
             * }
             * }
             * else{
             * //
             * this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Non � stato possibile eseguire lo storno:stati non coerenti " +
             * mpTransactionID );
             * 
             * System.out.println("REVERSE: Non � stato possibile eseguire lo storno:stati non coerenti");
             * 
             * userTransaction.commit();
             * poPReverseShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REVERSE_FAILURE);
             * return poPReverseShopTransactionResponse;
             * 
             * }
             */
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED Post Paid reverse transaction with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

}
