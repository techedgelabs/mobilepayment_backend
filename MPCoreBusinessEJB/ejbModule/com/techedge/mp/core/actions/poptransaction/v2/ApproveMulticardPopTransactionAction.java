package com.techedge.mp.core.actions.poptransaction.v2;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.RefuelingServiceRemote;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.CRMSfNotifyEvent;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveMulticardShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.CardDepositTransactionBean;
import com.techedge.mp.core.business.model.EsPromotionBean;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionAdditionalDataBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventBean;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.utilities.crm.EVENT_TYPE;
import com.techedge.mp.core.business.utilities.crm.PaymentModeUtil;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CodeEnum;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityConstants;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.LoadLoyaltyCreditsResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.integration.shop.interfaces.SendMPTransactionResultMessageResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.ExtendedGestPayData;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;
import com.techedge.mp.payment.adapter.business.interfaces.KeyValueData;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ApproveMulticardPopTransactionAction {

    @Resource
    private EJBContext                   context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager                em;

    @EJB
    private LoggerService                loggerService;

    @EJB
    private CRMService                   crmService;
    
    @EJB
    private RefuelingServiceRemote refuelingService;
    
    @EJB
    private ParametersService parametersService;
    
    private final String 				PARAM_CRM_SF_ACTIVE="CRM_SF_ACTIVE";

    private PostPaidTransactionEventBean lastPostPaidTransactionEventBean;
    private Integer                      sequenceID      = 0;
    private String                       newStatus       = null;
    private String                       oldStatus       = null;
    private PaymentInfoBean              paymentInfoBean = null;

    public ApproveMulticardPopTransactionAction() {}

    public PostPaidApproveMulticardShopTransactionResponse execute(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId,
            String paymentCryptogram, Integer pinCheckMaxAttempts, List<String> userBlockExceptionList, GPServiceRemote gpService, ForecourtPostPaidServiceRemote forecourtPPService,
            FidelityServiceRemote fidelityService, UserCategoryService userCategoryService, EmailSenderRemote emailSender, UnavailabilityPeriodService unavailabilityPeriodService,
            String proxyHost, String proxyPort, String proxyNoHosts, EncryptionAES encryptionAES, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        
        PostPaidApproveMulticardShopTransactionResponse postPaidApproveMulticardShopTransactionResponse = new PostPaidApproveMulticardShopTransactionResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_INVALID_TICKET);
                return postPaidApproveMulticardShopTransactionResponse;
            }
            //    		
            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_INVALID_TICKET);
                return postPaidApproveMulticardShopTransactionResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus);

                userTransaction.commit();

                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_UNAUTHORIZED);
                return postPaidApproveMulticardShopTransactionResponse;
            }

            PostPaidTransactionBean poPTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, mpTransactionID);
            if (poPTransactionBean == null) {

                // L'mpTransactionID inserito non corrisponde a nessuna transazione valida
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No postPaid transaction found by mpTransactionId "
                        + mpTransactionID);

                userTransaction.commit();

                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_NOT_RECOGNIZED);
                return postPaidApproveMulticardShopTransactionResponse;
            }
            
            StationBean stationBean = poPTransactionBean.getStationBean();
            System.out.println("PV active? " + stationBean.getNewAcquirerActive());
            if (stationBean == null || !stationBean.getNewAcquirerActive()) {
                
                // Il PV non risulta abilitato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "PV not enabled");

                userTransaction.commit();

                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PV_NOT_ENABLED);
                return postPaidApproveMulticardShopTransactionResponse;
            }

            Boolean isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            Boolean isGuestFlow       = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.GUEST_FLOW.getCode());
            Boolean isMulticardFlow   = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.MULTICARD_FLOW.getCode());

            if (!isNewAcquirerFlow && !isGuestFlow && !isMulticardFlow) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                        "User is not in new aquirer or guest flow category: " + userBean.getUserType());

                userTransaction.commit();

                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_UNAUTHORIZED);
                return postPaidApproveMulticardShopTransactionResponse;
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User found in new aquirer flow category");
            }

            Double amount = poPTransactionBean.getAmount();

            
            // Verifica che l'utente abbia gi� associato il pin
            PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();
            
            if ( !isMulticardFlow && (paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals(""))) {
                
                // Errore Pin non ancora inserito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");
            
                userTransaction.commit();
                
                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_UNAUTHORIZED);
                return postPaidApproveMulticardShopTransactionResponse;
            }

            // Verifica il metodo utilizzato per il pagamento
            System.out.println("Verifica metodo di pagamento");

            if (paymentMethodId == null) {

                // Se il medodo di pagamento non � specificato restituisci un errore
                
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "PaymentMethodId not inserted");
                
                userTransaction.commit();
                
                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_UNAUTHORIZED);
                return postPaidApproveMulticardShopTransactionResponse;
            }
            
            
            PaymentInfoBean paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);

            // Verifica che il metodo di pagamento selezionato sia valido
            if (paymentInfoBean == null) {

                // Il metodo di pagamento selezionato non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");

                userTransaction.commit();

                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PAYMENT_WRONG);
                return postPaidApproveMulticardShopTransactionResponse;
            }
            

            if (paymentInfoBean.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {

                // Il metodo di pagamento selezionato non pu� essere utilizzato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());

                userTransaction.commit();

                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PAYMENT_WRONG);
                return postPaidApproveMulticardShopTransactionResponse;
            }
            
            
            if ( paymentCryptogram == null ) {
                
                // Errore PaymentCryptogram non inserito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "PaymentCryptogram not inserted");
            
                userTransaction.commit();
                
                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_FAILURE);
                return postPaidApproveMulticardShopTransactionResponse;
            }
            
            
            System.out.println("full amount: " + amount);

            em.lock(poPTransactionBean, LockModeType.PESSIMISTIC_READ);

            System.out.println("Oggetto bloccato");

            if (!poPTransactionBean.getMpTransactionStatus().endsWith(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                        "Transaction Status not payable " + poPTransactionBean.getMpTransactionStatus());

                userTransaction.commit();

                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_NOT_RECOGNIZED);
                return postPaidApproveMulticardShopTransactionResponse;
            }

            poPTransactionBean.setTransactionCategory(TransactionCategoryType.TRANSACTION_CUSTOMER);
            
            //poPTransactionBean.setUserBean(userBean);
            /*
            if (poPTransactionBean.getEncodedSecretKey() == null && stationBean.getDataAcquirer() != null) {
                poPTransactionBean.setEncodedSecretKey(stationBean.getDataAcquirer().getEncodedSecretKey());
                poPTransactionBean.setShopLogin(stationBean.getDataAcquirer().getApiKey());
                poPTransactionBean.setAcquirerID(stationBean.getDataAcquirer().getAcquirerID());
                poPTransactionBean.setGroupAcquirer(stationBean.getDataAcquirer().getGroupAcquirer());
                poPTransactionBean.setCurrency(stationBean.getDataAcquirer().getCurrency());
                em.merge(poPTransactionBean);
            }
            */
            
            poPTransactionBean.setAcquirerID("MULTICARD");
            poPTransactionBean.setCurrency("EUR");
            em.merge(poPTransactionBean);
            

            String decodedSecretKey = "";

            poPTransactionBean.setPaymentMethodType(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);
            poPTransactionBean.setVoucherReconcile(false);
            poPTransactionBean.setLoyaltyReconcile(false);
            poPTransactionBean.setToReconcile(false);
            poPTransactionBean.setNotificationPaid(false);
            poPTransactionBean.setNotificationUser(false);
            String refuelMode = null; // valorizzato successivamente
            newStatus = StatusHelper.POST_PAID_STATUS_PAY_REQU;
            oldStatus = null;
            sequenceID = 1;
            Timestamp eventTimestamp = new Timestamp(new Date().getTime());
            String transactionEvent = StatusHelper.POST_PAID_EVENT_MA_PAY;
            String errorCode = null;
            String errorDescription = null;
            String eventResult = "OK";
            PostPaidTransactionEventBean lastPostPaidTransactionEventBean = poPTransactionBean.getLastPostPaidTransactionEventBean();
            List<ProductDetail> totalProductList = new ArrayList<ProductDetail>(0);
            
            PostPaidLoadLoyaltyCreditsBean crmPostPaidLoadLoyaltyCredits = null;
            PostPaidRefuelBean crmPostPaidRefuelBean = null;

            if (lastPostPaidTransactionEventBean != null) {
                sequenceID = lastPostPaidTransactionEventBean.getSequenceID() + 1;
                oldStatus = lastPostPaidTransactionEventBean.getNewState();
            }

            PostPaidTransactionEventBean postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                    eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

            oldStatus = newStatus;

            em.persist(postPaidTransactionEventBean);

            if (poPTransactionBean.getRefuelBean().isEmpty() && poPTransactionBean.getCartBean().isEmpty()) {

                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_NO_REFUEL_AND_CART_FOUND);
                return postPaidApproveMulticardShopTransactionResponse;
            }

            int totalTransactionTask = 1;

            if (poPTransactionBean.getRefuelBean().size() > 1) {
                totalTransactionTask = poPTransactionBean.getRefuelBean().size();
            }
            else {
                refuelMode = FidelityConstants.REFUEL_MODE_ENI_CAFE_ONLY;
            }

            PostPaidRefuelBean[] refuelBeanArray = new PostPaidRefuelBean[poPTransactionBean.getRefuelBean().size()];
            poPTransactionBean.getRefuelBean().toArray(refuelBeanArray);

            for (int index = 0; index < totalTransactionTask; index++) {

                Boolean productBDFound = Boolean.FALSE;
                
                refuelMode = null;
                List<ProductDetail> productList = new ArrayList<ProductDetail>(0);
                if (!poPTransactionBean.getRefuelBean().isEmpty()) {
                    PostPaidRefuelBean refuelBean = refuelBeanArray[index];
                    crmPostPaidRefuelBean = refuelBean;
                    System.out.println("trovato refuel mode: " + refuelBean.getRefuelMode());

                    if (refuelBean.getRefuelMode().equalsIgnoreCase("servito")) {
                        refuelMode = FidelityConstants.REFUEL_MODE_SERVITO;
                    }

                    if (refuelBean.getRefuelMode().equalsIgnoreCase("fai_da_te")) {
                        refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_POSTPAY;
                    }

                    if (refuelMode == null) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                                "Tipo di rifornimento sconosciuto. Operazione scartata!");
                    }

                    ProductDetail productDetail = new ProductDetail();
                    productDetail.setAmount(refuelBean.getFuelAmount());

                    System.out.println("trovato product id: " + refuelBean.getProductId());

                    if (refuelBean.getProductId().equals("SP")) {

                        // sp
                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_SP);
                    }
                    else {

                        if (refuelBean.getProductId().equals("GG")) {

                            // gasolio
                            productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GASOLIO);
                        }
                        else {

                            if (refuelBean.getProductId().equals("BS")) {

                                // blue_super
                                productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_SUPER);
                            }
                            else {

                                if (refuelBean.getProductId().equals("BD")) {

                                    // blue_diesel
                                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_DIESEL);
                                    productBDFound = Boolean.TRUE;
                                }
                                else {

                                    if (refuelBean.getProductId().equals("MT")) {

                                        // metano
                                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_METANO);
                                    }
                                    else {

                                        if (refuelBean.getProductId().equals("GP")) {

                                            // gpl
                                            productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GPL);
                                        }
                                        else {

                                            if (refuelBean.getProductId().equals("AD")) {

                                                // ???
                                                productDetail.setProductCode(null);
                                            }
                                            else {

                                                // non_oil
                                                productDetail.setProductCode(null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    productDetail.setQuantity(refuelBean.getFuelQuantity());
                    productList.add(productDetail);
                    totalProductList.add(productDetail);
                }

                if (index == 0 && !poPTransactionBean.getCartBean().isEmpty()) {
                    System.out.println("trovato shop");

                    if (refuelMode == null) {
                        refuelMode = FidelityConstants.REFUEL_MODE_ENI_CAFE_ONLY;
                    }

                    boolean validProductFound = false;
                    Double shopAmount = 0.0;
                    Integer shopCount = 0;

                    for (PostPaidCartBean postPaidCartBean : poPTransactionBean.getCartBean()) {

                        if (postPaidCartBean.getProductId().equals(FidelityConstants.PRODUCT_CODE_NON_OIL))     // PRODOTTI NON OIL 70
                        {
                            validProductFound = true;
                            shopAmount = shopAmount + postPaidCartBean.getAmount();
                            shopCount = shopCount + postPaidCartBean.getQuantity();

                            System.out.println("amount: " + postPaidCartBean.getAmount() + ", count: " + postPaidCartBean.getQuantity());
                        }
                    }

                    if (validProductFound) {
                        ProductDetail shopProductDetail = new ProductDetail();

                        shopProductDetail.setAmount(shopAmount);
                        shopProductDetail.setProductCode(FidelityConstants.PRODUCT_CODE_NON_OIL);
                        shopProductDetail.setQuantity(Double.valueOf(shopCount));

                        productList.add(shopProductDetail);
                        totalProductList.add(shopProductDetail);
                    }
                }

                System.out.println("Controllo caricamento punti carta fedelt�");
                
                // Se l'utente ha una carta loyalty associata allora bisogna effettuare anche il caricamento dei punti
                
                /* Inizio modifica per utilizzare solo le nuove carte dematerializzate
                 * LoyaltyCardBean loyaltyCardBean = userBean.getFirstLoyaltyCard();
                 * Fine modifica
                 */
                LoyaltyCardBean loyaltyCardBean = userBean.getVirtualLoyaltyCard();

                if (!productList.isEmpty() && (loyaltyCardBean != null || (refuelMode == FidelityConstants.REFUEL_MODE_SERVITO && productBDFound))) {

                    String fiscalCode = "";
                    String panCode    = "0000000000000000000";
                    String eanCode    = "0000000000000";
                    
                    if (loyaltyCardBean != null) {
                        System.out.println("Trovata carta fedelt�");
                        panCode = loyaltyCardBean.getPanCode();
                        eanCode = loyaltyCardBean.getEanCode();
                    }
                    else {
                        System.out.println("Carta fedelt� non presente ma rifornimento in servito -> invio richiesta con fiscalCode");
                        fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                    }

                    // Chimamata al servizio loadLoyaltyCredits

                    String operationID = new IdGenerator().generateId(16).substring(0, 33);
                    PartnerType partnerType = PartnerType.MP;
                    Long requestTimestamp = new Date().getTime();
                    String stationID = poPTransactionBean.getStationBean().getStationID();
                    String paymentMode = FidelityConstants.PAYMENT_METHOD_MULTICARD;

                    String BIN = paymentInfoBean.getCardBin();

                    PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean();
                    postPaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                    postPaidLoadLoyaltyCreditsBean.setOperationType("LOAD");
                    postPaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                    postPaidLoadLoyaltyCreditsBean.setEanCode(eanCode);

                    try {

                        System.out.println("Chiamata servizio di carico punti");

                        LoadLoyaltyCreditsResult loadLoyaltyCreditsResult = fidelityService.loadLoyaltyCredits(operationID, mpTransactionID, stationID,
                                panCode, BIN, refuelMode, paymentMode, "it", partnerType, requestTimestamp, fiscalCode, productList);

                        System.out.println("Risposta servizio di carico punti: " + loadLoyaltyCreditsResult.getStatusCode() + " (" + loadLoyaltyCreditsResult.getMessageCode()
                                + ")");

                        postPaidLoadLoyaltyCreditsBean.setBalance(loadLoyaltyCreditsResult.getBalance());
                        postPaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                        postPaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                        postPaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                        postPaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                        postPaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                        postPaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                        postPaidLoadLoyaltyCreditsBean.setCsTransactionID(loadLoyaltyCreditsResult.getCsTransactionID());
                        postPaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                        postPaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                        postPaidLoadLoyaltyCreditsBean.setMessageCode(loadLoyaltyCreditsResult.getMessageCode());
                        postPaidLoadLoyaltyCreditsBean.setStatusCode(loadLoyaltyCreditsResult.getStatusCode());
                        postPaidLoadLoyaltyCreditsBean.setWarningMsg(loadLoyaltyCreditsResult.getWarningMsg());

                        postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);
                        em.persist(postPaidLoadLoyaltyCreditsBean);
                        
                        if (loadLoyaltyCreditsResult.getVoucherList() != null && !loadLoyaltyCreditsResult.getVoucherList().isEmpty()) {
                            for(VoucherDetail voucherDetail : loadLoyaltyCreditsResult.getVoucherList()) {
                                PostPaidLoadLoyaltyCreditsVoucherBean postPaidLoadLoyaltyCreditsVoucherBean = new PostPaidLoadLoyaltyCreditsVoucherBean();
                                postPaidLoadLoyaltyCreditsVoucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                                postPaidLoadLoyaltyCreditsVoucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                                postPaidLoadLoyaltyCreditsVoucherBean.setInitialValue(voucherDetail.getInitialValue());
                                postPaidLoadLoyaltyCreditsVoucherBean.setPromoCode(voucherDetail.getPromoCode());
                                postPaidLoadLoyaltyCreditsVoucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                                postPaidLoadLoyaltyCreditsVoucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                                postPaidLoadLoyaltyCreditsVoucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                                postPaidLoadLoyaltyCreditsVoucherBean.setCode(voucherDetail.getVoucherCode());
                                postPaidLoadLoyaltyCreditsVoucherBean.setStatus(voucherDetail.getVoucherStatus());
                                postPaidLoadLoyaltyCreditsVoucherBean.setType(voucherDetail.getVoucherType());
                                postPaidLoadLoyaltyCreditsVoucherBean.setValue(voucherDetail.getVoucherValue());
                                postPaidLoadLoyaltyCreditsVoucherBean.setPostPaidLoadLoyaltyCreditsBean(postPaidLoadLoyaltyCreditsBean);
                                em.persist(postPaidLoadLoyaltyCreditsVoucherBean);
                                postPaidLoadLoyaltyCreditsBean.getPostPaidLoadLoyaltyCreditsVoucherBean().add(postPaidLoadLoyaltyCreditsVoucherBean);
                                
                            }
                        }

                        em.merge(postPaidLoadLoyaltyCreditsBean);
                        
                        poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(postPaidLoadLoyaltyCreditsBean);

                        crmPostPaidLoadLoyaltyCredits = postPaidLoadLoyaltyCreditsBean;
                        //em.persist(postPaidLoadLoyaltyCreditsBean);
                        //em.merge(poPTransactionBean);

                        if (!loadLoyaltyCreditsResult.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {
                            errorCode = loadLoyaltyCreditsResult.getStatusCode();
                            errorDescription = loadLoyaltyCreditsResult.getMessageCode();
                            eventResult = "KO";
                        }
                        else {
                            errorCode = null;
                            errorDescription = null;
                            eventResult = "OK";
                        }

                    }
                    catch (Exception e) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error loading loyalty credits");

                        postPaidLoadLoyaltyCreditsBean.setStatusCode(StatusHelper.LOYALTY_STATUS_TO_BE_VERIFIED);
                        postPaidLoadLoyaltyCreditsBean.setMessageCode("STORNO DA VERIFICARE (" + e.getMessage() + ")");
                        postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);
                        poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(postPaidLoadLoyaltyCreditsBean);
                        poPTransactionBean.setLoyaltyReconcile(true);

                        errorCode = "9999";
                        errorDescription = "Error loading loyalty credits (" + e.getMessage() + ")";
                        eventResult = "ERROR";
                    }

                    oldStatus = newStatus;
                    newStatus = StatusHelper.POST_PAID_STATUS_CREDITS_LOAD;
                    sequenceID += 1;
                    eventTimestamp = new Timestamp(new Date().getTime());
                    transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY;

                    postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp,
                            transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                    em.persist(postPaidTransactionEventBean);

                }
                else {
                
                    System.out.println("Nessuna carta fedelt� associata al codice fiscale (" + userBean.getPersonalDataBean().getFiscalCode() + ")");
                    
                }
            
            }
            
            System.out.println("Pagamento con Multicard -> voucher non abilitati");
            
            eventTimestamp = new java.sql.Timestamp(new Date().getTime());

                        
            Extension[] extension_array = new Extension[1];
            Extension i_extension = new Extension();
            i_extension.setKey("CST_DISTRID");
            i_extension.setValue(poPTransactionBean.getStationBean().getStationID());
            extension_array[0] = i_extension;

            sequenceID += 1;
            Integer sequencePaymentID = 1;

            PostPaidRefuelBean postPaidRefuelBean = null;
            if (!poPTransactionBean.getRefuelBean().isEmpty()) {
                postPaidRefuelBean = poPTransactionBean.getRefuelBean().iterator().next();
            }
            
            ExtendedGestPayData gestPayDataAUTHANDMOVResponse = doPaymentMov(poPTransactionBean, postPaidRefuelBean, paymentInfoBean, userBean, paymentMethodId, amount,
                    sequencePaymentID, requestID, sequenceID, gpService, paymentCryptogram);
            
            if (gestPayDataAUTHANDMOVResponse.getTransactionResult().equals("ERROR")) {
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);
                poPTransactionBean.setToReconcile(true);
                poPTransactionBean.setNotificationPaid(false);

                em.persist(poPTransactionBean);

                Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, stringSubstitution);
                
                userTransaction.commit();
                
                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                
                return postPaidApproveMulticardShopTransactionResponse;
            }

            if (gestPayDataAUTHANDMOVResponse.getTransactionResult().equals("KO")) {

                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);

                try {
                    // Storno caricamento punti carta loyalty e aggiornamento dati utante
                    this.revertLoadLoyaltyCredits(poPTransactionBean, fidelityService);
                }
                catch (FidelityServiceException ex) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                            "Exception in revertLoadLoyaltyCredits: " + ex.getLocalizedMessage());
                    poPTransactionBean.setLoyaltyReconcile(true);
                }

                Boolean loyaltyCredits = isCreditsLoyaltyLoaded(poPTransactionBean);
                PaymentTransactionResult paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataAUTHANDMOVResponse, amount, "MOV");

                sequenceID += 1;
                String notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                        poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                        forecourtPPService, StatusHelper.POST_PAID_STATUS_CANCELED, null, null);

                if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                        || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                        || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)) {

                    poPTransactionBean.setNotificationPaid(true);
                    poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);
                }
                else {
                    poPTransactionBean.setToReconcile(true);
                    poPTransactionBean.setNotificationPaid(false);
                }

                updateVoucherAndLoyaltyData(poPTransactionBean);

                em.persist(poPTransactionBean);
                
                if (( userBean.getUserType() == User.USER_TYPE_CUSTOMER || userBean.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER )) {
                    Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                            userCategoryService, stringSubstitution);
                }
                
                // Gestione stato != SUCCESS
                String code = gestPayDataAUTHANDMOVResponse.getErrorCode();
                
                String statusCode = this.convertCodeToStatusCode(code);
                
                userTransaction.commit();
                
                postPaidApproveMulticardShopTransactionResponse.setStatusCode(statusCode);
                
                return postPaidApproveMulticardShopTransactionResponse;
            }
            
            poPTransactionBean.setNotificationUser(true);
            Boolean loyaltyCredits = isCreditsLoyaltyLoaded(poPTransactionBean);

            PaymentTransactionResult paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataAUTHANDMOVResponse, amount, "MOV");
            
            for(KeyValueData keyValueData : gestPayDataAUTHANDMOVResponse.getReceiptElementList()) {
                PostPaidTransactionAdditionalDataBean postPaidTransactionAdditionalDataBean = new PostPaidTransactionAdditionalDataBean();
                postPaidTransactionAdditionalDataBean.setDataKey(keyValueData.getKey());
                postPaidTransactionAdditionalDataBean.setDataValue(keyValueData.getValue());
                postPaidTransactionAdditionalDataBean.setPostPaidTransactionBean(poPTransactionBean);
                em.persist(postPaidTransactionAdditionalDataBean);
                poPTransactionBean.getPostPaidTransactionAdditionalDataBeanList().add(postPaidTransactionAdditionalDataBean);
            }
            
            sequenceID += 1;
            String notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                    poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                    forecourtPPService, StatusHelper.POST_PAID_STATUS_PAY_COMPLETE, StatusHelper.POST_PAID_STATUS_NOTIFICATION_FAIL, StatusHelper.POST_PAID_STATUS_PAY_MOV_DELETE);

            if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS)) {
                poPTransactionBean.setNotificationPaid(true);
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_OK);
            }

            if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setToReconcile(true);
                poPTransactionBean.setNotificationPaid(false);
                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
            }

            if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                    || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                    || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)
                    || (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_REJECTED) && poPTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode()))) {

                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);
                poPTransactionBean.setNotificationPaid(true);

                sequenceID += 1;
                sequencePaymentID += 1;

                GestPayData gestPayDataREFUNDResponse = doPaymentRefund(poPTransactionBean, paymentInfoBean, userBean, paymentMethodId, amount, sequencePaymentID, requestID,
                        sequenceID, gpService, decodedSecretKey);

                if (!gestPayDataREFUNDResponse.getTransactionResult().equals("OK")) {

                    poPTransactionBean.setToReconcile(true);
                }
                else {

                    poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);

                    try {
                        // Storno caricamento punti carta loyalty e aggiornamento dati utante
                        this.revertLoadLoyaltyCredits(poPTransactionBean, fidelityService);
                    }
                    catch (FidelityServiceException ex) {
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                                "Exception in revertLoadLoyaltyCredits: " + ex.getLocalizedMessage());
                        poPTransactionBean.setLoyaltyReconcile(true);
                    }

                }

                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
            }

            em.persist(poPTransactionBean);
            updateVoucherAndLoyaltyData(poPTransactionBean);
            
            
            if ( userBean.getUserType() == User.USER_TYPE_CUSTOMER || userBean.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER ) {                
                receiveCRMEventOffer(poPTransactionBean, userBean, crmPostPaidRefuelBean, crmPostPaidLoadLoyaltyCredits, refuelMode);
                Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, stringSubstitution);
            }
            else {
                System.out.println("Email e evento al CRM non inviati per utente di tipo " + userBean.getUserType());
            }
            
            Date now = new Date();
            final String fiscalcode     = userBean.getPersonalDataBean().getFiscalCode();
            final String inputRequestId = (new Long(now.getTime())).toString();
            
            if(userBean.getSource() != null && userBean.getSource().equals(User.USER_SOURCE_ENJOY)){
                if (userBean.getSourceNotified() == null || !userBean.getSourceNotified()) {
                    new Thread(new Runnable() {
                        
                        @Override
                        public void run() {
                            String notifySubscriptionResponse = refuelingService.notifySubscription(inputRequestId, fiscalcode);
                            System.out.println("**** INFO from UseV2SkipVirtualizationAction ***** Calling notifySubscription for user enjoy notification (" + notifySubscriptionResponse + ")");
                            if (!notifySubscriptionResponse.equals(StatusHelper.REFUELING_SUBSCRIPTION_SUCCESS)) {
                                System.err.println("Error in sending subscription notification (" + notifySubscriptionResponse + ")");
                            }
                        }
                    }, "executeBatchNotifySubscription (TransactionPersistPaymentCompletitionStatusAction)").start();

                }
                else {
                    System.out.println("Notifica Refueling gi� inviata per l'utente con codice fiscale: " + fiscalcode);
                }
            }
            else {
                System.out.println("Notifica Refueling non inviata. L'utente non � di tipo ENJOY");
            }
            
            /************************************************************/
            /* Impostazione flag toBeProcessed per promo Vodafone Black */
            
            // Se per l'utente � presente una riga nella tabella ES_PROMOTION
            // con intervallo di valifit� che comprende la data di creazione
            // della transazione bisogna impostare il campo toBeProcessed
            // a true
            
            EsPromotionBean esPromotionBean = QueryRepository.findEsPromotionToBeProcessedWithNullVoucherCodeByDate(em, poPTransactionBean.getUserBean(), poPTransactionBean.getCreationTimestamp());
            if (esPromotionBean != null) {
                System.out.println("Trovata riga EsPromotionBean con flag da impostare");
                esPromotionBean.setToBeProcessed(Boolean.TRUE);
                em.merge(esPromotionBean);
            }
            
            /************************************************************/
            
            userTransaction.commit();
            
            postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_OK);
            return postPaidApproveMulticardShopTransactionResponse;
        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            if (ex2.getClass().getSimpleName().equals("LockAcquisitionException")) {

                System.out.println("Dentro If" + ex2.getClass().getSimpleName());
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Transaction Status not payable ");
                postPaidApproveMulticardShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_NOT_RECOGNIZED);

                try {
                    userTransaction.rollback();
                }
                catch (IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (SecurityException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (SystemException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                return postPaidApproveMulticardShopTransactionResponse;

            }

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED Post Paid approve transaction with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
    
    
    private ExtendedGestPayData doPaymentMov(PostPaidTransactionBean poPTransactionBean, PostPaidRefuelBean postPaidRefuelBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId, Double amount,
            Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String paymentCryptogram) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_MOV;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_MOV;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        String authorizationCode = null;
        String bankTransactionId = null;
        
        ExtendedGestPayData gestPayDataPagamAndSettleResponse = gpService.callPagamAndSettle(amount, poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), poPTransactionBean.getAcquirerID(), paymentInfoBean.getToken(), poPTransactionBean.getRefuelMode(), poPTransactionBean.getProductID(),
                postPaidRefuelBean.getFuelQuantity(), postPaidRefuelBean.getUnitPrice(), poPTransactionBean.getStationBean().getStationID(), paymentCryptogram);
        
        if (gestPayDataPagamAndSettleResponse == null) {
            gestPayDataPagamAndSettleResponse = new ExtendedGestPayData();
            gestPayDataPagamAndSettleResponse.setTransactionResult("ERROR");
            gestPayDataPagamAndSettleResponse.setErrorCode("9999");
            gestPayDataPagamAndSettleResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay SETTLE: " + gestPayDataPagamAndSettleResponse.getTransactionResult());

        errorCode = gestPayDataPagamAndSettleResponse.getErrorCode();
        errorDescription = gestPayDataPagamAndSettleResponse.getErrorDescription();
        eventResult = gestPayDataPagamAndSettleResponse.getTransactionResult();
        if (gestPayDataPagamAndSettleResponse.getGps2s() != null) {
            authorizationCode = gestPayDataPagamAndSettleResponse.getGps2s().getAuthorizationCode();
            bankTransactionId = gestPayDataPagamAndSettleResponse.getGps2s().getBankTransactionID();
        }

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataPagamAndSettleResponse, "MOV");

        em.persist(postPaidTransactionPaymentEventBean);

        poPTransactionBean.setPaymentMethodId(paymentMethodId);
        poPTransactionBean.setToken(paymentInfoBean.getToken());
        poPTransactionBean.setUserBean(userBean);
        poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
        poPTransactionBean.setLastModifyTimestamp(eventTimestamp);
        poPTransactionBean.setAuthorizationCode(authorizationCode);
        poPTransactionBean.setBankTansactionID(bankTransactionId);
        //poPTransactionBean.setBankTansactionID(gestPayDataSETTLEResponse.getBankTransactionID());

        //em.persist(poPTransactionBean);

        oldStatus = newStatus;

        return gestPayDataPagamAndSettleResponse;
    }


    private GestPayData doPaymentAuth(PostPaidTransactionBean poPTransactionBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId, Double amount,
            Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, Extension[] extension_array, String encodedSecretKey) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_AUTH;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_AUTH;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        String transactionCategory = poPTransactionBean.getTransactionCategory().getValue();
        
        String paymentMethodExpiration = "";
        
        if (paymentInfoBean != null) {
            
            CardDepositTransactionBean cardDepositTransactionBean = QueryRepository.findCardDepositTransactionByShopTransactionID(em, paymentInfoBean.getCheckShopTransactionID());
            if (cardDepositTransactionBean != null) {
                
                String tokenExpiryMonth = cardDepositTransactionBean.getTokenExpiryMonth();
                String tokenExpiryYear  = cardDepositTransactionBean.getTokenExpiryYear();
                
                paymentMethodExpiration = "20" + tokenExpiryYear + tokenExpiryMonth;
            }                
        }

        GestPayData gestPayDataAUTHResponse = gpService.callPagam(amount, poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), paymentInfoBean.getToken(), null, poPTransactionBean.getAcquirerID(), poPTransactionBean.getGroupAcquirer(), 
                encodedSecretKey, paymentMethodExpiration, extension_array, transactionCategory);

        if (gestPayDataAUTHResponse == null) {
            gestPayDataAUTHResponse = new GestPayData();
            gestPayDataAUTHResponse.setTransactionResult("ERROR");
            gestPayDataAUTHResponse.setErrorCode("9999");
            gestPayDataAUTHResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH: " + gestPayDataAUTHResponse.getTransactionResult());

        errorCode = gestPayDataAUTHResponse.getErrorCode();
        errorDescription = gestPayDataAUTHResponse.getErrorDescription();
        eventResult = gestPayDataAUTHResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataAUTHResponse, "AUT");

        em.persist(postPaidTransactionPaymentEventBean);

        poPTransactionBean.setPaymentMethodId(paymentMethodId);
        poPTransactionBean.setToken(paymentInfoBean.getToken());
        poPTransactionBean.setUserBean(userBean);
        poPTransactionBean.setLastModifyTimestamp(eventTimestamp);
        poPTransactionBean.setBankTansactionID(gestPayDataAUTHResponse.getBankTransactionID());
        poPTransactionBean.setAuthorizationCode(gestPayDataAUTHResponse.getAuthorizationCode());

        oldStatus = newStatus;

        return gestPayDataAUTHResponse;
    }

    private GestPayData doPaymentSettle(PostPaidTransactionBean poPTransactionBean, PostPaidRefuelBean postPaidRefuelBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId, Double amount,
            Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_MOV;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_MOV;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataSETTLEResponse = gpService.callSettle(amount, poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), poPTransactionBean.getAcquirerID(), poPTransactionBean.getGroupAcquirer(), encodedSecretKey,
                poPTransactionBean.getToken(), poPTransactionBean.getAuthorizationCode(), poPTransactionBean.getBankTansactionID(), poPTransactionBean.getRefuelMode(),
                poPTransactionBean.getProductID(), postPaidRefuelBean.getFuelQuantity(), postPaidRefuelBean.getUnitPrice());

        if (gestPayDataSETTLEResponse == null) {
            gestPayDataSETTLEResponse = new GestPayData();
            gestPayDataSETTLEResponse.setTransactionResult("ERROR");
            gestPayDataSETTLEResponse.setErrorCode("9999");
            gestPayDataSETTLEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay SETTLE: " + gestPayDataSETTLEResponse.getTransactionResult());

        errorCode = gestPayDataSETTLEResponse.getErrorCode();
        errorDescription = gestPayDataSETTLEResponse.getErrorDescription();
        eventResult = gestPayDataSETTLEResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataSETTLEResponse, "MOV");

        em.persist(postPaidTransactionPaymentEventBean);

        poPTransactionBean.setPaymentMethodId(paymentMethodId);
        poPTransactionBean.setToken(paymentInfoBean.getToken());
        poPTransactionBean.setUserBean(userBean);
        poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
        poPTransactionBean.setLastModifyTimestamp(eventTimestamp);
        //poPTransactionBean.setBankTansactionID(gestPayDataSETTLEResponse.getBankTransactionID());

        //em.persist(poPTransactionBean);

        oldStatus = newStatus;

        return gestPayDataSETTLEResponse;
    }

    private GestPayData doPaymentAuthDelete(PostPaidTransactionBean poPTransactionBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId, Double amount,
            Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_AUTH_DELETE;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_AUTH_DEL;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataDELETEResponse = gpService.deletePagam(poPTransactionBean.getAmount(), poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), poPTransactionBean.getBankTansactionID(), null, poPTransactionBean.getAcquirerID(), poPTransactionBean.getGroupAcquirer(),
                encodedSecretKey);

        if (gestPayDataDELETEResponse == null) {
            gestPayDataDELETEResponse = new GestPayData();
            gestPayDataDELETEResponse.setTransactionResult("ERROR");
            gestPayDataDELETEResponse.setErrorCode("9999");
            gestPayDataDELETEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH DEL: " + gestPayDataDELETEResponse.getTransactionResult());

        errorCode = gestPayDataDELETEResponse.getErrorCode();
        errorDescription = gestPayDataDELETEResponse.getErrorDescription();
        eventResult = gestPayDataDELETEResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataDELETEResponse, "DEL");

        em.persist(postPaidTransactionPaymentEventBean);

        oldStatus = newStatus;

        return gestPayDataDELETEResponse;
    }

    private GestPayData doPaymentRefund(PostPaidTransactionBean poPTransactionBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double residualAmount, Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_REVERSE;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        
        GestPayData gestPayDataREFUNDResponse = gpService.callRefund(residualAmount, poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), poPTransactionBean.getToken(), poPTransactionBean.getAuthorizationCode(), poPTransactionBean.getBankTansactionID(),
                poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getAcquirerID(), 
                poPTransactionBean.getGroupAcquirer(), encodedSecretKey);

        if (gestPayDataREFUNDResponse == null) {
            gestPayDataREFUNDResponse = new GestPayData();
            gestPayDataREFUNDResponse.setTransactionResult("ERROR");
            gestPayDataREFUNDResponse.setErrorCode("9999");
            gestPayDataREFUNDResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay REFUND: " + gestPayDataREFUNDResponse.getTransactionResult());

        errorCode = gestPayDataREFUNDResponse.getErrorCode();
        errorDescription = gestPayDataREFUNDResponse.getErrorDescription();
        eventResult = gestPayDataREFUNDResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataREFUNDResponse, "STO");

        em.persist(postPaidTransactionPaymentEventBean);

        oldStatus = newStatus;

        return gestPayDataREFUNDResponse;
    }

    private String notifyResponse(String requestID, Integer eventSequenceID, String stationID, String mpTransactionID, String srcTransactionID, String transactionResult,
            PaymentTransactionResult paymentTransactionResult, Boolean loyaltyCredits, PostPaidTransactionBean poPTransactionBean,
            ForecourtPostPaidServiceRemote forecourtPPService, String eventStatusOk, String eventStatusError, String eventStatusCancelled) {

        // TODO Auto-generated method stub

        System.out.println("*** notifyResponse ***");
        System.out.println("stationID: " + stationID);
        System.out.println("mpTransactionID: " + mpTransactionID);
        System.out.println("srcTransactionID: " + srcTransactionID);
        System.out.println("transactionResult: " + transactionResult);
        if (loyaltyCredits != null) {
            System.out.println("loyaltyCredits: " + loyaltyCredits);
        }
        else {
            System.out.println("loyaltyCredits: null");
        }

        List<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail> vouchers = new ArrayList<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail>(0);

        for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : poPTransactionBean.getPostPaidConsumeVoucherBeanList()) {

            for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {

                com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail voucherDetail = new com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail();

                if (postPaidConsumeVoucherDetailBean.getConsumedValue() == null || postPaidConsumeVoucherDetailBean.getConsumedValue().equals(new Double("0.0"))) {
                    continue;
                }

                voucherDetail.setPromoCode(postPaidConsumeVoucherDetailBean.getPromoCode());
                voucherDetail.setPromoDescription(postPaidConsumeVoucherDetailBean.getPromoDescription());
                voucherDetail.setVoucherAmount(postPaidConsumeVoucherDetailBean.getConsumedValue());
                voucherDetail.setVoucherCode(postPaidConsumeVoucherDetailBean.getVoucherCode());

                System.out.println("voucher - promoCode: " + voucherDetail.getPromoCode() + ", promoDescription: " + voucherDetail.getPromoDescription() + ", voucherAmount: "
                        + voucherDetail.getVoucherAmount() + ", voucherCode: " + voucherDetail.getVoucherCode());

                vouchers.add(voucherDetail);
            }
        }

        SendMPTransactionResultMessageResponse sendMPTransactionResultMessageResponse = null;
        String notifyResponse = null;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = null;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION;
        
        try {

            sendMPTransactionResultMessageResponse = forecourtPPService.sendMPTransactionResult(requestID, stationID, mpTransactionID, srcTransactionID, transactionResult,
                    paymentTransactionResult, loyaltyCredits, vouchers, null);
            notifyResponse = sendMPTransactionResultMessageResponse.getStatusCode();
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                    "Exception in sendMpTransactionResult: " + ex.getLocalizedMessage());

            notifyResponse = StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR;
            errorCode = "9999";
            errorDescription = "Error in sendMpTransactionResult (" + ex.getMessage() + ")";
            sendMPTransactionResultMessageResponse.setStatusCode(notifyResponse);
            sendMPTransactionResultMessageResponse.setMessageCode(ex.getMessage());
        }

        //if (!sendMPTransactionResultMessageResponse.getStatusCode().equals("MESSAGE_RECEIVED_200")) {}

        System.out.println("sendMPTransactionResult: " + notifyResponse);

        if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS)) {
            newStatus = eventStatusOk;
            eventResult = "OK";
        }
        else if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
            newStatus = (eventStatusError != null) ? eventStatusError : eventStatusOk;
            eventResult = "ERROR";
        }
        else if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
            newStatus = (eventStatusError != null) ? eventStatusError : eventStatusOk;
            eventResult = "ERROR";
            errorDescription = notifyResponse;
        }
        else {
            newStatus = (eventStatusCancelled != null) ? eventStatusCancelled : eventStatusOk;
            eventResult = "KO";
            errorDescription = notifyResponse;
        }

        PostPaidTransactionEventBean postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        oldStatus = newStatus;

        return sendMPTransactionResultMessageResponse.getStatusCode();
    }

    private PostPaidTransactionEventBean generateTransactionEvent(Integer eventSequenceID, String stateType, String requestID, Date eventTimestamp, String event,
            PostPaidTransactionBean poPTransactionBean, String result, String eventNewState, String eventOldState, String errorCode, String errorDescription) {

        PostPaidTransactionEventBean postPaidTransactionEventBean = new PostPaidTransactionEventBean();
        postPaidTransactionEventBean.setSequenceID(eventSequenceID);
        postPaidTransactionEventBean.setStateType(stateType);
        postPaidTransactionEventBean.setRequestID(requestID);
        postPaidTransactionEventBean.setEventTimestamp(eventTimestamp);
        postPaidTransactionEventBean.setEvent(event);
        postPaidTransactionEventBean.setTransactionBean(poPTransactionBean);
        postPaidTransactionEventBean.setResult(result);
        postPaidTransactionEventBean.setNewState(eventNewState);
        postPaidTransactionEventBean.setOldState(eventOldState);
        postPaidTransactionEventBean.setErrorCode(errorCode);
        postPaidTransactionEventBean.setErrorDescription(errorDescription);

        this.lastPostPaidTransactionEventBean = postPaidTransactionEventBean;

        return postPaidTransactionEventBean;
    }

    private PostPaidTransactionPaymentEventBean generateTransactionPaymentEvent(Integer eventSequenceID, PostPaidTransactionBean poPTransactionBean, GestPayData gestPayData,
            String event) {

        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean = new PostPaidTransactionPaymentEventBean();
        postPaidTransactionPaymentEventBean.setSequence(eventSequenceID);
        postPaidTransactionPaymentEventBean.setErrorCode(gestPayData.getErrorCode());
        postPaidTransactionPaymentEventBean.setErrorDescription(gestPayData.getErrorDescription());
        postPaidTransactionPaymentEventBean.setTransactionBean(poPTransactionBean);
        postPaidTransactionPaymentEventBean.setTransactionResult(gestPayData.getTransactionResult());
        postPaidTransactionPaymentEventBean.setAuthorizationCode(gestPayData.getAuthorizationCode());
        postPaidTransactionPaymentEventBean.setEventType(event);

        return postPaidTransactionPaymentEventBean;
    }

    private PaymentTransactionResult generatePaymentTransactionResult(PostPaidTransactionBean poPTransactionBean, GestPayData gestPayData, Double amount, String event) {
        PaymentTransactionResult paymentTransactionResult = new PaymentTransactionResult();
        paymentTransactionResult.setAmount(amount);
        paymentTransactionResult.setAcquirerID(poPTransactionBean.getAcquirerID());
        paymentTransactionResult.setAuthorizationCode(gestPayData.getAuthorizationCode());
        paymentTransactionResult.setBankTransactionID(gestPayData.getBankTransactionID());
        paymentTransactionResult.setCurrency(poPTransactionBean.getCurrency());
        paymentTransactionResult.setErrorCode(gestPayData.getErrorCode());
        paymentTransactionResult.setErrorDescription(gestPayData.getErrorDescription());
        paymentTransactionResult.setEventType(event);
        paymentTransactionResult.setShopLogin(poPTransactionBean.getShopLogin());
        paymentTransactionResult.setPaymentMode(poPTransactionBean.getPaymentMode());
        paymentTransactionResult.setShopTransactionID(gestPayData.getShopTransactionID());
        paymentTransactionResult.setTransactionResult(gestPayData.getTransactionResult().equals("ERROR") ? "KO" : gestPayData.getTransactionResult());

        return paymentTransactionResult;
    }
    
    private void revertLoadLoyaltyCredits(PostPaidTransactionBean poPTransactionBean, FidelityServiceRemote fidelityService) throws FidelityServiceException {

        System.out.println("Storno load loyalty credits");

        ArrayList<PostPaidLoadLoyaltyCreditsBean> revertedPostPaidLoadLoyaltyCreditsBeanList = new ArrayList<PostPaidLoadLoyaltyCreditsBean>(0);
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY_REVERSE;
        String requestID = String.valueOf(new Date().getTime());
        boolean reversed = false;

        for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {

            System.out.println("load loyalty credits status code: " + postPaidLoadLoyaltyCreditsBean.getStatusCode());
            // Bisogna effettuare lo storno solo se l'operazione di caricamento loyalty � andata a buon fine
            if (postPaidLoadLoyaltyCreditsBean == null || !postPaidLoadLoyaltyCreditsBean.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {

                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", poPTransactionBean.getMpTransactionID(), null,
                        "Storno caricamento punti carte loyalty non necessario");

                return;
            }

            // Chimamata al servizio reverseLoadLoyaltyCreditsTransaction

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            String operationIDtoReverse = postPaidLoadLoyaltyCreditsBean.getOperationID();
            PartnerType partnerType = PartnerType.MP;
            Long requestTimestamp = new Date().getTime();
            newStatus = StatusHelper.POST_PAID_STATUS_LOYALTY_REVERSE;

            ReverseLoadLoyaltyCreditsTransactionResult reverseLoadLoyaltyCreditsTransactionResult = fidelityService.reverseLoadLoyaltyCreditsTransaction(operationID,
                    operationIDtoReverse, partnerType, requestTimestamp);

            if (!reverseLoadLoyaltyCreditsTransactionResult.getStatusCode().equals(FidelityResponse.REVERSE_LOAD_LOYALTY_CREDITS_OK)) {

                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", operationID, null, "Error in reverseConsumeVoucherTransaction: "
                        + reverseLoadLoyaltyCreditsTransactionResult.getStatusCode());

                errorCode = reverseLoadLoyaltyCreditsTransactionResult.getStatusCode();
                errorDescription = reverseLoadLoyaltyCreditsTransactionResult.getMessageCode();
                eventResult = "KO";

                sequenceID += 1;
                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                oldStatus = newStatus;

                em.persist(postPaidTransactionEventBean);

                throw new FidelityServiceException("Error in reverseLoadLoyaltyCreditsTransaction: " + reverseLoadLoyaltyCreditsTransactionResult.getStatusCode());
            }
            else {

                // Aggiungi una riga per loggare l'esito dell'operazione di storno

                PostPaidLoadLoyaltyCreditsBean reversePostPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean();
                //reversePostPaidLoadLoyaltyCreditsBean.setBalance(reverseLoadLoyaltyCreditsTransactionResult.getBalance());
                //reversePostPaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                //reversePostPaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                reversePostPaidLoadLoyaltyCreditsBean.setCsTransactionID(reverseLoadLoyaltyCreditsTransactionResult.getCsTransactionID());
                //reversePostPaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                //reversePostPaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                reversePostPaidLoadLoyaltyCreditsBean.setMessageCode(reverseLoadLoyaltyCreditsTransactionResult.getMessageCode());
                reversePostPaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                reversePostPaidLoadLoyaltyCreditsBean.setOperationIDReversed(operationIDtoReverse);
                reversePostPaidLoadLoyaltyCreditsBean.setOperationType("REVERSE");
                reversePostPaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                reversePostPaidLoadLoyaltyCreditsBean.setStatusCode(reverseLoadLoyaltyCreditsTransactionResult.getStatusCode());

                reversePostPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);

                revertedPostPaidLoadLoyaltyCreditsBeanList.add(reversePostPaidLoadLoyaltyCreditsBean);

                reversed = true;
            }
        }

        if (reversed) {
            sequenceID += 1;
            PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp,
                    transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

            em.persist(postPaidTransactionEventBean);

        }
        else {
            System.out.println("Storno caricamento punti carte loyalty non necessario");
        }

        oldStatus = newStatus;

        for (PostPaidLoadLoyaltyCreditsBean revertedPostPaidLoadLoyaltyCreditsBean : revertedPostPaidLoadLoyaltyCreditsBeanList) {
            poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(revertedPostPaidLoadLoyaltyCreditsBean);
        }
    }

    private boolean isCreditsLoyaltyLoaded(PostPaidTransactionBean poPTransactionBean) {
        boolean loaded = false;

        for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {
            if (postPaidLoadLoyaltyCreditsBean.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)
                    && (postPaidLoadLoyaltyCreditsBean.getCredits() != null && postPaidLoadLoyaltyCreditsBean.getCredits() > 0)) {
                loaded = true;
                break;
            }
        }
        return loaded;
    }

    private void updateVoucherAndLoyaltyData(PostPaidTransactionBean poPTransactionBean) {
        boolean updated = false;

        if (poPTransactionBean.getPostPaidConsumeVoucherBeanList() != null && !poPTransactionBean.getPostPaidConsumeVoucherBeanList().isEmpty()) {
            for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : poPTransactionBean.getPostPaidConsumeVoucherBeanList()) {

                em.persist(postPaidConsumeVoucherBean);
                updated = true;
                if (postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean() != null && !postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean().isEmpty()) {
                    for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {
                        em.persist(postPaidConsumeVoucherDetailBean);
                        updated = true;
                    }
                }
            }
        }

        if (poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList() != null && !poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().isEmpty()) {
            for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {
                em.persist(postPaidLoadLoyaltyCreditsBean);
                updated = true;
            }
        }

        if (updated) {
            em.merge(poPTransactionBean);
        }
    }
    
    private void receiveCRMEventOffer(PostPaidTransactionBean poPTransactionBean, UserBean userBean, PostPaidRefuelBean crmPostPaidRefuelBean, 
            PostPaidLoadLoyaltyCreditsBean crmPostPaidLoadLoyaltyCredits, String refuelMode) {
    	
    	String crmSfActive="0";
    	try {
    		crmSfActive = parametersService.getParamValue(PARAM_CRM_SF_ACTIVE);
		} catch (ParameterNotFoundException e) {
			e.printStackTrace();
		}
        
        if (poPTransactionBean.getMpTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID)) {
            
            if (!userBean.getUserType().equals(User.USER_TYPE_GUEST)) {
                
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Request CRM Event Offer");
                
                final String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                Date timestamp = poPTransactionBean.getCreationTimestamp();
                String surname = poPTransactionBean.getUserBean().getPersonalDataBean().getLastName();
                //String firstName = poPTransactionBean.getUserBean().getPersonalDataBean().getFirstName();
                String pv = poPTransactionBean.getStationBean().getStationID();
                final Long sourceID = poPTransactionBean.getId();
                final CRMEventSourceType sourceType = CRMEventSourceType.POSTPAID;
                String product = (crmPostPaidRefuelBean != null) ? crmPostPaidRefuelBean.getProductDescription() : "";
                boolean flagPayment = true;
                Integer credits = (crmPostPaidLoadLoyaltyCredits != null) ? crmPostPaidLoadLoyaltyCredits.getCredits() : 0;
                Double refuelQuantity = (crmPostPaidRefuelBean != null) ? crmPostPaidRefuelBean.getFuelQuantity() : 0;
                Double amount = poPTransactionBean.getAmount();
                
                //recupero la modalita di pagamento postpaid
                String paymentMode=PaymentModeUtil.getPaymentModePostpaid(poPTransactionBean);
                
                refuelMode = (crmPostPaidRefuelBean != null) ? crmPostPaidRefuelBean.getRefuelMode() : refuelMode;
                boolean flagNotification = false;
                boolean flagCreditCard = false;
                String cardBrand = null;
                ClusterType cluster = ClusterType.ENIPAY;
                boolean privacyFlag1 = false;
                boolean privacyFlag2 = false;
                
                Set<TermsOfServiceBean> listTermOfService = userBean.getPersonalDataBean().getTermsOfServiceBeanData();
                if (listTermOfService == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "flagPrivacy null");
                }
                else {
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("NOTIFICATION_NEW_1")) {
                            flagNotification = item.getAccepted();
                            break;
                        }
                    }
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                            privacyFlag1 = item.getAccepted();
                            break;
                        }
                    }
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                            privacyFlag2 = item.getAccepted();
                            break;
                        }
                    }
                }
    
                if (userBean.getPaymentMethodTypeCreditCard() != null) {
                    flagCreditCard = true;
                    cardBrand = userBean.getPaymentMethodTypeCreditCard().getBrand();
                }
                
                if (userBean.getVirtualizationCompleted()) {
                    cluster = ClusterType.YOUENI;
                }
                
                //Nuova implementazione sf
                //recupero parametro CRM_SF_ACTIVE per la gestione della nuova chiamata al CRM SF
                //0 disattivato invio sf
                //1 attivo solo invio sf
                //2 attivi entrambi i crm ibm,sf
               
                if(crmSfActive.equalsIgnoreCase("0")||crmSfActive.equalsIgnoreCase("2")){
	                final UserProfile userProfile = UserProfile.getUserProfileForEventRefueling(fiscalCode, timestamp, surname, pv, product, flagPayment, credits, 
	                        refuelQuantity, amount, refuelMode, flagNotification, flagCreditCard, cardBrand, cluster);
	    
	                new Thread(new Runnable() {
	                    
	                    @Override
	                    public void run() {
	                        String crmResponse = crmService.sendNotifyEventOffer(fiscalCode, InteractionPointType.REFUELING, userProfile, sourceID, sourceType);
	    
	                        if (!crmResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
	                            System.err.println("Error in send crm notification (" + crmResponse + ")");
	                        }
	                    }
	                }, "executeBatchGetOffers (ApproveMulticardPopTransactionAction)").start();
                }
                
                if(crmSfActive.equalsIgnoreCase("1")||crmSfActive.equalsIgnoreCase("2")){
                	
                    /*
                     * Mapping campo modalit� operativa per evento di rifornimento pagato con app
                     * 
                     * Fai_da_te    --> Iperself
                     * 
                     */
                    
                    if (refuelMode.equals("Fai_da_te")) {
                        refuelMode = "Iperself";
                    }
                    
                	System.out.println("##################################################");
                	System.out.println("fiscalCode:" + fiscalCode);
                	System.out.println("timestamp:" + timestamp);
                	System.out.println("pv:" +pv );
                	System.out.println("product:" + product);
                	System.out.println("flagPayment:" + flagPayment);
                	System.out.println("credits:" +credits );
                	System.out.println("refuelQuantity:" +refuelQuantity );
                	System.out.println("refuelMode:" + refuelMode);
                	System.out.println("surname:" +surname );
                	System.out.println("getSecurityDataEmail():" +userBean.getPersonalDataBean().getSecurityDataEmail());
                	System.out.println("getBirthDate():" + userBean.getPersonalDataBean().getBirthDate());
                	System.out.println("flagNotification:" + flagNotification);
                	System.out.println("flagCreditCard:" + flagCreditCard);
                	System.out.println("cardBrand:" + cardBrand);
                	System.out.println("cluster.getValue():" +cluster.getValue() );
                	System.out.println("amount:" +amount );
                	System.out.println("paymentMode:" +paymentMode );
                	System.out.println("userBean.getActiveMobilePhone():" +userBean.getActiveMobilePhone() );
                	
                	String panCode="";
                	if(userBean.getVirtualLoyaltyCard()!=null){
                		System.out.println("getPanCode():" +userBean.getVirtualLoyaltyCard().getPanCode() );
                		panCode=userBean.getVirtualLoyaltyCard().getPanCode();
                	}else
                		System.out.println("getPanCode():null");
                	
                	System.out.println("EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue():" + EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue());
                	System.out.println("##################################################");
                	
                	String requestId = new IdGenerator().generateId(16).substring(0, 32);
                	/*:TODO da verificare requestId */
                	final CRMSfNotifyEvent crmSfNotifyEvent= new CRMSfNotifyEvent(requestId, fiscalCode, timestamp, pv, product, flagPayment, 
                			credits, refuelQuantity, refuelMode, null, surname, userBean.getPersonalDataBean().getSecurityDataEmail(), 
                			userBean.getPersonalDataBean().getBirthDate(), flagNotification, flagCreditCard, cardBrand==null?"":cardBrand, 
                			cluster.getValue(), amount, privacyFlag1, privacyFlag2, userBean.getActiveMobilePhone(), panCode, 
                			EVENT_TYPE.ESECUZIONE_RIFORNIMENTO_APP.getValue(), "", "", paymentMode);
	    
	                new Thread(new Runnable() {
	                    
	                    @Override
	                    public void run() {
	                    	NotifyEventResponse crmResponse = crmService.sendNotifySfEventOffer(crmSfNotifyEvent.getRequestId(), crmSfNotifyEvent.getFiscalCode(), crmSfNotifyEvent.getDate(), 
	                        		crmSfNotifyEvent.getStationId(), crmSfNotifyEvent.getProductId(), crmSfNotifyEvent.getPaymentFlag(), crmSfNotifyEvent.getCredits(), 
	                        		crmSfNotifyEvent.getQuantity(), crmSfNotifyEvent.getRefuelMode(), crmSfNotifyEvent.getFirstName(), crmSfNotifyEvent.getLastName(), 
	                        		crmSfNotifyEvent.getEmail(), crmSfNotifyEvent.getBirthDate(), crmSfNotifyEvent.getNotificationFlag(), crmSfNotifyEvent.getPaymentCardFlag(), 
	                        		crmSfNotifyEvent.getBrand(), crmSfNotifyEvent.getCluster(),crmSfNotifyEvent.getAmount(), crmSfNotifyEvent.getPrivacyFlag1(), 
	                        		crmSfNotifyEvent.getPrivacyFlag2(), crmSfNotifyEvent.getMobilePhone(), crmSfNotifyEvent.getLoyaltyCard(), crmSfNotifyEvent.getEventType(), 
	                        		crmSfNotifyEvent.getParameter1(), crmSfNotifyEvent.getParameter2(), crmSfNotifyEvent.getPaymentMode());
	    
	                    	System.out.println("crmResponse succes:" + crmResponse.getSuccess());
		                      System.out.println("crmResponse errorCode:" + crmResponse.getErrorCode());
		                      System.out.println("crmResponse message:" + crmResponse.getMessage());
		                      System.out.println("crmResponse requestId:" + crmResponse.getRequestId());
	                    }
	                }, "executeBatchGetOffersSF (ApproveMulticardPopTransactionAction)").start();
                }
            }
            else {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "No request CRM Event Offer. UserType guest");
            }
        }
        else {        
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "No request CRM Event Offer. Transaction finalStatus is not PAID");
        }
    }
    
    
    private String convertCodeToStatusCode(String code) {
        
        // Gestione errori in movimentazione
        
        String statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO;
        
        if (code.equals(CodeEnum.SUCCESS.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_PARAMETERS;
        }
        if (code.equals(CodeEnum.INVALID_PARAMETERS.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_PARAMETERS;
        }
        if (code.equals(CodeEnum.INVALID_OPERATION.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_OPERATION;
        }
        if (code.equals(CodeEnum.DPAN_NOT_FOUND.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_DPAN_NOT_FOUND;
        }
        if (code.equals(CodeEnum.OTP_NOT_FOUND.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_OTP_NOT_FOUND;
        }
        if (code.equals(CodeEnum.TOTP_NOT_FOUND.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_TOTP_NOT_FOUND;
        }
        if (code.equals(CodeEnum.INVALID_TOTP_OR_DPAN.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_TOTP_OR_DPAN;
        }
        if (code.equals(CodeEnum.INVALID_LOYALTY_CARD.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_LOYALTY_CARD;
        }
        if (code.equals(CodeEnum.INVALID_PARTNER_TYPE.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_PARTNER_TYPE;
        }
        if (code.equals(CodeEnum.CARD_ALREADY_ENABLED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_ALREADY_ENABLED;
        }
        if (code.equals(CodeEnum.MSGREASONCODE_MANDATORY.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_MSGREASONCODE_MANDATORY;
        }
        if (code.equals(CodeEnum.PAN_NOT_FOUND.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PAN_NOT_FOUND;
        }
        if (code.equals(CodeEnum.PAN_ALREADY_USED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PAN_ALREADY_USED;
        }
        if (code.equals(CodeEnum.EMAIL_ALREADY_USED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_EMAIL_ALREADY_USED;
        }
        if (code.equals(CodeEnum.PAYMENT_NOT_FOUND_FOR_OPERATION.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_NOT_FOUND_FOR_OPERATION;
        }
        if (code.equals(CodeEnum.USER_NOT_FOUND.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_USER_NOT_FOUND;
        }
        if (code.equals(CodeEnum.PAYMENT_WITH_ANOTHER_DEVICE.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_WITH_ANOTHER_DEVICE;
        }
        if (code.equals(CodeEnum.INVALID_CRIPTOGRAM.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_CRIPTOGRAM;
        }
        if (code.equals(CodeEnum.AUTHORIZATION_NOT_FOUND.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZATION_NOT_FOUND;
        }
        if (code.equals(CodeEnum.PAYMENT_NOT_FOUND_FOR_REVERSAL.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_NOT_FOUND_FOR_REVERSAL;
        }
        if (code.equals(CodeEnum.AUTHORIZED_0.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_0;
        }
        if (code.equals(CodeEnum.AUTHORIZED_1.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_1;
        }
        if (code.equals(CodeEnum.AUTHORIZED_PARTIAL_AMOUNT_2.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_PARTIAL_AMOUNT_2;
        }
        if (code.equals(CodeEnum.AUTHORIZED_3.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_3;
        }
        if (code.equals(CodeEnum.AUTHORIZED_5.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_5;
        }
        if (code.equals(CodeEnum.AUTHORIZED_PARTIAL_AMOUNT_6.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_PARTIAL_AMOUNT_6;
        }
        if (code.equals(CodeEnum.AUTHORIZED_7.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_7;
        }
        if (code.equals(CodeEnum.AUTHORIZED_10.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_10;
        }
        if (code.equals(CodeEnum.AUTHORIZED_80.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_80;
        }
        if (code.equals(CodeEnum.AUTHORIZED_81.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZED_81;
        }
        if (code.equals(CodeEnum.AUTHORIZATION_DENIED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHORIZATION_DENIED;
        }
        if (code.equals(CodeEnum.CARD_EXPIRED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_EXPIRED;
        }
        if (code.equals(CodeEnum.CONTACT_CALL_CENTER.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CONTACT_CALL_CENTER;
        }
        if (code.equals(CodeEnum.CARD_WITH_RESTRICTIONS.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_WITH_RESTRICTIONS;
        }
        if (code.equals(CodeEnum.PIN_ATTEMPTS_EXPIRED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PIN_ATTEMPTS_EXPIRED;
        }
        if (code.equals(CodeEnum.CONTACT_ISSUER.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CONTACT_ISSUER;
        }
        if (code.equals(CodeEnum.INVALID_OPERATOR.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_OPERATOR;
        }
        if (code.equals(CodeEnum.INVALID_AMOUNT.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_AMOUNT;
        }
        if (code.equals(CodeEnum.INVALID_CARD_NUMBER.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_CARD_NUMBER;
        }
        if (code.equals(CodeEnum.PIN_REQUIRED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PIN_REQUIRED;
        }
        if (code.equals(CodeEnum.INVALID_ACCOUNT.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_ACCOUNT;
        }
        if (code.equals(CodeEnum.UNSUPPORTED_FUNCTION.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_UNSUPPORTED_FUNCTION;
        }
        if (code.equals(CodeEnum.MAX_LIMIT_REACHED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_MAX_LIMIT_REACHED;
        }
        if (code.equals(CodeEnum.WRONG_PIN.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_WRONG_PIN;
        }
        if (code.equals(CodeEnum.CARD_NOT_FOUND.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_NOT_FOUND;
        }
        if (code.equals(CodeEnum.TRANSACTION_NOT_ENABLED_TO_HOLDER.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_TRANSACTION_NOT_ENABLED_TO_HOLDER;
        }
        if (code.equals(CodeEnum.TRANSACTION_NOT_ENABLED_TO_POS.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_TRANSACTION_NOT_ENABLED_TO_POS;
        }
        if (code.equals(CodeEnum.PLAFOND_EXCEEDED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PLAFOND_EXCEEDED;
        }
        if (code.equals(CodeEnum.LIMITATION_TIME_EXCEEDED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_LIMITATION_TIME_EXCEEDED;
        }
        if (code.equals(CodeEnum.INACTIVE_CARD.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INACTIVE_CARD;
        }
        if (code.equals(CodeEnum.INVALID_PIN_BLOCK.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_PIN_BLOCK;
        }
        if (code.equals(CodeEnum.WRONG_PIN_LENGTH.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_WRONG_PIN_LENGTH;
        }
        if (code.equals(CodeEnum.PIN_KEY_ERROR.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PIN_KEY_ERROR;
        }
        if (code.equals(CodeEnum.OUTDOOR_TRANSACTION_FORBIDDEN.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_OUTDOOR_TRANSACTION_FORBIDDEN;
        }
        if (code.equals(CodeEnum.PUMP_NOT_CONNECTED_TRANSACTION_FORBIDDEN.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PUMP_NOT_CONNECTED_TRANSACTION_FORBIDDEN;
        }
        if (code.equals(CodeEnum.WRONG_MILEAGE.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_WRONG_MILEAGE;
        }
        if (code.equals(CodeEnum.PARTNER_NOT_ENABLED_IN_PV.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PARTNER_NOT_ENABLED_IN_PV;
        }
        if (code.equals(CodeEnum.PV_NOT_ENABLED_FOR_CARD.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PV_NOT_ENABLED_FOR_CARD;
        }
        if (code.equals(CodeEnum.DAY_NOT_ENABLED_FOR_CARD.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_DAY_NOT_ENABLED_FOR_CARD;
        }
        if (code.equals(CodeEnum.TIME_NOT_ENABLED_FOR_CARD.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_TIME_NOT_ENABLED_FOR_CARD;
        }
        if (code.equals(CodeEnum.MANUAL_TRANSACTION_FORBIDDEN.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_MANUAL_TRANSACTION_FORBIDDEN;
        }
        if (code.equals(CodeEnum.IPERSELF_FORBIDDEN.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_IPERSELF_FORBIDDEN;
        }
        if (code.equals(CodeEnum.FAIDATE_FORBIDDEN.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_FAIDATE_FORBIDDEN;
        }
        if (code.equals(CodeEnum.OVERRIDE_FORBIDDEN.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_OVERRIDE_FORBIDDEN;
        }
        if (code.equals(CodeEnum.BOOKING_REQUIRED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_BOOKING_REQUIRED;
        }
        if (code.equals(CodeEnum.INVALID_BOOKING_FOR_PRODUCT.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_BOOKING_FOR_PRODUCT;
        }
        if (code.equals(CodeEnum.INVALID_BOOKING_FOR_PV.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_BOOKING_FOR_PV;
        }
        if (code.equals(CodeEnum.INVALID_BOOKING_FOR_DAY.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_BOOKING_FOR_DAY;
        }
        if (code.equals(CodeEnum.DRIVER_BLOCKED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_DRIVER_BLOCKED;
        }
        if (code.equals(CodeEnum.PRODUCT_NOT_REGISTERED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PRODUCT_NOT_REGISTERED;
        }
        if (code.equals(CodeEnum.INVALID_PRICE.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_PRICE;
        }
        if (code.equals(CodeEnum.UNKNOWN_POS.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_UNKNOWN_POS;
        }
        if (code.equals(CodeEnum.BLOCKED_POS.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_BLOCKED_POS;
        }
        if (code.equals(CodeEnum.CARD_NOT_ENABLED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_NOT_ENABLED;
        }
        if (code.equals(CodeEnum.CARD_BLOCKED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_BLOCKED;
        }
        if (code.equals(CodeEnum.BLOCKED_CLIENT.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_BLOCKED_CLIENT;
        }
        if (code.equals(CodeEnum.BLOCKED_CARD_ACCOUNT.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_BLOCKED_CARD_ACCOUNT;
        }
        if (code.equals(CodeEnum.BLOCKED_ISSUER.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_BLOCKED_ISSUER;
        }
        if (code.equals(CodeEnum.PRODUCT_NOT_ENABLED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PRODUCT_NOT_ENABLED;
        }
        if (code.equals(CodeEnum.PIN_ATTEMPTS_EXHAUSTED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PIN_ATTEMPTS_EXHAUSTED;
        }
        if (code.equals(CodeEnum.INVALID_AMOUNT_FOR_COMPANY.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_AMOUNT_FOR_COMPANY;
        }
        if (code.equals(CodeEnum.CARD_NOT_ENABLED_0.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_NOT_ENABLED_0;
        }
        if (code.equals(CodeEnum.CARD_NOT_ENABLED_1.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_NOT_ENABLED_1;
        }
        if (code.equals(CodeEnum.CARD_NOT_ENABLED_2.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_NOT_ENABLED_2;
        }
        if (code.equals(CodeEnum.PRODUCT_PLAFOND_EXCEEDED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PRODUCT_PLAFOND_EXCEEDED;
        }
        if (code.equals(CodeEnum.INSUFFICIENT_CREDIT.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INSUFFICIENT_CREDIT;
        }
        if (code.equals(CodeEnum.PLAFOND_EXCEEDED_1.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PLAFOND_EXCEEDED_1;
        }
        if (code.equals(CodeEnum.EXPIRED_CARD.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_EXPIRED_CARD;
        }
        if (code.equals(CodeEnum.EXPIRED_CARD_PICKUP.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_EXPIRED_CARD_PICKUP;
        }
        if (code.equals(CodeEnum.SUSPECTED_FRAUD.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_SUSPECTED_FRAUD;
        }
        if (code.equals(CodeEnum.CALL_ACQUIRER.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CALL_ACQUIRER;
        }
        if (code.equals(CodeEnum.CARD_WITH_RESTRICTIONS_0.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_WITH_RESTRICTIONS_0;
        }
        if (code.equals(CodeEnum.PIN_ATTEMPTS_EXHAUSTED_PICKUP.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PIN_ATTEMPTS_EXHAUSTED_PICKUP;
        }
        if (code.equals(CodeEnum.LOST_CARD.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_LOST_CARD;
        }
        if (code.equals(CodeEnum.STOLEN_CARD.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_STOLEN_CARD;
        }
        if (code.equals(CodeEnum.WRONG_FORMAT.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_WRONG_FORMAT;
        }
        if (code.equals(CodeEnum.CLOSURE_IN_PROGRESS.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CLOSURE_IN_PROGRESS;
        }
        if (code.equals(CodeEnum.CENTRAL_SYSTEM_NOT_WORKING.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CENTRAL_SYSTEM_NOT_WORKING;
        }
        if (code.equals(CodeEnum.SYSTEM_MALFUNCTION.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_SYSTEM_MALFUNCTION;
        }
        if (code.equals(CodeEnum.TIME_OUT_CARD_EMITTER.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_TIME_OUT_CARD_EMITTER;
        }
        if (code.equals(CodeEnum.CARD_EMITTER_UNREACHABLE.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_EMITTER_UNREACHABLE;
        }
        if (code.equals(CodeEnum.WRONG_MAC.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_WRONG_MAC;
        }
        if (code.equals(CodeEnum.ERROR_SINC_MAC_KEY.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_ERROR_SINC_MAC_KEY;
        }
        if (code.equals(CodeEnum.DECRYPTION_ERROR.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_DECRYPTION_ERROR;
        }
        if (code.equals(CodeEnum.SECURITY_ERROR.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_SECURITY_ERROR;
        }
        if (code.equals(CodeEnum.MESSAGE_OUT_OF_SEQUENCE.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_MESSAGE_OUT_OF_SEQUENCE;
        }
        if (code.equals(CodeEnum.WRONG_OPERATOR.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_WRONG_OPERATOR;
        }
        if (code.equals(CodeEnum.WRONG_COMPANY.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_WRONG_COMPANY;
        }
        
        /*
        if (code.equals(CodeEnum.PAYMENT_FAILED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_ERROR;
        }
        else if (code.equals(CodeEnum.INVALID_PARAMETERS.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_WRONG_INPUT_PARAMETERS;
        }
        else if (code.equals(CodeEnum.INVALID_PARTNER_TYPE .getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_PARTNER_TYPE;
        }
        else if (code.equals(CodeEnum.PAN_NOT_FOUND.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PAN_NOT_FOUND;
        }
        else if (code.equals(CodeEnum.PAYMENT_WITH_ANOTHER_DEVICE.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_ANOTHER_DEVICE_USED;
        }
        else if (code.equals(CodeEnum.INVALID_CRIPTOGRAM.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_CRYPTOGRAM_PARAMETERS;
        }
        else if (code.equals(CodeEnum.PIN_NOT_VERIFIED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PIN_NOT_VERIFIED;
        }
        else if (code.equals(CodeEnum.CARD_EXPIRED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_CARD_EXPIRED;
        }
        else if (code.equals(CodeEnum.PAYMENT_FAILED_MERCHANT.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_FAILED_MERCHANT;
        }
        else if (code.equals(CodeEnum.PAYMENT_FAILED_SECURITY.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_FAILED_SECURITY;
        }
        else if (code.equals(CodeEnum.PAYMENT_FAILED_TERMINAL.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_FAILED_TERMINAL;
        }
        else if (code.equals(CodeEnum.PAYMENT_FAILED_CUSTOMER.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PAYMENT_FAILED_CUSTOMER;
        }
        else if (code.equals(CodeEnum.INSUFFICIENT_CREDIT.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INSUFFICIENT_CREDIT;
        }
        else if (code.equals(CodeEnum.AUTHCODE_NOT_FOUND.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AUTHCODE_NOT_FOUND;
        }
        else if (code.equals(CodeEnum.AMOUNT_GREATER_THAN_AUTHORIZED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AMOUNT_GREATER_THAN_AUTHORIZED;
        }
        else if (code.equals(CodeEnum.AMOUNT_GREATER_THAN_SETTLED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_AMOUNT_GREATER_THAN_SETTLED;
        }
        else if (code.equals(CodeEnum.MAX_LIMIT_REACHED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_MAX_LIMIT_REACHED;
        }
        else if (code.equals(CodeEnum.REVERSAL_FAILED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_REVERSAL_FAILED;
        }
        else if (code.equals(CodeEnum.INVALID_AMOUNT.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_AMOUNT;
        }
        else if (code.equals(CodeEnum.INVALID_OPERATION.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_INVALID_OPERATION;
        }
        else if (code.equals(CodeEnum.PRODUCT_NOT_ENABLED.getValue())) {
            statusCode = ResponseHelper.PP_TRANSACTION_APPROVE_MULTICARD_PRODUCT_NOT_ENABLED;
        }
        */
        
        return statusCode;
    }
}
