package com.techedge.mp.core.actions.poptransaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidEnableShopTransactionResponse;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPTransactionEnableShopAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public PoPTransactionEnableShopAction() {
    }

    
    public PostPaidEnableShopTransactionResponse execute(
    					String requestID,
						String mpTransactionID) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		PostPaidEnableShopTransactionResponse poPEnableShopTransactionResponse = new PostPaidEnableShopTransactionResponse();
    		
//    		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "ServerName: " + serverName );
//    		
//    		CreateRefuelResponse createRefuelResponse = new CreateRefuelResponse();
//    		
//    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);
//		    
//    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
//    			
//    			// Ticket non valido
//    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket" );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
//    			return createRefuelResponse;
//    		}
//    		
//    		UserBean userBean = ticketBean.getUser();
//    		if ( userBean == null ) {
//    			
//    			// Ticket non valido
//    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found" );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
//    			return createRefuelResponse;
//    		}
    		
    		
//    		// Verifica lo stato dell'utente
//    		Integer userStatus = userBean.getUserStatus();
//    		if ( userStatus != User.USER_STATUS_VERIFIED ) {
//    			
//    			// Un utente che si trova in questo stato non pu� invocare questo servizio
//    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_UNAUTHORIZED);
//    			return createRefuelResponse;
//    		}
    		
    		
    		// Controlla se l'utente ha gi� una transazione attiva
//    		List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);
//		    
//		    if ( !transactionBeanList.isEmpty() ) {
//    			
//    			// Esiste una transazione associata all'utente non ancora completata
//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction found" );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
//    			return createRefuelResponse;
//    		}
		    
		    
//		    // Se il medodo di pagamento non � specificato utilizza quello di default
//		    PaymentInfoBean paymentInfoBean = null;
//		    if ( paymentMethodId == null && paymentMethodType == null ) {
//		    	
//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found" );
//				
//		    	paymentInfoBean = userBean.findDefaultPaymentInfoBean();
//		    }
//		    else {
//
//		    	paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
//		    }
//		    
//		    // Verifica che il metodo di pagamento selezionato sia in uno stato valido
//		    if ( paymentInfoBean == null ) {
//		    	
//		    	// Il metodo di pagamento selezionato non esiste
//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data" );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
//    			return createRefuelResponse;
//		    }
//		    
//		    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: " + paymentMethodType );
//		    
//		    if ( paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_BLOCKED     ||
//		    	 paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_ERROR       ||
//		    	 paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING     ) {
//		    	
//		    	// Il metodo di pagamento selezionato non pu� essere utilizzato
//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus() );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
//    			return createRefuelResponse;
//		    }
//		    
//    		if ( !encodedPin.equals(paymentInfoBean.getPin()) ) {
//    			
//    			// Il pin inserito non � valido
//    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin" );
//				
//    			// Si sottrae uno al numero di tentativi residui
//    			Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
//    			if ( pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0 ) {
//    				pinCheckAttemptsLeft--;
//    			}
//    			else {
//    				pinCheckAttemptsLeft = 0;
//    			}
//    			
//    			if ( pinCheckAttemptsLeft == 0 ) {
//    				
//    				// Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
//    				paymentInfoBean.setDefaultMethod(false);
//    				paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
//    			}
//    			
//    			
//    			paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);
//    			
//    			em.merge(paymentInfoBean);
//    			
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_WRONG);
//    			return createRefuelResponse;
//    		}
//    		
//    		
//    		// L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
//    		paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);
//    		
//    		em.merge(paymentInfoBean);
    		

    		   		
    		
    		PostPaidTransactionBean poPTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, mpTransactionID);
    		if ( poPTransactionBean == null ) {
    			// mpTransactionID inserito non corrisponde a nessuna stazione valida
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No postPaid transaction dor mpTransactioId " + mpTransactionID );
	   			userTransaction.commit();
       			poPEnableShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_NOT_RECOGNIZED);
    			return poPEnableShopTransactionResponse;
    		}
    		
    		
    		if (poPTransactionBean.getNotificationCreated()){
    			//TODO occorre inserire la verifica se la transazione � gi� abilitata. Cosa Fare?
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Transaction postPaid already enabled - no change" + mpTransactionID );
				userTransaction.commit();
    			poPEnableShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_SUCCESS);
    			return poPEnableShopTransactionResponse;
    		}
    		
    		
    		
    		
    		
    		Set <PostPaidTransactionEventBean> postPayedTransactionEventBeanList = poPTransactionBean.getPostPaidTransactionEventBean();
    		// Crea il nuovo stato
    		Integer sequenceID = 0;
    		String oldStatus= "";
    		
    		for( PostPaidTransactionEventBean postPayedTransactionEventBean : postPayedTransactionEventBeanList ) {
    			Integer transactionEventBeanSequenceID = postPayedTransactionEventBean.getSequenceID();
    			if ( sequenceID < transactionEventBeanSequenceID ) {
    				sequenceID = transactionEventBeanSequenceID;
    				oldStatus = postPayedTransactionEventBean.getNewState();
    			}
    			
    		}
    		sequenceID = sequenceID + 1;
    		
    		
    		
    		Date now = new Date();
    		Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());

    		// Aggiorno la transazione con i dati ricevuti in input
    		poPTransactionBean.setNotificationCreated(true);
    		
    		//salvataggio dell'evento;
    		PostPaidTransactionEventBean postPaidTransactionEventBean = new PostPaidTransactionEventBean();
    		postPaidTransactionEventBean.setSequenceID(sequenceID);
    		postPaidTransactionEventBean.setStateType(StatusHelper.POST_PAID_EVENT_TYPE_STANDARD);
    		postPaidTransactionEventBean.setRequestID(requestID);
    		postPaidTransactionEventBean.setEventTimestamp(creationTimestamp);
    		postPaidTransactionEventBean.setEvent(StatusHelper.POST_PAID_EVENT_GFG_ENABLE ); 		
    		postPaidTransactionEventBean.setTransactionBean(poPTransactionBean);
    		postPaidTransactionEventBean.setResult("OK");
    		postPaidTransactionEventBean.setNewState(StatusHelper.POST_PAID_STATUS_PAY_ABLE);
    		postPaidTransactionEventBean.setOldState(oldStatus);
    		
    	
    		em.persist(poPTransactionBean);
    		em.persist(postPaidTransactionEventBean);
    		

    		poPEnableShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_SUCCESS);
    		

    		userTransaction.commit();
    		
    		return poPEnableShopTransactionResponse;
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED Post Payed transaction creation with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
