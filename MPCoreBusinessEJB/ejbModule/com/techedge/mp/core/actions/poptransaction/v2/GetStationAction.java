package com.techedge.mp.core.actions.poptransaction.v2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.TransactionService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.CashInfo;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ExtendedPumpInfo;
import com.techedge.mp.core.business.interfaces.GetStationInfo;
import com.techedge.mp.core.business.interfaces.GetStationResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.CoordsHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.SourceDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.StationDetail;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.SendMPTransactionResultMessageResponse;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class GetStationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    private boolean       isNotAvailable = false;

    public GetStationAction() {}

    public GetStationResponse execute(String requestID, String ticketID, Double userPositionLatitude, Double userPositionLongitude, String stationID, Boolean clearTransaction, Boolean refresh,
             Double innerRangeThreshold, Double outerRangeThreshold, Boolean rangeThresholdBlocking, TransactionService transactionService, ForecourtInfoServiceRemote forecourtInfoService,
             ForecourtPostPaidServiceRemote forecourtPPService, UserCategoryService userCategoryService)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            isNotAvailable = false;

            GetStationResponse getStationsResponse = new GetStationResponse();

            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();
                getStationsResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_INVALID_TICKET);
                return getStationsResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                getStationsResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_INVALID_TICKET);
                return getStationsResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to get stations in status " + userStatus);

                userTransaction.commit();

                getStationsResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_UNAUTHORIZED);
                return getStationsResponse;
            }
            
            boolean useBusiness = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.BUSINESS.getCode());
            
            
            
            if (userPositionLatitude != null && userPositionLongitude!= null) {
                // Ricerca gli impianti in base alla distanza dalla posizione dell'utente

                /* Vecchia versione con singolo range di ricerca
                List<StationBean> proxStationBeanList = QueryRepository.getAllActiveStationBeans(em);

                for (StationBean stationBean : proxStationBeanList) {

                    // Controlla se l'impianto si trova all'interno del range di ricerca

                    double distance = CoordsHelper.calculateDistance(userPositionLatitude, userPositionLongitude, stationBean.getLatitude(), stationBean.getLongitude());

                    //System.out.println("station: " + stationBean.getStationID() + " - distance: " + distance);

                    if (distance < innerRangeThreshold) {

                        // Trovato un impianto in prossimit� della posizione dell'utente

                        GetStationInfo stationInfo = this.retrieveStationInfoFromStationBean(forecourtInfoService, stationBean);

                        if (stationInfo == null) {

                            // Il GFG non ha restituito le informazioni sull'impianto
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Dati dell'impianto non restituiti dal GFG");
                        }
                        else {

                            // Trasformazione delle coordinate della stazione da String a Double
                            Double stationLatitude = null;
                            Double stationLongitude = null;
                            try {
                                stationLatitude = new Double(stationInfo.getLatitude());
                                stationLongitude = new Double(stationInfo.getLongitude());
                            }
                            catch (Exception ex) {

                                String message = "Errore trasformazione coordinate stazione da string a double: " + ex.getMessage();
                                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

                                stationLatitude = 0.0;
                                stationLongitude = 0.0;
                            }

                            // Aggiornamento delle informazioni sulla stazione sul backend
                            transactionService.refreshStationInfo(ticketID, stationInfo.getStationId(), stationInfo.getAddress(), stationInfo.getCity(), stationInfo.getCountry(),
                                    stationInfo.getProvince(), stationLatitude, stationLongitude);

                            //System.out.println("Refresh station info response: " + result);

                            // Se la rilevazione avviene tramite beacon allora l'impianto � sempre in range

                            getStationsResponse.setOutOfRange(false);
                            //this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Rilevazione tramite GPS -> outOfRange false" );

                            getStationsResponse.getStationInfoList().add(stationInfo);
                        }
                    }
                }
                */
                
                // Nuova versione con doppio range di ricerca
                List<StationBean> innerRangeStationBeanList = new ArrayList<StationBean>(0);
                List<StationBean> outerRangeStationBeanList = new ArrayList<StationBean>(0);
                
                List<StationBean> proxStationBeanList = QueryRepository.getAllActiveStationBeans(em);
                
                //System.out.println("innerRangeThreshold: " + innerRangeThreshold);
                //System.out.println("outerRangeThreshold: " + outerRangeThreshold);
                
                for (StationBean stationBean : proxStationBeanList) {

                    // Controlla se l'impianto si trova all'interno del range di ricerca

                    double distance = CoordsHelper.calculateDistance(userPositionLatitude, userPositionLongitude, stationBean.getLatitude(), stationBean.getLongitude());

                    //System.out.println("station: " + stationBean.getStationID() + " - distance: " + distance);

                    if (distance < innerRangeThreshold) {
                        // Inserisci lo stationBean nella lista dei PV della prima fascia
                        //System.out.println("PV " + stationBean.getStationID() + " inserito nella prima fascia");
                        innerRangeStationBeanList.add(stationBean);
                    }
                    else {
                        
                        if (distance < outerRangeThreshold) {
                            // Inserisci lo stationBean nella lista dei PV della seconda fascia
                            //System.out.println("PV " + stationBean.getStationID() + " inserito nella seconda fascia");
                            outerRangeStationBeanList.add(stationBean);
                        }
                        else {
                            // Il PV si trova oltre il secondo range e non deve essere restituito
                            //System.out.println("PV " + stationBean.getStationID() + " scartato");
                        }
                    }
                }
                
                getStationsResponse.setOutOfRange(true);
                
                // Elaborazioni PV innerRange
                if(!innerRangeStationBeanList.isEmpty()) {
                    
                    getStationsResponse.setOutOfRange(false);
                    
                    for(StationBean stationBean : innerRangeStationBeanList) {

                        // Recupero dei dati del PV dal gestionale

                        GetStationInfo stationInfo = this.retrieveStationInfoFromStationBean(forecourtInfoService, stationBean, refresh);

                        if (stationInfo == null) {

                            // Il GFG non ha restituito le informazioni sull'impianto
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Dati dell'impianto non restituiti dal GFG");
                        }
                        else {

                            // Trasformazione delle coordinate della stazione da String a Double
                            Double stationLatitude = null;
                            Double stationLongitude = null;
                            try {
                                stationLatitude = new Double(stationInfo.getLatitude());
                                stationLongitude = new Double(stationInfo.getLongitude());
                            }
                            catch (Exception ex) {

                                String message = "Errore trasformazione coordinate stazione da string a double: " + ex.getMessage();
                                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

                                stationLatitude = 0.0;
                                stationLongitude = 0.0;
                            }

                            // Aggiornamento delle informazioni sulla stazione sul backend
                            transactionService.refreshStationInfo(ticketID, stationInfo.getStationId(), stationInfo.getAddress(), stationInfo.getCity(), stationInfo.getCountry(),
                                    stationInfo.getProvince(), stationLatitude, stationLongitude);

                            //System.out.println("Refresh station info response: " + result);

                            // Se la rilevazione avviene tramite beacon allora l'impianto � sempre in range

                            getStationsResponse.getStationInfoList().add(stationInfo);
                        }
                    }
                }
                
                if (getStationsResponse.getStationInfoList().isEmpty()) {
                    
                    getStationsResponse.setOutOfRange(true);
                    
                    // Elaborazioni PV outerRange
                    if(!outerRangeStationBeanList.isEmpty()) {
                        
                        for(StationBean stationBean : outerRangeStationBeanList) {

                            // Recupero dei dati del PV dal gestionale

                            GetStationInfo stationInfo = this.retrieveStationInfoFromStationBean(forecourtInfoService, stationBean, refresh);

                            if (stationInfo == null) {

                                // Il GFG non ha restituito le informazioni sull'impianto
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Dati dell'impianto non restituiti dal GFG");
                            }
                            else {

                                // Trasformazione delle coordinate della stazione da String a Double
                                Double stationLatitude = null;
                                Double stationLongitude = null;
                                try {
                                    stationLatitude = new Double(stationInfo.getLatitude());
                                    stationLongitude = new Double(stationInfo.getLongitude());
                                }
                                catch (Exception ex) {

                                    String message = "Errore trasformazione coordinate stazione da string a double: " + ex.getMessage();
                                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

                                    stationLatitude = 0.0;
                                    stationLongitude = 0.0;
                                }

                                // Aggiornamento delle informazioni sulla stazione sul backend
                                transactionService.refreshStationInfo(ticketID, stationInfo.getStationId(), stationInfo.getAddress(), stationInfo.getCity(), stationInfo.getCountry(),
                                        stationInfo.getProvince(), stationLatitude, stationLongitude);

                                //System.out.println("Refresh station info response: " + result);

                                // Se la rilevazione avviene tramite beacon allora l'impianto � sempre in range

                                getStationsResponse.getStationInfoList().add(stationInfo);
                            }
                        }
                    }
                }
                

                if (getStationsResponse.getStationInfoList().isEmpty()) {

                    // Non � stato rilevato nessun impianto in prossimit� dell'utente
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Nessun impianto rilevato in prossimit� dell'utente");

                    userTransaction.commit();

                    if (isNotAvailable) {
                        getStationsResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_STATION_NOT_AVAILABLE);
                    }
                    else {
                        getStationsResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_NOT_FOUND);
                    }

                    return getStationsResponse;
                }                
            }
            else {

                StationBean stationBean = QueryRepository.findActiveStationBeanById(em, stationID);

                if (stationBean == null) {

                    // Non � stata trovato nessun impianto associato al codice beacon inserito
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Nessun impianto associato al stationID inserito");

                    userTransaction.commit();

                    getStationsResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_ID_NOT_FOUND);
                    return getStationsResponse;
                }
                else {
                    
                    if (useBusiness && !stationBean.getBusinessActive()) {
                        
                        // Non � stata trovato nessun impianto associato al codice beacon inserito
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "L'impianto non � utilizzabile da utenti di tipo business");

                        userTransaction.commit();

                        getStationsResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_BUSINESS_NOT_ACTIVE);
                        return getStationsResponse;
                    }
                    

                    GetStationInfo stationInfo = this.retrieveStationInfoFromStationBean(forecourtInfoService, stationBean, refresh);

                    if (stationInfo == null) {

                        // Il GFG non ha restituito le informazioni sull'impianto
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Dati dell'impianto non restituiti dal GFG");

                        userTransaction.commit();

                        getStationsResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_NOT_FOUND);
                        return getStationsResponse;
                    }
                    else {

                        // Trasformazione delle coordinate della stazione da String a Double
                        Double stationLatitude = null;
                        Double stationLongitude = null;
                        try {
                            stationLatitude = new Double(stationInfo.getLatitude());
                            stationLongitude = new Double(stationInfo.getLongitude());
                        }
                        catch (Exception ex) {

                            String message = "Errore trasformazione coordinate stazione da string a double: " + ex.getMessage();
                            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

                            stationLatitude = 0.0;
                            stationLongitude = 0.0;
                        }

                        // Aggiornamento delle informazioni sulla stazione sul backend
                        transactionService.refreshStationInfo(ticketID, stationInfo.getStationId(), stationInfo.getAddress(), stationInfo.getCity(), stationInfo.getCountry(),
                                stationInfo.getProvince(), stationLatitude, stationLongitude);

                        //System.out.println("Refresh station info response: " + result);

                        // Impostazione del flag outOfRange
                        if (userPositionLatitude == null || userPositionLongitude == null) {

                            // Posizione utente non rilevata
                            getStationsResponse.setOutOfRange(false);

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                                    "Beacon rilevato, Posizione utente non rilevata -> outOfRange false");
                        }
                        else {

                            double distance = CoordsHelper.calculateDistance(userPositionLatitude, userPositionLongitude, stationLatitude, stationLongitude);

                            System.out.println("Distanza in metri: " + distance);

                            if (distance > innerRangeThreshold) {
                                getStationsResponse.setOutOfRange(true);
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                                        "Beacon rilevato, Posizione oltre limite -> outOfRange true");
                            }
                            else {
                                getStationsResponse.setOutOfRange(false);
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                                        "Beacon rilevato, Posizione non oltre limite -> outOfRange false");
                            }
                        }

                        if (rangeThresholdBlocking == true) {

                            System.out.println("Blocco utenti non in range attivo");

                            if (getStationsResponse.getOutOfRange() == true) {

                                System.out.println("user not in range");

                                // L'utente non si trova in prossimit� dell'impianto
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                                        "L'utente non si trova in prossimit� dell'impianto");

                                userTransaction.commit();

                                getStationsResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_USER_NOT_IN_RANGE);
                                return getStationsResponse;
                            }
                            else {

                                System.out.println("user not in range");
                            }
                        }
                        else {

                            System.out.println("Blocco utenti non in range non attivo");
                        }

                        getStationsResponse.getStationInfoList().add(stationInfo);
                        
                        if (clearTransaction != null && clearTransaction == Boolean.TRUE) {
                            
                            // Invio notifica UNPAID per annullare le eventuali transazioni associate all'utente sul PV
                            System.out.println("Passaggio da postpaid a shop -> cancellazione eventuali transazioni pending associate all'utente");
                            
                            // Ricerca transazioni ONHOLD associate all'utente sul PV
                            List<PostPaidTransactionBean> pendingPostPaidTransactionBeanList = QueryRepository.findPostPaidTransactionsOnHoldByUserBeanAndStationBean(em, userBean, stationBean);
                            
                            if (!pendingPostPaidTransactionBeanList.isEmpty()) {
                            
                                for(PostPaidTransactionBean postPaidtransactionBean : pendingPostPaidTransactionBeanList) {
                                    
                                    System.out.println("Trovata transazione " + postPaidtransactionBean.getMpTransactionID() +" in stato onhold associata all'utente");
                                    
                                    // Invio notifica al gestionale
                                    
                                    String notifyResponse = this.notifyResponse(requestID, stationBean.getStationID(), postPaidtransactionBean.getMpTransactionID(),
                                            postPaidtransactionBean.getSrcTransactionID(), StatusHelper.POST_PAID_FINAL_STATUS_UNPAID, forecourtPPService);
    
                                    /*
                                    if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                                            || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                                            || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)) {
    
                                        poPTransactionBean.setNotificationPaid(true);
    
                                    }
                                    else {
                                        poPTransactionBean.setToReconcile(true);
                                        poPTransactionBean.setNotificationPaid(false);
                                    }
                                    */
                                
                                    // Modifica stato transazione da ONHOLD a CANCELLED
                                    postPaidtransactionBean.setNotificationPaid(Boolean.TRUE);
                                    postPaidtransactionBean.setNotificationUser(Boolean.TRUE);
                                    postPaidtransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);
                                    
                                    em.merge(postPaidtransactionBean);
                                }
                            }
                        }
                    }
                }
            }

            userTransaction.commit();

            getStationsResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_SUCCESS);
            return getStationsResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED Post Payed get stations with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }

    }

    private GetStationInfo retrieveStationInfoFromStationBean(ForecourtInfoServiceRemote forecourtInfoService, StationBean stationBean, Boolean refresh) {

        GetStationInfo stationInfo = null;

        String getStationPumpRequestId = String.valueOf(new Date().getTime());

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "retrieveStationInfoFromStationBean", stationBean.getStationID(), null,
                "Invocazione servizio getStationDetails per stationID: " + stationBean.getStationID());

        GetStationDetailsResponse getStationDetailsResponse = forecourtInfoService.getStationDetails(getStationPumpRequestId, stationBean.getStationID(), null, refresh); // Il servizio ora arriva sempre fino al local forecoure per ottenere i dati reali aggiornati

        // STUB
        /*
        StationStatusMessageResponse stationStatusMessageResponse = new StationStatusMessageResponse();
        stationStatusMessageResponse.setStatusCode("STATION_PUMP_FOUND_200");
        StationDetail stationDetailResp = new StationDetail();
        stationDetailResp.setStationID("00001");
        stationDetailResp.setAddress("via Roma");
        stationDetailResp.setCity("Roma");
        stationDetailResp.setCountry("Italia");
        stationDetailResp.setLatitude("45.11");
        stationDetailResp.setLongitude("12.34");
        stationDetailResp.setProvince("RM");
        stationDetailResp.getPumpDetails().add(new PumpDetail());
        stationDetailResp.getCashDetails().add(new CashDetail());
        stationStatusMessageResponse.setStationDetail(stationDetailResp);
        */
        // END STUB

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "retrieveStationInfoFromStationBean", stationBean.getStationID(), null,
                "Risposta servizio getStationDetails: " + getStationDetailsResponse.getStatusCode());

        if (getStationDetailsResponse.getStatusCode().equals("PUMP_STATUS_NOT_AVAILABLE_500")) {
            isNotAvailable = true;
        }

        if (getStationDetailsResponse.getStatusCode().equals("STATION_PUMP_FOUND_200")) {

            StationDetail stationDetail = getStationDetailsResponse.getStationDetail();

            stationInfo = new GetStationInfo();

            stationInfo.setAddress(stationDetail.getAddress());
            stationInfo.setCity(stationDetail.getCity());
            stationInfo.setCountry(stationDetail.getCountry());
            stationInfo.setLatitude(stationDetail.getLatitude());
            stationInfo.setLongitude(stationDetail.getLongitude());
            stationInfo.setProvince(stationDetail.getProvince());
            stationInfo.setStationId(stationDetail.getStationID());
            stationInfo.setNewAcquirerEnabled(stationBean.getNewAcquirerActive());
            stationInfo.setRefuelingEnabled(stationBean.getRefuelingActive());
            stationInfo.setLoyaltyEnabled(stationBean.getLoyaltyActive());
            stationInfo.setBusinessEnabled(stationBean.getBusinessActive());

            for (SourceDetail sourceDetail : stationDetail.getSourceDetails()) {

                //System.out.println("cash:" + sourceDetail.getSourceID());

                CashInfo cashInfo = new CashInfo();
                cashInfo.setCashId(sourceDetail.getSourceID());
                cashInfo.setNumber(sourceDetail.getSourceNumber());
                stationInfo.getCashList().add(cashInfo);
            }

            for (PumpDetail pumpDetail : stationDetail.getPumpDetails()) {

                //System.out.println("pump:" + pumpDetail.getPumpID());

                ExtendedPumpInfo pumpInfo = new ExtendedPumpInfo();
                pumpInfo.setPumpId(pumpDetail.getPumpID());
                pumpInfo.setNumber(pumpDetail.getPumpNumber());
                pumpInfo.setRefuelMode(pumpDetail.getRefuelMode());

                for (ProductDetail productDetail : pumpDetail.getProductDetails()) {

                    pumpInfo.getFuelType().add(productDetail.getProductDescription());
                }

                stationInfo.getPumpList().add(pumpInfo);
            }
        }

        return stationInfo;
    }
    
    private String notifyResponse(String requestID, String stationID, String mpTransactionID, String srcTransactionID, String transactionResult, ForecourtPostPaidServiceRemote forecourtPPService) {

        System.out.println("*** notifyResponse ***");
        System.out.println("stationID: " + stationID);
        System.out.println("mpTransactionID: " + mpTransactionID);
        System.out.println("srcTransactionID: " + srcTransactionID);
        System.out.println("transactionResult: " + transactionResult);

        List<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail> vouchers = new ArrayList<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail>(0);

        SendMPTransactionResultMessageResponse sendMPTransactionResultMessageResponse = null;
        String notifyResponse = null;

        try {
            sendMPTransactionResultMessageResponse = forecourtPPService.sendMPTransactionResult(requestID, stationID, mpTransactionID, srcTransactionID, transactionResult,
                    null, Boolean.FALSE, vouchers, null);
            notifyResponse = sendMPTransactionResultMessageResponse.getStatusCode();
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                    "Exception in sendMpTransactionResult: " + ex.getLocalizedMessage());
            notifyResponse = "MESSAGE_ERROR_500";
        }

        System.out.println("sendMPTransactionResult: " + notifyResponse);
        
        return notifyResponse;
    }

}
