package com.techedge.mp.core.actions.poptransaction.v2;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.RefuelingServiceRemote;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.CardDepositTransactionBean;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PersonalDataBusinessBean;
import com.techedge.mp.core.business.model.PlateNumberBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventBean;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.ConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityConstants;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.LoadLoyaltyCreditsResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.ElectronicInvoice;
import com.techedge.mp.forecourt.integration.shop.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.integration.shop.interfaces.SendMPTransactionResultMessageResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ApprovePopTransactionBusinessAction {

    @Resource
    private EJBContext                   context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager                em;

    @EJB
    private LoggerService                loggerService;

    @EJB
    private RefuelingServiceRemote refuelingService;

    private PostPaidTransactionEventBean lastPostPaidTransactionEventBean;
    private Integer                      sequenceID      = 0;
    private String                       newStatus       = null;
    private String                       oldStatus       = null;
    private PaymentInfoBean              paymentInfoBean = null;

    public ApprovePopTransactionBusinessAction() {}

    public PostPaidApproveShopTransactionResponse execute(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId, String paymentMethodType,
            String encodedPin, Integer pinCheckMaxAttempts, List<String> userBlockExceptionList, GPServiceRemote gpService, ForecourtPostPaidServiceRemote forecourtPPService,
            UnavailabilityPeriodService unavailabilityPeriodService, EncryptionAES encryptionAES, FidelityServiceRemote fidelityService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        PostPaidApproveShopTransactionResponse poPApproveShopTransactionResponse = new PostPaidApproveShopTransactionResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_INVALID_TICKET);
                return poPApproveShopTransactionResponse;
            }
            //    		
            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_INVALID_TICKET);
                return poPApproveShopTransactionResponse;
            }

            if (userBlockExceptionList.contains(userBean.getPersonalDataBean().getSecurityDataEmail())) {

                System.out.println(userBean.getPersonalDataBean().getSecurityDataEmail() + " presente in lista utenti con eccezioni blocco");
            }
            else {
                // Controllo su disponibilit� del servizio di creazione transazioni postpaid
                ServiceAvailabilityData serviceAvailabilityData = unavailabilityPeriodService.retrieveServiceAvailability("CREATE_TRANSACTION_POSTPAID", new Date());
                if (serviceAvailabilityData != null) {

                    // Servizio non disponibile
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "ApprovePopTransaction unavailable");

                    userTransaction.commit();

                    poPApproveShopTransactionResponse.setStatusCode(serviceAvailabilityData.getStatusCode());
                    return poPApproveShopTransactionResponse;
                }
            }

            //    		// Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus);

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_UNAUTHORIZED);
                return poPApproveShopTransactionResponse;
            }

            PostPaidTransactionBean poPTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, mpTransactionID);
            if (poPTransactionBean == null) {

                // Lo stationID inserito non corrisponde a nessuna stazione valida
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No postPayed transaction found by mpTransactionId "
                        + mpTransactionID);

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_NOT_RECOGNIZED);
                return poPApproveShopTransactionResponse;
            }
            
            StationBean stationBean = poPTransactionBean.getStationBean();
            System.out.println("PV active? " + stationBean.getNewAcquirerActive());
            if (stationBean == null || !stationBean.getNewAcquirerActive()) {
                
                // Il PV non risulta abilitato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "PV not enabled");

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PV_NOT_ENABLED);
                return poPApproveShopTransactionResponse;
            }
            
            if (!stationBean.getBusinessActive()) {
                
                // Il PV non risulta abilitato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "L'impianto non � utilizzabile da utenti di tipo business");

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_BUSINESS_NOT_ACTIVE);
                return poPApproveShopTransactionResponse;
            }

            Double amount = poPTransactionBean.getAmount();

            PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(em, paymentMethodId, paymentMethodType);

            if (paymentInfoBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Payment Method not found");

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_FAILURE);
                return poPApproveShopTransactionResponse;
            }
            
            // Se l'utente ha scelto di pagare con voucher ma ha una carta di credito valida associata, bisogna restituire un messaggio di errore
            if (paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {
                
                if (userBean.getPaymentData() != null && !userBean.getPaymentData().isEmpty()) {
                    
                    for(PaymentInfoBean paymentInfoBeanCheck : userBean.getPaymentData()) {
                        
                        if (paymentInfoBeanCheck.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) &&
                                (paymentInfoBeanCheck.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED ||
                                 paymentInfoBeanCheck.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {
                            
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Valid credit card found -> unable to use voucher flow");

                            userTransaction.commit();

                            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_USE_CREDIT_CARD);
                            return poPApproveShopTransactionResponse;
                        }
                    }
                }
            }
            
            if (paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)) {
                
                double capAvailable = 0.0;

                if (userBean.getCapAvailable() != null) {
                    capAvailable = userBean.getCapAvailable().doubleValue();
                }
                
                if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {
    
                    // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Payment not verified and transaction amount exceeding the cap");
    
                    userTransaction.commit();
    
                    poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_CAP_FAILURE);
                    return poPApproveShopTransactionResponse;
                }
            }

            System.out.println("full amount: " + amount);

            poPApproveShopTransactionResponse = this.checkPaymentMethod(requestID, amount, userTransaction, userBean, paymentMethodId, paymentMethodType, encodedPin,
                    pinCheckMaxAttempts);

            if (poPApproveShopTransactionResponse.getStatusCode() != null) {
                return poPApproveShopTransactionResponse;
            }

            em.lock(poPTransactionBean, LockModeType.PESSIMISTIC_READ);

            System.out.println("Oggetto bloccato");

            if (!poPTransactionBean.getMpTransactionStatus().endsWith(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                        "Transaction Status not payable " + poPTransactionBean.getMpTransactionStatus());

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_NOT_RECOGNIZED);
                return poPApproveShopTransactionResponse;
            }

            poPTransactionBean.setTransactionCategory(TransactionCategoryType.TRANSACTION_BUSINESS);
            
            //poPTransactionBean.setUserBean(userBean);
            if (poPTransactionBean.getEncodedSecretKey() == null && stationBean.getDataAcquirer() != null) {
                poPTransactionBean.setEncodedSecretKey(stationBean.getDataAcquirer().getEncodedSecretKey());
                poPTransactionBean.setShopLogin(stationBean.getDataAcquirer().getApiKey());
                poPTransactionBean.setAcquirerID(stationBean.getDataAcquirer().getAcquirerID());
                poPTransactionBean.setGroupAcquirer(stationBean.getDataAcquirer().getGroupAcquirer());
                poPTransactionBean.setCurrency(stationBean.getDataAcquirer().getCurrency());
                em.merge(poPTransactionBean);
            }

            String decodedSecretKey = encryptionAES.decrypt(poPTransactionBean.getEncodedSecretKey());

            poPTransactionBean.setPaymentMethodType(paymentMethodType);
            poPTransactionBean.setVoucherReconcile(false);
            poPTransactionBean.setLoyaltyReconcile(false);
            poPTransactionBean.setToReconcile(false);
            poPTransactionBean.setNotificationPaid(false);
            poPTransactionBean.setNotificationUser(false);
            newStatus = StatusHelper.POST_PAID_STATUS_PAY_REQU;
            oldStatus = null;
            sequenceID = 1;
            Timestamp eventTimestamp = new Timestamp(new Date().getTime());
            String transactionEvent = StatusHelper.POST_PAID_EVENT_MA_PAY;
            String errorCode = null;
            String errorDescription = null;
            String eventResult = "OK";
            PostPaidTransactionEventBean lastPostPaidTransactionEventBean = poPTransactionBean.getLastPostPaidTransactionEventBean();
            
            if (lastPostPaidTransactionEventBean != null) {
                sequenceID = lastPostPaidTransactionEventBean.getSequenceID() + 1;
                oldStatus = lastPostPaidTransactionEventBean.getNewState();
            }

            PostPaidTransactionEventBean postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                    eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

            oldStatus = newStatus;

            em.persist(postPaidTransactionEventBean);

            if (poPTransactionBean.getRefuelBean().isEmpty() && poPTransactionBean.getCartBean().isEmpty()) {

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_NO_REFUEL_AND_CART_FOUND);
                return poPApproveShopTransactionResponse;
            }

            poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            
            
            // Inserimento chiamata servizio di caricamento punti per promo Diesel+
            String refuelMode = null;
            PostPaidLoadLoyaltyCreditsBean crmPostPaidLoadLoyaltyCredits = null;
            PostPaidRefuelBean crmPostPaidRefuelBean = null;
            List<ProductDetail> totalProductList = new ArrayList<ProductDetail>(0);
            
            int totalTransactionTask = 1;

            if (poPTransactionBean.getRefuelBean().size() > 1) {
                totalTransactionTask = poPTransactionBean.getRefuelBean().size();
            }
            else {
                refuelMode = FidelityConstants.REFUEL_MODE_ENI_CAFE_ONLY;
            }

            PostPaidRefuelBean[] refuelBeanArray = new PostPaidRefuelBean[poPTransactionBean.getRefuelBean().size()];
            poPTransactionBean.getRefuelBean().toArray(refuelBeanArray);

            for (int index = 0; index < totalTransactionTask; index++) {

                Boolean productBDFound = Boolean.FALSE;
                
                refuelMode = null;
                List<ProductDetail> productList = new ArrayList<ProductDetail>(0);
                if (!poPTransactionBean.getRefuelBean().isEmpty()) {
                    PostPaidRefuelBean refuelBean = refuelBeanArray[index];
                    crmPostPaidRefuelBean = refuelBean;
                    System.out.println("trovato refuel mode: " + refuelBean.getRefuelMode());

                    if (refuelBean.getRefuelMode().equalsIgnoreCase("servito")) {
                        refuelMode = FidelityConstants.REFUEL_MODE_SERVITO;
                    }

                    if (refuelBean.getRefuelMode().equalsIgnoreCase("fai_da_te")) {
                        refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_POSTPAY;
                    }

                    if (refuelMode == null) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                                "Tipo di rifornimento sconosciuto. Operazione scartata!");
                    }

                    ProductDetail productDetail = new ProductDetail();
                    productDetail.setAmount(refuelBean.getFuelAmount());

                    System.out.println("trovato product id: " + refuelBean.getProductId());

                    if (refuelBean.getProductId().equals("SP")) {

                        // sp
                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_SP);
                    }
                    else {

                        if (refuelBean.getProductId().equals("GG")) {

                            // gasolio
                            productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GASOLIO);
                        }
                        else {

                            if (refuelBean.getProductId().equals("BS")) {

                                // blue_super
                                productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_SUPER);
                            }
                            else {

                                if (refuelBean.getProductId().equals("BD")) {

                                    // blue_diesel
                                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_DIESEL);
                                    productBDFound = Boolean.TRUE;
                                }
                                else {

                                    if (refuelBean.getProductId().equals("MT")) {

                                        // metano
                                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_METANO);
                                    }
                                    else {

                                        if (refuelBean.getProductId().equals("GP")) {

                                            // gpl
                                            productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GPL);
                                        }
                                        else {

                                            if (refuelBean.getProductId().equals("AD")) {

                                                // ???
                                                productDetail.setProductCode(null);
                                            }
                                            else {

                                                // non_oil
                                                productDetail.setProductCode(null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    productDetail.setQuantity(refuelBean.getFuelQuantity());
                    productList.add(productDetail);
                    totalProductList.add(productDetail);
                }

                if (index == 0 && !poPTransactionBean.getCartBean().isEmpty()) {
                    System.out.println("trovato shop");

                    if (refuelMode == null) {
                        refuelMode = FidelityConstants.REFUEL_MODE_ENI_CAFE_ONLY;
                    }

                    boolean validProductFound = false;
                    Double shopAmount = 0.0;
                    Integer shopCount = 0;

                    for (PostPaidCartBean postPaidCartBean : poPTransactionBean.getCartBean()) {

                        if (postPaidCartBean.getProductId().equals(FidelityConstants.PRODUCT_CODE_NON_OIL))     // PRODOTTI NON OIL 70
                        {
                            validProductFound = true;
                            shopAmount = shopAmount + postPaidCartBean.getAmount();
                            shopCount = shopCount + postPaidCartBean.getQuantity();

                            System.out.println("amount: " + postPaidCartBean.getAmount() + ", count: " + postPaidCartBean.getQuantity());
                        }
                    }

                    if (validProductFound) {
                        ProductDetail shopProductDetail = new ProductDetail();

                        shopProductDetail.setAmount(shopAmount);
                        shopProductDetail.setProductCode(FidelityConstants.PRODUCT_CODE_NON_OIL);
                        shopProductDetail.setQuantity(Double.valueOf(shopCount));

                        productList.add(shopProductDetail);
                        totalProductList.add(shopProductDetail);
                    }
                }

                
                if (!productList.isEmpty() && refuelMode == FidelityConstants.REFUEL_MODE_SERVITO && productBDFound) {
                    
                    System.out.println("Carta fedelt� non presente ma rifornimento in servito e prodotto Diesel+ -> invio richiesta con fiscalCode");
                    String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                    String panCode    = "0000000000000000000";
                    String eanCode    = "0000000000000";

                    // Chimamata al servizio loadLoyaltyCredits

                    String operationID = new IdGenerator().generateId(16).substring(0, 33);
                    PartnerType partnerType = PartnerType.MP;
                    Long requestTimestamp = new Date().getTime();
                    String stationID = poPTransactionBean.getStationBean().getStationID();
                    String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;
                    String BIN = paymentInfoBean.getCardBin();

                    if (BIN != null && QueryRepository.findCardBinExists(em, BIN)) {
                        System.out.println("BIN valido (" + BIN + ")");
                        paymentMode = FidelityConstants.PAYMENT_METHOD_ENI_CARD;
                    }
                    else {
                        System.out.println("BIN Non Valido (" + BIN + ")");
                    }

                    PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean();
                    postPaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                    postPaidLoadLoyaltyCreditsBean.setOperationType("LOAD");
                    postPaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                    postPaidLoadLoyaltyCreditsBean.setEanCode(eanCode);

                    try {

                        System.out.println("Chiamata servizio di carico punti");

                        LoadLoyaltyCreditsResult loadLoyaltyCreditsResult = fidelityService.loadLoyaltyCredits(operationID, mpTransactionID, stationID,
                                panCode, BIN, refuelMode, paymentMode, "it", partnerType, requestTimestamp, fiscalCode, productList);

                        System.out.println("Risposta servizio di carico punti: " + loadLoyaltyCreditsResult.getStatusCode() + " (" + loadLoyaltyCreditsResult.getMessageCode()
                                + ")");

                        postPaidLoadLoyaltyCreditsBean.setBalance(loadLoyaltyCreditsResult.getBalance());
                        postPaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                        postPaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                        postPaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                        postPaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                        postPaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                        postPaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                        postPaidLoadLoyaltyCreditsBean.setCsTransactionID(loadLoyaltyCreditsResult.getCsTransactionID());
                        postPaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                        postPaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                        postPaidLoadLoyaltyCreditsBean.setMessageCode(loadLoyaltyCreditsResult.getMessageCode());
                        postPaidLoadLoyaltyCreditsBean.setStatusCode(loadLoyaltyCreditsResult.getStatusCode());
                        postPaidLoadLoyaltyCreditsBean.setWarningMsg(loadLoyaltyCreditsResult.getWarningMsg());

                        postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);
                        em.persist(postPaidLoadLoyaltyCreditsBean);
                        
                        if (loadLoyaltyCreditsResult.getVoucherList() != null && !loadLoyaltyCreditsResult.getVoucherList().isEmpty()) {
                            for(VoucherDetail voucherDetail : loadLoyaltyCreditsResult.getVoucherList()) {
                                PostPaidLoadLoyaltyCreditsVoucherBean postPaidLoadLoyaltyCreditsVoucherBean = new PostPaidLoadLoyaltyCreditsVoucherBean();
                                postPaidLoadLoyaltyCreditsVoucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                                postPaidLoadLoyaltyCreditsVoucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                                postPaidLoadLoyaltyCreditsVoucherBean.setInitialValue(voucherDetail.getInitialValue());
                                postPaidLoadLoyaltyCreditsVoucherBean.setPromoCode(voucherDetail.getPromoCode());
                                postPaidLoadLoyaltyCreditsVoucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                                postPaidLoadLoyaltyCreditsVoucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                                postPaidLoadLoyaltyCreditsVoucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                                postPaidLoadLoyaltyCreditsVoucherBean.setCode(voucherDetail.getVoucherCode());
                                postPaidLoadLoyaltyCreditsVoucherBean.setStatus(voucherDetail.getVoucherStatus());
                                postPaidLoadLoyaltyCreditsVoucherBean.setType(voucherDetail.getVoucherType());
                                postPaidLoadLoyaltyCreditsVoucherBean.setValue(voucherDetail.getVoucherValue());
                                postPaidLoadLoyaltyCreditsVoucherBean.setPostPaidLoadLoyaltyCreditsBean(postPaidLoadLoyaltyCreditsBean);
                                em.persist(postPaidLoadLoyaltyCreditsVoucherBean);
                                postPaidLoadLoyaltyCreditsBean.getPostPaidLoadLoyaltyCreditsVoucherBean().add(postPaidLoadLoyaltyCreditsVoucherBean);
                                
                            }
                        }

                        em.merge(postPaidLoadLoyaltyCreditsBean);
                        
                        poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(postPaidLoadLoyaltyCreditsBean);

                        crmPostPaidLoadLoyaltyCredits = postPaidLoadLoyaltyCreditsBean;
                        //em.persist(postPaidLoadLoyaltyCreditsBean);
                        //em.merge(poPTransactionBean);

                        if (!loadLoyaltyCreditsResult.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {
                            errorCode = loadLoyaltyCreditsResult.getStatusCode();
                            errorDescription = loadLoyaltyCreditsResult.getMessageCode();
                            eventResult = "KO";
                        }
                        else {
                            errorCode = null;
                            errorDescription = null;
                            eventResult = "OK";
                        }

                    }
                    catch (Exception e) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error loading loyalty credits");

                        postPaidLoadLoyaltyCreditsBean.setStatusCode(StatusHelper.LOYALTY_STATUS_TO_BE_VERIFIED);
                        postPaidLoadLoyaltyCreditsBean.setMessageCode("STORNO DA VERIFICARE (" + e.getMessage() + ")");
                        postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);
                        poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(postPaidLoadLoyaltyCreditsBean);
                        poPTransactionBean.setLoyaltyReconcile(true);

                        errorCode = "9999";
                        errorDescription = "Error loading loyalty credits (" + e.getMessage() + ")";
                        eventResult = "ERROR";
                    }

                    oldStatus = newStatus;
                    newStatus = StatusHelper.POST_PAID_STATUS_CREDITS_LOAD;
                    sequenceID += 1;
                    eventTimestamp = new Timestamp(new Date().getTime());
                    transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY;

                    postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp,
                            transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                    em.persist(postPaidTransactionEventBean);

                }

                System.out.println("Pagamento senza voucher");
            }
            

            eventTimestamp = new java.sql.Timestamp(new Date().getTime());

            System.out.println("Pagamento con carta di credito");

            Extension[] extension_array = new Extension[1];
            Extension i_extension = new Extension();
            i_extension.setKey("CST_DISTRID");
            i_extension.setValue(poPTransactionBean.getStationBean().getStationID());
            extension_array[0] = i_extension;

            sequenceID += 1;
            Integer sequencePaymentID = 1;

            GestPayData gestPayDataAUTHResponse = doPaymentAuth(poPTransactionBean, paymentInfoBean, userBean, paymentMethodId, amount, sequencePaymentID, requestID, sequenceID,
                    gpService, extension_array, decodedSecretKey);

            if (gestPayDataAUTHResponse.getTransactionResult().equals("ERROR")) {
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);
                poPTransactionBean.setToReconcile(true);

                Boolean loyaltyCredits = false;
                PaymentTransactionResult paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataAUTHResponse, amount, "AUT");

                sequenceID += 1;
                SendMPTransactionResultMessageResponse notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                        poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                        forecourtPPService, StatusHelper.POST_PAID_STATUS_PAY_AUTH_MISSING, null, null);

                System.out.println("ElectronicInvoiceID: " + notifyResponse.getElectronicInvoiceID());
                
                if (notifyResponse.getElectronicInvoiceID() != null) {
                    poPTransactionBean.setGFGElectronicInvoiceID(notifyResponse.getElectronicInvoiceID());
                }
                
                if (!notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SUCCESS)) {
                    poPTransactionBean.setNotificationPaid(false);
                }
                else {
                    poPTransactionBean.setNotificationPaid(true);
                }

                em.persist(poPTransactionBean);
                /*
                Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, null);
                */
                userTransaction.commit();
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_AUTH_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                return poPApproveShopTransactionResponse;
            }

            if (gestPayDataAUTHResponse.getTransactionResult().equals("KO")) {

                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);

                Boolean loyaltyCredits = false;
                PaymentTransactionResult paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataAUTHResponse, amount, "AUT");

                sequenceID += 1;
                SendMPTransactionResultMessageResponse notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                        poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                        forecourtPPService, StatusHelper.POST_PAID_STATUS_PAY_AUTH_REFUSED, null, null);

                System.out.println("ElectronicInvoiceID: " + notifyResponse.getElectronicInvoiceID());
                
                if (notifyResponse.getElectronicInvoiceID() != null) {
                    poPTransactionBean.setGFGElectronicInvoiceID(notifyResponse.getElectronicInvoiceID());
                }
                
                if (notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SUCCESS) || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                        || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                        || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)
                        || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_REJECTED)) {

                    poPTransactionBean.setNotificationPaid(true);

                }
                else {
                    poPTransactionBean.setToReconcile(true);
                    poPTransactionBean.setNotificationPaid(false);
                }

                em.persist(poPTransactionBean);
                /*
                Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, null);
                */        
                userTransaction.commit();
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_AUTH_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                return poPApproveShopTransactionResponse;
            }

            sequenceID += 1;
            sequencePaymentID += 1;

            PostPaidRefuelBean postPaidRefuelBean = null;
            if (!poPTransactionBean.getRefuelBean().isEmpty()) {
                postPaidRefuelBean = poPTransactionBean.getRefuelBean().iterator().next();
            }
            
            GestPayData gestPayDataSETTLEResponse = doPaymentSettle(poPTransactionBean, postPaidRefuelBean, paymentInfoBean, userBean, paymentMethodId, amount, sequencePaymentID, requestID,
                    sequenceID, gpService, decodedSecretKey);

            if (gestPayDataSETTLEResponse.getTransactionResult().equals("ERROR")) {
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);
                poPTransactionBean.setToReconcile(true);
                poPTransactionBean.setNotificationPaid(false);

                em.persist(poPTransactionBean);
                /*
                Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, null);
                */
                userTransaction.commit();
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                return poPApproveShopTransactionResponse;
            }

            if (gestPayDataSETTLEResponse.getTransactionResult().equals("KO")) {

                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);

                sequenceID += 1;
                sequencePaymentID += 1;

                GestPayData gestPayDataDELETEResponse = doPaymentAuthDelete(poPTransactionBean, paymentInfoBean, userBean, paymentMethodId, amount, sequencePaymentID, requestID,
                        sequenceID, gpService, decodedSecretKey);

                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);

                Boolean loyaltyCredits = false;
                PaymentTransactionResult paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataSETTLEResponse, amount, "MOV");

                if (!gestPayDataDELETEResponse.getTransactionResult().equals("OK")) {

                    poPTransactionBean.setToReconcile(true);

                    sequenceID += 1;
                    SendMPTransactionResultMessageResponse notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                            poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                            forecourtPPService, StatusHelper.POST_PAID_STATUS_PAY_AUTH_DELETE_MISSING, null, null);

                    System.out.println("ElectronicInvoiceID: " + notifyResponse.getElectronicInvoiceID());
                    
                    if (notifyResponse.getElectronicInvoiceID() != null) {
                        poPTransactionBean.setGFGElectronicInvoiceID(notifyResponse.getElectronicInvoiceID());
                    }
                    
                    if (notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SUCCESS) || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                            || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                            || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)) {

                        poPTransactionBean.setNotificationPaid(true);

                    }
                    else {
                        poPTransactionBean.setNotificationPaid(false);
                    }
                }
                else {
                    sequenceID += 1;
                    SendMPTransactionResultMessageResponse notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                            poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                            forecourtPPService, StatusHelper.POST_PAID_STATUS_CANCELED, null, null);

                    System.out.println("ElectronicInvoiceID: " + notifyResponse.getElectronicInvoiceID());
                    
                    if (notifyResponse.getElectronicInvoiceID() != null) {
                        poPTransactionBean.setGFGElectronicInvoiceID(notifyResponse.getElectronicInvoiceID());
                    }
                    
                    if (notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SUCCESS) || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                            || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                            || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)) {

                        poPTransactionBean.setNotificationPaid(true);
                        poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);
                    }
                    else {
                        poPTransactionBean.setToReconcile(true);
                        poPTransactionBean.setNotificationPaid(false);
                    }
                }

                em.persist(poPTransactionBean);
                /*
                if (( userBean.getUserType() == User.USER_TYPE_CUSTOMER || userBean.getUserType() == User.USER_TYPE_NEW_ACQUIRER_TESTER )) {
                    Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                            userCategoryService, null);
                }
                */
                userTransaction.commit();
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                return poPApproveShopTransactionResponse;
            }

            poPTransactionBean.setNotificationUser(true);
            Boolean loyaltyCredits = false;

            PaymentTransactionResult paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataSETTLEResponse, amount, "MOV");

            sequenceID += 1;
            SendMPTransactionResultMessageResponse notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                    poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                    forecourtPPService, StatusHelper.POST_PAID_STATUS_PAY_COMPLETE, StatusHelper.POST_PAID_STATUS_NOTIFICATION_FAIL, StatusHelper.POST_PAID_STATUS_PAY_MOV_DELETE);

            System.out.println("ElectronicInvoiceID: " + notifyResponse.getElectronicInvoiceID());
            
            if (notifyResponse.getElectronicInvoiceID() != null) {
                poPTransactionBean.setGFGElectronicInvoiceID(notifyResponse.getElectronicInvoiceID());
            }
            
            if (notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SUCCESS)) {
                poPTransactionBean.setNotificationPaid(true);
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_OK);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            }
            
            if (notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR)) {
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
                poPTransactionBean.setToReconcile(true);
                poPTransactionBean.setNotificationPaid(false);
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            }

            if (notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setToReconcile(true);
                poPTransactionBean.setNotificationPaid(false);
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            }

            if (notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                    || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                    || notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)
                    || (notifyResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_REJECTED) && poPTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode()))) {

                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);
                poPTransactionBean.setNotificationPaid(true);

                sequenceID += 1;
                sequencePaymentID += 1;

                GestPayData gestPayDataREFUNDResponse = doPaymentRefund(poPTransactionBean, paymentInfoBean, userBean, paymentMethodId, amount, sequencePaymentID, requestID,
                        sequenceID, gpService, decodedSecretKey);

                if (!gestPayDataREFUNDResponse.getTransactionResult().equals("OK")) {

                    poPTransactionBean.setToReconcile(true);
                }
                else {

                    poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);
                 }

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            }

            em.persist(poPTransactionBean);
            
            userTransaction.commit();
            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_OK);
            poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

            return poPApproveShopTransactionResponse;
        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            if (ex2.getClass().getSimpleName().equals("LockAcquisitionException")) {

                System.out.println("Dentro If" + ex2.getClass().getSimpleName());
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Transaction Status not payable ");
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_NOT_RECOGNIZED);

                try {
                    userTransaction.rollback();
                }
                catch (IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (SecurityException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (SystemException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                return poPApproveShopTransactionResponse;

            }

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED Post Paid approve transaction with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

    private PostPaidApproveShopTransactionResponse checkPaymentMethod(String requestID, Double amount, UserTransaction userTransaction, UserBean userBean, Long paymentMethodId,
            String paymentMethodType, String encodedPin, Integer pinCheckMaxAttempts) throws Exception {

        System.out.println("Controllo del sistema di pagamento utente");

        PostPaidApproveShopTransactionResponse poPApproveShopTransactionResponse = new PostPaidApproveShopTransactionResponse();

        PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();

        if (paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals("")) {

            // Errore Pin non ancora inserito
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");

            userTransaction.commit();
            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_UNAUTHORIZED);
            return poPApproveShopTransactionResponse;
        }
        
        if (paymentMethodId == null && paymentMethodType == null) {

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found");

            paymentInfoBean = userBean.findDefaultPaymentInfoBean();
        }
        else {

            paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
        }

        // Verifica che il metodo di pagamento selezionato sia in uno stato valido
        if (paymentInfoBean == null) {

            // Il metodo di pagamento selezionato non esiste
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");

            userTransaction.commit();

            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PAYMENT_WRONG);
            return poPApproveShopTransactionResponse;
        }

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: "
                + paymentMethodType);

        if (!paymentInfoBean.isValid()) {

            // Il metodo di pagamento selezionato non pu� essere utilizzato
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());

            userTransaction.commit();

            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PAYMENT_WRONG);
            return poPApproveShopTransactionResponse;
        }

        Boolean creditCardFound = Boolean.FALSE;

        for (PaymentInfoBean paymentInfo : userBean.getPaymentData()) {
            if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) && (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED)) {
                creditCardFound = Boolean.TRUE;
                System.out.println("Credit Card found");
                break;
            }
        }
        
        if (!creditCardFound) {
            
            // Il metodo di pagamento selezionato non pu� essere utilizzato
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No credit card found");

            userTransaction.commit();

            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PAYMENT_WRONG);
            return poPApproveShopTransactionResponse;
        }

        if (!encodedPin.equals(paymentInfoVoucherBean.getPin())) {

            // Il pin inserito non � valido
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");

            String response = ResponseHelper.REFUEL_TRANSACTION_CREATE_ERROR_PIN;
            
            // Si sottrae uno al numero di tentativi residui
            Integer pinCheckAttemptsLeft = paymentInfoVoucherBean.getPinCheckAttemptsLeft();
            if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                pinCheckAttemptsLeft--;
            }
            else {
                pinCheckAttemptsLeft = 0;
            }

            if (pinCheckAttemptsLeft == 0) {

                // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                paymentInfoVoucherBean.setDefaultMethod(false);
                paymentInfoVoucherBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

                for (PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {

                    if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                            && (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                        paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                        paymentInfoBean.setDefaultMethod(false);
                        em.merge(paymentInfoBean);
                    }
                }
            }
            else {
                
                if ( pinCheckAttemptsLeft == 1 ) {
                    
                    response = ResponseHelper.REFUEL_TRANSACTION_CREATE_ERROR_PIN_LAST_ATTEMPT;
                }
                else {
                    
                    response = ResponseHelper.REFUEL_TRANSACTION_CREATE_ERROR_PIN_ATTEMPTS_LEFT;
                }
            }

            paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

            em.merge(paymentInfoVoucherBean);
            

            userTransaction.commit();

            poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoVoucherBean.getPinCheckAttemptsLeft());
            poPApproveShopTransactionResponse.setStatusCode(response);
            return poPApproveShopTransactionResponse;
        }

        // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
        paymentInfoVoucherBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);
        
        em.merge(paymentInfoVoucherBean);
        
        return poPApproveShopTransactionResponse;
    }
    
    private GestPayData doPaymentAuth(PostPaidTransactionBean poPTransactionBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId, Double amount,
            Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, Extension[] extension_array, String encodedSecretKey) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_AUTH;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_AUTH;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        String transactionCategory = poPTransactionBean.getTransactionCategory().getValue();
        
        String paymentMethodExpiration = "";
        
        if (paymentInfoBean != null) {
            
            CardDepositTransactionBean cardDepositTransactionBean = QueryRepository.findCardDepositTransactionByShopTransactionID(em, paymentInfoBean.getCheckShopTransactionID());
            if (cardDepositTransactionBean != null) {
                
                String tokenExpiryMonth = cardDepositTransactionBean.getTokenExpiryMonth();
                String tokenExpiryYear  = cardDepositTransactionBean.getTokenExpiryYear();
                
                paymentMethodExpiration = "20" + tokenExpiryYear + tokenExpiryMonth;
            }                
        }

        GestPayData gestPayDataAUTHResponse = gpService.callPagam(amount, poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), paymentInfoBean.getToken(), null, poPTransactionBean.getAcquirerID(), poPTransactionBean.getGroupAcquirer(), 
                encodedSecretKey, paymentMethodExpiration, extension_array, transactionCategory);

        if (gestPayDataAUTHResponse == null) {
            gestPayDataAUTHResponse = new GestPayData();
            gestPayDataAUTHResponse.setTransactionResult("ERROR");
            gestPayDataAUTHResponse.setErrorCode("9999");
            gestPayDataAUTHResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH: " + gestPayDataAUTHResponse.getTransactionResult());

        errorCode = gestPayDataAUTHResponse.getErrorCode();
        errorDescription = gestPayDataAUTHResponse.getErrorDescription();
        eventResult = gestPayDataAUTHResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataAUTHResponse, "AUT");

        em.persist(postPaidTransactionPaymentEventBean);

        poPTransactionBean.setPaymentMethodId(paymentMethodId);
        poPTransactionBean.setToken(paymentInfoBean.getToken());
        poPTransactionBean.setUserBean(userBean);
        poPTransactionBean.setLastModifyTimestamp(eventTimestamp);
        poPTransactionBean.setBankTansactionID(gestPayDataAUTHResponse.getBankTransactionID());
        poPTransactionBean.setAuthorizationCode(gestPayDataAUTHResponse.getAuthorizationCode());

        oldStatus = newStatus;

        return gestPayDataAUTHResponse;
    }

    private GestPayData doPaymentSettle(PostPaidTransactionBean poPTransactionBean, PostPaidRefuelBean postPaidRefuelBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId, Double amount,
            Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_MOV;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_MOV;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataSETTLEResponse = gpService.callSettle(amount, poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), poPTransactionBean.getAcquirerID(), poPTransactionBean.getGroupAcquirer(), encodedSecretKey,
                poPTransactionBean.getToken(), poPTransactionBean.getAuthorizationCode(), poPTransactionBean.getBankTansactionID(), poPTransactionBean.getRefuelMode(),
                poPTransactionBean.getProductID(), postPaidRefuelBean.getFuelQuantity(), postPaidRefuelBean.getUnitPrice());

        if (gestPayDataSETTLEResponse == null) {
            gestPayDataSETTLEResponse = new GestPayData();
            gestPayDataSETTLEResponse.setTransactionResult("ERROR");
            gestPayDataSETTLEResponse.setErrorCode("9999");
            gestPayDataSETTLEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay SETTLE: " + gestPayDataSETTLEResponse.getTransactionResult());

        errorCode = gestPayDataSETTLEResponse.getErrorCode();
        errorDescription = gestPayDataSETTLEResponse.getErrorDescription();
        eventResult = gestPayDataSETTLEResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataSETTLEResponse, "MOV");

        em.persist(postPaidTransactionPaymentEventBean);

        poPTransactionBean.setPaymentMethodId(paymentMethodId);
        poPTransactionBean.setToken(paymentInfoBean.getToken());
        poPTransactionBean.setUserBean(userBean);
        poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
        poPTransactionBean.setLastModifyTimestamp(eventTimestamp);
        //poPTransactionBean.setBankTansactionID(gestPayDataSETTLEResponse.getBankTransactionID());

        //em.persist(poPTransactionBean);

        oldStatus = newStatus;

        return gestPayDataSETTLEResponse;
    }

    private GestPayData doPaymentAuthDelete(PostPaidTransactionBean poPTransactionBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId, Double amount,
            Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_AUTH_DELETE;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_AUTH_DEL;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataDELETEResponse = gpService.deletePagam(poPTransactionBean.getAmount(), poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), poPTransactionBean.getBankTansactionID(), null, poPTransactionBean.getAcquirerID(), poPTransactionBean.getGroupAcquirer(),
                encodedSecretKey);

        if (gestPayDataDELETEResponse == null) {
            gestPayDataDELETEResponse = new GestPayData();
            gestPayDataDELETEResponse.setTransactionResult("ERROR");
            gestPayDataDELETEResponse.setErrorCode("9999");
            gestPayDataDELETEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH DEL: " + gestPayDataDELETEResponse.getTransactionResult());

        errorCode = gestPayDataDELETEResponse.getErrorCode();
        errorDescription = gestPayDataDELETEResponse.getErrorDescription();
        eventResult = gestPayDataDELETEResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataDELETEResponse, "DEL");

        em.persist(postPaidTransactionPaymentEventBean);

        oldStatus = newStatus;

        return gestPayDataDELETEResponse;
    }

    private GestPayData doPaymentRefund(PostPaidTransactionBean poPTransactionBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double residualAmount, Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, String encodedSecretKey) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_REVERSE;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        
        GestPayData gestPayDataREFUNDResponse = gpService.callRefund(residualAmount, poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), poPTransactionBean.getToken(), poPTransactionBean.getAuthorizationCode(), poPTransactionBean.getBankTansactionID(),
                poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getAcquirerID(), 
                poPTransactionBean.getGroupAcquirer(), encodedSecretKey);

        if (gestPayDataREFUNDResponse == null) {
            gestPayDataREFUNDResponse = new GestPayData();
            gestPayDataREFUNDResponse.setTransactionResult("ERROR");
            gestPayDataREFUNDResponse.setErrorCode("9999");
            gestPayDataREFUNDResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay REFUND: " + gestPayDataREFUNDResponse.getTransactionResult());

        errorCode = gestPayDataREFUNDResponse.getErrorCode();
        errorDescription = gestPayDataREFUNDResponse.getErrorDescription();
        eventResult = gestPayDataREFUNDResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataREFUNDResponse, "STO");

        em.persist(postPaidTransactionPaymentEventBean);

        oldStatus = newStatus;

        return gestPayDataREFUNDResponse;
    }

    private SendMPTransactionResultMessageResponse notifyResponse(String requestID, Integer eventSequenceID, String stationID, String mpTransactionID, String srcTransactionID, String transactionResult,
            PaymentTransactionResult paymentTransactionResult, Boolean loyaltyCredits, PostPaidTransactionBean poPTransactionBean,
            ForecourtPostPaidServiceRemote forecourtPPService, String eventStatusOk, String eventStatusError, String eventStatusCancelled) {

        // TODO Auto-generated method stub

        System.out.println("*** notifyResponse ***");
        System.out.println("stationID: " + stationID);
        System.out.println("mpTransactionID: " + mpTransactionID);
        System.out.println("srcTransactionID: " + srcTransactionID);
        System.out.println("transactionResult: " + transactionResult);
        if (loyaltyCredits != null) {
            System.out.println("loyaltyCredits: " + loyaltyCredits);
        }
        else {
            System.out.println("loyaltyCredits: null");
        }

        List<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail> vouchers = new ArrayList<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail>(0);
        
        for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : poPTransactionBean.getPostPaidConsumeVoucherBeanList()) {

            for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {

                com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail voucherDetail = new com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail();

                if (postPaidConsumeVoucherDetailBean.getConsumedValue() == null || postPaidConsumeVoucherDetailBean.getConsumedValue().equals(new Double("0.0"))) {
                    continue;
                }

                voucherDetail.setPromoCode(postPaidConsumeVoucherDetailBean.getPromoCode());
                voucherDetail.setPromoDescription(postPaidConsumeVoucherDetailBean.getPromoDescription());
                voucherDetail.setVoucherAmount(postPaidConsumeVoucherDetailBean.getConsumedValue());
                voucherDetail.setVoucherCode(postPaidConsumeVoucherDetailBean.getVoucherCode());

                System.out.println("voucher - promoCode: " + voucherDetail.getPromoCode() + ", promoDescription: " + voucherDetail.getPromoDescription() + ", voucherAmount: "
                        + voucherDetail.getVoucherAmount() + ", voucherCode: " + voucherDetail.getVoucherCode());

                vouchers.add(voucherDetail);
            }
        }

        SendMPTransactionResultMessageResponse sendMPTransactionResultMessageResponse = new SendMPTransactionResultMessageResponse();
        String errorCode = null;
        String errorDescription = null;
        String eventResult = null;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION;
        
        ElectronicInvoice electronicInvoice = null;
        
        try {

            System.out.println("Utente business. Creazione della fattura elettronica");
            PersonalDataBusinessBean personalDataBusinessBean = poPTransactionBean.getUserBean().getPersonalDataBusinessList().iterator().next();
        
            if (personalDataBusinessBean == null) {
                throw new Exception("No business data for user: " + poPTransactionBean.getUserBean().getPersonalDataBean().getSecurityDataEmail());
            }

            electronicInvoice = new ElectronicInvoice();
            
            electronicInvoice.setAddress(personalDataBusinessBean.getAddress());
            electronicInvoice.setBusinessName(personalDataBusinessBean.getBusinessName());
            electronicInvoice.setCity(personalDataBusinessBean.getCity());
            electronicInvoice.setCountry(personalDataBusinessBean.getCountry());
            electronicInvoice.setEmailAdddress(poPTransactionBean.getUserBean().getPersonalDataBean().getSecurityDataEmail());
            
            // Ricerca la targa preferita
            String licensePlate = personalDataBusinessBean.getLicensePlate();
            for(PlateNumberBean plateNumberBean : poPTransactionBean.getUserBean().getPlateNumberList()) {
                
                if (plateNumberBean.getDefaultPlateNumber()) {
                    
                    licensePlate = plateNumberBean.getPlateNumber();
                    break;
                }
            }
            
            electronicInvoice.setLicensePlate(licensePlate);
            electronicInvoice.setName(personalDataBusinessBean.getFirstName());
            electronicInvoice.setPecEmailAdddress(personalDataBusinessBean.getPecEmail());
            electronicInvoice.setProvince(personalDataBusinessBean.getProvince());
            electronicInvoice.setSdiCode(personalDataBusinessBean.getSdiCode());
            electronicInvoice.setStreetNumber(personalDataBusinessBean.getStreetNumber());
            electronicInvoice.setSurname(personalDataBusinessBean.getLastName());
            electronicInvoice.setVatNumber(personalDataBusinessBean.getVatNumber());
            electronicInvoice.setZipCode(personalDataBusinessBean.getZipCode());
            electronicInvoice.setFiscalCode(personalDataBusinessBean.getFiscalCode());

            
            sendMPTransactionResultMessageResponse = forecourtPPService.sendMPTransactionResult(requestID, stationID, mpTransactionID, srcTransactionID, transactionResult,
                    paymentTransactionResult, loyaltyCredits, vouchers, electronicInvoice);
            
            sendMPTransactionResultMessageResponse.setStatusCode(sendMPTransactionResultMessageResponse.getStatusCode());
            sendMPTransactionResultMessageResponse.setElectronicInvoiceID(sendMPTransactionResultMessageResponse.getElectronicInvoiceID());
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                    "Exception in sendMpTransactionResult: " + ex.getLocalizedMessage());

            sendMPTransactionResultMessageResponse.setStatusCode(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR);
            sendMPTransactionResultMessageResponse.setMessageCode(ex.getMessage());
            errorCode = "9999";
            errorDescription = "Error in sendMpTransactionResult (" + ex.getMessage() + ")";
        }

        //if (!sendMPTransactionResultMessageResponse.getStatusCode().equals("MESSAGE_RECEIVED_200")) {}

        System.out.println("sendMPTransactionResult: " + sendMPTransactionResultMessageResponse.getStatusCode());

        if (sendMPTransactionResultMessageResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SUCCESS)) {
            newStatus = eventStatusOk;
            eventResult = "OK";
        }
        else if (sendMPTransactionResultMessageResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR) || sendMPTransactionResultMessageResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
            newStatus = (eventStatusError != null) ? eventStatusError : eventStatusOk;
            eventResult = "ERROR";
        }
        else if (sendMPTransactionResultMessageResponse.getStatusCode().equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
            newStatus = (eventStatusError != null) ? eventStatusError : eventStatusOk;
            eventResult = "ERROR";
            errorDescription = sendMPTransactionResultMessageResponse.getStatusCode();
        }
        else {
            newStatus = (eventStatusCancelled != null) ? eventStatusCancelled : eventStatusOk;
            eventResult = "KO";
            errorDescription = sendMPTransactionResultMessageResponse.getStatusCode();
        }

        PostPaidTransactionEventBean postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        oldStatus = newStatus;
        
        return sendMPTransactionResultMessageResponse;
    }

    private PostPaidTransactionEventBean generateTransactionEvent(Integer eventSequenceID, String stateType, String requestID, Date eventTimestamp, String event,
            PostPaidTransactionBean poPTransactionBean, String result, String eventNewState, String eventOldState, String errorCode, String errorDescription) {

        PostPaidTransactionEventBean postPaidTransactionEventBean = new PostPaidTransactionEventBean();
        postPaidTransactionEventBean.setSequenceID(eventSequenceID);
        postPaidTransactionEventBean.setStateType(stateType);
        postPaidTransactionEventBean.setRequestID(requestID);
        postPaidTransactionEventBean.setEventTimestamp(eventTimestamp);
        postPaidTransactionEventBean.setEvent(event);
        postPaidTransactionEventBean.setTransactionBean(poPTransactionBean);
        postPaidTransactionEventBean.setResult(result);
        postPaidTransactionEventBean.setNewState(eventNewState);
        postPaidTransactionEventBean.setOldState(eventOldState);
        postPaidTransactionEventBean.setErrorCode(errorCode);
        postPaidTransactionEventBean.setErrorDescription(errorDescription);

        this.lastPostPaidTransactionEventBean = postPaidTransactionEventBean;

        return postPaidTransactionEventBean;
    }

    private PostPaidTransactionPaymentEventBean generateTransactionPaymentEvent(Integer eventSequenceID, PostPaidTransactionBean poPTransactionBean, GestPayData gestPayData,
            String event) {

        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean = new PostPaidTransactionPaymentEventBean();
        postPaidTransactionPaymentEventBean.setSequence(eventSequenceID);
        postPaidTransactionPaymentEventBean.setErrorCode(gestPayData.getErrorCode());
        postPaidTransactionPaymentEventBean.setErrorDescription(gestPayData.getErrorDescription());
        postPaidTransactionPaymentEventBean.setTransactionBean(poPTransactionBean);
        postPaidTransactionPaymentEventBean.setTransactionResult(gestPayData.getTransactionResult());
        postPaidTransactionPaymentEventBean.setAuthorizationCode(gestPayData.getAuthorizationCode());
        postPaidTransactionPaymentEventBean.setEventType(event);

        return postPaidTransactionPaymentEventBean;
    }

    private PaymentTransactionResult generatePaymentTransactionResult(PostPaidTransactionBean poPTransactionBean, GestPayData gestPayData, Double amount, String event) {
        PaymentTransactionResult paymentTransactionResult = new PaymentTransactionResult();
        paymentTransactionResult.setAmount(amount);
        paymentTransactionResult.setAcquirerID(poPTransactionBean.getAcquirerID());
        paymentTransactionResult.setAuthorizationCode(gestPayData.getAuthorizationCode());
        paymentTransactionResult.setBankTransactionID(gestPayData.getBankTransactionID());
        paymentTransactionResult.setCurrency(poPTransactionBean.getCurrency());
        paymentTransactionResult.setErrorCode(gestPayData.getErrorCode());
        paymentTransactionResult.setErrorDescription(gestPayData.getErrorDescription());
        paymentTransactionResult.setEventType(event);
        paymentTransactionResult.setShopLogin(poPTransactionBean.getShopLogin());
        paymentTransactionResult.setPaymentMode(poPTransactionBean.getPaymentMode());
        paymentTransactionResult.setShopTransactionID(gestPayData.getShopTransactionID());
        paymentTransactionResult.setTransactionResult(gestPayData.getTransactionResult().equals("ERROR") ? "KO" : gestPayData.getTransactionResult());

        return paymentTransactionResult;
    }
    
}
