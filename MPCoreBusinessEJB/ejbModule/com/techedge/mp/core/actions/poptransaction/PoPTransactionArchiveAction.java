package com.techedge.mp.core.actions.poptransaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventHistoryBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPTransactionArchiveAction {

	@Resource
    private EJBContext context;
	
    @PersistenceContext( unitName = "CrudPU" )
    private EntityManager em;
    
    @EJB
    private LoggerService loggerService;
    
    
    public PoPTransactionArchiveAction() {
    }
    

    public String execute() throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		//TODO sistemare le estrazioni
    		List<PostPaidTransactionBean> poPTransactionBeanList1 = QueryRepository.findPostPaidTransactionBeanByMPStatus(em, StatusHelper.POST_PAID_FINAL_STATUS_PAID);
    		List<PostPaidTransactionBean> poPTransactionBeanList2 = QueryRepository.findPostPaidTransactionBeanByMPStatus(em, StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);
    		List<PostPaidTransactionBean> poPTransactionBeanList3 = QueryRepository.findPostPaidTransactionBeanByMPStatus(em, StatusHelper.POST_PAID_FINAL_STATUS_REVERSED);
		    
		    userTransaction.commit();
    		
    		Integer totalSize = 0;
    		if ( poPTransactionBeanList1 != null ) {
    			totalSize += poPTransactionBeanList1.size();
    		}
    		if ( poPTransactionBeanList2 != null ) {
    			totalSize += poPTransactionBeanList2.size();
    		}
    		
    		if ( poPTransactionBeanList3 != null ) {
    			totalSize += poPTransactionBeanList3.size();
    		}
    		

    		List<PostPaidTransactionBean> transactionBeanList = new ArrayList<PostPaidTransactionBean>(totalSize);

    		if ( poPTransactionBeanList1 != null ) {
    			transactionBeanList.addAll(poPTransactionBeanList1);
    		}
    		if ( poPTransactionBeanList2 != null ) {
    			transactionBeanList.addAll(poPTransactionBeanList2);
    		}
    		if ( poPTransactionBeanList3 != null ) {
    			transactionBeanList.addAll(poPTransactionBeanList3);
    		}
    		

    		// Per ogni transazione estratta
    		for( PostPaidTransactionBean postPaidTransactionBean : transactionBeanList ) {

    			userTransaction.begin();
    			
    			// 1) Crea la transazione archiviata
    			PostPaidTransactionHistoryBean postPaidTransactionHistoryBean = new PostPaidTransactionHistoryBean(postPaidTransactionBean);
    			postPaidTransactionHistoryBean.setArchivingDate(new Date());
    			
    			// 2) Persisti le informazioni principale
    			em.persist(postPaidTransactionHistoryBean);    			

    			// 3) Persisti le informazioni nelle tabelle archivio

    			for( PostPaidCartHistoryBean postPaidCartHistoryBean : postPaidTransactionHistoryBean.getCartHistoryBean() ) {
    				em.persist(postPaidCartHistoryBean);
    			}
    			for( PostPaidCartBean postPaidCartBean : postPaidTransactionBean.getCartBean()) {
    				em.remove(postPaidCartBean);
    			}
    			
    			for( PostPaidRefuelHistoryBean postPaidRefuelHistoryBean : postPaidTransactionHistoryBean.getRefuelHistoryBean() ) {
    				em.persist(postPaidRefuelHistoryBean);
    			}
    			for( PostPaidRefuelBean postPaidRefuelBean : postPaidTransactionBean.getRefuelBean()) {
    				em.remove(postPaidRefuelBean);
    			}
    			
    			for( PostPaidTransactionEventHistoryBean postPaidTransactionEventHistoryBean : postPaidTransactionHistoryBean.getPostPaidTransactionEventHistoryBean() ) {
    				em.persist(postPaidTransactionEventHistoryBean);
    			}
    			for( PostPaidTransactionEventBean postPaidTransactionEventBean : postPaidTransactionBean.getPostPaidTransactionEventBean()) {
    				em.remove(postPaidTransactionEventBean);
    			}
    			
    			for( PostPaidTransactionPaymentEventHistoryBean postPaidTransactionPaymentEventHistoryBean : postPaidTransactionHistoryBean.getPostPaidTransactionPaymentEventHistoryBean() ) {
    				em.persist(postPaidTransactionPaymentEventHistoryBean);
    			}
    			for( PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean : postPaidTransactionBean.getPostPaidTransactionPaymentEventBean()) {
    				em.remove(postPaidTransactionPaymentEventBean);
    			}
    			    			
    			em.remove(postPaidTransactionBean);
    			userTransaction.commit();

    		}

    		return "OK";
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED PoPtransaction archiving with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
