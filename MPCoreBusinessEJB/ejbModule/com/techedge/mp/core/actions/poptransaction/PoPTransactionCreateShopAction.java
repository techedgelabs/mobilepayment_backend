package com.techedge.mp.core.actions.poptransaction;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCreateShopTransactionResponse;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShopRefuelDetail;
import com.techedge.mp.forecourt.integration.shop.interfaces.ShoppingCartDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPTransactionCreateShopAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public PoPTransactionCreateShopAction() {}

    public PostPaidCreateShopTransactionResponse execute(String requestID,
            String source,  //"CASH-REGISTER"
            String sourceID, String sourceNumber, String stationID, String srcTransactionID, Double amount,
            String productType, // OIL NON_OIL MIXED
            String currency, Integer reconciliationMaxAttemps, List<ShoppingCartDetail> shoppingCartList, List<ShopRefuelDetail> shopRefuelDetailList,
            ForecourtPostPaidServiceRemote forecourtPPService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();
            PostPaidCreateShopTransactionResponse poPCreateShopTransactionResponse = new PostPaidCreateShopTransactionResponse();

//    		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "ServerName: " + serverName );

//    		CreateRefuelResponse createRefuelResponse = new CreateRefuelResponse();
//    		
//    		TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);
//		    
//    		if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {
//    			
//    			// Ticket non valido
//    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket" );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
//    			return createRefuelResponse;
//    		}
//    		
//    		UserBean userBean = ticketBean.getUser();
//    		if ( userBean == null ) {
//    			
//    			// Ticket non valido
//    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found" );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
//    			return createRefuelResponse;
//    		}

//    		// Verifica lo stato dell'utente
//    		Integer userStatus = userBean.getUserStatus();
//    		if ( userStatus != User.USER_STATUS_VERIFIED ) {
//    			
//    			// Un utente che si trova in questo stato non pu� invocare questo servizio
//    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_UNAUTHORIZED);
//    			return createRefuelResponse;
//    		}

            // Controlla se l'utente ha gi� una transazione attiva
//    		List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);
//		    
//		    if ( !transactionBeanList.isEmpty() ) {
//    			
//    			// Esiste una transazione associata all'utente non ancora completata
//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction found" );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
//    			return createRefuelResponse;
//    		}

//		    // Se il medodo di pagamento non � specificato utilizza quello di default
//		    PaymentInfoBean paymentInfoBean = null;
//		    if ( paymentMethodId == null && paymentMethodType == null ) {
//		    	
//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found" );
//				
//		    	paymentInfoBean = userBean.findDefaultPaymentInfoBean();
//		    }
//		    else {
//
//		    	paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
//		    }
//		    
//		    // Verifica che il metodo di pagamento selezionato sia in uno stato valido
//		    if ( paymentInfoBean == null ) {
//		    	
//		    	// Il metodo di pagamento selezionato non esiste
//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data" );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
//    			return createRefuelResponse;
//		    }
//		    
//		    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: " + paymentMethodType );
//		    
//		    if ( paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_BLOCKED     ||
//		    	 paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_ERROR       ||
//		    	 paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING     ) {
//		    	
//		    	// Il metodo di pagamento selezionato non pu� essere utilizzato
//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus() );
//				
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
//    			return createRefuelResponse;
//		    }
//		    
//    		if ( !encodedPin.equals(paymentInfoBean.getPin()) ) {
//    			
//    			// Il pin inserito non � valido
//    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin" );
//				
//    			// Si sottrae uno al numero di tentativi residui
//    			Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
//    			if ( pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0 ) {
//    				pinCheckAttemptsLeft--;
//    			}
//    			else {
//    				pinCheckAttemptsLeft = 0;
//    			}
//    			
//    			if ( pinCheckAttemptsLeft == 0 ) {
//    				
//    				// Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
//    				paymentInfoBean.setDefaultMethod(false);
//    				paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
//    			}
//    			
//    			
//    			paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);
//    			
//    			em.merge(paymentInfoBean);
//    			
//    			userTransaction.commit();
//    			
//    			createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_WRONG);
//    			return createRefuelResponse;
//    		}
//    		
//    		
//    		// L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
//    		paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);
//    		
//    		em.merge(paymentInfoBean);

            if (!productType.equals("OIL")) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Prodotti non oil non pagabili - " + "Tipo: " + productType);

                userTransaction.commit();

                poPCreateShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
                return poPCreateShopTransactionResponse;
            }

            if (shopRefuelDetailList.size() > 1) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Troppi prodotti oil..... Transazione non valida");

                userTransaction.commit();

                poPCreateShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
                return poPCreateShopTransactionResponse;
            }

            StationBean stationBean = QueryRepository.findStationBeanById(em, stationID);

            if (stationBean == null) {

                // Lo stationID inserito non corrisponde a nessuna stazione valida
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No station for stationID " + stationID);

                userTransaction.commit();

                poPCreateShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
                return poPCreateShopTransactionResponse;
            }

            //TODO occorre inserire la verifica che non ci sia una transazione aperta\attiva;

            String shopLogin = null;    //stationBean.getOilShopLogin();
            String acquirerID = null;    //stationBean.getOilAcquirerID();

            String mpTransactionID = new IdGenerator().generateId(16).substring(0, 32);

            Date now = new Date();
            Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());

            // Crea la transazione con i dati ricevuti in input
            PostPaidTransactionBean poPTransactionBean = new PostPaidTransactionBean();
            //transactionBean.setTransactionID(transactionID);

            poPTransactionBean.setStationBean(stationBean);
            //transactionBean.setUseVoucher(useVoucher);
            poPTransactionBean.setRequestID(requestID);
            poPTransactionBean.setSource(source);
            poPTransactionBean.setSourceNumber(sourceNumber);
            poPTransactionBean.setCreationTimestamp(creationTimestamp);
            poPTransactionBean.setSrcTransactionID(srcTransactionID);
            poPTransactionBean.setSourceID(sourceID);
            poPTransactionBean.setMpTransactionID(mpTransactionID);
            poPTransactionBean.setStationBean(stationBean);
            poPTransactionBean.setShopLogin(shopLogin);
            poPTransactionBean.setAcquirerID(acquirerID);
            poPTransactionBean.setProductType(productType);
            poPTransactionBean.setAmount(amount);
            poPTransactionBean.setCurrency(currency);
            poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD);
            poPTransactionBean.setNotificationCreated(false);
            poPTransactionBean.setNotificationPaid(false);
            poPTransactionBean.setNotificationUser(false);
            poPTransactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttemps);

            em.persist(poPTransactionBean);

//    		userTransaction.commit();
//    		userTransaction.begin();

            if (shopRefuelDetailList != null) {

                for (ShopRefuelDetail shopRefuelDetail : shopRefuelDetailList) {
                    PostPaidRefuelBean postPaidRefuelBean = new PostPaidRefuelBean(shopRefuelDetail, poPTransactionBean);
                    postPaidRefuelBean.setTimestampEndRefuel(creationTimestamp);
                    //poPTransactionBean.setRefuelBean(postPayedRefuelBean);
                    em.persist(postPaidRefuelBean);
                    poPTransactionBean.getRefuelBean().add(postPaidRefuelBean);
                }
            }

            if (shoppingCartList != null) {
                for (ShoppingCartDetail shoppingCart : shoppingCartList) {
                    PostPaidCartBean postPaidCartBean = new PostPaidCartBean(shoppingCart, poPTransactionBean);
                    em.persist(postPaidCartBean);
                    poPTransactionBean.getCartBean().add(postPaidCartBean);
                }

            }

            em.persist(poPTransactionBean);

            Integer sequenceID = 1;
            String oldStatus = "";
            Timestamp eventTimestamp = new Timestamp(new Date().getTime());

            //salvataggio dell'evento;
            PostPaidTransactionEventBean postPaidTransactionEventBean = new PostPaidTransactionEventBean();
            postPaidTransactionEventBean.setSequenceID(sequenceID);
            postPaidTransactionEventBean.setStateType(StatusHelper.POST_PAID_EVENT_TYPE_STANDARD);
            postPaidTransactionEventBean.setRequestID(requestID);
            postPaidTransactionEventBean.setEventTimestamp(eventTimestamp);
            postPaidTransactionEventBean.setEvent(StatusHelper.POST_PAID_EVENT_GFG_CREATE);
            postPaidTransactionEventBean.setTransactionBean(poPTransactionBean);
            postPaidTransactionEventBean.setResult("OK");
            postPaidTransactionEventBean.setNewState(StatusHelper.POST_PAID_STATUS_CREATED);
            postPaidTransactionEventBean.setOldState(oldStatus);

            em.persist(poPTransactionBean);
            em.persist(postPaidTransactionEventBean);

            //TODO verificare se inserire un primo stato nella tabella TransactionEvent;

            poPCreateShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_SUCCESS);
            poPCreateShopTransactionResponse.setMpTransactionID(mpTransactionID);

//    		PaymentTransactionResult paymentTransactionResult = new PaymentTransactionResult();
//    		paymentTransactionResult.setShopLogin(poPTransactionBean.getShopLogin());
//    		paymentTransactionResult.setAcquirerID(poPTransactionBean.getAcquirerID());
//    		paymentTransactionResult.setPaymentMode(poPTransactionBean.getPaymentMode());
//    		paymentTransactionResult.setShopTransactionID(poPTransactionBean.getMpTransactionStatus());
//    		paymentTransactionResult.setBankTransactionID(poPTransactionBean.getBankTansactionID());
//    		paymentTransactionResult.setAuthorizationCode(poPTransactionBean.getAuthorizationCode());
//    		paymentTransactionResult.setCurrency(poPTransactionBean.getCurrency());
//    		paymentTransactionResult.setAmount(poPTransactionBean.getAmount());
//    		paymentTransactionResult.setEventType(null);
//    		paymentTransactionResult.setTransactionResult(null);
//    		paymentTransactionResult.setErrorCode(null);
//    		paymentTransactionResult.setErrorDescription(null);

            userTransaction.commit();
//    		userTransaction.begin();
            //Notifica di pagamento al sistema forecourt; 
            //			String notifyResponse = this.notifyResponse(
            //					requestID,
            //					poPTransactionBean.getStationBean().getStationID(),
            //					poPTransactionBean.getMpTransactionID(),
            //					poPTransactionBean.getSrcTransactionID(),
            //					poPTransactionBean.getMpTransactionStatus(),
            //					null,// MOV OK
            //					forecourtPPService);
            //			
//			if(!notifyResponse.equals("MESSAGE_RECEIVED_200")){
//				poPTransactionBean.setNotificationPaid(true);
//				em.persist(poPTransactionBean);
//			}

            return poPCreateShopTransactionResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED Post Paid transaction creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
    /*
    private String notifyResponse(
    		String requestID,
    		String stationID,
    		String mpTransactionID,
    		String srcTransactionID,
    		String TransactionResult,
    		PaymentTransactionResult paymentTransactionResult,
    		ForecourtPostPaidServiceRemote forecourtPPService) {
    	// TODO Auto-generated method stub

        Boolean loyaltyCredits = false;
        List<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail> vouchers = new ArrayList<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail>(0);

    	SendMPTransactionResultMessageResponse sendMPTransactionResultMessageResponse = forecourtPPService.sendMPTransactionResult(requestID, stationID, mpTransactionID, srcTransactionID, TransactionResult, paymentTransactionResult, loyaltyCredits, vouchers);

    	if (!sendMPTransactionResultMessageResponse.getStatusCode().equals("MESSAGE_RECEIVED_200")){
    	}

    	return sendMPTransactionResultMessageResponse.getStatusCode();

    }
    */
}
