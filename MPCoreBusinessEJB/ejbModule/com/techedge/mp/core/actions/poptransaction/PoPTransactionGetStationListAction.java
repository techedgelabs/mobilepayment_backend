package com.techedge.mp.core.actions.poptransaction;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.TransactionService;
import com.techedge.mp.core.business.interfaces.ActiveCityInfo;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GetStationInfo;
import com.techedge.mp.core.business.interfaces.GetStationListResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CoordsHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;


@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPTransactionGetStationListAction {

	private static final double MILANO_LATITUDE = 45.464;

    private static final double MILANO_LONGITUDE = 9.188;

    private static final double ROMA_LATITUDE = 41.900;

    private static final double ROMA_LONGITUDE = 12.500;

    private static final String ITALY = "Italy";

    private static final String MILANO = "Milano";

    private static final String ROMA = "Roma";

    @Resource
	private EJBContext context;

	@PersistenceContext( unitName = "CrudPU" )
	private EntityManager em;

	@EJB
	private LoggerService loggerService;


	public PoPTransactionGetStationListAction() {
	}

    public GetStationListResponse execute(String requestID, String ticketID, Double userPositionLatitude, Double userPositionLongitude, Integer stationListMaxRange,
            Double stationListCorrectionCoefficient, TransactionService transactionService, ForecourtInfoServiceRemote forecourtInfoService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            GetStationListResponse getStationListResponse = new GetStationListResponse();

            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();
                getStationListResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_INVALID_TICKET);
                return getStationListResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                getStationListResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_INVALID_TICKET);
                return getStationListResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to get stations in status " + userStatus);

                userTransaction.commit();

                getStationListResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_UNAUTHORIZED);
                return getStationListResponse;
            }

            // Ricerca gli impianti in base alla distanza dalla posizione dell'utente
            List<StationBean> proxStationBeanList = QueryRepository.getAllActiveStationBeans(em);

            Integer romaStationCount = 0;
            Integer milanoStationCount = 0;
            
            String activeCity = "";
                        
            for (StationBean stationBean : proxStationBeanList) {

                // Controlla se l'impianto si trova all'interno del range di ricerca
                
                double distance = CoordsHelper.calculateDistance(userPositionLatitude, userPositionLongitude, stationBean.getLatitude(), stationBean.getLongitude());

                long roundedDistanceInt = Math.round(distance * stationListCorrectionCoefficient / 100);
                
                distance = roundedDistanceInt * 100;
                
                
                //stationCount per le citt� Roma e Milano
                String city = stationBean.getCity();
                //this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "execute","Station city", city, null);
                
                if (city.equalsIgnoreCase(ROMA)){
                    
                    romaStationCount ++;
                    
                }else if (city.equalsIgnoreCase(MILANO)){
                    
                    milanoStationCount ++;
                }
                
                
                if (distance < stationListMaxRange ) {

                    //this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "execute", "Stazione all'interno della distanza" , stationBean.getStationID(), "Nella citt� " + stationBean.getCity());
                    //System.out.println("distance: " + distance);
                    //System.out.println("Double distance: " + new Double(distance));
                    GetStationInfo stationInfo = new GetStationInfo(stationBean.getStationID(), stationBean.getAddress(), city, stationBean.getProvince(), stationBean.getCountry(), 
                            String.valueOf(stationBean.getLatitude()), String.valueOf(stationBean.getLongitude()), new Double(distance), stationBean.getPrepaidActive(), 
                            stationBean.getPostpaidActive(), stationBean.getShopActive(), stationBean.getBusinessActive()); 
                    getStationListResponse.getStationInfoList().add(stationInfo);
                    
                    if (activeCity.equals("")){
                        activeCity = setActiveCity(city);
                    }
                }
            }
    
            ActiveCityInfo activeRoma = new ActiveCityInfo();
            ActiveCityInfo activeMilano = new ActiveCityInfo();
            List<ActiveCityInfo> activeCityList = createActiveCityInfoList(activeCity, activeRoma, activeMilano, romaStationCount, milanoStationCount);

            List<GetStationInfo> stationListinfo = getStationListResponse.getStationInfoList();
            Collections.sort(stationListinfo, new Comparator<GetStationInfo>() {

                @Override
                public int compare(GetStationInfo o1, GetStationInfo o2) {
                    Double d = o1.getDistance() - o2.getDistance();
                    return d.intValue();
                }
                
            });
            
            
            getStationListResponse.setStationInfoList(stationListinfo);
            getStationListResponse.setActiveCityInfoList(activeCityList);
            
            if (getStationListResponse.getStationInfoList().isEmpty()) {

                // Non � stato rilevato nessun impianto in prossimit� dell'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Nessun impianto rilevato in prossimit� dell'utente");
            }
            
            if (getStationListResponse.getActiveCityInfoList().isEmpty()) {

                // Non � stato rilevato nessun impianto in prossimit� dell'utente
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Nessuna citt� attiva");

                userTransaction.commit();

                getStationListResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ACTIVE_CITY_LIST_NOT_FOUND);
                return getStationListResponse;
            }

            userTransaction.commit();

            getStationListResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_STATION_LIST_RETRIEVE_SUCCESS);
            return getStationListResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED Post Payed get stations with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }

    }


    private String setActiveCity(String city) {
        
        if (city.equals(ROMA)){
            return city;
        } else if(city.equals(MILANO)){
            return city;
        }
        
        return "";
    }


    private List<ActiveCityInfo> createActiveCityInfoList(String activeCity, ActiveCityInfo activeRoma, ActiveCityInfo activeMilano, Integer romaStationCount, Integer milanoStationCount) {
        
        List<ActiveCityInfo> activeCityInfoList = new ArrayList<ActiveCityInfo>();
        
        if (activeCity.equalsIgnoreCase(ROMA)){
            activeRoma.setCurrentCity(true);
            activeMilano.setCurrentCity(false);
        }else if (activeCity.equalsIgnoreCase(MILANO)){
            activeMilano.setCurrentCity(true);
            activeRoma.setCurrentCity(false);
        }else{
            activeRoma.setCurrentCity(false);
            activeMilano.setCurrentCity(false);
        }
        
        fillCommonFileds(activeRoma, romaStationCount, ROMA, activeCityInfoList, ROMA_LATITUDE,ROMA_LONGITUDE);
        fillCommonFileds(activeMilano, milanoStationCount, MILANO, activeCityInfoList, MILANO_LATITUDE, MILANO_LONGITUDE);
       
        return activeCityInfoList;
    }


    private void fillCommonFileds(ActiveCityInfo activeCityInfo, Integer stationCount, String city, List<ActiveCityInfo> activeCityInfoList, Double latitude, Double longitude) {
        
        activeCityInfo.setStationCount(stationCount);
        activeCityInfo.setCity(city);
        activeCityInfo.setProvince(Provinces.valueOf(city.toUpperCase()).toString());
        activeCityInfo.setCountry(ITALY);
        activeCityInfo.setLatitude(String.valueOf(latitude));
        activeCityInfo.setLongitude(String.valueOf(longitude));
        activeCityInfoList.add(activeCityInfo);
        
    }

    private enum Provinces {

        ROMA("RM"),
        MILANO("MI");
        
        private final String text;

        /**
         * @param text
         */
        private Provinces(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
        
    }
	
}
