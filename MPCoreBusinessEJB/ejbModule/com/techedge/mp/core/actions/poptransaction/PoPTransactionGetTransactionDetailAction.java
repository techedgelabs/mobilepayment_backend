package com.techedge.mp.core.actions.poptransaction;

import java.util.Date;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersServiceRemote;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PostPaidCart;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCredits;
import com.techedge.mp.core.business.interfaces.PostPaidLoadLoyaltyCreditsVoucher;
import com.techedge.mp.core.business.interfaces.PostPaidRefuel;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucher;
import com.techedge.mp.core.business.interfaces.PrePaidConsumeVoucherDetail;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetTransactionDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidTransactionAdditionalData;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherDetailHistoryBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherHistoryBean;
import com.techedge.mp.core.business.model.PrePaidLoadLoyaltyCreditsHistoryBean;
import com.techedge.mp.core.business.model.SurveyBean;
import com.techedge.mp.core.business.model.SurveySubmissionBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionHistoryAdditionalDataBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.TransactionStatusHistoryBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsVoucherHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryAdditionalDataBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.utilities.PanHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.TransactionFinalStatusConverter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPTransactionGetTransactionDetailAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    @EJB
    private ParametersServiceRemote parametersService;
    
    public PoPTransactionGetTransactionDetailAction() {}

    public PostPaidGetTransactionDetailResponse execute(String requestID, String ticketID, String mpTransactionID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();
            PostPaidGetTransactionDetailResponse popGetTransactionDetailResponse = new PostPaidGetTransactionDetailResponse();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_INVALID_TICKET);
                return popGetTransactionDetailResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_INVALID_TICKET);
                return popGetTransactionDetailResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus);

                userTransaction.commit();

                popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_UNAUTHORIZED);
                return popGetTransactionDetailResponse;
            }

            // Controlla se a mpTransactionID � associata una transazione post paid

            PostPaidTransactionBean poPTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, mpTransactionID);

            PostPaidTransactionHistoryBean poPTransactionHistoryBean = QueryRepository.findPostPaidTransactionHistoryBeanByMPId(em, mpTransactionID);

            if (poPTransactionHistoryBean == null) {

                if (poPTransactionBean != null) {

                    poPTransactionHistoryBean = new PostPaidTransactionHistoryBean(poPTransactionBean);
                }
            }

            if (poPTransactionHistoryBean == null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No postPaid transaction for mpTransactionId "
                        + mpTransactionID);

                // Controlla se a mpTransactionID � associata una transazione pre paid

                TransactionBean transactionBean = QueryRepository.findTransactionBeanById(em, mpTransactionID);

                TransactionHistoryBean transactionHistoryBean = QueryRepository.findTransactionHistoryBeanById(em, mpTransactionID);

                if (transactionHistoryBean == null) {

                    if (transactionBean != null) {

                        transactionHistoryBean = new TransactionHistoryBean(transactionBean);
                    }
                }

                if (transactionHistoryBean == null) {

                    // L'mpTransactionID inserito non corrisponde a nessuna transazione valida
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No prePayed transaction for mpTransactionId "
                            + mpTransactionID);

                    userTransaction.commit();

                    popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_NOT_RECOGNIZED);
                    return popGetTransactionDetailResponse;
                }
                else {

                    // Recupera le informazioni sul metodo di pagamento utilizzato
                    String pan = "";
                    System.out.println("paymentMethodId " + transactionHistoryBean.getPaymentMethodId());
                    System.out.println("paymentMethodType " + transactionHistoryBean.getPaymentMethodType());
                    
                    PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(em, transactionHistoryBean.getPaymentMethodId(), transactionHistoryBean.getPaymentMethodType());
                    if (paymentInfoBean != null) {
                        pan = paymentInfoBean.getPan();
                    }
                    else {
                        System.out.println("paymentInfoBean not found");
                    }
                    
                    // Trovata una transazione pre paid
                    popGetTransactionDetailResponse.setSourceType("FUEL");
                    popGetTransactionDetailResponse.setSourceStatus("PRE-PAY");

                    popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_SUCCESS);

                    popGetTransactionDetailResponse.setMpTransactionID(transactionHistoryBean.getTransactionID());
                    popGetTransactionDetailResponse.setTransactionType("OIL");

                    popGetTransactionDetailResponse.setStation(transactionHistoryBean.getStationBean().toStation());
                    popGetTransactionDetailResponse.setCreationTimestamp(transactionHistoryBean.getCreationTimestamp());
                    popGetTransactionDetailResponse.setAmount(transactionHistoryBean.getFinalAmount());
                    popGetTransactionDetailResponse.setBankTansactionID(transactionHistoryBean.getBankTansactionID());
                    popGetTransactionDetailResponse.setAuthorizationCode(transactionHistoryBean.getAuthorizationCode());
                    popGetTransactionDetailResponse.setPanCode(PanHelper.maskPan(pan));
                    popGetTransactionDetailResponse.setPaymentMethodId(transactionHistoryBean.getPaymentMethodId());
                    popGetTransactionDetailResponse.setPaymentMethodType(transactionHistoryBean.getPaymentMethodType());
                    popGetTransactionDetailResponse.setPaymentType(transactionHistoryBean.getPaymentType());
                    popGetTransactionDetailResponse.setAcquirerID(transactionHistoryBean.getAcquirerID());
                    popGetTransactionDetailResponse.setMpTransactionStatus(transactionHistoryBean.getFinalStatusType());
                    popGetTransactionDetailResponse.setGFGElectronicInvoiceID(transactionHistoryBean.getGFGElectronicInvoiceID());
                    
                    if (transactionHistoryBean.getPumpID() != null) {

                        popGetTransactionDetailResponse.setPumpID(transactionHistoryBean.getPumpID());
                        popGetTransactionDetailResponse.setPumpStatus(null);
                        popGetTransactionDetailResponse.setPumpNumber(transactionHistoryBean.getPumpNumber().toString());
                        popGetTransactionDetailResponse.setPumpFuelType(transactionHistoryBean.getProductDescription());
                    }

                    // Si estrae lo stato pi� recente associato alla transazione
                    Integer lastSequenceID = 0;
                    TransactionStatusHistoryBean lastTransactionStatusHistoryBean = null;

                    Set<TransactionStatusHistoryBean> transactionStatusHistoryData = transactionHistoryBean.getTransactionStatusHistoryBeanData();

                    for (TransactionStatusHistoryBean transactionStatusHistoryBean : transactionStatusHistoryData) {

                        Integer transactionStatusHistoryBeanSequenceID = transactionStatusHistoryBean.getSequenceID();
                        if (lastSequenceID < transactionStatusHistoryBeanSequenceID) {
                            lastSequenceID = transactionStatusHistoryBeanSequenceID;
                            lastTransactionStatusHistoryBean = transactionStatusHistoryBean;
                        }
                    }

                    if (lastTransactionStatusHistoryBean != null) {

                        popGetTransactionDetailResponse.setStatus(lastTransactionStatusHistoryBean.getStatus());
                        popGetTransactionDetailResponse.setSubStatus(lastTransactionStatusHistoryBean.getSubStatus());
                    }

                    PostPaidRefuel postPaidRefuel = new PostPaidRefuel();
                    postPaidRefuel.setFuelAmount(transactionHistoryBean.getFinalAmount());
                    postPaidRefuel.setFuelQuantity(transactionHistoryBean.getFuelQuantity());
                    postPaidRefuel.setFuelType(transactionHistoryBean.getProductDescription());
                    postPaidRefuel.setProductDescription(transactionHistoryBean.getProductDescription());
                    postPaidRefuel.setProductId(transactionHistoryBean.getProductID());
                    postPaidRefuel.setPumpId(transactionHistoryBean.getPumpID());
                    postPaidRefuel.setPumpNumber(transactionHistoryBean.getPumpNumber());

                    popGetTransactionDetailResponse.setUseVoucher(transactionHistoryBean.getUseVoucher());

                    popGetTransactionDetailResponse.getRefuelList().add(postPaidRefuel);

                    String statusTitle = null;
                    String statusDescription = null;

                    if (transactionHistoryBean.getFinalStatusType() != null) {
                        TransactionStatusHistoryBean transactionStatusHistoryBean = transactionHistoryBean.getLastTransactionStatusHistory();
                        String status = transactionHistoryBean.getFinalStatusType();
                        String subStatus = (transactionStatusHistoryBean != null ? transactionStatusHistoryBean.getStatus() : null);
                        statusTitle = TransactionFinalStatusConverter.getReceiptTitleText(status, subStatus);
                        statusDescription = TransactionFinalStatusConverter.getReceiptDescriptionText(status, subStatus);

                        if (status.equals(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL) && subStatus != null
                                && (subStatus.equals(StatusHelper.STATUS_PAYMENT_TRANSACTION_DELETED) || subStatus.equals(StatusHelper.STATUS_PAYMENT_AUTHORIZATION_DELETED))
                                || !status.equals(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL)) {
                            /*
                             * if (transactionHistoryBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_NOT_RECONCILIABLE) ||
                             * transactionHistoryBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_ERROR) ||
                             * transactionHistoryBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT) ||
                             * transactionHistoryBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_MISSING_PAYMENT)) {
                             */

                            popGetTransactionDetailResponse.setAmount(null);

                            if (popGetTransactionDetailResponse.getRefuelList().size() > 0) {
                                for (PostPaidRefuel tmpPostPaidRefuel : popGetTransactionDetailResponse.getRefuelList()) {
                                    tmpPostPaidRefuel.setFuelAmount(null);
                                    tmpPostPaidRefuel.setFuelQuantity(null);
                                    tmpPostPaidRefuel.setFuelType(null);
                                }
                            }

                        }
                    }

                    popGetTransactionDetailResponse.setStatusTitle(statusTitle);
                    popGetTransactionDetailResponse.setStatusDescription(statusDescription);

                    if (!transactionHistoryBean.getPrePaidConsumeVoucherHistoryBeanList().isEmpty()) {

                        System.out.println("Inizio lettura dati voucher");

                        for (PrePaidConsumeVoucherHistoryBean prePaidConsumeVoucherHistoryBean : transactionHistoryBean.getPrePaidConsumeVoucherHistoryBeanList()) {

                            PrePaidConsumeVoucher prePaidConsumeVoucher = new PrePaidConsumeVoucher();
                            prePaidConsumeVoucher.setCsTransactionID(prePaidConsumeVoucherHistoryBean.getCsTransactionID());
                            prePaidConsumeVoucher.setOperationID(prePaidConsumeVoucherHistoryBean.getOperationID());
                            prePaidConsumeVoucher.setOperationIDReversed(prePaidConsumeVoucherHistoryBean.getOperationIDReversed());
                            prePaidConsumeVoucher.setOperationType(prePaidConsumeVoucherHistoryBean.getOperationType());
                            prePaidConsumeVoucher.setRequestTimestamp(prePaidConsumeVoucherHistoryBean.getRequestTimestamp());
                            prePaidConsumeVoucher.setMarketingMsg(prePaidConsumeVoucherHistoryBean.getMarketingMsg());
                            prePaidConsumeVoucher.setWarningMsg(prePaidConsumeVoucherHistoryBean.getWarningMsg());
                            prePaidConsumeVoucher.setTotalConsumed(prePaidConsumeVoucherHistoryBean.getTotalConsumed());
                            prePaidConsumeVoucher.setReconciled(prePaidConsumeVoucherHistoryBean.getReconciled());
                            prePaidConsumeVoucher.setStatusCode(prePaidConsumeVoucherHistoryBean.getStatusCode());

                            for (PrePaidConsumeVoucherDetailHistoryBean prePaidConsumeVoucherDetailHistoryBean : prePaidConsumeVoucherHistoryBean.getPrePaidConsumeVoucherDetailHistoryBean()) {

                                System.out.println("Inizio inserimento voucher");

                                PrePaidConsumeVoucherDetail prePaidConsumeVoucherDetail = new PrePaidConsumeVoucherDetail();
                                prePaidConsumeVoucherDetail.setConsumedValue(prePaidConsumeVoucherDetailHistoryBean.getConsumedValue());
                                prePaidConsumeVoucherDetail.setExpirationDate(prePaidConsumeVoucherDetailHistoryBean.getExpirationDate());
                                prePaidConsumeVoucherDetail.setInitialValue(prePaidConsumeVoucherDetailHistoryBean.getInitialValue());
                                prePaidConsumeVoucherDetail.setPromoCode(prePaidConsumeVoucherDetailHistoryBean.getPromoCode());
                                prePaidConsumeVoucherDetail.setPromoDescription(prePaidConsumeVoucherDetailHistoryBean.getPromoDescription());
                                prePaidConsumeVoucherDetail.setPromoDoc(prePaidConsumeVoucherDetailHistoryBean.getPromoDoc());
                                prePaidConsumeVoucherDetail.setVoucherBalanceDue(prePaidConsumeVoucherDetailHistoryBean.getVoucherBalanceDue());
                                prePaidConsumeVoucherDetail.setVoucherCode(prePaidConsumeVoucherDetailHistoryBean.getVoucherCode());
                                prePaidConsumeVoucherDetail.setVoucherStatus(prePaidConsumeVoucherDetailHistoryBean.getVoucherStatus());
                                prePaidConsumeVoucherDetail.setVoucherType(prePaidConsumeVoucherDetailHistoryBean.getVoucherType());
                                prePaidConsumeVoucherDetail.setVoucherValue(prePaidConsumeVoucherDetailHistoryBean.getVoucherValue());

                                prePaidConsumeVoucher.getPrePaidConsumeVoucherDetail().add(prePaidConsumeVoucherDetail);

                                System.out.println("Fine inserimento voucher");

                            }

                            popGetTransactionDetailResponse.getPrePaidConsumeVoucherList().add(prePaidConsumeVoucher);

                        }
                        System.out.println("Fine lettura dati voucher");
                    }
                    
                    if (!transactionHistoryBean.getPrePaidLoadLoyaltyCreditsHistoryBeanList().isEmpty()) {

                        System.out.println("Inizio lettura dati loyalty");

                        for (PrePaidLoadLoyaltyCreditsHistoryBean prePaidLoadLoyaltyCreditsHistoryBean : transactionHistoryBean.getPrePaidLoadLoyaltyCreditsHistoryBeanList()) {

                            if (prePaidLoadLoyaltyCreditsHistoryBean != null
                                    && prePaidLoadLoyaltyCreditsHistoryBean.getCredits() != null) {

                                PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits = new PostPaidLoadLoyaltyCredits();
                                postPaidLoadLoyaltyCredits.setBalance(prePaidLoadLoyaltyCreditsHistoryBean.getBalance());
                                postPaidLoadLoyaltyCredits.setBalanceAmount(prePaidLoadLoyaltyCreditsHistoryBean.getBalanceAmount());
                                postPaidLoadLoyaltyCredits.setCardClassification(prePaidLoadLoyaltyCreditsHistoryBean.getCardClassification());
                                postPaidLoadLoyaltyCredits.setCardCodeIssuer(prePaidLoadLoyaltyCreditsHistoryBean.getCardCodeIssuer());
                                postPaidLoadLoyaltyCredits.setCardStatus(prePaidLoadLoyaltyCreditsHistoryBean.getCardStatus());
                                postPaidLoadLoyaltyCredits.setCardType(prePaidLoadLoyaltyCreditsHistoryBean.getCardType());
                                postPaidLoadLoyaltyCredits.setCredits(prePaidLoadLoyaltyCreditsHistoryBean.getCredits());
                                postPaidLoadLoyaltyCredits.setCsTransactionID(prePaidLoadLoyaltyCreditsHistoryBean.getCsTransactionID());
                                postPaidLoadLoyaltyCredits.setEanCode(prePaidLoadLoyaltyCreditsHistoryBean.getEanCode());
                                postPaidLoadLoyaltyCredits.setMarketingMsg(prePaidLoadLoyaltyCreditsHistoryBean.getMarketingMsg());
                                postPaidLoadLoyaltyCredits.setWarningMsg(prePaidLoadLoyaltyCreditsHistoryBean.getWarningMsg());
                                postPaidLoadLoyaltyCredits.setMessageCode(prePaidLoadLoyaltyCreditsHistoryBean.getMessageCode());
                                postPaidLoadLoyaltyCredits.setOperationID(prePaidLoadLoyaltyCreditsHistoryBean.getOperationID());
                                postPaidLoadLoyaltyCredits.setOperationType(prePaidLoadLoyaltyCreditsHistoryBean.getOperationType());
                                postPaidLoadLoyaltyCredits.setRequestTimestamp(prePaidLoadLoyaltyCreditsHistoryBean.getRequestTimestamp());
                                postPaidLoadLoyaltyCredits.setStatusCode(prePaidLoadLoyaltyCreditsHistoryBean.getStatusCode());

                                popGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList().add(postPaidLoadLoyaltyCredits);
                            }

                        }

                        System.out.println("Fine lettura dati loyalty");
                    }
                    
                    if (!transactionHistoryBean.getTransactionHistoryAdditionalDataBeanList().isEmpty()) {
                        
                        System.out.println("Inizio lettura dati multicard");
                        
                        for (TransactionHistoryAdditionalDataBean transactionHistoryAdditionalDataBean : transactionHistoryBean.getTransactionHistoryAdditionalDataBeanList()) {
                            
                            PostPaidTransactionAdditionalData postPaidTransactionAdditionalData  = new PostPaidTransactionAdditionalData();
                            postPaidTransactionAdditionalData.setDataKey(transactionHistoryAdditionalDataBean.getDataKey());
                            postPaidTransactionAdditionalData.setDataValue(transactionHistoryAdditionalDataBean.getDataValue());
                            System.out.println("Inserimento AdditionalData in postPaidTransaction");
                            System.out.println("Key:   " + postPaidTransactionAdditionalData.getDataKey());
                            System.out.println("Value: " + postPaidTransactionAdditionalData.getDataValue());
                            popGetTransactionDetailResponse.getPostPaidTransactionAdditionalDataList().add(postPaidTransactionAdditionalData);

                        }
                        
                        System.out.println("Fine lettura dati multicard");
                    }

                }
            }
            else {

                // Trovata una transazione post paid
                
                // Recupera le informazioni sul metodo di pagamento utilizzato
                String pan = "";
                PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(em, poPTransactionHistoryBean.getPaymentMethodId(), poPTransactionHistoryBean.getPaymentMethodType());
                if (paymentInfoBean != null) {
                    pan = paymentInfoBean.getPan();
                }

                String statusTitle = null;
                String statusDescription = null;

                if (poPTransactionHistoryBean.getMpTransactionStatus() != null) {
                    PostPaidTransactionEventHistoryBean poPTransactionEventHistoryBean = poPTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean();
                    String status = poPTransactionHistoryBean.getMpTransactionStatus();
                    String event = null;
                    String eventCode = null;
                    String eventResult = null;

                    if (poPTransactionEventHistoryBean != null) {
                        event = poPTransactionEventHistoryBean.getEvent();
                        eventCode = poPTransactionEventHistoryBean.getNewState();
                        eventResult = poPTransactionEventHistoryBean.getResult();
                    }
                    
                    statusTitle = TransactionFinalStatusConverter.getReceiptTitleText(status, event, eventCode, eventResult);
                    statusDescription = TransactionFinalStatusConverter.getReceiptDescriptionText(status, event, eventCode, eventResult);
                }

                popGetTransactionDetailResponse.setStatusTitle(statusTitle);
                popGetTransactionDetailResponse.setStatusDescription(statusDescription);

                popGetTransactionDetailResponse.setMpTransactionID(poPTransactionHistoryBean.getMpTransactionID());
                popGetTransactionDetailResponse.setMpTransactionStatus(poPTransactionHistoryBean.getMpTransactionStatus());
                popGetTransactionDetailResponse.setTransactionType(poPTransactionHistoryBean.getProductType());

                popGetTransactionDetailResponse.setStation(poPTransactionHistoryBean.getStationBean().toStation());
                popGetTransactionDetailResponse.setCreationTimestamp(poPTransactionHistoryBean.getCreationTimestamp());
                popGetTransactionDetailResponse.setAmount(poPTransactionHistoryBean.getAmount());
                popGetTransactionDetailResponse.setBankTansactionID(poPTransactionHistoryBean.getBankTansactionID());
                popGetTransactionDetailResponse.setAuthorizationCode(poPTransactionHistoryBean.getAuthorizationCode());
                popGetTransactionDetailResponse.setPanCode(PanHelper.maskPan(pan));
                popGetTransactionDetailResponse.setPaymentMethodId(poPTransactionHistoryBean.getPaymentMethodId());
                popGetTransactionDetailResponse.setPaymentMethodType(poPTransactionHistoryBean.getPaymentMethodType());
                popGetTransactionDetailResponse.setPaymentType(poPTransactionHistoryBean.getPaymentType());
                popGetTransactionDetailResponse.setAcquirerID(poPTransactionHistoryBean.getAcquirerID());

                popGetTransactionDetailResponse.setStatus(poPTransactionHistoryBean.getMpTransactionStatus());
                popGetTransactionDetailResponse.setSubStatus(null);
                popGetTransactionDetailResponse.setUseVoucher(poPTransactionHistoryBean.getUseVoucher());
                popGetTransactionDetailResponse.setGFGElectronicInvoiceID(poPTransactionHistoryBean.getGFGElectronicInvoiceID());
                //IF (OK)

                if (poPTransactionHistoryBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {

                    popGetTransactionDetailResponse.setSourceType("FUEL");

                    Set<PostPaidRefuelHistoryBean> postPaidRefuelHistoryBeanSet = poPTransactionHistoryBean.getRefuelHistoryBean();

                    PostPaidRefuelHistoryBean postPaidRefuelBeanHistoryFound = null;

                    for (PostPaidRefuelHistoryBean postPaidRefuelHistoryBean : postPaidRefuelHistoryBeanSet) {
                        postPaidRefuelBeanHistoryFound = postPaidRefuelHistoryBean;
                        break;
                    }

                    if (postPaidRefuelBeanHistoryFound != null) {

                        System.out.println("postPaidRefuelBeanFound - pumpID: " + postPaidRefuelBeanHistoryFound.getPumpId());

                        popGetTransactionDetailResponse.setPumpID(postPaidRefuelBeanHistoryFound.getPumpId());
                        popGetTransactionDetailResponse.setPumpStatus(null);
                        popGetTransactionDetailResponse.setPumpNumber(postPaidRefuelBeanHistoryFound.getPumpNumber().toString());
                        popGetTransactionDetailResponse.setPumpFuelType(postPaidRefuelBeanHistoryFound.getProductDescription());
                    }
                    else {

                        System.out.println("postPaidRefuelBeanFound null");
                    }
                }
                else {

                    popGetTransactionDetailResponse.setSourceType("SHOP");

                    popGetTransactionDetailResponse.setCashID(poPTransactionHistoryBean.getSourceID());

                    
                    
                    if (poPTransactionHistoryBean.getSourceNumber() != null) {
                        popGetTransactionDetailResponse.setCashNumber(poPTransactionHistoryBean.getSourceNumber().toString());
                    }
                    else {
                        popGetTransactionDetailResponse.setCashNumber(null);
                    }
                }

                popGetTransactionDetailResponse.setSourceStatus("POST-PAY");

                popGetTransactionDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_SUCCESS);

                if (poPTransactionHistoryBean.getCartHistoryBean() != null) {
                    for (PostPaidCartHistoryBean postPaidCartHistoryBean : poPTransactionHistoryBean.getCartHistoryBean()) {
                        PostPaidCart postPaidCart = new PostPaidCart();
                        postPaidCart.setAmount(postPaidCartHistoryBean.getAmount());
                        postPaidCart.setProductDescription(postPaidCartHistoryBean.getProductDescription());
                        postPaidCart.setProductId(postPaidCartHistoryBean.getProductId());
                        postPaidCart.setQuantity(postPaidCartHistoryBean.getQuantity());
                        popGetTransactionDetailResponse.getCartList().add(postPaidCart);
                    }
                }

                if (poPTransactionHistoryBean.getRefuelHistoryBean() != null) {
                    for (PostPaidRefuelHistoryBean postPaidRefuelHistoryBean : poPTransactionHistoryBean.getRefuelHistoryBean()) {
                        PostPaidRefuel postPaidRefuel = new PostPaidRefuel();
                        postPaidRefuel.setFuelAmount(postPaidRefuelHistoryBean.getFuelAmount());
                        postPaidRefuel.setFuelQuantity(postPaidRefuelHistoryBean.getFuelQuantity());
                        postPaidRefuel.setFuelType(postPaidRefuelHistoryBean.getFuelType());
                        postPaidRefuel.setProductDescription(postPaidRefuelHistoryBean.getProductDescription());
                        postPaidRefuel.setProductId(postPaidRefuelHistoryBean.getProductId());
                        postPaidRefuel.setPumpId(postPaidRefuelHistoryBean.getPumpId());
                        postPaidRefuel.setPumpNumber(postPaidRefuelHistoryBean.getPumpNumber());
                        popGetTransactionDetailResponse.getRefuelList().add(postPaidRefuel);
                    }
                }

                if (!poPTransactionHistoryBean.getPostPaidConsumeVoucherBeanList().isEmpty()) {

                    System.out.println("Inizio lettura dati voucher");

                    for (PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean : poPTransactionHistoryBean.getPostPaidConsumeVoucherBeanList()) {

                        PostPaidConsumeVoucher postPaidConsumeVoucher = new PostPaidConsumeVoucher();
                        postPaidConsumeVoucher.setCsTransactionID(postPaidConsumeVoucherHistoryBean.getCsTransactionID());
                        postPaidConsumeVoucher.setOperationID(postPaidConsumeVoucherHistoryBean.getOperationID());
                        postPaidConsumeVoucher.setOperationIDReversed(postPaidConsumeVoucherHistoryBean.getOperationIDReversed());
                        postPaidConsumeVoucher.setOperationType(postPaidConsumeVoucherHistoryBean.getOperationType());
                        postPaidConsumeVoucher.setRequestTimestamp(postPaidConsumeVoucherHistoryBean.getRequestTimestamp());
                        postPaidConsumeVoucher.setMarketingMsg(postPaidConsumeVoucherHistoryBean.getMarketingMsg());
                        postPaidConsumeVoucher.setWarningMsg(postPaidConsumeVoucherHistoryBean.getWarningMsg());
                        postPaidConsumeVoucher.setTotalConsumed(postPaidConsumeVoucherHistoryBean.getTotalConsumed());
                        postPaidConsumeVoucher.setReconciled(postPaidConsumeVoucherHistoryBean.getReconciled());
                        postPaidConsumeVoucher.setStatusCode(postPaidConsumeVoucherHistoryBean.getStatusCode());

                        for (PostPaidConsumeVoucherDetailHistoryBean postPaidConsumeVoucherDetailHistoryBean : postPaidConsumeVoucherHistoryBean.getPostPaidConsumeVoucherDetailHistoryBean()) {

                            System.out.println("Inizio inserimento voucher");

                            PostPaidConsumeVoucherDetail postPaidConsumeVoucherDetail = new PostPaidConsumeVoucherDetail();
                            postPaidConsumeVoucherDetail.setConsumedValue(postPaidConsumeVoucherDetailHistoryBean.getConsumedValue());
                            postPaidConsumeVoucherDetail.setExpirationDate(postPaidConsumeVoucherDetailHistoryBean.getExpirationDate());
                            postPaidConsumeVoucherDetail.setInitialValue(postPaidConsumeVoucherDetailHistoryBean.getInitialValue());
                            postPaidConsumeVoucherDetail.setPromoCode(postPaidConsumeVoucherDetailHistoryBean.getPromoCode());
                            postPaidConsumeVoucherDetail.setPromoDescription(postPaidConsumeVoucherDetailHistoryBean.getPromoDescription());
                            postPaidConsumeVoucherDetail.setPromoDoc(postPaidConsumeVoucherDetailHistoryBean.getPromoDoc());
                            postPaidConsumeVoucherDetail.setVoucherBalanceDue(postPaidConsumeVoucherDetailHistoryBean.getVoucherBalanceDue());
                            postPaidConsumeVoucherDetail.setVoucherCode(postPaidConsumeVoucherDetailHistoryBean.getVoucherCode());
                            postPaidConsumeVoucherDetail.setVoucherStatus(postPaidConsumeVoucherDetailHistoryBean.getVoucherStatus());
                            postPaidConsumeVoucherDetail.setVoucherType(postPaidConsumeVoucherDetailHistoryBean.getVoucherType());
                            postPaidConsumeVoucherDetail.setVoucherValue(postPaidConsumeVoucherDetailHistoryBean.getVoucherValue());

                            postPaidConsumeVoucher.getPostPaidConsumeVoucherDetail().add(postPaidConsumeVoucherDetail);

                            System.out.println("Fine inserimento voucher");

                        }

                        popGetTransactionDetailResponse.getPostPaidConsumeVoucherList().add(postPaidConsumeVoucher);

                    }
                    System.out.println("Fine lettura dati voucher");
                }

                if (!poPTransactionHistoryBean.getPostPaidLoadLoyaltyCreditsBeanList().isEmpty()) {

                    System.out.println("Inizio lettura dati loyalty");

                    for (PostPaidLoadLoyaltyCreditsHistoryBean postPaidLoadLoyaltyCreditsHistoryBean : poPTransactionHistoryBean.getPostPaidLoadLoyaltyCreditsBeanList()) {

                        if (postPaidLoadLoyaltyCreditsHistoryBean != null
                                && postPaidLoadLoyaltyCreditsHistoryBean.getCredits() != null
                                && (postPaidLoadLoyaltyCreditsHistoryBean.getPostPaidTransactionHistoryBean().getMpTransactionStatus().equals("PAID") || postPaidLoadLoyaltyCreditsHistoryBean.getPostPaidTransactionHistoryBean().getMpTransactionStatus().equals("0013"))) {

                            PostPaidLoadLoyaltyCredits postPaidLoadLoyaltyCredits = new PostPaidLoadLoyaltyCredits();
                            postPaidLoadLoyaltyCredits.setBalance(postPaidLoadLoyaltyCreditsHistoryBean.getBalance());
                            postPaidLoadLoyaltyCredits.setBalanceAmount(postPaidLoadLoyaltyCreditsHistoryBean.getBalanceAmount());
                            postPaidLoadLoyaltyCredits.setCardClassification(postPaidLoadLoyaltyCreditsHistoryBean.getCardClassification());
                            postPaidLoadLoyaltyCredits.setCardCodeIssuer(postPaidLoadLoyaltyCreditsHistoryBean.getCardCodeIssuer());
                            postPaidLoadLoyaltyCredits.setCardStatus(postPaidLoadLoyaltyCreditsHistoryBean.getCardStatus());
                            postPaidLoadLoyaltyCredits.setCardType(postPaidLoadLoyaltyCreditsHistoryBean.getCardType());
                            postPaidLoadLoyaltyCredits.setCredits(postPaidLoadLoyaltyCreditsHistoryBean.getCredits());
                            postPaidLoadLoyaltyCredits.setCsTransactionID(postPaidLoadLoyaltyCreditsHistoryBean.getCsTransactionID());
                            postPaidLoadLoyaltyCredits.setEanCode(postPaidLoadLoyaltyCreditsHistoryBean.getEanCode());
                            postPaidLoadLoyaltyCredits.setMarketingMsg(postPaidLoadLoyaltyCreditsHistoryBean.getMarketingMsg());
                            postPaidLoadLoyaltyCredits.setWarningMsg(postPaidLoadLoyaltyCreditsHistoryBean.getWarningMsg());
                            postPaidLoadLoyaltyCredits.setMessageCode(postPaidLoadLoyaltyCreditsHistoryBean.getMessageCode());
                            postPaidLoadLoyaltyCredits.setOperationID(postPaidLoadLoyaltyCreditsHistoryBean.getOperationID());
                            postPaidLoadLoyaltyCredits.setOperationType(postPaidLoadLoyaltyCreditsHistoryBean.getOperationType());
                            postPaidLoadLoyaltyCredits.setRequestTimestamp(postPaidLoadLoyaltyCreditsHistoryBean.getRequestTimestamp());
                            postPaidLoadLoyaltyCredits.setStatusCode(postPaidLoadLoyaltyCreditsHistoryBean.getStatusCode());

                            if (postPaidLoadLoyaltyCreditsHistoryBean.getPostPaidLoadLoyaltyCreditsVoucherHistoryBean() != null) {
                                for (PostPaidLoadLoyaltyCreditsVoucherHistoryBean postPaidLoadLoyaltyCreditsVoucherHistoryBean : postPaidLoadLoyaltyCreditsHistoryBean.getPostPaidLoadLoyaltyCreditsVoucherHistoryBean()) {
                                    PostPaidLoadLoyaltyCreditsVoucher postPaidLoadLoyaltyCreditsVoucher = postPaidLoadLoyaltyCreditsVoucherHistoryBean.toPostPaidLoadLoyaltyCreditsVoucher();
                                    postPaidLoadLoyaltyCredits.getPostPaidLoadLoyaltyCreditsVoucherList().add(postPaidLoadLoyaltyCreditsVoucher);
                                }
                            }
                            
                            popGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList().add(postPaidLoadLoyaltyCredits);
                        }

                    }

                    System.out.println("Fine lettura dati loyalty");
                }
                
                if (!poPTransactionHistoryBean.getPostPaidTransactionHistoryAdditionalDataBeanList().isEmpty()) {
                    
                    System.out.println("Inizio lettura dati multicard");
                    
                    for (PostPaidTransactionHistoryAdditionalDataBean postPaidTransactionHistoryAdditionalDataBean : poPTransactionHistoryBean.getPostPaidTransactionHistoryAdditionalDataBeanList()) {
                        
                        PostPaidTransactionAdditionalData postPaidTransactionAdditionalData  = new PostPaidTransactionAdditionalData();
                        postPaidTransactionAdditionalData.setDataKey(postPaidTransactionHistoryAdditionalDataBean.getDataKey());
                        postPaidTransactionAdditionalData.setDataValue(postPaidTransactionHistoryAdditionalDataBean.getDataValue());
                        System.out.println("Inserimento AdditionalData in postPaidTransaction");
                        System.out.println("Key:   " + postPaidTransactionAdditionalData.getDataKey());
                        System.out.println("Value: " + postPaidTransactionAdditionalData.getDataValue());
                        popGetTransactionDetailResponse.getPostPaidTransactionAdditionalDataList().add(postPaidTransactionAdditionalData);

                    }
                    
                    System.out.println("Fine lettura dati multicard");
                }

                if (poPTransactionHistoryBean.getMpTransactionStatus() != null) {
                    //PostPaidTransactionEventHistoryBean poPTransactionEventHistoryBean = poPTransactionHistoryBean.getLastPostPaidTransactionEventHistory();
                    String status = poPTransactionHistoryBean.getMpTransactionStatus();
                    //String subStatus = (poPTransactionEventHistoryBean != null ? poPTransactionEventHistoryBean.getEvent() : null);

                    if (!status.equals(StatusHelper.POST_PAID_FINAL_STATUS_PAID)) {
                        popGetTransactionDetailResponse.setAmount(null);

                        if (popGetTransactionDetailResponse.getRefuelList().size() > 0) {
                            for (PostPaidRefuel tmpPostPaidRefuel : popGetTransactionDetailResponse.getRefuelList()) {
                                tmpPostPaidRefuel.setFuelAmount(null);
                                tmpPostPaidRefuel.setFuelQuantity(null);
                                //tmpPostPaidRefuel.setFuelType(null);
                            }
                        }

                        if (popGetTransactionDetailResponse.getCartList().size() > 0) {
                            for (PostPaidCart tmpPostPaidCart : popGetTransactionDetailResponse.getCartList()) {
                                tmpPostPaidCart.setAmount(null);
                                //tmpPostPaidCart.setQuantity(null);
                            }
                        }

                        if (popGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList().size() > 0) {
                            popGetTransactionDetailResponse.getPostPaidLoadLoyaltyCreditsList().clear();
                        }

                        if (popGetTransactionDetailResponse.getPostPaidConsumeVoucherList().size() > 0) {
                            popGetTransactionDetailResponse.getPostPaidConsumeVoucherList().clear();

                        }
                    }

                }

            }
            
            /*
            SurveyBean surveyBean = QueryRepository.findSurveyActive(em, new Date());
            
            if (surveyBean != null) {
            
                SurveySubmissionBean surveySubmissionBean = QueryRepository.findSurveySubmissionByKey(em, popGetTransactionDetailResponse.getMpTransactionID());
    
                if (surveySubmissionBean == null) {
                    try {
                        String surveyUrl = parametersService.getParamValue("SURVEY_RECEIPT_URL");
                        popGetTransactionDetailResponse.setSurveyUrl(surveyUrl + popGetTransactionDetailResponse.getMpTransactionID());
                        
                    }
                    catch (Exception e) {
                        loggerService.log(ErrorLevel.WARN, this.getClass().getSimpleName(), null, null, "exception", e.getMessage());
                    }
                }

            }
            */
            
            
            String surveyCode = parametersService.getParamValue("SURVEY_TRANSACTION_CODE");
            
            SurveyBean surveyBean = QueryRepository.findSurveyByCode(em, surveyCode);
            
            Date today = new Date();
            
            if (surveyBean != null && surveyBean.getStatus() == 1 && surveyBean.getStartDate().getTime() <= today.getTime() && surveyBean.getEndDate().getTime() >= today.getTime()) {
                
                SurveySubmissionBean surveySubmissionBean = QueryRepository.findSurveySubmissionByKey(em, popGetTransactionDetailResponse.getMpTransactionID());
    
                if (surveySubmissionBean == null) {
                    
                    try {
                        
                        String surveyUrl = parametersService.getParamValue("SURVEY_RECEIPT_URL");
                        popGetTransactionDetailResponse.setSurveyUrl(surveyUrl + popGetTransactionDetailResponse.getMpTransactionID());                        
                    }
                    catch (Exception e) {
                        
                        loggerService.log(ErrorLevel.WARN, this.getClass().getSimpleName(), null, null, "exception", e.getMessage());
                    }
                }
            }
            
            userTransaction.commit();

            return popGetTransactionDetailResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED Post Payed transaction get with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
    /*
     * private ReceiptData addReceiptData(String key, String label, String unit, String value, int position, String parent, String link) {
     * 
     * ReceiptData receiptData = new ReceiptData();
     * 
     * receiptData.setLabel(label);
     * receiptData.setKey(key);
     * receiptData.setUnit(unit);
     * receiptData.setValue(value);
     * receiptData.setPosition(position);
     * receiptData.setParent(parent);
     * receiptData.setLink(link);
     * 
     * return receiptData;
     * }
     */
}
