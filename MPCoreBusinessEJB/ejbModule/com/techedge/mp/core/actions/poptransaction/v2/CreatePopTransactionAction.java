package com.techedge.mp.core.actions.poptransaction.v2;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.interfaces.CashInfo;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.postpaid.CoreProductDetail;
import com.techedge.mp.core.business.interfaces.postpaid.CorePumpDetail;
import com.techedge.mp.core.business.interfaces.postpaid.GetSourceDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCartData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetLastRefuelMessageResponse;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class CreatePopTransactionAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public CreatePopTransactionAction() {}

    public GetSourceDetailResponse execute(String requestID, String ticketID, String sourceID, Integer reconciliationMaxAttemps,
            ForecourtPostPaidServiceRemote forecourtPostPaidServiceRemote, UnavailabilityPeriodService unavailabilityPeriodService, List<String> userBlockExceptionList,
            Long retryTime) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            GetSourceDetailResponse getSourceDetailResponse = new GetSourceDetailResponse();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_INVALID_TICKET);
                return getSourceDetailResponse;
            }
            //          
            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_INVALID_TICKET);
                return getSourceDetailResponse;
            }

            if (userBlockExceptionList.contains(userBean.getPersonalDataBean().getSecurityDataEmail())) {

                System.out.println(userBean.getPersonalDataBean().getSecurityDataEmail() + " presente in lista utenti con eccezioni blocco");
            }
            else {
                // Controllo su disponibilit� del servizio di creazione transazioni postpaid
                ServiceAvailabilityData serviceAvailabilityData = unavailabilityPeriodService.retrieveServiceAvailability("CREATE_TRANSACTION_POSTPAID", new Date());
                if (serviceAvailabilityData != null) {

                    // Servizio non disponibile
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "CreatePopTransaction unavailable");

                    userTransaction.commit();

                    getSourceDetailResponse.setStatusCode(serviceAvailabilityData.getStatusCode());
                    return getSourceDetailResponse;
                }
            }

            //          // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus);

                userTransaction.commit();

                getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_UNAUTHORIZED);
                return getSourceDetailResponse;
            }

            PaymentInfoBean paymentInfoVoucherBean = userBean.getVoucherPaymentMethod();

            if (paymentInfoVoucherBean == null || paymentInfoVoucherBean.getPin() == null || paymentInfoVoucherBean.getPin().equals("")) {

                // Errore Pin non ancora inserito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");

                userTransaction.commit();
                getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_PAYMENT_WRONG);
                return getSourceDetailResponse;
            }

            Boolean creditCardFound = false;

            for (PaymentInfoBean paymentInfo : userBean.getPaymentData()) {
                if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) && paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {
                    creditCardFound = true;
                    System.out.println("Credit Card found");
                }
            }

            if (!creditCardFound) {
                // Il metodo di pagamento selezionato non pu� essere utilizzato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Credit card not found");

                userTransaction.commit();
                getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_UNAUTHORIZED);
                return getSourceDetailResponse;
            }

            if (sourceID.startsWith("C")) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "SourceID can't be a cash (" + sourceID + ")");

                userTransaction.commit();
                getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_STATION_PUMP_NOT_FOUND);
                return getSourceDetailResponse;
            }

            getSourceDetailResponse.setObjectType("FUEL");
            getSourceDetailResponse.setObjectStatus("POST-PAY");

            Boolean attemptRetry = false;

            GetLastRefuelMessageResponse getLastRefuelMessageResponse = forecourtPostPaidServiceRemote.getLastRefuel(requestID, sourceID);

            for (int i = 0; i < 2; i++) {
                if (getLastRefuelMessageResponse.getStatusCode().equals("PUMP_IN_FUELLING_500")) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Erogatore occupato per rifornimento");

                    userTransaction.commit();
                    getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_PUMP_IN_FUELLING);
                    return getSourceDetailResponse;
                }

                if (getLastRefuelMessageResponse.getRefuelDetail() == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "getLastRefuel->RefuelDetail is null");

                    if (attemptRetry == Boolean.FALSE) {
                        attemptRetry = Boolean.TRUE;
                        Thread.sleep(retryTime);
                    }
                    else {

                        userTransaction.commit();
                        getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_NOT_FOUND);
                        return getSourceDetailResponse;
                    }
                }
            }
            List<PostPaidTransactionBean> postPaidTransactionBeanListTest = QueryRepository.findPostPaidTransactionBeanBySRCId(em,
                    getLastRefuelMessageResponse.getSrcTransactionID());

            if (postPaidTransactionBeanListTest != null && !postPaidTransactionBeanListTest.isEmpty()) {

                System.out.println("Trovata una o pi� transazioni con stesso srcTransactinId");
            }

            List<PostPaidTransactionBean> postPaidTransactionBeanList = QueryRepository.findPostPaidTransactionBeanBySRCIdAndSource(em,
                    getLastRefuelMessageResponse.getSrcTransactionID(), getLastRefuelMessageResponse.getPumpID());

            PostPaidTransactionBean postPaidTransactionBean = null;
            if (postPaidTransactionBeanList != null && postPaidTransactionBeanList.size() > 0) {
                for (PostPaidTransactionBean postPaidTransactionBeanTemp : postPaidTransactionBeanList) {
                    //1) se ci sono N transazioni in stato ONHOLD  la transazione viene settata come CANCELLED e restituita una delle transazioni;
                    //2) se la transazione � in stato ONHOLD  --> la transazione viene restituita; (UTENZA in questa fase non � associata)
                    //3) se la transazione non esiste viene creata;
                    if (postPaidTransactionBean == null && postPaidTransactionBeanTemp.getMpTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)) {
                        postPaidTransactionBean = postPaidTransactionBeanTemp;
                        System.out.println("POP Refuel srcTransactionID: " + getLastRefuelMessageResponse.getSrcTransactionID() + " transaction exists");
                        continue;
                    }

                    if (postPaidTransactionBeanTemp.getMpTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)) { //1)
                        postPaidTransactionBeanTemp.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);
                        em.persist(postPaidTransactionBeanTemp);
                        System.out.println("POP Refuel srcTransactionID: " + getLastRefuelMessageResponse.getSrcTransactionID() + " transaction exists(more than one)");
                        System.out.println("POP Refuel srcTransactionID: " + getLastRefuelMessageResponse.getSrcTransactionID() + " transaction exists. MPtransaction "
                                + postPaidTransactionBeanTemp.getMpTransactionID() + "sets to CANCELLED");
                    }
                }

                //crea la transazione virtuale se le precedenti non sono corrette
                if (postPaidTransactionBean == null) {
                    postPaidTransactionBean = this.createTransaction(requestID, reconciliationMaxAttemps, getLastRefuelMessageResponse);
                    System.out.println("POP Refuel: " + getLastRefuelMessageResponse.getSrcTransactionID() + " transaction doesn't exist 1");
                }
            }
            else {
                //crea la transazione virtuale se non esiste
                postPaidTransactionBean = this.createTransaction(requestID, reconciliationMaxAttemps, getLastRefuelMessageResponse);
                System.out.println("POP Refuel: " + getLastRefuelMessageResponse.getSrcTransactionID() + " transaction doesn't exist 2");
            }

            getSourceDetailResponse.setTransactionID(postPaidTransactionBean.getMpTransactionID());
            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_SUCCESS);

            getSourceDetailResponse = this.getPostPaidTransaction(postPaidTransactionBean.getMpTransactionID(), getSourceDetailResponse);

            userTransaction.commit();

            return getSourceDetailResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                e.printStackTrace();
            }
            catch (SecurityException e) {
                e.printStackTrace();
            }
            catch (SystemException e) {
                e.printStackTrace();
            }

            String message = "FAILED Post Paid transaction creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

    private PostPaidTransactionBean createTransaction(String requestID, Integer reconciliationMaxAttempts, GetLastRefuelMessageResponse getLastRefuelMessageResponse) {

        //crea transazione virtuale
        StationBean stationBean = QueryRepository.findStationBeanById(em, getLastRefuelMessageResponse.getStationDetail().getStationID());

        if (stationBean == null) {
            //TODO DA GESTIRE
            // Lo stationID inserito non corrisponde a nessuna stazione valida

            //          this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No station for stationID " + stationID );
            //          userTransaction.commit();
            //          poPCreateShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
            //          return poPCreateShopTransactionResponse;
        }

        //TODO verificare che non ci sia una transazione aperta\attiva;

        String shopLogin = stationBean.getOilShopLogin();
        String acquirerID = stationBean.getOilAcquirerID();
        String currency = null;
        String groupAcquirer = null;
        String encodedSecretKey = null;

        if (stationBean.getDataAcquirer() != null) {
            shopLogin = stationBean.getDataAcquirer().getApiKey();
            acquirerID = stationBean.getDataAcquirer().getAcquirerID();
            groupAcquirer = stationBean.getDataAcquirer().getGroupAcquirer();
            encodedSecretKey = stationBean.getDataAcquirer().getEncodedSecretKey();
            currency = stationBean.getDataAcquirer().getCurrency();
        }

        String mpTransactionID = new IdGenerator().generateId(16).substring(0, 32);

        Date now = new Date();
        Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());

        // Crea la transazione con i dati ricevuti in input
        PostPaidTransactionBean postPaidTransactionBean = new PostPaidTransactionBean();
        postPaidTransactionBean.setStationBean(stationBean);
        postPaidTransactionBean.setRequestID(requestID);
        postPaidTransactionBean.setSource(PostPaidSourceType.SELF.getCode());
        postPaidTransactionBean.setCreationTimestamp(creationTimestamp);
        postPaidTransactionBean.setSrcTransactionID(getLastRefuelMessageResponse.getSrcTransactionID());
        postPaidTransactionBean.setSourceID(getLastRefuelMessageResponse.getPumpID());
        postPaidTransactionBean.setMpTransactionID(mpTransactionID);
        postPaidTransactionBean.setStationBean(stationBean);
        postPaidTransactionBean.setShopLogin(shopLogin);
        postPaidTransactionBean.setAcquirerID(acquirerID);
        postPaidTransactionBean.setProductType("OIL");
        postPaidTransactionBean.setAmount(getLastRefuelMessageResponse.getRefuelDetail().getAmount());
        postPaidTransactionBean.setCurrency(currency);
        postPaidTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD);
        postPaidTransactionBean.setNotificationCreated(true);
        postPaidTransactionBean.setNotificationPaid(false);
        postPaidTransactionBean.setNotificationUser(false);
        postPaidTransactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
        postPaidTransactionBean.setGroupAcquirer(groupAcquirer);
        postPaidTransactionBean.setEncodedSecretKey(encodedSecretKey);

        em.persist(postPaidTransactionBean);

        if (getLastRefuelMessageResponse.getRefuelDetail() != null) {

            PostPaidRefuelBean postPaidRefuelBean = new PostPaidRefuelBean();
            postPaidRefuelBean.setFuelType(getLastRefuelMessageResponse.getRefuelDetail().getFuelType());
            postPaidRefuelBean.setFuelQuantity(getLastRefuelMessageResponse.getRefuelDetail().getFuelQuantity());
            postPaidRefuelBean.setFuelAmount(getLastRefuelMessageResponse.getRefuelDetail().getAmount());
            postPaidRefuelBean.setProductDescription(getLastRefuelMessageResponse.getRefuelDetail().getProductDescription());
            postPaidRefuelBean.setProductId(getLastRefuelMessageResponse.getRefuelDetail().getProductID());
            postPaidRefuelBean.setPumpId(getLastRefuelMessageResponse.getPumpID());
            postPaidRefuelBean.setPumpNumber(Integer.valueOf(getLastRefuelMessageResponse.getPumpNumber()));
            postPaidRefuelBean.setRefuelMode(getLastRefuelMessageResponse.getRefuelMode());

            Timestamp endrefueltimeTimestamp = Timestamp.valueOf(getLastRefuelMessageResponse.getRefuelDetail().getTimestampEndRefuel());
            endrefueltimeTimestamp.getTime();
            Date endrefuel = new Date(endrefueltimeTimestamp.getTime());
            postPaidRefuelBean.setTimestampEndRefuel(endrefuel);
            postPaidRefuelBean.setTransactionBean(postPaidTransactionBean);

            postPaidTransactionBean.setSourceNumber(getLastRefuelMessageResponse.getPumpNumber());

            postPaidTransactionBean.getRefuelBean().add(postPaidRefuelBean);

            em.persist(postPaidRefuelBean);
        }

        return postPaidTransactionBean;
    }

    private GetSourceDetailResponse getPostPaidTransaction(String mpTransactionID, GetSourceDetailResponse getSourceDetailResponse) {

        PostPaidTransactionBean postPaidTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, mpTransactionID);
        if (postPaidTransactionBean == null) {
            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_NOT_RECOGNIZED);
            return getSourceDetailResponse;
        }

        //TODO occorre inserire la verifica se la transazione � gi� abilitata. Cosa Fare?

        //IF (OK)
        getSourceDetailResponse.setTransactionData(postPaidTransactionBean.toPoPTransactionData());
        getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_SUCCESS);
        getSourceDetailResponse.setTransactionID(postPaidTransactionBean.getMpTransactionID());
        getSourceDetailResponse.setTransactionStatus(postPaidTransactionBean.getMpTransactionStatus());
        getSourceDetailResponse.setStationID(postPaidTransactionBean.getStationBean().getStationID());
        getSourceDetailResponse.setStationName(null);
        getSourceDetailResponse.setStationAddress(postPaidTransactionBean.getStationBean().getAddress());
        getSourceDetailResponse.setStationCity(postPaidTransactionBean.getStationBean().getCity());
        getSourceDetailResponse.setStationProvince(postPaidTransactionBean.getStationBean().getProvince());
        getSourceDetailResponse.setStationCountry(postPaidTransactionBean.getStationBean().getCountry());
        getSourceDetailResponse.setStationLatitude(postPaidTransactionBean.getStationBean().getLatitude());
        getSourceDetailResponse.setStationLongitude(postPaidTransactionBean.getStationBean().getLongitude());
        getSourceDetailResponse.setAmount(postPaidTransactionBean.getAmount());

        PostPaidCartData postPaidCartData = new PostPaidCartData();

        if (postPaidTransactionBean.getCartBean() != null) {

            for (PostPaidCartBean postPaidCartBean : postPaidTransactionBean.getCartBean()) {

                postPaidCartData = new PostPaidCartData();
                postPaidCartData.setAmount(postPaidCartBean.getAmount());
                postPaidCartData.setProductDescription(postPaidCartBean.getProductDescription());
                postPaidCartData.setProductId(postPaidCartBean.getProductId());
                postPaidCartData.setQuantity(postPaidCartBean.getQuantity());
                getSourceDetailResponse.getPostPaidCartDataList().add(postPaidCartData);
            }
        }

        PostPaidRefuelData postPaidRefuelData = new PostPaidRefuelData();

        if (postPaidTransactionBean.getRefuelBean() != null) {

            for (PostPaidRefuelBean postPaidRefuelBean : postPaidTransactionBean.getRefuelBean()) {

                postPaidRefuelData = new PostPaidRefuelData();
                postPaidRefuelData.setFuelAmount(postPaidRefuelBean.getFuelAmount());
                postPaidRefuelData.setFuelQuantity(postPaidRefuelBean.getFuelQuantity());
                postPaidRefuelData.setFuelType(postPaidRefuelBean.getFuelType());
                postPaidRefuelData.setProductDescription(postPaidRefuelBean.getProductDescription());
                postPaidRefuelData.setProductId(postPaidRefuelBean.getProductId());
                postPaidRefuelData.setPumpId(postPaidRefuelBean.getPumpId());

                getSourceDetailResponse.getPostPaidRefuelDataList().add(postPaidRefuelData);

                if (postPaidTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {

                    CorePumpDetail corePumpDetail = new CorePumpDetail();
                    corePumpDetail.setPumpID(postPaidRefuelBean.getPumpId());

                    // TODO rimuovere se non pi� necessario
                    if (postPaidRefuelBean.getPumpNumber() != null) {
                        corePumpDetail.setPumpNumber(postPaidRefuelBean.getPumpNumber().toString());
                    }
                    else {
                        corePumpDetail.setPumpNumber(null);
                    }
                    corePumpDetail.setPumpStatus(null);
                    CoreProductDetail coreProductDetail;
                    coreProductDetail = new CoreProductDetail();
                    coreProductDetail.setFuelType(postPaidRefuelBean.getFuelType());
                    corePumpDetail.getProductDetails().add(coreProductDetail);

                    getSourceDetailResponse.setPumpInfo(corePumpDetail);
                }
            }
        }

        if (postPaidTransactionBean.getSource().equals("CASH REGISTER")) {

            CashInfo cashInfo = new CashInfo();

            cashInfo.setCashId(postPaidTransactionBean.getSourceID());

            if (postPaidTransactionBean.getSourceNumber() != null) {
                cashInfo.setNumber(postPaidTransactionBean.getSourceNumber());
            }

            getSourceDetailResponse.setCashInfo(cashInfo);
        }

        return getSourceDetailResponse;
    }

}
