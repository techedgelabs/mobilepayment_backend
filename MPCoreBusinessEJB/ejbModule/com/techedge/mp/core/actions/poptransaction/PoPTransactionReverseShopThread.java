package com.techedge.mp.core.actions.poptransaction;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.ejb.EJBException;
import javax.persistence.EntityManager;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetSrcTransactionStatusMessageResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

public class PoPTransactionReverseShopThread implements Runnable {

    private PostPaidTransactionBean        postPaidTransactionBean;
    private String                         requestID;
    private ForecourtPostPaidServiceRemote forecourtPPService;
    private GPServiceRemote                gpService;
    private LoggerService                  loggerService;
    private UserTransaction                userTransaction;
    private FidelityServiceRemote          fidelityService;
    private EmailSenderRemote              emailSender;
    private EntityManager                  em;
    private String                         proxyHost;
    private String                         proxyPort;
    private String                         proxyNoHosts;
    private Integer                        sequenceID;
    private String                         newStatus;
    private String                         oldStatus;

    private Boolean                        isNewFlow;

    private UserCategoryService            userCatService;
    
    private String                         decodedSecretKey;

    private StringSubstitution             stringSubstitution;

    public PoPTransactionReverseShopThread(PostPaidTransactionBean postPaidTransactionBean, String requestID, ForecourtPostPaidServiceRemote forecourtPPService, GPServiceRemote gpService, LoggerService loggerService, UserTransaction userTransaction, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, EntityManager em, String proxyHost, String proxyPort, String proxyNoHosts, UserCategoryService userCategoryService, StringSubstitution stringSubstitution, String decodedSecretKey) {

        System.out.println("PoPTransactionReverseShopThread constructor start");

        this.postPaidTransactionBean = postPaidTransactionBean;
        this.requestID = requestID;
        this.forecourtPPService = forecourtPPService;
        this.gpService = gpService;
        this.loggerService = loggerService;
        this.userTransaction = userTransaction;
        this.fidelityService = fidelityService;
        this.emailSender = emailSender;
        this.em = em;
        this.proxyHost = proxyHost;
        this.proxyPort = proxyPort;
        this.proxyNoHosts = proxyNoHosts;
        this.stringSubstitution = stringSubstitution;
        this.decodedSecretKey = decodedSecretKey;

        Boolean isNewFlow = false;
        userCatService = userCategoryService;
        isNewFlow = userCategoryService.isUserTypeInUserCategory(postPaidTransactionBean.getUserBean().getUserType(), UserCategoryType.NEW_PAYMENT_FLOW.getCode());
        if (isNewFlow) {

            System.out.println("Invio mail ad utente nuovo flusso");
        }
        else {

            System.out.println("Invio mail ad utente vecchio flusso");
        }

        System.out.println("PoPTransactionReverseShopThread constructor end");
    }

    @Override
    public void run() {

        try {

            System.out.println("PoPTransactionReverseShopThread run wait");

            try {
                Thread.sleep(10000);
            }
            catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }

            System.out.println("PoPTransactionReverseShopThread run start");

            this.userTransaction.begin();

            this.postPaidTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, this.postPaidTransactionBean.getMpTransactionID());

            Set<PostPaidTransactionEventBean> postPadTransactionEventBeanList = this.postPaidTransactionBean.getPostPaidTransactionEventBean();

            // Crea il nuovo stato
            sequenceID = 0;
            oldStatus = null;
            newStatus = null;
            for (PostPaidTransactionEventBean postPaidTransactionEventBean : postPadTransactionEventBeanList) {
                Integer transactionEventBeanSequenceID = postPaidTransactionEventBean.getSequenceID();
                if (sequenceID < transactionEventBeanSequenceID) {
                    sequenceID = transactionEventBeanSequenceID;
                    oldStatus = postPaidTransactionEventBean.getNewState();
                }
            }
            sequenceID = sequenceID + 1;

            ///
            Set<PostPaidTransactionPaymentEventBean> postPaidTransactionPaymentEventBeanList = this.postPaidTransactionBean.getPostPaidTransactionPaymentEventBean();
            Integer sequencePaymentID = 0;
            for (PostPaidTransactionPaymentEventBean postPaidTransactionEventBean : postPaidTransactionPaymentEventBeanList) {
                Integer transactionPaymentEventBeanSequenceID = postPaidTransactionEventBean.getSequence();
                if (sequencePaymentID < transactionPaymentEventBeanSequenceID) {
                    sequencePaymentID = transactionPaymentEventBeanSequenceID;
                }
            }
            sequencePaymentID = sequencePaymentID + 1;
            ///

            Date now = new Date();
            Timestamp eventTimestamp = new java.sql.Timestamp(now.getTime());
            PostPaidTransactionEventBean postPaidTransactionEventBean;
            PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;

            //(Eventuale storno) in base alla risposta del forecourt!
            //Verifica lo stato sul server Forecourt;
            String getStationPumpRequestId = String.valueOf(new Date().getTime());
            GetSrcTransactionStatusMessageResponse getSrcTransactionStatusMessageResponse = forecourtPPService.getTransactionStatus(getStationPumpRequestId,
                    this.postPaidTransactionBean.getMpTransactionID(), this.postPaidTransactionBean.getSrcTransactionID());

            //Test per storno esplicito
            /*
             * GetSrcTransactionStatusMessageResponse getSrcTransactionStatusMessageResponse = new GetSrcTransactionStatusMessageResponse();
             * getSrcTransactionStatusMessageResponse.setStatusCode("MESSAGE_RECEIVED_200");
             * SrcTransactionDetail srcTransactionDetail = new SrcTransactionDetail();
             * srcTransactionDetail.setSrcTransactionStatus("REVERSED");
             * getSrcTransactionStatusMessageResponse.setSrcTransactionDetail(srcTransactionDetail);
             */

            if (!getSrcTransactionStatusMessageResponse.getStatusCode().equals("MESSAGE_RECEIVED_200")) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", this.requestID, null,
                        "Non � stato possibile eseguire lo storno: recupero stato fallito " + this.postPaidTransactionBean.getMpTransactionID());

                System.out.println("Non � stato possibile eseguire lo storno: recupero stato fallito");

                this.userTransaction.commit();
            }
            else {

                System.out.println("srcTransactionStatus: " + getSrcTransactionStatusMessageResponse.getSrcTransactionDetail().getSrcTransactionStatus());

                if (getSrcTransactionStatusMessageResponse.getSrcTransactionDetail().getSrcTransactionStatus().equals("REVERSED")) {

                    // Calcolo dell'effettivo importo da stornare sottraendo dal valore amount iniziale il credito pagato con voucher
                    Double amount = this.postPaidTransactionBean.getAmount();

                    System.out.println("amount: " + amount);

                    for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : this.postPaidTransactionBean.getPostPaidConsumeVoucherBeanList()) {

                        if (postPaidConsumeVoucherBean == null || postPaidConsumeVoucherBean.getStatusCode() == null
                                || !postPaidConsumeVoucherBean.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {
                            System.out.println("PostPaidConsumeVoucherBean non valido (id: " + postPaidConsumeVoucherBean.getId() + ")");
                            continue;
                        }

                        System.out.println("Trovato PostPaidConsumeVoucherBean (id: " + postPaidConsumeVoucherBean.getId() + ")");

                        Double totalConsumed = postPaidConsumeVoucherBean.getTotalConsumed();

                        if (totalConsumed > amount) {

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", this.requestID, null, "Error: total voucher > amount");

                            amount = 0.0;
                        }
                        else {

                            amount = amount - totalConsumed;

                            // Effettua un arrotondamento alla seconda cifra decimale

                            System.out.println("round before: " + amount);

                            BigDecimal bd = new BigDecimal(amount).setScale(2, RoundingMode.HALF_EVEN);
                            amount = bd.doubleValue();

                            System.out.println("round after: " + amount);
                        }
                    }

                    if (amount > 0.0) {

                        // Storna la movimentazione, il consumo voucher e il caricamento punti

                        System.out.println("amount residuo > 0 -> Storna la movimentazione, il consumo voucher e il caricamento punti");

                        GestPayData gestPayDataREFUNDResponse = gpService.callRefund(amount, this.postPaidTransactionBean.getMpTransactionID(),
                                this.postPaidTransactionBean.getShopLogin(), this.postPaidTransactionBean.getCurrency(), this.postPaidTransactionBean.getToken(),
                                this.postPaidTransactionBean.getAuthorizationCode(), this.postPaidTransactionBean.getBankTansactionID(), this.postPaidTransactionBean.getStationBean().getStationID(),
                                this.postPaidTransactionBean.getAcquirerID(), this.postPaidTransactionBean.getGroupAcquirer(), this.decodedSecretKey);

                        newStatus = StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE;
                        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_REVERSE;

                        if (!gestPayDataREFUNDResponse.getTransactionResult().equals("OK")) {

                            newStatus = StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE;

                            postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp,
                                    transactionEvent, this.postPaidTransactionBean, "KO", newStatus, oldStatus, gestPayDataREFUNDResponse.getErrorCode(),
                                    gestPayDataREFUNDResponse.getErrorDescription());

                            this.em.persist(postPaidTransactionEventBean);

                            sequencePaymentID = sequencePaymentID + 1;
                            postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, this.postPaidTransactionBean, gestPayDataREFUNDResponse,
                                    "STO");

                            this.em.persist(postPaidTransactionPaymentEventBean);

                            this.userTransaction.commit();

                            /*
                             * poPReverseShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REVERSE_FAILURE);
                             * paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataREFUNDResponse, "REVERSE");
                             * 
                             * poPReverseShopTransactionResponse.setPaymentTransactionResult(paymentTransactionResult);
                             * return poPReverseShopTransactionResponse;
                             */
                        }
                        else {

                            newStatus = StatusHelper.POST_PAID_STATUS_REFUND;

                            postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp,
                                    transactionEvent, this.postPaidTransactionBean, "OK", newStatus, oldStatus, gestPayDataREFUNDResponse.getErrorCode(),
                                    gestPayDataREFUNDResponse.getErrorDescription());

                            this.em.persist(postPaidTransactionEventBean);

                            sequencePaymentID = sequencePaymentID + 1;
                            postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, this.postPaidTransactionBean, gestPayDataREFUNDResponse,
                                    "STO");

                            this.em.persist(postPaidTransactionPaymentEventBean);

                            this.postPaidTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED);
                            this.postPaidTransactionBean.setLastModifyTimestamp(eventTimestamp);

                            this.em.persist(this.postPaidTransactionBean);
                            //sequenceID += 1;

                            // Storno caricamento punti carta loyalty e aggiornamento dati utante
                            verifyLoyalty(this.postPaidTransactionBean, sequenceID);

                            // Storno movimentazione voucher e aggiornamento voucher utente
                            verifyVoucher(this.postPaidTransactionBean, sequenceID);

                            Email.sendPostPaidSummary(emailSender, postPaidTransactionBean, postPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, userCatService,
                                    stringSubstitution);

                            this.userTransaction.commit();

                            /*
                             * poPReverseShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REVERSE_SUCCESS);
                             * paymentTransactionResult = this.generatePaymentTransactionResult(this.postPaidTransactionBean, gestPayDataREFUNDResponse, "REVERSE");
                             * 
                             * poPReverseShopTransactionResponse.setPaymentTransactionResult(paymentTransactionResult);
                             * return poPReverseShopTransactionResponse;
                             */
                        }
                    }
                    else {

                        // Storna il consumo voucher e il caricamento punti

                        System.out.println("amount residuo = 0 -> Storna il consumo voucher e il caricamento punti");

                        this.postPaidTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_REVERSED);
                        this.postPaidTransactionBean.setLastModifyTimestamp(eventTimestamp);

                        this.em.persist(this.postPaidTransactionBean);
                        //sequenceID += 1;

                        // Storno caricamento punti carta loyalty e aggiornamento dati utante
                        verifyLoyalty(this.postPaidTransactionBean, sequenceID);

                        // Storno movimentazione voucher e aggiornamento voucher utente
                        verifyVoucher(this.postPaidTransactionBean, sequenceID);

                        PostPaidTransactionEventBean lastPostPaidTransactionEventBean = postPaidTransactionBean.getLastPostPaidTransactionEventBean();

                        Email.sendPostPaidSummary(emailSender, postPaidTransactionBean, lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, userCatService, stringSubstitution);

                        this.userTransaction.commit();
                    }
                }
                else {
                    //
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                            "Non � stato possibile eseguire lo storno: stati non coerenti " + this.postPaidTransactionBean.getMpTransactionID());

                    System.out.println("REVERSE: Non � stato possibile eseguire lo storno: stati non coerenti");

                    this.userTransaction.commit();

                }
            }

            System.out.println("PoPTransactionReverseShopThread run end");
        }
        catch (Exception ex2) {

            try {
                this.userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED Post Paid reverse transaction thread with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

    private PostPaidTransactionEventBean generateTransactionEvent(Integer sequenceID, String stateType, String requestID, Date eventTimestamp, String event,
            PostPaidTransactionBean poPTransactionBean, String result, String newState, String oldState, String errorCode, String errorDescription) {

        PostPaidTransactionEventBean postPaidTransactionEventBean = new PostPaidTransactionEventBean();

        postPaidTransactionEventBean.setSequenceID(sequenceID);
        postPaidTransactionEventBean.setStateType(stateType);
        postPaidTransactionEventBean.setRequestID(requestID);
        postPaidTransactionEventBean.setEventTimestamp(eventTimestamp);
        postPaidTransactionEventBean.setEvent(event);
        postPaidTransactionEventBean.setTransactionBean(poPTransactionBean);
        postPaidTransactionEventBean.setResult(result);
        postPaidTransactionEventBean.setNewState(newState);
        postPaidTransactionEventBean.setOldState(oldState);
        postPaidTransactionEventBean.setErrorCode(errorCode);
        postPaidTransactionEventBean.setErrorDescription(errorDescription);

        return postPaidTransactionEventBean;
    }

    private PostPaidTransactionPaymentEventBean generateTransactionPaymentEvent(Integer sequenceID, PostPaidTransactionBean poPTransactionBean, GestPayData gestPayData,
            String event) {

        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean = new PostPaidTransactionPaymentEventBean();

        postPaidTransactionPaymentEventBean.setSequence(sequenceID);
        postPaidTransactionPaymentEventBean.setErrorCode(gestPayData.getErrorCode());
        postPaidTransactionPaymentEventBean.setErrorDescription(gestPayData.getErrorDescription());
        postPaidTransactionPaymentEventBean.setTransactionBean(poPTransactionBean);
        postPaidTransactionPaymentEventBean.setTransactionResult(gestPayData.getTransactionResult());
        postPaidTransactionPaymentEventBean.setAuthorizationCode(gestPayData.getAuthorizationCode());
        postPaidTransactionPaymentEventBean.setEventType(event);

        return postPaidTransactionPaymentEventBean;
    }

    private void verifyVoucher(PostPaidTransactionBean postPaidTransactionBean, Integer eventSequenceID) {
        try {
            double amount = 0.0;
            PartnerType partnerType = PartnerType.MP;

            for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidTransactionBean.getPostPaidConsumeVoucherBeanList()) {

                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                Long requestTimestamp = new Date().getTime();
                String operationIDtoCheck = postPaidConsumeVoucherBean.getOperationID();

                System.out.println("Controllo consumo voucher transazione " + postPaidTransactionBean.getMpTransactionID());
                CheckConsumeVoucherTransactionResult checkConsumeVoucherTransactionResult = fidelityService.checkConsumeVoucherTransaction(operationID, operationIDtoCheck,
                        partnerType, requestTimestamp);
                List<VoucherDetail> voucherDetailList = checkConsumeVoucherTransactionResult.getVoucherList();

                for (VoucherDetail voucherDetail : voucherDetailList) {
                    if (voucherDetail.getConsumedValue().doubleValue() > 0.0) {
                        System.out.println("Trovato voucher consumato: " + voucherDetail.getVoucherCode());
                        amount += voucherDetail.getConsumedValue().doubleValue();
                    }
                }
            }

            if (amount == 0.0) {
                System.out.println("Storno consumo voucher non necessario. Importo consumato 0.0");
                postPaidTransactionBean.setVoucherReconcile(false);
                return;
            }

            this.revertConsumeVoucher(postPaidTransactionBean, eventSequenceID);
            postPaidTransactionBean.setVoucherReconcile(false);
        }
        catch (Exception ex) {
            System.out.println("Exception revertConsumeVoucher: " + ex.getMessage());
            postPaidTransactionBean.setVoucherReconcile(true);
        }
    }

    private void verifyLoyalty(PostPaidTransactionBean postPaidTransactionBean, Integer eventSequenceID) {
        try {
            PartnerType partnerType = PartnerType.MP;
            Integer credits = 0;

            for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : postPaidTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {
                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                Long requestTimestamp = new Date().getTime();
                String operationIDtoCheck = postPaidLoadLoyaltyCreditsBean.getOperationID();

                System.out.println("Controllo caricamento punti transazione " + postPaidTransactionBean.getMpTransactionID());
                CheckLoadLoyaltyCreditsTransactionResult checkLoadLoyaltyCreditsTransactionResult = fidelityService.checkLoadLoyaltyCreditsTransaction(operationID,
                        operationIDtoCheck, partnerType, requestTimestamp);
                credits += checkLoadLoyaltyCreditsTransactionResult.getCredits();

            }

            if (credits == 0) {
                System.out.println("Storno caricamento punti non necessario. Importo caricato 0");
                postPaidTransactionBean.setLoyaltyReconcile(false);
                return;
            }

            this.revertLoadLoyaltyCredits(postPaidTransactionBean, eventSequenceID);
            postPaidTransactionBean.setLoyaltyReconcile(false);
        }
        catch (Exception ex) {
            System.out.println("Exception revertLoadLoyaltyCredits: " + ex.getMessage());
            postPaidTransactionBean.setLoyaltyReconcile(true);
        }
    }

    private void revertConsumeVoucher(PostPaidTransactionBean poPTransactionBean, Integer eventSequenceID) throws Exception {

        System.out.println("Storno consume voucher transazione " + poPTransactionBean.getMpTransactionID());

        ArrayList<PostPaidConsumeVoucherBean> reversedPostPaidConsumeVoucherBeanList = new ArrayList<PostPaidConsumeVoucherBean>(0);
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_VOUCHER_REVERSE;
        String requestID = String.valueOf(new Date().getTime());

        if (poPTransactionBean.getPostPaidConsumeVoucherBeanList().isEmpty()) {
            System.out.println("Storno consumo voucher non necessario. Nessun voucher trovato");
        }

        for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : poPTransactionBean.getPostPaidConsumeVoucherBeanList()) {
            // Bisogna effettuare lo storno solo se l'operazione di consumo voucher � andata a buon fine

            System.out.println("consume voucher status code: " + postPaidConsumeVoucherBean.getStatusCode());

            if (postPaidConsumeVoucherBean == null) {
                System.out.println("Storno consumo voucher non necessario. PostPaidConsumeVoucherBean nullo");
                continue;
            }

            // Chimata al servizio reverseConsumeVoucherTransaction

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            String operationIDtoReverse = postPaidConsumeVoucherBean.getOperationID();
            PartnerType partnerType = PartnerType.MP;
            Long requestTimestamp = new Date().getTime();
            newStatus = StatusHelper.POST_PAID_STATUS_VOUCHER_REVERSE;

            try {

                ReverseConsumeVoucherTransactionResult reverseConsumeVoucherTransactionResult = fidelityService.reverseConsumeVoucherTransaction(operationID, operationIDtoReverse,
                        partnerType, requestTimestamp);

                if (!reverseConsumeVoucherTransactionResult.getStatusCode().equals(FidelityResponse.REVERSE_CONSUME_VOUCHER_TRANSACTION_OK)) {

                    System.out.println("Error in reverseConsumeVoucherTransaction: " + reverseConsumeVoucherTransactionResult.getStatusCode());

                    errorCode = reverseConsumeVoucherTransactionResult.getStatusCode();
                    errorDescription = reverseConsumeVoucherTransactionResult.getMessageCode();
                    eventResult = "KO";

                    sequenceID += 1;
                    eventSequenceID = sequenceID;
                    PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                            eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, null, errorCode, errorDescription);

                    oldStatus = newStatus;

                    em.persist(postPaidTransactionEventBean);

                }
                else {

                    // Aggiungi una riga per loggare l'esito dell'operazione di storno
                    PostPaidConsumeVoucherBean reversePostPaidConsumeVoucherBean = new PostPaidConsumeVoucherBean();
                    reversePostPaidConsumeVoucherBean.setCsTransactionID(reverseConsumeVoucherTransactionResult.getCsTransactionID());
                    reversePostPaidConsumeVoucherBean.setMessageCode(reverseConsumeVoucherTransactionResult.getMessageCode());
                    reversePostPaidConsumeVoucherBean.setOperationID(operationID);
                    reversePostPaidConsumeVoucherBean.setOperationIDReversed(operationIDtoReverse);
                    reversePostPaidConsumeVoucherBean.setOperationType("REVERSE");
                    reversePostPaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
                    reversePostPaidConsumeVoucherBean.setStatusCode(reverseConsumeVoucherTransactionResult.getStatusCode());
                    reversePostPaidConsumeVoucherBean.setTotalConsumed(0.0);
                    reversePostPaidConsumeVoucherBean.setPostPaidTransactionBean(poPTransactionBean);

                    reversedPostPaidConsumeVoucherBeanList.add(reversePostPaidConsumeVoucherBean);
                    //poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(reversePostPaidConsumeVoucherBean);

                    // Ripristina il valore iniziale dei voucher consumati

                    String operationID_check = new IdGenerator().generateId(16).substring(0, 33);
                    VoucherConsumerType voucherConsumerType_check = VoucherConsumerType.ENI;
                    PartnerType partnerType_check = PartnerType.MP;
                    Long requestTimestamp_check = new Date().getTime();

                    List<VoucherCodeDetail> voucherCodeList_check = new ArrayList<VoucherCodeDetail>(0);

                    for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {

                        VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                        voucherCodeDetail.setVoucherCode(postPaidConsumeVoucherDetailBean.getVoucherCode());
                        voucherCodeList_check.add(voucherCodeDetail);
                    }

                    CheckVoucherResult checkVoucherResult = new CheckVoucherResult();

                    try {

                        checkVoucherResult = fidelityService.checkVoucher(operationID_check, voucherConsumerType_check, partnerType_check, requestTimestamp_check,
                                voucherCodeList_check);
                        // Verifica l'esito del check
                        String checkVoucherStatusCode = checkVoucherResult.getStatusCode();

                        if (!checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_OK)) {
                            // Error
                            System.out.println("Error checking voucher: " + checkVoucherStatusCode);
                            //throw new FidelityServiceException("Error checking voucher: " + checkVoucherStatusCode);
                        }
                        else {

                            // Aggiorna lo stato dei voucher dell'utente

                            // Ricerca il voucher nella risposta del servizio di verifica
                            for (VoucherDetail voucherDetail : checkVoucherResult.getVoucherList()) {

                                System.out.println("Elaborazione voucher: " + voucherDetail.getVoucherCode() + " in stato: " + voucherDetail.getVoucherStatus());

                                for (VoucherBean voucherBean : poPTransactionBean.getUserBean().getVoucherList()) {

                                    if (voucherBean.getCode().equals(voucherDetail.getVoucherCode())) {

                                        System.out.println("Aggiornamento Voucher " + voucherDetail.getVoucherCode());

                                        // Aggiorna il voucher con i dati restituiti dal servizio
                                        voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                                        voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                                        voucherBean.setInitialValue(voucherDetail.getInitialValue());
                                        voucherBean.setPromoCode(voucherDetail.getPromoCode());
                                        voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                                        voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                                        voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                                        voucherBean.setCode(voucherDetail.getVoucherCode());
                                        voucherBean.setStatus(voucherDetail.getVoucherStatus());
                                        voucherBean.setType(voucherDetail.getVoucherType());
                                        voucherBean.setValue(voucherDetail.getVoucherValue());

                                        System.out.println("Voucher aggiornato");
                                    }
                                }
                            }

                            // Aggiorna l'utente
                            em.merge(poPTransactionBean.getUserBean());

                            sequenceID += 1;
                            eventSequenceID = sequenceID;
                            PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD,
                                    requestID, eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, null, errorCode, errorDescription);

                            oldStatus = newStatus;

                            em.persist(postPaidTransactionEventBean);
                        }
                    }
                    catch (Exception ex) {
                        System.out.println("Error in checkVoucherResult: " + ex.getMessage());
                        //throw new FidelityServiceException("Error in checkVoucherResult: " + e.getMessage());
                        errorCode = "9999";
                        errorDescription = ex.getMessage();
                        eventResult = "ERROR";

                        sequenceID += 1;
                        eventSequenceID = sequenceID;
                        PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD,
                                requestID, eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, null, errorCode, errorDescription);

                        oldStatus = newStatus;

                        em.persist(postPaidTransactionEventBean);
                    }
                }
            }
            catch (FidelityServiceException ex) {
                PostPaidConsumeVoucherBean newPostPaidConsumeVoucherBean = new PostPaidConsumeVoucherBean();
                newPostPaidConsumeVoucherBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
                newPostPaidConsumeVoucherBean.setMessageCode("STORNO DA VERIFICARE (" + ex.getMessage() + ")");
                newPostPaidConsumeVoucherBean.setTotalConsumed(postPaidConsumeVoucherBean.getTotalConsumed());
                newPostPaidConsumeVoucherBean.setPostPaidTransactionBean(poPTransactionBean);
                newPostPaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
                newPostPaidConsumeVoucherBean.setOperationID(operationID);
                newPostPaidConsumeVoucherBean.setOperationIDReversed(operationIDtoReverse);
                newPostPaidConsumeVoucherBean.setTotalConsumed(0.0);
                newPostPaidConsumeVoucherBean.setOperationType(postPaidConsumeVoucherBean.getOperationType());

                reversedPostPaidConsumeVoucherBeanList.add(newPostPaidConsumeVoucherBean);
                //poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(newPostPaidConsumeVoucherBean);

                errorCode = "9999";
                errorDescription = ex.getMessage();
                eventResult = "ERROR";

                sequenceID += 1;
                eventSequenceID = sequenceID;
                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, null, errorCode, errorDescription);

                oldStatus = newStatus;

                em.persist(postPaidTransactionEventBean);
            }
            catch (Exception ex) {
                errorCode = "9999";
                errorDescription = ex.getMessage();
                eventResult = "ERROR";

                sequenceID += 1;
                eventSequenceID = sequenceID;
                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, null, errorCode, errorDescription);

                oldStatus = newStatus;

                em.persist(postPaidTransactionEventBean);
            }
        }

        for (PostPaidConsumeVoucherBean revertedPostPaidConsumeVoucherBean : reversedPostPaidConsumeVoucherBeanList) {
            poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(revertedPostPaidConsumeVoucherBean);
        }

        em.merge(poPTransactionBean);

        oldStatus = newStatus;

    }

    private void revertLoadLoyaltyCredits(PostPaidTransactionBean poPTransactionBean, Integer eventSequenceID) throws FidelityServiceException {

        System.out.println("Storno load loyalty credits transazione " + poPTransactionBean.getMpTransactionID());

        ArrayList<PostPaidLoadLoyaltyCreditsBean> revertedPostPaidLoadLoyaltyCreditsBeanList = new ArrayList<PostPaidLoadLoyaltyCreditsBean>(0);
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY_REVERSE;
        String requestID = String.valueOf(new Date().getTime());
        boolean reversed = false;

        if (poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().isEmpty()) {
            System.out.println("Storno caricamento punti non necessario. Nessun caricamento trovato");
        }

        for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {

            System.out.println("load loyalty credits status code: " + postPaidLoadLoyaltyCreditsBean.getStatusCode());
            // Bisogna effettuare lo storno solo se l'operazione di caricamento loyalty � andata a buon fine
            // Chimamata al servizio reverseLoadLoyaltyCreditsTransaction

            if (postPaidLoadLoyaltyCreditsBean == null) {

                System.out.println("Storno caricamento punti non necessario. postPaidLoadLoyaltyCreditsBean nullo");
                continue;
            }

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            String operationIDtoReverse = postPaidLoadLoyaltyCreditsBean.getOperationID();
            PartnerType partnerType = PartnerType.MP;
            Long requestTimestamp = new Date().getTime();
            newStatus = StatusHelper.POST_PAID_STATUS_LOYALTY_REVERSE;

            ReverseLoadLoyaltyCreditsTransactionResult reverseLoadLoyaltyCreditsTransactionResult = fidelityService.reverseLoadLoyaltyCreditsTransaction(operationID,
                    operationIDtoReverse, partnerType, requestTimestamp);

            if (!reverseLoadLoyaltyCreditsTransactionResult.getStatusCode().equals(FidelityResponse.REVERSE_LOAD_LOYALTY_CREDITS_OK)) {

                System.out.println("Error in reverseConsumeVoucherTransaction: " + reverseLoadLoyaltyCreditsTransactionResult.getStatusCode());

                errorCode = reverseLoadLoyaltyCreditsTransactionResult.getStatusCode();
                errorDescription = reverseLoadLoyaltyCreditsTransactionResult.getMessageCode();
                eventResult = "KO";

                sequenceID += 1;
                eventSequenceID = sequenceID;
                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, null, errorCode, errorDescription);

                oldStatus = newStatus;

                em.persist(postPaidTransactionEventBean);
            }
            else {

                // Aggiungi una riga per loggare l'esito dell'operazione di storno

                PostPaidLoadLoyaltyCreditsBean reversePostPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean();
                //reversePostPaidLoadLoyaltyCreditsBean.setBalance(reverseLoadLoyaltyCreditsTransactionResult.getBalance());
                //reversePostPaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                //reversePostPaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                reversePostPaidLoadLoyaltyCreditsBean.setCsTransactionID(reverseLoadLoyaltyCreditsTransactionResult.getCsTransactionID());
                //reversePostPaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                //reversePostPaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                reversePostPaidLoadLoyaltyCreditsBean.setMessageCode(reverseLoadLoyaltyCreditsTransactionResult.getMessageCode());
                reversePostPaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                reversePostPaidLoadLoyaltyCreditsBean.setOperationIDReversed(operationIDtoReverse);
                reversePostPaidLoadLoyaltyCreditsBean.setOperationType("REVERSE");
                reversePostPaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                reversePostPaidLoadLoyaltyCreditsBean.setStatusCode(reverseLoadLoyaltyCreditsTransactionResult.getStatusCode());

                reversePostPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);

                revertedPostPaidLoadLoyaltyCreditsBeanList.add(reversePostPaidLoadLoyaltyCreditsBean);

                reversed = true;
            }
        }

        if (reversed) {
            sequenceID += 1;
            PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp,
                    transactionEvent, poPTransactionBean, eventResult, newStatus, null, errorCode, errorDescription);

            oldStatus = newStatus;

            em.persist(postPaidTransactionEventBean);

            for (PostPaidLoadLoyaltyCreditsBean revertedPostPaidLoadLoyaltyCreditsBean : revertedPostPaidLoadLoyaltyCreditsBeanList) {
                poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(revertedPostPaidLoadLoyaltyCreditsBean);
            }

            em.merge(poPTransactionBean);
        }
        else {
            System.out.println("Storno caricamento punti carte loyalty non necessario");
        }

        oldStatus = newStatus;
    }

}
