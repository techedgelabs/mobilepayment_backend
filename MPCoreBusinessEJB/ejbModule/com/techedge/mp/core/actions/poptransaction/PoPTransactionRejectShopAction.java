package com.techedge.mp.core.actions.poptransaction;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRejectShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.integration.shop.interfaces.SendMPTransactionResultMessageResponse;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPTransactionRejectShopAction {

	@Resource
	private EJBContext context;

	@PersistenceContext( unitName = "CrudPU" )
	private EntityManager em;

	@EJB
	private LoggerService loggerService;


	public PoPTransactionRejectShopAction() {
	}

	public PostPaidRejectShopTransactionResponse execute(
			String requestID,
			String ticketID,
			String mpTransactionID,
			ForecourtPostPaidServiceRemote forecourtPPService) throws EJBException {

		UserTransaction userTransaction = context.getUserTransaction();

		try {
			userTransaction.begin();
			PostPaidRejectShopTransactionResponse poPRejectShopTransactionResponse = new PostPaidRejectShopTransactionResponse();

			//    		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "ServerName: " + serverName );
			//    		
			//    		CreateRefuelResponse createRefuelResponse = new CreateRefuelResponse();
			//    		
			TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

			if ( ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket() ) {

				// Ticket non valido
				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket" );

				userTransaction.commit();

				poPRejectShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REJECT_INVALID_TICKET);
				return poPRejectShopTransactionResponse;
			}
			//    		
			UserBean userBean = ticketBean.getUser();
			if ( userBean == null ) {

				// Ticket non valido
				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found" );

				userTransaction.commit();

				poPRejectShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REJECT_INVALID_TICKET);
				return poPRejectShopTransactionResponse;
			}


			// Verifica lo stato dell'utente
			Integer userStatus = userBean.getUserStatus();
			if ( userStatus != User.USER_STATUS_VERIFIED ) {

				// Un utente che si trova in questo stato non pu� invocare questo servizio
				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus );

				userTransaction.commit();

				poPRejectShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REJECT_UNAUTHORIZED);
				return poPRejectShopTransactionResponse;
			}
			
			
			//TODO aggiungere controllo su stati che non devono essere modificabili;

			// Controlla se l'utente ha gi� una transazione attiva
			//    		List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);
			//		    
			//		    if ( !transactionBeanList.isEmpty() ) {
			//    			
			//    			// Esiste una transazione associata all'utente non ancora completata
			//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction found" );
			//				
			//    			userTransaction.commit();
			//    			
			//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
			//    			return createRefuelResponse;
			//    		}


			//		    // Se il medodo di pagamento non � specificato utilizza quello di default
			//		    PaymentInfoBean paymentInfoBean = null;
			//		    if ( paymentMethodId == null && paymentMethodType == null ) {
			//		    	
			//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found" );
			//				
			//		    	paymentInfoBean = userBean.findDefaultPaymentInfoBean();
			//		    }
			//		    else {
			//
			//		    	paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
			//		    }
			//		    
			//		    // Verifica che il metodo di pagamento selezionato sia in uno stato valido
			//		    if ( paymentInfoBean == null ) {
			//		    	
			//		    	// Il metodo di pagamento selezionato non esiste
			//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data" );
			//				
			//    			userTransaction.commit();
			//    			
			//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
			//    			return createRefuelResponse;
			//		    }
			//		    
			//		    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: " + paymentMethodType );
			//		    
			//		    if ( paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_BLOCKED     ||
			//		    	 paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_ERROR       ||
			//		    	 paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING     ) {
			//		    	
			//		    	// Il metodo di pagamento selezionato non pu� essere utilizzato
			//		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus() );
			//				
			//    			userTransaction.commit();
			//    			
			//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PAYMENT_DATA_WRONG);
			//    			return createRefuelResponse;
			//		    }
			//		    
			//    		if ( !encodedPin.equals(paymentInfoBean.getPin()) ) {
			//    			
			//    			// Il pin inserito non � valido
			//    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin" );
			//				
			//    			// Si sottrae uno al numero di tentativi residui
			//    			Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
			//    			if ( pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0 ) {
			//    				pinCheckAttemptsLeft--;
			//    			}
			//    			else {
			//    				pinCheckAttemptsLeft = 0;
			//    			}
			//    			
			//    			if ( pinCheckAttemptsLeft == 0 ) {
			//    				
			//    				// Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
			//    				paymentInfoBean.setDefaultMethod(false);
			//    				paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);
			//    			}
			//    			
			//    			
			//    			paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);
			//    			
			//    			em.merge(paymentInfoBean);
			//    			
			//    			userTransaction.commit();
			//    			
			//    			createRefuelResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
			//    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_WRONG);
			//    			return createRefuelResponse;
			//    		}
			//    		
			//    		
			//    		// L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
			//    		paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);
			//    		
			//    		em.merge(paymentInfoBean);



			//TODO occorre inserire la verifica che non ci sia una transazione aperta\attiva;
			PostPaidTransactionBean poPTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, mpTransactionID);
			if ( poPTransactionBean == null ) {

				// Lo stationID inserito non corrisponde a nessuna stazione valida
				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No postPaid transaction dor mpTransactioId " + mpTransactionID );

				userTransaction.commit();

				poPRejectShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REJECT_NOT_RECOGNIZED);
				return poPRejectShopTransactionResponse;
			}




			//IF (OK)

			Set <PostPaidTransactionEventBean> postPaidTransactionEventBeanList = poPTransactionBean.getPostPaidTransactionEventBean();
			// Crea il nuovo stato
			Integer sequenceID = 0;
			String oldStatus= "";

			for( PostPaidTransactionEventBean postPaidTransactionEventBean : postPaidTransactionEventBeanList ) {
				Integer transactionEventBeanSequenceID = postPaidTransactionEventBean.getSequenceID();
				if ( sequenceID < transactionEventBeanSequenceID ) {
					sequenceID = transactionEventBeanSequenceID;
					oldStatus = postPaidTransactionEventBean.getNewState();
				}

			}
			sequenceID = sequenceID + 1;

			if (oldStatus !=StatusHelper.POST_PAID_EVENT_GFG_ENABLE){
				this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "PostPaid transaction enable - no change " + mpTransactionID );

				userTransaction.commit();

				poPRejectShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_ENABLE_FAILURE);
				return poPRejectShopTransactionResponse;
			}

			Date now = new Date();
			Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());

			// Aggiorno la transazione con i dati ricevuti in input
			poPTransactionBean.setNotificationCreated(true);
			poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);
			poPTransactionBean.setUserBean(userBean);

			//salvataggio dell'evento;
			PostPaidTransactionEventBean postPaidTransactionEventBean = new PostPaidTransactionEventBean();
			postPaidTransactionEventBean.setSequenceID(sequenceID);
			postPaidTransactionEventBean.setStateType(StatusHelper.POST_PAID_EVENT_TYPE_STANDARD);
			postPaidTransactionEventBean.setRequestID(requestID);
			postPaidTransactionEventBean.setEventTimestamp(creationTimestamp);
			postPaidTransactionEventBean.setEvent(StatusHelper.POST_PAID_EVENT_MA_REJECT ); 		
			postPaidTransactionEventBean.setTransactionBean(poPTransactionBean);
			postPaidTransactionEventBean.setResult("OK");
			postPaidTransactionEventBean.setNewState(StatusHelper.POST_PAID_STATUS_CANCELED);
			postPaidTransactionEventBean.setOldState(oldStatus);


			em.persist(poPTransactionBean);
			em.persist(postPaidTransactionEventBean);

			//Notifica di pagamento al sistema forecourt; 
			String notifyResponse = this.notifyResponse(
					requestID,
					poPTransactionBean.getStationBean().getStationID(),
					poPTransactionBean.getMpTransactionID(),
					poPTransactionBean.getSrcTransactionID(),
					poPTransactionBean.getMpTransactionStatus(),
					null,// MOV OK
					forecourtPPService);
			
			if(!notifyResponse.equals("MESSAGE_RECEIVED_200")){
				poPTransactionBean.setNotificationPaid(true);
				em.persist(poPTransactionBean);
			}

			

			poPRejectShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_REJECT_SUCCESS);

			userTransaction.commit();

			return poPRejectShopTransactionResponse;

		}
		catch (Exception ex2) {

			try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			String message = "FAILED Post Paid transaction creation with message (" + ex2.getMessage() + ")";
			this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message );

			throw new EJBException(ex2);
		}
	}





	private String notifyResponse(
			String requestID,
			String stationID,
			String mpTransactionID,
			String srcTransactionID,
			String TransactionResult,
			PaymentTransactionResult paymentTransactionResult,
			ForecourtPostPaidServiceRemote forecourtPPService) {
		// TODO Auto-generated method stub

	    Boolean loyaltyCredits = false;
        List<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail> vouchers = new ArrayList<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail>(0);

		SendMPTransactionResultMessageResponse sendMPTransactionResultMessageResponse = forecourtPPService.sendMPTransactionResult(requestID, stationID, mpTransactionID, 
		        srcTransactionID, TransactionResult, paymentTransactionResult, loyaltyCredits, vouchers, null);

		if (!sendMPTransactionResultMessageResponse.getStatusCode().equals("MESSAGE_RECEIVED_200")){
		}

		return sendMPTransactionResultMessageResponse.getStatusCode();

	}


}
