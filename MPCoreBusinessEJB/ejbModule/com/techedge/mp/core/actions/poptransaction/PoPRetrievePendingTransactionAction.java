package com.techedge.mp.core.actions.poptransaction;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.CashInfo;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PostPaidTransaction;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.postpaid.CoreProductDetail;
import com.techedge.mp.core.business.interfaces.postpaid.CorePumpDetail;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCartData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidGetPendingTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.PollingIntervalBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltySessionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPRetrievePendingTransactionAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public PoPRetrievePendingTransactionAction() {}

    public PostPaidGetPendingTransactionResponse execute(String requestID, String ticketID, String loyaltySessionID, Integer loyaltySessionExpiryTime, Integer defaultNextPollingInterval) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            PostPaidGetPendingTransactionResponse pendingRefuelResponse = null;

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                pendingRefuelResponse = new PostPaidGetPendingTransactionResponse();
                pendingRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return pendingRefuelResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                pendingRefuelResponse = new PostPaidGetPendingTransactionResponse();
                pendingRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                return pendingRefuelResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to retrieve pending refuel in status " + userStatus);

                userTransaction.commit();

                pendingRefuelResponse = new PostPaidGetPendingTransactionResponse();
                pendingRefuelResponse.setStatusCode(ResponseHelper.USER_UPDATE_UNAUTHORIZED);
                return pendingRefuelResponse;
            }

            if (loyaltySessionID != null) {
                LoyaltySessionBean loyaltySessionBean = QueryRepository.findLoyaltySessionBySessionid(em, loyaltySessionID);
                
                if (loyaltySessionBean != null) {
                    loyaltySessionBean.renew(loyaltySessionExpiryTime);
                    em.merge(loyaltySessionBean);
                    System.out.println("Rinnovata loyaltySessionID: " + loyaltySessionID);
                }
                else {
                    System.out.println("LoyaltySessionBean nullo. Rinnovo non fatto");
                }
            }
            else {
                System.out.println("loyaltySessionID nullo. Rinnovo non fatto");
            }
            
            // Ricerca la transazione attiva
            List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);
            List<PostPaidTransactionBean> poPTransactionBeanList = QueryRepository.findPostPaidTransactionsActiveByUserBean(em, userBean);

            if (transactionBeanList.isEmpty() && poPTransactionBeanList.isEmpty()) {

                // Non esiste una transazione attiva
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction not found");

                userTransaction.commit();

                pendingRefuelResponse = new PostPaidGetPendingTransactionResponse();
                pendingRefuelResponse.setStatusCode(ResponseHelper.RETRIEVE_PENDING_REFUEL_FAILURE);
                return pendingRefuelResponse;
            }
            else {

                if (!transactionBeanList.isEmpty()) {

                    // esiste una transazione di tipo PREPAID

                    TransactionBean pendingTransactionBean = null;

                    for (TransactionBean transactionBean : transactionBeanList) {

                        // Si estrae la prima
                        pendingTransactionBean = transactionBean;
                    }

                    if (pendingTransactionBean == null) {

                        // Non esiste una transazione attiva
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Id wrong in confirm refuel");

                        userTransaction.commit();

                        pendingRefuelResponse = new PostPaidGetPendingTransactionResponse();
                        pendingRefuelResponse.setStatusCode(ResponseHelper.RETRIEVE_PENDING_REFUEL_FAILURE);
                        return pendingRefuelResponse;
                    }
                    else {

                        // Si estrae lo stato pi� recente associato alla transazione
                        Integer lastSequenceID = 0;
                        TransactionStatusBean lastTransactionStatusBean = null;

                        Set<TransactionStatusBean> transactionStatusData = pendingTransactionBean.getTransactionStatusBeanData();

                        for (TransactionStatusBean transactionStatusBean : transactionStatusData) {

                            Integer transactionStatusBeanSequenceID = transactionStatusBean.getSequenceID();
                            if (lastSequenceID < transactionStatusBeanSequenceID) {
                                lastSequenceID = transactionStatusBeanSequenceID;
                                lastTransactionStatusBean = transactionStatusBean;
                            }
                        }

                        if (lastTransactionStatusBean == null) {

                            // Errore nella lettura dello stato della transazione
                            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, "Id wrong in confirm refuel");

                            userTransaction.commit();

                            pendingRefuelResponse = new PostPaidGetPendingTransactionResponse();
                            pendingRefuelResponse.setStatusCode(ResponseHelper.RETRIEVE_PENDING_REFUEL_FAILURE);
                            return pendingRefuelResponse;
                        }
                        else {

                            pendingRefuelResponse = new PostPaidGetPendingTransactionResponse();
                            pendingRefuelResponse.setStatusCode(ResponseHelper.RETRIEVE_PENDING_REFUEL_SUCCESS);

                            pendingRefuelResponse.setCreationTimestamp(pendingTransactionBean.getCreationTimestamp());

                            pendingRefuelResponse.setObjectType("FUEL");
                            pendingRefuelResponse.setObjectStatus("PRE-PAY");

                            //RefuelData
                            pendingRefuelResponse.setRefuelID(pendingTransactionBean.getTransactionID());
                            pendingRefuelResponse.setStatus(lastTransactionStatusBean.getStatus());
                            pendingRefuelResponse.setSubStatus(lastTransactionStatusBean.getSubStatus());
                            pendingRefuelResponse.setUseVoucher(pendingTransactionBean.getUseVoucher());
                            pendingRefuelResponse.setInitialAmount(pendingTransactionBean.getInitialAmount());

                            //StationData
                            pendingRefuelResponse.setStationID(pendingTransactionBean.getStationBean().getStationID());
                            // TODO - il nome della stazione non viene restituito
                            pendingRefuelResponse.setStationName("");
                            pendingRefuelResponse.setStationAddress(pendingTransactionBean.getStationBean().getAddress());
                            pendingRefuelResponse.setStationCity(pendingTransactionBean.getStationBean().getCity());
                            pendingRefuelResponse.setStationProvince(pendingTransactionBean.getStationBean().getProvince());
                            pendingRefuelResponse.setStationCountry(pendingTransactionBean.getStationBean().getCountry());
                            pendingRefuelResponse.setStationLatitude(pendingTransactionBean.getStationBean().getLatitude());
                            pendingRefuelResponse.setStationLongitude(pendingTransactionBean.getStationBean().getLongitude());

                            //PumpData
                            CorePumpDetail corePumpDetail = new CorePumpDetail();
                            corePumpDetail.setPumpID(pendingTransactionBean.getPumpID());
                            corePumpDetail.setPumpNumber(pendingTransactionBean.getPumpNumber().toString());
                            corePumpDetail.setPumpStatus(null);
                            corePumpDetail.getProductDetails();
                            CoreProductDetail productDetail = new CoreProductDetail();
                            productDetail.setFuelType(pendingTransactionBean.getProductDescription());
                            productDetail.setProductDescription(pendingTransactionBean.getProductDescription());
                            corePumpDetail.getProductDetails().add(productDetail);
                            pendingRefuelResponse.setPumpInfo(corePumpDetail);
                            
                            String status = lastTransactionStatusBean.getStatus();
                            Integer nextPollingInterval = defaultNextPollingInterval;
                            
                            // Ricerca la durata dell'intervallo di polling in base allo stato della transazione
                            // Se non viene trovato si utilizza il valore di default
                            PollingIntervalBean pollingIntervalBean = QueryRepository.findPollingIntervalByStatus(em, status);
                            if (pollingIntervalBean != null) {
                                nextPollingInterval = pollingIntervalBean.getNextPollingInterval();
                            }
                            
                            pendingRefuelResponse.setNextPollingInterval(nextPollingInterval);

                            userTransaction.commit();

                            return pendingRefuelResponse;
                        }
                    }
                }
                else {

                    // esiste una transazione di tipo POSTPAID

                    PostPaidTransactionBean pendingPostPaidTransactionBean = null;

                    for (PostPaidTransactionBean popTransactionBean : poPTransactionBeanList) {

                        // Si estrae la prima
                        pendingPostPaidTransactionBean = popTransactionBean;
                    }

                    if (pendingPostPaidTransactionBean == null || pendingPostPaidTransactionBean.getMpTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)) {

                        // Non esiste una transazione attiva
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Id wrong in confirm refuel");

                        userTransaction.commit();
                        pendingRefuelResponse = new PostPaidGetPendingTransactionResponse();
                        pendingRefuelResponse.setStatusCode(ResponseHelper.RETRIEVE_PENDING_REFUEL_FAILURE);
                        return pendingRefuelResponse;
                    }
                    else {
                        pendingRefuelResponse = new PostPaidGetPendingTransactionResponse();
                        pendingRefuelResponse.setStatusCode(ResponseHelper.RETRIEVE_PENDING_REFUEL_SUCCESS);

                        pendingRefuelResponse.setCreationTimestamp(pendingPostPaidTransactionBean.getCreationTimestamp());

                        System.out.println("Pending Refuel Source: " + pendingPostPaidTransactionBean.getSource());

                        if (!pendingPostPaidTransactionBean.getSource().equals("CASH REGISTER")) {

                            pendingRefuelResponse.setObjectType("FUEL");
                            pendingRefuelResponse.setObjectStatus("POST-PAY");
                        }
                        else {

                            pendingRefuelResponse.setObjectType("SHOP");
                            pendingRefuelResponse.setObjectStatus("POST-PAY");
                        }

                        //StationData
                        pendingRefuelResponse.setStationID(pendingPostPaidTransactionBean.getStationBean().getStationID());
                        // TODO - il nome della stazione non viene restituito
                        pendingRefuelResponse.setStationName("");
                        pendingRefuelResponse.setStationAddress(pendingPostPaidTransactionBean.getStationBean().getAddress());
                        pendingRefuelResponse.setStationCity(pendingPostPaidTransactionBean.getStationBean().getCity());
                        pendingRefuelResponse.setStationProvince(pendingPostPaidTransactionBean.getStationBean().getProvince());
                        pendingRefuelResponse.setStationCountry(pendingPostPaidTransactionBean.getStationBean().getCountry());
                        pendingRefuelResponse.setStationLatitude(pendingPostPaidTransactionBean.getStationBean().getLatitude());
                        pendingRefuelResponse.setStationLongitude(pendingPostPaidTransactionBean.getStationBean().getLongitude());

                        pendingRefuelResponse.setTransactionID(pendingPostPaidTransactionBean.getMpTransactionID());
                        pendingRefuelResponse.setTransactionStatus(pendingPostPaidTransactionBean.getMpTransactionStatus());
                        pendingRefuelResponse.setAmount(pendingPostPaidTransactionBean.getAmount());

                        //List<PostPaidCartData> PostPaidCartDataList = new HashSet<PostPaidCartData>(0);
                        PostPaidCartData postPaidCartData = new PostPaidCartData();
                        if (pendingPostPaidTransactionBean.getCartBean() != null) {
                            System.out.println("Pending Refuel Cart: " + pendingPostPaidTransactionBean.getCartBean());
                            for (PostPaidCartBean postPaidCartBean : pendingPostPaidTransactionBean.getCartBean()) {

                                postPaidCartData = new PostPaidCartData();
                                postPaidCartData.setAmount(postPaidCartBean.getAmount());
                                postPaidCartData.setProductDescription(postPaidCartBean.getProductDescription());
                                postPaidCartData.setProductId(postPaidCartBean.getProductId());
                                postPaidCartData.setQuantity(postPaidCartBean.getQuantity());
                                pendingRefuelResponse.getPostPaidCartDataList().add(postPaidCartData);

                            }
                        }

                        PostPaidRefuelData postPaidRefuelData = new PostPaidRefuelData();
                        if (pendingPostPaidTransactionBean.getRefuelBean() != null) {
                            System.out.println("Pending Refuel Refuel: " + pendingPostPaidTransactionBean.getRefuelBean());
                            for (PostPaidRefuelBean postPaidRefuelBean : pendingPostPaidTransactionBean.getRefuelBean()) {

                                postPaidRefuelData = new PostPaidRefuelData();
                                postPaidRefuelData.setFuelAmount(postPaidRefuelBean.getFuelAmount());
                                postPaidRefuelData.setFuelQuantity(postPaidRefuelBean.getFuelQuantity());
                                postPaidRefuelData.setFuelType(postPaidRefuelBean.getFuelType());
                                postPaidRefuelData.setProductDescription(postPaidRefuelBean.getProductDescription());
                                postPaidRefuelData.setProductId(postPaidRefuelBean.getProductId());
                                postPaidRefuelData.setPumpId(postPaidRefuelBean.getPumpId());

                                pendingRefuelResponse.getPostPaidRefuelDataList().add(postPaidRefuelData);

                                if (pendingPostPaidTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {

                                    CorePumpDetail corePumpDetail = new CorePumpDetail();
                                    corePumpDetail.setPumpID(postPaidRefuelBean.getPumpId());

                                    if (postPaidRefuelBean.getPumpNumber() != null) {
                                        corePumpDetail.setPumpNumber(postPaidRefuelBean.getPumpNumber().toString());
                                    }
                                    else {
                                        corePumpDetail.setPumpNumber(null);
                                    }

                                    corePumpDetail.setPumpStatus(null);
                                    CoreProductDetail coreProductDetail;
                                    coreProductDetail = new CoreProductDetail();
                                    coreProductDetail.setFuelType(postPaidRefuelBean.getFuelType());
                                    corePumpDetail.getProductDetails().add(coreProductDetail);
                                    pendingRefuelResponse.setPumpInfo(corePumpDetail);
                                    System.out.println("Pending Refuel PumpInfo: " + corePumpDetail.getPumpID());
                                }
                            }
                        }

                        if (pendingPostPaidTransactionBean.getSource().equals("CASH REGISTER")) {

                            CashInfo cashInfo = new CashInfo();
                            cashInfo.setCashId(pendingPostPaidTransactionBean.getSourceID());
                            cashInfo.setNumber(pendingPostPaidTransactionBean.getSourceNumber());
                            pendingRefuelResponse.setCashInfo(cashInfo);
                            System.out.println("Pending Refuel cashInfo: " + cashInfo.getCashId());
                        }
                        
                        String status = pendingRefuelResponse.getStatus();
                        Integer nextPollingInterval = defaultNextPollingInterval;
                        
                        // Ricerca la durata dell'intervallo di polling in base allo stato della transazione
                        // Se non viene trovato si utilizza il valore di default
                        PollingIntervalBean pollingIntervalBean = QueryRepository.findPollingIntervalByStatus(em, status);
                        if (pollingIntervalBean != null) {
                            nextPollingInterval = pollingIntervalBean.getNextPollingInterval();
                        }
                        
                        pendingRefuelResponse.setNextPollingInterval(nextPollingInterval);

                        userTransaction.commit();

                        return pendingRefuelResponse;
                    }
                }
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED retrieve pending refuel with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
}