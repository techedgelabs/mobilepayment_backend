package com.techedge.mp.core.actions.poptransaction;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UnavailabilityPeriodService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.ServiceAvailabilityData;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidApproveShopTransactionResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.mail.Email;
import com.techedge.mp.core.business.model.CardDepositTransactionBean;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ConsumeVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityConstants;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.LoadLoyaltyCreditsResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseConsumeVoucherTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.ReverseLoadLoyaltyCreditsTransactionResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.PaymentTransactionResult;
import com.techedge.mp.forecourt.integration.shop.interfaces.SendMPTransactionResultMessageResponse;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;
import com.techedge.mp.payment.adapter.business.interfaces.Extension;
import com.techedge.mp.payment.adapter.business.interfaces.GestPayData;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPTransactionApproveShopAction {

    @Resource
    private EJBContext                   context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager                em;

    @EJB
    private LoggerService                loggerService;

    @EJB
    private CRMService                   crmService;

    private PostPaidTransactionEventBean lastPostPaidTransactionEventBean;
    private Integer                      sequenceID      = 0;
    private String                       newStatus       = null;
    private String                       oldStatus       = null;
    private PaymentInfoBean              paymentInfoBean = null;

    public PoPTransactionApproveShopAction() {}

    public PostPaidApproveShopTransactionResponse execute(String requestID, String ticketID, String mpTransactionID, Long paymentMethodId, String paymentMethodType,
            String encodedPin, Boolean useVoucher, Integer pinCheckMaxAttempts, Integer voucherPurchaseMinInterval, List<String> userBlockExceptionList, GPServiceRemote gpService,
            ForecourtPostPaidServiceRemote forecourtPPService, FidelityServiceRemote fidelityService, UserCategoryService userCategoryService, Boolean receiptEmailActive,
            EmailSenderRemote emailSender, UnavailabilityPeriodService unavailabilityPeriodService, String proxyHost, String proxyPort, String proxyNoHosts, 
            StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        PostPaidApproveShopTransactionResponse poPApproveShopTransactionResponse = new PostPaidApproveShopTransactionResponse();
        try {
            userTransaction.begin();

            //    		this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "ServerName: " + serverName );
            //    		
            //    		CreateRefuelResponse createRefuelResponse = new CreateRefuelResponse();
            //    		
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_INVALID_TICKET);
                return poPApproveShopTransactionResponse;
            }
            //    		
            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_INVALID_TICKET);
                return poPApproveShopTransactionResponse;
            }

            if (userBlockExceptionList.contains(userBean.getPersonalDataBean().getSecurityDataEmail())) {

                System.out.println(userBean.getPersonalDataBean().getSecurityDataEmail() + " presente in lista utenti con eccezioni blocco");
            }
            else {
                // Controllo su disponibilit� del servizio di creazione transazioni postpaid
                ServiceAvailabilityData serviceAvailabilityData = unavailabilityPeriodService.retrieveServiceAvailability("CREATE_TRANSACTION_POSTPAID", new Date());
                if (serviceAvailabilityData != null) {

                    // Servizio non disponibile
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "ApproveShopTransaction unavailable");

                    userTransaction.commit();

                    poPApproveShopTransactionResponse.setStatusCode(serviceAvailabilityData.getStatusCode());
                    return poPApproveShopTransactionResponse;
                }
            }

            //    		// Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Unable to create refuel transaction in status " + userStatus);

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_UNAUTHORIZED);
                return poPApproveShopTransactionResponse;
            }

            // Controlla se l'utente ha gi� una transazione attiva
            //    		List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsActiveByUserBean(em, userBean);
            //		    
            //		    if ( !transactionBeanList.isEmpty() ) {
            //    			
            //    			// Esiste una transazione associata all'utente non ancora completata
            //		    	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Active transaction found" );
            //				
            //    			userTransaction.commit();
            //    			
            //    			createRefuelResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            //    			return createRefuelResponse;
            //    		}

            PostPaidTransactionBean poPTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, mpTransactionID);
            if (poPTransactionBean == null) {

                // Lo stationID inserito non corrisponde a nessuna stazione valida
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No postPayed transaction found by mpTransactionId "
                        + mpTransactionID);

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_NOT_RECOGNIZED);
                return poPApproveShopTransactionResponse;
            }
            Boolean isNewFlow = false;
            poPTransactionBean.setUserBean(userBean);

            isNewFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.NEW_PAYMENT_FLOW.getCode());
            if (isNewFlow) {

                System.out.println("Invio mail ad utente nuovo flusso");
            }
            else {

                System.out.println("Invio mail ad utente vecchio flusso");
            }
            System.out.println("Oggetto trovato");

            Integer userType = userBean.getUserType();
            Boolean useNewFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_PAYMENT_FLOW.getCode());
            // Calcolo dell'importo effettivo da pagare, togliendo il consumo dei voucher
            Double amount = poPTransactionBean.getAmount();

            PaymentInfoBean paymentInfoBean = QueryRepository.findPaymentMethodByIdAndType(em, paymentMethodId, paymentMethodType);

            if (paymentInfoBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Payment Method not found");

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_FAILURE);
                return poPApproveShopTransactionResponse;
            }

            System.out.println("full amount: " + amount);

            if (useNewFlow) {
                if (!paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER)) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "New payment flow but payment method is not voucher type");

                    userTransaction.commit();

                    poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PAYMENT_WRONG);
                    return poPApproveShopTransactionResponse;
                }

                poPApproveShopTransactionResponse = this.paymentNewFlow(requestID, amount, userTransaction, userBean, paymentMethodId, paymentMethodType, encodedPin,
                        pinCheckMaxAttempts, emailSender);
            }
            else {
                poPApproveShopTransactionResponse = this.paymentOldFlow(requestID, amount, userTransaction, userBean, paymentMethodId, paymentMethodType, encodedPin,
                        pinCheckMaxAttempts, emailSender);
            }

            if (poPApproveShopTransactionResponse.getStatusCode() != null) {
                return poPApproveShopTransactionResponse;
            }

            //    		Map<String,Object> properties = new HashMap();
            //    		  properties.put("javax.persistence.lock.timeout", 1);
            em.lock(poPTransactionBean, LockModeType.PESSIMISTIC_READ);

            System.out.println("Oggetto bloccato");

            if (!poPTransactionBean.getMpTransactionStatus().endsWith(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                        "Transaction Status not payable " + poPTransactionBean.getMpTransactionStatus());

                userTransaction.commit();

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_NOT_RECOGNIZED);
                return poPApproveShopTransactionResponse;
            }

            poPTransactionBean.setVoucherReconcile(false);
            poPTransactionBean.setLoyaltyReconcile(false);
            poPTransactionBean.setToReconcile(false);
            poPTransactionBean.setNotificationPaid(false);
            poPTransactionBean.setNotificationUser(false);
            String refuelMode = null; // valorizzato successivamente
            newStatus = StatusHelper.POST_PAID_STATUS_PAY_REQU;
            oldStatus = null;
            sequenceID = 1;
            Timestamp eventTimestamp = new Timestamp(new Date().getTime());
            String transactionEvent = StatusHelper.POST_PAID_EVENT_MA_PAY;
            String errorCode = null;
            String errorDescription = null;
            String eventResult = "OK";
            PostPaidTransactionEventBean lastPostPaidTransactionEventBean = poPTransactionBean.getLastPostPaidTransactionEventBean();
            List<ProductDetail> totalProductList = new ArrayList<ProductDetail>(0);

            if (lastPostPaidTransactionEventBean != null) {
                sequenceID = lastPostPaidTransactionEventBean.getSequenceID() + 1;
                oldStatus = lastPostPaidTransactionEventBean.getNewState();
            }

            PostPaidTransactionEventBean postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                    eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

            oldStatus = newStatus;

            em.persist(postPaidTransactionEventBean);

            if (poPTransactionBean.getRefuelBean().isEmpty() && poPTransactionBean.getCartBean().isEmpty()) {

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_NO_REFUEL_AND_CART_FOUND);
                return poPApproveShopTransactionResponse;
            }

            if (useNewFlow) {

                poPApproveShopTransactionResponse = this.doPaymentVoucherCredit(poPTransactionBean, requestID, amount, totalProductList, userBean, fidelityService,
                        mpTransactionID, postPaidTransactionEventBean, paymentMethodId, paymentMethodType, voucherPurchaseMinInterval, forecourtPPService, userTransaction,
                        emailSender, proxyHost, proxyPort, proxyNoHosts, userCategoryService, stringSubstitution);

                return poPApproveShopTransactionResponse;
            }

            int totalTransactionTask = 1;

            if (poPTransactionBean.getRefuelBean().size() > 1) {
                totalTransactionTask = poPTransactionBean.getRefuelBean().size();
            }
            else {
                refuelMode = FidelityConstants.REFUEL_MODE_ENI_CAFE_ONLY;
            }

            PostPaidRefuelBean[] refuelBeanArray = new PostPaidRefuelBean[poPTransactionBean.getRefuelBean().size()];
            poPTransactionBean.getRefuelBean().toArray(refuelBeanArray);

            for (int index = 0; index < totalTransactionTask; index++) {

                refuelMode = null;
                List<ProductDetail> productList = new ArrayList<ProductDetail>(0);
                if (!poPTransactionBean.getRefuelBean().isEmpty()) {
                    PostPaidRefuelBean refuelBean = refuelBeanArray[index];
                    System.out.println("trovato refuel mode: " + refuelBean.getRefuelMode());

                    if (refuelBean.getRefuelMode().equalsIgnoreCase("servito")) {
                        refuelMode = FidelityConstants.REFUEL_MODE_SERVITO;
                    }

                    if (refuelBean.getRefuelMode().equalsIgnoreCase("fai_da_te")) {
                        refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_POSTPAY;
                    }

                    if (refuelMode == null) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                                "Tipo di rifornimento sconosciuto. Operazione scartata!");
                    }

                    ProductDetail productDetail = new ProductDetail();
                    productDetail.setAmount(refuelBean.getFuelAmount());

                    System.out.println("trovato product id: " + refuelBean.getProductId());

                    if (refuelBean.getProductId().equals("SP")) {

                        // sp
                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_SP);
                    }
                    else {

                        if (refuelBean.getProductId().equals("GG")) {

                            // gasolio
                            productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GASOLIO);
                        }
                        else {

                            if (refuelBean.getProductId().equals("BS")) {

                                // blue_super
                                productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_SUPER);
                            }
                            else {

                                if (refuelBean.getProductId().equals("BD")) {

                                    // blue_diesel
                                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_DIESEL);
                                }
                                else {

                                    if (refuelBean.getProductId().equals("MT")) {

                                        // metano
                                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_METANO);
                                    }
                                    else {

                                        if (refuelBean.getProductId().equals("GP")) {

                                            // gpl
                                            productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GPL);
                                        }
                                        else {

                                            if (refuelBean.getProductId().equals("AD")) {

                                                // ???
                                                productDetail.setProductCode(null);
                                            }
                                            else {

                                                // non_oil
                                                productDetail.setProductCode(null);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    productDetail.setQuantity(refuelBean.getFuelQuantity());
                    productList.add(productDetail);
                    totalProductList.add(productDetail);
                }

                if (index == 0 && !poPTransactionBean.getCartBean().isEmpty()) {
                    System.out.println("trovato shop");

                    if (refuelMode == null) {
                        refuelMode = FidelityConstants.REFUEL_MODE_ENI_CAFE_ONLY;
                    }

                    boolean validProductFound = false;
                    Double shopAmount = 0.0;
                    Integer shopCount = 0;

                    for (PostPaidCartBean postPaidCartBean : poPTransactionBean.getCartBean()) {

                        if (postPaidCartBean.getProductId().equals(FidelityConstants.PRODUCT_CODE_NON_OIL))     // PRODOTTI NON OIL 70
                        {
                            validProductFound = true;
                            shopAmount = shopAmount + postPaidCartBean.getAmount();
                            shopCount = shopCount + postPaidCartBean.getQuantity();

                            System.out.println("amount: " + postPaidCartBean.getAmount() + ", count: " + postPaidCartBean.getQuantity());
                        }
                    }

                    if (validProductFound) {
                        ProductDetail shopProductDetail = new ProductDetail();

                        shopProductDetail.setAmount(shopAmount);
                        shopProductDetail.setProductCode(FidelityConstants.PRODUCT_CODE_NON_OIL);
                        shopProductDetail.setQuantity(Double.valueOf(shopCount));

                        productList.add(shopProductDetail);
                        totalProductList.add(shopProductDetail);
                    }
                }

                System.out.println("Controllo caricamento punti carta fedelt�");
                // Se l'utente ha una carta loyalty associata allora bisogna effettuare anche il caricamento dei punti
                LoyaltyCardBean loyaltyCardBean = userBean.getFirstLoyaltyCard();

                if (loyaltyCardBean != null && !productList.isEmpty()) {

                    System.out.println("Trovata carta fedelt�");

                    // Chimamata al servizio loadLoyaltyCredits

                    String operationID = new IdGenerator().generateId(16).substring(0, 33);
                    PartnerType partnerType = PartnerType.MP;
                    Long requestTimestamp = new Date().getTime();
                    String stationID = poPTransactionBean.getStationBean().getStationID();
                    String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;

                    String BIN = paymentInfoBean.getCardBin();

                    if (BIN != null && QueryRepository.findCardBinExists(em, BIN)) {
                        System.out.println("BIN valido (" + BIN + ")");
                        paymentMode = FidelityConstants.PAYMENT_METHOD_ENI_CARD;
                    }
                    else {
                        System.out.println("BIN Non Valido (" + BIN + ")");
                    }

                    PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean();
                    postPaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                    postPaidLoadLoyaltyCreditsBean.setOperationType("LOAD");
                    postPaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                    postPaidLoadLoyaltyCreditsBean.setEanCode(loyaltyCardBean.getEanCode());

                    try {

                        System.out.println("Chiamata servizio di carico punti");

                        LoadLoyaltyCreditsResult loadLoyaltyCreditsResult = fidelityService.loadLoyaltyCredits(operationID, mpTransactionID, stationID,
                                loyaltyCardBean.getPanCode(), BIN, refuelMode, paymentMode, "it", partnerType, requestTimestamp, "", productList);

                        System.out.println("Risposta servizio di carico punti: " + loadLoyaltyCreditsResult.getStatusCode() + " (" + loadLoyaltyCreditsResult.getMessageCode()
                                + ")");

                        postPaidLoadLoyaltyCreditsBean.setBalance(loadLoyaltyCreditsResult.getBalance());
                        postPaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                        postPaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                        postPaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                        postPaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                        postPaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                        postPaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                        postPaidLoadLoyaltyCreditsBean.setCsTransactionID(loadLoyaltyCreditsResult.getCsTransactionID());
                        postPaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                        postPaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                        postPaidLoadLoyaltyCreditsBean.setMessageCode(loadLoyaltyCreditsResult.getMessageCode());
                        postPaidLoadLoyaltyCreditsBean.setStatusCode(loadLoyaltyCreditsResult.getStatusCode());
                        postPaidLoadLoyaltyCreditsBean.setWarningMsg(loadLoyaltyCreditsResult.getWarningMsg());

                        postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);

                        poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(postPaidLoadLoyaltyCreditsBean);

                        //em.persist(postPaidLoadLoyaltyCreditsBean);
                        //em.merge(poPTransactionBean);

                        if (!loadLoyaltyCreditsResult.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {
                            errorCode = loadLoyaltyCreditsResult.getStatusCode();
                            errorDescription = loadLoyaltyCreditsResult.getMessageCode();
                            eventResult = "KO";
                        }
                        else {
                            errorCode = null;
                            errorDescription = null;
                            eventResult = "OK";
                        }

                    }
                    catch (Exception e) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error loading loyalty credits");

                        postPaidLoadLoyaltyCreditsBean.setStatusCode(StatusHelper.LOYALTY_STATUS_TO_BE_VERIFIED);
                        postPaidLoadLoyaltyCreditsBean.setMessageCode("STORNO DA VERIFICARE (" + e.getMessage() + ")");
                        postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);
                        poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(postPaidLoadLoyaltyCreditsBean);
                        poPTransactionBean.setLoyaltyReconcile(true);

                        errorCode = "9999";
                        errorDescription = "Error loading loyalty credits (" + e.getMessage() + ")";
                        eventResult = "ERROR";
                    }

                    oldStatus = newStatus;
                    newStatus = StatusHelper.POST_PAID_STATUS_CREDITS_LOAD;
                    sequenceID += 1;
                    eventTimestamp = new Timestamp(new Date().getTime());
                    transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY;

                    postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp,
                            transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                    em.persist(postPaidTransactionEventBean);

                }

                System.out.println("Nessuna carta fedelt� associata al codice fiscale (" + userBean.getPersonalDataBean().getFiscalCode() + ")");

                // Se l'utente ha scelto di utilizzare i voucher bisogna effetutare la chiamata al servizio consumeVoucher del fidelityAdapter
                poPTransactionBean.setUseVoucher(useVoucher);

                System.out.println("Utilizzo voucher: " + useVoucher);

                if (useVoucher == true && !productList.isEmpty()) {

                    System.out.println("Pagamento con voucher");

                    // Chimamata al servizio consumeVoucher

                    String operationID = new IdGenerator().generateId(16).substring(0, 33);
                    VoucherConsumerType voucherConsumerType = VoucherConsumerType.ENI;
                    PartnerType partnerType = PartnerType.MP;
                    Long requestTimestamp = new Date().getTime();
                    String stationID = poPTransactionBean.getStationBean().getStationID();
                    String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;

                    String BIN = paymentInfoBean.getCardBin();

                    if (BIN != null && QueryRepository.findCardBinExists(em, BIN)) {
                        System.out.println("BIN valido (" + BIN + ")");
                        paymentMode = FidelityConstants.PAYMENT_METHOD_ENI_CARD;
                    }
                    else {
                        System.out.println("BIN Non Valido (" + BIN + ")");
                    }

                    List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);

                    Boolean noVoucher = true;

                    for (VoucherBean voucherBean : userBean.getVoucherList()) {

                        System.out.println("Trovato voucher " + voucherBean.getCode() + " con stato " + voucherBean.getStatus());

                        if (voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO)) {

                            System.out.println("Voucher inserito in richiesta");

                            VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                            voucherCodeDetail.setVoucherCode(voucherBean.getCode());
                            voucherCodeList.add(voucherCodeDetail);

                            noVoucher = false;
                        }
                    }

                    if (noVoucher) {

                        System.out.println("No voucher found");
                    }
                    else {

                        ConsumeVoucherResult consumeVoucherResult = new ConsumeVoucherResult();

                        Double totalConsumed = 0.0;
                        PostPaidConsumeVoucherBean postPaidConsumeVoucherBean = new PostPaidConsumeVoucherBean();
                        postPaidConsumeVoucherBean.setOperationID(operationID);
                        postPaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
                        postPaidConsumeVoucherBean.setOperationType("CONSUME");
                        postPaidConsumeVoucherBean.setPostPaidTransactionBean(poPTransactionBean);

                        try {

                            consumeVoucherResult = fidelityService.consumeVoucher(operationID, mpTransactionID, voucherConsumerType, stationID, refuelMode, paymentMode, "it",
                                    partnerType, requestTimestamp, productList, voucherCodeList, FidelityConstants.CONSUME_TYPE_PARTIAL, "");

                            postPaidConsumeVoucherBean.setCsTransactionID(consumeVoucherResult.getCsTransactionID());
                            postPaidConsumeVoucherBean.setMarketingMsg(consumeVoucherResult.getMarketingMsg());
                            postPaidConsumeVoucherBean.setMessageCode(consumeVoucherResult.getMessageCode());
                            postPaidConsumeVoucherBean.setStatusCode(consumeVoucherResult.getStatusCode());
                            postPaidConsumeVoucherBean.setWarningMsg(consumeVoucherResult.getWarningMsg());

                            for (VoucherDetail voucherDetail : consumeVoucherResult.getVoucherList()) {

                                if (voucherDetail.getConsumedValue() == 0.0) {

                                    System.out.println(voucherDetail.getVoucherCode() + ": Valore consumato 0.0 -> aggiornamento non necessario");
                                    continue;
                                }

                                PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean = new PostPaidConsumeVoucherDetailBean();

                                postPaidConsumeVoucherDetailBean.setConsumedValue(voucherDetail.getConsumedValue());
                                postPaidConsumeVoucherDetailBean.setExpirationDate(voucherDetail.getExpirationDate());
                                postPaidConsumeVoucherDetailBean.setInitialValue(voucherDetail.getInitialValue());
                                postPaidConsumeVoucherDetailBean.setPromoCode(voucherDetail.getPromoCode());
                                postPaidConsumeVoucherDetailBean.setPromoDescription(voucherDetail.getPromoDescription());
                                postPaidConsumeVoucherDetailBean.setPromoDoc(voucherDetail.getPromoDoc());
                                postPaidConsumeVoucherDetailBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                                postPaidConsumeVoucherDetailBean.setVoucherCode(voucherDetail.getVoucherCode());
                                postPaidConsumeVoucherDetailBean.setVoucherStatus(voucherDetail.getVoucherStatus());
                                postPaidConsumeVoucherDetailBean.setVoucherType(voucherDetail.getVoucherType());
                                postPaidConsumeVoucherDetailBean.setVoucherValue(voucherDetail.getVoucherValue());
                                postPaidConsumeVoucherDetailBean.setPostPaidConsumeVoucherBean(postPaidConsumeVoucherBean);

                                postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean().add(postPaidConsumeVoucherDetailBean);

                                totalConsumed = totalConsumed + voucherDetail.getConsumedValue();

                                // Aggiorna le informazioni sul voucher associato all'utente

                                System.out.println("Aggiornamento voucher utente");

                                for (VoucherBean voucherBean : userBean.getVoucherList()) {

                                    if (voucherBean.getCode().equals(voucherDetail.getVoucherCode())) {

                                        System.out.println("Aggiornamento voucher " + voucherDetail.getVoucherCode());

                                        voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                                        voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                                        voucherBean.setInitialValue(voucherDetail.getInitialValue());
                                        voucherBean.setPromoCode(voucherDetail.getPromoCode());
                                        voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                                        voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                                        voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                                        voucherBean.setCode(voucherDetail.getVoucherCode());
                                        voucherBean.setStatus(voucherDetail.getVoucherStatus());
                                        voucherBean.setType(voucherDetail.getVoucherType());
                                        voucherBean.setValue(voucherDetail.getVoucherValue());
                                    }
                                }
                            }

                            // Aggiornamento dei dati utente
                            em.merge(userBean);

                            postPaidConsumeVoucherBean.setTotalConsumed(totalConsumed);

                            poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(postPaidConsumeVoucherBean);

                            System.out.println("Totale pagato con voucher: " + totalConsumed);

                            if (!consumeVoucherResult.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {
                                oldStatus = newStatus;
                                newStatus = StatusHelper.POST_PAID_STATUS_PAY_VOUCHER_FAIL;
                                errorCode = consumeVoucherResult.getStatusCode();
                                errorDescription = consumeVoucherResult.getMessageCode();
                                eventResult = "KO";
                            }
                            else {

                                oldStatus = newStatus;

                                if (totalConsumed < amount) {
                                    newStatus = StatusHelper.POST_PAID_STATUS_PAY_PARTIAL_VOUCHER;
                                }
                                else {
                                    newStatus = StatusHelper.POST_PAID_STATUS_PAY_FULL_VOUCHER;
                                }

                                errorCode = null;
                                errorDescription = null;
                                eventResult = "OK";
                            }
                        }
                        catch (Exception e) {

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error consuming vouchers");

                            postPaidConsumeVoucherBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED/* PostPaidConsumeVoucherBean.STATUS_CONSUME_TO_VERIFY */);
                            postPaidConsumeVoucherBean.setMessageCode("STORNO DA VERIFICARE (" + e.getMessage() + ")");
                            postPaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
                            postPaidConsumeVoucherBean.setPostPaidTransactionBean(poPTransactionBean);
                            poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(postPaidConsumeVoucherBean);
                            poPTransactionBean.setVoucherReconcile(true);

                            oldStatus = newStatus;
                            newStatus = StatusHelper.POST_PAID_STATUS_PAY_VOUCHER_FAIL;
                            errorCode = "9999";
                            errorDescription = "Error consuming vouchers (" + e.getMessage() + ")";
                            eventResult = "ERROR";

                        }

                        sequenceID += 1;

                        eventTimestamp = new Timestamp(new Date().getTime());
                        transactionEvent = StatusHelper.POST_PAID_EVENT_BE_VOUCHER;

                        postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp,
                                transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                        oldStatus = newStatus;
                        em.persist(postPaidTransactionEventBean);
                    }

                }
                else {

                    System.out.println("Pagamento senza voucher");
                }
            }

            poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

            for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : poPTransactionBean.getPostPaidConsumeVoucherBeanList()) {

                if (postPaidConsumeVoucherBean == null || postPaidConsumeVoucherBean.getStatusCode() == null
                        || !postPaidConsumeVoucherBean.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {
                    System.out.println("PostPaidConsumeVoucherBean non valido (id: " + postPaidConsumeVoucherBean.getId() + ")");
                    continue;
                }

                System.out.println("Trovato PostPaidConsumeVoucherBean (id: " + postPaidConsumeVoucherBean.getId() + ")");

                Double totalConsumed = postPaidConsumeVoucherBean.getTotalConsumed();

                if (totalConsumed > amount) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error: total voucher > amount");

                    // TODO come procedere?

                    amount = 0.0;
                }
                else {

                    amount = amount - totalConsumed;

                    // Se amount > 0 e amount < 0.01 ==> effettua un arrotondamento alla seconda cifra decimale

                    System.out.println("round before: " + amount);

                    BigDecimal bd = new BigDecimal(amount).setScale(2, RoundingMode.HALF_EVEN);
                    amount = bd.doubleValue();

                    System.out.println("round after: " + amount);
                }

                System.out.println("result amount: " + amount);

                // Salva su db le informazioni sui voucher utilizzati
                System.out.println("Salvataggio su db delle informazioni sui voucher utilizzati");

                //em.merge(postPaidConsumeVoucherBean);
                //em.merge(poPTransactionBean);

                if (!postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean().isEmpty()) {

                    for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {

                        System.out.println("Trovato PostPaidConsumeVoucherDetailBean");

                        if (postPaidConsumeVoucherDetailBean.getConsumedValue() > 0) {

                            System.out.println("Trovato PostPaidConsumeVoucherDetailBean con consumedValue positivo");

                            //em.merge(postPaidConsumeVoucherDetailBean);
                            //em.merge(postPaidConsumeVoucherBean);
                        }
                    }
                }
                else {

                    System.out.println("PostPaidConsumeVoucherDetailBean vuoto");
                }

            }

            //PostPaidConsumeVoucherBean postPaidConsumeVoucherBean = poPTransactionBean.getLastPostPaidConsumeVoucherBean();

            System.out.println("Informazioni sui voucher utilizzati salvati");

            eventTimestamp = new java.sql.Timestamp(new Date().getTime());

            if (amount == 0.0) {

                System.out.println("Pagamento con carta di credito non necessario");

                poPTransactionBean.setPaymentMethodId(paymentMethodId);
                poPTransactionBean.setToken(paymentInfoBean.getToken());
                poPTransactionBean.setUserBean(userBean);
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
                poPTransactionBean.setLastModifyTimestamp(eventTimestamp);

                Boolean loyaltyCredits = isCreditsLoyaltyLoaded(poPTransactionBean);

                oldStatus = newStatus;
                sequenceID += 1;
                String notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                        poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), null, loyaltyCredits, poPTransactionBean, forecourtPPService,
                        StatusHelper.POST_PAID_STATUS_PAY_COMPLETE, StatusHelper.POST_PAID_STATUS_NOTIFICATION_FAIL, StatusHelper.POST_PAID_STATUS_CANCELED);

                if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS)) {
                    System.out.println("Storno non necessario");

                    poPTransactionBean.setNotificationPaid(true);
                    poPTransactionBean.setNotificationUser(true);

                    updateVoucherAndLoyaltyData(poPTransactionBean);

                    //em.persist(poPTransactionBean);
                    //Email.sendPostPaidSummary(emailSender, poPTransactionBean, null, proxyHost, proxyPort);

                    //userTransaction.commit();
                    poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_OK);
                    poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

                    //return poPApproveShopTransactionResponse;

                }

                if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
                    poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                    poPTransactionBean.setToReconcile(true);
                    poPTransactionBean.setNotificationPaid(false);
                    poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                    poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                }

                if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                        || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                        || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)
                        || (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_REJECTED) && poPTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode()))) {

                    System.out.println("Storno voucher e loyalty necessario");
                    poPTransactionBean.setNotificationPaid(true);

                    try {
                        // Storno caricamento punti carta loyalty e aggiornamento dati utante
                        this.revertLoadLoyaltyCredits(poPTransactionBean, fidelityService);
                    }
                    catch (FidelityServiceException ex) {
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                                "Exception in revertLoadLoyaltyCredits: " + ex.getLocalizedMessage());
                        poPTransactionBean.setLoyaltyReconcile(true);
                        poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                    }

                    try {
                        // Storno movimentazione voucher e aggiornamento voucher utente
                        this.reverseConsumeVoucher(poPTransactionBean, fidelityService);
                        poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);

                    }
                    catch (Exception ex) {
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                                "Exception in revertConsumeVoucher: " + ex.getLocalizedMessage());
                        poPTransactionBean.setVoucherReconcile(true);
                        poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                    }

                    updateVoucherAndLoyaltyData(poPTransactionBean);

                    //em.persist(poPTransactionBean);
                    //Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort);
                    //userTransaction.commit();
                    poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                    poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

                    //return poPApproveShopTransactionResponse;

                }
                //else if (notifyResponse.equals("MESSAGE_ERROR_500")) {
                /*
                 * else {
                 * poPTransactionBean.setToReconcile(true);
                 * poPTransactionBean.setVoucherReconcile(true);
                 * poPTransactionBean.setNotificationPaid(false);
                 * poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                 * em.persist(poPTransactionBean);
                 * 
                 * userTransaction.commit();
                 * 
                 * poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                 * poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                 * 
                 * return poPApproveShopTransactionResponse;
                 * 
                 * }
                 */

                em.persist(poPTransactionBean);
                Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, stringSubstitution);
                
                userTransaction.commit();
                return poPApproveShopTransactionResponse;
            }

            System.out.println("Pagamento con carta di credito");

            Extension[] extension_array = new Extension[1];
            Extension i_extension = new Extension();
            i_extension.setKey("CST_DISTRID");
            i_extension.setValue(poPTransactionBean.getStationBean().getStationID());
            extension_array[0] = i_extension;

            sequenceID += 1;
            Integer sequencePaymentID = 1;

            GestPayData gestPayDataAUTHResponse = doPaymentAuth(poPTransactionBean, paymentInfoBean, userBean, paymentMethodId, amount, sequencePaymentID, requestID, sequenceID,
                    gpService, extension_array);

            if (gestPayDataAUTHResponse.getTransactionResult().equals("ERROR")) {
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);
                poPTransactionBean.setToReconcile(true);

                try {
                    // Storno caricamento punti carta loyalty e aggiornamento dati utante
                    this.revertLoadLoyaltyCredits(poPTransactionBean, fidelityService);
                }
                catch (FidelityServiceException ex) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                            "Exception in revertLoadLoyaltyCredits: " + ex.getLocalizedMessage());
                    poPTransactionBean.setLoyaltyReconcile(true);
                }

                try {
                    // Storno movimentazione voucher e aggiornamento voucher utente
                    this.reverseConsumeVoucher(poPTransactionBean, fidelityService);
                }
                catch (Exception ex) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                            "Exception in revertConsumeVoucher: " + ex.getLocalizedMessage());
                    poPTransactionBean.setVoucherReconcile(true);
                }

                Boolean loyaltyCredits = isCreditsLoyaltyLoaded(poPTransactionBean);
                PaymentTransactionResult paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataAUTHResponse, amount, "AUT");

                sequenceID += 1;
                String notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                        poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                        forecourtPPService, StatusHelper.POST_PAID_STATUS_PAY_AUTH_MISSING, null, null);

                if (!notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS)) {
                    poPTransactionBean.setNotificationPaid(false);
                }
                else {
                    poPTransactionBean.setNotificationPaid(true);
                }

                updateVoucherAndLoyaltyData(poPTransactionBean);

                em.persist(poPTransactionBean);

                Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, stringSubstitution);
                
                userTransaction.commit();
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_AUTH_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                return poPApproveShopTransactionResponse;
            }

            if (gestPayDataAUTHResponse.getTransactionResult().equals("KO")) {

                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);

                try {
                    // Storno caricamento punti carta loyalty e aggiornamento dati utante
                    this.revertLoadLoyaltyCredits(poPTransactionBean, fidelityService);
                }
                catch (FidelityServiceException ex) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                            "Exception in revertLoadLoyaltyCredits: " + ex.getLocalizedMessage());
                    poPTransactionBean.setLoyaltyReconcile(true);
                }

                try {
                    // Storno movimentazione voucher e aggiornamento voucher utente
                    this.reverseConsumeVoucher(poPTransactionBean, fidelityService);
                }
                catch (Exception ex) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                            "Exception in revertConsumeVoucher: " + ex.getLocalizedMessage());
                    poPTransactionBean.setVoucherReconcile(true);
                }

                Boolean loyaltyCredits = isCreditsLoyaltyLoaded(poPTransactionBean);
                PaymentTransactionResult paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataAUTHResponse, amount, "AUT");

                sequenceID += 1;
                String notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                        poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                        forecourtPPService, StatusHelper.POST_PAID_STATUS_PAY_AUTH_REFUSED, null, null);

                if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                        || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                        || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)) {

                    poPTransactionBean.setNotificationPaid(true);

                }
                else {
                    poPTransactionBean.setToReconcile(true);
                    poPTransactionBean.setNotificationPaid(false);
                }

                updateVoucherAndLoyaltyData(poPTransactionBean);

                em.persist(poPTransactionBean);

                Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, stringSubstitution);
                
                userTransaction.commit();
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_AUTH_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                return poPApproveShopTransactionResponse;
            }

            sequenceID += 1;
            sequencePaymentID += 1;
            
            PostPaidRefuelBean postPaidRefuelBean = null;
            if (!poPTransactionBean.getRefuelBean().isEmpty()) {
                postPaidRefuelBean = poPTransactionBean.getRefuelBean().iterator().next();
            }

            GestPayData gestPayDataSETTLEResponse = doPaymentSettle(poPTransactionBean, postPaidRefuelBean, paymentInfoBean, userBean, paymentMethodId, amount, sequencePaymentID, requestID,
                    sequenceID, gpService);

            if (gestPayDataSETTLEResponse.getTransactionResult().equals("ERROR")) {
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);
                poPTransactionBean.setToReconcile(true);
                poPTransactionBean.setNotificationPaid(false);

                em.persist(poPTransactionBean);

                Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, stringSubstitution);
                
                userTransaction.commit();
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                return poPApproveShopTransactionResponse;
            }

            if (gestPayDataSETTLEResponse.getTransactionResult().equals("KO")) {

                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);

                sequenceID += 1;
                sequencePaymentID += 1;

                GestPayData gestPayDataDELETEResponse = doPaymentAuthDelete(poPTransactionBean, paymentInfoBean, userBean, paymentMethodId, amount, sequencePaymentID, requestID,
                        sequenceID, gpService);

                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);

                try {
                    // Storno caricamento punti carta loyalty e aggiornamento dati utante
                    this.revertLoadLoyaltyCredits(poPTransactionBean, fidelityService);
                }
                catch (FidelityServiceException ex) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                            "Exception in revertLoadLoyaltyCredits: " + ex.getLocalizedMessage());
                    poPTransactionBean.setLoyaltyReconcile(true);
                }

                try {
                    // Storno movimentazione voucher e aggiornamento voucher utente
                    this.reverseConsumeVoucher(poPTransactionBean, fidelityService);
                }
                catch (Exception ex) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                            "Exception in revertConsumeVoucher: " + ex.getLocalizedMessage());
                    poPTransactionBean.setVoucherReconcile(true);
                }

                Boolean loyaltyCredits = isCreditsLoyaltyLoaded(poPTransactionBean);
                PaymentTransactionResult paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataSETTLEResponse, amount, "MOV");

                if (!gestPayDataDELETEResponse.getTransactionResult().equals("OK")) {

                    poPTransactionBean.setToReconcile(true);

                    sequenceID += 1;
                    String notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                            poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                            forecourtPPService, StatusHelper.POST_PAID_STATUS_PAY_AUTH_DELETE_MISSING, null, null);

                    if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                            || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                            || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)) {

                        poPTransactionBean.setNotificationPaid(true);

                    }
                    else {
                        poPTransactionBean.setNotificationPaid(false);
                    }
                }
                else {
                    sequenceID += 1;
                    String notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                            poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                            forecourtPPService, StatusHelper.POST_PAID_STATUS_CANCELED, null, null);

                    if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                            || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                            || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)) {

                        poPTransactionBean.setNotificationPaid(true);
                        poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);
                    }
                    else {
                        poPTransactionBean.setToReconcile(true);
                        poPTransactionBean.setNotificationPaid(false);
                    }
                }

                updateVoucherAndLoyaltyData(poPTransactionBean);

                em.persist(poPTransactionBean);

                Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                        userCategoryService, stringSubstitution);
                
                userTransaction.commit();
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                return poPApproveShopTransactionResponse;
            }

            poPTransactionBean.setNotificationUser(true);
            Boolean loyaltyCredits = isCreditsLoyaltyLoaded(poPTransactionBean);

            PaymentTransactionResult paymentTransactionResult = this.generatePaymentTransactionResult(poPTransactionBean, gestPayDataSETTLEResponse, amount, "MOV");

            sequenceID += 1;
            String notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                    poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), paymentTransactionResult, loyaltyCredits, poPTransactionBean,
                    forecourtPPService, StatusHelper.POST_PAID_STATUS_PAY_COMPLETE, StatusHelper.POST_PAID_STATUS_NOTIFICATION_FAIL, StatusHelper.POST_PAID_STATUS_PAY_MOV_DELETE);

            if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS)) {
                poPTransactionBean.setNotificationPaid(true);
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_OK);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            }

            if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setToReconcile(true);
                poPTransactionBean.setNotificationPaid(false);
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            }

            if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED)
                    || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                    || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)
                    || (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_REJECTED) && poPTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode()))) {

                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setNotificationUser(true);
                poPTransactionBean.setNotificationPaid(true);

                sequenceID += 1;
                sequencePaymentID += 1;

                GestPayData gestPayDataREFUNDResponse = doPaymentRefund(poPTransactionBean, paymentInfoBean, userBean, paymentMethodId, amount, sequencePaymentID, requestID,
                        sequenceID, gpService);

                if (!gestPayDataREFUNDResponse.getTransactionResult().equals("OK")) {

                    poPTransactionBean.setToReconcile(true);
                }
                else {

                    poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);

                    try {
                        // Storno caricamento punti carta loyalty e aggiornamento dati utante
                        this.revertLoadLoyaltyCredits(poPTransactionBean, fidelityService);
                    }
                    catch (FidelityServiceException ex) {
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                                "Exception in revertLoadLoyaltyCredits: " + ex.getLocalizedMessage());
                        poPTransactionBean.setLoyaltyReconcile(true);
                    }

                    try {
                        // Storno movimentazione voucher e aggiornamento voucher utente
                        this.reverseConsumeVoucher(poPTransactionBean, fidelityService);
                    }
                    catch (Exception ex) {
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                                "Exception in revertConsumeVoucher: " + ex.getLocalizedMessage());
                        poPTransactionBean.setVoucherReconcile(true);
                    }
                }

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            }

            em.persist(poPTransactionBean);
            updateVoucherAndLoyaltyData(poPTransactionBean);

            Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                    userCategoryService, stringSubstitution);
            
            userTransaction.commit();
            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_OK);
            poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

            return poPApproveShopTransactionResponse;
        }
        catch (Exception ex2) {

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());

            if (ex2.getClass().getSimpleName().equals("LockAcquisitionException")) {

                System.out.println("Dentro If" + ex2.getClass().getSimpleName());
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Transaction Status not payable ");
                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_NOT_RECOGNIZED);

                try {
                    userTransaction.rollback();
                }
                catch (IllegalStateException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (SecurityException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                catch (SystemException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                return poPApproveShopTransactionResponse;

            }

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("Eccezione" + ex2.getClass().getSimpleName());
            String message = "FAILED Post Paid transaction creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

    private PostPaidApproveShopTransactionResponse paymentOldFlow(String requestID, Double amount,/* PostPaidTransactionBean poPTransactionBean, */
            UserTransaction userTransaction, UserBean userBean, Long paymentMethodId, String paymentMethodType, String encodedPin, Integer pinCheckMaxAttempts,
            EmailSenderRemote emailSender) throws Exception {
        System.out.println("Utilizzo vecchio flusso voucher");

        PostPaidApproveShopTransactionResponse poPApproveShopTransactionResponse = new PostPaidApproveShopTransactionResponse();

        if (paymentMethodId == null && paymentMethodType == null) {

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Default method found");

            paymentInfoBean = userBean.findDefaultPaymentInfoBean();
        }
        else {

            paymentInfoBean = userBean.findPaymentInfoBean(paymentMethodId, paymentMethodType);
        }

        // Verifica che il metodo di pagamento selezionato sia in uno stato valido
        if (paymentInfoBean == null) {

            // Il metodo di pagamento selezionato non esiste
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");

            userTransaction.commit();

            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PAYMENT_WRONG);
            return poPApproveShopTransactionResponse;
        }

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentMethodId + " paymentMethodType: "
                + paymentMethodType);

        if (!paymentInfoBean.isValid()) {

            // Il metodo di pagamento selezionato non pu� essere utilizzato
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data status: " + paymentInfoBean.getStatus());

            userTransaction.commit();

            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PAYMENT_WRONG);
            return poPApproveShopTransactionResponse;
        }

        double capAvailable = 0.0;

        if (userBean.getCapAvailable() != null) {
            capAvailable = userBean.getCapAvailable().doubleValue();
        }

        if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED && capAvailable < amount.doubleValue()) {

            // Transazione annullata per sistema di pagamento non verificato e ammontare superiore al cap
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Payment not verified and transaction amount exceeding the cap");

            Email.sendPaymentNotVerified(emailSender, paymentInfoBean, userBean, null, null);

            userTransaction.commit();

            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_CAP_FAILURE);
            return poPApproveShopTransactionResponse;
        }

        if (!encodedPin.equals(paymentInfoBean.getPin())) {

            // Il pin inserito non � valido
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");

            // Si sottrae uno al numero di tentativi residui
            Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
            if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                pinCheckAttemptsLeft--;
            }
            else {
                pinCheckAttemptsLeft = 0;
            }

            if (pinCheckAttemptsLeft == 0) {

                // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                paymentInfoBean.setDefaultMethod(false);
                paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

            }

            paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

            em.merge(paymentInfoBean);

            userTransaction.commit();

            poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_PIN_WRONG);
            return poPApproveShopTransactionResponse;
        }

        // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
        paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);
        em.merge(paymentInfoBean);
        return poPApproveShopTransactionResponse;
    }

    private PostPaidApproveShopTransactionResponse paymentNewFlow(String requestID, Double amount, UserTransaction userTransaction, UserBean userBean, Long paymentMethodId,
            String paymentMethodType, String encodedPin, Integer pinCheckMaxAttempts, EmailSenderRemote emailSender) throws Exception {
        System.out.println("Utilizzo nuovo flusso voucher");

        PostPaidApproveShopTransactionResponse poPApproveShopTransactionResponse = new PostPaidApproveShopTransactionResponse();

        System.out.println("Utilizzo voucher come metodo di pagamento");

        // Verifica che l'utente abbia gi� associato il pin
        paymentInfoBean = userBean.getVoucherPaymentMethod();

        if (paymentInfoBean == null || paymentInfoBean.getPin() == null || paymentInfoBean.getPin().equals("")) {

            // Errore Pin non ancora inserito
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Pin not inserted");

            userTransaction.commit();

            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_FAILURE);
            return poPApproveShopTransactionResponse;
        }

        // Verifica che il pin indicato per il rifornimento coincida con quello inserito in iscrizione
        if (!encodedPin.equals(paymentInfoBean.getPin())) {

            // Il pin inserito non � valido
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong pin");

            // Si sottrae uno al numero di tentativi residui
            Integer pinCheckAttemptsLeft = paymentInfoBean.getPinCheckAttemptsLeft();
            if (pinCheckAttemptsLeft != null && pinCheckAttemptsLeft > 0) {
                pinCheckAttemptsLeft--;
            }
            else {
                pinCheckAttemptsLeft = 0;
            }

            if (pinCheckAttemptsLeft == 0) {

                // Se i tentativi sono terminati il metodo di pagamento viene bloccato e il flag di default viene messo a false
                paymentInfoBean.setDefaultMethod(false);
                paymentInfoBean.setStatus(PaymentInfo.PAYMENTINFO_STATUS_BLOCKED);

                // e tutti i metodi di pagamento verificati e non verificati sono cancellati
                for (PaymentInfoBean paymentInfo : userBean.getPaymentData()) {

                    if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD)
                            && (paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED)) {

                        paymentInfo.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                        paymentInfo.setDefaultMethod(false);
                        em.merge(paymentInfo);
                    }
                }
            }

            paymentInfoBean.setPinCheckAttemptsLeft(pinCheckAttemptsLeft);

            em.merge(paymentInfoBean);

            userTransaction.commit();

            poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.REFUEL_TRANSACTION_CREATE_PIN_WRONG);
            return poPApproveShopTransactionResponse;
        }

        // L'utente ha inserito il pin corretto, quindi il numero di tentativi residui viene riportato al valore iniziale
        paymentInfoBean.setPinCheckAttemptsLeft(pinCheckMaxAttempts);

        em.merge(paymentInfoBean);

        return poPApproveShopTransactionResponse;
    }

    private GestPayData doPaymentAuth(PostPaidTransactionBean poPTransactionBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId, Double amount,
            Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService, Extension[] extension_array) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_AUTH;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_AUTH;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        String transactionType = poPTransactionBean.getTransactionCategory().getValue();
        
        String paymentMethodExpiration = "";
        
        if (paymentInfoBean != null) {
            
            CardDepositTransactionBean cardDepositTransactionBean = QueryRepository.findCardDepositTransactionByShopTransactionID(em, paymentInfoBean.getCheckShopTransactionID());
            if (cardDepositTransactionBean != null) {
                
                String tokenExpiryMonth = cardDepositTransactionBean.getTokenExpiryMonth();
                String tokenExpiryYear  = cardDepositTransactionBean.getTokenExpiryYear();
                
                paymentMethodExpiration = "20" + tokenExpiryYear + tokenExpiryMonth;
            }                
        }

        GestPayData gestPayDataAUTHResponse = gpService.callPagam(amount, poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), paymentInfoBean.getToken(), null, poPTransactionBean.getAcquirerID(), poPTransactionBean.getGroupAcquirer(),
                poPTransactionBean.getEncodedSecretKey(), paymentMethodExpiration, extension_array, transactionType);

        if (gestPayDataAUTHResponse == null) {
            gestPayDataAUTHResponse = new GestPayData();
            gestPayDataAUTHResponse.setTransactionResult("ERROR");
            gestPayDataAUTHResponse.setErrorCode("9999");
            gestPayDataAUTHResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH: " + gestPayDataAUTHResponse.getTransactionResult());

        errorCode = gestPayDataAUTHResponse.getErrorCode();
        errorDescription = gestPayDataAUTHResponse.getErrorDescription();
        eventResult = gestPayDataAUTHResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataAUTHResponse, "AUT");

        em.persist(postPaidTransactionPaymentEventBean);

        poPTransactionBean.setPaymentMethodId(paymentMethodId);
        poPTransactionBean.setToken(paymentInfoBean.getToken());
        poPTransactionBean.setUserBean(userBean);
        poPTransactionBean.setLastModifyTimestamp(eventTimestamp);
        poPTransactionBean.setBankTansactionID(gestPayDataAUTHResponse.getBankTransactionID());
        poPTransactionBean.setAuthorizationCode(gestPayDataAUTHResponse.getAuthorizationCode());

        oldStatus = newStatus;

        return gestPayDataAUTHResponse;
    }

    private GestPayData doPaymentSettle(PostPaidTransactionBean poPTransactionBean, PostPaidRefuelBean postPaidRefuelBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId, Double amount,
            Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_MOV;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_MOV;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataSETTLEResponse = gpService.callSettle(amount, poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), poPTransactionBean.getAcquirerID(), poPTransactionBean.getGroupAcquirer(), poPTransactionBean.getEncodedSecretKey(),
                poPTransactionBean.getToken(), poPTransactionBean.getAuthorizationCode(), poPTransactionBean.getBankTansactionID(), poPTransactionBean.getRefuelMode(),
                poPTransactionBean.getProductID(), postPaidRefuelBean.getFuelQuantity(), postPaidRefuelBean.getUnitPrice());

        if (gestPayDataSETTLEResponse == null) {
            gestPayDataSETTLEResponse = new GestPayData();
            gestPayDataSETTLEResponse.setTransactionResult("ERROR");
            gestPayDataSETTLEResponse.setErrorCode("9999");
            gestPayDataSETTLEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay SETTLE: " + gestPayDataSETTLEResponse.getTransactionResult());

        errorCode = gestPayDataSETTLEResponse.getErrorCode();
        errorDescription = gestPayDataSETTLEResponse.getErrorDescription();
        eventResult = gestPayDataSETTLEResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataSETTLEResponse, "MOV");

        em.persist(postPaidTransactionPaymentEventBean);

        poPTransactionBean.setPaymentMethodId(paymentMethodId);
        poPTransactionBean.setToken(paymentInfoBean.getToken());
        poPTransactionBean.setUserBean(userBean);
        poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
        poPTransactionBean.setLastModifyTimestamp(eventTimestamp);
        //poPTransactionBean.setBankTansactionID(gestPayDataSETTLEResponse.getBankTransactionID());

        //em.persist(poPTransactionBean);

        oldStatus = newStatus;

        return gestPayDataSETTLEResponse;
    }

    private GestPayData doPaymentAuthDelete(PostPaidTransactionBean poPTransactionBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId, Double amount,
            Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_AUTH_DELETE;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_AUTH_DEL;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataDELETEResponse = gpService.deletePagam(poPTransactionBean.getAmount(), poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), poPTransactionBean.getBankTansactionID(), null, poPTransactionBean.getAcquirerID(), poPTransactionBean.getGroupAcquirer(),
                poPTransactionBean.getEncodedSecretKey());

        if (gestPayDataDELETEResponse == null) {
            gestPayDataDELETEResponse = new GestPayData();
            gestPayDataDELETEResponse.setTransactionResult("ERROR");
            gestPayDataDELETEResponse.setErrorCode("9999");
            gestPayDataDELETEResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay AUTH DEL: " + gestPayDataDELETEResponse.getTransactionResult());

        errorCode = gestPayDataDELETEResponse.getErrorCode();
        errorDescription = gestPayDataDELETEResponse.getErrorDescription();
        eventResult = gestPayDataDELETEResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataDELETEResponse, "DEL");

        em.persist(postPaidTransactionPaymentEventBean);

        oldStatus = newStatus;

        return gestPayDataDELETEResponse;
    }

    private GestPayData doPaymentRefund(PostPaidTransactionBean poPTransactionBean, PaymentInfoBean paymentInfoBean, UserBean userBean, Long paymentMethodId,
            Double residualAmount, Integer sequencePaymentID, String requestID, Integer eventSequenceID, GPServiceRemote gpService) {

        PostPaidTransactionEventBean postPaidTransactionEventBean;
        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean;
        newStatus = StatusHelper.POST_PAID_STATUS_PAY_MOV_REVERSE;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_REVERSE;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";

        GestPayData gestPayDataREFUNDResponse = gpService.callRefund(residualAmount, poPTransactionBean.getMpTransactionID(), poPTransactionBean.getShopLogin(),
                poPTransactionBean.getCurrency(), poPTransactionBean.getToken(), poPTransactionBean.getAuthorizationCode(), poPTransactionBean.getBankTansactionID(),
                poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getAcquirerID(), poPTransactionBean.getGroupAcquirer(),
                poPTransactionBean.getEncodedSecretKey());

        if (gestPayDataREFUNDResponse == null) {
            gestPayDataREFUNDResponse = new GestPayData();
            gestPayDataREFUNDResponse.setTransactionResult("ERROR");
            gestPayDataREFUNDResponse.setErrorCode("9999");
            gestPayDataREFUNDResponse.setErrorDescription(StatusHelper.SUBSTATUS_PAYMENT_FAULT_DESCRIPTION);
            poPTransactionBean.setToReconcile(true);
        }

        System.out.println("Operazione GestPay REFUND: " + gestPayDataREFUNDResponse.getTransactionResult());

        errorCode = gestPayDataREFUNDResponse.getErrorCode();
        errorDescription = gestPayDataREFUNDResponse.getErrorDescription();
        eventResult = gestPayDataREFUNDResponse.getTransactionResult();

        postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        postPaidTransactionPaymentEventBean = this.generateTransactionPaymentEvent(sequencePaymentID, poPTransactionBean, gestPayDataREFUNDResponse, "STO");

        em.persist(postPaidTransactionPaymentEventBean);

        oldStatus = newStatus;

        return gestPayDataREFUNDResponse;
    }

    private String notifyResponse(String requestID, Integer eventSequenceID, String stationID, String mpTransactionID, String srcTransactionID, String transactionResult,
            PaymentTransactionResult paymentTransactionResult, Boolean loyaltyCredits, PostPaidTransactionBean poPTransactionBean,
            ForecourtPostPaidServiceRemote forecourtPPService, String eventStatusOk, String eventStatusError, String eventStatusCancelled) {

        // TODO Auto-generated method stub

        System.out.println("*** notifyResponse ***");
        System.out.println("stationID: " + stationID);
        System.out.println("mpTransactionID: " + mpTransactionID);
        System.out.println("srcTransactionID: " + srcTransactionID);
        System.out.println("transactionResult: " + transactionResult);
        if (loyaltyCredits != null) {
            System.out.println("loyaltyCredits: " + loyaltyCredits);
        }
        else {
            System.out.println("loyaltyCredits: null");
        }

        List<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail> vouchers = new ArrayList<com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail>(0);

        for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : poPTransactionBean.getPostPaidConsumeVoucherBeanList()) {

            for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {

                com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail voucherDetail = new com.techedge.mp.forecourt.integration.shop.interfaces.VoucherDetail();

                if (postPaidConsumeVoucherDetailBean.getConsumedValue() == null || postPaidConsumeVoucherDetailBean.getConsumedValue().equals(new Double("0.0"))) {
                    continue;
                }

                voucherDetail.setPromoCode(postPaidConsumeVoucherDetailBean.getPromoCode());
                voucherDetail.setPromoDescription(postPaidConsumeVoucherDetailBean.getPromoDescription());
                voucherDetail.setVoucherAmount(postPaidConsumeVoucherDetailBean.getConsumedValue());
                voucherDetail.setVoucherCode(postPaidConsumeVoucherDetailBean.getVoucherCode());

                System.out.println("voucher - promoCode: " + voucherDetail.getPromoCode() + ", promoDescription: " + voucherDetail.getPromoDescription() + ", voucherAmount: "
                        + voucherDetail.getVoucherAmount() + ", voucherCode: " + voucherDetail.getVoucherCode());

                vouchers.add(voucherDetail);
            }
        }

        SendMPTransactionResultMessageResponse sendMPTransactionResultMessageResponse = null;
        String notifyResponse = null;
        String errorCode = null;
        String errorDescription = null;
        String eventResult = null;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_NOTIFICATION;

        try {
            sendMPTransactionResultMessageResponse = forecourtPPService.sendMPTransactionResult(requestID, stationID, mpTransactionID, srcTransactionID, transactionResult,
                    paymentTransactionResult, loyaltyCredits, vouchers, null);
            notifyResponse = sendMPTransactionResultMessageResponse.getStatusCode();
        }
        catch (Exception ex) {
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                    "Exception in sendMpTransactionResult: " + ex.getLocalizedMessage());

            notifyResponse = StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR;
            errorCode = "9999";
            errorDescription = "Error in sendMpTransactionResult (" + ex.getMessage() + ")";
            sendMPTransactionResultMessageResponse.setStatusCode(notifyResponse);
            sendMPTransactionResultMessageResponse.setMessageCode(ex.getMessage());
        }

        //if (!sendMPTransactionResultMessageResponse.getStatusCode().equals("MESSAGE_RECEIVED_200")) {}

        System.out.println("sendMPTransactionResult: " + notifyResponse);

        if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS)) {
            newStatus = eventStatusOk;
            eventResult = "OK";
        }
        else if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
            newStatus = (eventStatusError != null) ? eventStatusError : eventStatusOk;
            eventResult = "ERROR";
        }
        else if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
            newStatus = (eventStatusError != null) ? eventStatusError : eventStatusOk;
            eventResult = "ERROR";
            errorDescription = notifyResponse;
        }
        else {
            newStatus = (eventStatusCancelled != null) ? eventStatusCancelled : eventStatusOk;
            eventResult = "KO";
            errorDescription = notifyResponse;
        }

        PostPaidTransactionEventBean postPaidTransactionEventBean = this.generateTransactionEvent(eventSequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

        em.persist(postPaidTransactionEventBean);

        oldStatus = newStatus;

        return sendMPTransactionResultMessageResponse.getStatusCode();
    }

    private PostPaidTransactionEventBean generateTransactionEvent(Integer eventSequenceID, String stateType, String requestID, Date eventTimestamp, String event,
            PostPaidTransactionBean poPTransactionBean, String result, String eventNewState, String eventOldState, String errorCode, String errorDescription) {

        PostPaidTransactionEventBean postPaidTransactionEventBean = new PostPaidTransactionEventBean();
        postPaidTransactionEventBean.setSequenceID(eventSequenceID);
        postPaidTransactionEventBean.setStateType(stateType);
        postPaidTransactionEventBean.setRequestID(requestID);
        postPaidTransactionEventBean.setEventTimestamp(eventTimestamp);
        postPaidTransactionEventBean.setEvent(event);
        postPaidTransactionEventBean.setTransactionBean(poPTransactionBean);
        postPaidTransactionEventBean.setResult(result);
        postPaidTransactionEventBean.setNewState(eventNewState);
        postPaidTransactionEventBean.setOldState(eventOldState);
        postPaidTransactionEventBean.setErrorCode(errorCode);
        postPaidTransactionEventBean.setErrorDescription(errorDescription);

        this.lastPostPaidTransactionEventBean = postPaidTransactionEventBean;

        return postPaidTransactionEventBean;
    }

    private PostPaidTransactionPaymentEventBean generateTransactionPaymentEvent(Integer eventSequenceID, PostPaidTransactionBean poPTransactionBean, GestPayData gestPayData,
            String event) {

        PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean = new PostPaidTransactionPaymentEventBean();
        postPaidTransactionPaymentEventBean.setSequence(eventSequenceID);
        postPaidTransactionPaymentEventBean.setErrorCode(gestPayData.getErrorCode());
        postPaidTransactionPaymentEventBean.setErrorDescription(gestPayData.getErrorDescription());
        postPaidTransactionPaymentEventBean.setTransactionBean(poPTransactionBean);
        postPaidTransactionPaymentEventBean.setTransactionResult(gestPayData.getTransactionResult());
        postPaidTransactionPaymentEventBean.setAuthorizationCode(gestPayData.getAuthorizationCode());
        postPaidTransactionPaymentEventBean.setEventType(event);

        return postPaidTransactionPaymentEventBean;
    }

    private PaymentTransactionResult generatePaymentTransactionResult(PostPaidTransactionBean poPTransactionBean, GestPayData gestPayData, Double amount, String event) {
        PaymentTransactionResult paymentTransactionResult = new PaymentTransactionResult();
        paymentTransactionResult.setAmount(amount);
        paymentTransactionResult.setAcquirerID(poPTransactionBean.getAcquirerID());
        paymentTransactionResult.setAuthorizationCode(gestPayData.getAuthorizationCode());
        paymentTransactionResult.setBankTransactionID(gestPayData.getBankTransactionID());
        paymentTransactionResult.setCurrency(poPTransactionBean.getCurrency());
        paymentTransactionResult.setErrorCode(gestPayData.getErrorCode());
        paymentTransactionResult.setErrorDescription(gestPayData.getErrorDescription());
        paymentTransactionResult.setEventType(event);
        paymentTransactionResult.setShopLogin(poPTransactionBean.getShopLogin());
        paymentTransactionResult.setPaymentMode(poPTransactionBean.getPaymentMode());
        paymentTransactionResult.setShopTransactionID(gestPayData.getShopTransactionID());
        paymentTransactionResult.setTransactionResult(gestPayData.getTransactionResult().equals("ERROR") ? "KO" : gestPayData.getTransactionResult());

        return paymentTransactionResult;
    }

    private void reverseConsumeVoucher(PostPaidTransactionBean poPTransactionBean, FidelityServiceRemote fidelityService) throws Exception {

        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_VOUCHER_REVERSE;
        String requestID = String.valueOf(new Date().getTime());
        System.out.println("Storno consume voucher");

        if (poPTransactionBean.getPostPaidConsumeVoucherBeanList().isEmpty()) {
            System.out.println("Storno consumo voucher non necessario");
            return;
        }

        ArrayList<PostPaidConsumeVoucherBean> reversedPostPaidConsumeVoucherBeanList = new ArrayList<PostPaidConsumeVoucherBean>(0);

        for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : poPTransactionBean.getPostPaidConsumeVoucherBeanList()) {
            // Bisogna effettuare lo storno solo se l'operazione di consumo voucher � andata a buon fine

            System.out.println("voucher operation type: " + postPaidConsumeVoucherBean.getOperationType() + " --- status code: " + postPaidConsumeVoucherBean.getStatusCode());

            if (postPaidConsumeVoucherBean == null || (!postPaidConsumeVoucherBean.getOperationType().equals("CONSUME")/*
                                                                                                                        * && !postPaidConsumeVoucherBean.getStatusCode().equals(
                                                                                                                        * FidelityResponse.CONSUME_VOUCHER_OK)
                                                                                                                        */)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", poPTransactionBean.getMpTransactionID(), null,
                        "Storno consumo non valido per questo voucher");
                continue;
            }

            // Chimamata al servizio reverseConsumeVoucherTransaction

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            String operationIDtoReverse = postPaidConsumeVoucherBean.getOperationID();
            PartnerType partnerType = PartnerType.MP;
            Long requestTimestamp = new Date().getTime();
            newStatus = StatusHelper.POST_PAID_STATUS_VOUCHER_REVERSE;

            try {
                ReverseConsumeVoucherTransactionResult reverseConsumeVoucherTransactionResult = fidelityService.reverseConsumeVoucherTransaction(operationID, operationIDtoReverse,
                        partnerType, requestTimestamp);

                if (!reverseConsumeVoucherTransactionResult.getStatusCode().equals(FidelityResponse.REVERSE_CONSUME_VOUCHER_TRANSACTION_OK)) {

                    errorCode = reverseConsumeVoucherTransactionResult.getStatusCode();
                    errorDescription = reverseConsumeVoucherTransactionResult.getMessageCode();
                    eventResult = "KO";

                    sequenceID += 1;
                    PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                            eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                    oldStatus = newStatus;

                    em.persist(postPaidTransactionEventBean);

                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", operationID, null, "Error in reverseConsumeVoucherTransaction: "
                            + reverseConsumeVoucherTransactionResult.getStatusCode());

                    throw new FidelityServiceException("Error in reverseConsumeVoucherTransaction: " + reverseConsumeVoucherTransactionResult.getStatusCode());
                }
                else {

                    // Aggiungi una riga per loggare l'esito dell'operazione di storno
                    PostPaidConsumeVoucherBean reversePostPaidConsumeVoucherBean = new PostPaidConsumeVoucherBean();
                    reversePostPaidConsumeVoucherBean.setCsTransactionID(reverseConsumeVoucherTransactionResult.getCsTransactionID());
                    reversePostPaidConsumeVoucherBean.setMessageCode(reverseConsumeVoucherTransactionResult.getMessageCode());
                    reversePostPaidConsumeVoucherBean.setOperationID(operationID);
                    reversePostPaidConsumeVoucherBean.setOperationIDReversed(operationIDtoReverse);
                    reversePostPaidConsumeVoucherBean.setOperationType("REVERSE");
                    reversePostPaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
                    reversePostPaidConsumeVoucherBean.setStatusCode(reverseConsumeVoucherTransactionResult.getStatusCode());
                    reversePostPaidConsumeVoucherBean.setTotalConsumed(0.0);
                    reversePostPaidConsumeVoucherBean.setPostPaidTransactionBean(poPTransactionBean);

                    reversedPostPaidConsumeVoucherBeanList.add(reversePostPaidConsumeVoucherBean);
                    //poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(reversePostPaidConsumeVoucherBean);

                    // Ripristina il valore iniziale dei voucher consumati

                    String operationID_check = new IdGenerator().generateId(16).substring(0, 33);
                    VoucherConsumerType voucherConsumerType_check = VoucherConsumerType.ENI;
                    PartnerType partnerType_check = PartnerType.MP;
                    Long requestTimestamp_check = new Date().getTime();

                    List<VoucherCodeDetail> voucherCodeList_check = new ArrayList<VoucherCodeDetail>(0);

                    for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {

                        VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                        voucherCodeDetail.setVoucherCode(postPaidConsumeVoucherDetailBean.getVoucherCode());
                        voucherCodeList_check.add(voucherCodeDetail);
                    }

                    CheckVoucherResult checkVoucherResult = new CheckVoucherResult();

                    try {

                        checkVoucherResult = fidelityService.checkVoucher(operationID_check, voucherConsumerType_check, partnerType_check, requestTimestamp_check,
                                voucherCodeList_check);
                        // Verifica l'esito del check
                        String checkVoucherStatusCode = checkVoucherResult.getStatusCode();

                        if (!checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_OK)) {
                            // Error
                            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", operationID, null, "Error checking voucher: "
                                    + checkVoucherStatusCode);
                            //throw new FidelityServiceException("Error checking voucher: " + checkVoucherStatusCode);
                        }
                        else {

                            // Aggiorna lo stato dei voucher dell'utente

                            // Ricerca il voucher nella risposta del servizio di verifica
                            for (VoucherDetail voucherDetail : checkVoucherResult.getVoucherList()) {

                                System.out.println("Elaborazione voucher: " + voucherDetail.getVoucherCode() + " in stato: " + voucherDetail.getVoucherStatus());

                                for (VoucherBean voucherBean : poPTransactionBean.getUserBean().getVoucherList()) {

                                    if (voucherBean.getCode().equals(voucherDetail.getVoucherCode())) {

                                        System.out.println("Aggiornamento Voucher " + voucherDetail.getVoucherCode());

                                        // Aggiorna il voucher con i dati restituiti dal servizio
                                        voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                                        voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                                        voucherBean.setInitialValue(voucherDetail.getInitialValue());
                                        voucherBean.setPromoCode(voucherDetail.getPromoCode());
                                        voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                                        voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                                        voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                                        voucherBean.setCode(voucherDetail.getVoucherCode());
                                        voucherBean.setStatus(voucherDetail.getVoucherStatus());
                                        voucherBean.setType(voucherDetail.getVoucherType());
                                        voucherBean.setValue(voucherDetail.getVoucherValue());

                                        System.out.println("Voucher aggiornato");
                                    }
                                }
                            }

                            // Aggiorna l'utente
                            em.merge(poPTransactionBean.getUserBean());

                            sequenceID += 1;
                            PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                                    eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                            oldStatus = newStatus;

                            em.persist(postPaidTransactionEventBean);
                        }
                    }
                    catch (Exception ex) {
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", operationID, oldStatus,
                                "Error in checkVoucherResult: " + ex.getMessage());
                        //throw new FidelityServiceException("Error in checkVoucherResult: " + e.getMessage());
                        errorCode = "9999";
                        errorDescription = ex.getMessage();
                        eventResult = "ERROR";

                        sequenceID += 1;
                        PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                                eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                        oldStatus = newStatus;

                        em.persist(postPaidTransactionEventBean);
                    }
                }
            }
            catch (FidelityServiceException ex) {
                PostPaidConsumeVoucherBean newPostPaidConsumeVoucherBean = new PostPaidConsumeVoucherBean();
                newPostPaidConsumeVoucherBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED/* PostPaidConsumeVoucherBean.STATUS_CONSUME_TO_VERIFY */);
                newPostPaidConsumeVoucherBean.setMessageCode("STORNO DA VERIFICARE (" + ex.getMessage() + ")");
                newPostPaidConsumeVoucherBean.setTotalConsumed(postPaidConsumeVoucherBean.getTotalConsumed());
                newPostPaidConsumeVoucherBean.setPostPaidTransactionBean(poPTransactionBean);
                newPostPaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
                newPostPaidConsumeVoucherBean.setOperationID(operationID);
                newPostPaidConsumeVoucherBean.setOperationIDReversed(operationIDtoReverse);
                newPostPaidConsumeVoucherBean.setTotalConsumed(0.0);
                newPostPaidConsumeVoucherBean.setOperationType(postPaidConsumeVoucherBean.getOperationType());

                reversedPostPaidConsumeVoucherBeanList.add(newPostPaidConsumeVoucherBean);
                //poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(newPostPaidConsumeVoucherBean);

                errorCode = "9999";
                errorDescription = ex.getMessage();
                eventResult = "ERROR";

                sequenceID += 1;
                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                oldStatus = newStatus;

                em.persist(postPaidTransactionEventBean);
            }
            catch (Exception ex) {
                errorCode = "9999";
                errorDescription = ex.getMessage();
                eventResult = "ERROR";

                sequenceID += 1;
                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                oldStatus = newStatus;

                em.persist(postPaidTransactionEventBean);

                throw ex;
            }
        }

        for (PostPaidConsumeVoucherBean revertedPostPaidConsumeVoucherBean : reversedPostPaidConsumeVoucherBeanList) {
            poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(revertedPostPaidConsumeVoucherBean);
        }

    }

    private void revertLoadLoyaltyCredits(PostPaidTransactionBean poPTransactionBean, FidelityServiceRemote fidelityService) throws FidelityServiceException {

        System.out.println("Storno load loyalty credits");

        ArrayList<PostPaidLoadLoyaltyCreditsBean> revertedPostPaidLoadLoyaltyCreditsBeanList = new ArrayList<PostPaidLoadLoyaltyCreditsBean>(0);
        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY_REVERSE;
        String requestID = String.valueOf(new Date().getTime());
        boolean reversed = false;

        for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {

            System.out.println("load loyalty credits status code: " + postPaidLoadLoyaltyCreditsBean.getStatusCode());
            // Bisogna effettuare lo storno solo se l'operazione di caricamento loyalty � andata a buon fine
            if (postPaidLoadLoyaltyCreditsBean == null || !postPaidLoadLoyaltyCreditsBean.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {

                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", poPTransactionBean.getMpTransactionID(), null,
                        "Storno caricamento punti carte loyalty non necessario");

                return;
            }

            // Chimamata al servizio reverseLoadLoyaltyCreditsTransaction

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            String operationIDtoReverse = postPaidLoadLoyaltyCreditsBean.getOperationID();
            PartnerType partnerType = PartnerType.MP;
            Long requestTimestamp = new Date().getTime();
            newStatus = StatusHelper.POST_PAID_STATUS_LOYALTY_REVERSE;

            ReverseLoadLoyaltyCreditsTransactionResult reverseLoadLoyaltyCreditsTransactionResult = fidelityService.reverseLoadLoyaltyCreditsTransaction(operationID,
                    operationIDtoReverse, partnerType, requestTimestamp);

            if (!reverseLoadLoyaltyCreditsTransactionResult.getStatusCode().equals(FidelityResponse.REVERSE_LOAD_LOYALTY_CREDITS_OK)) {

                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", operationID, null, "Error in reverseConsumeVoucherTransaction: "
                        + reverseLoadLoyaltyCreditsTransactionResult.getStatusCode());

                errorCode = reverseLoadLoyaltyCreditsTransactionResult.getStatusCode();
                errorDescription = reverseLoadLoyaltyCreditsTransactionResult.getMessageCode();
                eventResult = "KO";

                sequenceID += 1;
                PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID,
                        eventTimestamp, transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                oldStatus = newStatus;

                em.persist(postPaidTransactionEventBean);

                throw new FidelityServiceException("Error in reverseLoadLoyaltyCreditsTransaction: " + reverseLoadLoyaltyCreditsTransactionResult.getStatusCode());
            }
            else {

                // Aggiungi una riga per loggare l'esito dell'operazione di storno

                PostPaidLoadLoyaltyCreditsBean reversePostPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean();
                //reversePostPaidLoadLoyaltyCreditsBean.setBalance(reverseLoadLoyaltyCreditsTransactionResult.getBalance());
                //reversePostPaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                //reversePostPaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                //reversePostPaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                reversePostPaidLoadLoyaltyCreditsBean.setCsTransactionID(reverseLoadLoyaltyCreditsTransactionResult.getCsTransactionID());
                //reversePostPaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                //reversePostPaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                reversePostPaidLoadLoyaltyCreditsBean.setMessageCode(reverseLoadLoyaltyCreditsTransactionResult.getMessageCode());
                reversePostPaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                reversePostPaidLoadLoyaltyCreditsBean.setOperationIDReversed(operationIDtoReverse);
                reversePostPaidLoadLoyaltyCreditsBean.setOperationType("REVERSE");
                reversePostPaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                reversePostPaidLoadLoyaltyCreditsBean.setStatusCode(reverseLoadLoyaltyCreditsTransactionResult.getStatusCode());

                reversePostPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);

                revertedPostPaidLoadLoyaltyCreditsBeanList.add(reversePostPaidLoadLoyaltyCreditsBean);

                reversed = true;
            }
        }

        if (reversed) {
            sequenceID += 1;
            PostPaidTransactionEventBean postPaidTransactionEventBean = generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp,
                    transactionEvent, poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

            em.persist(postPaidTransactionEventBean);

        }
        else {
            System.out.println("Storno caricamento punti carte loyalty non necessario");
        }

        oldStatus = newStatus;

        for (PostPaidLoadLoyaltyCreditsBean revertedPostPaidLoadLoyaltyCreditsBean : revertedPostPaidLoadLoyaltyCreditsBeanList) {
            poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(revertedPostPaidLoadLoyaltyCreditsBean);
        }
    }

    private boolean isCreditsLoyaltyLoaded(PostPaidTransactionBean poPTransactionBean) {
        boolean loaded = false;

        for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {
            if (postPaidLoadLoyaltyCreditsBean.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)
                    && (postPaidLoadLoyaltyCreditsBean.getCredits() != null && postPaidLoadLoyaltyCreditsBean.getCredits() > 0)) {
                loaded = true;
                break;
            }
        }
        return loaded;
    }

    private void updateVoucherAndLoyaltyData(PostPaidTransactionBean poPTransactionBean) {
        boolean updated = false;

        if (poPTransactionBean.getPostPaidConsumeVoucherBeanList() != null && !poPTransactionBean.getPostPaidConsumeVoucherBeanList().isEmpty()) {
            for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : poPTransactionBean.getPostPaidConsumeVoucherBeanList()) {

                em.persist(postPaidConsumeVoucherBean);
                updated = true;
                if (postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean() != null && !postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean().isEmpty()) {
                    for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {
                        em.persist(postPaidConsumeVoucherDetailBean);
                        updated = true;
                    }
                }
            }
        }

        if (poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList() != null && !poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().isEmpty()) {
            for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList()) {
                em.persist(postPaidLoadLoyaltyCreditsBean);
                updated = true;
            }
        }

        if (updated) {
            em.merge(poPTransactionBean);
        }
    }

    private PostPaidApproveShopTransactionResponse doPaymentVoucherCredit(PostPaidTransactionBean poPTransactionBean, String requestID, Double amount,
            List<ProductDetail> totalProductList, UserBean userBean, FidelityServiceRemote fidelityService, String mpTransactionID,
            PostPaidTransactionEventBean postPaidTransactionEventBean, Long paymentMethodId, String paymentMethodType, Integer voucherPurchaseMinInterval,
            ForecourtPostPaidServiceRemote forecourtPPService, UserTransaction userTransaction, EmailSenderRemote emailSender, String proxyHost, String proxyPort,
            String proxyNoHosts, UserCategoryService userCategoryService, StringSubstitution stringSubstitution) throws Exception {

        String errorCode = null;
        String errorDescription = null;
        String eventResult = "OK";
        int totalTransactionTask = 1;
        Timestamp eventTimestamp = new Timestamp(new Date().getTime());
        String transactionEvent = null;
        String refuelMode = null;
        PostPaidApproveShopTransactionResponse poPApproveShopTransactionResponse = new PostPaidApproveShopTransactionResponse();
        ConsumeVoucherResult consumeVoucherResult = new ConsumeVoucherResult();

        poPTransactionBean.setUserBean(userBean);
        poPTransactionBean.setPaymentMethodId(paymentMethodId);
        poPTransactionBean.setPaymentMethodType(paymentMethodType);
        poPTransactionBean.setAcquirerID("QUENIT");
        poPTransactionBean.setShopLogin("ENIPAY" + poPTransactionBean.getStationBean().getStationID());
        em.merge(poPTransactionBean);
        
        if (poPTransactionBean.getRefuelBean().size() > 1) {
            totalTransactionTask = poPTransactionBean.getRefuelBean().size();
        }
        else {
            refuelMode = FidelityConstants.REFUEL_MODE_ENI_CAFE_ONLY;
        }

        PostPaidRefuelBean[] refuelBeanArray = new PostPaidRefuelBean[poPTransactionBean.getRefuelBean().size()];
        poPTransactionBean.getRefuelBean().toArray(refuelBeanArray);

        for (int index = 0; index < totalTransactionTask; index++) {

            refuelMode = null;
            List<ProductDetail> productList = new ArrayList<ProductDetail>(0);
            if (!poPTransactionBean.getRefuelBean().isEmpty()) {
                PostPaidRefuelBean refuelBean = refuelBeanArray[index];
                System.out.println("trovato refuel mode: " + refuelBean.getRefuelMode());

                if (refuelBean.getRefuelMode().equalsIgnoreCase("servito")) {
                    refuelMode = FidelityConstants.REFUEL_MODE_SERVITO;
                }

                if (refuelBean.getRefuelMode().equalsIgnoreCase("fai_da_te")) {
                    refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_POSTPAY;
                }

                if (refuelMode == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Tipo di rifornimento sconosciuto. Operazione scartata!");
                }

                ProductDetail productDetail = new ProductDetail();
                productDetail.setAmount(refuelBean.getFuelAmount());

                System.out.println("trovato product id: " + refuelBean.getProductId());

                if (refuelBean.getProductId().equals("SP")) {

                    // sp
                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_SP);
                }
                else {

                    if (refuelBean.getProductId().equals("GG")) {

                        // gasolio
                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GASOLIO);
                    }
                    else {

                        if (refuelBean.getProductId().equals("BS")) {

                            // blue_super
                            productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_SUPER);
                        }
                        else {

                            if (refuelBean.getProductId().equals("BD")) {

                                // blue_diesel
                                productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_BLUE_DIESEL);
                            }
                            else {

                                if (refuelBean.getProductId().equals("MT")) {

                                    // metano
                                    productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_METANO);
                                }
                                else {

                                    if (refuelBean.getProductId().equals("GP")) {

                                        // gpl
                                        productDetail.setProductCode(FidelityConstants.PRODUCT_CODE_GPL);
                                    }
                                    else {

                                        if (refuelBean.getProductId().equals("AD")) {

                                            // ???
                                            productDetail.setProductCode(null);
                                        }
                                        else {

                                            // non_oil
                                            productDetail.setProductCode(null);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                productDetail.setQuantity(refuelBean.getFuelQuantity());
                productList.add(productDetail);
                totalProductList.add(productDetail);
            }

            if (index == 0 && !poPTransactionBean.getCartBean().isEmpty()) {
                System.out.println("trovato shop");

                if (refuelMode == null) {
                    refuelMode = FidelityConstants.REFUEL_MODE_ENI_CAFE_ONLY;
                }

                boolean validProductFound = false;
                Double shopAmount = 0.0;
                Integer shopCount = 0;

                for (PostPaidCartBean postPaidCartBean : poPTransactionBean.getCartBean()) {

                    if (postPaidCartBean.getProductId().equals(FidelityConstants.PRODUCT_CODE_NON_OIL))     // PRODOTTI NON OIL 70
                    {
                        validProductFound = true;
                        shopAmount = shopAmount + postPaidCartBean.getAmount();
                        shopCount = shopCount + postPaidCartBean.getQuantity();

                        System.out.println("amount: " + postPaidCartBean.getAmount() + ", count: " + postPaidCartBean.getQuantity());
                    }
                }

                if (validProductFound) {
                    ProductDetail shopProductDetail = new ProductDetail();

                    shopProductDetail.setAmount(shopAmount);
                    shopProductDetail.setProductCode(FidelityConstants.PRODUCT_CODE_NON_OIL);
                    shopProductDetail.setQuantity(Double.valueOf(shopCount));

                    productList.add(shopProductDetail);
                    totalProductList.add(shopProductDetail);
                }
            }

            System.out.println("Controllo caricamento punti carta fedelt�");
            // Se l'utente ha una carta loyalty associata allora bisogna effettuare anche il caricamento dei punti
            LoyaltyCardBean loyaltyCardBean = userBean.getFirstLoyaltyCard();

            if (loyaltyCardBean != null && !productList.isEmpty()) {

                System.out.println("Trovata carta fedelt�");

                // Chimamata al servizio loadLoyaltyCredits

                String operationID = new IdGenerator().generateId(16).substring(0, 33);
                PartnerType partnerType = PartnerType.MP;
                Long requestTimestamp = new Date().getTime();
                String stationID = poPTransactionBean.getStationBean().getStationID();
                String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;

                String BIN = paymentInfoBean.getCardBin();

                if (BIN != null && QueryRepository.findCardBinExists(em, BIN)) {
                    System.out.println("BIN valido (" + BIN + ")");
                    paymentMode = FidelityConstants.PAYMENT_METHOD_ENI_CARD;
                }
                else {
                    System.out.println("BIN Non Valido (" + BIN + ")");
                }

                PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean = new PostPaidLoadLoyaltyCreditsBean();
                postPaidLoadLoyaltyCreditsBean.setOperationID(operationID);
                postPaidLoadLoyaltyCreditsBean.setOperationType("LOAD");
                postPaidLoadLoyaltyCreditsBean.setRequestTimestamp(requestTimestamp);
                postPaidLoadLoyaltyCreditsBean.setEanCode(loyaltyCardBean.getEanCode());

                try {

                    System.out.println("Chiamata servizio di carico punti");

                    LoadLoyaltyCreditsResult loadLoyaltyCreditsResult = fidelityService.loadLoyaltyCredits(operationID, mpTransactionID, stationID, loyaltyCardBean.getPanCode(),
                            BIN, refuelMode, paymentMode, "it", partnerType, requestTimestamp, "", productList);

                    System.out.println("Risposta servizio di carico punti: " + loadLoyaltyCreditsResult.getStatusCode() + " (" + loadLoyaltyCreditsResult.getMessageCode() + ")");

                    postPaidLoadLoyaltyCreditsBean.setBalance(loadLoyaltyCreditsResult.getBalance());
                    postPaidLoadLoyaltyCreditsBean.setBalanceAmount(loadLoyaltyCreditsResult.getBalanceAmount());
                    postPaidLoadLoyaltyCreditsBean.setCardClassification(loadLoyaltyCreditsResult.getCardClassification());
                    postPaidLoadLoyaltyCreditsBean.setCardCodeIssuer(loadLoyaltyCreditsResult.getCardCodeIssuer());
                    postPaidLoadLoyaltyCreditsBean.setCardStatus(loadLoyaltyCreditsResult.getCardStatus());
                    postPaidLoadLoyaltyCreditsBean.setCardType(loadLoyaltyCreditsResult.getCardType());
                    postPaidLoadLoyaltyCreditsBean.setCredits(loadLoyaltyCreditsResult.getCredits());
                    postPaidLoadLoyaltyCreditsBean.setCsTransactionID(loadLoyaltyCreditsResult.getCsTransactionID());
                    postPaidLoadLoyaltyCreditsBean.setEanCode(loadLoyaltyCreditsResult.getEanCode());
                    postPaidLoadLoyaltyCreditsBean.setMarketingMsg(loadLoyaltyCreditsResult.getMarketingMsg());
                    postPaidLoadLoyaltyCreditsBean.setMessageCode(loadLoyaltyCreditsResult.getMessageCode());
                    postPaidLoadLoyaltyCreditsBean.setStatusCode(loadLoyaltyCreditsResult.getStatusCode());
                    postPaidLoadLoyaltyCreditsBean.setWarningMsg(loadLoyaltyCreditsResult.getWarningMsg());

                    postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);

                    poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(postPaidLoadLoyaltyCreditsBean);

                    //em.persist(postPaidLoadLoyaltyCreditsBean);
                    //em.merge(poPTransactionBean);

                    if (!loadLoyaltyCreditsResult.getStatusCode().equals(FidelityResponse.LOAD_LOYALTY_CREDITS_OK)) {
                        errorCode = loadLoyaltyCreditsResult.getStatusCode();
                        errorDescription = loadLoyaltyCreditsResult.getMessageCode();
                        eventResult = "KO";
                    }
                    else {
                        errorCode = null;
                        errorDescription = null;
                        eventResult = "OK";
                    }

                }
                catch (Exception e) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Error loading loyalty credits");

                    postPaidLoadLoyaltyCreditsBean.setStatusCode(StatusHelper.LOYALTY_STATUS_TO_BE_VERIFIED);
                    postPaidLoadLoyaltyCreditsBean.setMessageCode("CARICAMENTO DA VERIFICARE (" + e.getMessage() + ")");
                    postPaidLoadLoyaltyCreditsBean.setPostPaidTransactionBean(poPTransactionBean);
                    postPaidLoadLoyaltyCreditsBean.setCredits(0);
                    postPaidLoadLoyaltyCreditsBean.setWarningMsg("Caricamento punti You&Eni non riuscito. "
                            + "Per questa volta rivolgiti al gestore per caricare i punti utilizzando la carta loyalty in tuo possesso.");
                    poPTransactionBean.getPostPaidLoadLoyaltyCreditsBeanList().add(postPaidLoadLoyaltyCreditsBean);

                    //poPTransactionBean.setLoyaltyReconcile(true);

                    errorCode = "9999";
                    errorDescription = "Error loading loyalty credits (" + e.getMessage() + ")";
                    eventResult = "ERROR";
                }

                oldStatus = newStatus;
                newStatus = StatusHelper.POST_PAID_STATUS_CREDITS_LOAD;
                sequenceID += 1;
                eventTimestamp = new Timestamp(new Date().getTime());
                transactionEvent = StatusHelper.POST_PAID_EVENT_BE_LOYALTY;

                postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                        poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                em.persist(postPaidTransactionEventBean);

            }

            System.out.println("Nessuna carta fedelt� associata al codice fiscale (" + userBean.getPersonalDataBean().getFiscalCode() + ")");

            // Se l'utente ha scelto di utilizzare i voucher bisogna effetutare la chiamata al servizio consumeVoucher del fidelityAdapter
            poPTransactionBean.setUseVoucher(true);

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            PartnerType partnerType = PartnerType.MP;
            VoucherConsumerType voucherType = VoucherConsumerType.ENI;
            long requestTimestamp = new Date().getTime();
            String paymentMode = FidelityConstants.PAYMENT_METHOD_OTHER;
            eventTimestamp = new Timestamp(new Date().getTime());
            transactionEvent = StatusHelper.POST_PAID_EVENT_BE_VOUCHER;
            oldStatus = newStatus;
            newStatus = StatusHelper.POST_PAID_STATUS_PAY_FULL_VOUCHER;

            String stationID = poPTransactionBean.getStationBean().getStationID();
            String language = FidelityConstants.LANGUAGE_ITALIAN;

            sequenceID += 1;
            Boolean loyaltyCredits = isCreditsLoyaltyLoaded(poPTransactionBean);

            //caricamento della ProductList dalla transazione; � un caricamento fittizio di una transazione refuel
            //refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_POSTPAY;

            if (refuelMode == null) {
                refuelMode = FidelityConstants.REFUEL_MODE_IPERSELF_POSTPAY;
            }

            Double totalConsumed = 0.0;
            PostPaidConsumeVoucherBean postPaidConsumeVoucherBean = new PostPaidConsumeVoucherBean();
            postPaidConsumeVoucherBean.setOperationID(operationID);
            postPaidConsumeVoucherBean.setRequestTimestamp(requestTimestamp);
            postPaidConsumeVoucherBean.setOperationType("CONSUME");
            postPaidConsumeVoucherBean.setPostPaidTransactionBean(poPTransactionBean);

            List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);

            for (VoucherBean voucherBean : userBean.getVoucherList()) {

                //System.out.println("Trovato voucher " + voucherBean.getCode() + " con stato " + voucherBean.getStatus());

                if (voucherBean.getStatus().equals(Voucher.VOUCHER_STATUS_VALIDO)) {

                    System.out.println("Voucher inserito in richiesta");

                    VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
                    voucherCodeDetail.setVoucherCode(voucherBean.getCode());
                    voucherCodeList.add(voucherCodeDetail);
                }
            }

            try {

                consumeVoucherResult = fidelityService.consumeVoucher(operationID, poPTransactionBean.getMpTransactionID(), voucherType, stationID, refuelMode, paymentMode,
                        language, partnerType, requestTimestamp, productList, voucherCodeList, FidelityConstants.CONSUME_TYPE_TOTAL, null);

                postPaidConsumeVoucherBean.setCsTransactionID(consumeVoucherResult.getCsTransactionID());
                postPaidConsumeVoucherBean.setMarketingMsg(consumeVoucherResult.getMarketingMsg());
                postPaidConsumeVoucherBean.setMessageCode(consumeVoucherResult.getMessageCode());
                postPaidConsumeVoucherBean.setStatusCode(consumeVoucherResult.getStatusCode());
                postPaidConsumeVoucherBean.setWarningMsg(consumeVoucherResult.getWarningMsg());
                em.persist(postPaidConsumeVoucherBean);

                for (VoucherDetail voucherDetail : consumeVoucherResult.getVoucherList()) {

                    if (voucherDetail.getConsumedValue() == 0.0) {

                        System.out.println(voucherDetail.getVoucherCode() + ": Voucher " + voucherDetail.getVoucherCode() + " valore consumato 0.0 -> aggiornamento non necessario");
                        continue;
                    }

                    PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean = new PostPaidConsumeVoucherDetailBean();

                    postPaidConsumeVoucherDetailBean.setConsumedValue(voucherDetail.getConsumedValue());
                    postPaidConsumeVoucherDetailBean.setExpirationDate(voucherDetail.getExpirationDate());
                    postPaidConsumeVoucherDetailBean.setInitialValue(voucherDetail.getInitialValue());
                    postPaidConsumeVoucherDetailBean.setPromoCode(voucherDetail.getPromoCode());
                    postPaidConsumeVoucherDetailBean.setPromoDescription(voucherDetail.getPromoDescription());
                    postPaidConsumeVoucherDetailBean.setPromoDoc(voucherDetail.getPromoDoc());
                    postPaidConsumeVoucherDetailBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                    postPaidConsumeVoucherDetailBean.setVoucherCode(voucherDetail.getVoucherCode());
                    postPaidConsumeVoucherDetailBean.setVoucherStatus(voucherDetail.getVoucherStatus());
                    postPaidConsumeVoucherDetailBean.setVoucherType(voucherDetail.getVoucherType());
                    postPaidConsumeVoucherDetailBean.setVoucherValue(voucherDetail.getVoucherValue());
                    postPaidConsumeVoucherDetailBean.setPostPaidConsumeVoucherBean(postPaidConsumeVoucherBean);
                    em.persist(postPaidConsumeVoucherDetailBean);

                    postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean().add(postPaidConsumeVoucherDetailBean);

                    totalConsumed = totalConsumed + voucherDetail.getConsumedValue();

                    // Aggiorna le informazioni sul voucher associato all'utente

                    System.out.println("Aggiornamento voucher utente");

                    for (VoucherBean voucherBean : userBean.getVoucherList()) {

                        if (voucherBean.getCode().equals(voucherDetail.getVoucherCode())) {

                            System.out.println("Aggiornamento voucher " + voucherDetail.getVoucherCode());

                            voucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                            voucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                            voucherBean.setInitialValue(voucherDetail.getInitialValue());
                            voucherBean.setPromoCode(voucherDetail.getPromoCode());
                            voucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                            voucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                            voucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                            voucherBean.setCode(voucherDetail.getVoucherCode());
                            voucherBean.setStatus(voucherDetail.getVoucherStatus());
                            voucherBean.setType(voucherDetail.getVoucherType());
                            voucherBean.setValue(voucherDetail.getVoucherValue());
                        }
                    }
                }

                // Aggiornamento dei dati utente
                em.merge(userBean);

                postPaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
                poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(postPaidConsumeVoucherBean);

                System.out.println("Totale pagato con voucher: " + totalConsumed);

                em.persist(postPaidConsumeVoucherBean);
                em.merge(poPTransactionBean);

                errorCode = consumeVoucherResult.getStatusCode();
                errorDescription = consumeVoucherResult.getMessageCode();

                if (consumeVoucherResult.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {
                    eventResult = "OK";
                }
                else {
                    eventResult = "KO";
                }

                postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                        poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                oldStatus = newStatus;
                em.persist(postPaidTransactionEventBean);
                /*
                if (consumeVoucherResult.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {
                    String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
                    Date timestamp = poPTransactionBean.getCreationTimestamp();
                    String pv = poPTransactionBean.getStationBean().getStationID();
                    String product = (crmPostPaidRefuelBean != null) ? crmPostPaidRefuelBean.getProductId() : poPTransactionBean.getProductID();
                    boolean flagPayment = true;
                    Integer credits = (crmPostPaidLoadLoyaltyCredits != null) ? crmPostPaidLoadLoyaltyCredits.getCredits() : 0;
                    Double refuelQuantity = (crmPostPaidRefuelBean != null) ? crmPostPaidRefuelBean.getFuelQuantity() : 0;
                    refuelMode = (crmPostPaidRefuelBean != null) ? crmPostPaidRefuelBean.getRefuelMode() : refuelMode;
                    boolean flagNotification = false;
                    boolean flagCreditCard = false;
                    String cardBrand = null;
                    ClusterType cluster = ClusterType.ENIPAY;
                    
                    if (userBean.getPaymentMethodTypeCreditCard() != null) {
                        flagCreditCard = true;
                        cardBrand = userBean.getPaymentMethodTypeCreditCard().getBrand();
                    }
                    
                    UserProfile userProfile = UserProfile.getUserProfileForEventRefueling(fiscalCode, timestamp, pv, product, flagPayment, credits, refuelQuantity, refuelMode, 
                            flagNotification, flagCreditCard, cardBrand, cluster);
                    
                    String crmResponse = crmService.sendNotifyEventOffer(fiscalCode, InteractionPointType.REFUELING, userProfile);

                    if (!crmResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Error in send crm notification (" + crmResponse + ")");
                    }
                }
                */
                if (!consumeVoucherResult.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_OK)) {

                    if (consumeVoucherResult.getStatusCode().equals(FidelityResponse.CONSUME_VOUCHER_PV_NOT_ENABLED)) {
                        poPTransactionBean.setNotificationUser(true);
                        poPTransactionBean.setToReconcile(false);

                        eventTimestamp = new java.sql.Timestamp(new Date().getTime());

                        System.out.println("Finalizzazione pagamento nuovo flusso voucher: PV non abilitato al servizio Eni Pay");

                        poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                        poPTransactionBean.setLastModifyTimestamp(eventTimestamp);

                        sequenceID += 1;
                        this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                                poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), null, loyaltyCredits, poPTransactionBean,
                                forecourtPPService, StatusHelper.POST_PAID_STATUS_PAY_VOUCHER_FAIL, null, null);

                        em.persist(poPTransactionBean);

                        userTransaction.commit();
                        poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_AUTH_KO);
                        poPApproveShopTransactionResponse.setMessageCode("Siamo spiacenti, il servizio Eni Pay non � attivo su questo impianto. (0323)");
                        poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                        return poPApproveShopTransactionResponse;

                    }

                    poPTransactionBean.setNotificationUser(true);
                    poPTransactionBean.setToReconcile(false);

                    eventTimestamp = new java.sql.Timestamp(new Date().getTime());

                    System.out.println("Finalizzazione pagamento nuovo flusso voucher");

                    poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                    poPTransactionBean.setLastModifyTimestamp(eventTimestamp);

                    sequenceID += 1;
                    this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                            poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), null, loyaltyCredits, poPTransactionBean, forecourtPPService,
                            StatusHelper.POST_PAID_STATUS_PAY_VOUCHER_FAIL, null, null);

                    em.persist(poPTransactionBean);

                    userTransaction.commit();
                    poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_VOUCHER_PAY_KO);
                    poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
                    return poPApproveShopTransactionResponse;

                }
            }
            catch (Exception e) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", "requestID", null, "Error consuming vouchers");

                postPaidConsumeVoucherBean.setStatusCode(StatusHelper.VOUCHER_STATUS_TO_BE_VERIFIED);
                postPaidConsumeVoucherBean.setMessageCode("CONSUMO DA VERIFICARE (" + e.getMessage() + ")");
                postPaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
                postPaidConsumeVoucherBean.setPostPaidTransactionBean(poPTransactionBean);

                em.persist(postPaidConsumeVoucherBean);

                eventTimestamp = new Timestamp(new Date().getTime());
                transactionEvent = StatusHelper.POST_PAID_EVENT_BE_VOUCHER;
                newStatus = StatusHelper.POST_PAID_STATUS_PAY_FULL_VOUCHER;
                errorCode = consumeVoucherResult.getStatusCode();
                errorDescription = consumeVoucherResult.getMessageCode();
                eventResult = "ERROR";

                postPaidTransactionEventBean = this.generateTransactionEvent(sequenceID, StatusHelper.POST_PAID_EVENT_TYPE_STANDARD, requestID, eventTimestamp, transactionEvent,
                        poPTransactionBean, eventResult, newStatus, oldStatus, errorCode, errorDescription);

                oldStatus = newStatus;
                em.persist(postPaidTransactionEventBean);

                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
                poPTransactionBean.setVoucherReconcile(true);
                poPTransactionBean.getPostPaidConsumeVoucherBeanList().add(postPaidConsumeVoucherBean);

                sequenceID += 1;
                this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                        poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), null, loyaltyCredits, poPTransactionBean, forecourtPPService,
                        StatusHelper.POST_PAID_STATUS_PAY_VOUCHER_FAIL, null, null);

                em.persist(poPTransactionBean);

                poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_FAILURE);
                poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

                userTransaction.commit();

                return poPApproveShopTransactionResponse;
            }
        }

        eventTimestamp = new java.sql.Timestamp(new Date().getTime());

        System.out.println("Finalizzazione pagamento nuovo flusso voucher");

        poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_PAID);
        poPTransactionBean.setLastModifyTimestamp(eventTimestamp);

        Boolean loyaltyCredits = isCreditsLoyaltyLoaded(poPTransactionBean);

        oldStatus = newStatus;
        sequenceID += 1;

        String notifyResponse = this.notifyResponse(requestID, sequenceID, poPTransactionBean.getStationBean().getStationID(), poPTransactionBean.getMpTransactionID(),
                poPTransactionBean.getSrcTransactionID(), poPTransactionBean.getMpTransactionStatus(), null, loyaltyCredits, poPTransactionBean, forecourtPPService,
                StatusHelper.POST_PAID_STATUS_PAY_COMPLETE, StatusHelper.POST_PAID_STATUS_NOTIFICATION_FAIL, StatusHelper.POST_PAID_STATUS_CANCELED);

        if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SUCCESS)) {
            System.out.println("Storno non necessario");

            poPTransactionBean.setNotificationPaid(true);
            poPTransactionBean.setNotificationUser(true);

            updateVoucherAndLoyaltyData(poPTransactionBean);

            //em.persist(poPTransactionBean);
            //Email.sendPostPaidSummary(emailSender, poPTransactionBean, null, proxyHost, proxyPort);

            //userTransaction.commit();
            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SUCCESS);
            poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

            //return poPApproveShopTransactionResponse;

        }

        if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_ERROR) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_RESPONSE_NOT_AVAILABLE)) {
            poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
            poPTransactionBean.setToReconcile(true);
            poPTransactionBean.setNotificationPaid(false);
            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
            poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());
        }

        if (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_TRANSACTION_CANCELLED) || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_TRANSACTION_NOT_RECOGNIZED)
                || notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_SOURCE_STATUS_NOT_AVAILABLE)
                || (notifyResponse.equals(StatusHelper.GFG_NOTIFICATION_MESSAGE_REJECTED) && poPTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode()))) {

            System.out.println("Storno voucher e loyalty necessario");
            poPTransactionBean.setNotificationPaid(true);

            try {
                // Storno caricamento punti carta loyalty e aggiornamento dati utante
                this.revertLoadLoyaltyCredits(poPTransactionBean, fidelityService);
            }
            catch (FidelityServiceException ex) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                        "Exception in revertLoadLoyaltyCredits: " + ex.getLocalizedMessage());
                poPTransactionBean.setLoyaltyReconcile(true);
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
            }

            try {
                // Storno movimentazione voucher e aggiornamento voucher utente
                this.reverseConsumeVoucher(poPTransactionBean, fidelityService);
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);

            }
            catch (Exception ex) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null,
                        "Exception in revertConsumeVoucher: " + ex.getLocalizedMessage());
                poPTransactionBean.setVoucherReconcile(true);
                poPTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_UNPAID);
            }

            updateVoucherAndLoyaltyData(poPTransactionBean);

            poPApproveShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_APPROVE_SETTLE_KO);
            poPApproveShopTransactionResponse.setPinCheckMaxAttempts(paymentInfoBean.getPinCheckAttemptsLeft());

        }

        em.persist(poPTransactionBean);

        Boolean isNewFlow = false;

        isNewFlow = userCategoryService.isUserTypeInUserCategory(poPTransactionBean.getUserBean().getUserType(), UserCategoryType.NEW_PAYMENT_FLOW.getCode());
        if (isNewFlow) {

            System.out.println("Invio mail ad utente nuovo flusso");
        }
        else {

            System.out.println("Invio mail ad utente vecchio flusso");
        }

        Email.sendPostPaidSummary(emailSender, poPTransactionBean, this.lastPostPaidTransactionEventBean, proxyHost, proxyPort, proxyNoHosts, 
                userCategoryService, stringSubstitution);
        
        userTransaction.commit();
        return poPApproveShopTransactionResponse;

    }
}
