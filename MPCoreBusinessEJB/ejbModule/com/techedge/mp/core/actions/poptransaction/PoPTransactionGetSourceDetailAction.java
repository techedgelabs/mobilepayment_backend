package com.techedge.mp.core.actions.poptransaction;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.CashInfo;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Station;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.postpaid.CoreProductDetail;
import com.techedge.mp.core.business.interfaces.postpaid.CoreProductIdEnum;
import com.techedge.mp.core.business.interfaces.postpaid.CorePumpDetail;
import com.techedge.mp.core.business.interfaces.postpaid.GetSourceDetailResponse;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidCartData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelData;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.CoordsHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StationHelper;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.ProductDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.PumpDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.SourceDetail;
import com.techedge.mp.forecourt.adapter.business.interfaces.StationDetail;
import com.techedge.mp.forecourt.integration.shop.business.ForecourtPostPaidServiceRemote;
import com.techedge.mp.forecourt.integration.shop.interfaces.GetLastRefuelMessageResponse;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class PoPTransactionGetSourceDetailAction {

    public static String  PREFIX_PUMP = "P";
    public static String  PREFIX_CASH = "C";

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public PoPTransactionGetSourceDetailAction() {}

    public GetSourceDetailResponse execute(String requestID, String ticketID, String codeType, String sourceID, String beaconCode, Double userPositionLatitude,
            Double userPositionLongitude, String currency, Integer reconciliationMaxAttemps, Double rangeThreshold, Boolean rangeThresholdBlocking, Integer transactionTimeout,
            ForecourtInfoServiceRemote forecourtInfoService, ForecourtPostPaidServiceRemote forecourtPostPaidServiceRemote, UserCategoryService userCategoryService)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            GetSourceDetailResponse getSourceDetailResponse = new GetSourceDetailResponse();
            getSourceDetailResponse.setOutOfRange(false);
            boolean checkDistance = true;

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);
            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                return createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "Invalid ticket", ResponseHelper.PP_GET_SOURCE_DETAIL_INVALID_TICKET);

            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // Ticket non valido
                return createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "User not found", ResponseHelper.PP_GET_SOURCE_DETAIL_INVALID_TICKET);
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                return createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "Unable to create refuel transaction in status " + String.valueOf(userStatus),
                        ResponseHelper.PP_GET_SOURCE_DETAIL_UNAUTHORIZED);
            }

            if (sourceID == null) {

                // Il campo sourceID � obbligatorio
                return createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "Missing parameter sourceID", ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
            }
            
            Boolean oldCategory = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.OLD_PAYMENT_FLOW.getCode());
            
            if (oldCategory) {
                // Controllo il metodo di pagamento non � specificato utilizza quello di default
                PaymentInfoBean paymentInfoBean = null;

                paymentInfoBean = userBean.findDefaultPaymentInfoBean();

                // Verifica che il metodo di pagamento selezionato sia in uno stato valido
                if (paymentInfoBean == null) {

                    // Il metodo di pagamento selezionato non esiste
                    return createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "Wrong payment data",
                            ResponseHelper.PP_GET_SOURCE_DETAIL_INVALID_PAYMENT_METHOD);
                }

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "paymentMethodId: " + paymentInfoBean.getId()
                        + " paymentMethodType: " + paymentInfoBean.getType());

                if (paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_BLOCKED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_ERROR
                        || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING) {

                    // Il metodo di pagamento selezionato non pu� essere utilizzato
                    return createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "Wrong payment data status: " + paymentInfoBean.getStatus(),
                            ResponseHelper.PP_GET_SOURCE_DETAIL_INVALID_PAYMENT_METHOD);
                }
            }
            else {
                Boolean pinFound = false;
                
                for (PaymentInfoBean paymentInfo : userBean.getPaymentData()) {
                    if (paymentInfo.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_VOUCHER) && paymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {
                        pinFound = true;
                        System.out.println("Pin found");
                    }
                }
                if (!pinFound) {
                    // Il metodo di pagamento selezionato non pu� essere utilizzato
                    return createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "Pin not found",
                            ResponseHelper.PP_GET_SOURCE_DETAIL_UNAUTHORIZED);
                }
            }

            if (userPositionLatitude == null || userPositionLongitude == null) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User position latitude or longitude is null");

                getSourceDetailResponse.setOutOfRange(true);

                if (rangeThresholdBlocking) {
                    userTransaction.commit();
                    getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_USER_NOT_IN_RANGE);
                    return getSourceDetailResponse;
                }
                else {
                    getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_SUCCESS);
                }

                checkDistance = false;
            }

            String getStationPumpRequestId = String.valueOf(new Date().getTime());

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invocazione servizio getStationDetails per sourceID: " + sourceID);

            GetStationDetailsResponse getStationDetailsResponse = forecourtInfoService.getStationDetails(getStationPumpRequestId, null, sourceID, true);

            if (getStationDetailsResponse == null || StationHelper.checkType(getStationDetailsResponse).equals("ERRORE")) {

                System.out.println("type: ERRORE");

                String prefix = sourceID.substring(0, 1);
                System.out.println("Prefix: " + prefix);

                if (getStationDetailsResponse.getStatusCode().equals("PUMP_STATUS_NOT_AVAILABLE_500")) {
                    return createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "stationDetail not available",
                            ResponseHelper.PP_GET_SOURCE_DETAIL_STATION_NOT_AVAILABLE);
                }

                return setMessageIfPumpOrCash(userTransaction, getSourceDetailResponse, prefix);

            }
            else {

                StationDetail stationDetail = getStationDetailsResponse.getStationDetail();

                if (stationDetail == null) {

                    return createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "stationDetail not active", ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
                }

                //quando il servizio getStationDetails restituisce uno stationDetail non null 
                //bisogna verificare che i dati dell'impianto memorizzati su db siano allineati a quelli restituiti 
                //dal servizio e, in caso ci fosse un disallineamento, bisogna aggiornare i dati sul db
                String stationDetailId = stationDetail.getStationID();
                StationBean stationBeanActive = QueryRepository.findActiveStationBeanById(em, stationDetailId);

                if (stationBeanActive == null) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Station not active");

                    //userTransaction.commit();

                    String prefix = sourceID.substring(0, 1);
                    System.out.println("Prefix: " + prefix);

                    return setMessageIfPumpOrCash(userTransaction, getSourceDetailResponse, prefix);
                }

                this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "execute", requestID, null, "Check if the stations are the same");
                checkValueAndUpdate(stationBeanActive, stationDetail);

                if (StationHelper.checkType(getStationDetailsResponse).equals("FUEL_PRE_PAY") || StationHelper.checkType(getStationDetailsResponse).equals("FUEL_POST_PAY")) {

                    String stationDetailLatitude = stationDetail.getLatitude();
                    String stationDetailLongitude = stationDetail.getLongitude();

                    if (stationDetailLatitude == null || stationDetailLongitude == null) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Station detail latitude or longitude is null");

                        getSourceDetailResponse.setOutOfRange(true);

                        if (rangeThresholdBlocking) {
                            userTransaction.commit();
                            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_USER_NOT_IN_RANGE);
                            return getSourceDetailResponse;
                        }
                        else {
                            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_SUCCESS);
                        }

                        checkDistance = false;
                    }

                    System.out.println("range threshold: " + rangeThreshold + " - range threshold blocking: " + rangeThresholdBlocking);
                    System.out.println("user latitude: " + userPositionLatitude + " - user longitude: " + userPositionLongitude);
                    System.out.println("station latitude: " + stationDetailLatitude + " - user longitude: " + stationDetailLongitude);

                    if (checkDistance) {

                        double distance = CoordsHelper.calculateDistance(userPositionLatitude, userPositionLongitude, new Double(stationDetailLatitude), new Double(
                                stationDetailLongitude));

                        if (distance > rangeThreshold.doubleValue()) {
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User position out of range: " + distance);

                            getSourceDetailResponse.setOutOfRange(true);

                            if (rangeThresholdBlocking) {
                                userTransaction.commit();
                                getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_USER_NOT_IN_RANGE);
                                return getSourceDetailResponse;
                            }
                            else {
                                getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_SUCCESS);
                            }

                        }

                        //System.out.println("station: " + stationDetailId + " - distance: " + distance);
                    }

                    if (getStationDetailsResponse.getStationDetail().getPumpDetails() != null && !getStationDetailsResponse.getStationDetail().getPumpDetails().isEmpty()) {

                        PumpDetail pumpInfo = getStationDetailsResponse.getStationDetail().getPumpDetails().get(0);

                        if (pumpInfo.getPumpStatus().equals("PUMP_NOT_AVAILABLE_501") || pumpInfo.getPumpStatus().equals("PUMP_STATUS_NOT_AVAILABLE_500")) {
                            
                            userTransaction.commit();
                            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_PUMP_NOT_AVAILABLE);
                            return getSourceDetailResponse;

                        }
                        
                        if (pumpInfo.getPumpStatus().equals("PUMP_BUSY_401")) {
                            
                            userTransaction.commit();
                            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_PUMP_BUSY);
                            return getSourceDetailResponse;

                        }
                    }
                }

                if (StationHelper.checkType(getStationDetailsResponse).equals("FUEL_PRE_PAY")) {

                    StationBean stationBean = QueryRepository.findStationBeanById(em, stationDetailId);

                    //CONTROLLO CHE LA STAZIONE SIA IN MODALIT� PREPAID
                    if (!stationBean.getPrepaidActive()) {
                        userTransaction.commit();
                        getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_NO_PRE_PAID);
                        return getSourceDetailResponse;
                    }

                    System.out.println("type: FUEL_PRE_PAY");

                    getSourceDetailResponse.setObjectType("FUEL");
                    getSourceDetailResponse.setObjectStatus("PRE-PAY");

                    PumpDetail pumpInfo = getStationDetailsResponse.getStationDetail().getPumpDetails().get(0);

                    //TODO gestire il caso in cui getPumpDetails non contenga elementi

                    CorePumpDetail corePumpDetail = fillCorePumpDetail(pumpInfo);

                    getSourceDetailResponse.setPumpInfo(corePumpDetail);
                    getSourceDetailResponse.setStationID(getStationDetailsResponse.getStationDetail().getStationID());
                    getSourceDetailResponse.setStationName(null);
                    getSourceDetailResponse.setStationAddress(getStationDetailsResponse.getStationDetail().getAddress());
                    getSourceDetailResponse.setStationCity(getStationDetailsResponse.getStationDetail().getCity());
                    getSourceDetailResponse.setStationProvince(getStationDetailsResponse.getStationDetail().getProvince());
                    getSourceDetailResponse.setStationCountry(getStationDetailsResponse.getStationDetail().getCountry());
                    getSourceDetailResponse.setStationLatitude(Double.valueOf(getStationDetailsResponse.getStationDetail().getLatitude()));
                    getSourceDetailResponse.setStationLongitude(Double.valueOf(getStationDetailsResponse.getStationDetail().getLongitude()));

                    userTransaction.commit();

                    getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_SUCCESS);
                    return getSourceDetailResponse;
                }
                else {

                    System.out.println("type: NO FUEL_PRE_PAY");

                    if (StationHelper.checkType(getStationDetailsResponse).equals("FUEL_POST_PAY")) {

                        StationBean stationBean = QueryRepository.findStationBeanById(em, stationDetailId);

                        //                      Controllo che la stazione non sia in modalit� postpaid.
                        //                      Se non lo � controllo se � attivo o no lo Shop e setto un messaggio di errore    
                        if (!stationBean.getPostpaidActive()) {
                            userTransaction.commit();
                            if (!stationBean.getShopActive()) {
                                getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_NO_POST_PAY_NO_SHOP_ACTIVE);
                                return getSourceDetailResponse;
                            }
                            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_NO_POST_PAY_SHOP_ACTIVE);
                            return getSourceDetailResponse;
                        }
                        System.out.println("type: FUEL_POST_PAY");

                        //TODO	occorre cercare la transazione se esiste; se non esiste occorre crearla;
                        getSourceDetailResponse.setObjectType("FUEL");
                        getSourceDetailResponse.setObjectStatus("POST-PAY");

                        PumpDetail pumpInfo = getStationDetailsResponse.getStationDetail().getPumpDetails().get(0);

                        //TODO gestire il caso in cui getPumpDetails non contenga elementi

                        CorePumpDetail corePumpDetail = fillCorePumpDetail(pumpInfo);

                        getSourceDetailResponse.setPumpInfo(corePumpDetail);

                        // Recupera l'informazione sulla transazione dal gestionale
                        GetLastRefuelMessageResponse getLastRefuelMessageResponse = forecourtPostPaidServiceRemote.getLastRefuel(requestID, sourceID);

                        if (!getLastRefuelMessageResponse.getStatusCode().endsWith("200")) {

                            // Nessuna transazione postpaid associata all'erogatore selezionato
                            GetSourceDetailResponse getSourceDetailResponseError = createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "No postPayed transaction for sourceID " + sourceID,
                                    ResponseHelper.PP_GET_SOURCE_DETAIL_NOT_RECOGNIZED);
                            
                            getSourceDetailResponseError.setObjectType("FUEL");
                            getSourceDetailResponseError.setObjectStatus("POST-PAY");
                            getSourceDetailResponseError.setPumpInfo(corePumpDetail);
                            
                            return getSourceDetailResponseError;
                        }
                        
                        // TODO se esiste gi� una transazione virtuale con gli stessi dati, restituisci quella
                        
                        List<PostPaidTransactionBean> postPaidTransactionBeanListTest = QueryRepository.findPostPaidTransactionBeanBySRCId(em,
                                getLastRefuelMessageResponse.getSrcTransactionID());
                        if ( postPaidTransactionBeanListTest != null && !postPaidTransactionBeanListTest.isEmpty() ) {
                            
                            System.out.println("Trovata una o pi� transazioni con stesso srcTransactinId");
                        }

                        List<PostPaidTransactionBean> postPaidTransactionBeanList = QueryRepository.findPostPaidTransactionBeanBySRCIdAndSource(em,
                                getLastRefuelMessageResponse.getSrcTransactionID(), getLastRefuelMessageResponse.getPumpID());

                        PostPaidTransactionBean postPaidTransactionBean = null;
                        if (postPaidTransactionBeanList != null && postPaidTransactionBeanList.size() > 0) {
                            for (PostPaidTransactionBean postPaidTransactionBeanTemp : postPaidTransactionBeanList) {
                                //1) se ci sono N transazioni in stato ONHOLD  la transazione viene settata come CANCELLED e restituita una delle transazioni;
                                //2) se la transazione � in stato ONHOLD  --> la transazione viene restituita; (UTENZA in questa fase non � associata)
                                //3) se la transazione non esiste viene creata;
                                if (postPaidTransactionBean == null && postPaidTransactionBeanTemp.getMpTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)) {
                                    postPaidTransactionBean = postPaidTransactionBeanTemp;
                                    System.out.println("POP Refuel srcTransactionID: " + getLastRefuelMessageResponse.getSrcTransactionID() + " transaction exists");
                                    continue;
                                }

                                if (postPaidTransactionBeanTemp.getMpTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)) { //1)
                                    postPaidTransactionBeanTemp.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_CANCELLED);
                                    em.persist(postPaidTransactionBeanTemp);
                                    System.out.println("POP Refuel srcTransactionID: " + getLastRefuelMessageResponse.getSrcTransactionID() + " transaction exists(more than one)");
                                    System.out.println("POP Refuel srcTransactionID: " + getLastRefuelMessageResponse.getSrcTransactionID() + " transaction exists. MPtransaction "
                                            + postPaidTransactionBeanTemp.getMpTransactionID() + "sets to CANCELLED");
                                }
                            }

                            //crea la transazione virtuale se le precedenti non sono corrette
                            if (postPaidTransactionBean == null) {
                                postPaidTransactionBean = this.createTransaction(requestID, currency, reconciliationMaxAttemps, getLastRefuelMessageResponse);
                                System.out.println("POP Refuel: " + getLastRefuelMessageResponse.getSrcTransactionID() + " transaction doesn't exist 1");
                            }
                        }
                        else {
                            //crea la transazione virtuale se non esiste
                            postPaidTransactionBean = this.createTransaction(requestID, currency, reconciliationMaxAttemps, getLastRefuelMessageResponse);
                            System.out.println("POP Refuel: " + getLastRefuelMessageResponse.getSrcTransactionID() + " transaction doesn't exist 2");
                        }

                        getSourceDetailResponse.setTransactionID(postPaidTransactionBean.getMpTransactionID());
                        getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_SUCCESS);

                        //userTransaction.commit();

                        getSourceDetailResponse = this.getPostPaidTransaction(postPaidTransactionBean.getMpTransactionID(), getStationDetailsResponse, getSourceDetailResponse,
                                userTransaction);
                        return getSourceDetailResponse;

                    }
                    else {

                        if (StationHelper.checkType(getStationDetailsResponse).equals("SHOP")) {

                            /*
                             * TODO Attivare il controllo dopo che sar� stata rilasciata la modifica al servizio getStationDetails
                             * StationBean stationBean = QueryRepository.findStationBeanById(em, stationDetail.getStationID());
                             * 
                             * // SHOP NON ATTIVA MESSAGGIO DI ERRORE
                             * if(!stationBean.getShopActive()){
                             * userTransaction.commit();
                             * getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_NO_SHOP_ACTIVE);
                             * return getSourceDetailResponse;
                             * }
                             */

                            System.out.println("type: SHOP");

                            //Si recupera la transazione shop pi� recente e si restituisce solo se in stato ONHOLD e se � stata confermata sul gestionale

                            getSourceDetailResponse.setObjectType("SHOP");
                            getSourceDetailResponse.setObjectStatus("POST-PAY");
                            
                            SourceDetail sourceDetail = getStationDetailsResponse.getStationDetail().getSourceDetails().get(0);

                            PostPaidTransactionBean postPaidTransactionBean = QueryRepository.findPostPaidLastTransactionBeanByQRId(em, sourceID);

                            if (postPaidTransactionBean == null) {

                                System.out.println("postPaidTransactionBean null");
                            }

                            if (postPaidTransactionBean != null && postPaidTransactionBean.getMpTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD)) {
                                Calendar calendarDeadTime = Calendar.getInstance();
                                Date now = Calendar.getInstance().getTime();
                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

                                long transactionDeadTime = (postPaidTransactionBean.getCreationTimestamp().getTime() + transactionTimeout.longValue());
                                calendarDeadTime.setTimeInMillis(transactionDeadTime);
                                System.out.println("TimeAlive transazione: " + transactionTimeout.longValue());
                                System.out.println("Data/ora creazione transazione: " + sdf.format(postPaidTransactionBean.getCreationTimestamp()) + " ("
                                        + postPaidTransactionBean.getCreationTimestamp().getTime() + ")");
                                System.out.println("Data/ora presunta scadenza transazione: " + sdf.format(calendarDeadTime.getTime()) + " (" + transactionDeadTime + ")");
                                System.out.println("Data/ora controllo : " + sdf.format(now) + " (" + now.getTime() + ")");

                                if (transactionDeadTime < now.getTime()) {
                                    postPaidTransactionBean = null;
                                }

                            }

                            if (postPaidTransactionBean != null
                                    && (!postPaidTransactionBean.getMpTransactionStatus().equals(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD) || postPaidTransactionBean.getNotificationCreated() == false)) {

                                System.out.println("popTransaction: " + postPaidTransactionBean.getMpTransactionID() + " pop transaction not valid");

                                postPaidTransactionBean = null;
                            }

                            if (postPaidTransactionBean == null) {

                                // Nessuna transazione shop associata alla cassa selezionata
                                GetSourceDetailResponse getSourceDetailResponseError = createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "No shop transaction for sourceID " + sourceID,
                                        ResponseHelper.PP_GET_SOURCE_DETAIL_NO_SHOP_TRANSACTION);
                                
                                if ( sourceDetail != null ) {
                                    CashInfo cashInfo = fillCoreShopDetail(sourceDetail);
                                    getSourceDetailResponseError.setCashInfo(cashInfo);
                                }
                                
                                getSourceDetailResponseError.setObjectType("SHOP");
                                getSourceDetailResponseError.setObjectStatus("POST-PAY");
                                
                                return getSourceDetailResponseError;
                            }
                            else {

                                System.out.println("MpTransactionStatus: " + postPaidTransactionBean.getMpTransactionStatus());
                                System.out.println("NotificationCreated: " + postPaidTransactionBean.getNotificationCreated());

                                if (postPaidTransactionBean.getStationBean() != null
                                        && postPaidTransactionBean.getStationBean().getStationStatus() == Station.STATION_STATUS_NOT_ACTIVE) {

                                    return createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "Station not active",
                                            ResponseHelper.PP_GET_SOURCE_DETAIL_CASH_NOT_FOUND);
                                }

                                String stationDetailLatitude = postPaidTransactionBean.getStationBean().getLatitude().toString();
                                String stationDetailLongitude = postPaidTransactionBean.getStationBean().getLongitude().toString();

                                if (stationDetailLatitude == null || stationDetailLongitude == null) {
                                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                                            "Station detail latitude or longitude is null");

                                    getSourceDetailResponse.setOutOfRange(true);

                                    if (rangeThresholdBlocking) {
                                        userTransaction.commit();
                                        getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_USER_NOT_IN_RANGE);
                                        return getSourceDetailResponse;
                                    }
                                    else {
                                        getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_SUCCESS);
                                    }

                                    checkDistance = false;
                                    //return getSourceDetailResponse;
                                }

                                System.out.println("range threshold: " + rangeThreshold + " - range threshold blocking: " + rangeThresholdBlocking);
                                System.out.println("user latitude: " + userPositionLatitude + " - user longitude: " + userPositionLongitude);
                                System.out.println("station latitude: " + stationDetailLatitude + " - user longitude: " + stationDetailLongitude);

                                if (checkDistance) {

                                    double distance = CoordsHelper.calculateDistance(userPositionLatitude, userPositionLongitude, new Double(stationDetailLatitude), new Double(
                                            stationDetailLongitude));

                                    if (distance > rangeThreshold.doubleValue()) {
                                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User position out of range: "
                                                + distance);

                                        getSourceDetailResponse.setOutOfRange(true);

                                        if (rangeThresholdBlocking) {
                                            userTransaction.commit();
                                            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_GETSTATIONS_USER_NOT_IN_RANGE);
                                            return getSourceDetailResponse;
                                        }
                                        else {
                                            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_SUCCESS);
                                        }

                                        //return getSourceDetailResponse;
                                    }

                                    //System.out.println("station: " + postPaidTransactionBean.getStationBean().getStationID() + " - distance: " + distance);
                                }

                                //compilare l'output SHOP
                                //userTransaction.commit();

                                getSourceDetailResponse = this.getPostPaidTransaction(postPaidTransactionBean.getMpTransactionID(), getStationDetailsResponse,
                                        getSourceDetailResponse, userTransaction);
                                return getSourceDetailResponse;
                            }
                        }
                        else {

                            // Modalit� operativa non riconosciuta
                            return createMessageError(requestID, userTransaction, ErrorLevel.INFO, "execute", "Modalit� operativa non riconosciuta",
                                    ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);
                        }
                    }
                }
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED Post Payed get source datail with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }

    }

    private GetSourceDetailResponse setMessageIfPumpOrCash(UserTransaction userTransaction, GetSourceDetailResponse getSourceDetailResponse, String prefix)
            throws RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException {

        if (prefix.equals(PREFIX_PUMP)) {

            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_PUMP_NOT_FOUND);
        }
        else {

            if (prefix.equals(PREFIX_CASH)) {

                getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_CASH_NOT_FOUND);
            }
            else {

                getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_FAILURE);

            }
        }

        userTransaction.commit();

        return getSourceDetailResponse;
    }

    private void checkValueAndUpdate(StationBean stationBeanSaved, StationDetail stationDetail) {

        if (isToUpdate(stationBeanSaved.getAddress(), stationDetail.getAddress())) {

            stationBeanSaved.setAddress(stationDetail.getAddress());
        }

        if (isToUpdate(stationBeanSaved.getCity(), stationDetail.getCity())) {

            stationBeanSaved.setCity(stationDetail.getCity());
        }

        if (isToUpdate(stationBeanSaved.getProvince(), stationDetail.getProvince())) {

            stationBeanSaved.setProvince(stationDetail.getProvince());
        }

        if (isToUpdate(stationBeanSaved.getCountry(), stationDetail.getCountry())) {

            stationBeanSaved.setCountry(stationDetail.getCountry());
        }

        if (isToUpdate(String.valueOf(stationBeanSaved.getLatitude()), stationDetail.getLatitude())) {

            stationBeanSaved.setLatitude(Double.parseDouble(stationDetail.getLatitude()));
        }

        if (isToUpdate(String.valueOf(stationBeanSaved.getLongitude()), stationDetail.getLongitude())) {

            stationBeanSaved.setLongitude(Double.parseDouble(stationDetail.getLongitude()));
        }

        em.merge(stationBeanSaved);
    }

    private boolean isToUpdate(String checked, String toUpdated) {

        if (!checked.equalsIgnoreCase(toUpdated)) {
            return false;
        }

        return true;
    }

    private CorePumpDetail fillCorePumpDetail(PumpDetail pumpInfo) {

        CorePumpDetail corePumpDetail = new CorePumpDetail();

        corePumpDetail.setPumpID(pumpInfo.getPumpID());
        corePumpDetail.setPumpNumber(pumpInfo.getPumpNumber());
        corePumpDetail.setPumpStatus(pumpInfo.getPumpStatus());
        corePumpDetail.setRefuelMode(pumpInfo.getRefuelMode());

        CoreProductDetail coreProductDetail = new CoreProductDetail();
        for (ProductDetail productInfo : pumpInfo.getProductDetails()) {
            coreProductDetail = new CoreProductDetail();
            coreProductDetail.setFuelType(productInfo.getFuelType());
            coreProductDetail.setProductID(CoreProductIdEnum.valueOf(productInfo.getProductID()));
            coreProductDetail.setProductDescription(productInfo.getProductDescription());
            coreProductDetail.setProductPrice(productInfo.getProductPrice());
            corePumpDetail.getProductDetails().add(coreProductDetail);
        }
        return corePumpDetail;
    }
    
    private CashInfo fillCoreShopDetail(SourceDetail sourceDetail) {

        CashInfo cashInfo = new CashInfo();

        cashInfo.setCashId(sourceDetail.getSourceID());
        cashInfo.setNumber(sourceDetail.getSourceNumber());
        
        return cashInfo;
    }

    private PostPaidTransactionBean createTransaction(String requestID, String currency, Integer reconciliationMaxAttempts,
            GetLastRefuelMessageResponse getLastRefuelMessageResponse) {

        //crea transazione virtuale
        StationBean stationBean = QueryRepository.findStationBeanById(em, getLastRefuelMessageResponse.getStationDetail().getStationID());

        if (stationBean == null) {
            //TODO DA GESTIRE
            // Lo stationID inserito non corrisponde a nessuna stazione valida

            //			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "No station for stationID " + stationID );
            //			userTransaction.commit();
            //			poPCreateShopTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
            //			return poPCreateShopTransactionResponse;
        }

        //TODO verificare che non ci sia una transazione aperta\attiva;

        String shopLogin = stationBean.getOilShopLogin();
        String acquirerID = stationBean.getOilAcquirerID();

        String mpTransactionID = new IdGenerator().generateId(16).substring(0, 32);

        Date now = new Date();
        Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());

        // Crea la transazione con i dati ricevuti in input
        PostPaidTransactionBean postPaidTransactionBean = new PostPaidTransactionBean();
        postPaidTransactionBean.setStationBean(stationBean);
        postPaidTransactionBean.setRequestID(requestID);
        postPaidTransactionBean.setSource(PostPaidSourceType.SELF.getCode());
        postPaidTransactionBean.setCreationTimestamp(creationTimestamp);
        postPaidTransactionBean.setSrcTransactionID(getLastRefuelMessageResponse.getSrcTransactionID());
        postPaidTransactionBean.setSourceID(getLastRefuelMessageResponse.getPumpID());
        postPaidTransactionBean.setMpTransactionID(mpTransactionID);
        postPaidTransactionBean.setStationBean(stationBean);
        postPaidTransactionBean.setShopLogin(shopLogin);
        postPaidTransactionBean.setAcquirerID(acquirerID);
        postPaidTransactionBean.setProductType("OIL");
        postPaidTransactionBean.setAmount(getLastRefuelMessageResponse.getRefuelDetail().getAmount());
        postPaidTransactionBean.setCurrency(currency);
        postPaidTransactionBean.setMpTransactionStatus(StatusHelper.POST_PAID_FINAL_STATUS_ONHOLD);
        postPaidTransactionBean.setNotificationCreated(true);
        postPaidTransactionBean.setNotificationPaid(false);
        postPaidTransactionBean.setNotificationUser(false);
        postPaidTransactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);

        em.persist(postPaidTransactionBean);

        if (getLastRefuelMessageResponse.getRefuelDetail() != null) {

            PostPaidRefuelBean postPaidRefuelBean = new PostPaidRefuelBean();
            postPaidRefuelBean.setFuelType(getLastRefuelMessageResponse.getRefuelDetail().getFuelType());
            postPaidRefuelBean.setFuelQuantity(getLastRefuelMessageResponse.getRefuelDetail().getFuelQuantity());
            postPaidRefuelBean.setFuelAmount(getLastRefuelMessageResponse.getRefuelDetail().getAmount());
            postPaidRefuelBean.setProductDescription(getLastRefuelMessageResponse.getRefuelDetail().getProductDescription());
            postPaidRefuelBean.setProductId(getLastRefuelMessageResponse.getRefuelDetail().getProductID());
            postPaidRefuelBean.setPumpId(getLastRefuelMessageResponse.getPumpID());
            postPaidRefuelBean.setPumpNumber(Integer.valueOf(getLastRefuelMessageResponse.getPumpNumber()));
            postPaidRefuelBean.setRefuelMode(getLastRefuelMessageResponse.getRefuelMode());

            Timestamp endrefueltimeTimestamp = Timestamp.valueOf(getLastRefuelMessageResponse.getRefuelDetail().getTimestampEndRefuel());
            endrefueltimeTimestamp.getTime();
            Date endrefuel = new Date(endrefueltimeTimestamp.getTime());
            postPaidRefuelBean.setTimestampEndRefuel(endrefuel);
            postPaidRefuelBean.setTransactionBean(postPaidTransactionBean);

            postPaidTransactionBean.setSourceNumber(getLastRefuelMessageResponse.getPumpNumber());

            postPaidTransactionBean.getRefuelBean().add(postPaidRefuelBean);

            em.persist(postPaidRefuelBean);
        }

        return postPaidTransactionBean;
    }

    private GetSourceDetailResponse getPostPaidTransaction(String mpTransactionID, GetStationDetailsResponse getStationDetailsResponse,
            GetSourceDetailResponse getSourceDetailResponse, UserTransaction userTransaction) throws NotSupportedException, SystemException, SecurityException,
            IllegalStateException, RollbackException, HeuristicMixedException, HeuristicRollbackException {

        //userTransaction.begin();

        PostPaidTransactionBean postPaidTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em, mpTransactionID);
        if (postPaidTransactionBean == null) {

            userTransaction.commit();

            getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_NOT_RECOGNIZED);
            return getSourceDetailResponse;
        }

        //TODO occorre inserire la verifica se la transazione � gi� abilitata. Cosa Fare?

        //IF (OK)
        getSourceDetailResponse.setTransactionData(postPaidTransactionBean.toPoPTransactionData());
        getSourceDetailResponse.setStatusCode(ResponseHelper.PP_GET_SOURCE_DETAIL_SUCCESS);
        getSourceDetailResponse.setTransactionID(postPaidTransactionBean.getMpTransactionID());
        getSourceDetailResponse.setTransactionStatus(postPaidTransactionBean.getMpTransactionStatus());
        getSourceDetailResponse.setStationID(postPaidTransactionBean.getStationBean().getStationID());
        getSourceDetailResponse.setStationName(null);
        getSourceDetailResponse.setStationAddress(postPaidTransactionBean.getStationBean().getAddress());
        getSourceDetailResponse.setStationCity(postPaidTransactionBean.getStationBean().getCity());
        getSourceDetailResponse.setStationProvince(postPaidTransactionBean.getStationBean().getProvince());
        getSourceDetailResponse.setStationCountry(postPaidTransactionBean.getStationBean().getCountry());
        getSourceDetailResponse.setStationLatitude(postPaidTransactionBean.getStationBean().getLatitude());
        getSourceDetailResponse.setStationLongitude(postPaidTransactionBean.getStationBean().getLongitude());
        getSourceDetailResponse.setAmount(postPaidTransactionBean.getAmount());

        PostPaidCartData postPaidCartData = new PostPaidCartData();

        if (postPaidTransactionBean.getCartBean() != null) {

            for (PostPaidCartBean postPaidCartBean : postPaidTransactionBean.getCartBean()) {

                postPaidCartData = new PostPaidCartData();
                postPaidCartData.setAmount(postPaidCartBean.getAmount());
                postPaidCartData.setProductDescription(postPaidCartBean.getProductDescription());
                postPaidCartData.setProductId(postPaidCartBean.getProductId());
                postPaidCartData.setQuantity(postPaidCartBean.getQuantity());
                getSourceDetailResponse.getPostPaidCartDataList().add(postPaidCartData);
            }
        }

        PostPaidRefuelData postPaidRefuelData = new PostPaidRefuelData();

        if (postPaidTransactionBean.getRefuelBean() != null) {

            for (PostPaidRefuelBean postPaidRefuelBean : postPaidTransactionBean.getRefuelBean()) {

                postPaidRefuelData = new PostPaidRefuelData();
                postPaidRefuelData.setFuelAmount(postPaidRefuelBean.getFuelAmount());
                postPaidRefuelData.setFuelQuantity(postPaidRefuelBean.getFuelQuantity());
                postPaidRefuelData.setFuelType(postPaidRefuelBean.getFuelType());
                postPaidRefuelData.setProductDescription(postPaidRefuelBean.getProductDescription());
                postPaidRefuelData.setProductId(postPaidRefuelBean.getProductId());
                postPaidRefuelData.setPumpId(postPaidRefuelBean.getPumpId());

                getSourceDetailResponse.getPostPaidRefuelDataList().add(postPaidRefuelData);

                if (postPaidTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {

                    CorePumpDetail corePumpDetail = new CorePumpDetail();
                    corePumpDetail.setPumpID(postPaidRefuelBean.getPumpId());

                    // TODO rimuovere se non pi� necessario
                    if (postPaidRefuelBean.getPumpNumber() != null) {
                        corePumpDetail.setPumpNumber(postPaidRefuelBean.getPumpNumber().toString());
                    }
                    else {
                        corePumpDetail.setPumpNumber(null);
                    }
                    corePumpDetail.setPumpStatus(null);
                    CoreProductDetail coreProductDetail;
                    coreProductDetail = new CoreProductDetail();
                    coreProductDetail.setFuelType(postPaidRefuelBean.getFuelType());
                    corePumpDetail.getProductDetails().add(coreProductDetail);
                }
            }
        }

        if (postPaidTransactionBean.getSource().equals("CASH REGISTER")) {

            CashInfo cashInfo = new CashInfo();

            cashInfo.setCashId(postPaidTransactionBean.getSourceID());

            if (postPaidTransactionBean.getSourceNumber() != null) {
                cashInfo.setNumber(postPaidTransactionBean.getSourceNumber());
            }

            getSourceDetailResponse.setCashInfo(cashInfo);
        }

        userTransaction.commit();

        return getSourceDetailResponse;
    }

    private GetSourceDetailResponse createMessageError(String requestID, UserTransaction userTransaction, ErrorLevel errorLevel, String methodName, String message,
            String statusCode) throws RollbackException, HeuristicMixedException, HeuristicRollbackException, SystemException {

        this.loggerService.log(errorLevel, this.getClass().getSimpleName(), methodName, requestID, null, message);

        userTransaction.commit();

        GetSourceDetailResponse getSourceDetailResponse = new GetSourceDetailResponse();
        getSourceDetailResponse.setStatusCode(statusCode);

        return getSourceDetailResponse;
    }
}
