package com.techedge.mp.core.actions.scheduler;

import java.awt.Color;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Sheet;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelModeType;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelSheetData;
import com.techedge.mp.core.business.utilities.QueryRepositoryBusiness;
import com.techedge.mp.core.business.utilities.TransactionFinalStatusConverter;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobDailyStatisticsReportBusinessAction {
    private static final String JOB_NAME = "JOB_DAILY_STATISTICS_REPORT_BUSINESS";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    //Cell style for header row
    //private ExcelCellStyle      csHeader;
    private ExcelCellStyle      csHeaderBordered;
    //Cell style for table row
    private ExcelCellStyle      csTableValue;
    private ExcelCellStyle      csTableValueBold;
    private ExcelCellStyle      csTableValueBorderedLeft;
    private ExcelCellStyle      csTableValueBorderedRight;
    private ExcelCellStyle      csTableLabelBoldBorderedLeft;
    private ExcelCellStyle      csTableLabelIndentBorderedLeft;
    private ExcelCellStyle      csTableValueBorderedBottom;
    private ExcelCellStyle      csTableValueBorderedBottomRight;
    private ExcelCellStyle      csTableLabelBoldBorderedBottomLeft;
    private ExcelCellStyle      csTableLabelIndentBorderedBottomLeft;

    public SchedulerJobDailyStatisticsReportBusinessAction() {}

    public String execute(EmailSenderRemote emailSender, String reportRecipient, String reportLogoUrl, String proxyHost, String proxyPort, String proxyNoHosts) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        Date now = new Date();

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("#,##0.000");
        
        NumberFormat integerFormat = NumberFormat.getNumberInstance(Locale.ITALIAN);
        integerFormat.setRoundingMode(RoundingMode.HALF_UP);
        integerFormat.setMinimumFractionDigits(0);
        integerFormat.setMaximumFractionDigits(0);

        System.out.println("job name: " + SchedulerJobDailyStatisticsReportBusinessAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {

            userTransaction.begin();

            SimpleDateFormat sdfFull = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");

            Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
            System.out.println("DATA ATTUALE:" + sdfFull.format(calendar.getTime()));
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date dailyEndDate = calendar.getTime();
            Date weeklyEndDate = dailyEndDate;

            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date dailyStartDate = calendar.getTime();

            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date weeklyStartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 20);
            calendar.set(Calendar.MONTH, Calendar.JUNE);
            calendar.set(Calendar.YEAR, 2018);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date totalStartDate = calendar.getTime();

            System.out.println("DATA GIORNALIERA INIZIO:" + sdfFull.format(dailyStartDate));
            System.out.println("DATA GIORNALIERA FINE:" + sdfFull.format(dailyEndDate));
            System.out.println("DATA SETTIMANALE INIZIO:" + sdfFull.format(weeklyStartDate));
            System.out.println("DATA SETTIMANALE FINE:" + sdfFull.format(weeklyEndDate));
            System.out.println("DATA TOTALI INIZIO:" + sdfFull.format(totalStartDate));

            List<Object> resultUsersCreditCard = QueryRepositoryBusiness.statisticsReportSynthesisUsersCreditCardBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, totalStartDate);

            resultUsersCreditCard.set(0, integerFormat.format(resultUsersCreditCard.get(0)));
            resultUsersCreditCard.set(1, integerFormat.format(resultUsersCreditCard.get(1)));
            resultUsersCreditCard.set(2, integerFormat.format(resultUsersCreditCard.get(2)));

            /*
             * Numero di transazioni (comprese quelle archiviate) Pre-Paid
             */
            
            
            System.out.println("Numero di transazioni (comprese quelle archiviate) Pre-Paid");
            
            List<Object> resultPrePaidTransactions = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, 
                    totalStartDate);
            
            List<Object> resultPrePaidTransactionsHistory = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate);

            List<Object> resultPrePaidTransactionsCurrentAndHistory = new ArrayList<Object>();

            resultPrePaidTransactionsCurrentAndHistory.add(
                    Integer.parseInt(resultPrePaidTransactions.get(0).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistory.get(0).toString()));
            
            resultPrePaidTransactionsCurrentAndHistory.add(
                    Integer.parseInt(resultPrePaidTransactions.get(1).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistory.get(1).toString()));

            resultPrePaidTransactionsCurrentAndHistory.add(
                    Integer.parseInt(resultPrePaidTransactions.get(2).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistory.get(2).toString()));
            
            
            /*
             * Litri erogati (comprese quelle archiviate) Pre-Paid
             */
            
            
            System.out.println("Litri erogati (comprese quelle archiviate) Pre-Paid");
            
            List<Object> resultPrePaidTransactionsFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsFuelQuantityBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, 
                    totalStartDate);
            
            List<Object> resultPrePaidTransactionsHistoryFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsHistoryFuelQuantityBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate);

            List<Object> resultPrePaidTransactionsCurrentAndHistoryFuelQuantity = new ArrayList<Object>();

            resultPrePaidTransactionsCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPrePaidTransactionsFuelQuantity.get(0).toString()) 
                    + Double.parseDouble(resultPrePaidTransactionsHistoryFuelQuantity.get(0).toString()));
            
            resultPrePaidTransactionsCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPrePaidTransactionsFuelQuantity.get(1).toString()) 
                    + Double.parseDouble(resultPrePaidTransactionsHistoryFuelQuantity.get(1).toString()));

            resultPrePaidTransactionsCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPrePaidTransactionsFuelQuantity.get(2).toString()) 
                    + Double.parseDouble(resultPrePaidTransactionsHistoryFuelQuantity.get(2).toString()));

            
            /*
             * Litri Diesel+ erogati (comprese quelle archiviate) Pre-Paid
             */
            String productDieselPiu = "Diesel+";
            
            System.out.println("Litri Diesel+ erogati (comprese quelle archiviate) Pre-Paid");
            
            List<Object> resultPrePaidTransactionsFuelQuantityDieselPlus = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsFuelQuantityByProductBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, 
                    totalStartDate, productDieselPiu);
            
            List<Object> resultPrePaidTransactionsHistoryFuelQuantityDieselPlus = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsHistoryFuelQuantityByProductBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, productDieselPiu);

            List<Object> resultPrePaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu = new ArrayList<Object>();
            
            resultPrePaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.add(
                    Double.parseDouble(resultPrePaidTransactionsFuelQuantityDieselPlus.get(0).toString()) 
                    + Double.parseDouble(resultPrePaidTransactionsHistoryFuelQuantityDieselPlus.get(0).toString()));
            
            resultPrePaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.add(
                    Double.parseDouble(resultPrePaidTransactionsFuelQuantityDieselPlus.get(1).toString()) 
                    + Double.parseDouble(resultPrePaidTransactionsHistoryFuelQuantityDieselPlus.get(1).toString()));

            resultPrePaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.add(
                    Double.parseDouble(resultPrePaidTransactionsFuelQuantityDieselPlus.get(2).toString()) 
                    + Double.parseDouble(resultPrePaidTransactionsHistoryFuelQuantityDieselPlus.get(2).toString()));

            
            /*
             *  Numero di transazioni (comprese quelle archiviate) Post-Paid Self
             */

            System.out.println("Numero di transazioni (comprese quelle archiviate) Post-Paid Self");
            
            List<Object> resultPostPaidTransactionsSelfServito = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());

            List<Object> resultPostPaidTransactionsSelf = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), null);
            
            List<Object> resultPostPaidTransactionsHistorySelfServito = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());

            List<Object> resultPostPaidTransactionsHistorySelf = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), null);
            
            List<Object> resultPostPaidTransactionsSelfServitoCurrentAndHistory = new ArrayList<Object>();

            resultPostPaidTransactionsSelfServitoCurrentAndHistory.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfServito.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfServito.get(0).toString()));
            
            resultPostPaidTransactionsSelfServitoCurrentAndHistory.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfServito.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfServito.get(1).toString()));

            resultPostPaidTransactionsSelfServitoCurrentAndHistory.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfServito.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfServito.get(2).toString()));
            

            List<Object> resultPostPaidTransactionsSelfCurrentAndHistory = new ArrayList<Object>();

            resultPostPaidTransactionsSelfCurrentAndHistory.add(
                    Integer.parseInt(resultPostPaidTransactionsSelf.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelf.get(0).toString()));
            
            resultPostPaidTransactionsSelfCurrentAndHistory.add(
                    Integer.parseInt(resultPostPaidTransactionsSelf.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelf.get(1).toString()));

            resultPostPaidTransactionsSelfCurrentAndHistory.add(
                    Integer.parseInt(resultPostPaidTransactionsSelf.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelf.get(2).toString()));
            

            
            /*
             *  Litri erogati (comprese quelle archiviate) Post-Paid Self
             */

            System.out.println("Litri erogati (comprese quelle archiviate) Post-Paid Self");
            
            List<Object> resultPostPaidTransactionsSelfServitoFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsFuelQuantityBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());

            List<Object> resultPostPaidTransactionsSelfFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsFuelQuantityBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), null);
            
            List<Object> resultPostPaidTransactionsHistorySelfServitoFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryFuelQuantityBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());

            List<Object> resultPostPaidTransactionsHistorySelfFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryFuelQuantityBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), null);
            
            List<Object> resultPostPaidTransactionsSelfServitoCurrentAndHistoryFuelQuantity = new ArrayList<Object>();

            resultPostPaidTransactionsSelfServitoCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPostPaidTransactionsSelfServitoFuelQuantity.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistorySelfServitoFuelQuantity.get(0).toString()));
            
            resultPostPaidTransactionsSelfServitoCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPostPaidTransactionsSelfServitoFuelQuantity.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistorySelfServitoFuelQuantity.get(1).toString()));

            resultPostPaidTransactionsSelfServitoCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPostPaidTransactionsSelfServitoFuelQuantity.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistorySelfServitoFuelQuantity.get(2).toString()));
            

            List<Object> resultPostPaidTransactionsSelfCurrentAndHistoryFuelQuantity = new ArrayList<Object>();

            resultPostPaidTransactionsSelfCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPostPaidTransactionsSelfFuelQuantity.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistorySelfFuelQuantity.get(0).toString()));
            
            resultPostPaidTransactionsSelfCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPostPaidTransactionsSelfFuelQuantity.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistorySelfFuelQuantity.get(1).toString()));

            resultPostPaidTransactionsSelfCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPostPaidTransactionsSelfFuelQuantity.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistorySelfFuelQuantity.get(2).toString()));
            
            
            /*
             *  Litri Diesel+ erogati (comprese quelle archiviate) Post-Paid Self
             */

            System.out.println("Litri Diesel+ erogati (comprese quelle archiviate) Post-Paid Self");
            
            List<Object> resultPostPaidTransactionsFuelQuantityDieselPiu = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsFuelQuantityByProductBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, productDieselPiu);

            List<Object> resultPostPaidTransactionsHistoryFuelQuantityDieselPiu = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryFuelQuantityByProductBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, productDieselPiu);
            
            List<Object> resultPostPaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu = new ArrayList<Object>();
            
            resultPostPaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.add(
                    Double.parseDouble(resultPostPaidTransactionsFuelQuantityDieselPiu.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistoryFuelQuantityDieselPiu.get(0).toString()));
            
            resultPostPaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.add(
                    Double.parseDouble(resultPostPaidTransactionsFuelQuantityDieselPiu.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistoryFuelQuantityDieselPiu.get(1).toString()));

            resultPostPaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.add(
                    Double.parseDouble(resultPostPaidTransactionsFuelQuantityDieselPiu.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistoryFuelQuantityDieselPiu.get(2).toString()));
            
            
            /*
             *  Numero di transazioni (comprese quelle archiviate) Post-Paid Shop
             */
            
            
            System.out.println("Numero di transazioni (comprese quelle archiviate) Post-Paid Shop");
            
            List<Object> resultPostPaidTransactionsShop = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            List<Object> resultPostPaidTransactionsHistoryShop = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            List<Object> resultPostPaidTransactionsShopCurrentAndHistory = new ArrayList<Object>();
            /*
            resultPostPaidTransactionsShopCurrentAndHistory.add(integerFormat.format(
                    Double.parseDouble(resultPostPaidTransactionsShop.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistoryShop.get(0).toString())));
            
            resultPostPaidTransactionsShopCurrentAndHistory.add(integerFormat.format(
                    Double.parseDouble(resultPostPaidTransactionsShop.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistoryShop.get(1).toString())));

            resultPostPaidTransactionsShopCurrentAndHistory.add(integerFormat.format(
                    Double.parseDouble(resultPostPaidTransactionsShop.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistoryShop.get(2).toString())));
            */
            resultPostPaidTransactionsShopCurrentAndHistory.add(
                    Integer.parseInt(resultPostPaidTransactionsShop.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShop.get(0).toString()));
            
            resultPostPaidTransactionsShopCurrentAndHistory.add(
                    Integer.parseInt(resultPostPaidTransactionsShop.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShop.get(1).toString()));

            resultPostPaidTransactionsShopCurrentAndHistory.add(
                    Integer.parseInt(resultPostPaidTransactionsShop.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShop.get(2).toString()));
            
            
            /*
             *  Litri erogati (comprese quelle archiviate) Post-Paid Shop
             */
            
            
            System.out.println("Litri erogati (comprese quelle archiviate) Post-Paid Shop");
            
            List<Object> resultPostPaidTransactionsShopFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsFuelQuantityBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            List<Object> resultPostPaidTransactionsHistoryShopFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryFuelQuantityBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            List<Object> resultPostPaidTransactionsShopCurrentAndHistoryFuelQuantity = new ArrayList<Object>();

            resultPostPaidTransactionsShopCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPostPaidTransactionsShopFuelQuantity.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistoryShopFuelQuantity.get(0).toString()));
            
            resultPostPaidTransactionsShopCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPostPaidTransactionsShopFuelQuantity.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistoryShopFuelQuantity.get(1).toString()));

            resultPostPaidTransactionsShopCurrentAndHistoryFuelQuantity.add(
                    Double.parseDouble(resultPostPaidTransactionsShopFuelQuantity.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsHistoryShopFuelQuantity.get(2).toString()));
            

            /*
             * Ammontare transazioni (comprese quelle archiviate) Pre-Paid
             */
            System.out.println("Ammontare transazioni (comprese quelle archiviate) Pre-Paid");
            
            List<Object> resultPrePaidTotalAmount = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTotalAmountBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, 
                    totalStartDate);
            
            List<Object> resultPrePaidTotalAmountHistory = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTotalAmountHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate);
            
            /*
             * Ammontare transazioni (comprese quelle archiviate) Post-Paid
             */
            System.out.println("Ammontare transazioni (comprese quelle archiviate) Post-Paid");
            
            List<Object> resultPostPaidTotalAmount = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTotalAmountBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, 
                    totalStartDate);
            
            List<Object> resultPostPaidTotalAmountHistory = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTotalAmountHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate);

            
            /*
             * Ammontare transazioni (comprese quelle archiviate) Pre-Paid e Post-Paid
             */
            System.out.println("Ammontare transazioni (comprese quelle archiviate) Pre-Paid e Post-Paid");
            
            List<Object> resultTotalAmount = new ArrayList<Object>();
            
            resultTotalAmount.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTotalAmount.get(0).toString())
                    + Double.parseDouble(resultPrePaidTotalAmountHistory.get(0).toString())
                    + Double.parseDouble(resultPostPaidTotalAmount.get(0).toString())
                    + Double.parseDouble(resultPostPaidTotalAmountHistory.get(0).toString())));
            
            resultTotalAmount.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTotalAmount.get(1).toString())
                    + Double.parseDouble(resultPrePaidTotalAmountHistory.get(1).toString())
                    + Double.parseDouble(resultPostPaidTotalAmount.get(1).toString())
                    + Double.parseDouble(resultPostPaidTotalAmountHistory.get(1).toString())));
            
            resultTotalAmount.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTotalAmount.get(2).toString())
                    + Double.parseDouble(resultPrePaidTotalAmountHistory.get(2).toString())
                    + Double.parseDouble(resultPostPaidTotalAmount.get(2).toString())
                    + Double.parseDouble(resultPostPaidTotalAmountHistory.get(2).toString())));

            /* Estrazione lista transazioni disattivata
            List<TransactionBean> resultPrePaidTransactionBeanListByDate = QueryRepositoryBusiness.statisticsReportDetailPrePaidTransactionsBusiness(em, dailyStartDate, dailyEndDate);
            
            List<TransactionHistoryBean> resultPrePaidTransactionHistoryBeanListByDate = QueryRepositoryBusiness.statisticsReportDetailPrePaidTransactionsHistoryBusiness(em, 
                    dailyStartDate, dailyEndDate);
            
            List<TransactionBean> resultWeeklyPrePaidTransactionBeanList = QueryRepositoryBusiness.statisticsReportDetailPrePaidTransactionsBusiness(em, weeklyStartDate, weeklyEndDate);
            
            List<TransactionHistoryBean> resultWeeklyPrePaidTransactionHistoryBeanList = QueryRepositoryBusiness.statisticsReportDetailPrePaidTransactionsHistoryBusiness(em, weeklyStartDate, weeklyEndDate);
            
            List<PostPaidTransactionBean> resultPostPaidTransactionBeanListByDate = QueryRepositoryBusiness.statisticsReportDetailPostPaidTransactionsBusiness(em, dailyStartDate, dailyEndDate);
            
            List<PostPaidTransactionHistoryBean> resultPostPaidTransactionHistoryBeanListByDate = QueryRepositoryBusiness.statisticsReportDetailPostPaidTransactionsHistoryBusiness(em, 
                    dailyStartDate, dailyEndDate);
            
            List<PostPaidTransactionBean> resultWeeklyPostPaidTransactionBeanList = QueryRepositoryBusiness.statisticsReportDetailPostPaidTransactionsBusiness(em, weeklyStartDate, weeklyEndDate);
            
            List<PostPaidTransactionHistoryBean> resultWeeklyPostPaidTransactionHistoryBeanList = QueryRepositoryBusiness.statisticsReportDetailPostPaidTransactionsHistoryBusiness(em, weeklyStartDate, weeklyEndDate);
            */
            
            /*
             * Utenti distinti
             */
            System.out.println("Utenti distinti");
            
            List<Object> resultDistinctUsersTransaction = QueryRepositoryBusiness.statisticsReportSynthesisDistinctUsersTransactionBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate);
            
            resultDistinctUsersTransaction.set(0, integerFormat.format(resultDistinctUsersTransaction.get(0)));
            resultDistinctUsersTransaction.set(1, integerFormat.format(resultDistinctUsersTransaction.get(1)));
            resultDistinctUsersTransaction.set(2, integerFormat.format(resultDistinctUsersTransaction.get(2)));
            
            Object resultStationsAppCount = QueryRepositoryBusiness.statisticsReportSynthesisStationsAppCountBusiness(em);
            
            List<Object> resultTransactionsTotalCount = new ArrayList<Object>();
            resultTransactionsTotalCount.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistory.get(0).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistory.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistory.get(0).toString())));
            
            resultTransactionsTotalCount.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistory.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistory.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistory.get(1).toString())));
            
            resultTransactionsTotalCount.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistory.get(2).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistory.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistory.get(2).toString())));
            
            
            /*
             * Totale litri erogati
             */
            System.out.println("Totale litri erogati");
            
            List<Object> resultTransactionsTotalFuelQuantity = new ArrayList<Object>();
            resultTransactionsTotalFuelQuantity.add(numberFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryFuelQuantity.get(0).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryFuelQuantity.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryFuelQuantity.get(0).toString())));
            
            resultTransactionsTotalFuelQuantity.add(numberFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryFuelQuantity.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryFuelQuantity.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryFuelQuantity.get(1).toString())));
            
            resultTransactionsTotalFuelQuantity.add(numberFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryFuelQuantity.get(2).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryFuelQuantity.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryFuelQuantity.get(2).toString())));

            
            /*
             * Totale litri Diesel+
             */
            System.out.println("Totale litri Diesel+");
            
            List<Object> resultTransactionsTotalFuelQuantityDieselPiu = new ArrayList<Object>();
            resultTransactionsTotalFuelQuantityDieselPiu.add(numberFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu .get(0).toString())
                    + Double.parseDouble(resultPostPaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.get(0).toString())));
            
            resultTransactionsTotalFuelQuantityDieselPiu.add(numberFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.get(1).toString())));
            
            resultTransactionsTotalFuelQuantityDieselPiu.add(numberFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.get(2).toString())
                    + Double.parseDouble(resultPostPaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.get(2).toString())));
            
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area NO
             */
            List<Object> resultPrePaidTransactionsAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, 
                    totalStartDate, "Area NO");
            
            List<Object> resultPrePaidTransactionsHistoryAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, "Area NO");
            
            List<Object> resultPostPaidTransactionsSelfAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NO");
            
            List<Object> resultPostPaidTransactionsHistorySelfAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NO");
            
            List<Object> resultPostPaidTransactionsShopAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NO");

            List<Object> resultPostPaidTransactionsHistoryShopAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NO");


            List<Object> resultPrePaidTransactionsCurrentAndHistoryAreaNO = new ArrayList<Object>();
            resultPrePaidTransactionsCurrentAndHistoryAreaNO.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaNO.get(0).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaNO.get(0).toString()));
            
            resultPrePaidTransactionsCurrentAndHistoryAreaNO.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaNO.get(1).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaNO.get(1).toString()));

            resultPrePaidTransactionsCurrentAndHistoryAreaNO.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaNO.get(2).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaNO.get(2).toString()));
            

            List<Object> resultPostPaidTransactionsSelfCurrentAndHistoryAreaNO = new ArrayList<Object>();
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaNO.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaNO.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaNO.get(0).toString()));
            
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaNO.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaNO.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaNO.get(1).toString()));

            resultPostPaidTransactionsSelfCurrentAndHistoryAreaNO.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaNO.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaNO.get(2).toString()));
           
            
            List<Object> resultPostPaidTransactionsShopCurrentAndHistoryAreaNO = new ArrayList<Object>();
             resultPostPaidTransactionsShopCurrentAndHistoryAreaNO.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaNO.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaNO.get(0).toString()));
            
            resultPostPaidTransactionsShopCurrentAndHistoryAreaNO.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaNO.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaNO.get(1).toString()));

            resultPostPaidTransactionsShopCurrentAndHistoryAreaNO.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaNO.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaNO.get(2).toString()));
             
            
            List<Object> resultTransactionsTotalCountAreaNO = new ArrayList<Object>();
            resultTransactionsTotalCountAreaNO.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaNO.get(0).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaNO.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaNO.get(0).toString())));
            
            resultTransactionsTotalCountAreaNO.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaNO.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaNO.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaNO.get(1).toString())));
            
            resultTransactionsTotalCountAreaNO.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaNO.get(2).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaNO.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaNO.get(2).toString())));
            
            
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area NE
             */
            List<Object> resultPrePaidTransactionsAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, 
                    totalStartDate, "Area NE");
            
            List<Object> resultPrePaidTransactionsHistoryAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, "Area NE");
            
            List<Object> resultPostPaidTransactionsSelfAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NE");
            
            List<Object> resultPostPaidTransactionsHistorySelfAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NE");
            
            List<Object> resultPostPaidTransactionsShopAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NE");

            List<Object> resultPostPaidTransactionsHistoryShopAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NE");


            List<Object> resultPrePaidTransactionsCurrentAndHistoryAreaNE = new ArrayList<Object>();
            resultPrePaidTransactionsCurrentAndHistoryAreaNE.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaNE.get(0).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaNE.get(0).toString()));
            
            resultPrePaidTransactionsCurrentAndHistoryAreaNE.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaNE.get(1).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaNE.get(1).toString()));

            resultPrePaidTransactionsCurrentAndHistoryAreaNE.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaNE.get(2).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaNE.get(2).toString()));
            

            List<Object> resultPostPaidTransactionsSelfCurrentAndHistoryAreaNE = new ArrayList<Object>();
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaNE.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaNE.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaNE.get(0).toString()));
            
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaNE.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaNE.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaNE.get(1).toString()));

            resultPostPaidTransactionsSelfCurrentAndHistoryAreaNE.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaNE.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaNE.get(2).toString()));
           
            
            List<Object> resultPostPaidTransactionsShopCurrentAndHistoryAreaNE = new ArrayList<Object>();
             resultPostPaidTransactionsShopCurrentAndHistoryAreaNE.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaNE.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaNE.get(0).toString()));
            
            resultPostPaidTransactionsShopCurrentAndHistoryAreaNE.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaNE.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaNE.get(1).toString()));

            resultPostPaidTransactionsShopCurrentAndHistoryAreaNE.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaNE.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaNE.get(2).toString()));
             
            
            List<Object> resultTransactionsTotalCountAreaNE = new ArrayList<Object>();
            resultTransactionsTotalCountAreaNE.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaNE.get(0).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaNE.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaNE.get(0).toString())));
            
            resultTransactionsTotalCountAreaNE.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaNE.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaNE.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaNE.get(1).toString())));
            
            resultTransactionsTotalCountAreaNE.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaNE.get(2).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaNE.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaNE.get(2).toString())));
            
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area CN
             */
            List<Object> resultPrePaidTransactionsAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, 
                    totalStartDate, "Area CN");
            
            List<Object> resultPrePaidTransactionsHistoryAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, "Area CN");
            
            List<Object> resultPostPaidTransactionsSelfAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CN");
            
            List<Object> resultPostPaidTransactionsHistorySelfAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CN");
            
            List<Object> resultPostPaidTransactionsShopAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CN");

            List<Object> resultPostPaidTransactionsHistoryShopAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CN");


            List<Object> resultPrePaidTransactionsCurrentAndHistoryAreaCN = new ArrayList<Object>();
            resultPrePaidTransactionsCurrentAndHistoryAreaCN.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaCN.get(0).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaCN.get(0).toString()));
            
            resultPrePaidTransactionsCurrentAndHistoryAreaCN.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaCN.get(1).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaCN.get(1).toString()));

            resultPrePaidTransactionsCurrentAndHistoryAreaCN.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaCN.get(2).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaCN.get(2).toString()));
            

            List<Object> resultPostPaidTransactionsSelfCurrentAndHistoryAreaCN = new ArrayList<Object>();
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaCN.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaCN.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaCN.get(0).toString()));
            
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaCN.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaCN.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaCN.get(1).toString()));

            resultPostPaidTransactionsSelfCurrentAndHistoryAreaCN.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaCN.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaCN.get(2).toString()));
           
            
            List<Object> resultPostPaidTransactionsShopCurrentAndHistoryAreaCN = new ArrayList<Object>();
             resultPostPaidTransactionsShopCurrentAndHistoryAreaCN.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaCN.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaCN.get(0).toString()));
            
            resultPostPaidTransactionsShopCurrentAndHistoryAreaCN.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaCN.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaCN.get(1).toString()));

            resultPostPaidTransactionsShopCurrentAndHistoryAreaCN.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaCN.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaCN.get(2).toString()));
             
            
            List<Object> resultTransactionsTotalCountAreaCN = new ArrayList<Object>();
            resultTransactionsTotalCountAreaCN.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaCN.get(0).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaCN.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaCN.get(0).toString())));
            
            resultTransactionsTotalCountAreaCN.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaCN.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaCN.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaCN.get(1).toString())));
            
            resultTransactionsTotalCountAreaCN.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaCN.get(2).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaCN.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaCN.get(2).toString())));

            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area C
             */
            List<Object> resultPrePaidTransactionsAreaC = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, 
                    totalStartDate, "Area C");
            
            List<Object> resultPrePaidTransactionsHistoryAreaC = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, "Area C");
            
            List<Object> resultPostPaidTransactionsSelfAreaC = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area C");
            
            List<Object> resultPostPaidTransactionsHistorySelfAreaC = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area C");
            
            List<Object> resultPostPaidTransactionsShopAreaC = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area C");

            List<Object> resultPostPaidTransactionsHistoryShopAreaC = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area C");


            List<Object> resultPrePaidTransactionsCurrentAndHistoryAreaC = new ArrayList<Object>();
            resultPrePaidTransactionsCurrentAndHistoryAreaC.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaC.get(0).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaC.get(0).toString()));
            
            resultPrePaidTransactionsCurrentAndHistoryAreaC.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaC.get(1).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaC.get(1).toString()));

            resultPrePaidTransactionsCurrentAndHistoryAreaC.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaC.get(2).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaC.get(2).toString()));
            

            List<Object> resultPostPaidTransactionsSelfCurrentAndHistoryAreaC = new ArrayList<Object>();
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaC.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaC.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaC.get(0).toString()));
            
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaC.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaC.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaC.get(1).toString()));

            resultPostPaidTransactionsSelfCurrentAndHistoryAreaC.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaC.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaC.get(2).toString()));
           
            
            List<Object> resultPostPaidTransactionsShopCurrentAndHistoryAreaC = new ArrayList<Object>();
             resultPostPaidTransactionsShopCurrentAndHistoryAreaC.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaC.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaC.get(0).toString()));
            
            resultPostPaidTransactionsShopCurrentAndHistoryAreaC.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaC.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaC.get(1).toString()));

            resultPostPaidTransactionsShopCurrentAndHistoryAreaC.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaC.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaC.get(2).toString()));
             
            
            List<Object> resultTransactionsTotalCountAreaC = new ArrayList<Object>();
            resultTransactionsTotalCountAreaC.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaC.get(0).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaC.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaC.get(0).toString())));
            
            resultTransactionsTotalCountAreaC.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaC.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaC.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaC.get(1).toString())));
            
            resultTransactionsTotalCountAreaC.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaC.get(2).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaC.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaC.get(2).toString())));
           
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area CS
             */
            List<Object> resultPrePaidTransactionsAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, 
                    totalStartDate, "Area CS");
            
            List<Object> resultPrePaidTransactionsHistoryAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, "Area CS");
            
            List<Object> resultPostPaidTransactionsSelfAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CS");
            
            List<Object> resultPostPaidTransactionsHistorySelfAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CS");
            
            List<Object> resultPostPaidTransactionsShopAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CS");

            List<Object> resultPostPaidTransactionsHistoryShopAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CS");


            List<Object> resultPrePaidTransactionsCurrentAndHistoryAreaCS = new ArrayList<Object>();
            resultPrePaidTransactionsCurrentAndHistoryAreaCS.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaCS.get(0).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaCS.get(0).toString()));
            
            resultPrePaidTransactionsCurrentAndHistoryAreaCS.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaCS.get(1).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaCS.get(1).toString()));

            resultPrePaidTransactionsCurrentAndHistoryAreaCS.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaCS.get(2).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaCS.get(2).toString()));
            

            List<Object> resultPostPaidTransactionsSelfCurrentAndHistoryAreaCS = new ArrayList<Object>();
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaCS.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaCS.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaCS.get(0).toString()));
            
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaCS.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaCS.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaCS.get(1).toString()));

            resultPostPaidTransactionsSelfCurrentAndHistoryAreaCS.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaCS.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaCS.get(2).toString()));
           
            
            List<Object> resultPostPaidTransactionsShopCurrentAndHistoryAreaCS = new ArrayList<Object>();
             resultPostPaidTransactionsShopCurrentAndHistoryAreaCS.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaCS.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaCS.get(0).toString()));
            
            resultPostPaidTransactionsShopCurrentAndHistoryAreaCS.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaCS.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaCS.get(1).toString()));

            resultPostPaidTransactionsShopCurrentAndHistoryAreaCS.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaCS.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaCS.get(2).toString()));
             
            
            List<Object> resultTransactionsTotalCountAreaCS = new ArrayList<Object>();
            resultTransactionsTotalCountAreaCS.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaCS.get(0).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaCS.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaCS.get(0).toString())));
            
            resultTransactionsTotalCountAreaCS.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaCS.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaCS.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaCS.get(1).toString())));
            
            resultTransactionsTotalCountAreaCS.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaCS.get(2).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaCS.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaCS.get(2).toString())));
            
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area S
             */
            List<Object> resultPrePaidTransactionsAreaS = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate, weeklyEndDate, 
                    totalStartDate, "Area S");
            
            List<Object> resultPrePaidTransactionsHistoryAreaS = QueryRepositoryBusiness.statisticsReportSynthesisPrePaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, "Area S");
            
            List<Object> resultPostPaidTransactionsSelfAreaS = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area S");
            
            List<Object> resultPostPaidTransactionsHistorySelfAreaS = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryAreaBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), "Area S");
            
            List<Object> resultPostPaidTransactionsShopAreaS = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsAreaBusiness(em, dailyStartDate, dailyEndDate, weeklyStartDate,
                    weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area S");

            List<Object> resultPostPaidTransactionsHistoryShopAreaS = QueryRepositoryBusiness.statisticsReportSynthesisPostPaidTransactionsHistoryBusiness(em, dailyStartDate, dailyEndDate, 
                    weeklyStartDate, weeklyEndDate, totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area S");


            List<Object> resultPrePaidTransactionsCurrentAndHistoryAreaS = new ArrayList<Object>();
            resultPrePaidTransactionsCurrentAndHistoryAreaS.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaS.get(0).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaS.get(0).toString()));
            
            resultPrePaidTransactionsCurrentAndHistoryAreaS.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaS.get(1).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaS.get(1).toString()));

            resultPrePaidTransactionsCurrentAndHistoryAreaS.add(
                    Integer.parseInt(resultPrePaidTransactionsAreaS.get(2).toString()) 
                    + Integer.parseInt(resultPrePaidTransactionsHistoryAreaS.get(2).toString()));
            

            List<Object> resultPostPaidTransactionsSelfCurrentAndHistoryAreaS = new ArrayList<Object>();
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaS.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaS.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaS.get(0).toString()));
            
            resultPostPaidTransactionsSelfCurrentAndHistoryAreaS.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaS.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaS.get(1).toString()));

            resultPostPaidTransactionsSelfCurrentAndHistoryAreaS.add(
                    Integer.parseInt(resultPostPaidTransactionsSelfAreaS.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistorySelfAreaS.get(2).toString()));
           
            
            List<Object> resultPostPaidTransactionsShopCurrentAndHistoryAreaS = new ArrayList<Object>();
             resultPostPaidTransactionsShopCurrentAndHistoryAreaS.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaS.get(0).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaS.get(0).toString()));
            
            resultPostPaidTransactionsShopCurrentAndHistoryAreaS.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaS.get(1).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaS.get(1).toString()));

            resultPostPaidTransactionsShopCurrentAndHistoryAreaS.add(
                    Integer.parseInt(resultPostPaidTransactionsShopAreaS.get(2).toString()) 
                    + Integer.parseInt(resultPostPaidTransactionsHistoryShopAreaS.get(2).toString()));
             
            
            List<Object> resultTransactionsTotalCountAreaS = new ArrayList<Object>();
            resultTransactionsTotalCountAreaS.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaS.get(0).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaS.get(0).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaS.get(0).toString())));
            
            resultTransactionsTotalCountAreaS.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaS.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaS.get(1).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaS.get(1).toString())));
            
            resultTransactionsTotalCountAreaS.add(integerFormat.format(
                    Double.parseDouble(resultPrePaidTransactionsCurrentAndHistoryAreaS.get(2).toString())
                    + Double.parseDouble(resultPostPaidTransactionsSelfCurrentAndHistoryAreaS.get(2).toString()) 
                    + Double.parseDouble(resultPostPaidTransactionsShopCurrentAndHistoryAreaS.get(2).toString())));
            
            
            //New Workbook
            ExcelWorkBook ewb = new ExcelWorkBook();

            final String[] sheetSynthesisDailyColumnsHeading = new String[] { "Sintesi della giornata " + sdfDate.format(dailyStartDate) };
            final String[] sheetSynthesisWeeklyColumnsHeading = new String[] { "Sintesi della settimana " + sdfDate.format(weeklyStartDate) + " - " + sdfDate.format(weeklyEndDate) };
            final String[] sheetSynthesisTotalColumnsHeading = new String[] { "Totali dal " + sdfDate.format(totalStartDate)};

            final String[] sheetSynthesisDailyPaymentRegionsColumnsHeading = new String[] { "Transazioni Payment " + sdfDate.format(dailyStartDate) };
            final String[] sheetSynthesisWeeklyPaymentRegionsColumnsHeading = new String[] { "Transazioni Payment " + sdfDate.format(weeklyStartDate) + " - " + sdfDate.format(weeklyEndDate) };
            final String[] sheetSynthesisTotalPaymentRegionsColumnsHeading = new String[] { "Totali Transazioni Payment dal " + sdfDate.format(totalStartDate) };

            final String[] sheetDetailColumnsHeading = new String[] { "ID Transazione", "Tipo Transazione", "Importo", 
                    "Esito Finale", "Data Rifornimento", "Codice PV", "Indirizzo PV", "Provincia PV" };

            //csHeader = ewb.createCellStyle(true, 12, Color.BLACK, new Color(254, 211, 0), HorizontalAlignment.CENTER_SELECTION);
            csHeaderBordered = ewb.createCellStyle(true, 12, Color.BLACK, new Color(178, 178, 178), HorizontalAlignment.CENTER_SELECTION, -1, 1, 1, 1, 1, Color.BLACK);
            csTableValue = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null);
            csTableValueBold = ewb.createCellStyle(true, 14, Color.BLACK, new Color(255, 255, 255), null);
            csTableValueBorderedLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, -1, 1, Color.BLACK);
            csTableValueBorderedRight = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, 1, -1, -1, Color.BLACK);
            csTableLabelBoldBorderedLeft = ewb.createCellStyle(true, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, -1, 1, Color.BLACK);
            csTableLabelIndentBorderedLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, 1, -1, -1, -1, 1, Color.BLACK);
            csTableValueBorderedBottom = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, 1, -1, Color.BLACK);
            csTableValueBorderedBottomRight = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, 1, 1, -1, Color.BLACK);
            csTableLabelBoldBorderedBottomLeft = ewb.createCellStyle(true, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, 1, 1, Color.BLACK);
            csTableLabelIndentBorderedBottomLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, 1, -1, -1, 1, 1, Color.BLACK);

            HashMap<String, ArrayList<ArrayList<String>>> excelDataSynthesis = new HashMap<String, ArrayList<ArrayList<String>>>();
            HashMap<String, ArrayList<ArrayList<String>>> excelDataSynthesisPaymentRegions = new HashMap<String, ArrayList<ArrayList<String>>>();
            HashMap<String, ArrayList<ArrayList<String>>> excelDataSynthesisLoyaltyRegions = new HashMap<String, ArrayList<ArrayList<String>>>();
            

            //ExcelSheetData excelDataTransaction = ewb.createSheetData();
            //ExcelSheetData excelDataWeeklyTransactions = ewb.createSheetData();
            
            excelDataSynthesis.put("daily", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesis.put("weekly", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesis.put("total", new ArrayList<ArrayList<String>>(0));

            excelDataSynthesisPaymentRegions.put("daily", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesisPaymentRegions.put("weekly", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesisPaymentRegions.put("total", new ArrayList<ArrayList<String>>(0));
            
            excelDataSynthesisLoyaltyRegions.put("daily", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesisLoyaltyRegions.put("weekly", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesisLoyaltyRegions.put("total", new ArrayList<ArrayList<String>>(0));
            
            System.out.println("Creazione excelCellStyleContainer");
            
            ArrayList<ArrayList<ExcelCellStyle>> excelCellStyleContainer = new ArrayList<ArrayList<ExcelCellStyle>>();
            ArrayList<ArrayList<ExcelCellStyle>> excelCellStyleContainerPayment = new ArrayList<ArrayList<ExcelCellStyle>>();
            
            insertDataSyntesis("Totale utenti iscritti", resultUsersCreditCard, excelDataSynthesis, excelCellStyleContainer, true, true, false);
            
            insertDataSyntesis("Utenti con prima transazione di pagamento", resultDistinctUsersTransaction, excelDataSynthesis, excelCellStyleContainer, true, true, false);

            insertDataSyntesis("Totale numero transazioni di pagamento", resultTransactionsTotalCount, excelDataSynthesis, excelCellStyleContainer, true, false, false);
            insertDataSyntesis("Totale importo transato �", resultTotalAmount, excelDataSynthesis, excelCellStyleContainer, true, false, false);
            insertDataSyntesis("Totale litri erogati", resultTransactionsTotalFuelQuantity, excelDataSynthesis, excelCellStyleContainer, true, false, false);
            insertDataSyntesis("   Di cui Diesel+", resultTransactionsTotalFuelQuantityDieselPiu, excelDataSynthesis, excelCellStyleContainer, false, false, false);
            insertDataSyntesis("   Numero transazioni pi� servito", resultPostPaidTransactionsSelfServitoCurrentAndHistory, excelDataSynthesis, excelCellStyleContainer, false, false, true);
            
            insertDataSyntesis("Area NO", resultTransactionsTotalCountAreaNO, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Area NE", resultTransactionsTotalCountAreaNE, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Area CN", resultTransactionsTotalCountAreaCN, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Area C", resultTransactionsTotalCountAreaC, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Area CS", resultTransactionsTotalCountAreaCS, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Area S", resultTransactionsTotalCountAreaS, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Totale transazioni payment", resultTransactionsTotalCount, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, true);
            
            
            
            Sheet sheet = ewb.addSheetBlank("Sintesi", false, false);
            sheet = ewb.addMergedCells(sheet, 7, 7, 1, 3, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisDailyColumnsHeading, excelDataSynthesis.get("daily"), csHeaderBordered, excelCellStyleContainer, 7, 1);
            sheet = ewb.addMergedCells(sheet, 7, 7, 5, 7, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisWeeklyColumnsHeading, excelDataSynthesis.get("weekly"), csHeaderBordered, excelCellStyleContainer, 7, 5);
            sheet = ewb.addMergedCells(sheet, 7, 7, 9, 11, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisTotalColumnsHeading, excelDataSynthesis.get("total"), csHeaderBordered, excelCellStyleContainer, 7, 9);

            sheet = ewb.addMergedCells(sheet, 18, 18, 1, 3, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisDailyPaymentRegionsColumnsHeading, excelDataSynthesisPaymentRegions.get("daily"), csHeaderBordered, excelCellStyleContainerPayment, 18, 1);
            sheet = ewb.addMergedCells(sheet, 18, 18, 5, 7, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisWeeklyPaymentRegionsColumnsHeading, excelDataSynthesisPaymentRegions.get("weekly"), csHeaderBordered, excelCellStyleContainerPayment, 18, 5);
            sheet = ewb.addMergedCells(sheet, 18, 18, 9, 11, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisTotalPaymentRegionsColumnsHeading, excelDataSynthesisPaymentRegions.get("total"), csHeaderBordered, excelCellStyleContainerPayment, 18, 9);
            
            sheet = ewb.addImageJpg(sheet, 1, 1, reportLogoUrl, false, proxyHost, proxyPort, proxyNoHosts);
            sheet = ewb.addMergedCells(sheet, 5, 5, 1, 5, csTableValueBold);
            sheet = ewb.addText(sheet, csTableValueBold, 5, 1, "Sintesi per " + resultStationsAppCount.toString().replaceAll("\\[|\\]", "") + " PV abilitati alla fatturazione elettronica");
            
            /*
            System.out.println("Creazione lista transazioni prepaid giornaliere");

            for (TransactionBean transactionBean : resultPrePaidTransactionBeanListByDate) {
                
                int rowIndex = excelDataTransaction.createRow();
                String dateRefuel = "";
 
                if (transactionBean.getCreationTimestamp() != null) {
                    dateRefuel = sdfFull.format(transactionBean.getCreationTimestamp());
                }
 
                double finalAmount = 0.0;
                String finalStatus = TransactionFinalStatusConverter.getReportText(transactionBean.getFinalStatusType(), transactionBean.getLastTransactionStatus().getStatus());
                
                if (transactionBean.getFinalAmount() != null) {
                    finalAmount = transactionBean.getFinalAmount().doubleValue();
                }
                
                excelDataTransaction.addRowData(transactionBean.getTransactionID(), rowIndex);
                excelDataTransaction.addRowData(TransactionType.PRE_PAID.getCode(), rowIndex);
                excelDataTransaction.addRowData(currencyFormat.format(finalAmount), rowIndex);
                excelDataTransaction.addRowData(finalStatus, rowIndex);
                excelDataTransaction.addRowData(dateRefuel, rowIndex);
                excelDataTransaction.addRowData(transactionBean.getStationBean().getStationID(), rowIndex);
                excelDataTransaction.addRowData(transactionBean.getStationBean().getFullAddress(), rowIndex);
                excelDataTransaction.addRowData(transactionBean.getStationBean().getProvince().toUpperCase(), rowIndex);
            }
            
            System.out.println("Creazione lista transazioni prepaid storicizzate giornaliere");
            
            for (TransactionHistoryBean transactionHistoryBean : resultPrePaidTransactionHistoryBeanListByDate) {
                
                int rowIndex = excelDataTransaction.createRow();
                String dateRefuel = "";
 
                if (transactionHistoryBean.getCreationTimestamp() != null) {
                    dateRefuel = sdfFull.format(transactionHistoryBean.getCreationTimestamp());
                }
 
                double finalAmount = 0.0;
                String finalStatus = TransactionFinalStatusConverter.getReportText(transactionHistoryBean.getFinalStatusType(), 
                        transactionHistoryBean.getLastTransactionStatusHistory().getStatus());
                
                if (transactionHistoryBean.getFinalAmount() != null) {
                    finalAmount = transactionHistoryBean.getFinalAmount().doubleValue();
                }
                
                excelDataTransaction.addRowData(transactionHistoryBean.getTransactionID(), rowIndex);
                excelDataTransaction.addRowData(TransactionType.PRE_PAID.getCode(), rowIndex);
                excelDataTransaction.addRowData(currencyFormat.format(finalAmount), rowIndex);
                excelDataTransaction.addRowData(finalStatus, rowIndex);
                excelDataTransaction.addRowData(dateRefuel, rowIndex);
                excelDataTransaction.addRowData(transactionHistoryBean.getStationBean().getStationID(), rowIndex);
                excelDataTransaction.addRowData(transactionHistoryBean.getStationBean().getFullAddress(), rowIndex);
                excelDataTransaction.addRowData(transactionHistoryBean.getStationBean().getProvince().toUpperCase(), rowIndex);
            }
            
            System.out.println("Creazione lista transazioni postpaid giornaliere");

            for (PostPaidTransactionBean postPaidTransactionBean : resultPostPaidTransactionBeanListByDate) {
                
                String status = postPaidTransactionBean.getMpTransactionStatus();
                String event = null;
                String eventCode = null;
                String eventResult = null;
                
                if (postPaidTransactionBean.getLastPostPaidTransactionEventBean() != null) {
                    event = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getEvent();
                    eventCode = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getNewState();
                    eventResult = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getResult();
                }

                if (postPaidTransactionBean.getPostPaidTransactionEventBean(StatusHelper.POST_PAID_EVENT_BE_MOV, true) == null &&
                        postPaidTransactionBean.getPostPaidTransactionEventBean(StatusHelper.POST_PAID_EVENT_BE_VOUCHER, true) == null) {
                    System.out.println(postPaidTransactionBean.getMpTransactionID() + " no BE_MOV or BE_VOUCHER event not found");
                    continue;
                }

                int rowIndex = excelDataTransaction.createRow();
                String dateRefuel = "";
 
                if (postPaidTransactionBean.getCreationTimestamp() != null) {
                    dateRefuel = sdfFull.format(postPaidTransactionBean.getCreationTimestamp());
                }
 
                double amount = 0.0;

                if (postPaidTransactionBean.getAmount() != null) {
                    amount = postPaidTransactionBean.getAmount().doubleValue();
                }
 
                String source = "";
                
                if (postPaidTransactionBean.getSource().equals(PostPaidSourceType.SHOP.getCode())) {
                    source = " (INDOOR)";
                }

                if (postPaidTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {
                    source = " (OUTDOOR)";
                }
                                
                excelDataTransaction.addRowData(postPaidTransactionBean.getMpTransactionID(), rowIndex);
                excelDataTransaction.addRowData(TransactionType.POST_PAID.getCode() + source, rowIndex);
                excelDataTransaction.addRowData(currencyFormat.format(amount), rowIndex);
                excelDataTransaction.addRowData(TransactionFinalStatusConverter.getReportText(status, event, eventCode, eventResult), rowIndex);
                excelDataTransaction.addRowData(dateRefuel, rowIndex);
                excelDataTransaction.addRowData(postPaidTransactionBean.getStationBean().getStationID(), rowIndex);
                excelDataTransaction.addRowData(postPaidTransactionBean.getStationBean().getFullAddress(), rowIndex);
                excelDataTransaction.addRowData(postPaidTransactionBean.getStationBean().getProvince().toUpperCase(), rowIndex);
            }
            
            System.out.println("Creazione lista transazioni postpaid storicizzate giornaliere");
   
            for (PostPaidTransactionHistoryBean postPaidTransactionHistoryBean : resultPostPaidTransactionHistoryBeanListByDate) {
                
                String status = postPaidTransactionHistoryBean.getMpTransactionStatus();
                String event = null;
                String eventCode = null;
                String eventResult = null;
                
                if (postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean() != null) {
                    event = postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean().getEvent();
                    eventCode = postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean().getNewState();
                    eventResult = postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean().getResult();
                }

                if (postPaidTransactionHistoryBean.getPostPaidTransactionEventHistoryBean(StatusHelper.POST_PAID_EVENT_BE_MOV, true) == null &&
                        postPaidTransactionHistoryBean.getPostPaidTransactionEventHistoryBean(StatusHelper.POST_PAID_EVENT_BE_VOUCHER, true) == null) {
                    System.out.println(postPaidTransactionHistoryBean.getMpTransactionID() + " no BE_MOV or BE_VOUCHER event not found");
                    continue;
                }

                int rowIndex = excelDataTransaction.createRow();
                String dateRefuel = "";
 
                if (postPaidTransactionHistoryBean.getCreationTimestamp() != null) {
                    dateRefuel = sdfFull.format(postPaidTransactionHistoryBean.getCreationTimestamp());
                }
 
                double amount = 0.0;

                if (postPaidTransactionHistoryBean.getAmount() != null) {
                    amount = postPaidTransactionHistoryBean.getAmount().doubleValue();
                }
 
                String source = "";
                
                if (postPaidTransactionHistoryBean.getSource().equals(PostPaidSourceType.SHOP.getCode())) {
                    source = " (INDOOR)";
                }

                if (postPaidTransactionHistoryBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {
                    source = " (OUTDOOR)";
                }
                                
                excelDataTransaction.addRowData(postPaidTransactionHistoryBean.getMpTransactionID(), rowIndex);
                excelDataTransaction.addRowData(TransactionType.POST_PAID.getCode() + source, rowIndex);
                excelDataTransaction.addRowData(currencyFormat.format(amount), rowIndex);
                excelDataTransaction.addRowData(TransactionFinalStatusConverter.getReportText(status, event, eventCode, eventResult), rowIndex);
                excelDataTransaction.addRowData(dateRefuel, rowIndex);
                excelDataTransaction.addRowData(postPaidTransactionHistoryBean.getStationBean().getStationID(), rowIndex);
                excelDataTransaction.addRowData(postPaidTransactionHistoryBean.getStationBean().getFullAddress(), rowIndex);
                excelDataTransaction.addRowData(postPaidTransactionHistoryBean.getStationBean().getProvince().toUpperCase(), rowIndex);
            }
            
            System.out.println("Creazione lista transazioni prepaid settimanali");
            
            for (TransactionBean transactionBean : resultWeeklyPrePaidTransactionBeanList) {
                
                int rowIndex = excelDataWeeklyTransactions.createRow();
                String dateRefuel = "";
 
                if (transactionBean.getCreationTimestamp() != null) {
                    dateRefuel = sdfFull.format(transactionBean.getCreationTimestamp());
                }
 
                double finalAmount = 0.0;
                String finalStatus = TransactionFinalStatusConverter.getReportText(transactionBean.getFinalStatusType(), transactionBean.getLastTransactionStatus().getStatus());
                
                if (transactionBean.getFinalAmount() != null) {
                    finalAmount = transactionBean.getFinalAmount().doubleValue();
                }
                
                excelDataWeeklyTransactions.addRowData(transactionBean.getTransactionID(), rowIndex);
                excelDataWeeklyTransactions.addRowData(TransactionType.PRE_PAID.getCode(), rowIndex);
                excelDataWeeklyTransactions.addRowData(currencyFormat.format(finalAmount), rowIndex);
                excelDataWeeklyTransactions.addRowData(finalStatus, rowIndex);
                excelDataWeeklyTransactions.addRowData(dateRefuel, rowIndex);
                excelDataWeeklyTransactions.addRowData(transactionBean.getStationBean().getStationID(), rowIndex);
                excelDataWeeklyTransactions.addRowData(transactionBean.getStationBean().getFullAddress(), rowIndex);
                excelDataWeeklyTransactions.addRowData(transactionBean.getStationBean().getProvince().toUpperCase(), rowIndex);
            }
            
            System.out.println("Creazione lista transazioni prepaid storicizzate settimanali");
            
            for (TransactionHistoryBean transactionHistoryBean : resultWeeklyPrePaidTransactionHistoryBeanList) {
                
                int rowIndex = excelDataWeeklyTransactions.createRow();
                String dateRefuel = "";
 
                if (transactionHistoryBean.getCreationTimestamp() != null) {
                    dateRefuel = sdfFull.format(transactionHistoryBean.getCreationTimestamp());
                }
 
                double finalAmount = 0.0;
                String finalStatus = TransactionFinalStatusConverter.getReportText(transactionHistoryBean.getFinalStatusType(), 
                        transactionHistoryBean.getLastTransactionStatusHistory().getStatus());
                
                if (transactionHistoryBean.getFinalAmount() != null) {
                    finalAmount = transactionHistoryBean.getFinalAmount().doubleValue();
                }
                
                excelDataWeeklyTransactions.addRowData(transactionHistoryBean.getTransactionID(), rowIndex);
                excelDataWeeklyTransactions.addRowData(TransactionType.PRE_PAID.getCode(), rowIndex);
                excelDataWeeklyTransactions.addRowData(currencyFormat.format(finalAmount), rowIndex);
                excelDataWeeklyTransactions.addRowData(finalStatus, rowIndex);
                excelDataWeeklyTransactions.addRowData(dateRefuel, rowIndex);
                excelDataWeeklyTransactions.addRowData(transactionHistoryBean.getStationBean().getStationID(), rowIndex);
                excelDataWeeklyTransactions.addRowData(transactionHistoryBean.getStationBean().getFullAddress(), rowIndex);
                excelDataWeeklyTransactions.addRowData(transactionHistoryBean.getStationBean().getProvince().toUpperCase(), rowIndex);
            }
            
            System.out.println("Creazione lista transazioni postpaid settimanali");

            for (PostPaidTransactionBean postPaidTransactionBean : resultWeeklyPostPaidTransactionBeanList) {
                
                String status = postPaidTransactionBean.getMpTransactionStatus();
                String event = null;
                String eventCode = null;
                String eventResult = null;
                
                if (postPaidTransactionBean.getLastPostPaidTransactionEventBean() != null) {
                    event = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getEvent();
                    eventCode = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getNewState();
                    eventResult = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getResult();
                }

                if (postPaidTransactionBean.getPostPaidTransactionEventBean(StatusHelper.POST_PAID_EVENT_BE_MOV, true) == null &&
                    postPaidTransactionBean.getPostPaidTransactionEventBean(StatusHelper.POST_PAID_EVENT_BE_VOUCHER, true) == null) {
                    System.out.println(postPaidTransactionBean.getMpTransactionID() + " no BE_MOV or BE_VOUCHER event not found");
                    continue;
                }
                    
                int rowIndex = excelDataWeeklyTransactions.createRow();
                String dateRefuel = "";
 
                if (postPaidTransactionBean.getCreationTimestamp() != null) {
                    dateRefuel = sdfFull.format(postPaidTransactionBean.getCreationTimestamp());
                }
 
                double amount = 0.0;

                if (postPaidTransactionBean.getAmount() != null) {
                    amount = postPaidTransactionBean.getAmount().doubleValue();
                }
 
                String source = "";
                
                if (postPaidTransactionBean.getSource().equals(PostPaidSourceType.SHOP.getCode())) {
                    source = " (INDOOR)";
                }

                if (postPaidTransactionBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {
                    source = " (OUTDOOR)";
                }
                
                excelDataWeeklyTransactions.addRowData(postPaidTransactionBean.getMpTransactionID(), rowIndex);
                excelDataWeeklyTransactions.addRowData(TransactionType.POST_PAID.getCode() + source, rowIndex);
                excelDataWeeklyTransactions.addRowData(currencyFormat.format(amount), rowIndex);
                excelDataWeeklyTransactions.addRowData(TransactionFinalStatusConverter.getReportText(status, event, eventCode, eventResult), rowIndex);
                excelDataWeeklyTransactions.addRowData(dateRefuel, rowIndex);
                excelDataWeeklyTransactions.addRowData(postPaidTransactionBean.getStationBean().getStationID(), rowIndex);
                excelDataWeeklyTransactions.addRowData(postPaidTransactionBean.getStationBean().getFullAddress(), rowIndex);
                excelDataWeeklyTransactions.addRowData(postPaidTransactionBean.getStationBean().getProvince().toUpperCase(), rowIndex);
            }
            
            System.out.println("Creazione lista transazioni postpaid storicizzate settimanali");
            
            for (PostPaidTransactionHistoryBean postPaidTransactionHistoryBean : resultWeeklyPostPaidTransactionHistoryBeanList) {
                
                String status = postPaidTransactionHistoryBean.getMpTransactionStatus();
                String event = null;
                String eventCode = null;
                String eventResult = null;
                
                if (postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean() != null) {
                    event = postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean().getEvent();
                    eventCode = postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean().getNewState();
                    eventResult = postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean().getResult();
                }
                
                if (postPaidTransactionHistoryBean.getPostPaidTransactionEventHistoryBean(StatusHelper.POST_PAID_EVENT_BE_MOV, true) == null &&
                        postPaidTransactionHistoryBean.getPostPaidTransactionEventHistoryBean(StatusHelper.POST_PAID_EVENT_BE_VOUCHER, true) == null) {
                    System.out.println(postPaidTransactionHistoryBean.getMpTransactionID() + " no BE_MOV or BE_VOUCHER event not found");
                    continue;
                }
                    
                int rowIndex = excelDataWeeklyTransactions.createRow();
                String dateRefuel = "";
 
                if (postPaidTransactionHistoryBean.getCreationTimestamp() != null) {
                    dateRefuel = sdfFull.format(postPaidTransactionHistoryBean.getCreationTimestamp());
                }
 
                double amount = 0.0;

                if (postPaidTransactionHistoryBean.getAmount() != null) {
                    amount = postPaidTransactionHistoryBean.getAmount().doubleValue();
                }
 
                String source = "";
                
                if (postPaidTransactionHistoryBean.getSource().equals(PostPaidSourceType.SHOP.getCode())) {
                    source = " (INDOOR)";
                }

                if (postPaidTransactionHistoryBean.getSource().equals(PostPaidSourceType.SELF.getCode())) {
                    source = " (OUTDOOR)";
                }
                
                excelDataWeeklyTransactions.addRowData(postPaidTransactionHistoryBean.getMpTransactionID(), rowIndex);
                excelDataWeeklyTransactions.addRowData(TransactionType.POST_PAID.getCode() + source, rowIndex);
                excelDataWeeklyTransactions.addRowData(currencyFormat.format(amount), rowIndex);
                excelDataWeeklyTransactions.addRowData(TransactionFinalStatusConverter.getReportText(status, event, eventCode, eventResult), rowIndex);
                excelDataWeeklyTransactions.addRowData(dateRefuel, rowIndex);
                excelDataWeeklyTransactions.addRowData(postPaidTransactionHistoryBean.getStationBean().getStationID(), rowIndex);
                excelDataWeeklyTransactions.addRowData(postPaidTransactionHistoryBean.getStationBean().getFullAddress(), rowIndex);
                excelDataWeeklyTransactions.addRowData(postPaidTransactionHistoryBean.getStationBean().getProvince().toUpperCase(), rowIndex);
            }
            */
            
            //excelDataTransaction.sortData(4, Date.class, "dd/MM/yyyy HH:mm:ss");
            //excelDataWeeklyTransactions.sortData(4, Date.class, "dd/MM/yyyy HH:mm:ss");
            
            System.out.println("Fine preparazione dati per generazione allegato excel");
            
            System.out.println("Esecuzione commit");
            
            userTransaction.commit();
            
            System.out.println("Commit eseguita");
            
            System.out.println("Creazione workbook");
            
            System.out.println("Creazione foglio 'Sintesi'");
            
            //System.out.println("Creazione foglio 'Dettaglio Giornata'");
            
            //ewb.addSheet("Pagamenti Giornata", sheetDetailColumnsHeading, excelDataTransaction, csHeaderBordered, csTableValue);
            
            //System.out.println("Creazione foglio 'Dettaglio Settimana'");
                        
            //ewb.addSheet("Pagamenti Settimana", sheetDetailColumnsHeading, excelDataWeeklyTransactions, csHeaderBordered, csTableValue);
                        
            System.out.println("Invio email");
            
            EmailType emailType = EmailType.DAILY_STATISTICS_REPORT_BUSINESS;
            List<Parameter> parameters = new ArrayList<Parameter>(0);

            parameters.add(new Parameter("DATE", sdfDate.format(dailyStartDate)));
            List<Attachment> attachments = new ArrayList<Attachment>(0);

            Attachment attachment = new Attachment();
            attachment.setFileName("report_statistiche_giornaliero_piva.xlsx");
            //attachment.setContent(attachmentContent);
            byte[] bytes = ewb.getBytesToStream();
            attachment.setBytes(bytes);
            attachments.add(attachment);

            //reportRecipient = "giovanni.dorazio@techedgegroup.com, luca.mancini@techedgegroup.com, alessandro.menale@techedgegroup.com";
            //reportRecipient = "giovanni.dorazio@techedgegroup.com, luca.mancini@techedgegroup.com";
            //reportRecipient = "giovanni.dorazio@techedgegroup.com";
            
            if ( reportRecipient == null || reportRecipient.equals("") ) {
                
                // Recipient empty
                System.out.println("SendEmailResult: not sent - recipient empty");
            }
            else {
                
                // Sending email
                System.out.println("Sending email to: " + reportRecipient);
                String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, reportRecipient, parameters, attachments);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }
            
            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");

            //userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }

    private enum TransactionType {
        PRE_PAID("PRE-PAID"), POST_PAID("POST-PAID");

        private final String code;

        TransactionType(final String code) {
            this.code = code;
        }

        public String getCode() {

            return code;
        }
    }
    

    private void insertDataSyntesis(String label, List<Object> result, HashMap<String, ArrayList<ArrayList<String>>> excelDataContainer,
            ArrayList<ArrayList<ExcelCellStyle>> excelCellStyleContainer, boolean rootLabel, boolean insertBlankRow, boolean lastRow) {
        ArrayList<String> dataDaily = new ArrayList<String>();
        ArrayList<String> dataWeekly = new ArrayList<String>();
        ArrayList<String> dataTotal = new ArrayList<String>();
        ArrayList<String> dataEmpty = new ArrayList<String>();
        ArrayList<ExcelCellStyle> dataCellStyle = new ArrayList<ExcelCellStyle>();
        ArrayList<ExcelCellStyle> dataEmptyCellStyle = new ArrayList<ExcelCellStyle>();

        if (rootLabel) {
            if (lastRow) {
                dataCellStyle.add(csTableLabelBoldBorderedBottomLeft);
            }
            else {
                dataCellStyle.add(csTableLabelBoldBorderedLeft);
            }
        }
        else {
            if (lastRow) {
                dataCellStyle.add(csTableLabelIndentBorderedBottomLeft);
            }
            else {
                dataCellStyle.add(csTableLabelIndentBorderedLeft);
            }
        }

        if (lastRow) {
            dataCellStyle.add(csTableValueBorderedBottom);
            dataCellStyle.add(csTableValueBorderedBottomRight);
        }
        else {
            dataCellStyle.add(csTableValue);
            dataCellStyle.add(csTableValueBorderedRight);
        }
        
        excelCellStyleContainer.add(dataCellStyle);

        if (insertBlankRow) {
            dataEmptyCellStyle.add(csTableValueBorderedLeft);
            dataEmptyCellStyle.add(csTableValue);
            dataEmptyCellStyle.add(csTableValueBorderedRight);
            excelCellStyleContainer.add(dataEmptyCellStyle);
        }

        dataDaily.add(label);
        dataDaily.add("");
        dataDaily.add(result.get(0).toString());
        excelDataContainer.get("daily").add(dataDaily);

        if (insertBlankRow) {
            dataEmpty.add("");
            dataEmpty.add("");
            dataEmpty.add("");
            excelDataContainer.get("daily").add(dataEmpty);
        }

        dataWeekly.add(label);
        dataWeekly.add("");
        dataWeekly.add(result.get(1).toString());
        excelDataContainer.get("weekly").add(dataWeekly);

        if (insertBlankRow) {
            dataEmpty.clear();
            dataEmpty.add("");
            dataEmpty.add("");
            dataEmpty.add("");
            excelDataContainer.get("weekly").add(dataEmpty);
        }

        dataTotal.add(label);
        dataTotal.add("");
        dataTotal.add(result.get(2).toString());
        excelDataContainer.get("total").add(dataTotal);

        if (insertBlankRow) {
            dataEmpty.clear();
            dataEmpty.add("");
            dataEmpty.add("");
            dataEmpty.add("");
            excelDataContainer.get("total").add(dataEmpty);
        }
    }
    /*
    private static String resultAsString(Object o) {
        if (o instanceof Object[]) {
            return Arrays.asList((Object[])o).toString();
        } else {
            return String.valueOf(o);
        }
    }
    */
}