package com.techedge.mp.core.actions.scheduler;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.SmsLogBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.ResendValidation;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.sms.adapter.business.SmsServiceRemote;
import com.techedge.mp.sms.adapter.business.interfaces.SendMessageResult;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobSmsPendingRetryAction {
    private static final String JOB_NAME = "JOB_SMS_PENDING_RETRY";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService loggerService;
    
    public SchedulerJobSmsPendingRetryAction() {}

    public String execute(Long pendingTimeout, SmsServiceRemote smsService, UserCategoryService userCategoryService, StringSubstitution stringSubstitution) throws EJBException {

        Date now = new Date();

        System.out.println("job name: " + SchedulerJobSmsPendingRetryAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());
        
        UserTransaction userTransaction = context.getUserTransaction();
        
        try {
            userTransaction.begin();
            
            
            List<SmsLogBean> smsLogBeanList = QueryRepository.findSmsLogPending(em);
            
            if (smsLogBeanList == null || smsLogBeanList.isEmpty()) {
                
                // Non esiste nessuna una transazione attiva
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Sms pending not found.");
                
                userTransaction.commit();
                
                System.out.println("job result: " + "OK");
                
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
            }

            for (SmsLogBean smsLogBean : smsLogBeanList) {

                Calendar calendarDeadTime = Calendar.getInstance();
                now = Calendar.getInstance().getTime();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                Date lastRetryTimestamp = smsLogBean.getLastRetryTimestamp();                
                Integer userType = smsLogBean.getUser().getUserType();
                Boolean useNewAcquirer = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
                String senderAlias = smsService.getSender();
                
                if (useNewAcquirer) {
                    senderAlias = stringSubstitution.getValue(senderAlias, 1);
                }

                if (lastRetryTimestamp == null) {
                    lastRetryTimestamp = smsLogBean.getCreationTimestamp();
                }
                
                long smsPendingTimeoutTime = (lastRetryTimestamp.getTime() + pendingTimeout);
                calendarDeadTime.setTimeInMillis(smsPendingTimeoutTime);
                System.out.println("Pending Timeout: " + pendingTimeout);
                System.out.println("Data ultima retry: " + sdf.format(lastRetryTimestamp) + " (" + lastRetryTimestamp.getTime() + ")");
                System.out.println("Data presunta scadenza sms pending: " + sdf.format(calendarDeadTime.getTime()) + " (" + smsPendingTimeoutTime + ")");
                System.out.println("Data sms pendenti: " + sdf.format(now));

                if (smsPendingTimeoutTime < now.getTime()) {
                    System.out.println("Sms pending " + smsLogBean.getCorrelationID() + " retry to new sending");
                    String destinationAddress = smsLogBean.getDestinationAddress();
                    String message = smsLogBean.getMessage();
                    String messageId = smsLogBean.getCorrelationID();
                    
                    smsLogBean.setLastRetryTimestamp(lastRetryTimestamp);
                    smsLogBean.setRetryAttemptsLeft(smsLogBean.getRetryAttemptsLeft() - 1);
                    em.merge(smsLogBean);
                    
                    loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null, "Sending sms to " + destinationAddress);

                    SendMessageResult sendMessageResult = smsService.sendShortMessage(senderAlias, destinationAddress, message, messageId);
                    
                    loggerService.log(ErrorLevel.INFO, ResendValidation.class.getSimpleName(), "execute", null, null, "sendShortMessage result: " + sendMessageResult.getResponseMessage());
                }
            }
            
            userTransaction.commit();
            
            System.out.println("job result: " + "OK");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
}
