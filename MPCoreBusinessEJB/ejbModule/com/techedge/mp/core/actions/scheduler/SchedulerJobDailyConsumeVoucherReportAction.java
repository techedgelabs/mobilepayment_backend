package com.techedge.mp.core.actions.scheduler;

import java.awt.Color;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.apache.poi.ss.usermodel.HorizontalAlignment;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherDetailHistoryBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherHistoryBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.model.voucher.VoucherTransactionBean;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelSheetData;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.TransactionFinalStatusConverter;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobDailyConsumeVoucherReportAction {
    private static final String JOB_NAME = "JOB_DAILY_CONSUME_VOUCHER_REPORT";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    //Cell style for header row
    //private ExcelCellStyle      csHeader;
    private ExcelCellStyle      csHeaderBordered;
    //Cell style for table row
    private ExcelCellStyle      csTableValue;
    private ExcelCellStyle      csTableValueBold;
    private ExcelCellStyle      csTableValueBorderedLeft;
    private ExcelCellStyle      csTableValueBorderedRight;
    private ExcelCellStyle      csTableLabelBoldBorderedLeft;
    private ExcelCellStyle      csTableLabelIndentBorderedLeft;
    private ExcelCellStyle      csTableValueBorderedBottom;
    private ExcelCellStyle      csTableValueBorderedBottomRight;
    private ExcelCellStyle      csTableLabelBoldBorderedBottomLeft;
    private ExcelCellStyle      csTableLabelIndentBorderedBottomLeft;

    public SchedulerJobDailyConsumeVoucherReportAction() {}

    public String execute(EmailSenderRemote emailSender, String reportRecipient, String reportRecipientBCC, String proxyHost, String proxyPort, String proxyNoHosts) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        Date now = new Date();

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");
        
        NumberFormat integerFormat = NumberFormat.getNumberInstance(Locale.ITALIAN);
        integerFormat.setRoundingMode(RoundingMode.HALF_UP);
        integerFormat.setMinimumFractionDigits(0);
        integerFormat.setMaximumFractionDigits(0);

        System.out.println("job name: " + SchedulerJobDailyConsumeVoucherReportAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {

            userTransaction.begin();

            SimpleDateFormat sdfLong = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            SimpleDateFormat sdfShort = new SimpleDateFormat("dd/MM/yyyy");

            Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
            System.out.println("DATA ATTUALE:" + sdfLong.format(calendar.getTime()));
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date dailyEndDate = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date dailyStartDate = calendar.getTime();

            System.out.println("DATA GIORNALIERA INIZIO:" + sdfLong.format(dailyStartDate));
            System.out.println("DATA GIORNALIERA FINE:" + sdfLong.format(dailyEndDate));
            
            List<TransactionBean> resultPrePaidTransactionBeanListByDate = QueryRepository.statisticsReportDetailPrePaidTransactions(em, dailyStartDate, dailyEndDate);
            
            List<TransactionHistoryBean> resultPrePaidTransactionHistoryBeanListByDate = QueryRepository.statisticsReportDetailPrePaidTransactionsHistory(em, 
                    dailyStartDate, dailyEndDate);
            
            List<PostPaidTransactionBean> resultPostPaidTransactionBeanListByDate = QueryRepository.statisticsReportDetailPostPaidTransactions(em, dailyStartDate, dailyEndDate);
            
            List<PostPaidTransactionHistoryBean> resultPostPaidTransactionHistoryBeanListByDate = QueryRepository.statisticsReportDetailPostPaidTransactionsHistory(em, 
                    dailyStartDate, dailyEndDate);
            
            //New Workbook
            ExcelWorkBook ewb = new ExcelWorkBook();

            final String[] sheetDetailColumnsHeading = new String[] { "ID Transazione", "Codice autorizzazione", "Importo Voucher", "Codice Voucher",
                    "Esito Finale", "Data e Ora Bruciatura", "Codice PV", "Indirizzo PV", "Provincia PV" };

            //csHeader = ewb.createCellStyle(true, 12, Color.BLACK, new Color(254, 211, 0), HorizontalAlignment.CENTER_SELECTION);
            csHeaderBordered = ewb.createCellStyle(true, 12, Color.BLACK, new Color(254, 211, 0), HorizontalAlignment.CENTER_SELECTION, -1, 1, 1, 1, 1, Color.BLACK);
            csTableValue = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null);
            csTableValueBold = ewb.createCellStyle(true, 14, Color.BLACK, new Color(255, 255, 255), null);
            csTableValueBorderedLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, -1, 1, Color.BLACK);
            csTableValueBorderedRight = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, 1, -1, -1, Color.BLACK);
            csTableLabelBoldBorderedLeft = ewb.createCellStyle(true, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, -1, 1, Color.BLACK);
            csTableLabelIndentBorderedLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, 1, -1, -1, -1, 1, Color.BLACK);
            csTableValueBorderedBottom = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, 1, -1, Color.BLACK);
            csTableValueBorderedBottomRight = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, 1, 1, -1, Color.BLACK);
            csTableLabelBoldBorderedBottomLeft = ewb.createCellStyle(true, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, 1, 1, Color.BLACK);
            csTableLabelIndentBorderedBottomLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, 1, -1, -1, 1, 1, Color.BLACK);

            ExcelSheetData excelData = ewb.createSheetData();

            System.out.println("Creazione lista transazioni prepaid giornaliere");

            for (TransactionBean transactionBean : resultPrePaidTransactionBeanListByDate) {

                Double consumed = 0.0;
                String voucherCode = "";
                
                String transactionID = transactionBean.getTransactionID();
                
                String authorizationCode = transactionBean.getAuthorizationCode();
                
                String finalStatus = TransactionFinalStatusConverter.getReportText(transactionBean.getFinalStatusType(), transactionBean.getLastTransactionStatus().getStatus());
                
                String timeRefuel = "";
                if (transactionBean.getCreationTimestamp() != null) {
                    timeRefuel = sdfLong.format(transactionBean.getCreationTimestamp());
                }
                
                // Cerca l'operazione di storno consumo voucher
                Boolean reverseFound = false;
                for(PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {
                    if ( prePaidConsumeVoucherBean.getOperationType().equals("REVERSE") ) {
                        reverseFound = true;
                    }
                }
                
                if (!reverseFound) {
                    
                    for (PrePaidConsumeVoucherBean prePaidConsumeVoucherBean : transactionBean.getPrePaidConsumeVoucherBeanList()) {
                        
                        if (prePaidConsumeVoucherBean.getTotalConsumed() != null && prePaidConsumeVoucherBean.getTotalConsumed() > 0) {
                            
                            for (PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean : prePaidConsumeVoucherBean.getPrePaidConsumeVoucherDetailBean()) {
                            
                                consumed    = prePaidConsumeVoucherDetailBean.getConsumedValue().doubleValue();
                                voucherCode = prePaidConsumeVoucherDetailBean.getVoucherCode();
                                
                                if ( isVoucherPurchased(voucherCode) ) {
                                
                                    int rowIndex = excelData.createRow();

                                    excelData.addRowData(transactionID, rowIndex);
                                    excelData.addRowData(authorizationCode, rowIndex);
                                    excelData.addRowData(currencyFormat.format(consumed), rowIndex);
                                    excelData.addRowData(voucherCode, rowIndex);
                                    excelData.addRowData(finalStatus, rowIndex);
                                    excelData.addRowData(timeRefuel, rowIndex);
                                    excelData.addRowData(transactionBean.getStationBean().getStationID(), rowIndex);
                                    excelData.addRowData(transactionBean.getStationBean().getFullAddress(), rowIndex);
                                    excelData.addRowData(transactionBean.getStationBean().getProvince().toUpperCase(), rowIndex);

                                }
                            }
                        }
                    }
                }
            }
            
            System.out.println("Creazione lista transazioni prepaid storicizzate giornaliere");
            
            for (TransactionHistoryBean transactionHistoryBean : resultPrePaidTransactionHistoryBeanListByDate) {

                Double consumed = 0.0;
                String voucherCode = "";
                
                String transactionID = transactionHistoryBean.getTransactionID();
                
                String authorizationCode = transactionHistoryBean.getAuthorizationCode();
                
                String finalStatus = TransactionFinalStatusConverter.getReportText(transactionHistoryBean.getFinalStatusType(), transactionHistoryBean.getLastTransactionStatusHistory().getStatus());
                
                String timeRefuel = "";
                if (transactionHistoryBean.getCreationTimestamp() != null) {
                    timeRefuel = sdfLong.format(transactionHistoryBean.getCreationTimestamp());
                }
                
                // Cerca l'operazione di storno consumo voucher
                Boolean reverseFound = false;
                for(PrePaidConsumeVoucherHistoryBean prePaidConsumeVoucherHistoryBean : transactionHistoryBean.getPrePaidConsumeVoucherHistoryBeanList()) {
                    if ( prePaidConsumeVoucherHistoryBean.getOperationType().equals("REVERSE") ) {
                        reverseFound = true;
                    }
                }
                
                if (!reverseFound) {
                    
                    for (PrePaidConsumeVoucherHistoryBean prePaidConsumeVoucherHistoryBean : transactionHistoryBean.getPrePaidConsumeVoucherHistoryBeanList()) {
                        
                        if (prePaidConsumeVoucherHistoryBean.getTotalConsumed() != null && prePaidConsumeVoucherHistoryBean.getTotalConsumed() > 0) {
                            
                            for (PrePaidConsumeVoucherDetailHistoryBean prePaidConsumeVoucherDetailHistoryBean : prePaidConsumeVoucherHistoryBean.getPrePaidConsumeVoucherDetailHistoryBean() ) {
                            
                                consumed    = prePaidConsumeVoucherDetailHistoryBean.getConsumedValue().doubleValue();
                                voucherCode = prePaidConsumeVoucherDetailHistoryBean.getVoucherCode();
                                
                                if ( isVoucherPurchased(voucherCode) ) {
                                    
                                    int rowIndex = excelData.createRow();
                                    
                                    excelData.addRowData(transactionID, rowIndex);
                                    excelData.addRowData(authorizationCode, rowIndex);
                                    excelData.addRowData(currencyFormat.format(consumed), rowIndex);
                                    excelData.addRowData(voucherCode, rowIndex);
                                    excelData.addRowData(finalStatus, rowIndex);
                                    excelData.addRowData(timeRefuel, rowIndex);
                                    excelData.addRowData(transactionHistoryBean.getStationBean().getStationID(), rowIndex);
                                    excelData.addRowData(transactionHistoryBean.getStationBean().getFullAddress(), rowIndex);
                                    excelData.addRowData(transactionHistoryBean.getStationBean().getProvince().toUpperCase(), rowIndex);
                                }
                            }
                        }
                    }
                }
            }
            
            System.out.println("Creazione lista transazioni postpaid giornaliere");

            for (PostPaidTransactionBean postPaidTransactionBean : resultPostPaidTransactionBeanListByDate) {

                String status = postPaidTransactionBean.getMpTransactionStatus();
                String event = null;
                String eventCode = null;
                String eventResult = null;
                
                if (postPaidTransactionBean.getLastPostPaidTransactionEventBean() != null) {
                    event = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getEvent();
                    eventCode = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getNewState();
                    eventResult = postPaidTransactionBean.getLastPostPaidTransactionEventBean().getResult();
                }

                if (postPaidTransactionBean.getPostPaidTransactionEventBean(StatusHelper.POST_PAID_EVENT_BE_MOV, true) == null &&
                        postPaidTransactionBean.getPostPaidTransactionEventBean(StatusHelper.POST_PAID_EVENT_BE_VOUCHER, true) == null) {
                    System.out.println(postPaidTransactionBean.getMpTransactionID() + " no BE_MOV or BE_VOUCHER event not found");
                    continue;
                }
                
                
                Double consumed = 0.0;
                String voucherCode = "";
                
                String transactionID = postPaidTransactionBean.getMpTransactionID();
                
                String authorizationCode = postPaidTransactionBean.getAuthorizationCode();
                
                String finalStatus = TransactionFinalStatusConverter.getReportText(status, event, eventCode, eventResult);
                
                String timeRefuel = "";
                if (postPaidTransactionBean.getCreationTimestamp() != null) {
                    timeRefuel = sdfLong.format(postPaidTransactionBean.getCreationTimestamp());
                }
                
                // Cerca l'operazione di storno consumo voucher
                Boolean reverseFound = false;
                for(PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidTransactionBean.getPostPaidConsumeVoucherBeanList()) {
                    if ( postPaidConsumeVoucherBean.getOperationType().equals("REVERSE") ) {
                        reverseFound = true;
                    }
                }
                
                if (!reverseFound) {
                    
                    for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidTransactionBean.getPostPaidConsumeVoucherBeanList()) {
                        
                        if (postPaidConsumeVoucherBean.getTotalConsumed() != null && postPaidConsumeVoucherBean.getTotalConsumed() > 0) {
                            
                            for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean() ) {
                            
                                consumed    = postPaidConsumeVoucherDetailBean.getConsumedValue().doubleValue();
                                voucherCode = postPaidConsumeVoucherDetailBean.getVoucherCode();
                                
                                if ( isVoucherPurchased(voucherCode) ) {
                                    
                                    int rowIndex = excelData.createRow();

                                    excelData.addRowData(transactionID, rowIndex);
                                    excelData.addRowData(authorizationCode, rowIndex);
                                    excelData.addRowData(currencyFormat.format(consumed), rowIndex);
                                    excelData.addRowData(voucherCode, rowIndex);
                                    excelData.addRowData(finalStatus, rowIndex);
                                    excelData.addRowData(timeRefuel, rowIndex);
                                    excelData.addRowData(postPaidTransactionBean.getStationBean().getStationID(), rowIndex);
                                    excelData.addRowData(postPaidTransactionBean.getStationBean().getFullAddress(), rowIndex);
                                    excelData.addRowData(postPaidTransactionBean.getStationBean().getProvince().toUpperCase(), rowIndex);
                                }
                            }
                        }
                    }
                }
            }
            
            System.out.println("Creazione lista transazioni postpaid storicizzate giornaliere");
   
            for (PostPaidTransactionHistoryBean postPaidTransactionHistoryBean : resultPostPaidTransactionHistoryBeanListByDate) {

                String status = postPaidTransactionHistoryBean.getMpTransactionStatus();
                String event = null;
                String eventCode = null;
                String eventResult = null;
                
                if (postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean() != null) {
                    event = postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean().getEvent();
                    eventCode = postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean().getNewState();
                    eventResult = postPaidTransactionHistoryBean.getLastPostPaidTransactionEventHistoryBean().getResult();
                }

                if (postPaidTransactionHistoryBean.getPostPaidTransactionEventHistoryBean(StatusHelper.POST_PAID_EVENT_BE_MOV, true) == null &&
                        postPaidTransactionHistoryBean.getPostPaidTransactionEventHistoryBean(StatusHelper.POST_PAID_EVENT_BE_VOUCHER, true) == null) {
                    System.out.println(postPaidTransactionHistoryBean.getMpTransactionID() + " no BE_MOV or BE_VOUCHER event not found");
                    continue;
                }
                
                
                Double consumed = 0.0;
                String voucherCode = "";
                
                String transactionID = postPaidTransactionHistoryBean.getMpTransactionID();
                
                String authorizationCode = postPaidTransactionHistoryBean.getAuthorizationCode();
                
                String finalStatus = TransactionFinalStatusConverter.getReportText(status, event, eventCode, eventResult);
                
                String timeRefuel = "";
                if (postPaidTransactionHistoryBean.getCreationTimestamp() != null) {
                    timeRefuel = sdfLong.format(postPaidTransactionHistoryBean.getCreationTimestamp());
                }
                
                // Cerca l'operazione di storno consumo voucher
                Boolean reverseFound = false;
                for(PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean : postPaidTransactionHistoryBean.getPostPaidConsumeVoucherBeanList() ) {
                    if ( postPaidConsumeVoucherHistoryBean.getOperationType().equals("REVERSE") ) {
                        reverseFound = true;
                    }
                }
                
                if (!reverseFound) {
                    
                    for (PostPaidConsumeVoucherHistoryBean postPaidConsumeVoucherHistoryBean : postPaidTransactionHistoryBean.getPostPaidConsumeVoucherBeanList()) {

                        if (postPaidConsumeVoucherHistoryBean.getTotalConsumed() != null && postPaidConsumeVoucherHistoryBean.getTotalConsumed() > 0) {
                            
                            for (PostPaidConsumeVoucherDetailHistoryBean postPaidConsumeVoucherDetailHistoryBean : postPaidConsumeVoucherHistoryBean.getPostPaidConsumeVoucherDetailHistoryBean() ) {
                            
                                consumed    = postPaidConsumeVoucherDetailHistoryBean.getConsumedValue().doubleValue();
                                voucherCode = postPaidConsumeVoucherDetailHistoryBean.getVoucherCode();
                                
                                if ( isVoucherPurchased(voucherCode) ) {
                                    
                                    int rowIndex = excelData.createRow();

                                    excelData.addRowData(transactionID, rowIndex);
                                    excelData.addRowData(authorizationCode, rowIndex);
                                    excelData.addRowData(currencyFormat.format(consumed), rowIndex);
                                    excelData.addRowData(voucherCode, rowIndex);
                                    excelData.addRowData(finalStatus, rowIndex);
                                    excelData.addRowData(timeRefuel, rowIndex);
                                    excelData.addRowData(postPaidTransactionHistoryBean.getStationBean().getStationID(), rowIndex);
                                    excelData.addRowData(postPaidTransactionHistoryBean.getStationBean().getFullAddress(), rowIndex);
                                    excelData.addRowData(postPaidTransactionHistoryBean.getStationBean().getProvince().toUpperCase(), rowIndex);
                                }
                            }
                        }
                    }
                }
            }
            
            excelData.sortData(5, Date.class, "dd/MM/yyyy HH:mm:ss");
            
            System.out.println("Fine preparazione dati per generazione allegato excel");
            
            System.out.println("Esecuzione commit");
            
            userTransaction.commit();
            
            System.out.println("Commit eseguita");
            
            System.out.println("Creazione workbook");
            
            System.out.println("Creazione foglio 'Report bruciature Voucher'");
            
            ewb.addSheet("Report bruciature Voucher", sheetDetailColumnsHeading, excelData, csHeaderBordered, csTableValue);
                        
            System.out.println("Invio email");
            
            EmailType emailType = EmailType.DAILY_CONSUME_VOUCHER_REPORT_V2;
            List<Parameter> parameters = new ArrayList<Parameter>(0);

            parameters.add(new Parameter("DATE", sdfShort.format(dailyStartDate)));
            List<Attachment> attachments = new ArrayList<Attachment>(0);

            // L'allegato deve essere inviato solo se � stato consumato almeno un voucher
            Attachment attachment = new Attachment();
            attachment.setFileName("report_bruciature_voucher.xlsx");
            //attachment.setContent(attachmentContent);
            byte[] bytes = ewb.getBytesToStream();
            attachment.setBytes(bytes);
            attachments.add(attachment);

            //reportRecipient = "giovanni.dorazio@techedgegroup.com, luca.mancini@techedgegroup.com, alessandro.menale@techedgegroup.com, giovanni.apuzzo@techedgegroup.com";
            //reportRecipient = "giovanni.dorazio@techedgegroup.com, luca.mancini@techedgegroup.com";
            //reportRecipient = "luca.mancini@techedgegroup.com";
            
            if ( reportRecipient == null || reportRecipient.equals("") ) {
                
                // Recipient empty
                System.out.println("SendEmailResult: not sent - recipient empty");
            }
            else {
                
                // Sending email
                System.out.println("Sending email to: " + reportRecipient);
                String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, reportRecipient, parameters, attachments);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }
            
            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");

            //userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
    
    
    private Boolean isVoucherPurchased(String voucherCode) {
        
        List<VoucherTransactionBean> voucherTransactionBeanList = QueryRepository.findVoucherTransactionsByVoucherCode(em, voucherCode);
        
        if ( voucherTransactionBeanList.isEmpty() ) {
            return Boolean.FALSE;
        }
        else {
            return Boolean.TRUE;
        }
    }
    

    private void insertDataSyntesis(String label, List<Object> result, HashMap<String, ArrayList<ArrayList<String>>> excelDataContainer,
            ArrayList<ArrayList<ExcelCellStyle>> excelCellStyleContainer, boolean rootLabel, boolean insertBlankRow, boolean lastRow) {
        ArrayList<String> dataDaily = new ArrayList<String>();
        ArrayList<String> dataWeekly = new ArrayList<String>();
        ArrayList<String> dataTotal = new ArrayList<String>();
        ArrayList<String> dataEmpty = new ArrayList<String>();
        ArrayList<ExcelCellStyle> dataCellStyle = new ArrayList<ExcelCellStyle>();
        ArrayList<ExcelCellStyle> dataEmptyCellStyle = new ArrayList<ExcelCellStyle>();

        if (rootLabel) {
            if (lastRow) {
                dataCellStyle.add(csTableLabelBoldBorderedBottomLeft);
            }
            else {
                dataCellStyle.add(csTableLabelBoldBorderedLeft);
            }
        }
        else {
            if (lastRow) {
                dataCellStyle.add(csTableLabelIndentBorderedBottomLeft);
            }
            else {
                dataCellStyle.add(csTableLabelIndentBorderedLeft);
            }
        }

        if (lastRow) {
            dataCellStyle.add(csTableValueBorderedBottom);
            dataCellStyle.add(csTableValueBorderedBottomRight);
        }
        else {
            dataCellStyle.add(csTableValue);
            dataCellStyle.add(csTableValueBorderedRight);
        }
        
        excelCellStyleContainer.add(dataCellStyle);

        if (insertBlankRow) {
            dataEmptyCellStyle.add(csTableValueBorderedLeft);
            dataEmptyCellStyle.add(csTableValue);
            dataEmptyCellStyle.add(csTableValueBorderedRight);
            excelCellStyleContainer.add(dataEmptyCellStyle);
        }

        dataDaily.add(label);
        dataDaily.add("");
        dataDaily.add(result.get(0).toString());
        excelDataContainer.get("daily").add(dataDaily);

        if (insertBlankRow) {
            dataEmpty.add("");
            dataEmpty.add("");
            dataEmpty.add("");
            excelDataContainer.get("daily").add(dataEmpty);
        }

        dataWeekly.add(label);
        dataWeekly.add("");
        dataWeekly.add(result.get(1).toString());
        excelDataContainer.get("weekly").add(dataWeekly);

        if (insertBlankRow) {
            dataEmpty.clear();
            dataEmpty.add("");
            dataEmpty.add("");
            dataEmpty.add("");
            excelDataContainer.get("weekly").add(dataEmpty);
        }

        dataTotal.add(label);
        dataTotal.add("");
        dataTotal.add(result.get(2).toString());
        excelDataContainer.get("total").add(dataTotal);

        if (insertBlankRow) {
            dataEmpty.clear();
            dataEmpty.add("");
            dataEmpty.add("");
            dataEmpty.add("");
            excelDataContainer.get("total").add(dataEmpty);
        }
    }

    private static String resultAsString(Object o) {
        if (o instanceof Object[]) {
            return Arrays.asList((Object[])o).toString();
        } else {
            return String.valueOf(o);
        }
    }

}