package com.techedge.mp.core.actions.scheduler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.dwh.DWHCategoryEarnDetailBean;
import com.techedge.mp.core.business.model.dwh.DWHPartnerDetailBean;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.interfaces.CategoryEarnDetail;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCategoryEarnResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetPartnerResult;
import com.techedge.mp.dwh.adapter.interfaces.PartnerDetail;
import com.techedge.mp.dwh.adapter.utilities.StatusCode;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobUpdateDWHEarnDataAction {
    private static final String JOB_NAME = "JOB_UPDATE_DWH_EARN_DATA";

    @EJB
    private LoggerService       loggerService;

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobUpdateDWHEarnDataAction() {}

    public String execute() throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        Date now = new Date();

        System.out.println("job name: " + SchedulerJobUpdateDWHEarnDataAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {

            System.out.println("Update Partner and categoryEarn List Action ...");

            List<PartnerDetail> partnerDetailList = new ArrayList<>();

            List<CategoryEarnDetail> categoryEarnDetailList = new ArrayList<>();

            DWHAdapterServiceRemote dwhAdapterService = EJBHomeCache.getInstance().getDWHAdapterService();

            DWHGetPartnerResult resultDwhGetPartnerResult = dwhAdapterService.getPartnerList();

            if (resultDwhGetPartnerResult == null || resultDwhGetPartnerResult.getStatusCode() == null
                    || !resultDwhGetPartnerResult.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                        "Si � verificato un errore nel servizio getPartnerList del DWHAdapterService");

                System.out.println("job endTimestamp: " + now.toString());
                System.out.println("job result: " + "OK");

                return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
            }

            DWHGetCategoryEarnResult resultEarnCategory = dwhAdapterService.getCategoryEarnList();

            if (resultEarnCategory == null || resultEarnCategory.getStatusCode() == null || !resultEarnCategory.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                        "Si � verificato un errore nel servizio getCategoryEarnList del DWHAdapterService");

                System.out.println("job endTimestamp: " + now.toString());
                System.out.println("job result: " + "OK");

                return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
            }

            partnerDetailList = resultDwhGetPartnerResult.getPartnerDetailList();
            categoryEarnDetailList = resultEarnCategory.getCategoryEarnList();

            userTransaction.begin();

            /* Rimozione dei partner prima dell'aggiornamento */
            List<DWHPartnerDetailBean> partnerListDetailBeanToRemove = QueryRepository.findAllDwhPartnerDetail(em);
            if (partnerListDetailBeanToRemove != null && partnerListDetailBeanToRemove.size() > 0) {
                for (DWHPartnerDetailBean dwhPartnerDetailBean : partnerListDetailBeanToRemove) {
                    em.remove(dwhPartnerDetailBean);
                }
            }

            /* Rimozione di category prima dell'aggiornamento */
            List<DWHCategoryEarnDetailBean> categoryEarnDetailBeanToRemove = QueryRepository.findAllDwhCategoryEarnDetailBean(em);
            if (categoryEarnDetailBeanToRemove != null && !categoryEarnDetailBeanToRemove.isEmpty()) {
                for (DWHCategoryEarnDetailBean dwhCategoryDetailBean : categoryEarnDetailBeanToRemove) {
                    em.remove(dwhCategoryDetailBean);
                }
            }

            for (PartnerDetail partnerDetail : partnerDetailList) {

                DWHPartnerDetailBean dwhPartnerDetailBean = new DWHPartnerDetailBean();

                dwhPartnerDetailBean.setCategoryId(partnerDetail.getCategoryId());
                dwhPartnerDetailBean.setCategory(this.getCategoryDescription(partnerDetail.getCategoryId(), categoryEarnDetailList));
                dwhPartnerDetailBean.setDescription(partnerDetail.getDescription());
                dwhPartnerDetailBean.setLogic(partnerDetail.getLogic());
                dwhPartnerDetailBean.setLogoSmallURL(partnerDetail.getLogoSmallUrl());
                dwhPartnerDetailBean.setLogoUrl(partnerDetail.getLogoUrl());
                dwhPartnerDetailBean.setName(partnerDetail.getName());
                dwhPartnerDetailBean.setPartnerId(partnerDetail.getPartnerId());
                dwhPartnerDetailBean.setTitle(partnerDetail.getTitle());
                dwhPartnerDetailBean.setCreationTimestamp(now);

                em.persist(dwhPartnerDetailBean);
            }

            for (CategoryEarnDetail categoryEarnDetail : categoryEarnDetailList) {

                DWHCategoryEarnDetailBean dWHCategoryDetailBean = new DWHCategoryEarnDetailBean();
                dWHCategoryDetailBean.setCategoryId(categoryEarnDetail.getCategoryId());
                dWHCategoryDetailBean.setCategory(categoryEarnDetail.getCategory());
                dWHCategoryDetailBean.setCategoryImageUrl(categoryEarnDetail.getCategoryImageUrl());
                dWHCategoryDetailBean.setCreationTimestamp(now);

                em.persist(dWHCategoryDetailBean);
            }

            System.out.println("Total partners persisted: " + partnerDetailList.size());
            System.out.println("Total category earn persisted: " + categoryEarnDetailList.size());

            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");

            userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }

    private String getCategoryDescription(Integer categoryId, List<CategoryEarnDetail> categoryEarnDetailList) {
        String categoryName = "";
        for (CategoryEarnDetail categoryEarnDetail : categoryEarnDetailList) {
            int categoryIdInteger = categoryEarnDetail.getCategoryId();
            if (categoryIdInteger == categoryId) {
                categoryName = categoryEarnDetail.getCategory();
            }

        }
        return categoryName;
    }

}