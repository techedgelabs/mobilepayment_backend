package com.techedge.mp.core.actions.scheduler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.EventNotificationService;
import com.techedge.mp.core.business.EventNotificationServiceRemote;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.model.EsPromotionBean;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VerificationCodeBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.CreateVoucherPromotionalEs;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationMessage;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.pushnotification.adapter.interfaces.StatusCode;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobCreateVoucherEsPromotionAction {
    private static final String JOB_NAME = "JOB_CREATE_VOUCHER_ES_PROMOTION";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobCreateVoucherEsPromotionAction() {}

    public String execute(EventNotificationService eventNotificationService, PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, String promoCode, Double amount, Integer sogliaPromo, Integer maxRetryAttemps, EmailSenderRemote emailSender, String landingWinnerVodafone) throws EJBException {
    	
        System.out.println("job name: " + SchedulerJobCreateVoucherEsPromotionAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + (new Date()).toString());
        
    	UserTransaction userTransaction = context.getUserTransaction();

    	SimpleDateFormat sdfFull = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        
    	Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date now = calendar.getTime();
        calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
        Date firstDayOfMonth = calendar.getTime();
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 0);
        Date lastDayOfMonth = calendar.getTime();
        
        System.out.println("Data attuale                      :"+ sdfFull.format(now));
        System.out.println("Primo giorno del mese attuale     :"+ sdfFull.format(firstDayOfMonth));
        System.out.println("Ultimo giorno del mese attuale    :"+ sdfFull.format(lastDayOfMonth));

        try {

            userTransaction.begin();
            
            //List<EsPromotionBean> listPromo = QueryRepository.findEsPromotionForCreateVoucher(em, null, firstDayOfMonth, lastDayOfMonth);
            List<EsPromotionBean> listPromo = QueryRepository.findEsPromotionToBeProcessed(em);
            
            System.out.println("Numero utenti da elaborare: " + listPromo.size());
            System.out.println();
            
            for(EsPromotionBean promo : listPromo) {
                
            	//per ogni promo se l'utente ha rifornimenti>=100 euro di rifornimenti nel mese 
            	//creare il voucher
                //inviare notifica push
                
                System.out.println("Elaborazione cf: " + promo.getFiscalCode());
                
                if ( promo.getUserBean() == null) {
                    System.out.println("UserBean null");
                    continue;
                }
            	
                UserBean userBean = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, promo.getFiscalCode());
                System.out.println("Calcolo ammontare transazioni. Inizio abilitazione promo: " + sdfFull.format(promo.getStartDate()) + " - termine abilitazione promo: " + sdfFull.format(promo.getEndDate()));
                
            	// calcolo rifornimenti da promo.getStartDate() a promo.getEndDate()
                Integer totalAmountPrepaid=QueryRepository.findAmountRefuelForPeriodPrepaid(em, userBean.getId(), promo.getStartDate(), promo.getEndDate());
                System.out.println("totalAmountPrepaid:  " + totalAmountPrepaid);
                
                Integer totalAmountPostpaid=QueryRepository.findAmountRefuelForPeriodPostpaid(em, userBean.getId(), promo.getStartDate(), promo.getEndDate());
                System.out.println("totalAmountPostpaid: " + totalAmountPostpaid);
                
                Integer totalAmount = Integer.valueOf(totalAmountPrepaid.intValue()+totalAmountPostpaid.intValue());
                System.out.println("totalAmount:         " + totalAmount);
                
                if(totalAmount.intValue() >= sogliaPromo.intValue()) {
                    
                	//crea voucher
                	System.out.println("Creazione voucher della promozione " + promoCode + " del valore " + amount + "�");
                	CreateVoucherPromotionalEs createVoucherPromotionalEs = new CreateVoucherPromotionalEs(em, fidelityService, userBean);
                	String createResponse = createVoucherPromotionalEs.create(promoCode, amount);
                	System.out.println("Esito chiamata servizio di creazione voucher: " + createResponse);
                	
                	String associateResponse = null;
                	if (createResponse.equals(ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_SUCCESS)) {
                	    
                		System.out.println("Assegnazione del voucher all'utente");
                        associateResponse = createVoucherPromotionalEs.associateToUser(createVoucherPromotionalEs.getCreateVoucherResult());
                        
                        System.out.println("Aggiornamento EsPromotionBean con il codice del voucher");
                        promo.setVoucherCode(createVoucherPromotionalEs.getCreateVoucherResult().getVoucher().getVoucherCode());
                        
                        System.out.println("Disattivazione flag toBeProcessed");
                        promo.setToBeProcessed(Boolean.FALSE);
                        
                        // Se l'utente ha accettato di ricevere comunicazioni da parte di Eni deve anche ricevere una notifica push e una dem
                        Boolean sendCommunications = Boolean.FALSE;
                        for(TermsOfServiceBean termsOfServiceBean : userBean.getPersonalDataBean().getTermsOfServiceBeanData()) {
                            if (termsOfServiceBean.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                                if (termsOfServiceBean.getValid() && termsOfServiceBean.getAccepted()) {
                                    sendCommunications = Boolean.TRUE;
                                }
                            }
                        }
                        
                        if (sendCommunications) {
                            
                        System.out.println("Invio notifica push");
                        	sendNotificationPush(promo, pushNotificationService, eventNotificationService, userBean, maxRetryAttemps, landingWinnerVodafone);
                    	
                    	System.out.println("Invio dem");
                    	sendDem(promo, userBean, emailSender);
                	}
                        else {
                	
                            System.out.println("Notifica e dem non inviate");
                }
                	}
                
                }
                
                promo.setToBeProcessed(Boolean.FALSE);
                
                em.persist(promo);
                
                System.out.println("----------------------------------------");
	
            }
            
            userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception e) {

            e.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
    
    private void sendNotificationPush(EsPromotionBean promo, PushNotificationServiceRemote pushNotificationService, EventNotificationServiceRemote eventNotificationService, UserBean userBean, Integer maxRetryAttemps, String landingWinnerVodafone){
    	
    	String arnEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();
    	PushNotificationContentType contentType = PushNotificationContentType.TEXT;
        PushNotificationStatusType statusCode = null;
        String statusMessage = null;
        String publishMessageID = null;
        Date sendingTimestamp = new Date();
        Long userID = userBean.getId();
        PushNotificationSourceType source = PushNotificationSourceType.CRM_ES;
        String notificationText = "Ecco il tuo voucher carburante da 5 �";
        String message = landingWinnerVodafone;
        String title = "Eni Station +";

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(sendingTimestamp);

        PushNotificationBean pushNotificationBean = new PushNotificationBean();
        pushNotificationBean.setTitle(title);
        pushNotificationBean.setSendingTimestamp(sendingTimestamp);
        pushNotificationBean.setExpiringTimestamp(calendar.getTime());
        pushNotificationBean.setSource(source);
        pushNotificationBean.setContentType(contentType);
        pushNotificationBean.setUser(userBean);
        pushNotificationBean.setStatusCode(PushNotificationStatusType.PENDING);
        pushNotificationBean.setRetryAttemptsLeft(maxRetryAttemps);
        
        em.persist(pushNotificationBean);
        
        promo.setPushNotificationBean(pushNotificationBean);
        em.persist(promo);

        PushNotificationMessage notificationMessage = new PushNotificationMessage();
        
        notificationMessage.setMessage(pushNotificationBean.getId(), notificationText, title);
    	
    	PushNotificationResult pushNotificationResult = pushNotificationService.publishMessage(arnEndpoint, notificationMessage, false, false);
    	
    	publishMessageID = pushNotificationResult.getMessageId();
        sendingTimestamp = pushNotificationResult.getRequestTimestamp();
        
        if (pushNotificationResult.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_SUCCESS)) {
            statusCode = PushNotificationStatusType.DELIVERED;
        }
        else if (pushNotificationResult.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_ARN_NOT_FOUND)) {
            statusCode = PushNotificationStatusType.ARN_NOT_FOUND;
        }
        else {
            statusCode = PushNotificationStatusType.ERROR;
        }
        
        pushNotificationBean.setStatusCode(statusCode);
        pushNotificationBean.setEndpoint(arnEndpoint);
        pushNotificationBean.setPublishMessageID(publishMessageID);
        pushNotificationBean.setSendingTimestamp(sendingTimestamp);
        pushNotificationBean.setMessage(message);

        pushNotificationBean.setStatusMessage(pushNotificationResult.getMessage());
        
        if (statusCode.equals(PushNotificationStatusType.ERROR)) {
            pushNotificationBean.setToReconcilie(true);
        }
        else {
            pushNotificationBean.setToReconcilie(false);
        }

        em.merge(pushNotificationBean);
    }
    
    
    private void sendDem(EsPromotionBean promo, UserBean userBean, EmailSenderRemote emailSender) {
        
        EmailType emailType = EmailType.WINNER_VODAFONE;
        
        String keyFrom = emailSender.getSender();
        String to      = userBean.getPersonalDataBean().getSecurityDataEmail();
        
        List<Parameter> parameters = new ArrayList<Parameter>(0);
        parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));

        System.out.println("Send email to " + to);

        String sendEmailResult = emailSender.sendEmail(emailType, keyFrom, to, parameters);

        System.out.println("SendEmailResult: " + sendEmailResult);
    }
}