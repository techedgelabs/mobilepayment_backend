package com.techedge.mp.core.actions.scheduler;

import java.awt.Color;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.techedge.mp.core.business.TransactionService;
import com.techedge.mp.core.business.interfaces.PendingTransactionRefuelResponse;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Transaction;
import com.techedge.mp.core.business.interfaces.TransactionStatus;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelSheetData;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobFinalizePendingTransactionAction {
    private static final String JOB_NAME = "JOB_FINALIZE_PENDING_TRANSACTION";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobFinalizePendingTransactionAction() {}

    public String execute(TransactionService transactionService, EmailSenderRemote emailSender, String reportRecipient) throws EJBException {

        Date now = new Date();

        System.out.println("job name: " + SchedulerJobFinalizePendingTransactionAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());
        
        try {

            PendingTransactionRefuelResponse pendingTransactionRefuelResponse = transactionService.finalizePendingRefuel();

            if (pendingTransactionRefuelResponse.getTransactionList().isEmpty()) {
                System.out.println("Nessuna transazione pending trovata. Invio email non necessario.");
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
            }
            
            //New Workbook
            ExcelWorkBook ewb = new ExcelWorkBook();
            
            //Cell style for header row
            ExcelCellStyle csHeader = ewb.createCellStyle(true, 12, Color.BLACK, new Color(254, 211, 0), null);
            //Cell style for table row
            ExcelCellStyle csTable = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null);

            String[] columnsHeading = new String[] { "Transazione MP", "Server", "Creazione", "Valore iniziale", "Stato Iniziale", "Utente", "Indirizzo PV" };

            ExcelSheetData excelData = ewb.createSheetData();
            
            for (Transaction transaction : pendingTransactionRefuelResponse.getTransactionList()) {

                //ArrayList<String> rowData = new ArrayList<String>(0);
                int rowIndex = excelData.createRow();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                String transactionCreation = sdf.format(transaction.getCreationTimestamp());
                String fullName = transaction.getUser().getPersonalData().getFirstName() + " "
                        + transaction.getUser().getPersonalData().getLastName();
                
                excelData.addRowData(transaction.getTransactionID(), rowIndex);
                excelData.addRowData(transaction.getServerName(), rowIndex);
                excelData.addRowData(transactionCreation, rowIndex);
                excelData.addRowData(transaction.getInitialAmount().toString(), rowIndex);
                String initialStatus = "";
                
                if (!transaction.getTransactionStatusData().isEmpty()) {

                    int member = 0;
                    int memberOffSet = (transaction.getTransactionStatusData().size() - 2);
                    
                    for (TransactionStatus transactionStatus : transaction.getTransactionStatusData()) {
                        if (member == memberOffSet) {
                            initialStatus = transactionStatus.getStatus().toString();
                        }
                        else
                        {
                            member++;
                        }
                    }
                }
                
                excelData.addRowData(initialStatus, rowIndex);
                excelData.addRowData(fullName, rowIndex);
                excelData.addRowData(transaction.getStation().getAddress(), rowIndex);
            }

            ewb.addSheet("Report Transazioni Interrotte", columnsHeading, excelData, csHeader, csTable);

             // Crea l'email con il report allegato

            SimpleDateFormat sdf = new SimpleDateFormat();
            String intervalBegin = sdf.format(now);
            String intervalEnd = intervalBegin;
            sdf.applyPattern("dd/MM/yy HH:mm:ss");

            EmailType emailType = EmailType.PENDING_TRANSACTION_REPORT_V2;
            List<Parameter> parameters = new ArrayList<Parameter>(0);
            Integer rowsCount = (excelData.getSize());

            parameters.add(new Parameter("COUNT", rowsCount.toString()));
            parameters.add(new Parameter("INTERVAL_BEGIN", intervalBegin));
            parameters.add(new Parameter("INTERVAL_END", intervalEnd));

            System.out.println("Invio email");

            List<Attachment> attachments = new ArrayList<Attachment>(0);

            // L'allegato deve essere inviato solo se � stato consumato almeno un voucher
            if (rowsCount > 0) {
                Attachment attachment = new Attachment();
                attachment.setFileName("pending_transaction_report.xlsx");
                //attachment.setContent(attachmentContent);
                byte[] bytes = ewb.getBytesToStream();
                attachment.setBytes(bytes);
                attachments.add(attachment);
            }

            System.out.println("Sending email to: " + reportRecipient);
            String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, reportRecipient, parameters, attachments);    
            System.out.println("SendEmailResult: " + sendEmailResult);
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
}
