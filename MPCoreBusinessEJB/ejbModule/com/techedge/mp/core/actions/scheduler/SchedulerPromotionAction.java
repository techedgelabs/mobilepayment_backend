package com.techedge.mp.core.actions.scheduler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PromoUsersOnHoldStatus;
import com.techedge.mp.core.business.interfaces.PromotionType;
import com.techedge.mp.core.business.interfaces.PromotionVoucherStatus;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PromoUsersOnHoldBean;
import com.techedge.mp.core.business.model.PromoVoucherBean;
import com.techedge.mp.core.business.model.PromotionBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerPromotionAction {

    private static final String JOB_NAME        = "JOB_PROMOTION";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService           loggerService;

    @EJB
    private UserService           userService;

    @SuppressWarnings("serial")
    private HashMap<String, String> voucherStatus    = new HashMap<String, String>() {
        {
            put(Voucher.VOUCHER_STATUS_ANNULLATO, ResponseHelper.USER_LOAD_VOUCHER_CANCELED);
            put(Voucher.VOUCHER_STATUS_CANCELLATO, ResponseHelper.USER_LOAD_VOUCHER_REMOVED);
            put(Voucher.VOUCHER_STATUS_DA_CONFERMARE, ResponseHelper.USER_LOAD_VOUCHER_TO_CONFIRM);
            put(Voucher.VOUCHER_STATUS_ESAURITO, ResponseHelper.USER_LOAD_VOUCHER_SPENT);
            put(Voucher.VOUCHER_STATUS_INESISTENTE, ResponseHelper.USER_LOAD_VOUCHER_INEXISTENT);
            put(Voucher.VOUCHER_STATUS_SCADUTO, ResponseHelper.USER_LOAD_VOUCHER_EXPIRED);
            put(Voucher.VOUCHER_STATUS_VALIDO, ResponseHelper.USER_LOAD_VOUCHER_VALID);
        }
    };
    
    public SchedulerPromotionAction() {}

    public String execute(EmailSenderRemote emailSender, Date searchRange, String promoLandingUrl) throws EJBException {

        Date now = new Date();

        System.out.println("job name: " + SchedulerPromotionAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());
     
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        System.out.println("Periodo di ricerca: " + sdf.format(searchRange));
        
        UserTransaction userTransaction = context.getUserTransaction();

        try {
      
            userTransaction.begin();
            
            System.out.println("Ricerca di utenti in attesa di un voucher....");
            List <PromoUsersOnHoldBean> promoUsersOnHoldBeanList = QueryRepository.findPromoUsersOnHold(em);
            List<PromoVoucherBean> promoVoucherBeanList = QueryRepository.findPromoVoucherNotAssigned(em);
            List<PromoVoucherBean> promoVoucherBeanListUsed = new ArrayList<PromoVoucherBean>(); 
            
            if (promoUsersOnHoldBeanList == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Nessuna utente in attesa di voucher");
            }
            else if (promoVoucherBeanList == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Nessuna voucher da assegnare agli utenti in attesa");
            }
            else {
                for (PromoUsersOnHoldBean promoUsersOnHoldBean : promoUsersOnHoldBeanList) {
                    UserBean userBean = promoUsersOnHoldBean.getUserBean();
                    PromotionBean promotionBean = promoUsersOnHoldBean.getPromotionBean();
                    
                    for (PromoVoucherBean promoVoucherBean : promoVoucherBeanList) {
                        
                        if (promoVoucherBeanListUsed.contains(promoVoucherBean)) {
                            continue;
                        }
                        
                        if (promoVoucherBean.getPromotionBean().equals(promotionBean)) {
                            String verificationValue = null;
                            String voucherCode = promoVoucherBean.getCode();
                            
                            // Modifica controllo per assegnazione voucher
                            /*
                            for (PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                                if (paymentInfoBean.getDefaultMethod()) {
                                    verificationValue = paymentInfoBean.getToken();
                                    break;
                                }
                            }
                            if (verificationValue == null) {
                                //System.out.println("VerificationValue nullo per l'utente: " + userBean.getId());
                            }
                            */
                            
                            Boolean promoVoucherFound = false;
                            
                            /* Disattivazione controllo su preesistenza voucher
                            for(VoucherBean voucherBean : userBean.getVoucherList()) {
                                
                                //System.out.println("Codice promozione: " + promotionBean.getCode());
                                //System.out.println("Codice promo voucher: " + voucherBean.getPromoCode());
                                
                                if ( promotionBean.getCode().equals(PromotionType.WELCOME.getPromoCode()) && 
                                        voucherBean.getPromoCode().equals(PromotionType.WELCOME.getVoucherCode())) {
                                    
                                    System.out.println("L'utente ha gi� usufruito della promozione di benvenuto");
                                    promoVoucherFound = true;
                                    break;
                                }
                                
                                if ( promotionBean.getCode().equals(PromotionType.MASTERCARD.getPromoCode()) && 
                                        voucherBean.getPromoCode().equals(PromotionType.MASTERCARD.getVoucherCode())) {
                                    
                                    System.out.println("L'utente ha gi� usufruito della promozione Mastercard");
                                    promoVoucherFound = true;
                                    break;
                                }
                            }
                            */
                            
                            //System.out.println("promoVoucherFound: " + promoVoucherFound);
                            
                            // Assegna il voucher se l'utente ha un pin e ha completato la registrazione
                            PaymentInfoBean voucherPaymentInfoBean = userBean.getVoucherPaymentMethod();
                            
                            if ( promoVoucherFound || voucherPaymentInfoBean == null || !userBean.getUserStatusRegistrationCompleted() ) {
                                System.out.println("L'utente " + userBean.getId() + " non si trova in uno stato valido per ricevere il voucher di benvenuto");
                                promoUsersOnHoldBean.setStatus(PromoUsersOnHoldStatus.INVALID.getValue());
                                em.merge(promoUsersOnHoldBean);
                            }
                            else {
                                String result = userService.loadPromoVoucher(voucherCode, userBean.getId(), verificationValue);
                                if (result.equals(ResponseHelper.USER_LOAD_VOUCHER_SUCCESS)) {
                                    promoUsersOnHoldBean.setStatus(PromoUsersOnHoldStatus.ASSIGNED.getValue());
                                    em.merge(promoUsersOnHoldBean);
                                    promoVoucherBeanListUsed.add(promoVoucherBean);
                                    System.out.println("Promo voucher " + voucherCode + " assegnato all'utente " + userBean.getId());
                                    break;
                                }
                                else if (!result.equals(ResponseHelper.USER_LOAD_VOUCHER_TO_RETRY)) {
                                    System.out.println("Errore nell'assegnazione del voucher promozione all'utente (" + result + ")");
                                    
                                    if (voucherStatus.containsValue(result)) {
                                        promoVoucherBeanListUsed.add(promoVoucherBean);
                                        System.out.println("Impostato stato INVALID al voucher promozionale " + voucherCode);
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
            
            /* Disattivazione dell'invio dell'email a seguito dell'assegnazione del voucher
            userTransaction.commit();
            userTransaction.begin();
            
            System.out.println("Ricerca di utenti con promo voucher assegnati a cui inviare l'email....");

            //promoVoucherBeanList = QueryRepository.findPromoVoucherByDateAndStatusConsumed(em, searchRange);
            promoVoucherBeanList = QueryRepository.findPromoVoucherByStatusConsumed(em);
            
            if (promoVoucherBeanList == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Nessuna email per promozione da inviare");
                userTransaction.commit();
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
            }
            
            for (PromoVoucherBean promoVoucherBean : promoVoucherBeanList) {
                UserBean userBean = promoVoucherBean.getUserBean();
                PromotionBean promotionBean = promoVoucherBean.getPromotionBean();
                EmailType emailType = null;
                String emailSubject = null;
                
                if (promotionBean.getCode().equals(PromotionType.WELCOME.getPromoCode())) {
                    emailType = EmailType.PROMOTION_WELCOME;
                    emailSubject = userBean.getPersonalDataBean().getFirstName() + ", 5 euro di voucher carburante per te";
                }
                else if (promotionBean.getCode().equals(PromotionType.MASTERCARD.getPromoCode())) {
                    emailType = EmailType.PROMOTION_MASTERCARD;
                    emailSubject = userBean.getPersonalDataBean().getFirstName() + ", 5 euro di voucher carburante offerti da MasterCard";
                }
                else {
                    System.err.println("Qualcosa non va... template email non trovato in base al codice promozione " + promotionBean.getCode());
                    continue;
                }
    
                if (userBean.getPersonalDataBean() != null && userBean.getPersonalDataBean().getSecurityDataEmail() != null) {
                    String emailTo = userBean.getPersonalDataBean().getSecurityDataEmail();
                    
                    System.out.println("Sending promotion email " + promotionBean.getCode() + " to " + emailTo);
                    
                    List<Parameter> parameters = new ArrayList<Parameter>(0);
                    parameters.add(new Parameter("PROMO_URL", promoLandingUrl));

                    
                    //String result = emailSender.sendEmail(emailType, emailTo, parameters);
                    String result = emailSender.sendEmail(emailType, emailTo, null, null, emailSubject, parameters);
                    
                    if (result.equals("SEND_MAIL_200")) {
                        promoVoucherBean.setStatus(PromotionVoucherStatus.SENT.getValue());
                        em.merge(promoVoucherBean);
                    }
                }
            }
            */

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");
            
            userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
}
