package com.techedge.mp.core.actions.scheduler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VerificationCodeBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobUserNewResendEmailAction {

    private static final String jobName = "JOB_USER_NEW_RESEND_EMAIL";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobUserNewResendEmailAction() {}

    public String execute(Integer userResendEmailTimeLimit, String activationLink, EmailSenderRemote emailSender, UserCategoryService userCategoryService, 
            StringSubstitution stringSubstitution) throws EJBException {

        Date now = new Date();

        System.out.println("job name: " + SchedulerJobUserNewResendEmailAction.jobName);

        System.out.println("job creationTimestamp: " + now.toString());

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Recupera gli utenti in stato NEW creati da un tempo superiore a userCreationTimeLimit

            List<UserBean> userBeanList = QueryRepository.findUserByStatus(em, User.USER_STATUS_NEW);

            int userElab = 0;

            // Cicla sugli utenti estratti

            for (UserBean userBean : userBeanList) {

                System.out.println("utente: " + userBean.getPersonalDataBean().getSecurityDataEmail());

                System.out.println("now: " + now.toString());

                System.out.println("createDate: " + userBean.getCreateDate().toString());

                if ((now.getTime() - userBean.getCreateDate().getTime()) < (userResendEmailTimeLimit * 1000)) {

                    System.out.println("utente: " + userBean.getPersonalDataBean().getSecurityDataEmail() + " da elaborare");

                    // Reinvio email di verifica

                    System.out.println("reinvio email a " + userBean.getPersonalDataBean().getSecurityDataEmail());

                    Integer userType = userBean.getUserType();
                    Boolean useNewAcquirer = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
                    String keyFrom = emailSender.getSender();
                    
                    EmailType emailType = EmailType.CONFIRM_EMAIL;
                    
                    if (useNewAcquirer) {
                        emailType = EmailType.CONFIRM_EMAIL_V2;

                        if (stringSubstitution != null) {
                            keyFrom = stringSubstitution.getValue(keyFrom, 1);
                        }
                    }

                    String to = userBean.getPersonalDataBean().getSecurityDataEmail();
                    List<Parameter> parameters = new ArrayList<Parameter>(0);

                    VerificationCodeBean verificationCodeBean = QueryRepository.findVerificationCodeBeanByUserAndStatus(em, userBean, VerificationCodeBean.STATUS_NEW);

                    if (verificationCodeBean == null) {
                        // Codice di verifica non trovato
                        System.out.println("Verification code not found");
                    }
                    else {

                        parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
                        parameters.add(new Parameter("ACTIVATION_LINK", activationLink));
                        parameters.add(new Parameter("VERIFICATION_CODE", verificationCodeBean.getVerificationCodeId()));
                        parameters.add(new Parameter("EMAIL", userBean.getPersonalDataBean().getSecurityDataEmail()));

                        System.out.println("Invio email");

                        String sendEmailResult = emailSender.sendEmail(emailType, keyFrom, to, parameters);

                        System.out.println("SendEmailResult: " + sendEmailResult);

                        userElab++;
                    }
                }
            }

            userTransaction.commit();

            if (userElab == 0) {

                System.out.println("job nessun utente da elaborare");
            }
            else {

                System.out.println("job numero utenti elaborati: " + userElab);
            }

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
}
