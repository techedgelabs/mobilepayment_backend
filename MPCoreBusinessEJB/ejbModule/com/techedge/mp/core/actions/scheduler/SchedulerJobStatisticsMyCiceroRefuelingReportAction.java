package com.techedge.mp.core.actions.scheduler;

import java.awt.Color;
import java.io.Serializable;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.apache.poi.ss.usermodel.HorizontalAlignment;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelSheetData;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobStatisticsMyCiceroRefuelingReportAction {
    private static final String JOB_NAME = "JOB_STATISTICS_MYCICERO_REFUELING_REPORT";

    public static class CustomReportDTO implements Serializable

    {
        /**
         * 
         */
        private static final long serialVersionUID = 1L;
        private double            amount;
        private String            mpTransactionID;
        private double            fuelQuantity;
        private String            productDescription;
        private String            pvCode;
        private Date              creationTimestamp;

        public CustomReportDTO(String mpTransactionID, double amount, double fuelQuantity, String productDescription, String pvCode, Date creationTimestamp) {
            super();
            this.amount = amount;
            this.mpTransactionID = mpTransactionID;
            this.fuelQuantity = fuelQuantity;
            this.productDescription = productDescription;
            this.pvCode = pvCode;
            this.creationTimestamp = creationTimestamp;
        }

        public double getAmount() {
            return amount;
        }

        public void setAmount(double amount) {
            this.amount = amount;
        }

        public String getMpTransactionID() {
            return mpTransactionID;
        }

        public void setMpTransactionID(String mpTransactionID) {
            this.mpTransactionID = mpTransactionID;
        }

        public double getFuelQuantity() {
            return fuelQuantity;
        }

        public void setFuelQuantity(double fuelQuantity) {
            this.fuelQuantity = fuelQuantity;
        }

        public String getProductDescription() {
            return productDescription;
        }

        public void setProductDescription(String productDescription) {
            this.productDescription = productDescription;
        }

        public String getPvCode() {
            return pvCode;
        }

        public void setPvCode(String pvCode) {
            this.pvCode = pvCode;
        }

        public Date getCreationTimestamp() {
            return creationTimestamp;
        }

        public void setCreationTimestamp(Date creationTimestamp) {
            this.creationTimestamp = creationTimestamp;
        }

    }

    @Resource
    private EJBContext     context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager  em;

    //Cell style for header row
    //private ExcelCellStyle      csHeader;
    private ExcelCellStyle csHeaderBordered;
    //Cell style for table row
    private ExcelCellStyle csTableValue;
    private ExcelCellStyle csTableValueBold;
    private ExcelCellStyle csTableValueBorderedLeft;
    private ExcelCellStyle csTableValueBorderedRight;
    private ExcelCellStyle csTableLabelBoldBorderedLeft;
    private ExcelCellStyle csTableLabelIndentBorderedLeft;
    private ExcelCellStyle csTableValueBorderedBottom;
    private ExcelCellStyle csTableValueBorderedBottomRight;
    private ExcelCellStyle csTableLabelBoldBorderedBottomLeft;
    private ExcelCellStyle csTableLabelIndentBorderedBottomLeft;

    public SchedulerJobStatisticsMyCiceroRefuelingReportAction() {}

    public String execute(EmailSenderRemote emailSender, String reportRecipient, String reportRecipientBCC, String proxyHost, String proxyPort, String proxyNoHosts)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        Date now = new Date();

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");

        NumberFormat integerFormat = NumberFormat.getNumberInstance(Locale.ITALIAN);
        integerFormat.setRoundingMode(RoundingMode.HALF_UP);
        integerFormat.setMinimumFractionDigits(0);
        integerFormat.setMaximumFractionDigits(0);

        System.out.println("job name: " + SchedulerJobStatisticsMyCiceroRefuelingReportAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {

            userTransaction.begin();

            SimpleDateFormat sdfLong = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            SimpleDateFormat sdfShort = new SimpleDateFormat("dd/MM/yyyy");

            Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
            System.out.println("DATA ATTUALE:" + sdfLong.format(calendar.getTime()));

            calendar.add(Calendar.MONTH, -1);
            calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date dailyEndDate = calendar.getTime();

            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date dailyStartDate = calendar.getTime();

            System.out.println("DATA INIZIO RILEVAZIONE: " + sdfLong.format(dailyStartDate));
            System.out.println("DATA FINE RILEVAZIONE: " + sdfLong.format(dailyEndDate));

            List<CustomReportDTO> result = QueryRepository.statisticsReportMyCiceroRefuelingTransactions(em, dailyStartDate, dailyEndDate);
            System.out.println("query result : " + result.size());

            //New Workbook
            ExcelWorkBook ewb = new ExcelWorkBook();

            final String[] sheetDetailColumnsHeading = new String[] { "ID Transazione", "Importo", "Prodotto", "Litri erogati", "Data rifornimento", "Codice PV" };

            //csHeader = ewb.createCellStyle(true, 12, Color.BLACK, new Color(254, 211, 0), HorizontalAlignment.CENTER_SELECTION);
            csHeaderBordered = ewb.createCellStyle(true, 12, Color.BLACK, new Color(254, 211, 0), HorizontalAlignment.CENTER_SELECTION, -1, 1, 1, 1, 1, Color.BLACK);
            csTableValue = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null);
            csTableValueBold = ewb.createCellStyle(true, 14, Color.BLACK, new Color(255, 255, 255), null);
            csTableValueBorderedLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, -1, 1, Color.BLACK);
            csTableValueBorderedRight = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, 1, -1, -1, Color.BLACK);
            csTableLabelBoldBorderedLeft = ewb.createCellStyle(true, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, -1, 1, Color.BLACK);
            csTableLabelIndentBorderedLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, 1, -1, -1, -1, 1, Color.BLACK);
            csTableValueBorderedBottom = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, 1, -1, Color.BLACK);
            csTableValueBorderedBottomRight = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, 1, 1, -1, Color.BLACK);
            csTableLabelBoldBorderedBottomLeft = ewb.createCellStyle(true, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, 1, 1, Color.BLACK);
            csTableLabelIndentBorderedBottomLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, 1, -1, -1, 1, 1, Color.BLACK);

            ExcelSheetData excelData = ewb.createSheetData();

            System.out.println("Creazione lista transazioni postpaid relative ad utenti MyCicero");

            for (CustomReportDTO element : result) {

                String transactionID = element.getMpTransactionID();
                String amount = String.valueOf(element.getAmount());
                String quantity = String.valueOf(element.getFuelQuantity());
                String productDescription = element.getProductDescription();
                String pvCode = element.getPvCode();
                Date creationTimestamp = element.getCreationTimestamp();
                
                String creationTimestampString = "";
                if (creationTimestamp != null) {
                    creationTimestampString = sdfLong.format(creationTimestamp);
                }

                int rowIndex = excelData.createRow();

                excelData.addRowData(transactionID, rowIndex);
                excelData.addRowData(amount, rowIndex);
                excelData.addRowData(productDescription, rowIndex);
                excelData.addRowData(quantity, rowIndex);
                excelData.addRowData(creationTimestampString, rowIndex);
                excelData.addRowData(pvCode, rowIndex);

            }

            System.out.println("Fine preparazione dati per generazione allegato excel");

            System.out.println("Esecuzione commit");

            userTransaction.commit();

            System.out.println("Commit eseguita");

            System.out.println("Creazione workbook");

            System.out.println("Creazione foglio 'Report MyCicero Refueling'");

            ewb.addSheet("Report MyCicero Refueling", sheetDetailColumnsHeading, excelData, csHeaderBordered, csTableValue);

            System.out.println("Invio email");

            EmailType emailType = EmailType.MONTHLY_MYCICERO_REFUELING_REPORT;
            List<Parameter> parameters = new ArrayList<Parameter>(0);
            parameters.add(new Parameter("DATE_START", sdfShort.format(dailyStartDate)));
            parameters.add(new Parameter("DATE_END", sdfShort.format(dailyEndDate)));

            List<Attachment> attachments = new ArrayList<Attachment>(0);

            Attachment attachment = new Attachment();
            attachment.setFileName("report_mycicero_refueling.xlsx");
            //attachment.setContent(attachmentContent);
            byte[] bytes = ewb.getBytesToStream();
            attachment.setBytes(bytes);
            attachments.add(attachment);

            //reportRecipient = "giovanni.dorazio@techedgegroup.com, luca.mancini@techedgegroup.com, alessandro.menale@techedgegroup.com, giovanni.apuzzo@techedgegroup.com";
            //reportRecipient = "giovanni.dorazio@techedgegroup.com, luca.mancini@techedgegroup.com";
            //reportRecipient = "luca.mancini@techedgegroup.com";

            if (reportRecipient == null || reportRecipient.equals("")) {

                // Recipient empty
                System.out.println("SendEmailResult: not sent - recipient empty");
            }
            else {

                // Sending email
                System.out.println("Sending email to: " + reportRecipient);
                String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, reportRecipient, parameters, attachments);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }

            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");

            //userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }

    }

}