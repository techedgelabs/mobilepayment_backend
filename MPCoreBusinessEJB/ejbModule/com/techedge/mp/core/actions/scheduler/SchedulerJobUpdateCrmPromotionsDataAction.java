package com.techedge.mp.core.actions.scheduler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.DuplicationPolicy;
import com.techedge.mp.core.business.interfaces.crm.GetOfferRequest;
import com.techedge.mp.core.business.interfaces.crm.Offer;
import com.techedge.mp.core.business.interfaces.crm.Response;
import com.techedge.mp.core.business.interfaces.crm.StatusCode;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.model.crm.CRMPromotionBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobUpdateCrmPromotionsDataAction {
    private static final String JOB_NAME = "JOB_UPDATE_CRM_PROMOTIONS_DATA";

    @EJB
    private LoggerService       loggerService;

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobUpdateCrmPromotionsDataAction() {}

    public String execute(String requestId, Long requestTimestamp, String interactionPoint, CRMAdapterServiceRemote crmAdapterService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        Date now = new Date();

        System.out.println("job name: " + SchedulerJobUpdateCrmPromotionsDataAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {
            
            userTransaction.begin();
            
            System.out.println("Job SchedulerJobUpdateCrmPromotionsDataAction ...");

            String sessionID = new IdGenerator().generateId(16).substring(0, 32);
            String audienceID = "888";
            String fiscalCode = "888";

            Response responseOffers = null;

            UserProfile userProfile = UserProfile.getUserProfileForEventPromotions(fiscalCode);

            //CRMEventBean crmEventBean = CRMEventBean.createEvent(sessionID, interactionPoint, sourceID, sourceType, userBean, maxRetryAttemps);

            //List<Response> responseBatch = crmAdapterService.executeBatchGetOffers(sessionID, fiscalCode, audienceID, interactionPoint, userProfile.getParameters());
            
            ArrayList<GetOfferRequest> requests = new ArrayList<GetOfferRequest>();
            
            GetOfferRequest request = new GetOfferRequest();
            request.setDuplicationPolicy(DuplicationPolicy.NO_DUPLICATION);
            request.setInteractionPoint(interactionPoint);
            request.setNumberRequested(100);
            requests.add(request);
            
            List<Response> responseBatch = crmAdapterService.executeBatchGetOffersForMultipleInteractionPoints(sessionID, fiscalCode, audienceID, requests, userProfile.getParameters());
            
            Response responseStartSession = responseBatch.get(0);

            //CRMEventOperationBean operationStartSessionBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.START_SESSION, responseStartSession);

            //em.persist(operationStartSessionBean);

            if (!responseStartSession.getStatusCode().equals(StatusCode.SUCCESS)) {

                String message = "";

                if (responseStartSession.getAdvisoryMessages().size() > 0) {
                    message = responseStartSession.getAdvisoryMessages().get(0).getMessage() + " (" + responseStartSession.getAdvisoryMessages().get(0).getDetailMessage() + ")";
                }

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error in InteractAPI(executeBatchGetOffers): " + message);

                if (responseStartSession.getStatusCode().equals(StatusCode.ERROR)) {

                    // Errore StartSession
                    System.out.println("job endTimestamp: " + now.toString());
                    System.out.println("job result: " + "KO");

                    return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
                }
            }

            responseOffers = responseBatch.get(1);

            //CRMEventOperationBean operationGetOffersBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.GET_OFFERS, responseOffers);

            //if (responseGetOffers.getStatusCode().equals(StatusCode.ERROR)
            //        && (responseGetOffers.getOfferList() == null || responseGetOffers.getOfferList().getRecommendedOffers().size() == 0)) {
            if (!responseOffers.getStatusCode().equals(StatusCode.SUCCESS)) {
                String message = "";

                if (responseOffers.getAdvisoryMessages().size() > 0) {
                    message = responseOffers.getAdvisoryMessages().get(0).getMessage() + " (" + responseOffers.getAdvisoryMessages().get(0).getDetailMessage() + ")";
                }

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error in InteractAPI(executeBatchGetOffers): " + message);

                if (responseOffers.getStatusCode().equals(StatusCode.ERROR)) {

                    // Errore GetOffers
                    System.out.println("job endTimestamp: " + now.toString());
                    System.out.println("job result: " + "KO");

                    return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
                }
            }

            //List<CRMOfferBean> crmOffersList = CRMOfferBean.createOffers(crmEventBean, responseOffers, false);

            Response responseEndSession = responseBatch.get(2);

            //CRMEventOperationBean operationEndSessionBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.END_SESSION, responseEndSession);

            
            // Cancellazione delle promozioni precedenti
            
            List<CRMPromotionBean> crmPromotionBeanToRemoveList = QueryRepository.findAllCrmPromotions(em);
            if (crmPromotionBeanToRemoveList != null && !crmPromotionBeanToRemoveList.isEmpty()) {
                for (CRMPromotionBean crmPromotionBeanToRemove : crmPromotionBeanToRemoveList) {
                    em.remove(crmPromotionBeanToRemove);
                }
            }
            
            
            // Creazione delle nuove promozioni
            
            Integer promotionsCreated = 0;
            
            if (responseOffers.getOfferList() != null) {

                for (Offer offer : responseOffers.getOfferList().getRecommendedOffers()) {

                    CRMPromotionBean crmPromotionBean = new CRMPromotionBean();

                    crmPromotionBean.setDescription(offer.getDescription());
                    crmPromotionBean.setCode(offer.getOfferCodeToString(";"));
                    crmPromotionBean.setName(offer.getOfferName());
                    crmPromotionBean.setTreatmentCode(offer.getTreatmentCode());
                    
                    System.out.println("promotion: " + crmPromotionBean.getName());

                    HashMap<String, Object> attributes = offer.getAdditionalAttributes();

                    System.out.println("param list:");
                    
                    for (String key : offer.getAdditionalAttributes().keySet()) {

                        String paramKey = key;
                        Object value = attributes.get(key);

                        String valueToString = value != null ? value.toString() : "";

                        System.out.println("key: " + paramKey + " - value: " + valueToString);

                        if (valueToString.equals("n/a")) {
                            value = "";
                        }
                        if (key.matches("^C_[0-9]+$")) {
                            if (key.equals("C_2")) {
                                crmPromotionBean.setC_2(valueToString);
                            }
                            else if (key.equals("C_3")) {
                                crmPromotionBean.setC_3(valueToString);
                            }
                            else if (key.equals("C_4")) {
                                crmPromotionBean.setC_4(valueToString);
                            }
                            else if (key.equals("C_5")) {
                                crmPromotionBean.setC_5(valueToString);
                            }
                            else if (key.equals("C_6")) {
                                crmPromotionBean.setC_6(valueToString);
                            }
                            else if (key.equals("C_7")) {
                                crmPromotionBean.setC_7(valueToString);
                            }
                            else if (key.equals("C_8")) {
                                crmPromotionBean.setC_8(valueToString);
                            }
                            else if (key.equals("C_9")) {
                                crmPromotionBean.setC_9(valueToString);
                            }
                            else if (key.equals("C_10")) {
                                crmPromotionBean.setC_10(valueToString);
                            }
                        }
                        else if (key.equals("UrlApp") && !valueToString.isEmpty()) {
                            crmPromotionBean.setImageUrl(valueToString);
                        }
                        else if (key.equals("TextAPP") && !valueToString.isEmpty()) {
                            crmPromotionBean.setLink(valueToString);
                        }
                        else if (key.equals("EffectiveDate") && !valueToString.isEmpty()) {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                            if (value.getClass().equals(Date.class)) {
                                crmPromotionBean.setStartDate((Date) value);
                            }
                            else {
                                try {
                                    crmPromotionBean.setStartDate((sdf.parse((String) value)));
                                }
                                catch (ParseException e) {
                                    System.err.println("Errore nel parsing della data: " + e.getMessage());
                                    crmPromotionBean.setStartDate(null);
                                }
                            }

                        }
                        else if (key.equals("ExpirationDate") && !valueToString.isEmpty()) {
                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                            if (value.getClass().equals(Date.class)) {
                                crmPromotionBean.setEndDate((Date) value);
                            }
                            else {
                                try {
                                    crmPromotionBean.setEndDate((sdf.parse((String) value)));
                                }
                                catch (ParseException e) {
                                    System.err.println("Errore nel parsing della data: " + e.getMessage());
                                    crmPromotionBean.setEndDate(null);
                                }
                            }

                        }
                    }

                    em.persist(crmPromotionBean);
                    
                    promotionsCreated++;
                }
            }

            System.out.println("Total promotions created: " + promotionsCreated);

            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");
            
            userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }

}