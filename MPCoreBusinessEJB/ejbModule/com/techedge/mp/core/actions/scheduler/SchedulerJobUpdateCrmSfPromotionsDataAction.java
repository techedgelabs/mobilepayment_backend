package com.techedge.mp.core.actions.scheduler;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.PromotionSF;
import com.techedge.mp.core.business.model.crmsf.CRMSfPromotionsBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobUpdateCrmSfPromotionsDataAction {
    private static final String JOB_NAME = "JOB_UPDATE_CRM_SF_PROMOTIONS_DATA";

    @EJB
    private LoggerService       loggerService;

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobUpdateCrmSfPromotionsDataAction() {}

    public String execute(String requestId, CRMAdapterServiceRemote crmAdapterService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        Date now = new Date();

        System.out.println("job name: " + SchedulerJobUpdateCrmSfPromotionsDataAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {
            
            userTransaction.begin();
            
            System.out.println("Job SchedulerJobUpdateCrmSfPromotionsDataAction ...");

            //String sessionID = new IdGenerator().generateId(16).substring(0, 32);
            
            // Cancellazione delle promozioni precedenti
            List<CRMSfPromotionsBean> crmSfPromotionsRemoveList = QueryRepository.findAllCrmSfPromotions(em);
            if(crmSfPromotionsRemoveList!=null && !crmSfPromotionsRemoveList.isEmpty()){
            	for(CRMSfPromotionsBean eleRemove:crmSfPromotionsRemoveList)
            		em.remove(eleRemove);
            }
            
            
            // Chiamare ws sf GetPromotions
            // parameter1, parameter2, parameter3, parameter4 parametri per implementazioni future
            List<PromotionSF> listPromoSf = crmAdapterService.getPromotionsSF(requestId, now, null, null, null, null);
            
            // riempire la tabella CRMSfPromotionsBean
            for(PromotionSF ele : listPromoSf){
            	CRMSfPromotionsBean promotion = new CRMSfPromotionsBean(ele.getOfferCode(), 
            			ele.getNome(), 
            			ele.getBannerId(), 
            			ele.getDataInizio(), 
            			ele.getDataFine(), 
            			ele.getUrl(), 
            			ele.getParameter1(), 
            			ele.getParameter2(), 
            			ele.getParameter3(), 
            			ele.getParameter4());
            	
            	em.persist(promotion);
            }
                    
            System.out.println("Total promotions created: ");

            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");
            
            userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }

}