package com.techedge.mp.core.actions.scheduler;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.TicketBean;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobSessionCleanerAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    private static final String JOB_NAME = "JOB_SESSION_CLEANER";

    public SchedulerJobSessionCleanerAction() {}

    public String execute(Date dateCheck, Date customerDateCheck) throws EJBException {
        
        Date now = new Date();

        System.out.println("job name: " + SchedulerJobSessionCleanerAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            
            // Cancellazione ticket di servizio
            String deleteTicketQueryString = "DELETE FROM TicketBean tb WHERE tb.type = " + TicketBean.TICKET_TYPE_SERVICE + " AND tb.expirationTimestamp <= :expirationTimestamp";
            
            Query deleteTicketQuery = em.createQuery(deleteTicketQueryString);
            deleteTicketQuery.setParameter("expirationTimestamp", dateCheck);
            int deleteTicketQueryStringResult = deleteTicketQuery.executeUpdate();
            
            System.out.println("Deleted " + deleteTicketQueryStringResult + " rows from table TICKETS (Service)");
            
            
            // Cancellazione ticket utente
            String deleteCustomerTicketQueryString = "DELETE FROM TicketBean tb WHERE tb.type = " + TicketBean.TICKET_TYPE_CUSTOMER + " AND tb.expirationTimestamp <= :expirationTimestamp";
            
            Query deleteCustomerTicketQuery = em.createQuery(deleteCustomerTicketQueryString);
            deleteCustomerTicketQuery.setParameter("expirationTimestamp", customerDateCheck);
            int deleteCustomerTicketQueryStringResult = deleteCustomerTicketQuery.executeUpdate();
            
            System.out.println("Deleted " + deleteCustomerTicketQueryStringResult + " rows from table TICKETS (Customer)");

            
            // Cancellazione ticket refueling
            String deleteRefuelingTicketQueryString = "DELETE FROM TicketBean tb WHERE tb.type = " + TicketBean.TICKET_TYPE_REFUELING + " AND tb.expirationTimestamp <= :expirationTimestamp";
            
            Query deleteRefuelingTicketQuery = em.createQuery(deleteRefuelingTicketQueryString);
            deleteRefuelingTicketQuery.setParameter("expirationTimestamp", dateCheck);
            int deleteRefuelingTicketQueryStringResult = deleteRefuelingTicketQuery.executeUpdate();
            
            System.out.println("Deleted " + deleteRefuelingTicketQueryStringResult + " rows from table TICKETS (Refueling)");
            
            
            // Cancellazione sessioni loyalty
            String deleteLoyaltySessionQueryString = "DELETE FROM LoyaltySessionBean lsb WHERE lsb.expirationTimestamp <= :expirationTimestamp";
            
            Query deleteLoyaltySessionQuery = em.createQuery(deleteLoyaltySessionQueryString);
            deleteLoyaltySessionQuery.setParameter("expirationTimestamp", dateCheck);
            int deleteLoyaltySessionQueryResult = deleteLoyaltySessionQuery.executeUpdate();
            
            System.out.println("Deleted " + deleteLoyaltySessionQueryResult + " rows from table LOYALTY_SESSION");

            
            System.out.println("/*****************************************************/");
            System.out.println("");
            System.out.println("    JOB CANCELLAZIONE SESSIONI:");
            System.out.println("");
            System.out.println("    TICKET UTENTE CANCELLATI: " + deleteCustomerTicketQueryStringResult);
            System.out.println("");
            System.out.println("    TICKET DI SERVIZIO CANCELLATI: " + deleteTicketQueryStringResult);
            System.out.println("");
            System.out.println("    TICKET REFUELING CANCELLATI: " + deleteRefuelingTicketQueryStringResult);
            System.out.println("");
            System.out.println("    SESSIONI LOYALTY CANCELLATE:   " + deleteLoyaltySessionQueryResult);
            System.out.println("");
            System.out.println("/*****************************************************/");
            
            
            userTransaction.commit();

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }

}
