package com.techedge.mp.core.actions.scheduler;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.payment.adapter.business.GPServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobRemoveOldPaymentAction {

    private static final String JOB_NAME = "JOB_REMOVE_OLD_PAYMENT";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobRemoveOldPaymentAction() {}

    public String execute(String shopLogin, String shopLoginNewAcquirer, Date dayToCompare, String encodedSecretKey, GPServiceRemote gpService, UserCategoryService userCategoryService) throws EJBException {

        Date now = new Date();

        System.out.println("job name: " + SchedulerJobRemoveOldPaymentAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();
            
            List<PaymentInfoBean> paymentInfoBeanToDelete = QueryRepository.findOldPaymentInfoBeanByUserBean(em, dayToCompare);
            
            if (paymentInfoBeanToDelete == null || paymentInfoBeanToDelete.isEmpty()) {
                
                System.out.println("Nessun metodo di pagamento non verificato da cancellare");

                userTransaction.commit();

                return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
            }
            
            Integer count = 0;
            
            for (PaymentInfoBean paymentInfoBean : paymentInfoBeanToDelete) {
                
                // Verifica che non ci siano transazioni pending associate al metodo di pagamento
                Boolean pendingTransactionFound = findPendingTransaction(paymentInfoBean);
                
                if ( pendingTransactionFound == true ) {
                    
                    System.out.println("metodo di pagamento " + paymentInfoBean.getId() + " non eliminato per transazione pending esistente");
                }
                else {
                    
                    // Se il token � associato a un altro metodo di pagamento in stato 1 o 2 la deleteToken non deve essere fatta
                    String deleteTokenResult = "SKIP";
                    
                    Long activePaymentMethodNumber = countActivePaymentMethods(paymentInfoBean.getToken());
                    
                    if ( activePaymentMethodNumber == 1 ) {
                    
                        Integer userType = paymentInfoBean.getUser().getUserType();
                        
                        String acquirerId = "BANCASELLA";
                        
                        Boolean useNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
                        if (useNewAcquirerFlow) {
                            shopLogin  = shopLoginNewAcquirer;
                            acquirerId = "CARTASI";
                        }
                        
                        // Cancellazione del token sul gestionale
                        deleteTokenResult = gpService.deleteToken(paymentInfoBean.getToken(), shopLogin, acquirerId, encodedSecretKey);
                    }
                    
                    
                    if ( deleteTokenResult.equals("KO") ) {
                        
                        System.out.println("metodo di pagamento " + paymentInfoBean.getId() + " non eliminato per errore deleteToken - result: " + deleteTokenResult);
                    }
                    else {
                        
                        paymentInfoBean.setStatus(5);
                        paymentInfoBean.setMessage("Payment method verification timeout");
                        paymentInfoBean.setToken("");
                        
                        em.persist(paymentInfoBean);
                        
                        System.out.println("eliminazione metodo di pagamento " + paymentInfoBean.getId() + " - deleteToken result: " + deleteTokenResult);
                        
                        count++;
                    }
                }
            }
            
            System.out.println("Totale metodi di pagamento non verificati eliminati: " + count);
            
            userTransaction.commit();

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
    
    
    private Boolean findPendingTransaction(PaymentInfoBean paymentInfoBean) {
        
        List<TransactionBean> transactionBeanList = QueryRepository.findTransactionsByPaymentMethodId(em, paymentInfoBean.getId());
        
        for (TransactionBean transactionBean : transactionBeanList) {
            
            if ( transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_SUCCESSFUL) ||
                 transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_FAILED) ) {
            }
            else {
                return true;
            }
        }
        
        List<PostPaidTransactionBean> postPaidTransactionBeanList = QueryRepository.findPostPaidTransactionsByPaymentMethodId(em, paymentInfoBean.getId());
        
        for (PostPaidTransactionBean postPaidTransactionBean : postPaidTransactionBeanList) {
            
            if ( postPaidTransactionBean.getToReconcile() == null || postPaidTransactionBean.getToReconcile() == false ) {
            }
            else {
                return true;
            }
        }
        
        return false;
    }
    
    
    private Long countActivePaymentMethods(String token) {
        
        Long activeTokenNumber = QueryRepository.countActiveToken(em, token);
        
        return activeTokenNumber;
    }
}