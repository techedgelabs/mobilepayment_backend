package com.techedge.mp.core.actions.scheduler;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VerificationCodeBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobUserNewClearAction {

    private static final String jobName = "JOB_USER_NEW_CLEAR";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobUserNewClearAction() {}

    public String execute(Integer userCreationTimeLimit) throws EJBException {

        Date now = new Date();

        System.out.println("job name: " + SchedulerJobUserNewClearAction.jobName);

        System.out.println("job creationTimestamp: " + now.toString());

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Recupera gli utenti in stato NEW creati da un tempo superiore a userCreationTimeLimit

            List<UserBean> userBeanList = QueryRepository.findUserByStatus(em, User.USER_STATUS_NEW);

            int userElab = 0;

            // Cicla sugli utenti estratti

            for (UserBean userBean : userBeanList) {

                System.out.println("utente: " + userBean.getPersonalDataBean().getSecurityDataEmail());

                System.out.println("now: " + now.toString());

                System.out.println("createDate: " + userBean.getCreateDate().toString());

                if ((now.getTime() - userBean.getCreateDate().getTime()) >= (userCreationTimeLimit * 1000)) {

                    System.out.println("utente: " + userBean.getPersonalDataBean().getSecurityDataEmail() + " da elaborare");

                    // Cancellazione dei VerificationCodeBean associati allo UserBean

                    List<VerificationCodeBean> verificationCodeBeanList = QueryRepository.findVerificationCodeBeanByUser(em, userBean);

                    for (VerificationCodeBean verificationCodeBean : verificationCodeBeanList) {

                        em.remove(verificationCodeBean);

                        System.out.println("eliminato VerificationCodeBean - id: " + verificationCodeBean.getId() + " code: " + verificationCodeBean.getVerificationCodeId());
                    }

                    // Cancellazione dei TicketBean associati allo UserBean

                    List<TicketBean> ticketBeanList = QueryRepository.findTicketByUser(em, userBean);

                    for (TicketBean ticketBean : ticketBeanList) {

                        em.remove(ticketBean);

                        System.out.println("eliminato TicketBean - id: " + ticketBean.getTicketId());
                    }

                    em.remove(userBean);

                    System.out.println("utente: " + userBean.getPersonalDataBean().getSecurityDataEmail() + " eliminato");

                    userElab++;
                }
            }

            userTransaction.commit();

            if (userElab == 0) {

                System.out.println("job nessun utente da elaborare");
            }
            else {

                System.out.println("job numero utenti elaborati: " + userElab);
            }

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            
            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
}
