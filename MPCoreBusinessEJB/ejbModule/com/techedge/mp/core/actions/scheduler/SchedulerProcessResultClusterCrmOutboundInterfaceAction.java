package com.techedge.mp.core.actions.scheduler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.EventNotificationService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.CrmOutboundProcessResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CrmDataElement;
import com.techedge.mp.core.business.interfaces.crm.CrmDataElementProcessingResult;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundProcessedStatusType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crm.CRMOutboundBean;
import com.techedge.mp.core.business.utilities.AbstractFTPService;
import com.techedge.mp.core.business.utilities.CrmDataProcessor;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationMessage;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.pushnotification.adapter.interfaces.StatusCode;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerProcessResultClusterCrmOutboundInterfaceAction {

    private static final String      JOB_NAME = "JOB_PROCESS_RESULT_CRM_OUTBOUND_INTERFACE";

    @Resource
    private EJBContext               context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager            em;

    @EJB
    private LoggerService            loggerService;

    @EJB
    private UserService              userService;

    @EJB
    private EventNotificationService eventNotificationService;

    public SchedulerProcessResultClusterCrmOutboundInterfaceAction() {}

    public CrmOutboundProcessResponse execute(String ftpsResultPath, String fileExtensionData, String fileExtensionOk, AbstractFTPService ftpService, PushNotificationServiceRemote pushNotificationService, 
            EmailSenderRemote emailSender, String summaryRecipient, Integer processResultDelay, Integer expiryTime, Integer maxRetryAttemps, 
            int maxDataPerThread, int maxThreads, String arnTopic) throws EJBException {
        
        UserTransaction userTransaction = context.getUserTransaction();

        Date now = new Date();

        System.out.println("job name: " + SchedulerProcessResultClusterCrmOutboundInterfaceAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        CrmOutboundProcessResponse crmOutboundProcessResponse = new CrmOutboundProcessResponse();

        try {
            userTransaction.begin();

            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_SUCCESS);
            crmOutboundProcessResponse.setStatusMessage("Esecuzione invio notifica push e creazione risultati terminato");

            int rowsCount = QueryRepository.getCRMOutboundRowsCount(em);

            if (rowsCount > 0) {
                
                int totalDataToProcess = QueryRepository.findCRMOutboundNotProcessedCount(em);
                
                if (totalDataToProcess == 0) {
                
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Sent push notification to topic: " + arnTopic);
                    
                    List<CRMOutboundBean> crmOutboundBeanList = QueryRepository.getAllCRMOutbound(em, true);
                    CRMOutboundBean crmOutboundBean = crmOutboundBeanList.get(0);
                    UserBean userBean = QueryRepository.getUserService(em);
                   
                    PushNotificationContentType contentType = PushNotificationContentType.TEXT;
                    PushNotificationStatusType statusCode = null;
                    String statusMessage = null;
                    String publishMessageID = null;
                    Date sendingTimestamp = new Date();
                    PushNotificationSourceType source = PushNotificationSourceType.CRM_OUTBOUND;
                    String message = crmOutboundBean.getMsg();
                    String title = "Eni Station +";
                    Long userID = userBean.getId();
           
                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(sendingTimestamp);
                    calendar.add(Calendar.SECOND, expiryTime);
            
                    PushNotificationBean pushNotificationBean = new PushNotificationBean();
                    pushNotificationBean.setTitle(title);
                    pushNotificationBean.setSendingTimestamp(sendingTimestamp);
                    pushNotificationBean.setExpiringTimestamp(calendar.getTime());
                    pushNotificationBean.setSource(source);
                    pushNotificationBean.setContentType(contentType);
                    pushNotificationBean.setUser(userBean);
                    pushNotificationBean.setRetryAttemptsLeft(maxRetryAttemps);
            
            
                    PushNotificationMessage notificationMessage = new PushNotificationMessage();
                    
                    String notificationText = "";
                    if (crmOutboundBean.getC5() != null && !crmOutboundBean.getC5().isEmpty()) {
                        notificationText = crmOutboundBean.getC5();
                    }
                    else {
                        notificationText = message;
                    }
                    
                    pushNotificationBean.setMessage(message);
                    
                    em.persist(pushNotificationBean);

                    notificationMessage.setMessage(pushNotificationBean.getId(), notificationText, title);

                    PushNotificationResult pushNotificationResult = pushNotificationService.publishMessage(arnTopic, notificationMessage, true, true);
                    
                    publishMessageID = pushNotificationResult.getMessageId();
                    sendingTimestamp = pushNotificationResult.getRequestTimestamp();
            
                    if (pushNotificationResult.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_SUCCESS)) {
                        statusCode = PushNotificationStatusType.DELIVERED;
                        statusMessage = "Messaggio pubblicato con successo";
                    }
                    else {
                        statusCode = PushNotificationStatusType.ERROR;
                        crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_ERROR_PUBLISH_MESSAGE);
                        crmOutboundProcessResponse.setStatusMessage("Errore nell'invio della notifica push");
                        statusMessage = pushNotificationResult.getMessage();
                    }
            
                    eventNotificationService.updatePushNotication(pushNotificationBean.getId(), publishMessageID, null, title, message, sendingTimestamp, source,
                            contentType, statusCode, statusMessage, userID);
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                            "Aggiornamento nella tabella CrmOutbound della notifica push...");
                    
                    Query query = em.createQuery("update CRMOutboundBean set pushNotificationBean = :pushNotificationBean, processed = :processed");
                    query.setParameter("pushNotificationBean", pushNotificationBean);
                    query.setParameter("processed", CrmOutboundProcessedStatusType.NOTIFICATION_SENT.getValue());
                    
                    int rows = query.executeUpdate();

                    if (rows <= 0) {
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Aggiornamento dei records presenti non andata a buon fine");
                    }

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Publish message to topic status: " + statusCode.toString() + " (" + publishMessageID + ")");
                }
                else {
                    
                    int processNotNotificationSentCount = QueryRepository.findCRMOutboundNotNotificationSentCount(em);
                    
                    if (processNotNotificationSentCount == 0) {
                        List<CRMOutboundBean> crmOutboundBeanList = QueryRepository.getAllCRMOutbound(em, true);
                        CRMOutboundBean crmOutboundBean = crmOutboundBeanList.get(0);
                        
                        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date processedDate = sdfDate.parse(crmOutboundBean.getTmsInserimento());
                        Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
                        calendar.setTime(processedDate);
                        calendar.set(Calendar.SECOND, processResultDelay);
                        
                        if (now.compareTo(calendar.getTime()) >= 0) {
                        
                            int threadCount = 1;
                            StringBuffer resultBuffer = new StringBuffer();
                            
                            System.out.println("Dati database CRM per generazione esiti: " + rowsCount);
            
                            if (rowsCount > maxDataPerThread) {
                                threadCount = rowsCount / maxDataPerThread;
                                
                                if (rowsCount % maxDataPerThread > 0) {
                                    threadCount++;
                                }
                            }
                            else {
                                maxDataPerThread = rowsCount;
                            }
            
                            ExecutorService executor = Executors.newFixedThreadPool(maxThreads);
            
                            for (int i = 0; i < threadCount; i++) {
                                
                                int start = i * maxDataPerThread;
                                int end = start + maxDataPerThread;
                                
                                if (end > rowsCount) {
                                    end = rowsCount;
                                }
                                
                                System.out.println("THREAD RESULT N " + (i + 1) + " indice inizio: " + start + "  indice fine: " + end);
                                
                                CRMDataElementProcessResult crmDataElementProcessResult = new CRMDataElementProcessResult(i + 1, em, start, end, resultBuffer);
                                
                                executor.submit(crmDataElementProcessResult);
                            }
                            
                            executor.shutdown();
            
                            while (!executor.isTerminated()) {}
            
                            System.out.println("THREADS RESULT TERMINATI");
                            
                            generateSFTPResult(resultBuffer.toString(), ftpsResultPath, fileExtensionData, fileExtensionOk, ftpService, emailSender, summaryRecipient);

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                                    "Eliminazione di tutti i records presenti nella tabella CrmOutbound...");
                            int rows = em.createQuery("delete from CRMOutboundBean").executeUpdate();
        
                            if (rows <= 0) {
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Eliminazione dei records presenti non andata a buon fine");
                            }
                        }
                        else {
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Data di processo risultati: " + sdfDate.format(calendar.getTime()));
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Data inserimento dati: " + sdfDate.format(processedDate));
                            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_RETRY);
                            crmOutboundProcessResponse.setStatusMessage("Esecuzione del job anticipata. "
                                    + "Data di processo risultati: " + sdfDate.format(calendar.getTime() + "   Data inserimento dati: " + sdfDate.format(processedDate)));
                        }
                    }
                    else {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Ci sono ancora records da processare prima dell'invio della notifica");
                    }
                }
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Tebella CRMOutbound vuota");
                crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_NO_DATA_TO_PROCESS);
                crmOutboundProcessResponse.setStatusMessage("Tebella CRMOutbound vuota");
            }

            userTransaction.commit();

            // Termina il job
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + crmOutboundProcessResponse.getStatusCode());

            return crmOutboundProcessResponse;

        }
        catch (Exception ex) {
            
            ex.printStackTrace();
            
            System.err.println("Errore nell'esecuzione del job: " + ex.getMessage());

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_FAILURE);
            crmOutboundProcessResponse.setStatusMessage("Errore nell'esecuzione del job di invio notifica push e creazione risultati: " + ex.getMessage());
            return crmOutboundProcessResponse;
        }
    }
    
    private class CRMDataElementProcessResult implements Runnable {
        
        private EntityManager em;
        private int threadID;
        private int start;
        private int end;
        StringBuffer resultBuffer;
        
        public CRMDataElementProcessResult(int threadID, EntityManager em, int start, int end, StringBuffer resultBuffer) {
            this.threadID = threadID;
            this.em = em;
            this.start = start;
            this.resultBuffer = resultBuffer;
        }

        @Override
        public void run() {
            
            try {

                InitialContext ctx = new InitialContext();
                UserTransaction userTransaction = 
                        (UserTransaction)ctx.lookup("java:jboss/UserTransaction");
                
                userTransaction.begin();

                List<CRMOutboundBean> crmOutboundBeanList = QueryRepository.getAllCRMOutbound(em, start, end);

                System.out.println("THREAD RESULT N " + threadID + " start");
                
                String result = generateResult(crmOutboundBeanList);
                
                if (result != null) {
                    resultBuffer.append(result);
                }
                
                userTransaction.commit();

                System.out.println("THREAD RESULT N " + threadID + " end");
            }
            catch (Exception ex) {
                System.err.println("THREAD RESULT N " + threadID + " errore: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
        
        private String generateResult(List<CRMOutboundBean> crmOutboundBeanList) {
            List<CrmDataElementProcessingResult> crmDataElementProcessingResultList = new ArrayList<CrmDataElementProcessingResult>();
            String categoryDesc = "APP";

            for (CRMOutboundBean crmOutboundBean : crmOutboundBeanList) {
                CrmDataElement crmDataElement = new CrmDataElement();
                crmDataElement.canale = crmOutboundBean.getCanale();
                crmDataElement.cdCarta = crmOutboundBean.getCdCarta();
                crmDataElement.cdCliente = crmOutboundBean.getCdCliente();
                crmDataElement.codiceFiscale = crmOutboundBean.getCodiceFiscale();
                crmDataElement.codIniziativa = crmOutboundBean.getCodIniziativa();
                crmDataElement.codOfferta = crmOutboundBean.getCodOfferta();
                crmDataElement.tmsInserimento = crmOutboundBean.getTmsInserimento();
                crmDataElement.treatmentCode = crmOutboundBean.getTreatmentCode();
                
                String deliveryId = "0";
                
                if (crmOutboundBean.getDeliveryId() != null) {
                    deliveryId = crmOutboundBean.getDeliveryId().getCode();
                }

                CrmDataElementProcessingResult crmDataElementProcessingResult = CrmDataProcessor.createCrmDataElementProcessingResult(crmDataElement, categoryDesc, deliveryId);
                crmDataElementProcessingResultList.add(crmDataElementProcessingResult);
            }

            // Genera il file con l'esito dell'elaborazione
            String output = CrmDataProcessor.generateOutput(crmDataElementProcessingResultList);
            return output;
        }
    }


    private String generateSFTPResult(String result, String ftpsResultPath, String fileExtensionData, String fileExtensionOk, 
            AbstractFTPService ftpService, EmailSenderRemote emailSender, String summaryRecipient) {

        // Scrivi il file con l'esito dell'elaborazione nella directory del server ftps ftpsResultPath
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd-HHmmss");
        String fileName = "EMM_ESITI_" + dateFormat.format(new Date());
        String outputFileName = ftpsResultPath + AbstractFTPService.separator + fileName + "." + fileExtensionData;

        Boolean putOutput = ftpService.putFile(result, outputFileName);

        if (!putOutput) {

            // Si � verificato un errore nella scrittura del file di output
            System.out.println("error creating file di output (" + outputFileName + ")");

            return ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_ERROR_SFTP_PUT_RESULT;
        }

        // Scrivi il file con l'ok dell'elaborazione nella directory del server ftps ftpsResultPath
        String resultFileName = ftpsResultPath + AbstractFTPService.separator + fileName + "." + fileExtensionOk;

        Boolean putResult = ftpService.putFile("", resultFileName);

        if (!putResult) {

            // Si � verificato un errore nella scrittura del file di result
            System.out.println("error creating file di result (" + resultFileName + ")");

            return ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_ERROR_SFTP_PUT_RESULT;
        }

        System.out.println("Invio email");
        
        EmailType emailType = EmailType.CRM_OUTBOUND_SUMMARY;
        List<Parameter> parameters = new ArrayList<Parameter>(0);

        
        
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
        parameters.add(new Parameter("DATE", sdfDate.format(new Date())));
        List<Attachment> attachments = new ArrayList<Attachment>(0);

        Attachment attachment = new Attachment();
        attachment.setFileName(fileName + "." + fileExtensionData);
        attachment.setBytes(result.getBytes());
        attachments.add(attachment);

        if ( summaryRecipient == null || summaryRecipient.equals("") ) {
            
            // Recipient empty
            System.out.println("SendEmailResult: not sent - recipient empty");
        }
        else {
            
            // Sending email
            System.out.println("Sending email to: " + summaryRecipient);
            String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, summaryRecipient, parameters, attachments);
            System.out.println("SendEmailResult: " + sendEmailResult);
        }
        
        
        return ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_RESULT_CLUSTER_SUCCESS;
    }


}
