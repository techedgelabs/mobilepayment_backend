package com.techedge.mp.core.actions.scheduler;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelModeType;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.model.StatisticalReportsBean;
import com.techedge.mp.core.business.utilities.QueryRepositoryBusiness;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobPrepareDailyStatisticsReportBusinessAction {
    private static final String JOB_NAME = "JOB_DAILY_STATISTICS_REPORT_BUSINESS";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobPrepareDailyStatisticsReportBusinessAction() {}

    public String execute(EmailSenderRemote emailSender, String reportRecipient,String reportRecipientService, String reportLogoUrl, String proxyHost, String proxyPort, String proxyNoHosts) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        Date now = new Date();

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("#,##0.000");
        
        NumberFormat integerFormat = NumberFormat.getNumberInstance(Locale.ITALIAN);
        integerFormat.setRoundingMode(RoundingMode.HALF_UP);
        integerFormat.setMinimumFractionDigits(0);
        integerFormat.setMaximumFractionDigits(0);

        System.out.println("job name: " + SchedulerJobPrepareDailyStatisticsReportBusinessAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {

            userTransaction.begin();

            SimpleDateFormat sdfFull = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            SimpleDateFormat sdfIdRif = new SimpleDateFormat("yyyyMMdd");

            Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
            System.out.println("DATA ATTUALE:" + sdfFull.format(calendar.getTime()));
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date dailyEndDate = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date dailyStartDate = calendar.getTime();

            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.MONTH, Calendar.JANUARY);
            calendar.set(Calendar.YEAR, 2019);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date totalStartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 20);
            calendar.set(Calendar.MONTH, Calendar.JUNE);
            calendar.set(Calendar.YEAR, 2018);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date totalProgressiveStartDate = calendar.getTime();

            System.out.println("DATA GIORNALIERA INIZIO:        " + sdfFull.format(dailyStartDate));
            System.out.println("DATA GIORNALIERA FINE:          " + sdfFull.format(dailyEndDate));
            System.out.println("DATA TOTALI INIZIO:             " + sdfFull.format(totalStartDate));
            System.out.println("DATA TOTALI PROGRESSIVI INIZIO: " + sdfFull.format(totalProgressiveStartDate));
            
            //Elimino se esistono le precedenti estrazioni Business
            List<StatisticalReportsBean> listPrecedentiEstrazioni = QueryRepositoryBusiness.statisticsReportFindForType(em, sdfIdRif.format(dailyStartDate), "B");
            System.out.println("Precedenti estrazioni Business size:"+listPrecedentiEstrazioni.size());
            for(StatisticalReportsBean ele:listPrecedentiEstrazioni)
            	em.remove(ele);

            /*
             * Numero di utenti
             * */
            
            System.out.println("Numero di Utenti registrati");
            
            Long resultUsersCreditCard = QueryRepositoryBusiness.statisticsReportSynthesisAllUsersCreditCardBusiness(em, dailyEndDate, totalStartDate);

            /*
             * Numero di transazioni (comprese quelle archiviate) Pre-Paid
             */
             
            System.out.println("Numero di transazioni (comprese quelle archiviate) Pre-Paid");
            
            Long resultPrePaidTransactions = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsBusiness(em, dailyEndDate, totalStartDate);
            Long resultPrePaidTransactionsHistory = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsHistoryBusiness(em, dailyEndDate, totalStartDate);

            Long resultPrePaidTransactionsCurrentAndHistory = Long.valueOf(resultPrePaidTransactions.intValue()+resultPrePaidTransactionsHistory.intValue());
            
            /*
             * Litri erogati (comprese quelle archiviate) Pre-Paid
             */
            
            System.out.println("Litri erogati (comprese quelle archiviate) Pre-Paid");
            
            Double resultPrePaidTransactionsFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsFuelQuantityBusiness(em, dailyEndDate, totalStartDate);
            Double resultPrePaidTransactionsHistoryFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsHistoryFuelQuantityBusiness(em, dailyEndDate, totalStartDate);
            
            Double resultPrePaidTransactionsCurrentAndHistoryFuelQuantity = Double.valueOf(resultPrePaidTransactionsFuelQuantity.doubleValue()
            		+resultPrePaidTransactionsHistoryFuelQuantity.doubleValue());
            
            
            /*
             * Litri Diesel+ erogati (comprese quelle archiviate) Pre-Paid
             */
            String productDieselPiu = "Diesel+";
            
            System.out.println("Litri Diesel+ erogati (comprese quelle archiviate) Pre-Paid");
            
            Double resultPrePaidTransactionsFuelQuantityDieselPlus = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsFuelQuantityByProductBusiness(em, dailyEndDate,
                    totalStartDate, productDieselPiu);
            
            Double resultPrePaidTransactionsHistoryFuelQuantityDieselPlus = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsHistoryFuelQuantityByProductBusiness(em, dailyEndDate, 
                    totalStartDate, productDieselPiu);

            Double resultPrePaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu = Double.valueOf(resultPrePaidTransactionsFuelQuantityDieselPlus.doubleValue()
            		+resultPrePaidTransactionsHistoryFuelQuantityDieselPlus.doubleValue());
            
            
            /*
             *  Litri Diesel+ erogati (comprese quelle archiviate) Post-Paid
             */

            System.out.println("Litri Diesel+ erogati (comprese quelle archiviate) Post-Paid Self");
            
            Double resultPostPaidTransactionsFuelQuantityDieselPiu = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsFuelQuantityByProductBusiness(em, dailyEndDate,
                    totalStartDate, productDieselPiu);

            Double resultPostPaidTransactionsHistoryFuelQuantityDieselPiu = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantityByProductBusiness(em, dailyEndDate, 
            		totalStartDate, productDieselPiu);
            
            Double resultPostPaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu = Double.valueOf(resultPostPaidTransactionsFuelQuantityDieselPiu.doubleValue()
            		+resultPostPaidTransactionsHistoryFuelQuantityDieselPiu.doubleValue());
            
            /*
             * Totale litri Diesel+
             */
            System.out.println("Totale litri Diesel+");
            
            System.out.println("prepaid:           " + resultPrePaidTransactionsFuelQuantityDieselPlus);
            System.out.println("prepaid h:         " + resultPrePaidTransactionsHistoryFuelQuantityDieselPlus);
            System.out.println("postpaid:          " + resultPostPaidTransactionsFuelQuantityDieselPiu);
            System.out.println("postpaid h:        " + resultPostPaidTransactionsHistoryFuelQuantityDieselPiu);
            
            Double resultTransactionsTotalFuelQuantityDieselPiu = Double.valueOf(resultPrePaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.doubleValue()
            		+resultPostPaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.doubleValue());

            /*
             *  Numero di transazioni (comprese quelle archiviate) Post-Paid Self
             */

            System.out.println("Numero di transazioni (comprese quelle archiviate) Post-Paid Self");
            
            Long resultPostPaidTransactionsSelfServito = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsBusiness(em, dailyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());
            Long resultPostPaidTransactionsSelf = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsBusiness(em, dailyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), null);
            Long resultPostPaidTransactionsHistorySelfServito = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryBusiness(em, dailyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());
            Long resultPostPaidTransactionsHistorySelf = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryBusiness(em, dailyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), null);
            
            Long resultPostPaidTransactionsSelfServitoCurrentAndHistory = Long.valueOf(resultPostPaidTransactionsSelfServito.intValue()+resultPostPaidTransactionsHistorySelfServito.intValue());
            Long resultPostPaidTransactionsSelfCurrentAndHistory = Long.valueOf(resultPostPaidTransactionsSelf.intValue()+resultPostPaidTransactionsHistorySelf.intValue());

            
            /*
             *  Litri erogati (comprese quelle archiviate) Post-Paid Self
             */

            System.out.println("Litri erogati (comprese quelle archiviate) Post-Paid Self");
            
            //Double resultPostPaidTransactionsSelfServitoFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsFuelQuantityBusiness(em, dailyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());
            Double resultPostPaidTransactionsSelfFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsFuelQuantityBusiness(em, dailyEndDate, totalStartDate, PostPaidSourceType.SELF.getCode(), null);
            
            //Double resultPostPaidTransactionsHistorySelfServitoFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantityBusiness(em, dailyEndDate, 
            //        totalStartDate, PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());
            Double resultPostPaidTransactionsHistorySelfFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantityBusiness(em, dailyEndDate, 
                   totalStartDate, PostPaidSourceType.SELF.getCode(), null);
            
            //Double resultPostPaidTransactionsSelfServitoCurrentAndHistoryFuelQuantity = Double.valueOf(resultPostPaidTransactionsSelfServitoFuelQuantity.doubleValue()+resultPostPaidTransactionsHistorySelfServitoFuelQuantity.doubleValue());
            Double resultPostPaidTransactionsSelfCurrentAndHistoryFuelQuantity = Double.valueOf(resultPostPaidTransactionsSelfFuelQuantity.doubleValue()+resultPostPaidTransactionsHistorySelfFuelQuantity.doubleValue());

            /*
             *  Numero di transazioni (comprese quelle archiviate) Post-Paid Shop
             */
            
            
            System.out.println("Numero di transazioni (comprese quelle archiviate) Post-Paid Shop");
            
            Long resultPostPaidTransactionsShop = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsBusiness(em, dailyEndDate,
                     totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            Long resultPostPaidTransactionsHistoryShop = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            Long resultPostPaidTransactionsShopCurrentAndHistory = Long.valueOf(resultPostPaidTransactionsShop.intValue()+resultPostPaidTransactionsHistoryShop.intValue());
            
            
            /*
             *  Litri erogati (comprese quelle archiviate) Post-Paid Shop
             */
            
            
            System.out.println("Litri erogati (comprese quelle archiviate) Post-Paid Shop");
            
            Double resultPostPaidTransactionsShopFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsFuelQuantityBusiness(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            Double resultPostPaidTransactionsHistoryShopFuelQuantity = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantityBusiness(em, dailyEndDate, 
                   totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            Double resultPostPaidTransactionsShopCurrentAndHistoryFuelQuantity = Double.valueOf(resultPostPaidTransactionsShopFuelQuantity.doubleValue()+resultPostPaidTransactionsHistoryShopFuelQuantity.doubleValue());

            /*
             * Ammontare transazioni (comprese quelle archiviate) Pre-Paid
             */
            System.out.println("Ammontare transazioni (comprese quelle archiviate) Pre-Paid");
            
            Double resultPrePaidTotalAmount = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTotalAmountBusiness(em, dailyEndDate,totalStartDate);
            
            Double resultPrePaidTotalAmountHistory = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTotalAmountHistoryBusiness(em, dailyEndDate,totalStartDate);
            
            /*
             * Ammontare transazioni (comprese quelle archiviate) Post-Paid
             */
            System.out.println("Ammontare transazioni (comprese quelle archiviate) Post-Paid");
            
            Double resultPostPaidTotalAmount = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTotalAmountBusiness(em, dailyEndDate, totalStartDate);
            
            Double resultPostPaidTotalAmountHistory = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTotalAmountHistoryBusiness(em, dailyEndDate, totalStartDate);

            
            /*
             * Ammontare transazioni (comprese quelle archiviate) Pre-Paid e Post-Paid
             */
            System.out.println("Ammontare transazioni (comprese quelle archiviate) Pre-Paid e Post-Paid");
            
            Double resultTotalAmount = Double.valueOf(resultPrePaidTotalAmount
            		+resultPrePaidTotalAmountHistory
            		+resultPostPaidTotalAmount
            		+resultPostPaidTotalAmountHistory);

            /* Utenti con prima transazione di pagamento
            */
            
            System.out.println("Utenti con prima transazione di pagamento");
            
            Long resultDistinctUsersTransaction = QueryRepositoryBusiness.statisticsReportSynthesisAllDistinctUsersTransactionBusiness(em, dailyEndDate, totalProgressiveStartDate);
            
            Long resultStationsAppCount = QueryRepositoryBusiness.statisticsReportSynthesisAllStationsAppCountBusiness(em);
            
            /*
             * Totale numero transazioni di pagamento
             */
            
            System.out.println("Totale numero transazioni di pagamento");
            Long resultTransactionsTotalCount = Long.valueOf(resultPrePaidTransactionsCurrentAndHistory.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistory.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistory.intValue());
            
            /*
             * Totale litri erogati
             */
            System.out.println("Totale litri erogati");
            Double resultTransactionsTotalFuelQuantity = Double.valueOf(resultPrePaidTransactionsCurrentAndHistoryFuelQuantity.doubleValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryFuelQuantity.doubleValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryFuelQuantity.doubleValue());
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area NO
             */
           
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area NO");
            Long resultPrePaidTransactionsAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsAreaBusiness(em, dailyEndDate, 
            		totalStartDate, "Area NO");
            
            Long resultPrePaidTransactionsHistoryAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
            		totalStartDate, "Area NO");
            
            Long resultPostPaidTransactionsSelfAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em, dailyEndDate, 
            		totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NO");
            
            Long resultPostPaidTransactionsHistorySelfAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
                     totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NO");
            
            Long resultPostPaidTransactionsShopAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NO");

            Long resultPostPaidTransactionsHistoryShopAreaNO = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryBusiness(em, dailyEndDate, 
                     totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NO");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaNO = Long.valueOf(resultPrePaidTransactionsAreaNO.intValue()+resultPrePaidTransactionsHistoryAreaNO.intValue());
            
            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaNO = Long.valueOf(resultPostPaidTransactionsSelfAreaNO.intValue()+resultPostPaidTransactionsHistorySelfAreaNO.intValue());
         
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaNO = Long.valueOf(resultPostPaidTransactionsShopAreaNO.intValue()+resultPostPaidTransactionsHistoryShopAreaNO.intValue());
             
            Long resultTransactionsTotalCountAreaNO = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaNO.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaNO.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaNO.intValue());
            
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area NE
             */
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area NE");
            Long resultPrePaidTransactionsAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsAreaBusiness(em, dailyEndDate, 
                    totalStartDate, "Area NE");
            
            Long resultPrePaidTransactionsHistoryAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
                    totalStartDate, "Area NE");
            
            Long resultPostPaidTransactionsSelfAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em, dailyEndDate,
                     totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NE");
            
            Long resultPostPaidTransactionsHistorySelfAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NE");
            
            Long resultPostPaidTransactionsShopAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NE");

            Long resultPostPaidTransactionsHistoryShopAreaNE = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NE");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaNE = Long.valueOf(resultPrePaidTransactionsAreaNE.intValue()+resultPrePaidTransactionsHistoryAreaNE.intValue());

            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaNE = Long.valueOf(resultPostPaidTransactionsSelfAreaNE.intValue()+resultPostPaidTransactionsHistorySelfAreaNE.intValue());           
            
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaNE = Long.valueOf(resultPostPaidTransactionsShopAreaNE.intValue()+resultPostPaidTransactionsHistoryShopAreaNE.intValue());
           
            Long resultTransactionsTotalCountAreaNE = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaNE.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaNE.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaNE.intValue());
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area CN
             */
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area CN");
            Long resultPrePaidTransactionsAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsAreaBusiness(em, dailyEndDate,
                    totalStartDate, "Area CN");
            
            Long resultPrePaidTransactionsHistoryAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
                    totalStartDate, "Area CN");
            
            Long resultPostPaidTransactionsSelfAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em,  dailyEndDate,
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CN");
            
            Long resultPostPaidTransactionsHistorySelfAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CN");
            
            Long resultPostPaidTransactionsShopAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CN");

            Long resultPostPaidTransactionsHistoryShopAreaCN = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CN");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaCN = Long.valueOf(resultPrePaidTransactionsAreaCN.intValue()+resultPrePaidTransactionsHistoryAreaCN.intValue());
            
            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaCN = Long.valueOf(resultPostPaidTransactionsSelfAreaCN.intValue()+resultPostPaidTransactionsHistorySelfAreaCN.intValue());
            
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaCN = Long.valueOf(resultPostPaidTransactionsShopAreaCN.intValue()+resultPostPaidTransactionsHistoryShopAreaCN.intValue());
            
            Long resultTransactionsTotalCountAreaCN = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaCN.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaCN.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaCN.intValue());
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area C
             */
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area C");
            Long resultPrePaidTransactionsAreaC = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsAreaBusiness(em, dailyEndDate, 
                    totalStartDate, "Area C");
            
            Long resultPrePaidTransactionsHistoryAreaC = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
                    totalStartDate, "Area C");
            
            Long resultPostPaidTransactionsSelfAreaC = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area C");
            
            Long resultPostPaidTransactionsHistorySelfAreaC = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area C");
            
            Long resultPostPaidTransactionsShopAreaC = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area C");

            Long resultPostPaidTransactionsHistoryShopAreaC = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area C");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaC = Long.valueOf(resultPrePaidTransactionsAreaC.intValue()+resultPrePaidTransactionsHistoryAreaC.intValue());

            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaC = Long.valueOf(resultPostPaidTransactionsSelfAreaC.intValue()+resultPostPaidTransactionsHistorySelfAreaC.intValue());
            
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaC = Long.valueOf(resultPostPaidTransactionsShopAreaC.intValue()+resultPostPaidTransactionsHistoryShopAreaC.intValue());
            
            Long resultTransactionsTotalCountAreaC = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaC.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaC.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaC.intValue());
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area CS
             */
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area CS");
            Long resultPrePaidTransactionsAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsAreaBusiness(em, dailyEndDate, 
                    totalStartDate, "Area CS");
            
            Long resultPrePaidTransactionsHistoryAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
                    totalStartDate, "Area CS");
            
            Long resultPostPaidTransactionsSelfAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CS");
            
            Long resultPostPaidTransactionsHistorySelfAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CS");
            
            Long resultPostPaidTransactionsShopAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CS");

            Long resultPostPaidTransactionsHistoryShopAreaCS = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CS");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaCS = Long.valueOf(resultPrePaidTransactionsAreaCS.intValue()+resultPrePaidTransactionsHistoryAreaCS.intValue());

            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaCS = Long.valueOf(resultPostPaidTransactionsSelfAreaCS.intValue()+resultPostPaidTransactionsHistorySelfAreaCS.intValue());
            
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaCS = Long.valueOf(resultPostPaidTransactionsShopAreaCS.intValue()+resultPostPaidTransactionsHistoryShopAreaCS.intValue());
            
            Long resultTransactionsTotalCountAreaCS = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaCS.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaCS.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaCS.intValue());
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area S
             */
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area S");
            Long resultPrePaidTransactionsAreaS = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsAreaBusiness(em, dailyEndDate,  
                    totalStartDate, "Area S");
            
            Long resultPrePaidTransactionsHistoryAreaS = QueryRepositoryBusiness.statisticsReportSynthesisAllPrePaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
                    totalStartDate, "Area S");
            
            Long resultPostPaidTransactionsSelfAreaS = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area S");
            
            Long resultPostPaidTransactionsHistorySelfAreaS = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryAreaBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area S");
            
            Long resultPostPaidTransactionsShopAreaS = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsAreaBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area S");

            Long resultPostPaidTransactionsHistoryShopAreaS = QueryRepositoryBusiness.statisticsReportSynthesisAllPostPaidTransactionsHistoryBusiness(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area S");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaS = Long.valueOf(resultPrePaidTransactionsAreaS.intValue()+resultPrePaidTransactionsHistoryAreaS.intValue());
            
            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaS = Long.valueOf(resultPostPaidTransactionsSelfAreaS.intValue()+resultPostPaidTransactionsHistorySelfAreaS.intValue());
            
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaS = Long.valueOf(resultPostPaidTransactionsShopAreaS.intValue()+resultPostPaidTransactionsHistoryShopAreaS.intValue());
             
            
            Long resultTransactionsTotalCountAreaS = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaS.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaS.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaS.intValue());
            
            //Totale utenti iscritti						resultUsersCreditCard
            //Utenti con prima transazione di pagamento		resultDistinctUsersTransaction
            //PV abilitati alla fatturazione elettronica    resultStationsAppCount
            //Totale numero transazioni di pagamento		resultTransactionsTotalCount
            //Totale importo transato �						resultTotalAmount
            //Totale litri erogati							resultTransactionsTotalFuelQuantity
            //Numero transazioni pi� servito				resultPostPaidTransactionsSelfServitoCurrentAndHistory
            //Area NO										resultTransactionsTotalCountAreaNO
            //Area NE										resultTransactionsTotalCountAreaNE
            //Area CN										resultTransactionsTotalCountAreaCN
            //Area C										resultTransactionsTotalCountAreaC
            //Area CS										resultTransactionsTotalCountAreaCS
            //Area S										resultTransactionsTotalCountAreaS
            //Totale transazioni payment					resultTransactionsTotalCount (2)
            
            Map<String, String> mapStatistical = new HashMap<>();

            mapStatistical.put("UsersCreditCard", resultUsersCreditCard.toString());
            mapStatistical.put("DistinctUsersTransaction", resultDistinctUsersTransaction.toString());
            mapStatistical.put("StationsAppCount", resultStationsAppCount.toString());
            mapStatistical.put("TransactionsTotalCount", resultTransactionsTotalCount.toString());
            mapStatistical.put("TotalAmount", resultTotalAmount.toString());
            mapStatistical.put("TransactionsTotalFuelQuantity", resultTransactionsTotalFuelQuantity.toString());
            mapStatistical.put("TransactionsTotalFuelQuantityDieselPiu", resultTransactionsTotalFuelQuantityDieselPiu.toString());
            mapStatistical.put("PostPaidTransactionsSelfServitoCurrentAndHistory", resultPostPaidTransactionsSelfServitoCurrentAndHistory.toString());
            mapStatistical.put("TransactionsTotalCountAreaNO", resultTransactionsTotalCountAreaNO.toString());
            mapStatistical.put("TransactionsTotalCountAreaNE", resultTransactionsTotalCountAreaNE.toString());
            mapStatistical.put("TransactionsTotalCountAreaCN", resultTransactionsTotalCountAreaCN.toString());
            mapStatistical.put("TransactionsTotalCountAreaC", resultTransactionsTotalCountAreaC.toString());
            mapStatistical.put("TransactionsTotalCountAreaCS", resultTransactionsTotalCountAreaCS.toString());
            mapStatistical.put("TransactionsTotalCountAreaS", resultTransactionsTotalCountAreaS.toString());
            
            
            for (Map.Entry<String,String> entry : mapStatistical.entrySet()) {
                String key = entry.getKey();
                String value = entry.getValue();
                
                StatisticalReportsBean statisticalReportsBean = new StatisticalReportsBean(sdfIdRif.format(dailyStartDate), dailyStartDate, Calendar.getInstance(Locale.ITALIAN).getTime(), "B", "T", key, value);
                em.persist(statisticalReportsBean);
            }
            
            userTransaction.commit();
            
            System.out.println("Commit eseguita");
            
            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");
            
            sendingServiceMail(emailSender, "Preparazione report giornaliero ", reportRecipientService, sdfIdRif.format(dailyStartDate), "Elaborazione terminata con successo.");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
    
    private void sendingServiceMail(EmailSenderRemote emailSender, String message, String reportRecipient, String dateExtraction, String detail){
    	
    	EmailType emailType = EmailType.ERROR_DAILY_STATISTICS_REPORT_V2;
    	
    	List<Parameter> parameters = new ArrayList<Parameter>(0);
    	parameters.add(new Parameter("MESSAGE", message));
    	parameters.add(new Parameter("TYPE", "BUSINESS"));
        parameters.add(new Parameter("DATE", dateExtraction));
        parameters.add(new Parameter("DETAIL", detail));
        
    	System.out.println("Sending email to: " + reportRecipient);
        String sendEmailResult = emailSender.sendEmail(emailType, reportRecipient, parameters);
        System.out.println("SendEmailResult: " + sendEmailResult);
    }
   
}