package com.techedge.mp.core.actions.scheduler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MailingList;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.MailingListBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerProcessMailingListAction {

    private static final String JOB_NAME        = "JOB_PROCESS_MAILING_LIST";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService           loggerService;

    @EJB
    private UserService           userService;
    
    public SchedulerProcessMailingListAction() {}

    public String execute(EmailSenderRemote emailSender, String emailSubject, Integer mailingListPacketSize) throws EJBException {

        Date now = new Date();

        System.out.println("job name: " + SchedulerProcessMailingListAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());
     
        System.out.println("Dimensione pacchetto da processare: " + mailingListPacketSize);
        
        UserTransaction userTransaction = context.getUserTransaction();

        try {
      
            userTransaction.begin();
            
            System.out.println("Ricerca elementi da elaborare....");
            
            List<MailingListBean> mailingListBeanList = QueryRepository.findMailingListByStatus(em, MailingList.MAILING_LIST_PENDING, mailingListPacketSize);
            
            if (mailingListBeanList == null || mailingListBeanList.isEmpty()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Nessun elemento in attesa di elaborazione");
            }
            else {
                
                for (MailingListBean mailingListBean : mailingListBeanList) {
                    
                    EmailType emailType = EmailType.NEW_APP_AVAILABLE;
                    
                    String emailTo = mailingListBean.getEmail();
                    String name    = mailingListBean.getName();
                    
                    System.out.println("Sending email to " + emailTo);
                    
                    List<Parameter> parameters = new ArrayList<Parameter>(0);
                    parameters.add(new Parameter("NAME", name));

                    String result = emailSender.sendEmail(emailType, null, emailTo, null, null, emailSubject, parameters);
                    
                    if (result.equals("SEND_MAIL_200")) {
                        
                        mailingListBean.setStatus(MailingList.MAILING_LIST_SENT);
                        mailingListBean.setTimestamp(new Date());
                        em.merge(mailingListBean);
                    }
                    else {
                        
                        mailingListBean.setStatus(MailingList.MAILING_LIST_ERROR);
                        mailingListBean.setTimestamp(new Date());
                        em.merge(mailingListBean);
                    }
                }
            }

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");
            
            userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
}
