package com.techedge.mp.core.actions.scheduler;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.dwh.DWHBrandDetailBean;
import com.techedge.mp.core.business.model.dwh.DWHCategoryBurnDetailBean;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.interfaces.BrandDetail;
import com.techedge.mp.dwh.adapter.interfaces.CategoryBurnDetail;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetBrandResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCatalogResult;
import com.techedge.mp.dwh.adapter.interfaces.DWHGetCategoryBurnResult;
import com.techedge.mp.dwh.adapter.utilities.StatusCode;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobUpdateDWHBurnDataAction {
    private static final String JOB_NAME = "JOB_UPDATE_DWH_BURN_DATA";

    @EJB
    private LoggerService       loggerService;

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobUpdateDWHBurnDataAction() {}

    public String execute(String site, String requestId, String card, Long requestTimestamp) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        Date now = new Date();

        System.out.println("job name: " + SchedulerJobUpdateDWHBurnDataAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {

            System.out.println("Job SchedulerJobUpdateDWHBurnDataAction ...");

            // I premi non sono pi� bufferizzati, ma sono letti dinamicamente quando l'utente lo richiede
            
            //List<AwardDetail> awardDetailList = new ArrayList<>();

            List<BrandDetail> brandDetailList = new ArrayList<BrandDetail>();

            List<CategoryBurnDetail> categoryBurnDetailList = new ArrayList<>();

            DWHAdapterServiceRemote dwhAdapterService = EJBHomeCache.getInstance().getDWHAdapterService();

            DWHGetCatalogResult resultDwhGetCatalogResult = dwhAdapterService.getCatalog(card, requestId, requestTimestamp);
            
            if(resultDwhGetCatalogResult == null || resultDwhGetCatalogResult.getStatusCode() == null || ! resultDwhGetCatalogResult.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS) ){
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Si � verificato un errore nel servizio getCatalog del DWHAdapterService");
               
                System.out.println("job endTimestamp: " + now.toString());
                System.out.println("job result: " + "OK");

                return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
            }

            DWHGetCategoryBurnResult resultBurnCategory = dwhAdapterService.getCategoryBurnList(requestId, requestTimestamp);
            
            if(resultBurnCategory == null || resultBurnCategory.getStatusCode() == null || ! resultBurnCategory.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS) ){
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Si � verificato un errore nel servizio getCategoryBurnList del DWHAdapterService");
               
                System.out.println("job endTimestamp: " + now.toString());
                System.out.println("job result: " + "OK");

                return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
            }

            DWHGetBrandResult resultBrandList = dwhAdapterService.getBrandList(requestId, requestTimestamp);
            
            if(resultBrandList == null || resultBrandList.getStatusCode() == null || ! resultBrandList.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS) ){
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Si � verificato un errore nel servizio getBrandList del DWHAdapterService");
               
                System.out.println("job endTimestamp: " + now.toString());
                System.out.println("job result: " + "OK");

                return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
            }

            //awardDetailList = resultDwhGetCatalogResult.getAwardDetailList();

            brandDetailList = resultBrandList.getBrandDetailList();

            categoryBurnDetailList = resultBurnCategory.getCategoryBurnList();
            
        
            userTransaction.begin();

            /* Rimozione del catalogo premi (awards) prima dell'aggiornamento */
            //List<DWHAwardDetailBean> awardDetailBeanToRemove = QueryRepository.findAllDwhAwardDetail(em);
            //if (awardDetailBeanToRemove != null && awardDetailBeanToRemove.size() > 0) {
            //    for (DWHAwardDetailBean dwhAwardDetailBean : awardDetailBeanToRemove) {
            //        em.remove(dwhAwardDetailBean);
            //    }
            //}

            /* Rimozione dei brand prima dell'aggiornamento */
            List<DWHBrandDetailBean> brandBeanToRemove = QueryRepository.findAllDwhBrandDetailBean(em);
            if (brandBeanToRemove != null && brandBeanToRemove.size() > 0) {
                for (DWHBrandDetailBean dwhBrandDetailBean : brandBeanToRemove) {
                    em.remove(dwhBrandDetailBean);
                }
            }

            /* Rimozione di category prima dell'aggiornamento */
            List<DWHCategoryBurnDetailBean> categoryBurnDetailBeanToRemove = QueryRepository.findAllDwhCategoryBurnDetailBean(em);
            if (categoryBurnDetailBeanToRemove != null && !categoryBurnDetailBeanToRemove.isEmpty()) {
                for (DWHCategoryBurnDetailBean dwhCategoryBurnDetailBean : categoryBurnDetailBeanToRemove) {
                    em.remove(dwhCategoryBurnDetailBean);
                }
            }

            /*
            for (AwardDetail awardDetail : awardDetailList) {

                DWHAwardDetailBean dwhAwardDetailBean = new DWHAwardDetailBean();
                dwhAwardDetailBean.setCreationTimestamp(now);
                dwhAwardDetailBean.setAwardId(awardDetail.getAwardId());
                dwhAwardDetailBean.setCategoryId(awardDetail.getCategoryId());
                dwhAwardDetailBean.setCategory( this.getCategoryDescription(awardDetail.getCategoryId(), categoryBurnDetailList));
                dwhAwardDetailBean.setBrandId(awardDetail.getBrandId());
                dwhAwardDetailBean.setBrand(this.getBrandDescription(awardDetail.getBrandId(), brandDetailList));
                dwhAwardDetailBean.setDescription(awardDetail.getDescription());
                dwhAwardDetailBean.setDescriptionShort(awardDetail.getDescriptionShort());
                dwhAwardDetailBean.setPoint(awardDetail.getContribution());
                dwhAwardDetailBean.setContribution(awardDetail.getContribution());
                dwhAwardDetailBean.setDateStartRedemption(awardDetail.getDateStartRedemption());
                dwhAwardDetailBean.setDateStopRedemption(awardDetail.getDateStopRedemption());
                dwhAwardDetailBean.setCancelable(awardDetail.getCancelable());
                dwhAwardDetailBean.setTangible(awardDetail.getTangible());
                dwhAwardDetailBean.setType(awardDetail.getType());
                dwhAwardDetailBean.setRedeemable(awardDetail.getRedeemable());
                dwhAwardDetailBean.setUrlImageAward(awardDetail.getUrlImageAward());
                dwhAwardDetailBean.setUrlImageBrand(awardDetail.getUrlImageBrand());
                em.persist(dwhAwardDetailBean);
            }
            */

            for (BrandDetail brandDetail : brandDetailList) {

                DWHBrandDetailBean brandDetailBean = new DWHBrandDetailBean();
                brandDetailBean.setCreationTimestamp(now);
                brandDetailBean.setBrandId(brandDetail.getBrandId());
                brandDetailBean.setBrand(brandDetail.getBrand());
                brandDetailBean.setBrandUrl(brandDetail.getBrandUrl());

                em.persist(brandDetailBean);
            }

            for (CategoryBurnDetail categoryBurnDetail : categoryBurnDetailList) {

                DWHCategoryBurnDetailBean dWHCategoryBurnDetailBean = new DWHCategoryBurnDetailBean();
                dWHCategoryBurnDetailBean.setCategoryId(categoryBurnDetail.getCategoryId());
                dWHCategoryBurnDetailBean.setCategory(categoryBurnDetail.getCategory());
                dWHCategoryBurnDetailBean.setCategoryImageUrl(categoryBurnDetail.getCategoryImageUrl());
                dWHCategoryBurnDetailBean.setCreationTimestamp(now);

                em.persist(dWHCategoryBurnDetailBean);
            }

            //System.out.println("Total awards persisted: "+awardDetailList.size());
            System.out.println("Total brands persisted: "+brandDetailList.size());
            System.out.println("Total category burn persisted: "+categoryBurnDetailList.size());
            
            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");
            userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
    
    private String getBrandDescription(Integer brandId,  List<BrandDetail> brandDetailList){
        String brandName = "";
        for (BrandDetail brandDetail : brandDetailList) {
            int brandIdInteger = brandDetail.getBrandId();
            if(brandIdInteger == brandId){
                brandName = brandDetail.getBrand();
            }
            
        }
        return brandName;
    }
    
    private String getCategoryDescription(Integer categoryId,  List<CategoryBurnDetail> categoryBurnDetailList){
        String categoryName = "";
        for (CategoryBurnDetail categoryBurnDetail : categoryBurnDetailList) {
            int categoryIdInteger = categoryBurnDetail.getCategoryId();
            if(categoryIdInteger == categoryId){
                categoryName = categoryBurnDetail.getCategory();
            }
            
        }
        return categoryName;
    }

}