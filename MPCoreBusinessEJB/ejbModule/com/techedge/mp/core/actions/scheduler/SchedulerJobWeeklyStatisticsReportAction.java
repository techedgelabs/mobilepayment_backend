package com.techedge.mp.core.actions.scheduler;

import java.awt.Color;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.apache.poi.ss.usermodel.HorizontalAlignment;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelSheetData;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobWeeklyStatisticsReportAction {
    private static final String JOB_NAME = "JOB_WEEKLY_STATISTICS_REPORT";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    private ExcelCellStyle      csHeaderBordered;
    private ExcelCellStyle      csTableValue;

    public SchedulerJobWeeklyStatisticsReportAction() {}

    public String execute(EmailSenderRemote emailSender, String reportRecipient, String proxyHost, String proxyPort, String proxyNoHosts) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        Date now = new Date();

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");
        
        NumberFormat integerFormat = NumberFormat.getNumberInstance(Locale.ITALIAN);
        integerFormat.setRoundingMode(RoundingMode.HALF_UP);
        integerFormat.setMinimumFractionDigits(0);
        integerFormat.setMaximumFractionDigits(0);

        System.out.println("job name: " + SchedulerJobWeeklyStatisticsReportAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {

            userTransaction.begin();

            SimpleDateFormat sdfFull = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
            
            /*
            Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
            System.out.println("DATA ATTUALE:" + sdfFull.format(calendar.getTime()));
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date weeklyEndDate = calendar.getTime();

            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date weeklyStartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.MONTH, Calendar.DECEMBER);
            calendar.set(Calendar.YEAR, 2017);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date totalStartDate = calendar.getTime();

            System.out.println("DATA SETTIMANALE INIZIO:" + sdfFull.format(weeklyStartDate));
            System.out.println("DATA SETTIMANALE FINE:" + sdfFull.format(weeklyEndDate));
            System.out.println("DATA TOTALI INIZIO:" + sdfFull.format(totalStartDate));
            */
            
            Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
            System.out.println("DATA ATTUALE:" + sdfFull.format(calendar.getTime()));
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date lastWeekEndDate = calendar.getTime();

            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date lastWeekStartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -8);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date previousWeekEndDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -8);
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date previousWeekStartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.MONTH, Calendar.JANUARY);
            calendar.set(Calendar.YEAR, 2018);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date progress2018StartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.MONTH, Calendar.JANUARY);
            calendar.set(Calendar.YEAR, 2019);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date progress2019StartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.YEAR, 2018);
            Date progress2018EndDate = calendar.getTime();
            
            Date progress2019EndDate = lastWeekEndDate;

            System.out.println("DATA SETTIMA INIZIO:            " + sdfFull.format(lastWeekStartDate));
            System.out.println("DATA SETTIMA FINE:              " + sdfFull.format(lastWeekEndDate));
            System.out.println("DATA SETTIMA PRECEDENTE INIZIO: " + sdfFull.format(previousWeekStartDate));
            System.out.println("DATA SETTIMA PRECEDENTE FINE:   " + sdfFull.format(previousWeekEndDate));
            System.out.println("DATA PROGRESSIVO 2018 INIZIO:   " + sdfFull.format(progress2018StartDate));
            System.out.println("DATA PROGRESSIVO 2018 FINE:     " + sdfFull.format(progress2018EndDate));
            System.out.println("DATA PROGRESSIVO 2019 INIZIO:   " + sdfFull.format(progress2019StartDate));
            System.out.println("DATA PROGRESSIVO 2019 FINE:     " + sdfFull.format(progress2019EndDate));

            
            List<Object> resultLastWeekPrePaidList = QueryRepository.statisticsReportStationsPrePaidTransactions(em, lastWeekStartDate, lastWeekEndDate, TransactionCategoryType.TRANSACTION_CUSTOMER);
            
            List<Object> resultPreviousWeekPrePaidList = QueryRepository.statisticsReportStationsPrePaidTransactions(em, previousWeekStartDate, previousWeekEndDate, TransactionCategoryType.TRANSACTION_CUSTOMER);
            
            List<Object> result2018TotalPrePaidList = QueryRepository.statisticsReportStationsPrePaidTransactions(em, progress2018StartDate, progress2018EndDate, TransactionCategoryType.TRANSACTION_CUSTOMER);
            
            List<Object> result2019TotalPrePaidList = QueryRepository.statisticsReportStationsPrePaidTransactions(em, progress2019StartDate, progress2019EndDate, TransactionCategoryType.TRANSACTION_CUSTOMER);

            
            List<Object> resultLastWeekPostPaidList = QueryRepository.statisticsReportStationsPostPaidTransactions(em, lastWeekStartDate, lastWeekEndDate, TransactionCategoryType.TRANSACTION_CUSTOMER);
            
            List<Object> resultPreviousWeekPostPaidList = QueryRepository.statisticsReportStationsPostPaidTransactions(em, previousWeekStartDate, previousWeekEndDate, TransactionCategoryType.TRANSACTION_CUSTOMER);

            List<Object> result2018TotalPostPaidList = QueryRepository.statisticsReportStationsPostPaidTransactions(em, progress2018StartDate, progress2018EndDate, TransactionCategoryType.TRANSACTION_CUSTOMER);
            
            List<Object> result2019TotalPostPaidList = QueryRepository.statisticsReportStationsPostPaidTransactions(em, progress2019StartDate, progress2019EndDate, TransactionCategoryType.TRANSACTION_CUSTOMER);

            
            List<Object> resultLastWeekPrePaidCreditsList = QueryRepository.statisticsReportStationsPrePaidCreditsTransactions(em, lastWeekStartDate, lastWeekEndDate);
            
            List<Object> resultPreviousWeekPrePaidCreditsList = QueryRepository.statisticsReportStationsPrePaidCreditsTransactions(em, previousWeekStartDate, previousWeekEndDate);
            
            List<Object> result2018TotalPrePaidCreditsList = QueryRepository.statisticsReportStationsPrePaidCreditsTransactions(em, progress2018StartDate, progress2018EndDate);
            
            List<Object> result2019TotalPrePaidCreditsList = QueryRepository.statisticsReportStationsPrePaidCreditsTransactions(em, progress2019StartDate, progress2019EndDate);
            
            
            List<Object> resultLastWeekPostPaidCreditsList = QueryRepository.statisticsReportStationsPostPaidCreditsTransactions(em, lastWeekStartDate, lastWeekEndDate);
            
            List<Object> resultPreviousWeekPostPaidCreditsList = QueryRepository.statisticsReportStationsPostPaidCreditsTransactions(em, previousWeekStartDate, previousWeekEndDate);
            
            List<Object> result2018TotalPostPaidCreditsList = QueryRepository.statisticsReportStationsPostPaidCreditsTransactions(em, progress2018StartDate, progress2018EndDate);
            
            List<Object> result2019TotalPostPaidCreditsList = QueryRepository.statisticsReportStationsPostPaidCreditsTransactions(em, progress2019StartDate, progress2019EndDate);

            
            List<Object> resultLastWeekEventNotificationList = QueryRepository.statisticsReportStationsEventNotificationTransactions(em, lastWeekStartDate, lastWeekEndDate);

            List<Object> resultPreviousWeekEventNotificationList = QueryRepository.statisticsReportStationsEventNotificationTransactions(em, previousWeekStartDate, previousWeekEndDate);
            
            List<Object> result2018TotalEventNotificationList = QueryRepository.statisticsReportStationsEventNotificationTransactions(em, progress2018StartDate, progress2018EndDate);

            List<Object> result2019TotalEventNotificationList = QueryRepository.statisticsReportStationsEventNotificationTransactions(em, progress2019StartDate, progress2019EndDate);

            
            //List<Object> resultWeeklyPostPaidLoyaltyList = QueryRepository.statisticsReportStationsPostPaidLoyaltyTransactions(em, weeklyStartDate, weeklyEndDate);

            //List<Object> resultTotalPostPaidLoyaltyList = QueryRepository.statisticsReportStationsPostPaidLoyaltyTransactions(em, totalStartDate, weeklyEndDate);

            
            List<Object> resultStationsList = QueryRepository.statisticsReportStationsList(em);
            
            ExcelWorkBook ewb = new ExcelWorkBook();
            
            ExcelSheetData excelDataLastWeekTransactions     = ewb.createSheetData();
            ExcelSheetData excelDataPreviousWeekTransactions = ewb.createSheetData();
            ExcelSheetData excelData2018TotalTransactions    = ewb.createSheetData();
            ExcelSheetData excelData2019TotalTransactions    = ewb.createSheetData();
            
            final String[] sheetDetailColumnsHeading = new String[] { "Codice PV", "Indirizzo", "Citt�", "Provincia", "Area", "Attivo payment", "Attivo loyalty", 
                    "Transazioni di pagamento", "Erogato transazioni di pagamento", "Punti transazioni di pagamento", "Transazioni loyalty", "Erogato transazioni loyalty",
                    "Punti transazioni loyalty" };
            csHeaderBordered = ewb.createCellStyle(true, 12, Color.BLACK, new Color(254, 211, 0), HorizontalAlignment.CENTER_SELECTION, -1, 1, 1, 1, 1, Color.BLACK);
            csTableValue = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null);
           
            ArrayList<ArrayList<Object>> resultLastWeekPayment            = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> resultLastWeekPaymentCredits     = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> resultLastWeekLoyalty            = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> resultPreviousWeekPayment        = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> resultPreviousWeekPaymentCredits = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> resultPreviousWeekLoyalty        = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> result2018TotalPayment           = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> result2018TotalPaymentCredits    = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> result2018TotalLoyalty           = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> result2019TotalPayment           = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> result2019TotalPaymentCredits    = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> result2019TotalLoyalty           = new ArrayList<ArrayList<Object>>();

            /***************************************/
            
            System.out.println("Inserimento dati pre paid nella lista resultLastWeekPayment");
            for (Object result : resultLastWeekPrePaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                resultLastWeekPayment.add(row);
            }

            System.out.println("Inserimento dati post paid nella lista resultLastWeekPayment");
            for (Object result : resultLastWeekPostPaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                resultLastWeekPayment.add(row);
            }
            
            System.out.println("Inserimento dati punti pre paid nella lista resultLastWeekPaymentCredits");
            for (Object result : resultLastWeekPrePaidCreditsList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                resultLastWeekPaymentCredits.add(row);
            }
            
            System.out.println("Inserimento dati punti post paid nella lista resultLastWeekPaymentCredits");
            for (Object result : resultLastWeekPostPaidCreditsList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                resultLastWeekPaymentCredits.add(row);
            }
            
            System.out.println("Inserimento event notification nella lista resultLastWeekLoyalty");
            for (Object result : resultLastWeekEventNotificationList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                row.add(resultList.get(3));
                resultLastWeekLoyalty.add(row);
            }
            
            /***************************************/
            
            System.out.println("Inserimento dati pre paid nella lista resultPreviousWeekPayment");
            for (Object result : resultPreviousWeekPrePaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                resultPreviousWeekPayment.add(row);
            }

            System.out.println("Inserimento dati post paid nella lista resultPreviousWeekPayment");
            for (Object result : resultPreviousWeekPostPaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                resultPreviousWeekPayment.add(row);
            }
            
            System.out.println("Inserimento dati punti pre paid nella lista resultPreviousWeekPaymentCredits");
            for (Object result : resultPreviousWeekPrePaidCreditsList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                resultPreviousWeekPaymentCredits.add(row);
            }
            
            System.out.println("Inserimento dati punti post paid nella lista resultPreviousWeekPaymentCredits");
            for (Object result : resultPreviousWeekPostPaidCreditsList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                resultPreviousWeekPaymentCredits.add(row);
            }
            
            System.out.println("Inserimento event notification nella lista resultPreviousWeekLoyalty");
            for (Object result : resultPreviousWeekEventNotificationList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                row.add(resultList.get(3));
                resultPreviousWeekLoyalty.add(row);
            }
            
            /***************************************/
            
            System.out.println("Inserimento dati pre paid nella lista result2018TotalPayment");
            for (Object result : result2018TotalPrePaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                result2018TotalPayment.add(row);
            }

            System.out.println("Inserimento dati post paid nella lista result2018TotalPayment");
            for (Object result : result2018TotalPostPaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                result2018TotalPayment.add(row);
            }
            
            System.out.println("Inserimento dati punti pre paid nella lista result2018TotalPaymentCredits");
            for (Object result : result2018TotalPrePaidCreditsList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                result2018TotalPaymentCredits.add(row);
            }
            
            System.out.println("Inserimento dati punti post paid nella lista result2018TotalPaymentCredits");
            for (Object result : result2018TotalPostPaidCreditsList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                result2018TotalPaymentCredits.add(row);
            }
            
            System.out.println("Inserimento event notification nella lista result2018TotalLoyalty");
            for (Object result : result2018TotalEventNotificationList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                row.add(resultList.get(3));
                result2018TotalLoyalty.add(row);
            }
            
            /***************************************/
            
            System.out.println("Inserimento dati pre paid nella lista result2019TotalPayment");
            for (Object result : result2019TotalPrePaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                result2019TotalPayment.add(row);
            }

            System.out.println("Inserimento dati post paid nella lista result2019TotalPayment");
            for (Object result : result2019TotalPostPaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                result2019TotalPayment.add(row);
            }
            
            System.out.println("Inserimento dati punti pre paid nella lista result2019TotalPaymentCredits");
            for (Object result : result2019TotalPrePaidCreditsList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                result2019TotalPaymentCredits.add(row);
            }
            
            System.out.println("Inserimento dati punti post paid nella lista result2019TotalPaymentCredits");
            for (Object result : result2019TotalPostPaidCreditsList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                result2019TotalPaymentCredits.add(row);
            }
            
            System.out.println("Inserimento event notification nella lista result20198TotalLoyalty");
            for (Object result : result2019TotalEventNotificationList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                row.add(resultList.get(3));
                result2019TotalLoyalty.add(row);
            }

            
            System.out.println("Creazione dati per file Excel foglio ultima settimana");
            
            buildExcelSheetData(excelDataLastWeekTransactions, resultStationsList, resultLastWeekPayment, resultLastWeekPaymentCredits, resultLastWeekLoyalty);

            System.out.println("Creazione foglio 'Ultima settimana'");

            ewb.addSheet("Ultima settimana", sheetDetailColumnsHeading, excelDataLastWeekTransactions, csHeaderBordered, csTableValue);
            
            
            System.out.println("Creazione dati per file Excel foglio settimana precedente");
            
            buildExcelSheetData(excelDataPreviousWeekTransactions, resultStationsList, resultPreviousWeekPayment, resultPreviousWeekPaymentCredits, resultPreviousWeekLoyalty);

            System.out.println("Creazione foglio 'Settimana precedente'");

            ewb.addSheet("Settimana precedente", sheetDetailColumnsHeading, excelDataPreviousWeekTransactions, csHeaderBordered, csTableValue);

            
            System.out.println("Creazione dati per file Excel foglio progressivo 2018");
            
            buildExcelSheetData(excelData2018TotalTransactions, resultStationsList, result2018TotalPayment, result2018TotalPaymentCredits, result2018TotalLoyalty);

            System.out.println("Creazione foglio 'Progressivo 2018'");

            ewb.addSheet("Progressivo 2018", sheetDetailColumnsHeading, excelData2018TotalTransactions, csHeaderBordered, csTableValue);

            
            System.out.println("Creazione dati per file Excel foglio progressivo 2019");
            
            buildExcelSheetData(excelData2019TotalTransactions, resultStationsList, result2019TotalPayment, result2019TotalPaymentCredits, result2019TotalLoyalty);

            System.out.println("Creazione foglio 'Progressivo 2019'");

            ewb.addSheet("Progressivo 2019", sheetDetailColumnsHeading, excelData2019TotalTransactions, csHeaderBordered, csTableValue);

            
            System.out.println("Invio email");
            
            EmailType emailType = EmailType.WEEKLY_STATISTICS_REPORT_V2;
            List<Parameter> parameters = new ArrayList<Parameter>(0);

            parameters.add(new Parameter("DATE_START", sdfDate.format(lastWeekStartDate)));
            parameters.add(new Parameter("DATE_END", sdfDate.format(lastWeekEndDate)));
            List<Attachment> attachments = new ArrayList<Attachment>(0);

            // L'allegato deve essere inviato solo se � stato consumato almeno un voucher
            Attachment attachment = new Attachment();
            attachment.setFileName("report_statistiche_settimanale_transazioni_per_PV.xlsx");
            //attachment.setContent(attachmentContent);
            byte[] bytes = ewb.getBytesToStream();
            attachment.setBytes(bytes);
            attachments.add(attachment);

            //reportRecipient = "giovanni.dorazio@techedgegroup.com, luca.mancini@techedgegroup.com, alessandro.menale@techedgegroup.com";
            //reportRecipient = "giovanni.dorazio@techedgegroup.com, luca.mancini@techedgegroup.com";
            //reportRecipient = "giovanni.dorazio@techedgegroup.com";
            
            if ( reportRecipient == null || reportRecipient.equals("") ) {
                
                // Recipient empty
                System.out.println("SendEmailResult: not sent - recipient empty");
            }
            else {
                
                // Sending email
                System.out.println("Sending email to: " + reportRecipient);
                String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, reportRecipient, parameters, attachments);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }
            
            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");

            userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
    
    private void buildExcelSheetData(ExcelSheetData excelSheetData, List<Object> resultStationsList, ArrayList<ArrayList<Object>> resultPayment, ArrayList<ArrayList<Object>> resultPaymentCredits, ArrayList<ArrayList<Object>> resultLoyalty) {
        
        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("#,##0.000");
        
        for (Object result : resultStationsList) {
            
            List<Object> row = Arrays.asList((Object[])result);
            int rowIndex = excelSheetData.createRow();
            int columnIndex = 1;
            String stationID = null;
            Integer totalPayment         = 0;
            Double  totalQuantity        = 0.0;
            Integer totalCredits         = 0;
            Integer totalLoyalty         = 0;
            Double  totalLoyaltyQuantity = 0.0;
            Integer totalLoyaltyCredits  = 0;
            
            for (Object data : row) {
                if (columnIndex == 1) {
                    stationID = data.toString();
                }
                
                if (columnIndex == 6 || columnIndex == 7) {
                    if (data == null) {
                        excelSheetData.addRowData("false", rowIndex);
                    }
                    else {
                        excelSheetData.addRowData(data.toString(), rowIndex);
                    }
                }
                else {
                    if (data == null) {
                        excelSheetData.addRowData("", rowIndex);
                    }
                    else {
                        excelSheetData.addRowData(data.toString(), rowIndex);
                    }                        
                }
                
                columnIndex++;
            }
            
            //System.out.println("stationID: " + stationID);
            
            for (ArrayList<Object> rowPayment : resultPayment) {
                if (stationID.equals(rowPayment.get(0).toString())) {
                    Integer count    = Integer.parseInt(rowPayment.get(1).toString());
                    Double  quantity = Double.parseDouble(rowPayment.get(2).toString());
                    //Integer credits  = Integer.parseInt(rowPayment.get(3).toString());
                    totalPayment  += count;
                    totalQuantity += quantity;
                    //totalCredits  += credits;
                    //break;
                }
            }
            
            for (ArrayList<Object> rowPaymentCredist : resultPaymentCredits) {
                if (stationID.equals(rowPaymentCredist.get(0).toString())) {
                    Integer credits  = Integer.parseInt(rowPaymentCredist.get(1).toString());
                    totalCredits  += credits;
                    //break;
                }
            }

            for (ArrayList<Object> rowLoyalty : resultLoyalty) {
                if (stationID.equals(rowLoyalty.get(0).toString())) {
                    Integer count = Integer.parseInt(rowLoyalty.get(1).toString());
                    Double  quantity = Double.parseDouble(rowLoyalty.get(2).toString());
                    Integer credits  = Integer.parseInt(rowLoyalty.get(3).toString());
                    totalLoyalty  += count;
                    totalLoyaltyQuantity += quantity;
                    totalLoyaltyCredits  += credits;
                    //break;
                }
            }
            
            String quantityString        = numberFormat.format(totalQuantity);
            String loyaltyQuantityString = numberFormat.format(totalLoyaltyQuantity);
            
            excelSheetData.addRowData(totalPayment.toString(), rowIndex);
            excelSheetData.addRowData(quantityString, rowIndex);
            excelSheetData.addRowData(totalCredits.toString(), rowIndex);
            excelSheetData.addRowData(totalLoyalty.toString(), rowIndex);
            excelSheetData.addRowData(loyaltyQuantityString, rowIndex);
            excelSheetData.addRowData(totalLoyaltyCredits.toString(), rowIndex);
        }
        
    }
    /*
    private static String resultAsString(Object o) {
        if (o instanceof Object[]) {
            return Arrays.asList((Object[])o).toString();
        } else {
            return String.valueOf(o);
        }
    }
    */
}