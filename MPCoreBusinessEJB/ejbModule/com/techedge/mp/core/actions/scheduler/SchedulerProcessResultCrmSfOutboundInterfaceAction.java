package com.techedge.mp.core.actions.scheduler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.EventNotificationService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundDeliveryStatusType;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundProcessedStatusType;
import com.techedge.mp.core.business.interfaces.crm.CrmSfDataElement;
import com.techedge.mp.core.business.interfaces.crm.CrmSfDataElementProcessingResult;
import com.techedge.mp.core.business.interfaces.crm.CrmSfOutboundDeliveryStatusType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfOutboundBean;
import com.techedge.mp.core.business.utilities.AbstractFTPService;
import com.techedge.mp.core.business.utilities.CrmSfDataProcessor;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.CreateVoucherPromotional;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerProcessResultCrmSfOutboundInterfaceAction {

    private static final String      JOB_NAME = "JOB_PROCESS_RESULT_CRM_SF_OUTBOUND_INTERFACE";

    @Resource
    private EJBContext               context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager            em;

    @EJB
    private LoggerService            loggerService;

    @EJB
    private UserService              userService;

    @EJB
    private EventNotificationService eventNotificationService;

    private UserTransaction          userTransaction;

    public SchedulerProcessResultCrmSfOutboundInterfaceAction() {}

    public String execute(String ftpsResultPath, String fileExtensionData, String fileExtensionOk, CRMServiceRemote CRMService, AbstractFTPService ftpService,
            PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, String summaryRecipient, Integer expiryTime, 
            Integer maxRetryAttemps, Integer maxUsersProcessed, Integer processResultDelay) throws EJBException {
        
        userTransaction = context.getUserTransaction();

        Date now = new Date();

        System.out.println("job name: " + SchedulerProcessResultCrmSfOutboundInterfaceAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {
            //userTransaction.begin();

            String result = ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

            int rowsCount = QueryRepository.getCRMSfOutboundRowsCount(em);

            if (rowsCount > 0) {

                List<CRMSfOutboundBean> crmOutboundBeanList = QueryRepository.findCRMSfOutboundDeliveryStatus(em, CrmSfOutboundDeliveryStatusType.CREATED.getCode(),maxUsersProcessed);

                if (crmOutboundBeanList != null && !crmOutboundBeanList.isEmpty()) {

                    for (CRMSfOutboundBean crmOutboundBean : crmOutboundBeanList) {
                        processFiscalCode(crmOutboundBean, pushNotificationService, fidelityService, expiryTime, maxRetryAttemps);
                    }
                }
                else {
                    
                    crmOutboundBeanList = QueryRepository.getAllCRMSfOutbound(em, true);
                    CRMSfOutboundBean crmOutboundBean = crmOutboundBeanList.get(0);
                    
                    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    //Date processedDate = sdfDate.parse("2019-02-04 11:11:11");
                    // prendo la prima data inserimento per che poi vado a confrontare con la data odierna per evitare l'elaborazione nelle 24H successive 		
                    Date processedDate = crmOutboundBean.getInsertDate();
                    Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
                    calendar.setTime(processedDate);
                    calendar.set(Calendar.SECOND, processResultDelay);
                    
                           
                    if (now.compareTo(calendar.getTime()) >= 0) {
                    
                        userTransaction.begin();
                        
                        crmOutboundBeanList = QueryRepository.getAllCRMSfOutbound(em, false);
                        
                        result = processResult(crmOutboundBeanList, ftpsResultPath, fileExtensionData, fileExtensionOk, ftpService, emailSender, summaryRecipient);
    
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                                "Eliminazione di tutti i records presenti nella tabella CrmSfOutbound...");
                        int rows = em.createQuery("delete from CRMSfOutboundBean").executeUpdate();
    
                        if (rows <= 0) {
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Eliminazione dei records presenti non andata a buon fine");
                        }
                        
                        userTransaction.commit();
                    }
                    else {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Data di processo risultati: " + sdfDate.format(calendar.getTime()));
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Data inserimento dati: " + sdfDate.format(processedDate));
                    }
                }
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Nessun CRM Outbound da processare");
            }

            //userTransaction.commit();

            // Termina il job
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + result);

            return result;

        }
        catch (Exception ex) {
            
            ex.printStackTrace();
            
            System.err.println("Errore nell'esecuzione del job: " + ex.getMessage());

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }

    private String processFiscalCode(CRMSfOutboundBean crmOutboundBean, PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, 
            Integer expiryTime, Integer maxRetryAttemps) {
        
        String fiscalCode = crmOutboundBean.getFiscalCode();

        try {
            
            userTransaction.begin();
            
            // Ricerca l'utente per codice fiscale
            UserBean userBean = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode);
    
            if (userBean == null) {
                crmOutboundBean.setDeliveryStatus(CrmSfOutboundDeliveryStatusType.USER_NOT_FOUND.getCode());
                //crmOutboundBean.setDeliveryMessage("Utente non trovato");
                //crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.UNAVAILABLE);
    
                em.merge(crmOutboundBean);
    
                System.out.println(fiscalCode + ": " + crmOutboundBean.getDeliveryStatus());
                userTransaction.commit();
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
            }
            
            // Verifica dati anagrafici
            if (!this.matchPersonalData(userBean, crmOutboundBean)) {
                crmOutboundBean.setDeliveryStatus(CrmSfOutboundDeliveryStatusType.INVALID_INPUT.getCode());
                //crmOutboundBean.setDeliveryMessage("Dati anagrafici non corrispondenti");
                //crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.UNAVAILABLE);
    
                em.merge(crmOutboundBean);
    
                //System.out.println(fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
                userTransaction.commit();
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
            }
            
           //assegna voucher
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Tento l'attivazione offerta voucher");
            
            if (crmOutboundBean.getVoucherCode() == null || crmOutboundBean.getVoucherCode().isEmpty()) {
                crmOutboundBean.setDeliveryStatus(CrmOutboundDeliveryStatusType.ERROR_VOUCHER.getCode());
    
                em.merge(crmOutboundBean);
    
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, fiscalCode + ": " + crmOutboundBean.getDeliveryStatus());
                userTransaction.commit();
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
            }

            if (crmOutboundBean.getVoucherAmount() == null || crmOutboundBean.getVoucherAmount().isEmpty()) {
            	crmOutboundBean.setDeliveryStatus(CrmOutboundDeliveryStatusType.ERROR_VOUCHER.getCode());
    
                em.merge(crmOutboundBean);
    
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, fiscalCode + ": " + crmOutboundBean.getDeliveryStatus());
                userTransaction.commit();
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
            }
            
            String promoCode = crmOutboundBean.getVoucherCode();
            Double voucherAmount = new Double(crmOutboundBean.getVoucherAmount());
            
            CreateVoucherPromotional createVoucherPromotional = new CreateVoucherPromotional(em, fidelityService, userBean);
            String createResponse = createVoucherPromotional.create(promoCode, voucherAmount, null);
            String associateResponse = null;

            if (!createResponse.equals(ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_SUCCESS)) {
                crmOutboundBean.setDeliveryStatus(CrmOutboundDeliveryStatusType.ERROR_VOUCHER.getCode());
    
                em.merge(crmOutboundBean);
    
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, fiscalCode + ": " + crmOutboundBean.getDeliveryStatus());
                userTransaction.commit();
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
            }

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "VoucherPromotionalBean: " + createVoucherPromotional.getVoucherPromotionalBean().toString());
            associateResponse = createVoucherPromotional.associateToUser(createVoucherPromotional.getCreateVoucherResult());

            if (Objects.equals(associateResponse, ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_ASSIGN_ERROR)) {

                crmOutboundBean.setDeliveryStatus(CrmOutboundDeliveryStatusType.ERROR_VOUCHER.getCode());
    
                em.merge(crmOutboundBean);
    
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, fiscalCode + ": " + crmOutboundBean.getDeliveryStatus());
                userTransaction.commit();
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
            }

    
            crmOutboundBean.setDeliveryStatus(CrmSfOutboundDeliveryStatusType.SUCCESS.getCode());
    
            em.merge(crmOutboundBean);
            
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, fiscalCode + ": " + crmOutboundBean.getDeliveryStatus());
            
            userTransaction.commit();

            return "";
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Errore nell'esecuzione del processamento del codice fiscale: " + ex.getMessage());
            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }

    private String processResult(List<CRMSfOutboundBean> crmOutboundBeanList, String ftpsResultPath, String fileExtensionData, String fileExtensionOk, 
            AbstractFTPService ftpService, EmailSenderRemote emailSender, String summaryRecipient) {
        List<CrmSfDataElementProcessingResult> crmDataElementProcessingResultList = new ArrayList<CrmSfDataElementProcessingResult>();
        String categoryDesc = "APP";

        for (CRMSfOutboundBean crmOutboundBean : crmOutboundBeanList) {
            CrmSfDataElement crmSfDataElement = new CrmSfDataElement();
            crmSfDataElement.contactCode = crmOutboundBean.getContactCode();
    		crmSfDataElement.cardCode = crmOutboundBean.getCardCode();
    		crmSfDataElement.firstName = crmOutboundBean.getFirstName();
    		crmSfDataElement.lastName = crmOutboundBean.getLastName();
    		crmSfDataElement.phone = crmOutboundBean.getPhone();
    		crmSfDataElement.email = crmOutboundBean.getEmail();
    		crmSfDataElement.channel = crmOutboundBean.getChannel();
    		crmSfDataElement.initiativeCode = crmOutboundBean.getInitiativeCode();
    		crmSfDataElement.offertCode = crmOutboundBean.getOffertCode();
    		crmSfDataElement.id = crmOutboundBean.getIdCrm();
    		crmSfDataElement.fiscalCode = crmOutboundBean.getFiscalCode();
    		crmSfDataElement.createDate = crmOutboundBean.getCreatedDate();
    		crmSfDataElement.type = crmOutboundBean.getType();
    		crmSfDataElement.voucherCode = crmOutboundBean.getVoucherCode();
    		crmSfDataElement.voucherAmount = crmOutboundBean.getVoucherAmount();
    		crmSfDataElement.parameter1 = crmOutboundBean.getParameter1();
    		crmSfDataElement.parameter2 = crmOutboundBean.getParameter2();
    		crmSfDataElement.parameter3 = crmOutboundBean.getParameter3();
    		crmSfDataElement.parameter4 = crmOutboundBean.getParameter4();
            
            String deliveryId = crmOutboundBean.getDeliveryStatus();
            
            CrmSfDataElementProcessingResult crmSfDataElementProcessingResult = CrmSfDataProcessor.createCrmDataElementProcessingResult(crmSfDataElement, categoryDesc, deliveryId);
            crmDataElementProcessingResultList.add(crmSfDataElementProcessingResult);
        }

        // Genera il file con l'esito dell'elaborazione
        String output = CrmSfDataProcessor.generateOutput(crmDataElementProcessingResultList);

        //System.out.println("Output file: " + output);

        // Scrivi il file con l'esito dell'elaborazione nella directory del server ftps ftpsResultPath
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String fileName = "SF_EL_010_RGMA_VOU_" + dateFormat.format(new Date()) + "_01_01";
        String outputFileName = ftpsResultPath + AbstractFTPService.separator + fileName + "." + fileExtensionData;

        Boolean putOutput = ftpService.putFile(output, outputFileName);

        if (!putOutput) {

            // Si � verificato un errore nella scrittura del file di output
            System.out.println("error creating file di output (" + outputFileName + ")");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }

        // Scrivi il file con l'ok dell'elaborazione nella directory del server ftps ftpsResultPath
        String resultFileName = ftpsResultPath + AbstractFTPService.separator + fileName + "." + fileExtensionOk;

        Boolean putResult = ftpService.putFile("", resultFileName);

        if (!putResult) {

            // Si � verificato un errore nella scrittura del file di result
            System.out.println("error creating file di result (" + resultFileName + ")");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }

        System.out.println("Invio email");
        
        EmailType emailType = EmailType.CRM_OUTBOUND_SUMMARY;
        List<Parameter> parameters = new ArrayList<Parameter>(0);

        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
        parameters.add(new Parameter("DATE", sdfDate.format(new Date())));
        List<Attachment> attachments = new ArrayList<Attachment>(0);

        Attachment attachment = new Attachment();
        attachment.setFileName(fileName + "." + fileExtensionData);
        attachment.setBytes(output.getBytes());
        attachments.add(attachment);

        if ( summaryRecipient == null || summaryRecipient.equals("") ) {
            
            // Recipient empty
            System.out.println("SendEmailResult: not sent - recipient empty");
        }
        else {
            
            // Sending email
            System.out.println("Sending email to: " + summaryRecipient);
            String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, summaryRecipient, parameters, attachments);
            System.out.println("SendEmailResult: " + sendEmailResult);
        }
        
        
        return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
    }

    private Boolean matchPersonalData(UserBean userBean, CRMSfOutboundBean crmOutboundBean) {

        String checkEmail = userBean.getPersonalDataBean().getSecurityDataEmail();
        //String checkTelCellulare = "";
        //String crmTelCellulare = crmOutboundBean.getTelCellulare().replaceAll("0039", "");
        /*if (userBean.getMobilePhoneList() != null) {
            for (MobilePhoneBean mobilePhoneBean : userBean.getMobilePhoneList()) {
                if (mobilePhoneBean.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_ACTIVE) {
                    checkTelCellulare = mobilePhoneBean.getNumber();
                    break;
                }
            }
        }

        if (checkTelCellulare == null || !checkTelCellulare.equals(crmTelCellulare)) {
            System.out.println("matchPersonalData error - field TelCellulare - expected: " + checkTelCellulare + ", found: " + crmOutboundBean.getTelCellulare());
            return Boolean.FALSE;
        }*/

        if (checkEmail == null || !checkEmail.equalsIgnoreCase(crmOutboundBean.getEmail())) {
            System.out.println("matchPersonalData error - field Email - expected: " + checkEmail + ", found: " + crmOutboundBean.getEmail());
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

}
