package com.techedge.mp.core.actions.scheduler;

import java.awt.Color;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import org.apache.poi.ss.usermodel.HorizontalAlignment;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelSheetData;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobWeeklyStatisticsReportBusinessAction {
    private static final String JOB_NAME = "JOB_WEEKLY_STATISTICS_REPORT";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    private ExcelCellStyle      csHeaderBordered;
    private ExcelCellStyle      csTableValue;

    public SchedulerJobWeeklyStatisticsReportBusinessAction() {}

    public String execute(EmailSenderRemote emailSender, String reportRecipient, String proxyHost, String proxyPort, String proxyNoHosts) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        Date now = new Date();

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("##0.000");
        
        NumberFormat integerFormat = NumberFormat.getNumberInstance(Locale.ITALIAN);
        integerFormat.setRoundingMode(RoundingMode.HALF_UP);
        integerFormat.setMinimumFractionDigits(0);
        integerFormat.setMaximumFractionDigits(0);

        System.out.println("job name: " + SchedulerJobWeeklyStatisticsReportBusinessAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {

            userTransaction.begin();

            SimpleDateFormat sdfFull = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
            

            Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
            System.out.println("DATA ATTUALE:" + sdfFull.format(calendar.getTime()));
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date lastWeekEndDate = calendar.getTime();

            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date lastWeekStartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -8);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date previousWeekEndDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -8);
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date previousWeekStartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.MONTH, Calendar.JANUARY);
            calendar.set(Calendar.YEAR, 2018);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date progress2018StartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.MONTH, Calendar.JANUARY);
            calendar.set(Calendar.YEAR, 2019);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date progress2019StartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            calendar.set(Calendar.YEAR, 2018);
            Date progress2018EndDate = calendar.getTime();
            
            Date progress2019EndDate = lastWeekEndDate;

            System.out.println("DATA SETTIMA INIZIO:            " + sdfFull.format(lastWeekStartDate));
            System.out.println("DATA SETTIMA FINE:              " + sdfFull.format(lastWeekEndDate));
            System.out.println("DATA SETTIMA PRECEDENTE INIZIO: " + sdfFull.format(previousWeekStartDate));
            System.out.println("DATA SETTIMA PRECEDENTE FINE:   " + sdfFull.format(previousWeekEndDate));
            System.out.println("DATA PROGRESSIVO 2018 INIZIO:   " + sdfFull.format(progress2018StartDate));
            System.out.println("DATA PROGRESSIVO 2018 FINE:     " + sdfFull.format(progress2018EndDate));
            System.out.println("DATA PROGRESSIVO 2019 INIZIO:   " + sdfFull.format(progress2019StartDate));
            System.out.println("DATA PROGRESSIVO 2019 FINE:     " + sdfFull.format(progress2019EndDate));

            
            List<Object> resultLastWeekPrePaidList = QueryRepository.statisticsReportStationsPrePaidTransactions(em, lastWeekStartDate, lastWeekEndDate, TransactionCategoryType.TRANSACTION_BUSINESS);
            
            List<Object> resultPreviousWeekPrePaidList = QueryRepository.statisticsReportStationsPrePaidTransactions(em, previousWeekStartDate, previousWeekEndDate, TransactionCategoryType.TRANSACTION_BUSINESS);
            
            List<Object> result2018TotalPrePaidList = QueryRepository.statisticsReportStationsPrePaidTransactions(em, progress2018StartDate, progress2018EndDate, TransactionCategoryType.TRANSACTION_BUSINESS);

            List<Object> result2019TotalPrePaidList = QueryRepository.statisticsReportStationsPrePaidTransactions(em, progress2019StartDate, progress2019EndDate, TransactionCategoryType.TRANSACTION_BUSINESS);

            
            List<Object> resultLastWeekPostPaidList = QueryRepository.statisticsReportStationsPostPaidTransactions(em, lastWeekStartDate, lastWeekEndDate, TransactionCategoryType.TRANSACTION_BUSINESS);
            
            List<Object> resultPreviousWeekPostPaidList = QueryRepository.statisticsReportStationsPostPaidTransactions(em, previousWeekStartDate, previousWeekEndDate, TransactionCategoryType.TRANSACTION_BUSINESS);
            
            List<Object> result2018TotalPostPaidList = QueryRepository.statisticsReportStationsPostPaidTransactions(em, progress2018StartDate, progress2018EndDate, TransactionCategoryType.TRANSACTION_BUSINESS);

            List<Object> result2019TotalPostPaidList = QueryRepository.statisticsReportStationsPostPaidTransactions(em, progress2019StartDate, progress2019EndDate, TransactionCategoryType.TRANSACTION_BUSINESS);

            
            List<Object> resultStationsList = QueryRepository.statisticsBusinessReportStationsList(em);
            
            
            ExcelWorkBook ewb = new ExcelWorkBook();
            
            ExcelSheetData excelDataLastWeekTransactions     = ewb.createSheetData();
            ExcelSheetData excelDataPreviousWeekTransactions = ewb.createSheetData();
            ExcelSheetData excelData2018TotalTransactions    = ewb.createSheetData();
            ExcelSheetData excelData2019TotalTransactions    = ewb.createSheetData();
            
            final String[] sheetDetailColumnsHeading = new String[] { "Codice PV", "Indirizzo", "Citt�", "Provincia", "Area", "Attivo business", "Transazioni di pagamento", "Litri erogati" };
            csHeaderBordered = ewb.createCellStyle(true, 12, Color.BLACK, new Color(254, 211, 0), HorizontalAlignment.CENTER_SELECTION, -1, 1, 1, 1, 1, Color.BLACK);
            csTableValue = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null);
           
            ArrayList<ArrayList<Object>> resultLastWeekPayment     = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> resultPreviousWeekPayment = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> result2018TotalPayment    = new ArrayList<ArrayList<Object>>();
            ArrayList<ArrayList<Object>> result2019TotalPayment    = new ArrayList<ArrayList<Object>>();

            System.out.println("Inserimento dati pre paid nella lista resultLastWeekPayment");
            for (Object result : resultLastWeekPrePaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                resultLastWeekPayment.add(row);
            }

            System.out.println("Inserimento dati post paid nella lista resultLastWeekPayment");
            for (Object result : resultLastWeekPostPaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                resultLastWeekPayment.add(row);
            }
            
            System.out.println("Inserimento dati pre paid nella lista resultPreviousWeekPayment");
            for (Object result : resultPreviousWeekPrePaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                resultPreviousWeekPayment.add(row);
            }

            System.out.println("Inserimento dati post paid nella lista resultPreviousWeekPayment");
            for (Object result : resultPreviousWeekPostPaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                resultPreviousWeekPayment.add(row);
            }
            
            System.out.println("Inserimento dati pre paid nella lista result2018TotalPayment");
            for (Object result : result2018TotalPrePaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                result2018TotalPayment.add(row);
            }

            System.out.println("Inserimento dati post paid nella lista result2018TotalPayment");
            for (Object result : result2018TotalPostPaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                result2018TotalPayment.add(row);
            }
            
            System.out.println("Inserimento dati pre paid nella lista result2019TotalPayment");
            for (Object result : result2019TotalPrePaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                result2019TotalPayment.add(row);
            }

            System.out.println("Inserimento dati post paid nella lista result2019TotalPayment");
            for (Object result : result2019TotalPostPaidList) {
                //System.out.println(resultAsString(result));
                List<Object> resultList = Arrays.asList((Object[])result);
                ArrayList<Object> row = new ArrayList<Object>();
                row.add(resultList.get(0));
                row.add(resultList.get(1));
                row.add(resultList.get(2));
                result2019TotalPayment.add(row);
            }
            
            System.out.println("Creazione dati per file Excel foglio ultima settimana");
            
            buildExcelSheetData(excelDataLastWeekTransactions, resultStationsList, resultLastWeekPayment);

            System.out.println("Creazione foglio 'Ultima settimana'");

            ewb.addSheet("Ultima settimana", sheetDetailColumnsHeading, excelDataLastWeekTransactions, csHeaderBordered, csTableValue);

            
            System.out.println("Creazione dati per file Excel foglio settimana precedente");
            
            buildExcelSheetData(excelDataPreviousWeekTransactions, resultStationsList, resultPreviousWeekPayment);

            System.out.println("Creazione foglio 'Settimana precedente'");

            ewb.addSheet("Settimana precedente", sheetDetailColumnsHeading, excelDataPreviousWeekTransactions, csHeaderBordered, csTableValue);
            
            
            System.out.println("Creazione dati per file Excel foglio progressivo 2018");
            
            buildExcelSheetData(excelData2018TotalTransactions, resultStationsList, result2018TotalPayment);

            System.out.println("Creazione foglio 'Progressivo 2018'");

            ewb.addSheet("Progressivo 2018", sheetDetailColumnsHeading, excelData2018TotalTransactions, csHeaderBordered, csTableValue);
            
            
            System.out.println("Creazione dati per file Excel foglio progressivo 2019");
            
            buildExcelSheetData(excelData2019TotalTransactions, resultStationsList, result2019TotalPayment);

            System.out.println("Creazione foglio 'Progressivo 2019'");

            ewb.addSheet("Progressivo 2019", sheetDetailColumnsHeading, excelData2019TotalTransactions, csHeaderBordered, csTableValue);
            

            System.out.println("Invio email");
            
            EmailType emailType = EmailType.WEEKLY_STATISTICS_REPORT_BUSINESS;
            List<Parameter> parameters = new ArrayList<Parameter>(0);

            parameters.add(new Parameter("DATE_START", sdfDate.format(lastWeekStartDate)));
            parameters.add(new Parameter("DATE_END", sdfDate.format(lastWeekEndDate)));
            List<Attachment> attachments = new ArrayList<Attachment>(0);

            // L'allegato deve essere inviato solo se � stato consumato almeno un voucher
            Attachment attachment = new Attachment();
            attachment.setFileName("report_statistiche_settimanale_piva_transazioni_per_PV.xlsx");
            //attachment.setContent(attachmentContent);
            byte[] bytes = ewb.getBytesToStream();
            attachment.setBytes(bytes);
            attachments.add(attachment);

            //reportRecipient = "giovanni.dorazio@techedgegroup.com, luca.mancini@techedgegroup.com, alessandro.menale@techedgegroup.com";
            //reportRecipient = "giovanni.dorazio@techedgegroup.com, luca.mancini@techedgegroup.com";
            //reportRecipient = "giovanni.dorazio@techedgegroup.com";
            
            if ( reportRecipient == null || reportRecipient.equals("") ) {
                
                // Recipient empty
                System.out.println("SendEmailResult: not sent - recipient empty");
            }
            else {
                
                // Sending email
                System.out.println("Sending email to: " + reportRecipient);
                String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, reportRecipient, parameters, attachments);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }
            
            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");

            userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
    
    private void buildExcelSheetData(ExcelSheetData excelSheetData, List<Object> resultStationsList, ArrayList<ArrayList<Object>> resultPayment) {
        
        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("#,##0.000");
        
        for (Object result : resultStationsList) {
            
            List<Object> row = Arrays.asList((Object[])result);
            int rowIndex = excelSheetData.createRow();
            int columnIndex = 1;
            String stationID = null;
            Integer totalPayment  = 0;
            Double  totalQuantity = 0.0;
            
            for (Object data : row) {
                if (columnIndex == 1) {
                    stationID = data.toString();
                }
                
                if (columnIndex == 7) {
                }
                else {
                    if (columnIndex == 6) {
                        if (data == null) {
                            excelSheetData.addRowData("false", rowIndex);
                        }
                        else {
                            excelSheetData.addRowData(data.toString(), rowIndex);
                        }
                    }
                    else {
                        if (data == null) {
                            excelSheetData.addRowData("", rowIndex);
                        }
                        else {
                            excelSheetData.addRowData(data.toString(), rowIndex);
                        }                        
                    }
                }
                
                columnIndex++;
            }
            
            for (ArrayList<Object> rowPayment : resultPayment) {
                if (stationID.equals(rowPayment.get(0).toString())) {
                    Integer count    = Integer.parseInt(rowPayment.get(1).toString());
                    Double  quantity = Double.parseDouble(rowPayment.get(2).toString());
                    totalPayment  += count;
                    totalQuantity += quantity;
                    //break;
                }
            }
            
            String quantityString = numberFormat.format(totalQuantity);
            
            excelSheetData.addRowData(totalPayment.toString(), rowIndex);
            excelSheetData.addRowData(quantityString, rowIndex);
        }
        
    }

}