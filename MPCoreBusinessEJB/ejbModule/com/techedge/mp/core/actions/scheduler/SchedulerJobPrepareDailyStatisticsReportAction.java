package com.techedge.mp.core.actions.scheduler;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidRefuelModeType;
import com.techedge.mp.core.business.interfaces.postpaid.PostPaidSourceType;
import com.techedge.mp.core.business.model.StatisticalReportsBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobPrepareDailyStatisticsReportAction {
    private static final String JOB_NAME = "JOB_PREPARE_DAILY_STATISTICS_REPORT";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobPrepareDailyStatisticsReportAction() {}

    public String execute(EmailSenderRemote emailSender, String reportRecipient,String reportRecipientService, String reportLogoUrl, 
            String proxyHost, String proxyPort, String proxyNoHosts) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();

        Date now = new Date();

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("#,##0.000");
        
        NumberFormat integerFormat = NumberFormat.getNumberInstance(Locale.ITALIAN);
        integerFormat.setRoundingMode(RoundingMode.HALF_UP);
        integerFormat.setMinimumFractionDigits(0);
        integerFormat.setMaximumFractionDigits(0);

        System.out.println("job name: " + SchedulerJobPrepareDailyStatisticsReportAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {

            userTransaction.begin();

            SimpleDateFormat sdfFull = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            SimpleDateFormat sdfIdRif = new SimpleDateFormat("yyyyMMdd");
            
            Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
            System.out.println("DATA ATTUALE:" + sdfFull.format(calendar.getTime()));
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date dailyEndDate = calendar.getTime();

            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date dailyStartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.MONTH, Calendar.JANUARY);
            calendar.set(Calendar.YEAR, 2019);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date totalStartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 01);
            calendar.set(Calendar.MONTH, Calendar.DECEMBER);
            calendar.set(Calendar.YEAR, 2017);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date totalProgressiveStartDate = calendar.getTime();

            System.out.println("DATA GIORNALIERA INIZIO:        " + sdfFull.format(dailyStartDate));
            System.out.println("DATA GIORNALIERA FINE:          " + sdfFull.format(dailyEndDate));
            System.out.println("DATA TOTALI INIZIO:             " + sdfFull.format(totalStartDate));
            System.out.println("DATA TOTALI PROGRESSIVI INIZIO: " + sdfFull.format(totalProgressiveStartDate));
            
            //Elimino se esistono le precedenti estrazioni
            List<StatisticalReportsBean> listPrecedentiEstrazioni = QueryRepository.statisticsReportFindForType(em, sdfIdRif.format(dailyStartDate), "C");
            System.out.println("Precedenti estrazioni Customer size:"+listPrecedentiEstrazioni.size());
            for(StatisticalReportsBean ele:listPrecedentiEstrazioni)
            	em.remove(ele);

            
            System.out.println("Numero di Utenti LoyaltyCard");
            Long resultUsersLoyaltyCard = QueryRepository.statisticsReportSynthesisAllUsersLoyaltyCard(em, dailyEndDate, totalStartDate);
            
            System.out.println("Numero di Utenti LoyaltyCardVirtual");
            Long resultUsersLoyaltyCardVirtual = QueryRepository.statisticsReportSynthesisAllUsersLoyaltyCardVirtual(em, dailyEndDate, totalStartDate);
            
            System.out.println("Numero di Utenti UsersCreditCard");
            Long resultUsersCreditCard = QueryRepository.statisticsReportSynthesisAllUsersCreditCard(em, dailyEndDate, totalStartDate);
            
            System.out.println("Numero di Utenti UsersActive");
            Long resultUsersActive = QueryRepository.statisticsReportSynthesisAllUsersActive(em, dailyEndDate, totalStartDate);
            
            System.out.println("Numero di Utenti ENJOY");
            Long resultEnjoyUsersActive = QueryRepository.statisticsReportSynthesisAllUsersActiveBySource(em, dailyEndDate, totalStartDate, "ENJOY");
            
            System.out.println("Numero di Utenti MYCICERO");
            Long resultMyCiceroUsersActive = QueryRepository.statisticsReportSynthesisAllUsersActiveBySource(em, dailyEndDate, totalStartDate, "MYCICERO");
            
            System.out.println("Numero di Utenti FACEBOOK");
            Long resultFacebookUsersActive = QueryRepository.statisticsReportSynthesisAllSocialUsersActive(em, dailyEndDate, totalStartDate, "FACEBOOK");
            
            System.out.println("Numero di Utenti GOOGLE");
            Long resultGoogleUsersActive = QueryRepository.statisticsReportSynthesisAllSocialUsersActive(em, dailyEndDate, totalStartDate, "GOOGLE");
            
            System.out.println("Numero di Utenti Guest");
            Long resultGuestUsersActive = QueryRepository.statisticsReportSynthesisAllGuestUsersActive(em, dailyEndDate, totalStartDate);
            
            /*
             * Numero di transazioni (comprese quelle archiviate) Pre-Paid
             */
            
            System.out.println("Numero di transazioni (comprese quelle archiviate) Pre-Paid");
            
            Long resultPrePaidTransactions = QueryRepository.statisticsReportSynthesisAllPrePaidTransactions(em, dailyEndDate, totalStartDate);
            
            Long resultPrePaidTransactionsHistory = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsHistory(em, dailyEndDate, totalStartDate);

            Long resultPrePaidTransactionsCurrentAndHistory = Long.valueOf(resultPrePaidTransactions.intValue()+resultPrePaidTransactionsHistory.intValue());
            
            /*
             * Litri erogati (comprese quelle archiviate) Pre-Paid
             */
            
            
            System.out.println("Litri erogati (comprese quelle archiviate) Pre-Paid");
            
            Double resultPrePaidTransactionsFuelQuantity = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsFuelQuantity(em, dailyEndDate, totalStartDate);
            
            Double resultPrePaidTransactionsHistoryFuelQuantity = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsHistoryFuelQuantity(em, dailyEndDate, totalStartDate);

            Double resultPrePaidTransactionsCurrentAndHistoryFuelQuantity = Double.valueOf(resultPrePaidTransactionsFuelQuantity.doubleValue()+resultPrePaidTransactionsHistoryFuelQuantity.doubleValue());
            
            /*
             * Litri Diesel+ erogati (comprese quelle archiviate) Pre-Paid
             */
            String productDieselPiu = "Diesel+";
            
            System.out.println("Litri Diesel+ erogati (comprese quelle archiviate) Pre-Paid");
            
            Double resultPrePaidTransactionsFuelQuantityDieselPlus = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsFuelQuantityByProduct(em, dailyEndDate,
                    totalStartDate, productDieselPiu);
            
            Double resultPrePaidTransactionsHistoryFuelQuantityDieselPlus = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsHistoryFuelQuantityByProduct(em, dailyEndDate, 
                    totalStartDate, productDieselPiu);

            Double resultPrePaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu = Double.valueOf(resultPrePaidTransactionsFuelQuantityDieselPlus.doubleValue()
            		+resultPrePaidTransactionsHistoryFuelQuantityDieselPlus.doubleValue());
            
            /*
             *  Litri Diesel+ erogati (comprese quelle archiviate) Post-Paid
             */

            System.out.println("Litri Diesel+ erogati (comprese quelle archiviate) Post-Paid Self");
            
            Double resultPostPaidTransactionsFuelQuantityDieselPiu = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsFuelQuantityByProduct(em, dailyEndDate,
                    totalStartDate, productDieselPiu);

            Double resultPostPaidTransactionsHistoryFuelQuantityDieselPiu = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantityByProduct(em, dailyEndDate, 
            		totalStartDate, productDieselPiu);
            
            Double resultPostPaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu = Double.valueOf(resultPostPaidTransactionsFuelQuantityDieselPiu.doubleValue()
            		+resultPostPaidTransactionsHistoryFuelQuantityDieselPiu.doubleValue());
            
            /*
             * Totale litri Diesel+
             */
            System.out.println("Totale litri Diesel+");
            
            Double resultTransactionsTotalFuelQuantityDieselPiu = Double.valueOf(resultPrePaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.doubleValue()
            		+resultPostPaidTransactionsCurrentAndHistoryFuelQuantityDieselPiu.doubleValue());
            
            /*
             *  Numero di transazioni (comprese quelle archiviate) Post-Paid Self
             */

            System.out.println("Numero di transazioni (comprese quelle archiviate) Post-Paid Self");
            
            Long resultPostPaidTransactionsSelfServito = QueryRepository.statisticsReportSynthesisAllPostPaidTransactions(em, dailyEndDate, totalStartDate, 
            		PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());

            Long resultPostPaidTransactionsSelf = QueryRepository.statisticsReportSynthesisAllPostPaidTransactions(em, dailyEndDate, totalStartDate, 
            		PostPaidSourceType.SELF.getCode(), null);
            
            Long resultPostPaidTransactionsHistorySelfServito = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistory(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());

            Long resultPostPaidTransactionsHistorySelf = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistory(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), null);
            
            Long resultPostPaidTransactionsSelfServitoCurrentAndHistory = Long.valueOf(resultPostPaidTransactionsSelfServito.intValue()+resultPostPaidTransactionsHistorySelfServito.intValue());

            Long resultPostPaidTransactionsSelfCurrentAndHistory = Long.valueOf(resultPostPaidTransactionsSelf.intValue()+resultPostPaidTransactionsHistorySelf.intValue());

            /*
             *  Litri erogati (comprese quelle archiviate) Post-Paid Self
             */

            System.out.println("Litri erogati (comprese quelle archiviate) Post-Paid Self");
            
            Double resultPostPaidTransactionsSelfServitoFuelQuantity = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsFuelQuantity(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());

            Double resultPostPaidTransactionsSelfFuelQuantity = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsFuelQuantity(em, dailyEndDate, 
            		totalStartDate, PostPaidSourceType.SELF.getCode(), null);
            
            Double resultPostPaidTransactionsHistorySelfServitoFuelQuantity = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantity(em,dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), PostPaidRefuelModeType.SERVITO.getCode());

            Double resultPostPaidTransactionsHistorySelfFuelQuantity = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantity(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), null);
            
            Double resultPostPaidTransactionsSelfServitoCurrentAndHistoryFuelQuantity = Double.valueOf(resultPostPaidTransactionsSelfServitoFuelQuantity.doubleValue()+resultPostPaidTransactionsHistorySelfServitoFuelQuantity.doubleValue());

            Double resultPostPaidTransactionsSelfCurrentAndHistoryFuelQuantity = Double.valueOf(resultPostPaidTransactionsSelfFuelQuantity.doubleValue()+resultPostPaidTransactionsHistorySelfFuelQuantity.doubleValue());

            
            /*
             *  Numero di transazioni (comprese quelle archiviate) Post-Paid Shop
             */
            
            
            System.out.println("Numero di transazioni (comprese quelle archiviate) Post-Paid Shop");
            
            Long resultPostPaidTransactionsShop = QueryRepository.statisticsReportSynthesisAllPostPaidTransactions(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            Long resultPostPaidTransactionsHistoryShop = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistory(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            Long resultPostPaidTransactionsShopCurrentAndHistory = Long.valueOf(resultPostPaidTransactionsShop.intValue()+resultPostPaidTransactionsHistoryShop.intValue());
            
            /*
             *  Litri erogati (comprese quelle archiviate) Post-Paid Shop
             */
            
            
            System.out.println("Litri erogati (comprese quelle archiviate) Post-Paid Shop");
            
            Double resultPostPaidTransactionsShopFuelQuantity = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsFuelQuantity(em, dailyEndDate,
                   totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            Double resultPostPaidTransactionsHistoryShopFuelQuantity = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistoryFuelQuantity(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), null);

            Double resultPostPaidTransactionsShopCurrentAndHistoryFuelQuantity = Double.valueOf(resultPostPaidTransactionsShopFuelQuantity.doubleValue()+resultPostPaidTransactionsHistoryShopFuelQuantity.doubleValue());

            /*
             * Ammontare transazioni (comprese quelle archiviate) Pre-Paid
             */
            System.out.println("Ammontare transazioni (comprese quelle archiviate) Pre-Paid");
            
            Double resultPrePaidTotalAmount = QueryRepository.statisticsReportSynthesisAllPrePaidTotalAmount(em, dailyEndDate, totalStartDate);
            
            Double resultPrePaidTotalAmountHistory = QueryRepository.statisticsReportSynthesisAllPrePaidTotalAmountHistory(em, dailyEndDate, totalStartDate);
            
            /*
             * Ammontare transazioni (comprese quelle archiviate) Post-Paid
             */
            System.out.println("Ammontare transazioni (comprese quelle archiviate) Post-Paid");
            
            Double resultPostPaidTotalAmount = QueryRepository.statisticsReportSynthesisAllPostPaidTotalAmount(em, dailyEndDate, totalStartDate);
            
            Double resultPostPaidTotalAmountHistory = QueryRepository.statisticsReportSynthesisAllPostPaidTotalAmountHistory(em, dailyEndDate, totalStartDate);

            /*
             * Ammontare transazioni (comprese quelle archiviate) Pre-Paid e Post-Paid
             */
            System.out.println("Ammontare transazioni (comprese quelle archiviate) Pre-Paid e Post-Paid");
            
            Double resultTotalAmount = Double.valueOf(
            		resultPrePaidTotalAmount.doubleValue()
            		+resultPrePaidTotalAmountHistory.doubleValue()
            		+resultPostPaidTotalAmount.doubleValue()
            		+resultPostPaidTotalAmountHistory.doubleValue()
            		);
            
            /* Estrazione lista transazioni disattivata
            */
            
            Long resultDistinctUsersTransaction = QueryRepository.statisticsReportSynthesisAllDistinctUsersTransaction(em, dailyEndDate, totalProgressiveStartDate);
            
            Long resultStationsAppCount = QueryRepository.statisticsReportSynthesisAllStationsAppCount(em);
            Long resultStationsLoyaltyCount = QueryRepository.statisticsReportSynthesisAllStationsLoyaltyCount(em);
            
            Long resultDistinctUsersMyCicero = QueryRepository.statisticsReportSynthesisAllDistinctUsersMyCicero(em, dailyEndDate, totalProgressiveStartDate);
           
            Long resultTransactionsTotalCount = Long.valueOf(
            		resultPrePaidTransactionsCurrentAndHistory.intValue()+
            		resultPostPaidTransactionsSelfCurrentAndHistory.intValue()+
            		resultPostPaidTransactionsShopCurrentAndHistory.intValue());
            
            Double resultTransactionsTotalFuelQuantity = Double.valueOf(
            		resultPrePaidTransactionsCurrentAndHistoryFuelQuantity.doubleValue()+
            		resultPostPaidTransactionsSelfCurrentAndHistoryFuelQuantity.doubleValue()+
            		resultPostPaidTransactionsShopCurrentAndHistoryFuelQuantity.doubleValue()
            		);
            
            /*
             * Ammontare transazioni parking
             */
            System.out.println("Ammontare transazioni parking");
            
            Double resultParkingTotalAmount = QueryRepository.statisticsReportSynthesisAllParkingTotalAmount(em, dailyEndDate, totalStartDate);

            /*
             * Numero transazioni parking
             */
            System.out.println("Numero transazioni parking");
            
            Long resultParkingTransactionsTotalCount = QueryRepository.statisticsReportSynthesisAllParkingTransactions(em, dailyEndDate,totalStartDate);
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area NO
             */
            
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area NO");
            
            Long resultPrePaidTransactionsAreaNO = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsArea(em, dailyEndDate, 
            		totalStartDate, "Area NO");
            
            Long resultPrePaidTransactionsHistoryAreaNO = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsHistoryArea(em, dailyEndDate,
            		totalStartDate, "Area NO");
            
            Long resultPostPaidTransactionsSelfAreaNO = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NO");
            
            Long resultPostPaidTransactionsHistorySelfAreaNO = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistoryArea(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NO");
            
            Long resultPostPaidTransactionsShopAreaNO = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate,
            		totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NO");

            Long resultPostPaidTransactionsHistoryShopAreaNO = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistory(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NO");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaNO = Long.valueOf(resultPrePaidTransactionsAreaNO.intValue()
            		+resultPrePaidTransactionsHistoryAreaNO.intValue());

            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaNO = Long.valueOf(resultPostPaidTransactionsSelfAreaNO.intValue()
            		+resultPostPaidTransactionsHistorySelfAreaNO.intValue());
            
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaNO = Long.valueOf(resultPostPaidTransactionsShopAreaNO.intValue()
            		+resultPostPaidTransactionsHistoryShopAreaNO.intValue());
            
            Long resultTransactionsTotalCountAreaNO = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaNO.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaNO.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaNO.intValue());
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area NE
             */
            
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area NE");
            
            Long resultPrePaidTransactionsAreaNE = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsArea(em, dailyEndDate, 
                    totalStartDate, "Area NE");
            
            Long resultPrePaidTransactionsHistoryAreaNE = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area NE");
            
            Long resultPostPaidTransactionsSelfAreaNE = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate,
            		totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NE");
            
            Long resultPostPaidTransactionsHistorySelfAreaNE = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistoryArea(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area NE");
            
            Long resultPostPaidTransactionsShopAreaNE = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NE");

            Long resultPostPaidTransactionsHistoryShopAreaNE = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistory(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area NE");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaNE = Long.valueOf(resultPrePaidTransactionsAreaNE.intValue()
            		+resultPrePaidTransactionsHistoryAreaNE.intValue());
            
            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaNE = Long.valueOf(resultPostPaidTransactionsSelfAreaNE.intValue()
            		+resultPostPaidTransactionsHistorySelfAreaNE.intValue());
            
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaNE = Long.valueOf(resultPostPaidTransactionsShopAreaNE.intValue()
            		+resultPostPaidTransactionsHistoryShopAreaNE.intValue());
            
            Long resultTransactionsTotalCountAreaNE = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaNE.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaNE.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaNE.intValue());            
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area CN
             */
            
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area CN");
            
            Long resultPrePaidTransactionsAreaCN = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsArea(em, dailyEndDate, 
                    totalStartDate, "Area CN");
            
            Long resultPrePaidTransactionsHistoryAreaCN = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area CN");
            
            Long resultPostPaidTransactionsSelfAreaCN = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CN");
            
            Long resultPostPaidTransactionsHistorySelfAreaCN = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistoryArea(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CN");
            
            Long resultPostPaidTransactionsShopAreaCN = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CN");

            Long resultPostPaidTransactionsHistoryShopAreaCN = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistory(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CN");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaCN = Long.valueOf(resultPrePaidTransactionsAreaCN.intValue()
            		+resultPrePaidTransactionsHistoryAreaCN.intValue());

            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaCN = Long.valueOf(resultPostPaidTransactionsSelfAreaCN.intValue()
            		+resultPostPaidTransactionsHistorySelfAreaCN.intValue());
            
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaCN = Long.valueOf(resultPostPaidTransactionsShopAreaCN.intValue()
            		+resultPostPaidTransactionsHistoryShopAreaCN.intValue());
            
            Long resultTransactionsTotalCountAreaCN = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaCN.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaCN.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaCN.intValue());
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area C
             */
            
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area C");
            
            Long resultPrePaidTransactionsAreaC = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsArea(em, dailyEndDate,  
                    totalStartDate, "Area C");
            
            Long resultPrePaidTransactionsHistoryAreaC = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area C");
            
            Long resultPostPaidTransactionsSelfAreaC = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area C");
            
            Long resultPostPaidTransactionsHistorySelfAreaC = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistoryArea(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area C");
            
            Long resultPostPaidTransactionsShopAreaC = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate,
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area C");

            Long resultPostPaidTransactionsHistoryShopAreaC = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistory(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area C");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaC = Long.valueOf(resultPrePaidTransactionsAreaC.intValue()
            		+resultPrePaidTransactionsHistoryAreaC.intValue());

            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaC = Long.valueOf(resultPostPaidTransactionsSelfAreaC.intValue()
            		+resultPostPaidTransactionsHistorySelfAreaC.intValue());
           
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaC = Long.valueOf(resultPostPaidTransactionsShopAreaC.intValue()
            		+resultPostPaidTransactionsHistoryShopAreaC.intValue());
            
            Long resultTransactionsTotalCountAreaC = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaC.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaC.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaC.intValue());
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area CS
             */
            
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area CS");
            
            Long resultPrePaidTransactionsAreaCS = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsArea(em, dailyEndDate, 
                    totalStartDate, "Area CS");
            
            Long resultPrePaidTransactionsHistoryAreaCS = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area CS");
            
            Long resultPostPaidTransactionsSelfAreaCS = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CS");
            
            Long resultPostPaidTransactionsHistorySelfAreaCS = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistoryArea(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area CS");
            
            Long resultPostPaidTransactionsShopAreaCS = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CS");

            Long resultPostPaidTransactionsHistoryShopAreaCS = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistory(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area CS");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaCS = Long.valueOf(resultPrePaidTransactionsAreaCS.intValue()
            		+resultPrePaidTransactionsHistoryAreaCS.intValue());

            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaCS = Long.valueOf(resultPostPaidTransactionsSelfAreaCS.intValue()
            		+resultPostPaidTransactionsHistorySelfAreaCS.intValue());
            
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaCS = Long.valueOf(resultPostPaidTransactionsShopAreaCS.intValue()
            		+resultPostPaidTransactionsHistoryShopAreaCS.intValue());
             
            
            Long resultTransactionsTotalCountAreaCS = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaCS.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaCS.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaCS.intValue());
            
            /*
             * Ammontare transazioni payment (comprese quelle archiviate) Area S
             */
            
            System.out.println("Ammontare transazioni payment (comprese quelle archiviate) Area S");
            
            Long resultPrePaidTransactionsAreaS = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsArea(em, dailyEndDate,  
                    totalStartDate, "Area S");
            
            Long resultPrePaidTransactionsHistoryAreaS = QueryRepository.statisticsReportSynthesisAllPrePaidTransactionsHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area S");
            
            Long resultPostPaidTransactionsSelfAreaS = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area S");
            
            Long resultPostPaidTransactionsHistorySelfAreaS = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistoryArea(em, dailyEndDate, 
                    totalStartDate, PostPaidSourceType.SELF.getCode(), "Area S");
            
            Long resultPostPaidTransactionsShopAreaS = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsArea(em, dailyEndDate, 
            		totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area S");

            Long resultPostPaidTransactionsHistoryShopAreaS = QueryRepository.statisticsReportSynthesisAllPostPaidTransactionsHistory(em, dailyEndDate,  
                    totalStartDate, PostPaidSourceType.SHOP.getCode(), "Area S");


            Long resultPrePaidTransactionsCurrentAndHistoryAreaS = Long.valueOf(resultPrePaidTransactionsAreaS.intValue()
            		+resultPrePaidTransactionsHistoryAreaS.intValue());

            Long resultPostPaidTransactionsSelfCurrentAndHistoryAreaS = Long.valueOf(resultPostPaidTransactionsSelfAreaS.intValue()
            		+resultPostPaidTransactionsHistorySelfAreaS.intValue());
           
            
            Long resultPostPaidTransactionsShopCurrentAndHistoryAreaS = Long.valueOf(resultPostPaidTransactionsShopAreaS.intValue()
            		+resultPostPaidTransactionsHistoryShopAreaS.intValue());
             
            
            Long resultTransactionsTotalCountAreaS = Long.valueOf(resultPrePaidTransactionsCurrentAndHistoryAreaS.intValue()
            		+resultPostPaidTransactionsSelfCurrentAndHistoryAreaS.intValue()
            		+resultPostPaidTransactionsShopCurrentAndHistoryAreaS);
            
            System.out.println("DistinctUsersLoyalty");
            
            Long resultDistinctUsersLoyalty = QueryRepository.statisticsReportSynthesisAllDistinctUsersLoyalty(em, dailyEndDate, totalProgressiveStartDate);
            
            System.out.println("PostPaidTotalLoyalty");
            
            Long resultPostPaidTotalLoyaltyTransactions = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyalty(em, dailyEndDate, totalStartDate);
            
            System.out.println("PostPaidLoyaltyServito");
            
            Long resultPostPaidServitoLoyaltyTransactions = QueryRepository.statisticsReportSynthesisAllPostPaidLoyaltyRefuelmode(em, dailyEndDate, totalStartDate, "Servito");
            
            System.out.println("PostPaidLoyaltyFaiDaTe");
            
            Long resultPostPaidFaidateLoyaltyTransactions = QueryRepository.statisticsReportSynthesisAllPostPaidLoyaltyRefuelmode(em, dailyEndDate, totalStartDate, "Fai_da_te");
            
            System.out.println("PostPaidTotalLoyaltyHistory");
            
            Long resultPostPaidTotalLoyaltyTransactionsHistory = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyHistory(em, dailyEndDate, totalStartDate);
            
            System.out.println("PostPaidTotalLoyaltyServitoHistory");
            
            Long resultPostPaidServitoLoyaltyTransactionsHistory = QueryRepository.statisticsReportSynthesisAllPostPaidLoyaltyRefuelmodeHistory(em, dailyEndDate, 
                     totalStartDate, "Servito");
            
            System.out.println("PostPaidTotalLoyaltyFaiDaTeHistory");
            
            Long resultPostPaidFaidateLoyaltyTransactionsHistory = QueryRepository.statisticsReportSynthesisAllPostPaidLoyaltyRefuelmodeHistory(em, dailyEndDate, 
                    totalStartDate, "Fai_da_te");

            System.out.println("PrePaidTotalLoyalty");
            
            Long resultPrePaidTotalLoyaltyTransactions = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyalty(em, dailyEndDate, totalStartDate);

            System.out.println("PrePaidTotalLoyaltyHistory");
            
            Long resultPrePaidTotalLoyaltyTransactionsHistory = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyHistory(em, dailyEndDate, totalStartDate);

            System.out.println("EventNotificationTotalLoyalty");
            
            Long resultEventNotificationTransactionsLoyalty = QueryRepository.statisticsReportSynthesisAllEventNotificationTotalLoyalty(em, dailyEndDate, totalStartDate);

            System.out.println("EventNotificationTotalLoyaltyServito");
            
            Long resultEventNotificationServitoTransactionsLoyalty = QueryRepository.statisticsReportSynthesisAllEventNotificationLoyaltyRefuelmode(em, dailyEndDate, 
                    totalStartDate, "S");
            
            System.out.println("EventNotificationTotalLoyaltyFaiDaTe");
            
            Long resultEventNotificationIperselfTransactionsLoyalty = QueryRepository.statisticsReportSynthesisAllEventNotificationLoyaltyRefuelmode(em, dailyEndDate, 
                     totalStartDate, "F");
            
            System.out.println("EventNotificationTotalLoyaltyNonOil");
            
            Long resultEventNotificationNonOilTransactionsLoyalty = QueryRepository.statisticsReportSynthesisAllEventNotificationLoyaltyNonOil(em, dailyEndDate, totalStartDate);
            
            System.out.println("EventNotificationTotalLoyaltyOilAndNonOil");
            
            Long resultEventNotificationOilAndNonOilTransactionsLoyalty = QueryRepository.statisticsReportSynthesisAllEventNotificationLoyaltyOilAndNonOil(em, dailyEndDate, 
                    totalStartDate);
            
            System.out.println("EventNotification elab 1");
            
            Long resultTotalUsersLoyaltyTransactions = Long.valueOf(resultEventNotificationTransactionsLoyalty.intValue()
            		+resultPostPaidTotalLoyaltyTransactions.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsHistory.intValue()
            		+resultPrePaidTotalLoyaltyTransactions.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsHistory.intValue());
            
            System.out.println("EventNotification elab 2");
            
            Long resultServitoUsersLoyaltyTransactions = Long.valueOf(resultEventNotificationServitoTransactionsLoyalty.intValue()
            		+resultPostPaidServitoLoyaltyTransactions.intValue()
            		+resultPostPaidServitoLoyaltyTransactionsHistory.intValue());

            System.out.println("EventNotification elab 3");
            
            Long resultIperselfUsersLoyaltyTransactions = Long.valueOf(resultEventNotificationIperselfTransactionsLoyalty.intValue()
            		+resultPostPaidFaidateLoyaltyTransactions.intValue()
            		+resultPostPaidFaidateLoyaltyTransactionsHistory.intValue()
            		+resultPrePaidTotalLoyaltyTransactions.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsHistory.intValue()
            		);
            
            System.out.println("EventNotification elab 4");
            
            Long resultNonOilUsersLoyaltyTransactions = Long.valueOf(resultEventNotificationNonOilTransactionsLoyalty.intValue()
            		-resultEventNotificationOilAndNonOilTransactionsLoyalty.intValue());

            /*
             * Ammontare transazioni loyalty (comprese quelle archiviate) Area NO
             */
            
            System.out.println("Ammontare transazioni loyalty (comprese quelle archiviate) Area NO");
            
            Long resultPostPaidTotalLoyaltyTransactionsAreaNO = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyArea(em, dailyEndDate, 
                     totalStartDate, "Area NO");

            Long resultPostPaidTotalLoyaltyTransactionsHistoryAreaNO = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area NO");

            Long resultPrePaidTotalLoyaltyTransactionsAreaNO = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area NO");

            Long resultPrePaidTotalLoyaltyTransactionsHistoryAreaNO = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area NO");

            Long resultEventNotificationTransactionsLoyaltyAreaNO = QueryRepository.statisticsReportSynthesisAllEventNotificationTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area NO");

            
            Long resultTotalUsersLoyaltyTransactionsAreaNO = Long.valueOf(resultEventNotificationTransactionsLoyaltyAreaNO.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsAreaNO.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsHistoryAreaNO.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsAreaNO.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsHistoryAreaNO.intValue());
            
            /*
             * Ammontare transazioni loyalty (comprese quelle archiviate) Area NE
             */
            
            System.out.println("Ammontare transazioni loyalty (comprese quelle archiviate) Area NE");
            
            Long resultPostPaidTotalLoyaltyTransactionsAreaNE = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area NE");

            Long resultPostPaidTotalLoyaltyTransactionsHistoryAreaNE = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area NE");

            Long resultPrePaidTotalLoyaltyTransactionsAreaNE = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area NE");

            Long resultPrePaidTotalLoyaltyTransactionsHistoryAreaNE = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area NE");

            Long resultEventNotificationTransactionsLoyaltyAreaNE = QueryRepository.statisticsReportSynthesisAllEventNotificationTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area NE");

            
            Long resultTotalUsersLoyaltyTransactionsAreaNE = Long.valueOf(resultEventNotificationTransactionsLoyaltyAreaNE.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsAreaNE.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsHistoryAreaNE.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsAreaNE.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsHistoryAreaNE.intValue());
            
            /*
             * Ammontare transazioni loyalty (comprese quelle archiviate) Area CN
             */
            
            System.out.println("Ammontare transazioni loyalty (comprese quelle archiviate) Area CN");
            
            Long resultPostPaidTotalLoyaltyTransactionsAreaCN = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area CN");

            Long resultPostPaidTotalLoyaltyTransactionsHistoryAreaCN = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area CN");

            Long resultPrePaidTotalLoyaltyTransactionsAreaCN = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area CN");

            Long resultPrePaidTotalLoyaltyTransactionsHistoryAreaCN = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area CN");

            Long resultEventNotificationTransactionsLoyaltyAreaCN = QueryRepository.statisticsReportSynthesisAllEventNotificationTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area CN");

            
            Long resultTotalUsersLoyaltyTransactionsAreaCN = Long.valueOf(resultEventNotificationTransactionsLoyaltyAreaCN.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsAreaCN.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsHistoryAreaCN.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsAreaCN.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsHistoryAreaCN.intValue());            
            
            /*
             * Ammontare transazioni loyalty (comprese quelle archiviate) Area C
             */
            
            System.out.println("Ammontare transazioni loyalty (comprese quelle archiviate) Area C");
            
            Long resultPostPaidTotalLoyaltyTransactionsAreaC = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyArea(em, dailyEndDate, 
                   totalStartDate, "Area C");

            Long resultPostPaidTotalLoyaltyTransactionsHistoryAreaC = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area C");

            Long resultPrePaidTotalLoyaltyTransactionsAreaC = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area C");

            Long resultPrePaidTotalLoyaltyTransactionsHistoryAreaC = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area C");

            Long resultEventNotificationTransactionsLoyaltyAreaC = QueryRepository.statisticsReportSynthesisAllEventNotificationTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area C");

            
            Long resultTotalUsersLoyaltyTransactionsAreaC = Long.valueOf(resultEventNotificationTransactionsLoyaltyAreaC.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsAreaC.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsHistoryAreaC.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsAreaC.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsHistoryAreaC.intValue());
            
            /*
             * Ammontare transazioni loyalty (comprese quelle archiviate) Area CS
             */
            
            System.out.println("Ammontare transazioni loyalty (comprese quelle archiviate) Area CS");
            
            Long resultPostPaidTotalLoyaltyTransactionsAreaCS = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area CS");

            Long resultPostPaidTotalLoyaltyTransactionsHistoryAreaCS = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area CS");

            Long resultPrePaidTotalLoyaltyTransactionsAreaCS = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area CS");

            Long resultPrePaidTotalLoyaltyTransactionsHistoryAreaCS = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area CS");

            Long resultEventNotificationTransactionsLoyaltyAreaCS = QueryRepository.statisticsReportSynthesisAllEventNotificationTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area CS");

            
            Long resultTotalUsersLoyaltyTransactionsAreaCS = Long.valueOf(resultEventNotificationTransactionsLoyaltyAreaCS.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsAreaCS.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsHistoryAreaCS.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsAreaCS.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsHistoryAreaCS.intValue());
            
            /*
             * Ammontare transazioni loyalty (comprese quelle archiviate) Area S
             */
            
            System.out.println("Ammontare transazioni loyalty (comprese quelle archiviate) Area S");
            
            Long resultPostPaidTotalLoyaltyTransactionsAreaS = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area S");

            Long resultPostPaidTotalLoyaltyTransactionsHistoryAreaS = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area S");

            Long resultPrePaidTotalLoyaltyTransactionsAreaS = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area S");

            Long resultPrePaidTotalLoyaltyTransactionsHistoryAreaS = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyHistoryArea(em, dailyEndDate, 
                    totalStartDate, "Area S");

            Long resultEventNotificationTransactionsLoyaltyAreaS = QueryRepository.statisticsReportSynthesisAllEventNotificationTotalLoyaltyArea(em, dailyEndDate, 
                    totalStartDate, "Area S");

            
            Long resultTotalUsersLoyaltyTransactionsAreaS = Long.valueOf(resultEventNotificationTransactionsLoyaltyAreaS.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsAreaS.intValue()
            		+resultPostPaidTotalLoyaltyTransactionsHistoryAreaS.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsAreaS.intValue()
            		+resultPrePaidTotalLoyaltyTransactionsHistoryAreaS.intValue());

            
            Long resultEventNotificationTotalLoyaltyCredits = QueryRepository.statisticsReportSynthesisAllEventNotificationTotalLoyaltyCredits(em, dailyEndDate, 
                    totalStartDate);
            
            Long resultPostPaidTotalLoyaltyCredits = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyCredits(em, dailyEndDate, totalStartDate);
            
            Long resultPostPaidTotalLoyaltyCreditsHistory = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyCreditsHistory(em, dailyEndDate, totalStartDate);
            
            Long resultPrePaidTotalLoyaltyCredits = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyCredits(em, dailyEndDate, totalStartDate);

            Long resultPrePaidTotalLoyaltyCreditsHistory = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyCreditsHistory(em, dailyEndDate, totalStartDate);

            Long resultTotalUsersLoyaltyCredits = Long.valueOf(resultEventNotificationTotalLoyaltyCredits.intValue()
            +resultPostPaidTotalLoyaltyCredits.intValue()
            +resultPostPaidTotalLoyaltyCreditsHistory.intValue()
            +resultPrePaidTotalLoyaltyCredits.intValue()
            +resultPrePaidTotalLoyaltyCreditsHistory.intValue());

            /*
             * Totale litri erogati loyalty
             */
            
            System.out.println("Fuel Quantity loyalty");
            
            Double resultEventNotificationTotalLoyaltyFuelQuantity = QueryRepository.statisticsReportSynthesisAllEventNotificationTotalLoyaltyFuelQuantity(em, dailyEndDate, 
                    totalStartDate);
            
            Double resultPostPaidTotalLoyaltyFuelQuantity = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyFuelQuantity(em, dailyEndDate, 
                    totalStartDate);
            
            Double resultPostPaidTotalLoyaltyHistoryFuelQuantity = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyHistoryFuelQuantity(em, dailyEndDate, 
                    totalStartDate);
            
            Double resultPrePaidTotalLoyaltyFuelQuantity = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyFuelQuantity(em, dailyEndDate, totalStartDate);

            Double resultPrePaidTotalLoyaltyHistoryFuelQuantity = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyHistoryFuelQuantity(em, dailyEndDate, 
                    totalStartDate);

            
            Double resultTotalUsersLoyaltyFuelQuantity = Double.valueOf(resultEventNotificationTotalLoyaltyFuelQuantity.doubleValue()
            		+resultPostPaidTotalLoyaltyFuelQuantity.doubleValue()
            		+resultPostPaidTotalLoyaltyHistoryFuelQuantity.doubleValue()
            		+resultPrePaidTotalLoyaltyFuelQuantity.doubleValue()
            		+resultPrePaidTotalLoyaltyHistoryFuelQuantity.doubleValue());
            
            /*
             * Totale litri erogati loyalty Diesel+
             */
            
            System.out.println("Fuel Quantity loyalty Diesel+");
            
            String productDieselPiuEventnotification="Eni Diesel +";
            
            Double resultEventNotificationTotalLoyaltyFuelQuantityDieselPiu = QueryRepository.statisticsReportSynthesisAllEventNotificationTotalLoyaltyFuelQuantityByProduct(em, dailyEndDate, 
                    totalStartDate, productDieselPiuEventnotification);
            
            
            Double resultPostPaidTotalLoyaltyFuelQuantityDieselPiu = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyFuelQuantityByProduct(em, dailyEndDate, 
                    totalStartDate, productDieselPiu);
            
            Double resultPostPaidTotalLoyaltyHistoryFuelQuantityDieselPiu = QueryRepository.statisticsReportSynthesisAllPostPaidTotalLoyaltyHistoryFuelQuantityByProduct(em, dailyEndDate, 
                    totalStartDate, productDieselPiu);
            
            Double resultPrePaidTotalLoyaltyFuelQuantityDieselPiu = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyFuelQuantityByProduct(em, dailyEndDate, 
            		totalStartDate, productDieselPiu);

            Double resultPrePaidTotalLoyaltyHistoryFuelQuantityDieselPiu = QueryRepository.statisticsReportSynthesisAllPrePaidTotalLoyaltyHistoryFuelQuantityByProduct(em, dailyEndDate, 
                    totalStartDate, productDieselPiu);
            
            Double resultTotalUsersLoyaltyFuelQuantityDieselPiu = Double.valueOf(resultEventNotificationTotalLoyaltyFuelQuantityDieselPiu.doubleValue()
            		+resultPostPaidTotalLoyaltyFuelQuantityDieselPiu.doubleValue()
            		+resultPostPaidTotalLoyaltyHistoryFuelQuantityDieselPiu.doubleValue()
            		+resultPrePaidTotalLoyaltyFuelQuantityDieselPiu.doubleValue()
            		+resultPrePaidTotalLoyaltyHistoryFuelQuantityDieselPiu.doubleValue());
            
            System.out.println("prepaid:           " + resultPrePaidTotalLoyaltyFuelQuantityDieselPiu);
            System.out.println("prepaid h:         " + resultPrePaidTotalLoyaltyHistoryFuelQuantityDieselPiu);
            System.out.println("postpaid:          " + resultPostPaidTotalLoyaltyFuelQuantityDieselPiu);
            System.out.println("postpaid h:        " + resultPostPaidTotalLoyaltyHistoryFuelQuantityDieselPiu);
            System.out.println("eventnotification: " + resultEventNotificationTotalLoyaltyFuelQuantityDieselPiu);
            
            
            
            
            
            
		  //resultUsersActive
		  //resultEnjoyUsersActive
		  //resultFacebookUsersActive
		  //resultGoogleUsersActive
		  //resultMyCiceroUsersActive
		  //resultGuestUsersActive
		  //resultUsersCreditCard
		  //resultUsersLoyaltyCard
		  //resultUsersLoyaltyCardVirtual
		  //
		  //resultDistinctUsersTransaction
		  //resultDistinctUsersLoyalty
		  //resultDistinctUsersMyCicero
		  //
		  //resultTransactionsTotalCount
		  //resultTotalAmount
		  //resultTransactionsTotalFuelQuantity
          //resultTransactionsTotalFuelQuantityDieselPiu
		  //resultPostPaidTransactionsSelfServitoCurrentAndHistory
		  //
		  //resultTotalUsersLoyaltyTransactions
		  //resultServitoUsersLoyaltyTransactions
		  //resultIperselfUsersLoyaltyTransactions
		  //resultNonOilUsersLoyaltyTransactions
		  //resultTotalUsersLoyaltyCredits
		  //resultTotalUsersLoyaltyFuelQuantity
          //resultTotalUsersLoyaltyFuelQuantityDieselPiu
		  //
		  //resultParkingTransactionsTotalCount
		  //resultParkingTotalAmount
		  //
		  //resultTotalUsersLoyaltyTransactionsAreaNO
		  //resultTotalUsersLoyaltyTransactionsAreaNE
		  //resultTotalUsersLoyaltyTransactionsAreaCN
		  //resultTotalUsersLoyaltyTransactionsAreaC
		  //resultTotalUsersLoyaltyTransactionsAreaCS
		  //resultTotalUsersLoyaltyTransactionsAreaS
		  //
		  //resultTransactionsTotalCountAreaNO
		  //resultTransactionsTotalCountAreaNE
		  //resultTransactionsTotalCountAreaCN
		  //resultTransactionsTotalCountAreaC
		  //resultTransactionsTotalCountAreaCS
		  //resultTransactionsTotalCountAreaS
		  //
		  //resultStationsAppCount
		  //resultStationsLoyaltyCount
		
            Map<String, String> mapStatistical = new HashMap<>();
		
		  	mapStatistical.put("UsersActive", resultUsersActive.toString());
		  	mapStatistical.put("EnjoyUsersActive", resultEnjoyUsersActive.toString());
		  	mapStatistical.put("FacebookUsersActive", resultFacebookUsersActive.toString());
		  	mapStatistical.put("GoogleUsersActive", resultGoogleUsersActive.toString());
		  	mapStatistical.put("MyCiceroUsersActive", resultMyCiceroUsersActive.toString());
		  	mapStatistical.put("GuestUsersActive", resultGuestUsersActive.toString());
		  	mapStatistical.put("UsersCreditCard", resultUsersCreditCard.toString());
		  	mapStatistical.put("UsersLoyaltyCard", resultUsersLoyaltyCard.toString());
		  	mapStatistical.put("UsersLoyaltyCardVirtual", resultUsersLoyaltyCardVirtual.toString());
		
		  	mapStatistical.put("DistinctUsersTransaction", resultDistinctUsersTransaction.toString());
		  	mapStatistical.put("DistinctUsersLoyalty", resultDistinctUsersLoyalty.toString());
		  	mapStatistical.put("DistinctUsersMyCicero", resultDistinctUsersMyCicero.toString());
		
		  	mapStatistical.put("TransactionsTotalCount", resultTransactionsTotalCount.toString());
		  	mapStatistical.put("TotalAmount", resultTotalAmount.toString());
		  	mapStatistical.put("TransactionsTotalFuelQuantity", resultTransactionsTotalFuelQuantity.toString());
		  	mapStatistical.put("TransactionsTotalFuelQuantityDieselPiu", resultTransactionsTotalFuelQuantityDieselPiu.toString());
		  	mapStatistical.put("PostPaidTransactionsSelfServitoCurrentAndHistory", resultPostPaidTransactionsSelfServitoCurrentAndHistory.toString());
		
		  	mapStatistical.put("TotalUsersLoyaltyTransactions", resultTotalUsersLoyaltyTransactions.toString());
		  	mapStatistical.put("ServitoUsersLoyaltyTransactions", resultServitoUsersLoyaltyTransactions.toString());
		  	mapStatistical.put("IperselfUsersLoyaltyTransactions", resultIperselfUsersLoyaltyTransactions.toString());
		  	mapStatistical.put("NonOilUsersLoyaltyTransactions", resultNonOilUsersLoyaltyTransactions.toString());
		  	mapStatistical.put("TotalUsersLoyaltyCredits", resultTotalUsersLoyaltyCredits.toString());
		  	mapStatistical.put("TotalUsersLoyaltyFuelQuantity", resultTotalUsersLoyaltyFuelQuantity.toString());
		  	mapStatistical.put("TotalUsersLoyaltyFuelQuantityDieselPiu", resultTotalUsersLoyaltyFuelQuantityDieselPiu.toString());
		
		  	mapStatistical.put("ParkingTransactionsTotalCount", resultParkingTransactionsTotalCount.toString());
		  	mapStatistical.put("ParkingTotalAmount", resultParkingTotalAmount.toString());
		
		  	mapStatistical.put("TotalUsersLoyaltyTransactionsAreaNO", resultTotalUsersLoyaltyTransactionsAreaNO.toString());
		  	mapStatistical.put("TotalUsersLoyaltyTransactionsAreaNE", resultTotalUsersLoyaltyTransactionsAreaNE.toString());
		  	mapStatistical.put("TotalUsersLoyaltyTransactionsAreaCN", resultTotalUsersLoyaltyTransactionsAreaCN.toString());
		  	mapStatistical.put("TotalUsersLoyaltyTransactionsAreaC", resultTotalUsersLoyaltyTransactionsAreaC.toString());
		  	mapStatistical.put("TotalUsersLoyaltyTransactionsAreaCS", resultTotalUsersLoyaltyTransactionsAreaCS.toString());
		  	mapStatistical.put("TotalUsersLoyaltyTransactionsAreaS", resultTotalUsersLoyaltyTransactionsAreaS.toString());
		
		  	mapStatistical.put("TransactionsTotalCountAreaNO", resultTransactionsTotalCountAreaNO.toString());
		  	mapStatistical.put("TransactionsTotalCountAreaNE", resultTransactionsTotalCountAreaNE.toString());
		  	mapStatistical.put("TransactionsTotalCountAreaCN", resultTransactionsTotalCountAreaCN.toString());
		  	mapStatistical.put("TransactionsTotalCountAreaC", resultTransactionsTotalCountAreaC.toString());
		  	mapStatistical.put("TransactionsTotalCountAreaCS", resultTransactionsTotalCountAreaCS.toString());
		  	mapStatistical.put("TransactionsTotalCountAreaS", resultTransactionsTotalCountAreaS.toString());
		
		  	mapStatistical.put("StationsAppCount", resultStationsAppCount.toString());
		  	mapStatistical.put("StationsLoyaltyCount", resultStationsLoyaltyCount.toString());
		  
			for (Map.Entry<String,String> entry : mapStatistical.entrySet()) {
			      String key = entry.getKey();
			     String value = entry.getValue();
			    
			    StatisticalReportsBean statisticalReportsBean = new StatisticalReportsBean(sdfIdRif.format(dailyStartDate), dailyStartDate, Calendar.getInstance(Locale.ITALIAN).getTime(), "C", "T", key, value);
			    em.persist(statisticalReportsBean);
			}
            
            userTransaction.commit();
            
            System.out.println("Commit eseguita");
            
            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");
            
            sendingServiceMail(emailSender, "Preparazione report giornaliero ", reportRecipientService, sdfIdRif.format(dailyStartDate), "Elaborazione terminata con successo.");
            
            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
    
    private void sendingServiceMail(EmailSenderRemote emailSender, String message, String reportRecipient, String dateExtraction, String detail){
    	
    	EmailType emailType = EmailType.ERROR_DAILY_STATISTICS_REPORT_V2;
    	
    	List<Parameter> parameters = new ArrayList<Parameter>(0);
    	parameters.add(new Parameter("MESSAGE", message));
    	parameters.add(new Parameter("TYPE", "CUSTOMER"));
        parameters.add(new Parameter("DATE", dateExtraction));
        parameters.add(new Parameter("DETAIL", detail));
        
    	System.out.println("Sending email to: " + reportRecipient);
        String sendEmailResult = emailSender.sendEmail(emailType, reportRecipient, parameters);
        System.out.println("SendEmailResult: " + sendEmailResult);
    }
    
}