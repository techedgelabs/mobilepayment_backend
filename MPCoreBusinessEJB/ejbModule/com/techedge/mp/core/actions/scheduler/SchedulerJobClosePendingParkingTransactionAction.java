package com.techedge.mp.core.actions.scheduler;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.ParkingTransactionV2Service;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.parking.EndParkingResult;
import com.techedge.mp.core.business.model.parking.ParkingTransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobClosePendingParkingTransactionAction {

    private static final String JOB_NAME = "JOB_CLOSE_PENDING_PARKING_TRANSACTION";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobClosePendingParkingTransactionAction() {}

    public String execute(Integer maxIntervalPendingParkingTransaction, ParkingTransactionV2Service parkingTransactionV2Service) throws EJBException {

        Date now = new Date();

        System.out.println("job name: " + SchedulerJobClosePendingParkingTransactionAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            // Ricerca delle transazioni di sosta terminate da maxIntervalPendingParkingTransaction ma ancora in stato STARTED
            List<ParkingTransactionBean> parkingTransactionBeanList = QueryRepository.findParkingTransactionToBeClosed(em, maxIntervalPendingParkingTransaction);

            // Per ogni transazione trovata richiama il servizio per effettuare la chiusura
            if (parkingTransactionBeanList != null && !parkingTransactionBeanList.isEmpty()) {

                for (ParkingTransactionBean parkingTransactionBean : parkingTransactionBeanList) {

                    Date requestTimestamp = new Date();
                    String requestId = String.valueOf(requestTimestamp.getTime());

                    EndParkingResult endParkingResult = parkingTransactionV2Service.endParking(null, requestId, parkingTransactionBean.getParkingTransactionId());

                    System.out.print("Transazione di sosta " + parkingTransactionBean.getParkingTransactionId());

                    if (endParkingResult != null) {

                        if (endParkingResult.getStatusCode().equals(ResponseHelper.PARKING_END_PARKING_SUCCESS)) {

                            System.out.println("Sosta terminata con successo");
                        }
                        else {

                            System.out.println("Errore nella chiusura della sosta: " + endParkingResult.getStatusCode());
                        }
                    }
                    else {

                        System.out.println("Errore nella chiusura della sosta: endParkingResult null");
                    }
                }
            }

            userTransaction.commit();

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
}