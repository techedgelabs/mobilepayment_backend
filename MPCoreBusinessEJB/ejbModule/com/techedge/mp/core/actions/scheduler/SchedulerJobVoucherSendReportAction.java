package com.techedge.mp.core.actions.scheduler;

import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.ManagerBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelSheetData;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobVoucherSendReportAction {

    private static final String JOB_NAME        = "JOB_VOUCHER_SEND_REPORT";
    /*private static final String FIELD_SEPARATOR = ";";
    private static final String LINE_SEPARATOR  = "\n";
    private static final String TABULATOR       = "\t";*/

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobVoucherSendReportAction() {}

    public String execute(EmailSenderRemote emailSender) throws EJBException {

        Date now = new Date();

        System.out.println("job name: " + SchedulerJobVoucherSendReportAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());
        
        // Calcolo del periodo di ricerca
        
        // La fine del periodo di ricerca � pari alla mezzanotte del giorno attuale
        Calendar calendar = new GregorianCalendar();
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date intervalEndDate = calendar.getTime();

        // La fine del periodo di ricerca � pari alla mezzanotte del giorno precedente
        calendar.add(Calendar.DAY_OF_MONTH, -1);
        //calendar.add(Calendar.DAY_OF_MONTH, -100);
        Date intervalStartDate = calendar.getTime();
        
        // Da rimuovere
        intervalEndDate = new Date();

        System.out.println("Periodo di ricerca: " + intervalStartDate.getTime() + " - " + intervalEndDate.getTime());
        
        UserTransaction userTransaction = context.getUserTransaction();

        try {
            
            userTransaction.begin();
            
            DecimalFormat df = new DecimalFormat("0.00");
            
            Integer count = 0;
                       
            List<StationBean> stationBeanList = QueryRepository.getAllStationBeans(em);
            
            for(StationBean stationBean : stationBeanList) {
            
                System.out.println("Elaborazione stazione " + stationBean.getStationID());
                
                // Se la stazione non ha gestori associati, allora va esclusa dall'eleaborazione
                if ( stationBean.getManagers().isEmpty() ) {
                    System.out.println("La stazione " + stationBean.getStationID() + " non ha gestori associati");
                    continue;
                }
                
                // Conta il numero di voucher consumati per la stazione in esame
                count = 0;
                
                //New Workbook
                ExcelWorkBook ewb = new ExcelWorkBook();
     
                //Cell style for header row
                ExcelCellStyle csHeader = ewb.createCellStyle(true, 12, Color.WHITE, new Color(84, 130, 53), null);                
                //Cell style for table row
                ExcelCellStyle csTable = ewb.createCellStyle(false, 12, Color.BLACK, new Color(230, 230, 230), null);
     
                String[] columnsHeading = new String[] {
                   "Transazione MP", "Operation ID", "Timestamp", "Voucher", "Stato", "Tipo",
                   "Valore", "Valore iniziale", "Valore consumato",  "Valore finale"    
                };
                
                
                // Per ogni stazione estrai tutte le transazioni postpaid in stato PAID
                List<PostPaidTransactionBean> postPaidTransactionBeanList = QueryRepository.findPostPaidTransactionBeanByStationBeanAndDate(em, stationBean, intervalStartDate, intervalEndDate);
                
                System.out.println("Trovate " + postPaidTransactionBeanList.size() + " transazioni da elaborare");
                
//                ArrayList<ArrayList<String>> excelData = new ArrayList<ArrayList<String>>(0);
                
                ExcelSheetData excelData = ewb.createSheetData();

                
                for(PostPaidTransactionBean postPaidTransactionBean : postPaidTransactionBeanList) {
                    int rowIndex = excelData.createRow();

                    // Se la transazione non � in stato PAID va esclusa dall'elaborazione
                    if ( !postPaidTransactionBean.getMpTransactionStatus().equals("PAID") ) {
                        continue;
                    }
                    
                    ArrayList<String> rowData = new ArrayList<String>(0);
                    
                    for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidTransactionBean.getPostPaidConsumeVoucherBeanList()) {
                        
                        System.out.println("OperationID: " + postPaidConsumeVoucherBean.getOperationID() + "total consumed: " + postPaidConsumeVoucherBean.getTotalConsumed());
                        
                        // Se alla transazione � associato un PostPaidConsumeVoucher con consumedValue > 0
                        if ( postPaidConsumeVoucherBean.getTotalConsumed() > 0.0 ) {
                            
                            System.out.println("Added OperationID: " + postPaidConsumeVoucherBean.getOperationID() + " to report");
                            count++;
                            // Crea un allegato con i voucher consumati
                            for (PostPaidConsumeVoucherDetailBean postPaidConsumeVoucherDetailBean : postPaidConsumeVoucherBean.getPostPaidConsumeVoucherDetailBean()) {
                             
//                                rowData.add(postPaidTransactionBean.getMpTransactionID());
//                                rowData.add(postPaidConsumeVoucherBean.getOperationID());
//                                rowData.add(postPaidConsumeVoucherBean.getRequestTimestamp().toString());
//                                rowData.add(postPaidConsumeVoucherDetailBean.getVoucherCode());
//                                rowData.add(postPaidConsumeVoucherDetailBean.getVoucherStatus());
//                                rowData.add(postPaidConsumeVoucherDetailBean.getVoucherType());
//                                rowData.add(df.format( postPaidConsumeVoucherDetailBean.getVoucherValue()));
//                                rowData.add(df.format( postPaidConsumeVoucherDetailBean.getInitialValue()));
//                                rowData.add(df.format( postPaidConsumeVoucherDetailBean.getConsumedValue()));
//                                rowData.add(df.format( postPaidConsumeVoucherDetailBean.getVoucherBalanceDue()));
//                                
//                                excelData.add(rowData);
                                
                                excelData.addRowData(postPaidTransactionBean.getMpTransactionID(), rowIndex);
                                excelData.addRowData(postPaidConsumeVoucherBean.getOperationID(), rowIndex);
                                excelData.addRowData(postPaidConsumeVoucherBean.getRequestTimestamp().toString(), rowIndex);
                                excelData.addRowData(postPaidConsumeVoucherDetailBean.getVoucherCode(), rowIndex);
                                excelData.addRowData(postPaidConsumeVoucherDetailBean.getVoucherStatus(), rowIndex);
                                excelData.addRowData(postPaidConsumeVoucherDetailBean.getVoucherType(), rowIndex);
                                excelData.addRowData(df.format( postPaidConsumeVoucherDetailBean.getVoucherValue()), rowIndex);
                                excelData.addRowData(df.format( postPaidConsumeVoucherDetailBean.getInitialValue()), rowIndex);
                                excelData.addRowData(df.format( postPaidConsumeVoucherDetailBean.getConsumedValue()).toUpperCase(), rowIndex);
                                excelData.addRowData(df.format( postPaidConsumeVoucherDetailBean.getVoucherBalanceDue()).toUpperCase(), rowIndex);
                            }
                            
                        }
                    }
                }
                
                if (count == 0) {
                    continue;
                }
                
                ewb.addSheet("report", columnsHeading, excelData, csHeader, csTable);
                
                /*
                if ( count > 0 ) {
                    FileOutputStream out = new FileOutputStream("C:\\voucher_report_" + stationBean.getStationID() + ".xlsx");
                    out.write(ewb.getBytesToStream());
                    out.close();
                }
                */
                
                // Crea l'email con il report allegato
                
                SimpleDateFormat sdf = new SimpleDateFormat();
                String intervalBegin = sdf.format(intervalStartDate);
                String intervalEnd = sdf.format(intervalEndDate);
                sdf.applyPattern("dd/MM/yy HH:mm:ss");
                
                String stationAddress = stationBean.getAddress() + ", " + stationBean.getCity() + " (" + stationBean.getProvince() + "), " + stationBean.getCountry();
                
                System.out.println("Impianto: " + stationAddress);
                System.out.println("Count: " + count);
                System.out.println("Periodo di ricerca: " + intervalBegin + " - " + intervalEnd);
                //System.out.println(attachmentContent);
                
                
                EmailType emailType = EmailType.VOUCHER_REPORT;
                List<Parameter> parameters = new ArrayList<Parameter>(0);
                Integer rowsCount = (count - 1);

                parameters.add(new Parameter("STATION_ADDRESS", stationAddress));
                parameters.add(new Parameter("COUNT", rowsCount.toString()));
                parameters.add(new Parameter("INTERVAL_BEGIN", intervalBegin));
                parameters.add(new Parameter("INTERVAL_END", intervalEnd));
                
                System.out.println("Invio email");
                
                List<Attachment> attachments = new ArrayList<Attachment>(0);
                
                // L'allegato deve essere inviato solo se � stato consumato almeno un voucher
                if (count > 0) {
                    Attachment attachment = new Attachment();
                    attachment.setFileName("voucher_report.xlsx");
                    //attachment.setContent(attachmentContent);
                    byte[] bytes = ewb.getBytesToStream();
                    attachment.setBytes(bytes);
                    attachments.add(attachment);
                }
                
                // Invia l'email a tutti i gestori della stazione
                for( ManagerBean managerBean : stationBean.getManagers() ) {
                    
                    String to = "";
                    
                    Integer recipientCount = 0;
                    
                    if( managerBean.getEmail() != null ) {
                        
                        if ( recipientCount > 0 ) {
                            
                            to = to + ",";
                        }
                        
                        to = to + managerBean.getEmail();
                        
                        recipientCount++;
                    }
                    
                    System.out.println("Sending email to: " + to);
                    
                    String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, to, parameters, attachments);

                    System.out.println("SendEmailResult: " + sendEmailResult);
                }
                
            }
            

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");
            
            userTransaction.commit();

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
}
