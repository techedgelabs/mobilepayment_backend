package com.techedge.mp.core.actions.scheduler;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.CrmOutboundProcessResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundDeliveryStatusType;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundProcessedStatusType;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crm.CRMOutboundBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.pushnotification.adapter.interfaces.StatusCode;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerProcessSubscribeCrmOutboundInterfaceAction {

    private static final String JOB_NAME = "JOB_PROCESS_SUBSCRIBE_CRM_OUTBOUND_INTERFACE";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private UserService         userService;

    public SchedulerProcessSubscribeCrmOutboundInterfaceAction() {}

    public CrmOutboundProcessResponse execute(PushNotificationServiceRemote pushNotificationService, int maxDataPerThread, int maxThreads, String arnTopic) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        Date now = new Date();

        System.out.println("job name: " + SchedulerProcessSubscribeCrmOutboundInterfaceAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        CrmOutboundProcessResponse crmOutboundProcessResponse = new CrmOutboundProcessResponse();

        try {
            
            /*
            boolean isEmpty = pushNotificationService.topicIsEmpty(arnTopic);
            
            if (!isEmpty) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                        "Il cluster non � stato svuotato. Sottoscrizione annullata");
                
                crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_TOPIC_IS_NOT_EMPTY);
                crmOutboundProcessResponse.setStatusMessage("Il cluster non � stato svuotato. Sottoscrizione annullata");
                
                return crmOutboundProcessResponse;
            }
            */
            
            userTransaction.begin();

            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_SUCCESS);
            crmOutboundProcessResponse.setStatusMessage("Esecuzione sottoscrizione al cluster terminato");

            int totalDataToProcess = QueryRepository.findCRMOutboundNotProcessedCount(em);

            if (totalDataToProcess > 0) {

                int threadCount = 1;

                System.out.println("Dati database CRM da sottoscrivere: " + totalDataToProcess);

                if (totalDataToProcess > maxDataPerThread) {
                    threadCount = totalDataToProcess / maxDataPerThread;

                    if (totalDataToProcess % maxDataPerThread > 0) {
                        threadCount++;
                    }
                }
                else {
                    maxDataPerThread = totalDataToProcess;
                }

                ExecutorService executor = Executors.newFixedThreadPool(maxThreads);

                for (int i = 0; i < threadCount; i++) {

                    int start = i * maxDataPerThread;
                    int end = start + maxDataPerThread;

                    if (end > totalDataToProcess) {
                        end = totalDataToProcess;
                    }

                    System.out.println("THREAD SUBSCRIBE AWS N " + (i + 1) + " indice inizio: " + start + "  indice fine: " + end);

                    CRMDataElementProcessSubscribe crmDataElementProcessSubscibe = new CRMDataElementProcessSubscribe(i + 1, em, start, end, pushNotificationService, arnTopic);

                    executor.submit(crmDataElementProcessSubscibe);
                }

                executor.shutdown();

                while (!executor.isTerminated()) {}

                System.out.println("THREADS SUBSCRIBE AWS TERMINATI");

            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                        "Nessun CRM Outbound da processare per la sottoscrizione al cluster");
                crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_NO_DATA_TO_PROCESS);
                crmOutboundProcessResponse.setStatusMessage("Nessun CRM Outbound da processare per la sottoscrizione al cluster");
            }

            userTransaction.commit();

            // Termina il job
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + crmOutboundProcessResponse.getStatusCode());

            return crmOutboundProcessResponse;

        }
        catch (Exception ex) {

            ex.printStackTrace();

            System.err.println("Errore nell'esecuzione del job: " + ex.getMessage());

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_CRM_OUTBOUND_PROCESS_SUBSCRIBE_FAILURE);
            crmOutboundProcessResponse.setStatusMessage("Errore nell'esecuzione del job di di sottoscrizione al cluster: " + ex.getMessage());
            return crmOutboundProcessResponse;
        }
    }

    private class CRMDataElementProcessSubscribe implements Runnable {

        private EntityManager                 em;
        private int                           threadID;
        private int                           start;
        private int                           end;
        private PushNotificationServiceRemote pushNotificationService;
        private String                        arnTopic;

        public CRMDataElementProcessSubscribe(int threadID, EntityManager em, int start, int end, PushNotificationServiceRemote pushNotificationService, String arnTopic) {
            this.threadID = threadID;
            this.em = em;
            this.start = start;
            this.pushNotificationService = pushNotificationService;
            this.arnTopic = arnTopic;
        }

        @Override
        public void run() {

            int count = 0;

            try {

                InitialContext ctx = new InitialContext();
                UserTransaction userTransaction = (UserTransaction) ctx.lookup("java:jboss/UserTransaction");

                List<CRMOutboundBean> crmOutboundBeanList = QueryRepository.findCRMOutboundNotProcessed(em, start, end);

                System.out.println("THREAD SUBSCRIBE AWS N " + threadID + " start - dati da sottoscrivere: " + crmOutboundBeanList.size());

                for (int i = 0; i < crmOutboundBeanList.size(); i++) {
                    userTransaction.begin();

                    try {

                        CRMOutboundBean crmOutboundBean = crmOutboundBeanList.get(i);

                        String response = subscribeFiscalCode(crmOutboundBean);

                        if (response.equals(ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS)) {
                            count += 1;
                        }

                        userTransaction.commit();
                    }
                    catch (Exception ex) {
                        System.err.println("THREAD SUBSCRIBE AWS N " + threadID + " riga " + (start + i) + " errore: " + ex.getMessage());
                        //ex.printStackTrace();
                        try {
                            userTransaction.commit();
                        }
                        catch (Exception ex2) {
                            System.err.println("THREAD SUBSCRIBE AWS N " + threadID + " riga " + (start + i) + " errore nella commit: " + ex2.getMessage());
                            //userTransaction.rollback();
                        }
                    }
                }

                System.out.println("THREAD SUBSCRIBE AWS N " + threadID + " end - dati inseriti: " + count);
            }
            catch (Exception ex) {
                System.err.println("THREAD SUBSCRIBE AWS N " + threadID + " errore: " + ex.getMessage());
                ex.printStackTrace();
            }
        }

        private String subscribeFiscalCode(CRMOutboundBean crmOutboundBean) {
            String fiscalCode = crmOutboundBean.getCodiceFiscale();

            try {

                CrmOutboundDeliveryStatusType deliveryStatusType = CrmOutboundDeliveryStatusType.ERROR_PUSH;

                // Ricerca l'utente per codice fiscale
                UserBean userBean = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode);

                if (userBean == null) {
                    crmOutboundBean.setDeliveryId(deliveryStatusType);
                    crmOutboundBean.setDeliveryMessage("Utente non trovato");
                    crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.UNAVAILABLE);

                    em.merge(crmOutboundBean);

                    System.out.println(fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
                    return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
                }

                // Verifica dati anagrafici
                if (!this.matchPersonalData(userBean, crmOutboundBean)) {
                    crmOutboundBean.setDeliveryId(deliveryStatusType);
                    crmOutboundBean.setDeliveryMessage("Dati anagrafici non corrispondenti");
                    crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.UNAVAILABLE);

                    em.merge(crmOutboundBean);

                    System.out.println(fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
                    return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
                }

                String arnEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();

                if (arnEndpoint == null) {

                    loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User arn endpoint not found");

                    crmOutboundBean.setDeliveryId(CrmOutboundDeliveryStatusType.ERROR_PUSH);
                    crmOutboundBean.setDeliveryMessage("Arn Endpoint non configurato per l'utente");
                    crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.PROCESSED);

                    em.merge(crmOutboundBean);

                    System.out.println(fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
                    return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
                }

                PushNotificationResult pushNotificationResult = pushNotificationService.subscribeToTopic(arnTopic, arnEndpoint);
                CrmOutboundDeliveryStatusType deliveryStatus;
                String deliveryMessage;
                CrmOutboundProcessedStatusType processedStatus;
                String response;

                if (pushNotificationResult.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_SUBSCRIBE_TO_TOPIC_SUCCESS)) {
                    deliveryStatus = CrmOutboundDeliveryStatusType.SENT;
                    processedStatus = CrmOutboundProcessedStatusType.PROCESSED;
                    response = ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
                    deliveryMessage = "Device sottoscritto con successo";
                }
                else if (pushNotificationResult.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_SUBSCRIBE_TO_TOPIC_TO_RETRY)) {
                    deliveryStatus = CrmOutboundDeliveryStatusType.ERROR_PUSH;
                    processedStatus = CrmOutboundProcessedStatusType.CREATED;
                    response = ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
                    String errorMessage = pushNotificationResult.getMessage();
                    
                    if (errorMessage.length() > 255) {
                        errorMessage = errorMessage.substring(0, 255);
                    }
                    
                    deliveryMessage = errorMessage;
                }
                else {
                    deliveryStatus = CrmOutboundDeliveryStatusType.ERROR_PUSH;
                    processedStatus = CrmOutboundProcessedStatusType.PROCESSED;
                    response = ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
                    String errorMessage = pushNotificationResult.getMessage();
                    
                    if (errorMessage.length() > 255) {
                        errorMessage = errorMessage.substring(0, 255);
                    }
                    
                    deliveryMessage = errorMessage;
                }

                crmOutboundBean.setDeliveryId(deliveryStatus);
                crmOutboundBean.setDeliveryMessage(deliveryMessage);
                crmOutboundBean.setProcessed(processedStatus);

                em.merge(crmOutboundBean);

                loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());

                return response;
            }
            catch (Exception ex) {
                ex.printStackTrace();
                System.err.println("Errore nell'esecuzione del processamento del codice fiscale: " + ex.getMessage());
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
            }
        }

        private Boolean matchPersonalData(UserBean userBean, CRMOutboundBean crmOutboundBean) {

            String checkEmail = userBean.getPersonalDataBean().getSecurityDataEmail();
            String checkTelCellulare = "";
            String crmTelCellulare = crmOutboundBean.getTelCellulare().replaceAll("0039", "");
            if (userBean.getMobilePhoneList() != null) {
                for (MobilePhoneBean mobilePhoneBean : userBean.getMobilePhoneList()) {
                    if (mobilePhoneBean.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_ACTIVE) {
                        checkTelCellulare = mobilePhoneBean.getNumber();
                        break;
                    }
                }
            }

            if (checkTelCellulare == null || !checkTelCellulare.equals(crmTelCellulare)) {
                System.out.println("matchPersonalData error - field TelCellulare - expected: " + checkTelCellulare + ", found: " + crmOutboundBean.getTelCellulare());
                return Boolean.FALSE;
            }

            if (checkEmail == null || !checkEmail.equalsIgnoreCase(crmOutboundBean.getEmail())) {
                System.out.println("matchPersonalData error - field Email - expected: " + checkEmail + ", found: " + crmOutboundBean.getEmail());
                return Boolean.FALSE;
            }

            return Boolean.TRUE;
        }

    }

}
