package com.techedge.mp.core.actions.scheduler;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobRemoveMobilePhoneNotVerifiedAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    private static final String JOB_NAME = "JOB_REMOVE_PENDING_PHONE_NUMBER";

    public SchedulerJobRemoveMobilePhoneNotVerifiedAction() {}

    public String execute(Date dateCheck) throws EJBException {
        
        Date now = new Date();

        System.out.println("job name: " + SchedulerJobRemoveMobilePhoneNotVerifiedAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            List<MobilePhoneBean> mobilePhoneBeanList = QueryRepository.getPendingMobilePhoneNumbers(em, dateCheck);

            if (mobilePhoneBeanList == null || mobilePhoneBeanList.isEmpty()) {

                System.out.println("Nessun numero in stato pending da eliminare");

                userTransaction.commit();

                return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
            }

            Integer count = 0;
 
            for (MobilePhoneBean mobilePhoneBean : mobilePhoneBeanList) {

                System.out.println("eliminazione numero: " + mobilePhoneBean.getPrefix() + " " + mobilePhoneBean.getNumber());

                mobilePhoneBean.setStatus(MobilePhone.MOBILE_PHONE_STATUS_CANCELED);
                
                em.merge(mobilePhoneBean);
                
                count++;
            }

            System.out.println("Numeri di telefono eliminati: " + count);

            userTransaction.commit();

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }

}
