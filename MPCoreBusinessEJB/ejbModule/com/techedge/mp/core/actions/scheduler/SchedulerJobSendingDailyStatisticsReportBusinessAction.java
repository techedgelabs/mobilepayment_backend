package com.techedge.mp.core.actions.scheduler;

import java.awt.Color;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.Sheet;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.utilities.ExcelWorkBook;
import com.techedge.mp.core.business.utilities.ExcelWorkBook.ExcelCellStyle;
import com.techedge.mp.core.business.utilities.QueryRepositoryBusiness;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobSendingDailyStatisticsReportBusinessAction {
    private static final String JOB_NAME = "JOB_SENDING_DAILY_STATISTICS_REPORT_BUSINESS";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    //Cell style for header row
    //private ExcelCellStyle      csHeader;
    private ExcelCellStyle      csHeaderBordered;
    //Cell style for table row
    private ExcelCellStyle      csTableValue;
    private ExcelCellStyle      csTableValueBold;
    private ExcelCellStyle      csTableValueBorderedLeft;
    private ExcelCellStyle      csTableValueBorderedRight;
    private ExcelCellStyle      csTableLabelBoldBorderedLeft;
    private ExcelCellStyle      csTableLabelIndentBorderedLeft;
    private ExcelCellStyle      csTableValueBorderedBottom;
    private ExcelCellStyle      csTableValueBorderedBottomRight;
    private ExcelCellStyle      csTableLabelBoldBorderedBottomLeft;
    private ExcelCellStyle      csTableLabelIndentBorderedBottomLeft;

    public SchedulerJobSendingDailyStatisticsReportBusinessAction() {}

    public String execute(EmailSenderRemote emailSender, String reportRecipient, String reportRecipientService, String reportLogoUrl, String proxyHost, String proxyPort, String proxyNoHosts) throws EJBException {

        Date now = new Date();

        DecimalFormat currencyFormat = (DecimalFormat) DecimalFormat.getCurrencyInstance(Locale.ITALIAN);
        currencyFormat.applyPattern("##0.00");

        DecimalFormat numberFormat = (DecimalFormat) DecimalFormat.getNumberInstance(Locale.ITALIAN);
        numberFormat.applyPattern("#,##0.000");
        
        NumberFormat integerFormat = NumberFormat.getNumberInstance(Locale.ITALIAN);
        integerFormat.setRoundingMode(RoundingMode.HALF_UP);
        integerFormat.setMinimumFractionDigits(0);
        integerFormat.setMaximumFractionDigits(0);

        System.out.println("job name: " + SchedulerJobSendingDailyStatisticsReportBusinessAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {

            SimpleDateFormat sdfFull = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdfidRif = new SimpleDateFormat("yyyyMMdd");

            Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
            Date toDay=calendar.getTime();
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date dailyEndDate = calendar.getTime();
            Date weeklyEndDate = dailyEndDate;

            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date dailyStartDate = calendar.getTime();
            
            calendar.add(Calendar.DATE, -1);
            Date twoDayAgo=calendar.getTime();

            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.add(Calendar.DATE, -1);
            calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date weeklyStartDate = calendar.getTime();
            
            calendar.add(Calendar.DATE, -1);
            Date weeklyBefore=calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 1);
            calendar.set(Calendar.MONTH, Calendar.JANUARY);
            calendar.set(Calendar.YEAR, 2019);
            calendar.set(Calendar.HOUR_OF_DAY, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.SECOND, 0);
            Date totalStartDate = calendar.getTime();
            
            calendar = Calendar.getInstance(Locale.ITALIAN);
            calendar.set(Calendar.DAY_OF_MONTH, 31);
            calendar.set(Calendar.MONTH, Calendar.DECEMBER);
            calendar.set(Calendar.YEAR, 2018);
            calendar.set(Calendar.HOUR_OF_DAY, 23);
            calendar.set(Calendar.MINUTE, 59);
            calendar.set(Calendar.SECOND, 59);
            Date fullBefore = calendar.getTime();

            System.out.println("DATA GIORNALIERA INIZIO:        " + sdfFull.format(dailyStartDate));
            System.out.println("DATA GIORNALIERA FINE:          " + sdfFull.format(dailyEndDate));
            System.out.println("DATA SETTIMANALE INIZIO:        " + sdfFull.format(weeklyStartDate));
            System.out.println("DATA SETTIMANALE FINE:          " + sdfFull.format(weeklyEndDate));
            System.out.println("DATA TOTALI INIZIO:             " + sdfFull.format(totalStartDate));
            
            System.out.println("DATA ATTUALE:                            " + sdfFull.format(toDay));
            System.out.println("DATA GIORNALIERA REPORT:                 " + sdfFull.format(dailyStartDate));
            System.out.println("DATA PRECEDENTE REPORT :                 " + sdfFull.format(twoDayAgo));
            System.out.println("DATA PRECEDENTE REPORT SETTIMANALE FINE: " + sdfFull.format(weeklyBefore));
            System.out.println("DATA PRECEDENTE REPORT TOTALE FINE:      " + sdfFull.format(fullBefore));
            
            System.out.println("Recupero info report alla STATISTICAL_REPORTS");
            
            //Recupero dati report dalla TB STATISTICAL_REPORT
            
            //Totale utenti iscritti						resultUsersCreditCard
            //Utenti con prima transazione di pagamento		resultDistinctUsersTransaction
            //PV abilitati alla fatturazione elettronica    resultStationsAppCount
            //Totale numero transazioni di pagamento		resultTransactionsTotalCount
            //Totale importo transato �						resultTotalAmount
            //Totale litri erogati							resultTransactionsTotalFuelQuantity
            //TransactionsTotalFuelQuantityDieselPiu		resultTransactionsTotalFuelQuantityDieselPiuDi     cui Diesel+
            //Numero transazioni pi� servito				resultPostPaidTransactionsSelfServitoCurrentAndHistory
            //Area NO										resultTransactionsTotalCountAreaNO
            //Area NE										resultTransactionsTotalCountAreaNE
            //Area CN										resultTransactionsTotalCountAreaCN
            //Area C										resultTransactionsTotalCountAreaC
            //Area CS										resultTransactionsTotalCountAreaCS
            //Area S										resultTransactionsTotalCountAreaS
            //Totale transazioni payment					resultTransactionsTotalCount (2)
            Map<String,String> mapResultTotalDay = QueryRepositoryBusiness.statisticsReportSynthesisFindForType(em, sdfidRif.format(dailyStartDate), "B");
            Map<String,String> mapResultDiffDay = QueryRepositoryBusiness.statisticsReportSynthesisFindForType(em, sdfidRif.format(twoDayAgo), "B");
            Map<String,String> mapResultDiffWeek = QueryRepositoryBusiness.statisticsReportSynthesisFindForType(em, sdfidRif.format(weeklyBefore), "B");
            Map<String,String> mapResultDiffFull = QueryRepositoryBusiness.statisticsReportSynthesisFindForType(em, sdfidRif.format(fullBefore), "B");
          
            if(mapResultTotalDay.size()<=0||
            		mapResultDiffDay.size()<=0||
            		mapResultDiffWeek.size()<=0||
            		mapResultDiffFull.size()<=0){
            	
            	String detail="Estrazione per data: "+sdfDate.format(dailyStartDate)+" risultati: "+mapResultTotalDay.size();
            	detail=detail.concat("<br>Estrazione per data: "+sdfDate.format(twoDayAgo)+" risultati: "+mapResultDiffDay.size());
            	detail=detail.concat("<br>Estrazione per data: "+sdfDate.format(weeklyBefore)+" risultati: "+mapResultDiffWeek.size());
            	detail=detail.concat("<br>Estrazione per data: "+sdfDate.format(fullBefore)+" risultati: "+mapResultDiffFull.size());
            	//sendingServiceMail(emailSender,"Errore report statistiche giornaliero", reportRecipientService, sdfDate.format(dailyStartDate),detail);
            	System.out.println("Errore nel recupero delle statistiche (TB: statistical_report) -> invio mail di dettaglio");
            	throw new Exception(detail);
            }
            
            System.out.println("Estrazione completata con successo.");
            
            Integer valueInt;
            Double 	valueDouble;
            
            String valueExec="UsersCreditCard";
            List<Object> resultUsersCreditCard = new ArrayList<Object>();
            valueInt=Integer.parseInt(mapResultTotalDay.get(valueExec));
            resultUsersCreditCard.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffDay.get(valueExec))));
            resultUsersCreditCard.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffWeek.get(valueExec))));
            resultUsersCreditCard.add(integerFormat.format(valueInt));
            		
            valueExec="DistinctUsersTransaction";
            List<Object> resultDistinctUsersTransaction= new ArrayList<Object>();
            valueInt=Integer.parseInt(mapResultTotalDay.get(valueExec));
            resultDistinctUsersTransaction.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffDay.get(valueExec))));
            resultDistinctUsersTransaction.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffWeek.get(valueExec))));
            resultDistinctUsersTransaction.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffFull.get(valueExec))));
            
            valueExec="TransactionsTotalCount";
            List<Object> resultTransactionsTotalCount= new ArrayList<Object>();
            valueInt=Integer.parseInt(mapResultTotalDay.get(valueExec));
            resultTransactionsTotalCount.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffDay.get(valueExec))));
            resultTransactionsTotalCount.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffWeek.get(valueExec))));
            resultTransactionsTotalCount.add(integerFormat.format(valueInt));
            
            valueExec="TotalAmount";
            List<Object> resultTotalAmount= new ArrayList<Object>();
            valueDouble=Double.parseDouble(mapResultTotalDay.get(valueExec));
            resultTotalAmount.add(integerFormat.format(valueDouble-Double.parseDouble(mapResultDiffDay.get(valueExec))));
            resultTotalAmount.add(integerFormat.format(valueDouble-Double.parseDouble(mapResultDiffWeek.get(valueExec))));
            resultTotalAmount.add(integerFormat.format(valueDouble));
            
            valueExec="TransactionsTotalFuelQuantity";
            List<Object> resultTransactionsTotalFuelQuantity= new ArrayList<Object>();
            valueDouble=Double.parseDouble(mapResultTotalDay.get(valueExec));
            resultTransactionsTotalFuelQuantity.add(numberFormat.format(valueDouble-Double.parseDouble(mapResultDiffDay.get(valueExec))));
            resultTransactionsTotalFuelQuantity.add(numberFormat.format(valueDouble-Double.parseDouble(mapResultDiffWeek.get(valueExec))));
            resultTransactionsTotalFuelQuantity.add(numberFormat.format(valueDouble));
            
            valueExec="TransactionsTotalFuelQuantityDieselPiu";
            List<Object> resultTransactionsTotalFuelQuantityDieselPiu= new ArrayList<Object>();
            valueDouble=Double.parseDouble(mapResultTotalDay.get(valueExec));
            resultTransactionsTotalFuelQuantityDieselPiu.add(numberFormat.format(valueDouble-Double.parseDouble(mapResultDiffDay.get(valueExec))));
            resultTransactionsTotalFuelQuantityDieselPiu.add(numberFormat.format(valueDouble-Double.parseDouble(mapResultDiffWeek.get(valueExec))));
            resultTransactionsTotalFuelQuantityDieselPiu.add(numberFormat.format(valueDouble));
            
            valueExec="PostPaidTransactionsSelfServitoCurrentAndHistory";
            List<Object> resultPostPaidTransactionsSelfServitoCurrentAndHistory= new ArrayList<Object>();
            valueInt=Integer.parseInt(mapResultTotalDay.get(valueExec));
            resultPostPaidTransactionsSelfServitoCurrentAndHistory.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffDay.get(valueExec))));
            resultPostPaidTransactionsSelfServitoCurrentAndHistory.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffWeek.get(valueExec))));
            resultPostPaidTransactionsSelfServitoCurrentAndHistory.add(integerFormat.format(valueInt));
            
            valueExec="TransactionsTotalCountAreaNO";
            List<Object> resultTransactionsTotalCountAreaNO= new ArrayList<Object>();
            valueInt=Integer.parseInt(mapResultTotalDay.get(valueExec));
            resultTransactionsTotalCountAreaNO.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffDay.get(valueExec))));
            resultTransactionsTotalCountAreaNO.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffWeek.get(valueExec))));
            resultTransactionsTotalCountAreaNO.add(integerFormat.format(valueInt));
            
            valueExec="TransactionsTotalCountAreaNE";
            List<Object> resultTransactionsTotalCountAreaNE= new ArrayList<Object>();
            valueInt=Integer.parseInt(mapResultTotalDay.get(valueExec));
            resultTransactionsTotalCountAreaNE.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffDay.get(valueExec))));
            resultTransactionsTotalCountAreaNE.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffWeek.get(valueExec))));
            resultTransactionsTotalCountAreaNE.add(integerFormat.format(valueInt));
            
            valueExec="TransactionsTotalCountAreaCN";
            List<Object> resultTransactionsTotalCountAreaCN= new ArrayList<Object>();
            valueInt=Integer.parseInt(mapResultTotalDay.get(valueExec));
            resultTransactionsTotalCountAreaCN.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffDay.get(valueExec))));
            resultTransactionsTotalCountAreaCN.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffWeek.get(valueExec))));
            resultTransactionsTotalCountAreaCN.add(integerFormat.format(valueInt));
            
            valueExec="TransactionsTotalCountAreaC";
            List<Object> resultTransactionsTotalCountAreaC= new ArrayList<Object>();
            valueInt=Integer.parseInt(mapResultTotalDay.get(valueExec));
            resultTransactionsTotalCountAreaC.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffDay.get(valueExec))));
            resultTransactionsTotalCountAreaC.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffWeek.get(valueExec))));
            resultTransactionsTotalCountAreaC.add(integerFormat.format(valueInt));
            
            valueExec="TransactionsTotalCountAreaCS";
            List<Object> resultTransactionsTotalCountAreaCS= new ArrayList<Object>();
            valueInt=Integer.parseInt(mapResultTotalDay.get(valueExec));
            resultTransactionsTotalCountAreaCS.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffDay.get(valueExec))));
            resultTransactionsTotalCountAreaCS.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffWeek.get(valueExec))));
            resultTransactionsTotalCountAreaCS.add(integerFormat.format(valueInt));
            
            valueExec="TransactionsTotalCountAreaS";
            List<Object> resultTransactionsTotalCountAreaS= new ArrayList<Object>();
            valueInt=Integer.parseInt(mapResultTotalDay.get(valueExec));
            resultTransactionsTotalCountAreaS.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffDay.get(valueExec))));
            resultTransactionsTotalCountAreaS.add(integerFormat.format(valueInt-Integer.parseInt(mapResultDiffWeek.get(valueExec))));
            resultTransactionsTotalCountAreaS.add(integerFormat.format(valueInt));
            
            //New Workbook
            ExcelWorkBook ewb = new ExcelWorkBook();

            final String[] sheetSynthesisDailyColumnsHeading = new String[] { "Sintesi della giornata " + sdfDate.format(dailyStartDate) };
            final String[] sheetSynthesisWeeklyColumnsHeading = new String[] { "Sintesi della settimana " + sdfDate.format(weeklyStartDate) + " - " + sdfDate.format(weeklyEndDate) };
            final String[] sheetSynthesisTotalColumnsHeading = new String[] { "Totali dal " + sdfDate.format(totalStartDate)};

            final String[] sheetSynthesisDailyPaymentRegionsColumnsHeading = new String[] { "Transazioni Payment " + sdfDate.format(dailyStartDate) };
            final String[] sheetSynthesisWeeklyPaymentRegionsColumnsHeading = new String[] { "Transazioni Payment " + sdfDate.format(weeklyStartDate) + " - " + sdfDate.format(weeklyEndDate) };
            final String[] sheetSynthesisTotalPaymentRegionsColumnsHeading = new String[] { "Totali Transazioni Payment dal " + sdfDate.format(totalStartDate) };

            final String[] sheetDetailColumnsHeading = new String[] { "ID Transazione", "Tipo Transazione", "Importo", 
                    "Esito Finale", "Data Rifornimento", "Codice PV", "Indirizzo PV", "Provincia PV" };

            csHeaderBordered = ewb.createCellStyle(true, 12, Color.BLACK, new Color(178, 178, 178), HorizontalAlignment.CENTER_SELECTION, -1, 1, 1, 1, 1, Color.BLACK);
            csTableValue = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null);
            csTableValueBold = ewb.createCellStyle(true, 14, Color.BLACK, new Color(255, 255, 255), null);
            csTableValueBorderedLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, -1, 1, Color.BLACK);
            csTableValueBorderedRight = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, 1, -1, -1, Color.BLACK);
            csTableLabelBoldBorderedLeft = ewb.createCellStyle(true, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, -1, 1, Color.BLACK);
            csTableLabelIndentBorderedLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, 1, -1, -1, -1, 1, Color.BLACK);
            csTableValueBorderedBottom = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, 1, -1, Color.BLACK);
            csTableValueBorderedBottomRight = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, 1, 1, -1, Color.BLACK);
            csTableLabelBoldBorderedBottomLeft = ewb.createCellStyle(true, 12, Color.BLACK, new Color(235, 235, 235), null, -1, -1, -1, 1, 1, Color.BLACK);
            csTableLabelIndentBorderedBottomLeft = ewb.createCellStyle(false, 12, Color.BLACK, new Color(235, 235, 235), null, 1, -1, -1, 1, 1, Color.BLACK);

            HashMap<String, ArrayList<ArrayList<String>>> excelDataSynthesis = new HashMap<String, ArrayList<ArrayList<String>>>();
            HashMap<String, ArrayList<ArrayList<String>>> excelDataSynthesisPaymentRegions = new HashMap<String, ArrayList<ArrayList<String>>>();
            HashMap<String, ArrayList<ArrayList<String>>> excelDataSynthesisLoyaltyRegions = new HashMap<String, ArrayList<ArrayList<String>>>();
            
            excelDataSynthesis.put("daily", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesis.put("weekly", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesis.put("total", new ArrayList<ArrayList<String>>(0));

            excelDataSynthesisPaymentRegions.put("daily", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesisPaymentRegions.put("weekly", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesisPaymentRegions.put("total", new ArrayList<ArrayList<String>>(0));
            
            excelDataSynthesisLoyaltyRegions.put("daily", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesisLoyaltyRegions.put("weekly", new ArrayList<ArrayList<String>>(0));
            excelDataSynthesisLoyaltyRegions.put("total", new ArrayList<ArrayList<String>>(0));
            
            System.out.println("Creazione excelCellStyleContainer");
            
            ArrayList<ArrayList<ExcelCellStyle>> excelCellStyleContainer = new ArrayList<ArrayList<ExcelCellStyle>>();
            ArrayList<ArrayList<ExcelCellStyle>> excelCellStyleContainerPayment = new ArrayList<ArrayList<ExcelCellStyle>>();
            
            insertDataSyntesis("Totale utenti iscritti", resultUsersCreditCard, excelDataSynthesis, excelCellStyleContainer, true, true, false);
            
            insertDataSyntesis("Utenti con prima transazione di pagamento", resultDistinctUsersTransaction, excelDataSynthesis, excelCellStyleContainer, true, true, false);

            insertDataSyntesis("Totale numero transazioni di pagamento", resultTransactionsTotalCount, excelDataSynthesis, excelCellStyleContainer, true, false, false);
            insertDataSyntesis("Totale importo transato �", resultTotalAmount, excelDataSynthesis, excelCellStyleContainer, true, false, false);
            insertDataSyntesis("Totale litri erogati", resultTransactionsTotalFuelQuantity, excelDataSynthesis, excelCellStyleContainer, true, false, false);
            insertDataSyntesis("   Di cui Diesel+", resultTransactionsTotalFuelQuantityDieselPiu, excelDataSynthesis, excelCellStyleContainer, false, false, false);
            insertDataSyntesis("   Numero transazioni pi� servito", resultPostPaidTransactionsSelfServitoCurrentAndHistory, excelDataSynthesis, excelCellStyleContainer, false, false, true);
            
            insertDataSyntesis("Area NO", resultTransactionsTotalCountAreaNO, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Area NE", resultTransactionsTotalCountAreaNE, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Area CN", resultTransactionsTotalCountAreaCN, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Area C", resultTransactionsTotalCountAreaC, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Area CS", resultTransactionsTotalCountAreaCS, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Area S", resultTransactionsTotalCountAreaS, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, false);
            insertDataSyntesis("Totale transazioni payment", resultTransactionsTotalCount, excelDataSynthesisPaymentRegions, excelCellStyleContainerPayment, true, false, true);
            
            
            
            Sheet sheet = ewb.addSheetBlank("Sintesi", false, false);
            sheet = ewb.addMergedCells(sheet, 7, 7, 1, 3, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisDailyColumnsHeading, excelDataSynthesis.get("daily"), csHeaderBordered, excelCellStyleContainer, 7, 1);
            sheet = ewb.addMergedCells(sheet, 7, 7, 5, 7, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisWeeklyColumnsHeading, excelDataSynthesis.get("weekly"), csHeaderBordered, excelCellStyleContainer, 7, 5);
            sheet = ewb.addMergedCells(sheet, 7, 7, 9, 11, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisTotalColumnsHeading, excelDataSynthesis.get("total"), csHeaderBordered, excelCellStyleContainer, 7, 9);

            sheet = ewb.addMergedCells(sheet, 18, 18, 1, 3, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisDailyPaymentRegionsColumnsHeading, excelDataSynthesisPaymentRegions.get("daily"), csHeaderBordered, excelCellStyleContainerPayment, 18, 1);
            sheet = ewb.addMergedCells(sheet, 18, 18, 5, 7, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisWeeklyPaymentRegionsColumnsHeading, excelDataSynthesisPaymentRegions.get("weekly"), csHeaderBordered, excelCellStyleContainerPayment, 18, 5);
            sheet = ewb.addMergedCells(sheet, 18, 18, 9, 11, csHeaderBordered);
            sheet = ewb.addTable(sheet, sheetSynthesisTotalPaymentRegionsColumnsHeading, excelDataSynthesisPaymentRegions.get("total"), csHeaderBordered, excelCellStyleContainerPayment, 18, 9);
            
            sheet = ewb.addImageJpg(sheet, 1, 1, reportLogoUrl, false, proxyHost, proxyPort, proxyNoHosts);
            sheet = ewb.addMergedCells(sheet, 5, 5, 1, 5, csTableValueBold);
            sheet = ewb.addText(sheet, csTableValueBold, 5, 1, "Sintesi per " + mapResultTotalDay.get("StationsAppCount").toString().replaceAll("\\[|\\]", "") + " PV abilitati alla fatturazione elettronica");
            
            System.out.println("Fine preparazione dati per generazione allegato excel");
            
            System.out.println("Creazione workbook");
            
            System.out.println("Creazione foglio 'Sintesi'");
            
            System.out.println("Invio email");
            
            EmailType emailType = EmailType.DAILY_STATISTICS_REPORT_BUSINESS;
            List<Parameter> parameters = new ArrayList<Parameter>(0);

            parameters.add(new Parameter("DATE", sdfDate.format(dailyStartDate)));
            List<Attachment> attachments = new ArrayList<Attachment>(0);

            Attachment attachment = new Attachment();
            attachment.setFileName("report_statistiche_giornaliero_piva.xlsx");
            //attachment.setContent(attachmentContent);
            byte[] bytes = ewb.getBytesToStream();
            attachment.setBytes(bytes);
            attachments.add(attachment);

            if ( reportRecipient == null || reportRecipient.equals("") ) {
                
                // Recipient empty
                System.out.println("SendEmailResult: not sent - recipient empty");
            }
            else {
                
                // Sending email
                System.out.println("Sending email to: " + reportRecipient);
                String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, reportRecipient, parameters, attachments);
                System.out.println("SendEmailResult: " + sendEmailResult);
            }
            
            System.out.println("job endTimestamp: " + now.toString());
            System.out.println("job result: " + "OK");


            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {
        	
        	sendingServiceMail(emailSender,"Errore report statistiche giornaliero", reportRecipientService, new SimpleDateFormat("yyyyMMdd").format(new Date()),ex2.getMessage());
            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }

    private void insertDataSyntesis(String label, List<Object> result, HashMap<String, ArrayList<ArrayList<String>>> excelDataContainer,
            ArrayList<ArrayList<ExcelCellStyle>> excelCellStyleContainer, boolean rootLabel, boolean insertBlankRow, boolean lastRow) {
        ArrayList<String> dataDaily = new ArrayList<String>();
        ArrayList<String> dataWeekly = new ArrayList<String>();
        ArrayList<String> dataTotal = new ArrayList<String>();
        ArrayList<String> dataEmpty = new ArrayList<String>();
        ArrayList<ExcelCellStyle> dataCellStyle = new ArrayList<ExcelCellStyle>();
        ArrayList<ExcelCellStyle> dataEmptyCellStyle = new ArrayList<ExcelCellStyle>();

        if (rootLabel) {
            if (lastRow) {
                dataCellStyle.add(csTableLabelBoldBorderedBottomLeft);
            }
            else {
                dataCellStyle.add(csTableLabelBoldBorderedLeft);
            }
        }
        else {
            if (lastRow) {
                dataCellStyle.add(csTableLabelIndentBorderedBottomLeft);
            }
            else {
                dataCellStyle.add(csTableLabelIndentBorderedLeft);
            }
        }

        if (lastRow) {
            dataCellStyle.add(csTableValueBorderedBottom);
            dataCellStyle.add(csTableValueBorderedBottomRight);
        }
        else {
            dataCellStyle.add(csTableValue);
            dataCellStyle.add(csTableValueBorderedRight);
        }
        
        excelCellStyleContainer.add(dataCellStyle);

        if (insertBlankRow) {
            dataEmptyCellStyle.add(csTableValueBorderedLeft);
            dataEmptyCellStyle.add(csTableValue);
            dataEmptyCellStyle.add(csTableValueBorderedRight);
            excelCellStyleContainer.add(dataEmptyCellStyle);
        }

        dataDaily.add(label);
        dataDaily.add("");
        dataDaily.add(result.get(0).toString());
        excelDataContainer.get("daily").add(dataDaily);

        if (insertBlankRow) {
            dataEmpty.add("");
            dataEmpty.add("");
            dataEmpty.add("");
            excelDataContainer.get("daily").add(dataEmpty);
        }

        dataWeekly.add(label);
        dataWeekly.add("");
        dataWeekly.add(result.get(1).toString());
        excelDataContainer.get("weekly").add(dataWeekly);

        if (insertBlankRow) {
            dataEmpty.clear();
            dataEmpty.add("");
            dataEmpty.add("");
            dataEmpty.add("");
            excelDataContainer.get("weekly").add(dataEmpty);
        }

        dataTotal.add(label);
        dataTotal.add("");
        dataTotal.add(result.get(2).toString());
        excelDataContainer.get("total").add(dataTotal);

        if (insertBlankRow) {
            dataEmpty.clear();
            dataEmpty.add("");
            dataEmpty.add("");
            dataEmpty.add("");
            excelDataContainer.get("total").add(dataEmpty);
        }
    }
    
    private void sendingServiceMail(EmailSenderRemote emailSender, String message, String reportRecipient, String dateExtraction, String detail){
    	
    	EmailType emailType = EmailType.ERROR_DAILY_STATISTICS_REPORT_V2;
    	
    	List<Parameter> parameters = new ArrayList<Parameter>(0);
    	parameters.add(new Parameter("MESSAGE", message));
    	parameters.add(new Parameter("TYPE", "BUSINESS"));
        parameters.add(new Parameter("DATE", dateExtraction));
        parameters.add(new Parameter("DETAIL", detail));
        
    	System.out.println("Sending email to: " + reportRecipient);
        String sendEmailResult = emailSender.sendEmail(emailType, reportRecipient, parameters);
        System.out.println("SendEmailResult: " + sendEmailResult);
    }
}