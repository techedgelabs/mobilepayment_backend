package com.techedge.mp.core.actions.scheduler;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.EventNotificationService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CrmDataElement;
import com.techedge.mp.core.business.interfaces.crm.CrmDataElementProcessingResult;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundDeliveryStatusType;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundProcessedStatusType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crm.CRMOutboundBean;
import com.techedge.mp.core.business.utilities.AbstractFTPService;
import com.techedge.mp.core.business.utilities.CrmDataProcessor;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.CreateVoucherPromotional;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.Attachment;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationMessage;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.pushnotification.adapter.interfaces.StatusCode;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerProcessResultCrmOutboundInterfaceAction {

    private static final String      JOB_NAME = "JOB_PROCESS_RESULT_CRM_OUTBOUND_INTERFACE";

    @Resource
    private EJBContext               context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager            em;

    @EJB
    private LoggerService            loggerService;

    @EJB
    private UserService              userService;

    @EJB
    private EventNotificationService eventNotificationService;

    private UserTransaction          userTransaction;

    public SchedulerProcessResultCrmOutboundInterfaceAction() {}

    public String execute(String ftpsResultPath, String fileExtensionData, String fileExtensionOk, CRMServiceRemote CRMService, AbstractFTPService ftpService,
            PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, EmailSenderRemote emailSender, String summaryRecipient, Integer expiryTime, 
            Integer maxRetryAttemps, Integer maxUsersProcessed, Integer processResultDelay) throws EJBException {
        
        userTransaction = context.getUserTransaction();

        Date now = new Date();

        System.out.println("job name: " + SchedulerProcessResultCrmOutboundInterfaceAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        try {
            //userTransaction.begin();

            String result = ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

            int rowsCount = QueryRepository.getCRMOutboundRowsCount(em);

            if (rowsCount > 0) {

                List<CRMOutboundBean> crmOutboundBeanList = QueryRepository.findCRMOutboundNotProcessed(em, maxUsersProcessed);

                if (crmOutboundBeanList != null && !crmOutboundBeanList.isEmpty()) {

                    for (CRMOutboundBean crmOutboundBean : crmOutboundBeanList) {
                        processFiscalCode(crmOutboundBean, pushNotificationService, fidelityService, expiryTime, maxRetryAttemps);
                    }
                }
                else {
                    
                    crmOutboundBeanList = QueryRepository.getAllCRMOutbound(em, true);
                    CRMOutboundBean crmOutboundBean = crmOutboundBeanList.get(0);
                    
                    SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date processedDate = sdfDate.parse(crmOutboundBean.getTmsInserimento());
                    Calendar calendar = Calendar.getInstance(Locale.ITALIAN);
                    calendar.setTime(processedDate);
                    calendar.set(Calendar.SECOND, processResultDelay);
                    
                           
                    if (now.compareTo(calendar.getTime()) >= 0) {
                    
                        userTransaction.begin();
                        
                        crmOutboundBeanList = QueryRepository.getAllCRMOutbound(em, false);
                        
                        result = processResult(crmOutboundBeanList, ftpsResultPath, fileExtensionData, fileExtensionOk, ftpService, emailSender, summaryRecipient);
    
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                                "Eliminazione di tutti i records presenti nella tabella CrmOutbound...");
                        int rows = em.createQuery("delete from CRMOutboundBean").executeUpdate();
    
                        if (rows <= 0) {
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Eliminazione dei records presenti non andata a buon fine");
                        }
                        
                        userTransaction.commit();
                    }
                    else {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Data di processo risultati: " + sdfDate.format(calendar.getTime()));
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Data inserimento dati: " + sdfDate.format(processedDate));
                    }
                }
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Nessun CRM Outbound da processare");
            }

            //userTransaction.commit();

            // Termina il job
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + result);

            return result;

        }
        catch (Exception ex) {
            
            ex.printStackTrace();
            
            System.err.println("Errore nell'esecuzione del job: " + ex.getMessage());

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }

    private String processFiscalCode(CRMOutboundBean crmOutboundBean, PushNotificationServiceRemote pushNotificationService, FidelityServiceRemote fidelityService, 
            Integer expiryTime, Integer maxRetryAttemps) {
        
        String fiscalCode = crmOutboundBean.getCodiceFiscale();

        try {
            
            userTransaction.begin();
            
            boolean promoVoucher = false;
            CrmOutboundDeliveryStatusType deliveryStatusType = CrmOutboundDeliveryStatusType.ERROR_PUSH;
            
            if (Objects.equals(crmOutboundBean.getC6(), "VOUCHER")) {
                promoVoucher = true;
                deliveryStatusType = CrmOutboundDeliveryStatusType.ERROR_VOUCHER;
            }
            
            // Ricerca l'utente per codice fiscale
            UserBean userBean = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode);
    
            if (userBean == null) {
                crmOutboundBean.setDeliveryId(deliveryStatusType);
                crmOutboundBean.setDeliveryMessage("Utente non trovato");
                crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.UNAVAILABLE);
    
                em.merge(crmOutboundBean);
    
                System.out.println(fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
                userTransaction.commit();
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
            }
    
            // Verifica dati anagrafici
            if (!this.matchPersonalData(userBean, crmOutboundBean)) {
                crmOutboundBean.setDeliveryId(deliveryStatusType);
                crmOutboundBean.setDeliveryMessage("Dati anagrafici non corrispondenti");
                crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.UNAVAILABLE);
    
                em.merge(crmOutboundBean);
    
                System.out.println(fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
                userTransaction.commit();
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
            }
    
            if (promoVoucher) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Trovata offerta voucher");
                
                if (crmOutboundBean.getC7() == null || crmOutboundBean.getC7().isEmpty()) {
                    crmOutboundBean.setDeliveryId(CrmOutboundDeliveryStatusType.ERROR_VOUCHER);
                    crmOutboundBean.setDeliveryMessage("Voucher promo code null or empty");
                    crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.UNAVAILABLE);
        
                    em.merge(crmOutboundBean);
        
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
                    userTransaction.commit();
                    return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
                }

                if (crmOutboundBean.getC8() == null || crmOutboundBean.getC8().isEmpty()) {
                    crmOutboundBean.setDeliveryId(CrmOutboundDeliveryStatusType.ERROR_VOUCHER);
                    crmOutboundBean.setDeliveryMessage("Voucher amount null or empty");
                    crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.UNAVAILABLE);
        
                    em.merge(crmOutboundBean);
        
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
                    userTransaction.commit();
                    return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
                }
                
                String promoCode = crmOutboundBean.getC7();
                Double voucherAmount = new Double(crmOutboundBean.getC8());
                
                CreateVoucherPromotional createVoucherPromotional = new CreateVoucherPromotional(em, fidelityService, userBean);
                String createResponse = createVoucherPromotional.create(promoCode, voucherAmount, null);
                String associateResponse = null;

                if (!createResponse.equals(ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_SUCCESS)) {
                    crmOutboundBean.setDeliveryId(CrmOutboundDeliveryStatusType.ERROR_VOUCHER);
                    crmOutboundBean.setDeliveryMessage(createVoucherPromotional.getCreateVoucherResult().getMessageCode());
                    crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.UNAVAILABLE);
        
                    em.merge(crmOutboundBean);
        
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
                    userTransaction.commit();
                    return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
                }

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "VoucherPromotionalBean: " + createVoucherPromotional.getVoucherPromotionalBean().toString());
                associateResponse = createVoucherPromotional.associateToUser(createVoucherPromotional.getCreateVoucherResult());

                if (Objects.equals(associateResponse, ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_ASSIGN_ERROR)) {

                    crmOutboundBean.setDeliveryId(CrmOutboundDeliveryStatusType.ERROR_VOUCHER);
                    crmOutboundBean.setDeliveryMessage("Errore nell'associazione voucher promozionale (" + createVoucherPromotional.getCreateVoucherResult().getVoucher().getPromoCode() + ") a l'utente (" + fiscalCode + ")");
                    crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.UNAVAILABLE);
        
                    em.merge(crmOutboundBean);
        
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
                    userTransaction.commit();
                    return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
                }
            }

            String arnEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();
            PushNotificationContentType contentType = PushNotificationContentType.TEXT;
            PushNotificationStatusType statusCode = null;
            String statusMessage = null;
            Long userID = userBean.getId();
            String publishMessageID = null;
            Date sendingTimestamp = new Date();
            PushNotificationSourceType source = PushNotificationSourceType.CRM_OUTBOUND;
            String message = crmOutboundBean.getMsg();
            String title = "Eni Station +";
    
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(sendingTimestamp);
            calendar.add(Calendar.SECOND, expiryTime);
    
            PushNotificationBean pushNotificationBean = new PushNotificationBean();
            pushNotificationBean.setTitle(title);
            pushNotificationBean.setSendingTimestamp(sendingTimestamp);
            pushNotificationBean.setExpiringTimestamp(calendar.getTime());
            pushNotificationBean.setSource(source);
            pushNotificationBean.setContentType(contentType);
            pushNotificationBean.setUser(userBean);
            pushNotificationBean.setRetryAttemptsLeft(maxRetryAttemps);
    
            em.persist(pushNotificationBean);
    
            PushNotificationMessage notificationMessage = new PushNotificationMessage();
            
            String notificationText = "";
            if (crmOutboundBean.getC5() != null && !crmOutboundBean.getC5().isEmpty()) {
                notificationText = crmOutboundBean.getC5();
            }
            else {
                notificationText = message;
            }
            
            notificationMessage.setMessage(pushNotificationBean.getId(), notificationText, title);
    
            if (arnEndpoint == null) {
    
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User arn endpoint not found");
    
                statusCode = PushNotificationStatusType.ERROR;
                statusMessage = "User arn endpoint not found";
    
                String response = eventNotificationService.updatePushNotication(pushNotificationBean.getId(), publishMessageID, arnEndpoint, title, message, sendingTimestamp, source,
                        contentType, statusCode, statusMessage, userID);
    
                if (response.equals(ResponseHelper.SYSTEM_ERROR)) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "SystemError in updatePushNotication");
                }
                else {
                    if (response.equals(ResponseHelper.USER_UPDATE_PUSH_NOTIFICATION_FAILURE)) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Event notification error in updatePushNotication");
                    }
                    else {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Event notification endpoint not found in updatePushNotication");
                    }
                }
    
                crmOutboundBean.setDeliveryId(CrmOutboundDeliveryStatusType.ERROR_PUSH);
                crmOutboundBean.setDeliveryMessage("Arn Endpoint non configurato per l'utente");
                crmOutboundBean.setProcessed(CrmOutboundProcessedStatusType.PROCESSED);
    
                em.merge(crmOutboundBean);
    
                System.out.println(fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
                userTransaction.commit();
                return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
            }
    
            PushNotificationResult pushNotificationResult = pushNotificationService.publishMessage(arnEndpoint, notificationMessage, false, false);
            publishMessageID = pushNotificationResult.getMessageId();
            sendingTimestamp = pushNotificationResult.getRequestTimestamp();
            CrmOutboundDeliveryStatusType deliveryStatus;
            String deliveryMessage;
            CrmOutboundProcessedStatusType processedStatus;
            String response;
    
            if (pushNotificationResult.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_SUCCESS)) {
                statusCode = PushNotificationStatusType.DELIVERED;
                deliveryStatus = CrmOutboundDeliveryStatusType.SENT;
                processedStatus = CrmOutboundProcessedStatusType.PROCESSED;
                response = ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
                deliveryMessage = "Notifica inviata con successo";
    
            }
            else if (pushNotificationResult.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_ARN_NOT_FOUND)) {
                statusCode = PushNotificationStatusType.ARN_NOT_FOUND;
                deliveryStatus = CrmOutboundDeliveryStatusType.ERROR_PUSH;
                processedStatus = CrmOutboundProcessedStatusType.PROCESSED;
                response = ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
                deliveryMessage = pushNotificationResult.getMessage();
            }
            else {
                statusCode = PushNotificationStatusType.ERROR;
                deliveryStatus = CrmOutboundDeliveryStatusType.ERROR_PUSH;
                processedStatus = CrmOutboundProcessedStatusType.PROCESSED;
                response = ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
                deliveryMessage = pushNotificationResult.getMessage();
            }
    
            statusMessage = pushNotificationResult.getMessage();
            eventNotificationService.updatePushNotication(pushNotificationBean.getId(), publishMessageID, arnEndpoint, title, message, sendingTimestamp, source,
                    contentType, statusCode, statusMessage, userID);
        
        
            crmOutboundBean.setDeliveryId(deliveryStatus);
            crmOutboundBean.setDeliveryMessage(deliveryMessage);
            crmOutboundBean.setPushNotificationBean(pushNotificationBean);
            crmOutboundBean.setProcessed(processedStatus);
    
            em.merge(crmOutboundBean);
            
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, fiscalCode + ": " + crmOutboundBean.getDeliveryMessage());
            
            userTransaction.commit();

            return response;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            System.err.println("Errore nell'esecuzione del processamento del codice fiscale: " + ex.getMessage());
            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }

    private String processResult(List<CRMOutboundBean> crmOutboundBeanList, String ftpsResultPath, String fileExtensionData, String fileExtensionOk, 
            AbstractFTPService ftpService, EmailSenderRemote emailSender, String summaryRecipient) {
        List<CrmDataElementProcessingResult> crmDataElementProcessingResultList = new ArrayList<CrmDataElementProcessingResult>();
        String categoryDesc = "APP";

        for (CRMOutboundBean crmOutboundBean : crmOutboundBeanList) {
            CrmDataElement crmDataElement = new CrmDataElement();
            crmDataElement.canale = crmOutboundBean.getCanale();
            crmDataElement.cdCarta = crmOutboundBean.getCdCarta();
            crmDataElement.cdCliente = crmOutboundBean.getCdCliente();
            crmDataElement.codiceFiscale = crmOutboundBean.getCodiceFiscale();
            crmDataElement.codIniziativa = crmOutboundBean.getCodIniziativa();
            crmDataElement.codOfferta = crmOutboundBean.getCodOfferta();
            crmDataElement.tmsInserimento = crmOutboundBean.getTmsInserimento();
            crmDataElement.treatmentCode = crmOutboundBean.getTreatmentCode();
            
            String deliveryId = "0";

            if (crmOutboundBean.getPushNotificationBean() != null && crmOutboundBean.getPushNotificationBean().getNotificationRead()) {
                deliveryId = "1";
            }
            
            CrmDataElementProcessingResult crmDataElementProcessingResult = CrmDataProcessor.createCrmDataElementProcessingResult(crmDataElement, categoryDesc, deliveryId);
            crmDataElementProcessingResultList.add(crmDataElementProcessingResult);
        }

        // Genera il file con l'esito dell'elaborazione
        String output = CrmDataProcessor.generateOutput(crmDataElementProcessingResultList);

        //System.out.println("Output file: " + output);

        // Scrivi il file con l'esito dell'elaborazione nella directory del server ftps ftpsResultPath
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd-HHmmss");
        String fileName = "EMM_ESITI_" + dateFormat.format(new Date());
        String outputFileName = ftpsResultPath + AbstractFTPService.separator + fileName + "." + fileExtensionData;

        Boolean putOutput = ftpService.putFile(output, outputFileName);

        if (!putOutput) {

            // Si � verificato un errore nella scrittura del file di output
            System.out.println("error creating file di output (" + outputFileName + ")");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }

        // Scrivi il file con l'ok dell'elaborazione nella directory del server ftps ftpsResultPath
        String resultFileName = ftpsResultPath + AbstractFTPService.separator + fileName + "." + fileExtensionOk;

        Boolean putResult = ftpService.putFile("", resultFileName);

        if (!putResult) {

            // Si � verificato un errore nella scrittura del file di result
            System.out.println("error creating file di result (" + resultFileName + ")");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }

        System.out.println("Invio email");
        
        EmailType emailType = EmailType.CRM_OUTBOUND_SUMMARY;
        List<Parameter> parameters = new ArrayList<Parameter>(0);

        
        
        SimpleDateFormat sdfDate = new SimpleDateFormat("dd/MM/yyyy");
        parameters.add(new Parameter("DATE", sdfDate.format(new Date())));
        List<Attachment> attachments = new ArrayList<Attachment>(0);

        Attachment attachment = new Attachment();
        attachment.setFileName(fileName + "." + fileExtensionData);
        attachment.setBytes(output.getBytes());
        attachments.add(attachment);

        if ( summaryRecipient == null || summaryRecipient.equals("") ) {
            
            // Recipient empty
            System.out.println("SendEmailResult: not sent - recipient empty");
        }
        else {
            
            // Sending email
            System.out.println("Sending email to: " + summaryRecipient);
            String sendEmailResult = emailSender.sendEmailWithAttachments(emailType, summaryRecipient, parameters, attachments);
            System.out.println("SendEmailResult: " + sendEmailResult);
        }
        
        
        return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
    }

    private Boolean matchPersonalData(UserBean userBean, CRMOutboundBean crmOutboundBean) {

        String checkEmail = userBean.getPersonalDataBean().getSecurityDataEmail();
        String checkTelCellulare = "";
        String crmTelCellulare = crmOutboundBean.getTelCellulare().replaceAll("0039", "");
        if (userBean.getMobilePhoneList() != null) {
            for (MobilePhoneBean mobilePhoneBean : userBean.getMobilePhoneList()) {
                if (mobilePhoneBean.getStatus() == MobilePhone.MOBILE_PHONE_STATUS_ACTIVE) {
                    checkTelCellulare = mobilePhoneBean.getNumber();
                    break;
                }
            }
        }

        if (checkTelCellulare == null || !checkTelCellulare.equals(crmTelCellulare)) {
            System.out.println("matchPersonalData error - field TelCellulare - expected: " + checkTelCellulare + ", found: " + crmOutboundBean.getTelCellulare());
            return Boolean.FALSE;
        }

        if (checkEmail == null || !checkEmail.equalsIgnoreCase(crmOutboundBean.getEmail())) {
            System.out.println("matchPersonalData error - field Email - expected: " + checkEmail + ", found: " + crmOutboundBean.getEmail());
            return Boolean.FALSE;
        }

        return Boolean.TRUE;
    }

}
