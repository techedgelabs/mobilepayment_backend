package com.techedge.mp.core.actions.scheduler;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.techedge.mp.core.business.CRMServiceRemote;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.interfaces.CrmOutboundProcessResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CrmSfDataElement;
import com.techedge.mp.core.business.utilities.AbstractFTPService;
import com.techedge.mp.core.business.utilities.CrmSfDataProcessor;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerProcessCrmSfOutboundInterfaceAction {

    private static final String JOB_NAME        = "JOB_PROCESS_CRM_SF_OUTBOUND_INTERFACE";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService           loggerService;

    @EJB
    private UserService           userService;
    
    
    public SchedulerProcessCrmSfOutboundInterfaceAction() {}

    public CrmOutboundProcessResponse execute(String ftpsReadPath, String ftpsArchivePath, String fileExtensionData, String fileExtensionOk, CRMServiceRemote CRMService, 
            AbstractFTPService ftpService, boolean checkEmptyTable, int maxDataPerThread, int maxThreads) throws EJBException {

        Date now = new Date();

        System.out.println("job name: " + SchedulerProcessCrmSfOutboundInterfaceAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());
        CrmOutboundProcessResponse crmOutboundProcessResponse = new CrmOutboundProcessResponse();

        try {
            
            if (checkEmptyTable) {
                int rowsCount = QueryRepository.getCRMSfOutboundRowsCount(em);
                
                if (rowsCount > 0) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Tabella CRM Outbound non vouta. "
                            + "Impossibile inizializzare il processo");

                    System.out.println("job endTimestamp: " + now.toString());

                    System.out.println("job result: " + "OK");

                    crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS);
                    crmOutboundProcessResponse.setStatusMessage("Tabella CRM Outbound non vouta. Impossibile inizializzare il processo");
                    return crmOutboundProcessResponse;
                }
            }
            
            // Lettura del file dalla directory del server ftps ftpsReadPath
            List<String> files = ftpService.openDirectory(ftpsReadPath);
            
            if (files.isEmpty()) {
                
                // La directory non contiene file da elaborare
                System.out.println("no valid file found in read path " + ftpsReadPath);
                
                System.out.println("job endTimestamp: " + now.toString());

                System.out.println("job result: " + "OK");

                crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS);
                crmOutboundProcessResponse.setStatusMessage("Directory sftp (" + ftpsReadPath + ") vuota. Nessun file da processare");
                return crmOutboundProcessResponse;
            }
            
            String fileOK = null;
            for (String file : files) {
                
                // Cerca il primo file con l'estensione OK
                if (file.toLowerCase().endsWith(fileExtensionOk)) {
                    fileOK = file;
                    break;
                }
            }
            
            if (fileOK == null) {
                
                // La directory non contiene nessun file con l'estensione richiesta
                System.out.println("no valid extension found in read path " + ftpsReadPath);
                
                System.out.println("job endTimestamp: " + now.toString());

                System.out.println("job result: " + "OK");

                crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS);
                crmOutboundProcessResponse.setStatusMessage("File OK valido o non presente");
                return crmOutboundProcessResponse;
            }
            
            
            // Sposta il file nella directory di archiviazione ftpsArchivePath
            String fileToProcess = fileOK.replace(fileExtensionOk, fileExtensionData);
            String remoteSource = ftpsReadPath + AbstractFTPService.separator + fileToProcess;
            String remoteDest   = ftpsArchivePath + AbstractFTPService.separator + fileToProcess;
            Boolean moveResult = ftpService.moveFile(remoteSource, remoteDest);
            
            if (!moveResult) {
                
                // Si � verificato un errore nello spostamento del file nel path di archiviazione
                System.out.println("error moving file from read path (" + ftpsReadPath + ") to archive path (" + ftpsArchivePath + ")");
                
                System.out.println("job endTimestamp: " + now.toString());

                System.out.println("job result: " + "OK");

                crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
                crmOutboundProcessResponse.setStatusMessage("Errore nello spostamento file nella directory archivio. Da: " + ftpsReadPath + "  a: " + ftpsArchivePath);
                return crmOutboundProcessResponse;
            }
            
            // Elimina il file OK
            String fileOkPath = ftpsReadPath + AbstractFTPService.separator + fileOK;
            Boolean deleteResult = ftpService.deleteFile(fileOkPath);
            
            if (!deleteResult) {
                
                // Si � verificato un errore nella cancellazione del file Ok dalla directory del server ftps ftpsReadPath
                System.out.println("error deleting file from read path (" + ftpsReadPath + ")");
                
                System.out.println("job endTimestamp: " + now.toString());

                System.out.println("job result: " + "OK");

                crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
                crmOutboundProcessResponse.setStatusMessage("Errore nella cancellazione del file: " + ftpsReadPath);
                return crmOutboundProcessResponse;
            }
            
            
            // Processa il file trasformandolo in un array di oggetti da elaborare
            //String data = "CD_CLIENTE|CD_CARTA|NOME|COGNOME|TEL_CELLULARE|EMAIL|SESSO|DATA_NASCITA|MSG|CANALE|COD_INIZIATIVA|COD_OFFERTA|TREATMENT_CODE|FLAG_ELAB|CODICE_FISCALE|C_2|C_3|C_4|C_5|TMS_INSERIMENTO|OBIETTIVO_PUNTI|OBIETTIVO_LITRI|C_6|C_7|C_8|C_9|C_10\n8056889|536200004770165318|LUCA|MANCINI|3383912245|lucamancini77@teletu.it|||Prova Messaggio Puntuale App Augusto|app|C000000588-000001381||000010198|P|MNCLCU77R15C773V|||||2017-05-22 18:27:53.0|||||||\n";
            String data = ftpService.getFile(remoteDest);
            
            List<CrmSfDataElement> crmDataElementList = CrmSfDataProcessor.process(data.trim(), maxDataPerThread, maxThreads);
            System.out.println("job total data to process: " + crmDataElementList.size());
            
            // Utilizza i dati estratti per generare le notifiche per gli utenti
            //:TODO
            CRMService.processCrmSfOutboundInterface(crmDataElementList);
            
            // Termina il job
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");

            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS);
            crmOutboundProcessResponse.setStatusMessage("Esecuzione lettura file ftp e inserimento dati terminato");
            return crmOutboundProcessResponse;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            crmOutboundProcessResponse.setStatusCode(ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE);
            crmOutboundProcessResponse.setStatusMessage("Errore nell'esecuzione del job di lettura file ftp e inserimento dati: " + ex2.getMessage());
            return crmOutboundProcessResponse;
        }
    }
}
