package com.techedge.mp.core.actions.scheduler;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SchedulerJobRemoveOnHoldTransactionAction {

    private static final String JOB_NAME = "JOB_REMOVE_ONHOLD_TRANSACTION";

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    public SchedulerJobRemoveOnHoldTransactionAction() {}

    public String execute(Date dateCheck) throws EJBException {

        Date now = new Date();

        System.out.println("job name: " + SchedulerJobRemoveOnHoldTransactionAction.JOB_NAME);

        System.out.println("job creationTimestamp: " + now.toString());

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            List<PostPaidTransactionBean> listTransaction = QueryRepository.findPostPaidTransactionOnHold(em, "ONHOLD", dateCheck);

            if (listTransaction == null || listTransaction.isEmpty()) {

                System.out.println("Nessuna transazione in stato ONHOLD da eliminare");

                userTransaction.commit();

                return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;
            }
            
            Integer count = 0;

            for (PostPaidTransactionBean postpaid : listTransaction) {

                System.out.println("eliminazione transazione: " + postpaid.getMpTransactionID());

                em.remove(postpaid);
                
                count++;
            }
            
            System.out.println("Transazioni totali eliminate: " + count);

            userTransaction.commit();

            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "OK");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            System.out.println("job endTimestamp: " + now.toString());

            System.out.println("job result: " + "KO");

            return ResponseHelper.SCHEDULER_JOB_EXECUTION_FAILURE;
        }
    }
}