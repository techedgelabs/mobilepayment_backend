package com.techedge.mp.core.actions.refueling;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.refueling.RefuelDetail;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTransactionStatusResponse;
import com.techedge.mp.core.business.interfaces.refueling.StatusMessageCode;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.RefuelingFuelTypeMapping;
import com.techedge.mp.core.business.utilities.RefuelingStatusMapping;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class RefuelingGetMPTransactionStatusAction extends RefuelingAbstractTransactionAction {

    private RefuelingStatusMapping          refuelingStatusMapping = new RefuelingStatusMapping();

    RefuelingGetMPTransactionStatusResponse refuelingGetMPTransactionStatusResponse;

    public RefuelingGetMPTransactionStatusAction() {}

    @Override
    protected <T extends StatusMessageCode> T execute(T instance, UserTransaction userTransaction, String requestID, String mpToken, String multicardCurrency, RefuelingNotificationServiceRemote refuelingNotificationService, 
            RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, PaymentServiceRemote paymentService, Object... obj) throws EJBException {

        refuelingGetMPTransactionStatusResponse = (RefuelingGetMPTransactionStatusResponse) instance;

        //        String statusCode = "MESSAGE_RECEIVED_200";
        //        String messageCode = "Message received";
        //        String mpTransactionStatus = "END_REFUELING";
        //        String timestampStartRefuel = "2016-01-01 10:30:00.123";
        //        String timestampEndRefuel = "2016-01-01 10:34:20.987";
        //        Double amount = 12.3;
        //        Double fuelQuantity = 9.8;
        //        String productID = "SP";
        //        String productDescription = "SenzaPb";
        //        String fuelType = "Benzina";

        String srcTransactionID = (String) obj[0];

        String mpTransactionID = null;

        if (obj.length > 1) {
            mpTransactionID = (String) obj[1];
        }

        System.out.println("Search with srcTransactionID: " + srcTransactionID + " and mpTransactionID: " + mpTransactionID);

        TransactionBean transactionBean = QueryRepository.findTransactionsBySrcTransactionID(getEm(), srcTransactionID);

        if (transactionBean != null) {

            if (mpTransactionID != null && !mpTransactionID.equals("")) {

                System.out.println("Verifica mpTransactionID");

                if (!transactionBean.getTransactionID().equals(mpTransactionID)) {

                    transactionBean = null;
                }
            }

            if (transactionBean != null) {

                String status = transactionBean.getLastTransactionStatus().getStatus();
                RefuelingEnumStatus mpTransactionStatus = refuelingStatusMapping.getEnumState(status);

                System.out.println("status: " + status);

                if (mpTransactionStatus != null) {
                    createRefuelDetail(status, transactionBean);
                    refuelingGetMPTransactionStatusResponse.setMpTransactionStatus(mpTransactionStatus.toString());
                }
                else {
                    System.out.println("mpTransactionStatus null");

                    refuelingGetMPTransactionStatusResponse.setMpTransactionStatus("");
                }

                refuelingGetMPTransactionStatusResponse.setMpTransactionID(transactionBean.getTransactionID());
                refuelingGetMPTransactionStatusResponse.setStatusCode(ResponseHelper.MESSAGE_RECEIVED_200);
            }
            else {

                System.out.println("mpTransactionID non corrispondente");

                refuelingGetMPTransactionStatusResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_EVENTS_WRONG_ID);
            }
        }
        else {

            System.out.println("No transaction found with srcTransactionID: " + srcTransactionID);

            refuelingGetMPTransactionStatusResponse.setStatusCode(ResponseHelper.RETRIEVE_REFUEL_EVENTS_WRONG_ID);
        }

        return (T) refuelingGetMPTransactionStatusResponse;
    }

    private void createRefuelDetail(String status, TransactionBean transactionBean) {

        RefuelDetail fuelDeatail = null;

        if (refuelingStatusMapping.getStringState(status).equals(RefuelingEnumStatus.END_REFUELING.toString())) {

            fuelDeatail = new RefuelDetail();

            fuelDeatail.setAmount(transactionBean.getFinalAmount());

            Date timestampStartRefuel = null;
            Date timestampEndRefuel   = null;
            
            for (TransactionStatusBean transactionStatusBean : transactionBean.getTransactionStatusBeanData()) {
                
                if ( transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PUMP_ENABLED)) {
                    timestampStartRefuel = new Date(transactionStatusBean.getTimestamp().getTime());
                }
                if ( transactionStatusBean.getStatus().equals(StatusHelper.STATUS_END_REFUEL_RECEIVED)) {
                    timestampEndRefuel = new Date(transactionStatusBean.getTimestamp().getTime());
                }
            }
            
            fuelDeatail.setTimestampStartRefuel(convertDateToString(timestampStartRefuel));
            fuelDeatail.setTimestampEndRefuel(convertDateToString(timestampEndRefuel));
            
            /*
            fuelDeatail.setTimestampStartRefuel(convertDateToString(transactionBean.getCreationTimestamp()));
            fuelDeatail.setTimestampEndRefuel(convertDateToString(transactionBean.getEndRefuelTimestamp()));
            */
            
            fuelDeatail.setAuthorizationCode(transactionBean.getAuthorizationCode());

            fuelDeatail.setFuelQuantity(transactionBean.getFuelQuantity());
            fuelDeatail.setProductID(transactionBean.getProductID());
            fuelDeatail.setProductDescription(transactionBean.getProductDescription());

            RefuelingFuelTypeMapping rftm = new RefuelingFuelTypeMapping();
            fuelDeatail.setFuelType(rftm.getFuelType(transactionBean.getProductID()));
        }

        refuelingGetMPTransactionStatusResponse.setRefuelDetail(fuelDeatail);
    }

    private String convertDateToString(Date refuelDate) {

        String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        try {
            dateString = sdfr.format(refuelDate);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        return dateString;
    }
}
