package com.techedge.mp.core.actions.refueling;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.refueling.StatusMessageCode;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

public abstract class RefuelingAbstractTransactionAction {

    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;

    @EJB
    private LoggerService   loggerService;

    private UserTransaction userTransaction;

    protected UserBean      userBean;
    
    protected Boolean       useNewAcquirerFlow;

    protected Boolean       isRefuelingOAuth2;

    protected abstract <T extends StatusMessageCode> T execute(T instance, UserTransaction userTransaction, String requestID, String mpToken, String multicardCurrency, RefuelingNotificationServiceRemote refuelingNotificationService, 
            RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, PaymentServiceRemote paymentService, Object... obj) throws EJBException;

    //    protected abstract List<TransactionBean> getTransactionBeanList(String startDate, String endDate, String mpTransaction);

    public <T extends StatusMessageCode> T commonExecute(Class<T> clazz, UserCategoryService userCategoryService, String requestID, String mpToken, String multicardCurrency, RefuelingNotificationServiceRemote refuelingNotificationService, 
            RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, PaymentServiceRemote paymentService, Object... obj) throws EJBException {

        try {

            userTransaction = context.getUserTransaction();
            T returned = clazz.newInstance();

             userTransaction.begin();

            //controllo che mpToken sia prensente
            TicketBean ticketBean = null;
            ticketBean = QueryRepository.findTicketById(em, mpToken);

            if (ticketBean != null && ticketBean.isRefuelingTicket() && ticketBean.isValid()) {

                userBean = ticketBean.getUser();
                if (userBean == null || (userBean.getUserType() != User.USER_TYPE_REFUELING && userBean.getUserType() != User.USER_TYPE_REFUELING_NEW_ACQUIRER 
                        && userBean.getUserType() != User.USER_TYPE_REFUELING_OAUTH2 )) {

                    // Ticket non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found");

                    userTransaction.commit();

                    returned.setStatusCode(ResponseHelper.USER_UPDATE_INVALID_TICKET);
                    return returned;
                }
                
                Integer userType = userBean.getUserType();
                this.useNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
                this.isRefuelingOAuth2 = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.UTENTI_REFUELING_OAUTH2.getCode());

                //chiamata alla classe concreta
                returned = execute(returned, userTransaction, requestID, mpToken, multicardCurrency, refuelingNotificationService, refuelingNotificationServiceOAuth2, paymentService, obj);

            }
            else {

                // Se non esiste il ticket resitituisci il messaggio di errore
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Token " + mpToken + " not found");
                returned.setStatusCode(ResponseHelper.TOKEN_INVALID_500);
            }

            userTransaction.commit();
            
            return returned;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED refueling authentication with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

    protected EJBContext getContext() {

        return context;
    }

    protected EntityManager getEm() {

        return em;
    }

    protected LoggerService getLoggerService() {

        return loggerService;
    }

    protected UserTransaction getUserTransaction() {

        return userTransaction;
    }

}
