package com.techedge.mp.core.actions.refueling;

public enum RefuelingEnumStatus {

    START_REFUELING ("START_REFUELING",0),
    REFUELING_IN_PROGRESS ("REFUELING_IN_PROGRESS",1),
    END_REFUELING ("END_REFUELING",2),
    RECONCILIATION ("RECONCILIATION",3),
    ERROR ("ERROR",4);

    private final String status;       
    private final int value; 
    
    private RefuelingEnumStatus(String status, int value) {
        this.status = status;
        this.value = value;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : status.equals(otherName);
    }

    public String toString() {
       return this.status;
    }
    
    public int getInt(){
        return this.value;
    }
}
