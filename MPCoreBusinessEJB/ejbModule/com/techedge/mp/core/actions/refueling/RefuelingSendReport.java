package com.techedge.mp.core.actions.refueling;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ejb.EJBTransactionRolledbackException;
import javax.persistence.EntityManager;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.LoggerServiceRemote;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Pair;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.RefuelingFuelTypeMapping;
import com.techedge.mp.core.business.utilities.RefuelingStatusMapping;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.MpTransactionDetail;
import com.techedge.mp.refueling.integration.entities.RefuelDetail;

public class RefuelingSendReport implements Runnable {

    private String                                   requestID;
    private Date                                     startDate;
    private Date                                     endDate;
    private String                                   mpTransactionID;
    private LoggerServiceRemote                      loggerService;
    private RefuelingNotificationServiceRemote       refuelingNotificationService;
    private RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2;
    private boolean                                  isRefuelingOAuth2;
    private EntityManager                            em;
    private UserTransaction                          userTransaction;

    public RefuelingSendReport(EntityManager em, Date startDate, Date endDate, String mpTransactionID, UserTransaction userTransaction, LoggerService loggerService, 
            RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, boolean isRefuelingOAuth2) {

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        this.startDate = calendar.getTime();

        calendar.setTime(endDate);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        this.endDate = calendar.getTime();

        this.em = em;
        this.mpTransactionID = mpTransactionID;
        this.userTransaction = userTransaction;
        this.loggerService = loggerService;
        this.refuelingNotificationService = refuelingNotificationService;
        this.refuelingNotificationServiceOAuth2 = refuelingNotificationServiceOAuth2;
        this.isRefuelingOAuth2 = isRefuelingOAuth2;
    }

    private void log(ErrorLevel level, String className, String methodName, String groupId, String phaseId, String message) {

        try {
            this.loggerService.log(level, className, methodName, groupId, phaseId, message);
        }
        catch (Exception ex) {

            System.out.println("LoggerService is not available: " + ex.getMessage());
        }
    }

    @Override
    public void run() {

        List<MpTransactionDetail> listResult = new ArrayList<MpTransactionDetail>(0);

        List<TransactionBean> listTransaction = new ArrayList<TransactionBean>(0);
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

        try {
            userTransaction.begin();

            String mpTransactionID = this.mpTransactionID;

            if (mpTransactionID == null || mpTransactionID.equals("")) {
                listTransaction = QueryRepository.findTransactionsByDateAndSrcTransaction(em, startDate, endDate);

            }
            else {
                TransactionBean transaction = QueryRepository.findTransactionBeanById(em, mpTransactionID);
                if (transaction != null) {
                    if (startDate == null && endDate == null) {
                        listTransaction.add(transaction);
                    }
                    else {
                        if (startDate == null) {
                            if (transaction.getCreationTimestamp().before(endDate)) {
                                listTransaction.add(transaction);
                            }
                        }
                        else if (endDate == null) {
                            if (transaction.getCreationTimestamp().after(startDate)) {
                                listTransaction.add(transaction);
                            }
                        }
                        else {
                            if (transaction.getCreationTimestamp().after(startDate) && transaction.getCreationTimestamp().before(endDate)) {
                                listTransaction.add(transaction);
                            }
                        }

                    }
                }
            }

            /* Conversione in MPTransactionDetail */
            RefuelingStatusMapping refuelingStatusMapping = new RefuelingStatusMapping();
            RefuelingFuelTypeMapping refuelingFuelTypeMapping = new RefuelingFuelTypeMapping();
            System.out.println(listTransaction.size());
            if (listTransaction != null) {
                for (TransactionBean transactionDetail : listTransaction) {
                    if (refuelingStatusMapping.getStringState(transactionDetail.getLastTransactionStatus().getStatus()) != null) {
                        //RefuelDetail

                        RefuelDetail refuelDetail = new RefuelDetail();

                        Date timestampStartRefuel = null;
                        Date timestampEndRefuel = null;

                        for (TransactionStatusBean transactionStatusBean : transactionDetail.getTransactionStatusBeanData()) {

                            if (transactionStatusBean.getStatus().equals(StatusHelper.STATUS_PUMP_ENABLED)) {
                                timestampStartRefuel = new Date(transactionStatusBean.getTimestamp().getTime());
                            }
                            if (transactionStatusBean.getStatus().equals(StatusHelper.STATUS_END_REFUEL_RECEIVED)) {
                                timestampEndRefuel = new Date(transactionStatusBean.getTimestamp().getTime());
                            }
                        }

                        refuelDetail.setTimestampStartRefuel(convertDateToString(timestampStartRefuel));
                        refuelDetail.setTimestampEndRefuel(convertDateToString(timestampEndRefuel));

                        if (transactionDetail.getAuthorizationCode() != null) {
                            refuelDetail.setAuthorizationCode(transactionDetail.getAuthorizationCode());
                        }
                        if (transactionDetail.getFinalAmount() != null) {
                            refuelDetail.setAmount(transactionDetail.getFinalAmount());
                        }
                        if (transactionDetail.getFuelQuantity() != null) {
                            refuelDetail.setFuelQuantity(transactionDetail.getFuelQuantity());
                        }
                        if (transactionDetail.getProductID() != null) {
                            refuelDetail.setProductID(transactionDetail.getProductID());
                        }
                        if (transactionDetail.getProductDescription() != null) {
                            refuelDetail.setProductDescription(transactionDetail.getProductDescription());
                        }
                        if (transactionDetail.getProductID() != null) {
                            refuelDetail.setFuelType(refuelingFuelTypeMapping.getFuelType(transactionDetail.getProductID()));
                        }

                        //MPTransactionDetail
                        MpTransactionDetail item = new MpTransactionDetail();
                        if (transactionDetail.getTransactionID() != null) {
                            item.setMpTransactionID(transactionDetail.getTransactionID());
                        }
                        if (transactionDetail.getSrcTransactionID() != null) {
                            item.setSrcTransactionID(transactionDetail.getSrcTransactionID());
                        }
                        if (transactionDetail.getLastTransactionStatus() != null) {
                            item.setMpTransactionStatus(refuelingStatusMapping.getStringState(transactionDetail.getLastTransactionStatus().getStatus()));
                        }
                        item.setRefuelDetail(refuelDetail);

                        listResult.add(item);
                    }

                }
            }
            //System.out.println("DIMENSIONE: " + listResult.size());
            String startDateS = format.format(startDate);
            String endDateS = format.format(endDate);
            Set<Pair<String, String>> inputParameters = new HashSet<Pair<String, String>>();
            inputParameters.add(new Pair<String, String>("requestID", this.requestID));
            inputParameters.add(new Pair<String, String>("startDate", startDateS));
            inputParameters.add(new Pair<String, String>("endDate", endDateS));

            String listTransactionString = "";
            listTransactionString = listTransactionString + "[ ";
            int count = 0;
            for (MpTransactionDetail item : listResult) {
                //System.out.println(count);
                if (count > 0) {
                    listTransactionString = listTransactionString + ",";
                }
                listTransactionString = listTransactionString + "{ \"mpTransactionID\": \"" + item.getMpTransactionID() + "\"," + "\"srcTransactionID\": \""
                        + item.getSrcTransactionID() + "\"," + "\"mpTransactionStatus\": \"" + item.getMpTransactionStatus() + "\"," + "\"refuelDetail\": {"
                        + "\"timestampStartRefuel\": \"" + item.getRefuelDetail().getTimestampStartRefuel() + "\"," + "\"timestampEndRefuel\": \""
                        + item.getRefuelDetail().getTimestampEndRefuel() + "\"," + "\"authorizationCode\": \"" + item.getRefuelDetail().getAuthorizationCode() + "\","
                        + "\"amount\": \"" + item.getRefuelDetail().getAmount() + "\"," + "\"fuelType\": \"" + item.getRefuelDetail().getFuelType() + "\","
                        + "\"fuelQuantity\": \"" + item.getRefuelDetail().getFuelQuantity() + "\"," + "\"productID\": \"" + item.getRefuelDetail().getProductID() + "\","
                        + "\"productDescription\": \"" + item.getRefuelDetail().getProductDescription() + "\"}}\n";
                count++;
            }
            listTransactionString = listTransactionString + " ]";
            System.out.println(listTransactionString);

            inputParameters.add(new Pair<String, String>("mpTransactionList", listTransactionString));
            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "run", this.requestID, "opening", ActivityLog.createLogMessage(inputParameters));

            String response;
            if (isRefuelingOAuth2) {
                response = refuelingNotificationServiceOAuth2.sendMPTransactionReport(requestID, startDateS, endDateS, listResult);
            }
            else {
                response = refuelingNotificationService.sendMPTransactionReport(requestID, startDateS, endDateS, listResult);
            }

            Set<Pair<String, String>> outputParameters = new HashSet<Pair<String, String>>();
            outputParameters.add(new Pair<String, String>("response", response));

            this.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "run", this.requestID, "closing", ActivityLog.createLogMessage(outputParameters));

            userTransaction.commit();
        }
        catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (EJBTransactionRolledbackException e) {
            System.out.println("Exception caught");
        }
        catch (Exception ex) {

            ex.printStackTrace();

            this.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "run", this.requestID, null, "Exception message: " + ex.getMessage());

        }
    }

    private String convertDateToString(Date refuelDate) {

        if (refuelDate == null) {
            return "";
        }

        String dateString = null;
        SimpleDateFormat sdfr = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");

        try {
            dateString = sdfr.format(refuelDate);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        return dateString;
    }
}
