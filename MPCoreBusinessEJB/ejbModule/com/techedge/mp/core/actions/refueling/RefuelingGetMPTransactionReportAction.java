package com.techedge.mp.core.actions.refueling;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTransactionReportResponse;
import com.techedge.mp.core.business.interfaces.refueling.StatusMessageCode;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class RefuelingGetMPTransactionReportAction extends RefuelingAbstractTransactionAction {

    DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    public RefuelingGetMPTransactionReportAction() {}

    //    public RefuelingGetMPTransactionReportResponse execute(String requestID, String mpToken, String startDate, String endDate, String mpTransactionID) throws EJBException {
    //        
    //        UserTransaction userTransaction = context.getUserTransaction();
    //
    //        try {
    //
    //            userTransaction.begin();
    //
    //            RefuelingGetMPTransactionReportResponse refuelingGetMPTransactionReportResponse = new RefuelingGetMPTransactionReportResponse();
    //            
    //            String statusCode = "MESSAGE_RECEIVED_200";
    //            String messageCode = "Message received";
    //           
    //            refuelingGetMPTransactionReportResponse.setMessageCode(messageCode);
    //            refuelingGetMPTransactionReportResponse.setStatusCode(statusCode);
    //            
    //            userTransaction.commit();
    //            
    //            return refuelingGetMPTransactionReportResponse;
    //        }
    //        catch (Exception ex2) {
    //
    //            try {
    //                userTransaction.rollback();
    //            }
    //            catch (IllegalStateException e) {
    //                // TODO Auto-generated catch block
    //                e.printStackTrace();
    //            }
    //            catch (SecurityException e) {
    //                // TODO Auto-generated catch block
    //                e.printStackTrace();
    //            }
    //            catch (SystemException e) {
    //                // TODO Auto-generated catch block
    //                e.printStackTrace();
    //            }
    //
    //            String message = "FAILED refueling getMPTransactionReport with message (" + ex2.getMessage() + ")";
    //            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);
    //
    //            throw new EJBException(ex2);
    //        }
    //        
    //    }

    @Override
    protected <T extends StatusMessageCode> T execute(T instance, UserTransaction userTransaction, String requestID, String mpToken, String multicardCurrency, RefuelingNotificationServiceRemote refuelingNotificationService, 
            RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, PaymentServiceRemote paymentService, Object... obj) throws EJBException {
        
        RefuelingGetMPTransactionReportResponse refuelingGetMPTransactionReportResponse = (RefuelingGetMPTransactionReportResponse) instance;
        
//        try {
//            Thread.sleep(60000);
//        }
//        catch (InterruptedException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }
        
        String statusCode = "";
        String messageCode = "";
        Date startDate;
        Date endDate;
        try {
            startDate = format.parse((String) obj[0]);
            endDate = format.parse((String) obj[1]);
            String mpTransactionID = (String) obj[2];
            LoggerService loggerService = (LoggerService) obj[3];
            Runnable r = new RefuelingSendReport(getEm(), startDate, endDate, mpTransactionID, userTransaction, loggerService, refuelingNotificationService, 
                    refuelingNotificationServiceOAuth2, isRefuelingOAuth2);
            
            new Thread(r).start();
            
            statusCode = "MESSAGE_RECEIVED_200";
            messageCode = "Report available";
            refuelingGetMPTransactionReportResponse.setMessageCode(messageCode);
            refuelingGetMPTransactionReportResponse.setStatusCode(statusCode);
        }
        catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return (T) refuelingGetMPTransactionReportResponse;

    }

}
