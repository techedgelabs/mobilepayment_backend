package com.techedge.mp.core.actions.refueling;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.TransactionV2Service;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.TransactionCategoryType;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingCreateMPTransactionResponse;
import com.techedge.mp.core.business.interfaces.refueling.StatusMessageCode;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TransactionAdditionalDataBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionOperationBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.voucher.PaymentTokenPackageBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.PaymentServiceRemote;
import com.techedge.mp.fidelity.adapter.business.exception.FidelityServiceException;
import com.techedge.mp.fidelity.adapter.business.interfaces.CodeEnum;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnumStatusType;
import com.techedge.mp.fidelity.adapter.business.interfaces.ExecuteAuthorizationResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.KeyValueInfo;
import com.techedge.mp.fidelity.adapter.business.interfaces.McCardEnjoyAuthorizeResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PaymentMode;
import com.techedge.mp.fidelity.adapter.business.interfaces.ProductDetailInfo;
import com.techedge.mp.fidelity.adapter.business.interfaces.ResultDetail;
import com.techedge.mp.fidelity.adapter.business.utilities.AmountConverter;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class RefuelingCreateMPTransactionAction extends RefuelingAbstractTransactionAction {

    private static final String PARAM_PAYMENT_TYPE                = "PAYMENT_TYPE";
    private static final String PARAM_SERVER_NAME                 = "SERVER_NAME";
    private static final String PARAM_RECONCILIATION_MAX_ATTEMPTS = "RECONCILIATION_MAX_ATTEMPTS";
    private static final String PARAM_STATUS_MAX_ATTEMPTS         = "STATUS_MAX_ATTEMPTS";

    private String              requestID;
    private String              srcTransactionID;
    private String              transactionID;
    private String              stationID;
    private String              pumpID;
    private String              currency;
    private Double              amount;
    private PaymentInfoBean     paymentInfoBean;
    private StationBean         stationBean;
    private Integer             pumpNumber;
    private String              refuelMode;

    @EJB
    private ParametersService   parametersService;

    public RefuelingCreateMPTransactionAction() {

    }

    @SuppressWarnings("unchecked")
    @Override
    protected <T extends StatusMessageCode> T execute(T instance, UserTransaction userTransaction, String requestID, String mpToken, String multicardCurrency, RefuelingNotificationServiceRemote refuelingNotificationService, 
            RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, PaymentServiceRemote paymentService, Object... obj) throws EJBException {

        RefuelingCreateMPTransactionResponse refuelingCreateTransactionResponse = (RefuelingCreateMPTransactionResponse) instance;

        this.requestID = requestID;

        srcTransactionID = (String) obj[0];
        stationID = (String) obj[1];
        pumpID = (String) obj[2];
        amount = (Double) obj[3];
        currency = (String) obj[4];
        pumpNumber = (Integer) obj[5];
        refuelMode = (String) obj[6];

        TransactionBean transactionBeanSearch = QueryRepository.findTransactionsBySrcTransactionID(getEm(), srcTransactionID);

        if (transactionBeanSearch != null) {

            // Esiste gi� una transazione con lo stesso srcTransactionID
            getLoggerService().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, "Transaction exists with srcTransactionID " + srcTransactionID);

            refuelingCreateTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
            return (T) refuelingCreateTransactionResponse;
        }

        transactionID = new IdGenerator().generateId(16).substring(0, 32);

        long timeStamp = System.currentTimeMillis();
        String creationTimestamp = String.valueOf(timeStamp);

        stationBean = QueryRepository.findStationBeanById(getEm(), stationID);

        if (stationBean == null) {

            // Lo stationID inserito non corrisponde a nessuna stazione valida
            getLoggerService().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, "No station for stationID " + stationID);

            refuelingCreateTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
            return (T) refuelingCreateTransactionResponse;
        }

        if (!stationBean.getRefuelingActive().booleanValue()) {

            // Lo stationID inserito non corrisponde a nessuna stazione valida
            getLoggerService().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, "Station is not active for refueling transactions: " + stationID);

            refuelingCreateTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
            return (T) refuelingCreateTransactionResponse;
        }
        
        paymentInfoBean = findPaymentInfo();
        // Verifica che il metodo di pagamento selezionato sia in uno stato valido
        if (paymentInfoBean == null) {

            // Il metodo di pagamento selezionato non esiste
            getLoggerService().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong payment data");
            refuelingCreateTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
            return (T) refuelingCreateTransactionResponse;
        }
        
        if (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
            
            String shopLogin  = "ENIPAY" + stationBean.getStationID();
            String acquirerID = "MULTICARD";
            
            TransactionBean transactionBean = createMulticardTransactionBean(timeStamp, multicardCurrency, shopLogin, acquirerID, paymentService);
            
            getEm().persist(transactionBean);
            
            if (transactionBean.getPaymentTokenPackageBean() != null) {
                System.out.println("Memorizzazione paymentTokenPackageBean");
                getEm().persist(transactionBean.getPaymentTokenPackageBean());
            }
            
            for (TransactionStatusBean transactionStatusBean : transactionBean.getTransactionStatusBeanData()) {
                System.out.println("Memorizzazione transactionStatusBean");
                getEm().persist(transactionStatusBean);
            }
            
            for (TransactionEventBean transactionEventBean : transactionBean.getTransactionEventBeanData()) {
                System.out.println("Memorizzazione transactionEventBean");
                getEm().persist(transactionEventBean);
            }
            
            for (TransactionOperationBean transactionOperationBean : transactionBean.getTransactionOperationBeanData()) {
                System.out.println("Memorizzazione transactionOperationBean");
                getEm().persist(transactionOperationBean);
            }
            
            refuelingCreateTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_SUCCESS);
            refuelingCreateTransactionResponse.setMpTransactionID(transactionID);
            refuelingCreateTransactionResponse.setMpCreationTimestamp(creationTimestamp);
            refuelingCreateTransactionResponse.setMpShopLogin(shopLogin);
            refuelingCreateTransactionResponse.setMpAcquirerID(acquirerID);
            refuelingCreateTransactionResponse.setToken(transactionBean.getToken());
            refuelingCreateTransactionResponse.setCurrency(multicardCurrency);
            
            if (transactionBean.getFinalStatusType() != null && transactionBean.getFinalStatusType().equals(StatusHelper.FINAL_STATUS_TYPE_ERROR)) {
                refuelingCreateTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
                refuelingCreateTransactionResponse.setMpTransactionID(null);
            }

            return (T) refuelingCreateTransactionResponse;
        }

        String shopLogin = stationBean.getOilShopLogin();
        String acquirerID = stationBean.getOilAcquirerID();
        
        if (this.useNewAcquirerFlow || this.isRefuelingOAuth2) {
            
            System.out.println("New acquirer flow");
            
            if (stationBean.getDataAcquirer() != null) {
                
                shopLogin  = stationBean.getDataAcquirer().getApiKey();
                acquirerID = stationBean.getDataAcquirer().getAcquirerID();
                currency   = stationBean.getDataAcquirer().getCurrency();
            }
            else {

                // Il PV non � configurato per il nuovo acquirer
                getLoggerService().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, "PV non enabled for new acquirer");
                refuelingCreateTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_FAILURE);
                return (T) refuelingCreateTransactionResponse;
            }
        }

        TransactionBean transactionBean = createTransactionBean(timeStamp);

        refuelingCreateTransactionResponse.setStatusCode(ResponseHelper.PP_TRANSACTION_CREATE_SUCCESS);
        refuelingCreateTransactionResponse.setMpTransactionID(transactionID);
        refuelingCreateTransactionResponse.setMpCreationTimestamp(creationTimestamp);
        refuelingCreateTransactionResponse.setMpShopLogin(shopLogin);
        refuelingCreateTransactionResponse.setMpAcquirerID(acquirerID);
        refuelingCreateTransactionResponse.setToken(transactionBean.getToken());
        refuelingCreateTransactionResponse.setCurrency(currency);
        
        System.out.println("currency: " + currency);

        return (T) refuelingCreateTransactionResponse;
    }

    private PaymentInfoBean findPaymentInfo() {

        // prendo il medodo di pagamento di default
        PaymentInfoBean paymentInfo = null;

        getLoggerService().log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "execute findPaymentInfo", requestID, null, "for search Default PaymentInfo");

        paymentInfo = this.userBean.findDefaultPaymentInfoBean();

        return paymentInfo;
    }

    private TransactionBean createTransactionBean(long timeStamp) {

        TransactionBean transactionB = new TransactionBean();

        try {

            String paymentType = parametersService.getParamValue(PARAM_PAYMENT_TYPE);
            String serverName = this.parametersService.getParamValue(PARAM_SERVER_NAME);
            Integer reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));

            
            String shopLogin        = stationBean.getOilShopLogin();
            String acquirerID       = stationBean.getOilAcquirerID();
            String groupAcquirer    = "";
            String encodedSecretKey = "";
            
            if (this.useNewAcquirerFlow || this.isRefuelingOAuth2) {
                
                currency   = stationBean.getDataAcquirer().getCurrency();
                shopLogin  = stationBean.getDataAcquirer().getApiKey();
                acquirerID = stationBean.getDataAcquirer().getAcquirerID();
                groupAcquirer = stationBean.getDataAcquirer().getGroupAcquirer();
                encodedSecretKey = stationBean.getDataAcquirer().getEncodedSecretKey();
            }
            
            transactionB.setSrcTransactionID(srcTransactionID);
            transactionB.setTransactionID(transactionID);
            transactionB.setUserBean(userBean);
            transactionB.setStationBean(stationBean);
            transactionB.setUseVoucher(false);
            transactionB.setCreationTimestamp(new Date(timeStamp));
            transactionB.setEndRefuelTimestamp(null);
            transactionB.setInitialAmount(amount);
            transactionB.setFinalAmount(null);
            transactionB.setFuelAmount(null);
            transactionB.setFuelQuantity(null);
            transactionB.setPumpID(pumpID);
            transactionB.setPumpNumber(pumpNumber);
            transactionB.setProductID(null);
            transactionB.setProductDescription(null);
            transactionB.setCurrency(currency);
            transactionB.setShopLogin(shopLogin);
            transactionB.setAcquirerID(acquirerID);
            transactionB.setBankTansactionID(null);
            transactionB.setToken(paymentInfoBean.getToken());
            transactionB.setProductID(null);
            transactionB.setPaymentType(paymentType);
            transactionB.setFinalStatusType(null);
            transactionB.setGFGNotification(false);
            transactionB.setConfirmed(false);
            transactionB.setPaymentMethodId(paymentInfoBean.getId());
            transactionB.setPaymentMethodType(paymentInfoBean.getType());
            transactionB.setServerName(serverName);
            transactionB.setStatusAttemptsLeft(null);
            transactionB.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
            transactionB.setOutOfRange("false");
            transactionB.setRefuelMode(refuelMode);
            transactionB.setVoucherReconciliation(false);
            transactionB.setGroupAcquirer(groupAcquirer);
            transactionB.setEncodedSecretKey(encodedSecretKey);
            transactionB.setTransactionCategory(TransactionCategoryType.TRANSACTION_CUSTOMER);

            TransactionStatusBean transactionStatusBean = createTransactionStatusBean(timeStamp, transactionB);

            getEm().persist(transactionB);
            getEm().persist(transactionStatusBean);

        }
        catch (ParameterNotFoundException e) {
            throw new EJBException(e.getMessage());
        }
        return transactionB;
    }

    private TransactionStatusBean createTransactionStatusBean(long timeStamp, TransactionBean transactionBean) {

        // Crea lo stato iniziale della transazione
        TransactionStatusBean transactionStatusBean = new TransactionStatusBean();

        transactionStatusBean.setTransactionBean(transactionBean);
        transactionStatusBean.setSequenceID(1);
        transactionStatusBean.setTimestamp(new Timestamp(timeStamp));
        transactionStatusBean.setStatus(StatusHelper.STATUS_NEW_REFUELING_REQUEST);
        transactionStatusBean.setSubStatus(null);
        transactionStatusBean.setSubStatusDescription("Started");
        transactionStatusBean.setRequestID(requestID);

        return transactionStatusBean;
    }
    
    
    private TransactionBean createMulticardTransactionBean(long timeStamp, String currency, String shopLogin, String acquirerID, PaymentServiceRemote paymentService) {

        String paymentType                = "";
        String serverName                 = "";
        Integer reconciliationMaxAttempts = null;
        Integer statusMaxAttempts         = null;
        
        try {
            paymentType               = this.parametersService.getParamValue(PARAM_PAYMENT_TYPE);
            serverName                = this.parametersService.getParamValue(PARAM_SERVER_NAME);
            reconciliationMaxAttempts = Integer.valueOf(this.parametersService.getParamValue(PARAM_RECONCILIATION_MAX_ATTEMPTS));
            statusMaxAttempts         = Integer.valueOf(this.parametersService.getParamValue(PARAM_STATUS_MAX_ATTEMPTS));
        }
        catch (ParameterNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            
            throw new EJBException("Errore inizializzazione parametri");
        }
        
        
        Date now = new Date();
        
        // Genera un operationID da utilizzare nella chiamata al servizio di generazione crittogramma
        String operationIDAuthCrypt = new IdGenerator().generateId(16).substring(0, 32);
        Integer integerAmount       = AmountConverter.toMobile(amount);
        String mcCardDpan           = paymentInfoBean.getToken();
        Long requestTimestamp       = now.getTime();
        String serverSerialNumber   = paymentInfoBean.getCheckBankTransactionID();
        String userId               = userBean.getPersonalDataBean().getSecurityDataEmail();
        
        getLoggerService().log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "createMulticardTransactionBean", requestID, null, "Generazione authCryptogram");
        
        McCardEnjoyAuthorizeResult mcCardEnjoyAuthorizeResult = null;
        
        try {
            mcCardEnjoyAuthorizeResult = paymentService.mcCardEnjoyAuthorize(
                    operationIDAuthCrypt,
                    integerAmount,
                    currency,
                    mcCardDpan,
                    PartnerType.EM,
                    PaymentMode.AUTH_ONLY,
                    requestTimestamp,
                    serverSerialNumber,
                    userId
                );
        }
        catch (FidelityServiceException e) {
            
            getLoggerService().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createMulticardTransactionBean", requestID, null, "authCryptogram generation error");
            
            //e.printStackTrace();
            throw new EJBException(e.getMessage());
        }
        
        if (mcCardEnjoyAuthorizeResult == null) {
            
            getLoggerService().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createMulticardTransactionBean", requestID, null, "mcCardEnjoyAuthorizeResult null");
            
            // Gestione ritorno null
            throw new EJBException("mcCardEnjoyAuthorizeResult null");
        }
        
        if (!mcCardEnjoyAuthorizeResult.getResult().getStatus().value().equals(EnumStatusType.SUCCESS.value())) {
            
            /* Codici di ritorno
            00000 Nessun errore
            00001 Parametri in input errati
            00106 Partner Type non valido
            00109 PAN non trovato
            00114 Operazione di pagamento eseguita con altro dispositivo
            00130 DPAN multicard associata ad altro ServerSerialNumber/Utenza Refueling
            00131 ServerSerialNumber Enjoy associato ad altra Utenza Refueling
            99001 Generic error 
            *
            */
            
            getLoggerService().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createMulticardTransactionBean", requestID, null, "mcCardEnjoyAuthorizeResult " + mcCardEnjoyAuthorizeResult.getResult().getCode().name());
            
            // Gestione ritorno null
            throw new EJBException("mcCardEnjoyAuthorizeResult " + mcCardEnjoyAuthorizeResult.getResult().getCode().name());
        }
        
        String authCryptogram = mcCardEnjoyAuthorizeResult.getAuthCryptogram();
        
        System.out.println("authCryptogram: " + authCryptogram);
        
        
        String outOfRange = "false";

        Timestamp creationTimestamp = new java.sql.Timestamp(now.getTime());
        
        PaymentTokenPackageBean paymentTokenPackageBean = new PaymentTokenPackageBean();
        paymentTokenPackageBean.setData(authCryptogram);
        paymentTokenPackageBean.setType(PaymentTokenPackageBean.TYPE_MULTICARD);
        
        
        // Genera un operationID da utilizzare nella chiamata al servizio di preautorizzazione
        String operationID = new IdGenerator().generateId(16).substring(0, 32);
        
        System.out.println("Preautorizzazione credito con Multicard");
        
        ExecuteAuthorizationResult executeAuthorizationResult = null;
        
        try {
            executeAuthorizationResult = paymentService.executeAuthorization(
                    operationID,
                    integerAmount,
                    authCryptogram,
                    currency,
                    mcCardDpan,
                    stationID,
                    PartnerType.EM,
                    requestTimestamp);
        }
        catch (FidelityServiceException e) {
            
            getLoggerService().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createMulticardTransactionBean", requestID, null, "executeAuthorization error");
            
            //e.printStackTrace();
            throw new EJBException(e.getMessage());
        }
        
        if (executeAuthorizationResult == null) {
            
            getLoggerService().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createMulticardTransactionBean", requestID, null, "executeAuthorizationResult null");
            
            // Crea la transazione con stato in errore per consentire la cancellazione dell'autorizzazione
            
            TransactionBean transactionBean = new TransactionBean();
            transactionBean.setSrcTransactionID(srcTransactionID);
            transactionBean.setTransactionID(transactionID);
            transactionBean.setUserBean(userBean);
            transactionBean.setStationBean(stationBean);
            transactionBean.setUseVoucher(false);
            transactionBean.setCreationTimestamp(creationTimestamp);
            transactionBean.setEndRefuelTimestamp(null);
            transactionBean.setInitialAmount(amount);
            transactionBean.setFinalAmount(null);
            transactionBean.setFuelAmount(null);
            transactionBean.setFuelQuantity(null);
            transactionBean.setPumpID(pumpID);
            transactionBean.setPumpNumber(pumpNumber);
            transactionBean.setCurrency(currency);
            transactionBean.setShopLogin(shopLogin);
            transactionBean.setAcquirerID(acquirerID);
            transactionBean.setBankTansactionID(null);
            transactionBean.setToken(mcCardDpan);
            transactionBean.setPaymentType(paymentType);
            transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_ERROR);
            transactionBean.setGFGNotification(false);
            transactionBean.setConfirmed(false);
            transactionBean.setPaymentMethodId(paymentInfoBean.getId());
            transactionBean.setPaymentMethodType(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);
            transactionBean.setServerName(serverName);
            transactionBean.setStatusAttemptsLeft(statusMaxAttempts);
            transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
            transactionBean.setOutOfRange(outOfRange);
            transactionBean.setRefuelMode(refuelMode);
            transactionBean.setVoucherReconciliation(false);
            transactionBean.setAuthorizationCode(null);
            transactionBean.setTransactionCategory(TransactionCategoryType.TRANSACTION_CUSTOMER);
            transactionBean.setPaymentTokenPackageBean(paymentTokenPackageBean);
            
            this.generateTransactionErrorData(requestID, transactionBean, creationTimestamp, amount, StatusHelper.STATUS_PAYMENT_NOT_AUTHORIZED, "", "Negative Response (TransactionResult=ERROR)", "99999", "Generic error", "ERROR", operationID, requestTimestamp, executeAuthorizationResult);
            
            return transactionBean;
        }
        
        if (!executeAuthorizationResult.getResult().getStatus().value().equals(EnumStatusType.SUCCESS.value())) {
            
            getLoggerService().log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "createMulticardTransactionBean", requestID, null, "executeAuthorizationResult " + executeAuthorizationResult.getResult().getCode().name());
            
            // Gestione stato != SUCCESS
            CodeEnum code  = executeAuthorizationResult.getResult().getCode();
            String message = executeAuthorizationResult.getResult().getMessage();
            
            code = CodeEnum.GENERIC_ERROR;
            
            // Gestione errori in preautorizzazione
            
            if (code.getValue().equals(CodeEnum.TIMEOUT_OPERATION.getValue()) ||
                code.getValue().equals(CodeEnum.OPERATION_NOT_COMPLETED.getValue()) ||
                code.getValue().equals(CodeEnum.GENERIC_ERROR.getValue())) {
                
                // Crea la transazione con stato in errore per consentire la cancellazione dell'autorizzazione
                
                TransactionBean transactionBean = new TransactionBean();
                transactionBean.setSrcTransactionID(srcTransactionID);
                transactionBean.setTransactionID(transactionID);
                transactionBean.setUserBean(userBean);
                transactionBean.setStationBean(stationBean);
                transactionBean.setUseVoucher(false);
                transactionBean.setCreationTimestamp(creationTimestamp);
                transactionBean.setEndRefuelTimestamp(null);
                transactionBean.setInitialAmount(amount);
                transactionBean.setFinalAmount(null);
                transactionBean.setFuelAmount(null);
                transactionBean.setFuelQuantity(null);
                transactionBean.setPumpID(pumpID);
                transactionBean.setPumpNumber(pumpNumber);
                transactionBean.setCurrency(currency);
                transactionBean.setShopLogin(shopLogin);
                transactionBean.setAcquirerID(acquirerID);
                transactionBean.setBankTansactionID(null);
                transactionBean.setToken(mcCardDpan);
                transactionBean.setPaymentType(paymentType);
                transactionBean.setFinalStatusType(StatusHelper.FINAL_STATUS_TYPE_ERROR);
                transactionBean.setGFGNotification(false);
                transactionBean.setConfirmed(false);
                transactionBean.setPaymentMethodId(paymentInfoBean.getId());
                transactionBean.setPaymentMethodType(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);
                transactionBean.setServerName(serverName);
                transactionBean.setStatusAttemptsLeft(statusMaxAttempts);
                transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
                transactionBean.setOutOfRange(outOfRange);
                transactionBean.setRefuelMode(refuelMode);
                transactionBean.setVoucherReconciliation(false);
                transactionBean.setAuthorizationCode(null);
                transactionBean.setTransactionCategory(TransactionCategoryType.TRANSACTION_CUSTOMER);
                transactionBean.setPaymentTokenPackageBean(paymentTokenPackageBean);
                
                this.generateTransactionErrorData(requestID, transactionBean, creationTimestamp, amount, StatusHelper.STATUS_PAYMENT_NOT_AUTHORIZED, "", "Negative Response (TransactionResult=KO)", code.getValue(), message, "KO", operationID, requestTimestamp, executeAuthorizationResult);
                
                return transactionBean;
            }
            
            // Gestione ritorno null
            throw new EJBException("executeAuthorizationResult " + executeAuthorizationResult.getResult().getCode().name());
        }
        
        
        System.out.println("RetrievalRefNumber: " + executeAuthorizationResult.getRetrievalRefNumber());
        System.out.println("AuthCode:           " + executeAuthorizationResult.getAuthCode());
        System.out.println("AmountAuthorized:   " + executeAuthorizationResult.getAmountAuthorized());
        
        System.out.println("Preautorizzazione Multicard con esito positivo");

        Integer amountAuthorized = executeAuthorizationResult.getAmountAuthorized();
        
        Double doubleAmountAuthorized = AmountConverter.toInternal(amountAuthorized);
        
        // Memorizzazione lista prodotti abilitati
        List<String> productIDList = this.getProductIDFromEnabledProductList(executeAuthorizationResult.getFuelEnabledProductList());
        
        String productIDListString = "";
        int productCount = 0;
        for(String productID : productIDList) {
          if (!productIDListString.contains(productID)) {
            if (productCount > 0)
              productIDListString += "|";
            productIDListString += productID;
            productCount++;
          }
        }
        
        System.out.println("productIDListString: " + productIDListString);
        
        
        // Crea la transazione con i dati ricevuti in input
        TransactionBean transactionBean = new TransactionBean();
        transactionBean.setSrcTransactionID(srcTransactionID);
        transactionBean.setTransactionID(transactionID);
        transactionBean.setUserBean(userBean);
        transactionBean.setStationBean(stationBean);
        transactionBean.setUseVoucher(false);
        transactionBean.setCreationTimestamp(creationTimestamp);
        transactionBean.setEndRefuelTimestamp(null);
        transactionBean.setInitialAmount(doubleAmountAuthorized);
        transactionBean.setFinalAmount(null);
        transactionBean.setFuelAmount(null);
        transactionBean.setFuelQuantity(null);
        transactionBean.setPumpID(pumpID);
        transactionBean.setPumpNumber(pumpNumber);
        transactionBean.setProductID("");
        transactionBean.setProductDescription("");
        transactionBean.setCurrency(currency);
        transactionBean.setShopLogin(shopLogin);
        transactionBean.setAcquirerID(acquirerID);
        transactionBean.setBankTansactionID(executeAuthorizationResult.getRetrievalRefNumber());
        transactionBean.setToken(mcCardDpan);
        transactionBean.setPaymentType(paymentType);
        transactionBean.setFinalStatusType(null);
        transactionBean.setGFGNotification(false);
        transactionBean.setConfirmed(false);
        transactionBean.setPaymentMethodId(paymentInfoBean.getId());
        transactionBean.setPaymentMethodType(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);
        transactionBean.setServerName(serverName);
        transactionBean.setStatusAttemptsLeft(statusMaxAttempts);
        transactionBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
        transactionBean.setOutOfRange(outOfRange);
        transactionBean.setRefuelMode(refuelMode);
        transactionBean.setVoucherReconciliation(false);
        transactionBean.setAuthorizationCode(executeAuthorizationResult.getAuthCode());
        transactionBean.setTransactionCategory(TransactionCategoryType.TRANSACTION_CUSTOMER);
        transactionBean.setPaymentTokenPackageBean(paymentTokenPackageBean);
        
        if (!executeAuthorizationResult.getReceiptElementList().isEmpty()) {
            for(KeyValueInfo keyValueInfo : executeAuthorizationResult.getReceiptElementList()) {
                TransactionAdditionalDataBean transactionAdditionalDataBean = new TransactionAdditionalDataBean();
                transactionAdditionalDataBean.setDataKey(keyValueInfo.getKey());
                transactionAdditionalDataBean.setDataValue(keyValueInfo.getValue());
                transactionAdditionalDataBean.setTransactionBean(transactionBean);
                transactionBean.getTransactionAdditionalDataBeanList().add(transactionAdditionalDataBean);
            }
        }
        
        // Crea la riga con la lista dei prodotti abilitati
        TransactionAdditionalDataBean transactionAdditionalDataBean = new TransactionAdditionalDataBean();
        transactionAdditionalDataBean.setDataKey("productIDListString");
        transactionAdditionalDataBean.setDataValue(productIDListString);
        transactionAdditionalDataBean.setTransactionBean(transactionBean);
        transactionBean.getTransactionAdditionalDataBeanList().add(transactionAdditionalDataBean);

        // Crea lo stato iniziale della transazione
        TransactionStatusBean transactionStatusBean = new TransactionStatusBean();
        transactionStatusBean.setTransactionBean(transactionBean);
        transactionStatusBean.setSequenceID(1);
        transactionStatusBean.setTimestamp(creationTimestamp);
        transactionStatusBean.setStatus(StatusHelper.STATUS_NEW_REFUELING_REQUEST);
        transactionStatusBean.setSubStatus(null);
        transactionStatusBean.setSubStatusDescription("Started");
        transactionStatusBean.setRequestID(requestID);
        transactionBean.getTransactionStatusBeanData().add(transactionStatusBean);
        
        // Crea il log dell'operazione di autorizzazione con multicard
        TransactionOperationBean transactionOperationBean = new TransactionOperationBean();
        transactionOperationBean.setCode(executeAuthorizationResult.getResult().getCode().getValue());
        transactionOperationBean.setMessage(executeAuthorizationResult.getResult().getMessage());
        transactionOperationBean.setStatus(executeAuthorizationResult.getResult().getStatus().value());
        transactionOperationBean.setRemoteTransactionId(executeAuthorizationResult.getResult().getTransactionId());
        transactionOperationBean.setOperationType("AUT");
        transactionOperationBean.setSequenceID(1);
        transactionOperationBean.setOperationId(operationID);
        transactionOperationBean.setRequestTimestamp(requestTimestamp);
        transactionOperationBean.setTransactionBean(transactionBean);
        transactionOperationBean.setAmount(executeAuthorizationResult.getAmountAuthorized());
        transactionBean.getTransactionOperationBeanData().add(transactionOperationBean);
        
        return transactionBean;
    }
    
    
    private List<String> getProductIDFromEnabledProductList(List<ProductDetailInfo> productDetailInfoList) {
        
        // Gestione multiprodotto
        
        List<String> productIDList = new ArrayList<String>(0);

        System.out.println("Rilevata lista prodotti abilitati");
        
        for(ProductDetailInfo productDetailInfo : productDetailInfoList) {
            System.out.println("ProductCode: " + productDetailInfo.getProductCode());
            String productID = convertProductCodeToProductID(productDetailInfo.getProductCode());
            System.out.println("ProductID: " + productID);
            if (productID != null && !productID.equals("")) {
                productIDList.add(productID);
            }
        }
        
        return productIDList;
    }
    
    private String convertProductCodeToProductID(String productCode) {
        // Impostare la trascodifica del codici prodotto
        /*
         * 021 BENZSP -> SP SenzaPb
         * 022 BENZSP -> SP SenzaPb
         * 023 BENZSP -> BS BluSuper
         * 029 BENZSP -> BS BluSuper
         * 030 DIESEL -> GG Gasolio
         * 032 DIESL+ -> BD BluDiesel
         * 033 DIESL+ -> BD BluDiesel
         * 039 METANO -> MT Metano
         * 034 G.P.L. -> GP GPL
         * AD ADBLU 
         */
        
        /*
        021 27101245 BENZSP          L
        022 27101245 BENZSP          L
        023 27101249 SP98            L
        029 27101249 SP98            L
        030 27102011 DIESEL          L
        032 27101943 DIESL+          L
        033 27101943 DIESL+          L
        034 ENI0034 G.P.L.           L
        039 ENI0039 METANO           L
        */

        String productID = "";
        
        switch(productCode) {
            
            case "021":
                productID = "SP";
                break;
            
            case "022":
                productID = "SP";
                break;
            
            case "023":
                productID = "BS";
                break;
            
            case "029":
                productID = "BS";
                break;
            
            case "030":
                productID = "GG";
                break;
            
            case "032":
                productID = "BD";
                break;
            
            case "033":
                productID = "BD";
                break;
            
            case "034":
                productID = "GP";
                break;
            
            case "039":
                productID = "MT";
                break;
                
        }
        
        return productID;
    }
    
    
    private void generateTransactionErrorData(String requestID, TransactionBean transactionBean, Timestamp creationTimestamp, Double amount, String paymentStatus, String paymentSubStatus, String paymentSubStatusDescription, String errorCode, String errorDescription, String result,
            String operationId, Long requestTimestamp, ExecuteAuthorizationResult executeAuthorizationResult) {
        
        Integer integerAmount = AmountConverter.toMobile(amount);
        
        // Crea lo stato iniziale della transazione
        TransactionStatusBean transactionStatusCreateBean = new TransactionStatusBean();
        transactionStatusCreateBean.setTransactionBean(transactionBean);
        transactionStatusCreateBean.setSequenceID(1);
        transactionStatusCreateBean.setTimestamp(creationTimestamp);
        transactionStatusCreateBean.setStatus(StatusHelper.STATUS_NEW_REFUELING_REQUEST);
        transactionStatusCreateBean.setSubStatus(null);
        transactionStatusCreateBean.setSubStatusDescription("Started");
        transactionStatusCreateBean.setRequestID(requestID);
        
        // Crea lo stato relativo al pagamento
        TransactionStatusBean transactionStatusPaymentBean = new TransactionStatusBean();
        transactionStatusPaymentBean.setTransactionBean(transactionBean);
        transactionStatusPaymentBean.setSequenceID(2);
        transactionStatusPaymentBean.setTimestamp(creationTimestamp);
        transactionStatusPaymentBean.setStatus(paymentStatus);
        transactionStatusPaymentBean.setSubStatus(paymentSubStatus);
        transactionStatusPaymentBean.setSubStatusDescription(paymentSubStatusDescription);
        transactionStatusPaymentBean.setRequestID(requestID);
        
        // Crea l'evento di autorizzazione pagamento in errore
        TransactionEventBean transactionEventBean = new TransactionEventBean();
        transactionEventBean.setErrorCode(errorCode);
        transactionEventBean.setErrorDescription(errorDescription);
        transactionEventBean.setEventAmount(amount);
        transactionEventBean.setEventType("AUT");
        transactionEventBean.setSequenceID(1);
        transactionEventBean.setTransactionBean(transactionBean);
        transactionEventBean.setTransactionResult(result);
        
        // Crea il log dell'operazione di autorizzazione con multicard
        TransactionOperationBean transactionOperationBean = new TransactionOperationBean();
        if (executeAuthorizationResult == null) {
            transactionOperationBean.setCode("99999");
            transactionOperationBean.setMessage("Generic erorr");
            transactionOperationBean.setStatus("ERROR");
            transactionOperationBean.setRemoteTransactionId("");
        }
        else {
            transactionOperationBean.setCode(executeAuthorizationResult.getResult().getCode().getValue());
            transactionOperationBean.setMessage(executeAuthorizationResult.getResult().getMessage());
            transactionOperationBean.setStatus(executeAuthorizationResult.getResult().getStatus().value());
            transactionOperationBean.setRemoteTransactionId(executeAuthorizationResult.getResult().getTransactionId());
        }
        transactionOperationBean.setOperationType("AUT");
        transactionOperationBean.setSequenceID(1);
        transactionOperationBean.setOperationId(operationId);
        transactionOperationBean.setRequestTimestamp(requestTimestamp);
        transactionOperationBean.setTransactionBean(transactionBean);
        transactionOperationBean.setAmount(integerAmount);
        
        transactionBean.getTransactionStatusBeanData().add(transactionStatusCreateBean);
        transactionBean.getTransactionStatusBeanData().add(transactionStatusPaymentBean);
        transactionBean.getTransactionEventBeanData().add(transactionEventBean);
        transactionBean.getTransactionOperationBeanData().add(transactionOperationBean);
    }
}
