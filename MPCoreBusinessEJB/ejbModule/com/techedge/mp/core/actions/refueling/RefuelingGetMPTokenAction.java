package com.techedge.mp.core.actions.refueling;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.refueling.RefuelingGetMPTokenResponse;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class RefuelingGetMPTokenAction {

    private static final String PARAM_TICKET_EXPIRY_TIME = "TICKET_EXPIRY_TIME";              // minutes

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private ParametersService   parametersService;

    public RefuelingGetMPTokenAction() {

    }

    public RefuelingGetMPTokenResponse execute(String requestID, String username, String password) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        try {

            userTransaction.begin();

            RefuelingGetMPTokenResponse refuelingGetMPTokenResponse = null;

            UserBean userBean = null;
            // Controlla se esiste un utente con username e password inseriti
            userBean = QueryRepository.findUserCustomerByEmail(em, username);

            if (userBean != null) {

                Integer userStatus = userBean.getUserStatus();
                // Verifica lo stato dell'utente
                if (!isUserBlocked(userStatus)) {
                    //stato dell'utente non bloccato 
                    //Controllo che sia di tipo refueling e che sia valida la password
                    if (isUserRefueling(userBean) && userBean.getPersonalDataBean().getSecurityDataPassword().equals(password)) {
                        
                        // 1) crea un nuovo ticket
                        TicketBean ticketBean = createTicket(userBean);

                        em.persist(ticketBean);

                        // 2) restituisci il risultato
                        refuelingGetMPTokenResponse = new RefuelingGetMPTokenResponse();
                        refuelingGetMPTokenResponse.setStatusCode(ResponseHelper.USER_AUTH_SUCCESS);
                        refuelingGetMPTokenResponse.setMpToken(ticketBean.getTicketId());

                        userTransaction.commit();

                        return refuelingGetMPTokenResponse;
                    }
                    else {

                        // La password inserita non � corretta o l'utente non � del tipo corretto
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Wrong password or user " + username + " is not of correct type");
                        refuelingGetMPTokenResponse = new RefuelingGetMPTokenResponse();
                        refuelingGetMPTokenResponse.setStatusCode(ResponseHelper.USER_AUTH_FAILED);

                        userTransaction.commit();

                        return refuelingGetMPTokenResponse;
                    }
                }
                else {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Login Locked access: User " + username + " in status "
                            + userStatus);
                    refuelingGetMPTokenResponse = new RefuelingGetMPTokenResponse();
                    refuelingGetMPTokenResponse.setStatusCode(ResponseHelper.USER_AUTH_FAILED);

                    userTransaction.commit();

                    return refuelingGetMPTokenResponse;
                }
            }

            // Se l'utente non esiste resitituisci il messaggio di errore
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User " + username + " not found");
            refuelingGetMPTokenResponse = new RefuelingGetMPTokenResponse();
            refuelingGetMPTokenResponse.setStatusCode(ResponseHelper.USER_AUTH_FAILED);

            userTransaction.commit();
            return refuelingGetMPTokenResponse;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED refueling authentication with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }

    private boolean isUserRefueling(UserBean userBean) {

        return ( userBean.getUserType() == User.USER_TYPE_REFUELING || userBean.getUserType() == User.USER_TYPE_REFUELING_NEW_ACQUIRER ||  userBean.getUserType() == User.USER_TYPE_REFUELING_OAUTH2);
    }

    private boolean isUserBlocked(Integer userStatus) {

        return userStatus == User.USER_STATUS_BLOCKED || userStatus == User.USER_STATUS_NEW;
    }

    private TicketBean createTicket(UserBean userBean) throws ParameterNotFoundException {

        Integer ticketType = TicketBean.TICKET_TYPE_REFUELING;

        Integer ticketExpiryTime = Integer.valueOf(this.parametersService.getParamValue(RefuelingGetMPTokenAction.PARAM_TICKET_EXPIRY_TIME));

        Date now = new Date();
        Date lastUsed = now;
        Date expiryDate = DateHelper.addMinutesToDate(ticketExpiryTime, now);

        String newTicketId = new IdGenerator().generateId(16).substring(0, 32);

        TicketBean ticketBean = new TicketBean(newTicketId, userBean, ticketType, now, lastUsed, expiryDate);
        return ticketBean;
    }
}
