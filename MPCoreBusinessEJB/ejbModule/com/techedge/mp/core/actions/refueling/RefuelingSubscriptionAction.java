package com.techedge.mp.core.actions.refueling;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.RefuelingSubscriptionsBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceOAuth2Remote;
import com.techedge.mp.refueling.integration.business.RefuelingNotificationServiceRemote;
import com.techedge.mp.refueling.integration.entities.NotifySubscriptionResult;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class RefuelingSubscriptionAction {
    
    public final String OPERATION = "NOTIFY_SUBSCRITPION";

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;
    
    public RefuelingSubscriptionAction() {}

    public NotifySubscriptionResult execute(String requestId,String fiscalCode, RefuelingNotificationServiceRemote refuelingNotificationService, RefuelingNotificationServiceOAuth2Remote refuelingNotificationServiceOAuth2, Integer maxRetryAttemps, Boolean refuelingOauth2Active) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        NotifySubscriptionResult result = new NotifySubscriptionResult();
        
        try {
            userTransaction.begin();
            
            if (fiscalCode == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                result.setStatusCode(ResponseHelper.USER_SKIP_VIRTUALIZATION_FAILURE);
                System.out.println("************ DEBUG fiscalCode for RefuelingSubscriptionAction null... User not found");
                userTransaction.commit();
                return result;
            }
            
            UserBean userBean = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode);

            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                result.setStatusCode(ResponseHelper.USER_SKIP_VIRTUALIZATION_FAILURE);
                userTransaction.commit();
                System.out.println("************ DEBUG userBean for RefuelingSubscriptionAction null... User not found");
                
                return result;
            }

            if(userBean.getSource() != null && userBean.getSource().equals(User.USER_SOURCE_ENJOY)){
                
                if(userBean.getSourceNotified() == null || !userBean.getSourceNotified()) {
                
                    RefuelingSubscriptionsBean refuelingSubscriptionsBean = new RefuelingSubscriptionsBean();
                    
                    Date now = new Date();
                    Long requestTimestamp = now.getTime();
                    requestId="ENS-".concat(requestTimestamp.toString());
                    
                    if (refuelingOauth2Active) {
                        System.out.println("Chiamata Enjoy 2.0");
                        result =  refuelingNotificationServiceOAuth2.notifySubscription(requestId, userBean.getPersonalDataBean().getFiscalCode());
                    }
                    else {
                        System.out.println("Chiamata Enjoy 1.0");
                        result =  refuelingNotificationService.notifySubscription(requestId, userBean.getPersonalDataBean().getFiscalCode());
                    }
                    
                    refuelingSubscriptionsBean.setOperation(OPERATION);
                    refuelingSubscriptionsBean.setRequestId(requestId);
                    refuelingSubscriptionsBean.setStatusCode(result.getStatusCode());
                    refuelingSubscriptionsBean.setStatusMessage(result.getStatusMessage());
                    refuelingSubscriptionsBean.setReconciliationAttemptsLeft(maxRetryAttemps);
                    
                    if(result != null && result.getStatusCode().equals(StatusHelper.REFUELING_SUBSCRIPTION_ERROR)){
                        refuelingSubscriptionsBean.setToReconcile(true);
                    }
                    else{
                        refuelingSubscriptionsBean.setToReconcile(false);
                    }
                    refuelingSubscriptionsBean.setUser(userBean);
                    em.persist(refuelingSubscriptionsBean);
                    
                    userBean.setSourceNotified(Boolean.TRUE);
                    em.merge(userBean);
                }
                else {
                    
                    result.setStatusCode(StatusHelper.REFUELING_SUBSCRIPTION_SUCCESS);
                    result.setStatusMessage("Notifica gi� inviata per l'utente con codice fiscale " + userBean.getPersonalDataBean().getFiscalCode());
                }
            }
            else{
                result.setStatusCode(ResponseHelper.USER_SKIP_VIRTUALIZATION_INVALID_TICKET);
                result.setStatusMessage("User ENJOY for subscription notification not found");
            }
            
            userTransaction.commit();
            
            return result;

        }

        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            String message = "FAILED refuelingSubscriptionsAction  with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);
            
            throw new EJBException(ex2);
        }
    }

}
