package com.techedge.mp.core.actions.eventnotification;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.loyalty.EventNotification;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransaction;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionNonOil;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionRefuel;
import com.techedge.mp.core.business.interfaces.loyalty.RewardTransaction;
import com.techedge.mp.core.business.interfaces.loyalty.RewardTransactionParameter;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.loyalty.EventNotificationBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltyTransactionBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltyTransactionNonOilBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltyTransactionRefuelBean;
import com.techedge.mp.core.business.model.loyalty.RewardTransactionBean;
import com.techedge.mp.core.business.model.loyalty.RewardTransactionParameterBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class GetTransactionDetailAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public GetTransactionDetailAction() {}

    public EventNotification execute(String ticketID, String requestID, Long notificationID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                return createMessageError(notificationID.toString(), userTransaction, "Invalid ticket", ResponseHelper.USER_REQU_INVALID_TICKET);

            }

            EventNotificationBean eventNotificationBean = QueryRepository.findEventNotificationById(em, notificationID);

            if (eventNotificationBean == null) {
                return createMessageError(notificationID.toString(), userTransaction, "Event notification id not found",
                        ResponseHelper.EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_NOT_FOUND);
            }

            if (eventNotificationBean.getUserBean().getId() != ticketBean.getUser().getId()) {
                return createMessageError(notificationID.toString(), userTransaction, "User ticket and user event notification don't match",
                        ResponseHelper.USER_REQU_INVALID_TICKET);
            }

            EventNotification eventNotificatinResponse = new EventNotification();

            eventNotificatinResponse.setEventType(eventNotificationBean.getEventType());
            eventNotificatinResponse.setFiscalCode(eventNotificationBean.getFiscalCode());
            eventNotificatinResponse.setRequestTimestamp(eventNotificationBean.getRequestTimestamp());
            eventNotificatinResponse.setSrcTransactionID(eventNotificationBean.getSrcTransactionID());
            eventNotificatinResponse.setBalance(eventNotificationBean.getBalance());
            eventNotificatinResponse.setCredits(eventNotificationBean.getCredits());
            eventNotificatinResponse.setMarketingMsg(eventNotificationBean.getMarketingMsg());
            eventNotificatinResponse.setPanCode(eventNotificationBean.getPanCode());
            eventNotificatinResponse.setSessionID(eventNotificationBean.getSessionID());
            eventNotificatinResponse.setStationID(eventNotificationBean.getStationID());
            eventNotificatinResponse.setTerminalID(eventNotificationBean.getTerminalID());
            eventNotificatinResponse.setTransactionDate(eventNotificationBean.getTransactionDate());
            eventNotificatinResponse.setTransactionResult(eventNotificationBean.getTransactionResult());
            eventNotificatinResponse.setWarningMsg(eventNotificationBean.getWarningMsg());

            if (eventNotificationBean.getLoyaltyTransactionDetail() != null) {
                LoyaltyTransaction loyaltyTransaction = new LoyaltyTransaction();
                LoyaltyTransactionBean loyaltyTransactionBean = eventNotificationBean.getLoyaltyTransactionDetail();

                for (LoyaltyTransactionNonOilBean loyaltyNonOilElement : loyaltyTransactionBean.getNonOilDetails()) {

                    LoyaltyTransactionNonOil loyaltyTransactionNonOil = new LoyaltyTransactionNonOil();

                    loyaltyTransactionNonOil.setAmount(loyaltyNonOilElement.getAmount());
                    loyaltyTransactionNonOil.setCredits(loyaltyNonOilElement.getCredits());
                    loyaltyTransactionNonOil.setProductDescription(loyaltyNonOilElement.getProductDescription());
                    loyaltyTransactionNonOil.setProductID(loyaltyNonOilElement.getProductID());
                    
                    loyaltyTransaction.getNonOilDetails().add(loyaltyTransactionNonOil);
                }
                
                loyaltyTransaction.setPaymentMode(loyaltyTransactionBean.getPaymentMode());
                
                for (LoyaltyTransactionRefuelBean transactionRefuelElement : loyaltyTransactionBean.getRefuelDetails()) {

                    LoyaltyTransactionRefuel loyaltyTransactionRefuel = new LoyaltyTransactionRefuel();

                    loyaltyTransactionRefuel.setAmount(transactionRefuelElement.getAmount());
                    loyaltyTransactionRefuel.setCredits(transactionRefuelElement.getCredits());
                    loyaltyTransactionRefuel.setFuelQuantity(transactionRefuelElement.getFuelQuantity());
                    loyaltyTransactionRefuel.setProductDescription(transactionRefuelElement.getProductDescription());
                    loyaltyTransactionRefuel.setProductID(transactionRefuelElement.getProductID().getCode());
                    loyaltyTransactionRefuel.setPumpNumber(transactionRefuelElement.getPumpNumber());
                    loyaltyTransactionRefuel.setRefuelMode(transactionRefuelElement.getRefuelMode().getCode());
                    
                    loyaltyTransaction.getRefuelDetails().add(loyaltyTransactionRefuel);
                }

                eventNotificatinResponse.setLoyaltyTransactionDetail(loyaltyTransaction);
            }
            
            eventNotificatinResponse.setOperationType(eventNotificationBean.getOperationType());
            eventNotificatinResponse.setSubtype(eventNotificationBean.getSubtype());

            if (eventNotificationBean.getRewardTransactionDetail() != null) {
                RewardTransaction rewardTransaction = new RewardTransaction();  
                RewardTransactionBean rewardTransactionBean = eventNotificationBean.getRewardTransactionDetail();
                for (RewardTransactionParameterBean rewardTransactionParameterElement : rewardTransactionBean.getParameterDetails()) {

                    RewardTransactionParameter rewardTransactionParameter = new RewardTransactionParameter();

                    rewardTransactionParameter.setParameterID(rewardTransactionParameterElement.getParameterID());
                    rewardTransactionParameter.setParameterValue(rewardTransactionParameterElement.getParameterValue());
                    
                    rewardTransaction.getParameterDetails().add(rewardTransactionParameter);
                }
                
                eventNotificatinResponse.setRewardTransactionDetail(rewardTransaction);
            }

            eventNotificatinResponse.setStatusCode(ResponseHelper.EVENT_NOTIFICATION_GET_TRANSACTION_DETAIL_SUCCESS);
            userTransaction.commit();

            return eventNotificatinResponse;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED Get Event Notification transaction detail get with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }

    }

    private EventNotification createMessageError(String requestId, UserTransaction userTransaction, String message, String statusCode) throws Exception {

        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, message);

        userTransaction.commit();

        EventNotification response = new EventNotification();
        response.setStatusCode(statusCode);

        return response;
    }

}
