package com.techedge.mp.core.actions.eventnotification;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.CheckTransactionResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.TransactionDetail;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.loyalty.CheckPaymentBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityConstants;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class CheckTransactionAction {

    @Resource
    private EJBContext               context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager            em;

    @EJB
    private LoggerService            loggerService;

    public CheckTransactionAction() {}

    public CheckTransactionResponse execute(String operationID, Long requestTimestamp, String stationID, String pumpNumber, Long interval) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        CheckTransactionResponse response = new CheckTransactionResponse();

        try {
            userTransaction.begin();

            if (operationID == null || (operationID != null && operationID.isEmpty())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "operationID null");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.TRANSACTION_PARAMETER_NOT_FOUND);
                return response;
            }

            if (requestTimestamp == null || (requestTimestamp != null && requestTimestamp == 0)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "requestTimestamp null");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.TRANSACTION_PARAMETER_NOT_FOUND);
                return response;
            }

            if (pumpNumber == null || (pumpNumber != null && pumpNumber.isEmpty())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "pumpNumber null");

                writeCheckPaymentLog(operationID, requestTimestamp, null, null, ResponseHelper.TRANSACTION_PUMP_NOT_VALID, "pumpNumber is null");
                userTransaction.commit();

                response.setStatusCode(ResponseHelper.TRANSACTION_PUMP_NOT_VALID);
                return response;
            }

            if (stationID == null || (stationID != null && stationID.isEmpty())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "stationID null");

                writeCheckPaymentLog(operationID, requestTimestamp, null, pumpNumber, ResponseHelper.TRANSACTION_STATION_NOT_ENABLED, "stationID is null");
                userTransaction.commit();

                response.setStatusCode(ResponseHelper.TRANSACTION_STATION_NOT_ENABLED);
                return response;
            }

            StationBean stationBean = QueryRepository.findActiveStationBeanById(em, stationID);

            if (stationBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "station not found or inactive");

                writeCheckPaymentLog(operationID, requestTimestamp, stationID, pumpNumber, ResponseHelper.TRANSACTION_STATION_NOT_ENABLED, "station not found or inactive");
                userTransaction.commit();

                response.setStatusCode(ResponseHelper.TRANSACTION_STATION_NOT_ENABLED);
                return response;
            }
            
            if (stationBean.getNewAcquirerActive() == Boolean.FALSE) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "station not enabled");

                writeCheckPaymentLog(operationID, requestTimestamp, stationID, pumpNumber, ResponseHelper.TRANSACTION_STATION_NOT_ENABLED, "station not enabled");
                userTransaction.commit();

                response.setStatusCode(ResponseHelper.TRANSACTION_STATION_NOT_ENABLED);
                return response;
            }
            
            PostPaidTransactionBean postPaidTransactionBean = QueryRepository.findPostPaidTransactinByPumpNumber(em, stationBean, pumpNumber);

            if (postPaidTransactionBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "postpaid transaction not found");

                writeCheckPaymentLog(operationID, requestTimestamp, stationID, pumpNumber, ResponseHelper.TRANSACTION_NOT_FOUND, "postpaid transaction not found");
                userTransaction.commit();

                response.setStatusCode(ResponseHelper.TRANSACTION_NOT_FOUND);
                return response;
            }

            Date now = new Date();

            if (postPaidTransactionBean != null && postPaidTransactionBean.getCreationTimestamp().getTime() < (now.getTime() - interval)) {
                System.out.println("Postpaid transaction found: " + postPaidTransactionBean.getId() + " - but create in: " + postPaidTransactionBean.getCreationTimestamp());
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "postpaid operation not found");

                writeCheckPaymentLog(operationID, requestTimestamp, stationID, pumpNumber, ResponseHelper.TRANSACTION_NOT_FOUND, "postpaid transaction expired for this service");
                userTransaction.commit();

                response.setStatusCode(ResponseHelper.TRANSACTION_NOT_FOUND);
                return response;
            }
            System.out.println("Postpaid transaction found: " + postPaidTransactionBean.getId() + " - in date: " + postPaidTransactionBean.getCreationTimestamp());

            // Le transazioni in stato REVERSED non devono essere restituite dal servizio
            if (postPaidTransactionBean.getMpTransactionStatus().equals("REVERSED")) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "postpaid transaction in status REVERSED -> NOT_FOUND");

                writeCheckPaymentLog(operationID, requestTimestamp, stationID, pumpNumber, ResponseHelper.TRANSACTION_NOT_FOUND, "postpaid transaction in status REVERSED -> NOT_FOUND");
                userTransaction.commit();

                response.setStatusCode(ResponseHelper.TRANSACTION_NOT_FOUND);
                return response;
            }
            
            if (postPaidTransactionBean.getMpTransactionStatus().equals("PAID")) {
                response.setStatusCode(ResponseHelper.TRANSACTION_SUCCESS);
            }
            else if (postPaidTransactionBean.getMpTransactionStatus().equals("UNPAID")) {
                response.setStatusCode(ResponseHelper.TRANSACTION_UNPAID);
            }
            else if (postPaidTransactionBean.getMpTransactionStatus().equals("ONHOLD")) {
                response.setStatusCode(ResponseHelper.TRANSACTION_ON_HOLD);
            }
            else if (postPaidTransactionBean.getMpTransactionStatus().equals("CANCELLED")) {
                response.setStatusCode(ResponseHelper.TRANSACTION_CANCELLED);
            }
            
            if (postPaidTransactionBean.getMpTransactionStatus().equals("PAID") ||
                postPaidTransactionBean.getMpTransactionStatus().equals("UNPAID") ||
                postPaidTransactionBean.getMpTransactionStatus().equals("CANCELLED")) {
                    
                TransactionDetail transactionDetail = new TransactionDetail();
                if (postPaidTransactionBean.getAmount() != null)
                    transactionDetail.setAmount(new BigDecimal(postPaidTransactionBean.getAmount(), MathContext.DECIMAL64));
    
                if (postPaidTransactionBean.getAuthorizationCode() != null)
                    transactionDetail.setAuthorizationCode(postPaidTransactionBean.getAuthorizationCode());
    
                if (postPaidTransactionBean.getBankTansactionID() != null)
                    transactionDetail.setBankTransactionID(postPaidTransactionBean.getBankTansactionID());
    
                if (postPaidTransactionBean.getRefuelBean().iterator().next().getFuelQuantity() != null)
                    transactionDetail.setFuelQuantity(new BigDecimal(postPaidTransactionBean.getRefuelBean().iterator().next().getFuelQuantity(), MathContext.DECIMAL64));
    
                if (postPaidTransactionBean.getProductID() != null) {
                    String productCode = "";
                    if (postPaidTransactionBean.getProductID().equalsIgnoreCase("GG")) {
                        productCode = FidelityConstants.PRODUCT_CODE_GASOLIO;
                    }
                    else if (postPaidTransactionBean.getProductID().equalsIgnoreCase("SP")) {
                        productCode = FidelityConstants.PRODUCT_CODE_SP;
                    }
                    else if (postPaidTransactionBean.getProductID().equalsIgnoreCase("BD")) {
                        productCode = FidelityConstants.PRODUCT_CODE_BLUE_DIESEL;
                    }
                    else if (postPaidTransactionBean.getProductID().equalsIgnoreCase("BS")) {
                        productCode = FidelityConstants.PRODUCT_CODE_BLUE_SUPER;
                    }
    
                    transactionDetail.setProductCode(productCode);
                }
    
                if (postPaidTransactionBean.getPumpNumber() != null)
                    transactionDetail.setPumpNumber(postPaidTransactionBean.getPumpNumber().toString());
    
                if (postPaidTransactionBean.getStationBean() != null)
                    transactionDetail.setStationID(postPaidTransactionBean.getStationBean().getStationID());
    
                response.setTransationDetail(transactionDetail);
            }
            
            writeCheckPaymentLog(operationID, requestTimestamp, stationID, pumpNumber, response.getStatusCode(), "");

            userTransaction.commit();

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED check authorization with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }
    
    private void writeCheckPaymentLog(String operationID, Long requestTimestamp, String stationID, String pumpNumber, String statusCode, String statusMessage) {
        CheckPaymentBean checkPaymentBean = new CheckPaymentBean();
        checkPaymentBean.setOperationID(operationID);
        checkPaymentBean.setPumpNumber(pumpNumber);
        checkPaymentBean.setStationID(stationID);
        checkPaymentBean.setRequestTimestamp(new Date(requestTimestamp));
        checkPaymentBean.setStatusCode(statusCode);
        checkPaymentBean.setStatusMessage(statusMessage);
        
        em.persist(checkPaymentBean);
    }
}
