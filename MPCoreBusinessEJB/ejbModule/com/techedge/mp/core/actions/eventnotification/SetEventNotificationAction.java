package com.techedge.mp.core.actions.eventnotification;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.CRMService;
import com.techedge.mp.core.business.EventNotificationService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.UserService;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.CRMSfNotifyEvent;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.NotifyEventResponse;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.crm.UserProfile.ClusterType;
import com.techedge.mp.core.business.interfaces.loyalty.EventNotification;
import com.techedge.mp.core.business.interfaces.loyalty.EventNotificationType;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransaction;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionNonOil;
import com.techedge.mp.core.business.interfaces.loyalty.LoyaltyTransactionRefuel;
import com.techedge.mp.core.business.interfaces.loyalty.RewardTransaction;
import com.techedge.mp.core.business.interfaces.loyalty.RewardTransactionParameter;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.loyalty.EventNotificationBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltyTransactionBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltyTransactionNonOilBean;
import com.techedge.mp.core.business.model.loyalty.LoyaltyTransactionRefuelBean;
import com.techedge.mp.core.business.model.loyalty.RewardTransactionBean;
import com.techedge.mp.core.business.model.loyalty.RewardTransactionParameterBean;
import com.techedge.mp.core.business.utilities.DateHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.core.business.utilities.crm.EVENT_TYPE;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationMessage;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SetEventNotificationAction {

    @Resource
    private EJBContext               context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager            em;

    @EJB
    private LoggerService            loggerService;

    @EJB
    private EventNotificationService eventNotificationService;

    @EJB
    private CRMService               crmService;

    @EJB
    private UserService              userService;
    
    @EJB
    private ParametersService parametersService;
    
    private final String 				PARAM_CRM_SF_ACTIVE="CRM_SF_ACTIVE";
    
    public SetEventNotificationAction() {}

    public String execute(EventNotification eventNotification, Integer expiryTime, Integer maxRetryAttemps, Integer sendingDelay,
            String voucherRewardAlertRecipient, PushNotificationServiceRemote pushNotificationService, EmailSenderRemote emailSender, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        boolean sendingCrmOffer = false;
        boolean stationValid = true;
        boolean userValid = true;
        LoyaltyTransaction loyaltyTransaction = null;
        LoyaltyTransactionRefuel crmloyaltyTransactionRefuel = null;
        String response = ResponseHelper.USER_SET_EVENT_NOTIFICATION_SUCCESS;
        UserBean userBean = null;

        try {
            userTransaction.begin();

            /*
             *  Controllo per evitare la creazione di righe duplicate per operazioni di caricamento loyalty
             *  in caso di richieste ripetute effettuate dal backend di Quenit a seguito di timeout 
             */
            if (eventNotification.getEventType().equals(EventNotificationType.LOYALTY) && eventNotification.getTransactionResult().equals("00") && eventNotification.getCredits() > 0) {
                
                // Controlla se esiste gi� un EventNotificationBean con stesso srcTransactionID
                
                EventNotificationBean eventNotificationBeanCheck = QueryRepository.findEventNotificationSuccessBySrcTransactionID(em, eventNotification.getSrcTransactionID(), "LOYALTY");
                
                /*
                if ( eventNotificationBeanCheck != null ) {
                    
                    System.out.println("EventType:         " + eventNotificationBeanCheck.getEventType());
                    System.out.println("TransactionResult: " + eventNotificationBeanCheck.getTransactionResult());
                    System.out.println("Credits:           " + eventNotificationBeanCheck.getCredits());
                }
                else {
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "eventNotification con srcTransactionID " + eventNotification.getSrcTransactionID() + " non presente.");
                }
                */
                        
                if ( eventNotificationBeanCheck != null ) {
                    
                    // L'evento � stato gi� ricevuto
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "eventNotification con srcTransactionID " + eventNotification.getSrcTransactionID() + " gi� ricevuto.");
                    
                    userTransaction.commit();
                    
                    return response;
                }
                else {
                    
                    //this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "eventNotification con srcTransactionID " + eventNotification.getSrcTransactionID() + " non presente.");
                }
                
            }

            EventNotificationBean eventNotificationBean = new EventNotificationBean();
            
            eventNotificationBean.setEventType(eventNotification.getEventType());
            eventNotificationBean.setSrcTransactionID(eventNotification.getSrcTransactionID());
            eventNotificationBean.setRequestTimestamp(eventNotification.getRequestTimestamp());
            eventNotificationBean.setFiscalCode(eventNotification.getFiscalCode());
            eventNotificationBean.setCardStatus(eventNotification.getCardStatus());
            eventNotificationBean.setSessionID(eventNotification.getSessionID());
            eventNotificationBean.setPanCode(eventNotification.getPanCode());
            eventNotificationBean.setTerminalID(eventNotification.getTerminalID());
            eventNotificationBean.setTransactionResult(eventNotification.getTransactionResult());
            eventNotificationBean.setTransactionDate(eventNotification.getTransactionDate());
            eventNotificationBean.setCredits(eventNotification.getCredits());
            eventNotificationBean.setBalance(eventNotification.getBalance());
            eventNotificationBean.setBalanceAmount(eventNotification.getBalanceAmount());
            eventNotificationBean.setMarketingMsg(eventNotification.getMarketingMsg());
            eventNotificationBean.setWarningMsg(eventNotification.getWarningMsg());
            eventNotificationBean.setOperationType(eventNotification.getOperationType());
            eventNotificationBean.setSubtype(eventNotification.getSubtype());
            eventNotificationBean.setStationID(eventNotification.getStationID());
            
            if (eventNotification.getFiscalCode() == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "FiscalCode null");

                eventNotificationBean.setMpStatusCode(ResponseHelper.USER_SET_EVENT_NOTIFICATION_USER_NOT_FOUND);
                eventNotificationBean.setMpStatusMessage("FiscalCode null");
                userValid = false;
            }
            else {
                String fiscalCode = eventNotification.getFiscalCode();
                userBean = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode);

                if (userBean == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User not found with fiscalcode: " + fiscalCode);

                    eventNotificationBean.setMpStatusCode(ResponseHelper.USER_SET_EVENT_NOTIFICATION_USER_NOT_FOUND);
                    eventNotificationBean.setMpStatusMessage("User not found with fiscalcode: " + fiscalCode);
                    userValid = false;
                }
                else {
                    eventNotificationBean.setUserBean(userBean);
                }
            }
            
            String stationID = eventNotification.getStationID();
            
            if (stationID == null || stationID.isEmpty()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "StationID null or empty");

                eventNotificationBean.setMpStatusCode(ResponseHelper.USER_SET_EVENT_NOTIFICATION_STATION_NOT_FOUND);
                eventNotificationBean.setMpStatusMessage("StationID null or empty");
                stationValid = false;
            }
            else {
                StationBean stationBean = QueryRepository.findStationBeanById(em, stationID);

                if (stationBean == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                            "Station not found with stationID: " + eventNotification.getStationID());

                    eventNotificationBean.setMpStatusCode(ResponseHelper.USER_SET_EVENT_NOTIFICATION_STATION_NOT_FOUND);
                    eventNotificationBean.setMpStatusMessage("Station not found with stationID: " + eventNotification.getStationID());
                    stationValid = false;
                }

                if (stationBean != null && !stationBean.getLoyaltyActive().booleanValue()) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                            "Station is not active for loyalty transactions: " + eventNotification.getStationID());

                    
                    eventNotificationBean.setMpStatusCode(ResponseHelper.USER_SET_EVENT_NOTIFICATION_STATION_NOT_FOUND);
                    eventNotificationBean.setMpStatusMessage("Station is not active for loyalty transactions: " + eventNotification.getStationID());
                    stationValid = false;
                }
            }
                        
            if (!stationValid) {
                response = ResponseHelper.USER_SET_EVENT_NOTIFICATION_STATION_NOT_FOUND;
            }
            
            em.persist(eventNotificationBean);

            if (eventNotification.getLoyaltyTransactionDetail() != null) {
                
                System.out.println("eventNotification.getLoyaltyTransactionDetail() is not null... " + eventNotification.getLoyaltyTransactionDetail().toString());
                loyaltyTransaction = eventNotification.getLoyaltyTransactionDetail();

                LoyaltyTransactionBean loyaltyTransactionBean = new LoyaltyTransactionBean();
                loyaltyTransactionBean.setEventNotificationBean(eventNotificationBean);

                loyaltyTransactionBean.setPaymentMode(loyaltyTransaction.getPaymentMode());

                em.persist(loyaltyTransactionBean);

                if (loyaltyTransaction.getRefuelDetails().size() > 0) {
                    for (LoyaltyTransactionRefuel refuelDetail : loyaltyTransaction.getRefuelDetails()) {
                        LoyaltyTransactionRefuelBean loyaltyTransactionRefuelBean = new LoyaltyTransactionRefuelBean();
                        loyaltyTransactionRefuelBean.setLoyaltyTransactionBean(loyaltyTransactionBean);
                        loyaltyTransactionRefuelBean.setRefuelMode(refuelDetail.getRefuelMode());
                        loyaltyTransactionRefuelBean.setPumpNumber(refuelDetail.getPumpNumber());
                        loyaltyTransactionRefuelBean.setProductID(refuelDetail.getProductID());
                        loyaltyTransactionRefuelBean.setProductDescription(refuelDetail.getProductDescription());
                        loyaltyTransactionRefuelBean.setFuelQuantity(refuelDetail.getFuelQuantity());
                        loyaltyTransactionRefuelBean.setAmount(refuelDetail.getAmount());
                        loyaltyTransactionRefuelBean.setCredits(refuelDetail.getCredits());

                        if (crmloyaltyTransactionRefuel == null) {
                            crmloyaltyTransactionRefuel = refuelDetail;
                        }

                        em.persist(loyaltyTransactionRefuelBean);
                    }
                }

                if (loyaltyTransaction.getNonOilDetails().size() > 0) {
                    for (LoyaltyTransactionNonOil nonOilDetail : loyaltyTransaction.getNonOilDetails()) {
                        LoyaltyTransactionNonOilBean loyaltyTransactionNonOilBean = new LoyaltyTransactionNonOilBean();
                        loyaltyTransactionNonOilBean.setLoyaltyTransactionBean(loyaltyTransactionBean);
                        loyaltyTransactionNonOilBean.setProductID(nonOilDetail.getProductID());
                        loyaltyTransactionNonOilBean.setProductDescription(nonOilDetail.getProductDescription());
                        loyaltyTransactionNonOilBean.setAmount(nonOilDetail.getAmount());
                        loyaltyTransactionNonOilBean.setCredits(nonOilDetail.getCredits());

                        em.persist(loyaltyTransactionNonOilBean);
                    }
                }
            }

            if (eventNotification.getRewardTransactionDetail() != null) {
                RewardTransaction rewardTransaction = eventNotification.getRewardTransactionDetail();
                RewardTransactionBean rewardTransactionBean = new RewardTransactionBean();
                rewardTransactionBean.setEventNotificationBean(eventNotificationBean);

                em.persist(rewardTransactionBean);

                if (rewardTransaction.getParameterDetails().size() > 0) {
                    for (RewardTransactionParameter rewardTransactionParameter : rewardTransaction.getParameterDetails()) {
                        RewardTransactionParameterBean rewardTransactionParameterBean = new RewardTransactionParameterBean();
                        rewardTransactionParameterBean.setRewardTransactionBean(rewardTransactionBean);
                        rewardTransactionParameterBean.setParameterID(rewardTransactionParameter.getParameterID());
                        rewardTransactionParameterBean.setParameterValue(rewardTransactionParameter.getParameterValue());
                        
                        if (rewardTransactionParameter.getParameterID().equalsIgnoreCase("voucherCode") && userBean != null) {
                            String voucherCode = rewardTransactionParameter.getParameterValue();
                            //Crea un nuovo ticket
                            Integer ticketType = TicketBean.TICKET_TYPE_CUSTOMER;
                            Date now = new Date();
                            Date lastUsed = now;
                            Date expiryDate = DateHelper.addMinutesToDate(5, now);
                            String ticketId = new IdGenerator().generateId(16).substring(0, 32);
                            TicketBean ticketBean = new TicketBean(ticketId, userBean, ticketType, now, lastUsed, expiryDate);
                            em.persist(ticketBean);

                            String loadVoucherResult = userService.loadVoucher(ticketId, String.valueOf(now.getTime()), voucherCode);
                            String userEmail = userBean.getPersonalDataBean().getSecurityDataEmail();
                            
                            if (!loadVoucherResult.equals(ResponseHelper.USER_LOAD_VOUCHER_SUCCESS)) {
                                
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error associating voucher " + voucherCode
                                        + " to user " + userEmail);

                                if (emailSender != null) {
                                    String keyFrom = emailSender.getSender();
                                    
                                    if (stringSubstitution != null) {
                                        keyFrom = stringSubstitution.getValue(keyFrom, 1);
                                    }
                                    
                                    System.out.println("keyFrom: " + keyFrom);
                                    
                                    EmailType emailType = EmailType.VOUCHER_REWARD_ALERT_V2;
                                    List<Parameter> parameters = new ArrayList<Parameter>(0);

                                    parameters.add(new Parameter("USER_EMAIL", userEmail));
                                    parameters.add(new Parameter("VOUCHER_CODE", voucherCode));
                                    parameters.add(new Parameter("ERROR_CODE", loadVoucherResult));


                                    String result = emailSender.sendEmail(emailType, keyFrom, voucherRewardAlertRecipient, parameters);

                                    //String result = "disabled";

                                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "SendEmail result: " + result);

                                }
                            }
                            else {
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Voucher " + voucherCode
                                        + " associated to user " + userEmail);
                            }
                        }
                        
                        em.persist(rewardTransactionParameterBean);
                    }
                }
            }

            System.out.println("eventNotification.getCredits: " + (eventNotification.getCredits() != null ? eventNotification.getCredits() : "null"));
            System.out.println("eventNotification.loyaltyTransaction: " + (loyaltyTransaction != null ? loyaltyTransaction.toString() : "null"));
            System.out.println("eventNotification.loyaltyTransaction.getRefuelDetails: " + (loyaltyTransaction != null ? loyaltyTransaction.getRefuelDetails().size() : "null"));

            if (loyaltyTransaction != null && loyaltyTransaction.getRefuelDetails() != null && loyaltyTransaction.getRefuelDetails().size() > 0) {

                sendingCrmOffer = true;

                System.out.println("Richiesta offerte CRM abilitate");
            }
            else {
                System.out.println("Richiesta offerte CRM non abilitate");
            }

            if (userValid) {
                System.out.println("Richiesta eventNotification valida. Notifica dello scontrino inviata");
                
                String arnEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();
                PushNotificationContentType contentType = PushNotificationContentType.TEXT;
                PushNotificationStatusType statusCode = null;
                String statusMessage = null;
                Long userID = userBean.getId();
                String publishMessageID = null;
                Date sendingTimestamp = new Date();
                PushNotificationSourceType source = PushNotificationSourceType.LOYALTY;
                String surnameName = userBean.getPersonalDataBean().getLastName() + " " + userBean.getPersonalDataBean().getFirstName();
                String message = "Nuova notifica per l'utente " + surnameName;
                String title = "Eni Station + Notifica";
    
                PushNotificationBean pushNotificationBean = null;
    
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(sendingTimestamp);
                calendar.add(Calendar.SECOND, expiryTime);
    
                pushNotificationBean = new PushNotificationBean();
                pushNotificationBean.setTitle(title);
                pushNotificationBean.setSendingTimestamp(sendingTimestamp);
                pushNotificationBean.setExpiringTimestamp(calendar.getTime());
                pushNotificationBean.setSource(source);
                pushNotificationBean.setContentType(contentType);
                pushNotificationBean.setUser(userBean);
                pushNotificationBean.setRetryAttemptsLeft(maxRetryAttemps);
    
                em.persist(pushNotificationBean);
    
                eventNotificationBean.setPushNotificationBean(pushNotificationBean);
                em.merge(eventNotificationBean);
    
                PushNotificationMessage notificationMessage = new PushNotificationMessage();
                notificationMessage.setMessage(pushNotificationBean.getId(), message, title);
    
                if (arnEndpoint == null) {
    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User arn endpoint not found");
    
                    statusCode = PushNotificationStatusType.ARN_NOT_FOUND;
                    statusMessage = "User arn endpoint not found";
    
                    String updateNotificationResponse = eventNotificationService.updatePushNotication(pushNotificationBean.getId(), publishMessageID, arnEndpoint, title, message, sendingTimestamp,
                            source, contentType, statusCode, statusMessage, userID);
    
                    if (updateNotificationResponse.equals(ResponseHelper.SYSTEM_ERROR)) {
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Error in updatePushNotication");
                    }
                }
                else {
                    PushNotificationResult pushNotificationResult = pushNotificationService.publishMessage(arnEndpoint, notificationMessage, false, false);
                    publishMessageID = pushNotificationResult.getMessageId();
                    sendingTimestamp = pushNotificationResult.getRequestTimestamp();
        
                    if (pushNotificationResult != null && pushNotificationResult.getStatusCode() != null) {
                        if (pushNotificationResult.getStatusCode().equals(com.techedge.mp.pushnotification.adapter.interfaces.StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_SUCCESS)) {
                            statusCode = PushNotificationStatusType.DELIVERED;
                        }
                        else if (pushNotificationResult.getStatusCode().equals(com.techedge.mp.pushnotification.adapter.interfaces.StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_ARN_NOT_FOUND)) {
                            statusCode = PushNotificationStatusType.ARN_NOT_FOUND;
                        }
                        else {
                            statusCode = PushNotificationStatusType.ERROR;
                        }
                    }
                    else {
                        statusCode = PushNotificationStatusType.ERROR;
                    }
        
                    statusMessage = pushNotificationResult.getMessage();
                    String updateNotificationResponse = eventNotificationService.updatePushNotication(pushNotificationBean.getId(), publishMessageID, arnEndpoint, title, message, sendingTimestamp, source,
                            contentType, statusCode, statusMessage, userID);
        
                    if (updateNotificationResponse.equals(ResponseHelper.SYSTEM_ERROR)) {
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Error in updatePushNotication");
                    }
                }
            }
            else {
                System.out.println("Richiesta eventNotification non valida. Notifica dello scontrino non inviata");
                response = ResponseHelper.USER_SET_EVENT_NOTIFICATION_USER_NOT_FOUND;
            }
            
            if (sendingCrmOffer && userBean != null) {
            	
            	String crmSfActive="0";
            	try {
            		crmSfActive = parametersService.getParamValue(PARAM_CRM_SF_ACTIVE);
        		} catch (ParameterNotFoundException e) {
        			e.printStackTrace();
        		}
            	
                Date timestamp = eventNotification.getTransactionDate();
                String surname = eventNotificationBean.getUserBean().getPersonalDataBean().getLastName();
                //String firstName = eventNotificationBean.getUserBean().getPersonalDataBean().getFirstName();
                String pv = eventNotification.getStationID();
                String product = crmloyaltyTransactionRefuel.getProductDescription();
                final Long sourceID = eventNotificationBean.getId();
                final CRMEventSourceType sourceType = CRMEventSourceType.EVENT_NOTIFICATION;
                boolean flagPayment = false;
                Integer loyaltyCredits = eventNotification.getCredits();
                Double refuelQuantity = (crmloyaltyTransactionRefuel != null) ? crmloyaltyTransactionRefuel.getFuelQuantity() : 0;
                Double amount = (crmloyaltyTransactionRefuel != null) ? crmloyaltyTransactionRefuel.getAmount() : 0;
                String refuelMode = crmloyaltyTransactionRefuel.getRefuelMode().getName();
                boolean flagNotification = false;
                boolean flagCreditCard = false;
                String cardBrand = null;
                ClusterType cluster = ClusterType.ENIPAY;
                final String fiscalCode = eventNotification.getFiscalCode();
                boolean privacyFlag1 = false;
                boolean privacyFlag2 = false;
                
                if (userBean.getPaymentMethodTypeCreditCard() != null) {
                    flagCreditCard = true;
                    cardBrand = userBean.getPaymentMethodTypeCreditCard().getBrand();
                }
                
                if (userBean.getVirtualizationCompleted()) {
                    cluster = ClusterType.YOUENI;
                }
                
                Set<TermsOfServiceBean> listTermOfService = userBean.getPersonalDataBean().getTermsOfServiceBeanData();
                if (listTermOfService == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "flagPrivacy null");
                }
                else {
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("NOTIFICATION_NEW_1")) {
                            flagNotification = item.getAccepted();
                            break;
                        }
                    }
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                            privacyFlag1 = item.getAccepted();
                            break;
                        }
                    }
                    for (TermsOfServiceBean item : listTermOfService) {
                        if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                            privacyFlag2 = item.getAccepted();
                            break;
                        }
                    }
                }
                
                //Nuova implementazione sf
                //recupero parametro CRM_SF_ACTIVE per la gestione della nuova chiamata al CRM SF
                //0 disattivato invio sf
                //1 attivo solo invio sf
                //2 attivi entrambi i crm ibm,sf
               
                if(crmSfActive.equalsIgnoreCase("0")||crmSfActive.equalsIgnoreCase("2")){
	                final UserProfile userProfile = UserProfile.getUserProfileForEventRefueling(fiscalCode, timestamp, surname, pv, product, flagPayment, 
	                        loyaltyCredits, refuelQuantity, amount, refuelMode, flagNotification, flagCreditCard, cardBrand, cluster);
	                
	                new Thread(new Runnable() {
	                    
	                    @Override
	                    public void run() {
	                        String crmResponse = crmService.sendNotifyEventOffer(fiscalCode, InteractionPointType.LOYALTY_CREDITS, userProfile, sourceID, sourceType);
	                        
	                        if (!crmResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
	                            System.out.println("Error in send crm notification (" + crmResponse + ")");
	                        }
	                    }
	                }, "executeBatchGetOffers (SetEventNotificationAction)").start();
	                
                }
                if(crmSfActive.equalsIgnoreCase("1")||crmSfActive.equalsIgnoreCase("2")){
                    
                    /*
                     * Mapping campo prodotto per evento di caricamento punti da POS
                     * 
                     * Per uniformare i prodotti inviati a seguito di eventi di rifornimento e di caricamento punti si utilizzer� il seguente mapping
                     * 
                     * Gasolio      --> Gasolio
                     * S.Piombo     --> SenzaPb
                     * GPL          --> GPL
                     * BluSuper     --> BluSuper
                     * Eni Diesel + --> Diesel+
                     * Metano       --> Metano
                     * 
                     */
                    
                    if (product.equals("S.Piombo")) {
                        product = "SenzaPb";
                    }
                    if (product.equals("Eni Diesel +")) {
                        product = "Diesel+";
                    }
                	
                	System.out.println("##################################################");
                	System.out.println("fiscalCode:" + fiscalCode);
                	System.out.println("timestamp:" + timestamp);
                	System.out.println("pv:" +pv );
                	System.out.println("product:" + product);
                	System.out.println("flagPayment:" + flagPayment);
                	System.out.println("loyaltyCredits:" +loyaltyCredits );
                	System.out.println("refuelQuantity:" +refuelQuantity );
                	System.out.println("refuelMode:" + refuelMode);
                	System.out.println("surname:" +surname );
                	System.out.println("getSecurityDataEmail():" +userBean.getPersonalDataBean().getSecurityDataEmail());
                	System.out.println("getBirthDate():" + userBean.getPersonalDataBean().getBirthDate());
                	System.out.println("flagNotification:" + flagNotification);
                	System.out.println("flagCreditCard:" + flagCreditCard);
                	System.out.println("cardBrand:" + cardBrand);
                	System.out.println("cluster.getValue():" +cluster.getValue() );
                	System.out.println("amount:" +amount );
                	System.out.println("userBean.getActiveMobilePhone():" +userBean.getActiveMobilePhone() );
                	
                	String panCode="";
                	if(eventNotification!=null){
                		System.out.println("getPanCode():" +eventNotification.getPanCode() );
                		panCode=eventNotification.getPanCode();
                	}else
                		System.out.println("getPanCode():null");
                	
                	System.out.println("eventNotification.getPanCode():" +panCode );
                	System.out.println("EVENT_TYPE.ACCREDITO_PUNTI_LOYALTY_NFC_TOTP.getValue():" + EVENT_TYPE.ACCREDITO_PUNTI_LOYALTY_NFC_TOTP.getValue());
                	System.out.println("##################################################");
                	
                	String requestId = new IdGenerator().generateId(16).substring(0, 32);
                	
                	final CRMSfNotifyEvent crmSfNotifyEvent= new CRMSfNotifyEvent(requestId, fiscalCode, timestamp, pv, product, flagPayment, 
                			loyaltyCredits, refuelQuantity, refuelMode, null, surname, userBean.getPersonalDataBean().getSecurityDataEmail(), 
                			userBean.getPersonalDataBean().getBirthDate(), flagNotification, flagCreditCard, cardBrand==null?"":cardBrand, 
                			cluster.getValue(), amount, privacyFlag1, privacyFlag2, userBean.getActiveMobilePhone(), panCode, 
                			EVENT_TYPE.ACCREDITO_PUNTI_LOYALTY_NFC_TOTP.getValue(), "", "", "");
	    
	                new Thread(new Runnable() {
	                    
	                    @Override
	                    public void run() {
	                    	NotifyEventResponse crmResponse = crmService.sendNotifySfEventOffer(crmSfNotifyEvent.getRequestId(), crmSfNotifyEvent.getFiscalCode(), crmSfNotifyEvent.getDate(), 
	                        		crmSfNotifyEvent.getStationId(), crmSfNotifyEvent.getProductId(), crmSfNotifyEvent.getPaymentFlag(), crmSfNotifyEvent.getCredits(), 
	                        		crmSfNotifyEvent.getQuantity(), crmSfNotifyEvent.getRefuelMode(), crmSfNotifyEvent.getFirstName(), crmSfNotifyEvent.getLastName(), 
	                        		crmSfNotifyEvent.getEmail(), crmSfNotifyEvent.getBirthDate(), crmSfNotifyEvent.getNotificationFlag(), crmSfNotifyEvent.getPaymentCardFlag(), 
	                        		crmSfNotifyEvent.getBrand(), crmSfNotifyEvent.getCluster(),crmSfNotifyEvent.getAmount(), crmSfNotifyEvent.getPrivacyFlag1(), 
	                        		crmSfNotifyEvent.getPrivacyFlag2(), crmSfNotifyEvent.getMobilePhone(), crmSfNotifyEvent.getLoyaltyCard(), crmSfNotifyEvent.getEventType(), 
	                        		crmSfNotifyEvent.getParameter1(), crmSfNotifyEvent.getParameter2(), crmSfNotifyEvent.getPaymentMode());
	    
	                    	System.out.println("crmResponse succes:" + crmResponse.getSuccess());
		                      System.out.println("crmResponse errorCode:" + crmResponse.getErrorCode());
		                      System.out.println("crmResponse message:" + crmResponse.getMessage());
		                      System.out.println("crmResponse requestId:" + crmResponse.getRequestId());
	                    }
	                }, "executeBatchGetOffersSF (SetEventNotificationAction)").start();
                }
                
                
                
                // Modificato per inviare la notifica al crm in modo asincrono
                /*
                String crmResponse = crmService.sendNotifyEventOffer(fiscalCode, InteractionPointType.LOYALTY_CREDITS, userProfile, sourceID, sourceType);

                if (!crmResponse.equals(ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS)) {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Error in send crm notification (" + crmResponse + ")");
                }
                */
            }

            userTransaction.commit();

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED check authorization with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }

}
