package com.techedge.mp.core.actions.crm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CommandType;
import com.techedge.mp.core.business.interfaces.crm.DuplicationPolicy;
import com.techedge.mp.core.business.interfaces.crm.GetOfferRequest;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.Offer;
import com.techedge.mp.core.business.interfaces.crm.OfferList;
import com.techedge.mp.core.business.interfaces.crm.Promo4MeResponse;
import com.techedge.mp.core.business.interfaces.crm.Response;
import com.techedge.mp.core.business.interfaces.crm.StatusCode;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crm.CRMEventBean;
import com.techedge.mp.core.business.model.crm.CRMEventOperationBean;
import com.techedge.mp.core.business.model.crm.CRMOfferBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class Promo4MeAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public Promo4MeAction() {}

    public Promo4MeResponse execute(String ticketID, String requestID, boolean useExecuteBatch, Integer maxRetryAttemps, 
            CRMAdapterServiceRemote crmAdapterService, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        Promo4MeResponse response = new Promo4MeResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.CRM_PROMO4ME_INVALID_TICKET);
                return response;
            }

            UserBean userBean = ticketBean.getUser();

            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User not found");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.CRM_PROMO4ME_INVALID_TICKET);
                return response;
            }

            Integer userType = userBean.getUserType();
            
            Boolean isNewAcquirerFlow = false;
            isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());

            if (!isNewAcquirerFlow) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User is not in new aquirer flow category: " + userType);

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.CRM_PROMO4ME_UNAUTHORIZED);
                return response;
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User found in new aquirer flow category");
            }
            
            if (userType == User.USER_TYPE_SERVICE || userType == User.USER_TYPE_TESTER || userType == User.USER_TYPE_REFUELING || userType == User.USER_TYPE_REFUELING_NEW_ACQUIRER) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Unable to get promo4me for user type " + userType);

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.CRM_PROMO4ME_UNAUTHORIZED);
                return response;
            }
            
            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Unable to get promo4me for user in status " + userStatus);

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.CRM_PROMO4ME_UNAUTHORIZED);
                return response;
            }

            String sessionID = new IdGenerator().generateId(16).substring(0, 32);

            UserProfile userProfile = new UserProfile();

            ArrayList<InteractionPointType> interactionPoints = new ArrayList<InteractionPointType>();

            interactionPoints.add(InteractionPointType.PROMO4ME_1);
            /*interactionPoints.add(InteractionPointType.PROMO4ME_2);
            interactionPoints.add(InteractionPointType.PROMO4ME_3);
            interactionPoints.add(InteractionPointType.PROMO4ME_4);
            interactionPoints.add(InteractionPointType.PROMO4ME_5);
            interactionPoints.add(InteractionPointType.PROMO4ME_6);*/

            String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
            String audienceID = fiscalCode;

            Response responseOffers = null;
            List<Response> responseBatch = null;

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Using executeBatch: " + useExecuteBatch);

            CRMEventBean crmEventBean = CRMEventBean.createEvent(sessionID, interactionPoints, null, null, userBean, maxRetryAttemps);
            //em.persist(crmEventBean);
            
            if (useExecuteBatch) {
                ArrayList<GetOfferRequest> requests = new ArrayList<GetOfferRequest>();

                for (InteractionPointType interactionPoint : interactionPoints) {
                    GetOfferRequest request = new GetOfferRequest();
                    request.setDuplicationPolicy(DuplicationPolicy.NO_DUPLICATION);
                    request.setInteractionPoint(interactionPoint.getPoint());
                    request.setNumberRequested(100);

                    requests.add(request);
                }

                responseBatch = crmAdapterService.executeBatchGetOffersForMultipleInteractionPoints(sessionID, fiscalCode, audienceID, requests, userProfile.getParameters());

                Response responseStartSession = responseBatch.get(0);

                CRMEventOperationBean operationStartSessionBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.START_SESSION, responseStartSession);

                //em.persist(operationStartSessionBean);

                if (!responseStartSession.getStatusCode().equals(StatusCode.SUCCESS)) {

                    String message = "";

                    if (responseStartSession.getAdvisoryMessages().size() > 0) {
                        message = responseStartSession.getAdvisoryMessages().get(0).getMessage() + " (" + responseStartSession.getAdvisoryMessages().get(0).getDetailMessage()
                                + ")";
                    }

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                            "Error in InteractAPI(executeBatchGetOffersForMultipleInteractionPoints): " + message);

                    userTransaction.commit();

                    response.setStatusCode(ResponseHelper.CRM_PROMO4ME_FAILURE);
                    return response;
                }

                responseOffers = responseBatch.get(1);

                CRMEventOperationBean operationGetOffersBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.GET_OFFERS_FOR_MULTIPLE_INTERACTION_POINTS, responseOffers);

                //em.persist(operationGetOffersBean);
                
                List<CRMOfferBean> crmOffersList = CRMOfferBean.createOffers(crmEventBean, responseOffers, false);
                
                for (CRMOfferBean crmOfferBean : crmOffersList) {
                    //em.persist(crmOfferBean);
                }
                
                for (int index = 0; index < responseBatch.size(); index++) {
                    Response response2 = responseBatch.get(index);
                    for (OfferList offerList : response2.getAllOfferLists()) {
                        if (offerList.getRecommendedOffers().size() > 0) {
                            for (int i = 0; i < offerList.getRecommendedOffers().size(); i++) {
                                Offer offer = offerList.getRecommendedOffers().get(i);
                                /*System.out.println("Offer name: " + offer.getOfferName());
                                System.out.println("Offer description: " + offer.getDescription());
                                System.out.println("Offer treatmentCode: " + offer.getTreatmentCode());
                                System.out.println("Offer code: " + offer.getOfferCodeToString(";"));
                                System.out.println("Offer additional attributes size: " + offer.getAdditionalAttributes().size());
                                System.out.println("  ========= ");*/
                                response.getOffersList().add(offer);
                            }
                        }
                    }
                }
            }
            else {
                Response responseStartSession = crmAdapterService.startSession(sessionID, fiscalCode, audienceID, userProfile.getParameters());

                CRMEventOperationBean operationStartSessionBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.START_SESSION, responseStartSession);

                //em.persist(operationStartSessionBean);

                if (!responseStartSession.getStatusCode().equals(StatusCode.SUCCESS)) {

                    String message = "";

                    if (responseStartSession.getAdvisoryMessages().size() > 0) {
                        message = responseStartSession.getAdvisoryMessages().get(0).getMessage() + " (" + responseStartSession.getAdvisoryMessages().get(0).getDetailMessage()
                                + ")";
                    }

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                            "Error in InteractAPI(executeBatchGetOffersForMultipleInteractionPoints): " + message);

                    userTransaction.commit();

                    response.setStatusCode(ResponseHelper.CRM_PROMO4ME_FAILURE);
                    return response;
                }

                ArrayList<GetOfferRequest> requests = new ArrayList<GetOfferRequest>();

                for (InteractionPointType interactionPoint : interactionPoints) {
                    GetOfferRequest request = new GetOfferRequest();
                    request.setDuplicationPolicy(DuplicationPolicy.NO_DUPLICATION);
                    request.setInteractionPoint(interactionPoint.getPoint());
                    request.setNumberRequested(100);

                    requests.add(request);
                }

                responseOffers = crmAdapterService.getOffersForMultipleInteractionPoints(sessionID, requests);

                CRMEventOperationBean operationGetOffersBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.GET_OFFERS_FOR_MULTIPLE_INTERACTION_POINTS, responseOffers);

                //em.persist(operationGetOffersBean);
                
                if (!responseOffers.getStatusCode().equals(StatusCode.SUCCESS)) {

                    String message = "";

                    if (responseOffers.getAdvisoryMessages().size() > 0) {
                        message = responseOffers.getAdvisoryMessages().get(0).getMessage() + " (" + responseOffers.getAdvisoryMessages().get(0).getDetailMessage() + ")";
                    }

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error in InteractAPI(getOffersForMultipleInteractionPoints): "
                            + message);

                    userTransaction.commit();

                    response.setStatusCode(ResponseHelper.CRM_PROMO4ME_FAILURE);
                    return response;
                }
                
                List<CRMOfferBean> crmOffersList = CRMOfferBean.createOffers(crmEventBean, responseOffers, false);
                
                for (CRMOfferBean crmOfferBean : crmOffersList) {
                    //em.persist(crmOfferBean);
                }
                
                crmAdapterService.endSession(sessionID);

                for (OfferList offerList : responseOffers.getAllOfferLists()) {
                    if (offerList.getRecommendedOffers().size() > 0) {
                        for (int i = 0; i < offerList.getRecommendedOffers().size(); i++) {
                            Offer offer = offerList.getRecommendedOffers().get(i);
                            /*System.out.println("Offer name: " + offer.getOfferName());
                            System.out.println("Offer description: " + offer.getDescription());
                            System.out.println("Offer treatmentCode: " + offer.getTreatmentCode());
                            System.out.println("Offer code: " + offer.getOfferCodeToString(";"));
                            System.out.println("Offer additional attributes size: " + offer.getAdditionalAttributes().size());
                            System.out.println("  ========= ");*/
                            response.getOffersList().add(offer);
                        }
                    }
                }

            }

            //createEvent(sessionID, CommandType.END_SESSION, interactionPoint, userBean, responseEndSession);

            /*if (responseGetOffers.getOfferList() == null || responseGetOffers.getOfferList().getRecommendedOffers().size() == 0) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Nessuna offerta ricevuta da comunicare");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.CRM_PROMO4ME_SUCCESS);
                return response;
            }*/

            sortOffers(response);

            userTransaction.commit();

            response.setStatusCode(ResponseHelper.CRM_PROMO4ME_SUCCESS);

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED CRM Prmo4Me with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }

    public void sortOffers(Promo4MeResponse response) {
        Collections.sort(response.getOffersList(), new Comparator<Offer>() {

            @Override
            public int compare(Offer offer1, Offer offer2) {
                int compare = 0;
                //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                try {
                    Date value1 = (Date) offer1.getAdditionalAttributes().get("EffectiveDate"); //sdf.parse((String) offer1.getAdditionalAttributes().get("EffectiveDate"));
                    Date value2 = (Date) offer2.getAdditionalAttributes().get("EffectiveDate"); //sdf.parse((String) offer2.getAdditionalAttributes().get("EffectiveDate"));

                    compare = value1.compareTo(value2);
                }
                catch (Exception ex) {
                    System.err.println("Si � verificato un errore nella comparazione dei dati: " + ex.getMessage());
                }

                return compare;
            }

        });
    }

}
