package com.techedge.mp.core.actions.crm;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.model.crmsf.CRMSfTokenBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class TokenSFAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public TokenSFAction() {}

    public String execute(String newToken) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        
        System.out.println("newToken: "+newToken);
        
        String token=null;

        try {
            userTransaction.begin();
            System.out.println("Tentativo di recupero token...");
            CRMSfTokenBean tokenBean = QueryRepository.findTokenCrmSf(em);
            
            //se newToken == null allora recupera token
            //se newToken != null allora aggiorna token
            if(newToken==null&&tokenBean!=null){
            	System.out.println("token recuperato: "+tokenBean.getToken());
            	token=tokenBean.getToken();
            }else if (newToken!=null){
            	System.out.println("Tentativo di rimozione vecchio token...");
            	if(tokenBean!=null)
            		em.remove(tokenBean);
            	
            	System.out.println("Tentativo di salvataggio nuovo token...");
            	CRMSfTokenBean newTokenBean = new CRMSfTokenBean(newToken);
            	em.persist(newTokenBean);
            	System.out.println("token salvato correttamente: "+newToken);
            }
            
            userTransaction.commit();
            
            return token;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                e.printStackTrace();
            }
            catch (SecurityException e) {
                e.printStackTrace();
            }
            catch (SystemException e) {
                e.printStackTrace();
            }

            String message = "FAILED CRM token manage with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }
}
