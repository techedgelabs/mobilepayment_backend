package com.techedge.mp.core.actions.crm;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.crm.CrmDataElement;
import com.techedge.mp.core.business.interfaces.crm.CrmDataElementProcessingResult;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundDeliveryStatusType;
import com.techedge.mp.core.business.model.crm.CRMOutboundBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ProcessCrmOutboundInterfaceAction {

    final static String              CATEGORY_DESC = "APP";

    @Resource
    private EJBContext               context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager            em;

    @EJB
    private LoggerService            loggerService;


    public ProcessCrmOutboundInterfaceAction() {}

    public List<CrmDataElementProcessingResult> execute(List<CrmDataElement> crmDataElementList, boolean checkEmptyTable, int maxDataPerThread, int maxThreads) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            List<CrmDataElementProcessingResult> crmDataElementProcessingResultList = new ArrayList<CrmDataElementProcessingResult>(0);

            userTransaction.begin();
            
            if (crmDataElementList == null || crmDataElementList.isEmpty()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "File da processare nullo o vuoto");
                userTransaction.commit();
                return crmDataElementProcessingResultList;
            }
            
            CrmDataElement crmDataElement = crmDataElementList.get(0);
            
            if (crmDataElement.isBroadcastType()) {
                /*
                // Estrai la lista degli utenti che devono ricevere la notifica
                List<UserBean> userBeanList = QueryRepository.findUsersForNotification(em);

                for (UserBean userBean : userBeanList) {

                    if (userBean.getLastLoginDataBean() == null) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "LastloginData not found for user " + userBean.getId());
                        continue;
                    }
                    
                    if (userBean.getVirtualLoyaltyCard() != null) {
                        crmDataElement.cdCarta = userBean.getVirtualLoyaltyCard().getPanCode();
                    }
                    else {
                        crmDataElement.cdCarta = null;
                    }
                    
                    crmDataElement.codiceFiscale = userBean.getPersonalDataBean().getFiscalCode();
                    crmDataElement.email = userBean.getPersonalDataBean().getSecurityDataEmail();
                    crmDataElement.Cognome = userBean.getPersonalDataBean().getLastName();
                    crmDataElement.Nome = userBean.getPersonalDataBean().getFirstName();
                    
                    String arnEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();
                    PushNotificationContentType contentType = PushNotificationContentType.TEXT;
                    PushNotificationStatusType statusCode = null;
                    String statusMessage = null;
                    Long userID = userBean.getId();
                    String publishMessageID = null;
                    Date sendingTimestamp = new Date();
                    PushNotificationSourceType source = PushNotificationSourceType.CRM_OUTBOUND;
                    String message = crmDataElement.msg;
                    String title = "Eni Station + Notifica";

                    Calendar calendar = Calendar.getInstance();
                    calendar.setTime(sendingTimestamp);
                    calendar.add(Calendar.SECOND, expiryTime);

                    PushNotificationBean pushNotificationBean = new PushNotificationBean();
                    pushNotificationBean.setTitle(title);
                    pushNotificationBean.setSendingTimestamp(sendingTimestamp);
                    pushNotificationBean.setExpiringTimestamp(calendar.getTime());
                    pushNotificationBean.setSource(source);
                    pushNotificationBean.setContentType(contentType);
                    pushNotificationBean.setUser(userBean);
                    pushNotificationBean.setRetryAttemptsLeft(maxRetryAttemps);
                    pushNotificationBean.setStatusCode(PushNotificationStatusType.PENDING);

                    em.persist(pushNotificationBean);

                    PushNotificationMessage notificationMessage = new PushNotificationMessage();
                    notificationMessage.setMessage(pushNotificationBean.getId(), message, title);

                    if (arnEndpoint == null) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User arn endpoint not found");

                        statusCode = PushNotificationStatusType.ERROR;
                        statusMessage = "User arn endpoint not found";

                        String response = eventNotificationService.updatePushNotication(pushNotificationBean.getId(), publishMessageID, arnEndpoint, title, message,
                                sendingTimestamp, source, contentType, statusCode, statusMessage, userID);

                        if (response.equals(ResponseHelper.SYSTEM_ERROR)) {
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "SystemError in updatePushNotication");
                        }
                        else {
                            if (response.equals(ResponseHelper.USER_UPDATE_PUSH_NOTIFICATION_FAILURE)) {
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                                        "Event notification error in updatePushNotication");
                            }
                            else {
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                                        "Event notification endpoint not found in updatePushNotication");
                            }
                        }

                        CRMOutboundBean crmOutboundBean = CRMOutboundBean.generate(crmDataElement, categoryDesc, CrmOutboundDeliveryStatusType.ERROR,
                                "Arn Endpoint non configurato per l'utente", pushNotificationBean);
                        em.persist(crmOutboundBean);

                        continue;
                    }

                    PushNotificationResult pushNotificationResult = pushNotificationService.publishMessage(arnEndpoint, notificationMessage);
                    publishMessageID = pushNotificationResult.getMessageId();
                    sendingTimestamp = pushNotificationResult.getRequestTimestamp();

                    if (pushNotificationResult.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_SUCCESS)) {
                        statusCode = PushNotificationStatusType.DELIVERED;
                    }
                    else {
                        statusCode = PushNotificationStatusType.ERROR;
                    }

                    statusMessage = pushNotificationResult.getMessage();
                    String response = eventNotificationService.updatePushNotication(pushNotificationBean.getId(), publishMessageID, arnEndpoint, title, message,
                            sendingTimestamp, source, contentType, statusCode, statusMessage, userID);

                    if (response.equals(ResponseHelper.SYSTEM_ERROR)) {

                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "SystemError in updatePushNotication");

                        CRMOutboundBean crmOutboundBean = CRMOutboundBean.generate(crmDataElement, categoryDesc, CrmOutboundDeliveryStatusType.ERROR,
                                "Errore nell'invio della notifica", pushNotificationBean);
                        em.persist(crmOutboundBean);

                        continue;
                    }
                    else {

                        if (response.equals(ResponseHelper.USER_UPDATE_PUSH_NOTIFICATION_FAILURE)) {

                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Invalid user in updatePushNotication");

                            CRMOutboundBean crmOutboundBean = CRMOutboundBean.generate(crmDataElement, categoryDesc, CrmOutboundDeliveryStatusType.ERROR, "Utente non valido",
                                    pushNotificationBean);
                            em.persist(crmOutboundBean);

                        }
                        else {

                            CRMOutboundBean crmOutboundBean = CRMOutboundBean.generate(crmDataElement, categoryDesc, CrmOutboundDeliveryStatusType.SENT,
                                    "Notifica inviata con successo", pushNotificationBean);
                            em.persist(crmOutboundBean);

                        }

                        continue;
                    }
                }*/
            }
            else {
                int rowsCount = QueryRepository.getCRMOutboundRowsCount(em);

                userTransaction.commit();

                if (!checkEmptyTable || rowsCount == 0) {
                    
                    int threadCount = 1;
                    int totalDataToProcess = crmDataElementList.size();
                    System.out.println("dati da processare: " + totalDataToProcess);
                    System.out.println("max dati per thread: " + maxDataPerThread);

                    if (totalDataToProcess > maxDataPerThread) {
                        threadCount = crmDataElementList.size() / maxDataPerThread;
                        
                        if (crmDataElementList.size() % maxDataPerThread > 0) {
                            threadCount++;
                        }
                    }
                    else {
                        maxDataPerThread = totalDataToProcess;
                    }
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Inizio inserimento dati...");
                    

                    ExecutorService executor = Executors.newFixedThreadPool(maxThreads);
                    
                    
                    for (int i = 0; i < threadCount; i++) {
                        
                        int start = i * maxDataPerThread;
                        int end = start + maxDataPerThread;
                        
                        if (end > totalDataToProcess) {
                            end = totalDataToProcess;
                        }
                        
                        final List<CrmDataElement> crmDataElementSubList = crmDataElementList.subList(start, end);
                        
                        System.out.println("THREAD SCRITTURA DB N " + (i + 1) + " indice inizio: " + start + "  indice fine: " + end);
                        
                        CRMDataElementProcess crmDataElementProcess = new CRMDataElementProcess(i + 1, em, crmDataElementSubList);
                        
                        executor.submit(crmDataElementProcess);
                    }
                    
                    executor.shutdown();

                    while (!executor.isTerminated()) {}
                    
                    System.out.println("THREADS SCRITTURA DB TERMINATI");
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Dati " + crmDataElementList.size() + " inseriti nel datatbase "
                            + "e pronti per essere processati");
                }
                else {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Tabella CRM Outbound non vouta. "
                            + "Impossibile inizializzare il processo");
                }
            }

            //userTransaction.commit();

            return crmDataElementProcessingResultList;

        }
        catch (Exception ex2) {
            String message = "FAILED processing crm outbound interface with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }
    
    private class CRMDataElementProcess implements Runnable {
        
        private EntityManager em;
        private List<CrmDataElement> crmDataElementList;
        private int threadID;
        
        public CRMDataElementProcess(int threadID, EntityManager em, List<CrmDataElement> crmDataElementList) {
            this.threadID = threadID;
            this.em = em;
            this.crmDataElementList = crmDataElementList;
        }

        @Override
        public void run() {
            
            int count = 0;
            try {

                InitialContext ctx = new InitialContext();
                UserTransaction userTransaction = 
                        (UserTransaction)ctx.lookup("java:jboss/UserTransaction");

                System.out.println("THREAD SCRITTURA DB N " + threadID + " start - dati letti: " + crmDataElementList.size());
                
                for (int i = 0; i < crmDataElementList.size(); i++) {
                    
                    userTransaction.begin();
                    int index = (threadID * crmDataElementList.size()) + i;
                    
                    try {
                    
                        CrmDataElement tmpCrmDataElement = crmDataElementList.get(i);
                        
                        if (tmpCrmDataElement == null) {
                            System.out.println("THREAD SCRITTURA DB N " + threadID + " elemento : " + index + " nullo");
                            continue;
                        }
                        
                        CRMOutboundBean crmOutboundBean = CRMOutboundBean.generate(tmpCrmDataElement, CATEGORY_DESC, CrmOutboundDeliveryStatusType.CREATED, null, null);
                        em.persist(crmOutboundBean);
                        count += 1;
                        
                        userTransaction.commit();
                    }
                    catch (Exception ex) {
                        System.err.println("THREAD SCRITTURA DB N " + threadID + " riga " + index + " errore: " + ex.getMessage());
                        ex.printStackTrace();
                        try {
                            userTransaction.commit();
                        }
                        catch (Exception ex2) {
                            System.err.println("THREAD SCRITTURA DB N " + threadID + " riga " + index + " errore nella commit: " + ex2.getMessage());
                            userTransaction.rollback();
                        }
                    }
                }
                
                System.out.println("THREAD SCRITTURA DB N " + threadID + " end - dati inseriti: " + count);
            }
            catch (Exception ex) {
                System.err.println("Errore scrittura tabella CRMOutboundBean: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

}
