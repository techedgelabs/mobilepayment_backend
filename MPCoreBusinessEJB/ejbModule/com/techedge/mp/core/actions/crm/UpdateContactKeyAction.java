package com.techedge.mp.core.actions.crm;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.exceptions.UserException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.crm.StatusCodeEnum;
import com.techedge.mp.core.business.interfaces.crm.UpdateContactKeyResult;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfContactKeyBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class UpdateContactKeyAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public UpdateContactKeyAction() {}

    public UpdateContactKeyResult execute(String operationId, String requestId, String fiscalCode, String contactKey, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        UpdateContactKeyResult updateContactKeyResult = new UpdateContactKeyResult();

        try {
            userTransaction.begin();
            
            // Verifica l'esistenza di un utente customer con il codice fiscale di input
            System.out.println("Verifica l'esistenza di un utente customer con il codice fiscale: " + fiscalCode.toUpperCase());
            //UserBean user = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode.toUpperCase());
            UserBean user = QueryRepository.findNotCancelledAndRegCompleteUserCustomerByFiscalCode(em, fiscalCode.toUpperCase());
            if (user==null){
                
                updateContactKeyResult.setStatusCode(StatusCodeEnum.USER_NOT_FOUND);
            	System.out.println("User customer non trovato!!!");
            	throw new UserException("User customer non trovato fiscalCode: " + fiscalCode);
            }
            
            System.out.println("Utente trovato");
            
            // Verifica che l'utente appartenga ad una categoria NewAcquirerFlow
            System.out.println("Verifica che l'utente appartenga ad una categoria NewAcquirerFlow");
            Boolean isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(user.getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            
            if(!isNewAcquirerFlow) {
                
                updateContactKeyResult.setStatusCode(StatusCodeEnum.USER_NOT_FOUND);
            	System.out.println("User customer non appartiene alla categoria NewAcquirerFlow");
            	throw new UserException("User customer fiscalCode: " + fiscalCode + " non appartiene alla categoria " + UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            }
            	
            // Aggiorno tabella CRM_SF_CONTACT_KEY
            CRMSfContactKeyBean findCrmSfContactKey = QueryRepository.findContactKeyBeanByUser(em, user.getId());
            
            CRMSfContactKeyBean crmSfContactKey;
            
            if(findCrmSfContactKey!=null){
            	System.out.println("ContactKey find for user:"+user.getId());
            	System.out.println("Update new contactKey: "+contactKey);
            	crmSfContactKey=findCrmSfContactKey;
            	crmSfContactKey.setContactKey(contactKey);
            }else{
            	System.out.println("ContactKey create for user:"+user.getId()+"("+contactKey+")");
            	crmSfContactKey = new CRMSfContactKeyBean(contactKey, new Date(), requestId, user);
            }
            em.persist(crmSfContactKey);
            
            System.out.println("eseguo Commit");
            userTransaction.commit();
            
            System.out.println("ritorno stato: " + StatusCodeEnum.SUCCESS);
            updateContactKeyResult.setStatusCode(StatusCodeEnum.SUCCESS);
            updateContactKeyResult.setOperationId(operationId);

            return updateContactKeyResult;
        }
        catch(UserException ue){
        	try {
				userTransaction.commit();
			} catch (SecurityException | IllegalStateException
					| RollbackException | HeuristicMixedException
					| HeuristicRollbackException | SystemException e) {
				e.printStackTrace();
			}
        	updateContactKeyResult.setStatusCode(StatusCodeEnum.USER_NOT_FOUND);
        	return updateContactKeyResult;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                e.printStackTrace();
            }
            catch (SecurityException e) {
                e.printStackTrace();
            }
            catch (SystemException e) {
                e.printStackTrace();
            }

            String message = "FAILED CRM updateContactKey with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }
}
