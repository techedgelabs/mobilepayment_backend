package com.techedge.mp.core.actions.crm;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ParametersService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.exceptions.ParameterNotFoundException;
import com.techedge.mp.core.business.interfaces.AppLink;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MissionDetailDataResponse;
import com.techedge.mp.core.business.interfaces.MissionInfoDataDetail;
import com.techedge.mp.core.business.interfaces.ParameterNotification;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CommandType;
import com.techedge.mp.core.business.interfaces.crm.DuplicationPolicy;
import com.techedge.mp.core.business.interfaces.crm.GetMissionsResponse;
import com.techedge.mp.core.business.interfaces.crm.GetOfferRequest;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.Offer;
import com.techedge.mp.core.business.interfaces.crm.OfferList;
import com.techedge.mp.core.business.interfaces.crm.Promo4MeResponse;
import com.techedge.mp.core.business.interfaces.crm.Response;
import com.techedge.mp.core.business.interfaces.crm.StatusCode;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crm.CRMEventBean;
import com.techedge.mp.core.business.model.crm.CRMEventOperationBean;
import com.techedge.mp.core.business.model.crm.CRMOfferBean;
import com.techedge.mp.core.business.model.crm.CRMOfferParametersBean;
import com.techedge.mp.core.business.model.crm.CRMOfferVoucherPromotionalBean;
import com.techedge.mp.core.business.model.crm.CRMPromotionBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.CreateVoucherPromotional;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class GetMissionsAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;
    
    @EJB
    private ParametersService parametersService;
    
    private final String 				PARAM_CRM_SF_ACTIVE="CRM_SF_ACTIVE";

    public GetMissionsAction() {}

    public GetMissionsResponse execute(String ticketID, String requestID, Integer maxRetryAttemps, 
            CRMAdapterServiceRemote crmAdapterService, String interactionPoint) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        GetMissionsResponse response = new GetMissionsResponse();

        try {
            userTransaction.begin();

            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketID);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.CRM_GET_MISSIONS_INVALID_TICKET);
                return response;
            }

            UserBean userBean = ticketBean.getUser();

            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User not found");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.CRM_GET_MISSIONS_INVALID_TICKET);
                return response;
            }

            Integer userType = userBean.getUserType();
            
//            Boolean isNewAcquirerFlow = false;
//            isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
//
//            if (!isNewAcquirerFlow) {
//                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User is not in new aquirer flow category: " + userType);
//
//                userTransaction.commit();
//
//                response.setStatusCode(ResponseHelper.CRM_GET_MISSIONS_UNAUTHORIZED);
//                return response;
//            }
//            else {
//                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User found in new aquirer flow category");
//            }
            
            if (userType == User.USER_TYPE_SERVICE || userType == User.USER_TYPE_TESTER || userType == User.USER_TYPE_REFUELING || userType == User.USER_TYPE_REFUELING_NEW_ACQUIRER) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Unable to get promo4me for user type " + userType);

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.CRM_GET_MISSIONS_UNAUTHORIZED);
                return response;
            }
            
            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Unable to get missions for user in status " + userStatus);

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.CRM_GET_MISSIONS_UNAUTHORIZED);
                return response;
            }

            String sessionID = new IdGenerator().generateId(16).substring(0, 32);

            UserProfile userProfile = new UserProfile();
            
            String fiscalCode = userBean.getPersonalDataBean().getFiscalCode();
            String audienceID = fiscalCode;

            Response responseOffers = null;
            
            //List<Response> responseBatch = crmAdapterService.executeBatchGetOffers(sessionID, fiscalCode, audienceID, interactionPoint, userProfile.getParameters());
            
            String crmSfActive="0";
	    	try {
	    		crmSfActive = parametersService.getParamValue(PARAM_CRM_SF_ACTIVE);
			} catch (ParameterNotFoundException e) {
				e.printStackTrace();
			}
            
            
            
            ArrayList<GetOfferRequest> requests = new ArrayList<GetOfferRequest>();
            
            GetOfferRequest request = new GetOfferRequest();
            request.setDuplicationPolicy(DuplicationPolicy.NO_DUPLICATION);
            request.setInteractionPoint(interactionPoint);
            request.setNumberRequested(100);
            requests.add(request);
            
            List<Response> responseBatch = crmAdapterService.executeBatchGetOffersForMultipleInteractionPoints(sessionID, fiscalCode, audienceID, requests, userProfile.getParameters());

            Response responseStartSession = responseBatch.get(0);

            if (!responseStartSession.getStatusCode().equals(StatusCode.SUCCESS)) {

                String message = "";

                if (responseStartSession.getAdvisoryMessages().size() > 0) {
                    message = responseStartSession.getAdvisoryMessages().get(0).getMessage() + " (" + responseStartSession.getAdvisoryMessages().get(0).getDetailMessage()
                            + ")";
                }

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error in InteractAPI(executeBatchGetOffers): " + message);

                if (responseStartSession.getStatusCode().equals(StatusCode.ERROR)) {
                    userTransaction.commit();
                    //TODO Gestire l'errore
                    response.setStatusCode(ResponseHelper.CRM_GET_MISSIONS_FAILURE);
                    return response;
                }
            }

            responseOffers = responseBatch.get(1);

            if (!responseOffers.getStatusCode().equals(StatusCode.SUCCESS)) {
                String message = "";

                if (responseOffers.getAdvisoryMessages().size() > 0) {
                    message = responseOffers.getAdvisoryMessages().get(0).getMessage() + " (" + responseOffers.getAdvisoryMessages().get(0).getDetailMessage() + ")";
                }

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error in InteractAPI(executeBatchGetOffers): " + message);

                if (responseOffers.getStatusCode().equals(StatusCode.ERROR)) {

                    //TODO gestire l'errore
                    userTransaction.commit();

                    response.setStatusCode(ResponseHelper.CRM_GET_MISSIONS_FAILURE);
                    return response;
                }
            }

            
            Response responseEndSession = responseBatch.get(2);


            if (responseOffers.getOfferList() == null || responseOffers.getOfferList().getRecommendedOffers().size() == 0) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Nessuna offerta ricevuta da comunicare");

                userBean.setUpdatedMission(Boolean.FALSE);
                em.merge(userBean);
                
                userTransaction.commit();

                response.setStatusCode(ResponseHelper.CRM_NOTIFY_EVENT_OFFERS_NOT_FOUND);
                return response;
            }
            
            
            //Popolo le missionDetail
            if (responseOffers.getOfferList() != null) {
                
                for (int index = 0; index < responseBatch.size(); index++) {
                    Response response2 = responseBatch.get(index);
                    for (OfferList offerList : response2.getAllOfferLists()) {
                        if (offerList.getRecommendedOffers().size() > 0) {
                            for (int i = 0; i < offerList.getRecommendedOffers().size(); i++) {
                                Offer offer = offerList.getRecommendedOffers().get(i);
                                response.getOffersList().add(offer);
                            }
                        }
                    }
                }

            }
            //End MissionDetail
            

            sortOffers(response);
            
            /*
            // Offer 01
            ArrayList<String> codeList1 = new ArrayList<String>();
            String code1 = "code1";
            codeList1.add(code1);
            
            Offer offer1 = new Offer();
            offer1.setDescription("Description1");
            offer1.setOfferCode(codeList1);
            offer1.setOfferName("offerta1");
            offer1.setScore(150);

            offer1.getAdditionalAttributes().put("TipoMissione", "a step");
            offer1.getAdditionalAttributes().put("NumeroOperazioniEseguite", "4");
            offer1.getAdditionalAttributes().put("NumeroOperazioniObiettivo", "4");
            offer1.getAdditionalAttributes().put("UrlApp", "http://5.10.69.210/ENI_VoucherCinema/index.php");
            offer1.getAdditionalAttributes().put("C_2", "2My8Y030fK");
            offer1.getAdditionalAttributes().put("C_3", "3My8Y030fK");
            offer1.getAdditionalAttributes().put("C_4", "4My8Y030fK");
            offer1.getAdditionalAttributes().put("C_5", "5My8Y030fK");
            offer1.getAdditionalAttributes().put("C_6", "6My8Y030fK");
            offer1.getAdditionalAttributes().put("C_7", "7My8Y030fK");
            offer1.getAdditionalAttributes().put("C_8", "8My8Y030fK");
            offer1.getAdditionalAttributes().put("C_9", "9My8Y030fK");
            offer1.getAdditionalAttributes().put("C_10", "10y8Y030fK");
            
            response.getOffersList().add(offer1);
            
            
            // Offer 02
            ArrayList<String> codeList2 = new ArrayList<String>();
            String code2 = "code2";
            codeList2.add(code2);
            
            Offer offer2 = new Offer();
            offer2.setDescription("Description2");
            offer2.setOfferCode(codeList2);
            offer2.setOfferName("offerta2");
            offer2.setScore(250);

            offer2.getAdditionalAttributes().put("TipoMissione", "a step");
            offer2.getAdditionalAttributes().put("NumeroOperazioniEseguite", "2");
            offer2.getAdditionalAttributes().put("NumeroOperazioniObiettivo", "4");
            offer2.getAdditionalAttributes().put("UrlApp", "http://5.10.69.210/ENI_VoucherCinema/index.php");
            offer2.getAdditionalAttributes().put("C_2", "2My8Y030fK");
            offer2.getAdditionalAttributes().put("C_3", "3My8Y030fK");
            offer2.getAdditionalAttributes().put("C_4", "4My8Y030fK");
            offer2.getAdditionalAttributes().put("C_5", "5My8Y030fK");
            offer2.getAdditionalAttributes().put("C_6", "6My8Y030fK");
            offer2.getAdditionalAttributes().put("C_7", "7My8Y030fK");
            offer2.getAdditionalAttributes().put("C_8", "8My8Y030fK");
            offer2.getAdditionalAttributes().put("C_9", "9My8Y030fK");
            offer2.getAdditionalAttributes().put("C_10", "10y8Y030fK");
            
            response.getOffersList().add(offer2);
            
            
            // Offer 03
            ArrayList<String> codeList3 = new ArrayList<String>();
            String code3 = "code3";
            codeList3.add(code3);
            
            Offer offer3 = new Offer();
            offer3.setDescription("Description3");
            offer3.setOfferCode(codeList3);
            offer3.setOfferName("offerta3");
            offer3.setScore(350);

            offer3.getAdditionalAttributes().put("TipoMissione", "temporizzata");
            offer3.getAdditionalAttributes().put("ExpirationDuration", "1523009467000");
            offer3.getAdditionalAttributes().put("EffectiveDate", "1523008467000");
            offer3.getAdditionalAttributes().put("UrlApp", "http://5.10.69.210/ENI_VoucherCinema/index.php");
            offer3.getAdditionalAttributes().put("C_2", "2My8Y030fK");
            offer3.getAdditionalAttributes().put("C_3", "3My8Y030fK");
            offer3.getAdditionalAttributes().put("C_4", "4My8Y030fK");
            offer3.getAdditionalAttributes().put("C_5", "5My8Y030fK");
            offer3.getAdditionalAttributes().put("C_6", "6My8Y030fK");
            offer3.getAdditionalAttributes().put("C_7", "7My8Y030fK");
            offer3.getAdditionalAttributes().put("C_8", "8My8Y030fK");
            offer3.getAdditionalAttributes().put("C_9", "9My8Y030fK");
            offer3.getAdditionalAttributes().put("C_10", "10y8Y030fK");
            
            response.getOffersList().add(offer3);
            
            
            // Offer 04
            ArrayList<String> codeList4 = new ArrayList<String>();
            String code4 = "code4";
            codeList4.add(code4);
            
            Offer offer4 = new Offer();
            offer4.setDescription("Description4");
            offer4.setOfferCode(codeList4);
            offer4.setOfferName("offerta4");
            offer4.setScore(450);

            offer4.getAdditionalAttributes().put("TipoMissione", "temporizzata");
            offer4.getAdditionalAttributes().put("ExpirationDuration", "1523009467000");
            offer4.getAdditionalAttributes().put("EffectiveDate", "1523028467000");
            offer4.getAdditionalAttributes().put("UrlApp", "http://5.10.69.210/ENI_VoucherCinema/index.php");
            offer4.getAdditionalAttributes().put("C_2", "2My8Y030fK");
            offer4.getAdditionalAttributes().put("C_3", "3My8Y030fK");
            offer4.getAdditionalAttributes().put("C_4", "4My8Y030fK");
            offer4.getAdditionalAttributes().put("C_5", "5My8Y030fK");
            offer4.getAdditionalAttributes().put("C_6", "6My8Y030fK");
            offer4.getAdditionalAttributes().put("C_7", "7My8Y030fK");
            offer4.getAdditionalAttributes().put("C_8", "8My8Y030fK");
            offer4.getAdditionalAttributes().put("C_9", "9My8Y030fK");
            offer4.getAdditionalAttributes().put("C_10", "10y8Y030fK");
            
            response.getOffersList().add(offer4);
            */
            //End dati stubbati
            
            // Disattiva il flag che informa l'utente che � presente un aggiornamento nelle missioni
            userBean.setUpdatedMission(Boolean.FALSE);
            em.merge(userBean);
            
            userTransaction.commit();

            response.setStatusCode(ResponseHelper.CRM_GET_MISSIONS_SUCCESS);

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED CRM getMissions with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }

    public void sortOffers(GetMissionsResponse response) {
        Collections.sort(response.getOffersList(), new Comparator<Offer>() {

            @Override
            public int compare(Offer offer1, Offer offer2) {
                int compare = 0;
                //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

                try {
                    Date value1 = (Date) offer1.getAdditionalAttributes().get("EffectiveDate"); //sdf.parse((String) offer1.getAdditionalAttributes().get("EffectiveDate"));
                    Date value2 = (Date) offer2.getAdditionalAttributes().get("EffectiveDate"); //sdf.parse((String) offer2.getAdditionalAttributes().get("EffectiveDate"));

                    compare = value1.compareTo(value2);
                }
                catch (Exception ex) {
                    System.err.println("Si � verificato un errore nella comparazione dei dati: " + ex.getMessage());
                }

                return compare;
            }

        });
    }

}
