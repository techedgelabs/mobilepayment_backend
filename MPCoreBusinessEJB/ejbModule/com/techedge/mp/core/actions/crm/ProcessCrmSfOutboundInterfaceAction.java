package com.techedge.mp.core.actions.crm;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.crm.CrmDataElementProcessingResult;
import com.techedge.mp.core.business.interfaces.crm.CrmOutboundDeliveryStatusType;
import com.techedge.mp.core.business.interfaces.crm.CrmSfDataElement;
import com.techedge.mp.core.business.interfaces.crm.CrmSfOutboundDeliveryStatusType;
import com.techedge.mp.core.business.model.crm.CRMOutboundBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfOutboundBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ProcessCrmSfOutboundInterfaceAction {

    final static String              CATEGORY_DESC = "APP";

    @Resource
    private EJBContext               context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager            em;

    @EJB
    private LoggerService            loggerService;


    public ProcessCrmSfOutboundInterfaceAction() {}

    public List<CrmDataElementProcessingResult> execute(List<CrmSfDataElement> crmSfDataElementList, boolean checkEmptyTable, int maxDataPerThread, int maxThreads) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            List<CrmDataElementProcessingResult> crmDataElementProcessingResultList = new ArrayList<CrmDataElementProcessingResult>(0);

            userTransaction.begin();
            
            if (crmSfDataElementList == null || crmSfDataElementList.isEmpty()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "File da processare nullo o vuoto");
                userTransaction.commit();
                return crmDataElementProcessingResultList;
            }
            
            CrmSfDataElement crmSfDataElement = crmSfDataElementList.get(0);
            
            if (crmSfDataElement.isBroadcastType()) {
                
            }
            else {
                int rowsCount = QueryRepository.getCRMSfOutboundRowsCount(em);

                userTransaction.commit();

                if (!checkEmptyTable || rowsCount == 0) {
                    
                    int threadCount = 1;
                    int totalDataToProcess = crmSfDataElementList.size();
                    System.out.println("dati da processare: " + totalDataToProcess);
                    System.out.println("max dati per thread: " + maxDataPerThread);

                    if (totalDataToProcess > maxDataPerThread) {
                        threadCount = crmSfDataElementList.size() / maxDataPerThread;
                        
                        if (crmSfDataElementList.size() % maxDataPerThread > 0) {
                            threadCount++;
                        }
                    }
                    else {
                        maxDataPerThread = totalDataToProcess;
                    }
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Inizio inserimento dati...");
                    

                    ExecutorService executor = Executors.newFixedThreadPool(maxThreads);
                    
                    
                    for (int i = 0; i < threadCount; i++) {
                        
                        int start = i * maxDataPerThread;
                        int end = start + maxDataPerThread;
                        
                        if (end > totalDataToProcess) {
                            end = totalDataToProcess;
                        }
                        
                        final List<CrmSfDataElement> crmSfDataElementSubList = crmSfDataElementList.subList(start, end);
                        
                        System.out.println("THREAD SCRITTURA DB N " + (i + 1) + " indice inizio: " + start + "  indice fine: " + end);
                        
                        CRMDataElementProcess crmDataElementProcess = new CRMDataElementProcess(i + 1, em, crmSfDataElementSubList);
                        
                        executor.submit(crmDataElementProcess);
                    }
                    
                    executor.shutdown();

                    while (!executor.isTerminated()) {}
                    
                    System.out.println("THREADS SCRITTURA DB TERMINATI");
                    
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Dati " + crmSfDataElementList.size() + " inseriti nel datatbase "
                            + "e pronti per essere processati");
                }
                else {
                    this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Tabella CRM Outbound non vouta. "
                            + "Impossibile inizializzare il processo");
                }
            }

            //userTransaction.commit();

            return crmDataElementProcessingResultList;

        }
        catch (Exception ex2) {
            String message = "FAILED processing crm outbound interface with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }
    
    private class CRMDataElementProcess implements Runnable {
        
        private EntityManager em;
        private List<CrmSfDataElement> crmDataElementList;
        private int threadID;
        
        public CRMDataElementProcess(int threadID, EntityManager em, List<CrmSfDataElement> crmDataElementList) {
            this.threadID = threadID;
            this.em = em;
            this.crmDataElementList = crmDataElementList;
        }

        @Override
        public void run() {
            
            int count = 0;
            try {

                InitialContext ctx = new InitialContext();
                UserTransaction userTransaction = 
                        (UserTransaction)ctx.lookup("java:jboss/UserTransaction");

                System.out.println("THREAD SCRITTURA DB N " + threadID + " start - dati letti: " + crmDataElementList.size());
                
                for (int i = 0; i < crmDataElementList.size(); i++) {
                    
                    userTransaction.begin();
                    int index = (threadID * crmDataElementList.size()) + i;
                    
                    try {
                    
                        CrmSfDataElement tmpCrmDataElement = crmDataElementList.get(i);
                        
                        if (tmpCrmDataElement == null) {
                            System.out.println("THREAD SCRITTURA DB N " + threadID + " elemento : " + index + " nullo");
                            continue;
                        }
                        
                        CRMSfOutboundBean crmOutboundBean = CRMSfOutboundBean.generate(tmpCrmDataElement, CrmSfOutboundDeliveryStatusType.CREATED.getCode());
                        em.persist(crmOutboundBean);
                        count += 1;
                        
                        userTransaction.commit();
                    }
                    catch (Exception ex) {
                        System.err.println("THREAD SCRITTURA DB N " + threadID + " riga " + index + " errore: " + ex.getMessage());
                        ex.printStackTrace();
                        try {
                            userTransaction.commit();
                        }
                        catch (Exception ex2) {
                            System.err.println("THREAD SCRITTURA DB N " + threadID + " riga " + index + " errore nella commit: " + ex2.getMessage());
                            userTransaction.rollback();
                        }
                    }
                }
                
                System.out.println("THREAD SCRITTURA DB N " + threadID + " end - dati inseriti: " + count);
            }
            catch (Exception ex) {
                System.err.println("Errore scrittura tabella CRMOutboundBean: " + ex.getMessage());
                ex.printStackTrace();
            }
        }
    }

}
