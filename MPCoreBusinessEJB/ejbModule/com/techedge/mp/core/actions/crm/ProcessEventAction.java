package com.techedge.mp.core.actions.crm;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.exceptions.CampaignGenericException;
import com.techedge.mp.core.business.exceptions.PromoVoucherException;
import com.techedge.mp.core.business.exceptions.UserException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.ProcessEventResult;
import com.techedge.mp.core.business.interfaces.crm.StatusCodeEnum;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crmsf.CRMSfEventBean;
import com.techedge.mp.core.business.model.voucher.VoucherPromotionalCrmSfBean;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.CreateVoucherPromotionalCrmSf;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class ProcessEventAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public ProcessEventAction() {}

    public ProcessEventResult execute(String operationId, String requestId, String fiscalCode, String promoCode, 
    		Double voucherAmount, FidelityServiceRemote fidelityService, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        ProcessEventResult processEventResult = new ProcessEventResult();

        try {
            userTransaction.begin();
            
            // Verifica l'esistenza di un utente customer con il codice fiscale di input
            System.out.println("Verifica l'esistenza di un utente customer con il codice fiscale: "+fiscalCode.toUpperCase());
            UserBean user = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode.toUpperCase());
            if (user==null){
            	processEventResult.setStatusCode(StatusCodeEnum.USER_NOT_FOUND);
            	System.out.println("User customer non trovato!!!");
            	throw new UserException("User customer non trovato fiscalCode:"+fiscalCode);
            }
            System.out.println("Utente trovato");
            
           
            // Verifica che l'utente estratto abbia completato l'iscrizione
            System.out.println("Verifica che l'utente estratto abbia completato l'iscrizione");
            if(user.getUserType().intValue()!=1 || !user.getUserStatusRegistrationCompleted()){
            	processEventResult.setStatusCode(StatusCodeEnum.USER_NOT_FOUND);
            	System.out.println("User customer non ha completato l'iscrizione!!!");
            	throw new UserException("User customer fiscalCode:"+fiscalCode+" non ha completato l'iscrizione!");
            }
            
            // Verifica che l'utente appartenga ad una categoria NewAcquirerFlow
            System.out.println("Verifica che l'utente appartenga ad una categoria NewAcquirerFlow");
            Boolean isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(user.getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            
            if(!isNewAcquirerFlow){
            	processEventResult.setStatusCode(StatusCodeEnum.USER_NOT_FOUND);
            	System.out.println("User customer  non appartiene alla categoria NewAcquirerFlow");
            	throw new UserException("User customer fiscalCode:"+fiscalCode+" non appartiene alla categoria "+UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            }
            	
            // Controlla se la richiesta � stata gi� processata
            //   se gi� processata con successo, restituisci l'esito positivo
            //   se gi� processata ma in errore prova a riprocessarla
            //   se non ancora processata processala
            System.out.println("Controlla se la richiesta � stata gi� processata");
            List<VoucherPromotionalCrmSfBean> listRequest = QueryRepository.findCRMSfVoucherPromotionalByOperationId(em, operationId);
            System.out.println("Lista richieste size:"+listRequest.size());
            
            if(checkProcess(listRequest)){
            	// Verifica la validit� dell'importo :TODO da definire voucherPromotional vaucher_code
            	
            	// Chiama il servizio di creazione voucher
            	System.out.println("Chiamo il servizio di creazione voucher");
            	CreateVoucherPromotionalCrmSf createVoucher= new CreateVoucherPromotionalCrmSf(em, fidelityService, user);
            	String response = createVoucher.create(promoCode, voucherAmount, operationId, requestId);
            	// Assegna il voucher all'utente
            	if(response.equalsIgnoreCase(ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_SUCCESS)){
            		System.out.println("Assegno il voucher all'utente");
            		createVoucher.associateToUser(createVoucher.getCreateVoucherResult());
            	}
            	else{
            		if(response.equalsIgnoreCase(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_INVALID_FISCAL_CODE))
            			throw new UserException("User customer fiscalCode:"+fiscalCode+" response:"+response);
            		else if(response.equalsIgnoreCase(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_INVALID_PROMO_CODE))
            			throw new PromoVoucherException("User customer fiscalCode:"+fiscalCode+" response:"+response);
            		else if(response.equalsIgnoreCase(ResponseHelper.USER_LOAD_VOUCHER_CAMPAIGN_GENERIC_ERROR))
            			throw new CampaignGenericException("User customer fiscalCode:"+fiscalCode+" response:"+response);
            	}
            	
            }else{
            	//TODO: verificare che il voucher_code sia realmente asociato all'utente
            }
            
            System.out.println("eseguo Commit");
            userTransaction.commit();
            
            System.out.println("ritorno stato: "+StatusCodeEnum.SUCCESS);
            processEventResult.setStatusCode(StatusCodeEnum.SUCCESS);
            processEventResult.setOperationId(operationId);

            return processEventResult;
        }
        catch(UserException ue){
        	try {
				userTransaction.commit();
			} catch (SecurityException | IllegalStateException
					| RollbackException | HeuristicMixedException
					| HeuristicRollbackException | SystemException e) {
				e.printStackTrace();
			}
        	processEventResult.setStatusCode(StatusCodeEnum.USER_NOT_FOUND);
        	return processEventResult;
        }
        catch(PromoVoucherException pe){
        	try {
				userTransaction.commit();
			} catch (SecurityException | IllegalStateException
					| RollbackException | HeuristicMixedException
					| HeuristicRollbackException | SystemException e) {
				e.printStackTrace();
			}
        	processEventResult.setStatusCode(StatusCodeEnum.INVALID_PROMO_CODE);
        	return processEventResult;
        }
        catch(CampaignGenericException pe){
        	try {
				userTransaction.commit();
			} catch (SecurityException | IllegalStateException
					| RollbackException | HeuristicMixedException
					| HeuristicRollbackException | SystemException e) {
				e.printStackTrace();
			}
        	processEventResult.setStatusCode(StatusCodeEnum.SYSTEM_ERROR);
        	return processEventResult;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                e.printStackTrace();
            }
            catch (SecurityException e) {
                e.printStackTrace();
            }
            catch (SystemException e) {
                e.printStackTrace();
            }

            String message = "FAILED CRM processEvent with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }
    
    /**
     * Controlla se la richiesta � stata gi� processata
     * @param listRequest
     * @return
     */
    private boolean checkProcess(List<VoucherPromotionalCrmSfBean> listRequest){
    	boolean result=true;
    	for(VoucherPromotionalCrmSfBean voucher:listRequest){
    		System.out.println("voucher :"+voucher.getId() +" SourceOperationId:"+voucher.getSourceOperationId()+" SourceRequestId:"+ voucher.getSourceRequestId());
    		if(voucher.getStatusCode().equalsIgnoreCase("00")){
    			System.out.println("Trovato voucher gi� processato ritorno SUCCESS");
    			result=false;
    			break;
    		}
    			
    	}
    	return result;
    }

}
