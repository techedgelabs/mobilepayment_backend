package com.techedge.mp.core.actions.crm;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.EventNotificationService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.crm.CommandType;
import com.techedge.mp.core.business.interfaces.crm.InteractionPointType;
import com.techedge.mp.core.business.interfaces.crm.Response;
import com.techedge.mp.core.business.interfaces.crm.CRMEventSourceType;
import com.techedge.mp.core.business.interfaces.crm.StatusCode;
import com.techedge.mp.core.business.interfaces.crm.UserProfile;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationContentType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationSourceType;
import com.techedge.mp.core.business.interfaces.pushnotification.PushNotificationStatusType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.crm.CRMEventBean;
import com.techedge.mp.core.business.model.crm.CRMEventOperationBean;
import com.techedge.mp.core.business.model.crm.CRMOfferBean;
import com.techedge.mp.core.business.model.crm.CRMOfferParametersBean;
import com.techedge.mp.core.business.model.crm.CRMOfferVoucherPromotionalBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.voucher.CreateVoucherPromotional;
import com.techedge.mp.crm.adapter.business.CRMAdapterServiceRemote;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationMessage;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class SendNotifyEventOfferAction {

    @Resource
    private EJBContext               context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager            em;

    @EJB
    private LoggerService            loggerService;

    @EJB
    private EventNotificationService eventNotificationService;

    public SendNotifyEventOfferAction() {}

    public String execute(String fiscalCode, InteractionPointType interactionPoint, UserProfile userProfile, Long sourceID, CRMEventSourceType sourceType, Integer expiryTime, 
            Integer maxRetryAttemps, boolean useExecuteBatch, CRMAdapterServiceRemote crmAdapterService, PushNotificationServiceRemote pushNotificationService, 
            FidelityServiceRemote fidelityService, UserCategoryService userCategoryService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            String response = null;

            UserBean userBean = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode);
            if (userBean == null) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User not found");

                userTransaction.commit();

                response = ResponseHelper.CRM_NOTIFY_EVENT_INVALID_TICKET;
                return response;
            }

            Integer userType = userBean.getUserType();
            
            Boolean isNewAcquirerFlow = false;
            isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());

            if (!isNewAcquirerFlow) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User is not in new aquirer flow category: " + userType);

                userTransaction.commit();

                response = ResponseHelper.CRM_NOTIFY_EVENT_UNAUTHORIZED;
                return response;
            }
            else {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User found in new aquirer flow category");
            }

            if (userType == User.USER_TYPE_SERVICE || userType == User.USER_TYPE_TESTER 
                    || userType == User.USER_TYPE_REFUELING || userType == User.USER_TYPE_REFUELING_NEW_ACQUIRER) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Unable to get offer for user type: " + userType);

                userTransaction.commit();

                response = ResponseHelper.CRM_NOTIFY_EVENT_UNAUTHORIZED;
                return response;
            }
            
            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // Un utente che si trova in questo stato non pu� invocare questo servizio
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Unable to get offer for user in status " + userStatus);

                userTransaction.commit();

                response = ResponseHelper.CRM_NOTIFY_EVENT_UNAUTHORIZED;
                return response;
            }

            String sessionID = new IdGenerator().generateId(16).substring(0, 32);
            String audienceID = "999";

            if (!interactionPoint.equals(InteractionPointType.REGISTRATION)) {
                audienceID = userBean.getPersonalDataBean().getFiscalCode();
            }

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Using executeBatch: " + useExecuteBatch);

            Response responseOffers = null;
            
            CRMEventBean crmEventBean = CRMEventBean.createEvent(sessionID, interactionPoint, sourceID, sourceType, userBean, maxRetryAttemps);
            em.persist(crmEventBean);
            
            if (useExecuteBatch) {
                List<Response> responseBatch = crmAdapterService.executeBatchGetOffers(sessionID, fiscalCode, audienceID, interactionPoint.getPoint(), userProfile.getParameters());
                Response responseStartSession = responseBatch.get(0);

                CRMEventOperationBean operationStartSessionBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.START_SESSION, responseStartSession);

                em.persist(operationStartSessionBean);

                if (!responseStartSession.getStatusCode().equals(StatusCode.SUCCESS)) {

                    String message = "";

                    if (responseStartSession.getAdvisoryMessages().size() > 0) {
                        message = responseStartSession.getAdvisoryMessages().get(0).getMessage() + " (" + responseStartSession.getAdvisoryMessages().get(0).getDetailMessage()
                                + ")";
                    }

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error in InteractAPI(executeBatchGetOffers): " + message);

                    if (responseStartSession.getStatusCode().equals(StatusCode.ERROR)) {
                        crmEventBean.setToReconcilie(true);
                        crmEventBean.setReconciliationAttemptsLeft(maxRetryAttemps);
                        em.merge(crmEventBean);
                        
                        userTransaction.commit();
    
                        response = ResponseHelper.CRM_NOTIFY_EVENT_FAILURE;
                        return response;
                    }
                }

                responseOffers = responseBatch.get(1);

                CRMEventOperationBean operationGetOffersBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.GET_OFFERS, responseOffers);

                em.persist(operationGetOffersBean);

                //if (responseGetOffers.getStatusCode().equals(StatusCode.ERROR)
                //        && (responseGetOffers.getOfferList() == null || responseGetOffers.getOfferList().getRecommendedOffers().size() == 0)) {
                if (!responseOffers.getStatusCode().equals(StatusCode.SUCCESS)) {
                    String message = "";

                    if (responseOffers.getAdvisoryMessages().size() > 0) {
                        message = responseOffers.getAdvisoryMessages().get(0).getMessage() + " (" + responseOffers.getAdvisoryMessages().get(0).getDetailMessage() + ")";
                    }

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error in InteractAPI(executeBatchGetOffers): " + message);

                    if (responseOffers.getStatusCode().equals(StatusCode.ERROR)) {
                        crmEventBean.setToReconcilie(true);
                        crmEventBean.setReconciliationAttemptsLeft(maxRetryAttemps);
                        em.merge(crmEventBean);
    
                        userTransaction.commit();
    
                        response = ResponseHelper.CRM_NOTIFY_EVENT_FAILURE;
                        return response;
                    }
                }

                List<CRMOfferBean> crmOffersList = CRMOfferBean.createOffers(crmEventBean, responseOffers, false);
                
                for (CRMOfferBean crmOfferBean : crmOffersList) {
                    em.persist(crmOfferBean);
                    crmEventBean.getOfferBeanList().add(crmOfferBean);
                }

                Response responseEndSession = responseBatch.get(2);

                CRMEventOperationBean operationEndSessionBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.END_SESSION, responseEndSession);

                em.persist(operationEndSessionBean);
            }
            else {
                Response responseStartSession = crmAdapterService.startSession(sessionID, fiscalCode, audienceID, userProfile.getParameters());

                CRMEventOperationBean operationStartSessionBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.START_SESSION, responseStartSession);

                em.persist(operationStartSessionBean);

                //Response responseStartSession = new Response();
                //responseStartSession.setStatusCode(StatusCode.SUCCESS);

                if (!responseStartSession.getStatusCode().equals(StatusCode.SUCCESS)) {

                    String message = "";

                    if (responseStartSession.getAdvisoryMessages().size() > 0) {
                        message = responseStartSession.getAdvisoryMessages().get(0).getMessage() + " (" + responseStartSession.getAdvisoryMessages().get(0).getDetailMessage()
                                + ")";
                    }

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error in InteractAPI(startSession): " + message);

                    if (responseStartSession.getStatusCode().equals(StatusCode.ERROR)) {
                        crmEventBean.setToReconcilie(true);
                        crmEventBean.setReconciliationAttemptsLeft(maxRetryAttemps);
                        em.merge(crmEventBean);
    
                        userTransaction.commit();
    
                        response = ResponseHelper.CRM_NOTIFY_EVENT_FAILURE;
                        return response;
                    }
                }

                responseOffers = crmAdapterService.getOffers(sessionID, interactionPoint.getPoint(), 1);

                CRMEventOperationBean operationGetOffersBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.GET_OFFERS, responseOffers);

                em.persist(operationGetOffersBean);

                //if (responseGetOffers.getStatusCode().equals(StatusCode.ERROR)
                //        && (responseGetOffers.getOfferList() == null || responseGetOffers.getOfferList().getRecommendedOffers().size() == 0)) {
                if (!responseOffers.getStatusCode().equals(StatusCode.SUCCESS)) {
                    String message = "";

                    if (responseOffers.getAdvisoryMessages().size() > 0) {
                        message = responseOffers.getAdvisoryMessages().get(0).getMessage() + " (" + responseOffers.getAdvisoryMessages().get(0).getDetailMessage() + ")";
                    }

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error in InteractAPI(getOffer): " + message);

                    if (responseOffers.getStatusCode().equals(StatusCode.ERROR)) {
                        crmEventBean.setToReconcilie(true);
                        crmEventBean.setReconciliationAttemptsLeft(maxRetryAttemps);
                        em.merge(crmEventBean);
    
                        userTransaction.commit();
    
                        response = ResponseHelper.CRM_NOTIFY_EVENT_FAILURE;
                        return response;
                    }
                }
                
                List<CRMOfferBean> crmOffersList = CRMOfferBean.createOffers(crmEventBean, responseOffers, false);
                
                for (CRMOfferBean crmOfferBean : crmOffersList) {
                    em.persist(crmOfferBean);
                    crmEventBean.getOfferBeanList().add(crmOfferBean);
                }

                Response responseEndSession = crmAdapterService.endSession(sessionID);

                CRMEventOperationBean operationEndSessionBean = CRMEventOperationBean.createOperation(crmEventBean, CommandType.END_SESSION, responseEndSession);

                em.persist(operationEndSessionBean);
            }

            if (responseOffers.getOfferList() == null || responseOffers.getOfferList().getRecommendedOffers().size() == 0) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Nessuna offerta ricevuta da comunicare");

                userTransaction.commit();

                response = ResponseHelper.CRM_NOTIFY_EVENT_OFFERS_NOT_FOUND;
                return response;
            }
            
            
            System.out.println("CRM EVENT OFFER LIST: " + crmEventBean.getOfferBeanList().size());
            
            for (CRMOfferBean crmOfferBean : crmEventBean.getOfferBeanList()) {
                System.out.println("CRM OFFER NAME: " + crmOfferBean.getOfferName());
                if (crmOfferBean.getDescription().equalsIgnoreCase("VOUCHER")) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Trovata offerta voucher");
                    Double amount = 0.0;
                    String promoCode = "VCRM";
                    Boolean amountFound = Boolean.FALSE;
                    Boolean promoCodeFound = Boolean.FALSE;

                    for (CRMOfferParametersBean parametersBean : crmOfferBean.getCRMParametersBeanList()) {
                        if (parametersBean.getParameterName().equals("VoucherAmount")) {
                            if (parametersBean.getParameterValue() != null) {
                                amount = new Double((String) parametersBean.getParameterValue());
                                amountFound = Boolean.TRUE;
                            }
                        }
                        if (parametersBean.getParameterName().equals("VoucherPromoCode")) {
                            if (parametersBean.getParameterValue() != null) {
                                promoCode = (String) parametersBean.getParameterValue();
                                promoCodeFound = Boolean.TRUE;
                            }
                        }
                    }
                    
                    
                    if (amountFound == Boolean.TRUE && promoCodeFound == Boolean.TRUE && amount > 0.0 && !promoCode.isEmpty()) {
                    
                        CRMOfferVoucherPromotionalBean crmOfferVoucherPromotionalBean = new CRMOfferVoucherPromotionalBean();
                        crmOfferVoucherPromotionalBean.setReconciliationAttemptsLeft(maxRetryAttemps);
                        crmOfferVoucherPromotionalBean.setCrmOfferBean(crmOfferBean);
                        
                        CreateVoucherPromotional createVoucherPromotional = new CreateVoucherPromotional(em, fidelityService, userBean);
                        String createResponse = createVoucherPromotional.create(promoCode, amount, crmOfferVoucherPromotionalBean);
                        crmOfferVoucherPromotionalBean.setVoucherPromotionalBean(createVoucherPromotional.getVoucherPromotionalBean());
                        String associateResponse = null;
    
                        
                        if (createResponse.equals(ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_SUCCESS)) {
                            associateResponse = createVoucherPromotional.associateToUser(createVoucherPromotional.getCreateVoucherResult());
                        }
    
                        System.out.println("VoucherPromotionalBean: " + createVoucherPromotional.getVoucherPromotionalBean().toString());
                        System.out.println("eventBean.getOfferBeanList(): " + crmEventBean.getOfferBeanList().size());
    
                        for (CRMOfferBean offerBean : crmEventBean.getOfferBeanList()) {
                            System.out.println("offerBean: getOfferName(): " + offerBean.getOfferName() + "  -  getDescription(): " + offerBean.getDescription());
                            if (offerBean.getDescription().equalsIgnoreCase("VOUCHER")) {
                                crmOfferBean = offerBean;
                                break;
                            }
                        }
    
                        if (createResponse.equals(ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_CREATE_ERROR)
                                || associateResponse.equals(ResponseHelper.CREATE_VOUCHER_PROMOTIONAL_ASSIGN_ERROR)) {
                            crmOfferVoucherPromotionalBean.setVoucherPromotionalToReconcilie(true);
                        }
                        else {
                            crmOfferVoucherPromotionalBean.setVoucherPromotionalToReconcilie(false);
                        }
    
                        em.persist(crmOfferVoucherPromotionalBean);
    
                        crmOfferBean.setCrmOfferVoucherPromotionalBean(crmOfferVoucherPromotionalBean);
                        em.merge(crmOfferBean);
                    }
                }
            

                String arnEndpoint = userBean.getLastLoginDataBean().getDeviceEndpoint();
                PushNotificationContentType contentType = PushNotificationContentType.TEXT;
                PushNotificationStatusType statusCode = null;
                String statusMessage = null;
                Long userID = userBean.getId();
                String publishMessageID = null;
                Date sendingTimestamp = new Date();
                PushNotificationSourceType source = PushNotificationSourceType.CRM_INBOUND;
                String message = crmOfferBean.getOfferName()/* + ": " + offer.getDescription()*/;
                String title = crmOfferBean.getOfferName();

                PushNotificationBean pushNotificationBean = null;

                Calendar calendar = Calendar.getInstance();
                calendar.setTime(sendingTimestamp);
                calendar.add(Calendar.SECOND, expiryTime);

                pushNotificationBean = new PushNotificationBean();
                pushNotificationBean.setTitle(title);
                pushNotificationBean.setSendingTimestamp(sendingTimestamp);
                pushNotificationBean.setExpiringTimestamp(calendar.getTime());
                pushNotificationBean.setSource(source);
                pushNotificationBean.setContentType(contentType);
                pushNotificationBean.setUser(userBean);
                pushNotificationBean.setRetryAttemptsLeft(maxRetryAttemps);
                pushNotificationBean.setToReconcilie(false);

                em.persist(pushNotificationBean);

                crmEventBean.setPushNotificationBean(pushNotificationBean);
                em.merge(crmEventBean);

                PushNotificationMessage notificationMessage = new PushNotificationMessage();
                notificationMessage.setMessage(pushNotificationBean.getId(), message, title);

                if (arnEndpoint == null) {

                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "User arn endpoint not found");

                    statusCode = PushNotificationStatusType.ARN_NOT_FOUND;
                    statusMessage = "User arn endpoint not found";
                    
                    String updateNotificationResponse = eventNotificationService.updatePushNotication(pushNotificationBean.getId(), publishMessageID, arnEndpoint, title, message,
                            sendingTimestamp, source, contentType, statusCode, statusMessage, userID);

                    if (updateNotificationResponse.equals(ResponseHelper.SYSTEM_ERROR)) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error update record in PushNotication table");
                    }
                }
                else {
                    PushNotificationResult pushNotificationResult = pushNotificationService.publishMessage(arnEndpoint, notificationMessage, false, false);
                    publishMessageID = pushNotificationResult.getMessageId();
                    sendingTimestamp = pushNotificationResult.getRequestTimestamp();

                    if (pushNotificationResult != null && pushNotificationResult.getStatusCode() != null) {
                        if (pushNotificationResult.getStatusCode().equals(com.techedge.mp.pushnotification.adapter.interfaces.StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_SUCCESS)) {
                            statusCode = PushNotificationStatusType.DELIVERED;
                        }
                        else if (pushNotificationResult.getStatusCode().equals(com.techedge.mp.pushnotification.adapter.interfaces.StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_ARN_NOT_FOUND)) {
                            statusCode = PushNotificationStatusType.ARN_NOT_FOUND;
                        }
                        else {
                            statusCode = PushNotificationStatusType.ERROR;
                        }
                    }
                    else {
                        statusCode = PushNotificationStatusType.ERROR;
                    }

                    statusMessage = pushNotificationResult.getMessage();
                    String updateNotificationResponse = eventNotificationService.updatePushNotication(pushNotificationBean.getId(), publishMessageID, arnEndpoint, title, message,
                            sendingTimestamp, source, contentType, statusCode, statusMessage, userID);

                    if (updateNotificationResponse.equals(ResponseHelper.SYSTEM_ERROR)) {
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error update record in PushNotication table");
                    }
                }
            }
            
            // Attiva il flag che informa l'utente che � presente un aggiornamento nelle missioni
            Integer offerCount = crmEventBean.getOfferBeanList().size();
            if (offerCount > 0) {
                userBean.setUpdatedMission(Boolean.TRUE);
                em.merge(userBean);
            }

            userTransaction.commit();

            return ResponseHelper.CRM_NOTIFY_EVENT_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED sending crm notify update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }

}
