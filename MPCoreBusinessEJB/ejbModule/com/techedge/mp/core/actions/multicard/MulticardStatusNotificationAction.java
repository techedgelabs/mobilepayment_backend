package com.techedge.mp.core.actions.multicard;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.DpanStatus;
import com.techedge.mp.core.business.interfaces.DpanStatusEnum;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MulticardStatusNotificationResponse;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.PaymentInfoLogStatusBean;
import com.techedge.mp.core.business.model.ShopTransactionDataBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserExternalDataBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.DpanDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.GetMcCardStatusResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class MulticardStatusNotificationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public MulticardStatusNotificationAction() {}

    public MulticardStatusNotificationResponse execute(String operationID, Long requestTimestamp, String userID, ArrayList<DpanStatus> dpanStatus, UserCategoryService userCategoryService,
            CardInfoServiceRemote cardInfoService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        MulticardStatusNotificationResponse response = new MulticardStatusNotificationResponse();
        PaymentInfoLogStatusBean paymentInfoLogStatusBean = null;
        Date now = new Date();

        try {
            userTransaction.begin();

            if (operationID == null || (operationID != null && operationID.isEmpty())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "operationID is null");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_FAILURE);
                return response;
            }

            if (requestTimestamp == null || (requestTimestamp != null && requestTimestamp == 0)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "requestTimestamp is null");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_FAILURE);
                return response;
            }

            if (userID == null || (userID != null && userID.isEmpty())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "userID is null");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_FAILURE);
                return response;
            }

            if (dpanStatus == null || (dpanStatus != null && dpanStatus.isEmpty())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "dpanStatus is null");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_FAILURE);
                return response;
            }
            UserBean userBean = null;;

            String fiscalCode = "";
            String email = "";

            if (!userID.contains("@")) {
                fiscalCode = userID;
                userBean = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode);
            }
            else {
                email = userID;
                UserExternalDataBean userExternalDataBean = QueryRepository.findExternalUserByUUID(em, userID, "MULTICARD");
                if (userExternalDataBean != null) {
                    userBean = userExternalDataBean.getUserBean();
                }
            }

            if (userBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "user is not found or inactive");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_USER_NOT_FOUND);
                return response;
            }
            
            Boolean isMulticardFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.MULTICARD_FLOW.getCode());
            Boolean isNewFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());

            if (!isMulticardFlow && !isNewFlow) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "userType is not valid: " + userBean.getUserType());

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_FAILURE);
                return response;
            }
            
            
            Boolean processed     = false;
            
            Boolean defaultMethod = true;
            if (!userBean.getPaymentData().isEmpty()) {
                for (PaymentInfoBean paymentInfoBeanTemp : userBean.getPaymentData()) {
                    if (paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                        if (paymentInfoBeanTemp.getDefaultMethod() == true) {
                            defaultMethod = false;
                            break;
                        }
                    }
                }
            }

            if (defaultMethod == true) {
                System.out.println("Non � stato trovato un metodo di pagamento di default");
            }
            
            ShopTransactionDataBean shopTransactionDataBean = QueryRepository.findShopTransactionBeanDataByTransactionId(em, operationID);

            if (shopTransactionDataBean != null) {
                
                System.out.println("Find shopTransactionDataBean with id " + shopTransactionDataBean.getId());
                
                if (!fiscalCode.isEmpty()) {

                    if (fiscalCode.equals(shopTransactionDataBean.getPaymentInfo().getUser().getPersonalDataBean().getFiscalCode())
                            && shopTransactionDataBean.getPaymentInfo().getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING) {
                        
                        if (!dpanStatus.get(dpanStatus.size() - 1).getStatus().name().equals(DpanStatusEnum.PRE_ENROLLED.name())) {
                            
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "dpan status is not in pre-enrolled status, but in "
                                    + dpanStatus.get(dpanStatus.size() - 1).getStatus());

                            writeCheckMulticardPaymentLog(paymentInfoLogStatusBean, shopTransactionDataBean, null, operationID, now,
                                    ResponseHelper.MULTICARD_STATUS_NOTIFICATION_FAILURE,
                                    "dpan status is not in pre-enrolled status, but in " + dpanStatus.get(dpanStatus.size() - 1).getStatus());

                            userTransaction.commit();

                            response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_FAILURE);
                            return response;
                        }
                        
                        String newActiveDpan = dpanStatus.get(dpanStatus.size() - 1).getDPAN();
                        shopTransactionDataBean.getPaymentInfo().getUser().setActiveMcCardDpan(newActiveDpan);

                        paymentInfoLogStatusBean = new PaymentInfoLogStatusBean();
                        paymentInfoLogStatusBean.setDpanBefore(shopTransactionDataBean.getPaymentInfo().getPan());
                        paymentInfoLogStatusBean.setStatusBefore(shopTransactionDataBean.getPaymentInfo().getStatus());
                        paymentInfoLogStatusBean.setOperationID(operationID);

                        shopTransactionDataBean.getPaymentInfo().setStatus(mapStatusFromDpan(dpanStatus.get(dpanStatus.size() - 1).getStatus().name()));
                        shopTransactionDataBean.getPaymentInfo().setPan(dpanStatus.get(dpanStatus.size() - 1).getDPAN());
                        shopTransactionDataBean.getPaymentInfo().setToken(dpanStatus.get(dpanStatus.size() - 1).getDPAN());

                        if (defaultMethod == true) {
                            shopTransactionDataBean.getPaymentInfo().setDefaultMethod(defaultMethod);
                        }
                        paymentInfoLogStatusBean.setDpanAfter(shopTransactionDataBean.getPaymentInfo().getPan());
                        paymentInfoLogStatusBean.setStatusAfter(shopTransactionDataBean.getPaymentInfo().getStatus());
                        paymentInfoLogStatusBean.setPaymentInfo(shopTransactionDataBean.getPaymentInfo());
                        paymentInfoLogStatusBean.setTimestamp(now);
                        
                        processed = true;

                    }
                    else {
                        
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "fiscal code is not found or paymentinfo is not pending");
                        
                        writeCheckMulticardPaymentLog(paymentInfoLogStatusBean, shopTransactionDataBean, null, operationID, now,
                                ResponseHelper.MULTICARD_STATUS_NOTIFICATION_USER_NOT_FOUND, "fiscal code is not found or paymentinfo is not pending");

                        //userTransaction.commit();

                        //response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_USER_NOT_FOUND);
                        //return response;
                    }
                }
                else {
                    
                    // Ricerca utente multicard per email
                    UserBean userBeanFound = null;
                    
                    for (UserExternalDataBean userExternalDataBean : shopTransactionDataBean.getPaymentInfo().getUser().getUserExternalData()) {
                        if (userExternalDataBean.getUuid().equalsIgnoreCase(email)) {
                            userBeanFound = userExternalDataBean.getUserBean();
                        }
                    }
                    
                    if (userBeanFound != null && shopTransactionDataBean.getPaymentInfo().getUser().getId() == userBeanFound.getId()
                            && shopTransactionDataBean.getPaymentInfo().getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING) {
                        
                        if (!dpanStatus.get(dpanStatus.size() - 1).getStatus().name().equals(DpanStatusEnum.PRE_ENROLLED.name())) {
                            
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "dpan status is not in pre-enrolled status, but in "
                                    + dpanStatus.get(dpanStatus.size() - 1).getStatus());

                            writeCheckMulticardPaymentLog(paymentInfoLogStatusBean, shopTransactionDataBean, null, operationID, now,
                                    ResponseHelper.MULTICARD_STATUS_NOTIFICATION_FAILURE,
                                    "dpan status is not in pre-enrolled status, but in " + dpanStatus.get(dpanStatus.size() - 1).getStatus());

                            userTransaction.commit();

                            response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_FAILURE);
                            return response;
                        }
                        
                        String newActiveDpan = dpanStatus.get(dpanStatus.size() - 1).getDPAN();
                        shopTransactionDataBean.getPaymentInfo().getUser().setActiveMcCardDpan(newActiveDpan);

                        paymentInfoLogStatusBean = new PaymentInfoLogStatusBean();
                        paymentInfoLogStatusBean.setDpanBefore(shopTransactionDataBean.getPaymentInfo().getPan());
                        paymentInfoLogStatusBean.setStatusBefore(shopTransactionDataBean.getPaymentInfo().getStatus());
                        paymentInfoLogStatusBean.setOperationID(operationID);
                        shopTransactionDataBean.getPaymentInfo().setStatus(mapStatusFromDpan(dpanStatus.get(dpanStatus.size() - 1).getStatus().name()));
                        shopTransactionDataBean.getPaymentInfo().setPan(dpanStatus.get(dpanStatus.size() - 1).getDPAN());
                        shopTransactionDataBean.getPaymentInfo().setToken(dpanStatus.get(dpanStatus.size() - 1).getDPAN());
                        if (defaultMethod == true) {
                            shopTransactionDataBean.getPaymentInfo().setDefaultMethod(defaultMethod);
                        }
                        paymentInfoLogStatusBean.setDpanAfter(shopTransactionDataBean.getPaymentInfo().getPan());
                        paymentInfoLogStatusBean.setStatusAfter(shopTransactionDataBean.getPaymentInfo().getStatus());
                        paymentInfoLogStatusBean.setPaymentInfo(shopTransactionDataBean.getPaymentInfo());
                        paymentInfoLogStatusBean.setTimestamp(now);

                        processed = true;
                    }
                    else {
                         
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "email is not found or paymentinfo is not pending");

                        writeCheckMulticardPaymentLog(paymentInfoLogStatusBean, shopTransactionDataBean, null, operationID, now,
                                ResponseHelper.MULTICARD_STATUS_NOTIFICATION_USER_NOT_FOUND, "email is not found or paymentinfo is not pending");

                        //userTransaction.commit();

                        //response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_USER_NOT_FOUND);
                        //return response;
                    }
                }
            }
            
            if (!processed) {
                
                System.out.println("shopTransactionDataBean not found or not PRE_ENROLLED event");
                
                // Aggiorna lo stato delle multicard dell'utente con le informazioni ricevute dal servizio
                List<PaymentInfoBean> multicardPaymentInfoList = QueryRepository.findMulticardPaymentInfoListByUserBean(em, userBean);
                
                DpanStatus dpanStatusActivated = null;
                for (DpanStatus dpanStatusElement : dpanStatus) {
                    Integer newStatus = mapStatusFromDpan(dpanStatusElement.getStatus().name());
                    
                    if (newStatus == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED) {
                        
                        System.out.println("Trovata multicard da associare");
                        
                        // Ricerca la multicard da aggiornare
                        // - in stato PENDING
                        Boolean found = Boolean.FALSE;
                        
                        System.out.println("Ricerca la multicard pi� recente in stato PENDING");
                        
                        for(PaymentInfoBean multicardPaymentInfo : multicardPaymentInfoList) {
                            
                            //System.out.println("Verifica stato multicard " + multicardPaymentInfo.getId() + " dpan: " + multicardPaymentInfo.getToken() + " status: " + multicardPaymentInfo.getStatus());
                            
                            if (multicardPaymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_PENDING) {
                                
                                Integer oldStatus = multicardPaymentInfo.getStatus();
                                System.out.println("Stato backend: " + oldStatus);
                                System.out.println("Stato multicard: " + newStatus);
                                
                                multicardPaymentInfo.setStatus(newStatus);
                                multicardPaymentInfo.setDefaultMethod(defaultMethod);
                                multicardPaymentInfo.setStatus(PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED);
                                multicardPaymentInfo.setPan(dpanStatusElement.getDPAN());
                                multicardPaymentInfo.setToken(dpanStatusElement.getDPAN());
                                
                                paymentInfoLogStatusBean = new PaymentInfoLogStatusBean();
                                paymentInfoLogStatusBean.setDpanBefore(multicardPaymentInfo.getToken());
                                paymentInfoLogStatusBean.setStatusBefore(oldStatus);
                                paymentInfoLogStatusBean.setOperationID(operationID);
                                paymentInfoLogStatusBean.setDpanAfter(multicardPaymentInfo.getToken());
                                paymentInfoLogStatusBean.setStatusAfter(newStatus);
                                paymentInfoLogStatusBean.setPaymentInfo(multicardPaymentInfo);
                                paymentInfoLogStatusBean.setTimestamp(now);
                                paymentInfoLogStatusBean.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_SUCCESS);
                                paymentInfoLogStatusBean.setMessage("OK");
                                em.persist(paymentInfoLogStatusBean);
                                
                                System.out.println("Associazione multicard: 0 -> 1");
                                found = Boolean.TRUE;
                                
                                break;
                            }
                        }
                        
                        if (!found) {
                            
                            System.out.println("Non trovato");
                            
                            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Unable to associate multicard: dpan " + dpanStatusElement.getDPAN() + " not found");

                            userTransaction.commit();

                            response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_DPAN_NOT_FOUND);
                            return response;
                        }
                    }
                    else {
                        
                        // Recupera la multicard che si trova in stato attivo, se presente
                        if (newStatus == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) {
                            
                            System.out.println("Trovata multicard da attivare");
                            
                            // Ricerca la multicard da aggiornare
                            // - in stato PRE_ENROLLED
                            // - in stato diverso da PENDING e da PRE_ENROLLED
                            Boolean found = Boolean.FALSE;
                            
                            System.out.println("Ricerca multicard in stato PRE_ENROLLED");
                            
                            for(PaymentInfoBean multicardPaymentInfo : multicardPaymentInfoList) {
                                
                                //System.out.println("Verifica stato multicard " + multicardPaymentInfo.getId() + " dpan: " + multicardPaymentInfo.getToken() + " status: " + multicardPaymentInfo.getStatus());
                                
                                if (multicardPaymentInfo.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED &&
                                    multicardPaymentInfo.getToken().equals(dpanStatusElement.getDPAN())) {
                                    
                                    Integer oldStatus = multicardPaymentInfo.getStatus();
                                    System.out.println("Stato backend: " + oldStatus);
                                    System.out.println("Stato multicard: " + newStatus);
                                    
                                    multicardPaymentInfo.setStatus(newStatus);
                                    multicardPaymentInfo.setDefaultMethod(defaultMethod);
                                    
                                    String newActiveDpan = dpanStatusElement.getDPAN();
                                    userBean.setActiveMcCardDpan(newActiveDpan);
                                    
                                    paymentInfoLogStatusBean = new PaymentInfoLogStatusBean();
                                    paymentInfoLogStatusBean.setDpanBefore(multicardPaymentInfo.getToken());
                                    paymentInfoLogStatusBean.setStatusBefore(oldStatus);
                                    paymentInfoLogStatusBean.setOperationID(operationID);
                                    paymentInfoLogStatusBean.setDpanAfter(multicardPaymentInfo.getToken());
                                    paymentInfoLogStatusBean.setStatusAfter(newStatus);
                                    paymentInfoLogStatusBean.setPaymentInfo(multicardPaymentInfo);
                                    paymentInfoLogStatusBean.setTimestamp(now);
                                    paymentInfoLogStatusBean.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_SUCCESS);
                                    paymentInfoLogStatusBean.setMessage("OK");
                                    em.persist(paymentInfoLogStatusBean);
                                    
                                    System.out.println("Attivazione multicard: 1 -> 2");
                                    found = Boolean.TRUE;
                                }
                            }
                            
                            if (!found) {
                                
                                System.out.println("Non trovato");
                                
                                System.out.println("Ricerca multicard in stato diverso da PRE_ENROLLED e PENDING");
                                
                                for(PaymentInfoBean multicardPaymentInfo : multicardPaymentInfoList) {
                                    
                                    //System.out.println("Verifica stato multicard " + multicardPaymentInfo.getId() + " dpan: " + multicardPaymentInfo.getToken() + " status: " + multicardPaymentInfo.getStatus());
                                    
                                    if (multicardPaymentInfo.getToken() != null && multicardPaymentInfo.getToken().equals(dpanStatusElement.getDPAN()) &&
                                        multicardPaymentInfo.getStatus() != PaymentInfo.PAYMENTINFO_STATUS_PENDING) {
                                        
                                        Integer oldStatus = multicardPaymentInfo.getStatus();
                                        System.out.println("Stato backend: " + oldStatus);
                                        System.out.println("Stato multicard: " + newStatus);
                                        
                                        multicardPaymentInfo.setStatus(newStatus);
                                        
                                        String newActiveDpan = dpanStatusElement.getDPAN();
                                        userBean.setActiveMcCardDpan(newActiveDpan);
                                        
                                        paymentInfoLogStatusBean = new PaymentInfoLogStatusBean();
                                        paymentInfoLogStatusBean.setDpanBefore(multicardPaymentInfo.getToken());
                                        paymentInfoLogStatusBean.setStatusBefore(oldStatus);
                                        paymentInfoLogStatusBean.setOperationID(operationID);
                                        paymentInfoLogStatusBean.setDpanAfter(multicardPaymentInfo.getToken());
                                        paymentInfoLogStatusBean.setStatusAfter(newStatus);
                                        paymentInfoLogStatusBean.setPaymentInfo(multicardPaymentInfo);
                                        paymentInfoLogStatusBean.setTimestamp(now);
                                        paymentInfoLogStatusBean.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_SUCCESS);
                                        paymentInfoLogStatusBean.setMessage("OK");
                                        em.persist(paymentInfoLogStatusBean);
                                        
                                        System.out.println("Rittivazione multicard: " + oldStatus + " -> " + PaymentInfo.PAYMENTINFO_STATUS_VERIFIED);
                                        found = Boolean.TRUE;
                                    }
                                }
                            }
                            
                            if (!found) {
                                
                                System.out.println("Non trovato");
                                
                                // Crea un nuovo PaymentInfo in stato ACTIVATED
                                PaymentInfoBean multicardPaymentInfo = new PaymentInfoBean();
                                
                                multicardPaymentInfo.setAttemptsLeft(0);
                                multicardPaymentInfo.setBrand(null);
                                multicardPaymentInfo.setCardBin("");
                                multicardPaymentInfo.setCheckAmount(0.0);
                                multicardPaymentInfo.setCheckBankTransactionID(null);
                                multicardPaymentInfo.setCheckCurrency(null);
                                multicardPaymentInfo.setCheckShopLogin(null);
                                multicardPaymentInfo.setCheckShopTransactionID(null);
                                multicardPaymentInfo.setDefaultMethod(defaultMethod);
                                multicardPaymentInfo.setExpirationDate(null);
                                multicardPaymentInfo.setHash(null);
                                multicardPaymentInfo.setInsertTimestamp(now);
                                multicardPaymentInfo.setMessage(null);
                                multicardPaymentInfo.setPan(dpanStatusElement.getDPAN());
                                multicardPaymentInfo.setPin(null);
                                multicardPaymentInfo.setPinCheckAttemptsLeft(null);
                                multicardPaymentInfo.setStatus(newStatus);
                                multicardPaymentInfo.setToken(dpanStatusElement.getDPAN());
                                multicardPaymentInfo.setToken3ds(null);
                                multicardPaymentInfo.setType(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);
                                multicardPaymentInfo.setUser(userBean);
                                multicardPaymentInfo.setVerifiedTimestamp(null);
                                em.persist(multicardPaymentInfo);
                                
                                userBean.getPaymentData().add(multicardPaymentInfo);
                                
                                String newActiveDpan = dpanStatusElement.getDPAN();
                                userBean.setActiveMcCardDpan(newActiveDpan);
                                
                                paymentInfoLogStatusBean = new PaymentInfoLogStatusBean();
                                paymentInfoLogStatusBean.setDpanBefore(multicardPaymentInfo.getToken());
                                paymentInfoLogStatusBean.setStatusBefore(null);
                                paymentInfoLogStatusBean.setOperationID(operationID);
                                paymentInfoLogStatusBean.setDpanAfter(multicardPaymentInfo.getToken());
                                paymentInfoLogStatusBean.setStatusAfter(newStatus);
                                paymentInfoLogStatusBean.setPaymentInfo(multicardPaymentInfo);
                                paymentInfoLogStatusBean.setTimestamp(now);
                                paymentInfoLogStatusBean.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_SUCCESS);
                                paymentInfoLogStatusBean.setMessage("OK");
                                em.persist(paymentInfoLogStatusBean);
                                
                                System.out.println("Attivazione multicard per cambio device: null -> " + PaymentInfo.PAYMENTINFO_STATUS_VERIFIED);
                                found = Boolean.TRUE;
                            }
                        }
                        else {
                            
                            if (newStatus == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED) {
                                
                                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Errore: il passaggio di stato in PRE_ENROLLED non � gestito in questo step");
    
                                userTransaction.commit();
    
                                response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_FAILURE);
                                return response;
                            }
                            
                            // Aggiorna gli stati di tutte le altre multicard
                            for(PaymentInfoBean multicardPaymentInfo : multicardPaymentInfoList) {
                                
                                if (multicardPaymentInfo.getToken() != null &&
                                    multicardPaymentInfo.getToken().equals(dpanStatusElement.getDPAN())) {
                                    
                                    //System.out.println("Verifica stato multicard " + multicardPaymentInfo.getId() + " dpan: " + multicardPaymentInfo.getToken() + " status: " + multicardPaymentInfo.getStatus());
                                    
                                    Integer oldStatus = multicardPaymentInfo.getStatus();
                                    //System.out.println("Stato backend: " + oldStatus);
                                    //System.out.println("Stato multicard: " + newStatus);
                                    
                                    multicardPaymentInfo.setStatus(newStatus);
                                    
                                    if (newStatus == PaymentInfo.PAYMENTINFO_STATUS_BLOCKED ||
                                        newStatus == PaymentInfo.PAYMENTINFO_STATUS_CANCELED ||
                                        newStatus == PaymentInfo.PAYMENTINFO_STATUS_ERROR) {
                                        
                                        multicardPaymentInfo.setDefaultMethod(Boolean.FALSE);
                                    }
                                    
                                    paymentInfoLogStatusBean = new PaymentInfoLogStatusBean();
                                    paymentInfoLogStatusBean.setDpanBefore(multicardPaymentInfo.getToken());
                                    paymentInfoLogStatusBean.setStatusBefore(oldStatus);
                                    paymentInfoLogStatusBean.setOperationID(operationID);
                                    paymentInfoLogStatusBean.setDpanAfter(multicardPaymentInfo.getToken());
                                    paymentInfoLogStatusBean.setStatusAfter(newStatus);
                                    paymentInfoLogStatusBean.setPaymentInfo(multicardPaymentInfo);
                                    paymentInfoLogStatusBean.setTimestamp(now);
                                    paymentInfoLogStatusBean.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_SUCCESS);
                                    paymentInfoLogStatusBean.setMessage("OK");
                                    em.persist(paymentInfoLogStatusBean);
                                    
                                    System.out.println("Aggiornamento stati multicard: " + oldStatus + " -> " + newStatus);
                                }
                            }
                        }
                    }
                }
            }
            
            
            Boolean defaultMethodFound = false;
            if (!userBean.getPaymentData().isEmpty()) {
                for (PaymentInfoBean paymentInfoBeanTemp : userBean.getPaymentData()) {
                    if (paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                        if (paymentInfoBeanTemp.getDefaultMethod() == true) {
                            defaultMethodFound = true;
                            break;
                        }
                    }
                }
            }
            
            if (defaultMethodFound == false) {
                
                System.out.println("Non esiste un medoto di pagamento di default");
                
                // Se non � stato definito un metodo di pagamento di default e esiste almeno un metodo di pagamento in stato 1 o 2 di tipo credit_card o multicard deve essere impostato come di default
                
                for(PaymentInfoBean paymentInfoBean : userBean.getPaymentData()) {
                    
                    System.out.println("Id " + paymentInfoBean.getId() + " type: " + paymentInfoBean.getType() + " status: " + paymentInfoBean.getStatus());
                    
                    if ((paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED || paymentInfoBean.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED) &&
                        (paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || paymentInfoBean.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD))) {
                        
                        paymentInfoBean.setDefaultMethod(Boolean.TRUE);
                        
                        System.out.println("Il nuovo medoto di pagamento di default ha id " + paymentInfoBean.getId());
                        break;
                    }
                }
            }
            
            
            
            
            if (userBean.getDepositCardStepCompleted() == null || !userBean.getDepositCardStepCompleted()) {
                userBean.setDepositCardStepCompleted(true);
                userBean.setDepositCardStepTimestamp(now);
            }
            
            if (isMulticardFlow) {
                if (userBean.getUserStatusRegistrationCompleted() == null || !userBean.getUserStatusRegistrationCompleted()) {
                    userBean.setUserStatusRegistrationCompleted(true);
                    userBean.setUserStatusRegistrationTimestamp(now);
                }
            }
             
            em.merge(userBean);

            response.setOperationID(operationID);
            response.setStatusCode(ResponseHelper.MULTICARD_STATUS_NOTIFICATION_SUCCESS);
            userTransaction.commit();
            
            
            userTransaction.begin();
            
            // Allineamento dpan carte
            System.out.println("Allineamento dpan carte");
            
            GetMcCardStatusResult getMcCardStatusResult = new GetMcCardStatusResult();

            try {
                PartnerType partnerType = PartnerType.EM;

                String operationId      = new IdGenerator().generateId(16).substring(0, 32);
                String userId           = userID;
                Long csRequestTimestamp = now.getTime();
                
                getMcCardStatusResult = cardInfoService.getMcCardStatus(operationId, userId, partnerType, csRequestTimestamp);
            }
            catch (Exception ex) {
                getMcCardStatusResult.setStatus("ERROR");
            }

            if (getMcCardStatusResult.getStatus() != null) {
                
                List<PaymentInfoBean> multicardPaymentInfoList = QueryRepository.findMulticardPaymentInfoListByUserBean(em, userBean);
                
                /*
                testGetMcCardStatusResponse.set_status(getMcCardStatusResult.getStatus());
                testGetMcCardStatusResponse.setCode(getMcCardStatusResult.getCode());
                testGetMcCardStatusResponse.setMessage(getMcCardStatusResult.getMessage());
                testGetMcCardStatusResponse.setTransactionId(getMcCardStatusResult.getTransactionId());
                for (DpanDetail dpanDetail : getMcCardStatusResult.getDpanDetailList()) {
                    testGetMcCardStatusResponse.getDpanDetailList().add(dpanDetail);
                }
                */
                if (getMcCardStatusResult.getStatus().equals("SUCCESS")) {
                    
                    //System.out.println("getMcCardStatusResult SUCCESS");
                    
                    for (DpanDetail dpanDetail : getMcCardStatusResult.getDpanDetailList()) {
                        
                        //System.out.println("Elaborazione dpan " + dpanDetail.getMcCardDpan());
                        
                        for(PaymentInfoBean paymentInfoBean : multicardPaymentInfoList) {
                            
                            //System.out.println("Verifica dpan " + paymentInfoBean.getToken());
                            
                            if (paymentInfoBean.getToken() != null && paymentInfoBean.getToken().equals(dpanDetail.getMcCardDpan())) {
                                
                                //System.out.println("Update pan " + dpanDetail.getMcCardPan());
                                
                                paymentInfoBean.setPan(dpanDetail.getMcCardPan());
                                em.merge(paymentInfoBean);
                            }
                        }
                    }
                }
                else {
                    
                    System.out.println("getMcCardStatus Error");
                }
                
            }
            
            userTransaction.commit();

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED check authorization with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }

    private void writeCheckMulticardPaymentLog(PaymentInfoLogStatusBean paymentInfoLogStatusBean, ShopTransactionDataBean shopTransactionDataBean, PaymentInfoBean paymentInfoBean,
            String operationID, Date now, String statusCode, String message) {
        if (shopTransactionDataBean != null) {

            paymentInfoLogStatusBean = new PaymentInfoLogStatusBean();
            paymentInfoLogStatusBean.setDpanBefore(shopTransactionDataBean.getPaymentInfo().getPan());
            paymentInfoLogStatusBean.setStatusBefore(shopTransactionDataBean.getPaymentInfo().getStatus());
            paymentInfoLogStatusBean.setOperationID(operationID);
            paymentInfoLogStatusBean.setDpanAfter(shopTransactionDataBean.getPaymentInfo().getPan());
            paymentInfoLogStatusBean.setStatusAfter(shopTransactionDataBean.getPaymentInfo().getStatus());
            paymentInfoLogStatusBean.setPaymentInfo(shopTransactionDataBean.getPaymentInfo());
            paymentInfoLogStatusBean.setTimestamp(now);
            paymentInfoLogStatusBean.setStatusCode(statusCode);
            paymentInfoLogStatusBean.setMessage(message);
            em.persist(paymentInfoLogStatusBean);
        }
        else if (paymentInfoBean != null) {

            paymentInfoLogStatusBean = new PaymentInfoLogStatusBean();
            paymentInfoLogStatusBean.setDpanBefore(paymentInfoBean.getPan());
            paymentInfoLogStatusBean.setStatusBefore(paymentInfoBean.getStatus());
            paymentInfoLogStatusBean.setOperationID(operationID);
            paymentInfoLogStatusBean.setDpanAfter(paymentInfoBean.getPan());
            paymentInfoLogStatusBean.setStatusAfter(paymentInfoBean.getStatus());
            paymentInfoLogStatusBean.setPaymentInfo(paymentInfoBean);
            paymentInfoLogStatusBean.setTimestamp(now);
            paymentInfoLogStatusBean.setStatusCode(statusCode);
            paymentInfoLogStatusBean.setMessage(message);
            em.persist(paymentInfoLogStatusBean);
        }

    }

    private Integer mapStatusFromDpan(String status) {
        if (status.equals(DpanStatusEnum.PRE_ENROLLED.name())) {
            return PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED;
        }
        else if (status.equals(DpanStatusEnum.ACTIVATED.name())) {
            return PaymentInfo.PAYMENTINFO_STATUS_VERIFIED;
        }
        else if (status.equals(DpanStatusEnum.CANCELLED.name())) {
            return PaymentInfo.PAYMENTINFO_STATUS_CANCELED;
        }
        else if (status.equals(DpanStatusEnum.BLOCKED.name())) {
            return PaymentInfo.PAYMENTINFO_STATUS_BLOCKED;
        }
        else if (status.equals(DpanStatusEnum.REASSIGNED.name())) {
            return PaymentInfo.PAYMENTINFO_STATUS_CANCELED;
        }
        else {
            return PaymentInfo.PAYMENTINFO_STATUS_CANCELED;
        }
    }
}
