package com.techedge.mp.core.actions.multicard;

import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.MulticardPaymentResponse;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.ShopTransactionDataBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class InsertMulticardPaymentMethodAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public InsertMulticardPaymentMethodAction() {}

    public MulticardPaymentResponse execute(String ticketId, String requestId, String paymentMethodType, Integer pinCheckMaxAttempts, UserCategoryService userCategoryService)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            MulticardPaymentResponse managePaymentResponse = null;

            // Verifica il ticket
            TicketBean ticketBean = QueryRepository.findTicketById(em, ticketId);

            if (ticketBean == null || !ticketBean.isValid() || !ticketBean.isCustomerTicket()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                managePaymentResponse = new MulticardPaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_INVALID_TICKET);
                return managePaymentResponse;
            }

            UserBean userBean = ticketBean.getUser();
            if (userBean == null) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                managePaymentResponse = new MulticardPaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_INVALID_TICKET);
                return managePaymentResponse;
            }

            // Verifica lo stato dell'utente
            Integer userStatus = userBean.getUserStatus();
            if (userStatus != User.USER_STATUS_VERIFIED) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to manage payment in status " + userStatus);

                userTransaction.commit();

                managePaymentResponse = new MulticardPaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_MULTICARD_PAYMENT_METHOD_UNAUTHORIZED);
                return managePaymentResponse;
            }

            Boolean isNewAcquirerFlow = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            Boolean isMulticardFlow   = userCategoryService.isUserTypeInUserCategory(userBean.getUserType(), UserCategoryType.MULTICARD_FLOW.getCode());
            
            if (!isNewAcquirerFlow && !isMulticardFlow) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User category is not valid");

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to manage payment with type " + userBean.getUserType());

                userTransaction.commit();

                managePaymentResponse = new MulticardPaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_MULTICARD_PAYMENT_METHOD_UNAUTHORIZED);
                return managePaymentResponse;
            }

            /*
            for (PaymentInfoBean item : userBean.getPaymentData()) {
                
                System.out.println("Ricerca metodi di pagamento gi� associati   id: " + item.getId() + ", status: " + item.getStatus() + ", type: " + item.getType());

                if (item.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)
                        && (item.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_VERIFIED || item.getStatus() == PaymentInfo.PAYMENTINFO_STATUS_NOT_VERIFIED)) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Already exists multicard payment method");

                    userTransaction.commit();

                    managePaymentResponse = new MulticardPaymentResponse();
                    managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_MULTICARD_PAYMENT_METHOD_EXISTS);
                    return managePaymentResponse;
                }
            }
            */

            // Genera uno shopTransactionID
            String shopTransactionId = new IdGenerator().generateId(16).substring(0, 33);

            // Switch implementazione per utenti che eseguono il nuovo flusso voucher

            PaymentInfoBean paymentInfoBean = null;

            if (!paymentMethodType.equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment method is not multicard");

                userTransaction.commit();

                managePaymentResponse = new MulticardPaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_MULTICARD_PAYMENT_METHOD_UNAUTHORIZED);
                return managePaymentResponse;
            }

            System.out.println("Inserimento multicard");

            /*
            String encodedPin = userBean.getEncodedPin();

            if (encodedPin == null || encodedPin.equals("")) {

                // Errore Pin non ancora inserito
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Pin not inserted");

                userTransaction.commit();
                managePaymentResponse = new MulticardPaymentResponse();
                managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_PAYMENT_METHOD_UNAUTHORIZED);
                return managePaymentResponse;
            }
            */
            
            
            Boolean defaultMethod = true;
            if (!userBean.getPaymentData().isEmpty()) {
                for (PaymentInfoBean paymentInfoBeanTemp : userBean.getPaymentData()) {
                    if (paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD) || paymentInfoBeanTemp.getType().equals(PaymentInfo.PAYMENTINFO_TYPE_MULTICARD)) {
                        if (paymentInfoBeanTemp.getDefaultMethod() == true) {
                            defaultMethod = false;
                            break;
                        }
                    }
                }
            }

            if (defaultMethod == true) {
                System.out.println("Non � stato trovato un metodo di pagamento di default");
            }
            
            paymentInfoBean = userBean.addNewPendingPaymentInfo(paymentMethodType, null, defaultMethod, pinCheckMaxAttempts, null, null, null, null, shopTransactionId);

            System.out.println("Salvataggio nuovo metodo di pagamento multicard");

            // Memorizza su db l'informazione sulla carta
            em.persist(paymentInfoBean);
            em.merge(userBean);

            // Registra su db l'associazione tra lo shopTransactionID e l'utente
            ShopTransactionDataBean shopTransactionData = ShopTransactionDataBean.createNewShopTransactionDataBean(shopTransactionId, paymentInfoBean);

            em.merge(shopTransactionData);

            // Rinnova il ticket
            //logger.log(Level.INFO, "Rinnova il ticket");
            ticketBean.renew();
            em.merge(ticketBean);

            userTransaction.commit();

            managePaymentResponse = new MulticardPaymentResponse();
            managePaymentResponse.setStatusCode(ResponseHelper.USER_INSERT_MULTICARD_PAYMENT_METHOD_SUCCESS);
            managePaymentResponse.setPaymentMethodId(paymentInfoBean.getId());
            managePaymentResponse.setPaymentMethodType(paymentInfoBean.getType());
            managePaymentResponse.setOperationID(shopTransactionData.getTransactionId());

            return managePaymentResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED insert multicard payment method with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
