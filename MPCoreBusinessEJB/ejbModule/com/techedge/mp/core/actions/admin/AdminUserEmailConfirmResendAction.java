package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.interfaces.user.UserCategoryType;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VerificationCodeBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.core.business.utilities.QueryRepositoryBusiness;
import com.techedge.mp.core.business.utilities.StringSubstitution;
import com.techedge.mp.email.sender.business.EmailSenderRemote;
import com.techedge.mp.email.sender.business.interfaces.EmailType;
import com.techedge.mp.email.sender.business.interfaces.Parameter;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUserEmailConfirmResendAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminUserEmailConfirmResendAction() {}

    public String execute(String adminTicketId, String requestId, String userEmail, String activationLink, String category, EmailSenderRemote emailSender, 
            UserCategoryService userCategoryService, StringSubstitution stringSubstitution) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            String response = null;

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_RESEND_CONFIRM_USER_EMAIL_FAILURE;

                return response;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }
            
            UserBean userBean = null;
            
            if (category.equals("BUSINESS")) {
                userBean = QueryRepositoryBusiness.findNotCancelledUserBusinessByEmail(em_crud, userEmail);
            }
            else {
                userBean = QueryRepository.findNotCancelledUserCustomerByEmail(em_crud, userEmail);
            }

            if (userBean == null) {

                // Utente non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_RESEND_CONFIRM_USER_EMAIL_FAILURE;

                return response;
            }

            if (userBean.getUserStatus() != User.USER_STATUS_NEW) {

                // Utente in stato non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User in status not valid");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_RESEND_CONFIRM_USER_EMAIL_FAILURE;

                return response;
            }

            // Invia l'email con il codice di attivazione
            Integer userType = userBean.getUserType();
            Boolean useNewAcquirer = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.NEW_ACQUIRER_FLOW.getCode());
            Boolean useBusiness    = userCategoryService.isUserTypeInUserCategory(userType, UserCategoryType.BUSINESS.getCode());
            EmailType emailType = EmailType.CONFIRM_EMAIL;
            
            if (useNewAcquirer) {
                emailType = EmailType.CONFIRM_EMAIL_V2;
            }
            
            if (useBusiness) {
                emailType = EmailType.CONFIRM_EMAIL_BUSINESS;
            }

            String keyFrom = emailSender.getSender();
            
            if (stringSubstitution != null) {
                keyFrom = stringSubstitution.getValue(keyFrom, 1);
            }
            
            System.out.println("keyFrom: " + keyFrom);
            
            String to = userBean.getPersonalDataBean().getSecurityDataEmail();
            List<Parameter> parameters = new ArrayList<Parameter>(0);

            VerificationCodeBean verificationCodeBean = QueryRepository.findVerificationCodeBeanByUserAndStatus(em_crud, userBean, VerificationCodeBean.STATUS_NEW);

            if (verificationCodeBean == null) {

                // Codice di verifica non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Verification code not found");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_RESEND_CONFIRM_USER_EMAIL_FAILURE;

                return response;
            }

            parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
            parameters.add(new Parameter("ACTIVATION_LINK", activationLink));
            parameters.add(new Parameter("VERIFICATION_CODE", verificationCodeBean.getVerificationCodeId()));
            parameters.add(new Parameter("EMAIL", userBean.getPersonalDataBean().getSecurityDataEmail()));

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Sending email to "
                    + userBean.getPersonalDataBean().getSecurityDataEmail());

            String result = emailSender.sendEmail(emailType, keyFrom, to, parameters);

            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "SendEmail result: " + result);

            userTransaction.commit();

            response = ResponseHelper.ADMIN_RESEND_CONFIRM_USER_EMAIL_SUCCESS;

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin update user with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
