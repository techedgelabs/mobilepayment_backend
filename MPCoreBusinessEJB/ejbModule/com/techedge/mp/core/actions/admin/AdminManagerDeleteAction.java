package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.Manager;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.ManagerBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminManagerDeleteAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @EJB
    private LoggerService loggerService;

    public AdminManagerDeleteAction() {}

    public String execute(String adminTicketId, String requestId, Manager manager) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            String response = null;

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_DELETE_INVALID_TICKET;

                return response;
            }
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }
            if (manager != null && manager.getId() != 0) {

                ManagerBean managerBean = QueryRepository.findManagerById(em_crud, manager.getId());

                if (managerBean == null) {

                    // Manager non trovato
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Manager not found");

                    userTransaction.commit();

                    response = ResponseHelper.ADMIN_DELETE_MANAGER_USER_NOT_FOUND;

                    return response;
                }

                em_crud.remove(managerBean);

            }
            else {

                // Manager non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Manager not found");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_DELETE_MANAGER_INVALID_PARAMETERS;

                return response;
            }

            userTransaction.commit();

            response = ResponseHelper.ADMIN_DELETE_MANAGER_SUCCESS;

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED manager delete user with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
