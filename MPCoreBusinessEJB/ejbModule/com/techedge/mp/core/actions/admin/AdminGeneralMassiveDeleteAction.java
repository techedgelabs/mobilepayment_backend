package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.AdminService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminGeneralMassiveDeleteAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminGeneralMassiveDeleteAction() {}

    public String execute(String adminTicketId, String requestID, String type, String idFrom, String idTo, AdminService adminService) throws EJBException, ClassNotFoundException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_MASSIVE_REMOVE_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            Long longIdFrom = Long.parseLong(idFrom);
            Long longIdTo   = Long.parseLong(idTo);
            
            if (longIdTo < longIdFrom) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ID (idTo < idFrom)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_MASSIVE_REMOVE_FAILURE;
            }
            /*
            for (Long i = Long.parseLong(idFrom); i <= Long.parseLong(idTo); i++) {
                
                adminService.adminGeneralDelete(adminTicketId, requestID, type, i.toString());
                
                System.out.println("Eliminato oggetto di tipo " + type + " con ID = " + i);
            }
            */
            
            Query deleteQuery = em.createQuery("DELETE FROM " + type + " e WHERE e.id >= :idFrom AND e.id <= :idTo");
            deleteQuery.setParameter("idFrom", longIdFrom);
            deleteQuery.setParameter("idTo",   longIdTo);
            int result = deleteQuery.executeUpdate();
            
            System.out.println("Deleted " + result + " rows from table " + type);
            
            userTransaction.commit();

            return ResponseHelper.ADMIN_MASSIVE_REMOVE_SUCCESS;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            
            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }

    }
}