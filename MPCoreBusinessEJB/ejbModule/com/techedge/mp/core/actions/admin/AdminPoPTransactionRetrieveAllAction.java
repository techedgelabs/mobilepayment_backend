package com.techedge.mp.core.actions.admin;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PostPaidTransactionHistory;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrievePoPTransactionListData;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminPoPTransactionRetrieveAllAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminPoPTransactionRetrieveAllAction() {}

    public RetrievePoPTransactionListData execute(String adminTicketId, String requestId, String mpTransactionId, Long userId, String stationId, String sourceId,
            String mpTransactionStatus, Boolean notificationCreated, Boolean notificationPaid, Boolean notificationUser, Date creationTimestampStart, Date creationTimestampEnd,
            Integer maxResults, Boolean transactionHistoryFlag) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            RetrievePoPTransactionListData retrievePoPTransactionListData = new RetrievePoPTransactionListData();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                retrievePoPTransactionListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_POP_TRANSACTION_LIST_INVALID_TICKET);

                return retrievePoPTransactionListData;
            }
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                retrievePoPTransactionListData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return retrievePoPTransactionListData;
            }
            userTransaction.commit();

            // Se viene il parametro mpTransactionID viene effettuata una ricerca mirata
            if (mpTransactionId != null) {

                System.out.println("Ricerca per mpTransactionID");

                userTransaction.begin();

                PostPaidTransactionBean postPaidTransactionBean = QueryRepository.findPostPaidTransactionBeanByMPId(em_crud, mpTransactionId);

                List<PostPaidTransactionHistory> postPaidTransactionHistoryList = new ArrayList<PostPaidTransactionHistory>(0);

                if (postPaidTransactionBean != null) {

                    // Se i dati della transazione estratta corrispondono ai parametri di input, allora si inserisce nel risultato
                    if (AdminPoPTransactionRetrieveAllAction.matchParams(postPaidTransactionBean, userId, stationId, sourceId, mpTransactionStatus, notificationCreated,
                            notificationPaid, notificationUser, creationTimestampStart, creationTimestampEnd)) {

                        PostPaidTransactionHistoryBean postPaidTransactionHistoryBean = new PostPaidTransactionHistoryBean(postPaidTransactionBean);
                        postPaidTransactionHistoryList.add(postPaidTransactionHistoryBean.toPostPaidTransactionHistory());
                    }
                }
                else {

                    System.out.println("Ricerca per mpTransactionID in history");

                    PostPaidTransactionHistoryBean postPaidTransactionHistoryBean = QueryRepository.findPostPaidTransactionHistoryBeanByMPId(em_crud, mpTransactionId);

                    if (postPaidTransactionHistoryBean != null) {

                        // Se i dati della transazione estratta corrispondono ai parametri di input, allora si inserisce nel risultato
                        if (AdminPoPTransactionRetrieveAllAction.matchParams2(postPaidTransactionHistoryBean, userId, stationId, sourceId, mpTransactionStatus,
                                notificationCreated, notificationPaid, notificationUser, creationTimestampStart, creationTimestampEnd)) {

                            PostPaidTransactionHistory postPaidTransactionHistory = postPaidTransactionHistoryBean.toPostPaidTransactionHistory();
                            postPaidTransactionHistoryList.add(postPaidTransactionHistory);
                        }
                    }
                }

                retrievePoPTransactionListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_POP_TRANSACTION_LIST_SUCCESS);
                retrievePoPTransactionListData.setPostPaidTransactionHistoryList(postPaidTransactionHistoryList);
            }
            else {

                if (creationTimestampStart == null) {
                    creationTimestampStart = new Timestamp(0);
                }
                if (creationTimestampEnd == null) {
                    creationTimestampEnd = new Timestamp(4102444800000l);
                }

                userTransaction.begin();

                System.out.println("Ricerca con parametri");

                List<PostPaidTransactionHistory> postPaidTransactionHistoryList = new ArrayList<PostPaidTransactionHistory>(0);

                List<PostPaidTransactionBean> postPaidtransactionBeanList = QueryRepository.searchPoPTransactions(em_crud, userId, stationId, this.buildSearchString(sourceId),
                        this.buildSearchString(mpTransactionStatus), notificationCreated, notificationPaid, notificationUser, creationTimestampStart, creationTimestampEnd,
                        maxResults);

                System.out.println("Ricerca con parametri ok");

                for (PostPaidTransactionBean postPaidTransactionBean : postPaidtransactionBeanList) {

                    PostPaidTransactionHistoryBean postPaidTransactionHistoryBean = new PostPaidTransactionHistoryBean(postPaidTransactionBean);
                    postPaidTransactionHistoryList.add(postPaidTransactionHistoryBean.toPostPaidTransactionHistory());
                }

                if (transactionHistoryFlag == true) {
                    System.out.println("Ricerca con parametri in history richiesta");
                    List<PostPaidTransactionHistoryBean> postPaidTransactionHistoryBeanList = QueryRepository.searchPoPTransactionsHistory(em_crud, userId, stationId,
                            this.buildSearchString(sourceId), this.buildSearchString(mpTransactionStatus), notificationCreated, notificationPaid, notificationUser,
                            creationTimestampStart, creationTimestampEnd, maxResults);
                    System.out.println("Ricerca con parametri in history positiva");

                    for (PostPaidTransactionHistoryBean postPaidTransactionHistoryBean : postPaidTransactionHistoryBeanList) {
                        postPaidTransactionHistoryList.add(postPaidTransactionHistoryBean.toPostPaidTransactionHistory());
                    }
                }
                else {
                    System.out.println("Ricerca con parametri in history non richiesta");
                }

                retrievePoPTransactionListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_POP_TRANSACTION_LIST_SUCCESS);
                retrievePoPTransactionListData.setPostPaidTransactionHistoryList(postPaidTransactionHistoryList);
            }

            userTransaction.commit();

            return retrievePoPTransactionListData;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin retrieve activity log with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

    private String buildSearchString(String searchS) {

        if (searchS == null || searchS == "") {
            return "%";
        }
        else {
            return "%" + searchS + "%";
        }

    }

    private static Boolean matchParams(PostPaidTransactionBean postPaidTransactionBean, Long userId, String stationId, String sourceId, String mpTransactionStatus,
            Boolean notificationCreated, Boolean notificationPaid, Boolean notificationUser, Date creationTimestampStart, Date creationTimestampEnd) {

        Boolean match = true;

        if (match && (userId != null && postPaidTransactionBean.getUserBean().getId() != userId)) {
            match = false;
        }
        if (match && (stationId != null && !postPaidTransactionBean.getStationBean().getStationID().equals(stationId))) {
            match = false;
        }
        if (match && (sourceId != null && !postPaidTransactionBean.getSourceID().equals(sourceId))) {
            match = false;
        }
        if (match && (mpTransactionStatus != null && !postPaidTransactionBean.getMpTransactionStatus().equals(mpTransactionStatus))) {
            match = false;
        }
        if (match && (notificationCreated != null && !postPaidTransactionBean.getNotificationCreated() != notificationCreated)) {
            match = false;
        }
        if (match && (notificationPaid != null && !postPaidTransactionBean.getNotificationPaid() != notificationPaid)) {
            match = false;
        }
        if (match && (notificationUser != null && !postPaidTransactionBean.getNotificationUser() != notificationUser)) {
            match = false;
        }
        if (match && (creationTimestampStart != null && postPaidTransactionBean.getCreationTimestamp().getTime() < creationTimestampStart.getTime())) {
            match = false;
        }
        if (match && (creationTimestampEnd != null && postPaidTransactionBean.getCreationTimestamp().getTime() > creationTimestampEnd.getTime())) {
            match = false;
        }

        return match;
    }

    private static Boolean matchParams2(PostPaidTransactionHistoryBean postPaidTransactionHistoryBean, Long userId, String stationId, String sourceId, String mpTransactionStatus,
            Boolean notificationCreated, Boolean notificationPaid, Boolean notificationUser, Date creationTimestampStart, Date creationTimestampEnd) {

        Boolean match = true;

        if (match && (userId != null && postPaidTransactionHistoryBean.getUserBean().getId() != userId)) {
            match = false;
        }
        if (match && (stationId != null && !postPaidTransactionHistoryBean.getStationBean().getStationID().equals(stationId))) {
            match = false;
        }
        if (match && (sourceId != null && !postPaidTransactionHistoryBean.getSourceID().equals(sourceId))) {
            match = false;
        }
        if (match && (mpTransactionStatus != null && !postPaidTransactionHistoryBean.getMpTransactionStatus().equals(mpTransactionStatus))) {
            match = false;
        }
        if (match && (notificationCreated != null && !postPaidTransactionHistoryBean.getNotificationCreated() != notificationCreated)) {
            match = false;
        }
        if (match && (notificationPaid != null && !postPaidTransactionHistoryBean.getNotificationPaid() != notificationPaid)) {
            match = false;
        }
        if (match && (notificationUser != null && !postPaidTransactionHistoryBean.getNotificationUser() != notificationUser)) {
            match = false;
        }
        if (match && (creationTimestampStart != null && postPaidTransactionHistoryBean.getCreationTimestamp().getTime() < creationTimestampStart.getTime())) {
            match = false;
        }
        if (match && (creationTimestampEnd != null && postPaidTransactionHistoryBean.getCreationTimestamp().getTime() > creationTimestampEnd.getTime())) {
            match = false;
        }

        return match;
    }

}
