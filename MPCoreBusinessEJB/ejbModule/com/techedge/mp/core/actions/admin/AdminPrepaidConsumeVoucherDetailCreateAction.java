package com.techedge.mp.core.actions.admin;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherDetailBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminPrepaidConsumeVoucherDetailCreateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminPrepaidConsumeVoucherDetailCreateAction() {}

    public String execute(String adminTicketId, String requestId, Double consumedValue, Date expirationDate, Double initialValue, String promoCode, String promoDescription,
            String promoDoc, Double voucherBalanceDue, String voucherCode, String voucherStatus, String voucherType, Double voucherValue, Long prePaidConsumeVoucherId)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = QueryRepository.findPrePaidConsumeVoucherBeanById(em, prePaidConsumeVoucherId);

            if (prePaidConsumeVoucherBean == null) {

                // prePaidConsumeVoucherBean non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "PrePaidConsumeVoucherBean non trovato");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_FAILURE;
            }

            PrePaidConsumeVoucherDetailBean prePaidConsumeVoucherDetailBean = new PrePaidConsumeVoucherDetailBean();

            prePaidConsumeVoucherDetailBean.setVoucherCode(voucherCode);
            prePaidConsumeVoucherDetailBean.setVoucherStatus(voucherStatus);
            prePaidConsumeVoucherDetailBean.setVoucherType(voucherType);
            prePaidConsumeVoucherDetailBean.setVoucherValue(voucherValue);
            prePaidConsumeVoucherDetailBean.setInitialValue(initialValue);
            prePaidConsumeVoucherDetailBean.setConsumedValue(consumedValue);
            prePaidConsumeVoucherDetailBean.setVoucherBalanceDue(voucherBalanceDue);
            prePaidConsumeVoucherDetailBean.setExpirationDate(expirationDate);
            prePaidConsumeVoucherDetailBean.setPromoCode(promoCode);
            prePaidConsumeVoucherDetailBean.setPromoDescription(promoDescription);
            prePaidConsumeVoucherDetailBean.setPromoDoc(promoDoc);
            prePaidConsumeVoucherDetailBean.setPrePaidConsumeVoucherBean(prePaidConsumeVoucherBean);

            em.persist(prePaidConsumeVoucherDetailBean);

            userTransaction.commit();

            return ResponseHelper.ADMIN_ADD_PREPAID_CONSUME_VOUCHER_DETAIL_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED removing transaction event with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
