package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PromotionVoucherStatus;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Voucher;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PromoVoucherBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VoucherBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.FidelityServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.CheckVoucherResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.FidelityResponse;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherCodeDetail;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherConsumerType;
import com.techedge.mp.fidelity.adapter.business.interfaces.VoucherDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminVoucherAssignAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminVoucherAssignAction() {}

    public String execute(String adminTicketId, String requestId, Long userId, String voucherCode, FidelityServiceRemote fidelityService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ASSIGN_VOUCHER_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }
            
            // Verifica la validit� dello userId
            UserBean userBean = QueryRepository.findUserById(em, userId);
            if ( userBean == null ) {
                
                // Utente non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Nessun utente associato allo userId " + userId);

                userTransaction.commit();

                return ResponseHelper.ADMIN_ASSIGN_VOUCHER_FAILURE;
            }
            
            if ( userBean.getUserStatus() != User.USER_STATUS_VERIFIED ) {
                
                // Stato utente non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Impossibile associare il voucher a un utente che non si trova in stato valido");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ASSIGN_VOUCHER_FAILURE;
            }
            
            // Verifica la validit� del voucherCode
            PromoVoucherBean promoVoucherBean = QueryRepository.findPromoVoucherByCode(em, voucherCode);
            if ( promoVoucherBean == null ) {
                
                // Voucher non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Voucher " + voucherCode + " non trovato");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ASSIGN_VOUCHER_FAILURE;
            }
            
            if ( promoVoucherBean.getStatus() != PromotionVoucherStatus.NEW.getValue() ) {
                
                // Stato voucher non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Il voucher risulta gi� utilizzato: stato " + promoVoucherBean.getStatus());

                userTransaction.commit();

                return ResponseHelper.ADMIN_ASSIGN_VOUCHER_FAILURE;
            }
            
            // Marca come utilizzato il voucher
            promoVoucherBean.setStatus(PromotionVoucherStatus.INVALID.getValue());
            
            // Assegna il voucher all'utente
            promoVoucherBean.assignUser(userBean, null);
            em.merge(promoVoucherBean);

            // Aggiorna lo stato dei voucher associati all'utente
            Date now = new Date();

            String operationID = new IdGenerator().generateId(16).substring(0, 33);
            PartnerType partnerType = PartnerType.MP;
            VoucherConsumerType voucherConsumerType = VoucherConsumerType.ENI;
            Long requestTimestamp = now.getTime();
            CheckVoucherResult checkVoucherResult = null;

            List<VoucherCodeDetail> voucherCodeList = new ArrayList<VoucherCodeDetail>(0);

            VoucherCodeDetail voucherCodeDetail = new VoucherCodeDetail();
            voucherCodeDetail.setVoucherCode(voucherCode);
            voucherCodeList.add(voucherCodeDetail);
            
            try {
                checkVoucherResult = fidelityService.checkVoucher(operationID, voucherConsumerType, partnerType, requestTimestamp, voucherCodeList);
            }
            catch (Exception ex) {
                this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, "Error checking voucher: " + ex.getMessage());
                userTransaction.commit();
                return ResponseHelper.ADMIN_ASSIGN_VOUCHER_FAILURE;
            }

            // Verifica l'esito del check
            String checkVoucherStatusCode = checkVoucherResult.getStatusCode();

            if (!checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_OK)) {

                if (checkVoucherStatusCode.equals(FidelityResponse.CHECK_VOUCHER_MAX_LIMIT_REACHED)) {

                    // Max limit reached
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error checking voucher: max limit reached");
                    userTransaction.commit();
                    return ResponseHelper.ADMIN_ASSIGN_VOUCHER_FAILURE;
                }
                else {

                    // Error
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error checking voucher: " + checkVoucherStatusCode);
                    userTransaction.commit();
                    return ResponseHelper.ADMIN_ASSIGN_VOUCHER_FAILURE;
                }
            }
            else {

                // Verifica che il check abbia restituito dei risultati
                if (checkVoucherResult.getVoucherList().isEmpty()) {

                    // Empty list
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null, "Error checking voucher: empty voucher list in response");
                    userTransaction.commit();
                    return ResponseHelper.ADMIN_ASSIGN_VOUCHER_FAILURE;
                }
                else {

                    // Verifica lo stato del voucher
                    VoucherDetail voucherDetail = checkVoucherResult.getVoucherList().get(0);

                    if (!voucherDetail.getVoucherStatus().equals(Voucher.VOUCHER_STATUS_VALIDO)) {
                        
                        promoVoucherBean.setStatus(PromotionVoucherStatus.INVALID.getValue());
                        promoVoucherBean.setUserBean(null);
                        promoVoucherBean.setVerificationValue(null);
                        em.merge(promoVoucherBean);                        

                        // Invalid voucher
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", null, null,
                                "Error checking voucher: voucher status " + voucherDetail.getVoucherStatus());

                        userTransaction.commit();
                        return ResponseHelper.ADMIN_ASSIGN_VOUCHER_FAILURE;
                    }
                    else {

                        promoVoucherBean.consume(userBean);
                        em.merge(promoVoucherBean);

                        VoucherBean newVoucherBean = new VoucherBean();

                        newVoucherBean.setConsumedValue(voucherDetail.getConsumedValue());
                        newVoucherBean.setExpirationDate(voucherDetail.getExpirationDate());
                        newVoucherBean.setInitialValue(voucherDetail.getInitialValue());
                        newVoucherBean.setPromoCode(voucherDetail.getPromoCode());
                        newVoucherBean.setPromoDescription(voucherDetail.getPromoDescription());
                        newVoucherBean.setPromoDoc(voucherDetail.getPromoDoc());
                        newVoucherBean.setVoucherBalanceDue(voucherDetail.getVoucherBalanceDue());
                        newVoucherBean.setCode(voucherDetail.getVoucherCode());
                        newVoucherBean.setStatus(voucherDetail.getVoucherStatus());
                        newVoucherBean.setType(voucherDetail.getVoucherType());
                        newVoucherBean.setValue(voucherDetail.getVoucherValue());

                        newVoucherBean.setUserBean(userBean);
                        em.persist(newVoucherBean);

                        userTransaction.commit();
                        return ResponseHelper.ADMIN_ASSIGN_VOUCHER_SUCCESS;
                    }
                }
            }
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED assign voucher with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
