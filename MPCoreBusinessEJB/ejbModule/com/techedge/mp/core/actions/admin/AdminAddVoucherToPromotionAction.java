package com.techedge.mp.core.actions.admin;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PromoVoucherInput;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.PromotionBean;
import com.techedge.mp.core.business.utilities.QueryRepository;

@LocalBean
@TransactionManagement(TransactionManagementType.BEAN)
@Stateless
public class AdminAddVoucherToPromotionAction extends AdminBaseAction {

    public AdminAddVoucherToPromotionAction() {}

    //ritorna lo status code 
//    public String execute(String adminTicketId, String requestId, PromoVoucherInput listPromotionVaucher, String promotionCode) throws EJBException {
//
//        UserTransaction userTransaction = context.getUserTransaction();
//
//        try {
//            userTransaction.begin();
//
//            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);
//
//            if (adminTicketBean == null || !adminTicketBean.isValid()) {
//
//                // Ticket non valido
//                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");
//
//                userTransaction.commit();
//
//                return ResponseHelper.ADMIN_ADD_VOUCHER_TP_PROMOTION_INVALID_TICKET;
//            }
//
//            
//            
//            PromotionBean promoBean = QueryRepository.findPromotionByCode(em_crud, promotionCode);
//            
//            if(promoBean == null){
//             
//                //non esistono promozioni
//                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid Promotion");
//
//                userTransaction.commit();
//
//                return ResponseHelper.ADMIN_ADD_VOUCHER_TO_PROMOTION_INVALID_PROMOTION;
//            }
//                 
//            if(!listPromotionVaucher.getPromoAssociation().isEmpty()) {
//                
//                promoBean.addPromoVouchers(listPromotionVaucher);
//                userTransaction.commit();
//                return ResponseHelper.ADMIN_ADD_VOUCHER_TO_PROMOTION_SUCCESS;
//            }  
//            
//            userTransaction.commit();
//            return ResponseHelper.ADMIN_ADD_VOUCHER_TO_PROMOTION_NO_VOUCHER;
//        } 
//        catch (Exception ex) {
//
//            try {
//                userTransaction.rollback();
//            } catch (IllegalStateException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            } catch (SecurityException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            } catch (SystemException e) {
//                // TODO Auto-generated catch block
//                e.printStackTrace();
//            }
//
//            String message = "FAILED admin add voucher to promotion  (" + ex.getMessage() + ")";
//            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
//
//            throw new EJBException(ex);
//        }
//       
//
//    }

    @Override
    public Object execute(UserTransaction userTransaction, String key, Object...objects) throws Exception {
        
        this.loggerService.log(ErrorLevel.DEBUG, this.getClass().getSimpleName(), "concrete execute", getRequestId(), "Execute class to associate Promotion Voucher to Promotion ", null);
        
        PromoVoucherInput listPromotionVaucher = (PromoVoucherInput) objects[0];
        
        String promotionCode = (String)objects[1];
        PromotionBean promoBean = QueryRepository.findPromotionByCode(em_crud, promotionCode);
        
        if(promoBean == null){
         
            //non esistono promozioni
            this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", getRequestId(), null, "Invalid Promotion");

            userTransaction.commit();

            return ResponseHelper.ADMIN_ADD_VOUCHER_TO_PROMOTION_INVALID_PROMOTION;
        }
             
        if(!listPromotionVaucher.getPromoAssociation().isEmpty()) {
            
            promoBean.addPromoVouchers(listPromotionVaucher);
            userTransaction.commit();
            return ResponseHelper.ADMIN_ADD_VOUCHER_TO_PROMOTION_SUCCESS;
        }  
        
        userTransaction.commit();
        return ResponseHelper.ADMIN_ADD_VOUCHER_TO_PROMOTION_NO_VOUCHER;
    }

}
