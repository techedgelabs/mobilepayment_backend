package com.techedge.mp.core.actions.admin;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrievePoPTransactionListData;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidConsumeVoucherBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidLoadLoyaltyCreditsBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminPoPTransactionRetrieveReconciliationAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminPoPTransactionRetrieveReconciliationAction() {}

    public RetrievePoPTransactionListData execute(String adminTicketId, String requestId, Integer maxResults) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            RetrievePoPTransactionListData retrievePoPTransactionListData = new RetrievePoPTransactionListData();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                retrievePoPTransactionListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_POP_TRANSACTION_RECONCILIATION_INVALID_TICKET);

                return retrievePoPTransactionListData;
            }
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                retrievePoPTransactionListData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return retrievePoPTransactionListData;
            }
            List<PostPaidConsumeVoucherBean> postPaidConsumeVoucherBeanList = QueryRepository.findPostPaidConsumeVoucherBeanReconciliation(em_crud);

            for (PostPaidConsumeVoucherBean postPaidConsumeVoucherBean : postPaidConsumeVoucherBeanList) {

                PostPaidTransactionBean postPaidTransactionBean = postPaidConsumeVoucherBean.getPostPaidTransactionBean();

                PostPaidTransactionHistoryBean postPaidTransactionHistoryBean = new PostPaidTransactionHistoryBean(postPaidTransactionBean);

                retrievePoPTransactionListData.getPostPaidTransactionHistoryList().add(postPaidTransactionHistoryBean.toPostPaidTransactionHistory());
            }

            List<PostPaidLoadLoyaltyCreditsBean> postPaidLoadLoyaltyCreditsBeanList = QueryRepository.findPostPaidLoadLoyaltyCreditsBeanReconciliation(em_crud);

            for (PostPaidLoadLoyaltyCreditsBean postPaidLoadLoyaltyCreditsBean : postPaidLoadLoyaltyCreditsBeanList) {

                PostPaidTransactionBean postPaidTransactionBean = postPaidLoadLoyaltyCreditsBean.getPostPaidTransactionBean();

                PostPaidTransactionHistoryBean postPaidTransactionHistoryBean = new PostPaidTransactionHistoryBean(postPaidTransactionBean);

                if (!retrievePoPTransactionListData.getPostPaidTransactionHistoryList().contains(postPaidTransactionHistoryBean.toPostPaidTransactionHistory())) {

                    retrievePoPTransactionListData.getPostPaidTransactionHistoryList().add(postPaidTransactionHistoryBean.toPostPaidTransactionHistory());
                }
            }

            userTransaction.commit();

            retrievePoPTransactionListData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_POP_TRANSACTION_RECONCILIATION_SUCCESS);

            return retrievePoPTransactionListData;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin retrieve activity log with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
