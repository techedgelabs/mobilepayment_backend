package com.techedge.mp.core.actions.admin;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveManagerListData;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.ManagerBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminManagerRetrieveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminManagerRetrieveAction() {}

    public RetrieveManagerListData execute(String adminTicketId, String requestId, Long id, String username, String email, Integer maxResults) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        RetrieveManagerListData retrieveManagerListData = new RetrieveManagerListData();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                retrieveManagerListData.setStatusCode(ResponseHelper.ADMIN_SEARCH_MANAGER_INVALID_TICKET);
                return retrieveManagerListData;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                retrieveManagerListData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return retrieveManagerListData;
            }

            /*if (id == null && username == null && email == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid parameters");

                userTransaction.commit();

                retrieveManagerListData.setStatusCode(ResponseHelper.ADMIN_SEARCH_MANAGER_INVALID_PARAMETERS);
                return retrieveManagerListData;
            }*/

            List<ManagerBean> managerBeanList = QueryRepository.searchManager(em, id, username, email, maxResults);

            retrieveManagerListData.setStatusCode(ResponseHelper.ADMIN_SEARCH_MANAGER_SUCCESS);

            for (ManagerBean managerBean : managerBeanList) {
                retrieveManagerListData.getManagerList().add(managerBean.toManager());
            }

            userTransaction.commit();
            return retrieveManagerListData;

        }
        catch (Exception ex2) {
            String message = "FAILED manager searching with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
