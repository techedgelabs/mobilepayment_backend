package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminDocumentAttributeCreateAction {
    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;
    @EJB
    private LoggerService   loggerService;

    private UserTransaction userTransaction;

    public AdminDocumentAttributeCreateAction() {}

    public String execute(String adminTicketId, String requestID, String checkKey, Boolean mandatory, Integer position, String conditionText, String extendedConditionText,
            String documentKey, String subtitle) throws EJBException {

        userTransaction = context.getUserTransaction();
        String result = "";

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_DOCUMENT_CREATE_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            DocumentAttributeBean documentAttribute = QueryRepository.findDocumentAttributeByCheckKey(em, checkKey);

            if (documentAttribute != null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Document attribute gi� esistente");

                userTransaction.commit();

                return ResponseHelper.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_FAILURE;
            }

            DocumentBean document = QueryRepository.findDocumentByKey(em, documentKey);

            if (document == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Document non esistente");

                userTransaction.commit();

                return ResponseHelper.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_FAILURE;
            }

            documentAttribute = new DocumentAttributeBean();
            documentAttribute.setCheckKey(checkKey);
            documentAttribute.setMandatory(mandatory);
            documentAttribute.setPosition(position);

            if (conditionText != null) {
                documentAttribute.setConditionText(conditionText);
            }
            else {
                documentAttribute.setConditionText("");
            }

            if (extendedConditionText != null) {
                documentAttribute.setExtendedConditionText(extendedConditionText);
            }
            else {
                documentAttribute.setExtendedConditionText("");
            }
            
            if (subtitle == null) {
                documentAttribute.setSubTitle("");
            }
            else {
                documentAttribute.setSubTitle(subtitle);
            }
            

            document.getDocumentCheck().add(documentAttribute);

            em.persist(documentAttribute);
            userTransaction.commit();

            return ResponseHelper.ADMIN_DOCUMENT_ATTRIBUTE_CREATE_SUCCESS;

        }
        catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }
}