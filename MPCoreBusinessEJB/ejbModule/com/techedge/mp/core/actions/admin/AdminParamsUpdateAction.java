package com.techedge.mp.core.actions.admin;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AdminUpdateParamsResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ParamInfo;
import com.techedge.mp.core.business.interfaces.ParamUpdateInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.ParameterBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminParamsUpdateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminParamsUpdateAction() {}

    public AdminUpdateParamsResponse execute(String adminTicketId, String requestId, List<ParamInfo> params) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            AdminUpdateParamsResponse adminUpdateParamsResponse = new AdminUpdateParamsResponse();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                adminUpdateParamsResponse.setStatusCode(ResponseHelper.ADMIN_UPDATE_PARAM_INVALID_TICKET);

                return adminUpdateParamsResponse;
            }
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                adminUpdateParamsResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return adminUpdateParamsResponse;
            }
            for (ParamInfo paramInfo : params) {

                ParameterBean parameterBean = QueryRepository.findSingleParameterBeanByName(em_crud, paramInfo.getKey());

                ParamUpdateInfo paramUpdateInfo = new ParamUpdateInfo();
                paramUpdateInfo.setKey(paramInfo.getKey());

                if (parameterBean != null) {

                    parameterBean.setValue(paramInfo.getValue());
                    em_crud.merge(parameterBean);
                    paramUpdateInfo.setStatusCode(ResponseHelper.ADMIN_UPDATE_PARAM_SUCCESS);
                }
                else {

                    paramUpdateInfo.setStatusCode(ResponseHelper.ADMIN_UPDATE_PARAM_FAILURE);
                }

                adminUpdateParamsResponse.getParamList().add(paramUpdateInfo);
            }

            adminUpdateParamsResponse.setStatusCode(ResponseHelper.ADMIN_UPDATE_PARAMS_SUCCESS);

            return adminUpdateParamsResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin update params with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
