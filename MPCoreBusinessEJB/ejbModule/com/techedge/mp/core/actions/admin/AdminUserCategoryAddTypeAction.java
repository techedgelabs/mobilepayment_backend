package com.techedge.mp.core.actions.admin;

import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.UserTypeBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUserCategoryAddTypeAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminUserCategoryAddTypeAction() {}

    public String execute(String adminTicketId, String requestId, String name, Integer code) throws EJBException {
        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_USERTYPE_CATEGORY_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            // Verifica che la categoria e lo user type esistano
            UserCategoryBean userCategoryBean = QueryRepository.findUserCategoryByName(em, name);
            UserTypeBean userTypeBean = QueryRepository.findUserTypeByCode(em, code);

            if (userCategoryBean != null && userTypeBean != null) {

                Set<UserTypeBean> listUserType = userCategoryBean.getUserTypes();

                if (listUserType != null) {
                    listUserType.add(userTypeBean);
                }
                userCategoryBean.setUserTypes(listUserType);
                em.merge(userCategoryBean);

                Set<UserCategoryBean> listUserCategory = userTypeBean.getUserCategories();

                if (listUserCategory != null) {
                    listUserCategory.add(userCategoryBean);
                }
                userTypeBean.setUserCategories(listUserCategory);
                em.merge(userTypeBean);

                userTransaction.commit();

                return ResponseHelper.ADMIN_USERTYPE_CATEGORY_UPDATE;
            }

            else {
                // La categoria o lo usertype non esistono
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "name or code not valid");

                userTransaction.commit();

                return ResponseHelper.ADMIN_USER_CATEGORY_CREATE_FAILURE;
            }

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED manager creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

}
