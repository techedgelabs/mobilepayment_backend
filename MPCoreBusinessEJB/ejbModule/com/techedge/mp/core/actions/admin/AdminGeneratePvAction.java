package com.techedge.mp.core.actions.admin;

import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.GeneratePvResponse;
import com.techedge.mp.core.business.interfaces.PvGenerationData;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StationData;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.DataAcquirerBean;
import com.techedge.mp.core.business.model.StationBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.EncryptionAES;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.forecourt.adapter.business.ForecourtInfoServiceRemote;
import com.techedge.mp.forecourt.adapter.business.interfaces.GetStationDetailsResponse;
import com.techedge.mp.forecourt.adapter.business.interfaces.StationDetail;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminGeneratePvAction {

    private final static String defaultNoOilAcquirerId = "BANCASELLA";
    private final static String defaultNoOilShopLogin  = "GESPAYXXXXX";
    private final static String defaultOilAcquirerId   = "BANCASELLA";
    private final static String defaultOilShopLogin    = "GESPAYXXXXX";
    
    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminGeneratePvAction() {}

    public GeneratePvResponse execute(String adminTicketId, String requestID, Integer finalStatus, Boolean skipCheck, String noOilAcquirerId, String noOilShopLogin,
            String oilAcquirerId, String oilShopLogin, List<StationData> stationList, ForecourtInfoServiceRemote forecourtInfoServiceRemote, String secretKey) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                GeneratePvResponse generatePvResponse = new GeneratePvResponse();
                generatePvResponse.setStatusCode(ResponseHelper.ADMIN_GENERATE_PV_INVALID_TICKET);

                return generatePvResponse;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                GeneratePvResponse generatePvResponse = new GeneratePvResponse();
                generatePvResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return generatePvResponse;
            }

            GeneratePvResponse generatePvResponse = new GeneratePvResponse();

            for (StationData station : stationList) {

                System.out.println("Verifica stationId: " + station.getStationID());
                
                System.out.println("AcquirerID: " + station.getAcquirerID());
                System.out.println("ApiKey: " + station.getApiKey());
                System.out.println("Currency: " + station.getCurrency());
                System.out.println("GroupAcquirer: " + station.getGroupAcquirer());
                System.out.println("EncodedSecretKey: " + station.getEncodedSecretKey());

                PvGenerationData pvGenerationData = new PvGenerationData();
                EncryptionAES encryptionAES = new EncryptionAES();
                if (station.getEncodedSecretKey() != null && !station.getEncodedSecretKey().isEmpty()) {
                    encryptionAES.loadSecretKey(secretKey);
                }

                if (skipCheck) {

                    System.out.println("Modalit� senza verifica dati su gestionale");

                    // Verifica che la stazione esista gi� sul backend e aggiorna lo stato
                    StationBean stationBean = QueryRepository.findStationBeanById(em, station.getStationID());

                    if (stationBean == null) {

                        System.out.println("Il PV non esiste sul backend");

                        pvGenerationData.setStationId(station.getStationID());
                        pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_NOT_FOUND);
                    }
                    else {

                        System.out.println("Stato PV sul backend impostato a " + finalStatus);

                        stationBean.setStationStatus(finalStatus);
                        em.merge(stationBean);

                        pvGenerationData.setStationId(station.getStationID());
                        pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_SUCCESS);
                    }
                }
                else {

                    System.out.println("Modalit� con verifica dati su gestionale");

                    // Ottieni le informazioni sulla stazione dal gestionale
                    GetStationDetailsResponse getStationDetailsResponse = forecourtInfoServiceRemote.getStationDetails(requestID, station.getStationID(), null, false);

                    if (getStationDetailsResponse != null && getStationDetailsResponse.getStatusCode().equals("STATION_PUMP_FOUND_200")
                            && getStationDetailsResponse.getStationDetail() != null) {

                        System.out.println("Dati PV recuperati con successo dal gestionale");

                        // Controlla se la stazione esiste gi� sul backend
                        StationBean stationBean = QueryRepository.findStationBeanById(em, station.getStationID());

                        if (stationBean == null) {

                            System.out.println("Il PV non esiste sul backend");
                            
                            // Se i campi di input sono vuoti sono valorizzati con dei dati di default
                            if (noOilAcquirerId == null || noOilAcquirerId.isEmpty()) {
                                noOilShopLogin = defaultNoOilAcquirerId;
                            }
                            if (noOilShopLogin == null || noOilShopLogin.isEmpty()) {
                                noOilShopLogin = defaultNoOilShopLogin;
                            }
                            if (oilAcquirerId == null || oilAcquirerId.isEmpty()) {
                                oilAcquirerId = defaultOilAcquirerId;
                            }
                            if (oilShopLogin == null || oilShopLogin.isEmpty()) {
                                oilShopLogin = defaultOilShopLogin;
                            }

                            try {

                                System.out.println("Creazione del PV sul backend");

                                StationDetail stationDetail = getStationDetailsResponse.getStationDetail();

                                Double latitudeDouble = Double.valueOf(stationDetail.getLatitude());
                                Double longitudeDouble = Double.valueOf(stationDetail.getLongitude());

                                StationBean newStationBean = new StationBean();
                                newStationBean.setAddress(stationDetail.getAddress());
                                newStationBean.setBeaconCode(null);
                                newStationBean.setCity(stationDetail.getCity());
                                newStationBean.setCountry(stationDetail.getCountry());
                                newStationBean.setLatitude(latitudeDouble);
                                newStationBean.setLongitude(longitudeDouble);
                                newStationBean.setProvince(stationDetail.getProvince());
                                newStationBean.setStationID(stationDetail.getStationID());
                                
                                // Valorizzazione dati acquirer solo se valorizzati
                                if(noOilAcquirerId != null && !noOilAcquirerId.isEmpty()) {
                                    newStationBean.setNoOilAcquirerID(noOilAcquirerId);
                                }
                                if(noOilShopLogin != null && !noOilShopLogin.isEmpty()) {
                                    newStationBean.setNoOilShopLogin(noOilShopLogin);
                                }
                                if(oilAcquirerId != null && !oilAcquirerId.isEmpty()) {
                                    newStationBean.setOilAcquirerID(oilAcquirerId);
                                }
                                if(oilShopLogin != null && !oilShopLogin.isEmpty()) {
                                    newStationBean.setOilShopLogin(oilShopLogin);
                                }
                                
                                newStationBean.setPostpaidActive(true);
                                newStationBean.setPrepaidActive(true);
                                newStationBean.setShopActive(true);
                                newStationBean.setStationStatus(finalStatus);
                                
                                if ( ( station.getAcquirerID()       != null && !station.getAcquirerID().isEmpty() )    ||
                                     ( station.getApiKey()           != null && !station.getApiKey().isEmpty() )        ||
                                     ( station.getCurrency()         != null && !station.getCurrency().isEmpty() )      ||
                                     ( station.getGroupAcquirer()    != null && !station.getGroupAcquirer().isEmpty() ) ||
                                     ( station.getEncodedSecretKey() != null && !station.getEncodedSecretKey().isEmpty() ) ) {
                                    
                                    newStationBean.setDataAcquirer(new DataAcquirerBean());
                                    newStationBean.getDataAcquirer().setAcquirerID(station.getAcquirerID());
                                    newStationBean.getDataAcquirer().setApiKey(station.getApiKey());
                                    newStationBean.getDataAcquirer().setCurrency(station.getCurrency());
                                    newStationBean.getDataAcquirer().setGroupAcquirer(station.getGroupAcquirer());
                                    newStationBean.getDataAcquirer().setEncodedSecretKey(encryptionAES.encrypt(station.getEncodedSecretKey()));
                                }

                                em.persist(newStationBean);

                                pvGenerationData.setStationId(station.getStationID());
                                pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_SUCCESS);
                            }
                            catch (Exception ex) {

                                System.out.println("Si � verificato un errore nella creazione del PV sul backend");

                                pvGenerationData.setStationId(station.getStationID());
                                pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_FAILURE);
                            }
                        }
                        else {

                            // La stazione esiste gi� sul backend e va aggiornata con i dati ricevuti
                            try {

                                System.out.println("Aggiornamento dei dati del PV sul backend");

                                StationDetail stationDetail = getStationDetailsResponse.getStationDetail();

                                Double latitudeDouble = Double.valueOf(stationDetail.getLatitude());
                                Double longitudeDouble = Double.valueOf(stationDetail.getLongitude());

                                stationBean.setAddress(stationDetail.getAddress());
                                stationBean.setCity(stationDetail.getCity());
                                stationBean.setCountry(stationDetail.getCountry());
                                stationBean.setLatitude(latitudeDouble);
                                stationBean.setLongitude(longitudeDouble);
                                stationBean.setProvince(stationDetail.getProvince());
                                stationBean.setStationID(stationDetail.getStationID());

                                if (noOilAcquirerId != null) {
                                    stationBean.setNoOilAcquirerID(noOilAcquirerId);
                                }

                                if (noOilShopLogin != null) {
                                    stationBean.setNoOilShopLogin(noOilShopLogin);
                                }

                                if (oilAcquirerId != null) {
                                    stationBean.setOilAcquirerID(oilAcquirerId);
                                }

                                if (oilShopLogin != null) {
                                    stationBean.setOilShopLogin(oilShopLogin);
                                }
                                if (station.getAcquirerID() != null || station.getApiKey() != null || station.getCurrency() != null || station.getGroupAcquirer() != null
                                        || station.getEncodedSecretKey() != null) {
                                    
                                    if (stationBean.getDataAcquirer() == null ) {
                                        stationBean.setDataAcquirer(new DataAcquirerBean());
                                    }
                                }
                                if (station.getAcquirerID() != null) {
                                    stationBean.getDataAcquirer().setAcquirerID(station.getAcquirerID());
                                }
                                if (station.getApiKey() != null) {
                                    stationBean.getDataAcquirer().setApiKey(station.getApiKey());
                                }
                                if (station.getCurrency() != null) {
                                    stationBean.getDataAcquirer().setCurrency(station.getCurrency());
                                }
                                if (station.getGroupAcquirer() != null) {
                                    stationBean.getDataAcquirer().setGroupAcquirer(station.getGroupAcquirer());
                                }
                                if (station.getEncodedSecretKey() != null) {
                                    stationBean.getDataAcquirer().setEncodedSecretKey(encryptionAES.encrypt(station.getEncodedSecretKey()));
                                }
                                stationBean.setStationStatus(finalStatus);

                                em.persist(stationBean);

                                pvGenerationData.setStationId(station.getStationID());
                                pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_SUCCESS);
                            }
                            catch (Exception ex) {

                                ex.printStackTrace();
                                
                                System.out.println("Si � verificato un errore nell'aggiornamento dei dati del PV sul backend");

                                pvGenerationData.setStationId(station.getStationID());
                                pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_FAILURE);
                            }
                        }
                    }
                    else {

                        System.out.println("Non � stato possibile recuperare i dati del PV dal gestionale");

                        pvGenerationData.setStationId(station.getStationID());
                        pvGenerationData.setStatusCode(ResponseHelper.ADMIN_GENERATE_SINGLE_PV_NOT_FOUND);
                    }
                }

                generatePvResponse.getPvGenerationDataList().add(pvGenerationData);
            }

            generatePvResponse.setStatusCode(ResponseHelper.ADMIN_GENERATE_PV_SUCCESS);

            userTransaction.commit();

            return generatePvResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED generating PV with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
}
