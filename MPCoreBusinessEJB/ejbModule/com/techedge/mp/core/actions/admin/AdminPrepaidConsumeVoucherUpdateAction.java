package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PrePaidConsumeVoucherBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminPrepaidConsumeVoucherUpdateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminPrepaidConsumeVoucherUpdateAction() {}

    public String execute(String adminTicketId, String requestId, Long id, String csTransactionID, String marketingMsg, String messageCode, String operationID,
            String operationIDReversed, String operationType, Boolean reconciled, String statusCode, Double totalConsumed, String warningMsg, Double amount,
            String preAuthOperationID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_INVALID_TICKET;
            }
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }
            if (id == null || id.equals("")) {

                // id non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid id");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_FAILURE;
            }

            PrePaidConsumeVoucherBean prePaidConsumeVoucherBean = QueryRepository.findPrePaidConsumeVoucherBeanById(em, id);

            if (prePaidConsumeVoucherBean == null) {

                // prePaidConsumeVoucherBean non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "PrePaidConsumeVoucherBean non trovato");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_FAILURE;
            }

            if (csTransactionID != null) {
                prePaidConsumeVoucherBean.setCsTransactionID(csTransactionID);
            }
            if (marketingMsg != null) {
                prePaidConsumeVoucherBean.setMarketingMsg(marketingMsg);
            }
            if (messageCode != null) {
                prePaidConsumeVoucherBean.setMessageCode(messageCode);
            }
            if (operationID != null) {
                prePaidConsumeVoucherBean.setOperationID(operationID);
            }
            if (operationIDReversed != null) {
                prePaidConsumeVoucherBean.setOperationIDReversed(operationIDReversed);
            }
            if (operationType != null) {
                prePaidConsumeVoucherBean.setOperationType(operationType);
            }
            if (reconciled != null) {
                prePaidConsumeVoucherBean.setReconciled(reconciled);
            }
            if (statusCode != null) {
                prePaidConsumeVoucherBean.setStatusCode(statusCode);
            }
            if (totalConsumed != null) {
                prePaidConsumeVoucherBean.setTotalConsumed(totalConsumed);
            }
            if (warningMsg != null) {
                prePaidConsumeVoucherBean.setWarningMsg(warningMsg);
            }
            prePaidConsumeVoucherBean.setAmount(amount);
            if (preAuthOperationID != null) {
                prePaidConsumeVoucherBean.setPreAuthOperationID(preAuthOperationID);
            }

            userTransaction.commit();

            return ResponseHelper.ADMIN_UPDATE_PREPAID_CONSUME_VOUCHER_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED removing transaction event with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
