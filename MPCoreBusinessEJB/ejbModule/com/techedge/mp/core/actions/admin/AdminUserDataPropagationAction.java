package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AdminPropagationUserDataResult;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.LoyaltyCard;
import com.techedge.mp.core.business.interfaces.MobilePhone;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.StatusHelper;
import com.techedge.mp.core.business.interfaces.user.DWHOperationType;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.LoyaltyCardBean;
import com.techedge.mp.core.business.model.MobilePhoneBean;
import com.techedge.mp.core.business.model.TermsOfServiceBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.UserSocialDataBean;
import com.techedge.mp.core.business.model.dwh.DWHEventBean;
import com.techedge.mp.core.business.model.dwh.DWHEventOperationBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.dwh.adapter.business.DWHAdapterServiceRemote;
import com.techedge.mp.dwh.adapter.interfaces.DWHAdapterResult;
import com.techedge.mp.dwh.adapter.utilities.StatusCode;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUserDataPropagationAction {

    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;

    @EJB
    private LoggerService   loggerService;

    private UserTransaction userTransaction;

    public AdminUserDataPropagationAction() {}

    public List<AdminPropagationUserDataResult> execute(String adminTicketId, String requestID, Long userID, Integer reconciliationMaxAttempts, List<String> fiscalCodeList) throws EJBException {
        
        userTransaction = context.getUserTransaction();
        List<AdminPropagationUserDataResult> resultList = new ArrayList<AdminPropagationUserDataResult>();
        
        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();
                
                AdminPropagationUserDataResult result = new AdminPropagationUserDataResult();
                
                result.setStatusCode(ResponseHelper.ADMIN_PROPAGATION_USERDATA_INVALID_TICKET);
                result.setStatusMessage("Invalid ticket");
                resultList.add(result);

                return resultList;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                AdminPropagationUserDataResult result = new AdminPropagationUserDataResult();
                
                result.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                result.setStatusMessage("Admin not authotized (not correct role)");
                resultList.add(result);

                return resultList;
            }

            if ((userID == null || userID == 0) && (fiscalCodeList == null || fiscalCodeList.isEmpty())) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "userID not valid");

                userTransaction.commit();

                AdminPropagationUserDataResult result = new AdminPropagationUserDataResult();
                
                result.setStatusCode(ResponseHelper.ADMIN_PROPAGATION_USERDATA_FAILURE);
                result.setStatusMessage("userID not valid");
                resultList.add(result);

                return resultList;
            }
            
            userTransaction.commit();
            
            if (userID == null || userID == 0) {
                for (String fiscalCode : fiscalCodeList) {
                    
                    requestID = String.valueOf(new Date().getTime());
                    // Ricerca l'utente per codice fiscale
                    AdminPropagationUserDataResult result = propagationUserData(userTransaction, null, fiscalCode, requestID, reconciliationMaxAttempts);
                    resultList.add(result);
                }
            }
            else {
                AdminPropagationUserDataResult result = propagationUserData(userTransaction, userID, null, requestID, reconciliationMaxAttempts);
                resultList.add(result);
            }
        }
        catch (Exception e) {
            AdminPropagationUserDataResult result = new AdminPropagationUserDataResult();
            
            result.setStatusCode(ResponseHelper.ADMIN_PROPAGATION_USERDATA_FAILURE);
            result.setStatusMessage(e.getMessage());
            resultList.add(result);
        }
        /*catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (InterfaceNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }*/

        return resultList;
    }
    
    private AdminPropagationUserDataResult propagationUserData(UserTransaction userTransaction, Long userID, String fiscalCode, String requestID, Integer reconciliationMaxAttempts) {
        
        AdminPropagationUserDataResult result = new AdminPropagationUserDataResult();
        
        try {
            
            userTransaction.begin();
            
            UserBean userBean;
            
            if (fiscalCode != null) {
                result.setFiscalCode(fiscalCode);
            }
            
            if (userID == null) {
                userBean = QueryRepository.findNotCancelledUserCustomerByFiscalCode(em, fiscalCode);
            }
            else {
                userBean = QueryRepository.findUserById(em, userID);
            }
            
            if (userBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "User not found or in status cancelled");

                userTransaction.commit();
                result.setStatusCode(ResponseHelper.ADMIN_PROPAGATION_USERDATA_FAILURE);
                result.setStatusMessage("User not found");
                return result;
            }
        
            if (!userBean.getUserStatusRegistrationCompleted()) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "registration not completed");

                userTransaction.commit();
                result.setStatusCode(ResponseHelper.ADMIN_PROPAGATION_USERDATA_FAILURE);
                result.setStatusMessage("registration not completed");
                return result;
            }
            
            if (userBean.getUserType().equals(User.USER_TYPE_GUEST)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "invalid user type: " + userBean.getUserType());

                userTransaction.commit();
                result.setStatusCode(ResponseHelper.ADMIN_PROPAGATION_USERDATA_FAILURE);
                result.setStatusMessage("invalid user type: " + userBean.getUserType());
                return result;
            }
            
            result.setFiscalCode(userBean.getPersonalDataBean().getFiscalCode());
            
            DWHAdapterServiceRemote dwhAdapterService = EJBHomeCache.getInstance().getDWHAdapterService();

            HashMap<String, Boolean> flagPrivacy = new HashMap<>(0);
            List<TermsOfServiceBean> listTermOfService = QueryRepository.findTermOfServiceByPersonalDataId(em, userBean.getPersonalDataBean());
            
            if (listTermOfService == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "flagPrivacy null");
                
                result.setStatusCode(ResponseHelper.ADMIN_PROPAGATION_USERDATA_FAILURE);
                result.setStatusMessage("flagPrivacy null");
                
                userTransaction.commit();
                return result;
            }
            
            for (TermsOfServiceBean item : listTermOfService) {
                if (item.getKeyval().equals("INVIO_COMUNICAZIONI_ENI_NEW_1")) {
                    flagPrivacy.put("privacy_eni", item.getAccepted());
                }
                if (item.getKeyval().equals("INVIO_COMUNICAZIONI_PARTNER_NEW_1")) {
                    flagPrivacy.put("flg_privacy_partner", item.getAccepted());
                    flagPrivacy.put("flg_privacy_analisi", item.getAccepted());
                }
                //if (item.getKeyval().equals("GEOLOCALIZZAZIONE_NEW_1")) {
                    flagPrivacy.put("flg_privacy_geo", Boolean.TRUE);
                //}
            }
            Date date = new Date();
            String codCard = null;
            for (LoyaltyCardBean item : userBean.getLoyaltyCardList()) {
                if (item.getStatus().equals(LoyaltyCard.LOYALTY_CARD_STATUS_VALIDA)) {
                    codCard = item.getPanCode();
                }
            }

            String mobilePhone = null;

            for (MobilePhoneBean item : userBean.getMobilePhoneList()) {
                if (item.getStatus().equals(MobilePhone.MOBILE_PHONE_STATUS_ACTIVE)) {
                    mobilePhone = item.getPrefix() + item.getNumber();
                    break;
                }
            }
            Boolean userBlock = false;
            if (userBean.getUserStatus().equals(User.USER_STATUS_BLOCKED)) {
                userBlock = true;
            }

            String campaignType = "NOLOY";
            if(userBean.getVirtualizationCompleted() != null && userBean.getVirtualizationCompleted() == Boolean.TRUE) {    
                campaignType = "YOUCA";
            }
            
            String securityDataPassword = userBean.getPersonalDataBean().getSecurityDataPassword();
            
            Boolean isSocial  = Boolean.FALSE;
            String socialType = "";
            String idSocial   = null;
            
            String socialProvider = null;
            if (!userBean.getUserSocialData().isEmpty()) {
                for(UserSocialDataBean userSocialDataBean : userBean.getUserSocialData()) {
                    socialProvider = userSocialDataBean.getProvider();
                    idSocial       = userSocialDataBean.getUuid();
                }
            }
            
            if (socialProvider != null) {
                if (socialProvider.equalsIgnoreCase("GOOGLE")) {
                    isSocial             = Boolean.TRUE;
                    socialType           = "google";
                    securityDataPassword = "";
                }
                if (socialProvider.equalsIgnoreCase("FACEBOOK")) {
                    isSocial             = Boolean.TRUE;
                    socialType           = "facebook";
                    securityDataPassword = "";
                }
            }
            
            String source = userBean.getSource();
            if (source != null) {
                if (source.equalsIgnoreCase("ENJOY")) {
                    isSocial             = Boolean.FALSE;
                    socialType           = "enjoy";
                }
                if (source.equalsIgnoreCase("MYCICERO")) {
                    isSocial             = Boolean.FALSE;
                    socialType           = "mycicero";
                }
            }
            
            String statusCode = ResponseHelper.ADMIN_PROPAGATION_USERDATA_SUCCESS;
            String statusMessage = "Progazione eseguita";
            String eventStatus = StatusHelper.DWH_EVENT_STATUS_SUCCESS;
            boolean toReconcilie = false;
            DWHEventBean dWHEventBean = QueryRepository.findDWHEventByUserAndOperation(em, userBean, DWHOperationType.SET_USER_DATA.name());

            DWHAdapterResult resultDWH = dwhAdapterService.setUserDataPlus(userBean.getPersonalDataBean().getSecurityDataEmail(),
                    securityDataPassword, userBean.getPersonalDataBean().getFirstName(), userBean.getPersonalDataBean().getLastName(),
                    userBean.getPersonalDataBean().getSex(), userBean.getPersonalDataBean().getFiscalCode(), userBean.getPersonalDataBean().getBirthDate(),
                    userBean.getPersonalDataBean().getBirthMunicipality(), userBean.getPersonalDataBean().getBirthProvince(), mobilePhone, codCard, flagPrivacy, requestID,
                    Long.valueOf(date.getTime()), userBlock, campaignType, isSocial, socialType, idSocial);

            if (!resultDWH.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SUCCESS)) {
                eventStatus = StatusHelper.DWH_EVENT_STATUS_FAILED;
                statusCode = ResponseHelper.ADMIN_PROPAGATION_USERDATA_FAILURE;

                // L'utente non esiste
                // Errore nella chiamata al servizio DWH
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null,
                        "DWH error: " + resultDWH.getMessageCode() + " (" + resultDWH.getStatusCode() + ")");

                statusMessage = "DWH error: " + resultDWH.getMessageCode() + " (" + resultDWH.getStatusCode() + ")";
                
                if (resultDWH.getStatusCode().equals(StatusCode.DWH_ADAPTER_RESULT_SYSTEM_ERROR)) {
                    toReconcilie = true;
                    eventStatus = StatusHelper.DWH_EVENT_STATUS_ERROR;
                }
            }
            
            if (dWHEventBean == null) {
                dWHEventBean = new DWHEventBean();
                dWHEventBean.setReconciliationAttemptsLeft(reconciliationMaxAttempts);
                dWHEventBean.setToReconcilie(toReconcilie);
                dWHEventBean.setEventStatus(eventStatus);
                dWHEventBean.setOperation(DWHOperationType.SET_USER_DATA);
                dWHEventBean.setUserBean(userBean);

                em.persist(dWHEventBean);
            }
            else {
                dWHEventBean.setEventStatus(eventStatus);
                em.merge(dWHEventBean);
            }


            DWHEventOperationBean dWHEventOperationBean = new DWHEventOperationBean();
            dWHEventOperationBean.setRequestTimestamp(new Date());
            dWHEventOperationBean.setDWHEventBean(dWHEventBean);
            dWHEventOperationBean.setRequestId(requestID);
            dWHEventOperationBean.setStatusCode(resultDWH.getStatusCode());
            dWHEventOperationBean.setMessageCode(resultDWH.getMessageCode());

            em.persist(dWHEventOperationBean);
            
            result.setStatusCode(statusCode);
            result.setStatusMessage(statusMessage);
            
            userTransaction.commit();
            
        }
        catch (Exception ex) {
            result.setStatusCode(ResponseHelper.ADMIN_PROPAGATION_USERDATA_FAILURE);
            result.setStatusMessage(ex.getMessage());
        }
        
        return result;
    }
}
