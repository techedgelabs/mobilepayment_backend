package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.UserCategory;
import com.techedge.mp.core.business.interfaces.user.UserCategoryData;
import com.techedge.mp.core.business.interfaces.user.UserType;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.UserTypeBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUserCategoryRetrieveAction {
    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminUserCategoryRetrieveAction() {}

    public UserCategoryData execute(String adminTicketId, String requestId, String name) throws EJBException {
        UserTransaction userTransaction = context.getUserTransaction();
        UserCategoryData userCategoryData = new UserCategoryData();
        List<UserCategory> listUserCategory = new ArrayList<UserCategory>(0);
        List<UserCategoryBean> listUserCategoryBean = new ArrayList<UserCategoryBean>(0);
        UserCategoryBean userCategoryBean = new UserCategoryBean();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userCategoryData.setStatusCode(ResponseHelper.ADMIN_USER_CATEGORY_INVALID_TICKET);

                userTransaction.commit();

                return userCategoryData;

            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userCategoryData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                userTransaction.commit();

                return userCategoryData;
            }

            if (name != null) {

                userCategoryBean = QueryRepository.findUserCategoryByName(em, name);
                if (userCategoryBean != null) {
                    UserCategory userCategory = new UserCategory();
                    userCategory.setName(userCategoryBean.getName());
                    if (userCategoryBean.getUserTypes() != null || userCategoryBean.getUserTypes().size() > 0) {
                        Set<UserType> listUserTypes = new HashSet<UserType>(0);
                        for (UserTypeBean userType : userCategoryBean.getUserTypes()) {
                            UserType type = new UserType();
                            type.setCode(userType.getCode());
                            listUserTypes.add(type);
                        }
                        userCategory.setUserTypes(listUserTypes);
                    }
                    listUserCategory.add(userCategory);
                }
                else {

                }

            }
            else {
                listUserCategoryBean = QueryRepository.findUserCategoryAllName(em);
                if (listUserCategoryBean != null && listUserCategoryBean.size() > 0) {
                    for (UserCategoryBean itemUserCategoryBean : listUserCategoryBean) {
                        UserCategory userCategory = new UserCategory();
                        userCategory.setName(itemUserCategoryBean.getName());
                        if (itemUserCategoryBean.getUserTypes() != null || itemUserCategoryBean.getUserTypes().size() > 0) {
                            Set<UserType> listUserTypes = new HashSet<UserType>(0);
                            for (UserTypeBean userType : itemUserCategoryBean.getUserTypes()) {
                                UserType type = new UserType();
                                type.setCode(userType.getCode());
                                listUserTypes.add(type);
                            }
                            userCategory.setUserTypes(listUserTypes);
                        }
                        listUserCategory.add(userCategory);
                    }
                }

            }

            if (listUserCategory.size() > 0) {
                userCategoryData.setStatusCode(ResponseHelper.ADMIN_USER_CATEGORY_RETRIEVE);
                userCategoryData.setUserCategories(listUserCategory);
            }
            else {
                userCategoryData.setStatusCode(ResponseHelper.ADMIN_USER_CATEGORY_RETRIEVE_FAILURE);
            }

            userTransaction.commit();
            return userCategoryData;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user category creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
