package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.DocumentAttribute;
import com.techedge.mp.core.business.interfaces.DocumentData;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveDocumentResponse;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.DocumentAttributeBean;
import com.techedge.mp.core.business.model.DocumentBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminDocumentRetrieveAction {
    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;
    @EJB
    private LoggerService   loggerService;

    private UserTransaction userTransaction;

    public AdminDocumentRetrieveAction() {}

    public RetrieveDocumentResponse execute(String adminTicketId, String requestID, String documentKey) throws EJBException {
        userTransaction = context.getUserTransaction();
        RetrieveDocumentResponse result = new RetrieveDocumentResponse();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                result.setStatusCode(ResponseHelper.ADMIN_DOCUMENT_DELETE_INVALID_TICKET);
                return result;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                result.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return result;

            }

            List<DocumentBean> listDocument = new ArrayList<DocumentBean>(0);

            if (documentKey != null) {
                DocumentBean document = QueryRepository.findDocumentByKey(em, documentKey);
                if (document != null) {
                    listDocument.add(document);
                }
            }
            else {
                listDocument = QueryRepository.findDocumentAll(em);
            }

            List<DocumentData> documentDataList = new ArrayList<DocumentData>(0);

            if (listDocument != null && !listDocument.isEmpty()) {
                for (DocumentBean item : listDocument) {
                    DocumentData documentData = new DocumentData();
                    if (item.getDocumentCheck() != null) {
                        List<DocumentAttribute> documentAttributeList = new ArrayList<DocumentAttribute>(0);
                        for (DocumentAttributeBean documentAttributeBean : item.getDocumentCheck()) {
                            DocumentAttribute documentAttribute = new DocumentAttribute();
                            documentAttribute.setCheckKey(documentAttributeBean.getCheckKey());
                            documentAttribute.setConditionText(documentAttributeBean.getConditionText());
                            documentAttribute.setExtendedConditionText(documentAttributeBean.getExtendedConditionText());
                            documentAttribute.setMandatory(documentAttributeBean.isMandatory());
                            documentAttribute.setPosition(documentAttributeBean.getPosition());
                            documentAttributeList.add(documentAttribute);
                        }
                        documentData.setAttributes(documentAttributeList);
                    }

                    documentData.setDocumentKey(item.getDocumentkey());
                    documentData.setTemplateFile(item.getTemplateFile());
                    documentData.setPosition(item.getPosition());
                    documentData.setTitle(item.getTitle());
                    documentData.setSubtitle(item.getSubTitle());
                    if (item.getUserCategory() != null) {
                        documentData.setUserCategory(item.getUserCategory());
                    }
                    if (item.getGroupCategory() != null) {
                        documentData.setGroupCategory(item.getGroupCategory());
                    }
                    documentDataList.add(documentData);
                }
            }
            else {
                listDocument = new ArrayList<DocumentBean>(0);
            }
            result.setStatusCode(ResponseHelper.ADMIN_DOCUMENT_RETRIEVE_SUCCESS);
            result.setListDocument(documentDataList);
            userTransaction.commit();

            return result;

        }
        catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }
}