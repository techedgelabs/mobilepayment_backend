package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.UserCategory;
import com.techedge.mp.core.business.interfaces.user.UserType;
import com.techedge.mp.core.business.interfaces.user.UserTypeData;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.UserCategoryBean;
import com.techedge.mp.core.business.model.UserTypeBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUserTypeRetrieveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminUserTypeRetrieveAction() {}

    public UserTypeData execute(String adminTicketId, String requestId, Integer code) throws EJBException {
        UserTransaction userTransaction = context.getUserTransaction();
        UserTypeData userTypeData = new UserTypeData();
        List<UserType> listUserType = new ArrayList<UserType>(0);
        List<UserTypeBean> listUserTypeBean = new ArrayList<UserTypeBean>(0);
        UserTypeBean userTypeBean = new UserTypeBean();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTypeData.setStatusCode(ResponseHelper.ADMIN_USER_TYPE_INVALID_TICKET);

                userTransaction.commit();

                return userTypeData;

            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                userTypeData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                userTransaction.commit();

                return userTypeData;
            }

            if (code != null) {

                userTypeBean = QueryRepository.findUserTypeByCode(em, code);
                if (userTypeBean != null) {
                    UserType userType = new UserType();
                    userType.setCode(userTypeBean.getCode());
                    if (userTypeBean.getUserCategories() != null || userTypeBean.getUserCategories().size() > 0) {
                        Set<UserCategory> listUserCategories = new HashSet<UserCategory>(0);
                        for (UserCategoryBean userCategory : userTypeBean.getUserCategories()) {
                            UserCategory category = new UserCategory();
                            category.setName(userCategory.getName());
                            listUserCategories.add(category);
                        }
                        userType.setUserCategories(listUserCategories);
                    }
                    listUserType.add(userType);
                }

            }
            else {
                listUserTypeBean = QueryRepository.findUserTypeAllCode(em);
                if (listUserTypeBean != null && listUserTypeBean.size() > 0) {
                    for (UserTypeBean itemUserTypeBean : listUserTypeBean) {
                        UserType userType = new UserType();
                        userType.setCode(itemUserTypeBean.getCode());
                        if (itemUserTypeBean.getUserCategories() != null || itemUserTypeBean.getUserCategories().size() > 0) {
                            Set<UserCategory> listUserCategories = new HashSet<UserCategory>(0);
                            for (UserCategoryBean userCategory : itemUserTypeBean.getUserCategories()) {
                                UserCategory category = new UserCategory();
                                category.setName(userCategory.getName());
                                listUserCategories.add(category);
                            }
                            userType.setUserCategories(listUserCategories);
                        }
                        listUserType.add(userType);
                    }
                }

            }

            if (listUserType.size() > 0) {
                userTypeData.setStatusCode(ResponseHelper.ADMIN_USER_TYPE_RETRIEVE);
                userTypeData.setUserTypes(listUserType);
            }
            else {
                userTypeData.setStatusCode(ResponseHelper.ADMIN_USER_TYPE_RETRIEVE_FAILURE);
            }

            userTransaction.commit();
            return userTypeData;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED manager creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }

}
