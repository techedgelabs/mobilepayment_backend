package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.AdminRole;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminBean;
import com.techedge.mp.core.business.model.AdminRoleBean;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.email.sender.business.EmailSenderRemote;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminCreateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminCreateAction() {}

    public String execute(String adminTicketId, String requestID, Admin admin, Integer verificationCodeExpiryTime, String activationLink, EmailSenderRemote emailSender)
            throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_CREATE_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            // Verifica che non esista gi� un amministratore con quella email
            AdminBean oldAdminBean = QueryRepository.findAdminByEmail(em, admin.getEmail());

            if (oldAdminBean != null) {

                // Esiste gi� un admin con quella email
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin exists");

                userTransaction.commit();

                return ResponseHelper.ADMIN_CREATE_ADMIN_EXISTS;
            }

            // Verifica la validit� dei dati dell'admin che si sta creando
            // TODO

            // Crea l'utente
            AdminBean adminBean = AdminBean.createNewAdmin(admin);

            if (admin.getRoles() != null) {
                for (AdminRole item : admin.getRoles()) {
                    AdminRoleBean adminRoleBean = QueryRepository.findAdminRoleByName(em, item.getName());
                    if (adminRoleBean == null) {
                        // Esiste gi� un admin con quella email
                        this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Role not valid");

                        userTransaction.commit();

                        return ResponseHelper.ADMIN_CREATE_ADMIN_FAILURE;
                    }
                    adminBean.getRoles().add(adminRoleBean);
                }
            }

            // Salva l'utente su db
            em.persist(adminBean);

            // TODO verifica email admin
            /*
            // Genera il codice per la verifica dell'email
            Date now = new Date();
            Date lastUsed = now;
            Date expiryDate = DateHelper.addMinutesToDate(verificationCodeExpiryTime, now);
            
            String newVerificationCodeId = new IdGenerator().generateId(10).substring(0, 10);
            
            VerificationCodeBean verificationCodeBean = new VerificationCodeBean(
            		newVerificationCodeId,
            		userBean,
            		VerificationCodeBean.STATUS_NEW,
            		now,
            		lastUsed,
            		expiryDate);
            
            em.persist(verificationCodeBean);
            
            
            // Invia l'email con il codice di attivazione
            if (emailSender != null) {
            	
            	EmailType emailType = EmailType.CONFIRM_EMAIL;
            	String to = user.getPersonalData().getSecurityDataEmail();
            	List<Parameter> parameters = new ArrayList<Parameter>(0);
            	
            	parameters.add(new Parameter("NAME", userBean.getPersonalDataBean().getFirstName()));
            	parameters.add(new Parameter("ACTIVATION_LINK", activationLink));
            	parameters.add(new Parameter("VERIFICATION_CODE", newVerificationCodeId));
            	parameters.add(new Parameter("EMAIL", userBean.getPersonalDataBean().getSecurityDataEmail()));
            	
            	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Sending email to " + user.getPersonalData().getSecurityDataEmail() );
            	
            	String result = emailSender.sendEmail(
            			emailType,
            			to,
            			parameters);
            	
            	
            	//String result = "disabled";
            	
            	this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "SendEmail result: " + result );
            	
            }
            */

            // Rinnova il ticket
            adminTicketBean.renew();
            em.merge(adminTicketBean);

            userTransaction.commit();

            return ResponseHelper.ADMIN_CREATE_ADMIN_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }
    }
}
