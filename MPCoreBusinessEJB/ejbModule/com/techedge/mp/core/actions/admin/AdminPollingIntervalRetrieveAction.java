package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PollingInterval;
import com.techedge.mp.core.business.interfaces.PollingIntervalData;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PollingIntervalBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminPollingIntervalRetrieveAction {
    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminPollingIntervalRetrieveAction() {}

    public PollingIntervalData execute(String adminTicketId, String requestId) throws EJBException {
        UserTransaction userTransaction = context.getUserTransaction();
        PollingIntervalData pollingIntervalData = new PollingIntervalData();
        List<PollingInterval> pollingIntervalList = new ArrayList<PollingInterval>(0);
        List<PollingIntervalBean> pollingIntervalBeanList = new ArrayList<PollingIntervalBean>(0);

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                pollingIntervalData.setStatusCode(ResponseHelper.RETRIEVE_POLLING_INTERVAL_INVALID_TICKET);

                userTransaction.commit();

                return pollingIntervalData;

            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                pollingIntervalData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                userTransaction.commit();

                return pollingIntervalData;
            }

            pollingIntervalBeanList = QueryRepository.findAllPollingInterval(em);

            if (pollingIntervalBeanList.size() > 0) {
                pollingIntervalData.setStatusCode(ResponseHelper.RETRIEVE_POLLING_INTERVAL_SUCCESS);
                for (PollingIntervalBean item : pollingIntervalBeanList) {
                    PollingInterval pollingInterval = new PollingInterval();
                    pollingInterval.setNextPollingInterval(item.getNextPollingInterval());
                    pollingInterval.setStatus(item.getStatus());
                    pollingInterval.setId(item.getId());
                    pollingIntervalList.add(pollingInterval);
                }
                pollingIntervalData.setPollingInterval(pollingIntervalList);
            }
            else {
                pollingIntervalData.setStatusCode(ResponseHelper.RETRIEVE_POLLING_INTERVAL_FAILURE);
            }

            userTransaction.commit();
            return pollingIntervalData;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user category creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
