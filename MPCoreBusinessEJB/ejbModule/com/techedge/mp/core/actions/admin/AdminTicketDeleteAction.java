package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.AdminService;
import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AdminDeleteTicketResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminTicketDeleteAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminTicketDeleteAction() {}

    public AdminDeleteTicketResponse execute(String adminTicketId, String requestID, String ticketType, String idFrom, String idTo, AdminService adminService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {

            userTransaction.begin();
            
            String where = "WHERE tb.id >= :idFrom AND tb.id <= :idTo";

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);
            
            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();
                
                AdminDeleteTicketResponse adminDeleteTicketResponse = new AdminDeleteTicketResponse();
                adminDeleteTicketResponse.setStatusCode(ResponseHelper.ADMIN_REMOVE_TICKET_INVALID_TICKET);
                
                return adminDeleteTicketResponse;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                AdminDeleteTicketResponse adminDeleteTicketResponse = new AdminDeleteTicketResponse();
                adminDeleteTicketResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                
                return adminDeleteTicketResponse;
            }

            Long longIdFrom = Long.parseLong(idFrom);
            Long longIdTo   = Long.parseLong(idTo);
            
            if (longIdTo < longIdFrom) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ID (idTo < idFrom)");

                userTransaction.commit();

                AdminDeleteTicketResponse adminDeleteTicketResponse = new AdminDeleteTicketResponse();
                adminDeleteTicketResponse.setStatusCode(ResponseHelper.ADMIN_REMOVE_TICKET_FAILURE);
                
                return adminDeleteTicketResponse;
            }
            
            if (ticketType != null && !ticketType.isEmpty()) {
                
                if(ticketType.equalsIgnoreCase("customer")){
                    where = "WHERE tb.type = " + TicketBean.TICKET_TYPE_CUSTOMER + " AND tb.id >= :idFrom AND tb.id <= :idTo";
                }
                
                if(ticketType.equalsIgnoreCase("service")){
                    where = "WHERE tb.type = " + TicketBean.TICKET_TYPE_SERVICE + " AND tb.id >= :idFrom AND tb.id <= :idTo";
                }
                
                if(ticketType.equalsIgnoreCase("refueling")){
                    where = "WHERE tb.type = " + TicketBean.TICKET_TYPE_REFUELING + " AND tb.id >= :idFrom AND tb.id <= :idTo";
                }
            }
            
            Query deleteQuery = em.createQuery("DELETE FROM TicketBean tb "+ where);
            deleteQuery.setParameter("idFrom", longIdFrom);
            deleteQuery.setParameter("idTo",   longIdTo);
            int result = deleteQuery.executeUpdate();
            
            System.out.println("Deleted " + result + " rows from table ticket where ticketType = " + ticketType);
            
            userTransaction.commit();

            AdminDeleteTicketResponse adminDeleteTicketResponse = new AdminDeleteTicketResponse();
            adminDeleteTicketResponse.setStatusCode(ResponseHelper.ADMIN_REMOVE_TICKET_SUCCESS);
            adminDeleteTicketResponse.setDeletedTickets(result);
            
            return adminDeleteTicketResponse;

        }
        catch (Exception ex2) {

            ex2.printStackTrace();
            
            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestID, null, message);

            throw new EJBException(ex2);
        }

    }
}