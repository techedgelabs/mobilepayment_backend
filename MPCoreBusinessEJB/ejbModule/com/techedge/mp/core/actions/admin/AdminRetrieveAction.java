package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.Admin;
import com.techedge.mp.core.business.interfaces.AdminRetrieveDataResponse;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminBean;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminRetrieveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminRetrieveAction() {}

    public AdminRetrieveDataResponse execute(String ticketId, String requestId, Long id, String email, Integer status) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        AdminRetrieveDataResponse response = new AdminRetrieveDataResponse();
        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, ticketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_INVALID_TICKET);
                return response;
            }
            /*
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                response.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return response;
            }
            */

            AdminBean adminBean = adminTicketBean.getAdminBean();
            if (adminBean == null) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();
                response.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_INVALID_TICKET);
                return response;
            }


            // Verifica lo stato dell'utente
            Integer adminStatus = adminBean.getStatus();
            if (adminStatus != Admin.ADMIN_STATUS_VERIFIED) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unable to update admin in status " + adminStatus);

                userTransaction.commit();
                response.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_UNAUTHORIZED);
                return response;
            }

            List<AdminBean> listAdmin = new ArrayList<>(0);
            AdminBean adminResult = null;
            if (id != null) {
                adminResult = QueryRepository.findAdminByID(em, id);
                if (adminResult == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not found (ID)");

                    userTransaction.commit();
                    response.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_FAILURE);
                    return response;
                }
                if (email != null && !email.isEmpty() && !adminResult.getEmail().equals(email)) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not found");

                    userTransaction.commit();
                    response.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_FAILURE);
                    return response;
                }

                if (status != null && adminResult.getStatus() != status) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not found");

                    userTransaction.commit();
                    response.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_FAILURE);
                    return response;
                }

            }
            else if (email != null) {
                adminResult = QueryRepository.findAdminByEmail(em, email);
                if (adminResult == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not found (email)");

                    userTransaction.commit();
                    response.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_FAILURE);
                    return response;
                }
                if (status != null && adminResult.getStatus() != status) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not found");

                    userTransaction.commit();
                    response.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_FAILURE);
                    return response;
                }
            }
            else if (status != null) {
                listAdmin = QueryRepository.findAdminByStatus(em, status);
            }
            else {
                listAdmin = QueryRepository.findAdmins(em);
            }
            if (adminResult != null) {
                listAdmin.add(adminResult);
            }

            List<Admin> listAdminResult = new ArrayList<>(0);

            if (listAdmin != null && !listAdmin.isEmpty()) {
                for (AdminBean item : listAdmin) {
                    listAdminResult.add(item.toAdmin());
                }
            }
            response.setAdmins(listAdminResult);

            response.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_SUCCESS);

            // Rinnova il ticket
            adminTicketBean.renew();
            em.merge(adminTicketBean);

            userTransaction.commit();

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user update with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
