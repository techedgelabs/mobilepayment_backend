package com.techedge.mp.core.actions.admin;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AdminRoleEnum;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUserUpdateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminUserUpdateAction() {}

    public String execute(String adminTicketId, String requestId, Long id, String firstName, String lastName, String email, Date birthDate, String birthMunicipality,
            String birthProvince, String sex, String fiscalCode, String language, Integer status, Boolean registrationCompleted, Double capAvailable, Double capEffective,
            String externalUserId, Integer userType, Boolean virtualizationCompleted, Integer virtualizationAttemptsLeft, Boolean eniStationUserType, Boolean depositCardStepCompleted) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            String response = null;

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_UPDATE_USER_INVALID_TICKET;

                return response;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            userTransaction.commit();

            userTransaction.begin();

            UserBean userBean = QueryRepository.findUserById(em_crud, id);

            if (userBean == null) {

                // Utente non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_UPDATE_USER_FAILURE;

                return response;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())
                    && !CheckRoleHelper.isAuthorizedForRole(this.getClass().getName(), adminTicketBean.getAdminBean(), AdminRoleEnum.ROLE_REFUELING_MANAGER)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            if ((userBean.getUserType() != User.USER_TYPE_REFUELING && userBean.getUserType() != User.USER_TYPE_REFUELING_NEW_ACQUIRER 
                    && userBean.getUserType() != User.USER_TYPE_REFUELING_OAUTH2)
                    && CheckRoleHelper.isAuthorizedForRole(this.getClass().getName(), adminTicketBean.getAdminBean(), AdminRoleEnum.ROLE_REFUELING_MANAGER)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized to update this user");

                userTransaction.commit();
                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            if ((userType != User.USER_TYPE_REFUELING && userType != User.USER_TYPE_REFUELING_NEW_ACQUIRER && userBean.getUserType() != User.USER_TYPE_REFUELING_OAUTH2)
                    && CheckRoleHelper.isAuthorizedForRole(this.getClass().getName(), adminTicketBean.getAdminBean(), AdminRoleEnum.ROLE_REFUELING_MANAGER)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null,
                        "Error new User type: is only Refuling or Refueling new Acquirer");

                userTransaction.commit();
                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            if (email != null) {
                userBean.getPersonalDataBean().setSecurityDataEmail(email);
            }

            if (language != null) {
                userBean.getPersonalDataBean().setLanguage(language);
            }

            if (firstName != null) {
                userBean.getPersonalDataBean().setFirstName(firstName);
            }

            if (lastName != null) {
                userBean.getPersonalDataBean().setLastName(lastName);
            }

            if (birthDate != null) {
                userBean.getPersonalDataBean().setBirthDate(birthDate);
            }

            if (birthMunicipality != null) {
                userBean.getPersonalDataBean().setBirthMunicipality(birthMunicipality);

            }

            if (birthProvince != null) {
                userBean.getPersonalDataBean().setBirthProvince(birthProvince);
            }

            if (sex != null) {
                userBean.getPersonalDataBean().setSex(sex);
            }

            if (fiscalCode != null) {
                userBean.getPersonalDataBean().setFiscalCode(fiscalCode);
            }

            if (registrationCompleted == true) {
                userBean.setRegistrationCompleted();
            }
            else {
                userBean.unsetRegistrationCompleted();
            }

            if (status != null) {
                userBean.changeStatus(status);
            }

            if (capAvailable != null) {
                userBean.setCapAvailable(capAvailable);
            }
            
            if (capEffective != null) {
                userBean.setCapEffective(capEffective);
            }
            
            if (externalUserId != null) {
                userBean.setExternalUserId(externalUserId);
            }
            
            if (userType != null) {
                userBean.setUserType(userType);
            }
            
            if (virtualizationCompleted != null) {
                userBean.setVirtualizationCompleted(virtualizationCompleted);
            }
            
            if (virtualizationAttemptsLeft != null) {
                userBean.setVirtualizationAttemptsLeft(virtualizationAttemptsLeft);
            }
            
            if (eniStationUserType != null) {
                userBean.setEniStationUserType(eniStationUserType);
            }
            
            if (depositCardStepCompleted != null) {
                userBean.setDepositCardStepCompleted(depositCardStepCompleted);
            }
            
            userTransaction.commit();

            response = ResponseHelper.ADMIN_UPDATE_USER_SUCCESS;

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin update user with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
