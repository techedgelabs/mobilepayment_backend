package com.techedge.mp.core.actions.admin;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.AdminArchiveInvertResponse;
import com.techedge.mp.core.business.interfaces.ArchiveTransactionResult;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionEventBean;
import com.techedge.mp.core.business.model.TransactionEventHistoryBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.TransactionStatusBean;
import com.techedge.mp.core.business.model.TransactionStatusHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidCartHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidRefuelHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionEventHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionHistoryBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventBean;
import com.techedge.mp.core.business.model.postpaid.PostPaidTransactionPaymentEventHistoryBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminTransactionArchiveInvertAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @EJB
    private LoggerService loggerService;

    public AdminTransactionArchiveInvertAction() {}

    public AdminArchiveInvertResponse execute(String adminTicketId, String requestId, String transactionID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        AdminArchiveInvertResponse adminArchiveInvertResponse = new AdminArchiveInvertResponse();

        try {
            userTransaction.begin();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                adminArchiveInvertResponse.setStatusCode(ResponseHelper.ADMIN_ARCHIVE_INVALID_TICKET);
                return adminArchiveInvertResponse;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                adminArchiveInvertResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return adminArchiveInvertResponse;
            }

            userTransaction.commit();
            //Prepaid
            TransactionHistoryBean transactionHistoryBean = new TransactionHistoryBean();

            //PostPaid
            PostPaidTransactionHistoryBean postPaidTransaction_HistoryBean = new PostPaidTransactionHistoryBean();

            //Oggetto ID - TIPO - STATUS
            List<ArchiveTransactionResult> listArchiveTransactionResults = new ArrayList<ArchiveTransactionResult>();

            if (QueryRepository.findTransactionHistoryBeanById(em_crud, transactionID) != null) {
                TransactionHistoryBean transactionBeanFind = QueryRepository.findTransactionHistoryBeanById(em_crud, transactionID);
                if (transactionBeanFind != null) {
                    transactionHistoryBean = transactionBeanFind;
                }
                // Per ogni transazione prepaid estratta
                userTransaction.begin();

                // 0) Ricarica i dati nel nuovo blocco transazione
                transactionHistoryBean = QueryRepository.findTransactionHistoryBeanById(em_crud, transactionHistoryBean.getTransactionID());

                // 1) Crea la transazione archiviata
                TransactionBean transactionBean = new TransactionBean(transactionHistoryBean);

                // 2) Persisti le informazioni nelle tabelle archivio

                em_crud.persist(transactionBean);

                for (TransactionStatusBean transactionStatusBean : transactionBean.getTransactionStatusBeanData()) {

                    em_crud.persist(transactionStatusBean);
                }

                // 3) Elimina la transazione dalle tabelle attive

                for (TransactionStatusHistoryBean transactionStatusHistoryBean : transactionHistoryBean.getTransactionStatusHistoryBeanData()) {

                    em_crud.remove(transactionStatusHistoryBean);
                }

                // 4) Persisti le informazioni nelle tabelle archivio

                for (TransactionEventBean transactionEventBean : transactionBean.getTransactionEventBeanData()) {

                    em_crud.persist(transactionEventBean);
                }

                // 5) Elimina la transazione dalle tabelle attive

                for (TransactionEventHistoryBean transactionEventHistoryBean : transactionHistoryBean.getTransactionEventHistoryBeanData()) {

                    em_crud.remove(transactionEventHistoryBean);
                }

                em_crud.remove(transactionHistoryBean);

                ArchiveTransactionResult archiveTransactionResult = new ArchiveTransactionResult(transactionHistoryBean.getTransactionID(), "PrePaid Transaction",
                        ResponseHelper.ADMIN_ARCHIVE_SUCCESS);

                listArchiveTransactionResults.add(archiveTransactionResult);

                userTransaction.commit();

            }
            else if (QueryRepository.findPostPaidTransactionHistoryBeanByMPId(em_crud, transactionID) != null) {
                PostPaidTransactionHistoryBean postPaidTransactionBeanFind = QueryRepository.findPostPaidTransactionHistoryBeanByMPId(em_crud, transactionID);
                if (postPaidTransactionBeanFind != null) {
                    postPaidTransaction_HistoryBean = postPaidTransactionBeanFind;
                }
                // Per ogni transazione PostPaid estratta

                userTransaction.begin();

                // 0) Ricarica i dati nel nuovo blocco transazione
                postPaidTransaction_HistoryBean = QueryRepository.findPostPaidTransactionHistoryBeanByMPId(em_crud, postPaidTransaction_HistoryBean.getMpTransactionID());
                System.out.println("Ricarica i dati nel nuovo blocco transazione");

                // 1) Crea la transazione archiviata
                PostPaidTransactionBean postPaidTransactionBean = new PostPaidTransactionBean(postPaidTransaction_HistoryBean);

                // 2) Persisti le informazioni nelle tabelle archivio

                em_crud.persist(postPaidTransactionBean);
                System.out.println("Persisti le informazioni nelle tabelle archivio");

                // 3) Persistenza dei refuel collegati alla transazione archivio
                for (PostPaidRefuelBean postPaidRefuelBean : postPaidTransactionBean.getRefuelBean()) {
                    em_crud.persist(postPaidRefuelBean);
                }
                System.out.println("Persistenza dei refuel collegati alla transazione archivio");

                // 4) Persistenza dei shop collegati alla transazione archivio
                for (PostPaidCartBean postPaidCartBean : postPaidTransactionBean.getCartBean()) {

                    em_crud.persist(postPaidCartBean);
                }
                System.out.println("Persistenza dei shop collegati alla transazione archivio");

                // 5) Persistenza dei event collegati alla transazione archivio
                for (PostPaidTransactionEventBean postPaidTransactionEventBean : postPaidTransactionBean.getPostPaidTransactionEventBean()) {

                    em_crud.persist(postPaidTransactionEventBean);
                }

                // 6) Persistenza dei payment event collegati alla transazione archivio
                for (PostPaidTransactionPaymentEventBean postPaidTransactionPaymentEventBean : postPaidTransactionBean.getPostPaidTransactionPaymentEventBean()) {

                    em_crud.persist(postPaidTransactionPaymentEventBean);
                }

                // 7) Eliminazione dei refuel collegati alla transazione
                for (PostPaidRefuelHistoryBean postPaidRefuelBean : postPaidTransaction_HistoryBean.getRefuelHistoryBean()) {

                    em_crud.remove(postPaidRefuelBean);
                }

                // 8) Eliminazione dei shop collegati alla transazione
                for (PostPaidCartHistoryBean postPaidCartBean : postPaidTransaction_HistoryBean.getCartHistoryBean()) {

                    em_crud.remove(postPaidCartBean);
                }

                // 9) Eliminazione dei event collegati alla transazione
                for (PostPaidTransactionEventHistoryBean postPaidTransactionEventBean : postPaidTransaction_HistoryBean.getPostPaidTransactionEventHistoryBean()) {

                    em_crud.remove(postPaidTransactionEventBean);
                }

                // 10) Eliminazione dei payment event collegati alla transazione
                for (PostPaidTransactionPaymentEventHistoryBean postPaidTransactionPaymentEventBean : postPaidTransaction_HistoryBean.getPostPaidTransactionPaymentEventHistoryBean()) {

                    em_crud.remove(postPaidTransactionPaymentEventBean);
                }

                // 11) Eliminazione della transazione
                em_crud.remove(postPaidTransaction_HistoryBean);

                ArchiveTransactionResult popArchiveTransactionResult = new ArchiveTransactionResult(postPaidTransactionBean.getMpTransactionID(), "PostPaid Transaction",
                        ResponseHelper.ADMIN_POPARCHIVE_SUCCESS);

                listArchiveTransactionResults.add(popArchiveTransactionResult);

                userTransaction.commit();
            }
            else if (listArchiveTransactionResults.size() == 0) {
                adminArchiveInvertResponse.setArchiveTransactionResultList(listArchiveTransactionResults);
                adminArchiveInvertResponse.setStatusCode(ResponseHelper.ADMIN_ARCHIVE_FAILURE);
                adminArchiveInvertResponse.setStatusMessage("ID not valid");
                return adminArchiveInvertResponse;
            }

            adminArchiveInvertResponse.setArchiveTransactionResultList(listArchiveTransactionResults);
            adminArchiveInvertResponse.setStatusCode(ResponseHelper.ADMIN_ARCHIVE_SUCCESS);
            adminArchiveInvertResponse.setStatusMessage("Correct transaction");

            return adminArchiveInvertResponse;
        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED transaction archiving with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", null, null, message);

            throw new EJBException(ex2);
        }
    }
}
