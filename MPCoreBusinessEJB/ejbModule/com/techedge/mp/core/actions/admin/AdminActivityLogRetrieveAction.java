package com.techedge.mp.core.actions.admin;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ActivityLog;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.RetrieveActivityLogData;
import com.techedge.mp.core.business.model.ActivityLogBean;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminActivityLogRetrieveAction {

	@Resource
    private EJBContext context;
	
	@PersistenceContext( unitName = "CrudPU" )
    private EntityManager em_crud;
	
    @PersistenceContext( unitName = "LogPU" )
    private EntityManager em_log;
    
    @EJB
    private LoggerService loggerService;
    
    
    public AdminActivityLogRetrieveAction() {
    }
    
    
    public RetrieveActivityLogData execute(
    		String adminTicketId,
    		String requestId,
    		Timestamp start,
    		Timestamp end,
    		ErrorLevel minLevel,
    		ErrorLevel maxLevel,
    		String source,
    		String groupId,
    		String phaseId,
    		String messagePattern,
    		Integer maxResults
    		) throws EJBException {
    	
    	UserTransaction userTransaction = context.getUserTransaction();
    	
    	try {
    		userTransaction.begin();
    		
    		RetrieveActivityLogData retrieveActivityLogData = new RetrieveActivityLogData();
    		
    		AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);
    		
    		if ( adminTicketBean == null || !adminTicketBean.isValid() ) {
    			
    			// Ticket non valido
    			this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket" );
				
    			userTransaction.commit();
    			
    			retrieveActivityLogData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_ACTIVITY_LOG_INVALID_TICKET);
    			
    			return retrieveActivityLogData;
    		}
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                retrieveActivityLogData.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);
                return retrieveActivityLogData;
            }
    		
    		userTransaction.commit();
    		
    		if ( start == null ) {
    			start = new Timestamp(0);
    		}
    		if ( end == null ) {
    			end = new Timestamp(4102444800000l);
    		}
    		if ( minLevel == null ) {
    			minLevel = ErrorLevel.TRACE;
    		}
    		if ( maxLevel == null ) {
    			maxLevel = ErrorLevel.OFF;
    		}
    		if ( source == null || source == "" ) {
    			source = "%";
    		}
    		else {
    			source = "%" + source + "%";
    		}
    		if ( groupId == null || groupId == "" ) {
    			groupId = "%";
    		}
    		else {
    			groupId = "%" + groupId + "%";
    		}
    		if ( phaseId == null || phaseId == "" ) {
    			phaseId = "%";
    		}
    		else {
    			phaseId = "%" + phaseId + "%";
    		}
    		
    		if ( messagePattern == null || messagePattern == "" || messagePattern == "%" ) {
    			messagePattern = "%";
    		}
    		else {
    			messagePattern = "%" + messagePattern + "%";
    		}
    		
    		userTransaction.begin();

    		List<ActivityLogBean> activityLogBeanList = QueryRepository.searchActivityLog(
    				em_log,
    				start,
    	    		end,
    	    		minLevel,
    	    		maxLevel,
    	    		source,
    	    		groupId,
    	    		phaseId,
    	    		messagePattern,
    	    		maxResults);
    		
    		List<ActivityLog> activityLogList = new ArrayList<ActivityLog>(0);
    		
    		for ( ActivityLogBean activityLogBean: activityLogBeanList ) {
    			
    			ActivityLog activityLog = activityLogBean.toActivityLog();
    			
    			activityLogList.add(activityLog);
    		}
    		
    		retrieveActivityLogData.setStatusCode(ResponseHelper.ADMIN_RETRIEVE_ACTIVITY_LOG_SUCCESS);
    		retrieveActivityLogData.setActivityLogList(activityLogList);
    		
    		userTransaction.commit();
    		
    		return retrieveActivityLogData;
    		
    	}
    	catch (Exception ex2) {
    		
    		try {
				userTransaction.rollback();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SystemException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		
    		String message = "FAILED admin retrieve activity log with message (" + ex2.getMessage() + ")";
	        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message );
			    
	        throw new EJBException(ex2);
    	}
    }
}
