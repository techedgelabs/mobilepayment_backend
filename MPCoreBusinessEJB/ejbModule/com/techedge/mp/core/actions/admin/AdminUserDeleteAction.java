package com.techedge.mp.core.actions.admin;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.TicketBean;
import com.techedge.mp.core.business.model.TransactionBean;
import com.techedge.mp.core.business.model.TransactionHistoryBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.model.VerificationCodeBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUserDeleteAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminUserDeleteAction() {}

    public String execute(String adminTicketId, String requestId, Long userId, Date creationStart, Date creationEnd) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            String response = null;

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_UPDATE_USER_INVALID_TICKET;

                return response;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            if (userId != null && userId != 0) {

                UserBean userBean = QueryRepository.findUserById(em_crud, userId);

                if (userBean == null) {

                    // Utente non trovato
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                    userTransaction.commit();

                    response = ResponseHelper.ADMIN_DELETE_USER_FAILURE;

                    return response;
                }

                List<VerificationCodeBean> vcbList = QueryRepository.findVerificationCodeBeanByUser(em_crud, userBean);
                if (vcbList != null) {
                    for (VerificationCodeBean vcb : vcbList) {
                        em_crud.remove(vcb);
                    }
                }
                List<TransactionBean> tList = QueryRepository.findTransactionByUser(em_crud, userBean);
                if (tList != null) {
                    for (TransactionBean tb : tList) {
                        tb.setUserBean(null);
                        em_crud.merge(tb);
                    }
                }

                List<TransactionHistoryBean> thList = QueryRepository.findTransactionHistoryByUser(em_crud, userBean);
                if (thList != null) {
                    for (TransactionHistoryBean thb : thList) {
                        thb.setUserBean(null);
                        em_crud.merge(thb);
                    }
                }

                List<TicketBean> tbList = QueryRepository.findTicketByUser(em_crud, userBean);
                if (tbList != null) {
                    for (TicketBean tb : tbList) {
                        tb.setUser(null);
                        em_crud.merge(tb);
                    }
                }

                em_crud.remove(userBean);

            }
            else if (creationStart != null && creationEnd != null) {
                List<UserBean> userBeanList = QueryRepository.findUserByCreationDate(em_crud, creationStart, creationEnd);
                if (userBeanList == null) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                    userTransaction.commit();

                    response = ResponseHelper.ADMIN_DELETE_USER_FAILURE;

                    return response;

                }

                for (UserBean ub : userBeanList) {
                    List<VerificationCodeBean> vcbList = QueryRepository.findVerificationCodeBeanByUser(em_crud, ub);
                    if (vcbList != null) {
                        for (VerificationCodeBean vcb : vcbList) {
                            em_crud.remove(vcb);
                        }
                    }

                    List<TransactionBean> tList = QueryRepository.findTransactionByUser(em_crud, ub);
                    if (tList != null) {
                        for (TransactionBean tb : tList) {
                            tb.setUserBean(null);
                            em_crud.merge(tb);
                        }
                    }

                    List<TransactionHistoryBean> thList = QueryRepository.findTransactionHistoryByUser(em_crud, ub);
                    if (thList != null) {
                        for (TransactionHistoryBean thb : thList) {
                            thb.setUserBean(null);
                            em_crud.merge(thb);
                        }
                    }

                    List<TicketBean> tbList = QueryRepository.findTicketByUser(em_crud, ub);
                    if (tbList != null) {
                        for (TicketBean tb : tbList) {
                            tb.setUser(null);
                            em_crud.merge(tb);
                        }
                    }

                    em_crud.remove(ub);
                }

            }
            else {

                // Utente non trovato
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                response = ResponseHelper.ADMIN_DELETE_USER_FAILURE;

                return response;
            }

            userTransaction.commit();

            response = ResponseHelper.ADMIN_DELETE_USER_SUCCESS;

            return response;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin update user with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
