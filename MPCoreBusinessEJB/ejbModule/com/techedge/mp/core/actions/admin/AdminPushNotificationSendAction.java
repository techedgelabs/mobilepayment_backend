package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.exceptions.InterfaceNotFoundException;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PushNotificationBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.EJBHomeCache;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.pushnotification.adapter.business.PushNotificationServiceRemote;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationMessage;
import com.techedge.mp.pushnotification.adapter.interfaces.PushNotificationResult;
import com.techedge.mp.pushnotification.adapter.interfaces.StatusCode;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminPushNotificationSendAction {
    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;
    @EJB
    private LoggerService   loggerService;

    private UserTransaction userTransaction;

    public AdminPushNotificationSendAction() {}

    public String execute(String adminTicketId, String requestID, Long userID, Long idMessage, String titleNotification, String messageNotification) throws EJBException {
        userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_PUSH_NOTIFICATION_TEST_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            if (userID == null) {
                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "invalid userID");

                userTransaction.commit();

                return ResponseHelper.ADMIN_PUSH_NOTIFICATION_TEST_INVALID_PARAMETER;
            }

            UserBean userBean = QueryRepository.findUserById(em, userID);
            if (userBean == null) {

                // userID non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid userBean for userID");

                userTransaction.commit();

                return ResponseHelper.ADMIN_PUSH_NOTIFICATION_TEST_NOT_EXISTS;
            }

            PushNotificationServiceRemote pushNotificationServiceRemote;
            try {
                pushNotificationServiceRemote = EJBHomeCache.getInstance().getPushNotificationService();

                if (pushNotificationServiceRemote == null) {
                    // userID non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid pushNotificationRemote");

                    userTransaction.commit();

                    return ResponseHelper.ADMIN_PUSH_NOTIFICATION_TEST_INVALID_PARAMETER;
                }
                PushNotificationMessage pushNotificationMessage = new PushNotificationMessage();
                PushNotificationBean pushNotificationBean = QueryRepository.findPushNotificationById(em, idMessage);
                if (pushNotificationBean == null) {
                    pushNotificationMessage.setMessage(idMessage, messageNotification, titleNotification);
                }
                else {
                    pushNotificationMessage.setMessage(pushNotificationBean.getId(), pushNotificationBean.getMessage(), pushNotificationBean.getTitle());
                }
                PushNotificationResult result = pushNotificationServiceRemote.publishMessage(userBean.getLastLoginDataBean().getDeviceEndpoint(), pushNotificationMessage, false, true);

                if (!result.getStatusCode().equals(StatusCode.PUSH_NOTIFICATION_PUBLISH_MESSAGE_SUCCESS)) {
                    // userID non valido
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid pushNotificationRemote");

                    userTransaction.commit();

                    return ResponseHelper.ADMIN_PUSH_NOTIFICATION_TEST_FAILURE;
                }

                userTransaction.commit();

                return ResponseHelper.ADMIN_PUSH_NOTIFICATION_TEST_SUCCESS;
            }
            catch (InterfaceNotFoundException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }
        catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return ResponseHelper.ADMIN_PUSH_NOTIFICATION_TEST_FAILURE;
    }
}