package com.techedge.mp.core.actions.admin;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.Survey;
import com.techedge.mp.core.business.interfaces.SurveyList;
import com.techedge.mp.core.business.interfaces.SurveyQuestion;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.SurveyBean;
import com.techedge.mp.core.business.model.SurveyQuestionBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminSurveyRetrieveAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminSurveyRetrieveAction() {}

    public SurveyList execute(String adminTicketId, String requestId, String surveyCode, Date startSearch, Date endSearch, Integer status) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();
        SurveyList surveyList = new SurveyList();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                surveyList.setStatusCode(ResponseHelper.ADMIN_INVALID_TICKET);

                userTransaction.commit();

                return surveyList;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();
                surveyList.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return surveyList;
            }

            List<SurveyBean> surveyBeanList = QueryRepository.findSurvey(em, surveyCode, startSearch, endSearch, status);

            for (SurveyBean surveyBean : surveyBeanList) {
                Survey survey = new Survey();
                survey.setCode(surveyBean.getCode());
                survey.setDescription(surveyBean.getDescription());
                survey.setNote(surveyBean.getNote());
                survey.setStartDate(surveyBean.getStartDate());
                survey.setEndDate(surveyBean.getEndDate());
                survey.setStatus(surveyBean.getStatus());

                for (SurveyQuestionBean questionBean : surveyBean.getQuestionsList()) {
                    SurveyQuestion question = new SurveyQuestion();
                    question.setCode(questionBean.getCode());
                    question.setSequence(questionBean.getSequence());
                    question.setText(questionBean.getText());
                    question.setType(questionBean.getType());
                    survey.getQuestions().add(question);
                }

                surveyList.getSurveyList().add(survey);
            }

            surveyList.setStatusCode(ResponseHelper.ADMIN_SURVEY_GET_SUCCESS);

            userTransaction.commit();

            return surveyList;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
