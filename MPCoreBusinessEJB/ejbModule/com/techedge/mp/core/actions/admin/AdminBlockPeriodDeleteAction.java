package com.techedge.mp.core.actions.admin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.HeuristicMixedException;
import javax.transaction.HeuristicRollbackException;
import javax.transaction.NotSupportedException;
import javax.transaction.RollbackException;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.UnavailabilityPeriodBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminBlockPeriodDeleteAction {
    @Resource
    private EJBContext      context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager   em;
    @EJB
    private LoggerService   loggerService;

    private UserTransaction userTransaction;

    public AdminBlockPeriodDeleteAction() {}

    public String execute(String adminTicketId, String requestID, String code) throws EJBException {
        userTransaction = context.getUserTransaction();
        String result = "";

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_BLOCK_PERIOD_DELETE_INVALID_TICKET;
            }
            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }
            UnavailabilityPeriodBean unavailabilityPeriodBean = QueryRepository.findUnavailabilityPeriodByCode(em, code);

            if (unavailabilityPeriodBean == null) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestID, null, "Code exists");

                userTransaction.commit();

                return ResponseHelper.ADMIN_BLOCK_PERIOD_DELETE_NOT_EXIST_CODE;
            }

            em.remove(unavailabilityPeriodBean);
            userTransaction.commit();

            return ResponseHelper.ADMIN_BLOCK_PERIOD_DELETE_SUCCESS;

        }

        catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (NotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (SystemException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (RollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicMixedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        catch (HeuristicRollbackException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return result;
    }

    public boolean isThisDateValid(String dateToValidate, String dateFromat) {

        if (dateToValidate == null) {
            System.out.println("Data non valida");
            return false;
        }

        SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
        sdf.setLenient(false);

        try {
            Date date = sdf.parse(dateToValidate);
            System.out.println(date);
        }
        catch (ParseException e) {
            System.out.println("Data non valida");
            return false;
        }
        System.out.println("Data valida");
        return true;
    }

}