package com.techedge.mp.core.actions.admin;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PromoUsersOnHoldStatus;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PromoUsersOnHoldBean;
import com.techedge.mp.core.business.model.PromotionBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminUserInsertInPromoOnHoldAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em;

    @EJB
    private LoggerService loggerService;

    public AdminUserInsertInPromoOnHoldAction() {}

    public String execute(String adminTicketId, String requestId, String promotionCode, Long userId) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_INSERT_USER_IN_PROMO_ONHOLD_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            if (promotionCode == null || promotionCode.equals("") || userId == null || userId.equals("")) {

                // parametri non validi
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid parameters");

                userTransaction.commit();

                return ResponseHelper.ADMIN_INSERT_USER_IN_PROMO_ONHOLD_FAILURE;
            }

            // Ricerca la promozione
            PromotionBean promotionBean = QueryRepository.findSinglePromotionByCode(em, promotionCode);

            if (promotionBean == null) {

                // La promozione non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Promotion not found");

                userTransaction.commit();

                return ResponseHelper.ADMIN_INSERT_USER_IN_PROMO_ONHOLD_FAILURE;
            }

            // Ricerca l'utente
            UserBean userBean = QueryRepository.findUserById(em, userId);

            if (userBean == null) {

                // L'utente non esiste
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not found");

                userTransaction.commit();

                return ResponseHelper.ADMIN_INSERT_USER_IN_PROMO_ONHOLD_FAILURE;
            }

            // Aggiungo l'oggetto PromoUsersOnHoldBean
            PromoUsersOnHoldBean promoUsersOnHoldBean = new PromoUsersOnHoldBean();

            promoUsersOnHoldBean.setInsertedData(new Date());
            promoUsersOnHoldBean.setPromotionBean(promotionBean);
            promoUsersOnHoldBean.setStatus(PromoUsersOnHoldStatus.NEW.getValue());
            promoUsersOnHoldBean.setUserBean(userBean);

            em.persist(promoUsersOnHoldBean);

            userTransaction.commit();

            return ResponseHelper.ADMIN_INSERT_USER_IN_PROMO_ONHOLD_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED insert user in promo on hold table with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
