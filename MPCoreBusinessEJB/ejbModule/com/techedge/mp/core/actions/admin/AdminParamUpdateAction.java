package com.techedge.mp.core.actions.admin;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.ParameterBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminParamUpdateAction {

    @Resource
    private EJBContext    context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager em_log;

    @EJB
    private LoggerService loggerService;

    public AdminParamUpdateAction() {}

    public String execute(String adminTicketId, String requestId, String param, String value, String operation) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");
                userTransaction.commit();
                return ResponseHelper.ADMIN_UPDATE_PARAM_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            if (!operation.equals("N") && !operation.equals("U") && !operation.equals("D")) {
                //Operazione di modifica non valida
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Unrecognized operation");
                userTransaction.commit();
                return ResponseHelper.ADMIN_UPDATE_PARAM_UNRECOGNIZED_OPERATION;
            }

            //Valori per operation (N= NEW, U= Update, D = Delete)

            List<ParameterBean> parameterBeanList = QueryRepository.findParameterBeanByName(em_crud, param);

            System.out.println("parameterBeanList size: " + parameterBeanList.size());

            if (!operation.equals("N")) {

                if (parameterBeanList == null || parameterBeanList.size() != 1) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Parameter not found or homonymy");
                    userTransaction.commit();
                    return ResponseHelper.ADMIN_UPDATE_PARAM_FAILURE;
                }

                for (ParameterBean pb : parameterBeanList) {
                    if (operation.equals("U")) {
                        pb.setValue(value);
                        em_crud.merge(pb);
                    }

                    if (operation.equals("D")) {
                        em_crud.remove(pb);
                    }

                    ParameterBean parameterBeanTSUpdate = QueryRepository.findSingleParameterBeanByName(em_crud, "TS_UPDATE");
                    Date now = new Date();
                    String stringTS = String.valueOf(now.getTime());
                    parameterBeanTSUpdate.setValue(stringTS);
                    em_crud.persist(parameterBeanTSUpdate);

                    userTransaction.commit();
                    return ResponseHelper.ADMIN_UPDATE_PARAM_SUCCESS;
                }
            }
            else {
                if (parameterBeanList != null && parameterBeanList.size() > 0) {
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Parameter exist");
                    userTransaction.commit();
                    return ResponseHelper.ADMIN_UPDATE_PARAM_ALREADY_EXISTS;
                }

                ParameterBean parameterBean = new ParameterBean();
                parameterBean.setName(param);
                parameterBean.setValue(value);
                em_crud.persist(parameterBean);

                ParameterBean parameterBeanTSUpdate = QueryRepository.findSingleParameterBeanByName(em_crud, "TS_UPDATE");
                Date now = new Date();
                String stringTS = String.valueOf(now.getTime());
                parameterBeanTSUpdate.setValue(stringTS);
                em_crud.persist(parameterBeanTSUpdate);

                userTransaction.commit();
                return ResponseHelper.ADMIN_UPDATE_PARAM_SUCCESS;
            }

            return ResponseHelper.ADMIN_UPDATE_PARAM_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin retrieve activity log with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
