package com.techedge.mp.core.actions.admin;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.UserCategoryService;
import com.techedge.mp.core.business.interfaces.AdminRoleEnum;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.PaymentInfo;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.user.User;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.model.PaymentInfoBean;
import com.techedge.mp.core.business.model.UserBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.IdGenerator;
import com.techedge.mp.core.business.utilities.QueryRepository;
import com.techedge.mp.fidelity.adapter.business.CardInfoServiceRemote;
import com.techedge.mp.fidelity.adapter.business.interfaces.DeleteMcCardRefuelingResult;
import com.techedge.mp.fidelity.adapter.business.interfaces.EnumStatusType;
import com.techedge.mp.fidelity.adapter.business.interfaces.PartnerType;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminRefuelingUserRemovePaymentMethodAction {

    @Resource
    private EJBContext          context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager       em;

    @EJB
    private LoggerService       loggerService;

    @EJB
    private UserCategoryService userCategoryService;

    public AdminRefuelingUserRemovePaymentMethodAction() {}

    public String execute(String ticketId, String requestId, String username, Long id, CardInfoServiceRemote cardInfoService) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        try {
            userTransaction.begin();

            // Verifica il ticket
            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em, ticketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();

                return ResponseHelper.ADMIN_RM_PAY_METH_REFUELING_USER_INVALID_TICKET;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())
                    && !CheckRoleHelper.isAuthorizedForRole(this.getClass().getName(), adminTicketBean.getAdminBean(), AdminRoleEnum.ROLE_REFUELING_MANAGER)) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                return ResponseHelper.ADMIN_ROLE_UNAUTHORIZED;
            }

            // Verifica che non esista gi� un utente con quella email
            UserBean userBean = QueryRepository.findUserCustomerByEmail(em, username);

            if (userBean == null) {

                // Esiste gi� un utente con quella email
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not exists");

                userTransaction.commit();

                return ResponseHelper.ADMIN_UPD_PWD_REFUELING_USER_USER_NOT_EXISTS;
            }

            if (!userBean.getUserType().equals(User.USER_TYPE_REFUELING) && !userBean.getUserType().equals(User.USER_TYPE_REFUELING_NEW_ACQUIRER)
                    && !userBean.getUserType().equals(User.USER_TYPE_REFUELING_OAUTH2)) {

                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "User not refueling");

                userTransaction.commit();

                return ResponseHelper.ADMIN_RM_PAY_METH_REFUELING_USER_USER_NOT_REFUELING;
            }
            
            /*
            Boolean found = false;
            if (userBean.getPaymentData() != null) {
                for (PaymentInfoBean item : userBean.getPaymentData()) {
                    if (item.getId() == id && !item.getStatus().equals(PaymentInfo.PAYMENTINFO_STATUS_CANCELED)) {
                        item.setStatus(PaymentInfo.PAYMENTINFO_STATUS_CANCELED);
                        found = Boolean.TRUE;
                    }
                }
            }

            if (!found) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Payment ID to cancel not found");

                userTransaction.commit();

                return ResponseHelper.ADMIN_RM_PAY_METH_REFUELING_USER_FAILURE;
            }
            */
            
            PaymentInfoBean paymentInfoBeanToRemove = userBean.removePaymentInfo(id, PaymentInfo.PAYMENTINFO_TYPE_CREDIT_CARD);
            if (paymentInfoBeanToRemove == null) {
                
                // Controlla se si tratta di un metodo di pagamento di tipo Multicard
                paymentInfoBeanToRemove = userBean.findPaymentInfoBean(id, PaymentInfo.PAYMENTINFO_TYPE_MULTICARD);
                
                if (paymentInfoBeanToRemove == null) {
                    
                    // Payment info non trovato
                    this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "PaymentInfo not found");
                    
                    userTransaction.commit();
                    
                    return ResponseHelper.ADMIN_RM_PAY_METH_REFUELING_USER_FAILURE;
                }
                else {
                    
                    System.out.println("Eliminazione metoto di pagamento Multicard");
                    
                    Date now = new Date();
                    
                    PartnerType partnerType   = PartnerType.EM;
                    String operationId        = new IdGenerator().generateId(16).substring(0, 32);
                    String userId             = userBean.getPersonalDataBean().getSecurityDataEmail();
                    Long requestTimestamp     = now.getTime();
                    String mcCardDpan         = paymentInfoBeanToRemove.getToken();
                    String serverSerialNumber = paymentInfoBeanToRemove.getCheckBankTransactionID();
                    
                    DeleteMcCardRefuelingResult deleteMcCardRefuelingResult = cardInfoService.deleteMcCardRefueling(
                            operationId,
                            userId,
                            partnerType,
                            requestTimestamp,
                            mcCardDpan,
                            serverSerialNumber);
                    
                    if (deleteMcCardRefuelingResult == null) {
                        
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", operationId, null, "deleteMcCardRefuelingResult null");
                        
                        userTransaction.commit();
                        
                        return ResponseHelper.ADMIN_RM_PAY_METH_REFUELING_USER_FAILURE;
                    }
                    
                    if (!deleteMcCardRefuelingResult.getStatus().equals(EnumStatusType.SUCCESS.value())) {
                        
                        /* Codici di ritorno
                        00000 Nessun errore
                        00001 Parametri in input errati
                        00106 Partner Type non valido
                        00113 Utente non presente
                        00109 PAN non trovato
                        99001 Generic error 
                        *
                        */
                        
                        this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", operationId, null, "deleteMcCardRefuelingResult " + deleteMcCardRefuelingResult.getCode());
                        
                        String response = ResponseHelper.ADMIN_RM_PAY_METH_REFUELING_USER_FAILURE;
                        
                        String code = deleteMcCardRefuelingResult.getCode();
                        
                        if (code.equals("00001")) {
                            response = ResponseHelper.ADMIN_RM_PAY_METH_REFUELING_USER_INVALID_PARAMETERS;
                        }
                        if (code.equals("00106")) {
                            response = ResponseHelper.ADMIN_RM_PAY_METH_REFUELING_USER_INVALID_PARTNER_TYPE;
                        }
                        if (code.equals("00113")) {
                            response = ResponseHelper.ADMIN_RM_PAY_METH_REFUELING_USER_USER_NOT_EXISTS;
                        }
                        if (code.equals("00109")) {
                            response = ResponseHelper.ADMIN_RM_PAY_METH_REFUELING_USER_PAN_NOT_FOUND;
                        }
                        
                        userTransaction.commit();
                        
                        return response;
                    }
                }
            }
            
            // Salva l'utente su db
            em.merge(userBean);
            
            userTransaction.commit();

            return ResponseHelper.ADMIN_RM_PAY_METH_REFUELING_USER_SUCCESS;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED user creation with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }
    }
}
