package com.techedge.mp.core.actions.admin;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.EJBContext;
import javax.ejb.EJBException;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.SystemException;
import javax.transaction.UserTransaction;

import com.techedge.mp.core.business.LoggerService;
import com.techedge.mp.core.business.ReconciliationService;
import com.techedge.mp.core.business.interfaces.ErrorLevel;
import com.techedge.mp.core.business.interfaces.ResponseHelper;
import com.techedge.mp.core.business.interfaces.reconciliation.ReconciliationDetail;
import com.techedge.mp.core.business.model.AdminTicketBean;
import com.techedge.mp.core.business.utilities.CheckRoleHelper;
import com.techedge.mp.core.business.utilities.QueryRepository;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
@LocalBean
public class AdminReconcileDetailAction {

    @Resource
    private EJBContext            context;

    @PersistenceContext(unitName = "CrudPU")
    private EntityManager         em_crud;

    @PersistenceContext(unitName = "LogPU")
    private EntityManager         em_log;

    @EJB
    private LoggerService         loggerService;

    final String                  reconciliationServiceJndi = "java:app/MPCoreBusinessEJB/ReconciliationService!com.techedge.mp.core.business.ReconciliationService";

    private ReconciliationService reconciliationService     = null;

    public AdminReconcileDetailAction() {}

    public ReconciliationDetail execute(String adminTicketId, String requestId, String transactionID) throws EJBException {

        UserTransaction userTransaction = context.getUserTransaction();

        this.reconciliationService = (ReconciliationService) context.lookup(reconciliationServiceJndi);

        try {
            userTransaction.begin();

            AdminTicketBean adminTicketBean = QueryRepository.findAdminTicketById(em_crud, adminTicketId);

            if (adminTicketBean == null || !adminTicketBean.isValid()) {

                // Ticket non valido
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Invalid ticket");

                userTransaction.commit();
                ReconciliationDetail reconciliationDetailResponse = new ReconciliationDetail();
                reconciliationDetailResponse.setStatusCode(ResponseHelper.ADMIN_RECONCILE_INVALID_TICKET);

                return reconciliationDetailResponse;
            }

            if (!CheckRoleHelper.isAuthorized(this.getClass().getName(), adminTicketBean.getAdminBean())) {
                this.loggerService.log(ErrorLevel.INFO, this.getClass().getSimpleName(), "execute", requestId, null, "Admin not authotized (not correct role)");

                userTransaction.commit();

                ReconciliationDetail reconciliationDetailResponse = new ReconciliationDetail();
                reconciliationDetailResponse.setStatusCode(ResponseHelper.ADMIN_ROLE_UNAUTHORIZED);

                return reconciliationDetailResponse;
            }

            userTransaction.commit();

            ReconciliationDetail reconciliationDetailResponse = this.reconciliationService.getReconciliationDetail(adminTicketId, requestId, transactionID);

            return reconciliationDetailResponse;

        }
        catch (Exception ex2) {

            try {
                userTransaction.rollback();
            }
            catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            catch (SystemException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            String message = "FAILED admin reconcile detail with message (" + ex2.getMessage() + ")";
            this.loggerService.log(ErrorLevel.ERROR, this.getClass().getSimpleName(), "execute", requestId, null, message);

            throw new EJBException(ex2);
        }

    }
}
